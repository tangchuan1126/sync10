package com.cwc.app.api;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.helpCenter.DoubleCatalogNameException;
import com.cwc.app.exception.helpCenter.DoubleTopicNameException;
import com.cwc.app.exception.helpCenter.HaveTopicCatalogException;
import com.cwc.app.exception.helpCenter.NotBottomCatalogException;
import com.cwc.app.exception.helpCenter.ParentCatalogException;
import com.cwc.app.floor.api.FloorHelpCenterMgr;
import com.cwc.app.iface.HelpCenterIFace;
import com.cwc.app.lucene.HelpCenterTopicIndexMgr;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;
import com.cwc.util.Tree;

public class HelpCenterMgr implements HelpCenterIFace
{
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorHelpCenterMgr fhctm;

	
	public DBRow[] getHelpCenterCatalogTree()
		throws Exception
	{
		try
		{
			Tree tree = new Tree(ConfigBean.getStringValue("help_center_catalog"));
			DBRow rows[] = tree.getTree();
			return(rows);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getHelpCenterCatalogTree",log);
		}
	}
		
	public DBRow[] getTopicsByCid(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fhctm.getTopicsByCid(cid, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getTopicsByCid",log);
		}
	}
	
	public DBRow[] getTopicsByCidSortAsc(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fhctm.getTopicsByCidSortAsc(cid, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getTopicsByCidSortAsc",log);
		}
	}
		
	public DBRow getDetailTopicsByHTCID(long hct_id)
		throws Exception
	{
		try
		{
			return(fhctm.getDetailTopicsByHTCID(hct_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailTopicsByHTCID",log);
		}
	}
		
	public DBRow[] getCatalogByParentid(long parentid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fhctm.getCatalogByParentid(parentid,null));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getCatalogByParentid",log);
		}
	}
	
	/**
	 * 添加帮助中心分类
	 */
	public long addCatalog(HttpServletRequest request) 
		throws DoubleCatalogNameException,Exception 
	{
		try 
		{
			
			
			
			
			String title=StringUtil.getString(request,"title");
			long parentid=StringUtil.getLong(request,"parentid");
			
			DBRow detailHelpCatalog=fhctm.getCatalogByTitle(title,parentid);
			
			if(detailHelpCatalog!=null)
			{
				throw new DoubleCatalogNameException();
			}
			
			long parentId=StringUtil.getLong(request,"parentid");
			DBRow[] sonCatalog=fhctm.getCatalogByParentidOrderSort(parentid);
			
			checkOrderSort(sonCatalog);
			
			int sort;
			if(sonCatalog.length==0)
			{
				sort=1;
			}
			else
			{
				sort=sonCatalog.length+1;
			}
			
			DBRow dbrow=new DBRow();
			dbrow.add("parentid",parentId);
			dbrow.add("title",title);
			dbrow.add("sort",sort);
			long id=fhctm.addCatalog(dbrow);
			
			return (id);
		}
		catch (DoubleCatalogNameException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addCatalog",log);
		}
	}
	/**
	 * 根据ID获得帮助中心分类
	 */
	public DBRow getDetailCatalog(HttpServletRequest request) 
		throws Exception {
		try 
		{
			long id=StringUtil.getLong(request,"id");
			DBRow detail=fhctm.getDetailCatalogById(id);
			
			return (detail);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getCatalog",log);
		}
	}
	/**
	 * 根据ID获得分类，page使用
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailCatalog(long id) 
		throws Exception 
	{
		try 
		{
			DBRow detail=fhctm.getDetailCatalogById(id);
			
			return (detail);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getCatalog",log);
		}
	}
	
	/**
	 * 修改帮助中心分类
	 */
	public void modCatalog(HttpServletRequest request) 
		throws DoubleCatalogNameException,Exception 
	{
		try 
		{
			long id = StringUtil.getLong(request,"id");
			String title = StringUtil.getString(request,"title");
			long parentid = StringUtil.getLong(request,"parentid");
			
			DBRow detailHelpCatalog = fhctm.getCatalogByTitle(title,parentid);
			if(detailHelpCatalog!=null&&detailHelpCatalog.get("id",0)!=id)
			{
				throw new DoubleCatalogNameException();
			}
			//更改帮助中心分类
			DBRow rowo = new DBRow();
			rowo.add("title",title);
			fhctm.modCatalog(id, rowo);
		}
		catch (DoubleCatalogNameException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modCatalog",log);
		}
	}
	
	/**
	 * 修改帮助中心分类顺序
	 */
	public void modCatalogSort(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long parentid=StringUtil.getLong(request,"parentid");
			long id=StringUtil.getLong(request,"id");
			String type=StringUtil.getString(request,"type");
			DBRow[] sonCatalog=fhctm.getCatalogByParentidOrderSort(parentid);
			int nowsort = 0;
			
			for(int i=0;i<sonCatalog.length;i++)
			{
				if(sonCatalog[i].get("id",0)==id)
				{
					nowsort=i;//现在的位置(数组下标)
					break;
				}
			}
			
			DBRow temp=sonCatalog[nowsort];
			
			if (nowsort>0) 
			{
				if (type.equals("up")) {
					sonCatalog[nowsort] = sonCatalog[nowsort - 1];
					sonCatalog[nowsort - 1] = temp;
				}
			}
			if (nowsort<sonCatalog.length-1) 
			{
				if (type.equals("down")) {
					sonCatalog[nowsort] = sonCatalog[nowsort + 1];
					sonCatalog[nowsort + 1] = temp;
				}
			}
			
			checkOrderSort(sonCatalog);
			
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"modCatalogSort",log);
		}
	}
	
	/**
	 * 删除帮助中心分类
	 */
	public void delCatalog(HttpServletRequest request)
		throws ParentCatalogException, Exception 
	{
		try 
		{
			long id = StringUtil.getLong(request,"id");
			long parentid = StringUtil.getLong(request,"parentid");
			
			DBRow[] sonCatalog = fhctm.getCatalogByParentid(id,null);
			
			if(sonCatalog.length!=0)
			{
				throw new ParentCatalogException();
			}
			
			//查询分类中是否有文章
			DBRow[] topics = fhctm.getTopicsByCid(id,null);
			if(topics.length!=0)
			{
				throw new HaveTopicCatalogException();
			}
			
			fhctm.delCatalog(id);
			
			DBRow[] brotherCatalog = fhctm.getCatalogByParentidOrderSort(parentid);
			checkOrderSort(brotherCatalog);
		}
		catch (ParentCatalogException e)
		{
			throw e;
		}
		catch(HaveTopicCatalogException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delCatalog",log);
		}
	}
	
	/**
	 * 子级分类检查sort参数
	 * @param sonCatalog
	 * @throws Exception
	 */
	private void checkOrderSort(DBRow[] sonCatalog) 
		throws Exception
	{
		try {
			for(int i=0;i<sonCatalog.length;i++)
			{
				DBRow paras = new DBRow();
				paras.add("sort",i+1);
				fhctm.modCatalog(sonCatalog[i].get("id", 01), paras);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"checkOrderSort",log);
		}
	}
	
	
	/**
	 * 获得文章列表
	 */
	public DBRow[] getTopic(long parentid,PageCtrl pc) 
		throws Exception 
	{
		try
		{
			ArrayList<String> cids = new ArrayList<String>();
			
			cids.add(String.valueOf(parentid));
	
			return (fhctm.getTopics(parentid, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTopic",log);
		}
	}
	
	/**
	 * 添加文章
	 */
	public long addTopic(HttpServletRequest request)
	throws DoubleTopicNameException, Exception 
	{
		try {
			String title = StringUtil.getString(request,"title");
			String content = StringUtil.getString(request,"content");
			long cid=StringUtil.getLong(request,"catalog_id");
			
//			DBRow detailDBrow=fhctm.getTopicByTitle(title, cid);
//			if(detailDBrow!=null)
//			{
//				throw new DoubleTopicNameException();
//			}
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String author=adminLoggerBean.getAccount();
			
			String last_mod_date=DateUtil.NowStr();
			
			//文章图片
			String imgs[] = StringUtil.regMatchers(content, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			for(int i=0;i<imgs.length;i++)
			{
				String img=imgs[i].substring(imgs[i].lastIndexOf("/")+1); 
				if(img.indexOf("_topic")!=-1)
				{
					String ulr=Environment.getHome()+"upl_imags_tmp/"+img;
					String url=Environment.getHome()+"administrator/help_center/upload_helpcenter_img/"+img;
					FileUtil.moveImg(ulr,url);
				}	
			}
			
			
			//FileUtil.moveImg(Environment.getHome()+"."+ConfigBean.getStringValue("upl_imags_tmp")+row.getString("img_b"), Environment.getHome()+"."+ConfigBean.getStringValue("upload_pro_img")+row.getString("img_b"));
			
			DBRow dbrow=new DBRow();
			dbrow.add("title",title);
			dbrow.add("content",content.replaceAll("upl_imags_tmp","administrator/help_center/upload_helpcenter_img"));
			dbrow.add("cid",cid);
			dbrow.add("author",author);
			dbrow.add("last_mod_date",last_mod_date);
			dbrow.add("issue",0);
			long hct_id=fhctm.addTopic(dbrow);
			
			//添加索引
			String text= Html2Text(content);
						
			HelpCenterTopicIndexMgr.getInstance().addIndex(hct_id,cid,title, text, last_mod_date,author);
			
			return (hct_id);
		}
		catch (DoubleTopicNameException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addTopic",log);
		}
	}
	
	/**
	 * 文章发布
	 */
	public String[] modTopicIssue(int issue,String ids,AdminLoginBean adminLoggerBean) 
		throws Exception 
	{
		try 
		{
			DBRow para=new DBRow();
			
			para.add("issue",issue);
			
			String post_date=DateUtil.NowStr();
			para.add("post_date",post_date);
			
			
			String promulgator=adminLoggerBean.getAccount();
			para.add("promulgator",promulgator);
			
			String[] id=ids.split(",");
			fhctm.modTopicIssue(id, para);
			return (id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modTopicIssue",log);
		}
	}
	
	/**
	 * 文章取消发布
	 */
	public String[] modTopicCancelIssue(int issue,String ids,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			DBRow para=new DBRow();
			
			para.add("issue",issue);
			
			String post_date=DateUtil.NowStr();
			para.add("post_date",post_date);

			String promulgator=adminLoggerBean.getAccount();
			para.add("promulgator",promulgator);
			
			
			String[] id=ids.split(",");
			fhctm.modTopicIssue(id, para);
			return (id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modTopicCancelIssue",log);
		}
	}
	
	/**
	 * 删除文章
	 */
	public void delTopic(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long hct_id=StringUtil.getLong(request,"hct_id");
			
			//删除索引(先删除索引)
			HelpCenterTopicIndexMgr.getInstance().deleteIndex(hct_id);
			
			String content = fhctm.getDetailTopicsByHTCID(hct_id).getString("content");
			String imgs[] = StringUtil.regMatchers(content, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			
			for(int i=0;i<imgs.length;i++)
			{
				String img=imgs[i].substring(imgs[i].lastIndexOf("/")+1); 
				if(img.indexOf("_topic")!=-1)
				{
					String url=Environment.getHome()+"administrator/help_center/upload_helpcenter_img/"+img;
					FileUtil.delFile(url);
				}
			}
			
			fhctm.delTopic(hct_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delTopic",log);
		}
	}
	
	/**
	 * 修改文章
	 */
	public void modTopic(HttpServletRequest request)
		throws DoubleTopicNameException, Exception 
	{
		try 
		{
			String title = StringUtil.getString(request,"title");
			String content = StringUtil.getString(request,"content");
			long cid = StringUtil.getLong(request,"catalog_id");
			long hct_id = StringUtil.getLong(request,"hct_id");
			String author = StringUtil.getString(request,"author");
			String oldcontent = fhctm.getDetailTopicsByHTCID(hct_id).getString("content");
//			DBRow detailDBrow=fhctm.getTopicByTitle(title, cid);
//			if(detailDBrow!=null&&detailDBrow.get("hct_id",0)!=hct_id)
//			{
//				throw new DoubleTopicNameException();
//			}
			//文章图片
			String imgs[] = StringUtil.regMatchers(content, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			
			String oldimgs[] = StringUtil.regMatchers(oldcontent, "<\\s*img\\s+(alt=\"\"\\s+src=\"([^\"]*)\"[^>]*)\\s*/>", 2);
			HashMap newimg=new HashMap();
			
			//移动文章新图片
			for(int i=0;i<imgs.length;i++)
			{
				String img=imgs[i].substring(imgs[i].lastIndexOf("/")+1);
				if(img.indexOf("_topic")!=-1)
				{
					String ulr=Environment.getHome()+"upl_imags_tmp/"+img;
					String url=Environment.getHome()+"administrator/help_center/upload_helpcenter_img/"+img;
					newimg.put(img,img);
					FileUtil.moveImg(ulr,url);
				}
			}
			
			//修改后原本有，现在没有引用的图片删除
			for(int j=0;oldimgs!=null&&j<oldimgs.length;j++)
			{
				String oldimg=oldimgs[j].substring(oldimgs[j].lastIndexOf("/")+1);
				if(!newimg.containsKey(oldimg))
				{
					FileUtil.delFile(Environment.getHome()+"administrator/help_center/upload_helpcenter_img/"+oldimg);
				}
			}
			
			String last_mod_date=DateUtil.NowStr();
			
			DBRow dbrow=new DBRow();
			dbrow.add("title",title);
			dbrow.add("content",content.replaceAll("upl_imags_tmp","administrator/help_center/upload_helpcenter_img"));
			dbrow.add("last_mod_date",last_mod_date);
			fhctm.modTopic(hct_id, dbrow);
			
			
			//修改索引
			String text=Html2Text(content);
			HelpCenterTopicIndexMgr.getInstance().updateIndex(hct_id,cid, title, text, last_mod_date,author);
			
			
		} 
		catch (DoubleTopicNameException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modTopic",log);
		}
		
	}
	
	/**
	 * 分类文章合并
	 */
	public void modTopicCatalog(HttpServletRequest request)
		throws NotBottomCatalogException, Exception 
	{
		try 
		{
			long catalog_id=StringUtil.getLong(request,"catalog_id");
			
			DBRow[] sonCatalog=fhctm.getCatalogIsParent(catalog_id);
			
			if(catalog_id==0||sonCatalog.length!=0)
			{
				throw new NotBottomCatalogException();
			}
			
			String[] ids=StringUtil.getString(request,"hct_id").split(",");
			
			DBRow para=new DBRow();
			para.add("cid",catalog_id);
			
			fhctm.modTopicCatalog(ids, para);
			
			for(int i=0;i<ids.length;i++)
			{
				long hct_id = Long.parseLong(ids[i]);
				
				DBRow topicdetail = fhctm.getDetailTopicsByHTCID(hct_id);
				String title=topicdetail.getString("title");
				String last_mod_date=topicdetail.getString("last_mod_date");
				String author=topicdetail.getString("author");
				
				String text = this.Html2Text(topicdetail.getString("content"));
				HelpCenterTopicIndexMgr.getInstance().updateIndex(hct_id,catalog_id, title, text, last_mod_date,author);
			}
			
		} 
		catch (NotBottomCatalogException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modTopicCatalog",log);
		}
	}
	
	/**
	 * 上传文件
	 * @param request
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public String uploadTopicImage(HttpServletRequest request,HttpServletResponse response)
		throws Exception
	{
		String msg = "";
	
		try
		{	
			String callback = request.getParameter("CKEditorFuncNum");//CKEditor回调函数参数，必须有
			String permitFile = "gif,jpg";
			
			response.setCharacterEncoding("utf-8");
			//PrintWriter out = response.getWriter();
			
			TUpload upload = new TUpload();
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String author=adminLoggerBean.getAccount();
			
			upload.setFileName(String.valueOf(new Date().getTime())+"_"+author+"_topic");
			upload.setRootFolder("/upl_imags_tmp/");
			upload.setPermitFile(permitFile);
			
			String url="";
			int flag = upload.upload(request);
			
			if (flag==2)
			{
				msg = "只能上传:"+permitFile;
			}
			else if (flag==1)
			{
				msg = "上传出错";
			}
			else
			{
				 
				String path = request.getContextPath();  
				url="../../upl_imags_tmp/"+upload.getFileName();
				msg ="";
			}
//			out.println("<script type=\"text/javascript\">");  
//			out.println("window.parent.CKEDITOR.tools.callFunction("+callback + ",'"+url+ "','"+msg+"'" + ")");  
//			out.println("</script>");  
			String result="<script type=\"text/javascript\">"+"window.parent.CKEDITOR.tools.callFunction("+callback + ",'"+url+ "','"+msg+"'" + ")"+"</script>";
			return(result);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"uploadTopicImage",log);
		}
	}
	
	/**
	 * 根据索引搜索文章
	 */
	public DBRow[] getSearchTopics4CT(String key, PageCtrl pc) 
		throws Exception 
	{
		return (HelpCenterTopicIndexMgr.getInstance().getSearchResults(key, pc));
	}
	
	
	/**
	 * 清除文本内特定形式字符
	 * @param txt
	 * @param entity
	 * @param replace
	 */
	private void repaceEntities ( StringBuffer txt,String entity,String replace)
    {
        int pos=-1;
        while(-1!=(pos=txt.indexOf(entity)))
        {
            txt.replace(pos,pos+entity.length(),replace);
        }
    }
	
	/**
	 * 过滤html标签
	 * @param inputString
	 * @return
	 */
	public String Html2Text(String inputString) 
	{
        String htmlStr = inputString; //含html标签的字符串
        String textStr ="";
	      java.util.regex.Pattern p_script;
	      java.util.regex.Matcher m_script;
	      java.util.regex.Pattern p_style;
	      java.util.regex.Matcher m_style;
	      java.util.regex.Pattern p_html;
	      java.util.regex.Matcher m_html;
  
	      try 
	      {
	    	  String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>"; //定义script的正则表达式{或<script[^>]*?>[\\s\\S]*?<\\/script> }
	    	  String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>"; //定义style的正则表达式{或<style[^>]*?>[\\s\\S]*?<\\/style> }
	          String regEx_html = "<[^>]+>"; //定义HTML标签的正则表达式
	     
	          p_script = Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE);
	          m_script = p_script.matcher(htmlStr);
	          htmlStr = m_script.replaceAll(""); //过滤script标签
	
	          p_style = Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE);
	          m_style = p_style.matcher(htmlStr);
	          htmlStr = m_style.replaceAll(""); //过滤style标签
	     
	          p_html = Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE);
	          m_html = p_html.matcher(htmlStr);
	          htmlStr = m_html.replaceAll(""); //过滤html标签
	     
	          StringBuffer txt = new StringBuffer(htmlStr);
	          repaceEntities(txt,"&amp;","&");
	          repaceEntities(txt,"&lt;","<");       
	          repaceEntities(txt,"&gt;",">");
	          repaceEntities(txt,"&quot;","\"");
	          repaceEntities(txt,"&nbsp;","");      
	          String content=txt.toString();
	          
	          Pattern pattern = Pattern.compile("[\\s\\p{Zs}]");//过滤空格换行
	          Matcher re = pattern.matcher(content);
	          content=re.replaceAll("");
	          
	          textStr = htmlStr;
	     
	      }
	      catch(Exception e) 
	      {
	               System.err.println("Html2Text: " + e.getMessage());
	      }
	  
	      return textStr;//返回文本字符串
       }
	
	/**
	 *  根据索引查询文章（前台，只显示已发布的）
	 */
	public DBRow[] getSearchTopics4CTUse(String key, PageCtrl pc)
		throws Exception 
	{
		return (HelpCenterTopicIndexMgr.getInstance().getSearchUseResults(key, pc));
	}
	/**
	 * 前台使用文章列表，首页，根据分类查询
	 */
	public DBRow[] getTopicsUse(long cid, PageCtrl pc) 
		throws Exception 
	{	
//		ArrayList<String> cids=new ArrayList<String>();
//		
//		cids.add(String.valueOf(cid));
//	    
//		if (cid!=0) 
//	    {
//			DBRow[] sonCatalog = fhctm.getCatalogByParentidOrderSort(cid);
//			for (int i = 0; i < sonCatalog.length; i++) 
//			{
//				cids.add(sonCatalog[i].getString("id"));
//			}
//		}
		
		return (fhctm.getTopicsUse(cid, pc));
	}
	
	public void setFhctm(FloorHelpCenterMgr fhctm)
	{
		this.fhctm = fhctm;
	}

	

	

//	AdminMgr adminMgr = new AdminMgr();
//    AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StrUtil.getSession(request));
//    adminLoggerBean.getAccount();DateUtil.NowStr()

}








