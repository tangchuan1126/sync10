package com.cwc.app.api.gql;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.zwb.UploadAndDownloadPDF;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.gql.FloorPrintLabelMgrGql;
import com.cwc.app.floor.api.zj.FloorSpaceResourcesRelationMgr;
import com.cwc.app.floor.api.zr.FloorRelativeFileMgrZr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.checkin.PdfUtilIFace;
import com.cwc.app.iface.gql.PrintLabelMgrIfaceGql;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.FileMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.service.file.action.zr.ImgToPdfMgr;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.fr.base.core.UUID;
import com.lowagie.text.DocumentException;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class PrintLabelMgrGql implements PrintLabelMgrIfaceGql {
	private FloorCheckInMgrZwb floorCheckInMgrZwb;
	private FloorRelativeFileMgrZr floorRelativeFileMgrZr;
	private FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr;
	private AdminMgrIFace adminMgr;
	private SQLServerMgrIFaceZJ sqlServerMgrZJ;
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private CheckInMgrIfaceZr checkInMgrZr;
	private FloorPrintLabelMgrGql floorPrintLabelMgrGql;
	private CompressPdfToZip pdfToZip;
	private FileMgrIfaceZr fileMgrZr;
	private ImgToPdfMgr imgToPdfMgr;
	private UploadAndDownloadPDF uploadAndDownloadPDF;// f上传下载类
	private AndroidPrintMgrIfaceZr androidPrintMgrIfaceZr;
	private PdfUtilIFace pdfUtilIFace = null;
	protected String downLoadFileUrl = "";
	protected String uploadFileUrl = "";

	public void setPdfUtilIFace(PdfUtilIFace pdfUtilIFace) {
		this.pdfUtilIFace = pdfUtilIFace;
	}

	// private UploadAndDownloadPDFForFtpServer
	// uploadAndDownloadPDFForFtpServer;
	// 保存的路径
	private static String PDFPath = Environment.getHome().replace("\\", "/")
			+ "upl_pdf_tmp/";
	// 下载下来的pdf临时存放的路径
	private static String Download_Pdf_Path = Environment.getHome().replace(
			"\\", "/")
			+ "vvme_pdf/";
	// 下载下来的pdf临时存放的路径
	private static String Zip_Pdf_Path = Environment.getHome().replace("\\",
			"/")
			+ "zip_pdf_temp/";
	// ftp上传的相关参数
	String proxyHost = "";
	String proxyPort = "";
	String proxyUser = "";
	String proxyPassword = "";
	// task processing图片 一页的数目
	int imageOfTaskProcessingNum = 4;
	static Logger log = Logger.getLogger("ACTION");

	/**
	 * 根据条件查询是否转pdf，是否需要发送请求
	 * 
	 * @param conver_pdf
	 * @return
	 * @throws Exception
	 */
	public DBRow[] queryPrintTaskBy(int conver_pdf) throws Exception {
		try {
			DBRow[] rows = floorCheckInMgrZwb.queryPrintTaskBy(conver_pdf);
			return rows;
		} catch (Exception e) {
			throw new SystemException(e,
					"PrintLabelMgrGql.queryPrintTaskBy(conver_pdf)", log);
		}
	}

	/**
	 * 根据print_task_id更改print_task状态
	 * 
	 * @param print_task_id
	 * @return
	 * @throws Exception
	 */
	public long updateConvertStatusByPintTaskId(long print_task_id)
			throws Exception {
		try {
			floorCheckInMgrZwb.updateConvertStatusByPintTaskId(print_task_id);
			return print_task_id;
		} catch (Exception e) {
			throw new SystemException(
					e,
					"PrintLabelMgrGql.updateConvertStatusByPintTaskId(print_task_id)",
					log);
		}
	}

	/**
	 * 查询masterbol和bol签的url
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getPrintUrlByParam(HttpServletRequest request)
			throws Exception {
		try {
			/*
			 * 1、server页面其他方式创建pdf，传递的参数只有entry_id和detail_id
			 * 2、downLoad页面创建pdf时，传递的参数只有entry_id和detail_id和customer_id
			 */
			long entry_id = StringUtil.getLong(request, "entry_id");
			long detail_id = StringUtil.getLong(request, "detail_id");
			String customer_id = StringUtil.getString(request, "customer_id",
					null);// downLoad页面创建pdf时，传递改参数

			DBRow result = new DBRow();
			String containerNo = "";
			String outSeal = "";
			String door = "";
			DBRow[] rows = floorSpaceResourcesRelationMgr
					.getTaskByEntryIdWithDetailId(entry_id, detail_id);

			if (rows != null && rows.length > 0) {
				DBRow row = rows[0];
				long equipment_id = row.get("equipment_id", 0l);
				int resources_type = row.get("resources_type", 0);
				DBRow equipmentRowSeals = checkInMgrZr
						.getEquipmentSeals(equipment_id);
				String tempOutSeals = equipmentRowSeals.getString("out_seal");
				// row.add("out_seal", tempOutSeals);
				if (resources_type == OccupyTypeKey.DOOR) {
					// row.add("resources_name", row.getString("doorId"));
					door = row.getString("doorId");
				} else if (resources_type == OccupyTypeKey.SPOT) {
					// row.add("resources_name", row.getString("yc_no"));
					door = row.getString("yc_no");
				}

				String number = row.get("number", "");
				int number_type = row.get("number_type", 0);
				String companyId = row.get("company_id", "");
				String customerId = row.get("customer_id", "");
				String order = row.get("order_no", "");
				containerNo = equipmentRowSeals.getString("equipment_number");
				outSeal = tempOutSeals;

				/*
				 * 表示该请求是download页面发送的创建pdf的请求，此时要按customer_id参数创建pdf
				 * 若是server页面发送的请求，只按entry_id和detail_id创建pdf
				 */
				if (customer_id != null && !"".equals(customer_id)) {
					customerId = customer_id;
				}

				if (!"".equals(number)
						&& number_type == ModuleKey.CHECK_IN_LOAD) {
					checkInMgrZwb.deletPdfByLoad(request, entry_id, number,
							customer_id);// 删除load下的所有的pdf
					result = getLoadPrintUrl(number, companyId, customerId,
							number_type, door, outSeal, containerNo, request);// 获取url
					result.add("number", number);
					result.add("number_type", number_type);
				} else if (!"".equals(number)
						&& (number_type == ModuleKey.CHECK_IN_PONO || number_type == ModuleKey.CHECK_IN_ORDER)) {
					checkInMgrZwb.deletPdfByLoad(request, entry_id, number,
							customer_id);// 删除load下的所有的pdf
					result = getOrderPrintUrl(number, companyId, customerId,
							order, number_type, door, outSeal, containerNo,
							request);
					result.add("number", number);
					result.add("number_type", number_type);
				}
			}

			return result;
		} catch (Exception e) {
			throw new SystemException(e,
					"PrintLabelMgrGql.getPrintUrlByParam(request)", log);
		}
	}

	/**
	 * 查询masterbol和bol签的url
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPrintUrlByTime(HttpServletRequest request)
			throws Exception {
		try {
			Long ps_id = StringUtil.getLong(request, "ps_id");

			String startTime = StringUtil.getString(request, "startTime");// 手动按时间段创建pdf
			if (ps_id > 0l) {
				startTime = DateUtil.showUTCTime(
						DateUtil.StrtoYYMMDDHHMMSS(startTime), ps_id);
			}

			// String startTime = "2015-03-10 07:22:07";//手动按时间段创建pdf
			String endTime = StringUtil.getString(request, "endTime");// 手动按时间段创建pdf
			if (ps_id > 0l) {
				endTime = DateUtil.showUTCTime(
						DateUtil.StrtoYYMMDDHHMMSS(endTime), ps_id);
			}

			// ArrayList<DBRow> result=new ArrayList<DBRow>();
			// String containerNo="";
			// String outSeal="";
			// String door = "";
			// if("startTime".equals(endTime)){
			// endTime+=endTime+" 23:59:59";
			// }

			/*
			 * 1、手动按时间段创建pdf,传递的参数只有startTime和endTime
			 */
			DBRow[] rows = floorSpaceResourcesRelationMgr.getTaskByTime(
					startTime, endTime, ps_id);
			/*
			 * DBRow tempResult=new DBRow(),tempOl=null;
			 * if(rows!=null&&rows.length>0){ for (DBRow row : rows) { long
			 * equipment_id = row.get("equipment_id", 0l); int resources_type =
			 * row.get("resources_type", 0); DBRow equipmentRowSeals =
			 * checkInMgrZr.getEquipmentSeals(equipment_id); String tempOutSeals
			 * = equipmentRowSeals.getString("out_seal") ; if(resources_type ==
			 * OccupyTypeKey.DOOR){ door = row.getString("doorId"); }else
			 * if(resources_type == OccupyTypeKey.SPOT){ door =
			 * row.getString("yc_no"); } String number = row.get("number","");
			 * int number_type = row.get("number_type",0); String
			 * companyId=row.get("company_id",""); String
			 * customerId=row.get("customer_id",""); String order =
			 * row.get("order_no","");
			 * containerNo=equipmentRowSeals.getString("equipment_number");
			 * outSeal=tempOutSeals; long entryId=row.get("entry_id",0l); long
			 * detailId=row.get("detail_id",0l);
			 * if(!"".equals(number)&&number_type==ModuleKey.CHECK_IN_LOAD){
			 * checkInMgrZwb
			 * .deletPdfByLoad(request,entryId,number,customerId);//
			 * 删除load下的所有的pdf tempOl =
			 * getLoadPrintUrl(number,companyId,customerId
			 * ,number_type,door,outSeal,containerNo,request);//获取url
			 * tempResult.
			 * add("url",StringUtil.convertDBRowsToJsonString(tempOl));
			 * tempResult.add("number", number); tempResult.add("number_type",
			 * number_type); tempResult.add("entry_id", entryId);
			 * tempResult.add("detail_id", detailId); }else
			 * if(!"".equals(number)&&(number_type==ModuleKey.CHECK_IN_PONO||
			 * number_type==ModuleKey.CHECK_IN_ORDER)){
			 * checkInMgrZwb.deletPdfByLoad
			 * (request,entryId,number,customerId);//删除load下的所有的pdf tempOl =
			 * getOrderPrintUrl
			 * (number,companyId,customerId,order,number_type,door
			 * ,outSeal,containerNo,request);
			 * tempResult.add("url",StringUtil.convertDBRowsToJsonString
			 * (tempOl)); tempResult.add("number", number);
			 * tempResult.add("number_type", number_type);
			 * tempResult.add("entry_id", entryId); tempResult.add("detail_id",
			 * detailId); } result.add(tempResult); } } DBRow[] r =
			 * result.toArray(new DBRow[]{});
			 */
			return rows;
		} catch (Exception e) {
			throw new SystemException(e,
					"PrintLabelMgrGql.getPrintUrlByParam(request)", log);
		}
	}

	/**
	 * 获取签的url
	 * 
	 * @param number
	 * @param companyId
	 * @param customerId
	 * @param number_type
	 * @param door
	 * @param outSeal
	 * @param containerNo
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getLoadPrintUrl(String number, String companyId,
			String customerId, int number_type, String door, String outSeal,
			String containerNo, HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		try {
			String by_time = StringUtil.getString(request, "by_time");// 按时间生成pdf页面使用，gql
																		// 2015/4/24
			boolean flag = false;// 默认不生成pdf（没有masterbol标签，或者masterbol未签字）
									// 按时间生成pdf页面使用，gql gql 2015/4/24
			long entry_id = StringUtil.getLong(request, "entry_id");
			String customer_id = StringUtil.getString(request, "customer_id",
					null);// downLoad页面创建pdf时，传递该参数
			DBRow[] rows = null;
			ArrayList<DBRow> masterBolUrl = new ArrayList<DBRow>();
			ArrayList<DBRow> bolUrl = new ArrayList<DBRow>();

			// 此处所使用的方法以及获取方式，均copy自check_in_select_lableTemplate.jsp.jsp页面获取
			if (customer_id != null && !"".equals(customer_id)) {
				rows = sqlServerMgrZJ.findBillOfLadingTemplateByLoad(number,
						companyId, customer_id, 0, request);// 查询该customer_id条件下的
			} else {
				rows = sqlServerMgrZJ.findBillOfLadingTemplateByLoad(number,
						companyId, "", 0, request);// 查询是不包含customer条件
			}

			rows = checkInMgrZwb.findLableByName(rows, number_type);
			for (int m = 0; m < rows.length; m++) {
				companyId = rows[m].getString("CompanyID");
				customerId = rows[m].getString("CustomerID");
				String master_bol_nos = rows[m].getString("master_bol_nos");
				String order_nos = rows[m].getString("order_nos");
				String path = "";
				String loadNo = number;

				DBRow masterBol = (DBRow) rows[m].get("MasterBOLFormatBAK",
						new DBRow());
				if (!"".equals(masterBol.get("template_path", ""))) {
					// 查询relative_file表参数relative_value和relative_type=2（relative_value字段：entry_id+"_"+loadNo+"_"+master_bol_nos），满足此条件获取其url
					// String
					// relative_value=entry_id+"_"+loadNo+"_"+master_bol_nos;
					// DBRow[] relativeFileRows =
					// floorRelativeFileMgrZr.getRelativeFile(relative_value,
					// 2);
					// if(relativeFileRows!=null && relativeFileRows.length>0){
					path = masterBol.get("template_path", "");
					DBRow dbrow = getPrintUrlSingle(path, entry_id, number,
							companyId, customerId, door, outSeal, containerNo,
							master_bol_nos, order_nos);
					masterBolUrl.add(dbrow);
					flag = true;
					// }
				}

				/*
				 * 1、按时间生成pdf操作时，存在masterbol，并且已经签字时；生成pdf 2、其他操作，不进行限制
				 */
				if (!"yes".equals(by_time) || flag) {
					// bol order不判断签字等，直接获取满足条件url追加
					DBRow Bol = (DBRow) rows[m]
							.get("BOLFormatBAK", new DBRow());
					if (!"".equals(Bol.get("template_path", ""))) {
						path = Bol.get("template_path", "");
						DBRow dbrow = getPrintUrlSingle(path, entry_id, number,
								companyId, customerId, door, outSeal,
								containerNo, master_bol_nos, order_nos);
						bolUrl.add(dbrow);
					}
				}
			}

			result.add("masterBolUrl",
					masterBolUrl.toArray(new DBRow[masterBolUrl.size()]));
			result.add("bolUrl", bolUrl.toArray(new DBRow[bolUrl.size()]));
			return result;
		} catch (Exception e) {
			throw new SystemException(e, "PrintLabelMgrGql.getLoadPrintUrl",
					log);
		}
	}

	/**
	 * getOrderPrintUrl
	 * 
	 * @param number
	 * @return
	 * @throws Exception
	 */
	public DBRow getOrderPrintUrl(String number, String companyId,
			String customerId, String order, int number_type, String door,
			String outSeal, String containerNo, HttpServletRequest request)
			throws Exception {
		DBRow result = new DBRow();
		try {
			long entry_id = StringUtil.getLong(request, "entry_id");
			DBRow[] rows = null;
			ArrayList<DBRow> masterBolUrl = new ArrayList<DBRow>();
			ArrayList<DBRow> bolUrl = new ArrayList<DBRow>();

			// bol order不判断签字等，直接获取满足条件url追加
			if (number_type == ModuleKey.CHECK_IN_ORDER) {
				rows = sqlServerMgrZJ.findBillOfLadingTemplateByOrderNo(
						Long.parseLong(number), companyId, customerId, 0,
						request);
			} else if (number_type == ModuleKey.CHECK_IN_PONO
					&& !"".equals(order)) {
				rows = sqlServerMgrZJ.findBillOfLadingTemplateByOrderNo(
						Long.parseLong(order), companyId, customerId, 0,
						request);
			}

			if (rows != null) {
				rows = checkInMgrZwb.findLableByName(rows, number_type);
				for (int a = 0; a < rows.length; a++) {
					companyId = rows[a].getString("CompanyID");
					customerId = rows[a].getString("CustomerID");
					String master_bol_nos = rows[a].getString("master_bol_nos");

					DBRow Bol = (DBRow) rows[a]
							.get("BOLFormatBAK", new DBRow());
					String path = Bol.get("template_path", "");
					if (!"".equals(path)) {
						path = Bol.get("template_path", "");
						DBRow dbrow = getPrintUrlSingleOrder(number_type, path,
								entry_id, number, companyId, customerId, door,
								outSeal, containerNo, master_bol_nos, order);
						bolUrl.add(dbrow);
					}

				}
			}

			result.add("masterBolUrl",
					masterBolUrl.toArray(new DBRow[masterBolUrl.size()]));
			result.add("bolUrl", bolUrl.toArray(new DBRow[bolUrl.size()]));
			return result;
		} catch (Exception e) {
			throw new SystemException(e, "PrintLabelMgrGql.getOrderPrintUrl",
					log);
		}
	}

	/**
	 * 获取url
	 * 
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public DBRow getPrintUrlSingle(String path, long entry_id, String number,
			String companyId, String customerId, String door, String outSeal,
			String containerNo, String master_bol_nos, String order_nos)
			throws Exception {
		DBRow result = new DBRow();
		try {
			String systenFolder = ConfigBean.getStringValue("systenFolder");
			String json = "[{" + "\"checkDataType\":\"PICKUP\","
					+ "\"number\":\"" + number + "\"," + "\"door_name\":\""
					+ door + "\"," + "\"companyId\":\"" + companyId + "\","
					+ "\"customerId\":\"" + customerId + "\","
					+ "\"master_bol_nos\":\"" + master_bol_nos + "\","
					+ "\"order_nos\":\"" + order_nos + "\"," + "\"checkLen\":1"
					+ "}]";

			String pathUrl = systenFolder + path + "?jsonString=" + json
					+ "&entry_id=" + entry_id + "&out_seal=" + outSeal
					+ "&container_no=" + containerNo + "&isprint=1";
			result.add("printUrl", pathUrl);
			return result;
		} catch (Exception e) {
			throw new SystemException(e, "PrintLabelMgrGql.getPrintUrlSingle",
					log);
		}
	}

	public DBRow getPrintUrlSingleOrder(int number_type, String path,
			long entry_id, String number, String companyId, String customerId,
			String door, String outSeal, String containerNo,
			String master_bol_nos, String order_no) throws Exception {
		DBRow result = new DBRow();
		try {
			String systenFolder = ConfigBean.getStringValue("systenFolder");
			String json = "[{" + "\"checkDataType\":\"PICKUP\","
					+ "\"number\":\""
					+ number
					+ "\","
					+ "\"door_name\":\""
					+ door
					+ "\","
					+ "\"companyId\":\""
					+ companyId
					+ "\","
					+ "\"customerId\":\""
					+ customerId
					+ "\","
					+ "\"master_bol_nos\":\""
					+ master_bol_nos
					+ "\","
					+ "\"order_no\":\""
					+ order_no
					+ "\","
					+ "\"number_type\":\""
					+ number_type
					+ "\","
					+ "\"checkLen\":1" + "}]";

			String pathUrl = systenFolder + path + "?jsonString=" + json
					+ "&entry_id=" + entry_id + "&out_seal=" + outSeal
					+ "&container_no=" + containerNo + "&isprint=1";
			result.add("printUrl", pathUrl);
			return result;
		} catch (Exception e) {
			throw new SystemException(e,
					"PrintLabelMgrGql.getPrintUrlSingleOrder", log);
		}
	}

	/**
	 * counting sheet 追加到pdf 在最后上传pdf
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String addCountingSheetToPdf(HttpServletRequest request)
			throws Exception {
		String message = "false";
		try {
			Long entry_id = StringUtil.getLong(request, "entry_id");
			Long detail_id = StringUtil.getLong(request, "detail_id");
			String customerIdFromRequest = StringUtil.getString(request,
					"customer_id");
			String sessionId = request.getRequestedSessionId();
			int year = DateUtil.getYear();
			String strMonth = DateUtil.getStrCurrMonth();
			int month = Integer.parseInt(strMonth) + 1;
			if (month < 10) {
				strMonth = "0" + String.valueOf(month);
			}
			String directoryPath = "/" + String.valueOf(year) + "/" + strMonth;

			// 如果customid存在 则是create触发
			if (StrUtil.isBlank(customerIdFromRequest)) {
				// 找出对应load
				DBRow[] loadnos = floorSpaceResourcesRelationMgr
						.getTaskByEntryIdWithDetailId(entry_id, detail_id);
				if (loadnos != null && loadnos.length > 0) {
					for (DBRow load : loadnos) {
						String loadno = load.getString("number");
						String custom_id = load.getString("customer_id");
						if (custom_id != null) {
							String[] custom_ids = custom_id.split(",");
							if (custom_ids != null && custom_ids.length > 0) {
								for (String customid : custom_ids) {
									message = subAddCountingSheetToPdf(message,
											entry_id, detail_id, sessionId,
											directoryPath, loadno, customid);
								}
							}
						}
					}
				}
			} else {
				DBRow[] loadnos = floorSpaceResourcesRelationMgr
						.getTaskByEntryIdWithDetailId(entry_id, detail_id);
				if (loadnos != null && loadnos.length > 0) {
					for (DBRow load : loadnos) {
						String loadno = load.getString("number");
						message = subAddCountingSheetToPdf(message, entry_id,
								detail_id, sessionId, directoryPath, loadno,
								customerIdFromRequest);
					}
				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new SystemException(e, "addCountingSheetToPdf", log);
		}
		return message;
	}

	@SuppressWarnings("static-access")
	private String subAddCountingSheetToPdf(String message, Long entry_id,
			Long detail_id, String sessionId, String directoryPath,
			String loadno, String customid) throws Exception, IOException,
			FileNotFoundException, DocumentException {
		File returnfile = null;
		String dnname;
		DBRow[] fileids = floorCheckInMgrZwb.getFileIdByEntryId(entry_id,
				loadno, "", customid);

		/*
		 * step1 添加countingsheet
		 */

		DBRow[] sheets = fileMgrZr.getCheckInCountingSheet(entry_id, detail_id,
				customid);
		if (sheets != null && sheets.length > 0) {
			for (DBRow sheet : sheets) {
				String fileName = fileMgrZr.downloadFromFileServ(
						"upl_imags_tmp/", sheet.get("file_id", 0l), sessionId);
				if (fileName == null || "".equals(fileName)) {
					continue;
				}
				File sheetFile = new File(fileName);

				/*** Local Debug Start ****/
				/*
				 * File sheetFile = null; long fileId = sheet.get("file_id",
				 * 0l); DBRow row = fileMgrZr.getFileByFileId(fileId); if(row !=
				 * null){ String filePath =
				 * Environment.getHome()+File.separator+
				 * "upl_imags_tmp"+File.separator
				 * +File.separator+row.getString("file_path"); sheetFile = new
				 * File(filePath); }
				 */
				/**** Local Debug End ***/

				if (fileids != null && fileids.length > 0) {
					for (DBRow fileid : fileids) {
						dnname = fileid.getString("dn");

						File serverFile = new File(PDFPath + dnname + ".pdf");
						BufferedImage sheetimage = ImageIO.read(sheetFile);
						BufferedImage target = new BufferedImage(
								imgToPdfMgr.width, imgToPdfMgr.height,
								BufferedImage.TYPE_INT_BGR);
						target.getGraphics().drawImage(sheetimage, 0, 0,
								imgToPdfMgr.width, imgToPdfMgr.height, null);
						// 先压缩图片
						File file = imgToPdfMgr.compressImg(PDFPath, target,
								0.5f, false);
						imgToPdfMgr.addContentToPDF(
								serverFile.getAbsolutePath(),
								ImageIO.read(new FileInputStream(file)), false,
								true);
					}
				}
				// 删除临时图片
				System.out.println("=========delete sheet file "
						+ sheetFile.getAbsolutePath() + "============");
				sheetFile.delete();
			}
		}

		/*
		 * step2 添加task proccessing
		 */
		DBRow[] taskSheets = fileMgrZr.getCheckInTaskProcessing(entry_id,
				detail_id);
		ArrayList<BufferedImage> imageList = new ArrayList<BufferedImage>();
		BufferedImage[] buffImageArray = null;

		int subHeight = 0, subWidth = 0;
		int tempIndex = 0, lastLength = 0;
		// 存在就 处理 ---添加---并上传
		if (taskSheets != null && taskSheets.length > 0) {
			// tempIndex=taskSheets.length%imageOfTaskProcessingNum;
			// lastLength=taskSheets.length/imageOfTaskProcessingNum;
			// if(tempIndex<=0){
			// tempIndex=tempIndex+lastLength*imageOfTaskProcessingNum;
			// }
			// 处理图片
			for (int i = 0; i < taskSheets.length; i++) {
				// 处理如果图片超过4张 要重新计算高度
				// if(i>=lastLength*imageOfTaskProcessingNum){
				// subHeight=imgToPdfMgr.height/tempIndex;
				// }else{
				// subHeight=imgToPdfMgr.height/imageOfTaskProcessingNum;
				// }
				// 图片改为固定大小
				subWidth = imgToPdfMgr.a4Width / 2;
				subHeight = imgToPdfMgr.a4Height / 2;
				/*** Local Debug Start ****/
				/*
				 * long fileId = taskSheets[i].get("file_id", 0l); DBRow row =
				 * fileMgrZr.getFileByFileId(fileId); if(row != null){ String
				 * filePath =
				 * Environment.getHome()+File.separator+"upl_imags_tmp"
				 * +File.separator+File.separator+row.getString("file_path");
				 * File sheetFile = new File(filePath);
				 * drawImageBySubHeight(sheetFile, imageList, subWidth-40
				 * ,subHeight-40); }
				 */
				/**** Local Debug End ***/
				String fileName = fileMgrZr.downloadFromFileServ(
						"upl_imags_tmp/", taskSheets[i].get("file_id", 0l),
						sessionId);
				if (fileName != null && !"".equals(fileName)) {
					File sheetFile = new File(fileName);
					drawImageBySubHeight(sheetFile, imageList, subWidth - 40,
							subHeight - 40);
					sheetFile.delete();
				}

			}
			/**
			 * 开始根据图片数进行处理
			 */
			// imagenum>=4
			if (imageList.size() >= imageOfTaskProcessingNum) {
				tempIndex = imageList.size() % imageOfTaskProcessingNum;
				lastLength = imageList.size() / imageOfTaskProcessingNum;
				if (tempIndex == 0) {
					lastLength--;
					tempIndex = imageOfTaskProcessingNum;
				}

				for (int i = 0; i <= lastLength; i++) {
					if (i == lastLength) {
						buffImageArray = imageList.subList(
								i * imageOfTaskProcessingNum,
								i * imageOfTaskProcessingNum + tempIndex)
								.toArray(new BufferedImage[] {});
					} else {
						buffImageArray = imageList.subList(
								i * imageOfTaskProcessingNum,
								(i + 1) * imageOfTaskProcessingNum).toArray(
								new BufferedImage[] {});
					}
					if (buffImageArray != null && buffImageArray.length > 0) {
						if (fileids != null && fileids.length > 0) {
							for (DBRow fileid : fileids) {
								dnname = fileid.getString("dn");
								File serverFile = new File(PDFPath + dnname
										+ ".pdf");
								imgToPdfMgr.addContentToPDF(
										serverFile.getAbsolutePath(),
										buffImageArray, false, subHeight,
										subWidth);
							}
						}
					}
				}
				if (fileids != null && fileids.length > 0) {
					for (DBRow fileid : fileids) {
						dnname = fileid.getString("dn");
						returnfile = new File(PDFPath + dnname + ".pdf");
						message = uploadPdfAndUpdateData(message, entry_id,
								sessionId, customid, returnfile, dnname);
					}
				}
			} else {
				buffImageArray = imageList.toArray(new BufferedImage[imageList
						.size()]);
				// 页数1=<imagenum<4
				if (buffImageArray != null && buffImageArray.length > 0) {
					if (fileids != null && fileids.length > 0) {
						for (DBRow fileid : fileids) {
							dnname = fileid.getString("dn");

							File serverFile = new File(PDFPath + dnname
									+ ".pdf");
							returnfile = imgToPdfMgr.addContentToPDF(
									serverFile.getAbsolutePath(),
									buffImageArray, false, subHeight, subWidth);
							message = uploadPdfAndUpdateData(message, entry_id,
									sessionId, customid, returnfile, dnname);
						}
					}

				} else if (fileids != null && fileids.length > 0) {// 如果需要添加的文件都丢失了则直接上传

					for (DBRow fileid : fileids) {
						dnname = fileid.getString("dn");
						File serverFile = new File(PDFPath + dnname + ".pdf");
						message = uploadPdfAndUpdateData(message, entry_id,
								sessionId, customid, serverFile, dnname);
					}
				}

			}
			// 不存在的话就直接上传
		} else {
			if (fileids != null && fileids.length > 0) {
				for (DBRow fileid : fileids) {
					dnname = fileid.getString("dn");
					File serverFile = new File(PDFPath + dnname + ".pdf");
					message = uploadPdfAndUpdateData(message, entry_id,
							sessionId, customid, serverFile, dnname);
				}
			}
		}

		return message;
	}

	/**
	 * 上传pdf并且更新数据库
	 * 
	 * @param message
	 * @param entry_id
	 * @param sessionId
	 * @param customid
	 * @param returnfile
	 * @param dnname
	 * @return
	 * @throws Exception
	 */
	private String uploadPdfAndUpdateData(String message, Long entry_id,
			String sessionId, String customid, File returnfile, String dnname)
			throws Exception {
		String file_id;
		if (returnfile != null && returnfile.exists()) {
			// 上传
			file_id = UploadAndDownloadPDF.upLoadPDF(uploadFileUrl,
					returnfile.getAbsolutePath(), sessionId, 0);
			if (file_id != null) {
				// 更新数据库
				DBRow db = new DBRow();
				db.add("file_id", file_id);

				// boolean
				// succ=uploadAndDownloadPDFForFtpServer.uploadPdfToFtpserver(returnfile,
				// directoryPath, proxyHost, proxyPort, proxyUser,
				// proxyPassword);
				if (true) {
					db.add("flag", YesOrNotKey.YES);
					message = "true";
					floorCheckInMgrZwb.updatePrintedLabel(entry_id, dnname,
							customid, db);
				}

			} else {
				message = "false";
			}
			// 删除本地的pdf文件
			System.out.println("=============try to delete file :"
					+ returnfile.getAbsolutePath() + "================");
			returnfile.delete();
		}
		message = "true";
		return message;
	}

	/**
	 * 压缩图片
	 * 
	 * @param taskSheet
	 * @param imageList
	 * @param subHeight
	 * @param i
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	@SuppressWarnings("static-access")
	private void drawImageBySubHeight(File taskSheet,
			ArrayList<BufferedImage> imageList, int subWidth, int subHeight)
			throws Exception, IOException, FileNotFoundException {

		BufferedImage sheetimage = ImageIO.read(taskSheet);
		BufferedImage target = new BufferedImage(subWidth, subHeight,
				BufferedImage.TYPE_BYTE_INDEXED);
		target.getGraphics().drawImage(sheetimage, 0, 0, subWidth, subHeight,
				null);
		// File jpgFile = new File(PDFPath + "temp.jpg");
		String tempFileName = UUID.randomUUID().toString() + ".jpg";
		File jpgFile = new File(PDFPath + tempFileName);
		FileOutputStream out = new FileOutputStream(jpgFile); // 输出到文件流
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		encoder.encode(target);

		imageList.add(target);

		out.close();
		jpgFile.delete();
	}

	/**
	 * 将trailer loader 和freight counted的值记录到label_template_detail表 记录页面上trailer
	 * loader 和freight counted勾选状态
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long editLabelTempDetail(HttpServletRequest request)
			throws Exception {
		try {
			long entryId = StringUtil.getLong(request, "entry_id");
			String loadNo = StringUtil.getString(request, "load_no");
			int trailerLoader = StringUtil.getInt(request, "trailer_loader");
			int freightCounted = StringUtil.getInt(request, "freight_counted");
			DBRow row = new DBRow();
			row.add("entry_id", entryId);
			row.add("load_no", loadNo);
			row.add("trailer_loader", trailerLoader);
			row.add("freight_counted", freightCounted);
			floorPrintLabelMgrGql.deleteLabelTempDetail(entryId, loadNo);
			long id = floorPrintLabelMgrGql.addLabelTempDetail(row);
			return id;
		} catch (Exception e) {
			throw new SystemException(e, "editLabelTempDetail", log);
		}
	}

	/**
	 * 查询label_template_detail表信息
	 * 
	 * @param entryId
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow findLabelTempDetail(String entryId, String loadNo)
			throws Exception {
		try {
			DBRow row = floorPrintLabelMgrGql.findLabelTempDetail(
					entryId.trim(), loadNo);
			return row;
		} catch (Exception e) {
			throw new SystemException(e, "findLabelTempDetail", log);
		}
	}

	/**
	 * 处理日期
	 * 
	 * @throws Exception
	 */
	private String formatDate(String date) throws Exception {
		if (!StringUtil.isBlank(date)) {
			SimpleDateFormat from = new SimpleDateFormat("MM/dd/yy HH:mm");
			SimpleDateFormat to = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return to.format(from.parse(date));
		}
		return null;
	}

	/**
	 * response 设置
	 * 
	 * @response
	 */
	private void setResponse(HttpServletResponse response) {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}

	/**
	 * 从request中取出参数值
	 * 
	 * @param request
	 * @throws IOException
	 * @throws JSONException
	 */
	private DBRow getParamFromRequest(HttpServletRequest request)
			throws Exception {
		DBRow dbRow = new DBRow();
		Enumeration<String> paramEnum = request.getParameterNames();
		while (paramEnum != null && paramEnum.hasMoreElements()) {
			String key = (String) paramEnum.nextElement();
			dbRow.add(key, StringUtil.getString(request, key));
		}
		if (dbRow.isEmpty()) {
			String result = IOUtils.toString(request.getReader());
			if (!StringUtil.isBlank(result)) {
				JSONObject jsonData = new JSONObject(result);
				dbRow = DBRowUtils.convertToDBRow(jsonData);
			}
		}
		return dbRow;
	}

	/**
	 * 查询所有customer信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllCustomerId() throws Exception {
		try {
			return floorPrintLabelMgrGql.getAllCustomerId();
		} catch (Exception e) {
			throw new SystemException(e, "getAllCustomerId", log);
		}

	};

	/**
	 * 查询所有Carrier信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllCarrier() throws Exception {
		try {
			return floorPrintLabelMgrGql.getAllCarrier();
		} catch (Exception e) {
			throw new SystemException(e, "getAllCustomerId", log);
		}

	};

	/*
	 * carrier 自动提示
	 */

	public DBRow[] getSearchPdfLoadJSON(String load_no, String... customerIds)
			throws Exception {
		load_no = load_no.replace("'", "''");
		return floorPrintLabelMgrGql.getSearchPdfLoadJSON(load_no, customerIds);
	}

	public DBRow[] getSearchPdfDnJSON(String dn, String... customerIds)
			throws Exception {
		dn = dn.replace("'", "''");
		return floorPrintLabelMgrGql.getSearchPdfDnameJSON(dn, customerIds);
	}

	public DBRow[] getSearchPdfLoadJSON(String load_no) throws Exception {
		load_no = load_no.replace("'", "''");
		return floorPrintLabelMgrGql.getSearchPdfLoadJSON(load_no);
	}

	public DBRow[] getSearchPdfDnJSON(String dn) throws Exception {
		dn = dn.replace("'", "''");
		return floorPrintLabelMgrGql.getSearchPdfDnameJSON(dn);
	}

	@Override
	public DBRow[] getDnNameBySearchInfo(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return this.getDnNameBySearchInfo(request, response, new String[] {});
	}

	/**
	 * 得到需要的dn
	 */
	public DBRow[] getDnNameBySearchInfo(HttpServletRequest request,
			HttpServletResponse response, String... customerIds)
			throws Exception {
		try {
			DBRow row = this.getParamFromRequest(request);

			if (!row.isEmpty()) {

				String cmd = row.get("cmd", null);
				if (!StringUtil.isBlank(cmd)) {
					// load
					String loadno = row.get("loadno", null);

					String account_id = row.get("shipto", null);
					String company_name = row.get("carrier", null);
					String customer_id = row.get("customer", null);

					String start_time = row.get("start_time", null);
					String end_time = row.get("end_time", null);

					String dn = row.get("dn", null);
					Long ps_id = row.get("ps", 0l);
					Long title_id = row.get("title", 0l);

					DBRow[] number = null;
					String numberCompany = "";
					String[] loadCompany = null;
					DBRow[] info = null;
					if (!StringUtil.isBlank(loadno)) {
						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								null, null, null, null, 0l, 0l, null, loadno,
								dn, ModuleKey.CHECK_IN_LOAD, customerIds);
						if (number != null && number.length > 0) {
							loadCompany = new String[number.length];
							for (int i = 0; i < loadCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								loadCompany[i] = numberCompany;
							}
						}

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno, dn,
								ModuleKey.CHECK_IN_PONO, customerIds);
						String[] ponoCompany = null;
						if (number != null && number.length > 0) {
							ponoCompany = new String[number.length];
							for (int i = 0; i < ponoCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								ponoCompany[i] = numberCompany;
							}
						}

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno, dn,
								ModuleKey.CHECK_IN_ORDER, customerIds);
						String[] orderCompany = null;
						if (number != null && number.length > 0) {
							orderCompany = new String[number.length];
							for (int i = 0; i < orderCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								orderCompany[i] = numberCompany;
							}
						}

						info = sqlServerMgrZJ
								.findDNByLoadCompanyPoCompanyOrderCompany(
										loadCompany, ponoCompany, orderCompany,
										null);
					} else {

						if (ps_id == 0l) {
							AdminMgr adminMgr = (AdminMgr) MvcUtil
									.getBeanFromContainer("adminMgr");
							AdminLoginBean adminLoginBean = adminMgr
									.getAdminLoginBean(StringUtil
											.getSession(request));
							ps_id = adminLoginBean.getPs_id();
						}

						if (!StringUtil.isBlank(start_time) && ps_id > 0l) {
							start_time = DateUtil.showUTCTime(
									this.formatDate(start_time), ps_id);
						} else {
							start_time = this.formatDate(start_time);
						}

						if (!StringUtil.isBlank(end_time) && ps_id > 0l) {
							end_time = DateUtil.showUTCTime(
									this.formatDate(end_time), ps_id);
						} else {
							end_time = this.formatDate(end_time);
						}
						// 包装参数 loadcompanyId

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno, dn,
								ModuleKey.CHECK_IN_LOAD, customerIds);
						if (number != null && number.length > 0) {
							loadCompany = new String[number.length];
							for (int i = 0; i < loadCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								loadCompany[i] = numberCompany;
							}
						}

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno, dn,
								ModuleKey.CHECK_IN_PONO, customerIds);
						String[] ponoCompany = null;
						if (number != null && number.length > 0) {
							ponoCompany = new String[number.length];
							for (int i = 0; i < ponoCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								ponoCompany[i] = numberCompany;
							}
						}

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno, dn,
								ModuleKey.CHECK_IN_ORDER, customerIds);
						String[] orderCompany = null;
						if (number != null && number.length > 0) {
							orderCompany = new String[number.length];
							for (int i = 0; i < orderCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								orderCompany[i] = numberCompany;
							}
						}

						// 根据条件查询出来的number 查询 wms 得出所有的dn 建立临时表 最后关联临时表查询
						info = sqlServerMgrZJ
								.findDNByLoadCompanyPoCompanyOrderCompany(
										loadCompany, ponoCompany, orderCompany,
										dn);
					}

					// create temp table
					floorPrintLabelMgrGql.createTempTable("printed_label_temp");

					floorPrintLabelMgrGql.insertDBrow("printed_label_temp",
							info);

					DBRow[] rows = floorPrintLabelMgrGql.getDnNameByParam(
							account_id, company_name, start_time, end_time,
							ps_id, title_id, customer_id, loadno);

					// delete data
					floorPrintLabelMgrGql.deleteTempTable("printed_label_temp");

					return rows;

				} else {
					return new DBRow[0];
				}
			} else {
				return new DBRow[0];
			}
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("getDnNameBySearchInfo").append(e).toString());
		} finally {

		}

	};

	public DBRow[] getCheckInfo(String start_time, String end_time,
			String customer_id, Long ps_id) throws Exception {
		try {

			DBRow[] number = null;
			String numberCompany = "";
			String[] loadCompany = null;
			DBRow[] info = null;

			number = floorPrintLabelMgrGql.getNumberBySearchInfo(null, null,
					start_time, end_time, ps_id, null, customer_id, null, null,
					ModuleKey.CHECK_IN_LOAD);
			if (number != null && number.length > 0) {
				loadCompany = new String[number.length];
				for (int i = 0; i < loadCompany.length; i++) {
					numberCompany = number[i].getString("number") + "-"
							+ number[i].getString("company_id");
					loadCompany[i] = numberCompany;
				}
			}

			number = floorPrintLabelMgrGql.getNumberBySearchInfo(null, null,
					start_time, end_time, ps_id, null, customer_id, null, null,
					ModuleKey.CHECK_IN_PONO);
			String[] ponoCompany = null;
			if (number != null && number.length > 0) {
				ponoCompany = new String[number.length];
				for (int i = 0; i < ponoCompany.length; i++) {
					numberCompany = number[i].getString("number") + "-"
							+ number[i].getString("company_id");
					ponoCompany[i] = numberCompany;
				}
			}

			number = floorPrintLabelMgrGql.getNumberBySearchInfo(null, null,
					start_time, end_time, ps_id, null, customer_id, null, null,
					ModuleKey.CHECK_IN_ORDER);
			String[] orderCompany = null;
			if (number != null && number.length > 0) {
				orderCompany = new String[number.length];
				for (int i = 0; i < orderCompany.length; i++) {
					numberCompany = number[i].getString("number") + "-"
							+ number[i].getString("company_id");
					orderCompany[i] = numberCompany;
				}
			}

			// 根据条件查询出来的number 查询 wms 得出所有的dn 建立临时表 最后关联临时表查询
			info = sqlServerMgrZJ.findDNByLoadCompanyPoCompanyOrderCompany(
					loadCompany, ponoCompany, orderCompany, null);

			// create temp table
			String tempTableName = "order_info_temp";
			floorPrintLabelMgrGql.createTempTable(tempTableName);

			floorPrintLabelMgrGql.insertDBrow(tempTableName, info);

			DBRow[] rows = floorPrintLabelMgrGql.getCheckinAndOrderInfo(
					start_time, end_time, ps_id, customer_id, tempTableName);

			// delete data
			floorPrintLabelMgrGql.deleteTempTable(tempTableName);

			return rows;

		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("getDnNameBySearchInfo").append(e).toString());
		}

	};
	

	public DBRow[] getMissedPdfStat(HttpServletRequest request,String... customerIds)
			throws Exception {
		DBRow[] rows = new DBRow[0];
		DBRow row = this.getParamFromRequest(request);
		if (!row.isEmpty()) {
			String cmd = row.get("cmd", null);
			Long ps_id = row.get("ps", 0l);
			if (!StringUtil.isBlank(cmd)) {
				String start_time = row.get("start_time", null);
				String end_time = row.get("end_time", null);
				String customer = row.get("customer", null);
				String loadNo = row.get("loadNo", null);
				if(customer!=null && !"".equals(customer)){
					customerIds = new String[]{customer};
				} 
				if (!StringUtil.isBlank(start_time) && ps_id > 0l) {
					start_time = DateUtil.showUTCTime(
							this.formatDate(start_time), ps_id);
				} else {
					start_time = this.formatDate(start_time);
				}

				if (!StringUtil.isBlank(end_time) && ps_id > 0l) {
					end_time = DateUtil.showUTCTime(this.formatDate(end_time),
							ps_id);
				} else {
					end_time = this.formatDate(end_time);
				}

				rows = floorPrintLabelMgrGql.getMissedPdfStat(start_time, end_time, ps_id,loadNo,customerIds);
			}

		}
		return rows;
	}

	public DBRow[] getMissedPdfList(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		DBRow[] rows = null;
		try {
			DBRow row = this.getParamFromRequest(request);

			if (!row.isEmpty()) {

				String cmd = row.get("cmd", null);
				if (!StringUtil.isBlank(cmd)) {
					// load
					String loadno = row.get("loadno", null);

					String account_id = row.get("shipto", null);
					String company_name = row.get("carrier", null);
					String customer_id = row.get("customer", null);
					String start_time = row.get("start_time", null);
					String end_time = row.get("end_time", null);

					// String dn=row.get("dn", null);
					Long ps_id = row.get("ps", 0l);
					Long title_id = row.get("title", 0l);

					DBRow[] number = null;
					String numberCompany = "";
					String[] loadCompany = null;
					DBRow[] info = null;
					if (!StringUtil.isBlank(loadno)) {
						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								null, null, null, null, 0l, 0l, null, loadno,
								ModuleKey.CHECK_IN_LOAD);
						if (number != null && number.length > 0) {
							loadCompany = new String[number.length];
							for (int i = 0; i < loadCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								loadCompany[i] = numberCompany;
							}
						}

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno,
								ModuleKey.CHECK_IN_PONO);
						String[] ponoCompany = null;
						if (number != null && number.length > 0) {
							ponoCompany = new String[number.length];
							for (int i = 0; i < ponoCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								ponoCompany[i] = numberCompany;
							}
						}

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno,
								ModuleKey.CHECK_IN_ORDER);
						String[] orderCompany = null;
						if (number != null && number.length > 0) {
							orderCompany = new String[number.length];
							for (int i = 0; i < orderCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								orderCompany[i] = numberCompany;
							}
						}

						info = sqlServerMgrZJ
								.findDNByLoadCompanyPoCompanyOrderCompany(
										loadCompany, ponoCompany, orderCompany);
					} else {

						if (ps_id == 0l) {
							AdminMgr adminMgr = (AdminMgr) MvcUtil
									.getBeanFromContainer("adminMgr");
							AdminLoginBean adminLoginBean = adminMgr
									.getAdminLoginBean(StringUtil
											.getSession(request));
							ps_id = adminLoginBean.getPs_id();
						}

						if (!StringUtil.isBlank(start_time) && ps_id > 0l) {
							start_time = DateUtil.showUTCTime(
									this.formatDate(start_time), ps_id);
						} else {
							start_time = this.formatDate(start_time);
						}

						if (!StringUtil.isBlank(end_time) && ps_id > 0l) {
							end_time = DateUtil.showUTCTime(
									this.formatDate(end_time), ps_id);
						} else {
							end_time = this.formatDate(end_time);
						}
						// 包装参数 loadcompanyId

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno, null,
								ModuleKey.CHECK_IN_LOAD);
						if (number != null && number.length > 0) {
							loadCompany = new String[number.length];
							for (int i = 0; i < loadCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								loadCompany[i] = numberCompany;
							}
						}

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno, null,
								ModuleKey.CHECK_IN_PONO);
						String[] ponoCompany = null;
						if (number != null && number.length > 0) {
							ponoCompany = new String[number.length];
							for (int i = 0; i < ponoCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								ponoCompany[i] = numberCompany;
							}
						}

						number = floorPrintLabelMgrGql.getNumberBySearchInfo(
								account_id, company_name, start_time, end_time,
								ps_id, title_id, customer_id, loadno, null,
								ModuleKey.CHECK_IN_ORDER);
						String[] orderCompany = null;
						if (number != null && number.length > 0) {
							orderCompany = new String[number.length];
							for (int i = 0; i < orderCompany.length; i++) {
								numberCompany = number[i].getString("number")
										+ "-"
										+ number[i].getString("company_id");
								orderCompany[i] = numberCompany;
							}
						}

						// 根据条件查询出来的number 查询 wms 得出所有的dn 建立临时表 最后关联临时表查询
						info = sqlServerMgrZJ
								.findDNByLoadCompanyPoCompanyOrderCompany(
										loadCompany, ponoCompany, orderCompany,
										null);
					}

					// create temp table
					floorPrintLabelMgrGql.createTempTable("printed_label_temp");

					floorPrintLabelMgrGql.insertDBrow("printed_label_temp",
							info);

					floorPrintLabelMgrGql
							.createMissedPdfTempTable("tb_missed_pdf");
					DBRow[] filterRows = floorPrintLabelMgrGql
							.getloadNoCompanyCustomerIDGroup("tb_missed_pdf");
					if (filterRows != null) {
						DBRow[] masterBolRows = sqlServerMgrZJ
								.getMasterBOLNos(filterRows);
						rows = floorPrintLabelMgrGql
								.getMissedPdfList(masterBolRows);
					}

					// delete data
					floorPrintLabelMgrGql.deleteTempTable("printed_label_temp");
					floorPrintLabelMgrGql.deleteTempTable("tb_missed_pdf");

					return rows;

				} else {
					return new DBRow[0];
				}
			} else {
				return new DBRow[0];
			}
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("getDnNameBySearchInfo").append(e).toString());
		} finally {

		}

	};

	@Override
	public DBRow[] getMissedPdfList(String start_time, String end_time) throws Exception {
		return floorPrintLabelMgrGql.getMissedPdfStat(start_time,end_time, 0L,null);
	}

	private DBRow copyRow(DBRow source) {
		DBRow newDBRow = new DBRow();

		ArrayList fields = source.getFieldNames();
		for (int i = 0; i < fields.size(); i++) {
			newDBRow.add(fields.get(i).toString(),
					source.getString(fields.get(i).toString()));
		}
		return newDBRow;

	}

	/**
	 * 下载pdf zip 文件
	 */
	public void downLoadPdfZip(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			DBRow data = this.getParamFromRequest(request);
			String sessionId = request.getRequestedSessionId();

			File file = new File(Download_Pdf_Path);
			if (!file.exists()) {
				file.mkdirs();
			}
			file = new File(Zip_Pdf_Path);
			if (!file.exists()) {
				file.mkdirs();
			}

			// step1 先下载
			String temp = "", dnname = "";
			String file_ids = data.getString("file_ids");

			// //测试使用
			// DBRow[]
			// ids=dbUtilAutoTran.selectMutliple(" SELECT * FROM `printed_label` order by create_time desc LIMIT 1,300");
			//
			// if(ids!=null&&ids.length>0){
			// for (int i = 0; i < ids.length; i++) {
			// temp=ids[i].getString("file_id");
			// if(!StringUtil.isBlank(temp)){
			// uploadAndDownloadPDF.getFile(downLoadFileUrl+temp+".pdf",
			// Download_Pdf_Path+temp+".pdf", sessionId);
			// }
			// }
			// }

			JSONArray array = new JSONArray(file_ids);

			if (array != null && array.length() > 0) {
				for (int i = 0; i < array.length(); i++) {
					temp = array.getJSONObject(i).getString("file_id");
					dnname = array.getJSONObject(i).getString("dn");
					if (!StringUtil.isBlank(temp)) {
						uploadAndDownloadPDF.getFile(downLoadFileUrl + temp
								+ ".pdf", Download_Pdf_Path + dnname + ".pdf",
								sessionId);
					}
				}
			}
			// step2 再压缩
			String zip_path = Zip_Pdf_Path + "vvme.zip";

			pdfToZip.createPdfZip(Download_Pdf_Path, zip_path);

			// step3 再回复 zip
			setResponseZipOfPdf(zip_path, response);

		} catch (Exception e) {
			throw new Exception((new StringBuilder()).append("downLoadPdfZip")
					.append(e).toString());
		}
	}

	/**
	 * 读取zip文件并回复
	 * 
	 * @param zipSrcPath
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	private void setResponseZipOfPdf(String zipSrcPath,
			HttpServletResponse response) throws Exception {
		// 读取zip文件 提高性能
		try {
			ServletOutputStream out = response.getOutputStream();
			FileChannel fc = new RandomAccessFile(zipSrcPath, "r").getChannel();
			MappedByteBuffer byteBuffer = fc.map(MapMode.READ_ONLY, 0,
					fc.size()).load();
			byte[] pdfByte = new byte[(int) fc.size()];
			if (byteBuffer.remaining() > 0) {
				byteBuffer.get(pdfByte, 0, byteBuffer.remaining());
			}
			fc.close();

			response.setContentType("application/zip");
			response.setContentLength(pdfByte.length);
			String file_name = DateUtil.NowStr();
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ file_name + ".zip\"");
			out.write(pdfByte, 0, pdfByte.length);
			out.flush();
			out.close();
			if (new File(zipSrcPath).exists()) {
				new File(zipSrcPath).delete();
			}
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("setResponseZipOfPdf").append(e).toString());
		}

	};

	/**
	 * print 调用pdf服务生成pdf
	 */
	public void createPdfByPdfServer(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		System.out
				.println("============================createPdfByPdfServer==================================");
		DBRow data = this.getParamFromRequest(request);
		if (!data.isEmpty()) {
			Long entry_id = data.get("entry_id", 0l);
			Long detail_id = data.get("detail_id", 0l);
			if (entry_id != 0l && detail_id != 0l) {
				androidPrintMgrIfaceZr.addCreatePdfTask(entry_id, detail_id,
						request);
				// this.pdfUtilIFace.createPdf(data.getString("entry_id"),
				// data.getString("detail_id"), request);
			}
		}
	};

	/**
	 * masterBol和bol标签，web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24
	 */
	public DBRow getDetailByEntryId(long entry_id, String load_no)
			throws Exception {
		try {
			DBRow row = floorPrintLabelMgrGql.getDetailByEntryId(entry_id,
					load_no);
			return row;
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("getDetailByEntryId").append(e).toString());
		}
	}

	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setSqlServerMgrZJ(SQLServerMgrIFaceZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public void setFloorRelativeFileMgrZr(
			FloorRelativeFileMgrZr floorRelativeFileMgrZr) {
		this.floorRelativeFileMgrZr = floorRelativeFileMgrZr;
	}

	public void setFloorSpaceResourcesRelationMgr(
			FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr) {
		this.floorSpaceResourcesRelationMgr = floorSpaceResourcesRelationMgr;
	}

	public void setCheckInMgrZr(CheckInMgrIfaceZr checkInMgrZr) {
		this.checkInMgrZr = checkInMgrZr;
	}

	public void setFileMgrZr(FileMgrIfaceZr fileMgrZr) {
		this.fileMgrZr = fileMgrZr;
	}

	public void setImgToPdfMgr(ImgToPdfMgr imgToPdfMgr) {
		this.imgToPdfMgr = imgToPdfMgr;
	}

	public void setUploadAndDownloadPDF(
			UploadAndDownloadPDF uploadAndDownloadPDF) {
		this.uploadAndDownloadPDF = uploadAndDownloadPDF;
	}

	public void setDownLoadFileUrl(String downLoadFileUrl) {
		this.downLoadFileUrl = downLoadFileUrl;
	}

	public void setUploadFileUrl(String uploadFileUrl) {
		this.uploadFileUrl = uploadFileUrl;
	}

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

	public void setProxyUser(String proxyUser) {
		this.proxyUser = proxyUser;
	}

	public void setProxyPassword(String proxyPassword) {
		this.proxyPassword = proxyPassword;
	}

	public void setFloorPrintLabelMgrGql(
			FloorPrintLabelMgrGql floorPrintLabelMgrGql) {
		this.floorPrintLabelMgrGql = floorPrintLabelMgrGql;
	}

	public void setPdfToZip(CompressPdfToZip pdfToZip) {
		this.pdfToZip = pdfToZip;
	}

	public void setAndroidPrintMgrIfaceZr(
			AndroidPrintMgrIfaceZr androidPrintMgrIfaceZr) {
		this.androidPrintMgrIfaceZr = androidPrintMgrIfaceZr;
	}

}