package com.cwc.app.api.gql;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.floor.api.gql.FlooReturnOrderMgrGql;
import com.cwc.app.floor.api.zyj.FloorReturnMgrZyj;
import com.cwc.app.iface.gql.ReturnOrderMgrIfaceGql;
import com.cwc.app.key.ReturnOrderStatusKey;
import com.cwc.app.key.ReturnServicePalletHandleStatusKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ReturnOrderMgrGql implements ReturnOrderMgrIfaceGql {
	static Logger log = Logger.getLogger("ACTION");
	 private FlooReturnOrderMgrGql floorReturnOrderMgrGql;
	 private FloorReturnMgrZyj floorReturnMgrZyj;
	 
	/**
	 * 查询主单据功能 带分页
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	 @Override
	public DBRow[] getReturnOrdet(PageCtrl pc)throws Exception
	{
		try{
			return this.floorReturnOrderMgrGql.getReturnOrdet(pc);
		}catch(Exception e){
			throw new SystemException(e,"getReturnOrdet",log);
		}
	}
	
	 public DBRow[] getReturnOrdetsByPara(String receive_from,String receive_category,String input_st_date,String input_en_date,PageCtrl pc)throws Exception
	 {
		 try{
			 	
				return this.floorReturnOrderMgrGql.getReturnOrdetsByPara(receive_from,receive_category,input_st_date,input_en_date,pc);
			}catch(Exception e){
				throw new SystemException(e,"getReturnOrdet",log);
			}
	 }
	 

	 public DBRow[] getReturnOrdersById(long id,PageCtrl pc)throws Exception
	 {
		 try{
				return this.floorReturnOrderMgrGql.getReturnOrdersById(id,pc);
			}catch(Exception e){
				throw new SystemException(e,"getReturnOrdersById",log);
			}
	 }
	 
	/**
	 * 根据receive_id查询return主单据信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow returnOrderById(long id)throws Exception
	{
		try{
			return this.floorReturnOrderMgrGql.returnOrderById(id);
		}catch(Exception e){
			throw new SystemException(e,"returnOrderById",log);
		}
	}

	/**新增
	 * @param row
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow addReturnOrder(HttpServletRequest request) throws Exception {
		try{
			long logingUserId = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			String time = DateUtil.NowStr();
			int count = (0==StringUtil.getInt(request,"pallet_count")?StringUtil.getInt(request,"package_count"):StringUtil.getInt(request,"pallet_count"));
			
			String receive_from = StringUtil.getString(request,"receive_from");
			String receive_store = StringUtil.getString(request,"receive_store");
			String pallet_count = StringUtil.getString(request,"pallet_count");
			String package_count = StringUtil.getString(request,"package_count");
			String bol_tracking = StringUtil.getString(request,"bol_tracking");
			String update_pallet_count = StringUtil.getString(request,"update_pallet_count");
			String dock_receive_date = StringUtil.getString(request,"dock_receive_date");
			String receive_carrier = StringUtil.getString(request,"receive_carrier");
			String receive_category = StringUtil.getString(request,"receive_category");
			int status = ReturnOrderStatusKey.RECEIVING;
			
			DBRow returnOrder = new DBRow();
			returnOrder.add("receive_from",receive_from);
			returnOrder.add("receive_store",receive_store);
			returnOrder.add("pallet_count",pallet_count);
			returnOrder.add("package_count",package_count);
			returnOrder.add("bol_tracking",bol_tracking);
			returnOrder.add("update_pallet_count",update_pallet_count);
			returnOrder.add("dock_receive_date",dock_receive_date);
			returnOrder.add("receive_carrier",receive_carrier);
			returnOrder.add("receive_category",receive_category);
			returnOrder.add("create_date",time);
			returnOrder.add("creator",logingUserId);
			returnOrder.add("status",status);
			
			long receive_id = floorReturnOrderMgrGql.addReturnOrder(returnOrder);//添加return主单信息
			DBRow[] rows = addReturnPallet(receive_id,logingUserId,count,time);//根据palletCount或者packageCount添加pallet或者package
			
			//返回相关信息
			DBRow result = new DBRow();
			result.add("receive_id", receive_id);
			result.add("receive_pallet_rows", rows);//放入增加的pallet信息
			result.add("bol_tracking", bol_tracking);
			result.add("receive_carrier", receive_carrier);
			result.add("pallet_count", pallet_count);
			result.add("dock_receive_date", dock_receive_date);
			
			return result;
			
		 }catch (Exception e) {
			 throw new SystemException(e,"addReturnOrder",log);
		}
	}
	
	
	/**
	 * 根据pallet_count或者package_count添加ReturnPallet
	 */
	@Override
	public DBRow[] addReturnPallet(long receive_id,long logingUserId,int count,String time) throws Exception{
		try 
		{
			DBRow[] rows = new DBRow[count];
			int status = ReturnServicePalletHandleStatusKey.NON_PROCESS;//pallet的status:未处理
			for(int i=0;i<count;i++)
			{
				DBRow row	= new DBRow();
				row.add("return_receive_id", receive_id);
				row.add("creator", logingUserId);
				row.add("create_date", time);
				row.add("pallet_no", receive_id+"-"+(i+1));
				row.add("status", status);
				long palletId = floorReturnMgrZyj.addReturnPalletRow(row);
				row.add("pallet_view_id", palletId);
				rows[i] = row;
			}	
			return rows;
		} catch (Exception e) {
			throw new SystemException(e,"addReturnPallet",log);
		}
		
	}
	
	

	/**
	 * 修改
	 */
	@Override
	public void updateReturnOrderById(HttpServletRequest request) throws Exception {
		try{
			String receive_id = StringUtil.getString(request,"receive_id");
			String receive_from = StringUtil.getString(request,"receive_from");
			String receive_store = StringUtil.getString(request,"receive_store");
			String pallet_count = StringUtil.getString(request,"pallet_count");
			String package_count = StringUtil.getString(request,"package_count");
			String bol_tracking = StringUtil.getString(request,"bol_tracking");
			String update_pallet_count = StringUtil.getString(request,"update_pallet_count");
			String dock_receive_date = StringUtil.getString(request,"dock_receive_date");
			String receive_carrier = StringUtil.getString(request,"receive_carrier");
			String receive_category = StringUtil.getString(request,"receive_category");
			String status = StringUtil.getString(request,"status");
			
			DBRow returnOrder = new DBRow();
			returnOrder.add("receive_from",receive_from);
			returnOrder.add("receive_store",receive_store);
			returnOrder.add("pallet_count",pallet_count);
			returnOrder.add("package_count",package_count);
			returnOrder.add("bol_tracking",bol_tracking);
			returnOrder.add("update_pallet_count",update_pallet_count);
			returnOrder.add("dock_receive_date",dock_receive_date);
			returnOrder.add("receive_carrier",receive_carrier);
			returnOrder.add("receive_category",receive_category);
			returnOrder.add("status",status);
			
			floorReturnOrderMgrGql.updateReturnOrderById(returnOrder,receive_id);
		 }catch (Exception e) {
			 throw new SystemException(e,"updateReturnOrderById",log);
		}
	}

	 public void setFloorReturnOrderMgrGql(FlooReturnOrderMgrGql floorReturnOrderMgrGql) 
	 {
		this.floorReturnOrderMgrGql = floorReturnOrderMgrGql;
	 }
	 
	 public void setFloorReturnMgrZyj(FloorReturnMgrZyj floorReturnMgrZyj) {
			this.floorReturnMgrZyj = floorReturnMgrZyj;
	}

}
