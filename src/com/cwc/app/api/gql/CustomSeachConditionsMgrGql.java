package com.cwc.app.api.gql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.gql.FlooCustomSeachConditionsGql;
import com.cwc.app.floor.api.zyj.model.Title;
import com.cwc.app.floor.api.zyj.service.ProductCustomerSerivce;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.gql.CustomSeachConditionsIfaceGql;
import com.cwc.app.iface.zwb.ProductLableTempMgrIfaceWFH;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.LableTemplateProductKey;
import com.cwc.app.key.LableTemplateTypeKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;

public class CustomSeachConditionsMgrGql implements CustomSeachConditionsIfaceGql {
	 static Logger log = Logger.getLogger("ACTION");
	 private FlooCustomSeachConditionsGql flooCustomSeachConditions;
	 private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	 private StorageCatalogMgrZyjIFace storageCatalogMgrZyj;
	 private CatalogMgrIFace catalogMgr;
	 private ProductLableTempMgrIfaceWFH productLableTemp;
	 private ProductCustomerSerivce productCustomerSerivce;
	

	//TODO 添加数据到商品详细模版表
	public void addProductDetailLable(HttpServletRequest request)throws Exception{
		try{
			String title_id=StringUtil.getString(request, "title");
			String product_line = StringUtil.getString(request, "pcLine");
			String one_catalog=StringUtil.getString(request, "oneCatalog");
			String two_catalog=StringUtil.getString(request, "twoCatalog");
			String three_catalog=StringUtil.getString(request, "threeCatalog");
			int  product_type=StringUtil.getInt(request,"productType");
			String product_catclog="";
			if(!three_catalog.equals("")){  	 //点击3级分类 只查询商品
				product_catclog=three_catalog;
			}else if(!two_catalog.equals("")){   //点击2级分类 查询 3级分类
				product_catclog=two_catalog;
			}else if(!one_catalog.equals("")){   //点击1级分类查询2级分类
				product_catclog=one_catalog;
			}
			JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
			DBRow dataRow = DBRowUtils.convertToDBRow(jsonData);
			//获取商品数据
			DBRow[] productRows = proprietaryMgrZyj.filterProductByPcLineCategoryTitleId(true, product_catclog, product_line,product_type, 0, 0,title_id, 0L,0,0,0,null, request);
			
			DBRow row=new DBRow();
			row.add("type",dataRow.get("type",0L));
			row.add("lable_template_type",dataRow.get("lable_template_type",0L));
			row.add("lable_name", dataRow.get("lable_name", ""));
			row.add("lable_content",dataRow.get("lable_content", "").trim());
			row.add("lable_type", dataRow.get("lable_type", ""));
			row.add("lable_width",dataRow.get("lable_width", ""));
			row.add("lable_height",dataRow.get("lable_height", ""));
			row.add("print_name",dataRow.get("print_name", ""));
			row.add("title_id",title_id);
			long id=this.flooCustomSeachConditions.addProductDetailLable(row);
			
			for(int i=0;i<productRows.length;i++){
				DBRow relationRow=new DBRow();
				relationRow.add("pc_id",productRows[i].get("pc_id",0L));
				relationRow.add("lable_template_detail_id",id);
				long reId=this.flooCustomSeachConditions.addLableTemplateRelation(relationRow);
				
			}
		}catch(Exception e){
			throw new SystemException(e,"addProductDetailLable",log);
		}
	}
	
	
	
	//替换html 里的占位符
	public String getHtml(Map map,String html)throws Exception{
		try{
			java.util.Iterator it = map.entrySet().iterator();
			while(it.hasNext()){
				java.util.Map.Entry entry = (java.util.Map.Entry)it.next();
				String key=entry.getKey().toString();         //返回与此项对应的键
				String value=entry.getValue().toString();       //返回与此项对应的值
				html=html.replace("{"+key+"}", value);
			}
		return html;
		}catch(Exception e){
			throw new SystemException(e,"getHtml",log);
		}
	}
	
	//获取 商品列表
	public DBRow[] getProductList(String title_id,String product_catclog,String product_line,PageCtrl pc,HttpServletRequest request,int productType)throws Exception{
			try{
				                                    
				DBRow[] productRows =proprietaryMgrZyj.filterProductAndRelateInfoByPcLineCategoryTitleId(true,product_catclog,product_line,productType, 0, 0,title_id, 0L, 0, 5,0,0, pc, request);
				DBRow[] product_list = new DBRow[productRows.length];
				long titleId=0;
				if(!title_id.equals("")){
					titleId=Long.parseLong(title_id);
				}
				for(int i=0;i<productRows.length;i++){
					DBRow row=new DBRow();
					row.add("pc_id",productRows[i].get("pc_id", 0l));
					row.add("pc_name",productRows[i].get("p_name", ""));
					row.add("catalog_id",productRows[i].get("catalog_id", 0l));
					row.add("p_code",productRows[i].get("p_code", ""));
					float length=productRows[i].get("length", 0f);
					float width=productRows[i].get("width", 0f);
					float heigth=productRows[i].get("heigth", 0f);
					String Dimension=length+"*"+width+"*"+heigth;
					row.add("dimension",Dimension);
					long catalog_id=productRows[i].get("catalog_id", 0L);
					StringBuffer catalogText=this.getInformation(catalog_id);
					row.add("catalog_text",catalogText);
					//获取title 列表
					this.getProductTitleList(row, productRows[i].get("pc_id", 0l));
					//根据商品id查询 详细模版
					this.getLableDetail(row, productRows[i].get("pc_id", 0l),titleId);
					product_list[i]=row;
				}
				return product_list;
			}catch(Exception e){
				throw new SystemException(e,"getProductList",log);
			}
	}
	
	//获得商品的title列表
	public void getProductTitleList(DBRow product_row,long pc_id)throws Exception{
		try{
			
			//DBRow[] rows=this.flooCustomSeachConditions.getProductTitleList(pc_id);
			List<DBRow> title_list = new ArrayList<DBRow>(); 
			List<Title> titles = this.productCustomerSerivce.getTitles((int)pc_id);
			for(Title title : titles){
				DBRow row=new DBRow();
				String title_name= title.getName();
				long title_id= title.getId();
				row.add("title_name",title_name);
				row.add("title_id",title_id);
				title_list.add(row);
			}
			product_row.add("title_list", DBRowUtils.dbRowArrayAsJSON(title_list.toArray(new DBRow[title_list.size()])));
		}catch(Exception e){
			throw new SystemException(e,"getProductTitleList",log);
		}
	}
	
	//获得商品的查询线 分类列表
	public StringBuffer getInformation(long catalog_id)throws Exception{
		try{
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			StringBuffer catalogText = new StringBuffer("<span>");
		  
		  	DBRow allFather[] = tree.getAllFather(catalog_id);
		  	for (int jj=0; jj<allFather.length-1; jj++){
		  		catalogText.append("<img src='img/folderopen.gif'/>"+allFather[jj].getString("title")+"<br/>");
		  		String s = (jj==0) ? "<img src='img/joinbottom.gif'/>"  : "<img  src='img/empty.gif'/><img src='img/joinbottom.gif'/>";
		  		catalogText.append(s);
		  	}
		  	
		  	DBRow catalog = catalogMgr.getDetailProductCatalogById(StringUtil.getLong(String.valueOf(catalog_id)));
		  	if (catalog!=null){
		  		catalogText.append("<img  src='img/page.gif'/>"+catalog.getString("title"));
		  	}  
		  	return catalogText;
		}catch(Exception e){
			throw new SystemException(e,"getInformation",log);
		}
	}
	
	//查询商品模版详细表
	public DBRow getLableDetail(DBRow row,long pc_id,long title_id)throws Exception{
		try{
			DBRow[] rows=this.flooCustomSeachConditions.getLableDetail(pc_id,0,title_id);
			List<DBRow> productLable = new ArrayList<DBRow>();   //商品模版详细列表
			List<DBRow> contaionerLable = new ArrayList<DBRow>();   //商品模版详细列表
			
			for(int i=0;i<rows.length;i++){
				long type=rows[i].get("lable_template_type", 0L);
				if(type==LableTemplateTypeKey.PRODUCTLABEL){
					DBRow productRow=new DBRow();
					productRow.add("relation_id",rows[i].get("relation_id", 0L));
					productRow.add("detail_lable_id",rows[i].get("detail_lable_id", 0L));
					productRow.add("lable_template_type",rows[i].get("lable_template_type", 0L));
					productRow.add("lable_name",rows[i].getString("lable_name"));
					productRow.add("lable_content",rows[i].getString("lable_content"));
					productRow.add("lable_type",rows[i].getString("lable_type"));
					productRow.add("lable_height",rows[i].getString("lable_height"));
					productRow.add("lable_width",rows[i].getString("lable_width"));
					productRow.add("print_name",rows[i].getString("print_name"));
					productRow.add("type",rows[i].getString("type"));
					productLable.add(productRow);
				}else if(type==LableTemplateTypeKey.CONTAINERTLABEL){
					DBRow contaionerRow=new DBRow();
					contaionerRow.add("relation_id",rows[i].get("relation_id", 0L));
					contaionerRow.add("detail_lable_id",rows[i].get("detail_lable_id", 0L));
					contaionerRow.add("lable_template_type",rows[i].get("lable_template_type", 0L));
					contaionerRow.add("lable_name",rows[i].getString("lable_name"));
					contaionerRow.add("lable_content",rows[i].getString("lable_content"));
					contaionerRow.add("lable_type",rows[i].getString("lable_type"));
					contaionerRow.add("lable_height",rows[i].getString("lable_height"));
					contaionerRow.add("lable_width",rows[i].getString("lable_width"));
					contaionerRow.add("print_name",rows[i].getString("print_name"));
					contaionerRow.add("type",rows[i].getString("type"));
					contaionerLable.add(contaionerRow);
				}
			}		
		    // //system.out.println("长度"+productLable.size());
		    if(productLable.size()!= 0){
		    	row.add("product_lable",DBRowUtils.dbRowArrayAsJSON(productLable.toArray(new DBRow[productLable.size()])));
		    }
		    if(contaionerLable.size()!=0){
		    	row.add("contaioner_lable",DBRowUtils.dbRowArrayAsJSON(contaionerLable.toArray(new DBRow[contaionerLable.size()])));
		    }
		    return row;	
		}catch(Exception e){
			throw new SystemException(e,"getLableDetail",log);
		}
	}
	
	//获取 商品线列表
	public JSONObject getProductLineList(String title_id,HttpServletRequest request)throws Exception{
		//获取 商品线 数据
		try{
			DBRow[] productLineRows=proprietaryMgrZyj.findProductLinesByTitleId(true, 0L, "", title_id, 0, 0, null, request);
			DBRow[] product_line_list = new DBRow[productLineRows.length];
			for(int i=0;i<productLineRows.length;i++){
				DBRow row=new DBRow();
				row.add("id",productLineRows[i].get("id", 0l));
				row.add("name",productLineRows[i].getString("name"));
				row.add("key","pcLine");
				product_line_list[i]=row;
			}
			JSONObject result_product_line_row = new JSONObject();
			//Map<String, Object> result_product_line_row = new HashMap<String, Object>();
			if(productLineRows.length>0 && productLineRows!=null){
				result_product_line_row.put("key", "pcLine");
				//result_product_line_row.put("parentKey", "title");
				result_product_line_row.put("type", "PRODUCT LINE：");
				result_product_line_row.put("mulit", false);
				result_product_line_row.put("url", "/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action?search=conditions");
				result_product_line_row.put("data", DBRowUtils.dbRowArrayAsJSON(product_line_list));
	//			JSONArray resultlist = new JSONArray();
	//			resultlist.put(result_product_line_row);
			}
			return result_product_line_row;	
		}catch(Exception e){
			throw new SystemException(e,"getProductLineList",log);
		}
	}
	
	//根据产品线 查询1级分类
	public JSONObject getOneCatalog(String product_line_id,String title_id,HttpServletRequest request)throws Exception{
		try{
			DBRow[] rows=proprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(true, 0, product_line_id, "","", title_id, 0, 0, null, request);
			DBRow[] oneCatalogRow = new DBRow[rows.length];
			for(int i=0;i<rows.length;i++){
				DBRow row=new DBRow();
				row.add("id",rows[i].get("id", 0l));
				row.add("name",rows[i].getString("name"));
				row.add("key","oneCatalog");
				oneCatalogRow[i]=row;
			}
			JSONObject result_one_catalog_row = new JSONObject();
			if(rows.length>0 && rows!=null){
				//Map<String, Object> result_one_catalog_row = new HashMap<String, Object>();
				result_one_catalog_row.put("key", "oneCatalog");
				//result_one_catalog_row.put("parentKey", "title pcLine");
				result_one_catalog_row.put("type", "ONE CATALOG：");
				result_one_catalog_row.put("mulit", false);
				result_one_catalog_row.put("url", "/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action?search=conditions");
				result_one_catalog_row.put("data", DBRowUtils.dbRowArrayAsJSON(oneCatalogRow));
	//			JSONArray resultlist = new JSONArray();
	//			resultlist.put(result_one_catalog_row);
			}
			return result_one_catalog_row;
		}catch(Exception e){
			throw new SystemException(e,"getOneCatalog",log);
		}
	}
	
	//根据1级分类查询2级
	public JSONObject getTwoCatalog(String oneCatalog,String title_id,HttpServletRequest request)throws Exception{
		try{
			DBRow[] rows=proprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(true, 0, "", oneCatalog,"", title_id, 0, 0, null, request);
			DBRow[] towCatalogRow = new DBRow[rows.length];
			for(int i=0;i<rows.length;i++){
				DBRow row=new DBRow();
				row.add("id",rows[i].get("id", 0l));
				row.add("name",rows[i].getString("name"));
				row.add("key","twoCatalog");
				towCatalogRow[i]=row;
			}
			//Map<String, Object> result_two_catalog_row = new HashMap<String, Object>();
			JSONObject result_two_catalog_row = new JSONObject();
			if(rows.length>0 && rows!=null){
				result_two_catalog_row.put("key", "twoCatalog");
				//result_two_catalog_row.put("parentKey", "title pcLine oneCatalog");
				result_two_catalog_row.put("type", "TWO CATALOG：");
				result_two_catalog_row.put("mulit", false);
				result_two_catalog_row.put("url", "/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action?search=conditions");
				result_two_catalog_row.put("data", DBRowUtils.dbRowArrayAsJSON(towCatalogRow));
	//			JSONArray resultlist = new JSONArray();
	//			resultlist.put(result_two_catalog_row);
			}
			return result_two_catalog_row;
		}catch(Exception e){
			throw new SystemException(e,"getTwoCatalog",log);
		}
	}
	
	//根据2级分类查询三级
	public JSONObject getThreeCatalog(String twoCatalog,String title_id,HttpServletRequest request)throws Exception{
		try{
			DBRow[] rows=proprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(true, 0, "",twoCatalog,"", title_id, 0, 0, null, request);
			DBRow[] threeCatalogRow = new DBRow[rows.length];
			for(int i=0;i<rows.length;i++){
				DBRow row=new DBRow();
				row.add("id",rows[i].get("id", 0l));
				row.add("name",rows[i].getString("name"));
				row.add("key","threeCatalog");
				threeCatalogRow[i]=row;
			}
			//Map<String, Object> result_three_catalog_row = new HashMap<String, Object>();
			JSONObject result_three_catalog_row = new JSONObject();
			if(rows.length>0 && rows!=null){
				result_three_catalog_row.put("key", "threeCatalog");
				//result_three_catalog_row.put("parentKey", "title pcLine oneCatalog twoCatalog");
				result_three_catalog_row.put("type", "THREE CATALOG：");
				result_three_catalog_row.put("mulit", false);
				result_three_catalog_row.put("url", "/Sync10/action/administrator/lableTemplate/CustomSeachConditionsAction.action?search=conditions");
				result_three_catalog_row.put("data", DBRowUtils.dbRowArrayAsJSON(threeCatalogRow));
	//			JSONArray resultlist = new JSONArray();
	//			resultlist.put(result_three_catalog_row);
			}
			return result_three_catalog_row;
		}catch(Exception e){
			throw new SystemException(e,"getThreeCatalog",log);
		}
	}
	
	
	//TODO 单个商品上添加模版
	public long addSingleDetailLable(HttpServletRequest request)throws Exception{
		try{
			JSONObject jsonData = new JSONObject(IOUtils.toString(request.getReader()));
			DBRow dataRow = DBRowUtils.convertToDBRow(jsonData);
			String title_id=StringUtil.getString(request, "title");
			DBRow row=new DBRow();
			row.add("type",  dataRow.get("type", ""));
			row.add("lable_template_type",  dataRow.get("lable_template_type", ""));
			row.add("lable_name", dataRow.get("lable_name", ""));
			row.add("lable_content",dataRow.get("lable_content", ""));
			row.add("lable_type", dataRow.get("lable_type", ""));
			row.add("lable_width",dataRow.get("lable_width", ""));
			row.add("lable_height",dataRow.get("lable_height", ""));
			row.add("print_name",dataRow.get("print_name", ""));
			row.add("title_id",title_id);
			long id=this.flooCustomSeachConditions.addProductDetailLable(row);
			DBRow relationRow=new DBRow();
			relationRow.add("pc_id",dataRow.get("pc_id",""));
			relationRow.add("lable_template_detail_id",id);
			long reId=this.flooCustomSeachConditions.addLableTemplateRelation(relationRow);
			return reId;
		}catch(Exception e){
			throw new SystemException(e,"addSingleDetailLable",log);
		}
	}
	
	//根据 详细模版id 查询是否 有多商品 使用该标签
	public DBRow getCountDetailLableTemplate(long lable_template_detail_id)throws Exception{
		try{
			return this.flooCustomSeachConditions.getCountDetailLableTemplate(lable_template_detail_id);
		}catch(Exception e){
			throw new SystemException(e,"getCountDetailLableTemplate",log);
		}
	}
	
	//删除 商品与模版关系
	public long detLableTemplateRelation(long relation_id,long detail_lable_id)throws Exception{
		try{
			//删除商品与模版的关系
			long id=this.flooCustomSeachConditions.detLableTemplateRelation(relation_id);
			//判断 如果没有其他商品调用这个模版 直接删除该模版
			DBRow row=this.getCountDetailLableTemplate(detail_lable_id);
			if(row.get("count", 0L)==0){
				this.flooCustomSeachConditions.detLableTemplateDetail(detail_lable_id);
			}
			return id;
		}catch(Exception e){
			throw new SystemException(e,"detLableTemplateRelation",log);
		}
	}

	//查询仓库条件
	public Map<String, Object> getWarehouseData()throws Exception{
		try{
			DBRow[] treeRows =storageCatalogMgrZyj.getProductStorageCatalogByType(StorageTypeKey.SELF, null);
			DBRow[] warehouse = new DBRow[treeRows.length];
			for(int i=0;i<treeRows.length;i++){
				DBRow row=new DBRow();
				row.add("id",treeRows[i].get("id", 0L));
				row.add("name",treeRows[i].get("title",""));                                                                                                                                                                                                                                                                                                                                                                                      
				row.add("key","warehouse");
				warehouse[i]=row;
			}
			Map<String, Object> result_warehouse_row = new HashMap<String, Object>();
			result_warehouse_row.put("key","warehouse");
			result_warehouse_row.put("title","WAREHOUSE：");
			result_warehouse_row.put("select",false);
			result_warehouse_row.put("value",DBRowUtils.dbRowArrayAsJSON(warehouse));
			return result_warehouse_row;
		}catch(Exception e){
			throw new SystemException(e,"getWarehouseData",log);
		}
	}
	
	//获得商品 类型 查询条件
	public Map<String, Object> getProductType()throws Exception{
		try{
			DBRow[] product_type = new DBRow[2];
			DBRow row=new DBRow();
			row.add("id",0);
			row.add("name","ITEM");                                                                                                                                                                                                                                                                                                                                                                                      
			row.add("key","productType");
			product_type[0]=row;
			DBRow rowa=new DBRow();
			rowa.add("id",1);
			rowa.add("name","UNIT");                                                                                                                                                                                                                                                                                                                                                                                      
			rowa.add("key","productType");
			product_type[1]=rowa;
			
			Map<String, Object> prduct_type_row = new HashMap<String, Object>();
			prduct_type_row.put("key","productType");
			prduct_type_row.put("type","Product Type：");
			prduct_type_row.put("mulit",false);
			prduct_type_row.put("data",DBRowUtils.dbRowArrayAsJSON(product_type));
			return prduct_type_row;
		}catch(Exception e){
			throw new SystemException(e,"getProductType",log);
		}
	}
	
	
	//修改模版详细
	public long updateDetailLableTemplate(DBRow row)throws Exception{
		try{
//			JSONObject jsonData = new JSONObject(IOUtils.toString(request
//					.getReader()));
//			DBRow row = DBRowUtils.convertToDBRow(jsonData);
			long detail_lable_id = row.get("detail_lable_id", 0L);
			String lable_content=row.get("lable_content", "");
			String lable_name=row.get("lable_name", "");
			String lable_type=row.get("lable_type", "");
			long type=row.get("type", 0L);
			long lable_template_type=row.get("lable_template_type", 0L);
			long lable_width=row.get("lable_width", 0L);
			long lable_height=row.get("lable_height", 0L);
			String print_name=row.get("print_name", "");
			
			DBRow lableRow=new DBRow();
			lableRow.add("lable_content", lable_content);
			lableRow.add("lable_name", lable_name);
			lableRow.add("lable_type", lable_type);
			lableRow.add("type", type);
			lableRow.add("lable_template_type", lable_template_type);
			lableRow.add("lable_width", lable_width);
			lableRow.add("lable_height", lable_height);
			lableRow.add("print_name", print_name);
			
			
			return this.flooCustomSeachConditions.updateDetailLableTemplate(detail_lable_id, lableRow);
		}catch(Exception e){
			throw new SystemException(e,"updateDetailLableTemplate",log);
		}
	}
	
	
	//查处商品的 商品标签
	public DBRow[] getLableLableTemplateByType(long pc_id,long login_id,long lable_template_type,long detail_type,HttpServletRequest request)throws Exception{
		try{
			//赋值时候用
			return this.flooCustomSeachConditions.findLableByPcIdAndLoginId(pc_id,login_id, lable_template_type,detail_type);
		}catch(Exception e){
			throw new SystemException(e,"getLableLableTemplateByType",log);
		}
	}
	
	//根据商品名索引查询商品 
	public DBRow[] getProductByName(HttpServletRequest request,PageCtrl pc)throws Exception{
		try{
			String name= StringUtil.getString(request,"pc_name");
			String title_id = StringUtil.getString(request, "title_id");
			DBRow[] productRows=proprietaryMgrZyj.findDetailProductRelateInfoLikeSearch(true, name, title_id, 0L, 0, 5,-1,0,0, pc, request);
			DBRow[] product_list = new DBRow[productRows.length];
			long titleId=0;
			if(!title_id.equals("")){
				titleId=Long.parseLong(title_id);
			}
			for(int i=0;i<productRows.length;i++){
				DBRow row=new DBRow();
				row.add("pc_id",productRows[i].get("pc_id", 0l));
				row.add("pc_name",productRows[i].get("p_name", ""));
				row.add("catalog_id",productRows[i].get("catalog_id", 0l));
				row.add("p_code",productRows[i].get("p_code", ""));
				float length=productRows[i].get("length", 0f);
				float width=productRows[i].get("width", 0f);
				float heigth=productRows[i].get("heigth", 0f);
				String Dimension=length+"*"+width+"*"+heigth;
				row.add("dimension",Dimension);
				long catalog_id=productRows[i].get("catalog_id", 0L);
				StringBuffer catalogText=this.getInformation(catalog_id);
				row.add("catalog_text",catalogText);
				//获取title 列表
				this.getProductTitleList(row, productRows[i].get("pc_id", 0l));
				
				//根据商品id查询 详细模版
				this.getLableDetail(row, productRows[i].get("pc_id", 0l),titleId);
				product_list[i]=row;
			}
			return product_list;
		}catch(Exception e){
			throw new SystemException(e,"getProductByName",log);
		}
	}
	
	//根据模版名字查询模版 删除用
	public DBRow[] getLableByName(HttpServletRequest request)throws Exception{
		try{
			String name = request.getParameter("name");
			long title = StringUtil.getLong(request, "title");
			return this.flooCustomSeachConditions.getLableByName(name,title);
		}catch(Exception e){
			throw new SystemException(e,"getLableByName",log);
		}
	}
	
	/*
	 * 批量删除商品模版(non-Javadoc)
	 * modify by gql 2015/05/20
	 * 删除title下的所有商品关联的模板名称为lablename的详细模板
	 */
	public void batchDelete(HttpServletRequest request)throws Exception{
		try{
			long title_id=StringUtil.getLong(request, "title_id");
			String lable_name=StringUtil.getString(request, "lable_name");
			if(!StrUtil.isBlank(lable_name)){
//				this.flooCustomSeachConditions.batchDelLableTemplateRelation(title_id, lable_name);//删除模板关系
//				this.flooCustomSeachConditions.batchDelLableTemplateDetail(title_id, lable_name);//删除详细模板
				this.flooCustomSeachConditions.batchDelLable(title_id, lable_name);//删除模板关系和详细模板
			}
		}catch(Exception e){
			throw new SystemException(e,"batchDelete",log);
		}
	}
/*	//批量删除商品模版
	public void batchDelete(HttpServletRequest request)throws Exception{
		try{
			String title_id=request.getParameter("title");
			String product_line = StringUtil.getString(request, "pcLine");
			String one_catalog=StringUtil.getString(request, "oneCatalog");
			String two_catalog=StringUtil.getString(request, "twoCatalog");
			String three_catalog=StringUtil.getString(request, "threeCatalog");
			int  product_type=StringUtil.getInt(request,"productType");
			long lable_template_detail_id=StringUtil.getLong(request,"lable_template_detail_id");
			String product_catclog="";
			if(!three_catalog.equals("")){  	 //点击3级分类 只查询商品
				product_catclog=three_catalog;
			}else if(!two_catalog.equals("")){   //点击2级分类 查询 3级分类
				product_catclog=two_catalog;
			}else if(!one_catalog.equals("")){   //点击1级分类查询2级分类
				product_catclog=one_catalog;
			}
			//获取商品数据
			DBRow[] productRows = proprietaryMgrZyj.filterProductByPcLineCategoryTitleId(true, product_catclog, product_line,product_type, 0, 0,title_id, 0L,0,0,null, request);
			//删除商品与详细模版关系
			for(int i=0;i<productRows.length;i++){
				this.flooCustomSeachConditions.detLableTemplateRelationByPcId(productRows[i].get("pc_id",0L), lable_template_detail_id);
			}
			//判断改详细模版是否还有调用 如果没有删除
			DBRow row=this.flooCustomSeachConditions.getCountDetailLableTemplate(lable_template_detail_id);
			if(row.get("count", 0l)==0){
				this.flooCustomSeachConditions.detLableTemplateDetail(lable_template_detail_id);
			}
		}catch(Exception e){
			throw new SystemException(e,"batchDelete",log);
		}
	}
*/	
	//根据详细模版id查询 模版信息
	public DBRow getLableTemplateById(long detail_lable_id)throws Exception{
		try{
			return this.flooCustomSeachConditions.getLableTemplateById(detail_lable_id);
		}catch(Exception e){
			throw new SystemException(e,"getLableTemplateById",log);
		}
	}
	
	// TODO 添加商品是，取基础模板第一个商品标签和clp标签，建立商品和标签模板的关系
	public void addLableTempByPcId(long pc_id,long title_id) throws Exception{
		try {
			DBRow[] lableTempRows = productLableTemp.findAllLable("", "", 0, null);//查询所有基础模板
			DBRow pcTemp = null;
			DBRow clpTemp = null;
			//取基础标签模板中第一个商品模板和第一个clp模板
			for(int n=0;n<lableTempRows.length;n++){
				lableTempRows[n].remove("lable_id");//去掉基础模板的id
				//如果是商品标签
				if(lableTempRows[n].get("lable_template_type", 0)==LableTemplateProductKey.FIRST){
					if(pcTemp==null){
						pcTemp=lableTempRows[n];
					}else if(clpTemp!=null){
						break;
					}else{
						continue;
					}
				}else if(lableTempRows[n].get("type", 0)==ContainerTypeKey.CLP){
					if(clpTemp==null){
						clpTemp=lableTempRows[n];
					}else if(pcTemp!=null){
						break;
					}else{
						continue;
					}
					
				}
			}
			//如果基础模板存在商品模板，给该新增商品添加第一个商品模板
			if(pcTemp!=null){
				pcTemp.add("title_id", title_id);
				long pcTempId=this.flooCustomSeachConditions.addProductDetailLable(pcTemp);
				DBRow row = new DBRow();
				row.add("pc_id", pc_id);
				row.add("lable_template_detail_id", pcTempId);
				this.flooCustomSeachConditions.addLableTemplateRelation(row);
			}
			//如果基础模板存在商品模板，给该新增商品添加第一个clp模板
			if(clpTemp!=null){
				clpTemp.add("title_id", title_id);
				long clpTempId=this.flooCustomSeachConditions.addProductDetailLable(clpTemp);
				DBRow row = new DBRow();
				row.add("pc_id", pc_id);
				row.add("lable_template_detail_id", clpTempId);
				this.flooCustomSeachConditions.addLableTemplateRelation(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	 
	/**
	 * // TODO 根据参数查询相应的产品标签模板
	 * labelType=1：所有商品基础模板；labelType=2：所有商品关联的详细模板；
	 * @param labelType
	 * @throws Exception
	 */
	//根据模版名字查询模版 删除用
	public DBRow[] getLableTemp(long labelType)throws Exception{
		try{
			DBRow[] rows = productLableTemp.findAllLable("", "", 1, null);//所有商品基础模板
			if(labelType==2){
				rows = flooCustomSeachConditions.getLableDetail(0,1,0);//所有商品关联的详细模板
				//根据模板名去重
				if(rows!=null&&rows.length>0){
					List<DBRow> resultList = new ArrayList<DBRow>();
					List<String> stringList = new ArrayList<String>();
					for (int i = 0; i < rows.length; i++) {
						String labelName = rows[i].getString("lable_name");
						if (StringUtils.isNotEmpty(labelName)) {
							if (!stringList.contains(labelName)) {
								stringList.add(labelName);
								resultList.add(rows[i]);
							} 
						}
					}
					rows = resultList.toArray(new DBRow[resultList.size()]);
//					while(true){
//						DBRow[] temp = this.array_unique(rows, rows[0].getString("lable_name"));
//						if(rows.length==temp.length){
//							break;
//						}else{
//							rows=temp;
//						}
//						
//					}
				}
			}
			return rows;
		}catch(Exception e){
			throw new SystemException(e,"CustomSeachConditionsMgrGql.getLableTemp",log);
		}
	}
	//去除数组中重复的记录  
	public DBRow[] array_unique(DBRow[] a,String fieldNm) {  
	    // array_unique  
	    List<String> list = new LinkedList<String>(); 
	    List<DBRow> resultList = new ArrayList<DBRow>(); 
	    for(int i = 0; i < a.length; i++) {  
	        if(!"".equals(a[i].getString(fieldNm)) && !list.contains(a[i].getString(fieldNm))) {
	        	list.add(a[i].getString(fieldNm));
	        	resultList.add(a[i]);
	        }  
	    }  
	    return resultList.toArray(new DBRow[resultList.size()]);  
	} 

	public void setFlooCustomSeachConditions(
			FlooCustomSeachConditionsGql flooCustomSeachConditions) {
		this.flooCustomSeachConditions = flooCustomSeachConditions;
	}

	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}

	public void setStorageCatalogMgrZyj(
			StorageCatalogMgrZyjIFace storageCatalogMgrZyj) {
		this.storageCatalogMgrZyj = storageCatalogMgrZyj;
	}



	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
		this.catalogMgr = catalogMgr;
	}



	public void setProductLableTemp(ProductLableTempMgrIfaceWFH productLableTemp) {
		this.productLableTemp = productLableTemp;
	}



	public void setProductCustomerSerivce(
			ProductCustomerSerivce productCustomerSerivce) {
		this.productCustomerSerivce = productCustomerSerivce;
	}
	
	 
}
