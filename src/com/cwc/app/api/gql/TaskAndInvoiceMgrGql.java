package com.cwc.app.api.gql;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.api.zr.BillMgrZr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.SpecialTaskLogBean;
import com.cwc.app.exception.special.SpecialAddException;
import com.cwc.app.exception.special.SpecialDelException;
import com.cwc.app.exception.special.SpecialUpdateException;
import com.cwc.app.floor.api.FloorLogFilterIFace;
import com.cwc.app.floor.api.FloorLogMgrIFace;
import com.cwc.app.floor.api.gql.FloorCreatePdfMgr;
import com.cwc.app.floor.api.gql.FloorPrintLabelMgrGql;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.gql.CreatePdfFileByServerIfaceGql;
import com.cwc.app.iface.gql.TaskAndInvoiceMgrIfaceGql;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.ScheduleModel;
import com.cwc.app.key.BillStateKey;
import com.cwc.app.key.InvoiceStateKey;
import com.cwc.app.key.InvoiceStatusKey;
import com.cwc.app.key.LogServModelKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.PdfTypeKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.TaskStatusKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.lucene.zwb.InvoiceIndexMgr;
import com.cwc.app.lucene.zwb.ScheduleIndexMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.json.JsonUtils;
import com.cwc.reportcenter.ReportCenter;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.fr.report.core.barcode.Module;

public class TaskAndInvoiceMgrGql implements TaskAndInvoiceMgrIfaceGql{
	 private AdminMgrIFace adminMgr;
	 private FloorPrintLabelMgrGql floorPrintLabelMgrGql;
	 private BillMgrZr billMgrZr;
	 static Logger log = Logger.getLogger("ACTION");
	 private ScheduleMgrIfaceZR scheduleMgrIfaceZR;
	 private ScheduleMgrIfaceZR scheduleMgrZr;
	 private CreatePdfFileByServerIfaceGql createPdfFileByServerIfaceGql;
	 private FloorCreatePdfMgr floorCreatePdfMgr;
	 
	 private FloorLogMgrIFace floorLogMgrIFace;
	 private SpecialProjectLogUtil specialProjectLogUtil;
	 /**
	  *  invoice 状态变成open时 可以发通知
	  *  zwb
	  */
	 public void assignInvoice(HttpServletRequest request,HttpServletResponse response)throws Exception{
		 try{
			 //创建时间
		    Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createTime = dateformat.format(date);
			//获得创建人
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long createManId = adminLoginBean.getAdid();
			//获取值
			DBRow data=this.getParamFromRequest(request);
			String bill_id=data.getString("bill_id", null);
			String ids=data.getString("ids", null);
			String subject=data.getString("subject", null);
			String content=data.getString("content", null);
			
	        String emailTitle=subject;
	        String context=content;
	        scheduleMgrZr.addScheduleByExternalArrangeNow(createTime,"",createManId,ids,"",Long.parseLong(bill_id), ModuleKey.SPECIAL_TASK, emailTitle, context, false, true, false,request,true,ProcessKey.SPECIAL_INVOICE);
				
		 }catch(Exception e){
		     throw new SystemException(e,"assignInvoice",log);
		 }
	 }
	/**
	 * 添加schedule任务
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void addAndUpdateSchedule(HttpServletRequest request,HttpServletResponse response) throws Exception{
		try{	
			//获取值
			DBRow data=this.getParamFromRequest(request);
			
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
  		    AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
            Long ps_id =  adminLoginBean.getPs_id();
            Long adid=adminLoginBean.getAdid();
            String name=adminLoginBean.getEmploye_name();
            DBRow[] department=adminLoginBean.getDepartment();
            
			Long schedule_id=0l; DBRow dbrow=null;boolean issuccess=true;
			/**
			 * schedule-任务表  schedule-sub 任务执行人表   schedule-join 任务参与人表  schedule-repeat  任务重复表(貌似没有在用)  schedule-replay 任务回复表
			 */
			if(!data.isEmpty()){
				          DBRow bean=new DBRow();
                          //得到invoice_id 判断是task页面还是invoice页面的请求
				          String  invoice_id=data.getString("subinvoice", null);
				          JSONArray array=null;
				          if(!StringUtil.isBlank(invoice_id)){
				        	  array=new JSONArray(invoice_id);
				          }
				          
						  String schedule_id_from_request=data.getString("schedule_id","");
						
						if(!StringUtil.isBlank(schedule_id_from_request)){
							//重新包装数据
							data.remove("is_main_schedule");
							
							String id=data.get("schedule_id", null);
							data.remove("schedule_id");
							data.add("task_id", id);
							
							String task_overview=data.get("schedule_overview", null);
							data.remove("schedule_overview");
							data.add("task_overview",task_overview);
							
							String task_detail=data.get("schedule_detail", null);
							data.remove("schedule_detail");
							data.add("task_detail",task_detail);
							
							String schedule_execute_id=data.getString("schedule_execute_id",null);
							data.remove("schedule_execute_id");
							data.add("addusers",convertStringByAnyChar(schedule_execute_id));
							
							String deleteusers=data.getString("deleteusers",null);
							data.remove("deleteusers");
							data.add("deleteusers", deleteusers);
							
							String schedule_join_execute_id=data.getString("schedule_join_execute_id",null);
							data.remove("schedule_join_execute_id");
							data.add("add_join_user_id", convertStringByAnyChar(schedule_join_execute_id));
							
							String del_join_user_id=data.getString("del_join_user_id",null);
							data.remove("del_join_user_id");
							data.add("del_join_user_id", del_join_user_id);
							
							String delsubinvoice=data.getString("delsubinvoice",null);
							data.remove("delsubinvoice");String del_result_invoice_id="";
							if(delsubinvoice!=null&&delsubinvoice.length()>0){
								JSONArray invoiceArray=new JSONArray(delsubinvoice);
								if(invoiceArray!=null&&invoiceArray.length()>0){
									String bill_id=null;
									int count = invoiceArray.length()-1;
									for (int i = 0; i < count; i++) {
										bill_id=invoiceArray.getString(i);
										del_result_invoice_id+=bill_id+",";
									}
									del_result_invoice_id+=invoiceArray.getString(count);
								}
							}
							
							data.add("delsubinvoice", del_result_invoice_id);
							
							//特殊处理subinvoice 只获取其中的bill_id的值
							data.remove("subinvoice");String result_invoice_id="";
							if(array!=null&&array.length()>0){
								String bill_id=null;
								int count = array.length()-1;
								for (int i = 0; i < count; i++) {
									bill_id=array.getString(i);
									result_invoice_id+=bill_id+",";
								}
								result_invoice_id+=array.getString(count);
							}
							data.add("subinvoice", result_invoice_id);
							
							//特殊处理delsubfile 只获取其中的file_id的值
							String delsubfile=data.getString("delsubfile",null);
							data.remove("delsubfile");String result_file_id="";
							if(delsubfile!=null&&delsubfile.length()>0){
								JSONArray fileArray=new JSONArray(delsubfile);
								if(fileArray!=null&&fileArray.length()>0){
									String bill_id=null;
									int count = fileArray.length()-1;
									for (int i = 0; i < count; i++) {
										bill_id=fileArray.getJSONObject(i).getString("file_id".toUpperCase());
										result_file_id+=bill_id+",";
									}
									result_file_id+=fileArray.getJSONObject(count).getString("file_id".toUpperCase());
								}
							}
							data.add("delsubfile", result_file_id);
							
							
							String start_time=data.getString("start_time", null);
							data.remove("start_time");
							start_time=this.formatDate(start_time);
							if(!StrUtil.isBlank(start_time)){
								data.add("start_time", DateUtil.showUTCTime(start_time, ps_id));
							}
							String end_time=data.getString("end_time", null);
							data.remove("end_time");
							end_time=this.formatDate(end_time);
							if(!StrUtil.isBlank(end_time)){
								data.add("end_time",  DateUtil.showUTCTime(end_time, ps_id));
							}
							data.getString("schedule_execute_id", null);
							
							data.remove("parent_schedule_id");
							
							data.remove("subschedule");
							
							data.remove("project_id");
							//修改
							dbrow=floorPrintLabelMgrGql.updateSchedule(Long.valueOf(schedule_id_from_request), data);
							issuccess=false;
							//查询一下 父节点Id
							Long project_id=this.getProjectIdByAnyId(Long.valueOf(schedule_id_from_request));
							//增加日志
							try {
								SpecialTaskLogBean logBean=new SpecialTaskLogBean();
								logBean.setModule(SpecialKey.TASK);
								logBean.setType(SpecialKey.TASK);
								logBean.setRelation_id(Long.valueOf(project_id));
								logBean.setAdid(adid);
								logBean.setAccount(name);
								logBean.setOperation_content(SpecialKey.OPERATION_TASK);
								logBean.setOperation_note(" Update Task ");
								logBean.setDepartment(new JSONObject(department));
								
								floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
							} catch (Exception e) {
								//出现异常 不做任何处理
							}
							issuccess=true;								//修改索引
							ScheduleIndexMgr.getInstance().updateIndex(Long.valueOf(schedule_id_from_request), forScheduleIndex(data));;
						}else{
							//新增(区分主任务还是子任务增加)
							String project_id=data.getString("project_id",null);
							if(!StringUtil.isBlank(project_id)){
								//主任务 或者子任务增加 1 主任务增加关系 与 project 关联 2 子任务增加关系 与 主任务关联 
								String master_schdule_id=data.getString("parent_schedule_id", null);
								
								
								if(!StringUtil.isBlank(master_schdule_id)){
									//任务增加
									schedule_id = addSchedule(data, bean,false,2,ps_id);
									//添加关系
									issuccess=false;
									//增加日志
									try {
										SpecialTaskLogBean logBean=new SpecialTaskLogBean();
										logBean.setModule(SpecialKey.TASK);
										logBean.setType(SpecialKey.MAINTASK);
										logBean.setRelation_id(Long.valueOf(project_id));
										logBean.setAdid(adid);
										logBean.setAccount(name);
										logBean.setOperation_content(SpecialKey.OPERATION_TASK);
										logBean.setOperation_note(" Add Subtask ");
										logBean.setDepartment(new JSONObject(department));
										
										floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
									} catch (Exception e) {
										//出现异常 不做任何处理
									}
									
									//子任务增加 添加关系
									bean=new DBRow();
									bean.add("parent_task_id", Long.parseLong(master_schdule_id));
									bean.add("task_id", schedule_id);
									Long relation_id=floorPrintLabelMgrGql.addScheduleRelationship(bean);

									if(relation_id!=null&&relation_id!=0l){
										issuccess=true;
									}
								}else{
									//任务增加
									schedule_id = addSchedule(data, bean,false,3,ps_id);
									
									issuccess=false;
									//增加日志
									try {
										SpecialTaskLogBean logBean=new SpecialTaskLogBean();
										logBean.setModule(SpecialKey.TASK);
										logBean.setType(SpecialKey.SUBTASK);
										logBean.setRelation_id(Long.valueOf(project_id));
										logBean.setAdid(adid);
										logBean.setAccount(name);
										logBean.setOperation_content(SpecialKey.OPERATION_TASK);
										logBean.setOperation_note(" Add Maintask ");
										logBean.setDepartment(new JSONObject(department));
										
										floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
									} catch (Exception e) {
										//出现异常 不做任何处理
									}
									
									//主任务增加 添加关系
									
									bean=new DBRow();
									bean.add("parent_task_id", Long.parseLong(project_id));
									bean.add("task_id", schedule_id);
									Long relation_id=floorPrintLabelMgrGql.addScheduleRelationship(bean);
									
									
									if(schedule_id!=null&&schedule_id!=0l){
										//处理增加主任务时候直接添加invoice
									    DBRow relation=null;String invoiceId=null;
									    for (int i = 0; i < array.length(); i++) {
									    	issuccess=false;
									    	invoiceId=array.getString(i);
										    //增加关系
								    		relation=new DBRow();
									    	relation.add("invoice_id", invoiceId);
									    	relation.add("task_id", schedule_id);
									    	relation_id=floorPrintLabelMgrGql.addScheduleAndInvoiceRelationship(relation);
									    	if(relation_id!=null&&relation_id!=0l){
												issuccess=true;
											}
									     }
									}
									if(relation_id!=null&&relation_id!=0l){
										issuccess=true;
									}
								}
							}else{
								//项目增加 不用关联
								schedule_id = addSchedule(data, bean,true,1,ps_id);
								
								try {
									SpecialTaskLogBean logBean=new SpecialTaskLogBean();
									logBean.setModule(SpecialKey.TASK);
									logBean.setType(SpecialKey.PROJECT);
									logBean.setRelation_id(Long.valueOf(schedule_id));
									logBean.setAdid(adid);
									logBean.setAccount(name);
									logBean.setOperation_content(SpecialKey.OPERATION_TASK);
									logBean.setOperation_note(" Add Project ");
									logBean.setDepartment(new JSONObject(department));
									
									floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
								} catch (Exception e) {
									//出现异常 不做任何处理
								}
								
								if(schedule_id!=null&&schedule_id!=0l){
									issuccess=true;
									 //增加索引
									 ScheduleIndexMgr.getInstance().addIndex(schedule_id,forScheduleIndex(data));
								}
								//project 增加时候处理一下 file
								String file_id=null,original_file_name=null;
								String taskfiles=data.get("subfile", null);
								if(!StringUtil.isBlank(taskfiles)){
									JSONArray files=new JSONArray(taskfiles);
									if(files!=null&&files.length()>0){
										for (int i = 0; i < files.length(); i++) {
											issuccess=false;
											file_id=files.getJSONObject(i).getString("file_id".toUpperCase());
											original_file_name=files.getJSONObject(i).getString("original_file_name".toUpperCase());
											if(!StringUtil.isBlank(file_id)){
												bean=new DBRow();
												bean.add("task_id", schedule_id);
												bean.add("file_id", file_id);
												bean.add("original_file_name", original_file_name);
												Long id=floorPrintLabelMgrGql.addScheduleAndFileRelationship(bean);
												if(id!=0l){
													issuccess=true;
												}
											}
										}
									}
								}
							 }
						 }

				//返回信息
				this.setResponse(response);
				if(issuccess){
					response.getWriter().print(new JSONObject().put("success", true).put("schedule_id", schedule_id).toString());
				}else{
					response.getWriter().print(new JSONObject().put("success", false).toString());
				}
			}
		}catch(Exception e){
			response.getWriter().print(new JSONObject().put("success", false).toString());
			throw new SystemException(e,"addAndUpdateSchedule",log);
		}
		
	}
	/**
	 * 
	 * @param string
	 * @return
	 * @throws Exception 
	 */
//	private String getInvoiceIdFromBean(String invoice) throws Exception {
//		String result="";
//		if(!StringUtil.isBlank(invoice)){
//			JSONObject json=new JSONObject(invoice);
//			if(json!=null){
//				result=json.getString("bill_id".toUpperCase());
//			}
//		}
//		return result;
//	}

	/**
	 * 得到scheduleIndex数据
	 * @return
	 */
	private DBRow forScheduleIndex(DBRow data){
		DBRow row=new DBRow();
		if(!data.isEmpty()){
			row.add("schedule_overview", data.getString("schedule_overview",null));
		}
		return row;
	}
	/**
	 * 得到invoiceIndex数据
	 * @return
	 */
	private DBRow forInvoiceIndex(DBRow data){
		DBRow row=new DBRow();
		if(!data.isEmpty()){
			row.add("client_id",data.get("client_id", ""));
			row.add("account_name",data.get("account_name", ""));
		}
		return row;
	}
	
	
	

	
    /**
     * task增加
     * @param data
     * @param scheduleBean
     * @return
     * @throws Exception
     */
	private Long addSchedule(DBRow data, DBRow bean,boolean ismain,int task_type,Long ps_id) throws Exception {
		Long schedule_id;
		Long assign_user_id=data.get("assign_user_id", 0l);//任务安排人
		bean.add("assign_user_id",assign_user_id);
		
		JSONArray array=null;
		String execute_user_id = data.getString("schedule_execute_id",null);//任务执行人
		if(!StringUtil.isBlank(execute_user_id)){
			array=new JSONArray(execute_user_id);
			if(array!=null&&array.length()>0){
				execute_user_id=this.convertStringByAnyChar(execute_user_id);
				bean.add("execute_user_id",execute_user_id);
			}else{
				bean.add("execute_user_id",null);
			}
		}
		
		
		String schedule_join_execute_id = data.getString("schedule_join_execute_id",null);//任务参与人
		if(!StringUtil.isBlank(schedule_join_execute_id)){
			array=new JSONArray(schedule_join_execute_id);
			if(array!=null&&array.length()>0){
				schedule_join_execute_id=this.convertStringByAnyChar(schedule_join_execute_id);
				bean.add("schedule_join_execute_id",schedule_join_execute_id);
			}else{
				bean.add("schedule_join_execute_id",null);
			}
		}
		
		String startTime = data.getString("start_time",null);//任务开始时间
		String endTime = data.getString("end_time",null);//任务结束时间
		if(startTime!=null&&startTime.length()>0){
			bean.add("start_time", DateUtil.showUTCTime(formatDate(startTime), ps_id));
		}
		if(endTime!=null&&endTime.length()>0){
			bean.add("end_time",  DateUtil.showUTCTime(formatDate(endTime), ps_id));
		}
		
		String schedule_overview = data.getString("schedule_overview",null); //任务标题
		bean.add("task_overview",schedule_overview);
		
		
		String schedule_detail = data.getString("schedule_detail",null);//任务描述
		bean.add("task_detail",schedule_detail);
		 
		
		bean.add("create_time", DateUtil.NowStr());
		
		int is_additional_task=data.get("is_additional_task", YesOrNotKey.NO);
		bean.add("is_additional_task", is_additional_task);
		bean.add("task_status", TaskStatusKey.OPEN);
//		int is_additional_task=data.get("is_additional_task", YesOrNotKey.NO);
//		if(is_additional_task==YesOrNotKey.YES){
//			bean.add("is_additional_task", is_additional_task);
//			bean.add("task_status", TaskStatusKey.PENDING);
//		}else{
//			bean.add("is_additional_task", is_additional_task);
//			bean.add("task_status", TaskStatusKey.PROCESSING);
//		}
		
	    bean.add("is_main_task",task_type);
		if(!ismain){
		    bean.add("task_number",data.get("task_number",0l));
		}
		schedule_id=floorPrintLabelMgrGql.addSchedule(bean);
		
		return schedule_id;
	};
	
	
	/**
	 * 处理日期
	 * @throws Exception 
	 */
	private  String formatDate(String date) throws Exception{
		if(!StringUtil.isBlank(date)){
			SimpleDateFormat from = new SimpleDateFormat("MM/dd/yy");
	        SimpleDateFormat to = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        return to.format(from.parse(date));
		}
		return null;
	}
	/**
	 * @throws sException 
	 * 
	 */
	private String convertStringByAnyChar(String codeString) throws Exception{
		String result="";
		if(!StringUtil.isBlank(codeString)&&!codeString.equals("null")){
			JSONArray array=new JSONArray(codeString);
			if(array!=null&&array.length()>0){
				int count = array.length()-1;
				for (int index = 0 ; index < count ; index++){
					String  json = array.getString(index);
					result+=json+",";
				}
				result+=array.getString(count);
			}
		}
		return result;
	}
	
	
	/**
	 * 添加账单 --修改账单
	 * @param request
	 * @param isUpdate
	 * @return
	 * @throws Exception
	 */
	public void addAndUpdateInvoice(HttpServletRequest request, HttpServletResponse response) throws Exception{
		try{
			//获取值
			DBRow data=this.getParamFromRequest(request);
			boolean issuccess=false;
			DBRow billRow = new DBRow();
			List<DBRow> itemList = new ArrayList<DBRow>();
			//判断schedule_id y-task  n-invoice 
			String schedule_id=data.getString("schedule_id", null);
			long insert_bill_id=0l;
			if(!data.isEmpty()){
				long billId = data.get("bill_id",0l);//得到billid 判断是新增还是修改
				boolean updateFlag =  (billId != 0l ? true:false);
			   
				long rp_id	= data.get("rp_id",0l);//质保请求id
				long sid	= data.get("sid",0l);//服务单id
				long poid	= data.get("oid",0l);
				
				
				
				billRow.add("client_id", data.get("client_id", null));
				billRow.add("payer_email", data.get("payer_email", null));
				billRow.add("tel", data.get("tel", null));
				billRow.add("address_name", data.get("address_name", null));
				billRow.add("address_country", data.get("address_country", null));
				billRow.add("address_city", data.get("address_city", null));
				billRow.add("address_zip", data.get("address_zip", null));
				billRow.add("address_street", data.get("address_street", null));
				billRow.add("address_country_code", data.get("address_country_code", null));
				billRow.add("address_state", data.get("address_state", null));
				billRow.add("account", data.get("account",null));
				billRow.add("account_name", data.get("account_name",null));
				billRow.add("note", data.get("note", null));
				billRow.add("memo_note", data.get("memo_note", null));
				
				billRow.add("rp_id",rp_id);
				billRow.add("sid", sid);
				
				
				//product information
				billRow.add("subtotal",  data.get("subtotal", 0.0));
				billRow.add("shipping_fee",  data.get("shipping_fee", 0.0));
//				billRow.add("rate",  data.get("rate", 0.0));
				billRow.add("product_cost",  data.get("total_cost", 0.0));
				billRow.add("rate_type",  data.get("rate_type", null));
				
				billRow.add("attention",  data.get("attention", null));
				billRow.add("fax",  data.get("fax", null));
				billRow.add("payment_term",  data.get("payment_term", null));
				//lr_type lr_no
				billRow.add("lr_type",  data.get("lr_type", 0l));
				billRow.add("lr_no",  data.get("lr_no", null));
			
				//ccid,pro_id,ps_id
				billRow.add("ccid", data.get("ccid",0l));//国家的id
				billRow.add("pro_id", data.get("pro_id",0l));//省份的id
				billRow.add("ps_id", data.get("ps_id",0l));
				billRow.add("sc_id", data.get("sc_id",0l));
				billRow.add("bill_type", data.get("bill_type",0));//1 ebay 2 products 3 shipping 4 warranty 5 order
				billRow.add("total_weight", data.get("total_weight",0.0f));
				billRow.add("total_quantity",data.get("total_quantity",0.0f));
				billRow.add("account_payee",data.get("account_payee",0l));
				billRow.add("weight_type", data.get("weight_type",null));
				billRow.add("key_type", data.get("key_type",0));
				
				
				billRow.add("total_discount",data.get("total_discount",0));//总折扣
				billRow.add("final_discount",data.get("total_discount",0));
				billRow.add("save",data.get("total",0.0f));
				
				billRow.add("subtotal",data.get("total",0.0f)-data.get("total_discount",0.0f));
				
				String bill_order_item=data.getString("bill_detail", null);
				
				Double total_quantity=0d;
				if(!StringUtil.isBlank(bill_order_item)){
					JSONArray array=new JSONArray(bill_order_item);
					for (int i = 0; i < array.length(); i++) {
						DBRow item = new DBRow();
						item.add("line_no", array.getJSONObject(i).getString("line_no".toUpperCase()));
						item.add("service_id", array.getJSONObject(i).getString("service_id".toUpperCase()));
						item.add("description", array.getJSONObject(i).getString("description".toUpperCase()));
						item.add("unit_price", array.getJSONObject(i).getString("unit_price".toUpperCase()));
						item.add("rate", array.getJSONObject(i).getDouble("rate".toUpperCase()));
						
						Double  quantity=array.getJSONObject(i).getDouble("quantity".toUpperCase());
						item.add("quantity",quantity);
						total_quantity+=quantity;
						
						item.add("amount", array.getJSONObject(i).getString("amount".toUpperCase()));
						
						itemList.add(item);
					}
				}
				billRow.add("total_quantity",total_quantity);
				
				
				AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
				
			 	long adid = adminLoggerBean.getAdid(); 
			 	String name= adminLoggerBean.getEmploye_name();
			 	DBRow[] department=adminLoggerBean.getDepartment();
				
				if(StringUtil.isBlank(schedule_id)){
					//invoice页面
					if(updateFlag){
//						billRow.add("invoice_state", StringUtil.getInt(request, "invoice_state"));
						 //修改
						insert_bill_id=billId;
						floorPrintLabelMgrGql.updateBill(billId, billRow);
						if(itemList != null && itemList.size() > 0){
							List<Long> ids = new ArrayList<Long>();
							double quantity = 0.0d;
							for(DBRow itemRow : itemList){
							 	quantity += itemRow.get("quantity", 0.0d);
								long itemId = floorPrintLabelMgrGql.saveOrUpdateBillItem(itemRow.get("bill_item_id", 0l), itemRow,billId);
								ids.add(itemId);
							}
							floorPrintLabelMgrGql.deleteItemsByNotInIdsAndUpdateBillQuantity(ids, quantity, billId);
						   issuccess=true;
						}
						try {
							SpecialTaskLogBean logBean=new SpecialTaskLogBean();
							logBean.setModule(SpecialKey.INVOICE);
							logBean.setType(SpecialKey.INVOICE);
							logBean.setRelation_id(Long.valueOf(billId));
							logBean.setAdid(adid);
							logBean.setAccount(name);
							logBean.setOperation_content(SpecialKey.OPERATION_INVOICE);
							logBean.setOperation_note(" Update Invoice ");
							logBean.setDepartment(new JSONObject(department));
							
							floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
						} catch (Exception e) {
							//出现异常 不做任何处理
						}
						
						//修改索引
						InvoiceIndexMgr.getInstance().updateIndex(billId,forInvoiceIndex(data));
						//生成PDF
						try {
							data.add("module_key", ModuleKey.SPECIAL_TASK);
							data.add("pdf_type_key", PdfTypeKey.Invoice);
							data.add("adid", adid);
							createPdfFileByServerIfaceGql.sendHtml(data);
						} catch (Exception e) {
							// TODO Auto-generated catch block
						}
					}else{
						billRow.add("bill_status",  BillStateKey.quote);
						billRow.add("invoice_state", InvoiceStateKey.NewAdd);
						billRow.add("invoice_status",  InvoiceStatusKey.IMPORTED);
						 //新增
						billRow.add("create_date", DateUtil.NowStr());
						
					 	billRow.add("create_adid",data.get("admin_id", adid));
					 	billRow.add("ps_id",adminLoggerBean.getPs_id());
					 	
						insert_bill_id = floorPrintLabelMgrGql.addBill(billRow);
						if(itemList != null && itemList.size() > 0 ){
							for(DBRow row : itemList){
								row.add("bill_id",insert_bill_id);
								long item_id=floorPrintLabelMgrGql.addBillItem(row);
								if(item_id!=0l){
									issuccess=true;
								}
							}
						}
						
						try {
							SpecialTaskLogBean logBean=new SpecialTaskLogBean();
							logBean.setModule(SpecialKey.INVOICE);
							logBean.setType(SpecialKey.INVOICE);
							logBean.setRelation_id(Long.valueOf(insert_bill_id));
							logBean.setAdid(adid);
							logBean.setAccount(name);
							logBean.setOperation_content(SpecialKey.OPERATION_INVOICE);
							logBean.setOperation_note(" Add Invoice ");
							logBean.setDepartment(new JSONObject(department));
							
							floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
						} catch (Exception e) {
							//出现异常 不做任何处理
						}
						//生成PDF
						try {
							data.add("bill_id", insert_bill_id);
							data.add("module_key", ModuleKey.SPECIAL_TASK);
							data.add("pdf_type_key", PdfTypeKey.Invoice);
							data.add("adid", adid);
							createPdfFileByServerIfaceGql.sendHtml(data);
						} catch (Exception e) {
							// TODO Auto-generated catch block
						}
						//增加索引
						InvoiceIndexMgr.getInstance().addIndex(insert_bill_id,forInvoiceIndex(data));
				    }
					
			}else{	
				//task页面
				//增加  添加关系
				billRow.add("create_date", DateUtil.NowStr());
			 	
			 	billRow.add("create_adid",data.get("admin_id", adid));
			 	
				insert_bill_id = floorPrintLabelMgrGql.addBill(billRow);
				if(itemList != null && itemList.size() > 0 ){
					for(DBRow row : itemList){
						row.add("bill_id",insert_bill_id);
						long item_id=floorPrintLabelMgrGql.addBillItem(row);
						if(item_id!=0l){
							issuccess=true;
						}
					}
				}
				if(insert_bill_id!=0l){
					DBRow bean=new DBRow();
					bean.add("invoice_id", insert_bill_id);
					bean.add("schedule_id", schedule_id);
					Long relation_id=floorPrintLabelMgrGql.addScheduleAndInvoiceRelationship(bean);
					if(relation_id!=null&&relation_id!=0l){
						issuccess=true;
					}
				}
				
				try {
					SpecialTaskLogBean logBean=new SpecialTaskLogBean();
					logBean.setModule(SpecialKey.INVOICE);
					logBean.setType(SpecialKey.INVOICE);
					logBean.setRelation_id(Long.valueOf(insert_bill_id));
					logBean.setAdid(adid);
					logBean.setAccount(name);
					logBean.setOperation_content(SpecialKey.OPERATION_INVOICE);
					logBean.setOperation_note(" Add Invoice ");
					logBean.setDepartment(new JSONObject(department));
					
					floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
				} catch (Exception e) {
					//出现异常 不做任何处理
				}
				//生成PDF
				try {
					data.add("bill_id", insert_bill_id);
					data.add("module_key", ModuleKey.SPECIAL_TASK);
					data.add("pdf_type_key", PdfTypeKey.Invoice);
					data.add("adid", adid);
					createPdfFileByServerIfaceGql.sendHtml(data);
				} catch (Exception e) {
					// TODO Auto-generated catch block
				}
				//增加索引
				InvoiceIndexMgr.getInstance().addIndex(insert_bill_id,forInvoiceIndex(data));
			  }
			}
			
			//返回信息
			this.setResponse(response);
			if(issuccess){
				response.getWriter().print(new JSONObject().put("success", true).put("bill_id".toUpperCase(), insert_bill_id).toString());
			}else{
				response.getWriter().print(new JSONObject().put("success", false).toString());
			}
		}catch(Exception e){
			response.getWriter().print(new JSONObject().put("success", false).toString());
			throw new SystemException(e,"addAndUpdateInvoice",log);
		}
	};
	/**
	 * 查询task
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void  getTaskAndDetail(HttpServletRequest request, HttpServletResponse response) throws Exception{
		DBRow[] data=null;
		try{
			DBRow row=this.getParamFromRequest(request);
			
			HttpSession session=request.getSession(true);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		  	
		   	long adid = adminLoggerBean.getAdid(); 
		   	long ps_id=adminLoggerBean.getPs_id();
			row.add("executeUserId",adid);
			JSONObject json=new JSONObject();
			PageCtrl pc = new PageCtrl();
			
			int pageNo = row.get("pageNo", 1);
			int pageSize = row.get("pageSize",5);
			pc.setPageNo(pageNo);
			pc.setPageSize(pageSize);
			
			
			if(!row.isEmpty()){
				    String schedule_id_from_request=row.getString("schedule_id", null);
				    if(!StringUtil.isBlank(schedule_id_from_request)){
				    	DBRow schedule=floorPrintLabelMgrGql.getScheduleByScheduleId(Long.parseLong(schedule_id_from_request));
				    	
				    	
				    	//处理时间格式
						String  start_time=schedule.get("start_time", null);
						if(!StringUtil.isBlank(start_time)){
							schedule.remove("start_time");
							schedule.add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
						}
						String  end_time=schedule.get("end_time", null);
						if(!StringUtil.isBlank(end_time)){
							schedule.remove("end_time");
							schedule.add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
						}
						String  create_time=schedule.get("create_time", null);
						if(!StringUtil.isBlank(create_time)){
							schedule.remove("create_time");
							schedule.add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
						}
						String  finish_time=schedule.get("finish_time", null);
						if(!StringUtil.isBlank(finish_time)){
							schedule.remove("finish_time");
							schedule.add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
						}
				    	
				    	
				    	DBRow[] subSchedule=null;
				    	
				    	//得到关联主任务
						subSchedule=floorPrintLabelMgrGql.getRelationTaskByScheduleId(schedule_id_from_request,null);
//						for (int j = 0; j < subSchedule.length; j++) {
//							String sub_schedule_id=subSchedule[j].get("schedule_id", null);
//							//得到关联的子任务
//							if(!StringUtil.isBlank(sub_schedule_id)){
//								DBRow[] sub_task_task=floorPrintLabelMgrGql.getRelationTaskByScheduleId(sub_schedule_id,null);
//								subSchedule[j].add("subsubschedule", sub_task_task);
//							}
//						}
						schedule.add("subschedule", subSchedule);
						//得到关联invoice
						subSchedule=floorPrintLabelMgrGql.getRelationInvoiceByProjectId(schedule_id_from_request);;
						for (DBRow dbRow : subSchedule) {
							String create_date=dbRow.get("create_date", null);
							if(!StringUtil.isBlank(create_date)){
								dbRow.remove("create_date");
								dbRow.add("create_date",DateUtil.showLocalparseDateTo24Hours(create_date,ps_id).substring(0, 8));
							}
						}
						schedule.add("subinvoice", subSchedule);
						//得到关联file 只有主任务有
						subSchedule=floorPrintLabelMgrGql.getSopByScheduleId(schedule_id_from_request);
						schedule.add("subfile", subSchedule);
						//得到关联log
						try {
							PageCtrl pageCtrl = new PageCtrl();
							pageCtrl.setPageSize(100);
							pageCtrl.setPageNo(1);
							
						    SpecialTaskLogBean queryJson = new SpecialTaskLogBean();
							queryJson.setModule(SpecialKey.TASK);;
							queryJson.setRelation_id(Long.parseLong(schedule_id_from_request));
							
							subSchedule=specialProjectLogUtil.queryLogs(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(queryJson), pageCtrl,ps_id);
							for (DBRow dbRow : subSchedule) {
							      String post_date=dbRow.get("post_date", null);
							      dbRow.add("post_date", DateUtil.showLocalparseDateTo24Hours(post_date,ps_id));
							}
						} catch (Exception e) {
							
						}
						schedule.add("sublog", subSchedule);
						data=new DBRow[]{schedule};
						
						json.put("schedule".toUpperCase(), data);
						
						pc.setAllCount(1);
						pc.setPageCount(1);
						pc.setRowCount(1);
                        json.put("pageCtrl",new JSONObject(pc));
						
						this.setResponse(response);
						
						response.getWriter().print(json.toString());
				    	
				    }else{
						
						
						data=floorPrintLabelMgrGql.getScheduleByExecuteUserIdAndDetailAndPc(row,pc);

						
						DBRow[] schedule_return=new DBRow[data.length];
						
						
						if(data!=null&&data.length>0){
							DBRow temp=null;String schedule_id=null;DBRow[] subSchedule=null;
							for (int i = 0; i < data.length; i++) {
								temp=data[i];
								if(temp!=null){
									schedule_id=temp.getString("schedule_id");
									//得到关联主任务任务
									subSchedule=floorPrintLabelMgrGql.getRelationTaskByScheduleId(schedule_id,null);
//									for (int j = 0; j < subSchedule.length; j++) {
//										String sub_schedule_id=subSchedule[j].get("schedule_id", null);
//										//得到关联的子任务
//										if(!StringUtil.isBlank(sub_schedule_id)){
//											DBRow[] sub_task_task=floorPrintLabelMgrGql.getRelationTaskByScheduleId(sub_schedule_id,null);
//											subSchedule[j].add("subsubschedule", sub_task_task);
//										}
//									}
									temp.add("subschedule", subSchedule);
									
									//处理时间格式
									String  start_time=data[i].get("start_time", null);
									if(!StringUtil.isBlank(start_time)){
										data[i].remove("start_time");
										data[i].add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
									}
									String  end_time=data[i].get("end_time", null);
									if(!StringUtil.isBlank(end_time)){
										data[i].remove("end_time");
										data[i].add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
									}
									String  create_time=data[i].get("create_time", null);
									if(!StringUtil.isBlank(create_time)){
										data[i].remove("create_time");
										data[i].add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
									}
									String  finish_time=data[i].get("finish_time", null);
									if(!StringUtil.isBlank(finish_time)){
										data[i].remove("finish_time");
										data[i].add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
									}
									
									//invoice
									subSchedule=floorPrintLabelMgrGql.getRelationInvoiceByProjectId(schedule_id);
									for (DBRow dbRow : subSchedule) {
										String create_date=dbRow.get("create_date", null);
										if(!StringUtil.isBlank(create_date)){
											dbRow.remove("create_date");
											dbRow.add("create_date",DateUtil.showLocalparseDateTo24Hours(create_date,ps_id).substring(0, 8));
										}
										double save=dbRow.get("save", 0d);
										DecimalFormat format=new DecimalFormat("#.00");
										dbRow.add("save", format.format(save));
										//invoice detail
										String bill_id=dbRow.get("bill_id", null);
										if(!StringUtil.isBlank(bill_id)){
											DBRow[] detail=floorPrintLabelMgrGql.getBillItemByBillOrderId(Long.parseLong(bill_id));
											//处理数据 格式 quantity amount rate
											if(detail!=null&&detail.length>0){
												for (int k = 0; k < detail.length; k++) {
													float quantity=detail[k].get("quantity", 0f);
													detail[k].add("quantity", format.format(quantity));
													double amount=detail[k].get("amount", 0d);
													detail[k].add("amount", format.format(amount));
													
													format=new DecimalFormat("#.0000");
													double rate=detail[k].get("rate", 0d);
													detail[k].add("rate", format.format(rate));
												}
											}
											dbRow.add("bill_detail", detail);
										}
									}
									
									
									
									temp.add("subinvoice", subSchedule); 
									
									//得到关联file 只有主任务有
									subSchedule=floorPrintLabelMgrGql.getSopByScheduleId(schedule_id);
									for (DBRow dbRow : subSchedule) {
										  start_time=dbRow.get("start_time", null);
										if(!StringUtil.isBlank(start_time)){
											dbRow.remove("start_time");
											dbRow.add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
										}
										  end_time=dbRow.get("end_time", null);
										if(!StringUtil.isBlank(end_time)){
											dbRow.remove("end_time");
											dbRow.add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
										}
										  create_time=dbRow.get("create_time", null);
										if(!StringUtil.isBlank(create_time)){
											dbRow.remove("create_time");
											dbRow.add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
										}
										  finish_time=dbRow.get("finish_time", null);
										if(!StringUtil.isBlank(finish_time)){
											dbRow.remove("finish_time");
											dbRow.add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
										}
									}
									temp.add("subfile", subSchedule);
									
									//得到关联log
									try {
										PageCtrl pageCtrl = new PageCtrl();
										pageCtrl.setPageSize(100);
										pageCtrl.setPageNo(1);
										
									    SpecialTaskLogBean queryJson = new SpecialTaskLogBean();
										queryJson.setModule(SpecialKey.TASK);;
										queryJson.setRelation_id(Long.parseLong(schedule_id));
										
										
										subSchedule=specialProjectLogUtil.queryLogs(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(queryJson), pageCtrl,ps_id);
										for (DBRow dbRow : subSchedule) {
										      String post_date=dbRow.get("post_date", null);
										      dbRow.add("post_date", DateUtil.showLocalparseDateTo24Hours(post_date,ps_id));
										}
									} catch (Exception e) {
										
									}
									
									temp.add("sublog", subSchedule);
									schedule_return[i]=temp;
								}
							}
						}
						if(schedule_return!=null&&schedule_return.length>0){
							json.put("schedule".toUpperCase(), data);
						}
						
						json.put("pageCtrl",new JSONObject(pc));
						
						this.setResponse(response);
						
						response.getWriter().print(json.toString());
				    }
				    
			}
		}catch(Exception e){
			throw new SystemException(e,"getTaskAndDetail",log);
		}
	};
	
	
	
	/**
	 * 查询invoice
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void getInvoiceAndDetail(HttpServletRequest request, HttpServletResponse response) throws Exception{
		 DBRow[] data=null;
		 DBRow row=this.getParamFromRequest(request);;
		 PageCtrl pc = new PageCtrl();
			
		 int pageNo = row.get("pageNo", 1);
		 int pageSize = row.get("pageSize",10);
		 pc.setPageNo(pageNo);
		 pc.setPageSize(pageSize);
		 
		 HttpSession session=request.getSession(true);
		 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		  	
		 long adid_ = adminLoggerBean.getAdid(); 
		try{
			    Long invoice_id_from_request=row.get("bill_id",0l);
			    JSONObject json=new JSONObject();
			
			    if(invoice_id_from_request!=null&&invoice_id_from_request!=0l){
			    	DBRow result=floorPrintLabelMgrGql.getDBRowByIdAndTableName(invoice_id_from_request, "bill_order", "bill_id");
                  
					if(result!=null&&!result.isEmpty()){
						Long ps_id=result.get("ps_id", 0l);
						//得到每条invoice对应的detail
						DBRow[] detail=null;
						long bill_id=result.get("bill_id", 0l);
						if(bill_id!=0l){
							//detail
							detail=floorPrintLabelMgrGql.getBillItemByBillOrderId(bill_id);
							if(detail!=null&&detail.length>0){
								result.add("bill_detail", detail);
							}
							//关联的project
							detail=floorPrintLabelMgrGql.getRelationScheduleByBillId(String.valueOf(bill_id),true);
							if(detail!=null&&detail.length>0){
								//关联的main_task
								if(detail!=null&&detail.length>0){
									for (int i = 0; i < detail.length; i++) {
										DBRow[] rows=floorPrintLabelMgrGql.getRelationScheduleByBillId(String.valueOf(bill_id),false);
									    detail[i].add("subschedule", rows);
									}
								}
								result.add("schedule", detail);
							}
							
						}
						
						String create_time=result.get("create_date", null);
						if(!StringUtil.isBlank(create_time)){
							result.remove("create_date");
							result.add("create_date",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
						}
						
						String close_time=result.get("close_time", null);
						if(!StringUtil.isBlank(close_time)){
							result.remove("close_time");
							result.add("close_time",DateUtil.showLocalparseDateTo24Hours(close_time,ps_id).substring(0, 8));
						}
						//得到关联log
					   DBRow[]	sublog=null;
					   try {
							PageCtrl pageCtrl = new PageCtrl();
							pageCtrl.setPageSize(100);
							pageCtrl.setPageNo(1);
							
						    SpecialTaskLogBean queryJson = new SpecialTaskLogBean();
							queryJson.setModule(SpecialKey.INVOICE);
							queryJson.setRelation_id(bill_id);
							
							sublog=specialProjectLogUtil.queryLogs(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(queryJson), pageCtrl,ps_id);
							for (DBRow dbRow : sublog) {
							      String post_date=dbRow.get("post_date", null);
							      dbRow.add("post_date", DateUtil.showLocalparseDateTo24Hours(post_date,ps_id));
							  }
					        } catch (Exception e) {
							
						     }
					        result.add("sublog", sublog);
					        
					        //查询file_id
						   DBRow[] temp_file=floorCreatePdfMgr.Search(String.valueOf(adid_), ModuleKey.SPECIAL_TASK, PdfTypeKey.Invoice, Long.valueOf(bill_id));
						   if(temp_file!=null&&temp_file.length>0){
							   result.add("file_id", temp_file[0].get("file_id", null));
						   }
					}
					pc.setAllCount(1);
					pc.setPageCount(1);
					pc.setRowCount(1);
					json.put("pageCtrl", new JSONObject(pc));
					
					
					data=new DBRow[]{result};
					
					json.put("invoice".toUpperCase(), data);
					
                    this.setResponse(response);
					
					response.getWriter().print(json.toString());
			    }else{
					
					data=floorPrintLabelMgrGql.getAllBill(pc);//getBillItemByBillOrderId
					if(data!=null&&data.length>0){
						//处理数据 小数位数  --save
						for (int i = 0; i < data.length; i++) {
							double save=data[i].get("save", 0d);
							DecimalFormat format=new DecimalFormat("#.00");
							data[i].add("save", format.format(save));
						}
					}
					
					
					//得到每条invoice对应的detail
					DBRow[] detail=null;DBRow admininfo=new DBRow();
					for (DBRow dbRow : data) {
						long bill_id=dbRow.get("bill_id", 0l);
						Long ps_id=dbRow.get("ps_id", 0l);
						if(bill_id!=0l){
							//detail
							detail=floorPrintLabelMgrGql.getBillItemByBillOrderId(bill_id);
							//处理数据 格式 quantity amount rate
							if(detail!=null&&detail.length>0){
								for (int i = 0; i < detail.length; i++) {
									DecimalFormat format=new DecimalFormat("#.00");
									float quantity=detail[i].get("quantity", 0f);
									detail[i].add("quantity", format.format(quantity));
									double amount=detail[i].get("amount", 0d);
									detail[i].add("amount", format.format(amount));
									
									format=new DecimalFormat("#.0000");
									double rate=detail[i].get("rate", 0d);
									detail[i].add("rate", format.format(rate));
								}
							}
							if(detail!=null&&detail.length>0){
								dbRow.add("bill_detail", detail);
							}
							//关联的project
							detail=floorPrintLabelMgrGql.getRelationScheduleByBillId(String.valueOf(bill_id),true);
							if(detail!=null&&detail.length>0){
								//关联的main_task
								if(detail!=null&&detail.length>0){
									for (int i = 0; i < detail.length; i++) {
										DBRow[] rows=floorPrintLabelMgrGql.getRelationScheduleByBillId(String.valueOf(bill_id),false);
									    detail[i].add("subschedule", rows);
									}
								}
								dbRow.add("schedule", detail);
							}
							
						}
						long adid=dbRow.get("create_adid", 0l);
						if(adid!=0l){
							admininfo=floorPrintLabelMgrGql.getAdminInformationByAdid(adid);
							if(admininfo!=null&&!admininfo.isEmpty()){
								dbRow.add("admin_name", admininfo.getString("employe_name", null));
							}
						}
						String create_time=dbRow.get("create_date", null);
						if(!StringUtil.isBlank(create_time)){
							dbRow.remove("create_date");
							dbRow.add("create_date",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
						}
						
						String close_time=dbRow.get("close_time", null);
						if(!StringUtil.isBlank(close_time)){
							dbRow.remove("close_time");
							dbRow.add("close_time",DateUtil.showLocalparseDateTo24Hours(close_time,ps_id).substring(0, 8));
						}
						
						long pro_id=dbRow.get("pro_id", 0l);
						if(pro_id!=0l){
							DBRow proNameBean=floorPrintLabelMgrGql.getProvinceNameByProId(pro_id);
							if(proNameBean!=null){
								dbRow.add("address_state_code",proNameBean.get("p_code", null));
							}
							
						}
						
						//得到关联log
					   DBRow[]	sublog=null;
					   try {
							PageCtrl pageCtrl = new PageCtrl();
							pageCtrl.setPageSize(100);
							pageCtrl.setPageNo(1);
							
						    SpecialTaskLogBean queryJson = new SpecialTaskLogBean();
							queryJson.setModule(SpecialKey.INVOICE);
							queryJson.setRelation_id(bill_id);
							
							
							sublog=specialProjectLogUtil.queryLogs(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(queryJson), pageCtrl,ps_id);
							for (DBRow dbow : sublog) {
							      String post_date=dbow.get("post_date", null);
							      dbow.add("post_date", DateUtil.showLocalparseDateTo24Hours(post_date,ps_id));
							}	
					   } catch (Exception e) {
							
						}
					   dbRow.add("sublog", sublog);
					   
					   //查询file_id
					   DBRow[] temp_file=floorCreatePdfMgr.Search(String.valueOf(adid_), ModuleKey.SPECIAL_TASK, PdfTypeKey.Invoice, Long.valueOf(bill_id));
					   if(temp_file!=null&&temp_file.length>0){
						   dbRow.add("file_id", temp_file[0].get("file_id", null));
					   }
					}
					
					if(data!=null&&data.length>0){
						json.put("invoice".toUpperCase(), data);
					}
					
					json.put("pageCtrl",new JSONObject(pc));
					
					
					this.setResponse(response);
					
					response.getWriter().print(json.toString());
			    }
		}catch(Exception e){
			throw new SystemException(e,"getInvoiceAndDetail",log);
		}
	};
	
	/**
	 * 删除invoice
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void deleteInvoiceByInvoiceId(HttpServletRequest request, HttpServletResponse response) throws Exception{
		try{
			DBRow row=this.getParamFromRequest(request);
			if(!row.isEmpty()){
				Long billId=row.get("bill_id", 0l);
				if(billId!=0l){
					   AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
					   AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
					
				 	   long adid = adminLoggerBean.getAdid(); 
				 	   String name= adminLoggerBean.getEmploye_name();
				 	   DBRow[] department=adminLoggerBean.getDepartment();
				 	
					    Long schedule_id=row.get("schedule_id", 0l);
					    DBRow data=new DBRow();
					    if(schedule_id!=0l){
					    	//task页面invoice的删除 只是删除关系
					    	data.add("schedule_id", schedule_id);
					    	data.add("bill_id", billId);
					    	floorPrintLabelMgrGql.deleteScheduleAndInvoiceRelationship(data);
					    }else{
					    	 //删除invoice后 再删除关系
			  		    	 floorPrintLabelMgrGql.deleteBillById(billId);
					    	 data.add("bill_id", billId);
					    	 floorPrintLabelMgrGql.deleteScheduleAndInvoiceRelationship(data);
					    }
					    
					    try {
							SpecialTaskLogBean logBean=new SpecialTaskLogBean();
							logBean.setModule(SpecialKey.INVOICE);
							logBean.setType(SpecialKey.INVOICE);
							logBean.setRelation_id(Long.valueOf(billId));
							logBean.setAdid(adid);
							logBean.setAccount(name);
							logBean.setOperation_content(SpecialKey.OPERATION_INVOICE);
							logBean.setOperation_note(" Delete Invoice ");
							logBean.setDepartment(new JSONObject(department));
							
							floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
						} catch (Exception e) {
							//出现异常 不做任何处理
						}
						this.setResponse(response);
						response.getWriter().print(new JSONObject().put("success", true).toString());
						//删除索引
						InvoiceIndexMgr.getInstance().deleteIndex(billId);
				}
			}
			
		}catch(Exception e){
			this.setResponse(response);
			response.getWriter().print(new JSONObject().put("success", false).toString());
			throw new SystemException(e,"deleteScheduleByScheduleId",log);
		}
	};
	
	
	
	/**
	 * 删除schedule任务
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void deleteScheduleByScheduleId(HttpServletRequest request,HttpServletResponse response) throws Exception{
		try{
			DBRow row=this.getParamFromRequest(request);
			if(!row.isEmpty()){
				AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
	  		    AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
	            Long adid=adminLoginBean.getAdid();
	            String name=adminLoginBean.getEmploye_name();
	            DBRow[] department=adminLoginBean.getDepartment();
	            
				Long schedule_id=row.get("schedule_id", 0l);
				
				int task_type=row.get("is_main_schedule", 0); 
				
				String task_detail=row.get("schedule_detail", null);
				
				String operation_note="";
				
				if(schedule_id!=0l){
					    Long bill_id=row.get("bill_id", 0l);
					    if(bill_id!=0l){
					    	//invoice页面task的删除
					    	DBRow data=new DBRow();
					    	data.add("schedule_id", schedule_id);
					    	data.add("bill_id", bill_id);
					    	floorPrintLabelMgrGql.deleteScheduleAndInvoiceRelationship(data);
					    }else{
					    	floorPrintLabelMgrGql.deleteMainSchedule(schedule_id);
					    }
					    
					    
					    
					    
					    //增加日志
						try {
							Long project_id=null;
							
							SpecialTaskLogBean logBean=new SpecialTaskLogBean();
							logBean.setModule(SpecialKey.TASK);
							
							logBean.setAdid(adid);
							logBean.setAccount(name);
							logBean.setOperation_content(SpecialKey.OPERATION_TASK);
							
							project_id=this.getProjectIdByAnyId(Long.valueOf(schedule_id));
							if(task_type==3){
						    	operation_note=" Delete Sub Task "+task_detail;
						    	logBean.setType(SpecialKey.SUBTASK);
						    }else if(task_type==2){
						    	operation_note=" Delete Main Task "+task_detail;
						    	logBean.setType(SpecialKey.MAINTASK);
						    }else{
						    	operation_note=" Delete Project "+task_detail;
						    	logBean.setType(SpecialKey.PROJECT);
						    }
							logBean.setRelation_id(project_id==null?schedule_id:Long.valueOf(project_id));
							logBean.setOperation_note(operation_note);
							logBean.setDepartment(new JSONObject(department));
							
							floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
						} catch (Exception e) {
							//出现异常 不做任何处理
						}
						this.setResponse(response);
						response.getWriter().print(new JSONObject().put("success", true).toString());
						//删除索引
						ScheduleIndexMgr.getInstance().deleteIndex(schedule_id);
				}
			}
		}catch(Exception e){
			this.setResponse(response);
			response.getWriter().print(new JSONObject().put("success", false).toString());
			throw new SystemException(e,"deleteScheduleByScheduleId",log);
		}
	};
	
	
	 /**
	  * 根据client_id得到客户信息
	  */
	public void getAddressInfoListByClientId(HttpServletRequest request, HttpServletResponse response) throws Exception{
	   String client_id=StringUtil.getString(request, "client_id");
	   if(!StringUtil.isBlank(client_id)){
		   DBRow[] list=floorPrintLabelMgrGql.getAddressInfoListByClientId(client_id);
		   this.setResponse(response);
		   response.getWriter().print(JsonUtils.toJsonArray(list));
	   }
	};
	/**
	 * 得到账户信息
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void getAccountInfoList(HttpServletRequest request,HttpServletResponse response) throws Exception{
		String id=StringUtil.getString(request, "id",null);
		if(!StringUtil.isBlank(id)){
			this.setResponse(response);
			DBRow[] array=floorPrintLabelMgrGql.getAccountInfoList(Long.valueOf(id));
			response.getWriter().print(JsonUtils.toJsonArray(array));
		}
	}
	/**
	 * 得到country
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void getAllCountryCode(HttpServletRequest request, HttpServletResponse response) throws Exception{
		DBRow[] list=floorPrintLabelMgrGql.getAllCountryCode();
		if(list!=null&&list.length>0){
			this.setResponse(response);
			response.getWriter().print(JsonUtils.toJsonArray(list));
		}
	};
	/**
	 * 得到country下的state
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void getStorageProvinceByCcid(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String ccid=StringUtil.getString(request, "ccid");
		if(!StringUtil.isBlank(ccid)){
			DBRow[] list=floorPrintLabelMgrGql.getStorageProvinceByCcid(Long.valueOf(ccid));
				this.setResponse(response);
				response.getWriter().print(JsonUtils.toJsonArray(list));
		}
	};
	/**
	 * 得到admin信息
	 */
	public void getAdminInformation(HttpServletRequest request, HttpServletResponse response) throws Exception{
		JSONObject json=new JSONObject();
		
		HttpSession session=request.getSession(true);
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
	  	
		json.put("adminBean", new JSONObject(adminLoggerBean));
		
		this.setResponse(response);
		
		response.getWriter().print(json.toString());
	};
	/**
	 * 得到configvalue
	 * @throws Exception 
	 */
	public void getConfigValue(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String[] keys=new String[]{"USD","AUD","EUR","CAD","CHF","CZK","GBP","NOK"};
		ArrayList<DBRow> list=new ArrayList<DBRow>();
		DBRow row=null;
		SystemConfig systemConfig=new SystemConfig();
		//增加一个RMB
		row=new DBRow();
		row.add("target", "RMB");
		row.add("value", "1");
		row.add("description", "人民币");
		list.add(row);
		for (String key : keys) {
			row=new DBRow();
			row.add("target", key);
			String value=systemConfig.getStringConfigValue(key);
			row.add("value", value);
			String description=systemConfig.getStringConfigDescription(key);
			row.add("description", description);
			list.add(row);
		}
		
		this.setResponse(response);
		response.getWriter().print(JsonUtils.toJsonArray(list));
	}
	 /**
	  * 得到用户的所有信息
	  */
    public void getAllUserInformationAction(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try{
    		 DBRow[] data=floorPrintLabelMgrGql.getAllUserInfoAndDept();
 		     this.setResponse(response);
 			 response.getWriter().print(StringUtil.convertDBRowArrayToJsonString(data));
		}catch (Exception e) {
			throw new SystemException(e,"getAllUserInformationAction",log);
		}
    };
	 /**
   	  * 根据客户关键字搜索   索引方式
   	  * @param 是否搜索schedule  是 schedule 否 invoice 
   	  */
    public void getSearchResultByIndex(HttpServletRequest request, HttpServletResponse response,boolean isScheduleSearch) throws Exception{
    	try
        {
    		String param=StringUtil.getString(request, "param");
    		SystemConfig systemConfig=new SystemConfig();
    		int suggestNum = systemConfig.getIntConfigValue("page_size");
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(suggestNum);
			if(!param.equals("")&&!param.equals("\""))
			{
				param = param.replaceAll("\"","");
				param = param.replaceAll("\\*","");
				param +="*";
				
			}
			DBRow bills[]=null;
        	if(isScheduleSearch){
        		 bills=ScheduleIndexMgr.getInstance().mergeSearch("merge_field",param.toLowerCase(), pc);
        	}else{
        		 bills= InvoiceIndexMgr.getInstance().mergeSearch("merge_field",param.toLowerCase(), pc);
        	}
        	response.getWriter().print(JsonUtils.toJsonArray(bills));
        }
        catch(Exception e)
        {
            throw new Exception((new StringBuilder()).append("deleteScheduleById(id):").append(e).toString());
        }
    	
    };
    /**
     * 关闭任务
     */
    public void closeTask(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try{
    		DBRow data=this.getParamFromRequest(request);
    		if(!data.isEmpty()){
    			
    			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
      		    AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
                Long adid=adminLoginBean.getAdid();
                long createManId=adminLoginBean.getAdid();
                String name=adminLoginBean.getEmploye_name();
                DBRow[] department=adminLoginBean.getDepartment();
                
               //创建时间
    		    Date date=(Date)Calendar.getInstance().getTime();
    			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    			String createTime = dateformat.format(date);
    			
    			String emailTitle="";
    			
    			String context="";
    			
    			Long schedule_id=data.get("schedule_id", 0l);
    			
    			String task_detail=data.get("schedule_detail", null);
    			
    			boolean is_main=false,is_invoice=false;
    			
    			String operation_note=" Close ";
    			
    			int task_type=data.get("is_main_schedule", 0);
    				if(schedule_id!=null){
    					int num=-1;DBRow closeInfo=null;
    					if(task_type==3){
    						//sub task 其实应该迭代
    						floorPrintLabelMgrGql.closeTask(schedule_id);
    						closeInfo=floorPrintLabelMgrGql.isChildTaskAllClosed(schedule_id,false);
    						
    						operation_note+=" Sub Task "+task_detail;
    						
    						num=closeInfo.get("num", -1);
    						if(num==0){
    							//关闭 main task 
    							schedule_id=closeInfo.get("task_id", 0l);
    							is_main=floorPrintLabelMgrGql.closeTask(schedule_id)>0?true:false;
    							//  false 是否open invoice
    							closeInfo=floorPrintLabelMgrGql.isChildTaskAllClosed(schedule_id,true);
    							num=closeInfo.get("num", -1);
    							if(num==0){
    								schedule_id=closeInfo.get("task_id", 0l);
    								
    								//open invoice 
    								try {
										Long invoice_id=closeInfo.get("invoice_id", 0l);
										DBRow row=new DBRow();
										row.add("bill_id", invoice_id);
										row.add("invoice_status", InvoiceStatusKey.OPEN);
										floorPrintLabelMgrGql.updateBill(invoice_id, row);
										is_invoice=true;
										
										row=floorPrintLabelMgrGql.getSingleTaskInfomationByTaskId(schedule_id);
										String exe_user_ids=row.get("assign_user_id", null);
										
										//组织邮件标题
										emailTitle="Invoice "+invoice_id+" is ready for your review";
										
										//组织邮件内容
										context="Invoice#"+invoice_id+" is ready for your review. \n";
										context+="\n";
										context+="Associated tasks (Project#"+schedule_id+"):\n";
										DBRow[] main_task=floorPrintLabelMgrGql.getRelationScheduleByBillId(String.valueOf(invoice_id), false);
										int sort=1;
										for (int j = 0; j < main_task.length; j++) {
											context+=sort+"."+main_task[j].get("schedule_detail", "")+"\n";
											sort++;
										}
										
										scheduleMgrZr.addScheduleByExternalArrangeNow(createTime,"",createManId,exe_user_ids,"",invoice_id, ModuleKey.SPECIAL_TASK, emailTitle, context, false, true, false,request,true,ProcessKey.INVOICE);
										
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
    							}
    						}
    					}else if(task_type==2){
    					    //main task
    						floorPrintLabelMgrGql.closeTask(schedule_id);
    						
    						operation_note+=" Main Task "+task_detail;
    						
    						// true invoice open
    						closeInfo=floorPrintLabelMgrGql.isChildTaskAllClosed(schedule_id,true);
    						num=closeInfo.get("num", -1);
							if(num==0){
								//关闭  project 
								schedule_id=closeInfo.get("task_id", 0l);
								
								//open invoice 
								try {
									Long invoice_id=closeInfo.get("invoice_id", 0l);
									DBRow row=new DBRow();
									row.add("bill_id", invoice_id);
									row.add("invoice_status", InvoiceStatusKey.OPEN);
									floorPrintLabelMgrGql.updateBill(invoice_id, row);
									
									row=floorPrintLabelMgrGql.getSingleTaskInfomationByTaskId(schedule_id);
									String exe_user_ids=row.get("assign_user_id", null);
									
									//组织邮件标题
									emailTitle="Invoice "+invoice_id+" is ready for your review";
									
									//组织邮件内容
									context="Invoice#"+invoice_id+" is ready for your review. \n";
									context+="\n";
									context+="Associated tasks (Project#"+schedule_id+"):\n";
									DBRow[] main_task=floorPrintLabelMgrGql.getRelationScheduleByBillId(String.valueOf(invoice_id), false);
									int sort=1;
									for (int j = 0; j < main_task.length; j++) {
										context+=sort+"."+main_task[j].get("schedule_detail", "")+"\n";
										sort++;
									}
									
									
									scheduleMgrZr.addScheduleByExternalArrangeNow(createTime,"",createManId,exe_user_ids,"",invoice_id, ModuleKey.SPECIAL_TASK, emailTitle, context, false, true, false,request,true,ProcessKey.INVOICE);
									
									is_invoice=true;
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
    					}else{
    					    //project
    						floorPrintLabelMgrGql.closeTask(schedule_id);
    						
    						operation_note+=" Project "+task_detail;
    					}
    					
        				
        				//增加日志
						try {
							Long project_id=null;
							
							SpecialTaskLogBean logBean=new SpecialTaskLogBean();
							logBean.setModule(SpecialKey.TASK);
							project_id=this.getProjectIdByAnyId(Long.valueOf(schedule_id));
							if(task_type==3){
						    	logBean.setType(SpecialKey.SUBTASK);
						    }else if(task_type==2){
						    	logBean.setType(SpecialKey.MAINTASK);
						    }else{
						    	logBean.setType(SpecialKey.PROJECT);
						    }
							
							logBean.setRelation_id(project_id==null?schedule_id:Long.valueOf(project_id));
							logBean.setAdid(adid);
							logBean.setAccount(name);
							logBean.setOperation_content(SpecialKey.OPERATION_TASK);
							logBean.setOperation_note(operation_note);
							logBean.setDepartment(new JSONObject(department));
							
							floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
						} catch (Exception e) {
							//出现异常 不做任何处理
						}
        				
        				
        				this.setResponse(response);
        				response.getWriter().print(new JSONObject().put("success", true).put("is_main_close", is_main).put("is_invoice_open",is_invoice).toString());
    			}
    		}
    	}catch(Exception e){
    		 this.setResponse(response);
			 response.getWriter().print(new JSONObject().put("success", false).put("is_main_close", false).toString());
    		 throw new Exception((new StringBuilder()).append("closeTask():").append(e).toString());
    	}
    };
    /**
     * 关闭invoice
     */
    public void closeInvoice(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try{
    		DBRow data=this.getParamFromRequest(request);
    		if(!data.isEmpty()){
    			Long bill_id=data.get("bill_id", 0l);
    			if(bill_id!=0l){
    				   AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
					   AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
					
				 	   long adid = adminLoggerBean.getAdid(); 
				 	   String name= adminLoggerBean.getEmploye_name();
				 	   DBRow[] department=adminLoggerBean.getDepartment();
				 	   
    				   floorPrintLabelMgrGql.closeInvoice(bill_id);;
    				
    				  try {
							SpecialTaskLogBean logBean=new SpecialTaskLogBean();
							logBean.setModule(SpecialKey.INVOICE);
							logBean.setType(SpecialKey.INVOICE);
							logBean.setRelation_id(Long.valueOf(bill_id));
							logBean.setAdid(adid);
							logBean.setAccount(name);
							logBean.setOperation_content(SpecialKey.OPERATION_INVOICE);
							logBean.setOperation_note(" Close Invoice ");
							logBean.setDepartment(new JSONObject(department));
							
							floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
						} catch (Exception e) {
							//出现异常 不做任何处理
						}
    				
    				this.setResponse(response);
    				response.getWriter().print(new JSONObject().put("success", true).toString());
    			}
    		}
    	}catch(Exception e){
    		 this.setResponse(response);
			 response.getWriter().print(new JSONObject().put("success", false).toString());
    		 throw new Exception((new StringBuilder()).append("closeTask():").append(e).toString());
    	}
    };
	/**
	 * response 设置
	 * @response
	 */
	private void setResponse(HttpServletResponse response){
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}
	
	/**
	 * 从request中取出参数值
	 * @param request
	 * @throws IOException 
	 * @throws JSONException 
	 */
	private DBRow getParamFromRequest(HttpServletRequest request) throws Exception{
		DBRow dbRow=new DBRow();
		//Step 1
		 String result=IOUtils.toString(request.getReader());
		 if(!StringUtil.isBlank(result)){
			 JSONObject jsonData = new JSONObject(result);
			 dbRow=DBRowUtils.convertToDBRow(jsonData);
		 }
		//Step 2
		Enumeration<String> paramEnum=request.getParameterNames();
		while (paramEnum != null &&paramEnum.hasMoreElements()) {
			String key = (String) paramEnum.nextElement();
			dbRow.add(key, StringUtil.getString(request, key));
		}
		return dbRow;
	}
	/**o
	 * 查询所有customer信息
	 * @return
	 * @throws Exception
	 */
	public void getAllCustomerId(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try
        {   
//    		 DBRow[] data=floorPrintLabelMgrGql.getAllCustomerId();
    		DBRow[] customer = floorPrintLabelMgrGql.getAllCustomerId();
			Map<String,String> map = new HashMap<String, String>();
			List<DBRow> temp_customer = new ArrayList<DBRow>();
			for(DBRow row:customer){      //先将不含有 "," 的放入list
				if(row.get("customer_id", "").contains(",")){  
					String[] customers = row.get("customer_id", "").split(",");
					for (String s : customers) {
						map.put(s, s);
					}
					continue;
				}
				map.put(row.get("customer_id", ""), row.get("customer_key", ""));
			}
			for(String key:map.keySet()){
				DBRow row = new DBRow();
				row.add("customer_id", key);
				row.add("customer_key", map.get(key));
				temp_customer.add(row);
			}
			
			 customer = (DBRow[]) temp_customer.toArray(new DBRow[0]);
    		 this.setResponse(response);
    		 response.getWriter().print(JsonUtils.toJsonArray(customer));
        }
        catch(Exception e)
        {
        	throw new SystemException(e,"getAllCustomerId",log);
        }
    	
    };
    
	
   
    
	 /**
	   * 生成pdf bill_id
	   */
    @SuppressWarnings({"unchecked" })
	public void exportPdf(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try{ 
//   			 String result=IOUtils.toString(request.getReader());
//   			 if(!StringUtil.isBlank(result)){
//   			 }
//    		
//    		 String html1=request.getParameter("bill_html");
//    		 String html2=(String) request.getAttribute("bill_html");
//    		 ServletInputStream tim=request.getInputStream();
//    		 
//    		 
//    		 String html=StringUtil.getString(request,"bill_html"); 
//             String bill_id=StringUtil.getString(request,"bill_id"); 
//             String bill_css=StringUtil.getString(request,"bill_css"); 
//    		if(!StringUtil.isBlank(html)){
//    			if(!StringUtil.isBlank(html)){
//    				File file=new File(PDFPath);
//		       	    if(!file.exists()){
//		       	    	file.mkdirs();
//		       	    }    
//    		       	    
//    				
//    				
//    				imgToPdfMgr.handleHtmlToPdf(html, PDFPath+bill_id+".pdf");
//    				
//    				
//    				ServletOutputStream out = response.getOutputStream();
//    				FileChannel fc  = new RandomAccessFile(PDFPath+bill_id+".pdf", "r").getChannel();  
//    				MappedByteBuffer byteBuffer = fc.map(MapMode.READ_ONLY, 0,  
//    				        fc.size()).load();  
//    				byte[] pdfByte = new byte[(int) fc.size()];  
//    				if (byteBuffer.remaining() > 0) {  
//    				    byteBuffer.get(pdfByte, 0, byteBuffer.remaining());  
//    				}  
//    				fc.close();
//    				
//    				response.setContentType("application/pdf");
//    				response.setContentLength(pdfByte.length);
//    				String file_name=DateUtil.NowStr();    	    
//    				response.setHeader("Content-Disposition", "attachment; filename=\""+file_name+".pdf\"");
//    				out.write(pdfByte, 0, pdfByte.length);
//    				out.flush();
//    				out.close();
//    				
//    				
//    				
//    			}
//    		}
    		
    		
    		//plan 2
    		String bill_id =StringUtil.getString(request, "bill_id");
    		if(!StringUtil.isBlank(bill_id)){
    			DBRow result = billMgrZr.getPDFDatas(Long.parseLong(bill_id));
    			
    			String reporViewTemplatetFilePath = result.getString("view_path");
    			DBRow dataRows[] = (DBRow[])result.get("items",new Object());
    			HashMap<String, String> paras = (HashMap<String, String>)result.get("paras",new Object());

    			ReportCenter reportCenter = new ReportCenter(reporViewTemplatetFilePath, paras,dataRows);
    			JasperPrint printer = reportCenter.getPrinter();
    	 

    			//pdf
    			ServletOutputStream out = response.getOutputStream();
    			byte[] pdfByte = JasperExportManager.exportReportToPdf(printer);
    			response.setContentType("application/pdf");
    		    response.setContentLength(pdfByte.length);
    		    response.setHeader("Content-Disposition", "attachment; filename=\"VisionariInvoice"+String.valueOf(bill_id)+".pdf\"");
    		    out.write(pdfByte, 0, pdfByte.length);
    	        out.flush();
    	        out.close();
    		}
    	}catch(Exception e){
    		throw new Exception((new StringBuilder()).append("exportPdf").append(e).toString());
    	}
    };
   
    /**
     * 对子任务排序
     */
    public void sortSubScheduleNumber(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try{
    		 DBRow data=this.getParamFromRequest(request);this.setResponse(response);
      	     if(!data.isEmpty()){
      		   String sortSubTask=data.getString("sortSubTask", null);
      		   if(!StringUtil.isBlank(sortSubTask)){
      			   JSONArray array=new JSONArray(sortSubTask);
      			   for (int i = 0; i < array.length(); i++) {
  					 String schedule_id=array.getJSONObject(i).getString("schedule_id".toUpperCase());
  					 String task_number=array.getJSONObject(i).getString("task_number".toUpperCase());
  					floorPrintLabelMgrGql.updateTaskNumberById(schedule_id, task_number);
  				}
      		   }
      	     }
      	     response.getWriter().print(new JSONObject().put("success", true).toString());
    	}catch(Exception e){
    		 response.getWriter().print(new JSONObject().put("success", false).toString());
    		 throw new Exception((new StringBuilder()).append("sortSubScheduleNumber").append(e).toString());
    	}
    };
    /**
     * 增加log日志
     */
    public void addLogToSchedule(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try{
			DBRow data=this.getParamFromRequest(request);this.setResponse(response);
			if(!data.isEmpty()){
				String schedule_id=data.get("schedule_id", null);
				if(!StringUtil.isBlank(schedule_id)){
					DBRow row=new DBRow();
					row.add("sch_replay_context", data.getString("sch_replay_context", null));
					row.add("sch_replay_time", DateUtil.NowStr());
					row.add("sch_replay_type", data.get("sch_replay_type", '1'));
					row.add("schedule_id", schedule_id);
					row.add("adid", data.get("adid", null));
					row.add("employe_name", data.get("employe_name", null));
					row.add("sch_state", 1);
					Long id=floorPrintLabelMgrGql.insertScheduleReplay(row);
					if(id!=0l){
						response.getWriter().print(new JSONObject().put("success", true).toString());
					}
				}
			}
    	}catch(Exception e){
    		response.getWriter().print(new JSONObject().put("success", false).toString());
   		    throw new Exception((new StringBuilder()).append("sortSubScheduleNumber").append(e).toString());
    	}
    };
    /**
     * 根据主任务Id查询子任务
     */
    public void getSubTaskByMainId(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try{
			DBRow data=this.getParamFromRequest(request);this.setResponse(response);DBRow bean=new DBRow();
			if(!data.isEmpty()){
				HttpSession session=request.getSession(true);
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
			  	
			   	long ps_id=adminLoggerBean.getPs_id();
				
				String parent_schedule_id=data.get("parent_schedule_id", null);
				if(!StringUtil.isBlank(parent_schedule_id)){
					DBRow[] result=floorPrintLabelMgrGql.getRelationTaskByScheduleId(parent_schedule_id,null);
					for (int i = 0; i < result.length; i++) {
						//处理时间格式
						String  start_time=result[i].get("start_time", null);
						if(!StringUtil.isBlank(start_time)){
							result[i].remove("start_time");
							result[i].add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
						}
						String  end_time=result[i].get("end_time", null);
						if(!StringUtil.isBlank(end_time)){
							result[i].remove("end_time");
							result[i].add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
						}
						String  create_time=result[i].get("create_time", null);
						if(!StringUtil.isBlank(create_time)){
							result[i].remove("create_time");
							result[i].add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
						}
						String  finish_time=result[i].get("finish_time", null);
						if(!StringUtil.isBlank(finish_time)){
							result[i].remove("finish_time");
							result[i].add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
						}
						//得到所属的主任务
						String main_schedule_id=result[i].get("schedule_id", null);
						//得到关联的子任务
						if(!StringUtil.isBlank(main_schedule_id)){
							DBRow[] sub_task=floorPrintLabelMgrGql.getRelationTaskByScheduleId(main_schedule_id,null);
							result[i].add("subschedule", sub_task);
						}
						//得到关联的invoice
						if(!StringUtil.isBlank(main_schedule_id)){
							DBRow[] main_invoice=floorPrintLabelMgrGql.getRelationTaskByInvoiceIdOrInvoiceByTaskId(main_schedule_id, true);
							if(main_invoice.length>0){
								result[i].add("bill_id", main_invoice[0].get("bill_id", null));
							}
						}
					}
					bean.add("maintasks", result);
					//得到所属的所有的invoice
					DBRow[] invoices=floorPrintLabelMgrGql.getRelationInvoiceByProjectId(parent_schedule_id);
					
					DBRow[] project_invoice=new DBRow[invoices.length]; DBRow row=null;
					for (int i = 0; i < project_invoice.length; i++) {
						row=new DBRow();
						String bill_id=invoices[i].get("bill_id", null);
						int invoice_status=invoices[i].get("invoice_status", -1);
						row.add("bill_id", bill_id);
						row.add("invoice_status", invoice_status);
						project_invoice[i]=row;
					}
					
					bean.add("projectinvoice",project_invoice);
					
				    response.getWriter().print(new JSONObject(bean));
				}
			}
    	}catch(Exception e){
    		response.getWriter().print(new JSONObject().put("success", false).toString());
   		    throw new Exception((new StringBuilder()).append("sortSubScheduleNumber").append(e).toString());
    	}
    };
    /**
     * 根据warehouseid及groupid查询所有的用户
     */
    public void getUserByWarehouseIdAndGroupId(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try {
    		DBRow data=this.getParamFromRequest(request);this.setResponse(response);
    		DBRow[] result=null;
        	if(!data.isEmpty()){
        		Long ps_id=data.get("warehouse_id", 0l);
        		Long group_id=data.get("group_id", 0l);
        		Long projs_id=data.get("user_id", -2l);
        		if(ps_id!=null&&ps_id!=0l){
        			result=floorPrintLabelMgrGql.getAllUserAndDept(projs_id, ps_id, group_id);
        		}
        	}
        	response.getWriter().print(JsonUtils.toJsonArray(result));
		} catch (Exception e) {
			response.getWriter().print(new JSONObject().put("success", false).toString());
   		    throw new Exception((new StringBuilder()).append("getUserByWarehouseIdAndGroupId").append(e).toString());
		}
    };
     /**
     * 生成invoice pdf
     */
    public void createPdfOfInvoice(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	
    	
    };
    
    
    
    /**
     * assign main_task 分配主任务到supervisor 
     */
    public void assignMainTask(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try {
//   		    DBRow  data=this.getParamFromRequest(request);
//   		    this.setResponse(response);
//   		    //创建时间
//		    Date date=(Date)Calendar.getInstance().getTime();
//			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			String createTime = dateformat.format(date);
//			//获得创建人
//			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
//			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
//			long createManId = adminLoginBean.getAdid();
//   		    
//   	       if(!data.isEmpty()){
//   	    	   //step 1 schedule_sub 执行人
//   	    	   String  assignJson=data.getString("assignJson", null);
//   	    	   
//   	    	   Long main_task_status=data.get("project_status", -1l);
//   	    	   
//   	    	   String main_task_id=data.get("project_id", null);
//   	    	   
//   	    	   String  schedule_id=null;String exe_user_ids=null,def_user_ids=null;DBRow row=null;
//   	    	   if(!StringUtil.isBlank(assignJson)){
//   	    		 JSONArray array=new JSONArray(assignJson);
//   	    		 for (int i = 0; i < array.length(); i++) {
//					JSONObject json=array.getJSONObject(i);
//					schedule_id=json.getString("schedule_id");
//					
//					if(json.has("def_user_ids")){
//						def_user_ids=json.getString("def_user_ids");
//						JSONArray defbean=new JSONArray(def_user_ids);
//						for (int j = 0; j < defbean.length(); j++) {
//							String def_id=defbean.getString(j);
//							row=new DBRow();
//							row.add("task_id", schedule_id);
//					        row.add("task_execute_id", def_id);
//					        floorPrintLabelMgrGql.androidDelScheduleSub(row);
//					        //删除通知
//					        //scheduleMgrZr.deleteScheduleByScheduleId(shRows[0].get("schedule_id", 0l), adminLoginBean); //删除通知
//						}
//					}
//					
//					if(json.has("exe_user_id")){
//						exe_user_ids=json.getString("exe_user_id");
//						JSONArray exebean=new JSONArray(exe_user_ids);
//						for (int j = 0; j < exebean.length(); j++) {
//							String exe_id=exebean.getString(j);
//							 //step 1 schedule_sub 执行人
//							row=new DBRow();
//					        row.add("task_execute_id", exe_id);
//					        row.add("task_id", schedule_id);
//					  
//					        floorPrintLabelMgrGql.androidAddScheduleSub(row);
//					        //发通知
////					        String emailTitle="this is main task";
////					        String context="你有一个主任务需要处理，单据号为："+schedule_id;
////					        scheduleMgrZr.addScheduleByExternalArrangeNow(createTime,"",createManId,this.convertStringByAnyChar(exe_user_ids),"",Long.parseLong(schedule_id), ModuleKey.SPECIAL_TASK, emailTitle, context, false, true, false,request,true,70);
//						 }
//					}
//					
//				  }
//   	    	    }
//   	    	   
//   	    	   //update main_task_status 
//   	    	   if(main_task_status!=null&&main_task_status!=-1l&&main_task_status==TaskStatusKey.OPEN){
//   	    		   if(!StringUtil.isBlank(main_task_id)){
//   	    			   floorPrintLabelMgrGql.androidUpdateTaskStatus(main_task_id,TaskStatusKey.PROCESSING);
//   	    			   
//   	    		   }
//   	    	   }
//   	       }
		} catch (Exception e) {
  		    throw new Exception((new StringBuilder()).append("androidAssignSubTask").append(e).toString());
		}
    };
    /**
     * get project by search_param
     */
    public void getProjectBySearchParam(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try {
    		DBRow row=this.getParamFromRequest(request);JSONObject json=new JSONObject();
        	PageCtrl pc = new PageCtrl();
        	HttpSession session=request.getSession(true);
    		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
    	  	
    	   	long ps_id=adminLoggerBean.getPs_id();
        	if(!row.isEmpty()){
        		int pageNo = row.get("pageNo", 1);
        		int pageSize = row.get("pageSize",5);
        		pc.setPageNo(pageNo);
        		pc.setPageSize(pageSize);
        		DBRow bean=new DBRow();
        		
        		
        		//改成通用的 注意 bean 里面add的key 要与数据库字段对应
        		if(row.containsKey("task_status")){
        			int task_status=row.get("task_status", -1);
        			if(task_status!=-1){
        				bean.add("task_status", task_status);
        			}
        		}
        		if(row.containsKey("start_time")){
        			String start_time=row.get("start_time", null);
        			if(!StringUtil.isBlank(start_time)){
        				bean.add("start_time",  DateUtil.showUTCTime(formatDate(start_time), ps_id));
        			}
        		}
        		if(row.containsKey("end_time")){
        			String end_time=row.get("end_time", null);
        			if(!StringUtil.isBlank(end_time)){
        				bean.add("end_time",  DateUtil.showUTCTime(formatDate(end_time), ps_id));
        			}
        		}
        		
        		
        		
        		DBRow[] data=floorPrintLabelMgrGql.getProjectBySearchParam(bean, pc);
        		
        		DBRow[] schedule_return=new DBRow[data.length];
    			
    			
    			if(data!=null&&data.length>0){
    				DBRow temp=null;String schedule_id=null;DBRow[] subSchedule=null;
    				for (int i = 0; i < data.length; i++) {
    					temp=data[i];
    					if(temp!=null){
    						schedule_id=temp.getString("schedule_id");
    						//得到关联主任务任务
    						subSchedule=floorPrintLabelMgrGql.getRelationTaskByScheduleId(schedule_id,null);
//    						for (int j = 0; j < subSchedule.length; j++) {
//    							String sub_schedule_id=subSchedule[j].get("schedule_id", null);
//    							//得到关联的子任务
//    							if(!StringUtil.isBlank(sub_schedule_id)){
//    								DBRow[] sub_task_task=floorPrintLabelMgrGql.getRelationTaskByScheduleId(sub_schedule_id,null);
//    								subSchedule[j].add("subsubschedule", sub_task_task);
//    							}
//    						}
    						temp.add("subschedule", subSchedule);
    						
    						//处理时间格式
    						String  start_time=data[i].get("start_time", null);
    						if(!StringUtil.isBlank(start_time)){
    							data[i].remove("start_time");
    							data[i].add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
    						}
    						String  end_time=data[i].get("end_time", null);
    						if(!StringUtil.isBlank(end_time)){
    							data[i].remove("end_time");
    							data[i].add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
    						}
    						String  create_time=data[i].get("create_time", null);
    						if(!StringUtil.isBlank(create_time)){
    							data[i].remove("create_time");
    							data[i].add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
    						}
    						String  finish_time=data[i].get("finish_time", null);
    						if(!StringUtil.isBlank(finish_time)){
    							data[i].remove("finish_time");
    							data[i].add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
    						}
    						
    						//invoice
    						subSchedule=floorPrintLabelMgrGql.getRelationInvoiceByProjectId(schedule_id);
    						for (DBRow dbRow : subSchedule) {
    							String create_date=dbRow.get("create_date", null);
    							if(!StringUtil.isBlank(create_date)){
    								dbRow.remove("create_date");
    								dbRow.add("create_date",DateUtil.showLocalparseDateTo24Hours(create_date,ps_id).substring(0, 8));
    							}
    							double save=dbRow.get("save", 0d);
    							DecimalFormat format=new DecimalFormat("#.00");
    							dbRow.add("save", format.format(save));
    							//invoice detail
    							String bill_id=dbRow.get("bill_id", null);
    							if(!StringUtil.isBlank(bill_id)){
    								DBRow[] detail=floorPrintLabelMgrGql.getBillItemByBillOrderId(Long.parseLong(bill_id));
    								//处理数据 格式 quantity amount rate
    								if(detail!=null&&detail.length>0){
    									for (int k = 0; k < detail.length; k++) {
    										float quantity=detail[k].get("quantity", 0f);
    										detail[k].add("quantity", format.format(quantity));
    										double amount=detail[k].get("amount", 0d);
    										detail[k].add("amount", format.format(amount));
    										
    										format=new DecimalFormat("#.0000");
    										double rate=detail[k].get("rate", 0d);
    										detail[k].add("rate", format.format(rate));
    									}
    								}
    								dbRow.add("bill_detail", detail);
    							}
    						}
    						
    						
    						
    						temp.add("subinvoice", subSchedule);
    						
    						//得到关联file 只有主任务有
    						subSchedule=floorPrintLabelMgrGql.getSopByScheduleId(schedule_id);
    						for (DBRow dbRow : subSchedule) {
    							  start_time=dbRow.get("start_time", null);
    							if(!StringUtil.isBlank(start_time)){
    								dbRow.remove("start_time");
    								dbRow.add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
    							}
    							  end_time=dbRow.get("end_time", null);
    							if(!StringUtil.isBlank(end_time)){
    								dbRow.remove("end_time");
    								dbRow.add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
    							}
    							  create_time=dbRow.get("create_time", null);
    							if(!StringUtil.isBlank(create_time)){
    								dbRow.remove("create_time");
    								dbRow.add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
    							}
    							  finish_time=dbRow.get("finish_time", null);
    							if(!StringUtil.isBlank(finish_time)){
    								dbRow.remove("finish_time");
    								dbRow.add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
    							}
    						}
    						temp.add("subfile", subSchedule);
    						
    						//得到关联log
    						try {
    							PageCtrl pageCtrl = new PageCtrl();
    							pageCtrl.setPageSize(100);
    							pageCtrl.setPageNo(1);
    							
    						    SpecialTaskLogBean queryJson = new SpecialTaskLogBean();
    							queryJson.setModule(SpecialKey.TASK);;
    							queryJson.setRelation_id(Long.parseLong(schedule_id));
    							
    							
    							subSchedule=specialProjectLogUtil.queryLogs(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(queryJson), pageCtrl,ps_id);
    							for (DBRow dbRow : subSchedule) {
  							      String post_date=dbRow.get("post_date", null);
  							      dbRow.add("post_date", DateUtil.showLocalparseDateTo24Hours(post_date,ps_id));
  							    }
    						} catch (Exception e) {
    							
    						}
    						
    						temp.add("sublog", subSchedule);
    						schedule_return[i]=temp;
    					}
    				}
    			}
    			if(schedule_return!=null&&schedule_return.length>0){
    				json.put("schedule".toUpperCase(), data);
    			}
    			
    			json.put("pageCtrl",new JSONObject(pc));
    			
    			this.setResponse(response);
    			
    			response.getWriter().print(json.toString());
		     } 
         }catch (Exception e) {
			 throw new Exception((new StringBuilder()).append("getProjectBySearchParam").append(e).toString());
    		
    	}
    };
    /**
     * getprojectIdById
     * @throws Exception 
     */
    private Long getProjectIdByAnyId(Long task_id) throws Exception{
    	try {
    		 Long result=0l;
			 DBRow row=floorPrintLabelMgrGql.getParentIdByScheduleId(String.valueOf(task_id));
			 if(!row.isEmpty()){
				 Long id=row.get("parent_task_id",0l);
				 if(id==0l){
					 result=task_id;
				 }else{
					return getProjectIdByAnyId(id);
				 }
			 }else{
				 result=task_id;
			 }
			 return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 throw new Exception((new StringBuilder()).append("getProjectIdByAnyId").append(e).toString());
		}
    }
    /*android 开始*/
    /**
     * 根据adid查询project
     */
    public DBRow[] androidGetProjectByAdid(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try {
    		 DBRow  data=this.getParamFromRequest(request);DBRow[] result=null;this.setResponse(response);
    		 HttpSession session=request.getSession(true);
 			 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
 		  	
 		   	 long ps_id=adminLoggerBean.getPs_id();
 		   	 String ps_name=adminLoggerBean.getWarehouse()!=null&&adminLoggerBean.getWarehouse().length>0?adminLoggerBean.getWarehouse()[0].get("warename", null):null;
    	      
 		   	 if(!data.isEmpty()){
    	    	   String adid=data.get("adid", null);
    	    	   if(!StringUtil.isBlank(adid)){
    	    		   result=floorPrintLabelMgrGql.androidGetProjectByAdid(adid);
    	    		   for (int i = 0; i < result.length; i++) {
    	    			   String  start_time=result[i].get("start_time", null);
    						if(!StringUtil.isBlank(start_time)){
    							result[i].remove("start_time");
    							result[i].add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
    						}
    						String  end_time=result[i].get("end_time", null);
    						if(!StringUtil.isBlank(end_time)){
    							result[i].remove("end_time");
    							result[i].add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
    						}
    						result[i].add("ps_id", ps_id);
    						result[i].add("warehouse", ps_name);
					}
    	    	   }
    	       }
    	       return result;
		} catch (Exception e) {
   		    throw new Exception((new StringBuilder()).append("androidGetProjectByAdid").append(e).toString());
		}
    };
    /**
     * 根据schedule_id查询main的详细信息
     */
    public DBRow androidMainTaskByScheduleId(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try {
   		    DBRow  data=this.getParamFromRequest(request);DBRow schedule=null;
   		    this.setResponse(response);
   		    
			HttpSession session=request.getSession(true);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		  	
			long adid=adminLoggerBean.getAdid();
		   	long ps_id=adminLoggerBean.getPs_id();
   	       if(!data.isEmpty()){
   	    	   Long schedule_id=data.get("schedule_id", 0l);
   	    	   if(schedule_id!=null&&schedule_id!=0l){
   	    		  schedule=floorPrintLabelMgrGql.getScheduleByScheduleId(schedule_id);
   	    	      //处理时间格式
					String  start_time=schedule.get("start_time", null);
					if(!StringUtil.isBlank(start_time)){
						schedule.remove("start_time");
						schedule.add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
					}
					String  end_time=schedule.get("end_time", null);
					if(!StringUtil.isBlank(end_time)){
						schedule.remove("end_time");
						schedule.add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
					}
					String  create_time=schedule.get("create_time", null);
					if(!StringUtil.isBlank(create_time)){
						schedule.remove("create_time");
						schedule.add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
					}
					String  finish_time=schedule.get("finish_time", null);
					if(!StringUtil.isBlank(finish_time)){
						schedule.remove("finish_time");
						schedule.add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
					}
			    	
			    	
			    	DBRow[] subSchedule=null;
			    	
			    	//得到关联主任务
					subSchedule=floorPrintLabelMgrGql.getRelationTaskByScheduleId(String.valueOf(schedule_id),String.valueOf(adid));
					for (int j = 0; j < subSchedule.length; j++) {
						//处理时间格式
						  start_time=subSchedule[j].get("start_time", null);
						if(!StringUtil.isBlank(start_time)){
							subSchedule[j].remove("start_time");
							subSchedule[j].add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
						}
						  end_time=subSchedule[j].get("end_time", null);
						if(!StringUtil.isBlank(end_time)){
							subSchedule[j].remove("end_time");
							subSchedule[j].add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
						}
						  create_time=subSchedule[j].get("create_time", null);
						if(!StringUtil.isBlank(create_time)){
							subSchedule[j].remove("create_time");
							subSchedule[j].add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
						}
						  finish_time=subSchedule[j].get("finish_time", null);
						if(!StringUtil.isBlank(finish_time)){
							subSchedule[j].remove("finish_time");
							subSchedule[j].add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
						}
					}
					schedule.add("subschedule", subSchedule);
   	    	   }
   	       }
   	       return schedule;
		} catch (Exception e) {
  		    throw new Exception((new StringBuilder()).append("androidGetProjectInfomationByScheduleId").append(e).toString());
		}
    };
    /**
     * 根据schedule_id查询maintask的按时间分组
     */
    @SuppressWarnings("unchecked")
	public DBRow androidGetMainTaskGroupByTime(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try {
   		    DBRow  data=this.getParamFromRequest(request);DBRow schedule=null;DBRow result=new DBRow();
   		    this.setResponse(response);
   		    
			HttpSession session=request.getSession(true);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
		  	
		   	long ps_id=adminLoggerBean.getPs_id();
			String ps_name=adminLoggerBean.getWarehouse()!=null&&adminLoggerBean.getWarehouse().length>0?adminLoggerBean.getWarehouse()[0].get("warename", null):null;
			
   	        if(!data.isEmpty()){
   	    	   Long schedule_id=data.get("schedule_id", 0l);
   	    	   if(schedule_id!=null&&schedule_id!=0l){
   	    		   schedule=floorPrintLabelMgrGql.getScheduleByScheduleId(schedule_id);
   	    		   
   	    	        String subject=schedule.get("schedule_overview", null);
   	    	        result.add("subject", subject);
   	    	        String lr_no=schedule.get("lr_no", null);
   	    	        result.add("lr_no", lr_no);
   	    	        int lr_type=schedule.get("lr_type", 0);
   	    	        result.add("lr_type", lr_type);
   	    		    result.add("schedule_id", schedule_id);
   	    		    result.add("ps_id", ps_id);
   	    		    result.add("warehouse", ps_name);
   	    		    
					String  start_time=schedule.get("start_time", null);
					 //处理时间格式
					if(!StringUtil.isBlank(start_time)){
						result.add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
					}
					String  end_time=schedule.get("end_time", null);
					if(!StringUtil.isBlank(end_time)){
						result.add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
					}
//					String  create_time=schedule.get("create_time", null);
//					if(!StringUtil.isBlank(create_time)){
//						schedule.remove("create_time");
//						result.add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
//					}
//					String  finish_time=schedule.get("finish_time", null);
//					if(!StringUtil.isBlank(finish_time)){
//						schedule.remove("finish_time");
//						result.add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
//					}
			    	
			    	
			    	DBRow[] subSchedule=null;ArrayList<DBRow> list=null;HashMap<String,Object> map=new HashMap<String, Object>();
			    	
			    	//得到关联主任务
					subSchedule=floorPrintLabelMgrGql.getRelationTaskByScheduleId(String.valueOf(schedule_id),null);
					
					for (int j = 0; j < subSchedule.length; j++) {
						//处理时间格式
						String key="";DBRow temp=new DBRow();
						start_time=subSchedule[j].get("start_time", null);
						if(!StringUtil.isBlank(start_time)){
							start_time=DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8);
							key+=start_time;
						}
						  end_time=subSchedule[j].get("end_time", null);
						if(!StringUtil.isBlank(end_time)){
							end_time=DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8);
							key+=","+end_time;
						}
						  String id=subSchedule[j].get("schedule_id", null);
						  temp.add("schedule_id", id);
						  temp.add("task_status", subSchedule[j].get("task_status", 0l));
						  temp.add("description", subSchedule[j].get("schedule_detail", null));
						  
						 
//						  create_time=subSchedule[j].get("create_time", null);
//						if(!StringUtil.isBlank(create_time)){
//							subSchedule[j].remove("create_time");
//							subSchedule[j].add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
//						}
//						  finish_time=subSchedule[j].get("finish_time", null);
//						if(!StringUtil.isBlank(finish_time)){
//							subSchedule[j].remove("finish_time");
//							subSchedule[j].add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
//						}
						if(map.containsKey(key)){
							list=(ArrayList<DBRow>) map.get(key);
							list.add(temp);
						}else{
							list=new ArrayList<DBRow>();
							list.add(temp);
							map.put(key, list);
						}
					}
					//排序
					List<Map.Entry<String, ArrayList<DBRow>>> result_list=sortByStartTime(map);
					DBRow[] return_subSchedule=new DBRow[result_list.size()];
					if(!result_list.isEmpty()){
						for (int i = 0; i < result_list.size(); i++) {
							 schedule=new DBRow();
							 Map.Entry<String , ArrayList<DBRow>> temp=result_list.get(i);
							 String key =temp.getKey();
							 schedule.add("start_time", StringUtil.isBlank(key.split(",")[0])?"":key.split(",")[0]);
							 schedule.add("end_time",   key.split(",").length>1?key.split(",")[1]:"");
							 
							 //判断每个主任务的子任务的完成情况
//						     DBRow close_information= floorPrintLabelMgrGql.androidGetCloseInformationByMainId(String.valueOf(schedule_id));
//						     result.add("closedCount", close_information.get("closedcount", null));
//						     result.add("totalCount",  close_information.get("totalcount", null));
							 list=temp.getValue();
							 DBRow[] rows=list.toArray(new DBRow[list.size()]);String temp_id=null;
							 for (int k = 0; k < rows.length; k++) {
								temp_id=rows[k].get("schedule_id", null);
								if(!StringUtil.isBlank(temp_id)){
									DBRow close_information= floorPrintLabelMgrGql.androidGetCloseInformationByMainId(String.valueOf(temp_id));
									rows[k].add("closedCount", close_information.get("closedcount", 0l));
								    rows[k].add("totalCount",  close_information.get("totalcount", 0l));
								}
							}
							 schedule.add("child_tasks", rows);
							 return_subSchedule[i]=schedule;
						}
					}
					result.add("main_tasks", return_subSchedule);
   	    	   }
   	       }
   	       return result;
		} catch (Exception e) {
  		    throw new Exception((new StringBuilder()).append("androidGetMainTaskGroupByTime").append(e).toString());
		}
    }
	@SuppressWarnings("unchecked")
	private List<Map.Entry<String, ArrayList<DBRow>>> sortByStartTime(HashMap<String, Object> map) throws Exception{
		try{
		    //排序
			List<Map.Entry<String, ArrayList<DBRow>>> list =new ArrayList<Map.Entry<String,ArrayList<DBRow>>>();
			Iterator<Entry<String, Object>> iterator= map.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Object> entry = iterator.next();
				final ArrayList<DBRow> value=(ArrayList<DBRow>) entry.getValue();
				final String key=entry.getKey();
				Entry<String, ArrayList<DBRow>> temp=new Entry<String, ArrayList<DBRow>>() {
					
					@Override
					public String getKey() {
						// TODO Auto-generated method stub
						return key;
					}

					@Override
					public ArrayList<DBRow> getValue() {
						// TODO Auto-generated method stub
						return value;
					}

					@Override
					public ArrayList<DBRow> setValue(ArrayList<DBRow> value) {
						// TODO Auto-generated method stub
						return value;
					}
				};
				list.add(temp);
			}
			Collections.sort(list,new Comparator<Map.Entry<String, ArrayList<DBRow>>>() {   
				String start=null,end=null;
			    public int compare(Map.Entry<String, ArrayList<DBRow>> o1, Map.Entry<String, ArrayList<DBRow>> o2) { 
			    		start=o2.getKey();
			    		end=o1.getKey();
			    		start=DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat, DateUtil.StringToDate(start));
			            end=DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat, DateUtil.StringToDate(end));
					    try {
					    	int result=DateUtil.getSubDay(start, end);
					    	if(result>0){
					    		return 1;
					    	}
					    	if(result<0){
					    		return -1;
					    	}else{
					    		return 0;
					    	}
						} catch (Exception e) {
							
						}
					    return 0;
			    }});
		   return list;
		  }catch(Exception e){
			  throw new SystemException(e.getMessage());
		  }
		
	};
    /**
     * 根据mainTaskId查询子任务
     */
    public  DBRow androidGetSubTaskInformationByMainId(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try {
   		    DBRow  data=this.getParamFromRequest(request);DBRow[] schedule=null;
   		    this.setResponse(response); 
   		    
   		    DBRow[] result=new DBRow[]{};DBRow temp=null;DBRow bean=new DBRow();
			HttpSession session=request.getSession(true);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
			adminLoggerBean.getAdgid();
			Long adid_default=adminLoggerBean.getAdid();
			Long adid=data.get("adid",0l);
			if(adid==0l){
				adid=adid_default;
			}
		   	long ps_id=adminLoggerBean.getPs_id(); 
   	       if(!data.isEmpty()){
   	    	   Long schedule_id=data.get("schedule_id", 0l);
   	    	   
   	    	   if(schedule_id!=null&&schedule_id!=0l){
   	    		   //main
   	    		   temp=floorPrintLabelMgrGql.getScheduleByScheduleId(schedule_id);
   	    		   if(!temp.isEmpty()){
   	    			   bean.add("description", temp.get("schedule_detail", null));
   	    			   bean.add("start_time", DateUtil.showLocalparseDateTo24Hours(temp.get("start_time", null),ps_id).substring(0, 8));
   	    			   bean.add("end_time", DateUtil.showLocalparseDateTo24Hours(temp.get("end_time", null),ps_id).substring(0, 8));
   	    			   bean.add("task_no", temp.get("schedule_id", 0));
   	    		   }
   	    		   //sub
   	    		  DBRow role_bean=floorPrintLabelMgrGql.androidGetRoleByAdid(String.valueOf(adid));
   	    		  long role_num=role_bean.get("role", 0l);
   	    		  
   	    		  
   	    		  schedule=floorPrintLabelMgrGql.getRelationTaskByScheduleId(String.valueOf(schedule_id),null);
   	    		  result=new DBRow[schedule.length];
   	    		  for (int i = 0; i < schedule.length; i++) {
   	    			temp=new DBRow();
   	    			temp.add("schedule_id", schedule[i].get("schedule_id", null));
   	    			temp.add("schedule_execute_id",schedule[i].get("schedule_execute_id", null));
   	    			temp.add("exeemployname",schedule[i].get("exeemployname", null));
   	    			temp.add("task_status",schedule[i].get("task_status", 0));
   	    			temp.add("description",schedule[i].get("schedule_detail", null));
   	    			 //处理时间格式
  					String  start_time=schedule[i].get("start_time", null);
  					if(!StringUtil.isBlank(start_time)){
  						temp.add("start_time",DateUtil.showLocalparseDateTo24Hours(start_time,ps_id).substring(0, 8));
  					}
  					String  end_time=schedule[i].get("end_time", null);
  					if(!StringUtil.isBlank(end_time)){
  						temp.add("end_time",DateUtil.showLocalparseDateTo24Hours(end_time,ps_id).substring(0, 8));
  					}
//  					String  create_time=schedule[i].get("create_time", null);
//  					if(!StringUtil.isBlank(create_time)){
//  						schedule[i].remove("create_time");
//  						schedule[i].add("create_time",DateUtil.showLocalparseDateTo24Hours(create_time,ps_id).substring(0, 8));
//  					}
//  					String  finish_time=schedule[i].get("finish_time", null);
//  					if(!StringUtil.isBlank(finish_time)){
//  						schedule[i].remove("finish_time");
//  						schedule[i].add("finish_time",DateUtil.showLocalparseDateTo24Hours(finish_time,ps_id).substring(0, 8));
//  					}
  					result[i]=temp;
				  }
   	    		bean.add("subtasks", result);
   	    	   }
   	       }
   	       
   	      return bean;
		} catch (Exception e) {
			response.getWriter().print(new JSONObject().put("success", false).toString());
  		    throw new Exception((new StringBuilder()).append("androidGetSubTaskInformationByMainId").append(e).toString());
		}
    	
    };
    /**
     * assign subtask
     */
    public void androidAssignSubTask(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	try {
    		DBRow  data=this.getParamFromRequest(request);
   		    this.setResponse(response);
   		    //获得创建人
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long createManId = adminLoginBean.getAdid();
			long ps_id=adminLoginBean.getPs_id();
   	       if(!data.isEmpty()){
   	    	   //step 1 schedule_sub 执行人

   	    	   
   	    	   String  assignJson=data.getString("assignJson", null);
   	    	   
   	    	   Long main_task_status=data.get("main_task_status", -1l);
   	    	   
   	    	   String main_task_id=data.get("main_task_id", null);
   	    	   
		    	String start_time=data.get("start_time",null);
				start_time=DateUtil.showUTCTime(formatDate(start_time), ps_id);
				String end_time=data.get("end_time",null);
				end_time=DateUtil.showUTCTime(formatDate(end_time), ps_id);
   	    	   
   	    	   String  schedule_id=null;String exe_user_ids=null,def_user_ids=null;DBRow row=null;
   	    	   if(!StringUtil.isBlank(assignJson)){
   	    		 JSONArray array=new JSONArray(assignJson);
   	    		 for (int i = 0; i < array.length(); i++) {
					JSONObject json=array.getJSONObject(i);
					schedule_id=json.getString("schedule_id");
					
					if(json.has("def_user_ids")){
						def_user_ids=json.getString("def_user_ids");
						JSONArray defbean=new JSONArray(def_user_ids);
						for (int j = 0; j < defbean.length(); j++) {
							String def_id=defbean.getString(j);
							row=new DBRow();
							row.add("task_id", schedule_id);
					        row.add("task_execute_id", def_id);
					        floorPrintLabelMgrGql.androidDelScheduleSub(row);
						}
						
						//删除通知
						DBRow[] schedule_temp=scheduleMgrIfaceZR.getScheduleBy(Long.parseLong(schedule_id),ProcessKey.SPECIAL_TASK, ModuleKey.SPECIAL_TASK);
						if(schedule_temp!=null&&schedule_temp.length>0){
							for (int j = 0; j < schedule_temp.length; j++) {
								String temp_id=schedule_temp[j].get("schedule_id", null);
								if(!StringUtil.isBlank(temp_id)){
									scheduleMgrIfaceZR.deleteScheduleByScheduleId(Long.parseLong(temp_id), adminLoginBean);
								}
								
							}
							
						}
					}
					
					if(json.has("exe_user_id")){
						exe_user_ids=json.getString("exe_user_id");
						JSONArray exebean=new JSONArray(exe_user_ids);
						for (int j = 0; j < exebean.length(); j++) {
							String exe_id=exebean.getString(j);
							 //step 1 schedule_sub 执行人
							row=new DBRow();
					        row.add("task_execute_id", exe_id);
					        row.add("task_id", schedule_id);
					        floorPrintLabelMgrGql.androidAddScheduleSub(row);
					        
					        //step 2 notice assigner
					        scheduleMgrIfaceZR.addSchedule(ScheduleModel.getSpecialTaskScheduleModel(createManId, exe_id, Long.parseLong(schedule_id), "Special Tasks","You have a new special task. NO."+schedule_id, start_time, end_time,main_task_id) );
//					        scheduleMgrIfaceZR.addScheduleByExternalArrangeNow(start_time, end_time, createManId, exe_id, "", 0, ModuleKey.SPECIAL_TASK, "Special Task", "", true, false, false, adminLoginBean, true, ProcessKey.SPECIAL_TASK);
					        
						 }
					}
					//修改子任务状态为assign
   	    		   if(!StringUtil.isBlank(schedule_id)){
	    			   floorPrintLabelMgrGql.androidUpdateTaskStatus(schedule_id,TaskStatusKey.ASSIGNED);
	    		    }
				  }
   	    		 
   	    		  
   	    	    }
   	    	   
   	    	   //update main_task_status 
   	    	   if(main_task_status!=null&&main_task_status!=-1l&&main_task_status==TaskStatusKey.OPEN){
   	    		   if(!StringUtil.isBlank(main_task_id)){
   	    			   floorPrintLabelMgrGql.androidUpdateTaskStatus(main_task_id,TaskStatusKey.PROCESSING);
   	    		   }
   	    	   }
   	       }
		} catch (Exception e) {
  		    throw new Exception((new StringBuilder()).append("androidAssignSubTask").append(e).toString());
		}
    	
    };
    public DBRow[] androidGetNoticeSpecialTask(HttpServletRequest request, HttpServletResponse response) throws Exception{
     try {
    	        DBRow data=this.getParamFromRequest(request);
    	        AdminMgr adminMgr = new AdminMgr();
    			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
    			long ps_id=adminLoginBean.getPs_id();
    	        this.setResponse(response);
    	        DBRow[] temp=null;ArrayList<DBRow> result=null;
    	        if(!data.isEmpty()){
    	        	DBRow row=new DBRow();
    	        	row.add("executeUserId", data.get("adid", null));
    	        	row.add("associate_type", ModuleKey.SPECIAL_TASK);
    	        	row.add("associate_process", ProcessKey.SPECIAL_TASK);
    	        	temp=floorPrintLabelMgrGql.androidGetNoticeByExecuteUserIdAndAssociateTypeAndAssociateId(row);
    	        	if(temp!=null&&temp.length>0){
    	        		result=new ArrayList<DBRow>();DBRow bean=null;
    	        		for (int i = 0; i < temp.length; i++) {
    	        			   int status=temp[i].get("task_status", -1);
    	        			   Long task_id=temp[i].get("task_id", 0l);
    	        			   if(status!=-1&&status==TaskStatusKey.ASSIGNED){
    	        				  DBRow update=new DBRow();
    	        				  update.add("task_status", TaskStatusKey.ACCEPTED);
    	        				  floorPrintLabelMgrGql.updateSchedule(task_id,update);
    	        				  status=TaskStatusKey.ACCEPTED;
    	        			   }
    	        				bean=new DBRow();
        	        			bean.add("task_id", task_id);
        	        			bean.add("task_description", temp[i].get("task_detail", null));
        	        			bean.add("notice_time", temp[i].get("notice_time", null));
        	        			bean.add("task_status", status);
        	        			Long main_id=temp[i].get("associate_main_id", 0l);
        	        			if(main_id!=0l){
        	        				DBRow main_temp=floorPrintLabelMgrGql.getSingleTaskInfomationByTaskId(main_id);
        	        				bean.add("start_time", DateUtil.showUTCTime(main_temp.get("start_time", null), ps_id));
        	        				bean.add("end_time", DateUtil.showUTCTime(main_temp.get("end_time", null), ps_id));
        	        			}
        	        			result.add(bean);
    	        			
    	        			
						}
    	        	}
    	        }
    	        return result.toArray(new DBRow[result.size()]);
		} catch (Exception e) {
  		    throw new Exception((new StringBuilder()).append("androidGetNoticeSpecialTask").append(e).toString());
		}
    };
    /**
     * close sub task
     */
    public DBRow androidCloseSubTask(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	DBRow data=this.getParamFromRequest(request);DBRow result=new DBRow();int main_status=TaskStatusKey.OPEN;
    	if(!data.isEmpty()){
    		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
  		    AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
            long createManId=adminLoginBean.getAdid();
            main_status=data.get("mainTaskStatus",TaskStatusKey.OPEN);
            
           //创建时间
		    Date date=(Date)Calendar.getInstance().getTime();
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createTime = dateformat.format(date);
			
			String emailTitle="";
			
			String context="";
			
			if(data.containsKey("completeTaskIds")){
				String completeTaskIds=data.get("completeTaskIds", null);
				int num=-1;DBRow closeInfo=null;
				if(!StringUtil.isBlank(completeTaskIds)){
					JSONArray array=new JSONArray(completeTaskIds);
					for (int i = 0; i < array.length(); i++) {
						Long schedule_id=Long.parseLong(array.getString(i));
						//sub task 其实应该迭代
						floorPrintLabelMgrGql.closeTask(schedule_id);
						closeInfo=floorPrintLabelMgrGql.isChildTaskAllClosed(schedule_id,false);
						
						
						num=closeInfo.get("num", -1);
						if(num==0){
							//关闭 main task 
							schedule_id=closeInfo.get("task_id", 0l);
							try {
								floorPrintLabelMgrGql.closeTask(schedule_id);
								main_status=TaskStatusKey.CLOSE;
							} catch (Exception e1) {
								// TODO Auto-generated catch block
							}
							//  false 是否open invoice
							closeInfo=floorPrintLabelMgrGql.isChildTaskAllClosed(schedule_id,true);
							num=closeInfo.get("num", -1);
							if(num==0){
								schedule_id=closeInfo.get("task_id", 0l);
								
								//open invoice 
								try {
									Long invoice_id=closeInfo.get("invoice_id", 0l);
									DBRow row=new DBRow();
									row.add("bill_id", invoice_id);
									row.add("invoice_status", InvoiceStatusKey.OPEN);
									floorPrintLabelMgrGql.updateBill(invoice_id, row);
									
									row=floorPrintLabelMgrGql.getSingleTaskInfomationByTaskId(schedule_id);
									String exe_user_ids=row.get("assign_user_id", null);
									
									//组织邮件标题
									emailTitle="Invoice "+invoice_id+" is ready for your review";
									
									//组织邮件内容
									context="Invoice#"+invoice_id+" is ready for your review. \n";
									context+="\n";
									context+="Associated tasks (Project#"+schedule_id+"):\n";
									DBRow[] main_task=floorPrintLabelMgrGql.getRelationScheduleByBillId(String.valueOf(invoice_id), false);
									int sort=1;
									for (int j = 0; j < main_task.length; j++) {
										context+=sort+"."+main_task[j].get("schedule_detail", "")+"\n";
										sort++;
									}
									scheduleMgrZr.addScheduleByExternalArrangeNow(createTime,"",createManId,exe_user_ids,"",invoice_id, ModuleKey.SPECIAL_TASK, emailTitle, context, false, true, false,request,true,ProcessKey.INVOICE);
									
								} catch (Exception e) {
									// TODO Auto-generated catch block
									 throw new Exception((new StringBuilder()).append("androidCloseSubTask").append(e).toString());
								}
							}
						}
						
					}
				}
			}
    	}
    	result.add("main_status", main_status);
		return result;
    };
    /**
     * 批量删除
     */
    public void androidBatchDelTask(HttpServletRequest request, HttpServletResponse response) throws Exception{
    		DBRow data=this.getParamFromRequest(request);
        	AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
    		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
            Long adid=adminLoginBean.getAdid();
            String name=adminLoginBean.getEmploye_name();
            DBRow[] department=adminLoginBean.getDepartment();
            
        	if(!data.isEmpty()){
        		String del=data.get("del", null);
        		if(!StringUtil.isBlank(del)){
        			JSONArray del_array=new JSONArray(del);Long temp_id=null;JSONObject delJsonObject=null;String task_detail=null;
        		 	for (int i = 0; i < del_array.length(); i++) {
        				delJsonObject=del_array.getJSONObject(i);
        				temp_id=delJsonObject.getLong("task_id");
        				//验证
        				DBRow temp_bean=floorPrintLabelMgrGql.getSingleTaskInfomationByTaskId(temp_id);
        				if(temp_bean!=null&&!temp_bean.isEmpty()){
        					  int status=temp_bean.get("task_status", 0);
        	        		   if(!(status==TaskStatusKey.OPEN||status==TaskStatusKey.CLOSE)){
        	   	            	 throw new SpecialDelException(status+"");
        	   	               }
        				}
        				
        				task_detail=delJsonObject.getString("task_detail");
                         if(temp_id!=0l){
                        	 floorPrintLabelMgrGql.deleteMainSchedule(temp_id);
                        	 //增加日志
     						try {
     							Long project_id=this.getProjectIdByAnyId(temp_id);
     							
     							SpecialTaskLogBean logBean=new SpecialTaskLogBean();
     							logBean.setModule(SpecialKey.TASK);
     							
     							logBean.setAdid(adid);
     							logBean.setAccount(name);
     							logBean.setOperation_content(SpecialKey.OPERATION_TASK);
     							
    					    	String operation_note="Android Delete Sub Task "+task_detail;
    					    	logBean.setType(SpecialKey.SUBTASK);
     						   
     							logBean.setRelation_id(project_id);
     							logBean.setOperation_note(operation_note);
     							logBean.setDepartment(new JSONObject(department));
     							
     							floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
     						} catch (Exception e) {
     							//出现异常 不做任何处理
     						}
                         }
    				}
        		}
        	}
    };
    /**
     * 批量增加
     */
    public DBRow[] androidBatchAddTask(HttpServletRequest request, HttpServletResponse response) throws Exception{
    		DBRow data=this.getParamFromRequest(request);
        	AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
    		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
            Long ps_id =  adminLoginBean.getPs_id();
            Long adid=adminLoginBean.getAdid();
            String name=adminLoginBean.getEmploye_name();
            DBRow[] department=adminLoginBean.getDepartment();
            
            DBRow[]  result_array=null;
            DBRow result_row=null;
        	if(!data.isEmpty()){
        	   String add_json=data.get("add", null);
        	   String main_task_id=data.get("main_task_id", null);
        	   DBRow temp_bean=floorPrintLabelMgrGql.getSingleTaskInfomationByTaskId(Long.valueOf(main_task_id));
        	   if(temp_bean!=null&&!temp_bean.isEmpty()){
        		   int status=temp_bean.get("task_status", 0);
        		   if(status==TaskStatusKey.CLOSE){
   	            	 throw new SpecialAddException();
   	               }
        	   }
        	   
        	   if(!StringUtil.isBlank(add_json)&&main_task_id!=null){
        		   JSONArray add_array=new JSONArray(add_json);JSONObject temp=null;DBRow add=null,bean=new DBRow();Long task_id=0l;
        		   result_array=new DBRow[add_array.length()];
        		   for (int i = 0; i < add_array.length(); i++) {
        			 add=new DBRow();result_row=new DBRow();
    				 temp=add_array.getJSONObject(i);
    				 if(temp!=null){
    					 if(temp.has("task_detail")){
    						 add.add("schedule_detail",temp.getString("task_detail")); 
    						 result_row.add("description", temp.getString("task_detail"));
    					 }
    					 if(temp.has("task_order_no")){
    						 add.add("task_number",temp.getString("task_order_no")); 
    					 }
    					 add.add("assign_user_id", data.get("adid", adid));
    					 task_id = addSchedule(add, bean,false,2,ps_id);
    					 
    					 result_row.add("schedule_id",task_id);
    					 result_row.add("task_status", TaskStatusKey.OPEN);
    					 
    					 Long project_id=this.getProjectIdByAnyId(Long.valueOf(main_task_id));
    					//增加日志
    					try {
    						SpecialTaskLogBean logBean=new SpecialTaskLogBean();
    						logBean.setModule(SpecialKey.TASK);
    						logBean.setType(SpecialKey.SUBTASK);
    						logBean.setRelation_id(Long.valueOf(project_id));
    						logBean.setAdid(adid);
    						logBean.setAccount(name);
    						logBean.setOperation_content(SpecialKey.OPERATION_TASK);
    						logBean.setOperation_note("Android Add Subtask ");
    						logBean.setDepartment(new JSONObject(department));
    						
    						floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
    					} catch (Exception e) {
    						//出现异常 不做任何处理
    					}
    					 
    					 if(task_id!=0l){
    						    bean=new DBRow();
    							bean.add("parent_task_id", Long.parseLong(main_task_id));
    							bean.add("task_id", task_id);
    							floorPrintLabelMgrGql.addScheduleRelationship(bean);
    					 }
    				 }
    				 result_array[i]=result_row;
    			  }
        	   }
        	}
        	return result_array;
    };
    /**
     * 修改
     */
    public void androidUpdateTask(HttpServletRequest request, HttpServletResponse response) throws Exception{
			DBRow data=this.getParamFromRequest(request);
			
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
  		    AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
            Long adid=adminLoginBean.getAdid();
            String name=adminLoginBean.getEmploye_name();
            DBRow[] department=adminLoginBean.getDepartment();
            
			if(!data.isEmpty()){
				
				DBRow bean=new DBRow();
				String task_detail=data.get("task_detail", null);
				bean.add("task_detail", task_detail);
				Long task_id=data.get("task_id", 0l);
				 //校验 Open/Accepted/Assigned
				DBRow row=floorPrintLabelMgrGql.getSingleTaskInfomationByTaskId(task_id);
				int status=row.get("task_status", 0);
	            if(!(status==TaskStatusKey.OPEN||status==TaskStatusKey.ASSIGNED)){
	            	throw new SpecialUpdateException();
	            }else{
					floorPrintLabelMgrGql.updateSchedule(task_id, bean);
					//查询一下 父节点Id
					Long project_id=this.getProjectIdByAnyId(task_id);
					//增加日志
					try {
						SpecialTaskLogBean logBean=new SpecialTaskLogBean();
						logBean.setModule(SpecialKey.TASK);
						logBean.setType(SpecialKey.SUBTASK);
						logBean.setRelation_id(project_id);
						logBean.setAdid(adid);
						logBean.setAccount(name);
						logBean.setOperation_content(SpecialKey.OPERATION_TASK);
						logBean.setOperation_note("Android Update Task ");
						logBean.setDepartment(new JSONObject(department));
						
						floorLogMgrIFace.addLog(LogServModelKey.SPECIAL_TASK_LOG, new JSONObject(logBean), false);
					} catch (Exception e) {
						//出现异常 不做任何处理
					}
	            }
			}
    };
    /*android 结束*/
    public void setBillMgrZr(BillMgrZr billMgrZr) {
		this.billMgrZr = billMgrZr;
	}
    

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}


	public void setFloorPrintLabelMgrGql(FloorPrintLabelMgrGql floorPrintLabelMgrGql) {
		this.floorPrintLabelMgrGql = floorPrintLabelMgrGql;
	}

	public void setScheduleMgrIfaceZR(ScheduleMgrIfaceZR scheduleMgrIfaceZR) {
		this.scheduleMgrIfaceZR = scheduleMgrIfaceZR;
	}

	public void setFloorLogMgrIFace(FloorLogMgrIFace floorLogMgrIFace) {
		this.floorLogMgrIFace = floorLogMgrIFace;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
	public void setCreatePdfFileByServerIfaceGql(
			CreatePdfFileByServerIfaceGql createPdfFileByServerIfaceGql) {
		this.createPdfFileByServerIfaceGql = createPdfFileByServerIfaceGql;
	}
	public void setFloorCreatePdfMgr(FloorCreatePdfMgr floorCreatePdfMgr) {
		this.floorCreatePdfMgr = floorCreatePdfMgr;
	}
	public void setSpecialProjectLogUtil(SpecialProjectLogUtil specialProjectLogUtil) {
		this.specialProjectLogUtil = specialProjectLogUtil;
	}
	
	
	
	
	
}

class SpecialKey{
	public static int INVOICE = 1;//module
	public static int TASK = 2;//module
	public static int PROJECT = 3;//type
	public static int MAINTASK=4;//type
	public static int SUBTASK=5;//type
	public static String OPERATION_TASK="operation_task";//operation
	public static String OPERATION_INVOICE="operation_invoice";//operation
}


