package com.cwc.app.api.gql;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.directwebremoting.Browser;
import org.directwebremoting.ScriptSessions;

import sun.misc.BASE64Decoder;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.cwc.app.api.zwb.UploadAndDownloadPDF;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.gql.FloorCreatePdfMgr;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.gql.CreatePdfFileByServerIfaceGql;
import com.cwc.app.iface.zr.AndroidPrintMgrIfaceZr;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.PdfTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.service.file.action.zr.ImgToPdfMgr;
import com.cwc.util.StringUtil;

public class CreatePdfFileByServerMgrGql implements CreatePdfFileByServerIfaceGql{
	static Logger log = Logger.getLogger("ACTION");
	private ImgToPdfMgr imgToPdfMgr;
	private UploadAndDownloadPDF uploadAndDownloadPDF;
	private FloorCreatePdfMgr floorCreatePdfMgr;
	private BASE64Decoder decoder; 
	private AndroidPrintMgrIfaceZr androidPrintMgrIfaceZr;
	private AdminMgrIFace adminMgr;
	// 保存的路径
  	private static String PDFPath = Environment.getHome().replace("\\", "/")
  			+ "upl_pdf_tmp/";
  	protected String downLoadFileUrl = "";
	protected String uploadFileUrl = "";
  	
	public void sendHtml(DBRow data) throws Exception{
		try {
			androidPrintMgrIfaceZr.sendDataToPDFServerHtml(data);
		} catch (Exception e) {
			 throw new SystemException(e,"handleHtmlToPdf",log);
		}
	}
	/**
	 * 处理htmltopdf
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void handleHtmlToPdf(HttpServletRequest request,HttpServletResponse response) throws Exception{
		try {
			DBRow data=this.getParamFromRequest(request);
			if(!data.isEmpty()){
				String base64StringOfImage=data.get("imageBuffer", null);
				Long associate_id=data.get("associate_id", 0l);
				String sessionId=request.getRequestedSessionId();
				
				 HttpSession session=request.getSession(true);
				 AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session); 
				  	
				 long adid_ = adminLoggerBean.getAdid(); 
				//转换图片流
				BufferedImage bi1 =null;
				if(base64StringOfImage!=null){
					byte[] bt = decoder.decodeBuffer(base64StringOfImage);
					ByteArrayInputStream bais = new ByteArrayInputStream(bt);
					bi1 = ImageIO.read(bais);
				}
				
				File pdfFilePath = new File(PDFPath);
				if (!pdfFilePath.exists()) {
					pdfFilePath.mkdirs();
				}
				//判断是否已经存在 如果存在 就删除记录 及文件
				int module_key=data.get("module", 0);
				int pdf_type_key=data.get("pdftype", 0);
				long adid=data.get("adid", 0l);
				if(module_key!=0&&pdf_type_key!=0l&&associate_id!=0){
					DBRow[] temp=floorCreatePdfMgr.Search(String.valueOf(adid), module_key, pdf_type_key,associate_id);
					if(temp!=null&&temp.length>0){
						String del_url=null;
						for (DBRow dbRow : temp) {
							del_url=downLoadFileUrl;
							//step1 删除文件服务器文件
							long file_id=dbRow.get("file_id", 0l);
							if(file_id!=0l){
								del_url+=file_id;
								uploadAndDownloadPDF.deleteFile(del_url, sessionId);							
								}
							//step2 删除记录
							long id=dbRow.get("id", 0l);
							if(id!=0l){
								floorCreatePdfMgr.Delete(id);
							}
						}
					}
				}
				//step 1 生成
				String subpdfpath=PDFPath+associate_id+ ".pdf";
				imgToPdfMgr.handleImageOfPdfServer(bi1,null,
						subpdfpath);
				//step 2 上传
				String file_id=uploadAndDownloadPDF.upLoadPDF(uploadFileUrl, subpdfpath, sessionId);
				
				//step 3 写入数据
				PdfTypeKey pdfTypeKey=new PdfTypeKey();
				DBRow row=new DBRow();
				row.add("module_type", ModuleKey.SPECIAL_TASK);
				row.add("associate_type",pdfTypeKey.Invoice);
				row.add("associate_id", associate_id);
				row.add("create_time", DateUtil.NowStr());
				row.add("file_id", file_id);
				row.add("adid", adid_);
				floorCreatePdfMgr.Add(row);
				this.setResponse(response);
				response.getWriter().print(new JSONObject().put("file_id", file_id));
				
			}
		} catch (Exception e) {
			 throw new SystemException(e,"handleHtmlToPdf",log);
		}
		
	};
	/**
	 * response 设置
	 * @response
	 */
	private void setResponse(HttpServletResponse response){
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}
	/**
	 * 从request中取出参数值
	 * @param request
	 * @throws IOException 
	 * @throws JSONException 
	 */
	private DBRow getParamFromRequest(HttpServletRequest request) throws Exception{
		DBRow dbRow=new DBRow();
		//Step 1
		 String result=IOUtils.toString(request.getReader());
		 if(!StringUtil.isBlank(result)){
			 JSONObject jsonData = new JSONObject(result);
			 dbRow=DBRowUtils.convertToDBRow(jsonData);
		 }
		//Step 2
		Enumeration<String> paramEnum=request.getParameterNames();
		while (paramEnum != null &&paramEnum.hasMoreElements()) {
			String key = (String) paramEnum.nextElement();
			dbRow.add(key, StringUtil.getString(request, key));
		}
		return dbRow;
	}
	
	public void setImgToPdfMgr(ImgToPdfMgr imgToPdfMgr) {
		this.imgToPdfMgr = imgToPdfMgr;
	}
	
	public void setUploadAndDownloadPDF(UploadAndDownloadPDF uploadAndDownloadPDF) {
		this.uploadAndDownloadPDF = uploadAndDownloadPDF;
	}
	
	public void setFloorCreatePdfMgr(FloorCreatePdfMgr floorCreatePdfMgr) {
		this.floorCreatePdfMgr = floorCreatePdfMgr;
	}
    
	public void setDecoder(BASE64Decoder decoder) {
		this.decoder = decoder;
	}
	
	public void setDownLoadFileUrl(String downLoadFileUrl) {
		this.downLoadFileUrl = downLoadFileUrl;
	}
	
	public void setUploadFileUrl(String uploadFileUrl) {
		this.uploadFileUrl = uploadFileUrl;
	}
	
	public void setAndroidPrintMgrIfaceZr(
			AndroidPrintMgrIfaceZr androidPrintMgrIfaceZr) {
		this.androidPrintMgrIfaceZr = androidPrintMgrIfaceZr;
	}
	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	
	
}



