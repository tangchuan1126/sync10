package com.cwc.app.api.gql;

import static us.monoid.web.Resty.content;
import static us.monoid.web.Resty.put;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;
import us.monoid.web.Resty.Option;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

/***
 * 日志操作类
 * @author Administrator
 *
 */
public class SpecialProjectLogUtil {
	
	private String restBaseUrl;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	static Logger log = Logger.getLogger("ACTION");
	/**
	 * 取日志
	 * @param restBaseUrl
	 */
	public DBRow[]  queryLogs(String modelName, JSONObject queryJson,PageCtrl pageCtrl,long ps_id){
		String start_date="2010-01-01T00:00:00";
		String end_date= sdf.format(new Date());
		DBRow[] result=null;
		try {
			result=invokeRest(modelName, queryJson,pageCtrl,start_date, end_date);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());
			result=new DBRow[]{};
		}
		return result;
	}
	
	private DBRow[] invokeRest(String modelName, JSONObject queryJson,PageCtrl pageCtrl, 
			String start_date, String end_date) throws Exception {
		JSONObject pcJSON = new JSONObject();
		pcJSON.put("pageNo", pageCtrl.getPageNo());
		pcJSON.put("pageSize", pageCtrl.getPageSize());
		JSONObject reqJSON = new JSONObject();
		reqJSON.put("fields", queryJson);
		reqJSON.put("page_ctrl", pcJSON);
		
		reqJSON.put("start_date", start_date);
		reqJSON.put("end_date", end_date);

		String restUrl = restBaseUrl + "/log_filter/" + modelName;
		JSONResource result = new Resty(Option.timeout(1000000)).json(restUrl,
				put(content(reqJSON)));

		pcJSON = (JSONObject) result.get("page_ctrl");
		pageCtrl.setAllCount(pcJSON.getInt("allCount"));
		pageCtrl.setRowCount(pcJSON.getInt("rowCount"));
		pageCtrl.setPageCount(pcJSON.getInt("pageCount"));

		JSONArray rows = (JSONArray) result.get("rows");

		return convertRows(rows);
		
		
	}
	private DBRow[] convertRows(JSONArray rows) throws Exception {
		DBRow[] dbRows = new DBRow[rows.length()];
		for (int i = 0; i < rows.length(); i++) {
			JSONObject row = rows.getJSONObject(i);
			dbRows[i] = convertRow(row);
		}
		return dbRows;
	}
	
	private DBRow convertRow(JSONObject row) throws Exception {
		DBRow dbRow = new DBRow();
		for (Iterator<String> itr = row.keys(); itr.hasNext();) {
			String k = itr.next();

			if (k.equals("post_date")) {
				// 时间格式转换
				String v = row.getString(k);
				v = v.replace('T', ' ').substring(0, 19);
				dbRow.add(k, v);
			} else if (row.get(k) instanceof JSONArray) {
				JSONArray a = (JSONArray) row.get(k);
				Object[] oa = new Object[a.length()];
				for (int j = 0; j < a.length(); j++) {
					oa[j] = a.get(j);
				}
				dbRow.addObject(k, oa);
			} else if(k.equals("_id")) {
				dbRow.add("id", row.getString(k));
			} else if (!k.startsWith("_")) {
				dbRow.addObject(k, row.get(k));
			}
		}// for
		return dbRow;
	}
	
	public void setRestBaseUrl(String restBaseUrl) {
		this.restBaseUrl = restBaseUrl;
	}
	
}
