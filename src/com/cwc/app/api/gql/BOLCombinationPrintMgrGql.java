package com.cwc.app.api.gql;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.cwc.app.iface.gql.BOLCombinationPrintIfaceGql;
import com.cwc.app.iface.hlb.CheckInMgrIfaceHlb;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.iface.zr.RelativeFileMgrIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.RelativeFileKey;
import com.cwc.app.key.WmsItemUnitKey;
import com.cwc.app.key.WmsItemUnitTicketKey;
import com.cwc.app.key.WmsSearchItemStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONException;
import com.fr.base.core.json.JSONObject;

/**
 * @author 	Zhengziqi
 * 2014年9月16日
 *
 */
public class BOLCombinationPrintMgrGql implements BOLCombinationPrintIfaceGql {
	 static Logger log = Logger.getLogger("ACTION");
	 private CheckInMgrIfaceZwb checkInMgrZwb; 
	 private SQLServerMgrIFaceZJ sqlServerMgrZJ;
	 private RelativeFileMgrIfaceZr relativeFileMgrZr;
	 private CheckInMgrIfaceHlb checkInMgrHlb;
	 
	 
	 ModuleKey moduleKey = new ModuleKey();
	
	
	@Override
	public JSONArray bolCombinationPrintMessage(JSONArray jsons, String out_seal, long entry_id, long adid, String printType, String pageName)throws Exception{
		try{
			//target json array
			JSONArray bolCombinationPrintJsonArray = new JSONArray();
			
	    	DBRow[] mainRows = checkInMgrZwb.selectAllMain(entry_id, null);
	    	DBRow mainRow=mainRows.length > 0?mainRows[0]:null;
	    	
			for(int m=0;m<jsons.length();m++){
				//biuld single json object
				JSONObject result_map = new JSONObject();
				String date = new TDate().getFormateTime(DateUtil.NowStr(), "MM/dd/yyyy");
				result_map.put("nowDate", date);
				
				//parse json parama
	    		JSONObject json = jsons.getJSONObject(m);
	    		String number = (json.has("number"))?json.getString("number"):"";
	    		int number_type = (json.has("number_type"))?json.getInt("number_type"):0;
	    		String companyId = (json.has("companyId"))?json.getString("companyId"):"";
	    		String customerId = (json.has("customerId"))?json.getString("customerId"):"";
	        	String loadNo=number;
	        	result_map.put("loadNo", loadNo);
	        	String master_bol_nos = (json.has("master_bol_nos"))?json.getString("master_bol_nos"):"";
	        	result_map.put("master_bol_nos", master_bol_nos);
	        	String relativeValue = 	entry_id+"_"+loadNo+"_"+master_bol_nos;	//签字文件 关联的value
	        	if(StringUtils.equals("a4print_vics2", printType)){
	        		relativeValue = entry_id+"_"+number+"_";	//签字文件 关联的value
	        	}
	        	long orderNo = 0;
	        	if(pageName != null && pageName != ""){
	        		if(number_type==ModuleKey.CHECK_IN_ORDER){
		        		orderNo=Long.parseLong(number);
		        	}else{
		        		orderNo=json.getLong("order_no");
		        	}
	        	}
	        	
	        	//handle mainRow data
				JSONObject mainRowJSON = new JSONObject();
				mainRowJSON.put("gate_container_no", mainRow.get("gate_container_no"));
				mainRowJSON.put("gate_liscense_plate", mainRow.get("gate_liscense_plate"));
				mainRowJSON.put("gate_driver_liscense", mainRow.get("gate_driver_liscense"));
				mainRowJSON.put("seal", mainRow.get("seal"));
				mainRowJSON.put("imei", mainRow.get("imei"));
				result_map.put("mainModel", mainRowJSON);
	        	
				DBRow[] targetRows = aquireTargetRowsByPrintType(pageName, printType, loadNo, master_bol_nos, companyId, customerId, adid);
	        	if(targetRows.length > 0){
	    		    buildResultMap(result_map, targetRows, mainRow, out_seal, master_bol_nos, companyId, customerId, adid, loadNo, printType, relativeValue, orderNo);
	        	}
	        	bolCombinationPrintJsonArray.put(result_map);
	    	}
			return bolCombinationPrintJsonArray;
		}catch(Exception e){
			throw new SystemException(e,"addProductDetailLable",log);
		}
	}
	
	@Override
	public JSONObject loadCombinationPrintMessage(String loadNo, String orderNo, String companyId, String customerId, long adid, String window_check_in_time, String printType)throws Exception{
		try{
			Boolean is_print_order_master_wms = StringUtils.equals("print_order_master_wms", printType);
			JSONObject result = new JSONObject();
			//target json array
			JSONArray loadCombinationPrintJsonArray = new JSONArray();
			
			DBRow[] targetRows = null;
			if(is_print_order_master_wms){
				targetRows = sqlServerMgrZJ.findMasterBolsByLoadNo(loadNo, companyId, customerId, adid, null);
			}else if(StringUtils.equals("print_order_no_master_wms_order", printType)){
				targetRows = sqlServerMgrZJ.findOrdersSomeInfoWmsByOrderNo(Long.valueOf(orderNo), companyId, customerId, adid, null);
			}else {
				targetRows = sqlServerMgrZJ.findOrdersSomeInfoWmsByLoadNo(loadNo, companyId, customerId, adid, null);
			}
			result.put(is_print_order_master_wms?"loadInfoRowsLength":"orderInfoRowsLength", targetRows.length);
			int palletTotal = 0;
	  		int itemTotal = 0;
	  		int caseTotal = 0;
	  		HashMap<String, Integer> map = new HashMap<String, Integer>();
	  		map.put("palletTotal", palletTotal);
	  		map.put("itemTotal", itemTotal);
	  		map.put("caseTotal", caseTotal);
	  		double loadPalletNum	= 0d;
	  		int count = 0;
			for(int m = 0; m < targetRows.length; m ++){
				//biuld single json object
				JSONObject result_map = new JSONObject();
				result_map.put(is_print_order_master_wms?"loadInfoRow":"orderInfoRow", (JSONObject)StringUtil.getJSON(targetRows[m]));
				
				result_map.put("masterBOLNo", MoneyUtil.fillZeroByRequire(targetRows[m].get("MasterBOLNo", 0L), 7));
				result_map.put("window_check_in_time", DateUtil.parseDateFormat(window_check_in_time, "yyyy-MM-dd"));
				result_map.put("orderNo", MoneyUtil.fillZeroByRequire(targetRows[m].get("orderNo", 0L), 7));
				//print_order_master_wms
				if(is_print_order_master_wms){
					handleLoadOrderRows(result_map, targetRows, m, adid, is_print_order_master_wms);	
				//print_order_no_master_wms
				}else {
					count = handleOrderItemRows(result_map, targetRows, m, adid, is_print_order_master_wms, map, loadPalletNum, count);
				}
				loadCombinationPrintJsonArray.put(result_map);
			}
			String infoRowName = is_print_order_master_wms?"loadInfoRows":"orderInfoRows";
			result.put(infoRowName, loadCombinationPrintJsonArray);
			String date = new TDate().getFormateTime(DateUtil.NowStr(), "yyyy-MM-dd HH:mm:ss");
			result.put("nowDate", date);
			result.put("palletTotal", map.get("palletTotal"));
			result.put("itemTotal", map.get("itemTotal"));
			result.put("caseTotal", map.get("caseTotal"));
			return result;
		}catch(Exception e){
			throw new SystemException(e,"addProductDetailLable",log);
		}
	}
	
	public void handleLoadOrderRows(JSONObject result_map, DBRow[] loadInfoRow, int m, long adid, Boolean is_print_order_master_wms) throws Exception{
		JSONArray arrays = new JSONArray();
		if(loadInfoRow[m].get("error_order_status", 0) >1){
			WmsSearchItemStatusKey wmsSearchItemStatusKey = new WmsSearchItemStatusKey();
			result_map.put("error_order_status_key", wmsSearchItemStatusKey.getStatusById(loadInfoRow[m].get("error_order_status", 0)));
		}else{
			DBRow loadInfoOne = loadInfoRow[m];
			String companyId = loadInfoOne.getString("CompanyID");
			DBRow[] loadOrderRows = sqlServerMgrZJ.findB2BOrderItemPlatesWmsByMasterBolNo(loadInfoOne.get("MasterBOLNo", 0L), companyId, adid, null);
			result_map.put("loadOrderRowsLength", loadOrderRows.length);
			long currentOrderNo = 0;
	  		boolean isSameOrder = true;
	  		int palletTotal = 0;
	  		int itemTotal = 0;
	  		boolean palletNumIsAdd = false;
	  		int caseTotal = 0;
	  		double loadPalletNum	= 0d;
	  		for(int i = 0; i < loadOrderRows.length; i ++){
	  			JSONObject loadOrderRowJSONObject = new JSONObject();
	  			DBRow loadOrderRow = loadOrderRows[i];
	  			loadOrderRowJSONObject = (JSONObject)StringUtil.getJSON(loadOrderRow);
	  			loadOrderRowJSONObject.put("OrderNo", MoneyUtil.fillZeroByRequire(loadOrderRow.get("OrderNo", 0L), 7));
	  			if(currentOrderNo != loadOrderRow.get("OrderNo", 0L)){
	  				currentOrderNo = loadOrderRow.get("OrderNo", 0L);
	  				isSameOrder    = false;
	  				palletNumIsAdd = false;
	  				DBRow loadPalletNumRow = sqlServerMgrZJ.findPalletsByOrderNoCompanyId(loadOrderRow.getString("CompanyID"), loadOrderRow.get("OrderNo", 0L));
		  			loadPalletNum = loadPalletNumRow.get("palletNum", 0d);
		  			loadOrderRowJSONObject.put("loadPalletNum", loadPalletNum);
	  			}else{
	  				isSameOrder = true;
	  				palletNumIsAdd = true;
	  			}
	  			if(!palletNumIsAdd){
	  				palletTotal += loadPalletNum;
	  			}
	  			itemTotal += loadOrderRow.get("ShippedQty", 0d);
	  			loadOrderRowJSONObject.put("isSameOrder", isSameOrder);
	  			caseTotal = handleQtyInfoRow(loadOrderRowJSONObject, loadOrderRows, i, caseTotal, currentOrderNo, is_print_order_master_wms);
				arrays.put(loadOrderRowJSONObject);
	  		}
	  		result_map.put("caseTotal", caseTotal);
	  		result_map.put("itemTotal", itemTotal);
	  		result_map.put("palletTotal", palletTotal);
	  		result_map.put("loadOrderRows", arrays);
		}
	}
	
	public int handleOrderItemRows(JSONObject result_map, DBRow[] loadInfoRows, int m, long adid, Boolean is_print_order_master_wms, HashMap<String, Integer> map, double loadPalletNum, int count) throws Exception{
		int palletTotal = map.get("palletTotal");
		int itemTotal = map.get("itemTotal");
		int caseTotal = map.get("caseTotal");
		JSONArray arrays = new JSONArray();
		DBRow orderInfoOneRow = loadInfoRows[m];
  		//pallet总数
  		DBRow loadPalletNumRow  = sqlServerMgrZJ.findPalletsByOrderNoCompanyId(orderInfoOneRow.getString("CompanyID"), orderInfoOneRow.get("OrderNo", 0L));
		loadPalletNum = loadPalletNumRow.get("palletNum", 0d);	
		result_map.put("loadPalletNum", loadPalletNum);
		palletTotal += loadPalletNum;
		DBRow[] orderItemRows = sqlServerMgrZJ.findOrdersItemsWmsByOrderNo(orderInfoOneRow.get("orderNo", 0L), orderInfoOneRow.getString("CompanyID"));
  		for(int i = 0; i < orderItemRows.length; i ++){
  			JSONObject loadOrderRowJSONObject = new JSONObject();
  			DBRow orderItemRow = orderItemRows[i];
  			loadOrderRowJSONObject = (JSONObject)StringUtil.getJSON(orderItemRow);
  			count ++;
  			loadOrderRowJSONObject.put("count", StringUtil.parseIndexTo4Str(count));
  			itemTotal += orderItemRow.get("ShippedQty", 0d);
  			caseTotal = handleQtyInfoRow(loadOrderRowJSONObject, orderItemRows, i, caseTotal, 0, is_print_order_master_wms);
  			
  			arrays.put(loadOrderRowJSONObject);
  		}
  		map.put("caseTotal", caseTotal);
  		map.put("itemTotal", itemTotal);
  		map.put("palletTotal", palletTotal);
  		result_map.put("orderItemRows", arrays);
  		return count;
	}
	
	public int handleQtyInfoRow(JSONObject loadOrderRowJSONObject, DBRow[] targetRows, int i, int caseTotal, long currentOrderNo, Boolean is_print_order_master_wms) throws Exception{
		DBRow qtyInfoRow 	= sqlServerMgrZJ.findOrderCaseQtyByItemIdQty(targetRows[i].getString("ItemID"),targetRows[i].getString("CompanyID"), targetRows[i].get("ShippedQty", 0d), targetRows[i].getString("CustomerID"));
		loadOrderRowJSONObject.put("qtyInfoRow", (JSONObject)StringUtil.getJSON(qtyInfoRow));
		int caseQty			= qtyInfoRow.get("case_qty", 0);
		int pieceQty		= qtyInfoRow.get("piece_qty", 0);
		int innerQty		= qtyInfoRow.get("inner_qty", 0);
		int caseSumQty		= qtyInfoRow.get("case_sum_qty", 0);
		String Unit			= qtyInfoRow.getString("unit");
		String caseQtyStr	= "";
		if(0 != caseQty){
			caseQtyStr += "+" + (int)caseQty + "(Case)";
		}
		if(0 != pieceQty){
			caseQtyStr += "+" + (int)pieceQty + "(Piece)";
		}
		if(0 != innerQty){
			caseQtyStr += "+" + (int)innerQty + "(Inner)";
		}
		if(0 != pieceQty){
			caseQtyStr += "=" + (int)caseSumQty + "(Case)";
		}
		if(caseQtyStr.length() > 0){
			caseQtyStr = caseQtyStr.substring(1);
		}
		caseTotal += caseSumQty;
		boolean isLast = false;
		if(is_print_order_master_wms && (i+1) < targetRows.length && currentOrderNo != targetRows[i+1].get("OrderNo", 0L)){
			isLast = true;
		}
		if((!is_print_order_master_wms) && (i+1) == targetRows.length){
			isLast = true;
		}
		WmsItemUnitTicketKey wmsItemUnitTicketKey = new WmsItemUnitTicketKey();
		loadOrderRowJSONObject.put("unit", wmsItemUnitTicketKey.getWmsItemUnitKeyByKey(Unit));
		loadOrderRowJSONObject.put("caseSumQty", caseSumQty);
		loadOrderRowJSONObject.put("caseQtyStr", caseQtyStr);
		loadOrderRowJSONObject.put("isLast", isLast);
		return caseTotal;
	}
	
	/**
	 * @param printType
	 * @param loadNo
	 * @param master_bol_nos
	 * @param companyId
	 * @param customerId
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] aquireTargetRowsByPrintType(String pageName, String printType, String loadNo, String master_bol_nos, String companyId, String customerId, long adid) throws Exception{
		DBRow[] targetRows = null;
		if(StringUtils.equals("masterBol", printType)){
			targetRows = sqlServerMgrZJ.findMasterBolsSomeInfoOneByMasterBolStr(loadNo,master_bol_nos, companyId, customerId, adid, null);
		}else if(StringUtils.equals("bol", printType)){
			targetRows = StringUtils.equals("a4print_vics2_order", printType)?null:sqlServerMgrZJ.findMasterBolLinesByMasterBolStr(loadNo,master_bol_nos,companyId,customerId,adid, null);
		}
		return targetRows;
	}
	
	/**
	 * @param result_map
	 * @param targetRows
	 * @param mainRow
	 * @param out_seal
	 * @param master_bol_nos
	 * @param companyId
	 * @param customerId
	 * @param adid
	 * @param loadNo
	 * @param printType
	 * @param relativeValue
	 * @throws Exception
	 */
	public void buildResultMap(JSONObject result_map, DBRow[] targetRows, DBRow mainRow, String out_seal, String master_bol_nos, String companyId, String customerId, long adid, String loadNo, String printType, String relativeValue, long orderNo) throws Exception{
		//handle first part labels data
		if(StringUtil.isBlank(out_seal)){
			out_seal = (null!=mainRow)?mainRow.getString("seal"):"";
		}
		result_map.put("out_seal", out_seal);
		DBRow shipperImag = relativeFileMgrZr.queryReativeFile(relativeValue, RelativeFileKey.BiLLOfLoadingShipper);
	    DBRow carrierImag = relativeFileMgrZr.queryReativeFile(relativeValue, RelativeFileKey.BillOfLoadingCarrier);
		
	    if(StringUtils.equals("masterBol", printType)){
			handleFirstPartLabels(result_map, targetRows, mainRow, out_seal, master_bol_nos, companyId, customerId, printType);
			
			//handle second part Customer Order data
	    	DBRow[] orderPONoRows = sqlServerMgrZJ.findLoadNoOrdersPONoInfoByMasterBolNoStr(loadNo,master_bol_nos, companyId, customerId, adid, null);
	    	int orderLength = (orderPONoRows != null)?orderPONoRows.length:0;
			result_map.put("orderLength", orderLength);
	    	handleSecondPartCustomerOrder(result_map, orderPONoRows);
	    	
	    	//handle third part Carrier
	    	DBRow[] orderItemRows = sqlServerMgrZJ.findLoadNoOrdersItemInfoGroupByMasterBolNoStr(loadNo,master_bol_nos, companyId,customerId, adid, null);
	    	int itemLength = (orderItemRows !=null)?orderItemRows.length:0;
			result_map.put("orderItemLength", itemLength);
			handleThirdPartCarrier(result_map, orderItemRows, orderLength, itemLength);
	    	
	    	//else Part
		    handleElsePart(result_map, shipperImag, carrierImag, mainRow);
		//handle other bol types: a4print_vics2_order, a4print_vics2, a4print_bol_order
		}else if(StringUtils.equals("bol", printType)){
			handleVics2Order(result_map, targetRows, mainRow, out_seal, master_bol_nos, companyId, customerId, adid, printType, orderNo);
		//handle bol type of a4print_bol
		}else if(StringUtils.equals("a4print_bol", printType)){
			handleBolType(result_map, targetRows, mainRow, out_seal, master_bol_nos, companyId, customerId, adid, printType);
			//else Part
			handleElsePart(result_map, shipperImag, carrierImag, mainRow);
		}
	}
	
	/**
	 * @param result_map
	 * @param targetRows
	 * @param mainRow
	 * @param out_seal
	 * @param master_bol_nos
	 * @param companyId
	 * @param customerId
	 * @param printType
	 * @throws Exception
	 */
	private void handleBolType(JSONObject result_map, DBRow[] targetRows, DBRow mainRow, String out_seal, String master_bol_nos, String companyId, String customerId, long adid, String printType) throws Exception{
		//except note attrabute, no difference between masterBol and bol type
		JSONArray arrays = new JSONArray();
		for(int n = 0; n < targetRows.length; n++){
			JSONObject loadOrderNoRowJSONObject = new JSONObject();
			long orderNo = targetRows[n].get("OrderNo", 0L);
    		String companyIdOr = targetRows[n].getString("CompanyID");
    		DBRow orderInfoRow = sqlServerMgrZJ.findOrderSomeInfoByOrderNo(orderNo, companyIdOr, adid, null);
    		DBRow orderPONoRow = sqlServerMgrZJ.findOrderPONoInfoByOrderNo(orderNo, companyIdOr, adid, null);
    		if(null!=orderInfoRow && null!=orderPONoRow){
    			DBRow[] orderItemRows = sqlServerMgrZJ.findOrderItemsInfoByOrderNo(orderNo, companyIdOr,adid, null);
    			
				DBRow loadOrderNosRow = targetRows[n];  
				loadOrderNoRowJSONObject = (JSONObject)StringUtil.getJSON(loadOrderNosRow);
				loadOrderNoRowJSONObject.put("APPOINTMENTDATE", DateUtil.parseDateTo12Hour(orderInfoRow.getString("AppointmentDate")));
				orderInfoRow.add("ShipToNameNo", StringUtil.fromStringGetNumber(orderInfoRow.getString("ShipToName")));
				loadOrderNoRowJSONObject.put("orderInfoRow", DBRowUtils.dbRowAsMap(orderInfoRow));
				
				DBRow carrier = sqlServerMgrZJ.findCarrierInfoByCarrierId(orderInfoRow.getString("CarrierID"), orderInfoRow.getString("CompanyID"));
				Map<String, Object> carrierMap = new HashMap<String, Object>();
				carrierMap.put("carrierName", carrier.get("CARRIERNAME"));
				carrierMap.put("SCACCode", carrier.get("SCACCODE"));
				loadOrderNoRowJSONObject.put("carrier", carrierMap);
				
				String bolNote = loadOrderNosRow.getString("BOLNote");
				String[] bolNoteArray = null;
				String bolNoteValue = "";
				bolNote = (!StringUtil.isBlank(bolNote))?bolNote:sqlServerMgrZJ.findFirstTimeOrderBolNoteByMasterBols(master_bol_nos, companyId, customerId); 
	          	if(!StringUtil.isBlank(bolNote)){  
	          		bolNoteArray = StringUtil.replaceEnter(bolNote).split("<br>");  
	          		int temp = (bolNoteArray.length > 5)?5:bolNoteArray.length;
	          		for(int i = 0 ;i < temp; i++ ){
	          			bolNoteValue += bolNoteArray[i] + "<br>";
	          		}
	          	}
	          	loadOrderNoRowJSONObject.put("note", bolNoteValue);
	          	
	          	//orderPONoRow
	          	loadOrderNoRowJSONObject.put("orderPONoRow", DBRowUtils.dbRowAsMap(orderPONoRow));
	          	
	          	//orderItemRows
	          	JSONArray orderItemArray = new JSONArray();
	          	double palletCountSum = 0d;
			    int caseCountSum = 0;
			    int weightSum = 0;
			    int blankCount = orderItemRows.length < 7?(6-orderItemRows.length):6;
			    int itemLength = (orderItemRows !=null)?orderItemRows.length:0;
	          	for(int i=0; i< orderItemRows.length; i++){
	          		JSONObject tempObject = new JSONObject();
	          		DBRow tempRow = orderItemRows[i];
	          		
	          		double palletCount = tempRow.get("palletCount", 0d);
			      	palletCountSum += palletCount;
			      	int orderLineCase = tempRow.get("orderLineCase", 0);
			      	caseCountSum += orderLineCase;
			      	int weight = tempRow.get("weightItemAndPallet", 0);
			      	weightSum += weight;
			      	
	          		tempObject = (JSONObject)StringUtil.getJSON(tempRow);
	          		
	          		tempObject.put("palletcount", MoneyUtil.formatDoubleDecimal(tempRow.get("PALLETCOUNT", 0d)));
	          		tempObject.put("weightitemandpallet", MoneyUtil.formatDoubleDecimal(tempRow.get("WEIGHTITEMANDPALLET", 0d)));
	          		tempObject.put("orderLineCaseType", 0!=tempRow.get("ORDERLINECASE", 0)?"CTNS":"Piece");
	          		tempObject.put("unit", new WmsItemUnitKey().getWmsItemUnitKeyByKey(tempRow.getString("UNIT")));
	          		
	          		orderItemArray.put(tempObject);
	          	}
	          	loadOrderNoRowJSONObject.put("orderItemRows", orderItemArray);
	          	loadOrderNoRowJSONObject.put("palletCountSum", palletCountSum);
	          	loadOrderNoRowJSONObject.put("caseCountSum", caseCountSum);
	          	loadOrderNoRowJSONObject.put("weightSum", weightSum);
	          	loadOrderNoRowJSONObject.put("blankCount", blankCount);
    			loadOrderNoRowJSONObject.put("orderItemLength", itemLength);
    		}
          	
          	arrays.put(loadOrderNoRowJSONObject);
		}
		result_map.put("loadOrderNoRows", arrays);
	}
	
	private void handleVics2Order(JSONObject result_map, DBRow[] targetRows, DBRow mainRow, String out_seal, String master_bol_nos, String companyId, String customerId, long adid, String printType, long orderNo) throws Exception{
		//specific bol type: a4print_vics2_order, a4print_vics2
		JSONObject loadOrderNoRowJSONObject = new JSONObject();
		DBRow orderInfoRow = sqlServerMgrZJ.findOrderSomeInfoByOrderNo(orderNo, companyId, adid, null);
		DBRow orderPONoRow = sqlServerMgrZJ.findOrderPONoInfoByOrderNo(orderNo, companyId, adid, null);
		if(null!=orderInfoRow && null!=orderPONoRow){
			DBRow[] orderItemRows = sqlServerMgrZJ.findOrderItemsInfoByOrderNo(orderNo, companyId, adid, null);
			
			loadOrderNoRowJSONObject.put("orderInfoRow", DBRowUtils.dbRowAsMap(orderInfoRow));
			loadOrderNoRowJSONObject.put("APPOINTMENTDATE", DateUtil.parseDateTo12Hour(orderInfoRow.getString("AppointmentDate")));
			orderInfoRow.add("ShipToNameNo", StringUtil.fromStringGetNumber(orderInfoRow.getString("ShipToName")));
			
			DBRow carrier = sqlServerMgrZJ.findCarrierInfoByCarrierId(orderInfoRow.getString("CarrierID"), orderInfoRow.getString("CompanyID"));
			Map<String, Object> carrierMap = new HashMap<String, Object>();
			carrierMap.put("carrierName", carrier.get("CARRIERNAME"));
			carrierMap.put("SCACCode", carrier.get("SCACCODE"));
			loadOrderNoRowJSONObject.put("carrier", carrierMap);
			
			String bolNoteValue = "";
			if(!orderInfoRow.getString("BOLNote").equals("")){ 
				String[] bolNoteArray=StringUtil.replaceEnter(orderInfoRow.getString("BOLNote")).split("<br>");
				int temp = (bolNoteArray.length > 5)?5:bolNoteArray.length;
          		for(int i = 0 ;i < temp; i++ ){
          			bolNoteValue += bolNoteArray[i] + "<br>";
          		}
			}
          	loadOrderNoRowJSONObject.put("note", bolNoteValue);
	          	
          	//orderPONoRow
          	loadOrderNoRowJSONObject.put("orderPONoRow", DBRowUtils.dbRowAsMap(orderPONoRow));
	          	
          	//orderItemRows
          	JSONArray orderItemArray = new JSONArray();
          	double palletCountSum = 0d;
		    int caseCountSum = 0;
		    int weightSum = 0;
		    int blankCount = orderItemRows.length < 7?(6-orderItemRows.length):6;
		    int itemLength = (orderItemRows !=null)?orderItemRows.length:0;
          	for(int i=0; i< orderItemRows.length; i++){
          		JSONObject tempObject = new JSONObject();
          		DBRow tempRow = orderItemRows[i];
          		
          		double palletCount = tempRow.get("palletCount", 0d);
		      	palletCountSum += palletCount;
		      	int orderLineCase = tempRow.get("orderLineCase", 0);
		      	caseCountSum += orderLineCase;
		      	int weight = tempRow.get("weightItemAndPallet", 0);
		      	weightSum += weight;
		      	
          		tempObject = (JSONObject)StringUtil.getJSON(tempRow);
          		
          		tempObject.put("palletcount", MoneyUtil.formatDoubleDecimal(tempRow.get("PALLETCOUNT", 0d)));
          		tempObject.put("weightitemandpallet", MoneyUtil.formatDoubleDecimal(tempRow.get("WEIGHTITEMANDPALLET", 0d)));
          		tempObject.put("orderLineCaseType", 0!=tempRow.get("ORDERLINECASE", 0)?"CTNS":"Piece");
          		tempObject.put("unit", new WmsItemUnitKey().getWmsItemUnitKeyByKey(tempRow.getString("UNIT")));
          		
          		orderItemArray.put(tempObject);
          	}
          	loadOrderNoRowJSONObject.put("orderItemRows", orderItemArray);
          	loadOrderNoRowJSONObject.put("palletCountSum", palletCountSum);
          	loadOrderNoRowJSONObject.put("caseCountSum", caseCountSum);
          	loadOrderNoRowJSONObject.put("weightSum", weightSum);
          	loadOrderNoRowJSONObject.put("blankCount", blankCount);
			loadOrderNoRowJSONObject.put("orderItemLength", itemLength);
		}
		result_map.put("loadOrderNoRows", loadOrderNoRowJSONObject);
	}
	
	/**
	 * @param result_map
	 * @param masterBols
	 * @param mainRow
	 * @param out_seal
	 * @param master_bol_nos
	 * @param companyId
	 * @param customerId
	 * @throws Exception
	 */
	private void handleFirstPartLabels(JSONObject result_map, DBRow[] targetRows, DBRow mainRow, String out_seal, String master_bol_nos, String companyId, String customerId, String printType) throws Exception{
		//except note attrabute, no difference between masterBol and bol type
		JSONArray firstPartLabels = new JSONArray();
		for(int n = 0; n < targetRows.length; n++){
			JSONObject masterSomeInforRowJSONObject = new JSONObject();
			
			DBRow masterSomeInfoRow = targetRows[n];  
			masterSomeInforRowJSONObject = (JSONObject)StringUtil.getJSON(masterSomeInfoRow);
			masterSomeInforRowJSONObject.put("APPOINTMENTDATE", DateUtil.parseDateTo12Hour(masterSomeInfoRow.getString("AppointmentDate")));
			
			DBRow carrier = sqlServerMgrZJ.findCarrierInfoByCarrierId(masterSomeInfoRow.getString("CarrierID"), masterSomeInfoRow.getString("CompanyID"));
			Map<String, Object> carrierMap = new HashMap<String, Object>();
			carrierMap.put("carrierName", carrier.get("CARRIERNAME"));
			carrierMap.put("SCACCode", carrier.get("SCACCODE"));
			masterSomeInforRowJSONObject.put("carrier", carrierMap);
			
			String masterNote = masterSomeInfoRow.getString("Note");
			String[] masterNoteArray = null;
			String masterNoteValue = "";
			masterNote = (!StringUtil.isBlank(masterNote))?masterNote:sqlServerMgrZJ.findFirstTimeOrderBolNoteByMasterBols(master_bol_nos, companyId, customerId); 
          	if(!StringUtil.isBlank(masterNote)){  
          		masterNoteArray = StringUtil.replaceEnter(masterNote).split("<br>");  
          		int temp = (masterNoteArray.length > 5)?5:masterNoteArray.length;
          		for(int i = 0 ;i < temp; i++ ){
          			masterNoteValue += masterNoteArray[i] + "<br>";
          		}
          	}
          	masterSomeInforRowJSONObject.put("note", masterNoteValue);
          	
          	firstPartLabels.put(masterSomeInforRowJSONObject);
		}
		result_map.put("masterBols", firstPartLabels);
	}

	/**
	 * @param result_map
	 * @param orderPONoRows
	 * @throws JSONException
	 */
	private void handleSecondPartCustomerOrder(JSONObject result_map, DBRow[] orderPONoRows) throws JSONException{
		JSONArray secondPartCustomerOrder = new JSONArray();
    	int orderPkgSum = 0;
	  	int orderWeight = 0;
		for(int j = 0; j < orderPONoRows.length; j++){
			Map<String, Object> orderPONoRowMap = new HashMap<String, Object>();
			int orderLineCaseSum = orderPONoRows[j].get("orderLineCaseSum", 0);
	  		orderPkgSum += orderLineCaseSum;
	  		int weightItemAndPallet = orderPONoRows[j].get("weightItemAndPallet", 0);
	  		orderWeight += weightItemAndPallet;
	  		String ponoStr = orderPONoRows[j].getString("PONo");
	  		String ShipToStoreNo = orderPONoRows[j].getString("ShipToStoreNo");
	  		if(!StringUtil.isBlank(ShipToStoreNo)){
	  			ShipToStoreNo = StringUtil.fromStringGetNumber(ShipToStoreNo);
	  			if(!StringUtil.isBlank(ShipToStoreNo)){
	  				ponoStr += "-"+ShipToStoreNo;
	  			}
	  		}
	  		String DeptNo = orderPONoRows[j].getString("DeptNo");
	  		if(!StringUtil.isBlank(DeptNo)){
	  			ponoStr += " Dept:"+DeptNo;
	  		}
	  		orderPONoRowMap.put("ponoStr", ponoStr);
	  		orderPONoRowMap.put("orderLineCaseSum", orderLineCaseSum);
	  		orderPONoRowMap.put("weightItemAndPallet", weightItemAndPallet);
	  		orderPONoRowMap.put("ReferenceNo", orderPONoRows[j].getString("ReferenceNo"));
	  		secondPartCustomerOrder.put(orderPONoRowMap);
		}
    	result_map.put("orderPkgSum", orderPkgSum);
    	result_map.put("orderWeight", orderWeight);	
    	result_map.put("orderPONos", secondPartCustomerOrder);
	}
	
	/**
	 * @param result_map
	 * @param orderItemRows
	 * @param orderLength
	 * @param itemLength
	 * @throws JSONException
	 */
	private void handleThirdPartCarrier(JSONObject result_map, DBRow[] orderItemRows, int orderLength, int itemLength) throws JSONException{
		JSONArray thirdPartCarrier = new JSONArray();
    	double orderItemPalletSum = 0d;
      	int orderItemCaseSum = 0;
    	int orderItemWeightSum = 0;
    	int orderRows = orderLength<1?1:orderLength;
    	for(int j = 0; j < orderItemRows.length; j ++){
    		Map<String, Object> orderItemRowMap = new HashMap<String, Object>();
	   		double palletCount = orderItemRows[j].get("palletCount", 0d);
	   		int orderLineCase = orderItemRows[j].get("orderLineCase", 0);
	   		int weightItemAndPallet = orderItemRows[j].get("weightItemAndPallet", 0);
	   		orderItemPalletSum += palletCount;
	   	  	orderItemCaseSum += orderLineCase;
	   		orderItemWeightSum += weightItemAndPallet;   
	   		String orderItemPallet = (0!=palletCount)?MoneyUtil.formatDoubleDecimal(palletCount):("-");
	   		orderItemRowMap.put("palletCount", orderItemPallet);
	   		orderItemRowMap.put("orderLineCase", orderLineCase);
	   		orderItemRowMap.put("weightItemAndPallet", weightItemAndPallet);
	   		String CommodityDescription = orderItemRows[j].getString("CommodityDescription");
	   		orderItemRowMap.put("CommodityDescription", StringUtil.isBlank(CommodityDescription)?"":CommodityDescription);
	   		String nmfc = orderItemRows[j].getString("NMFC");
	   		orderItemRowMap.put("NMFC", StringUtil.isBlank(nmfc)?"":nmfc);
	   		String freightClass = orderItemRows[j].getString("freightClass");
	   		orderItemRowMap.put("CLASS", StringUtil.isBlank(freightClass)?"":freightClass);
   			orderItemRowMap.put("orderLineCaseType", (0!=orderLineCase)?"CTNS":"Piece");
	   		thirdPartCarrier.put(orderItemRowMap);
    	}
    	int blankCount = 0;
		if(orderLength>=10){
			blankCount=8;
		}else if((orderRows+itemLength)>10){
			blankCount= 9-orderLength;
		}else{
			blankCount=10-(itemLength+orderLength);
		}
    	result_map.put("orderItemPalletSum", orderItemPalletSum);
    	result_map.put("orderItemCaseSum", orderItemCaseSum);
    	result_map.put("orderItemWeightSum", orderItemWeightSum);
    	result_map.put("orderRows", orderRows);
    	result_map.put("blankCount", blankCount);
    	result_map.put("orderItems", thirdPartCarrier);
    	result_map.put("orderItemCaseSumType", (0!=orderItemCaseSum)?"CTNS":"Piece");
	}	
	
	/**
	 * @param result_map
	 * @param shipperImag
	 * @param carrierImag
	 * @param mainRow
	 * @throws JSONException
	 */
	private void handleElsePart(JSONObject result_map, DBRow shipperImag, DBRow carrierImag, DBRow mainRow) throws JSONException{
		String shipperUrl = (shipperImag == null)?"":shipperImag.getString("relative_file_path");
	    String systenFolder = ConfigBean.getStringValue("systenFolder");
	    String carrierUrl = (carrierImag == null)?"":carrierImag.getString("relative_file_path");
	    String timeIn = "";
	    String timeOut = "";
	    if(carrierImag != null){
			//timeIn 的时间是DockCheckIn的时间
			//timeOut 的时间是签字的时间create_time
          	timeIn =  DateUtil.parseDateTo12HourNoYear(mainRow.getString("warehouse_check_in_time"));
        	timeOut = DateUtil.parseDateTo12HourNoYear(carrierImag.getString("create_time"));
	    }
	    result_map.put("shipperUrl", shipperUrl);
	    result_map.put("carrierUrl", carrierUrl);
	    result_map.put("systemFolder", systenFolder);
	    result_map.put("timeIn", timeIn);
	    result_map.put("timeOut", timeOut);
	}
	
	@Override
	public JSONArray countingSheetMessage(JSONArray jsons, long entry_id, long adid, String printType)throws Exception{
		try{
			//target json array
			JSONArray countingSheetJsonArray = new JSONArray();
			for(int x=0;x<jsons.length();x++){
				JSONObject orderItemRowsJSONObject = new JSONObject();
				DBRow mainRow = checkInMgrZwb.findGateCheckInById(entry_id);
				orderItemRowsJSONObject.put("mainRow", (JSONObject)StringUtil.getJSON(mainRow));
				
		    	JSONObject json = jsons.getJSONObject(x);
		    	String loadNo = "";
		    	String number = json.getString("number");
		    	orderItemRowsJSONObject.put("number", number);
		    	String door_name = json.getString("door_name");
		    	orderItemRowsJSONObject.put("door_name", door_name);
		    	int number_type = json.getInt("number_type");
		    	orderItemRowsJSONObject.put("number_type", number_type);
		    	if(number_type == ModuleKey.CHECK_IN_LOAD){
		    		loadNo = number;			
		    	}
		    	orderItemRowsJSONObject.put("loadNo", loadNo);
		    	String date = new TDate().getFormateTime(DateUtil.NowStr(), "MM/dd/yyyy");
		    	orderItemRowsJSONObject.put("nowDate", date);
		    	
		    	long orderNo = 0;
	    		if(number_type == ModuleKey.CHECK_IN_PONO) {
	    			orderNo = json.getLong("order");
	    		} else if(number_type == ModuleKey.CHECK_IN_ORDER) {
	    			orderNo = json.getLong("number");
	    		}	    	
		    	String companyId = json.getString("companyId");
				String customerId = json.getString("customerId");
				
		    	double palletCountSum = 0d;
			    int caseCountSum = 0;
			    HashMap<String, Object> map = new HashMap<String, Object>();
		  		map.put("palletCountSum", palletCountSum);
		  		map.put("caseCountSum", caseCountSum);
		  		
		  		JSONArray result_map = new JSONArray();
			    if(StringUtils.equals("a4print_counting_sheet", printType)){
			    	DBRow[] loadOrderNos = sqlServerMgrZJ.findMasterBolLinesByLoadNo(loadNo,companyId,customerId, adid, null);
					if(0 == loadOrderNos.length) {
						loadOrderNos = sqlServerMgrZJ.findOrdersNoAndCompanyByLoadNo(loadNo,companyId,customerId, adid, null);
					}
					for(int k = 0; k < loadOrderNos.length; k++) {
						orderNo = loadOrderNos[k].get("OrderNo", 0L);
						String companyIdOr = loadOrderNos[k].getString("CompanyID");
						DBRow[] orderItemRows = sqlServerMgrZJ.findOrderItemsInfoForCountingByOrderNo(orderNo, companyIdOr, adid, null);
						handleOrderItemRowsInCountingSheet(result_map, orderItemRows, map, orderNo);
					}
			    }else{
			    	String companyIdOr = json.getString("companyId");
			    	DBRow[] orderItemRows = sqlServerMgrZJ.findOrderItemsInfoForCountingByOrderNo(orderNo, companyIdOr, adid, null);
			    	handleOrderItemRowsInCountingSheet(result_map, orderItemRows, map, orderNo);
			    }
			    orderItemRowsJSONObject.put("orderItemRows", result_map);
			    int itemCount = result_map.length();
			    int count = 20 - (itemCount%20);
			    orderItemRowsJSONObject.put("itemCount", itemCount);
			    orderItemRowsJSONObject.put("count", count);
			    orderItemRowsJSONObject.put("palletCountSum", map.get("palletCountSum"));
			    orderItemRowsJSONObject.put("caseCountSum", map.get("caseCountSum"));
	        	countingSheetJsonArray.put(orderItemRowsJSONObject);
	    	}
			return countingSheetJsonArray;
		}catch(Exception e){
			throw new SystemException(e,"addProductDetailLable",log);
		}
	}
	
	public void handleOrderItemRowsInCountingSheet(JSONArray jsonArray, DBRow[] orderItemRows, HashMap<String, Object> map, long orderNo) throws JSONException{
		double palletCountSum = Double.valueOf(map.get("palletCountSum").toString());
	    int caseCountSum = Integer.valueOf(map.get("caseCountSum").toString());
	    
		for(int m = 0; m < orderItemRows.length; m ++) {
			JSONObject orderItemRowJSONObject = new JSONObject();
			orderItemRowJSONObject.put("orderno", orderNo);
			
	      	double palletCount = orderItemRows[m].get("palletCount", 0d);
	      	palletCountSum += palletCount;
	      	int orderLineCase = orderItemRows[m].get("orderLineCase", 0);
	      	caseCountSum += orderLineCase;
	      	int palletCountToInt = MoneyUtil.formatDoubleUpInt(palletCount);
	      	orderItemRowJSONObject.put("palletCountToInt", palletCountToInt);
	      	orderItemRowJSONObject.put("orderItemRowsLength", orderItemRows.length);
 		    jsonArray.put(orderItemRowJSONObject);
	    }
		map.put("palletCountSum", palletCountSum);
  		map.put("caseCountSum", caseCountSum);
	}
	
	@Override
	public JSONArray packingMessage(JSONArray jsons, long adid, String printType)throws Exception{
		try{
			//target json array
			JSONArray countingSheetJsonArray = new JSONArray();
			for(int i = 0; i < jsons.length(); i ++){
				JSONObject orderInfoRowsJSONObject = new JSONObject();
				JSONObject json=jsons.getJSONObject(i);
				long order=Long.parseLong(json.getString("order_no"));
				String companyId=json.getString("companyId");
				String customerId=json.getString("customerId");
				
				DBRow orderInfo = sqlServerMgrZJ.findOrderSomeInfosByOrderNo(order,companyId, customerId, adid, null);
				if(orderInfo == null){
					continue; 
				}
				orderInfo.add("OrderedDate", StringUtil.isBlank(orderInfo.getString("OrderedDate"))?"":DateUtil.parseDateFormat(orderInfo.getString("OrderedDate"), "M/d/yy"));
				orderInfo.add("RequestedDate", StringUtil.isBlank(orderInfo.getString("RequestedDate"))?"":DateUtil.parseDateFormat(orderInfo.getString("RequestedDate"), "M/d/yy"));
				orderInfo.add("ShippedDate", StringUtil.isBlank(orderInfo.getString("ShippedDate"))?"":DateUtil.parseDateFormat(orderInfo.getString("ShippedDate"), "M/d/yy"));
				orderInfoRowsJSONObject.put("orderInfo", (JSONObject)StringUtil.getJSON(orderInfo));
				String date = DateUtil.parseDateTo12HourNoYear(DateUtil.NowStr());
				orderInfoRowsJSONObject.put("nowDate", date);
				
				DBRow[] orderItems = sqlServerMgrZJ.findOrderItemsSomeInfoGroupByOrderNo(order, companyId, customerId, adid, null);
				float volumeTotal = 0F;
			    int unitTotal = 0;
			    int caseTotal = 0 ;
			    JSONArray orderItemsJsonArray = new JSONArray();
				for(DBRow orderItem : orderItems ){
					JSONObject orderItemRowJSONObject = new JSONObject();
					if(StringUtil.isBlank(orderItem.getString("ItemID"))){
						orderItem.add("ItemID", "");
					}
					if(StringUtil.isBlank(orderItem.getString("CommodityDescription"))){
						orderItem.add("CommodityDescription", "");
					}
					if(StringUtil.isBlank(orderItem.getString("BuyerItemID"))){
						orderItem.add("BuyerItemID", "");
					}
					if(StringUtil.isBlank(orderItem.getString("UPC"))){
						orderItem.add("UPC", "");
					}
					if(StringUtil.isBlank(orderItem.getString("StyleNo"))){
						orderItem.add("StyleNo", "");
					}
					if(StringUtil.isBlank(orderItem.getString("Unit"))){
						orderItem.add("Unit", "");
					}
					if(StringUtil.isBlank(orderItem.getString("Size"))){
						orderItem.add("Size", "");
					}
					orderItemRowJSONObject = (JSONObject)StringUtil.getJSON(orderItem);				
					orderItemsJsonArray.put(orderItemRowJSONObject);
					
					volumeTotal += orderItem.get("volume1", 0F); 
					unitTotal += orderItem.get("ShippedQty", 0);  
					caseTotal += orderItem.get("orderLineCase", 0);  
				}
				int volumeTotalInt = (int) ((volumeTotal*10 + 5)/10);
				orderInfoRowsJSONObject.put("orderItems", orderItemsJsonArray);
				orderInfoRowsJSONObject.put("volumeTotal", volumeTotalInt);
				orderInfoRowsJSONObject.put("unitTotal", unitTotal);
				orderInfoRowsJSONObject.put("caseTotal", caseTotal);
				countingSheetJsonArray.put(orderInfoRowsJSONObject);
			}
			return countingSheetJsonArray;
		}catch(Exception e){
			throw new SystemException(e,"addProductDetailLable",log);
		}
	}
	
	@Override
	public JSONObject cargoTrackingNoteTicketMessage(String door_name, long entryId, long adid, String bolNo, String ctnNo, String companyId, String customerId, String printType)throws Exception{
		try{
			JSONObject cargoTrackingNoteTicketJSONObject = new JSONObject();
			String nowDate = new TDate().getFormateTime(DateUtil.NowStr(), "MM-dd-yy HH:mm:ss");
			cargoTrackingNoteTicketJSONObject.put("nowDate", nowDate);
			cargoTrackingNoteTicketJSONObject.put("entryId", entryId);
			cargoTrackingNoteTicketJSONObject.put("door_name", door_name);
			DBRow[] receipts = sqlServerMgrZJ.findReceiptsByBolNoOrContainerNo(bolNo, ctnNo, companyId, customerId , adid, null);
			int count = 0;
			JSONArray receiptsJSONArray = new JSONArray();
			for(int b = 0; b < receipts.length; b ++){
				JSONObject receiptJSONObject = new JSONObject();
				DBRow receipt = receipts[b];
				receipt.add("DevannedDate", DateUtil.parseDateFormat(receipt.getString("DevannedDate"), "MM-dd-yy"));
				receiptJSONObject = (JSONObject)StringUtil.getJSON(receipt);
				receiptJSONObject.put("receiptnoBar", MoneyUtil.fillZeroByRequire(receipt.get("ReceiptNo", 0L), 7));
				DBRow[] receiptLines = sqlServerMgrZJ.findReceiptLinesPalteByBolNoOrCtnNO(bolNo, ctnNo, receipts[b].get("ReceiptNo", 0), adid, null);
		  		JSONArray receiptLinesJSONArray = new JSONArray();
				for(int i = 0; i < receiptLines.length; i ++){
					JSONObject receiptLineJSONObject = new JSONObject();
		  			DBRow receiptLine = receiptLines[i];
		  			receiptLine.add("PlateNo", 0==receiptLine.get("PlateNo", 0L)?"":receiptLine.get("PlateNo", 0L)+"");
		  			receiptLineJSONObject = (JSONObject)StringUtil.getJSON(receiptLine);
		  			
		  			receiptLineJSONObject.put("lineBar", MoneyUtil.fillZeroByRequire(receiptLine.get("LineNo", 0), 3));
		  			receiptLineJSONObject.put("count", MoneyUtil.fillZeroByRequire(++count, 3));
		  			
		  			DBRow locationRow = sqlServerMgrZJ.findLocationWmsByPlateNo(receiptLine.get("PlateNo", 0), receipt.getString("CompanyID"));
		  			receiptLineJSONObject.put("locationID", locationRow.getString("LocationID"));
		  			receiptLinesJSONArray.put(receiptLineJSONObject);
				}
				receiptJSONObject.put("receiptLines", receiptLinesJSONArray);
		  		receiptsJSONArray.put(receiptJSONObject);
			}
			cargoTrackingNoteTicketJSONObject.put("receipts", receiptsJSONArray);
			return cargoTrackingNoteTicketJSONObject;
		}catch(Exception e){
			throw new SystemException(e,"addProductDetailLable",log);
		}
	}
	
	
	//门卫标签
	@Override
	public JSONObject gateCheckMessage(long infoId, long psId, String printType)throws Exception{
		try{
			JSONObject result= new JSONObject();
			DBRow row = checkInMgrZwb.findGateCheckInById(infoId);
			DBRow[] detailRows = checkInMgrZwb.findAllLoadByMainId(row.get("dlo_id",0l));
			DBRow detailRow=detailRows.length > 0?detailRows[0]:null;
			
			String ctreate_time = row.getString("create_time");     					    //进门卫的时间
			String gate_check_in_time=row.getString("create_time").substring(0,16); 		//门卫时间截取格式
			String appointment_time = row.get("appointment_time","");  					    //预约的时间
			long isLive=row.get("isLive",0l);     						  					//判断是哪种类型的签
			String container_no=row.get("gate_container_no","");          					//货柜号  用来判断 是车头来，还是车头车尾一起来
			long rel_type=row.get("rel_type",0l);						  					//提货/送货
			long entry_id=infoId;										 				 	//主单据id
			String liscense_palte=row.getString("gate_liscense_plate","").toUpperCase(); 	//驾照
			int number_type=detailRow.get("number_type",0);								    //子单据类型
			String number=detailRow.get("number","");										//子单据号
			String mc_dot=row.get("mc_dot","");												//mc_dot
			String company_name=row.get("company_name","");									//运输公司名字
			long yc_id=row.get("yc_id",0l);												    //停车位id
			String yc_name="";																//停车位名字
			String door_name="";															//门名字
			long door_id=detailRow.get("rl_id",0l);											//门id
			String search_container_no=row.getString("search_gate_container_no","");        //提走的货柜号
			
			if(yc_id!=0){
				DBRow ycRow=checkInMgrZwb.findStorageYardControl(row.get("yc_id", 0l)); 
				yc_name=ycRow.getString("yc_no");
			}else{
				DBRow doorRow=checkInMgrZwb.findDoorById(detailRow.get("rl_id",0l));
				door_name=doorRow.getString("doorId");
			}
			long detail_length=detailRows.length;
			String title=detailRow.get("supplier_id","");                                   //title
			String account_id=detailRow.get("account_id","");								//account_id
			String isShow="";																//计算迟到
			if(!"".equals(appointment_time)){	
				//转化为分钟
				long gate_check_in_min = DateUtil.getDate2LongTime(ctreate_time)/60000;
				long appointment_min = DateUtil.getDate2LongTime(appointment_time)/60000;
				if((gate_check_in_min-appointment_min)>-30 || (gate_check_in_min-appointment_min)<30 ){
					isShow="late";
				}else{
					isShow="early";
				}
			}
			String rel_type_show="";														//rel_type显示
			if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.DELIVERY){ 
				rel_type_show="Delivery";
		  	}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.PICK_UP){
		  		rel_type_show="Pick Up";
		  	}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.BOTH){
		  		rel_type_show="Both";
		  	}else if(row.get("rel_type",0l)==CheckInMainDocumentsRelTypeKey.NONE){
		  		rel_type_show="None";
		  	}
			String number_type_show=moduleKey.getModuleName(number_type);
			
			String parkOrDoor="";
			String parkOrDoorVal="";
			if(yc_id!=0){
				parkOrDoor="Parking";
				parkOrDoorVal=yc_name;
			}else if(door_id!=0){
				parkOrDoor="Dock";
				parkOrDoorVal=door_name;
			}else{
				parkOrDoor="Parking";
				parkOrDoorVal="";
			}
			
			String searchParkOrDoor="";
			String searchParkOrDoorVal="";
			if(!row.getString("search_gate_container_no","").equals("")){ 
				DBRow pickUpRow=checkInMgrZwb.findSpotByCtnNo(row.getString("search_gate_container_no",""),psId);
				if(pickUpRow!=null){ 
	   	     		DBRow[] searchDetailRow=checkInMgrZwb.findUseDoorByMainId(pickUpRow.get("dlo_id",0l));
	   	     		if(pickUpRow.get("yc_id",0l)!=0){ 
	   	     			searchParkOrDoor="Parking";
	   	     			DBRow ycRow=checkInMgrZwb.findStorageYardControl(pickUpRow.get("yc_id", 0l)); 
	   	     			searchParkOrDoorVal=ycRow.getString("yc_no");
	   	     		}else{
	   	     			searchParkOrDoor="Dock";
	   	     			DBRow doorRow=checkInMgrZwb.findDoorById(searchDetailRow[0].get("rl_id",0l));
	   	     			searchParkOrDoorVal=doorRow.getString("doorId");
	   	     		}
				}
			}
			
			
			DBRow keyRow=new DBRow();
			keyRow.add("isLive", isLive);
			result.put("key", keyRow);
			DBRow valRow=new DBRow();
			if(detail_length==1){       //判断显示title 、account_id
				if(rel_type==CheckInMainDocumentsRelTypeKey.DELIVERY){
					valRow.add("title_account","Title");
					valRow.add("title_account_val",title);
				}else if(rel_type==CheckInMainDocumentsRelTypeKey.PICK_UP){
					valRow.add("title_account","AccountID");
					valRow.add("title_account_val",account_id);
				}
			}
			
			valRow.add("search_container_no",search_container_no);
			valRow.add("search_park_door",searchParkOrDoor);
			valRow.add("search_park_door_val",searchParkOrDoorVal);
			
			valRow.add("detail_length",detail_length);
			valRow.add("gate_check_in_time",gate_check_in_time);
			valRow.add("container_no", container_no);
			valRow.add("rel_type",rel_type);
			valRow.add("entry_id",entry_id);
			valRow.add("liscense_palte",liscense_palte);
			valRow.add("number_type",number_type);
			valRow.add("number",number);
			valRow.add("mc_dot",mc_dot);
			valRow.add("company_name",company_name);
			valRow.add("park_door_title",parkOrDoor);
			valRow.add("park_door_val",parkOrDoorVal);
			valRow.add("isShow",isShow);
			valRow.add("rel_type_show",rel_type_show);
			valRow.add("number_type_show",number_type_show);
			result.put("value",valRow);
			
			return result;
		}catch(Exception e){
			throw new SystemException(e,"addProductDetailLable",log);
		}
	}
	
	
	public CheckInMgrIfaceHlb getCheckInMgrHlb() {
		return checkInMgrHlb;
	}

	public void setCheckInMgrHlb(CheckInMgrIfaceHlb checkInMgrHlb) {
		this.checkInMgrHlb = checkInMgrHlb;
	}
	
	public CheckInMgrIfaceZwb getCheckInMgrZwb() {
		return checkInMgrZwb;
	}

	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}

	public RelativeFileMgrIfaceZr getRelativeFileMgrZr() {
		return relativeFileMgrZr;
	}

	public void setRelativeFileMgrZr(RelativeFileMgrIfaceZr relativeFileMgrZr) {
		this.relativeFileMgrZr = relativeFileMgrZr;
	}

	public SQLServerMgrIFaceZJ getSqlServerMgrZJ() {
		return sqlServerMgrZJ;
	}

	public void setSqlServerMgrZJ(SQLServerMgrIFaceZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}
}
