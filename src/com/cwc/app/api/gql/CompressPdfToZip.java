package com.cwc.app.api.gql;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.Deflater;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;

/***
 * pdf压缩成zip
 * @author Administrator
 *
 */
public class CompressPdfToZip {
    /**
     * 生成pdfzip
     * @param src
     * @throws Exception 
     */
	@SuppressWarnings("resource")
	public void createPdfZip(String src,String target) throws Exception{
	try{
		FileOutputStream fos=null;
		do {
			try {
				fos = new FileOutputStream(new String(target.getBytes("UTF-8")));//压缩文件到指定的路径 
			} catch (Exception e) {
				// TODO Auto-generated catch block
				continue;
			} 
		} while (fos==null);
		
		 
		 CheckedOutputStream csum = new CheckedOutputStream(fos, new CRC32());//使用指定校验和创建输出流 
		 
		 ZipOutputStream zos = new ZipOutputStream(csum);  
		 
		 zos.setEncoding("UTF-8");  
		 
		 BufferedOutputStream out = new BufferedOutputStream(zos);   
		 
		 zos.setMethod(ZipOutputStream.DEFLATED);    //压缩级别为最强压缩，但时间要花得多一点   
		 zos.setLevel(Deflater.BEST_SPEED);
		 
		 File srcFile = new File(src); //压缩源
		 
		 //校验
		 if (!srcFile.exists() || (srcFile.isDirectory() && srcFile.list().length == 0)) {  
	         throw new FileNotFoundException(  
	                 "File must exist and  ZIP file must have at least one entry.");  
	     }  
		 
		 //获取压缩源所在父目录   
	     src = src.replaceAll("\\\\", "/");  
		 String prefixDir = null;  
	     if (srcFile.isFile()) {  
	         prefixDir = src.substring(0, src.lastIndexOf("/") + 1);  
	     } else {  
	         prefixDir = (src.replaceAll("/$", "") + "/");  
	     }  

	     //如果不是根目录   
	     if (prefixDir.indexOf("/") != (prefixDir.length() - 1) && true) {  
	         prefixDir = prefixDir.replaceAll("[^/]+/$", "");  
	     }  
	     //开始压缩文件
	     writeRecursive(zos, out, srcFile, prefixDir); 
	     out.close();
	     // 注：校验和要在流关闭后才准备，一定要放在流被关闭后使用   
	     System.out.println("Checksum: " + csum.getChecksum().getValue());  
		
		
	   }catch(Exception e){
		   throw new Exception((new StringBuilder()).append("createPdfZip").append(e).toString());
	   }
		
	}
	/**
	 * 迭代压缩文件
	 * @param zos
	 * @param bo
	 * @param srcFile
	 * @param prefixDir
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private static void writeRecursive(ZipOutputStream zos, BufferedOutputStream bo,  
	         File srcFile, String prefixDir) throws Exception {  
		try{
			 ZipEntry zipEntry;  
	         //处理文件路径
		     String filePath = srcFile.getAbsolutePath().replaceAll("\\\\", "/").replaceAll(  
		             "//", "/");  
		     if (srcFile.isDirectory()) {  
		         filePath = filePath.replaceAll("/$", "") + "/";  
		     }  
		     String entryName = filePath.replace(prefixDir, "").replaceAll("/$", "");  
		     if (srcFile.isDirectory()) {  
		         if (!"".equals(entryName)) {  
		             //如果是目录，则需要在写目录后面加上 /    
		             zipEntry = new ZipEntry(entryName + "/");  
		             zos.putNextEntry(zipEntry);  
		         }  

		         File srcFiles[] = srcFile.listFiles();  
		         for (int i = 0; i < srcFiles.length; i++) {  
		             writeRecursive(zos, bo, srcFiles[i], prefixDir);  
		         }  
		     } else {  
		         BufferedInputStream bi = new BufferedInputStream(new FileInputStream(srcFile));  

		         //开始写入新的ZIP文件条目并将流定位到条目数据的开始处   
		         zipEntry = new ZipEntry(entryName);  
		         zos.putNextEntry(zipEntry);  
		         byte[] buffer = new byte[1024];  
		         int readCount = bi.read(buffer);  

		         while (readCount != -1) {  
		             bo.write(buffer, 0, readCount);  
		             readCount = bi.read(buffer);  
		         }  
		         //注，在使用缓冲流写压缩文件时，一个条件完后一定要刷新一把，不 然可能有的内容就会存入到后面条目中去了   
		         bo.flush();  
		         //文件读完后关闭   
		         bi.close();  
		         srcFile.delete();
		     }  
		}catch(Exception e){
			throw new Exception((new StringBuilder()).append("writeRecursive").append(e).toString());
		}
		
	    
	 }   
	
}
