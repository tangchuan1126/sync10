package com.cwc.app.api;

import javax.xml.bind.annotation.XmlEnumValue;

public enum CheckType {
	@XmlEnumValue("Checkin")
    CHECKIN,

    @XmlEnumValue("Checkout")
    CHECKOUT
}
