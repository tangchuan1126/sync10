package com.cwc.app.api.zl;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zl.FloorCodeMissMgrZYZ;
import com.cwc.app.iface.zl.CodeMissMgrIFaceZYZ;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;

public class CodeMissMgrZYZ implements CodeMissMgrIFaceZYZ{
	
	private FloorCodeMissMgrZYZ floorCodeMissMgrZYZ;
	
	static Logger log = Logger.getLogger("ACTION");

	//获取所有基础数据信息（带分页）
	@Override
	public DBRow[] getAllCodeMiss(PageCtrl pc) throws Exception {
		
		try {
			return floorCodeMissMgrZYZ.getAllCodeMiss(pc);
		} catch (Exception e) {
			throw new SystemException(e,"getAllCodeMiss",log);
		}
	}
	//根据miss_code获取ItemNumber
	public DBRow[] getItemNumber(String miss_code) throws Exception{
	
		try {
			return floorCodeMissMgrZYZ.getItemNumber(miss_code);
		} catch (Exception e) {
			throw new SystemException(e,"getItemNumber",log);
		}
	}
	//根据miss_code获取Miss_id
	public DBRow[] getMissID(String miss_code) throws Exception{
		
		try {
			return floorCodeMissMgrZYZ.getMissID(miss_code);
		} catch (Exception e) {
			throw new SystemException(e,"getMissID",log);
		}
	}
	
	public void setFloorCodeMissMgrZYZ(FloorCodeMissMgrZYZ floorCodeMissMgrZYZ) {
		this.floorCodeMissMgrZYZ = floorCodeMissMgrZYZ;
	}
	
}
