package com.cwc.app.api.zl;

import java.io.File;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;

import com.cwc.app.api.CatalogMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.exception.product.ProductCantBeSetException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductHasRelationStorageException;
import com.cwc.app.exception.product.ProductInUnionException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductUnionIsInSetException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorProductCodeMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductLogsMgrZJ;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.zj.ProductCodeMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.app.iface.zl.ProductBaseDataMgrIFaceZYZ;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.ProductEditReasonKey;
import com.cwc.app.key.ProductEditTypeKey;
import com.cwc.app.lucene.ProductIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class ProductBaseDataMgrZYZ implements ProductBaseDataMgrIFaceZYZ{
	
	static Logger log = Logger.getLogger("ACTION");
	private ProductMgrIFaceZJ productMgrZJ;
	private ProductMgrIFace productMgr;
	private ProductCodeMgrIFaceZJ productCodeMgrZJ;
	private FloorTransportMgrZr floorTransportMgrZr;
	private FloorProductCodeMgrZJ floorProductCodeMgrZJ;
	private FloorProductMgr floorProductMgr;
	private FloorProductLogsMgrZJ floorProductLogsMgrZJ;
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj ;
	/**
	 * 添加商品基础数据
	 */
	public long addProductXML(String xml,AdminLoginBean adminLoggerBean)
	throws ProductNameIsExistException, ProductCodeIsExistException ,XMLDataErrorException ,NetWorkException, Exception{
		
		try {
			String products = getSampleNode(xml,"Products");
			
			if(products.equals("")||products.length() == 0){
				throw new XMLDataErrorException();
			}
			
				String p_name	= getSampleNode(xml,"p_name").toUpperCase();
				String p_code	= getSampleNode(xml,"p_code").toUpperCase();
				
				//1可用
				int flag = addProductCodeForCheck("0", p_code,CodeTypeKey.Main);//floorProductMgr.getDetailProductByPcode(p_code);
				
				if (2 == flag)
				{
					throw new ProductCodeIsExistException();	//商品条码已经存在
				}
				
				long catalog_id ;
				if(getSampleNode(xml,"catalog_id").equals("")){
					catalog_id = 0;
				}else{
				   catalog_id = Long.parseLong(getSampleNode(xml,"catalog_id"));
				}
				String unit_name = getSampleNode(xml,"unit_name");
				double unit_price = Double.parseDouble(getSampleNode(xml,"unit_price"));
				float length = Float.parseFloat(getSampleNode(xml,"length"));
				float width = Float.parseFloat(getSampleNode(xml,"width"));
				float height = Float.parseFloat(getSampleNode(xml,"height"));
				float weight = Float.parseFloat(getSampleNode(xml,"weight"));
				int sn_length = Integer.parseInt(getSampleNode(xml,"sn_length").trim());

				long pc_id = this.gridAddProductSub(p_name,p_code.toUpperCase(),catalog_id,unit_name,unit_price,length,width,height,weight,sn_length,adminLoggerBean);
				//添加条码
				this.saveProductCodeByXml(xml, pc_id, adminLoggerBean.getAdid());
				//添加title ,这里改成我在Android 端已经选择了商品的title，不用去加载当前登录的人的主title
				String titles = getSampleNode(xml, "title_id");
				if(!StringUtil.isNull(titles)){
					String[] titleArrays = titles.split(",");
					for(String title : titleArrays){
						DBRow titleProduct = new DBRow();
						titleProduct.add("tp_pc_id", pc_id);
						titleProduct.add("tp_title_id",Long.parseLong(title));
						proprietaryMgrZyj.addProprietaryTitleProductByDBRow(titleProduct);
					}
				}
				 
				return pc_id;
			
		}catch(ProductNameIsExistException e)//商品名称已经存在
		{
			throw e;
		}
		catch(ProductCodeIsExistException	e)//商品条码已经存在
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e) {
			throw new SystemException(e,"addProductXML",log);

		}
	}
	private DBRow getMainPriorityTitle(DBRow[] titles){
		if(titles != null && titles.length > 0 ){
			int mini = titles[0].get("title_admin_sort", 0) ;
			DBRow titleMini = titles[0] ;
			for(DBRow title : titles){
				int titleSort = title.get("title_admin_sort", 0);
				if(titleSort < mini ){
					titleMini = title ;
				}
			}
			return  titleMini;
		}
		return null ;
	}
	//添加商品基础数据接口提交 
	public long gridAddProductSub(String p_name, String p_code,
			long catalog_id, String unit_name, double unit_price, float length,
			float width, float heigth, float weight,
			int sn_length ,AdminLoginBean adminLoggerBean)
		throws ProductCodeIsExistException,ProductNameIsExistException,Exception
	{
		try
		{
			float volume = length*width*heigth/1000;//添加商品体积
			
			DBRow detailPro = floorProductMgr.getDetailProductByPname(p_name);
			
			if (detailPro!=null)
			{
				throw new ProductNameIsExistException();	//商品名称已经存在
			}
			//1可用
			int flag = addProductCodeForCheck("0", p_code,CodeTypeKey.Main);//floorProductMgr.getDetailProductByPcode(p_code);
			
			if (2 == flag)
			{
				throw new ProductCodeIsExistException();	//商品条码已经存在
			}
			
			DBRow product = new DBRow();
			product.add("p_name",p_name);
			//product.add("p_code",p_code)
			product.add("catalog_id",catalog_id);
			product.add("unit_name",unit_name);
			product.add("unit_price",unit_price);
			//product.add("gross_profit",(gross_profit/100));
			product.add("weight",weight);
			product.add("union_flag",0);
			product.add("alive",1);//允许抄单
			product.add("volume",volume);//设定商品体积zhanjie
			product.add("length",length);
			product.add("width",width);
			product.add("heigth",heigth);
			product.add("sn_size",sn_length);

			long pc_id = floorProductMgr.addProduct(product);
			
			productCodeMgrZJ.addProductCodeSubNoLog(pc_id,p_code,CodeTypeKey.Main,adminLoggerBean);
			
			//记录商品添加日志
			DBRow product_log = floorProductMgr.getDetailProductByPcid(pc_id);
			
			product_log.add("account",adminLoggerBean.getAdid());
			product_log.add("edit_type",ProductEditTypeKey.ADD);
			product_log.add("edit_reason",ProductEditReasonKey.SELF);
			product_log.add("post_date",DateUtil.NowStr());
			floorProductLogsMgrZJ.addProductLogs(product_log);
			
			//把新商品在所有仓库建库
			CatalogMgr catalogMgr = (CatalogMgr)MvcUtil.getBeanFromContainer("catalogMgr");
			DBRow treeRows[] = catalogMgr.getProductStorageCatalogTree();
			for ( int i=0; i<treeRows.length; i++ )
			{
				if (treeRows[i].get("parentid",0)==0)
				{
					DBRow store = new DBRow();
					store.add("store_station", null);
					store.add("post_date",DateUtil.NowStr());
					store.add("cid",treeRows[i].get("id",0));
					store.add("pc_id",pc_id);
					store.add("store_count",0);
					store.add("store_count_alert",0);
					floorProductMgr.addProductStorage(store);
				}
			}
			
			DBRow[] pcodes = productCodeMgrZJ.getProductCodesByPcid(pc_id);
			String pcodeString = "";
			for (int j = 0; j < pcodes.length; j++) 
			{
				pcodeString += pcodes[j].getString("p_code")+" ";
			}
			
			
			ProductIndexMgr.getInstance().addIndexAsyn(pc_id, p_name,pcodeString, catalog_id,unit_name,1);
			return pc_id ;
		}
		catch (ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (ProductNameIsExistException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"gridAddProductSub",log);
		}
	}
	@Override
	public void saveProductCodeByXml(String xml, long pc_id, long adid) throws Exception
	{
		try
		{
			List<String> codeNames = new ArrayList<String>();
			List<String> codeTypes = new ArrayList<String>();
			String productCodes = xml.split("<productCodes>")[1];
			productCodes = productCodes.substring(0, productCodes.length()-15);
			if(null != productCodes && !"".equals(productCodes) && !"null".equals(productCodes))
			{
				String[] productCodeArr = xml.substring(xml.indexOf("<productCodes>"),xml.lastIndexOf("</productCodes>")).split("</productCodeType>");
				if(productCodeArr.length > 0)
				{
					for (int i = 0; i < productCodeArr.length; i++) 
					{
						codeNames.add(StringUtil.getSampleNode(productCodeArr[i], "productCode"));
						codeTypes.add(StringUtil.getSampleNode(productCodeArr[i], "productCodeType"));
						////system.out.println("-----------------------------");
					}
				}
			}
			
			for (int i = 0; i < codeNames.size(); i++)
			{
				String codeType = codeTypes.get(i);
				if(String.valueOf(CodeTypeKey.Main).equals(codeTypes.get(i)))
				{
					codeType = String.valueOf(CodeTypeKey.Old);
				}
				//新加条码
				DBRow product_code = new DBRow();
				product_code.add("pc_id",pc_id);
				product_code.add("p_code",codeNames.get(i).toUpperCase());
				product_code.add("code_type",codeType);
				product_code.add("create_adid",adid);
				product_code.add("create_time",DateUtil.NowStr());
				
				floorProductCodeMgrZJ.addProductCode(product_code);
			}
		}
		catch (Exception e) {
			throw new SystemException(e,"saveProductCodeByXml(String xml, long pc_id, long adid) ",log);
	
		}
	}
	
	
	/**
	 * 删除商品
	 */
	public void delProductXML(String xml) throws Exception {
		
		try {
			String products = getSampleNode(xml,"Products");
			 
			if(products.equals("")||products.length() == 0){
				throw new XMLDataErrorException();
			}
				long pcid = Long.parseLong(getSampleNode(xml, "pcid"));
				productMgrZJ.gridDelProductSub(pcid);
				
		} 
		catch (ProductInUnionException e)
		{
			throw e;
		}
		catch (ProductHasRelationStorageException e)
		{
			throw e;
		}
		catch (Exception e) {
			throw new SystemException(e,"delProductXML",log);
		}
		
	}
	/**
	 * 修改商品
	 */
	public void modProductXML(String xml, AdminLoginBean adminLoggerBean)  
	throws Exception{
		
		try {
			String products = getSampleNode(xml,"Products");
			
			if(products.equals("")||products.length() == 0){
				throw new XMLDataErrorException();
			}
			
				long pc_id = Long.parseLong(getSampleNode(xml, "pc_id"));
				String p_name = getSampleNode(xml,"p_name").toUpperCase();
				String p_code = getSampleNode(xml,"p_code").toUpperCase();
				long catalog_id = Long.parseLong(getSampleNode(xml,"catalog_id"));
				String unit_name = getSampleNode(xml,"unit_name");
				double unit_price = Double.parseDouble(getSampleNode(xml,"unit_price"));
				float length = Float.parseFloat(getSampleNode(xml,"length"));
				float width = Float.parseFloat(getSampleNode(xml,"width"));
				float height = Float.parseFloat(getSampleNode(xml,"height"));
				float weight = Float.parseFloat(getSampleNode(xml,"weight"));
				int sn_length = Integer.parseInt(getSampleNode(xml, "sn_length").trim());
 
				
				//张睿 sn_size
				productMgrZJ.modProductSub(pc_id,p_name,p_code,catalog_id,unit_name,unit_price,length,width,height,weight,sn_length,adminLoggerBean);
		}
		catch(ProductNameIsExistException e)
		{
			throw e;
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modProductXML",log);
		}
	}
	/**
	 * 创建组合关系  
	 */
	public void addProductUnionXML(String xml,AdminLoginBean adminLoggerBean)
	throws Exception{
		
		try {
			String productUnions = getSampleNode(xml,"ProductUnions");
			 
			if(productUnions.equals("")||productUnions.length() == 0){
				throw new XMLDataErrorException();
			}
			
				String p_name = getSampleNode(xml,"p_name");
				float quantity = Float.parseFloat(getSampleNode(xml,"quantity"));
				long set_pid = Long.parseLong(getSampleNode(xml,"set_pid"));
				
				productMgr.addProductUnionSub(p_name, quantity, set_pid,adminLoggerBean);
				
		}catch(ProductNotExistException e)//检测商品是否存在
		{
			throw e;
		}
		catch(ProductUnionSetCanBeProductException	e)//已经设置了的组合套装不能做子商品
		{
			throw e;
		}
		catch(ProductUnionIsInSetException	e)//同一个组合套装下，商品不能重复
		{
			throw e;
		}
		catch(ProductCantBeSetException e)//已经作为组合的商品，不能变成组合，在其下创建商品
		{
			throw e;
		}
		catch(XMLDataLengthException e)
		{
			throw e;
		}
		catch(XMLDataErrorException e)
		{
			throw e;
		}
		catch(NetWorkException e)
		{
			throw e;
		}
		catch (Exception e) {
			throw new SystemException(e,"addProductUnionXML",log);

		}
			
	}
	/**
	 * 删除商品套装关系
	 */
	public  void delProductUnionXML(String xml,AdminLoginBean adminLoggerBean) 
	throws Exception{
		
		 try {
			String productUnions = getSampleNode(xml,"ProductUnions");
			 
			if(productUnions.equals("")||productUnions.length() == 0){
				throw new XMLDataErrorException();
			}
				long set_pid = Long.parseLong(getSampleNode(xml,"set_pid"));
				long pid = Long.parseLong(getSampleNode(xml,"pid"));
				productMgrZJ.delProductUnionSub(set_pid, pid, adminLoggerBean);
				
		} catch (Exception e) {
			throw new SystemException(e,"delProductUnionXML",log);
		}
	}
	/**
	 * 修改商品套装
	 */
	public void modProductUnionXML(String xml, AdminLoginBean adminLoggerBean)  
	throws Exception{
		
		try {
			String ProductUnions = getSampleNode(xml,"ProductUnions");
			
			if(ProductUnions.equals("")||ProductUnions.length() == 0){
				throw new XMLDataErrorException();
			}
			
				long set_pid = Long.parseLong(getSampleNode(xml, "set_pid"));
				long pid = Long.parseLong(getSampleNode(xml, "pid"));
				float quantity = Float.parseFloat(getSampleNode(xml, "quantity"));
				String p_name = getSampleNode(xml,"p_name").toUpperCase();
				String p_code = getSampleNode(xml,"p_code").toUpperCase();
				String unit_name = getSampleNode(xml,"unit_name");
				double unit_price = Double.parseDouble(getSampleNode(xml,"unit_price"));
				float weight = Float.parseFloat(getSampleNode(xml,"weight"));
				
				productMgrZJ.modUnionWithJqgridSub(p_name, set_pid, pid, quantity, unit_price, weight, unit_name, p_code, adminLoggerBean);
		}
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductUnionIsInSetException e)
		{
			throw e;
		}
		catch (ProductCantBeSetException e)
		{
			throw e;
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	/**
	 * 修改商品类型
	 */
	public void modProductCatalogXML(String xml,AdminLoginBean adminLoggerBean) 
	throws Exception{
		
			try {
				String productCatalog = getSampleNode(xml,"ProductCatalog");
				if(productCatalog.equals("")||productCatalog.length() == 0){
					throw new XMLDataErrorException();
				}
				long pc_id = Long.parseLong(getSampleNode(xml,"pc_id"));
				long catalog_id = Long.parseLong(getSampleNode(xml,"catalog_id"));
				
				productMgrZJ.modProductCatalogSub(pc_id,catalog_id);
				
			} catch (Exception e) {
				throw new SystemException(e,"modProductCatalogXML",log);
			}
	}
	/**
	 * 添加商品条码
	 */
	public int addProductCodeXML(String xml, AdminLoginBean adminLoggerBean)  
	throws Exception{
		try {
			String ProductCode = getSampleNode(xml,"ProductCode");
			if(ProductCode.equals("")||ProductCode.length() == 0){
				throw new XMLDataErrorException();
			}
			
			long pc_id = Long.parseLong(getSampleNode(xml,"pc_id"));
			String p_code = getSampleNode(xml, "p_code");
			int code_type = Integer.parseInt(getSampleNode(xml,"code_type"));
			
			int flag = 1;
			if(CodeTypeKey.Main == code_type)
			{
				flag = addProductCodeForCheck(pc_id+"", p_code,CodeTypeKey.Main);
			}
			if(1 == flag)
			{
				productCodeMgrZJ.addProductCodeSub(pc_id, p_code.toUpperCase(), code_type, adminLoggerBean);
			}
			return flag;
			
		}catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) {
			throw new SystemException(e,"modProductCatalogXML",log);
		}
	}
	
	/**
	 * 验证商品条码
	 * 1可以使用；2不可使用
	 */
	public int addProductCodeXMLForCheck(String xml, AdminLoginBean adminLoggerBean) throws Exception
	{
		try 
		{
			int flag = 1;
			String ProductCode = getSampleNode(xml,"ProductCode");
			String pc_id = getSampleNode(xml, "pc_id");
			if(ProductCode.equals("")||ProductCode.length() == 0){
				throw new XMLDataErrorException();
			}
			String p_code = getSampleNode(xml, "p_code");
			int code_type = Integer.parseInt(getSampleNode(xml,"code_type"));
			flag = addProductCodeForCheck(pc_id, p_code, code_type);
			return flag;
			
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addProductCodeXMLForCheck",log);
		}
	}
	
	/**
	 * 验证商品条码
	 * 1可以使用；2不可使用
	 */
	public int addProductCodeForCheck(String pc_id_str, String p_code, int code_type) throws Exception
	{
		try 
		{
			long pc_id = 0L;
			if(null != pc_id_str && "null".equals(pc_id_str) && "".equals(pc_id_str) && "0".equals(pc_id_str))
			{
				pc_id = Long.parseLong(pc_id_str);
			}
			int flag = 1;
			DBRow[] rows = productMgr.getProductCodeByTypeAndCode(code_type, p_code.toUpperCase()); 
			if(rows.length > 0)
			{
				if(0 == pc_id)
				{
					flag = 2;
				}
				else
				{
					Set<String> set = new HashSet<String>();
					set.add(pc_id_str);
					boolean isSameId = true;
					for (int i = 0; i < rows.length; i++) 
					{
						if(set.add(rows[i].getString("pc_id")))
						{
							isSameId = false;
							break;
						}
					}
					if(isSameId)
					{
						flag = 1;
					}
					else
					{
						flag = 2;
					}
				}
				
			}
			else
			{
				flag = 1;
			}
			return flag;
			
		}
		catch(ProductCodeIsExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addProductCodeForCheck",log);
		}
	}
	
	/**
	 * 上传商品图片
	 */
	public void handleUploadProductFile(String file_with_id, String file_with_type,
			String file_with_class, String upload_adid,String upload_time, String path , List<FileItem> fileItems) throws Exception {
		 try{
			 for(FileItem item : fileItems){
				 if(!item.isFormField()){
					 StringBuffer filePath = new StringBuffer();
					 filePath.append(Environment.getHome()).append("upload/").append(path).append("/");
					 File file = new File(filePath.toString(), item.getName());
					 if(file.exists()){file.delete();}
					 item.write(file);
					 
					 DBRow fileRow = new DBRow();
					 fileRow.add("pc_id", file_with_id);
					 fileRow.add("file_name", item.getName());
					 fileRow.add("file_with_id", file_with_id);
					 fileRow.add("file_with_type", file_with_type);
					 fileRow.add("product_file_type", file_with_class.trim().length() < 1 ? 0 :Integer.parseInt(file_with_class.trim()));
					 fileRow.add("upload_adid", upload_adid);
					 fileRow.add("upload_time", upload_time);
					 floorTransportMgrZr.addProductFile(fileRow);
				 }
			 }
			 
		 }catch (Exception e) {
			throw new SystemException(e,"handleUploadFile",log);
		}
	}
	 //删除商品图片
	public void delProductFile(String xml)throws Exception{
		try {

			long pf_id = Long.parseLong(getSampleNode(xml, "pf_id"));
			
			floorTransportMgrZr.deleteFileBy(pf_id, "product_file", "pf_id");
				
		} 
		catch (Exception e) {
			throw new SystemException(e,"delProductFile",log);
		}
		
	}

	//----------------------------------------------------------------------------
	/**
	 * 获得节点数据（只适合唯一名称节点）
	 * @return
	 */
	public String getSampleNode(String xml,String name)
		throws Exception
	{
		try 
		{
			String xmlSplit1[] = xml.split("<"+name+">");
			String xmlSplit2[] = xmlSplit1[1].split("</"+name+">");

			return(xmlSplit2[0]);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			throw new NetWorkException();
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getSampleNode",log);
		}
	}
	
	
	/**
	 * 加密算法
	 * @param str
	 * @return
	 */
	private  String HashBase64(String str)throws Exception
	{
		  try 
		  {
			String ret="";
			  try 
			  {
				    //Hash算法
				   MessageDigest sha = MessageDigest.getInstance("SHA-1");
				   sha.update(str.getBytes());
				   ret= new String(new Base64().encode(sha.digest()));
			  }
			  catch (Exception e) 
			  {
				  throw e;
			  }
			  return ret;
		  } 
		  catch (Exception e) 
		  {
			throw new SystemException(e,"HashBase64",log);
		  }
	}


	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}
	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}
	public void setProductCodeMgrZJ(ProductCodeMgrIFaceZJ productCodeMgrZJ) {
		this.productCodeMgrZJ = productCodeMgrZJ;
	}
	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}

	public void setFloorProductCodeMgrZJ(FloorProductCodeMgrZJ floorProductCodeMgrZJ) {
		this.floorProductCodeMgrZJ = floorProductCodeMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorProductLogsMgrZJ(FloorProductLogsMgrZJ floorProductLogsMgrZJ) {
		this.floorProductLogsMgrZJ = floorProductLogsMgrZJ;
	}
	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}
	
	
}
