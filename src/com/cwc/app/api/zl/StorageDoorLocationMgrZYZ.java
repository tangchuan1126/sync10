package com.cwc.app.api.zl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zl.FloorStorageDoorLocationMgrZYZ;
import com.cwc.app.iface.zl.StorageDoorLocationIFaceZYZ;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class StorageDoorLocationMgrZYZ implements StorageDoorLocationIFaceZYZ{
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorStorageDoorLocationMgrZYZ floorStorageDoorLocationZYZ;
	

	public void setFloorStorageDoorLocationZYZ(
			FloorStorageDoorLocationMgrZYZ floorStorageDoorLocationZYZ) {
		this.floorStorageDoorLocationZYZ = floorStorageDoorLocationZYZ;
	}


	/**
	 * 获取所有卸货门
	 */
	public DBRow[] getAllStorageDoor(PageCtrl pc) throws Exception {
		
		try {
			return floorStorageDoorLocationZYZ.getAllStorageDoor(pc);
			
		} catch (Exception e) {
			throw new SystemException(e,"getAllContainer",log);
		}
		
	}
	/**
	 * 卸货门高级查询
	 */
	public DBRow[] getSearchStorageDoor(String doorId, long psId, PageCtrl pc)
			throws Exception {
		
		    try {
				return floorStorageDoorLocationZYZ.getSearchStorageDoor(doorId,psId,pc);
				
			} catch (Exception e) {
				throw new SystemException(e,"getSearchStorageDoor",log);
			}
	}
	/**
	 * 获取某个装卸门信息
	 */
	public DBRow getDetailStorageDoor(long sdId) throws Exception {

		try {
			return floorStorageDoorLocationZYZ.getDetailStorageDoor(sdId);
			
		} catch (Exception e) {
			throw new SystemException(e,"getDetailStorageDoor",log);
		}
	}
	/**
	 * 增加卸货门
	 */
	public long addStorageDoor(HttpServletRequest request) throws Exception {
		
			try {
				String doorId = StringUtil.getString(request,"doorId");
				long psId = StringUtil.getLong(request,"ps_id");
				DBRow doorRow = new DBRow();
				doorRow.add("doorId", doorId);
				doorRow.add("ps_id", psId);
				return floorStorageDoorLocationZYZ.addStorageDoor(doorRow);
				
			} catch (Exception e) {
				throw new SystemException(e,"addStorageDoor",log);
			}
	}
	/**
	 * 修改装卸门
	 */
	public void modStorageDoor(HttpServletRequest request) throws Exception {

			try {
				long sdId = StringUtil.getLong(request,"sdId");
				String doorId = StringUtil.getString(request,"doorId");
				long psId = StringUtil.getLong(request,"ps_id");
				DBRow drow = new DBRow();
				drow.add("doorId", doorId);
				drow.add("ps_id", psId);
				floorStorageDoorLocationZYZ.modStorageDoor(sdId,drow);
				
			} catch (Exception e) {
				throw new SystemException(e,"modStorageDoor",log);
			}
		
	}
	/**
	 * 删除装卸门
	 */
	public void deleteStorageDoor(long sdId) throws Exception {
			
		try {
			floorStorageDoorLocationZYZ.deleteStorageDoor(sdId);
			
		} catch (Exception e) {
			throw new SystemException(e,"deleteLoadUnloadLocation",log);
		}
		
	}
	/**
	 * 获取所有装卸位置
	 */
	public DBRow[] getAllLoadUnloadLocation(PageCtrl pc) throws Exception {

			try {
				return floorStorageDoorLocationZYZ.getAllLoadUnloadLocation(pc);
				
			} catch (Exception e) {
				throw new SystemException(e,"getAllLoadUnloadLocation",log);
			}
	}
	/**
	 * 高级查询
	 */
	public DBRow[] getSearchLoadUnloadLocation(String locationName, long pscId,
			PageCtrl pc) throws Exception {
		
			try {
				return floorStorageDoorLocationZYZ.getSearchLoadUnloadLocation(locationName,pscId,pc);
				
			} catch (Exception e) {
				throw new SystemException(e,"getSearchLoadUnloadLocation",log);
			}
	}

	/**
	 * 获取某个装卸位置
	 */
	@Override
	public DBRow getDetailLoadUnloadLocation(long locationId) throws Exception {

			try {
				return floorStorageDoorLocationZYZ.getDetailLoadUnloadLocation(locationId);
				
			} catch (Exception e) {
				throw new SystemException(e,"getDetailLoadUnloadLocation",log);
			}
	}
	

	/**
	 * 增加装卸位置
	 */
	public long addLoadUnloadLoaction(HttpServletRequest request)
			throws Exception {
		
		try {
			String locationName = StringUtil.getString(request,"location_name");
			long pscId = StringUtil.getLong(request,"psc_id");
			
			DBRow rows = new DBRow();
			rows.add("location_name", locationName);
			rows.add("psc_id", pscId);
			return floorStorageDoorLocationZYZ.addLoadUnloadLocation(rows);
			
		} catch (Exception e) {
			throw new SystemException(e,"addLoadUnloadLoaction",log);
		}
	}

	/**
	 * 修改装卸位置
	 */
	public void modLoadUnloadLoaction(HttpServletRequest request)
			throws Exception {
			
			try {
				long locationId = StringUtil.getLong(request,"locationId");
				String locationName = StringUtil.getString(request,"location_name");
				long pscId = StringUtil.getLong(request,"psc_id");
				
				DBRow rows = new DBRow();
				rows.add("location_name", locationName);
				rows.add("psc_id", pscId);
				floorStorageDoorLocationZYZ.modLoadUnloadLocation(locationId,rows);
				
			} catch (Exception e) {
				throw new SystemException(e,"modLoadUnloadLoaction",log);
			}
		
	}

	/**
	 * 删除装卸位置
	 */
	public void deleteLoadUnloadLocation(long locationId) throws Exception {
			
		try {
			floorStorageDoorLocationZYZ.deleteLoadUnloadLocation(locationId);
			
		} catch (Exception e) {
			throw new SystemException(e,"deleteLoadUnloadLocation",log);
		}
		
	}

	/**
	 * 根据仓库ID获取装卸门信息
	 */
	public DBRow[] getStorageDoorByPsid(long psid) throws Exception {
		
		try {
			return floorStorageDoorLocationZYZ.getStorageDoorByPsid(psid);
			
		} catch (Exception e) {
			throw new SystemException(e,"getStorageDoorByPsid",log);
		}
	}

	/**
	 * 根据仓库ID获取装卸位置信息
	 */
	public DBRow[] getLoadUnloadLocationByPsid(long psid) throws Exception {
			
			try {
				return floorStorageDoorLocationZYZ.getLoadUnloadLocationByPsid(psid);
				
			} catch (Exception e) {
				throw new SystemException(e,"getLoadUnloadLocationByPsid",log);
			}
		}
}
