package com.cwc.app.api.zl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.app.exception.LPType.LPTypeHasExistException;
import com.cwc.app.floor.api.zl.FloorContainerMgrZYZ;
import com.cwc.app.iface.zl.ContainerMgrIFaceZYZ;
import com.cwc.app.iface.zr.ContainerMgrIfaceZr;
import com.cwc.app.key.ContainerHasSnTypeKey;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.ImportContainerErrorKey;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ContainerMgrZYZ implements ContainerMgrIFaceZYZ{
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorContainerMgrZYZ floorContainerMgrZYZ;
	private ContainerMgrIfaceZr containerMgrZr ;
	
	/**
	 * 查询所有容器托盘
	 */
	public DBRow[] getAllContainer(PageCtrl pc) throws Exception {
		
		try {
			return floorContainerMgrZYZ.getAllContainer(pc);
		} catch (Exception e) {
			throw new SystemException(e,"getAllContainer",log);
		}
	}
	
	/**
	 * 查询一个容器的详细信息
	 */
	public DBRow getDetailContainer(long containerId) throws Exception {
		
		try {
			return  floorContainerMgrZYZ.getDetailContainer(containerId);
		} catch (Exception e) {
			
			throw new SystemException(e,"getDetailContainer",log);
		}
	}
	/**
	 * 高级查询
	 */
	public DBRow[] getSearchContainer(String key, long type,int container_type ,PageCtrl pc)throws Exception{
		
		try {
			return floorContainerMgrZYZ.getSearchContainer(key,type,container_type,pc);
			
		} catch (Exception e) {
			throw new SystemException(e,"getSearchContainer",log);
		}
	}
	
	/**
	 * 添加容器托盘信息
	 */
	public long addContainer(HttpServletRequest request) 
		throws Exception 
	{
		  try 
		  {
			  String container = StringUtil.getString(request,"container");
			  String hardwareId = StringUtil.getString(request,"hardwareId");
			  int container_type = StringUtil.getInt(request, "container_type");
 			  long typeId = StringUtil.getLong(request,"typeId");
			  
			  DBRow row = new DBRow();
			  row.add("container", container);
			  row.add("hardwareId", hardwareId);
			  row.add("type_id", typeId);
			  row.add("container_type", container_type);
			  row.add("is_has_sn", ContainerHasSnTypeKey.NOSN);
			  row.add("is_full", ContainerProductState.EMPTY);
			 DBRow insertRow = containerMgrZr.addContainer(row);
			 return  insertRow != null ? insertRow.get("con_id", 0l) : 0l;
		  } 
		  catch (Exception e) 
		  {
			throw new SystemException(e,"addContainer",log);
		  }
	}
	
	/**
	 * 修改容器信息
	 */
	public void modContainer(HttpServletRequest request) throws Exception {
		
		try {
			  long containerId = StringUtil.getLong(request,"containerId");
			 String container = StringUtil.getString(request,"container");
			  String hardwareId = StringUtil.getString(request,"hardwareId");
			  long typeId = StringUtil.getLong(request,"typeId");
			  
			  DBRow containerRow = new DBRow();
			  containerRow.add("container", container);
			  containerRow.add("hardwareId", hardwareId);
			  containerRow.add("type_id", typeId);
			  
			  floorContainerMgrZYZ.modContainer(containerId,containerRow);
			  
			
		} catch (Exception e) {
			
			throw new SystemException(e,"ModContainer",log);
		}
	}
	/**
	 * 删除容器托盘信息
	 */
	public void deleteContainer(long containerId) throws Exception {
		
		try {
			floorContainerMgrZYZ.deleteContainer(containerId);
		} catch (Exception e) {
			throw new SystemException(e,"deleteContainer",log);
		}
		
	}

	/**
	 * 查询所有容器托盘类型
	 */
	public DBRow[] getAllContainerType(PageCtrl pc, String searchKey) throws Exception {
		try {
			return floorContainerMgrZYZ.getAllContainerType(pc, searchKey);
		} catch (Exception e) {
			
			throw new SystemException(e,"getAllContainerType",log);
		}
	}
	
	public DBRow[] getClpContainerType() throws Exception{
		
		try {
			
			return floorContainerMgrZYZ.getClpContainerType();
			
		} catch (Exception e) {
			
			throw new SystemException(e,"getClpContainerType",log);
		}
	}
	
	public DBRow[] getTlpContainerType() throws Exception{
		
		try {
			return floorContainerMgrZYZ.getTlpContainerType();
			
		} catch (Exception e) {
			
			throw new SystemException(e,"getTlpContainerType",log);
		}
	}
	
	/**
	 * 查询单个容器托盘类型
	 * @param typeId
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailContainerType(long typeId) throws Exception {
		
		try {
			return floorContainerMgrZYZ.getDetailContainerType(typeId);
		} catch (Exception e) {
			
			throw new SystemException(e,"getDetailContainerType",log);
		}
	}
	/**
	 * 修改容器托盘类型
	 */
	public void modContainerType(HttpServletRequest request) throws Exception {
		
		try {
			int container_type = StringUtil.getInt(request, "container_type");
			long typeId = StringUtil.getLong(request,"typeId");
			String typeName = StringUtil.getString(request,"typeName");
			float length = StringUtil.getFloat(request,"length");
			float width = StringUtil.getFloat(request,"width");
			float height = StringUtil.getFloat(request, "height");
			float weight = StringUtil.getFloat(request, "weight");
			float max_load = StringUtil.getFloat(request,"max_load");
			float max_height = StringUtil.getFloat(request,"max_height");
			
			DBRow rows = new DBRow();
			rows.add("type_name", typeName);
			rows.add("length", length);
			rows.add("width", width);
			rows.add("height", height);
			rows.add("weight", weight);
			rows.add("max_load", max_load);
			rows.add("max_height", max_height);
			rows.add("container_type", container_type);
			
			floorContainerMgrZYZ.modContainerType(typeId,rows);
		} catch (Exception e) {
			
			throw new SystemException(e,"modContainerType",log);
		}
	}
	/**
	 * 增加容器托盘类型
	 * @return
	 * @throws Exception
	 */
	public long addContainerType(HttpServletRequest request) throws Exception {
		
		try {
			
			//int containerType = StringUtil.getInt(request, "container_type");
			
			String typeName = StringUtil.getString(request,"typeName");
			float length = StringUtil.getFloat(request,"length");
			float width = StringUtil.getFloat(request,"width");
			float height = StringUtil.getFloat(request, "height");
			float weight = StringUtil.getFloat(request, "weight");
			float max_load = StringUtil.getFloat(request,"max_load");
			float max_height = StringUtil.getFloat(request,"max_height");
			length = MoneyUtil.round(length, 2);
			width = MoneyUtil.round(width, 2);
			height = MoneyUtil.round(height, 2);
			weight = MoneyUtil.round(weight, 2);
			max_load = MoneyUtil.round(max_load, 2);
			max_height = MoneyUtil.round(max_height, 2);
			int length_uom = StringUtil.getInt(request,"length_uom");
			int weight_uom = StringUtil.getInt(request,"weight_uom");
			
			DBRow isExistContainerType = floorContainerMgrZYZ.getContainerTypeIdByName(typeName);
			
			if (isExistContainerType != null) {
				
				return -1;
			}
			
			DBRow addRow = new DBRow();
			addRow.add("type_name", typeName);
			addRow.add("length", length);
			addRow.add("width", width);
			addRow.add("height", height);
			addRow.add("weight", weight);
			addRow.add("max_load", max_load);
			addRow.add("max_height", max_height);
			//addRow.add("container_type",containerType );
			addRow.add("length_uom", length_uom);
			addRow.add("weight_uom",weight_uom );
			
			return floorContainerMgrZYZ.addContainerType(addRow);
			
		} catch (Exception e) {
			throw new SystemException(e,"addContainerType",log);
		}
	}
	/**
	 * 删除容器托盘类型
	 * @param typeId
	 * @return
	 * @throws Exception
	 */
	public long deleteContainerType(long typeId, int container_type) throws Exception {
		
		try {
			//删除之前要先检查该分类下有没有容器托盘
			switch(container_type)
			{
			/*
				case ContainerTypeKey.ILP:
					if(checkIlpBasicCanDel(typeId, container_type))
						return floorContainerMgrZYZ.deleteContainerType(typeId, container_type);
					break;
				case ContainerTypeKey.BLP:
					if(checkBlpBasicCanDel(typeId, container_type))
						return floorContainerMgrZYZ.deleteContainerType(typeId, container_type);
					break;*///去掉ILP和BLP
				case ContainerTypeKey.CLP:
					if(checkClpBasicCanDel(typeId, container_type))
						return floorContainerMgrZYZ.deleteContainerType(typeId, container_type);
					break;
				case ContainerTypeKey.TLP:
					if(checkTlpBasicCanDel(typeId, container_type))
						return floorContainerMgrZYZ.deleteContainerType(typeId, container_type);
					break;
				default:
					if(checkLPBasicCanDel(typeId) && checkTlpBasicCanDel(typeId,container_type))
						return floorContainerMgrZYZ.deleteContainerType(typeId, container_type);
					break;
			}
			return 0;
		} catch (Exception e) {
			throw new SystemException(e,"deleteContainerType",log);
		}
		
	}
	
	public boolean checkLPBasicCanDel(long type_id) throws Exception{
		try
		{
			DBRow[] lpContainer = floorContainerMgrZYZ.getClpContainerByType(type_id);
			if(lpContainer.length > 0)
			{
				return false;
			}
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"checkIllBasicCanDel",log);
		}
	}
	
	public boolean checkIlpBasicCanDel(long type_id, int container_type) throws Exception
	{
		try
		{
			DBRow[] IlpContainer = floorContainerMgrZYZ.getIlpContainerByType(type_id);
			if(IlpContainer.length > 0)
			{
				return false;
			}
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"checkIllBasicCanDel",log);
		}
	}
	
	public boolean checkBlpBasicCanDel(long type_id, int container_type) throws Exception
	{
		try
		{
			DBRow[] BlpContainer = floorContainerMgrZYZ.getBlpContainerByType(type_id);
			if(BlpContainer.length > 0)
			{
				return false;
			}
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"checkBlpBasicCanDel",log);
		}
	}
	
	public boolean checkClpBasicCanDel(long type_id, int container_type) throws Exception
	{
		try
		{
			DBRow[] ClpContainer = floorContainerMgrZYZ.getClpContainerByType(type_id);
			if(ClpContainer.length > 0)
			{
				return false;
			}
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"checkClpBasicCanDel",log);
		}
	}
	
	public boolean checkTlpBasicCanDel(long type_id, int container_type) throws Exception
	{
		try
		{
			DBRow[] TlpContainer = floorContainerMgrZYZ.getTlpContainerByType(type_id, container_type);
			if(TlpContainer.length > 0)
			{
				return false;
			}
			return true;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"checkTlpBasicCanDel",log);
		}
	}
	/**
	 * 导出容器
	 */
	public String exportContainer(HttpServletRequest request) throws Exception {
		
			try {
				
				String search_key = StringUtil.getString(request, "search_key");
				
				DBRow[] exportContainer = floorContainerMgrZYZ.getAllContainerType(null,search_key);
				long[] containers = new long[exportContainer.length];
				
				if(exportContainer.length > 0){
					
					XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(Environment.getHome()+"/administrator/container/PackagingType.xlsm"));
					XSSFSheet sheet = wb.getSheetAt(0);
					XSSFRow row = sheet.getRow(0);
					
					for(int i = 0 ; i< exportContainer.length ; i++){
						row = sheet.createRow((int)i+1);
						sheet.setDefaultColumnWidth(30);
						LengthUOMKey lengthUOMKey = new LengthUOMKey();
						WeightUOMKey weightUOMKey = new WeightUOMKey();
						
						DBRow containerType = exportContainer[i];
						//设置excel列的值
						containers[i] = containerType.get("con_id",0l);
						//row.getCell(2).setCellStyle(style);
						row.createCell(1).setCellValue(containerType.getString("type_name"));
						row.createCell(2).setCellValue(containerType.getString("length"));
						row.createCell(3).setCellValue(containerType.getString("width"));
						row.createCell(4).setCellValue(containerType.getString("height"));
						row.createCell(5).setCellValue(containerType.getString("weight"));
						row.createCell(6).setCellValue(containerType.getString("max_load"));
						row.createCell(7).setCellValue(lengthUOMKey.getLengthUOMKey(containerType.get("length_uom",0)));//在excel中是隐藏的H
						row.createCell(8).setCellValue(weightUOMKey.getWeightUOMKey(containerType.get("weight_uom",0)));//在excel中是隐藏的I
						row.createCell(9).setCellValue(containerType.getString("max_height"));
						
					}
					//写文件  
					String path = "upl_excel_tmp/export_basic_type_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xlsm";
					FileOutputStream os= new FileOutputStream(Environment.getHome()+path);
					wb.write(os);  
					os.close();  
					return (path);
				}else
				{
					return "0";
				}
				
			} catch (Exception e) {
				throw new SystemException(e,"exportContainer",log);
			}
	}

	/**
	 * 导入容器
	 */
	public HashMap<String, DBRow[]> excelShow(String filename, String type)throws Exception {
		
		try 
		{
		
			String path = "";
			InputStream is;
			Workbook wrb;
			HashMap<String,DBRow[]> resultMap = new HashMap<String, DBRow[]>();//返回值
			if (type.equals("show")) 
			{
				path = Environment.getHome() + "upl_imags_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				
				DBRow[] containers = containerData(wrb);
				DBRow[] errorContainers = checkContainer(containers);//检查容器
				resultMap.put("errorContainers",errorContainers);
			}
			else if(type.equals("data"))
			{
				path = Environment.getHome() + "upl_imags_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				containerData(wrb);
				
				DBRow[] Containers = containerData(wrb);//DBRow[]
				
				resultMap.put("Containers",Containers);
			}
			
			return resultMap;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"excelShow",log);
		}
	}
	/**
	 * 检查容器信息
	 * @param containers
	 * @return
	 * @throws Exception
	 */
	private DBRow[] checkContainer(DBRow[] containers) throws Exception {
	 try 
	  {
		ArrayList<DBRow> errorContainer = new ArrayList<DBRow>();
		for (int i = 0; i < containers.length; i++) 
		{
			boolean checkResult = false;//检查检查结果，true表示有错，false表示没错
			StringBuffer errorMessage = new StringBuffer("");//错误信息
			DBRow rows = containers[i];
			if(rows.getString("container").trim().equals(""))   //检查容器编号是否为空
			{
				checkResult = true;
				errorMessage.append(ImportContainerErrorKey.CONTAINERID+",");
				
			}
			if(rows.getString("hardwareId").trim().equals(""))   //检查硬件号是否为空
			{
				checkResult = true;
				errorMessage.append(ImportContainerErrorKey.HARDWOARID+",");
				
			}
			if(rows.get("type_id",0l)==0)   //检查容器类型是否存在
			{
				checkResult = true;
				errorMessage.append(ImportContainerErrorKey.CONTAINERTYPEERROR+",");
				
			}
			if (checkResult)//一条记录检查有问题
			{
				rows.add("errorMessage", errorMessage.toString());
				rows.add("errorContainerRow",i+2);
				errorContainer.add(rows);
			}
		}
		return (errorContainer.toArray(new DBRow[0]));
	} 
	catch (Exception e) 
	{
		throw new SystemException(e,"checkContainer(rows)",log);
	}
	
}
	
	/**
	 * 转化成DBRow类型
	 * @param wrb
	 * @return
	 * @throws Exception
	 */
	public DBRow[] containerData(Workbook wrb) throws Exception
	{
		try {
			HashMap containerFiled = new HashMap();
			containerFiled.put(0, "container");
			containerFiled.put(1, "hardwareId");
			containerFiled.put(2, "type_id");
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			Sheet rs = wrb.getSheet(0);
			int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
			  DBRow drow;
			  for(int i=1;i<rsRows;i++)
			  {
			   DBRow pro = new DBRow();

			   
			   boolean result = false;//判断是否纯空行数据
			   for (int j=0;j<rsColumns;j++)
			   {
				   	String content = rs.getCell(j,i).getContents().trim();
				   	if(j==2){	
			     		if(!content.equals("")&&content!=null){		   				   		
			     			drow = floorContainerMgrZYZ.getContainerTypeIdByName(content);
			     			if(drow!=null){
			     				content = drow.getString("type_id");
			     			}else{
			     				content="0";
			     			}
			     		}else{
			     			content = "0";
			     		}
				   	}
				 
					pro.add(containerFiled.get(j).toString(),content);  //转换成DBRow数组输出,去掉空格
			   
			   ArrayList proName=pro.getFieldNames();//DBRow 字段名
			   for(int p=0;p<proName.size();p++)
			    {
			    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
			    	{
			    		result = true;
			    	}
			    }
			   }
			   if(result)//不是纯空行就加入到DBRow
			   {
				   al.add(pro);   
			   }
			  }
			  return (al.toArray(new DBRow[0]));
			  
		} catch (Exception e) {
			throw new SystemException(e,"containerData",log);
		}
	}
	/**
	 * 上传的excel导出结果集
	 */
	public void uploadContainers(HttpServletRequest request) throws Exception {
		
		try {
			String tempfilename = StringUtil.getString(request,"tempfilename");
			HashMap<String,DBRow[]> excelDBRow = excelShow(tempfilename,"data");//excel导出结果集
			DBRow[] rows = excelDBRow.get("Containers");
			importContainers(rows);
			
		} catch (Exception e) {
			throw new SystemException(e,"uploadContainers",log);
		}
	}
	/**
	 * 保存上传的容器信息
	 * @param rows
	 * @throws Exception
	 */
	public void importContainers(DBRow[] rows)throws Exception{
		
			try {
				if(rows!=null){
					for(int i = 0;i < rows.length; i++){
						DBRow container = rows[i];
						if(container.getString("hardwareId").equals("")){
						    container.remove("container");
						}
						if(container.getString("hardwareId").equals("")){
							container.remove("hardwateId");
						}
						if(container.get("type_id",0l)==0){
							container.remove("type_id");
						}
						floorContainerMgrZYZ.addContainer(container);
					}
				}
			} catch (Exception e) {
				throw new SystemException(e,"importContainers",log);
			}
	}
	
	/**
	 * 通过容器分类查询容器基础容器类型
	 * @param containerType
	 * @return
	 * @throws Exception
	 * zyj
	 */
	public DBRow[] findContainerTypeByContainerType(int containerType) throws Exception
	{
		try
		{
			return floorContainerMgrZYZ.findContainerTypeByContainerType(containerType);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"findContainerTypeByContainerType",log);
		}
	}

	public void setFloorContainerMgrZYZ(FloorContainerMgrZYZ floorContainerMgrZYZ) {
		this.floorContainerMgrZYZ = floorContainerMgrZYZ;
	}

	public void setContainerMgrZr(ContainerMgrIfaceZr containerMgrZr) {
		this.containerMgrZr = containerMgrZr;
	}
	


}
