package com.cwc.app.api.tc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;






import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class PdfGenertor implements Callable{
	private int startIndex=0;
	private int pageSize=0;
	public HttpServletRequest request;
	private ArrayList<JSONObject>  taskList =null;
	private String generateIncludeWithoutSign = "false";
	public PdfGenertor(int startIndex,int pageSize,ArrayList<JSONObject>  taskList,HttpServletRequest request,String generateIncludeWithoutSign){
		this.startIndex = startIndex;
		this.pageSize = pageSize;
		this.taskList = taskList;
		this.request = request;
		this.generateIncludeWithoutSign = generateIncludeWithoutSign;
	}
	@Override
	public Object call() throws Exception {
		
		for(int i=startIndex;i<startIndex+pageSize;i++){
			if(i>=taskList.size()) break;
			JSONObject item= taskList.get(i);
			System.out.println("================["+i+"]==============");
			try{
				String ip="127.0.0.1";
				if(!this.request.getLocalAddr().equals("0:0:0:0:0:0:0:1")){
				  ip = this.request.getLocalAddr();
			    }
			    if(ip.indexOf(":")==-1){
				   ip= ip+":"+this.request.getLocalPort();
			    }
				String url="http://"+ip+"/Sync10/action/administrator/file_up/DeleteExistPrintedLabelAction.action";
				String params = "entry_id="+item.getString("entry_id")+"&detail_id="+item.getString("detail_id")+"&manual="+generateIncludeWithoutSign;
				SentGetRequest(url,params,this.request.getSession().getId());
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		// TODO Auto-generated method stub
		return null;
	}
	
	public static HashMap<String, Object> SendRequest(String url, String param,String sessionId) {
		String result = "";
		URL httpurl = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {
			httpurl = new URL(url);
			HttpURLConnection httpConn = (HttpURLConnection) httpurl
					.openConnection();
			httpConn.setRequestMethod("POST");
			httpConn.setRequestProperty("accept", "*/*");

			httpConn.setRequestProperty("Accept-Charset", "utf-8");
			httpConn.setRequestProperty("connection", "Keep-Alive");
			httpConn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			httpConn.setRequestProperty("contentType", "utf-8");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			PrintWriter out = new PrintWriter(httpConn.getOutputStream());
			//out.print(URLEncoder.encode(param+"", "UTF-8"));
			out.print(param);
			out.flush();
			out.close();
			
			Map hfs=httpConn.getHeaderFields();
			String cookieValue=httpConn.getHeaderField("Set-Cookie");
            System.out.println("cookie value:"+cookieValue);
            //String sessionId=cookieValue.substring(0, cookieValue.indexOf(";"));
            //sessionId = sessionId.split("\\=")[1];
            map.put("SessionId", sessionId);
            
			BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),"UTF-8"));
			String line="";
			while ((line = in.readLine()) != null) {
				result = result + line;
			}
			in.close();
			map.put("responseText", result);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return map;
	}
	
	public static String SentGetRequest(String url, String param, String sessionId)throws Exception{
		String result = "";
		
		
		HttpClient httpClient = new DefaultHttpClient();
		try {
			if(param!=null && !"".equals(param)){
				String spit = url.indexOf("?")==-1?"?":"&";
				url += spit+param;
			}
			HttpGet httpget = new HttpGet(url);
			httpget.setHeader("Cookie", "JSESSIONID=" + sessionId);
			//httpget.setHeader("Cookie", "JSESSIONID=D33475275E3F72C6C9C50551E263AFF7" );
			
			HttpResponse response = httpClient.execute(httpget);
			
			StatusLine statusLine = response.getStatusLine();
			int httpStatus=statusLine.getStatusCode();
			
			
			if (httpStatus == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				BufferedReader br = null;
				InputStream in = entity.getContent();
				try {
					br = new BufferedReader(new InputStreamReader(entity.getContent()));  
		            String line="";  
		            while ((line = br.readLine()) != null) {  
		                result += "\n" + line;  
		            }  
		            
				} finally {
					// 关闭低层流。
					in.close();
				}
				//把底层的流给关闭
				EntityUtils.consume(entity);
			}
			
		} catch (IOException e) {
		
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		return result;
	}
	
}