package com.cwc.app.api.tc;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.app.api.zr.FileMgrZr;
import com.cwc.db.DBRow;
import com.cwc.util.CompressFile;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;


public class FileMgrTc extends FileMgrZr {
	private static Logger log = Logger.getLogger("ACTION");
	protected String saveFileRoot= null;
	protected String uploadFileRoot= null;
	
	public void setSaveFileRoot(String saveFileRoot) {
		this.saveFileRoot = saveFileRoot;
	}
	
	public void setUploadFileRoot(String uploadFileRoot) {
		this.uploadFileRoot = uploadFileRoot;
	}
	
	public HttpServletResponse download(String path, HttpServletResponse response) {
        try {
            // path是指欲下载的文件的路径。
            File file = new File(path);
            // 取得文件名。
            String filename = file.getName();
            // 取得文件的后缀名。
            String ext = filename.substring(filename.lastIndexOf(".") + 1).toUpperCase();

            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return response;
    }
	public String createPackageByFileIds(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String fileIds= StringUtil.getString(request, "file_ids");
		String fileWithId= StringUtil.getString(request, "file_with_id");
		String targetFilePath="";
		UUID uuid = UUID.randomUUID();
		
    	String folder4ZipPath = uploadFileRoot+File.separator+uuid.toString()+File.separator;
    	FileUtil.createDirectory(folder4ZipPath);
    	if(fileIds!=null && !"".equals(fileIds)){
    		String[] arr = fileIds.split(",");
    		for(int i=0;i<arr.length;i++){
    			DBRow row = this.getFileByFileId(Long.parseLong(arr[i]));
    			if(row!=null){
    				String fileName = row.getString("file_name");
    				String filePath = row.getString("file_path");
    				String orgFileName = row.getString("original_file_name");
    				if(filePath.indexOf(".jpg")==-1 && filePath.indexOf(".JPG")==-1){
    					continue;
    				}
    				String folderName = getFolderPath(fileName, folder4ZipPath);
    				FileUtil.copyFileAndRename(this.saveFileRoot+File.separator+filePath, folderName, orgFileName);
    				
    			}
    			//String fileName = arr[i];
    			
    		}
    		targetFilePath = uploadFileRoot+File.separator+uuid.toString();
    		CompressFile.zipFolder(folder4ZipPath, targetFilePath);
        	FileUtil.delFolder(folder4ZipPath);
        	
        	File file = new File(targetFilePath+".zip");
        	String filename = file.getName();
        	
        	 // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(targetFilePath+".zip"));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            String downLoadFileName = filename;
            if(fileWithId!=null && !"".equals(fileWithId)){
            	downLoadFileName  = "photos_checkin_E"+fileWithId+".zip";
            }
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(downLoadFileName.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
    	}
    	FileUtil.delFile(targetFilePath+".zip");
    	/*String[] files = FileUtil.getFileList(photoFolderPath);
    	for(int i=0;i<files.length;i++){
    		String fileName = files[i];
    		if(fileName.indexOf(".jpg")>=0 || fileName.indexOf(".JPG")>=0){
    			String filePath = photoFolderPath+fileName;
    			String folderName = getFolderPath(fileName, folder4ZipPath);
    			FileUtil.copyFileAndRename(filePath, folderName, getCopy2Name(fileName));
    			System.out.println(files[i]);
    			
    		}
    	}
    	CompressFile.zipFolder(folder4ZipPath, rootPath+uuid.toString());
    	FileUtil.delFolder(folder4ZipPath);*/
		return targetFilePath;
	}
	
	
	

	public static String getFolderPath(String fileName,String rootPath){
	    	String folderPath="";
	    	
	    	Pattern p = Pattern.compile("\\D+_\\d");
			Matcher match = p.matcher(fileName);
			if (match.find()) {
				String type = match.group(0);
				type = type.substring(0, type.length()-2);
				folderPath = rootPath+type;
				File folder = new File(folderPath);
	    		if(!folder.exists()){
	    			FileUtil.createDirectory(folderPath);
	    		}
			}
	    	return folderPath;
	    }
	    
	    public static String getCopy2Name(String fileName){
	    	return "download_"+fileName;
	    }


	
}
