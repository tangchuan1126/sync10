package com.cwc.app.api.tc;


import com.cwc.app.api.CheckinActionType;
import com.cwc.app.api.EntryTicketCheck;
import com.cwc.app.api.Equipment;
import com.cwc.app.api.GateCheckoutInfo;
import com.cwc.app.api.Warehouse;
import com.cwc.app.api.zwb.CheckInMgrZwb;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.iface.checkin.PdfUtilIFace;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.startup.RabbitMQListener;
import com.cwc.util.StringUtil;
import com.lowagie.text.pdf.PdfWriter;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.drools.time.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.SerializationUtils;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.swing.*;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PdfUtil implements PdfUtilIFace {

    @Autowired
    public HttpServletRequest request;
    protected String sessionId = null;
    private FloorCheckInMgrZwb floorCheckInMgrZwb;

    private CheckInMgrZwb checkInMgrZwb;
    private boolean isManual = false;

    private String ip = "127.0.0.1";
    private static final String UPLOAD_URL="http://52.36.230.170/Sync10/_fileserv/upload"; 
    private static final String LOGIN_URL = "http://52.36.230.170:8000/Sync10/action/admin/AccountLoginMgrAction.action";
    private static final String FILE_DOWNLOAD_URL = "http://ltxmteam.iask.in:18080/viabaron/file-app/file-download/";
    //private static final String FILE_DOWNLOAD_URL = "http://localhost/turnbull/file-download/";
    private static final String FILE_SERVICE_URL = "/viabaron/file-app/file-download/";
    
    
    
    public static String login(String loginUrl, String acount, String password) throws IOException {
        String sessionId = null;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost method = new HttpPost(loginUrl);
        if (method != null) {

            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("account", acount));
                params.add(new BasicNameValuePair("pwd", password));
                method.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));

                // 设置编码
                HttpResponse response = httpClient.execute(method);
                Header[] headers = response.getHeaders("Set-Cookie");
                if (headers != null && headers.length > 1) {
                    Header header = headers[0];
                    String value = header.getValue();
                    Matcher m = Pattern.compile("JSESSIONID=(\\w+)").matcher(value);
                    if (m.find()) {
                        sessionId = m.group().split("=")[1];
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return sessionId;
    }

    public void setCheckInMgrZwb(CheckInMgrZwb checkInMgrZwb) {
        this.checkInMgrZwb = checkInMgrZwb;
    }

    public FloorCheckInMgrZwb getFloorCheckInMgrZwb() {
        return floorCheckInMgrZwb;
    }

    public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
        this.floorCheckInMgrZwb = floorCheckInMgrZwb;
    }
    
    
    public boolean deletedExistPrintedLabel(HttpServletRequest req)
        throws NumberFormatException, Exception {
        boolean message = false;

        Long entryId = StringUtil.getLong(req, "entry_id");

        DBRow[] rows = floorCheckInMgrZwb.getLoadNoByEntryId(entryId);
        if (rows != null && rows.length > 0) {
            for (int i = 0; i < rows.length; i++) {
                String loadNo = rows[i].getString("load_no");
                floorCheckInMgrZwb.deletePrintedLabelByLoad(entryId, loadNo,
                    null);
            }
        }
        message = true;
        return message;

    }

    public boolean deletedExistPrintedLabel(Long entryId, String loadNo,
                                            String customerId) throws NumberFormatException, Exception {
        boolean message = false;
        floorCheckInMgrZwb
            .deletePrintedLabelByLoad(entryId, loadNo, customerId);
        message = true;
        return message;

    }

    public boolean checkExist(String entryId) throws NumberFormatException,
        Exception {
        boolean exist = false;
        DBRow[] rows = floorCheckInMgrZwb.getLoadNoByEntryId(Long
            .parseLong(entryId));

        if (rows != null && rows.length > 0) {
            exist = true;
        }
        return exist;
    }

    public boolean checkSigned(String entryId, String loadNo,
                               HttpServletRequest req) throws NumberFormatException, Exception {
        boolean signed = false;
        try {
            DBRow[] rows = checkInMgrZwb.getFileIdByEntryId(
                Long.parseLong(entryId), req);
            if (rows != null) {
                for (int i = 0; i < rows.length; i++) {
                    String _loadNo = rows[i].get("number", "");
                    String signFlag = rows[i].get("signFlag", "");
                    if (_loadNo.equals(loadNo) && "success".equals(signFlag)) {
                        signed = true;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            // ex.printStackTrace();
        }
        return signed;
    }

    public int createPdf(String entryId, String detailId,
                         HttpServletRequest req, boolean isManual, String loadNo)
        throws Exception {
        if (!this.checkSigned(entryId, loadNo, req)) {
            System.out.println("======================ignore[" + entryId
                + "] without sign========================");
            return 0;
        }
        return this.createPdf(entryId, detailId, req, isManual);
    }

    public int createPdf(String entryId, String detailId,
                         HttpServletRequest req, boolean isManual) throws Exception {
        int flag = 0;

        this.request = req;
        this.sessionId = req.getSession().getId();
        this.isManual = isManual;
        if (!this.request.getLocalAddr().equals("0:0:0:0:0:0:0:1")) {
            this.ip = this.request.getLocalAddr();
        }
        if (ip.indexOf(":") == -1) {
            this.ip = this.ip + ":" + this.request.getLocalPort();
        }

        // 检查是否已经生成过了
        /*
		 * if(checkExist(entryId)){
		 * System.out.println("====================Pdf files of Entry ID["
		 * +entryId+"] have been created!!!!!!==========================");
		 * return -2; }
		 */
        System.out
            .println("======================start to create pdf========================");
        String url = "http://"
            + this.ip
            + "/Sync10/action/administrator/file_up/getPrintUrlByParam.action";
        String addCountSheetUrl = "http://"
            + this.ip
            + "/Sync10/action/administrator/file_up/addCountingSheetToPdf.action";
        String param = "entry_id=" + entryId + "&detail_id=" + detailId;
        int tryTimes = 0;
        String strJson = "";
        JSONArray masterbolurl = null;
        JSONArray bolurl = null;

        while (masterbolurl == null && tryTimes <= 5) {
            try {
                strJson = SentGetRequest(url, param, sessionId);
                JSONObject json = JSONObject.fromObject(strJson);

                masterbolurl = json.getJSONArray("masterbolurl");
                bolurl = json.getJSONArray("bolurl");
                for (int i = 0; i < masterbolurl.size(); i++) {
                    String strUrl = masterbolurl.getJSONObject(i).getString(
                        "printurl");
                    createBolPdfByUrl(strUrl, entryId, "masterBol_", false,
                        sessionId);
                }

                for (int i = 0; i < bolurl.size(); i++) {
                    String strUrl = bolurl.getJSONObject(i).getString(
                        "printurl");
                    createBolPdfByUrl(strUrl, entryId, "bol_", true, sessionId);
                }

                HashMap<String, String> map = new HashMap<String, String>();
                map.put("entry_id", entryId);
                map.put("detail_id", detailId);
                PdfUtil.SentPostRequest(addCountSheetUrl, map, sessionId);
                flag = 1;
            } catch (Exception ex) {
                System.out.println("---------------Try " + tryTimes
                    + " times-----------------");
                if (tryTimes >= 5) {
                    ex.printStackTrace();
                }

            }
            tryTimes++;
        }

        System.out.println("======================create  pdf  finish========================");
        return flag;
    }

    public void createBolPdfByUrl(String url, String entryId, String prefix,
                                  boolean withDnname, String sessionId) throws NumberFormatException,
        Exception {
        String html2PdfUrl = "http://"
            + this.ip
            + "/Sync10/action/administrator/file_up/Html2PdfUploadAction.action";
        String[] arr = url.split("\\?");
        String printUrl = "http://" + this.ip + arr[0];
        HashMap<String, String> params = ParseUrlParams(arr[1]);
        boolean successed = false;
        int tryTimes = 0;
        while (!successed && tryTimes <= 5) {
            try {
                String strHtml = SentPostRequest(printUrl, params, sessionId);
                String[] arr2 = null;
                Pattern p = Pattern
                    .compile(
                        "(<!--\\s+loop body start\\s+-->(.*)?<!--\\s+loop body end\\s+-->)",
                        Pattern.DOTALL);
                Matcher m = p.matcher(strHtml);
                if (m.find()) {
                    successed = true;
                    strHtml = m.group().toString();
                    arr2 = strHtml.split("<!--\\s+loop body start\\s+-->");
                    for (int i = 1; i < arr2.length; i++) {
                        String str = arr2[i];
                        String dnName = "";
                        String type = prefix + i;
                        String companyId = GetInputValue(str, "companyId");
                        String customerId = GetInputValue(str, "customerId");
                        String loadNo = GetInputValue(str, "loadNo");
                        // 检查是否签名过
                        if (!isManual
                            && !checkSigned(entryId, loadNo, this.request)) {
                            System.out
                                .println("====================Entry ID ["
                                    + entryId
                                    + "] LoadNo["
                                    + loadNo
                                    + "] without signature!!!!!!==========================");
                            continue;
                        }
                        Pattern p1 = Pattern.compile("<input(.*?)(/?>)");
                        str = str.replaceAll("<input(.*?)(/?>)",
                            "<input $1 ></input>");
                        str = str.replaceAll("<img(.*?)(/?>)",
                            "<img $1 ></img>");
                        str = str.replaceAll("<br>", "<br></br>");
                        str = str.replaceAll("&nbsp", "&#160");
                        str = str.replaceAll("\\s+&\\s+", "&#38;");
                        str = str.replaceAll("<tbody>", "");
                        str = str.replaceAll("</tbody>", "");
                        str = str.replaceAll("<script>", "");
                        str = str.replaceAll("</script>", "");
                        String header = "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
                            + " <head>"
                            + " <meta name=\"generator\" content=\"HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39\" />"
                            + " <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
                            + "   <style type=\"text/css\">"
                            + "   @page:left { margin: 5mm }"
                            + "   @page{ size: 210mm 297mm;}"
                            + "   @page:right { margin: 5mm }"
                            + " </style>"
                            + " </head>"
                            + " <body>"
                            + " <div style=\"border:1px #ffffff solid; width:195mm;\">";
                        String strPost = header + str + "</div></body></html>";
                        String paramUrl = "entryId=" + entryId + "&type="
                            + type + "&companyId=" + companyId
                            + "&customerId=" + customerId + "&loadNo="
                            + loadNo;
                        if (withDnname) {
                            dnName = GetInputValue(str, "dn");
                            paramUrl += "&dn=" + dnName;
                            // DBRow[] rows =
                            // floorCheckInMgrZwb.selectCountPrintedLabel(Long.parseLong(entryId),
                            // type, dnName, customerId);
                        }

                        HashMap<String, String> param1 = ParseUrlParams(paramUrl);
                        param1.put("isManual", isManual + "");
                        param1.put("template", strPost);

                        String ret = SentPostRequest(html2PdfUrl, param1,
                            sessionId);
                    }
                } else {
                    System.out.println("---------------Try " + tryTimes
                        + " times-----------------");
                }
            } catch (Exception ex) {
                System.out.println("---------------Try " + tryTimes
                    + " times-----------------");
                if (tryTimes >= 5) {
                    ex.printStackTrace();
                }
            }
            tryTimes++;
        }

    }

    public static String GetInputValue(String strHtml, String els) {
        String value = "";
        Pattern p = Pattern.compile("(.*)?id=\"" + els + "\"(.*)?");
        Matcher m = p.matcher(strHtml);
        if (m.find()) {
            String result = m.group().toString();
            Pattern p1 = Pattern.compile("(value=\")(.*?)(\")");
            Matcher m2 = p1.matcher(result);
            if (m2.find()) {
                value = m2.group(2).toString();
            }
        }
        return value;
    }

    public static HashMap<String, String> ParseUrlParams(String strParams) {
        HashMap<String, String> params = new HashMap<String, String>();
        String[] arr = strParams.split("&");
        for (int i = 0; i < arr.length; i++) {
            String subParam = arr[i];
            String[] arr1 = subParam.split("=");
            params.put(arr1[0], arr1[1]);
        }
        return params;
    }

    public static String SentPostRequest(String url, HashMap<String, String> parameters, String sessionId) {
        String body = null;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost method = new HttpPost(url);
        method.setHeader("Cookie", "JSESSIONID=" + sessionId);
        if (method != null & parameters != null) {

            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                Iterator iter = parameters.entrySet().iterator();
                while (iter.hasNext()) {
                    Map.Entry entry = (Map.Entry) iter.next();
                    params.add(new BasicNameValuePair(entry.getKey() + "",
                        entry.getValue() + ""));
                }

                method.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));

                // 设置编码
                HttpResponse response = httpClient.execute(method);

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_OK) {
                    // Read the response body
                    body = EntityUtils.toString(response.getEntity());
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return body;
    }

    public static HashMap<String, Object> SendRequest(String url, String param,
                                                      String sessionId) {
        String result = "";
        URL httpurl = null;
        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            httpurl = new URL(url);
            HttpURLConnection httpConn = (HttpURLConnection) httpurl
                .openConnection();
            httpConn.setRequestMethod("POST");
            httpConn.setRequestProperty("accept", "*/*");

            httpConn.setRequestProperty("Accept-Charset", "utf-8");
            httpConn.setRequestProperty("connection", "Keep-Alive");
            httpConn.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            httpConn.setRequestProperty("contentType", "utf-8");

            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            PrintWriter out = new PrintWriter(httpConn.getOutputStream());
            // out.print(URLEncoder.encode(param+"", "UTF-8"));
            out.print(param);
            out.flush();
            out.close();

            Map hfs = httpConn.getHeaderFields();
            String cookieValue = httpConn.getHeaderField("Set-Cookie");
            System.out.println("cookie value:" + cookieValue);

            map.put("SessionId", sessionId);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                httpConn.getInputStream(), "UTF-8"));
            String line = "";
            while ((line = in.readLine()) != null) {
                result = result + line;
            }
            in.close();
            map.put("responseText", result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return map;
    }

    protected static void Htmo2Image(String url, int width, int height,
                                     String type, String savePath) throws Exception {

        JEditorPane ed = new JEditorPane(new URL(url));
        ed.setSize(1000, 800);

        // create a new image
        BufferedImage image = new BufferedImage(ed.getWidth(), ed.getHeight(),
            BufferedImage.TYPE_INT_ARGB);

        // paint the editor onto the image
        SwingUtilities.paintComponent(image.createGraphics(), ed, new JPanel(),
            0, 0, image.getWidth(), image.getHeight());
        // save the image to file
        ImageIO.write((RenderedImage) image, type, new File(savePath));
    }

    /**
     * 把URL转换为PDF
     *
     * @param outputFile ， 示例：/data/fs/inspector/BJ20150522001.pdf
     * @param url        ，示例：http :xxxx
     * @return
     * @throws Exception
     */
    public static boolean htmlToPdf(String outputFile, String url)
        throws Exception {
        File outFile = new File(outputFile);
        if (!outFile.exists()) {
            outFile.getParentFile().mkdirs();
        }
        OutputStream os = new FileOutputStream(outputFile);
        ITextRenderer renderer = new ITextRenderer();

        renderer.setDocument(url);

        // String fontPath = PdfUtil.class.getClassLoader().getResource("/")
        // .getPath();

        // 解决中文支持问题
        ITextFontResolver fontResolver = renderer.getFontResolver();

        PdfWriter pw = renderer.getWriter();
        renderer.layout();

        renderer.createPDF(os);
        os.flush();
        os.close();
        return true;
    }

    public static String SentGetRequest(String url, String param,
                                        String sessionId) throws Exception {
        String result = "";

        HttpClient httpClient = new DefaultHttpClient();
        try {
            if (param != null && !"".equals(param)) {
                String spit = url.indexOf("?") == -1 ? "?" : "&";
                url += spit + param;
            }
            HttpGet httpget = new HttpGet(url);
            httpget.setHeader("Cookie", "JSESSIONID=" + sessionId);
            // httpget.setHeader("Cookie",
            // "JSESSIONID=D33475275E3F72C6C9C50551E263AFF7" );

            HttpResponse response = httpClient.execute(httpget);

            StatusLine statusLine = response.getStatusLine();
            int httpStatus = statusLine.getStatusCode();

            if (httpStatus == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                BufferedReader br = null;
                InputStream in = entity.getContent();
                try {
                    br = new BufferedReader(new InputStreamReader(
                        entity.getContent()));
                    String line = "";
                    while ((line = br.readLine()) != null) {
                        result += "\n" + line;
                    }

                } finally {
                    // 关闭低层流。
                    in.close();
                }
                // 把底层的流给关闭
                EntityUtils.consume(entity);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return result;
    }

    public static void main(String[] args) throws Exception {

       /* Channel channel;
        Connection connection;
        ConnectionFactory factory = new ConnectionFactory();

       
        factory.setHost(RabbitMQListener.USER_NAME);
        factory.setUsername(RabbitMQListener.USER_NAME);
        factory.setPassword(RabbitMQListener.PASSWORD);
        connection = factory.newConnection();
        channel = connection.createChannel();*/

        //declaring a queue for this channel. If queue does not exist,
        //it will be created on the server.

       /* GateCheckoutInfo checkOut = new GateCheckoutInfo();
        List<Equipment> equipments = new ArrayList<>();
        checkOut.entryId = "ET-1365";
        Equipment tractor = new Equipment();
        tractor.equipmentNo = "HJCU1611558";
        Equipment trailer = new Equipment();
        trailer.equipmentNo = "JCLU1870641";
        equipments.add(tractor);
        equipments.add(trailer);
        checkOut.equipments = equipments;
        byte[] bytes = SerializationUtils.serialize(checkOut);*/
        //channel.basicPublish("amq.direct", RabbitMQListener, null, bytes);
        
        /*List<String> trailers = new ArrayList<>();
        trailers.add("74512");
        trailers.add("8756");
        List<String> containers = new ArrayList<>();
        containers.add("CTN421313");
        
        EntryTicketCheck entryTicketCheck = new EntryTicketCheck();
        entryTicketCheck.entryId = "ET-2";
        entryTicketCheck.driverName = "WANGLAN";
        entryTicketCheck.driverLicense = "8754216";
        entryTicketCheck.mcDot = "1525204";
        entryTicketCheck.seal = "S-0001";
        entryTicketCheck.actionType = CheckinActionType.PICK_UP;
        entryTicketCheck.carrierName = "AGI TRUCKING COMPANY";
        entryTicketCheck.companyName = "W12";
        entryTicketCheck.tractor = "HYUZ029100";
        entryTicketCheck.trailers = trailers;
        entryTicketCheck.spotName = "20";
        entryTicketCheck.containerNOs = containers;
        entryTicketCheck.warehouse = Warehouse.DALLAS.name();
        byte[] bytes = SerializationUtils.serialize(entryTicketCheck);
        channel.basicPublish("amq.direct", "checkin-entry-queue", null, bytes);*/
    /*	HashMap<String, String> params = new HashMap<String, String>();
		params.put("account", "wanglan");
		params.put("pwd", "damawl200");
		String sessionId = PdfUtil.login(LOGIN_URL, "wanglan", "damawl200");
		upLoadPDF(UPLOAD_URL,"d:\\temp\\xls_Lecta_Inventory_Summary.rpt",sessionId);*/
    }
    
	public static  String upLoadPDF(String url, String fullPath, String sessionId) throws Exception {
		String fileId = null;
		HttpClient httpClient = new DefaultHttpClient();
		boolean shutDowned = false;
		try {
			HttpPost httppost = new HttpPost(url);

			httppost.setHeader("Cookie", "JSESSIONID=" + sessionId);
			FileBody bin = new FileBody(new File(fullPath));
			MultipartEntity reqEntity = new MultipartEntity();

			reqEntity.addPart("file", bin);// file1为请求后台的File upload;属性
			httppost.setEntity(reqEntity);

			HttpResponse response = httpClient.execute(httppost);

			int statusCode = response.getStatusLine().getStatusCode();
			
			if (statusCode == HttpStatus.SC_OK) {
				HttpEntity resEntity = response.getEntity();
				String resp = EntityUtils.toString(resEntity);
				JSONArray json = JSONArray.fromObject(resp);
				JSONObject jo = json.getJSONObject(0);
				fileId = jo.getString("file_id");
				EntityUtils.consume(resEntity);
			}
			return fileId;
		} catch (IOException e) {
			e.printStackTrace();
			httpClient.getConnectionManager().shutdown();
		} finally {
			if(!shutDowned){
				try {
					httpClient.getConnectionManager().shutdown();
				} catch (Exception ignore) {
					ignore.printStackTrace();
				}
			}
		}
		return fileId;
	}
	
	public static String downloadFromFileServ( String downLoadUrl,long fileId,String savePath)throws Exception{
		String result = null;
		
		HttpClient httpClient = new DefaultHttpClient();
		try {
           HttpGet httpget = new HttpGet(downLoadUrl + fileId);
			
			//httpget.setHeader("Cookie", "JSESSIONID=" + sessionId);
		/*	String serviceUrl = FILE_SERVICE_URL + fileId;
			String localTime = StringUtil.getLocalTime();
			String key = StringUtil.generate("WISE", "d09adbbb-ba37-4cf1-859e-4dce55b6486a", serviceUrl,localTime);
			httpget.setHeader("Authorization", key);
            httpget.setHeader("timestamp", localTime);
            httpget.setHeader("TimeZone",timeZone.getID());
            httpget.setHeader("trace","true");*/

			HttpResponse response = httpClient.execute(httpget);
			
			StatusLine statusLine = response.getStatusLine();
			int httpStatus=statusLine.getStatusCode();
			
			
			File file = new File(savePath);
			if (httpStatus == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				
				InputStream in = entity.getContent();
				try {
					FileOutputStream fout = new FileOutputStream(file);
					int l = -1;
					byte[] tmp = new byte[1024];
					while ((l = in.read(tmp)) != -1) {
						fout.write(tmp, 0, l);
					}
					fout.flush();
					fout.close();
				} finally {
					// 关闭低层流。
					in.close();
				}
				//把底层的流给关闭
				EntityUtils.consume(entity);
				//result = filePath;
			}
			else{
				System.out.println("file:["+fileId+"] dose not existed");
			}
		} catch (Exception e) {
			System.out.println("file:["+fileId+"] dose not existed");
			e.printStackTrace();
			//throw new SystemException(e, "deleteFromFileServ", log);
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		return result;
	}


}
