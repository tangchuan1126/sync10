package com.cwc.app.api.ll;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.floor.api.ll.FloorAssetsMgrLL;
import com.cwc.app.iface.ll.AssetsMgrIFaceLL;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;

public class AssetsMgrLL implements AssetsMgrIFaceLL {
	static Logger log = Logger.getLogger("ACTION");
	private AdminMgr adminMgr = new AdminMgr();
	private FloorAssetsMgrLL floorAssetsMgrLL;
	
	public FloorAssetsMgrLL getFloorAssetsMgrLL() {
		return floorAssetsMgrLL;
	}

	public void setFloorAssetsMgrLL(FloorAssetsMgrLL floorAssetsMgrLL) {
		this.floorAssetsMgrLL = floorAssetsMgrLL;
	}

	public DBRow[] search(HttpServletRequest request,PageCtrl pc) throws Exception{
		try
		{
			DBRow para =Utils.getDBRowByName("search", request);
			para.add("a_name", para.getString("key"));
			para.add("a_barcode", para.getString("key"));
			para.add("requisitioned", para.getString("key"));
			para.remove("key");

			DBRow[] rows = null;
			try {
				rows = floorAssetsMgrLL.getRowsByPara(para,null,null, "aid desc", pc);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			return rows;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"search",log);
		}
		
	}
	
	public DBRow[] filter(HttpServletRequest request,PageCtrl pc) throws Exception{
		try
		{
			DBRow para =Utils.getDBRowByName("filter", request);	
			
			DBRow[] rows = null;
			try {
				rows = floorAssetsMgrLL.getRowsByPara(para,null,null, "aid desc", pc);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			return rows;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"filter",log);
		}
		
	}
}
