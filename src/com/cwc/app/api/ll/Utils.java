package com.cwc.app.api.ll;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public class Utils {
	public static DBRow getDBRowByName(String name,HttpServletRequest request) {
		DBRow row = new DBRow();
		Enumeration<String> pNames=request.getParameterNames();
		while(pNames.hasMoreElements()){
		    String pName=(String)pNames.nextElement();
		    String value=request.getParameter(pName)==null?"":request.getParameter(pName);
		    if(pName.indexOf(name+".")>=0)
		    	row.add(pName.replace(name+".", ""),value);
		    	////system.out.println(pName+" "+value);
		}
		return row;
	}
}
