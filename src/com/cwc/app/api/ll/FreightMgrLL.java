package com.cwc.app.api.ll;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.ccc.FloorComputeProductMgrCCC;
import com.cwc.app.floor.api.ll.FloorFreightMgrLL;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.zj.FloorProductStoreLogsDetailMgrZJ;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.ll.FreightMgrIFaceLL;
import com.cwc.app.iface.zyj.ApplyFundsMgrZyjIFace;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class FreightMgrLL implements FreightMgrIFaceLL {
	
	static Logger log = Logger.getLogger("ACTION");
	private AdminMgr adminMgr = new AdminMgr();
	private FloorFreightMgrLL floorFreightMgrLL;
	private FloorTransportMgrLL floorTransportMgrLL;
	private ApplyFundsMgrZyjIFace applyFundsMgrZyj;
	
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private FloorComputeProductMgrCCC floorComputeProductMgrCCC;
	private FloorProductStoreLogsDetailMgrZJ floorProductStoreLogsDetailMgrZJ;
	
	
	
	/**
	 * 更新交货单，转运单的运费到批次上
	 */
	public void updateTransportShippingFeeToProductStoreLogDetail(long transport_id)
		throws Exception
	{
		try 
		{
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			
			if(transport.get("transport_status",0)==TransportOrderKey.FINISH)//判断单据是否已入库，没入库的单子未产生批次，不必执行更新运费
			{
				int bill_type = ProductStoreBillKey.TRANSPORT_ORDER;

				long purchase_id = transport.get("purchase_id",0l);
				if(purchase_id!=0)//如果是采购交货转运，库存操作日志记录为交货单入库
				{
					bill_type = ProductStoreBillKey.DELIVERY_ORDER;
				}
				
				double sumPrice = floorComputeProductMgrCCC.getSumTransportFreightCost(transport_id).get("sum_price",0d);
				
				
				DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null, null, null);
				
				float sumWeight = floorTransportMgrZJ.getTransportWeight(transport_id);
				
				for (int i = 0; i < transportDetails.length; i++) 
				{
					
					float productSumWeight = transportDetails[i].get("transport_weight",0f)*transportDetails[i].get("transport_reap_count",0f);
					
					double product_fee = productSumWeight/sumWeight*sumPrice/transportDetails[i].get("transport_reap_count",0f);//每种商品单个运费
					
					DBRow para = new DBRow();
					para.add("shipping_fee",product_fee);
					
					DBRow[] productStoreLogDetails = floorProductStoreLogsDetailMgrZJ.getProductStoreLogsDetailsInStore(transportDetails[i].get("transport_pc_id",0l),transport.get("receive_psid",0l),bill_type,transport_id,ProductStoreCountTypeKey.STORECOUNT);
					/**
					 * 批次循环修改
					 */
//				for (int j = 0; j < productStoreLogDetails.length; j++) 
//				{
//					floorProductStoreLogsDetailMgrZJ.modProductStoreLogsDetail(productStoreLogDetails[j].get("psld_id",0l),para);
//				}
					//一次性修改批次的运费
					if (productStoreLogDetails.length>0)
					{
						floorProductStoreLogsDetailMgrZJ.modProductStoreLogDetails(productStoreLogDetails[0].get("psl_id",0l),para);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public DBRow transportSetFreight(HttpServletRequest request) throws Exception {
		String transport_id = StringUtil.getString(request,"transport_id");
		String[] tfc_ids = request.getParameterValues("tfc_id");
		String[] tfc_project_names = request.getParameterValues("tfc_project_name");
		String[] tfc_ways = request.getParameterValues("tfc_way");
		String[] tfc_units = request.getParameterValues("tfc_unit");
		String[] tfc_unit_prices = request.getParameterValues("tfc_unit_price");
		String[] tfc_unit_counts = request.getParameterValues("tfc_unit_count");
		String[] tfc_currency = request.getParameterValues("tfc_currency");
		String[] tfc_exchange_rate = request.getParameterValues("tfc_exchange_rate");
//		int stock_in_set = StrUtil.getInt(request,"stock_in_set");
		int changed = StringUtil.getInt(request,"changed",0);
		
		double modFreightTotal = 0.0;
		//修改运费时，如果修改最终的值比申请的值小，不能修改
		for(int i=0;tfc_ids!=null && i<tfc_ids.length;i++) {
			if(!"".equals(tfc_unit_prices[i]) && !"".equals(tfc_exchange_rate[i]) && !"".equals(tfc_unit_counts[i]))
			{
				modFreightTotal += Double.parseDouble(tfc_unit_prices[i])*Double.parseDouble(tfc_exchange_rate[i])*Double.parseDouble(tfc_unit_counts[i]);
			}
		}
		long purchase_id = StringUtil.getLong(request, "purchase_id");
		int typeId = 0==purchase_id?6:5;
		double applyFreight = applyFundsMgrZyj.getTransportFreightTotalApply(Long.parseLong(transport_id), typeId);
		DBRow result = new DBRow();
		SystemConfigIFace systemConfig	=((SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig"));
		if((modFreightTotal >= applyFreight) || (modFreightTotal<applyFreight && (Math.abs(applyFreight-modFreightTotal)<=Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))*modFreightTotal*0.01))){
			if(changed == 2) 
			{
				floorFreightMgrLL.deleteTransportFreightCostByDeliveryId(transport_id);
			}
			
			for(int i=0;tfc_ids!=null && i<tfc_ids.length;i++) {
				DBRow row = new DBRow();
				row.add("tfc_id", tfc_ids[i]);
				row.add("transport_id", transport_id);
				row.add("tfc_project_name", tfc_project_names[i]);
				row.add("tfc_way", tfc_ways[i]);
				row.add("tfc_unit", tfc_units[i]);
				row.add("tfc_unit_price", tfc_unit_prices[i]);
				row.add("tfc_unit_count", tfc_unit_counts[i]);
				row.add("tfc_currency", tfc_currency[i]);
				row.add("tfc_exchange_rate", tfc_exchange_rate[i]);
				
				if(changed == 2 || row.getString("tfc_id").equals("") || row.getString("tfc_id").equals("0"))
				{
					row.remove("tfc_id");
					floorFreightMgrLL.insertTransportFreightCost(row);
				}
				else 
				{
					floorFreightMgrLL.updateTransportFreightCost(row);
				}		
			}
			result.add("flag", "success");
		}
		else
		{
			result.add("flag", "fail");
			result.add("apply_freight", String.valueOf(applyFreight));
		}
		//设置运费，对批次进行运费更新(屏蔽)
		this.updateTransportShippingFeeToProductStoreLogDetail(Long.parseLong(transport_id));
		
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		floorTransportMgrLL.insertLogs(transport_id, "转运单设置运费:单号"+transport_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3);
		return result;
	}
	
	public void transportSetFreight(long transport_id,HttpServletRequest request) 
		throws Exception 
	{
		String[] tfc_ids = request.getParameterValues("tfc_id");
		String[] tfc_project_names = request.getParameterValues("tfc_project_name");
		String[] tfc_ways = request.getParameterValues("tfc_way");
		String[] tfc_units = request.getParameterValues("tfc_unit");
		String[] tfc_unit_prices = request.getParameterValues("tfc_unit_price");
		String[] tfc_unit_counts = request.getParameterValues("tfc_unit_count");
		String[] tfc_currency = request.getParameterValues("tfc_currency");
		String[] tfc_exchange_rate = request.getParameterValues("tfc_exchange_rate");
		int stock_in_set = StringUtil.getInt(request,"stock_in_set");
		int changed = StringUtil.getInt(request,"changed",0);
		
		if(changed == 2) 
		{
			floorFreightMgrLL.deleteTransportFreightCostByDeliveryId(String.valueOf(transport_id));
		}
		
		for(int i=0;tfc_ids!=null && i<tfc_ids.length;i++) 
		{
			DBRow row = new DBRow();
			row.add("tfc_id", tfc_ids[i]);
			row.add("transport_id", transport_id);
			row.add("tfc_project_name", tfc_project_names[i]);
			row.add("tfc_way", tfc_ways[i]);
			row.add("tfc_unit", tfc_units[i]);
			row.add("tfc_unit_price", tfc_unit_prices[i]);
			row.add("tfc_unit_count", tfc_unit_counts[i]);
			row.add("tfc_currency", tfc_currency[i]);
			row.add("tfc_exchange_rate", tfc_exchange_rate[i]);
			
			if(changed == 2 || row.getString("tfc_id").equals("") || row.getString("tfc_id").equals("0")) 
			{
				row.remove("tfc_id");
				floorFreightMgrLL.insertTransportFreightCost(row);
			}
			else 
			{
				floorFreightMgrLL.updateTransportFreightCost(row);
			}		
		}
		
		
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		floorTransportMgrLL.insertLogs(String.valueOf(transport_id), "转运单设置运费:单号"+transport_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3);
	}
	
	public DBRow[] getAllFreightCost() throws Exception {
		return floorFreightMgrLL.getAllFreightCost();
	}

	public FloorTransportMgrLL getFloorTransportMgrLL() {
		return floorTransportMgrLL;
	}

	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}

	public FloorFreightMgrLL getFloorFreightMgrLL() {
		return floorFreightMgrLL;
	}

	public void setFloorFreightMgrLL(FloorFreightMgrLL floorFreightMgrLL) {
		this.floorFreightMgrLL = floorFreightMgrLL;
	}
	
	public DBRow[] getAllFreightResources() throws Exception {
		return floorFreightMgrLL.getAllFreightResources();
	}
	
	public DBRow[] getFreightResourcesByCompany(String company,PageCtrl pc) throws Exception {
		return floorFreightMgrLL.getFreightResourcesByCompany(company, pc);
	}
	
	public DBRow getCountryById(String id) throws Exception {
		return floorFreightMgrLL.getCountryById(id);
	}
	
	public DBRow[] getCompanys() throws Exception {
		return floorFreightMgrLL.getCompanys();
	}
	
	public DBRow[] getDeliveryFreightCostByDeliveryId(String deliveryId) throws Exception {
		return floorFreightMgrLL.getDeliveryFreightCostByDeliveryId(deliveryId);
	}
	
	public DBRow[] getTransportFreightCostByTransportId(String transport_id) throws Exception{
		return floorFreightMgrLL.getTransportFreightCostByTransportId(transport_id);
	}
	
	public DBRow[] getFreightCostByfrId(String ft_id) throws Exception {
		return floorFreightMgrLL.getFreightCostByfrId(ft_id);
	}
	public void setApplyFundsMgrZyj(ApplyFundsMgrZyjIFace applyFundsMgrZyj) {
		this.applyFundsMgrZyj = applyFundsMgrZyj;
	}

	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}

	public void setFloorComputeProductMgrCCC(
			FloorComputeProductMgrCCC floorComputeProductMgrCCC) {
		this.floorComputeProductMgrCCC = floorComputeProductMgrCCC;
	}

	public void setFloorProductStoreLogsDetailMgrZJ(
			FloorProductStoreLogsDetailMgrZJ floorProductStoreLogsDetailMgrZJ) {
		this.floorProductStoreLogsDetailMgrZJ = floorProductStoreLogsDetailMgrZJ;
	}
}
