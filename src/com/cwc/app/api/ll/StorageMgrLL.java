package com.cwc.app.api.ll;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.productStorage.NoExistStorageException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.ll.FloorStorageMgrLL;
import com.cwc.app.floor.api.tjh.FloorProductLineMgrTJH;
import com.cwc.app.floor.api.zj.FloorProductStorageMgrZJ;
import com.cwc.app.iface.ll.StorageIFaceMgrLL;
import com.cwc.app.iface.zj.ProductStoreEditLogMgrIFaceZJ;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;

public class StorageMgrLL implements StorageIFaceMgrLL {
	static Logger log = Logger.getLogger("ACTION");
	private AdminMgr adminMgr = new AdminMgr();
	private FloorProductLineMgrTJH floorProductLineMgrTJH;
	private FloorCatalogMgr floorCatalogMgr;
	private FloorProductStorageMgrZJ floorProductStorageMgrZJ;
	private FloorProductMgr floorProductMgr;
	private ProductStoreEditLogMgrIFaceZJ productStoreEditLogMgrZJ;
	private FloorStorageMgrLL floorStorageMgrLL;
	
	public String downloadStorageCost(HttpServletRequest request) throws Exception {
		String sheetName = "所有商品";
		
		long ps_id = StringUtil.getLong(request,"ps_id");
		
		Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
		ArrayList al = tree.getAllSonNode(ps_id);
		al.add(String.valueOf(ps_id));
		
		long catalog_id = StringUtil.getLong(request,"catalog_id");
		long product_line_id = StringUtil.getLong(request,"product_line");
		
		if(product_line_id !=0)//为0则不查询子分类
		{
			DBRow product_line = floorProductLineMgrTJH.getProductLineById(product_line_id);
			
			sheetName = "产品线-"+product_line.getString("name");
		}
		
		String catalog_ids = "";
//		if(product_line_id.contains("-p"))//选择的是产品线,产品线第一层
//		{
//			long pro_line_id = Long.parseLong(product_line_id.substring(0,product_line_id.indexOf("-")));
//			
//			
//		}
//		else if(!product_line_id.contains("-p")&&!product_line_id.equals(""))//产品线下拉框第二层
//		{
//			if(Long.parseLong(product_line_id)!=0)
//			{
//				catalog_id = Long.parseLong(product_line_id);
//			}
//			
//		}
		//选择产品线第二层，实际上选择的是商品分类
		if(catalog_id!=0)
		{
			DBRow catalog = floorCatalogMgr.getDetailProductCatalogById(catalog_id);
			sheetName = sheetName+"商品分类-"+catalog.getString("title");
		}
		
		DBRow storage = floorProductStorageMgrZJ.getProductStorageCatalogByPsid(ps_id);
		
		DBRow[] product_storages = getAllProductStorageByPsid((String[])al.toArray(new String[0]),catalog_id,product_line_id);
		POIFSFileSystem fs = null;//获得模板
		fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product/ExportStorageCost.xls"));
		HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
		
		wb.setSheetName(0,"所有商品");
		HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
		
		HSSFRow row = sheet.getRow(0);
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_LEFT); //创建一个居中格式
		style.setLocked(false);//设置不锁定
		style.setWrapText(true); 
		
		for (int i = 0; i < product_storages.length; i++) 
		{
			DBRow productStorage = product_storages[i];
			long pc_id = productStorage.get("pc_id",0l);
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			
			row = sheet.createRow((int) i + 1);
			if(product == null)
			{
				continue;
			}
			
			row.createCell(0).setCellValue(productStorage.getString("cid"));
			row.getCell(0).setCellStyle(style);
			
			row.createCell(1).setCellValue(productStorage.getString("pc_id"));
			row.getCell(1).setCellStyle(style);

			row.createCell(2).setCellValue(product.getString("p_name"));
			row.getCell(2).setCellStyle(style);
			
			row.createCell(3).setCellValue(product.getString("p_code"));
			row.getCell(3).setCellStyle(style);
			
			row.createCell(4).setCellValue(productStorage.getString("storage_unit_freight"));
			row.getCell(4).setCellStyle(style);

			row.createCell(5).setCellValue(productStorage.getString("storage_unit_price"));
			row.getCell(5).setCellStyle(style);
		}
		String path = "upl_excel_tmp/"+storage.getString("title")+".xls";
		FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
		wb.write(fout);
		fout.close();

		return (path);
	}
	
	public DBRow[] checkImportStorageAlert(String filename, String storage_title)
		throws Exception 
	{
		InputStream is = null;
		Workbook rwb = null; 
		
		try 
		{
				DBRow product_storage = floorProductStorageMgrZJ.getDetailProductStorageCatalogByTitle(storage_title);
				
				if(product_storage == null)
				{
					throw new NoExistStorageException();
				}
				
				long ps_id = product_storage.get("id",0l);
				
				String path = "";
				
				path = Environment.getHome() + "upl_excel_tmp/" + filename;
				is = new FileInputStream(path);
				rwb = Workbook.getWorkbook(is);
				
				DBRow[] store_alerts = storeAlertData(rwb);
				ArrayList<DBRow> list = new ArrayList<DBRow>();
				
				for (int i = 0; i < store_alerts.length; i++) 
				{
					boolean error = false;
					StringBuffer error_message = new StringBuffer("<br/><font color='red'>错误行号："+i+"<br/>");
					try 
					{
						DBRow product = floorProductMgr.getDetailProductByPname(store_alerts[i].getString("p_name"));
						
						if(product.get("pc_id",0l)!=Long.parseLong(store_alerts[i].getString("pc_id")))//商品名与商品ID不对应
						{
							error = true;
							error_message.append("商品名异常，必须使用导出文件修改<br/>");
						}
						
						if(Long.parseLong(store_alerts[i].getString("storage_id"))!=ps_id)//仓库ID不对应
						{
							error = true;
							error_message.append("对应仓库异常，必须使用导出文件修改<br/>");
						}

						float storage_unit_freight = Float.parseFloat(store_alerts[i].getString("storage_unit_freight"));
						
						if(!(storage_unit_freight>0))//警戒值不大于0
						{
							error = true;
							error_message.append("运费异常，必须大于0<br/>");
						}
						
						float storage_unit_price = Float.parseFloat(store_alerts[i].getString("storage_unit_price"));
						
						if(!(storage_unit_price>0))//警戒值不大于0
						{
							error = true;
							error_message.append("库存费用异常，必须大于0<br/>");
						}
					} 
					catch (NumberFormatException e) 
					{
						error = true;
						error_message.append("未使用导出文件修改或警戒值非数字<br/>");
					}
					
					if(error)
					{
						error_message.append("</font>");
						store_alerts[i].add("error_line",error_message);
						list.add(store_alerts[i]);
					}
				}
				return (list.toArray(new DBRow[0]));
		}
		catch(NoExistStorageException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"checkImportStorageAlert",log);
		}
	}
	
	public DBRow[] storeAlertData(Workbook rwb) 
		throws Exception
	{
		HashMap<Integer, String> productFiled = new HashMap<Integer, String>();
		productFiled.put(0, "storage_id");
		productFiled.put(1, "pc_id");
		productFiled.put(2, "p_name");
		productFiled.put(3, "p_code");
		productFiled.put(4, "storage_unit_freight");
		productFiled.put(5, "storage_unit_price");
		
		ArrayList<DBRow> al = new ArrayList<DBRow>();
		Sheet rs = rwb.getSheet(0);
		int rsColumns = rs.getColumns();  //excel表字段数
		int rsRows = rs.getRows();    //excel表记录行数
		
		  for(int i=1;i<rsRows;i++)
		  {
		   DBRow pro = new DBRow();
	
		   
		   boolean result = false;//判断是否纯空行数据
		   for (int j=0;j<rsColumns;j++)
		   {
			   	String content = rs.getCell(j,i).getContents().trim();
			   	if(j==2)
			   	{
			   		content = content.toUpperCase();
			   	}
				pro.add(productFiled.get(j).toString(),content);  //转换成DBRow数组输出,去掉空格
		    
			    ArrayList proName=pro.getFieldNames();//DBRow 字段名
			    for(int p=0;p<proName.size();p++)
			    {
			    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
			    	{
			    		result = true;
			    	}
			    }
		   }
		   
		   if(result)//不是纯空行就加入到DBRow
		   {
			   al.add(pro);   
		   }
		  }
		  
		  return (al.toArray(new DBRow[0]));
	}
	
	/**
	 * 保存上传库存警戒值
	 * @param request
	 * @throws Exception
	 */
	public void saveImportStorageCost(HttpServletRequest request)
		throws Exception
	{
		InputStream is = null;
		Workbook rwb = null; 
		
		try {
			String filename = StringUtil.getString(request,"filename");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			String path = "";
			
			path = Environment.getHome() + "upl_excel_tmp/" + filename;
			is = new FileInputStream(path);
			rwb = Workbook.getWorkbook(is);
			
			DBRow[] store_alerts = storeAlertData(rwb);
			
			for (int i = 0; i < store_alerts.length; i++) 
			{
				long pc_id = Long.parseLong(store_alerts[i].getString("pc_id"));
				long ps_id = Long.parseLong(store_alerts[i].getString("storage_id"));
				float storage_unit_freight = Float.parseFloat(store_alerts[i].getString("storage_unit_freight"));
				float storage_unit_price = Float.parseFloat(store_alerts[i].getString("storage_unit_price"));
				
				floorProductStorageMgrZJ.modProductStorageCost(pc_id, ps_id, storage_unit_freight, storage_unit_price);
				
				productStoreEditLogMgrZJ.addProductStoreLogForPcidAndPsid(pc_id, ps_id, adminLoggerBean, 1);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"saveImportStorageAlert",log);
		}
		finally
		{
			rwb.close();
			is.close();
			
		}
	}
	
	public DBRow[] getCostLog(long ps_id, String p_name, PageCtrl pc) throws Exception {
		return floorStorageMgrLL.getCostLog(ps_id, p_name, pc);
	}
	
	public DBRow[] getAllProductStorageByPsid(String[] ps_ids,long catalog_id,long pro_line_id) 
		throws Exception 
	{
		return floorProductStorageMgrZJ.getAllProductStorageByPsid(ps_ids,catalog_id,pro_line_id);
	}
	public FloorProductLineMgrTJH getFloorProductLineMgrTJH() {
		return floorProductLineMgrTJH;
	}

	public void setFloorProductLineMgrTJH(
			FloorProductLineMgrTJH floorProductLineMgrTJH) {
		this.floorProductLineMgrTJH = floorProductLineMgrTJH;
	}

	public FloorCatalogMgr getFloorCatalogMgr() {
		return floorCatalogMgr;
	}

	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}

	public FloorProductStorageMgrZJ getFloorProductStorageMgrZJ() {
		return floorProductStorageMgrZJ;
	}

	public void setFloorProductStorageMgrZJ(
			FloorProductStorageMgrZJ floorProductStorageMgrZJ) {
		this.floorProductStorageMgrZJ = floorProductStorageMgrZJ;
	}

	public FloorProductMgr getFloorProductMgr() {
		return floorProductMgr;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public ProductStoreEditLogMgrIFaceZJ getProductStoreEditLogMgrZJ() {
		return productStoreEditLogMgrZJ;
	}

	public void setProductStoreEditLogMgrZJ(
			ProductStoreEditLogMgrIFaceZJ productStoreEditLogMgrZJ) {
		this.productStoreEditLogMgrZJ = productStoreEditLogMgrZJ;
	}

	public FloorStorageMgrLL getFloorStorageMgrLL() {
		return floorStorageMgrLL;
	}

	public void setFloorStorageMgrLL(FloorStorageMgrLL floorStorageMgrLL) {
		this.floorStorageMgrLL = floorStorageMgrLL;
	}
}
