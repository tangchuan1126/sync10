package com.cwc.app.api.ll;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.ll.FloorAdminMgrLL;
import com.cwc.app.iface.ll.AdminMgrIFaceLL;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class AdminMgrLL implements AdminMgrIFaceLL {
	static Logger log = Logger.getLogger("ACTION");
	private FloorAdminMgrLL floorAdminMgrLL;
	
	
	public FloorAdminMgrLL getFloorAdminMgrLL() {
		return floorAdminMgrLL;
	}


	public void setFloorAdminMgrLL(FloorAdminMgrLL floorAdminMgrLL) {
		this.floorAdminMgrLL = floorAdminMgrLL;
	}


	public DBRow getAdminById(String id) throws Exception {
		try
		{
			return floorAdminMgrLL.getAdminById(id);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getAdminById",log);
		}			
	}
}
