package com.cwc.app.api.ll;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.ll.FloorPurchaseMgrLL;
import com.cwc.app.iface.ll.PurchaseMgrIFaceLL;
import com.cwc.app.key.CurrencyKey;
import com.cwc.app.key.DrawbackKey;
import com.cwc.app.key.InvoiceKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class PurchaseMgrLL implements PurchaseMgrIFaceLL {
	static Logger log = Logger.getLogger("ACTION");
	private AdminMgr adminMgr = new AdminMgr();
	private FloorPurchaseMgrLL floorPurchaseMgrLL;
	

	public FloorPurchaseMgrLL getFloorPurchaseMgrLL() {
		return floorPurchaseMgrLL;
	}

	public void setFloorPurchaseMgrLL(FloorPurchaseMgrLL floorPurchaseMgrLL) {
		this.floorPurchaseMgrLL = floorPurchaseMgrLL;
	}

	public void updatePurchase(HttpServletRequest request) throws Exception {
		try {
			DBRow row =Utils.getDBRowByName("purchase", request);
			long nation = StringUtil.getLong(request, "deliver_nation");
			long proviceId = StringUtil.getLong(request, "deliver_pro_id");
			String pro_input = "";
			if(0 == proviceId || -1 == proviceId){
				pro_input = StringUtil.getString(request, "deliver_pro_input");
			}else{
				pro_input = "";
			}
			row.add("deliver_native", nation);
			row.add("deliver_provice", proviceId);
			row.add("deliver_pro_input", pro_input);
			row.remove("invoice");
			row.remove("invoince_amount");
			row.remove("invoince_currency");
			row.remove("invoince_dot");
			row.remove("drawback");
			row.remove("rebate_rate");
			row.remove("need_tag");
			row.remove("quality_inspection");
			floorPurchaseMgrLL.updatePurchase(row);
			DBRow detailRow =Utils.getDBRowByName("purchase_detail", request);
			if(detailRow.get("eta", "").length() <= 0){
				detailRow.remove("eta");
			}
			floorPurchaseMgrLL.updatePurchaseDetailByPurchaseId(row.getString("purchase_id"), detailRow);
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			InvoiceKey invoiceKey	= new InvoiceKey();
			CurrencyKey currencyKey	= new CurrencyKey();
			DrawbackKey drawbackKey = new DrawbackKey();
			
			insertLogs(row.getString("purchase_id"),"[修改采购单]:单号"+row.getString("purchase_id"),adminLoggerBean.getAdid(),adminLoggerBean.getEmploye_name(),3);
			return;
		} catch (Exception e) {
			throw new SystemException(e,"updatePurchase",log);
		}
	}
	
	
	public DBRow getPurchaseById(String id) throws Exception {
		try {
			return floorPurchaseMgrLL.getPurchaseById(id);
		} catch (Exception e) {
			throw new SystemException(e,"getPurchaseById",log);
		}
	}
	
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type) throws Exception{
		try {
			floorPurchaseMgrLL.insertLogs(id, content, oprator_id, oprator, type);
		} catch (Exception e) {
			throw new SystemException(e,"insertLogs",log);
		}
	}
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type, int typeSub) throws Exception{
		try {
			floorPurchaseMgrLL.insertLogs(id, content, oprator_id, oprator, type, typeSub);
		} catch (Exception e) {
			throw new SystemException(e,"insertLogs",log);
		}
	}
	
	
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception {
		try {
			return floorPurchaseMgrLL.getAnalysis(st, en, analysisType, analysisStatus, day, pc);
		} catch (Exception e) {
			throw new SystemException(e,"getAnalysis",log);
		}
	}
}
