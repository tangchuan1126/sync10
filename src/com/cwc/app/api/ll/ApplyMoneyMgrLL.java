package com.cwc.app.api.ll;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.floor.api.ll.FloorAdminMgrLL;
import com.cwc.app.floor.api.ll.FloorApplyImagesMgrLL;
import com.cwc.app.floor.api.ll.FloorApplyMoneyLogsMgrLL;
import com.cwc.app.floor.api.ll.FloorApplyMoneyMgrLL;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.ll.ApplyMoneyMgrIFaceLL;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class ApplyMoneyMgrLL implements ApplyMoneyMgrIFaceLL {
	static Logger log = Logger.getLogger("ACTION");
	private AdminMgr adminMgr = new AdminMgr();
	private FloorApplyMoneyMgrLL floorApplyMoneyMgrLL;
	private FloorApplyImagesMgrLL floorApplyImagesMgrLL;
	private FloorAdminMgrLL floorAdminMgrLL;
	private FloorApplyMoneyLogsMgrLL floorApplyMoneyLogsMgrLL;
	private FloorTransportMgrLL floorTransportMgrLL;
	private FloorTransportMgrZr floorTransportMgrZr;
	private ApplyMoneyMgrIFace applyMoneyMgrZZZ;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private TransportMgrIfaceZr transportMgrZr;
	
	
	public DBRow insertRow(DBRow row) throws Exception {
		try
		{
			return floorApplyMoneyMgrLL.insertRow(row);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"insertRow",log);
		}
	}
	
	public DBRow[] getMoneyAssociationTypeAll() throws Exception {
		try
		{
			return floorApplyMoneyMgrLL.getMoneyAssociationTypeAll();
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getMoneyAssociationTypeAll",log);
		}
	}
	
	public DBRow getMoneyAssociationTypeById(String id) throws Exception {
		return floorApplyMoneyMgrLL.getMoneyAssociationTypeById(id);
	}
	
	public DBRow getRowById(String id) throws Exception {
		try
		{
			return floorApplyMoneyMgrLL.getRowById(id);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getRowById",log);
		}
	}
	
	public boolean checkApplyMoneyAssociationById(String className, String id, int type) throws Exception {
		try
		{
			boolean b = true;
			
			com.cwc.app.beans.ll.BaseBeanLL associationTable = (com.cwc.app.beans.ll.BaseBeanLL)MvcUtil.getBeanFromContainer(className);

			if(associationTable.checkApplyMoneyAssociationById(id,type)) {
				b = true;
			}else {
				b = false;
			}
			
			return b;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"checkApplyMoneyAssociationById",log);
		}
	}
	
	public DBRow[] getAllByTable(String tableName) throws Exception {
		try
		{
			com.cwc.app.beans.ll.BaseBeanLL table = (com.cwc.app.beans.ll.BaseBeanLL)MvcUtil.getBeanFromContainer(tableName);
			return table.getAll(); 
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getAllByTable",log);
		}
	}
	
	public DBRow[] getAllByReadView(String readViewName) throws Exception {
		try
		{
			com.cwc.app.beans.ll.BaseReadViewLL readView = (com.cwc.app.beans.ll.BaseReadViewLL)MvcUtil.getBeanFromContainer(readViewName);
			return readView.getAll(); 
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getAllByReadView",log);
		}
	}
	
	public DBRow[] getApplyTransferByBusiness(String businessId,int associationTypeId) throws Exception {
		try
		{
			return floorApplyMoneyMgrLL.getApplyTransferByBusiness(businessId, associationTypeId);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyFundsCategory",log);
		}
	}
	
	public DBRow[] getApplyMoneyByBusiness(String businessId,int associationTypeId) throws Exception {
		try
		{
			return floorApplyMoneyMgrLL.getApplyMoneyByBusiness(businessId, associationTypeId);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"addApplyFundsCategory",log);
		}
	}
	
	public DBRow[] getApplyTransferByApplyMoney(String id) throws Exception{
		try
		{
			return floorApplyMoneyMgrLL.getApplyTransferByApplyMoney(id);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getApplyTransferByBusiness",log);
		}
	}
	
	public DBRow insertApplyMoney(HttpServletRequest request) throws Exception {
		try
		{
			SystemConfigIFace systemConfig	=((SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig"));
			DBRow row = Utils.getDBRowByName("apply_money", request);
			String currency = StringUtil.getString(request, "apply_money.currency");
			double amount	= StringUtil.getDouble(request, "apply_money.amount");
			if("RMB".equals(currency)){
				row.add("standard_money", amount);
			}else{
				row.add("standard_money", amount*Double.parseDouble(systemConfig.getStringConfigValue(currency)));
			}
			DBRow applyMoneyRow = floorApplyMoneyMgrLL.insertRow(row);
			
			long apply_money_id = applyMoneyRow.get("apply_id",0l);
			long association_id	= applyMoneyRow.get("association_id", 0L);
			applyMoneyMgrZZZ.editApplyMoneyIndex(apply_money_id,"add");
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			//加任务和日志
			String last_time = StringUtil.getString(request, "apply_money.last_time");
			this.addTransferFreightSchedule(request, apply_money_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), amount, currency,association_id,last_time);
			
			return applyMoneyRow;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"insertApplyMoney",log);
		}
		
	}
	/**
	 * 加运费申请资金的任务
	 * @param request
	 * @param apply_id
	 * @param adid
	 * @param adName
	 * @param amount
	 * @param currency
	 * @throws Exception
	 */
	private void addTransferFreightSchedule(HttpServletRequest request, long apply_id, long adid, String adName, double amount, String currency, long association_id, String last_time) throws Exception
	{
		try
		{
			String adminUserNames	= StringUtil.getString(request, "adminUserNames");
			String adminUserIds		= StringUtil.getString(request, "adminUserIds");
			int isNeedMail			= StringUtil.getInt(request, "isNeedMail");
			int isNeedMessage		= StringUtil.getInt(request, "isNeedMessage");
			int isNeedPage			= StringUtil.getInt(request, "isNeedPage");
			boolean isSendMail = false;
			boolean isSendMessage = false;
			boolean isSendPage = false;
			if(2 == isNeedMail)
			{
				isSendMail = true;
			}
			if(2 == isNeedMessage)
			{
				isSendMessage = true;
			}
			if(2 == isNeedPage)
			{
				isSendPage = true;
			}
			String content = "创建了资金申请:"+apply_id+",金额:"+amount+currency+",负责人:"+adminUserNames;
			//加运费申请资金的任务
			scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), last_time+" 23:59:59", 
						adid,adminUserIds, "",
						apply_id, ModuleKey.APPLY_MONEY, "申请运费:"+apply_id, 
						adName+content,
						isSendPage, isSendMail, isSendMessage, request, true, ProcessKey.TRANSPORT_STOCK_APPLY_FUNDS);
			
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(association_id);
			TransportStockInSetKey transportStockInSetKey = new TransportStockInSetKey();
			int transport_stock_in_set = transport.get("stock_in_set", 0);
			//跟进运费
			scheduleMgrZr.addScheduleReplayExternal(association_id, ModuleKey.TRANSPORT_ORDER, 5, adName+"确认了["+transportStockInSetKey.getStatusById(transport_stock_in_set)+"]阶段,"+content, false, request, "transport_stock_in_set_period");
			//加日志
			transportMgrZr.insertTransportLogs(association_id, "["+transportStockInSetKey.getStatusById(transport_stock_in_set)+"]阶段,"+content, adid, adName, 5, "", transport_stock_in_set);
			
		} catch (Exception e) {
			throw new SystemException(e,"addTransferFreightSchedule",log);
		}
	}
	
	
	public DBRow insertApplyMoneyByTransport(HttpServletRequest request)
		throws Exception 
	{
		DBRow applyMoneyRow = insertApplyMoney(request);
		
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		String typesName = floorApplyMoneyMgrLL.getApplyMoneyCategoryById(applyMoneyRow.getString("types")).getString("category_name");
		
		floorTransportMgrLL.insertLogs(applyMoneyRow.getString("association_id"), "转运单"+typesName+"申请:单号"+applyMoneyRow.getString("apply_id"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3);
		return applyMoneyRow;
	}
	
	public String uploadImage(HttpServletRequest request) throws Exception {
		try {
			// 移动文件。放到指定的位置
			// 重新命名文件名称
			
			String fileNames = StringUtil.getString(request, "file_names");
			String path = StringUtil.getString(request, "path");
			String sn = StringUtil.getString(request,"sn");
	 
			String[] fileNameArray = null ;
			long association_id = StringUtil.getLong(request,"association_id");
			long association_type = StringUtil.getLong(request,"association_type");
			if(fileNames.trim().length() > 0){
				fileNameArray = fileNames.split(",");
			}
			if(fileNameArray != null && fileNameArray.length > 0 ){
				String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
				for(String name : fileNameArray){
					DBRow row = new DBRow();
					StringBuffer realFileName = new StringBuffer(sn);
					realFileName.append("_").append(association_id).append("_");
					String suffix = getSuffix(name);
					
				 
					realFileName.append(name.replace("."+suffix, ""));
					int indexFileName = floorTransportMgrZr.getIndexOfApplyMoneyImageByFileName(realFileName.toString());
					if(indexFileName != 0){
						realFileName.append("_").append(indexFileName);
					}
					realFileName.append(".").append(suffix);
					
					row.add("association_id", association_id);
					row.add("association_type", association_type);
					row.add("path", realFileName.toString());
					floorApplyImagesMgrLL.insertApplyImages(row);
					//移动文件
					String tempUrl = baseTempUrl+name;
					StringBuffer url = new StringBuffer();
					url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
					FileUtil.moveFile(tempUrl,url.toString());
				}

			}
			return null ;
		} catch (Exception e) {
			throw new SystemException(e,"uploadImage",log);
		}
	}
	/**
	 * 保存与业务无关的申请资金的上传文件
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Override
	public String uploadImageApplyMoneyNoAssociate(HttpServletRequest request) throws Exception {
		try {
			String permitFile = "jpg,gif,bmp";
			
			TUpload upload = new TUpload();
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));//文件名
			upload.setRootFolder("/upload/financial_management/");//文件保存路径
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			if (flag==2)
			{
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			String fileName=upload.getFileName();
			long association_id = upload.getRequestRow().get("association_id",0);
			long association_type = upload.getRequestRow().get("association_type",0);
			String backUrl = upload.getRequestRow().getString("backUrl");
			String path = fileName;
			
			DBRow row = new DBRow();
			
			row.add("association_id", association_id);
			row.add("association_type", association_type);
			row.add("path", path);
			floorApplyImagesMgrLL.insertApplyImages(row);
			
			return backUrl;
		} catch (Exception e) {
			throw new SystemException(e,"uploadImage",log);
		}
	}
	
	public String uploadOldImage(HttpServletRequest request) throws Exception {
		try {
			String permitFile = "jpg,gif,bmp";
			
			TUpload upload = new TUpload();
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));//文件名
			upload.setRootFolder("/upload/financial_management/");//文件保存路径
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			if (flag==2)
			{
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			String fileName=upload.getFileName();
			long association_id = upload.getRequestRow().get("association_id",0);
			long association_type = upload.getRequestRow().get("association_type",0);
			String backUrl = upload.getRequestRow().getString("backUrl");
			String path = fileName;
			
			DBRow row = new DBRow();
			
			row.add("association_id", association_id);
			row.add("association_type", association_type);
			row.add("path", path);
			floorApplyImagesMgrLL.insertApplyImages(row);
			
			return backUrl;
		} catch (Exception e) {
			throw new SystemException(e,"uploadImage",log);
		}
	}
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	public List getImageList(String association_id, String association_type) throws Exception {
		try {
			return floorApplyImagesMgrLL.getImageList(association_id, association_type);
		} catch (Exception e) {
			throw new SystemException(e,"getImageList",log);
		}
	}
	
	public void deleteImage(HttpServletRequest request) throws Exception {
		try {
			String id = StringUtil.getString(request, "id");
			floorApplyImagesMgrLL.deleteImage(id);
		} catch (Exception e) {
			throw new SystemException(e,"deleteImage",log);
		}
	}
	
	public DBRow getApplyMoneyCreate(String user_id) throws Exception {
		return floorAdminMgrLL.getAdminById(user_id);
	}
	
	public void remark(HttpServletRequest request) throws Exception {
		String apply_id = StringUtil.getString(request,"apply_id");
		String content = "转账备注:"+StringUtil.getString(request,"followup_content");
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		floorApplyMoneyLogsMgrLL.insertLogs(apply_id, content, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name());
	}
	
	public DBRow[] getApplyMoneyByIds(String ids, PageCtrl pc) throws Exception {
		return floorApplyMoneyMgrLL.getApplyMoneyByIds(ids, pc);
	}
	
	public DBRow[] getApplyTransferByIds(String ids, PageCtrl pc) throws Exception {
		return floorApplyMoneyMgrLL.getApplyTransferByIds(ids, pc);
	}
	public void setApplyMoneyMgrZZZ(ApplyMoneyMgrIFace applyMoneyMgrZZZ) {
		this.applyMoneyMgrZZZ = applyMoneyMgrZZZ;
	}
	
	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr)
	{
		this.floorTransportMgrZr = floorTransportMgrZr;
	}
	public FloorTransportMgrLL getFloorTransportMgrLL() {
		return floorTransportMgrLL;
	}

	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}

	public FloorApplyMoneyLogsMgrLL getFloorApplyMoneyLogsMgrLL() {
		return floorApplyMoneyLogsMgrLL;
	}

	public void setFloorApplyMoneyLogsMgrLL(
			FloorApplyMoneyLogsMgrLL floorApplyMoneyLogsMgrLL) {
		this.floorApplyMoneyLogsMgrLL = floorApplyMoneyLogsMgrLL;
	}

	public FloorAdminMgrLL getFloorAdminMgrLL() {
		return floorAdminMgrLL;
	}

	public void setFloorAdminMgrLL(FloorAdminMgrLL floorAdminMgrLL) {
		this.floorAdminMgrLL = floorAdminMgrLL;
	}

	public FloorApplyImagesMgrLL getFloorApplyImagesMgrLL() {
		return floorApplyImagesMgrLL;
	}

	public void setFloorApplyImagesMgrLL(FloorApplyImagesMgrLL floorApplyImagesMgrLL) {
		this.floorApplyImagesMgrLL = floorApplyImagesMgrLL;
	}

	public FloorApplyMoneyMgrLL getFloorApplyMoneyMgrLL() {
		return floorApplyMoneyMgrLL;
	}

	public void setFloorApplyMoneyMgrLL(FloorApplyMoneyMgrLL floorApplyMoneyMgrLL) {
		this.floorApplyMoneyMgrLL = floorApplyMoneyMgrLL;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}

	public void setTransportMgrZr(TransportMgrIfaceZr transportMgrZr) {
		this.transportMgrZr = transportMgrZr;
	}
}