package com.cwc.app.api.ll;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.transport.TransportStockInHandleErrorException;
import com.cwc.app.exception.transport.TransportStockOutHandleErrorException;
import com.cwc.app.floor.api.ll.FloorFreightMgrLL;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.iface.ll.TransportMgrIFaceLL;
import com.cwc.app.iface.zj.TransportMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportOutboundApproveMgrIFaceZJ;
import com.cwc.app.iface.zj.TransportOutboundMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zyj.DoorOrLocationOccupancyMgrZyjIFace;
import com.cwc.app.key.ClearanceKey;
import com.cwc.app.key.DeclarationKey;
import com.cwc.app.key.DrawbackKey;
import com.cwc.app.key.InvoiceKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.RepairOrderKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class TransportMgrLL implements TransportMgrIFaceLL {
	static Logger log = Logger.getLogger("ACTION");
	private AdminMgr adminMgr = new AdminMgr();
	private FloorTransportMgrLL floorTransportMgrLL;
	private TransportMgrIFaceZJ transportMgrZJ;
	private TransportOutboundMgrIFaceZJ transportOutboundMgrZJ;
	private TransportOutboundApproveMgrIFaceZJ transportOutboundApproveMgrZJ;
	private FloorFreightMgrLL floorFreightMgrLL;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj;
	
	
	
	public DBRow getTransportById(String id) throws Exception {
		try {
			return floorTransportMgrLL.getTransportById(id);
		} catch (Exception e) {
			throw new SystemException(e,"getTransportById",log);
		}
	}
	
	public DBRow[] getCounty() throws Exception {
		try {
			return floorTransportMgrLL.getCounty();
		} catch (Exception e) {
			throw new SystemException(e,"getCounty",log);
		}
	}
	
	public DBRow getCountyById(String id) throws Exception {
		try {
			return floorTransportMgrLL.getCountyById(id);
		} catch (Exception e) {
			throw new SystemException(e,"getCountyById",log);
		}
	}
	
	public void updateTransport(HttpServletRequest request) throws Exception {
		try {
			DBRow row =Utils.getDBRowByName("transport", request);
			floorTransportMgrLL.updateTransport(row);
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			insertLogs(row.getString("transport_id"), "转运单修改:单号"+row.getString("transport_id"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3);
		} catch (Exception e) {
			throw new SystemException(e,"updateTransport",log);
		}
	}
	
	public void freightTransport(HttpServletRequest request) throws Exception {
		try 
		{
			DBRow row =Utils.getDBRowByName("transport", request);
			floorTransportMgrLL.updateTransport(row);
			
			long transport_id = row.get("transport_id",0l);
			
			transportMgrZJ.editTransportIndex(transport_id,"update");
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			insertLogs(row.getString("transport_id"), "转运单设置运单:单号"+row.getString("transport_id"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateTransport",log);
		}
	}
	
	public void wayoutTransport(HttpServletRequest request) throws Exception {
		try {
			DBRow row =Utils.getDBRowByName("transport", request);
			floorTransportMgrLL.updateTransport(row);
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			insertLogs(row.getString("transport_id"), "转运单设置清关退税:单号"+row.getString("transport_id"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3);
		} catch (Exception e) {
			throw new SystemException(e,"updateTransport",log);
		}
	}
	
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type) throws Exception{
		try {
			floorTransportMgrLL.insertLogs(id, content, oprator_id, oprator, type);
		} catch (Exception e) {
			throw new SystemException(e,"insertLogs",log);
		}
	}
	
	public void transportLogsInsert(HttpServletRequest request) throws Exception {
		try {
			String transport_id = StringUtil.getString(request,"transport_id");
			String transport_content = StringUtil.getString(request,"transport_content");
			int transport_type = StringUtil.getInt(request,"transport_type");
			int type = StringUtil.getInt(request,"type");
			int stage = StringUtil.getInt(request,"stage");
			DBRow transportRow = getTransportById(transport_id);
			TDate tDate = new TDate();
			TDate endDate = new TDate(transportRow.getString("transport_date")+" 00:00:00");
			
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			DBRow row = new DBRow();
			row.add("transport_id", transport_id);
			if(type==1) {
				ClearanceKey clearanceKey = new ClearanceKey();
				transport_content = clearanceKey.getStatusById(stage)+":"+transport_content;
				row.add("clearance", stage);
				if(stage==ClearanceKey.FINISH) {
					row.add("clearance_over", diffDay);
				}
			}else if(type==2) {
				DeclarationKey declarationKey = new DeclarationKey();
				transport_content = declarationKey.getStatusById(stage)+":"+transport_content;
				row.add("declaration", stage);
				if(stage==DeclarationKey.FINISH) {
					row.add("declaration_over", diffDay);
				}
			}else if(type==3) {
				InvoiceKey invoiceKey = new InvoiceKey();
				transport_content = invoiceKey.getInvoiceById(String.valueOf(stage))+":"+transport_content;
				row.add("invoice", stage);
				if(stage==InvoiceKey.FINISH) {
					row.add("invoice_over", diffDay);
				}
			}else if(type==4) {
				DrawbackKey drawbackKey = new DrawbackKey();
				transport_content = drawbackKey.getDrawbackById(String.valueOf(stage))+":"+transport_content;
				row.add("drawback", stage);
				if(stage==DrawbackKey.FINISH) {
					row.add("drawback_over", diffDay);
				}
			}
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			insertLogs(transport_id, transport_content, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),transport_type );
			//更新主表状态
			floorTransportMgrLL.updateTransport(row);
		} catch (Exception e) {
			throw new SystemException(e,"transportLogsInsert",log);
		}
	}
	
	public DBRow[] getTransportLogsByType(long transport_id, int transport_type) throws Exception {
		try {
			return floorTransportMgrLL.getTransportLogsByType(transport_id, transport_type);
		} catch (Exception e) {
			throw new SystemException(e,"getTransportLogsByType",log);
		}
	}
	
	/**
	 * 通过转运单ID，日志类型，查询相应日志
	 */
	public DBRow[] getTransportLogsByTransportIdAndType(long transport, int[] types) throws Exception{
		try {
			return floorTransportMgrLL.getTransportLogsByTransportIdAndType(transport, types);
		} catch (Exception e) {
			throw new SystemException(e,"getTransportLogsByTransportIdAndType",log);
		}
	}
	/**
	 * 通过转运单的ID,查询所有日志
	 */
	public DBRow[] getTransportLogsByTransportId(long transport_id) throws Exception{
		try {
			return floorTransportMgrLL.getTransportLogsByTransportId(transport_id);
		} catch (Exception e) {
			throw new SystemException(e,"getTransportLogsByTransportId",log);
		}
	}
	public void transportRemark(HttpServletRequest request) throws Exception {
		try {
			String transport_id = StringUtil.getString(request,"transport_id");
			String transport_content = StringUtil.getString(request,"transport_content");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			floorTransportMgrLL.insertLogs(transport_id, transport_content, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 1);
		} catch (Exception e) {
			throw new SystemException(e,"transportRemark",log);
		}
	}
	
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception {
		try {
			return floorTransportMgrLL.getAnalysis(st, en, analysisType, analysisStatus, day, pc);
		} catch (Exception e) {
			throw new SystemException(e,"getAnalysis",log);
		}
	}
	
	public void transportAproveCancel(String transport_approve_id,AdminLoginBean adminLoggerBean) throws Exception {
		try 
		{
			DBRow approveRow = floorTransportMgrLL.getTransportApproveById(transport_approve_id);
			String transport_id = approveRow.getString("transport_id");
			DBRow transportOrderRow = floorTransportMgrLL.getTransportById(transport_id);
			
			floorTransportMgrLL.deleteTransportWarehouseByTransportId(transport_id);//删除库存临时表
			String delTransportApproveDetailCond = "where ta_id="+transport_approve_id;
			floorTransportMgrLL.deleteTransportApproveDetailByCond(delTransportApproveDetailCond);//删除审核明细
			floorTransportMgrLL.deleteTransportApproveById(transport_approve_id);//删除转运单审核主表
			
			DBRow transportDetailRow = new DBRow();
			transportDetailRow.add("transport_reap_count", 0);
			String updateTransportDetailCond = "where transport_id = "+transport_id;
			floorTransportMgrLL.updateTransportDetailByCond(updateTransportDetailCond,transportDetailRow);//更新转运单实际入库数量
			//更新转运单状态
			DBRow[] transportDetailRows = floorTransportMgrLL.getTransportDetailByTransportId(transport_id);
			for(int i=0;i<transportDetailRows.length; i++) 
			{
				if(transportDetailRows[i].get("transport_send_count", 0f)==0&&transportDetailRows[i].get("transport_count", 0f)==0)//多到商品删除
				{
					floorTransportMgrLL.deleteTransportDetailById(transportDetailRows[i].getString("transport_detail_id"));
				}
			}
//			status = status!=TransportOrderKey.ERRORINTRANSIT?TransportOrderKey.INTRANSIT:status;
			DBRow transportRow = new DBRow();
			transportRow.add("transport_id", transport_id);
			if(TransportOrderKey.APPROVEING == transportOrderRow.get("transport_status", 0))
			{
				transportRow.add("transport_status", TransportOrderKey.INTRANSIT);
			}
			floorTransportMgrLL.updateTransport(transportRow);
			DBRow transportOrderRowUp = floorTransportMgrLL.getTransportById(transport_id);
			floorTransportMgrLL.insertLogs(transport_id, "取消入库审核:单号"+transport_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 1);
			floorTransportMgrLL.insertLogs(transport_id, "取消入库审核:单号"+transport_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 4);
			scheduleMgrZr.addScheduleReplayExternal(Long.parseLong(transport_id) , ModuleKey.TRANSPORT_ORDER ,
					4 , "转运单:"+transport_id+"取消入库审核" ,
					transportOrderRowUp.get("transport_status",0)==RepairOrderKey.FINISH?true:false, adminLoggerBean, "transport_intransit_period","true");
		} catch (Exception e) {
			throw new SystemException(e,"transportAproveCancel",log);
		}
	}
	
	public DBRow[] getTransportDetailByTransportId(String transport_id) 
		throws Exception
	{
		return floorTransportMgrLL.getTransportDetailByTransportId(transport_id);
	}
	
	public void transportOutboundAproveCancel(String transport_approve_id,AdminLoginBean adminLoggerBean) 
		throws Exception
	{
		try 
		{
			DBRow approveOutboundRow = floorTransportMgrLL.getTransportOutboundApproveById(transport_approve_id);
			String transport_id = approveOutboundRow.getString("transport_id");
			
			String delTransportOutboundApproveDetailCond = "where tsa_id="+transport_approve_id;
			floorTransportMgrLL.deleteTransportOutboundApproveDetailByCond(delTransportOutboundApproveDetailCond);//删除审核明细
			floorTransportMgrLL.deleteTransportOutboundApproveById(transport_approve_id);//删除转运单审核主表
			
			DBRow transportOutboundDetailRow = new DBRow();
			transportOutboundDetailRow.add("transport_send_count", 0);
			String updateTransportOutboundDetailCond = "where transport_id = "+transport_id;
			floorTransportMgrLL.updateTransportOutboundDetailByCond(updateTransportOutboundDetailCond,transportOutboundDetailRow);//更新转运单实际入库数量
			//多出库商品删除
			DBRow[] transportDetailRows = floorTransportMgrLL.getTransportDetailByTransportId(transport_id);
			for(int i=0;i<transportDetailRows.length; i++) {
				if(transportDetailRows[i].get("transport_count", 0d)==0)
					floorTransportMgrLL.deleteTransportDetailById(transportDetailRows[i].getString("transport_detail_id"));
			}
			//更新转运单状态
			DBRow transportRow = new DBRow();
			transportRow.add("transport_id", transport_id);
			transportRow.add("transport_status", TransportOrderKey.PACKING);
			floorTransportMgrLL.updateTransport(transportRow);
			
			floorTransportMgrLL.insertLogs(transport_id, "取消出库审核:单号"+transport_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 1);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"transportAproveCancel",log);
		}
	}
	
	public DBRow getDrawback(String transport_id) throws Exception {
		return floorTransportMgrLL.getDrawback(transport_id);
	}
	
	public void transportSetDrawback(HttpServletRequest request) 
		throws Exception 
	{
		DBRow row =Utils.getDBRowByName("transport_drawback", request);
		
		floorTransportMgrLL.transportSetDrawback(row);
	}
	
	public FloorFreightMgrLL getFloorFreightMgrLL() {
		return floorFreightMgrLL;
	}

	public void setFloorFreightMgrLL(FloorFreightMgrLL floorFreightMgrLL) {
		this.floorFreightMgrLL = floorFreightMgrLL;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public TransportOutboundMgrIFaceZJ getTransportOutboundMgrZJ() {
		return transportOutboundMgrZJ;
	}

	public void setTransportOutboundMgrZJ(
			TransportOutboundMgrIFaceZJ transportOutboundMgrZJ) {
		this.transportOutboundMgrZJ = transportOutboundMgrZJ;
	}

	public TransportOutboundApproveMgrIFaceZJ getTransportOutboundApproveMgrZJ() {
		return transportOutboundApproveMgrZJ;
	}

	public void setTransportOutboundApproveMgrZJ(
			TransportOutboundApproveMgrIFaceZJ transportOutboundApproveMgrZJ) {
		this.transportOutboundApproveMgrZJ = transportOutboundApproveMgrZJ;
	}

	public TransportMgrIFaceZJ getTransportMgrZJ() {
		return transportMgrZJ;
	}

	public void setTransportMgrZJ(TransportMgrIFaceZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

	public FloorTransportMgrLL getFloorTransportMgrLL() {
		return floorTransportMgrLL;
	}

	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}
	
	public void setDoorOrLocationOccupancyMgrZyj(
			DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj) {
		this.doorOrLocationOccupancyMgrZyj = doorOrLocationOccupancyMgrZyj;
	}
}
