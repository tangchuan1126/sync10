package com.cwc.app.api.ccc;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.ccc.FloorFreightResourcesMgrCCC;
import com.cwc.app.iface.ccc.FreightResourcesMgrIFaceCCC;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class FreightResourcesMgrCCC implements FreightResourcesMgrIFaceCCC{

	static Logger log = Logger.getLogger("ACTION");
	private FloorFreightResourcesMgrCCC floorFreightResourcesMgrCCC;
	
	public FloorFreightResourcesMgrCCC getFloorFreightResourcesMgrCCC() {
		return floorFreightResourcesMgrCCC;
	}

	public void setFloorFreightResourcesMgrCCC(
			FloorFreightResourcesMgrCCC floorFreightResourcesMgrCCC) {
		this.floorFreightResourcesMgrCCC = floorFreightResourcesMgrCCC;
	}
	
	
	public void addFreightResources(HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		//long fr_id = StrUtil.getLong(request,"fr_id");
		String fr_company = StringUtil.getString(request,"fr_company");
		
		DBRow freight_resources = new DBRow();
		//freight_resources.add("fr_id", fr_id);
		freight_resources.add("fr_company", fr_company);
		freight_resources.add("fr_way", StringUtil.getLong(request,"fr_way"));
		freight_resources.add("fr_undertake_company", StringUtil.getString(request,"fr_undertake_company"));
		freight_resources.add("fr_from_country", StringUtil.getLong(request,"fr_from_country"));
		freight_resources.add("fr_to_country", StringUtil.getLong(request,"fr_to_country"));
		freight_resources.add("fr_from_port", StringUtil.getString(request,"fr_from_port"));
		freight_resources.add("fr_to_port", StringUtil.getString(request,"fr_to_port"));
		freight_resources.add("fr_contact", StringUtil.getString(request,"fr_contact"));
		freight_resources.add("fr_contact_tel", StringUtil.getString(request,"fr_contact_tel"));
		freight_resources.add("fr_delivery_address", StringUtil.getString(request,"fr_delivery_address"));
		freight_resources.add("fr_remark", StringUtil.getString(request,"fr_remark"));
		floorFreightResourcesMgrCCC.addFreightResources(freight_resources);
		
	}
	
	public DBRow[] getFreightResourcesByCondition(HttpServletRequest request,PageCtrl pc)
    throws Exception
    {
    	try
    	{
    		String cmd = StringUtil.getString(request, "cmd");
    		String company = StringUtil.getString(request, "company");
	    	String fr_company=StringUtil.getString(request, "fr_company");	
	    	String fr_undertake_company=StringUtil.getString(request,"fr_undertake_company");
	    	String fr_way = StringUtil.getString(request, "fr_way");
	    	String fr_from_country = StringUtil.getString(request, "fr_from_country");
	    	String fr_from_port = StringUtil.getString(request, "fr_from_port");
	    	String fr_to_country = StringUtil.getString(request, "fr_to_country");
	    	String fr_to_port = StringUtil.getString(request, "fr_to_port");
	    	
	    	return (floorFreightResourcesMgrCCC.getFreightResourcesByCondition(fr_company,fr_undertake_company,fr_way,fr_from_country,fr_from_port,fr_to_country,fr_to_port,cmd,company,pc));
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"getFreightResourcesByCondition",log);
    	}
    }

	@Override
	public DBRow[] listFreightResources(HttpServletRequest request,PageCtrl pc)
			throws Exception {
		try
    	{
	    	String fr_company=StringUtil.getString(request, "fr_company");	
	    	String fr_undertake_company=StringUtil.getString(request,"fr_undertake_company");
	    	
	    	return (floorFreightResourcesMgrCCC.listFreightResources(fr_company,fr_undertake_company,pc));
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"getFreightResourcesByCondition",log);
    	}
	}

	@Override
	public DBRow getFreightResourcesById(String fr_id)
			throws Exception {
		// TODO Auto-generated method stub
		DBRow dbrow = null;
		if(!fr_id.equals("")){
			dbrow =floorFreightResourcesMgrCCC.getFreightResourcesById(fr_id);
		}
		return dbrow;
	}

	@Override
	public long updateFreightResources(HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		String fr_company = StringUtil.getString(request,"fr_company");
		
		DBRow freight_resources = new DBRow();
		freight_resources.add("fr_id", StringUtil.getLong(request,"fr_id"));
		long fr_id = StringUtil.getLong(request,"fr_id"); 
		freight_resources.add("fr_company", fr_company);
		freight_resources.add("fr_way", StringUtil.getLong(request,"fr_way"));
		freight_resources.add("fr_undertake_company", StringUtil.getString(request,"fr_undertake_company"));
		freight_resources.add("fr_from_country", StringUtil.getLong(request,"fr_from_country"));
		freight_resources.add("fr_to_country", StringUtil.getLong(request,"fr_to_country"));
		freight_resources.add("fr_from_port", StringUtil.getString(request,"fr_from_port"));
		freight_resources.add("fr_to_port", StringUtil.getString(request,"fr_to_port"));
		freight_resources.add("fr_contact", StringUtil.getString(request,"fr_contact"));
		freight_resources.add("fr_contact_tel", StringUtil.getString(request,"fr_contact_tel"));
		freight_resources.add("fr_delivery_address", StringUtil.getString(request,"fr_delivery_address"));
		freight_resources.add("fr_remark", StringUtil.getString(request,"fr_remark"));
		floorFreightResourcesMgrCCC.updateFreightResources(freight_resources,fr_id);
		return  fr_id;
		
	}

	@Override
	public void deleteFreightResources(HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		String fr_id = StringUtil.getString(request,"fr_id");
		if(fr_id != null && !fr_id.equals("")){
			long frid = Long.valueOf(fr_id);
			floorFreightResourcesMgrCCC.deleteFreightResources(frid);
		}
	}

	@Override
	public DBRow getCountryById(String id) throws Exception {
		// TODO Auto-generated method stub
		DBRow dbrow = null;
		if(!id.equals("")){
			dbrow =floorFreightResourcesMgrCCC.getCountryById(id);
		}
		return dbrow;
	}

}
