package com.cwc.app.api.ccc;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.ccc.FloorFreightCostMgrCCC;
import com.cwc.app.iface.ccc.FreightCostMgrIFaceCCC;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class FreightCostMgrCCC implements FreightCostMgrIFaceCCC{

	static Logger log = Logger.getLogger("ACTION");
	private FloorFreightCostMgrCCC floorFreightCostMgrCCC;
	
	public FloorFreightCostMgrCCC getFloorFreightCostMgrCCC() {
		return floorFreightCostMgrCCC;
	}

	public void setFloorFreightCostMgrCCC(
			FloorFreightCostMgrCCC floorFreightCostMgrCCC) {
		this.floorFreightCostMgrCCC = floorFreightCostMgrCCC;
	}

	@Override
	public void addFreightCost(HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		
		DBRow freight_cost = new DBRow();
		freight_cost.add("fc_project_name", StringUtil.getString(request,"fc_project_name"));
		freight_cost.add("fc_way", StringUtil.getString(request,"fc_way"));
		freight_cost.add("fc_unit", StringUtil.getString(request,"fc_unit"));
		freight_cost.add("fc_unit_price", StringUtil.getDouble(request,"fc_unit_price"));
		freight_cost.add("fr_id", StringUtil.getLong(request,"fr_id"));
		floorFreightCostMgrCCC.addFreightCost(freight_cost);
		
	}
	
	public DBRow[] getFreightCostByCondition(String fr_id,HttpServletRequest request,PageCtrl pc)
    throws Exception
    {
		try
    	{
	    	return (floorFreightCostMgrCCC.getFreightCostByCondition(fr_id,pc));
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"getFreightCostByCondition",log);
    	}
    }

	@Override
	public DBRow[] listFreightCost(HttpServletRequest request,PageCtrl pc)
			throws Exception {
		try
    	{
	    	String fr_company=StringUtil.getString(request, "fr_company");	
	    	String fr_undertake_company=StringUtil.getString(request,"fr_undertake_company");
	    	
	    	return (floorFreightCostMgrCCC.listFreightCost(fr_company,fr_undertake_company,pc));
    	}
    	catch(Exception e)
    	{
    		throw new SystemException(e,"listFreightCost",log);
    	}
	}

	@Override
	public DBRow getFreightCostById(String fc_id)
			throws Exception {
		// TODO Auto-generated method stub
		DBRow dbrow = null;
		if(!fc_id.equals("")){
			dbrow =floorFreightCostMgrCCC.getFreightCostById(fc_id);
		}
		return dbrow;
	}

	@Override
	public long updateFreightCost(HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		
		DBRow freight_cost = new DBRow();
		long fc_id = !StringUtil.getString(request,"fc_id").equals("")?Long.valueOf(StringUtil.getString(request,"fc_id")):0;
		freight_cost.add("fc_project_name", StringUtil.getString(request,"fc_project_name"));
		freight_cost.add("fc_way", StringUtil.getString(request,"fc_way"));
		freight_cost.add("fc_unit", StringUtil.getString(request,"fc_unit"));
		freight_cost.add("fc_unit_price", StringUtil.getDouble(request,"fc_unit_price"));
		freight_cost.add("fr_id", StringUtil.getLong(request,"fr_id"));
		floorFreightCostMgrCCC.updateFreightCost(freight_cost,fc_id);
		return  fc_id;
		
	}

	@Override
	public void deleteFreightCost(HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		String fc_id = StringUtil.getString(request,"fc_id");
		if(fc_id != null && !fc_id.equals("")){
			long fcid = Long.valueOf(fc_id);
			floorFreightCostMgrCCC.deleteFreightCost(fcid);
		}
	}

	@Override
	public DBRow getCountryById(String id) throws Exception {
		// TODO Auto-generated method stub
		DBRow dbrow = null;
		if(!id.equals("")){
			dbrow =floorFreightCostMgrCCC.getCountryById(id);
		}
		return dbrow;
	}
	

	public boolean changeFreightIndex(HttpServletRequest request)
		throws Exception {
		String fc_id = StringUtil.getString(request,"fc_id");
		int freight_index = StringUtil.getInt(request,"freight_index");
		
		return floorFreightCostMgrCCC.changeFreightIndex(fc_id, freight_index);
	}
}
