package com.cwc.app.api.ccc;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.ccc.FloorComputeProductMgrCCC;
import com.cwc.app.iface.ccc.ComputeProductMgrIFaceCCC;
import com.cwc.db.DBRow;

public class ComputeProductMgrCCC implements ComputeProductMgrIFaceCCC {

	static Logger log = Logger.getLogger("ACTION");
	private FloorComputeProductMgrCCC floorComputeProductMgrCCC;
	
	public FloorComputeProductMgrCCC getFloorComputeProductMgrCCC() {
		return floorComputeProductMgrCCC;
	}

	public void setFloorComputeProductMgrCCC(
			FloorComputeProductMgrCCC floorComputeProductMgrCCC) {
		this.floorComputeProductMgrCCC = floorComputeProductMgrCCC;
	}

	/**
	 * 计算库存成本
	 */
	@Override
	public void computeFreightForDelivery(int delivery_order_id) throws Exception {
		// TODO Auto-generated method stub
		this.floorComputeProductMgrCCC.computeFreightForDelivery(delivery_order_id);
	}

	public DBRow getSumTransportFreightCost(String transport_id) throws Exception {
		return floorComputeProductMgrCCC.getSumTransportFreightCost(Long.parseLong(transport_id));
	}
	
	public DBRow getSumDeliveryFreightCost(String delivery_order_id) throws Exception {
		return floorComputeProductMgrCCC.getSumDeliveryFreightCost(delivery_order_id);
	}
}
