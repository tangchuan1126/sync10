package com.cwc.app.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductNotProfitException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.iface.CustomCartQuoteIFace;
import com.cwc.app.util.Config;
import com.cwc.db.*;
import com.cwc.exception.SystemException;
import com.cwc.util.*;

/**
 * 定制购物车
 * 
 * 购物车结构：
 * 
 * 一个购物车应该包含两大块信息：购物车信息和购物车商品信息
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CustomCartQuote implements CustomCartQuoteIFace
{
	static Logger log = Logger.getLogger("ACTION");
	
	private ProductMgr pm;
	private QuoteMgr quoteMgr;

	/**
	 * 增加商品到购物车
	 * @param session
	 * @param pid			商品ID
	 * @param quantity		商品数量
	 * @param product_type	商品类型
	 * @throws Exception
	 */
	public void put2Cart(HttpSession session,long set_pid,long pid,float quantity) 
		throws  Exception
	{
		if ( pid != 0 )
		{
			String setPidKey = String.valueOf(set_pid);
			
			if (quantity<=0)
			{
				quantity = 1;
			}

			/**
			 * 该session存放大购物车中所有定制商品和定制里面的子商品ID
			 * 
			 * 一对多关系！
			 */
			HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.customUnionCartQuoteSession);
			
			if ( setProduct == null||setProduct.get(setPidKey)==null )
			{
				//记录定制下的子商品
				DBRow row = new DBRow();
				row.add("cart_pid",pid);
				row.add("cart_quantity",quantity);
				
				ArrayList<DBRow> al = new ArrayList<DBRow>();
				al.add(row);

				if (setProduct == null)
				{
					setProduct = new HashMap<String, ArrayList<DBRow>>();
				}
				setProduct.put(setPidKey, al);
			}
			else
			{
				ArrayList<DBRow> al = (ArrayList)setProduct.get(setPidKey);
				dealWithRepeat(al,pid,quantity);
				setProduct.put(setPidKey, al);
			}

			session.setAttribute(Config.customUnionCartQuoteSession,setProduct);			
		}
	}

	/**
	 * 大购物车初始化定制购物车
	 * @param session
	 * @param set_pid
	 * @param pid
	 * @param quantity
	 * @throws Exception
	 */
	public void put2FinalCart(HttpSession session,long set_pid,long pid,float quantity) 
		throws  Exception
	{
		String setPidKey = String.valueOf(set_pid);
		HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.customUnionCartQuoteSession);
		
		if ( setProduct == null||setProduct.get(setPidKey)==null )
		{
			//记录定制下的子商品
			DBRow row = new DBRow();
			row.add("cart_pid",pid);
			row.add("cart_quantity",quantity);
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			al.add(row);

			if (setProduct == null)
			{
				setProduct = new HashMap<String, ArrayList<DBRow>>();
			}
			setProduct.put(setPidKey, al);
		}
		else
		{
			ArrayList<DBRow> al = (ArrayList)setProduct.get(setPidKey);
			dealWithRepeat(al,pid,quantity);
			setProduct.put(setPidKey, al);
		}

		session.setAttribute(Config.customUnionCartQuoteSession,setProduct);			
	}
	
	/**
	 * 处理重复提交商品
	 * @param pid
	 * @param quantity
	 * @return		返回true：有旧商品，修改了数量  返回alse：新增加商品
	 * @throws Exception 
	 */
	private boolean dealWithRepeat(ArrayList<DBRow> al,long pid,float quantity) 
		throws  Exception
	{
		DBRow productOld;
		
		for ( int i=0;i<al.size(); i++ )
		{
			productOld = (DBRow)al.get(i);		//获得购物车一行记录
			
			if ( StringUtil.getLong(productOld.getString("cart_pid")) == pid )		
			{
				float t = StringUtil.getFloat(productOld.getString("cart_quantity")) + quantity;
				productOld.add("cart_quantity",t);
				al.set(i,productOld);
				return(true);
			}
		}

		//没有重复商品，则作为新纪录
		DBRow row = new DBRow();
		row.add("cart_pid",pid);
		row.add("cart_quantity",quantity);
		
		al.add(row);
		
		return(false);
	}

	/**
	 * 清空购物车
	 */
	public void clearCart(HttpSession session)
	{
		if ( session.getAttribute(Config.customUnionCartQuoteSession) != null )
		{
			session.removeAttribute(Config.customUnionCartQuoteSession);
		}

		if ( session.getAttribute(Config.finalCustomUnionCartQuoteSesion) != null )
		{
			session.removeAttribute(Config.finalCustomUnionCartQuoteSesion);
		}
	}
	
	/**
	 * 判断购物车是否为空
	 * @return
	 */
	public boolean isEmpty(HttpSession session,long set_pid)
	{
		return( getSimpleProducts(session, set_pid).length==0 );
	}
	
	/**
	 * 从session获得购物车商品
	 * @return
	 */
	public DBRow[] getSimpleProducts(HttpSession session,long set_pid)
	{
		HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.customUnionCartQuoteSession);
		
		if ( setProduct == null||setProduct.get(String.valueOf(set_pid))==null )
		{
			return(new DBRow[0]);
		}
		else
		{
			ArrayList<DBRow> al = (ArrayList)setProduct.get(String.valueOf(set_pid));
			ArrayList newAL = (ArrayList)al.clone();
			return((DBRow[])newAL.toArray(new DBRow[0]));			
		}
	}

	/**
	 * 获得更丰富购物车商品信息
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDetailProducts(HttpSession session,long set_pid)
		throws Exception
	{
		DBRow products[] = this.getSimpleProducts(session, set_pid);
		DBRow detailP;
		
		try
		{
			for (int i=0; i<products.length; i++)
			{
				detailP = pm.getDetailProductByPcid(StringUtil.getLong(products[i].getString("cart_pid")));
				products[i].append(detailP);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProducts",log);
		}
		
		return(products);
	}

	/**
	 * 从购物车移除商品
	 * @throws Exception 
	 * @throws  
	 *
	 */
	public void removeProduct(HttpSession session,long set_pid,long pid)
		throws Exception
	{
		HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.customUnionCartQuoteSession);
		
		if ( setProduct != null )
		{
			DBRow rowOld;
			ArrayList al = (ArrayList)setProduct.get(String.valueOf(set_pid));
			
			for ( int i=0;i<al.size(); i++ )
			{
				rowOld = (DBRow)al.get(i);
				if ( StringUtil.getLong(rowOld.getString("cart_pid")) == pid )
				{
					al.remove(i);
				}
			}
			
			setProduct.put(String.valueOf(set_pid), al);
			session.setAttribute(Config.customUnionCartQuoteSession,setProduct);
		}
	}
	
	public void removeSetProduct(HttpSession session,long set_pid)
		throws Exception
	{

			HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.customUnionCartQuoteSession);
			
			if ( setProduct != null )
			{
				setProduct.remove(String.valueOf(set_pid));
				session.setAttribute(Config.customUnionCartQuoteSession,setProduct);
			}

	}

	/**
	 * 批量修改商品数量
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	public void modQuantity(HttpSession session,long set_pid,String pids[],String quantitys[]) 
		throws  Exception
	{
		HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.customUnionCartQuoteSession);
		
		if ( setProduct != null )
		{
			ArrayList al = (ArrayList)setProduct.get(String.valueOf(set_pid));
			DBRow product;
			float quantity;
			
			for (int i=0; i<pids.length; i++)
			{
				for (int j=0; j<al.size(); j++)
				{
					product = (DBRow)al.get(j);
					if ( product.getString("cart_pid").equals(pids[i]) )
					{
						quantity = StringUtil.getFloat(quantitys[i]);
						if ( quantity<=0 )
						{
							quantity = 1;
						}
						product.add("cart_quantity",quantity);
					}
					al.set(j,product);
				}
			}
			setProduct.put(String.valueOf(set_pid), al);
			session.setAttribute(Config.customUnionCartQuoteSession,setProduct);
		}
	}

	/**
	 * 批量修改商品数量
	 * @param request
	 * @throws Exception
	 */
	public void modQuantity(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long set_pid = StringUtil.getLong(request, "set_pid");
			String pids[] = request.getParameterValues("pids");
			String quantitys[] = request.getParameterValues("quantitys");
			
			modQuantity(StringUtil.getSession(request),set_pid, pids, quantitys);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modQuantity",log);
		}
	}
	
	/**
	 * 把商品添加到购物车
	 * @param request
	 * @throws Exception
	 */
	public void put2Cart(HttpServletRequest request)
		throws ProductNotProfitException,ProductUnionSetCanBeProductException,ProductNotCreateStorageException,ProductNotExistException,Exception
	{
		try
		{
			String p_name = StringUtil.getString(request, "p_name");
			float quantity = StringUtil.getFloat(request, "quantity");
			long ps_id = StringUtil.getLong(request, "ps_id");
			long set_pid = StringUtil.getLong(request, "set_pid");
			long ccid = StringUtil.getLong(request, "ccid");
			
			long pid = 0;

			//先检测商品是否存在，存在则取得PID
			DBRow detailP = pm.getDetailProductByPname(p_name);
			if (detailP==null||detailP.get("alive", 0)==0) 
			{
				throw new ProductNotExistException();
			}
			else
			{
				pid = detailP.get("pc_id", 0l);
			}

			//套装不能放入定制
			if (detailP.get("union_flag", 0)==1)
			{
				throw new ProductUnionSetCanBeProductException("ProductUnionSetCanBeProductException");
			}
			
//			//验证商品是否设置了毛利
//			if ( quoteMgr.getDetailProductProfitByPidCcid(pid, ccid)==null )
//			{
//				throw new ProductNotProfitException();//暂时屏蔽
//			}

			/**
			 * 检测商品在发货仓库是否已经建库
			 * 套装商品：标准套装或者散件，至少其中一样需要建库
			 */
			DBRow product = new DBRow();
			product.add("cart_pid", pid);
			product.add("cart_product_type", 0);
			DBRow products[] = new DBRow[] {product} ;
			pm.validateProductsCreateStorage(ps_id,products);
			
			this.put2Cart(StringUtil.getSession(request),set_pid, pid, quantity);
		}
		catch (ProductNotProfitException e)
		{
			throw e;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (ProductNotCreateStorageException e)
		{
			throw e;
		}
		catch (ProductUnionSetCanBeProductException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"put2Cart",log);
		}
	}
	
	/**
	 * 不能直接复制hashmap，因为传递的是指针
	 */
	public void copy2FianlSession(HttpSession session)
	{
		HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.customUnionCartQuoteSession);
		
		if (setProduct!=null)
		{
			HashMap finalSetProduct = new HashMap();
			
			Iterator iter = setProduct.entrySet().iterator();
			while (iter.hasNext()) {
			    Map.Entry entry = (Map.Entry) iter.next();
			    String key = (String)entry.getKey();
			    ArrayList val = (ArrayList)entry.getValue();
			    
			    finalSetProduct.put(key, val.clone());
			} 
			
			session.setAttribute(Config.finalCustomUnionCartQuoteSesion, finalSetProduct);
		}
		
	}

	/**
	 * 获得最终确定的定制内容
	 * @param session
	 * @param set_pid
	 * @return
	 */
	public DBRow[] getFinalSimpleProducts(HttpSession session,long set_pid)
	{
		HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.finalCustomUnionCartQuoteSesion);
		
		if ( setProduct == null||setProduct.get(String.valueOf(set_pid))==null )
		{
			return(new DBRow[0]);
		}
		else
		{
			ArrayList<DBRow> al = (ArrayList)setProduct.get(String.valueOf(set_pid));
			ArrayList newAL = (ArrayList)al.clone();
			return((DBRow[])newAL.toArray(new DBRow[0]));			
		}
	}

	/**
	 * 获得最终定制的商品信息
	 * @param session
	 * @param set_pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFinalDetailProducts(HttpSession session,long set_pid)
		throws Exception
	{
		DBRow products[] = this.getFinalSimpleProducts(session, set_pid);
		DBRow detailP;

		try
		{
			for (int i=0; i<products.length; i++)
			{
				detailP = pm.getDetailProductByPcid(StringUtil.getLong(products[i].getString("cart_pid")));
				products[i].append(detailP);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getFinalDetailProducts",log);
		}
		
		return(products);
	}

	public void copyFinalSet2TempSet(HttpSession session,long set_pid)
	{
		HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.customUnionCartQuoteSession);
		HashMap<String, ArrayList<DBRow>> finalSetProduct = (HashMap)session.getAttribute(Config.finalCustomUnionCartQuoteSesion);
		
		setProduct.put(String.valueOf(set_pid), (ArrayList)finalSetProduct.get(String.valueOf(set_pid)).clone());
			
		session.setAttribute(Config.customUnionCartQuoteSession, setProduct);
	}

	/**
	 * 从定制页面的购物车拷贝到大购物车的session中
	 * @param session
	 * @param set_pid			定制前的商品ID
	 * @param new_set_pid		定制后的商品ID
	 */
	public void copyTempSet2FinalSet(HttpSession session,long set_pid,long new_set_pid)
	{
		HashMap<String, ArrayList<DBRow>> setProduct = (HashMap)session.getAttribute(Config.customUnionCartQuoteSession);//定制页面购物车
		HashMap<String, ArrayList<DBRow>> finalSetProduct = (HashMap)session.getAttribute(Config.finalCustomUnionCartQuoteSesion);
		
		if (finalSetProduct==null)
		{
			finalSetProduct = new HashMap();
		}
		
		//防止连续快速两次点击鼠标
		if (setProduct!=null)
		{
			ArrayList products = (ArrayList)setProduct.get(String.valueOf(set_pid)).clone();
			
			//先把旧商品ID数据移除
			setProduct.remove(String.valueOf(set_pid));
			finalSetProduct.remove(String.valueOf(set_pid));
			
			setProduct.put(String.valueOf(new_set_pid), products);//因为重新编辑已经定制过的套装需要这个ID
			finalSetProduct.put(String.valueOf(new_set_pid), products);

			session.setAttribute(Config.finalCustomUnionCartQuoteSesion, finalSetProduct);
			session.setAttribute(Config.customUnionCartQuoteSession, setProduct);
		}
		else
		{
			log.warn("copyTempSet2FinalSet:setProduct is NULL!");
		}
	}
	
	
	
	public void setQuoteMgr(QuoteMgr quoteMgr)
	{
		this.quoteMgr = quoteMgr;
	}

	public void setPm(ProductMgr pm)
	{
		this.pm = pm;
	}

	
}






