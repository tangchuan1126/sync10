package com.cwc.app.api.cc;


import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.floor.api.wp.FloorFolderInfoMgrWp;
import com.cwc.app.floor.api.wp.FloorKmlInfoMgrWp;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.iface.wp.CreateKmlMgrIfaceWp;
import com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp;
import com.cwc.app.key.WebcamPositionTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.JsonException;
import com.cwc.exception.SystemException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class GoogleMapsMgrCc implements GoogleMapsMgrIfaceCc {
	static Logger log = Logger.getLogger("ACTION");
	AdminMgrIFace adminMgr;
	private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;
	private FloorFolderInfoMgrWp floorFolderInfoMgrWp;
	private FloorKmlInfoMgrWp floorKmlInfoMgrWp;
	private CreateKmlMgrIfaceWp createKmlMgrWp;
	private QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp;
	private static Map storageBounds = new HashMap();
	
	 
	@Override
	public DBRow getGoogleMapsBoundaries(HttpServletRequest request) throws Exception {
		String regionIds = StringUtil.getString(request, "regionIds");
		String type = StringUtil.getString(request, "type");
		int psId = StringUtil.getInt(request, "psId");
		String layer = StringUtil.getString(request,"layer","");
		
		//仓库kml
		if("kml_storage".equals(type.toLowerCase())){
			//return getBoundariesFromKml(psId,layer);
			return getLayerFromDB(psId,layer);
		}else{  //地图边界
			return getBoundariesFromDB(regionIds,type);
		}
	}
	
	//查询地界
	public DBRow getBoundariesFromDB(String regionIds,String type) throws Exception {
		DBRow res = new DBRow();
		DBRow[] boundary = null;
		if("country".equals(type)){
			boundary = floorGoogleMapsMgrCc.getGoogleMapsBoundaryByCountry(regionIds);
		}else if("province".equals(type)){
			boundary = floorGoogleMapsMgrCc.getGoogleMapsBoundaryByProvince(regionIds);
		}
		res.add("boundary", boundary);
		return res;
	}
	/**
	 * 解析边界坐标
	 * @param boundaries
	 * @return
	 * @throws DocumentException
	 */
	public List parseBoundaries(String boundaries) throws DocumentException{
		if("".equals(boundaries)){//字段为空
			return null;
		}
		List<List<List<String>>> result = new ArrayList<List<List<String>>>();
		
		Document doc = new SAXReader().read(new StringReader(boundaries));
		Element root = (Element) doc.getRootElement();
		List<String> coordinates = new ArrayList<String>();
		if("MultiGeometry".equals(root.getName())){
			List<Element> polygons=root.elements("Polygon");
			for (Element p : polygons) {
				String polygon = p.element("outerBoundaryIs").element("LinearRing").element("coordinates").getTextTrim();
				if(!"".equals(polygon)){
					coordinates.add(polygon);
				}
			}
		}else if("Polygon".equals(root.getName())){
			String polygon = root.element("outerBoundaryIs").element("LinearRing").element("coordinates").getTextTrim();
			if(!"".equals(polygon)){
				coordinates.add(polygon);
			}
		}
		if(coordinates == null || coordinates.size()==0){return null;}//无坐标数据
		for (String regionStr : coordinates) {
			List<List<String>> region = new ArrayList<List<String>>();
			String[] latlngs = regionStr.split(" ");
			for (String s : latlngs) {
				List<String> latlng = new ArrayList<String>();
				latlng.add(s.split(",")[0].trim());
				latlng.add(s.split(",")[1].trim());
				region.add(latlng);
			}
			result.add(region);
		}
		return result;
	}
	//查询仓库区域坐标
	public DBRow getBoundariesFromKml(int psId,String layer) throws Exception {
		DBRow kmlRow = getStorageKmlByPsId(psId);
		String name = kmlRow.getString("kml");
		if(!"".equals(layer)){
			name = layer+"_"+name;
		}
		DBRow row = (DBRow)storageBounds.get(name);
		DBRow res = new DBRow();
		res.add("flag", "false");
		if(row == null){
			row = new DBRow();
			try {
				String temp_url = Environment.getHome()+"upload/kml/"+name;
				Document doc = new SAXReader().read(new FileInputStream(new File(temp_url)));
				Element root = (Element) doc.getRootElement().elements().get(0);
				
				List<Element> folders=root.elements("Folder");
				for (Element folder : folders) {
					String folderName = folder.element("name").getTextTrim();
					if(folderName.indexOf("WarehouseBase")!=0) continue;
					
					List<Element> placemarks = folder.elements("Placemark");
					for (Element placemark : placemarks) {
						String placemarkName = placemark.element("name").getTextTrim();
						String bound = null;
						if(placemarkName.indexOf("area")==0 || placemarkName.indexOf("base")==0){  //线
							bound = placemark.element("LineString").element("coordinates").getTextTrim();
							row.add(placemarkName,bound);
						}
						if(placemarkName.indexOf("staging")==0 || placemarkName.indexOf("location")==0 || 
								placemarkName.indexOf("parking")==0 || placemarkName.indexOf("docks")==0){ //多边形
							bound = placemark.element("Polygon").element("outerBoundaryIs").element("LinearRing").element("coordinates").getTextTrim();
							//location打上area标记，方便页面分区域处理
							if(placemarkName.indexOf("location")==0){
								String description = placemark.element("description").getTextTrim();
								String area = description.split("_", 3)[2];
								String[] p = placemarkName.split("_",2);
								placemarkName = p[0]+"["+area+"]_"+p[1]; 
							}
							row.add(placemarkName,bound);
						}
					}
				}
				storageBounds.put(name, row);
			} catch (Exception e) {
				throw new Exception("GoogleMapsMgrCc.getBoundariesFromKml:"+e);
			}
		}
		res.add("flag", "true");
		res.add("bounds", row);
		return row;
	}
	public DBRow getLayerFromDB(int psId,String layer) throws Exception {
		DBRow row = new DBRow();
		DBRow res = new DBRow();
		res.add("flag", "false");
		if("".equals(layer)){
			DBRow[] bases=floorFolderInfoMgrWp.getExcelBaseData(Long.toString(psId));
			DBRow[] stagings=floorFolderInfoMgrWp.getExcelStagingData(Long.toString(psId));
			DBRow[] docks=floorFolderInfoMgrWp.getExcelDocksData(Long.toString(psId));
			DBRow[] parkings=floorFolderInfoMgrWp.getExcelParkingData(Long.toString(psId));
			DBRow[] areas=floorFolderInfoMgrWp.getExcelAreaData(Long.toString(psId)); 
			String baseValue="";
			String _base="";
			for(int i=0;i<bases.length;i++){
				if(bases[i].getString("type").equals("warehouse")){
					if("".equals(baseValue)){
						_base=bases[i].getString("lng")+","+bases[i].getString("lat")+",0.0";	
					}
					String 	lat =bases[i].getString("lat");
					String 	lng =bases[i].getString("lng");
					String latlng =lng+","+lat;
					baseValue+=latlng;
					baseValue+=",0.0 ";
				}
			}
			baseValue+=_base;
			row.add("base",baseValue);
			for(DBRow dock :docks){
				String 	latlng =dock.getString("latlng");
				String 	doorId =dock.getString("doorId");
				String 	door_key="docks_"+doorId;
				row.add(door_key,latlng);
			}
			for(DBRow stging :stagings){
				String 	latlng =stging.getString("latlng");
				String 	location_name =stging.getString("location_name");
				String 	staging_key="staging_"+location_name;
				row.add(staging_key,latlng);
			}
			for(DBRow parking :parkings){
				String 	latlng =parking.getString("latlng");
				String 	yc_no =parking.getString("yc_no");
				String 	parking_key="parking_"+yc_no;
				row.add(parking_key,latlng);
			}
			for(DBRow area :areas){
				String 	latlng =area.getString("latlng");
				String 	area_name =area.getString("area_name");
				String 	area_key="area_"+area_name;
				row.add(area_key,latlng);
			}
		}else{
			DBRow[] locations=floorFolderInfoMgrWp.getExcelLocationData(Long.toString(psId)); 
			for(DBRow location :locations){
				String 	latlng =location.getString("latlng");
				String 	area_name =location.getString("area_name");
				String slc_position_all=location.getString("slc_position_all");
				String slc_position=location.getString("slc_position");
				String 	location_key="location["+area_name+"]_"+slc_position_all+"_"+slc_position;
				row.add(location_key,latlng);
			}
			
		}
		res.add("flag", "true");
		res.add("bounds", row);
		return row;
	}
	@Override
	public DBRow[] getAllAssetWithGroup() throws Exception {
		return floorGoogleMapsMgrCc.getAllAsset();
	}

	@Override
	public DBRow[] getContextCommand() throws Exception {
		return floorGoogleMapsMgrCc.getContextCommand();
	}

	@Override
	public DBRow[] getAllStorageKml() throws Exception {
		
		return floorGoogleMapsMgrCc.getAllStorageKml();
	}
	@Override
	public DBRow[] getAllStorageKmlByAdmin(AdminLoginBean bean) throws Exception {
		long adgid =bean.getAdgid();
		long psId =bean.getPs_id();
		if(adgid==10000){
			return  floorGoogleMapsMgrCc.getAllStorageKml();
		}else {
			DBRow r = floorGoogleMapsMgrCc.getStorageKmlByPsId((int)psId);
			DBRow[] rs = {r};
			return rs;
		}
	}

	@Override
	public DBRow[] getParkingDocksOccupancy(HttpServletRequest request)
			throws Exception {
		long psId = StringUtil.getLong(request,"psId");
		DBRow row = new DBRow();
		row.add("psId", psId);
		return floorGoogleMapsMgrCc.getParkingDocksOccupancy(psId);
	}
	@Override
	public DBRow[] getParkingDocksOccupancyNew(HttpServletRequest request)
			throws Exception {
		long psId = StringUtil.getLong(request,"psId");
		DBRow row = new DBRow();
		row.add("psId", psId);
		return floorGoogleMapsMgrCc.getParkingDocksOccupancy(psId);
	}

	@Override
	public DBRow getStorageKmlByPsId(int psId) throws Exception {
		return floorGoogleMapsMgrCc.getStorageKmlByPsId(psId);
	}

	@Override
	public DBRow[] getGeoAlarmLabel(DBRow row, PageCtrl pc) throws Exception {
		return floorGoogleMapsMgrCc.getGeoAlarmLabel(row, pc);
	}
	
	@Override
	public DBRow[] getGeoAlarmLabelCount() throws Exception {
		return floorGoogleMapsMgrCc.getGeoAlarmLabelCount();
	}

	@Override
	public long saveGeoALarmLabel(HttpServletRequest request) throws Exception {
		String geoType = StringUtil.getString(request,"type");
		String name = StringUtil.getString(request,"name");
		String def = StringUtil.getString(request,"def");
		String points = StringUtil.getString(request,"points");
		
		int geotype = 0;
		if("point".equals(geoType)){
			geotype = 1;
		}else if("line".equals(geoType)){
			geotype = 2;
		}else if("polygon".equals(geoType)){
			geotype = 3;
		}else if("circle".equals(geoType)){
			geotype = 4;
		}else if("rect".equals(geoType)){
			geotype = 5;
		}
		long time = System.currentTimeMillis();
		DBRow row = new DBRow();
		row.add("name", name);
		row.add("def", def);
		row.add("points", points);
		row.add("geotype", geotype);
		row.add("createtime", time);
		row.add("updatetime", time);
		return floorGoogleMapsMgrCc.saveGeoALarmLabel(row);
	}
	@Override
	public void removeAlarmLabel(HttpServletRequest request) throws Exception {
		String ids = StringUtil.getString(request,"ids"); //id1,id2,id3...
		floorGoogleMapsMgrCc.removeAlarmLabel(ids);
	}
	@Override
	public DBRow[] getAssetAlarm(DBRow row, PageCtrl pc) throws Exception {
		String search = row.get("search", "");   //1 与asset_id已关联的alarm   2 与asset_id未关联的alarm
		String geoType = row.get("geo_type", "");
		String assetId = row.get("asset_id", "0");
		
		DBRow condition = new DBRow();
		condition.add("assetid", assetId);
		condition.add("geotype", geoType);
		if("exist".equals(search)){
			return floorGoogleMapsMgrCc.getAlarmByAsset(condition,pc);
		}else{
			return floorGoogleMapsMgrCc.getAlarmNoAsset(condition,pc);
		}
	} 

	@Override
	public long addAssetAlarm(HttpServletRequest request) throws Exception {
		long assetId = StringUtil.getLong(request,"asset_id");
		long alarmId = StringUtil.getLong(request,"alarm_id");
		long alarmType = StringUtil.getLong(request,"alarm_type");
		long time = System.currentTimeMillis();
		
		DBRow row = new DBRow();
		row.add("assetid", assetId);
		row.add("alarmid", alarmId);
		row.add("alarmtype", alarmType);
		row.add("createtime", time);
		row.add("updatetime", time);
		return floorGoogleMapsMgrCc.addAssetAlarm(row);
	}

	@Override
	public void removeAssetAlarm(HttpServletRequest request) throws Exception {
		long id = StringUtil.getLong(request,"id");
		floorGoogleMapsMgrCc.removeAssetAlarm(id);
	}

	@Override
	public DBRow[] findAllGroup() throws Exception {
		return floorGoogleMapsMgrCc.findAllGroup();
	}


	@Override
	public DBRow getCamInfo(String ip, String port) throws Exception {
		return  floorGoogleMapsMgrCc.getWebcamByIp(ip, port);
	}

	@Override
	public DBRow getCamPositionInfo(String positionId,int type) throws Exception {
		return  floorGoogleMapsMgrCc.getWebcamPositionByIp(positionId ,type);
	}

	@Override
	public long addWebcam(DBRow row) throws Exception {
		return floorGoogleMapsMgrCc.addWebcam(row);
	}


	@Override
	public int updateWebcamInfo(DBRow row) throws Exception {
		return floorGoogleMapsMgrCc.updateWebcamInfo(row);
	}

	
	@Override
	public DBRow[] getAreaPositionByName(long psId, String areaName) throws Exception {
		return floorGoogleMapsMgrCc.getAreaPositionByName(psId, areaName);
	}
	@Override
	public DBRow[] getLocationInfoByName(long psId, String slc_position)
			throws Exception {
		return floorGoogleMapsMgrCc.getLoctionInfoByName(psId, slc_position);
	}
	@Override
	public DBRow[] getStagingInfoByName(long psId, String positionName)
			throws Exception {
		return floorGoogleMapsMgrCc.getStagingInfoByName(psId, positionName);
	}
	@Override
	public DBRow[] getDocksInfoByName(long psId, String positionName)
			throws Exception {
		return floorGoogleMapsMgrCc.getDocksInfoByName(psId, positionName);
	}
	@Override
	public DBRow[] getParkingInfoByName(long psId, String positionName)
			throws Exception {
		return floorGoogleMapsMgrCc.getParkingInfoByName(psId, positionName);
	}
	@Override
	public int updateStorageArea(long areaId, DBRow row) throws Exception {
		return floorGoogleMapsMgrCc.updateStorageArea(areaId, row);
	}
	@Override
	public int  updateStorageLocation(long slc_id,DBRow data)throws Exception{
	
		return floorGoogleMapsMgrCc.updateStorageLocation(slc_id, data);
		}
	@Override
	public int  updateStorageStaging(long slc_id,DBRow data)throws Exception{
		
		return floorGoogleMapsMgrCc.updateStorageStaging(slc_id, data);
	}
	@Override
	public int  updateStorageDocks(long slc_id,DBRow data)throws Exception{
		
		return floorGoogleMapsMgrCc.updateStorageDocks(slc_id, data);
	}
	@Override
	public int  updateStorageParking(long slc_id,DBRow data)throws Exception{
		
		return floorGoogleMapsMgrCc.updateStorageParking(slc_id, data);
	}

	@Override
	public int updateFolderkml(DBRow data,DBRow condition) throws Exception {
       int result =0;
		try {
			result= floorGoogleMapsMgrCc.updateFolderkml(data,condition);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public DBRow[] getAreaTitleByPsId(String psId) throws Exception {
		return null;//floorGoogleMapsMgrCc.getAreaTitleByPsId(psId);
	}
	
	@Override
	public DBRow[] getAreaDoorByPsId(String psId) throws Exception {
		return floorGoogleMapsMgrCc.getAreaDoorByPsId(psId);
	}
	
	@Override
	public int setStorageDoorStatus(long sd_id, int status) throws Exception {
		return floorGoogleMapsMgrCc.setStorageDoorStatus(sd_id, status);
	}
	
	@Override
	public String convertCoordinateToLatlng(long psId, double x, double y)throws Exception{
		DBRow[] baseData = floorFolderInfoMgrWp.getExcelBaseData(Long.toString(psId));
		double maxX = 0;
		double maxY = 0;
		
		int pCount = 0;
		char[] whChar = new char[26];
		DBRow whRow = new DBRow();
		for(int i=0; i<baseData.length; i++){
			DBRow r = baseData[i];
			String type = r.getString("type").toLowerCase();
			String val = r.getString("param").toLowerCase();
			if("warehouse".equals(type)){
				char p = val.toCharArray()[0];
				whChar[pCount] = p;
				whRow.add(p+"",r.getString("lng")+","+r.getString("lat"));
				pCount++;
			}else if("maxx".equals(type)){
				maxX = Double.parseDouble(val);
			}else if("maxy".equals(type)){
				maxY = Double.parseDouble(val);
			}
		}
		//基础数据有误，无法计算
		if(pCount<3 || maxX<=0 || maxY<=0){
			return "";
		}
		//按字母编号排序
		whChar = Arrays.copyOfRange(whChar, 0, pCount);
		Arrays.sort(whChar);
		String pointA = whRow.getString(whChar[0]+"");
		String pointB = whRow.getString(whChar[1]+"");
		String pointD = whRow.getString(whChar[whChar.length-1]+"");
		
		//计算缩放比例和旋转角度
		double aLng = Double.parseDouble(pointA.split(",")[0]);
		double aLat = Double.parseDouble(pointA.split(",")[1]);
		double bLng = Double.parseDouble(pointB.split(",")[0]);
		double bLat = Double.parseDouble(pointB.split(",")[1]);
		double dLng = Double.parseDouble(pointD.split(",")[0]);
		double dLat = Double.parseDouble(pointD.split(",")[1]);
		double ratio = Math.hypot(aLat-bLat,aLng-bLng)/maxX;
		double radX = Math.atan((aLat-bLat)/(aLng-bLng));
		double radY = Math.atan((aLat-dLat)/(aLng-dLng));
		if(radY <= 0){
			radY += Math.PI;
		}
		radY -= Math.PI/2;
		double radN = radX - radY;
		double maxY_map = Math.hypot(aLat-dLat,aLng-dLng)/ratio; 
		
		double X = x;
		double Y = y;
		
		//仓库相对地图旋转、缩放、平移等处理
		Y /= maxY/maxY_map;  //Y轴拉伸
		
		X = X + Y*Math.sin(radN)*Math.cos(radN);
		Y = Y*Math.cos(radN);
		
		int sign = X>0 ? 1 : -1 ;//由于atan(x)结果在第1,4象限，计算rad时2,3象限的点被旋转180°，即中心对称，因此需作此处理
		double rad = radX + Math.atan(Y/X);
		double len = Math.hypot(X,Y); 
		double lat = sign*len*ratio*Math.sin(rad) + aLat;
		double lng = sign*len*ratio*Math.cos(rad) + aLng;
		if(Double.isNaN(lat)){
			lat = aLat;
		}
		if(Double.isNaN(lng)){
			lng = aLng;
		}
		return lng +","+ lat;
	}
	@Override
	public String convertCoordinateToLatlng(long psId, double x, double y, double xPosition, double yPosition, double angle) throws Exception {
		
		DBRow[] baseData = floorFolderInfoMgrWp.getExcelBaseData(Long.toString(psId));
		
		double maxX = 0;
		double maxY = 0;
		
		int pCount = 0;
		char[] whChar = new char[26];
		DBRow whRow = new DBRow();
		for(int i=0; i<baseData.length; i++){
			DBRow r = baseData[i];
			String type = r.getString("type").toLowerCase();
			String val = r.getString("param").toLowerCase();
			if("warehouse".equals(type)){
				char p = val.toCharArray()[0];
				whChar[pCount] = p;
				whRow.add(p+"",r.getString("lng")+","+r.getString("lat"));
				pCount++;
			}else if("maxx".equals(type)){
				maxX = Double.parseDouble(val);
			}else if("maxy".equals(type)){
				maxY = Double.parseDouble(val);
			}
		}
		//基础数据有误，无法计算
		if(pCount<3 || maxX<=0 || maxY<=0){
			return "";
		}
		//按字母编号排序
		whChar = Arrays.copyOfRange(whChar, 0, pCount);
		Arrays.sort(whChar);
		String pointA = whRow.getString(whChar[0]+"");
		String pointB = whRow.getString(whChar[1]+"");
		String pointD = whRow.getString(whChar[whChar.length-1]+"");
		
		//计算缩放比例和旋转角度
		double aLng = Double.parseDouble(pointA.split(",")[0]);
		double aLat = Double.parseDouble(pointA.split(",")[1]);
		double bLng = Double.parseDouble(pointB.split(",")[0]);
		double bLat = Double.parseDouble(pointB.split(",")[1]);
		double dLng = Double.parseDouble(pointD.split(",")[0]);
		double dLat = Double.parseDouble(pointD.split(",")[1]);
		
		
		double ratio = Math.hypot(aLat-bLat,aLng-bLng)/maxX;
		double radX = Math.atan((aLat-bLat)/(aLng-bLng));
		double radY = Math.atan((aLat-dLat)/(aLng-dLng));
		if(radY <= 0){
			radY += Math.PI;
		}
		radY -= Math.PI/2;
		double radN = radX - radY;
		double maxY_map = Math.hypot(aLat-dLat,aLng-dLng)/ratio; 
		
		double X = x;
		double Y = y;
		double L = xPosition;
		double W = yPosition;
		
		double[] XS = new double[]{X,X+L,X+L,X};
		double[] YS = new double[]{Y,Y,Y+W,Y+W};
		//位置相对仓库旋转处理
		if(angle != 0){
			for(int j=1; j<4; j++){
				XS[j] -= XS[0];
				YS[j] -= YS[0];
				
				double r = Math.toRadians(angle) + Math.atan(YS[j]/XS[j]);
				double l = Math.hypot(XS[j],YS[j]); 
				XS[j] = l*Math.cos(r) + XS[0];
				YS[j] = l*Math.sin(r) + YS[0];
				if(Double.isNaN(XS[j])){
					XS[j] = XS[0];
				}
				if(Double.isNaN(YS[j])){
					YS[j] = YS[0];
				}
			}
		}
		String cds = "";
		//仓库相对地图旋转、缩放、平移等处理
		for(int j=0; j<4; j++){
			YS[j] /= maxY/maxY_map;  //Y轴拉伸
			
			XS[j] = XS[j] + YS[j]*Math.sin(radN)*Math.cos(radN);
			YS[j] = YS[j]*Math.cos(radN);
			
			int sign = XS[j]>0 ? 1 : -1 ;//由于atan(x)结果在第1,4象限，计算rad时2,3象限的点被旋转180°，即中心对称，因此需作此处理
			double rad = radX + Math.atan(YS[j]/XS[j]);
			double len = Math.hypot(XS[j],YS[j]); 
			double lat = sign*len*ratio*Math.sin(rad) + aLat;
			double lng = sign*len*ratio*Math.cos(rad) + aLng;
			if(Double.isNaN(lat)){
				lat = aLat;
			}
			if(Double.isNaN(lng)){
				lng = aLng;
			}
			cds += lng +","+ lat + ",0.0 ";
		}
		cds += cds.split(" ")[0];
		return cds;
	}
	

	@Override
	public String convertCoordinateToLatlng(String psId, String x, String y) throws Exception {
		long _psId = Long.parseLong(psId);
		double _x = Double.parseDouble(x);
		double _y = Double.parseDouble(y);
		return convertCoordinateToLatlng(_psId,_x,_y);
	}
	@Override
	public String convertCoordinateToLatlng(String psId, String x, String y,
			String xPosition, String yPosition, String angle) throws Exception {
		long _psId = Long.parseLong(psId);
		double _x = Double.parseDouble(x);
		double _y = Double.parseDouble(y);
		double _xp = Double.parseDouble(xPosition);
		double _yp = Double.parseDouble(yPosition);
		double _angle = Double.parseDouble(angle);
		return convertCoordinateToLatlng(_psId,_x,_y,_xp,_yp,_angle);
	}
    
	@Override
	public String convertLatlngToCoordinate(long psId,double lat, double lng)throws Exception {
		DBRow[] baseData = floorFolderInfoMgrWp.getExcelBaseData(Long.toString(psId));
		double maxX = 0;
		double maxY = 0;
		
		int pCount = 0;
		char[] whChar = new char[26];
		DBRow whRow = new DBRow();
		for(int i=0; i<baseData.length; i++){
			DBRow r = baseData[i];
			String type = r.getString("type").toLowerCase();
			String val = r.getString("param").toLowerCase();
			if("warehouse".equals(type)){
				char p = val.toCharArray()[0];
				whChar[pCount] = p;
				whRow.add(p+"",r.getString("lng")+","+r.getString("lat"));
				pCount++;
			}else if("maxx".equals(type)){
				maxX = Double.parseDouble(val);
			}else if("maxy".equals(type)){
				maxY = Double.parseDouble(val);
			}
		}
		//基础数据有误，无法计算
		if(pCount<3 || maxX<=0 || maxY<=0){
			return "";
		}
		//按字母编号排序
		whChar = Arrays.copyOfRange(whChar, 0, pCount);
		Arrays.sort(whChar);
		String pointA = whRow.getString(whChar[0]+"");
		String pointB = whRow.getString(whChar[1]+"");
		String pointD = whRow.getString(whChar[whChar.length-1]+"");
		
		//计算缩放比例和旋转角度
		double aLng = Double.parseDouble(pointA.split(",")[0]);
		double aLat = Double.parseDouble(pointA.split(",")[1]);
		double bLng = Double.parseDouble(pointB.split(",")[0]);
		double bLat = Double.parseDouble(pointB.split(",")[1]);
		double dLng = Double.parseDouble(pointD.split(",")[0]);
		double dLat = Double.parseDouble(pointD.split(",")[1]);
		double ratio = Math.hypot(aLat-bLat,aLng-bLng)/maxX;
		double radX = Math.atan((aLat-bLat)/(aLng-bLng));
		double radY = Math.atan((aLat-dLat)/(aLng-dLng));
		if(radY <= 0){
			radY += Math.PI;
		}
		radY -= Math.PI/2;
		double radN = radX - radY;
		double maxY_map = Math.hypot(aLat-dLat,aLng-dLng)/ratio; 
		
		lng -= aLng;
		lat -= aLat;
		
		int sign = lng>0 ? 1 : -1 ;//由于atan(x)结果在第1,4象限，计算rad时2,3象限的点被旋转180°，即中心对称，因此需作此处理
		double rad = radX - Math.atan(lat/lng);
		double len = Math.hypot(lat,lng);
		
		double Y = sign*len*Math.sin(rad)/ratio*maxY/maxY_map;
		double X = sign*len*Math.cos(rad)/ratio;
		
		Y /= -Math.cos(radN);
		X -= Y*Math.sin(radN)/(maxY/maxY_map);
		
		if(Double.isNaN(X)){
			X = 0;
		}
		if(Double.isNaN(Y)){
			Y = 0;
		}
		return X+","+Y;
		
	}

	@Override
	public DBRow[] getStorageLayerData(int type, Long psId) throws Exception {
		DBRow[] rows = {};
		switch(type){
		case WebcamPositionTypeKey.AREA:
			break;
		case WebcamPositionTypeKey.DOCKS:
			break;
		case WebcamPositionTypeKey.LOCATION:
			break;
		case WebcamPositionTypeKey.PARKING:
			break;
		case WebcamPositionTypeKey.STAGING:
			break;
		case WebcamPositionTypeKey.WEBCAM:
			rows =floorFolderInfoMgrWp.getWebcambyPsid(psId);
			break;
		case WebcamPositionTypeKey.PRINTER:
			rows=floorFolderInfoMgrWp.getPrinterbyPsid(psId);
			break;
		case WebcamPositionTypeKey.LIGHT:
			rows=floorGoogleMapsMgrCc.getStorageLightByPsid(psId);
			break;
		}
		return rows;
	}
	@Override
	public String calculatePointtoLine(String a,String b,String c,String d,String m,String n )throws Exception{
		double _a =Double.parseDouble(a);
		double _b =Double.parseDouble(b);
		double _c =Double.parseDouble(c);
		double _d =Double.parseDouble(d);
		double _m =Double.parseDouble(m);
		double _n =Double.parseDouble(n);
		return calculatePointtoLine(_a,_b,_c,_d,_m,_n);
	}
	@Override
	public String calculatePointtoLine(double a,double b,double c,double d, double m,double n )throws Exception{
		if(b-d==0){
			return m+","+b+","+a+","+n;
		}
		if(a-c==0){
			return a+","+n+","+m+","+b;
		}
		double n1=n+(a-c)*m/(b-d);
		double k=(b-d)/(a-c);
		double n2=(c*b-a*d)/(c-a);
		double x1=k*(n1-n2)/(k*k+1);
		double y1=(k*k*n1+n2)/(k*k+1);
		double n3=n-(b-d)*m/(a-c);
		double n4=b+(a-c)*a/(b-d);
		double x2=-(n3-n4)*k/(k*k+1);
		double y2=(n4*k*k+n3)/(k*k+1);
		return x1+","+y1+","+x2+","+y2;
	}
	

	@Override
	public String convertLatlngToCoordinate(String psId, String lat, String lng)
			throws Exception {
		long _psId =Long.parseLong(psId);
		double  _lat= Double.parseDouble(lat);
		double  _lng =Double.parseDouble(lng);
		return this.convertLatlngToCoordinate(_psId,_lat,_lng);
		
	}
	@Override
	public DBRow[] getPrintByPrintServer(long printServerId) throws Exception {
		try{
			 return floorGoogleMapsMgrCc.getPrintByPrintServer(printServerId);
		}catch (Exception e) {
			throw new SystemException(e,"getPrintByPrintServer",log);
		}
	}
	
	@Override
	public  long  addPrinter(DBRow row) throws Exception {
			return floorGoogleMapsMgrCc.addPrinter(row);
	}

	@Override
	public  long  addPrinterArea(DBRow row) throws Exception {
		return floorGoogleMapsMgrCc.addPrinterArea(row);
	}
	@Override
	public  long  deletePrinterArea(long service_id) throws Exception {
		return floorGoogleMapsMgrCc.deletePrinterArea(service_id);
	}
	@Override
	public int updatePrinter(DBRow row) throws Exception {
		return floorGoogleMapsMgrCc.updatePrinter(row);
	}

	@Override
	public DBRow[] getStorageRoadByPsid(long psId) throws Exception {
		return floorGoogleMapsMgrCc.getStorageRoadByPsid(psId);
	}
	
	@Override
	public DBRow[] getStorageRoadPointByPsid(long psId) throws Exception {
		return floorGoogleMapsMgrCc.getStorageRoadPointByPsid(psId);
	}

	@Override
	public DBRow[] getLocationEnteryRoadByPsid(long psId) throws Exception {
		return floorGoogleMapsMgrCc.getLocationEnteryRoadByPsid(psId);
	}

	@Override
	public DBRow[] getAreaDoorCountsByPsId(long psId) throws Exception {
		return floorGoogleMapsMgrCc.getAreaDoorCountsByPsId(psId);
	}
	@Override
	public DBRow[] getAreaDoorByPsIdandArea(long psId,long area_id) throws Exception {
		return floorGoogleMapsMgrCc.getAreaDoorByPsIdandArea(psId,area_id);
	}
	@Override
	public DBRow[] getAreaPersonByPsIdandArea(long psId,long area_id) throws Exception {
		return floorGoogleMapsMgrCc.getAreaPersonByPsIdandArea(psId,area_id);
	}

	@Override
	public DBRow[] getRoutePath(long psId, int fromType, String fromPosition, String fromLatlng, int toType, String toPosition, String toLatlng) throws Exception {
		DBRow[] result = null;
		
		String fRoadStr = null;
		String tRoadStr = null;
		
		long fRid = 0l;
		long tRid = 0l;
		
		//开始路段
		if(fromType == WebcamPositionTypeKey.LOCATION){
			DBRow fLoc = floorKmlInfoMgrWp.getLocationByPosition(psId, fromPosition);
			fRoadStr = fLoc.getString("entery_point")+","+fLoc.getString("road_point");
			fRid = fLoc.get("entery_road", 0l);
		}else{
			String nearRoad = floorGoogleMapsMgrCc.getNearestRoad(psId, fromLatlng).getString("road");
			fRoadStr = nearRoad.split(",")[1];
			fRid = Integer.parseInt(nearRoad.split(",")[0]);
		}
		//结束路段
		if(toType == WebcamPositionTypeKey.LOCATION){
			DBRow tLoc = floorKmlInfoMgrWp.getLocationByPosition(psId, toPosition);
			tRoadStr = tLoc.getString("road_point")+","+tLoc.getString("entery_point");
			tRid = tLoc.get("entery_road", 0l);
		}else{
			String nearRoad = floorGoogleMapsMgrCc.getNearestRoad(psId, toLatlng).getString("road");
			tRoadStr = nearRoad.split(",")[1];
			tRid = Integer.parseInt(nearRoad.split(",")[0]);
		}
		
		if(fRid == tRid){
			DBRow r = new DBRow();
			r.add("path", fRoadStr+","+tRoadStr);
			result = new DBRow[]{r};
		}else{
			DBRow fRoad = floorGoogleMapsMgrCc.getStorageRoadByRid(fRid);
			DBRow tRoad = floorGoogleMapsMgrCc.getStorageRoadByRid(tRid);
			
			long fSource = fRoad.get("source", 0l);
			long fTarget = fRoad.get("target", 0l);
			long tSource = tRoad.get("source", 0l);
			long tTarget = tRoad.get("target", 0l);
			
			String pathStr = floorGoogleMapsMgrCc.getDijkstraByPointId(psId, fSource+"", tSource+","+tTarget).getString("path");
			pathStr += " "+floorGoogleMapsMgrCc.getDijkstraByPointId(psId, fTarget+"", tSource+","+tTarget).getString("path");
			
			String[] paths = pathStr.split(" ");
			String[] data = new String[4];
			int count = 0;
			for (String s : paths) {
				if(!"0".equals(s)){
					DBRow[] points = floorGoogleMapsMgrCc.getRoutePathPoints(psId, s);
					
					String[] ps = s.split(",");
					String pStr = fRoadStr;
					for (String p : ps) {
						for (DBRow point : points) {
							String p_id = point.getString("point_id");
							if(p.equals(p_id)){
								String ss = null;
								Object obj = point.getValue("latlng");
								if(obj instanceof String){
									ss = (String)obj;
								}else{
									ss = byteArrayToString((byte[])obj);
								}
								pStr += ","+ss;
							}
						}
					}
					pStr += ","+tRoadStr;
					data[count++] = pStr;
				}
			}
			
			result = new DBRow[count];
			for(int i=0; i<count; i++){
				DBRow r = new DBRow();
				r.add("path", data[i]);
				result[i] = r;
			}
		}
		return result;
	}
	//byte[]转string    解决dbrow封装bug:sql查询结果集封装为dbrow时，string类型转换稀烂    x_x
	public String byteArrayToString(byte[] a){
		StringBuffer sb = new StringBuffer();
		for (int i : a) {
			sb.append((char)i);
		}
		return sb.toString();
	}
	@Override
	public DBRow[] getPersonforAreaByPsId(long psId) throws Exception {
		return floorGoogleMapsMgrCc.getPersonforAreaByPsId(psId);
	}
	@Override
	public DBRow[] getStorageCoordinateSys(long psId) throws Exception {
		return floorGoogleMapsMgrCc.getStorageCoordinateSys(psId);
	}
	public static void setLog(Logger log) {
		GoogleMapsMgrCc.log = log;
	}
	public void setFloorGoogleMapsMgrCc(FloorGoogleMapsMgrCc floorGoogleMapsMgrCc) {
		this.floorGoogleMapsMgrCc = floorGoogleMapsMgrCc;
	}
	public void setFloorFolderInfoMgrWp(FloorFolderInfoMgrWp floorFolderInfoMgrWp) {
		this.floorFolderInfoMgrWp = floorFolderInfoMgrWp;
	}
	public void setFloorKmlInfoMgrWp(FloorKmlInfoMgrWp floorKmlInfoMgrWp) {
		this.floorKmlInfoMgrWp = floorKmlInfoMgrWp;
	}
	public void setCreateKmlMgrWp(CreateKmlMgrIfaceWp createKmlMgrWp) {
		this.createKmlMgrWp = createKmlMgrWp;
	}
	public void setQueryKmlInfoMgrWp(QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp) {
		this.queryKmlInfoMgrWp = queryKmlInfoMgrWp;
	}

	@Override
	public DBRow[] getPrinterServerByAreaIdandPtype(long area_id)
			throws Exception {
		return floorGoogleMapsMgrCc.getPrinterServerByAreaIdandPtype(area_id);
	}
	@Override
	public DBRow[] getLayerInfoBypsIdAndName(long psId ,String name,int type )
			throws Exception {
	return floorGoogleMapsMgrCc.getLayerInfoBypsIdAndName(psId ,name,type );
	}
	@Override
	public DBRow checkPrinterNameIsExist(String name,long psId) throws Exception{
		// TODO Auto-generated method stub
		return floorGoogleMapsMgrCc.checkPrinterNameIsExist(name, psId);
	}

	@Override
	public void updatePersonAreaByAdid(long id, int areaId) throws Exception {
		// TODO Auto-generated method stub
		//this.floorGoogleMapsMgrCc.updatePersonAreaByAdid(id, areaId);
		
	}

	@Override
	public void deletePersonAreaByAdid(long id) throws Exception {
		// TODO Auto-generated method stub
		//this.floorGoogleMapsMgrCc.deletePersonAreaByAdid(id);
		
	}

	@Override
	public DBRow[] getPersonByPsId(long psId) throws Exception {
		// TODO Auto-generated method stub
		return floorGoogleMapsMgrCc.getPersonByPsId(psId);
	}

	@Override
	public void deleteAreaDoorByAreaId(long position_id)  {
		// TODO Auto-generated method stub
		try {
			this.floorGoogleMapsMgrCc.deleteAreaDoorByAreaId(position_id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public DBRow[] getLightData(long psId) throws Exception  {
			return floorGoogleMapsMgrCc.getStorageLightByPsid(psId);
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	@Override
	public  long  addLight(DBRow row) throws Exception {
		return floorGoogleMapsMgrCc.addLight(row);
	}
	@Override
	public  DBRow  queryLightSingle(long psId ,String name) throws Exception {
		return floorGoogleMapsMgrCc.queryLightSingleByIdAndName(psId,name);
	}
	@Override
	public  long  updateLight(DBRow row) throws Exception {
		return floorGoogleMapsMgrCc.updateLight(row);
	}
	@Override
	public  DBRow[]  selectContainer(int status) throws Exception {
		return floorGoogleMapsMgrCc.selectContainer(status);
	}
	@Override
	public  DBRow[]  selectContainerPlates(long ic_id,long stagingId) throws Exception {
		return floorGoogleMapsMgrCc.selectContainerPlates(ic_id,stagingId);
	}
	@Override
	public  DBRow[]  queryContainerCounts(long ic_id,long psId) throws Exception {
		return floorGoogleMapsMgrCc.queryContainerCounts(ic_id,psId);
	}
	@Override
	public  DBRow[]  distinctContainerPlates(long ic_id,long stagingId) throws Exception {
		return floorGoogleMapsMgrCc.distinctContainerPlates(ic_id, stagingId);
	}
	@Override
	public  DBRow[]  getStorageTimeOutBypsId(long ps_id) throws Exception {
		return floorGoogleMapsMgrCc.getStorageTimeOutBypsId(ps_id);
	}
	@Override
	public  long  addStorageTimeOut(DBRow row) throws Exception {
		return floorGoogleMapsMgrCc.addStorageTimeOut(row);
	}
	@Override
	public  long  updateStorageTimeOutByPsId(DBRow row) throws Exception {
		return floorGoogleMapsMgrCc.updateStorageTimeOutByPsId(row);
	}
}

