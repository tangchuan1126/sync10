package com.cwc.app.api.cc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.cc.FloorReportDataInterfaceMgrCc;
import com.cwc.app.iface.cc.ReportDataInterfaceMgrIfaceCc;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class ReportDataInterfaceMgrCc implements ReportDataInterfaceMgrIfaceCc{
	private FloorReportDataInterfaceMgrCc floorReportDataInterfaceMgrCc;
	
	public void setFloorReportDataInterfaceMgrCc(
			FloorReportDataInterfaceMgrCc floorReportDataInterfaceMgrCc) {
		this.floorReportDataInterfaceMgrCc = floorReportDataInterfaceMgrCc;
	}
	
	@Override
	public DBRow[] getItemsByCustomerId(String customerId) throws Exception {
		return floorReportDataInterfaceMgrCc.getItemsByCustomerId(customerId);
	}

	@Override
	public DBRow[] getReceiveByCustomerIdAndDate(String customerId, String from, String to) throws Exception {
		return floorReportDataInterfaceMgrCc.getReceiveByCustomerIdAndDate(customerId, from, to);
	}
	@Override
	public DBRow[] getIntradayReceiveByCustomerId(String customerId, String companyId, String dateStr) throws Exception {
		long psId = this.getPsIdByCompanyId(companyId);
		Date date = DateUtil.utcTime(dateStr, psId);
		String from = DateUtil.DatetimetoStr(date);
		String to = DateUtil.DatetimetoStr(new Date(date.getTime()+24*60*60*1000));
		DBRow[] rows = getReceiveByCustomerIdAndDate(customerId, from, to);
		fixTimeField(rows, "time", psId);
		return rows;
	}
	
	/**
	 * 时间字段修改为本地时间
	 * @param rows
	 * @param field
	 * @param psId
	 * @throws Exception
	 */
	private void fixTimeField(DBRow[] rows, String field, long psId) throws Exception{
		for (DBRow r : rows) {
			String time = r.getString(field);
			if(!StringUtil.isBlank(time)){
				time = DateUtil.locationZoneTimeString(time, psId);
				r.add(field, time);
			}
		}
	}
	private long getPsIdByCompanyId(String companyId) throws Exception{
		DBRow r = floorReportDataInterfaceMgrCc.getPsIdByCompanyId(companyId);
		return r.get("ps_id", 0l);
	}

	@Override
	public Map appointmentCount(Map<String, String[]> map) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat dateFmt = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFmt = new SimpleDateFormat("HH:00");
		
		Map result = new HashMap();
		
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String psName = it.next();
			String[] companyId = map.get(psName);
			DBRow[] inbound = floorReportDataInterfaceMgrCc.appointmentCountInbound(companyId);
			DBRow[] outbound = floorReportDataInterfaceMgrCc.appointmentCountOutbound(companyId);
			if(inbound!=null && inbound.length>0){
				for (DBRow row : inbound) {
					String dateStr = row.getString("appointmentdate");
					int num = row.get("num", 1);
					
					Date date = sdf.parse(dateStr);
					String d = dateFmt.format(date);
					String t = timeFmt.format(date);
					
					putdata(result, d, t, psName, "inbound", num);
				}
			}
			if(outbound!=null && outbound.length>0){
				for (DBRow row : outbound) {
					String dateStr = row.getString("appointmentdate");
					int num = row.get("num", 1);
					
					Date date = sdf.parse(dateStr);
					String d = dateFmt.format(date);
					String t = timeFmt.format(date);
					
					putdata(result, d, t, psName, "outbound", num);
				}
			}
		}
		return result;
	}
	
	private void putdata(Map map, String day, String time, String psName, String countType, int num){
		//天
		Map _day = null;
		if(map.containsKey(day)){
			_day = (HashMap)map.get(day);
		}else {
			_day = new HashMap();
			map.put(day, _day);
		}
		//仓库
		Map ps = null;
		if(_day.containsKey(psName)){
			ps = (HashMap)_day.get(psName);
		}else{
			ps = new HashMap();
			_day.put(psName, ps);
		}
		
		//小时段
		Map<String, Integer> h = null;
		if(ps.containsKey(time)){
			h = (HashMap)ps.get(time);
		}else{
			h = new HashMap();
			h.put("inbound",0);
			h.put("outbound",0);
			
			ps.put(time, h);
		}
		h.put(countType, h.get(countType)+num);
	}
}
