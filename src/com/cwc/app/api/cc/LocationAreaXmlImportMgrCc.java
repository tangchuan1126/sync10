package com.cwc.app.api.cc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.floor.api.cc.FloorLocationAreaXmlImportMgrCc;
import com.cwc.app.floor.api.wp.FloorFolderInfoMgrWp;
import com.cwc.app.floor.api.zl.FloorStorageDoorLocationMgrZYZ;
import com.cwc.app.iface.cc.LocationAreaXmlImportMgrIfaceCc;
import com.cwc.app.iface.zj.LocationMgrIFaceZJ;
import com.cwc.app.key.WebcamPositionTypeKey;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class LocationAreaXmlImportMgrCc implements LocationAreaXmlImportMgrIfaceCc{
	static Logger log = Logger.getLogger("ACTION");
	private FloorLocationAreaXmlImportMgrCc floorLocationAreaXmlImportMgrCc;
	private LocationMgrIFaceZJ locationMgrZJ;
	private FloorStorageDoorLocationMgrZYZ floorStorageDoorLocationZYZ;
	private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;
	private FloorFolderInfoMgrWp floorFolderInfoMgrWp;
	
	@Override
	public void savePraseRusult(List<DBRow> rows) throws Exception {
		try {
			floorLocationAreaXmlImportMgrCc.savePraseRusult(rows);
		} catch (Exception e) {
			throw new SystemException(e,"savePraseRusult",log);
		}
	}

	@Override
	public void truncateTableFolderFromKml() throws Exception {
		try {
			floorLocationAreaXmlImportMgrCc.truncateTableFolderFromKml();
		} catch (Exception e) {
			throw new SystemException(e,"truncateTableFolderFromKml",log);
		}
	}

	@Override
	public DBRow getAllAreaInfoForStorage(DBRow storage) throws Exception {
		try {
			return floorLocationAreaXmlImportMgrCc.getAllAreaInfoForStorage(storage);
		} catch (Exception e) {
			throw new SystemException(e,"getAllAreaInfoForStorage",log);
		}
	}

	@Override
	public void saveLocationArea(HttpServletRequest request) throws Exception {
		long pscId=StringUtil.getLong(request, "area_psid");//仓库id
		String title=StringUtil.getString(request, "title");//仓库title
		String kmlFileName=StringUtil.getString(request, "kml_name");//仓库kml
		try {
			DBRow condition = new DBRow();
			condition.add("id", pscId);
			condition.add("source", kmlFileName);
			DBRow data = getAllAreaInfoForStorage(condition);
			DBRow[] warehouseBase = (DBRow[])data.getValue("warehouseBase");
			DBRow[] areaData = (DBRow[])data.getValue("locationName");
			DBRow[] locationData = (DBRow[])data.getValue("location");
			DBRow[] docksData = (DBRow[])data.getValue("docks");
			DBRow[] stagingData = (DBRow[])data.getValue("staging");
			DBRow[] parkingData = (DBRow[])data.getValue("parking");
			DBRow[] webcamData = (DBRow[])data.getValue("webcam");
			//TITLE
			List<DBRow> titleList = new ArrayList<DBRow>();
			//zone_dock关系
			List<DBRow> areaDoorList = new ArrayList<DBRow>();
			//区域不存在时添加新区域
			DBRow newAreaId = new DBRow();
			DBRow newDocksId = new DBRow();
			//保存warehouse base
			if(warehouseBase != null && warehouseBase.length>0){
				//先删除老数据
				floorGoogleMapsMgrCc.deleteStorageWarehouseBaseByPsid(pscId);
				for(int i=0; i<warehouseBase.length; i++){
					floorGoogleMapsMgrCc.addStorageWarehouseBase(warehouseBase[i]);
				}
			}
			//保存area
			if(areaData != null && areaData.length>0){
				for(int i=0; i<areaData.length; i++){
					DBRow r = areaData[i];
					String areaName = r.getString("area_name");
					long areaIdTmp = r.get("area_id", 0l);
					if(areaIdTmp == 0l && newAreaId.get(areaName, 0l) == 0l){
						int area_type = 0;
						String area_type_str = r.getString("area_type");
						switch(area_type_str){
							case "location" : area_type = 1;break;
							case "docks" : area_type = 2;break;
							case "parking" : area_type = 3;break;
						}
						DBRow dbrow = new DBRow();
						dbrow.add("area_name",areaName);
						dbrow.add("area_psid",pscId);
						dbrow.add("area_img", "");
						dbrow.add("area_type", area_type);
						dbrow.add("x", r.getString("x"));
						dbrow.add("y", r.getString("y"));
						dbrow.add("width", r.getString("width"));
						dbrow.add("height", r.getString("height"));
						dbrow.add("angle", r.getString("angle"));
						dbrow.add("latlng", r.getString("latlng"));
						areaIdTmp = locationMgrZJ.addLocationAreaSub(dbrow);
						newAreaId.add(areaName, areaIdTmp);
					}
					String titleName = r.getString("title");
					if(!StringUtil.isBlank(titleName)){
						String titles[] = titleName.split(",");
						for(int j=0; j<titles.length; j++){
							DBRow dbrow = new DBRow();
							dbrow.add("area_id",areaIdTmp);
							dbrow.add("title_name",titles[j].trim());
							dbrow.add("title_id", r.getString("title_id"));
							titleList.add(dbrow);
						}
					}
					String docks = r.getString("docks");
					if(!StringUtil.isBlank(docks)){
						DBRow dbrow = new DBRow();
						dbrow.add("area_id",areaIdTmp);
						dbrow.add("docks",docks);
						areaDoorList.add(dbrow);
					}
				}
			}
			//保存location
			if(locationData != null && locationData.length>0){
				for(int i=0; i<locationData.length; i++){
					DBRow r = locationData[i];
					long  id = r.get("slc_id", 0l); 
					if(id == 0l){
						DBRow dbrow = new DBRow();
						dbrow.add("slc_area",r.get("area_id", 0)==0?newAreaId.get(r.getString("area_name"),0):r.get("area_id", 0));
						dbrow.add("slc_type","");
						dbrow.add("slc_psid",pscId);
						dbrow.add("slc_x",0);
						dbrow.add("slc_y",0);
						dbrow.add("slc_position",r.getString("placemark_name"));
						dbrow.add("slc_position_all",title+r.getString("area_name")+r.getString("placemark_name"));
//						dbrow.add("slc_position_all",r.getString("area_name")+r.getString("placemark_name"));
						dbrow.add("slc_ps_title",title);
						dbrow.add("is_three_dimensional",r.getString("is3D"));
						dbrow.add("x", r.getString("x"));
						dbrow.add("y", r.getString("y"));
						dbrow.add("width", r.getString("width"));
						dbrow.add("height", r.getString("height"));
						dbrow.add("angle", r.getString("angle"));
						dbrow.add("latlng", r.getString("latlng"));
						id = locationMgrZJ.addLocationCatalogSub(dbrow);
					}
			
				}
			}
			//保存docks
			if(docksData != null && docksData.length>0){
				for(int i=0; i<docksData.length; i++){
					DBRow r = docksData[i];
					long id = r.get("door_id", 0l); 
					if(id == 0l){
						DBRow doorRow = new DBRow();
						doorRow.add("doorId", r.getString("placemark_name"));
						doorRow.add("ps_id", pscId);
						doorRow.add("area_id", r.get("area_id", 0)==0?newAreaId.get(r.getString("area_name"),0):r.get("area_id", 0));
						doorRow.add("x", r.getString("x"));
						doorRow.add("y", r.getString("y"));
						doorRow.add("width", r.getString("width"));
						doorRow.add("height", r.getString("height"));
						doorRow.add("angle", r.getString("angle"));
						doorRow.add("latlng", r.getString("latlng"));
						id = floorStorageDoorLocationZYZ.addStorageDoor(doorRow);
					}
					newDocksId.add(r.getString("placemark_name"), id);
				
				}
			}
			//保存staging
			if(stagingData != null && stagingData.length>0){
				for(int i=0; i<stagingData.length; i++){
					DBRow r = stagingData[i];
					long id = r.get("staging_id", 0l); 
					if(id == 0l){
						DBRow rows = new DBRow();
						rows.add("location_name", r.getString("placemark_name"));
						rows.add("psc_id", pscId);
						rows.add("sd_id", r.get("door_id", 0)==0?newDocksId.get(r.getString("door_name"),0):r.get("door_id", 0));
						rows.add("x", r.getString("x"));
						rows.add("y", r.getString("y"));
						rows.add("width", r.getString("width"));
						rows.add("height", r.getString("height"));
						rows.add("angle", r.getString("angle"));
						rows.add("latlng", r.getString("latlng"));
						id = floorStorageDoorLocationZYZ.addLoadUnloadLocation(rows);
					}
					
				}
			}
			//保存parking
			if(parkingData != null && parkingData.length>0){
				for(int i=0; i<parkingData.length; i++){
					DBRow r = parkingData[i];
					long id = r.get("parking_id", 0l); 
					if(id == 0l){
						DBRow rows = new DBRow();
						rows.add("yc_no", r.getString("placemark_name"));
						rows.add("ps_id", pscId);
						rows.add("yc_status", 1);
						rows.add("area_id", r.get("area_id", 0)==0?newAreaId.get(r.getString("area_name"),0):r.get("area_id", 0));
						rows.add("x", r.getString("x"));
						rows.add("y", r.getString("y"));
						rows.add("width", r.getString("width"));
						rows.add("height", r.getString("height"));
						rows.add("angle", r.getString("angle"));
						rows.add("latlng", r.getString("latlng"));
						id = floorLocationAreaXmlImportMgrCc.addStorageYardControl(rows);
					}
				
				}
			}
			//保存area和docks关系
			if(!areaDoorList.isEmpty()){
				for(int i=0; i<areaDoorList.size(); i++){
					DBRow row = (DBRow)areaDoorList.get(i);
					long areaId = row.get("area_id", 0l);
					String docks = row.getString("docks");
					String[] dock = docks.split(",");
					for(int j=0; j<dock.length; j++){
						long dockId = newDocksId.get(dock[j].trim(), 0l);
						if(dockId != 0){
							floorLocationAreaXmlImportMgrCc.addStorageAreaDoor(areaId, dockId);
						}
					}
				}
			}
			//保存摄像头
			if(webcamData.length>0){
				for(int i=0;i<webcamData.length;i++){
					DBRow webcam =new DBRow();
					webcam.add("ps_id",webcamData[i].getString("ps_id"));
					webcam.add("ip",webcamData[i].getString("ip"));
					webcam.add("port",webcamData[i].getString("port"));
					webcam.add("user",webcamData[i].getString("user"));
					webcam.add("password",webcamData[i].getString("password"));
					webcam.add("x",webcamData[i].getString("x"));
					webcam.add("y",webcamData[i].getString("y"));
					webcam.add("inner_radius",webcamData[i].getString("area_name"));
					webcam.add("outer_radius",webcamData[i].getString("placemark_name"));
					webcam.add("s_degree",webcamData[i].getString("width"));
					webcam.add("e_degree",webcamData[i].getString("height"));
					webcam.add("latlng",webcamData[i].getString("latlng"));
					long id =Long.parseLong(webcamData[i].getString("id"));
					if(id==0l){//表示webcam为新
						floorGoogleMapsMgrCc.addWebcam(webcam);	
					}
					
				}
			}
			//保存Title
			if(!titleList.isEmpty()){
				savaTitle(titleList);
			}
			//另存kml文件
			if(!"".equals(kmlFileName)){
				saveKmlFile(kmlFileName);   //非location部分
				saveKmlFile("loc_"+kmlFileName);  //location部分
				DBRow row = new DBRow();
				row.add("ps_id", pscId);
				row.add("kml", kmlFileName);
				row.add("update_time",System.currentTimeMillis());
				floorLocationAreaXmlImportMgrCc.addOrUpdateStorageKml(row);
			}
		} catch (Exception e) {
			throw new SystemException(e,"saveLocationArea",log);
		} finally{
			//删除folder_from_kml临时数据
			//floorLocationAreaXmlImportMgrCc.deleteFolderFromKml(kmlFileName);
		}
	}
	//另存文件
	public void saveKmlFile(String file) throws Exception {
		try {
			InputStream in = new FileInputStream(Environment.getHome()+"upl_imags_tmp/"+file);
			OutputStream out = new FileOutputStream( Environment.getHome()+"upload/kml/"+file);
			byte[] b = new byte[1024*1024];
			int len = -1;
			while((len = in.read(b))>-1){
				out.write(b, 0, len);
			}
			in.close();
			out.flush();
			out.close();
		} catch (Exception e) {
			throw new SystemException(e,"saveKmlFile",log);
		}
	}
	//保存摄像头
	public void saveWebcam(List<DBRow> cam) throws Exception{
		Map<String, Long> camId = new HashMap<String, Long>();
		Iterator<DBRow> it  = cam.iterator();
		while(it.hasNext()){
			DBRow row = it.next();
			String key = row.getString("ip")+":"+row.getString("port");
			long id = 0l;
			if(!camId.containsKey(key)){
				DBRow w = floorGoogleMapsMgrCc.getWebcamByIp(row.getString("ip"), row.getString("port"));
				if(w==null){
					DBRow r = new DBRow();
					r.add("ip", row.getString("ip"));
					r.add("port", row.getString("port"));
					r.add("user", row.getString("user"));
					r.add("password", row.getString("password"));
					id = floorGoogleMapsMgrCc.addWebcam(r);
				}else{
					id = w.get("id", 0l);
				}
				camId.put(key, id);
			}else{
				id = camId.get(key);
			}
			DBRow r = new DBRow();
			r.add("w_id", id);
			r.add("position_id", row.getString("position_id"));
			r.add("position_type", row.getString("position_type"));
		}
	}
	//保存title
	public void savaTitle(List<DBRow> cam) throws Exception{
		Map<String, Long> titleIds = new HashMap<String, Long>();
		Iterator<DBRow> it  = cam.iterator();
		while(it.hasNext()){
			DBRow row = it.next();
			String title = row.getString("title_name");
			long titleId = row.get("title_id",0l);
			long areaId = row.get("area_id",0l);
			if(titleId==0l){
				if(titleIds.containsKey(title)){
					titleId = titleIds.get(title);
				}else{
					DBRow[] t = floorGoogleMapsMgrCc.getTitle(title);
					if(t.length>0){
						titleId = t[0].get("title_id", 0l);
					}else{
						DBRow r = new DBRow();
						r.add("title_name", title);
						titleId = floorGoogleMapsMgrCc.addTitle(r);
					}
					titleIds.put(title, titleId);
				}
			}
			DBRow[] zt = floorGoogleMapsMgrCc.getZoneTitle(areaId, titleId);
			if(zt.length==0){
				DBRow r = new DBRow();
				r.add("area_id", areaId);
				r.add("title_id", titleId);
				floorGoogleMapsMgrCc.addZoneTitle(r);
			}
		}
	}

	@Override
	public void deleteDataByid(String ps_id) throws Exception {
		this.floorLocationAreaXmlImportMgrCc.deleteDataByid(ps_id);
	}

	@Override
	public DBRow[] getLocationData(String ps_id) throws Exception {
		return floorFolderInfoMgrWp.getExcelLocationData(ps_id);
	}

	@Override
	public DBRow[] getAreaData(String ps_id) throws Exception {
		return floorFolderInfoMgrWp.getExcelAreaData(ps_id);
	}

	@Override
	public DBRow[] getStagingData(String ps_id) throws Exception {
		return floorFolderInfoMgrWp.getExcelStagingData(ps_id);
	}

	@Override
	public DBRow[] getParkingData(String ps_id) throws Exception {
		return floorFolderInfoMgrWp.getExcelParkingData(ps_id);
	}

	@Override
	public DBRow[] getDocksData(String ps_id) throws Exception {
		return floorFolderInfoMgrWp.getExcelDocksData(ps_id);
	}

	@Override
	public DBRow[] getAreaByPsId(String ps_id) throws Exception {
		return floorFolderInfoMgrWp.getAreaByPsId(ps_id);
	}
	@Override
	public DBRow[] getAreaByPsidAndType(String ps_id,String type) throws Exception {
		return floorFolderInfoMgrWp.getAreaByPsidAndType(ps_id,type);
	}
	@Override
	public DBRow[] getDoorByPsId(String ps_id) throws Exception {
		return floorFolderInfoMgrWp.getDoorByPsId(ps_id);
	}
	@Override
	public DBRow[] getAllTitle() throws Exception {
		return floorFolderInfoMgrWp.getAllTitle();
	}

	public void setLocationMgrZJ(LocationMgrIFaceZJ locationMgrZJ) {
		this.locationMgrZJ = locationMgrZJ;
	}	
	public void setFloorLocationAreaXmlImportMgrCc(FloorLocationAreaXmlImportMgrCc floorLocationAreaXmlImportMgrCc) {
		this.floorLocationAreaXmlImportMgrCc = floorLocationAreaXmlImportMgrCc;
	}
	public void setFloorStorageDoorLocationZYZ(FloorStorageDoorLocationMgrZYZ floorStorageDoorLocationZYZ) {
		this.floorStorageDoorLocationZYZ = floorStorageDoorLocationZYZ;
	}
	public void setFloorGoogleMapsMgrCc(FloorGoogleMapsMgrCc floorGoogleMapsMgrCc) {
		this.floorGoogleMapsMgrCc = floorGoogleMapsMgrCc;
	}
	public void setFloorFolderInfoMgrWp(FloorFolderInfoMgrWp floorFolderInfoMgrWp) {
		this.floorFolderInfoMgrWp = floorFolderInfoMgrWp;
	}

	@Override
	public DBRow[] getLocationbyArea(String slc_area,long ps_id) throws Exception {
		return floorFolderInfoMgrWp.getLocationbyArea(slc_area,ps_id);
	}

	@Override
	public DBRow[] getWebcamData(String ps_id) throws Exception {
		return floorFolderInfoMgrWp.getExcelWebcamData(ps_id);
	}
	@Override
	public DBRow[] getPrinterData(String ps_id) throws Exception {
		return floorFolderInfoMgrWp.getPrinterbyPsid(Long.parseLong(ps_id));
	}

	@Override
	public void ddStorageAreaDoor(long position_id, long sdId) {
		try {
			this.floorLocationAreaXmlImportMgrCc.addStorageAreaDoor(position_id ,sdId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
