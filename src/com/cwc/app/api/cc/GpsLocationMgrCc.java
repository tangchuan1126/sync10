package com.cwc.app.api.cc;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.iface.cc.GpsLocationMgrIfaceCc;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.ConstantC;
import com.cwc.app.util.PointD;
import com.cwc.db.DBRow;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

public class GpsLocationMgrCc implements GpsLocationMgrIfaceCc {
	
	private ConstantC constantC;
	
	@Override
	public DBRow[] getCurrentLocation(Map query) throws Exception {
		
		String collection = query.get("collection")+"";
		String aIds = query.get("assetIds")+"";
		String[] aId = aIds.split(",");
		if(aId.length>0){
			Map q = new HashMap();
			List list = new ArrayList();
			for (String s : aId) {
				list.add(new HashMap().put("$or", s));
			}
			q.put("aid", list.toArray());
			q.put("collection", collection);
			queryHis(q);
		}
		return null;
	}
	
	
	//mongo---------------------------------------------------------
	static private Mongo  mongo ;
	public DB   db;
	private DBCollection history;
	private DBCollection lastHistory;
	static { 
		if (mongo == null) {
			try {
				mongo = new Mongo("localhost",27017);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
	} 
	private void auth(DB db){
		if(!db.isAuthenticated()){
			char[] pwd_char = "gps".toCharArray(); 
			boolean auth = db.authenticate("gps",pwd_char);
			if(!auth){ 
				throw new RuntimeException(); 
			}
		}
	}

	public String queryHis(Map query) {
		DBObject queryCondition = new BasicDBObject();
		queryCondition.put("aid", query.get("aid"));
		DBCursor dbCursor = history.find(queryCondition);
		
		
		
		
		DBRow[] rows = new DBRow[dbCursor.size()];
		int i = 0;
		while (dbCursor.hasNext()) {
			DBObject db = dbCursor.next();
			String time = constantC.longToTime(Long.parseLong(db.get("gt").toString()));
			
			db.put("gt", time);
			String t = constantC.longToTime(Long.parseLong(db.get("t").toString()));
			db.put("t", t);
			db.put("sta",constantC.getStatus(db.get("sta").toString()));
			
			double lon = Double.parseDouble(db.get("lon").toString());
			double lat = Double.parseDouble(db.get("lat").toString());
			PointD p = constantC.changeMap(lon,lat,1);
			db.put("lon", p.getX());
			db.put("lat", p.getY());
			
			db.removeField("_id");
			//list.add(db);
		}
		/*List list = new ArrayList();
		while (dbCursor.hasNext()) {
			DBObject db = dbCursor.next();
			String time = ConstantC.longToTime(Long.parseLong(db.get("gt")
					.toString()));
			db.put("gt", time);
			String t = ConstantC.longToTime(Long.parseLong(db.get("t")
					.toString()));
			db.put("t", t);
			db.put("sta", ConstantC.getStatus(db.get("sta").toString()));

			double lon = Double.parseDouble(db.get("lon").toString());
			double lat = Double.parseDouble(db.get("lat").toString());
			PointD p = ConstantC.changeMap(lon, lat, 1);
			db.put("lon", p.getX());
			db.put("lat", p.getY());

			db.removeField("_id");
			list.add(db);
		}*/
		return "";
	}
	//mongo end ---------------------------------------

	public void setConstantC(ConstantC constantC) {
		this.constantC = constantC;
	}
}
