package com.cwc.app.api.cc;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.cc.FloorStorageCatalogLocationMgrCc;
import com.cwc.app.iface.cc.StorageCatalogLocationMgrIfaceCc;
import com.cwc.app.key.GlobalKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class StorageCatalogLocationMgrCc implements	StorageCatalogLocationMgrIfaceCc {
	
	FloorStorageCatalogLocationMgrCc floorStorageCatalogLocationMgrCc;
		
	@Override
	public DBRow[] findStorageCatalogLocation(boolean adidNullToLogin,String pc_line_id, String catalog_id, String ps_id, String code,int union_flag, long adid, String title_id, int type,String lot_number_id, HttpServletRequest request) throws Exception {
		try{
			adid = findAdminIdByCondition(adidNullToLogin, adid, request);
			String[] lotNumberArr = null;
			if(!StringUtil.isBlank(lot_number_id)){
				lotNumberArr = lot_number_id.split(",");
			}
			return floorStorageCatalogLocationMgrCc.findStorageCatalogLocation(this.parseStrToLongArr(pc_line_id), this.parseStrToLongArr(catalog_id), this.parseStrToLongArr(ps_id), code, union_flag, adid, this.parseStrToLongArr(title_id), type, lotNumberArr);
		}
		catch (Exception e){
			throw new Exception("StorageCatalogLocationMgrCc.findStorageCatalogLocation:"+e);
		}
	}
	
	
	private long findAdminIdByCondition(boolean adidNullToLogin, long adid, HttpServletRequest request) throws Exception{
		try{
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));//获取系统当前登录者
			if(GlobalKey.CLIENT_ADGID == adminLoggerBean.getAdgid() && adidNullToLogin && 0 == adid){
				adid = adminLoggerBean.getAdid();
			}
			return adid;
		}
		catch(Exception e){
			throw new SystemException("StorageCatalogLocationMgrCc:"+e);
		}	
	}
	
	private Long[] parseStrToLongArr(String str) throws Exception{
		List<Long> longList = new ArrayList<Long>();
		if(!StringUtil.isBlank(str)){
			String[] arr = str.split(",");
			for (int i = 0; i < arr.length; i++){
				if(!StringUtil.isBlankAndCanParseLong(arr[i])){
					longList.add(Long.valueOf(arr[i]));
				}
			}
		}
		return longList.toArray(new Long[0]);
	}
	
	public void setFloorStorageCatalogLocationMgrCc(
			FloorStorageCatalogLocationMgrCc floorStorageCatalogLocationMgrCc) {
		this.floorStorageCatalogLocationMgrCc = floorStorageCatalogLocationMgrCc;
	}
	
}
