package com.cwc.app.api;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.SuperAdminException;
import com.cwc.app.floor.api.FloorCrmMgr;
import com.cwc.app.iface.CrmMgrIFace;
import com.cwc.app.key.CrmTraceTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

/**
 * 会员相关操作类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CrmMgr implements CrmMgrIFace
{
	static Logger log = Logger.getLogger("ACTION");

	private FloorCrmMgr fcm = null;

	public void synchronizeClients()
		throws Exception
	{
		try
		{
			DBRow clients[] = fcm.getNewOrderClients(null);
			
			for (int i=0; i<clients.length; i++)
			{
				////system.out.println(i+"/"+clients.length);
				
				DBRow newClient = new DBRow();
				newClient.add("adgid", 100198l);
				fcm.addClients(newClient);
			}
			

		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"synchronizeClients",log);
		}
	}
	
	/**
	 * 获得近期频繁购买潜在客户
	 * @param adid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFrequentlyReadyClients(long adid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getFrequentlyReadyClients(adid,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getFrequentlyReadyClients",log);
		}
	}
	
	/**
	 * 获得近期频繁购买必须跟进客户
	 * @param adid
	 * @param period
	 * @param target_type
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFrequentlyMustTraceClients(long adid,int period,int target_type,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getFrequentlyMustTraceClients( adid, period, target_type, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getFrequentlyMustTraceClients",log);
		}
	}
	
	/**
	 * 获得某个客户的跟进
	 * @param ccid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTraceByCcid(long ccid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getTraceByCcid( ccid, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceByCcid",log);
		}
	}
	
	/**
	 * 增加跟进
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addTrace(HttpServletRequest request)
		throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));	
			
			int target_type = StringUtil.getInt(request, "target_type");
			int cc_id = StringUtil.getInt(request, "cc_id");
			int trace_type = StringUtil.getInt(request, "trace_type");
			String trace_date = DateUtil.NowStr();
			String memo = StringUtil.getString(request, "memo");
			
			long ctc_id = this.addTraceSub( target_type, cc_id, trace_type, trace_date, memo,adminLoggerBean.getAdid());
			
			//更新客户数据
			updateClientByCcid( cc_id, trace_date, target_type);
			
			return(ctc_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addTrace",log);
		}
	}	
	
	/**
	 * 更新客户跟踪信息
	 * @param cc_id
	 * @param trace_date
	 * @param target_type
	 * @throws Exception
	 */
	public void updateClientByCcid(long cc_id,String trace_date,int target_type)
		throws Exception
	{
		try
		{
			//更新客户数据
			DBRow oldClientInfo = fcm.getDetailClientByCcid(cc_id);
			
			DBRow newClientInfo = new DBRow();
			newClientInfo.add("last_trace_date", trace_date);
			newClientInfo.add("trace_count", oldClientInfo.get("trace_count", 0)+1);
			newClientInfo.add("target_type", target_type);
			
			if (oldClientInfo.getString("create_trace_date").equals(""))
			{
				newClientInfo.add("create_trace_date", trace_date);
			}
			
			fcm.modClientByCcid(cc_id, newClientInfo);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateClientByCcid",log);
		}
	}
	
	
	
	/**
	 * 增加跟进
	 * @param target_type
	 * @param cc_id
	 * @param trace_type
	 * @param trace_date
	 * @param memo
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public long addTraceSub(int target_type,int cc_id,int trace_type,String trace_date,String memo,long adid,long qoid)
		throws Exception
	{
		try
		{
			DBRow trace = new DBRow();
			trace.add("target_type", target_type);
			trace.add("cc_id", cc_id);
			trace.add("trace_type", trace_type);
			trace.add("trace_date", trace_date);
			trace.add("memo", memo);
			trace.add("adid", adid);
			trace.add("qoid", qoid);
			long ctc_id = fcm.addTrace(trace);
			
			return(ctc_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addTraceSub",log);
		}
	}	
	
	public long addTraceSub(int target_type,int cc_id,int trace_type,String trace_date,String memo,long adid)
		throws Exception
	{
		return(addTraceSub( target_type, cc_id, trace_type, trace_date, memo, adid,0));
	}
	
	/**
	 * 增加特殊关注客户
	 * @throws Exception
	 */
	public void addSpecialClients(HttpServletRequest request)
		throws Exception
	{
		try
		{
			int cc_id = StringUtil.getInt(request, "cc_id");
			int target_type = StringUtil.getInt(request, "target_type");
			
			String trace_date = DateUtil.NowStr();
			String memo = StringUtil.getString(request, "memo");
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));	
			
			DBRow special = new DBRow();
			special.add("cc_id", cc_id);
			fcm.addSpecialClients(special);
			
			//增加跟进记录
			long ctc_id = this.addTraceSub( target_type, cc_id, CrmTraceTypeKey.SPECIAL, trace_date, memo,adminLoggerBean.getAdid());
			
			//更新客户数据
			updateClientByCcid( cc_id, trace_date, target_type);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addSpecialClients",log);
		}
	}
		
	/**
	 * 获得特别关注客户列表
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSpecialClients(long adid,int period,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getSpecialClients(adid,period,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSpecialClients",log);
		}
	}
	
	/**
	 * 取消特别关注
	 * @param request
	 * @throws SuperAdminException
	 * @throws Exception
	 */
	public void delSpecialClientByCscId(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			String trace_date = DateUtil.NowStr();
			int target_type = StringUtil.getInt(request, "target_type");
			long cc_id = StringUtil.getLong(request, "cc_id");
			String memo = StringUtil.getString(request, "memo");
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			
			long csc_id = StringUtil.getLong(request,"csc_id");
			fcm.delSpecialClientByCscId(csc_id);
			
			//增加跟进记录
			DBRow trace = new DBRow();
			trace.add("target_type", target_type);
			trace.add("cc_id", cc_id);
			trace.add("trace_type", CrmTraceTypeKey.CANCEL_SPECIAL);
			trace.add("trace_date", trace_date);
			trace.add("memo", memo);
			trace.add("adid", adminLoggerBean.getAdid());
			fcm.addTrace(trace);
			
			//更新客户数据
			updateClientByCcid( cc_id, trace_date, target_type);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delSpecialClientByCscId",log);
		}
	}
	
	/**
	 * 放弃跟进客户
	 * @param request
	 * @throws Exception
	 */
	public void delClientByCcId(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long cc_id = StringUtil.getLong(request, "cc_id");
			fcm.delClientByCcId(cc_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delClientByCcId",log);
		}
	}
		
		
		
	
	public void setFcm(FloorCrmMgr fcm) 
	{
		this.fcm = fcm;
	}
	
	
}








