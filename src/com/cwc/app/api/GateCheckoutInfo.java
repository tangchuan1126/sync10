package com.cwc.app.api;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class GateCheckoutInfo implements Serializable{
	
	@XmlElement(name = "entryId")
	public String entryId;
	
	@XmlElement(name = "warehouse")
	public String warehouse;

	@XmlElement(name = "equipments")
	public List<Equipment> equipments;
	
	@XmlElement(name = "driverName")
	public String driverName;

	@XmlElement(name = "driverLicense")
	public String driverLicense;

	@XmlElement(name = "mcDot")
	public String mcDot;

	@XmlElement(name = "carrierName")
	public String carrierName;
	
	@XmlElement(name = "tractor")
	public String tractor;
	
	@XmlElement(name = "checkOutTime")
	public String checkOutTime;
	
	

}
