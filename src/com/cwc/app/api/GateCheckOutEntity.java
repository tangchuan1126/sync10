package com.cwc.app.api;

import java.io.Serializable;
import java.util.List;

public class GateCheckOutEntity implements Serializable {
	public String entryId;
	public String warehouse;
	public String tractor;
	public List<String> trailers;
	public String equipmentType;
	
	
}
