package com.cwc.app.api;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.task.NotTaskOwnerException;
import com.cwc.app.floor.api.FloorTaskMgr;
import com.cwc.app.iface.TaskIFace;
import com.cwc.app.key.OrderTaskKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class TaskMgr implements TaskIFace
{
	static Logger log = Logger.getLogger("ACTION");
	
	private AdminMgr adminMgr;
	private FloorTaskMgr ftm;

	/**
	 * 创建订单任务
	 * @param session
	 * @param t_reason
	 * @param t_operation
	 * @param oid
	 * @param t_note
	 * @param t_target_admin
	 * @param t_target_group
	 * @return
	 * @throws Exception
	 */
	public long createOrderTask(AdminLoginBean adminLoggerBean,int t_reason,int t_operationsA[],long oid,String t_note,long t_target_admin,long t_target_group,long ps_id)
		throws Exception
	{
		try
		{
			synchronized(String.valueOf(oid))
			{
				/**
				 * 先检测是否已经存在 当天没执行订单任务
				 */
				DBRow todayUndoOrderTasks[] = ftm.getTodayUnDoOrderTasksByOid(oid,ps_id, null);
				
				String t_operation = "";
				OrderTaskKey orderTaskKey = new OrderTaskKey();
				
				String datetime = DateUtil.NowStr();
				
				//拼装操作
				int i=0;
				for (; i<t_operationsA.length-1; i++)
				{
					t_operation += orderTaskKey.getOperationById(t_operationsA[i])+" -> ";
				}
				t_operation += orderTaskKey.getOperationById(t_operationsA[i]);
				
				DBRow orderTask = new DBRow();
				orderTask.add("t_reason",t_reason);//原因
				orderTask.add("t_operation",t_operation);//操作
				orderTask.add("oid",oid);//订单号
				orderTask.add("t_note",t_note);//任务备注
				orderTask.add("t_target_admin",t_target_admin);//目标执行人
				orderTask.add("t_target_group",t_target_group);//目标执行角色
				orderTask.add("create_admin",adminLoggerBean.getAccount());//创建任务帐号
				orderTask.add("create_date",datetime);//创建日期
				orderTask.add("t_operator","");//实际执行人
				orderTask.add("t_operate_date",datetime);//实际执行日期
				orderTask.add("t_status",OrderTaskKey.T_STATUS_WAITING);//任务状态
				orderTask.add("t_operation_note","");//任务执行备注
				orderTask.add("ps_id",ps_id);
				
				long ot_id = ftm.createOrderTask(orderTask);
				
				/**
				 * 当天没执行订单任务内容包含于新任务执行内容，删除旧任务
				 */
				for (int j=0; j<todayUndoOrderTasks.length; j++)
				{
					if ( t_operation.indexOf(todayUndoOrderTasks[j].getString("t_operation"))!=-1 )
					{
						ftm.delOrderTaskByOid(todayUndoOrderTasks[j].get("ot_id", 0l));
					}
				}
				
				return(ot_id);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"createOrderTask",log);
		}
	}

	/**
	 * 获得所有任务
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOrderTasks(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(ftm.getOrderTasks(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderTasks",log);
		}
	}
	
	public DBRow[] getOrderTasksByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(ftm.getOrderTasksByPsId(ps_id,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderTasksByPsId",log);
		}
	}
	
	public DBRow[] getOrderTasksByOid(long oid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(ftm.getOrderTasksByOid( oid, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderTasksByOid",log);
		}
	}
	
	/**
	 * 获得详细任务内容
	 * @param ot_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailOrderTaskByOtid(long ot_id)
		throws Exception
	{
		try
		{
			return(ftm.getDetailOrderTaskByOtid(ot_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailOrderTaskByOtid",log);
		}
	}
	
	/**
	 * 完成任务
	 * @param request
	 * @throws Exception
	 */
	public void completeOrderTask(HttpServletRequest request)
		throws NotTaskOwnerException,Exception
	{
		try
		{
			AdminLoginBean adminInfo = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String datetime = DateUtil.NowStr();
			
			long ot_id = StringUtil.getLong(request,"ot_id");
			String t_operation_note = StringUtil.getString(request, "t_operation_note");
			DBRow detail = ftm.getDetailOrderTaskByOtid(ot_id);
			
			//需要检测执行人或者角色
			if (detail.get("t_target_admin", 0l)!=0)
			{
				if (detail.get("t_target_admin", 0l)!=adminInfo.getAdid())
				{
					throw new NotTaskOwnerException();
				}
			}
			else if (detail.get("t_target_group", 0l)!=0)
			{
				if (detail.get("t_target_group", 0l)!=adminInfo.getAdgid())
				{
					throw new NotTaskOwnerException();
				}
			}
			
			DBRow row = new DBRow();
			row.add("t_status",OrderTaskKey.T_STATUS_COMPLETE);
			row.add("t_operator",adminInfo.getAccount());
			row.add("t_operate_date",datetime);
			row.add("t_operation_note",t_operation_note);
			ftm.modOrderTaskByOtid(ot_id, row);
		} 
		catch (NotTaskOwnerException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"completeOrderTask",log);
		}
	}
		
	public void unCompleteOrderTask(HttpServletRequest request)
		throws NotTaskOwnerException,Exception
	{
		try
		{
			AdminLoginBean adminInfo = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			String datetime = DateUtil.NowStr();
			
			long ot_id = StringUtil.getLong(request,"ot_id");
			String t_operation_note = StringUtil.getString(request, "t_operation_note");
			DBRow detail = ftm.getDetailOrderTaskByOtid(ot_id);
			
			//需要检测执行人或者角色
			if (detail.get("t_target_admin", 0l)!=0)
			{
				if (detail.get("t_target_admin", 0l)!=adminInfo.getAdid())
				{
					throw new NotTaskOwnerException();
				}
			}
			else if (detail.get("t_target_group", 0l)!=0)
			{
				if (detail.get("t_target_group", 0l)!=adminInfo.getAdgid())
				{
					throw new NotTaskOwnerException();
				}
			}
			
			DBRow row = new DBRow();
			row.add("t_status",OrderTaskKey.T_STATUS_UNCOMPLETE);
			row.add("t_operator",adminInfo.getAccount());
			row.add("t_operate_date",datetime);
			row.add("t_operation_note",t_operation_note);
			ftm.modOrderTaskByOtid(ot_id, row);
		} 
		catch (NotTaskOwnerException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"unCompleteOrderTask",log);
		}
	}
	
	public DBRow getTasksCountByOidStatus(long oid,int status)
		throws Exception
	{
		try
		{
			return(ftm.getTasksCountByOidStatus( oid, status));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getTasksCountByOidStatus",log);
		}
	}

	public int getOrderTasksCount()
		throws Exception
	{
		try
		{
			return(ftm.getOrderTasksCount());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderTasksCount",log);
		}
	}
		
	public DBRow[] getTodayUnDoOrderTasksByOid(long oid,long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(ftm.getTodayUnDoOrderTasksByOid(oid,ps_id,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getTodayUnDoOrderTasksByOid",log);
		}
	}
		
	
	public static void main(String args[])
	{
		String aaa = "撕毁原单 -> 重新打印";
		String bbb = "撕毁原单 -> 重新打印";
		
		////system.out.println( bbb.indexOf(aaa) );
	}
	
		
		
	
	

	public void setAdminMgr(AdminMgr adminMgr)
	{
		this.adminMgr = adminMgr;
	}

	public void setFtm(FloorTaskMgr ftm)
	{
		this.ftm = ftm;
	}

	
	
	
	
}








