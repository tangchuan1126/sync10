package com.cwc.app.api;

import java.io.IOException;

import net.sf.json.JSONObject;



import org.drools.lang.dsl.DSLMapParser.value_chunk_return;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;


public class CheckoutQueueConsumer implements Consumer, Runnable {

	private Channel channel;
	private Connection connection;
	private ConnectionFactory factory;
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private final static int MAX_RETRY_TIMES = 5; 
	/*private final static String ROUTE_KEY = "checkout-entry-queue"; */
	
	public CheckoutQueueConsumer() {
		WebApplicationContext webContext = ContextLoader .getCurrentWebApplicationContext();
		checkInMgrZwb = (CheckInMgrIfaceZwb) webContext.getBean("checkInMgrZwb");
	}

	@Override
	public void run() {
		initChannel(0);
	}

	private void initChannel(int times) {
		if(times >= MAX_RETRY_TIMES) 
			return;
		factory = new ConnectionFactory();
		factory.setHost(checkInMgrZwb.getRabbitMqServer());
		factory.setUsername(checkInMgrZwb.getRabbitMqUserName());
		factory.setPassword(checkInMgrZwb.getRabbitMqPassword());
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			channel.basicConsume(checkInMgrZwb.getCheckOutQueue(), false, this);
			
			System.out.println("============Init rabbit MQ of check out finished:["+ checkInMgrZwb.getRabbitMqServer() +"]============");
		} catch (Exception e) {
			channel = null;
			e.printStackTrace();
			try {
				Thread.sleep(1000 * 60);
			} catch (InterruptedException e1) {}
			times++;
			System.out.println("===========retry:" + times + "=============");
			initChannel(times);
		}
	}

	@Override
	public void handleConsumeOk(String consumerTag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleCancelOk(String consumerTag) {
	}

	@Override
	public void handleCancel(String consumerTag) throws IOException {
	}

	@Override
	public void handleDelivery(String arg0, Envelope envelope,BasicProperties properties, byte[] body) throws IOException {
		try {
			String strJson = new String(body);
			JSONObject json = JSONObject.fromObject(strJson);
			if(json.containsKey("entryId")){
				String warehouse = getSaftValue("warehouse", json);
				long psId = Warehouse.getWareHourseId(warehouse);
				if (psId != 0) {
					checkInMgrZwb.addYmsCheckOutRequest(json.getString("entryId"), psId, strJson);
				}
			}
			channel.basicAck(envelope.getDeliveryTag(), false);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}
	
	private String getSaftValue(String key, JSONObject json){
		String value = null;
		if(json.containsKey(key)){
			value = json.getString(key);
		}
		return value;
	}
	
	@Override
	public void handleShutdownSignal(String consumerTag,
			ShutdownSignalException sig) {
		// TODO Auto-generated method stub
	}

	@Override
	public void handleRecoverOk(String consumerTag) {
		// TODO Auto-generated method stub
	}

}
