package com.cwc.app.api.wp;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.cwc.app.floor.api.cc.FloorLocationAreaXmlImportMgrCc;
import com.cwc.app.floor.api.wp.FloorFolderInfoMgrWp;
import com.cwc.app.iface.wp.CreateKmlMgrIfaceWp;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

public class CreateKmlMgrWp implements CreateKmlMgrIfaceWp{
	
	
	private FloorFolderInfoMgrWp floorFolderInfoMgrWp;
	private FloorLocationAreaXmlImportMgrCc floorLocationAreaXmlImportMgrCc;
	
	
	/**
	 *  查询基础数据
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public  DBRow[] qureyBaseData(String ps_id) throws Exception{
		
		try {
			return floorFolderInfoMgrWp.getExcelBaseData(ps_id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("CreateKmlMgrWp.qureyBaseData():"+e);
		}
		
		
	} 
	
	@Override
	public List<DBRow> createKmlNew(String ps_id) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String filename = sdf.format(new Date()) + ".kml";
		List<DBRow> rows = new ArrayList<DBRow>(); // 返回页面显示的数据
		// 生成除location之外的kml
		Document doc = DocumentHelper.createDocument();
		Element kml = doc.addElement("kml");
		kml.addAttribute("xmlns", "http://www.opengis.net/kml/2.2");
		Element document = kml.addElement("Document"); // 生成kml文件
		document.addElement("name").setText("Data from ");
		Element warehouse = document.addElement("Folder");
		warehouse.addElement("name").setText("WarehouseBase");
		// location单独生成一个kml
		Document doc_loc = DocumentHelper.createDocument();
		Element kml_loc = doc_loc.addElement("kml");
		kml_loc.addAttribute("xmlns", "http://www.opengis.net/kml/2.2");
		Element document_loc = kml_loc.addElement("Document"); // 生成kml文件
		document_loc.addElement("name").setText("Data from ");
		Element warehouse_loc = document_loc.addElement("Folder");
		warehouse_loc.addElement("name").setText("WarehouseBase");
		DBRow[] baseRow = qureyBaseData(ps_id);
		// 将warehouse写入kml
		String whCoordinates = "";
		for (int i = 0; i < baseRow.length; i++) {
			if ("warehouse".equals(baseRow[i].getString("type").toLowerCase())) {
				whCoordinates += baseRow[i].getString("lng") + ","
						+ baseRow[i].getString("lat") + ",0.0 ";
			}
		}
		whCoordinates += whCoordinates.substring(0, whCoordinates.indexOf(" "));
		Element placemark = warehouse.addElement("Placemark");
		placemark.addElement("styleUrl").setText("#style-warehouse");
		placemark.addElement("name").setText("base");
		Element lineString = placemark.addElement("LineString");
		lineString.addElement("tessellate").setText("0");
		lineString.addElement("coordinates").setText(whCoordinates);

		DBRow[] location = floorFolderInfoMgrWp.getExcelLocationData(ps_id);
		DBRow[] area = floorFolderInfoMgrWp.getExcelAreaData(ps_id);
		DBRow[] staging = floorFolderInfoMgrWp.getExcelStagingData(ps_id);
		DBRow[] parking = floorFolderInfoMgrWp.getExcelParkingData(ps_id);
		DBRow[] docks = floorFolderInfoMgrWp.getExcelDocksData(ps_id);
		String pName = null;
		String areaName = null;
		String areaNext = null;
		for (int i = 0; i < location.length; i++) {
			areaName = location[i].getString("area_name").trim();
			String locationNameAll = location[i].getString("slc_position_all").trim();
			String locationName = location[i].getString("slc_position").trim();
			placemark = warehouse_loc.addElement("Placemark");
			//locationName = locationName.replace("_", "-");
			pName = "location" + "_" + locationNameAll + "_" + locationName; // 方便商品库存位置查询
			placemark.addElement("styleUrl").setText("#style-" + "location");
			placemark.addElement("description").setText(ps_id + "_" + filename + "_" + areaName);
			placemark.addElement("name").setText(pName);
			
			Element linearRing = placemark.addElement("Polygon").addElement("outerBoundaryIs").addElement("LinearRing");
			linearRing.addElement("tessellate").setText("0");
			linearRing.addElement("coordinates").setText(location[i].getString("latlng"));
		}
		for (int i = 0; i < area.length; i++) {
			areaName = area[i].getString("area_name").trim();
			placemark = warehouse.addElement("Placemark");
			pName = "area" + "_" + areaName;
			
			placemark.addElement("styleUrl").setText("#style-area");
			placemark.addElement("description").setText(ps_id+"_"+filename+"_"+areaName);
			placemark.addElement("name").setText(pName);
			
			lineString = placemark.addElement("LineString");
			lineString.addElement("tessellate").setText("0");
			lineString.addElement("coordinates").setText(
					area[i].getString("latlng"));
		}
		for (int i = 0; i < staging.length; i++) {
			placemark = warehouse.addElement("Placemark");
			areaNext = staging[i].getString("location_name").trim();
			pName = "staging" + "_" + areaNext;
			
			placemark.addElement("styleUrl").setText("#style-staging");
			placemark.addElement("description").setText(ps_id+"_"+filename+"_"+areaName);
			placemark.addElement("name").setText(pName);
			
			Element linearRing = placemark.addElement("Polygon")
					.addElement("outerBoundaryIs").addElement("LinearRing");
			linearRing.addElement("tessellate").setText("0");
			linearRing.addElement("coordinates").setText(
					staging[i].getString("latlng"));
		}
		for (int i = 0; i < parking.length; i++) {
			placemark = warehouse.addElement("Placemark");
			areaNext = parking[i].getString("yc_no").trim();
			pName = "parking" + "_" + areaNext;
			
			placemark.addElement("styleUrl").setText("#style-parking");
			placemark.addElement("description").setText(ps_id+"_"+filename+"_"+areaName);
			placemark.addElement("name").setText(pName);
			
			Element linearRing = placemark.addElement("Polygon")
					.addElement("outerBoundaryIs").addElement("LinearRing");
			linearRing.addElement("tessellate").setText("0");
			linearRing.addElement("coordinates").setText(
					parking[i].getString("latlng"));
		}
		for (int i = 0; i < docks.length; i++) {
			placemark = warehouse.addElement("Placemark");
			areaNext = docks[i].getString("doorId").trim();
			pName = "docks" + "_" + areaNext;
			
			placemark.addElement("styleUrl").setText("#style-docks");
			placemark.addElement("description").setText(ps_id+"_"+filename+"_"+areaName);
			placemark.addElement("name").setText(pName);
			
			Element linearRing = placemark.addElement("Polygon").addElement("outerBoundaryIs").addElement("LinearRing");
			linearRing.addElement("tessellate").setText("0");
			linearRing.addElement("coordinates").setText(
					docks[i].getString("latlng"));
		}

	
		// kml样式
		Map<String, String> styleMap = new HashMap<String, String>();
		// 线，线宽，填充
		styleMap.put("warehouse", "AAAAAA,3,00AAAAAA");
		styleMap.put("area", "AAAAAA,2,0096FB3D");
		styleMap.put("location", "465079,1,FF7DFAB8");
		// styleMap.put("docks", "FDDD90,2,FFFCFFB4");
		styleMap.put("docks", "6ED469,2,FFBFFB93");
		styleMap.put("staging", "0F6E96,1,FFB4FFFF");
		styleMap.put("parking", "6ED469,2,FFBFFB93");
		Set<String> styleKey = styleMap.keySet();
		for (String s : styleKey) {
			Element style = document.addElement("Style");
			style.addAttribute("id", "style-" + s);
			Element lineStyle = style.addElement("LineStyle");
			lineStyle.addElement("color").setText(
					"FF" + styleMap.get(s).split(",")[0]);
			lineStyle.addElement("width").setText(
					styleMap.get(s).split(",")[1] + "");
			Element polyStyle = style.addElement("PolyStyle");
			polyStyle.addElement("color").setText(
					styleMap.get(s).split(",")[2] + "");
			polyStyle.addElement("fill").setText("1");
			polyStyle.addElement("outline").setText("1");
			style = document_loc.addElement("Style");
			style.addAttribute("id", "style-" + s);
			lineStyle = style.addElement("LineStyle");
			lineStyle.addElement("color").setText(
					"FF" + styleMap.get(s).split(",")[0]);
			lineStyle.addElement("width").setText(
					styleMap.get(s).split(",")[1] + "");
			polyStyle = style.addElement("PolyStyle");
			polyStyle.addElement("color").setText(
					styleMap.get(s).split(",")[2] + "");
			polyStyle.addElement("fill").setText("1");
			polyStyle.addElement("outline").setText("1");
		}

		// 保存为kml文件
		OutputFormat format = OutputFormat.createPrettyPrint();
		// location
		XMLWriter writer = new XMLWriter(new FileOutputStream(
				Environment.getHome() + "upload/kml/loc_" + filename), format);
		writer.write(doc_loc);
		writer.flush();
		// 非location
		writer = new XMLWriter(new FileOutputStream(Environment.getHome()
				+ "upload/kml/" + filename), format);
		writer.write(doc);
		writer.flush();
		writer.close();
		// update storage_kml表
		DBRow row = new DBRow();
		row.add("ps_id", ps_id);
		row.add("kml", filename);
		row.add("update_time", System.currentTimeMillis());
		floorLocationAreaXmlImportMgrCc.addOrUpdateStorageKml(row);
		return rows;
	}
	public void setFloorFolderInfoMgrWp(FloorFolderInfoMgrWp floorFolderInfoMgrWp) {
		this.floorFolderInfoMgrWp = floorFolderInfoMgrWp;
	}

	public void setFloorLocationAreaXmlImportMgrCc(
			FloorLocationAreaXmlImportMgrCc floorLocationAreaXmlImportMgrCc) {
		this.floorLocationAreaXmlImportMgrCc = floorLocationAreaXmlImportMgrCc;
	}
   
}
