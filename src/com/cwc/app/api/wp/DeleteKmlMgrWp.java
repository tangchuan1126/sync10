package com.cwc.app.api.wp;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.floor.api.wp.FloorFolderInfoMgrWp;
import com.cwc.app.iface.cc.LocationAreaXmlImportMgrIfaceCc;
import com.cwc.app.iface.wp.DeleteKmlMgrIfaceWp;
import com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.cwc.app.util.ConfigBean;
import com.cwc.json.JsonObject;
import com.cwc.app.util.Environment;
import com.cwc.util.StringUtil;

public class DeleteKmlMgrWp implements DeleteKmlMgrIfaceWp {

	private FloorFolderInfoMgrWp floorFolderInfoMgrWp;
	private LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc;
	private ProductStoreMgrIFaceZJ productStoreMgr;
	private QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp;

	@Override
	public void doAllDelete(String ps_id) throws Exception {
		try {
			deleteDoor(ps_id);
			deleteErea(ps_id);
			deleteYardControl(ps_id);
			deleteCatelog(ps_id);
			deleteLocation(ps_id);
			deleteWebcam(ps_id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public void doDeleteKml(HttpServletRequest request) throws Exception {

		/*String ps_id = StringUtil.getString(request, "area_psid");
		DBRow[] rows = productStoreMgr.getProductStoreCount(ps_id, 0, 0, "", 0,	"", "", 0);
		DBRow data = new DBRow();
		if (rows.length == 0) {
			data.add("flag", "true");
			data.add("result", "Successful implementation!");
			try {
				doAllDelete(ps_id);
				locationAreaXmlImportMgrCc.saveLocationArea(request);
				//删除无效kml
				queryKmlInfoMgrWp.deleteInvalidKml(Environment.getHome() + "upload/kml");
			} catch (Exception e) {
				e.printStackTrace();
				data.add("flag", "false");
				data.add("result", "Delete old data failed!");
				throw new JsonException(new JsonObject(data));
			}
		} else {
			data.add("flag", "false");
			data.add("result", "Storage is not empty");

		}
		throw new JsonException(new JsonObject(data));
		*/
	}

	@Override
	public boolean  deleteDoor(String ps_id) throws Exception {
		// TODO Auto-generated method stub
		boolean result =false;
		String tablename = ConfigBean.getStringValue("storage_door");
		String field ="ps_id";
		 try {
			floorFolderInfoMgrWp.deleteKmlTabel(field,ps_id, tablename);
			result =true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return result;
	}

	@Override
	public boolean  deleteErea(String ps_id) throws Exception {
		// TODO Auto-generated method stub
		boolean result =false;
		String tablename = ConfigBean.getStringValue("storage_location_area");
		String field ="area_psid";
		 try {
			floorFolderInfoMgrWp.deleteKmlTabel(field,ps_id, tablename);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return result;
	}

	@Override
	public boolean  deleteYardControl(String ps_id) throws Exception {
		// TODO Auto-generated method stub
		boolean result =false;
		String tablename = ConfigBean.getStringValue("storage_yard_control");
		String field ="ps_id";
		 try {
			floorFolderInfoMgrWp.deleteKmlTabel(field,ps_id, tablename);
			result =true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return result;
	}

	@Override
	public boolean  deleteCatelog(String ps_id) throws Exception {
		// TODO Auto-generated method stub
		boolean result =false;
		String tablename = ConfigBean.getStringValue("storage_location_catalog");
		String field ="slc_psid";
		 try {
			floorFolderInfoMgrWp.deleteKmlTabel(field,ps_id, tablename);
			
			//图数据库待处理
			
			
			result =true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return result;
	}

	@Override
	public boolean  deleteLocation(String ps_id) throws Exception {
		// TODO Auto-generated method stub
		boolean result =false;
		String tablename = ConfigBean.getStringValue("storage_load_unload_location");
		String field ="psc_id";
		try {
			floorFolderInfoMgrWp.deleteKmlTabel(field,ps_id, tablename);
			result =true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public void setFloorFolderInfoMgrWp(FloorFolderInfoMgrWp floorFolderInfoMgrWp) {
		this.floorFolderInfoMgrWp = floorFolderInfoMgrWp;
	}

	@Override
	public boolean  deleteWebcam(String ps_id) throws Exception {
         boolean result =false;
		 try {
			floorFolderInfoMgrWp.deleteWebcam(ps_id);
			result =true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return  result;
	}

	@Override
	public boolean deleteCheckInZone(String ps_id) throws Exception {
		// TODO Auto-generated method stub
		boolean result =false;
		try {
			floorFolderInfoMgrWp.deleteCheckInZone(ps_id);
			result =true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		
	}

	public FloorFolderInfoMgrWp getFloorFolderInfoMgrWp() {
		return floorFolderInfoMgrWp;
	}

	public LocationAreaXmlImportMgrIfaceCc getLocationAreaXmlImportMgrCc() {
		return locationAreaXmlImportMgrCc;
	}
	public void setLocationAreaXmlImportMgrCc(
			LocationAreaXmlImportMgrIfaceCc locationAreaXmlImportMgrCc) {
		this.locationAreaXmlImportMgrCc = locationAreaXmlImportMgrCc;
	}
	public void setProductStoreMgr(ProductStoreMgrIFaceZJ productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}
	public void setQueryKmlInfoMgrWp(QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp) {
		this.queryKmlInfoMgrWp = queryKmlInfoMgrWp;
	}
	
}
