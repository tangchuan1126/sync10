package com.cwc.app.api.wp;

import java.io.File;

import com.cwc.app.floor.api.wp.FloorKmlInfoMgrWp;
import com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp;
import com.cwc.app.key.WebcamPositionTypeKey;
import com.cwc.db.DBRow;

public class QueryKmlInfoMgrWp implements QueryKmlInfoMgrIfaceWp {
      
	private  FloorKmlInfoMgrWp floorKmlInfoMgrWp;
	
	@Override
	public long getPositionId(long psId, String  position, int position_type)
			throws Exception {
		// TODO Auto-generated method stub
		DBRow dbrow= new  DBRow();
		long id = 0l;
		switch(position_type){
			case WebcamPositionTypeKey.LOCATION : 
				dbrow= floorKmlInfoMgrWp.getLocationByPosition(psId, position);
				if(dbrow != null){
					id = dbrow.get("slc_id", 0l);
				}
				break;
			case WebcamPositionTypeKey.STAGING : 
				dbrow= floorKmlInfoMgrWp.getStagingByPosition(psId, position);
				if(dbrow != null){
					id = dbrow.get("id", 0l);
				}
				break;
			case WebcamPositionTypeKey.DOCKS : 
				dbrow= floorKmlInfoMgrWp.getDoorByPosition(psId, position);
				if(dbrow != null){
					id = dbrow.get("sd_id", 0l);
				}
				break;
			case WebcamPositionTypeKey.PARKING : 
				dbrow= floorKmlInfoMgrWp.getParkingByPosition(psId, position);
				if(dbrow != null){
					id = dbrow.get("yc_id", 0l);
				}
				break;
			case WebcamPositionTypeKey.AREA : 
				dbrow= floorKmlInfoMgrWp.getAreaByPosition(psId, position);
				if(dbrow != null){
					id = dbrow.get("area_id", 0l);
				}
				break;
		}
		return id;
	}

	public void setFloorKmlInfoMgrWp(FloorKmlInfoMgrWp floorKmlInfoMgrWp) {
		this.floorKmlInfoMgrWp = floorKmlInfoMgrWp;
	}

	@Override
	public boolean deleteInvalidKml(String path) throws Exception {
		boolean result = false;
		DBRow[] rows = floorKmlInfoMgrWp.queryStorageKml();
		File file = new File(path);
		if (!file.isDirectory()) {
			////system.out.println("路径为一个文件");
		} else {
			////system.out.println("路径是一个文件夹");
			String[] kmlNames = file.list();// 获得该文件夹下面的全部文件
			for (int i = 0; i <  kmlNames.length; i++) {
				String kmlName = kmlNames[i];
				boolean delete = true;
				for (int j = 0; j < rows.length; j++) {
					String kml = rows[j].getString("kml");
					String loc_kml = "loc_"+kml;
					if (kml.equals(kmlName) || loc_kml.equals(kmlName)) {
						delete = false;
					}
				}
				if(delete){
					String deletePath = path +"/"+ kmlName;
					File deletefile = new File(deletePath);
					deletefile.delete();
					result = true;
				}
			}
		}
		return result;
	}

}
