package com.cwc.app.api.wp;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import com.cwc.app.floor.api.wp.FloorFolderInfoMgrWp;
import com.cwc.app.iface.wp.ExportFolderDataMgrIfaceWp;
import com.cwc.db.DBRow;

public class ExportFolderDataMgrWp implements ExportFolderDataMgrIfaceWp {
	private FloorFolderInfoMgrWp floorFolderInfoMgrWp;

	public boolean createExcelNew(String ps_id, String path) throws Exception {
		
		try {
			WritableWorkbook workbook = null;
			String outputFile = path;
			OutputStream os;
			os = new FileOutputStream(outputFile);
			workbook = Workbook.createWorkbook(os);
			WritableSheet webcam = workbook.createSheet("Webcam", 0);
			WritableSheet parking = workbook.createSheet("Parking", 0);
			WritableSheet staging = workbook.createSheet("Staging", 0);
			WritableSheet docks = workbook.createSheet("Docks", 0);
			WritableSheet location = workbook.createSheet("Location", 0);
			WritableSheet area = workbook.createSheet("Zone", 0);
			WritableSheet base = workbook.createSheet("Base", 0);
			DBRow[] baseDate = floorFolderInfoMgrWp.getExcelBaseData(ps_id);
			DBRow[] areaData = floorFolderInfoMgrWp.getExcelAreaData(ps_id);
			DBRow[] docksData = floorFolderInfoMgrWp.getExcelDocksData(ps_id);
			DBRow[] parkingData = floorFolderInfoMgrWp.getExcelParkingData(ps_id);
			DBRow[] locationData = floorFolderInfoMgrWp.getExcelLocationData(ps_id);
			DBRow[] stagingData = floorFolderInfoMgrWp.getExcelStagingData(ps_id);
			DBRow[] webcamData = floorFolderInfoMgrWp.getExcelWebcamData(ps_id);
			String[] str = new String[9];
			String[] webcamStr = new String[11];
			int num = 1;
			Pattern pattern = Pattern.compile("^[0-9]+(.[0-9]*)?$");
			for (int m = 0; m < baseDate.length; m++) {
				str[0] = baseDate[m].getString("type", "");
				str[1] = baseDate[m].getString("param", "");
				str[2] = baseDate[m].getString("lng", "");
				str[3] = baseDate[m].getString("lat", "");
				for (int n = 0; n < str.length; n++) {
					if (n == 0 || n == 1) {
						Matcher match = pattern.matcher(str[n]);
						if (!match.matches()) {
							Label label = new Label(n, num, str[n]);
							base.addCell(label);
						} else {
							jxl.write.Number number = new jxl.write.Number(n,
									num, Double.parseDouble(str[n]));
							base.addCell(number);
						}
					} else if (n == 2 || n == 3) {
						if (str[n] != null && !str[n].equals("")) {
							jxl.write.Number number = new jxl.write.Number(n,
									num, Double.parseDouble(str[n]));
							base.addCell(number);
						} else {
							Label label = new Label(n, num, str[n]);
							base.addCell(label);
						}
					}
				}
				num++;
			}
			num=1;
			String[] head = {"Type","Zone Name","Titles","Docks","X","Y","X-Length","Y-Length","Angle"};
			for(int i=0; i<head.length; i++){
				area.addCell(new Label(i, num, head[i]));
			}
			if (areaData.length > 0) {
				num++;
				for (int i = 0; i < areaData.length; i++) {
					str[0] = "Zone";
					str[1] = areaData[i].getString("area_name");
					str[2] = areaData[i].getString("title_name");
					str[3] = areaData[i].getString("doorId");
					str[4] = areaData[i].getString("x");
					str[5] = areaData[i].getString("y");
					str[6] = areaData[i].getString("height");
					str[7] = areaData[i].getString("width");
					str[8] = areaData[i].getString("angle");
					for (int j = 0; j < str.length; j++) {
						if (j == 0 || j == 1 || j == 2 || j == 3 ) {
							Label label = new Label(j, num, str[j].toString());
							area.addCell(label);
						} else if (j == 4 || j == 5 || j == 6 || j == 7
								|| j == 8 ) {
							if (str[j] != null && !str[j].equals("")) {
								jxl.write.Number number = new jxl.write.Number(
										j, num, Double.parseDouble(str[j]));
								area.addCell(number);
							} else {
								/*
								 * Label label = new Label(j, i,
								 * str[j].toString()); sheet1.addCell(label);
								 */
							}
						}
					}
					
					num++;
				}
			}
			// docks
			num = 1;
			String[] docks_head = {"Type","Zone","Dock Name","Storage Type","X","Y","X-Length","Y-Length","Angle"};
			for(int i=0; i<head.length; i++){
				docks.addCell(new Label(i, num, docks_head[i]));
			}
			if (docksData.length > 0) {
				
				num++;
				for (int i = 0; i < docksData.length; i++) {
					str[0] = "Docks";
					str[1] = docksData[i].getString("area_name");
					str[2] = docksData[i].getString("doorId");
					str[3] = "";
					str[4] = docksData[i].getString("x");
					str[5] = docksData[i].getString("y");
					str[6] = docksData[i].getString("height");
					str[7] = docksData[i].getString("width");
					str[8] = docksData[i].getString("angle");
					for (int j = 0; j < str.length; j++) {
						if (j == 0 || j == 1 ||j== 2 ){
							Label label = new Label(j, num, str[j].toString());
							docks.addCell(label);
						} else if ( j == 4|| j == 5 || j == 6 || j == 7
								|| j == 8 ) {
							if (str[j] != null && !str[j].equals("")) {
								jxl.write.Number number = new jxl.write.Number(
										j, num, Double.parseDouble(str[j]));
								docks.addCell(number);
							} else {
								/*
								 * Label label = new Label(j, i,
								 * str[j].toString()); sheet1.addCell(label);
								 */
							}
						}
					}
					num++;
				}
			}
			// location
			num = 1;
			String[] location_head = {"Type","Zone","Location Name","Storage Type","X","Y","X-Length","Y-Length","Angle"};
			for(int i=0; i<head.length; i++){
				location.addCell(new Label(i, num, location_head[i]));
			}
			if (locationData.length > 0) {
				num++;
				for (int i = 0; i < locationData.length; i++) {
					str[0] = "Location";
					str[1] = locationData[i].getString("area_name");
					str[2] = locationData[i].getString("slc_position");
					str[3] = locationData[i].getString("is_three_dimensional");
					str[4] = locationData[i].getString("x");
					str[5] = locationData[i].getString("y");
					str[6] = locationData[i].getString("height");
					str[7] = locationData[i].getString("width");
					str[8] = locationData[i].getString("angle");
					for (int j = 0; j < str.length; j++) {
						if (j == 0 || j == 1 || j == 2 || j == 3 ) {
							if(str[3].equals("0")||str[3]=="0"){
								str[3]="2D";
							}else if(str[3].equals("1")||str[3]=="1"){
								str[3]="3D";
							}
							Label label = new Label(j, num, str[j].toString());
							location.addCell(label);
						} else if (j == 4 || j == 5 || j == 6 || j == 7
								|| j == 8 ) {
							if (str[j] != null && !str[j].equals("")) {
								jxl.write.Number number = new jxl.write.Number(
										j, num, Double.parseDouble(str[j]));
								location.addCell(number);
							} else {
								/*
								 * Label label = new Label(j, i,
								 * str[j].toString()); sheet1.addCell(label);
								 */
							}
						}
					}
					num++;
				}
				
			}
			// parking
			num = 1;
			String[] parking_head = {"Type","Zone","Parking Name","Storage Type","X","Y","X-Length","Y-Length","Angle"};
			for(int i=0; i<head.length; i++){
				parking.addCell(new Label(i, num, parking_head[i]));
			}
			if (parkingData.length > 0) {
				num++;
				for (int i = 0; i < parkingData.length; i++) {
					str[0] = "Parking";
					str[1] = parkingData[i].getString("area_name");
					str[2] = parkingData[i].getString("yc_no");
					str[3] = "";
					str[4] = parkingData[i].getString("x");
					str[5] = parkingData[i].getString("y");
					str[6] = parkingData[i].getString("height");
					str[7] = parkingData[i].getString("width");
					str[8] = parkingData[i].getString("angle");
					for (int j = 0; j < str.length; j++) {
						if (j == 0 ||j==1|| j == 2  ) {
							Label label = new Label(j, num, str[j].toString());
							parking.addCell(label);
						} else if (j == 4 || j == 5 || j == 6 || j == 7
								|| j == 8 ) {
							if (str[j] != null && !str[j].equals("")) {
								jxl.write.Number number = new jxl.write.Number(
										j, num, Double.parseDouble(str[j]));
								parking.addCell(number);
							} else {
								/*
								 * Label label = new Label(j, i,
								 * str[j].toString()); sheet1.addCell(label);
								 */
							}
						}
					}
					num++;
				}
			}
			// staging
			num = 1;
			String[] staging_head = {"Type","Docks","Staging Name","Storage Type","X","Y","X-Length","Y-Length","Angle"};
			for(int i=0; i<head.length; i++){
				staging.addCell(new Label(i, num, staging_head[i]));
			}
			
			if (stagingData.length > 0) {
				num++;
				for (int i = 0; i < stagingData.length; i++) {
					str[0] = "Staging";
					str[1] = stagingData[i].getString("doorId");
					str[2] = stagingData[i].getString("location_name");
					str[3] = "";
					str[4] = stagingData[i].getString("x");
					str[5] = stagingData[i].getString("y");
					str[6] = stagingData[i].getString("height");
					str[7] = stagingData[i].getString("width");
					str[8] = stagingData[i].getString("angle");
					for (int j = 0; j < str.length; j++) {
						if (j == 0  || j == 1 || j== 2 ) {
							Label label = new Label(j, num, str[j].toString());
							staging.addCell(label);
						} else if (j == 4 || j == 5 || j == 6 || j == 7
								|| j == 8 ) {
							if (str[j] != null && !str[j].equals("")) {
								jxl.write.Number number = new jxl.write.Number(
										j, num, Double.parseDouble(str[j]));
								staging.addCell(number);
							} else {
								/*
								 * Label label = new Label(j, i,
								 * str[j].toString()); sheet1.addCell(label);
								 */
							}
						}
					}
					num++;
				}
			}
			num=1;
			String[] webcam_head ={"Type","IP","Port","User","Password","X","Y","Inner_Radius","Outer_Radius","S_Degree","E_Degree"};
			for(int i=0; i<head.length; i++){
				webcam.addCell(new Label(i, num, webcam_head[i]));
			}
			if(webcamData.length>0){
			num++;	
			for (int i = 0; i < webcamData.length; i++) {
				webcamStr[0] = "Webcam";
				webcamStr[1] = webcamData[i].getString("ip");
				webcamStr[2] = webcamData[i].getString("port");
				webcamStr[3] = webcamData[i].getString("user");
				webcamStr[4] = webcamData[i].getString("password");
				webcamStr[5] = webcamData[i].getString("x");
				webcamStr[6] = webcamData[i].getString("y");
				webcamStr[7] = webcamData[i].getString("inner_radius");
				webcamStr[8] = webcamData[i].getString("outer_radius");
				webcamStr[9] = webcamData[i].getString("s_degree");
				webcamStr[10] = webcamData[i].getString("e_degree");
				
				for (int j = 0; j < webcamStr.length; j++) {
					
					if (j == 0||j == 1||j==3||j==4) {
						Label label = new Label(j, num, webcamStr[j].toString());
						webcam.addCell(label);
						
					}else if (j == 2 || j == 5 || j == 6
							|| j == 7||j==8||j==9||j==10 ) {
						if (webcamStr[j] != null && !webcamStr[j].equals("")) {
							jxl.write.Number number = new jxl.write.Number(
									j, num, Double.parseDouble(webcamStr[j]));
							webcam.addCell(number);
						} else {
							/*
							 * Label label = new Label(j, i,
							 * str[j].toString()); sheet1.addCell(label);
							 */
						}
					}
				}
				num++;
			}
				
			}
			// 输出到文件
			workbook.write();
			// 关闭文件
			workbook.close();
			os.flush();
			os.close();
			return true;
			
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (WriteException ex) {
			ex.printStackTrace();
		}
		return false;
	}
	

	public void setFloorFolderInfoMgrWp(FloorFolderInfoMgrWp floorFolderInfoMgrWp) {
		this.floorFolderInfoMgrWp = floorFolderInfoMgrWp;
	}


}