package com.cwc.app.api.wp;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.floor.api.cc.FloorLocationAreaXmlImportMgrCc;
import com.cwc.app.floor.api.zl.FloorStorageDoorLocationMgrZYZ;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.cc.GoogleMapsMgrIfaceCc;
import com.cwc.app.iface.wp.CreateKmlMgrIfaceWp;
import com.cwc.app.iface.wp.EditStorageInfoMgrIfaceWp;
import com.cwc.app.iface.wp.QueryKmlInfoMgrIfaceWp;
import com.cwc.app.iface.zj.LocationMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.app.key.WebcamPositionTypeKey;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.cwc.json.JsonObject;
import com.cwc.util.StringUtil;

public class EditStorageInfoMgrWp implements EditStorageInfoMgrIfaceWp {

	private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;
	private LocationMgrIFaceZJ locationMgrZJ;
	private FloorStorageDoorLocationMgrZYZ floorStorageDoorLocationZYZ;
	private FloorLocationAreaXmlImportMgrCc floorLocationAreaXmlImportMgrCc;
	private GoogleMapsMgrIfaceCc googleMapsMgrCc;
	private CreateKmlMgrIfaceWp createKmlMgrWp;
	private QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp;
	private AdminMgrIFace adminMgr;

	@Override
	public String editStorageKmlLayer(HttpServletRequest request)//  long id, int type, DBRow data
			throws Exception {
		  try {
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			  boolean  ismanager = adminLoggerBean.isAdministrator();
			  if(!ismanager){
			  throw new Exception("autherror");
			  }
			long id=0l;
			String ip ="";
			String port="";
			String latlng="";
			int type = StringUtil.getInt(request,"location_type");
			String isChanged = StringUtil.getString(request,"isChanged");
			boolean positionChange = "true".equals(isChanged) ? true : false;
			String x = StringUtil.getString(request,"x");
			String y = StringUtil.getString(request,"y");
			String xPosition = StringUtil.getString(request,"height");
			String yPosition = StringUtil.getString(request,"width");
			String angle = StringUtil.getString(request,"angle");
			String from = StringUtil.getString(request,"from");
			if(StringUtil.isBlank(angle)){
				angle = "0";
			}
			DBRow row = new DBRow();
			row.add("x", x);
			row.add("y", y);
			row.add("angle", angle);
			row.add("height", xPosition);
			row.add("width", yPosition);
			String psId = StringUtil.getString(request,"ps_id");
			String psTitle = StringUtil.getString(request,"ps_title");
			String cds = "";
			if(positionChange 
					&& !StringUtil.isBlank(x)
					&& !StringUtil.isBlank(y)
					&& !StringUtil.isBlank(xPosition)
					&& !StringUtil.isBlank(yPosition)&&type!=6&&type!=7){
				cds = googleMapsMgrCc.convertCoordinateToLatlng(psId, x, y,	xPosition, yPosition, angle);
			}
			long position_id = 0l;
			switch (type) {
			case WebcamPositionTypeKey.LOCATION:
				String positionName ="";// 定义位置名词
				//判断方法调用来源，map或者jsp，
				if(!from.equals("map")){
					position_id = StringUtil.getLong(request,"slc_id");
				    psTitle = StringUtil.getString(request,"ps_title");
				    positionName = StringUtil.getString(request,"slc_position");
					String slcPositionAll = psTitle + StringUtil.getString(request,"area_name") + positionName;
					row.add("slc_position_all", slcPositionAll);
					row.add("is_three_dimensional", StringUtil.getLong(request,"is_three_dimensional"));
					row.add("slc_area", StringUtil.getLong(request,"slc_area"));
				}else{
					position_id = StringUtil.getLong(request,"position_id");
					positionName=StringUtil.getString(request,"positionName");
					row.add("slc_area", StringUtil.getLong(request,"area_id"));
				}
				row.add("slc_position", positionName);
			    //判断位置是否改变			
				if(positionChange){
					row.add("latlng", cds);
				}
				//判断是否为新增数据，如果是新增数据 则insert 否则 update
				if (position_id != 0l) {
					floorGoogleMapsMgrCc.updateStorageLocation(position_id, row);
				} else {
					row.add("slc_psid", psId);
					row.add("slc_ps_title", psTitle);
					row.add("latlng", cds);
					position_id = locationMgrZJ.addLocationCatalogSub(row);
				}
				floorGoogleMapsMgrCc.updateStorageAreaType(StringUtil.getLong(request,"slc_area"), 1);
				break;
			case WebcamPositionTypeKey.AREA:
				position_id = StringUtil.getLong(request,"area_id");
				String areaName = StringUtil.getString(request,"area_name");
				String titles =StringUtil.getString(request,"title_id");
				
				row.add("area_name", areaName);
				if(positionChange){
					row.add("latlng", cds);
				}
				if (position_id != 0l) {
					floorGoogleMapsMgrCc.updateStorageArea(position_id, row); 
				} else {
					row.add("area_psid", psId);
					row.add("area_type", 0);
					row.add("area_img", "");
					row.add("latlng", cds);
					position_id = locationMgrZJ.addLocationAreaSub(row);
				}
				if(!titles.equals("0")){  //0表示未修改
					floorGoogleMapsMgrCc.deleteCheckTitleByAreaId(position_id);
					if(!titles.equals("")){
						String[] title =titles.split(",");
						for(int i=0; i<title.length;i++){
							long  titleId =Long.parseLong(title[i]);
							DBRow r = new DBRow();
							r.add("area_id", position_id);
							r.add("title_id", titleId);
							floorGoogleMapsMgrCc.addZoneTitle(r);
						}
					}
				}
				String doors =StringUtil.getString(request,"sd_id");
				if(!doors.equals("0")){	//0表示未修改
					floorGoogleMapsMgrCc.deleteAreaDoorByAreaId(position_id);
					if(!doors.equals("")){
						String[] door =doors.split(",");
						for(int i=0; i<door.length;i++){
							long  sdId =Long.parseLong(door[i]);
							floorLocationAreaXmlImportMgrCc.addStorageAreaDoor(position_id ,sdId);
						}
					}
				}
				break;
			case WebcamPositionTypeKey.STAGING:
				String location_name="";
				long sdId=0l;
				if(!from.equals("map")){
					position_id = StringUtil.getLong(request,"id");
					location_name = StringUtil.getString(request,"location_name");
				}else{
					position_id = StringUtil.getLong(request,"position_id");
					location_name = StringUtil.getString(request,"positionName");
				}
				sdId = StringUtil.getLong(request,"sd_id");
				row.add("sd_id", sdId);
				row.add("location_name", location_name);
				if(positionChange){
					row.add("latlng", cds);
				}
				if (position_id != 0l) {
					floorGoogleMapsMgrCc.updateStorageStaging(position_id, row);
				} else {
					row.add("psc_id", psId);
					//row.add("latlng", cds);
					position_id = floorStorageDoorLocationZYZ.addLoadUnloadLocation(row);
				}
				break;
			case WebcamPositionTypeKey.PARKING:
				String ycNo="";
				//判断该方法调用位置map地图 或者是页面
				if(!from.equals("map")){
					position_id = StringUtil.getLong(request,"yc_id");
					ycNo = StringUtil.getString(request,"yc_no");
				}else{
					position_id = StringUtil.getLong(request,"position_id");
					ycNo = StringUtil.getString(request,"positionName");
				}
				long areaId = StringUtil.getLong(request,"area_id");
				row.add("area_id", areaId);
				row.add("yc_no", ycNo);
				//判断位置是否改变 如果改变需要重新计算
				if(positionChange){
					row.add("latlng", cds);
				}
				//判断该条记录是否为新记录，如果不是update否则insert。
				if (position_id != 0l) {
					floorGoogleMapsMgrCc.updateStorageParking(position_id, row);
				} else {
					row.add("ps_id", psId);
					row.add("yc_status", 1);
					row.add("latlng", cds);
					position_id = floorLocationAreaXmlImportMgrCc.addStorageYardControl(row);
				}
				floorGoogleMapsMgrCc.updateStorageAreaType(StringUtil.getLong(request,"area_id"), 3);
				break;
			case WebcamPositionTypeKey.DOCKS:
				String doorId="";
				if(!from.equals("map")){
					position_id = StringUtil.getLong(request,"sd_id");
					doorId = StringUtil.getString(request,"doorid");
				}else{
					position_id = StringUtil.getLong(request,"position_id");
					doorId = StringUtil.getString(request,"positionName");
					
				}
				long areaId_d = StringUtil.getLong(request,"area_id");
				row.add("area_id", areaId_d);
				row.add("doorId", doorId);
				if(positionChange){
					row.add("latlng", cds);
				}
				if (position_id != 0l) {
					floorGoogleMapsMgrCc.updateStorageDocks(position_id, row);
				} else {
					row.add("ps_id", psId);
					position_id = floorStorageDoorLocationZYZ.addStorageDoor(row);
				}
				floorGoogleMapsMgrCc.updateStorageAreaType(StringUtil.getLong(request,"area_id"), 2);
				break;
			case WebcamPositionTypeKey.WEBCAM:
				DBRow cam=new DBRow();
				 id =StringUtil.getLong(request,"cam_id");
				 ip = StringUtil.getString(request,"ip");
				 port = StringUtil.getString(request,"port");
				String username = StringUtil.getString(request,"user");
				String password = StringUtil.getString(request,"password");
				String inner_radius = StringUtil.getString(request,"inner_radius");
				String outer_radius = StringUtil.getString(request,"outer_radius");
				String s_degree = StringUtil.getString(request,"s_degree");
				String e_degree = StringUtil.getString(request,"e_degree");
				if(positionChange){
				latlng =googleMapsMgrCc.convertCoordinateToLatlng(psId, x, y);
				cam.add("latlng",latlng);
				}
				cam.add("id",id);
				cam.add("ip",ip);
				cam.add("port",port);
				cam.add("user",username);
				cam.add("password",password);
				cam.add("x",x);
				cam.add("y",y);
				cam.add("inner_radius",inner_radius);
				cam.add("outer_radius",outer_radius);
				cam.add("s_degree",s_degree);
				cam.add("e_degree",e_degree);
				// latlng需要计算的出来
				cam.add("ps_id",psId);
				if(id==0l){
					floorGoogleMapsMgrCc.addWebcam(cam);
				}else{
					floorGoogleMapsMgrCc.updateWebcamByid(cam);
				}
				break;
			case WebcamPositionTypeKey.PRINTER:
			
				DBRow printer=new DBRow();
				id =StringUtil.getLong(request,"p_id");
				ip = StringUtil.getString(request,"ip");
				port = StringUtil.getString(request,"port");
				String zone_id = StringUtil.getString(request,"area_id");
				String[] zone_ids =zone_id.split(",");
				String printer_type =StringUtil.getString(request,"type");
				String printer_name =StringUtil.getString(request,"name");
				String size =StringUtil.getString(request,"size");
				if(positionChange){
				latlng=googleMapsMgrCc.convertCoordinateToLatlng(psId, x, y);
				printer.add("latlng",latlng);
				}
				printer.add("p_id",id);
				printer.add("type",printer_type);
				printer.add("name",printer_name);
				printer.add("size",size);
				printer.add("ps_id",psId);
				printer.add("x",x);
				printer.add("y",y);
				printer.add("ip",ip);
				printer.add("port",port);
				if(id==0l){
				long p_id=floorGoogleMapsMgrCc.addPrinter(printer);
				for(int i=0;i<zone_ids.length;i++){
					DBRow zonePrinter=new DBRow();
					zonePrinter.add("area_id", zone_ids[i]);
					zonePrinter.add("p_id",p_id );
					floorGoogleMapsMgrCc.addPrinterArea(zonePrinter);
				}
				}else{
					floorGoogleMapsMgrCc.updatePrinter(printer);
					floorGoogleMapsMgrCc.deletePrinterArea(id);
					for(int i=0;i<zone_ids.length;i++){
						DBRow zonePrinter=new DBRow();
						zonePrinter.add("area_id", zone_ids[i]);
						zonePrinter.add("p_id",id );
						floorGoogleMapsMgrCc.addPrinterArea(zonePrinter);
					}
				}
				break;
			}
			return position_id+"";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public void setFloorGoogleMapsMgrCc(
			FloorGoogleMapsMgrCc floorGoogleMapsMgrCc) {
		this.floorGoogleMapsMgrCc = floorGoogleMapsMgrCc;
	}

	public void setFloorStorageDoorLocationZYZ(
			FloorStorageDoorLocationMgrZYZ floorStorageDoorLocationZYZ) {
		this.floorStorageDoorLocationZYZ = floorStorageDoorLocationZYZ;
	}

	public void setFloorLocationAreaXmlImportMgrCc(
			FloorLocationAreaXmlImportMgrCc floorLocationAreaXmlImportMgrCc) {
		this.floorLocationAreaXmlImportMgrCc = floorLocationAreaXmlImportMgrCc;
	}

	public void setLocationMgrZJ(LocationMgrIFaceZJ locationMgrZJ) {
		this.locationMgrZJ = locationMgrZJ;
	}

	public void setGoogleMapsMgrCc(GoogleMapsMgrIfaceCc googleMapsMgrCc) {
		this.googleMapsMgrCc = googleMapsMgrCc;
	}

	public void setCreateKmlMgrWp(CreateKmlMgrIfaceWp createKmlMgrWp) {
		this.createKmlMgrWp = createKmlMgrWp;
	}

	public void setQueryKmlInfoMgrWp(QueryKmlInfoMgrIfaceWp queryKmlInfoMgrWp) {
		this.queryKmlInfoMgrWp = queryKmlInfoMgrWp;
	}

	@Override
	public DBRow saveStorageLayer(HttpServletRequest request) throws Exception {
		
		DBRow result =new DBRow();
		  AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		  long adgId = adminLoggerBean.getAdgid();
		  if(adgId!=10000){
			  result.add("returnValue", -2);
			  return result;
		  }
		int type = StringUtil.getInt(request,"type");
		long psId = StringUtil.getLong(request,"ps_id");
		String x = StringUtil.getString(request,"x");
		String y = StringUtil.getString(request,"y");
		String xPosition = StringUtil.getString(request,"height");
		String yPosition = StringUtil.getString(request,"width");
		String angle = StringUtil.getString(request,"angle");
		String positionName =StringUtil.getString(request,"name");
		String storage_name = StringUtil.getString(request,"storage_name");
		long position_id=0l;
		DBRow[] dbrow =googleMapsMgrCc.getLayerInfoBypsIdAndName(psId, positionName, type);
		if(dbrow.length>0){
			result.add("returnValue", 0);
			return result;
		}
		if(StringUtil.isBlank(angle)){
			angle = "0";
		}
		DBRow row = new DBRow();
		row.add("x", x);
		row.add("y", y);
		row.add("angle", angle);
		row.add("height", xPosition);
		row.add("width", yPosition);
		long area_id=0l;
		String 	cds = googleMapsMgrCc.convertCoordinateToLatlng(Long.toString(psId), x, y,	xPosition, yPosition, angle);
		switch (type) {
		case WebcamPositionTypeKey.LOCATION:
				area_id =StringUtil.getLong(request, "area_id");
				String slcPositionAll = storage_name +StringUtil.getString(request,"area_name")+ positionName;
				row.add("slc_position_all", slcPositionAll);
				row.add("is_three_dimensional", StringUtil.getLong(request,"is_three_dimensional"));
				row.add("slc_area", area_id);
				row.add("slc_position", positionName);
				row.add("latlng", cds);
				row.add("slc_psid", psId);
				row.add("slc_ps_title", storage_name);
				position_id = locationMgrZJ.addLocationCatalogSub(row);
				floorGoogleMapsMgrCc.updateStorageAreaType(area_id, 1);
				//重新生成kml
				createKmlMgrWp.createKmlNew(Long.toString(psId));
				//删除无效kml
				queryKmlInfoMgrWp.deleteInvalidKml(Environment.getHome() + "upload/kml");
			break;
		case WebcamPositionTypeKey.AREA:
			String titles =StringUtil.getString(request,"title_id");
			String doors =StringUtil.getString(request,"sd_id");
			row.add("area_name", positionName);
			row.add("latlng", cds);
			row.add("area_psid", psId);
			row.add("area_type", 0);
			row.add("area_img", "");
			position_id = locationMgrZJ.addLocationAreaSub(row);
			String aotulocation =StringUtil.getString(request,"aotulocations");
			int child_type =StringUtil.getInt(request,"child_type");
			String[] aotulocations = null ;
			if(aotulocation!=null&&!aotulocation.equals("")){
				aotulocations=aotulocation.split(";");
			}
			if(aotulocation!=null&&!aotulocation.equals("")&&aotulocations.length>0){
				if(child_type==1){
				for(int i=0;i<aotulocations.length;i++){
					String[] aotuloca=aotulocations[i].split(",");
					if(aotuloca.length>0){
						DBRow aoturow=new DBRow();
						String 	auto_cds = googleMapsMgrCc.convertCoordinateToLatlng(aotuloca[0], aotuloca[4], aotuloca[5],	aotuloca[2], aotuloca[3], angle);
								String aotuname_all = storage_name +positionName+ aotuloca[1];
								DBRow[] autoLocal =googleMapsMgrCc.getLayerInfoBypsIdAndName(psId, aotuloca[1], 1);
								if(autoLocal.length>0){
									result.add("returnValue",-1);
									throw new Exception("-1");
								}
								aoturow.add("x", aotuloca[4]);
								aoturow.add("y", aotuloca[5]);
								aoturow.add("angle", aotuloca[6]);
								aoturow.add("height", roundOff(aotuloca[2],2));
								aoturow.add("width", roundOff(aotuloca[3],2));
								aoturow.add("slc_position_all", aotuname_all.toUpperCase());
								aoturow.add("is_three_dimensional",0);
								aoturow.add("slc_area", position_id);
								aoturow.add("slc_position", aotuloca[1].toUpperCase());
								aoturow.add("latlng", auto_cds);
								aoturow.add("slc_psid", aotuloca[0]);
								aoturow.add("slc_ps_title", storage_name);
								locationMgrZJ.addLocationCatalogSub(aoturow);
								//重新生成kml
						}
					}
							floorGoogleMapsMgrCc.updateStorageAreaType(position_id, 1);
							createKmlMgrWp.createKmlNew(Long.toString(psId));
							//删除无效kml
							queryKmlInfoMgrWp.deleteInvalidKml(Environment.getHome() + "upload/kml");
				}
						if(child_type==2){
							for(int i=0;i<aotulocations.length;i++){
								String[] aotuloca=aotulocations[i].split(",");
									if(aotuloca.length>0){
									DBRow aoturow=new DBRow();
									String 	auto_cds = googleMapsMgrCc.convertCoordinateToLatlng(aotuloca[0], aotuloca[4], aotuloca[5],	aotuloca[2], aotuloca[3], angle);
									DBRow[] autoLocal =googleMapsMgrCc.getLayerInfoBypsIdAndName(psId, aotuloca[1], 2);
									if(autoLocal.length>0){
										result.add("returnValue",-1);
										throw new Exception("-1");
									}
									aoturow.add("x", aotuloca[4]);
									aoturow.add("y", aotuloca[5]);
									aoturow.add("angle", aotuloca[6]);
									aoturow.add("height", roundOff(aotuloca[2],2));
									aoturow.add("width", roundOff(aotuloca[3],2));
									aoturow.add("location_name", aotuloca[1].toUpperCase());
									aoturow.add("latlng", auto_cds);
									aoturow.add("psc_id", aotuloca[0]);
									floorStorageDoorLocationZYZ.addLoadUnloadLocation(aoturow);
								}
							}
							
						}
						if(child_type==3){
							for(int i=0;i<aotulocations.length;i++){
								String[] aotuloca=aotulocations[i].split(",");
								if(aotuloca.length>0){
								DBRow aoturow=new DBRow();
								String 	auto_cds = googleMapsMgrCc.convertCoordinateToLatlng(aotuloca[0], aotuloca[4], aotuloca[5],	aotuloca[2], aotuloca[3], angle);
								DBRow[] autoLocal =googleMapsMgrCc.getLayerInfoBypsIdAndName(psId, aotuloca[1], 3);
								if(autoLocal.length>0){
									result.add("returnValue",-1);
									throw new Exception("-1");
								}
								aoturow.add("x", aotuloca[4]);
								aoturow.add("y", aotuloca[5]);
								aoturow.add("angle", aotuloca[6]);
								aoturow.add("height", roundOff(aotuloca[2],2));
								aoturow.add("width", roundOff(aotuloca[3],2));
								aoturow.add("area_id", position_id);
								aoturow.add("doorId", aotuloca[1].toUpperCase());
								aoturow.add("latlng", auto_cds);
								aoturow.add("ps_id", aotuloca[0]);
								floorStorageDoorLocationZYZ.addStorageDoor(aoturow);
								}
							}
							floorGoogleMapsMgrCc.updateStorageAreaType(position_id, 2);
						}
						if(child_type==4){
							for(int i=0;i<aotulocations.length;i++){
								String[] aotuloca=aotulocations[i].split(",");
									if(aotuloca.length>0){
									DBRow aoturow=new DBRow();
									String 	auto_cds = googleMapsMgrCc.convertCoordinateToLatlng(aotuloca[0], aotuloca[4], aotuloca[5],	aotuloca[2], aotuloca[3], angle);
									DBRow[] autoLocal =googleMapsMgrCc.getLayerInfoBypsIdAndName(psId, aotuloca[1], 4);
									if(autoLocal.length>0){
										result.add("returnValue",-1);
										throw new Exception("-1");
									}
									aoturow.add("x", aotuloca[4]);
									aoturow.add("y", aotuloca[5]);
									aoturow.add("angle", aotuloca[6]);
									aoturow.add("height", roundOff(aotuloca[2],2));
									aoturow.add("width", roundOff(aotuloca[3],2));
									aoturow.add("area_id", position_id);
									aoturow.add("yc_no", aotuloca[1].toUpperCase());
									aoturow.add("latlng", auto_cds);
									aoturow.add("ps_id", aotuloca[0]);
									aoturow.add("yc_status", 1);
									floorLocationAreaXmlImportMgrCc.addStorageYardControl(aoturow);
								
								}
							}
							floorGoogleMapsMgrCc.updateStorageAreaType(position_id, 3);
						}
				
			}
			if(!titles.equals("")){
					floorGoogleMapsMgrCc.deleteCheckTitleByAreaId(position_id);
					String[] title =titles.split(",");
					for(int i=0; i<title.length;i++){
						long  titleId =Long.parseLong(title[i]);
						DBRow r = new DBRow();
						r.add("area_id", position_id);
						r.add("title_id", titleId);
						floorGoogleMapsMgrCc.addZoneTitle(r);
					}
			}
				if(!doors.equals("")){
					floorGoogleMapsMgrCc.deleteAreaDoorByAreaId(position_id);
					String[] door =doors.split(",");
					for(int i=0; i<door.length;i++){
						long  sdId =Long.parseLong(door[i]);
						floorLocationAreaXmlImportMgrCc.addStorageAreaDoor(position_id ,sdId);
					}
			}
			break;
		case WebcamPositionTypeKey.STAGING:
				long sdId = StringUtil.getLong(request,"sd_id");
				row.add("sd_id", sdId);
				row.add("location_name", positionName);
				row.add("latlng", cds);
				row.add("psc_id", psId);
				position_id = floorStorageDoorLocationZYZ.addLoadUnloadLocation(row);
			break;
		case WebcamPositionTypeKey.PARKING:
				long areaId = StringUtil.getLong(request,"area_id");
				row.add("area_id", areaId);
				row.add("yc_no", positionName);
				row.add("latlng", cds);
				row.add("ps_id", psId);
				row.add("yc_status", 1);
				position_id = floorLocationAreaXmlImportMgrCc.addStorageYardControl(row);
				floorGoogleMapsMgrCc.updateStorageAreaType(areaId, 3);
				break;
		case WebcamPositionTypeKey.DOCKS:
				area_id = StringUtil.getLong(request,"area_id");
				row.add("area_id", area_id);
				row.add("doorId", positionName);
				row.add("latlng", cds);
				row.add("ps_id", psId);
				position_id = floorStorageDoorLocationZYZ.addStorageDoor(row);
				floorGoogleMapsMgrCc.updateStorageAreaType(area_id, 2);
				break;
		}
		

		result.add("returnValue", position_id);
		return result;
	}
	public  double roundOff(String num,int len ){
		double x =Math.pow(10, len);
		double d = Double.parseDouble(num);
		return ((int)(d*x+0.5))/x;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}
	
	
}
