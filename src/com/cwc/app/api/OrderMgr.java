package com.cwc.app.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.qll.OrdersMgrQLL;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuOrderBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.emailtemplate.velocity.returnproduct.ReturnReceiveEmailPageString;
import com.cwc.app.emailtemplate.velocity.returnproduct.ReturnVoiceEmailPageString;
import com.cwc.app.exception.client.OrderNotExistException;
import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.exception.order.CurrencyNotExistException;
import com.cwc.app.exception.order.DeliveryInfoBeUsedException;
import com.cwc.app.exception.order.DeliveryStorageNotSameException;
import com.cwc.app.exception.order.InvoiceTemplateBeUsedException;
import com.cwc.app.exception.order.LackingOrderCanBeMergePrintedOrderException;
import com.cwc.app.exception.order.LostMoneyException;
import com.cwc.app.exception.order.MergeOrderNotCancelException;
import com.cwc.app.exception.order.MergeOrderNotExistException;
import com.cwc.app.exception.order.MergeOrderNotSelfException;
import com.cwc.app.exception.order.MergeOrderStatusConflictException;
import com.cwc.app.exception.order.MergeOrderStatusIncorrectException;
import com.cwc.app.exception.order.MergeOrderStorageNotMatchException;
import com.cwc.app.exception.order.NotNormalOrderException;
import com.cwc.app.exception.order.OrderHasWayBillNotCancelException;
import com.cwc.app.exception.order.TxnIdExistException;
import com.cwc.app.exception.product.ProductDataErrorException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorWayBillOrderMgrZJ;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.zj.TradeMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.key.AfterServiceKey;
import com.cwc.app.key.HandStatusleKey;
import com.cwc.app.key.HandleKey;
import com.cwc.app.key.InOutStoreKey;
import com.cwc.app.key.OrderTaskKey;
import com.cwc.app.key.PayTypeKey;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.key.ReturnProductStatusKey;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.lucene.OrderIndexMgr;
import com.cwc.app.ordertrace.OrderTraceFather;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.SystemException;
import com.cwc.service.order.action.OrderCostBean;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;
import com.cwc.util.Tree;

public class OrderMgr implements OrderMgrIFace
{
	static Logger log = Logger.getLogger("ACTION");

	private FloorOrderMgr fom ;
	private FloorProductMgr fpm;
	private SystemConfig systemConfig;
	private Cart cart;
	private TaskMgr taskMgr;
	private CustomCart customCart;
	private ExpressMgr expressMgr;
	private CartReturn cartReturn;
	private CartWarranty cartWarranty;
	private CustomCartWarranty customCartWarranty;
	private OrdersMgrQLL ordersMgrQLL;
	private ProductMgr productMgr;
	private FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ;
	private TradeMgrIFaceZJ tradeMgrZJ;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	
	public static String ORDER_SOURCE_EBAY = "EBAY";
	public static String ORDER_SOURCE_WEBSITE = "WEBSITE";
	public static String ORDER_SOURCE_DIRECTPAY = "DIRECTPAY";
	public static String ORDER_SOURCE_MANUAL = "MANUAL";
	public static String ORDER_SOURCE_QUOTE = "报价单";
	public static String ORDER_SOURCE_WANRRANTY = "质保单";
	public static String ORDER_SOURCE_PAY = "付款单";//付款单
	public static String ORDER_SPLIT = "拆单";
	public static String ORDER_LOCAL_BUY = "上门购买";
	public static String ORDER_DIRECT_BUY = "特殊销售"; 
	public static String ORDER_AMAZON = "AMAZON"; 
	public static String ORDER_ECRATER = "ECRATER"; 
	public static String ORDER_BONANZA = "BONANZA";
	

	public void setOrdersMgrQLL(OrdersMgrQLL ordersMgrQLL) {
		this.ordersMgrQLL = ordersMgrQLL;
	}
	
	//根据订单ID重建订单索引
	public void newIndexByOid(long oid,String ems_id)
		throws Exception
	{
				String trackingNumber = ems_id;
				trackingNumber = trackingNumber.replaceAll(":", " ");
				DBRow para = new DBRow();
				para.add("ems_id", trackingNumber);
				fom.modPOrder(oid, para);
	}
	
	public synchronized long addPOrder(HttpServletRequest request)
		throws Exception
	{
		long oid = 0;
		
		try
		{	
			String txn_id = StringUtil.getString(request, "txn_id");
			String parent_txn_id = StringUtil.getString(request, "parent_txn_id");
			String payment_status = StringUtil.getString(request, "payment_status");
			
			String custom = StringUtil.getString(request, "custom");
			
			DBRow orgOrder = fom.getDetailPOrderByTxnid( txn_id );
			String memo = StringUtil.getString(request, "memo");
			
			String order_source;
			
			
				//先判断是否有parent_txn_id或者是投诉订单
				if ( this.haveParentOrder(request)||this.isDisputeOrder(request) )
				{
					////system.out.println("1111");
					fom.addRelationOrder( this.getSpecialOrder(request) );

					if ( this.isDisputeOrder(request) )
					{
						////system.out.println("4444");
						markDisputeOrder(request);
					}
					else
					{
						////system.out.println("5555");
						updateOrderPaymentStatus(parent_txn_id,payment_status);
					}
				}
				else if ( orgOrder!=null )   //对已经存在的txnid的订单同样插入到关联订单
				{
					////system.out.println("2222");
					
					fom.addRelationOrder( this.getSpecialOrder(request) );
					updateOrderPaymentStatus(txn_id,payment_status);
				}
				else
				{
//					String txn_id = StrUtil.getString(request,"");
					String item_name = StringUtil.getString(request,"");
					String item_number = StringUtil.getString(request,"");
					String mc_currency = StringUtil.getString(request,"");
					String first_name = StringUtil.getString(request,"");
					String last_name = StringUtil.getString(request,"");
					String auction_buyer_id = StringUtil.getString(request,"");
					String business = StringUtil.getString(request,"");
					String payer_status = StringUtil.getString(request,"");
					String address_country = StringUtil.getString(request,"");
					String address_city = StringUtil.getString(request,"");
					String address_zip = StringUtil.getString(request,"");
					String address_street = StringUtil.getString(request,"");
					String address_country_code = StringUtil.getString(request,"");
					String address_state = StringUtil.getString(request,"");
					String address_status = StringUtil.getString(request,"");
//					String payment_status = StrUtil.getString(request,"");
					String client_id = StringUtil.getString(request,"");
					String payment_date = StringUtil.getString(request,"");
					String post_date = StringUtil.getString(request,"");
					
					DBRow porder = new DBRow();
					
					porder.add("txn_id",txn_id);
					porder.add("item_name",item_name);
					porder.add("item_number",item_number);
					porder.add("mc_currency",mc_currency);
					porder.add("first_name",first_name);
					porder.add("last_name",last_name);
					porder.add("auction_buyer_id",auction_buyer_id);
					porder.add("business",business);
					porder.add("payer_status",payer_status);
					porder.add("address_country",address_country);
					porder.add("address_city",address_city);
					porder.add("address_zip",address_zip);
					porder.add("address_street",address_street);
					porder.add("address_country_code",address_country_code);
					porder.add("address_state",address_state);
					porder.add("address_status",address_status);
					porder.add("payment_status",payment_status);
					porder.add("client_id",client_id);
					porder.add("payment_date",payment_date);
					porder.add("post_date",post_date);
					
					
					String address_name = StringUtil.getString(request, "address_name");
					address_name = StringUtil.replaceString(address_name,"\"", ".");
					address_name = StringUtil.replaceString(address_name,"  ", " ");
					porder.add("address_name",address_name);
					
					if (custom.indexOf("@")>=0)
					{
						String tmpA[] =  custom.split(",");
						
						porder.add("business", tmpA[0]);

						if (tmpA.length>1)
						{
							porder.add("vvme_webservice_url", tmpA[1].substring(0,tmpA[1].lastIndexOf(":")));
						}
						
					}

					//判断订单来源
//					String auction_buyer_id = StrUtil.getString(request, "auction_buyer_id");
					String txn_type = StringUtil.getString(request, "txn_type");

					if (!auction_buyer_id.equals(""))
					{
						order_source = OrderMgr.ORDER_SOURCE_EBAY;
					}
					else
					{
						if (custom.indexOf(":WEBSITE")>=0)
						{
							order_source = OrderMgr.ORDER_SOURCE_WEBSITE;
						}
						else
						{
							order_source = OrderMgr.ORDER_SOURCE_DIRECTPAY;
							//获得电话号码
							if (custom.indexOf("payer_phone=")>=0)
							{
								porder.add("tel",custom.split("payer_phone=")[1]);
							}
							
						}
					}

					//获得国家代码
					DBRow countryCode = fom.getDetailCountryCodeByCountry(porder.getString("address_country").trim());
					
					if (countryCode==null)
					{
						porder.add("ccid",0);
						porder.add("pro_id",-1);
					}
					else
					{
						if(countryCode.get("ccid", 0l)==11036)//美国订单将邮编截取前5位
						{
							String zip = porder.getString("address_zip");
							if(zip.length()>5)
							{
								zip = zip.substring(0,5);
							}
							porder.add("address_zip",zip);
						}
						
						
						porder.add("ccid",countryCode.get("ccid", 0l));
						if (porder.getString("address_state").trim().equals(""))
						{
							porder.add("pro_id",-1);
						}
						else
						{
								//获得省份ID
								DBRow detailProvince = fpm.getDetailProvinceByNationIDPCode(countryCode.get("ccid", 0l),porder.getString("address_state").trim());//通过国家ID，省份/州缩写查询
								if(detailProvince == null)//判断是否可以通过缩写查询到对应State的ID
								{
									detailProvince = fpm.getDetailProvinceByNationIDPName(countryCode.get("ccid",0l),porder.getString("address_state").trim());//通过国家ID，省份/州缩写查询
								}
								
								if (detailProvince==null)
								{
									porder.add("pro_id",-1);
								}
								else
								{
									porder.add("pro_id",detailProvince.get("pro_id", 0l));
									porder.add("address_state",detailProvince.getString("pro_name"));
								}
						}
					}
					/**
					 * 对ebay_txn_id处理下
					 * 在多item情况下，ebay_txn_id也有多个
					 */
					String ebay_txn_id="";
					if ( StringUtil.getString(request, "item_number").indexOf(",")>0 )
					{
						int inI = 1;
						for (;inI<StringUtil.getString(request, "item_number").split(",").length;inI++)
						{
							ebay_txn_id += StringUtil.getString(request, "ebay_txn_id"+inI) + ",";
						}
						ebay_txn_id += StringUtil.getString(request, "ebay_txn_id"+inI);
					}
					else
					{
						ebay_txn_id = StringUtil.getString(request, "ebay_txn_id1");
					}
					
					porder.add("order_source",order_source);
					porder.add("append_name","");
					porder.add("note",memo);
					porder.add("dispute_flag",0);
					porder.add("mc_gross",StringUtil.getFloat(request, "mc_gross",0));
					porder.add("mc_fee",StringUtil.getFloat(request,"mc_fee",0));
					porder.add("tax",StringUtil.getFloat(request, "tax",0));
					porder.add("shipping",StringUtil.getFloat(request, "shipping",0));
					porder.add("quantity",StringUtil.getFloat(request, "num_cart_items",0));
					porder.add("sender_status",request.getAttribute("sender_status").toString());
					porder.add("handle",HandleKey.WAIT4_RECORD);
					porder.add("handle_status",0);
					porder.add("product_status",0);
					porder.add("ip",request.getRemoteAddr());
					porder.add("post_date",DateUtil.NowStr());
					porder.add("verify_cost",0);
					porder.add("trace_flag",1);
					porder.add("ebay_txn_id",ebay_txn_id);
					porder.add("mc_feee",StringUtil.getFloat(request, "mc_fee",0));
					porder.add("pending_reason",StringUtil.getString(request, "pending_reason"));
					porder.add("custom",custom);
					
					//处理信用卡Filter Pending信息
					StringBuffer fmpfSB = new StringBuffer("");
					for (int zz=1; zz<=17; zz++)
					{
						String fraud_management_pending_filters = StringUtil.getString(request, "fraud_management_pending_filters_"+zz);
						if (fraud_management_pending_filters.trim().equals("")==false)
						{
							fmpfSB.append(fraud_management_pending_filters);
							fmpfSB.append("\n");
						}
						else
						{
							break;
						}
					}
					porder.add("fraud_management_pending_filters",fmpfSB.toString());
					oid = fom.addPOrder(porder);

					//地址验证
//					ordersMgrQLL.addressValidateFedexJMS(oid,porder.getString("address_street"),porder.getString("address_zip"));
					OrderIndexMgr.getInstance().addIndex(porder.getString("address_name"),porder.getString("client_id"),oid,porder.getString("txn_id"),porder.getString("auction_buyer_id"),porder.getString("item_number"),"",porder.getString("address_country"));

					fom.addRelationOrder( this.getSpecialOrder(request) );
				}	
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addPOrder",log);
		}
		
		return(oid);
	}

	public long addPOrderByHandle(HttpServletRequest request)
		throws TxnIdExistException,Exception
	{
		try
		{
			int manual_order_type = StringUtil.getInt(request, "manual_order_type");//1 - 质保订单
			long manual_order_rel_oid = StringUtil.getLong(request, "manual_order_rel_oid");
			String pay_info="";
			int pay_type = StringUtil.getInt(request, "pay_type");
			
			String business = StringUtil.getString(request,"business");
			String quantity = StringUtil.getString(request,"quantity");
			String client_id = StringUtil.getString(request,"client_id");
			String mc_gross = StringUtil.getString(request,"mc_gross");
			String type = StringUtil.getString(request,"type");
			String address_name = StringUtil.getString(request,"address_name");
			String address_street = StringUtil.getString(request,"address_street");
			String address_city = StringUtil.getString(request,"address_city");
			String address_state = StringUtil.getString(request,"address_state");
			String address_zip = StringUtil.getString(request,"address_zip");
			String address_country = StringUtil.getString(request,"address_country");
			String tel = StringUtil.getString(request,"tel");
//			String address_country = StrUtil.getString(request,"address_country");
			String ccid = StringUtil.getString(request,"ccid");
			String mc_currency = StringUtil.getString(request,"mc_currency");
			String txn_id = StringUtil.getString(request,"txn_id");
			String order_source = StringUtil.getString(request,"order_source");
			
			DBRow porder = new DBRow();
			
			if ( porder.getString("txn_id").trim().equals("")==false&&fom.getDetailPOrderByTxnid(porder.getString("txn_id").trim())!=null )
			{
				throw new TxnIdExistException(porder.getString("txn_id"));
			}
			
			DBRow detailCountry = fom.getDetailCountryCodeByCcid(StringUtil.getLong(porder.getString("ccid")));
			
			long pro_id = StringUtil.getLong(request, "pro_id");
			if(pro_id!=-1)//省份并非other
			{
				DBRow detailProvince = fpm.getDetailProvinceByProId(pro_id);
				
				porder.add("address_state",detailProvince.getString("pro_name"));
			}
			
			porder.add("address_country",detailCountry.getString("c_country"));
			porder.add("handle",1);
			porder.add("handle_status",0);
			porder.add("ip",request.getRemoteAddr());
			porder.add("post_date",DateUtil.NowStr());
			porder.add("manual_order_type",manual_order_type);
			porder.add("manual_order_rel_oid",manual_order_rel_oid);
			porder.add("pay_type",pay_type);
			porder.add("pro_id",pro_id);
			long oid = fom.addPOrder(porder);
			
			//地址矫正
			ordersMgrQLL.addressValidateFedexJMS(oid,porder.getString("address_street"),porder.getString("address_zip"));
			
			if(pay_type==PayTypeKey.Wire) 
			{
				pay_info = "汇款帐号："+StringUtil.getString(request, "pay_info_sendmoney_account");
			}
			else if(pay_type==PayTypeKey.Check) 
			{
				pay_info = "支票号码："+StringUtil.getString(request, "pay_info_check_account");
			}
			else if(pay_type==PayTypeKey.ParentOrder)
			{
				String parentoid = StringUtil.getString(request, "parentoid");
				
				pay_info = "父订单："+parentoid;
				
				if (parentoid.trim().equals("")==false)
				{
					this.addPOrderNotePrivate(StringUtil.getLong(parentoid),"拆单："+oid,TracingOrderKey.OTHERS, StringUtil.getSession(request),0);
				}
			}
			
			this.addPOrderNotePrivate(oid,pay_info,TracingOrderKey.OTHERS, StringUtil.getSession(request),0);

			OrderIndexMgr.getInstance().addIndex(porder.getString("address_name"),porder.getString("client_id"),oid,porder.getString("txn_id"),porder.getString("auction_buyer_id"),porder.getString("item_number"),"",porder.getString("address_country"));
			
			return(oid);
		} 
		catch (TxnIdExistException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addPOrderByHandle",log);
		}
	}

	/**
	 * 用来性能测试增加订单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addPOrderByHandle4LoadTest(HttpServletRequest request)
		throws Exception
	{
			return(this.addPOrderByHandle(request));
	}

	public DBRow getDetailPOrderByTxnid(String id)
		throws Exception
	{
		try
		{
			return(fom.getDetailPOrderByTxnid(id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailPOrderByTxnid",log);
		}
	}
	
	public DBRow[] getAllOrders(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getAllOrders(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllOrders",log);
		}
	}
	
//	public DBRow[] getSearchOrdersByField(String field,String value,PageCtrl pc)
//		throws Exception
//	{
//		try
//		{
//			value = StrUtil.replaceString(value,"|","+");
//			return(fom.getSearchOrdersByField(field.trim(),value.trim(),pc));
//		} 
//		catch (Exception e) 
//		{
//			throw new SystemException(e,"getSearchOrdersByField",log);
//		}
//	}
	
	public DBRow[] colorFilter(DBRow data[])
		throws Exception
	{
		for (int i=0; i<data.length; i++)
		{
			//////system.out.println("==>"+data[i].get("handle_statust",0));
			
			if ( data[i].getString("sender_status").equals("invalid") )
			{
				data[i].add("invalid_color",systemConfig.getStringConfigValue("invalid_color"));
			}
			else
			{
				data[i].add("invalid_color","");
			}
						
			if ( data[i].get("handle_print",0)==1&&data[i].get("handle_delivery",0)==0 )
			{
				data[i].add("handle_order_color",systemConfig.getStringConfigValue("only_print"));
			}
			else if ( data[i].get("handle_statust",0)==1 )//疑问订单
			{
				data[i].add("handle_order_color",systemConfig.getStringConfigValue("only_deliver"));
			}
			else if ( data[i].get("handle_print",0)==1&&data[i].get("handle_delivery",0)==1 )
			{
				data[i].add("handle_order_color",systemConfig.getStringConfigValue("print_deliver"));
			}
			
			if ( data[i].getString("payment_status").toLowerCase().equals("completed") )
			{
				data[i].add("payment_status_color",systemConfig.getStringConfigValue("payment_status_color"));
			}
			else
			{
				data[i].add("payment_status_color",systemConfig.getStringConfigValue("payment_status_false_color"));
			}
			
			if ( data[i].getString("address_status").toLowerCase().equals("confirmed") )
			{
				data[i].add("address_status_color",systemConfig.getStringConfigValue("payment_status_color"));
			}
			else
			{
				data[i].add("address_status_color",systemConfig.getStringConfigValue("payment_status_false_color"));
			}
		}

		return(data);
	}
	
	public void markDeliver(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request,"oid");
			
			DBRow row = new DBRow();
			row.add("handle","4");
			fom.modPOrder(oid,row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"markDeliver",log);
		}
	}

	public DBRow[] getOrdersByFilter(HttpServletRequest request,PageCtrl pc)
		throws Exception
	{
		try
		{
			String st = StringUtil.getString(request,"st");
			String en = StringUtil.getString(request,"en");
			String business = StringUtil.getString(request,"business");
			String p_name = StringUtil.getString(request,"p_name");
			int handle = StringUtil.getInt(request,"handle",0);
			int after_service_status = StringUtil.getInt(request,"after_service_status",0);
			int handle_status = StringUtil.getInt(request,"handle_status",0);
			int product_status = StringUtil.getInt(request,"product_status");
			int product_type_int = StringUtil.getInt(request,"product_type_int",0);
			long filter_ps_id = StringUtil.getLong(request,"filter_ps_id");
			long order_note_adid = StringUtil.getLong(request,"order_note_adid");
			int order_note_trace_type = StringUtil.getInt(request,"order_note_trace_type",-1);
			String order_source = StringUtil.getString(request,"order_source");
			int bad_feedback_flag = StringUtil.getInt(request,"bad_feedback_flag");
			
			DBRow results[] = fom.getOrdersByFilter(st,en,business,handle,after_service_status,handle_status,product_status,product_type_int,filter_ps_id,p_name,order_note_adid,order_note_trace_type,order_source,bad_feedback_flag,pc);
			
			return(results);
		} 
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrdersByFilter",log);
		}
	}

	public void modPOrderByChangePaymentStatus(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String txn_id = StringUtil.getString(request,"txn_id");
			String payment_status = StringUtil.getString(request,"payment_status");
			
			DBRow porder = new DBRow();
			porder.add("txn_id",txn_id);
			porder.add("payment_status",payment_status);
			
			
			DBRow oldOrder = fom.getDetailPOrderByTxnid( porder.getString("txn_id") );
			String psc = oldOrder.getString("payment_status_change");
			if ( psc.equals("") )
			{
				psc = oldOrder.getString("payment_status")+"-"+porder.getString("payment_status");				
			}
			else
			{
				psc = psc+"-"+porder.getString("payment_status");
			}
			
			porder.add("sender_status",request.getAttribute("sender_status").toString());
			porder.add("ip",request.getRemoteAddr());
			porder.add("post_date",DateUtil.NowStr());
			porder.add("payment_status_change",psc);
			fom.modPOrder(porder.getString("txn_id"),porder);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modPOrderByChangePaymentStatus",log);
		}
	}

	public void modPOrderNote(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String note = StringUtil.getString(request, "note");
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminInfo = am.getAdminLoginBean( request.getSession(true) );
			
			long on_id = StringUtil.getLong(request,"on_id");
			DBRow detailNote = fom.getDetailPOrderNoteByOnid(on_id);

			if (detailNote.get("adid", 0l)==adminInfo.getAdid())
			{
				DBRow orderNote = new DBRow();
				orderNote.add("note", note);
				fom.modPOrderNote(on_id,orderNote);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modPOrderNote",log);
		}
	}

	public void modPOrderHandleStatus(HttpServletRequest request)
		throws Exception
	{
		try
		{
			DBRow porder = new DBRow();
			long oid = StringUtil.getLong(request,"oid");
			String oid_sl[] = request.getParameterValues("oid_sl");
			String handle_type = StringUtil.getString(request, "batch_modify_handle_type");
			int handle_status = StringUtil.getInt(request, "handle_status");
			
			if (handle_type.equals("handlestatus"))
			{
				porder.add("handle_status",handle_status);
			}
			else
			{
				porder.add("handle",handle_status);
			}
			
			if (oid!=0)
			{
				fom.modPOrder(oid,porder);
			}
			else
			{
				for (int i=0; i<oid_sl.length; i++)
				{
					fom.modPOrder(StringUtil.getLong(oid_sl[i]),porder);
				}
			}
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminInfo = am.getAdminLoginBean( request.getSession(true) );
			
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modPOrderHandleStatus",log);
		}
	}
	
//	public DBRow[] getSearchOrdersByKeyLike(String field,String value,PageCtrl pc)
//		throws Exception
//	{
//		try
//		{
//			return(fom.getSearchOrdersByKeyLike(field.trim(),value.trim(),pc));
//		} 
//		catch (Exception e) 
//		{
//			throw new SystemException(e,"getSearchOrdersByKeyLike",log);
//		}
//	}

	/**
	 * 检测定制套装内的商品是否被改变
	 * @return
	 * @throws Exception
	 */
	public boolean customProductNotChanged(DBRow oldProducts[],DBRow newProducts[])
		throws Exception
	{
		boolean noChangeFlag = true;

		if (oldProducts.length==newProducts.length)
		{
			ArrayList oldProductsInSetAl = new ArrayList();
			for (int i=0; i<oldProducts.length; i++)
			{
				oldProductsInSetAl.add(oldProducts[i].getString("pc_id")+"_"+oldProducts[i].getString("quantity"));
			}
			
			for (int i=0; i<newProducts.length; i++)
			{
				oldProductsInSetAl.remove(newProducts[i].getString("pc_id")+"_"+newProducts[i].getString("cart_quantity"));
			}
			
			if (oldProductsInSetAl.size()>0)
			{
				noChangeFlag = false;
			}
		}
		else
		{
			noChangeFlag = false;
		}
		
		return(noChangeFlag);
	}

	/**
	 * 回退库存
	 * @param cartProducts  购物车
	 * @param handle_status	订单状态
	 * @param ps_id			仓库
	 * @param order			订单内容
	 * @param orderItem		订单商品
	 * @param ses		
	 * @throws Exception
	 */
	public void stockback(DBRow cartProducts[],int handle_status,long ps_id,DBRow order,DBRow orderItem[],HttpSession ses)
		throws Exception
	{
		try
		{
			/**
			 * 原来是 正常已抄单 的订单可以退库存
			 * 因为疑问成本订单算是 特殊正常订单，因为他变成疑问成本，不走抄单流程，也就是没有退库存，假如这是执行删除或取消操作，则会导致库存没有正常回退
			 */
//			if ( order.get("handle_status", 0)==HandStatusleKey.NORMAL )
//			{
			if ( order.get("product_status", 0)!=ProductStatusKey.UNKNOWN)
			{	
				
				/**
				 * 退库存区分普通商品、标准套装、标准套装拼装、定制套装
				 * 普通商品：直接退库存
				 * 标准套装：直接退库存
				 * 标准套装拼装：套装中的散件退库存
				 * 定制套装：套装中的散件退库存
				 */
				for (int i=0; i<orderItem.length; i++)
				{
					//原来有货商品，并且存在于购物车中，并且没有修改数量，不需要回退库存
					//只有正常抄单才区别处理库存，疑问抄单和切换仓库都需要把所有库存退掉
					if ( orderItem[i].get("lacking",0)==ProductStatusKey.IN_STORE&&cartProducts!=null&&handle_status==HandStatusleKey.NORMAL&&order.get("ps_id", 0l)==ps_id )
					{
						boolean notStockBack = false;
						
						for (int j=0; j<cartProducts.length; j++)
						{
							if (cartProducts[j].getString("cart_pid").equals(orderItem[i].getString("pid"))&&cartProducts[j].getString("cart_product_type").equals(orderItem[i].getString("product_type"))&&StringUtil.getFloat(cartProducts[j].getString("cart_quantity"))==orderItem[i].get("quantity",0f))
							{
								//如果是定制商品，需要做进一步判断
								if ( orderItem[i].get("product_type",0)==ProductTypeKey.UNION_CUSTOM )
								{
									DBRow oldCustomProducts[] = fpm.getProductsCustomInSetBySetPid(orderItem[i].get("pid",0l));
									DBRow newCustomProducts[] = customCart.getFinalDetailProducts(ses, orderItem[i].get("pid",0l));
									
									if (this.customProductNotChanged(oldCustomProducts, newCustomProducts))
									{
										notStockBack = true;
									}
								}
								else
								{
									notStockBack = true;
								}
							}
						}

						if (notStockBack)
						{
							continue;
						}
					}

					////system.out.println("["+DateUtil.NowStr()+"] [退库存] "+orderItem[i].getString("name"));
					ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
					//ses是否为正常Session有待检查
					inProductStoreLogBean.setAccount( (new AdminMgr()).getAdminLoginBean(ses).getAccount() );
					inProductStoreLogBean.setOid(order.get("oid", 0l));
					
					if (cartProducts==null)
					{
//						inProductStoreLogBean.setOperation("删除/取消订单 回退 库存");
					}
					else
					{
//						inProductStoreLogBean.setOperation("抄单 回退 库存");						
					}
					
					if (orderItem[i].get("product_type", 0)==ProductTypeKey.NORMAL||orderItem[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD)
					{
						fpm.incProductStoreCountByPcid(inProductStoreLogBean,order.get("ps_id", 0l),orderItem[i].get("pid", 0l), orderItem[i].get("quantity", 0f),0l);						
					}
					else if ( orderItem[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )
					{
						DBRow productsInSet[] = fpm.getProductsInSetBySetPid(orderItem[i].get("pid", 0l));
						for (int j=0; j<productsInSet.length; j++)
						{
							fpm.incProductStoreCountByPcid(inProductStoreLogBean,order.get("ps_id", 0l),productsInSet[j].get("pid", 0l), productsInSet[j].get("quantity", 0f)*orderItem[i].get("quantity", 0f),0l);								
						}
					}
					else if ( orderItem[i].get("product_type", 0)==ProductTypeKey.UNION_CUSTOM )
					{
						DBRow productsCustomInSet[] = fpm.getProductsCustomInSetBySetPid(orderItem[i].get("pid", 0l));
						
						for (int j=0; j<productsCustomInSet.length; j++)
						{
							fpm.incProductStoreCountByPcid(inProductStoreLogBean,order.get("ps_id", 0l),productsCustomInSet[j].get("pid", 0l), productsCustomInSet[j].get("quantity", 0f)*orderItem[i].get("quantity", 0f),0l);								
						}
					}
					
					DBRow item = new DBRow();
					item.add("lacking",2);//回退库存后，订单上的货物明细标记为未知
				}
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"stockback",log);
		}
	}

	/**
	 * 抄单
	 * 是否疑问状态，由外部参数handle_status决定 
	 * @param request
	 * @throws Exception
	 */
	public int recordOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(StringUtil.getSession(request));
			
			boolean orderIsLacking = false;

				String record_order_note = StringUtil.getString(request,"record_order_note");
				long oid = StringUtil.getLong(request,"oid");
				//int order_error_email = StrUtil.getInt(request,"order_error_email");
				long ccid = StringUtil.getLong(request,"ccid");
				long ps_id = StringUtil.getLong(request,"ps_id");		//发货仓库
				String inv_dog = StringUtil.getString(request, "inv_dog");
				String inv_rfe = StringUtil.getString(request, "inv_rfe");
				String inv_uv = StringUtil.getString(request, "inv_uv");
				String inv_tv = StringUtil.getString(request, "inv_tv");
				long inv_di_id = StringUtil.getLong(request,"inv_di_id");
				String delivery_note = StringUtil.getString(request, "delivery_note");
				int product_type = StringUtil.getInt(request, "product_type");
				
				long recom_express = StringUtil.getLong(request,"recom_express");
				long pro_id = StringUtil.getLong(request,"pro_id");
				
				String tel = StringUtil.getString(request,"tel");
				String handle_status = StringUtil.getString(request,"handle_status");
				String address_name = StringUtil.getString(request,"address_name");
				String address_city = StringUtil.getString(request,"address_city");
				String address_zip = StringUtil.getString(request,"address_zip");
				String address_street = StringUtil.getString(request,"address_street");
				
				DBRow porder = new DBRow();
				porder.add("tel",tel);
				porder.add("handle_status",handle_status);
				porder.add("address_name",address_name);
				porder.add("address_city",address_city);
				porder.add("address_zip",address_zip);
				porder.add("address_street",address_street);

				DBRow oldOrder = fom.getDetailPOrderByOid(oid);
				DBRow oldOrderItems[] = fom.getPOrderItemsByOid(oid);
				DBRow cartProducts[] = cart.getSimpleProducts(StringUtil.getSession(request));

				/**
				 * 重复正常抄单，需要重新计算库存商品：原来缺货商品、新增商品、原来有货，但是修改了数量的商品
				 * 条件：正常抄单，订单原来状态是有货/缺货，并且发货仓库没有变化
				 */
				HashMap need2ReCalculateStoreHM = new HashMap();
				for (int i=0; i<cartProducts.length; i++)
				{
					need2ReCalculateStoreHM.put(cartProducts[i].getString("cart_pid")+"|"+cartProducts[i].getString("cart_product_type")+"|"+cartProducts[i].getString("cart_quantity"),cartProducts[i].getString("cart_pid")+"|"+cartProducts[i].getString("cart_product_type"));
				}
				if ( StringUtil.getInt( porder.getString("handle_status") )==HandStatusleKey.NORMAL&&oldOrder.get("product_status", 0)!=ProductStatusKey.UNKNOWN&&ps_id==oldOrder.get("ps_id", 0l) )
				{
					//商品唯一标识：pid+product_type+quantity
					//抹掉不需要重新计算库存商品
					for (int i=0; i<oldOrderItems.length; i++)
					{
						//缺货肯定要重新计算
						if ( oldOrderItems[i].get("lacking", 0)==ProductStatusKey.STORE_OUT )
						{
							continue;
						}
						
						//如果是定制商品，需要做进一步判断
						if ( oldOrderItems[i].get("product_type",0)==ProductTypeKey.UNION_CUSTOM )
						{
							DBRow oldCustomProducts[] = fpm.getProductsCustomInSetBySetPid(oldOrderItems[i].get("pid",0l));
							DBRow newCustomProducts[] = customCart.getFinalDetailProducts(StringUtil.getSession(request), oldOrderItems[i].get("pid",0l));
							
							if (!customProductNotChanged(oldCustomProducts, newCustomProducts))
							{
								continue;
							}
						}

						need2ReCalculateStoreHM.remove( oldOrderItems[i].getString("pid")+"|"+oldOrderItems[i].getString("product_type")+"|"+oldOrderItems[i].getString("quantity") );
					}
				}

				/**
				 * 插入订单商品
				 * 插入商品前，先把原来的删掉
				 * 
				 * 因为有可能出现修改抄单的情况，为了保证库存正确，所以需要检测该订单是否已经抄单，如果曾经抄单，需要把现有商品的数量返回给现在的库存（注意区分普通商品和组合商品）
				 * 为了避免一些复杂情况，订单管理页面已经限制发货后，不能做操作
				 */
				
				this.stockback(cartProducts,StringUtil.getInt( porder.getString("handle_status") ),ps_id, oldOrder, oldOrderItems,StringUtil.getSession(request));
				//重新计算购物车数据后，获得详细信息
				cart.flush(StringUtil.getSession(request));
				cartProducts = cart.getDetailProducts();
				//退回库存后，清掉旧数据
				fom.delPOrderItem(oid);

				//缺货商品清单
				ArrayList lackingProductList = new ArrayList();

				for (int i=0; i<cartProducts.length; i++)
				{
					/**
					 * 正常抄单才减掉库存，疑问抄单不减少，以免影不正常订单情况影响库存
					 * 缺货抄单需要减掉库存，是因为可以使库存出现负值，使仓库人员能知道
					 * 
					 * 重复抄单还需要过滤掉部分商品
					 */
					////system.out.println(cartProducts[i].getString("cart_pid"));
					if (StringUtil.getInt( porder.getString("handle_status") )==HandStatusleKey.NORMAL&&need2ReCalculateStoreHM.containsValue(cartProducts[i].getString("cart_pid")+"|"+cartProducts[i].getString("cart_product_type")))
					{
						////system.out.println("["+DateUtil.NowStr()+"] [扣库存] "+cartProducts[i].getString("p_name"));

						/**
						 * 减少库存区分普通商品、标准套装、标准套装拼装、定制套装
						 * 普通商品：直接减少库存
						 * 标准套装：
						 *	1、标准套装够库存，直接减少库存
						 *  2、标准套装满足部分库存，散件满足部分库存，标准套装和散件分别减少库存
						 *  3、标准套装没库存，散件拼装有库存，减少散件库存
						 *  4、标准套装没库存，散件拼装没库存，减少标准套装库存
						 * 注意：当标准套装库存不够，散件够，需要拆分成虚拟标准套装（即拼装成的） 
						 * 
						 * 定制套装：套装中的散件减少库存
						 */

							//先把手工套装转换为标准套装
							//为了让系统重新判断套装是否需要拆分
							if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL)
							{
								cartProducts[i].add("cart_product_type", ProductTypeKey.UNION_STANDARD);
							}

							//进出库日志
							ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
							deInProductStoreLogBean.setAccount( (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAccount() );
							deInProductStoreLogBean.setOid(oid);
//							deInProductStoreLogBean.setOperation("抄单 提取 库存");
							
							if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.NORMAL)
							{
								int lacking = ProductStatusKey.IN_STORE;		//防止购物车有多个普通商品的时候，其中一个缺货，其他也被标记为缺货

								//确保两个线程同时检查库存的时候有先后顺序
								synchronized(String.valueOf(ps_id)+"_"+cartProducts[i].getString("cart_pid"))
								{
									//检查库存
									//orderIsLacking
									DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,StringUtil.getLong(cartProducts[i].getString("cart_pid")));
									
									if (detailPro==null)	//	商品还没进库，当然缺货
									{
										orderIsLacking = true;
										lacking = ProductStatusKey.STORE_OUT;
									}
									else
									{
										if (detailPro.get("store_count", 0f)<StringUtil.getFloat(cartProducts[i].getString("cart_quantity")))
										{
											orderIsLacking = true;
											lacking = ProductStatusKey.STORE_OUT;
										}
									}
									
									//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), StrUtil.getFloat(cartProducts[i].getString("cart_quantity")));
								}

								DBRow orderItem = new DBRow();
								orderItem.add("oid",oid);
								orderItem.add("pid",cartProducts[i].getString("cart_pid"));
								orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
								orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
								orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
								orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
								orderItem.add("weight",cartProducts[i].get("weight", 0f));
								orderItem.add("name",cartProducts[i].getString("p_name"));
								orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
								orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
								orderItem.add("lacking",lacking);
								fom.addPOrderItem(orderItem);

								//如果缺货，插入缺货清单列表
								if (lacking==ProductStatusKey.STORE_OUT)
								{
									this.addLackingProductList(lackingProductList, oid, StringUtil.getLong(cartProducts[i].getString("cart_pid")), cartProducts[i].getString("p_name"), StringUtil.getFloat(cartProducts[i].getString("cart_quantity")));
								}
							}
							else if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD)
							{
								synchronized(String.valueOf(ps_id)+"_"+cartProducts[i].getString("cart_pid"))
								{
									boolean notBeCreateStorage = false;
									DBRow detailProductStorage = fpm.getDetailProductProductStorageByPcid( ps_id,StringUtil.getLong(cartProducts[i].getString("cart_pid")) );
									if (detailProductStorage==null)
									{
										notBeCreateStorage = true;//没建库
										detailProductStorage = new DBRow();
										detailProductStorage.add("store_count",0);
									}

									//1、标准套装够库存，直接减少库存
									if (detailProductStorage.get("store_count", 0f)>=StringUtil.getFloat(cartProducts[i].getString("cart_quantity")))
									{
										DBRow orderItem = new DBRow();
										orderItem.add("oid",oid);
										orderItem.add("pid",cartProducts[i].getString("cart_pid"));
										orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
										orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
										orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
										orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
										orderItem.add("weight",cartProducts[i].get("weight", 0f));
										orderItem.add("name",cartProducts[i].getString("p_name"));
										orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
										orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
										orderItem.add("lacking",ProductStatusKey.IN_STORE);
										fom.addPOrderItem(orderItem);

										//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), StrUtil.getFloat(cartProducts[i].getString("cart_quantity")));
									}
									//2、标准套装满足部分库存，散件满足部分库存，标准套装和散件分别减少库存
									else
									{
										float needManualQuantity ;
										//计算需要散件的套数
										if (detailProductStorage.get("store_count", 0f)>0)
										{
											//除了标准套装提供的数量外，还需要瓶装多少套散件
											needManualQuantity = StringUtil.getFloat(cartProducts[i].getString("cart_quantity")) - detailProductStorage.get("store_count", 0f);
										}
										else
										{
											needManualQuantity = StringUtil.getFloat(cartProducts[i].getString("cart_quantity"));
										}

										//标准套装不够，检查散件是否够拼装
										boolean isLacking = false;
										DBRow productsInSet[] = fpm.getProductsInSetBySetPid( StringUtil.getLong(cartProducts[i].getString("cart_pid")) );
										for (int j=0; j<productsInSet.length; j++)
										{
											float productsInSetCount = productsInSet[j].get("quantity", 0f)*needManualQuantity;
											DBRow productStorage = fpm.getDetailProductStorageByRscIdPcid(ps_id,productsInSet[j].get("pid", 0l));
											if (productStorage==null||productStorage.get("store_count", 0f)<productsInSetCount)
											{
												isLacking = true;
												break;
											}
										}
										//散件也不够库存，并且商品已经在这个区域仓库建库，直接减掉标准套装库存
										if (isLacking&&notBeCreateStorage==false)
										{
											DBRow orderItem = new DBRow();
											orderItem.add("oid",oid);
											orderItem.add("pid",cartProducts[i].getString("cart_pid"));
											orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
											orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
											orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
											orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
											orderItem.add("weight",cartProducts[i].get("weight", 0f));
											orderItem.add("name",cartProducts[i].getString("p_name"));
											orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
											orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
											orderItem.add("lacking",ProductStatusKey.STORE_OUT);
											fom.addPOrderItem(orderItem);
											
											//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), StrUtil.getFloat(cartProducts[i].getString("cart_quantity")));
											
											//如果缺货，插入缺货清单列表
											this.addLackingProductList(lackingProductList, oid, StringUtil.getLong(cartProducts[i].getString("cart_pid")), cartProducts[i].getString("p_name"), StringUtil.getFloat(cartProducts[i].getString("cart_quantity")));
										}
										//散件够库存
										else
										{
											//实际需要标准套装数-瓶装提供的数目
											float needUnionStandardCount = StringUtil.getFloat(cartProducts[i].getString("cart_quantity")) - needManualQuantity;

											//标准套装需要的数目
											if (needUnionStandardCount>0)
											{
												DBRow orderItem = new DBRow();
												orderItem.add("oid",oid);
												orderItem.add("pid",cartProducts[i].getString("cart_pid"));
												orderItem.add("quantity",needUnionStandardCount);
												orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
												orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
												orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
												orderItem.add("weight",cartProducts[i].get("weight", 0f));
												orderItem.add("name",cartProducts[i].getString("p_name"));
												orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
												orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
												orderItem.add("lacking",ProductStatusKey.IN_STORE);
												fom.addPOrderItem(orderItem);
												
												//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), needUnionStandardCount);
											}

											//插入散件套装
											DBRow orderItem = new DBRow();
											orderItem.add("oid",oid);
											orderItem.add("pid",cartProducts[i].getString("cart_pid"));
											orderItem.add("quantity",needManualQuantity);
											orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
											orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
											orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
											orderItem.add("weight",cartProducts[i].get("weight", 0f));
											orderItem.add("name",cartProducts[i].getString("p_name"));
											orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
											orderItem.add("product_type",ProductTypeKey.UNION_STANDARD_MANUAL);

											if (isLacking)
											{
												orderItem.add("lacking",ProductStatusKey.STORE_OUT);		
											}
											else
											{
												orderItem.add("lacking",ProductStatusKey.IN_STORE);
											}

											////system.out.println(isLacking+" | "+orderItem.getString("lacking"));
											
											fom.addPOrderItem(orderItem);
											
											//减掉散件拼装的数量
											for (int j=0; j<productsInSet.length; j++)
											{
												float productsInSetCount = productsInSet[j].get("quantity", 0f)*needManualQuantity;
												//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsInSet[j].get("pc_id",0l), productsInSetCount);
												
												DBRow productStorage = fpm.getDetailProductStorageByRscIdPcid(ps_id,productsInSet[j].get("pid", 0l));
												if (productStorage==null||productStorage.get("store_count", 0f)<productsInSetCount)
												{
													//如果缺货，插入缺货清单列表
													this.addLackingProductList(lackingProductList, oid, StringUtil.getLong(productsInSet[j].getString("pc_id")), productsInSet[j].getString("p_name"), productsInSetCount);
												}
											}
										}

										//只有当散件也缺货的时候，才把订单也标记为缺货
										if (isLacking)
										{
											orderIsLacking = isLacking;
										}
									}
								}
							}
							else  //定制 
							{
								int lacking = 0;		//防止购物车有多个普通商品的时候，其中一个缺货，其他也被标记为缺货
								
								//先把旧的定制商品删掉
								fpm.delProductCustomUnionBySetPid(StringUtil.getLong(cartProducts[i].getString("cart_pid")));
								
								DBRow productsCustomInSet[] = customCart.getFinalDetailProducts(StringUtil.getSession(request), StringUtil.getLong(cartProducts[i].getString("cart_pid")));
								for (int j=0; j<productsCustomInSet.length; j++)
								{
									synchronized(String.valueOf(ps_id)+"_"+productsCustomInSet[j].getString("pc_id"))
									{
										//检查库存
										DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,productsCustomInSet[j].get("pc_id", 0l));
										if (detailPro==null)	//	商品还没进库，当然缺货
										{
											orderIsLacking = true;
											lacking = ProductStatusKey.STORE_OUT;
										}
										else
										{
											if (detailPro.get("store_count", 0f)< StringUtil.getFloat(productsCustomInSet[j].getString("cart_quantity"))*StringUtil.getFloat(cartProducts[i].getString("cart_quantity")) )
											{
												orderIsLacking = true;
												lacking = ProductStatusKey.STORE_OUT;
												
												//如果缺货，插入缺货清单列表
												this.addLackingProductList(lackingProductList, oid, productsCustomInSet[j].get("pc_id", 0l), productsCustomInSet[j].getString("p_name"), StringUtil.getFloat(productsCustomInSet[j].getString("cart_quantity"))*StringUtil.getFloat(cartProducts[i].getString("cart_quantity")));
											}
										}
										fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsCustomInSet[j].get("pc_id", 0l), StringUtil.getFloat(productsCustomInSet[j].getString("cart_quantity"))*StringUtil.getFloat(cartProducts[i].getString("cart_quantity")));
										
										//插入到定制表
										DBRow customPro = new DBRow();
										customPro.add("set_pid",cartProducts[i].getString("cart_pid"));
										customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
										customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
										fpm.addProductCustomUnion(customPro);
									}
								}

								DBRow orderItem = new DBRow();
								orderItem.add("oid",oid);
								orderItem.add("pid",cartProducts[i].getString("cart_pid"));
								orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
								orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
								orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
								orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
								orderItem.add("weight",cartProducts[i].get("weight", 0f));
								orderItem.add("name",cartProducts[i].getString("p_name"));
								orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
								orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
								orderItem.add("lacking",lacking);
								
								fom.addPOrderItem(orderItem);
							}
						}
					else
					{
						//除了正常抄单和缺货抄单外，其他抄单都不需要处理库存，直接插入数据即可
						DBRow orderItem = new DBRow();
						orderItem.add("oid",oid);
						orderItem.add("pid",cartProducts[i].getString("cart_pid"));
						orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
						orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
						orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
						orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
						orderItem.add("weight",cartProducts[i].get("weight", 0f));
						orderItem.add("name",cartProducts[i].getString("p_name"));
						orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
						orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
						orderItem.add("lacking",ProductStatusKey.UNKNOWN);//默认商品未扣库存
						fom.addPOrderItem(orderItem);
						
						/**
						 * 如果是定制商品，还需要插入定制商品
						 * 一种特殊情况：第一次抄单，抄的是定制商品，但是是疑问抄单，如果不插入定制商品，则重新抄单修改定制商品会出错
						 */
						if ( StringUtil.getLong(cartProducts[i].getString("cart_product_type"))==ProductTypeKey.UNION_CUSTOM )
						{
							
							//先把旧的定制商品删掉
							fpm.delProductCustomUnionBySetPid(StringUtil.getLong(cartProducts[i].getString("cart_pid")));
							DBRow productsCustomInSet[] = customCart.getFinalDetailProducts(StringUtil.getSession(request), StringUtil.getLong(cartProducts[i].getString("cart_pid")));
							for (int j=0; j<productsCustomInSet.length; j++)
							{
								//插入到定制表
								DBRow customPro = new DBRow();
								customPro.add("set_pid",cartProducts[i].getString("cart_pid"));
								customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
								customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
								fpm.addProductCustomUnion(customPro);
							}
						}
					}
					
				}
				cart.clearCart(StringUtil.getSession(request));			//清空购物车
				
				//插入缺货商品列表
				this.addOrderLackingList(oid, lackingProductList);
				
				
				/**
				 * 正常抄单才检查是否缺货(只有正常抄单，因为疑问状态抄单，不需要减少库存)
				 */
				if (porder.getString("handle_status").equals(String.valueOf( HandStatusleKey.NORMAL )))
				{
					if (orderIsLacking)
					{
						porder.add("product_status",ProductStatusKey.STORE_OUT);
						record_order_note += " - 缺货";			
						
						//如果这个时候订单处于已打印，需要把订单改为 待打印
						if ( oldOrder.get("handle",0)==HandleKey.PRINTED )
						{
							porder.add("handle",HandleKey.WAIT4_PRINT);
						}
					}
					else
					{
						porder.add("product_status",ProductStatusKey.IN_STORE);
					}
				}
				else
				{
					/**
					 * 疑问抄单会把之前库存退掉，并且不做库存计算，所以需要把货物状态标记为 未知！
					 */
					porder.add("product_status",ProductStatusKey.UNKNOWN);
				}

				/**
				 * 只有正常抄单才能标记为 待打印
				 * 不管是有货还是缺货
				 */
				if ( oldOrder.get("handle",0)==HandleKey.WAIT4_RECORD&&porder.getString("handle_status").equals(String.valueOf( HandStatusleKey.NORMAL )) )
				{
					porder.add("handle",HandleKey.WAIT4_PRINT);
					//此处添加地址验证
//					if(oldOrder.getString("address_country").toLowerCase().equals("united states"))
//					{
//						//收货地址是美国就验证
//						if(!oldOrder.getString("address_street").equals("")&&!oldOrder.getString("address_zip").equals(""))
//						{
							ordersMgrQLL.addressValidateFedexJMS(oid,oldOrder.getString("address_street"),oldOrder.getString("address_zip"));
//						}
//					}
					
					if(porder.getString("address_country").toLowerCase().equals("united states"))
					{
						//收货地址是美国就验证
						if(!porder.getString("address_street").equals("")&&!porder.getString("address_zip").equals(""))
						{
							ordersMgrQLL.addressValidateFedexJMS(oid,porder.getString("address_street"),porder.getString("address_zip"));
						}
					}
				}

				//如果PAYPAL进来的国家不对，这里需要抄国家
				if (ccid>0)
				{
					DBRow detailCountryCode = fom.getDetailCountryCodeByCcid(ccid);
					porder.add("address_country",detailCountryCode.getString("c_country"));
					porder.add("ccid",ccid);
				}

				//订单原来的状态为：正常、缺货、疑问，在抄单的时候才修改状态
				//这个逻辑需要修改下
//				if (oldOrder.get("handle_status", 0)!=HandStatusleKey.NORMAL&&oldOrder.get("handle_status", 0)!=HandStatusleKey.LACK&&oldOrder.get("handle_status", 0)!=HandStatusleKey.DOUBT)
//				{
//					porder.remove("handle_status");
//				}

				//如果是第一次抄单，则记录抄单时间和抄单人
				if (oldOrder.getString("record_account").equals(""))
				{
					porder.add("record_account",adminLoggerBean.getAccount());
					porder.add("record_date",DateUtil.NowStr());
				}

				porder.add("ps_id",ps_id);
				porder.add("quantity",cart.getSumQuantity());
				porder.add("inv_dog",inv_dog);
				porder.add("inv_tv",inv_tv);
				porder.add("inv_uv",inv_uv);
				porder.add("inv_rfe",inv_rfe);
				porder.add("inv_di_id",inv_di_id);
				porder.add("delivery_note",delivery_note);
				porder.add("product_type",product_type);
				porder.add("recom_express",recom_express);
				porder.add("pro_id",pro_id);

				fom.modPOrder(oid,porder);

				//如果状态为已打印，
				if ( oldOrder.get("handle", 0)==HandleKey.PRINTED && ( (oldOrder.get("handle_status", 0)==HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("handle_status"))==HandStatusleKey.NORMAL)||(oldOrder.get("handle_status", 0)!=HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("handle_status"))==HandStatusleKey.NORMAL)||(oldOrder.get("handle_status", 0)==HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("handle_status"))!=HandStatusleKey.NORMAL) ) )
				{
					String t_note = "";
					long t_target_admin = 0;//目标管理员0，代表发送给整个角色执行
					long t_target_group = StringUtil.getLong( systemConfig.getStringConfigValue("order_task_operator_id") );//hard code
					int t_operationsA[];
					
					/**
					 * 进来只有三种情况：
					 * 正常 -> 正常
					 * 正常 -> 疑问
					 * 疑问 -> 正常
					 */
					
					//没有切换仓库的情况下
					if (oldOrder.get("ps_id", 0l)==ps_id)
					{
						if ( StringUtil.getInt(porder.getString("handle_status"))==HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("product_status"))==ProductStatusKey.IN_STORE )
						{
							//正常/有货需要重新打印
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY,OrderTaskKey.T_OPERATION_RE_PRINT};
						}
						else
						{
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY};
						}
						
						taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_RECORD, t_operationsA, oid, t_note, t_target_admin, t_target_group,oldOrder.get("ps_id", 0l));
					}
					else
					{
						if ( StringUtil.getInt(porder.getString("handle_status"))==HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("product_status"))==ProductStatusKey.IN_STORE )
						{
							//旧仓库撕毁
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY};
							taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_RECORD, t_operationsA, oid, t_note, t_target_admin, t_target_group,oldOrder.get("ps_id", 0l));
							
							//新仓库重打
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_RE_PRINT};
							taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_RECORD, t_operationsA, oid, t_note, t_target_admin, t_target_group,ps_id);
						}
						else
						{
							//旧仓库撕毁
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY};
							taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_RECORD, t_operationsA, oid, t_note, t_target_admin, t_target_group,oldOrder.get("ps_id", 0l));
						}
					}
				}

				//增加相关备注
				int trace_type = 0;
				
				if ( StringUtil.getInt(request,"handle_status")==HandStatusleKey.DOUBT )
				{
					trace_type = TracingOrderKey.RECORD_DOUBT_ORDER;
				}
				else if ( StringUtil.getInt(request,"handle_status")==HandStatusleKey.DOUBT_ADDRESS )
				{
					trace_type = TracingOrderKey.RECORD_DOUBT_ADDRESS_ORDER;
				}
				else if ( StringUtil.getInt(request,"handle_status")==HandStatusleKey.DOUBT_PAY )
				{
					trace_type = TracingOrderKey.RECORD_DOUBT_PAY_ORDER;
				}
				else
				{
					//再判断是否缺货抄单
					if ( porder.get("product_status",0)==ProductStatusKey.STORE_OUT )
					{
						trace_type = TracingOrderKey.RECORD_LACKING_ORDER;
					}
					else
					{
						trace_type = TracingOrderKey.RECORD_NORMAL_ORDER;
					}
				}
				
				this.addPOrderNotePrivate(oid, record_order_note,trace_type, StringUtil.getSession(request),0);

				//计算订单商品成本和运费成本

				
				
				//根据外部参数，决定发送邮件通知类型
				//不再发邮件
//				if ( order_error_email==1 )
//				{
//					sendMailForNotModel(oid);//无型号邮件
//				}
//				else if ( order_error_email==2 )
//				{
//					sendMailForDoubtModel(oid);//疑问型号邮件
//				}

				//通知该订单是缺货抄单
				if (porder.get("product_status",0)==ProductStatusKey.STORE_OUT)
				{
					return(ProductStatusKey.STORE_OUT);
				}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"recordOrder",log);
		}
		
		return(HandStatusleKey.NORMAL);
	}
	
	/**
	 * 新版抄单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public int newRecordOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(StringUtil.getSession(request));
			
			boolean orderIsLacking = false;
	
				String record_order_note = StringUtil.getString(request,"record_order_note");
				long oid = StringUtil.getLong(request,"oid");
				//int order_error_email = StrUtil.getInt(request,"order_error_email");
				long ccid = StringUtil.getLong(request,"ccid");
				long ps_id = StringUtil.getLong(request,"ps_id");		//发货仓库
				String inv_dog = StringUtil.getString(request, "inv_dog");
				String inv_rfe = StringUtil.getString(request, "inv_rfe");
				String inv_uv = StringUtil.getString(request, "inv_uv");
				String inv_tv = StringUtil.getString(request, "inv_tv");
				long inv_di_id = StringUtil.getLong(request,"inv_di_id");
				String delivery_note = StringUtil.getString(request, "delivery_note");
				int product_type = StringUtil.getInt(request, "product_type");
				
				long recom_express = StringUtil.getLong(request,"recom_express");
				long pro_id = StringUtil.getLong(request,"pro_id");
				
				String tel = StringUtil.getString(request,"tel");
				String handle_status = StringUtil.getString(request,"handle_status");
				String address_name = StringUtil.getString(request,"address_name");
				String address_city = StringUtil.getString(request,"address_city");
				String address_zip = StringUtil.getString(request,"address_zip");
				String address_street = StringUtil.getString(request,"address_street");
				
				DBRow porder = new DBRow();
				porder.add("tel",tel);
				porder.add("handle_status",handle_status);
				porder.add("address_name",address_name);
				porder.add("address_city",address_city);
				porder.add("address_zip",address_zip);
				porder.add("address_street",address_street);
	
				DBRow oldOrder = fom.getDetailPOrderByOid(oid);
				DBRow oldOrderItems[] = fom.getPOrderItemsByOid(oid);
				DBRow cartProducts[] = cart.getSimpleProducts(StringUtil.getSession(request));
	
				/**
				 * 重复正常抄单，需要重新计算库存商品：原来缺货商品、新增商品、原来有货，但是修改了数量的商品
				 * 条件：正常抄单，订单原来状态是有货/缺货，并且发货仓库没有变化
				 */
				HashMap need2ReCalculateStoreHM = new HashMap();
				for (int i=0; i<cartProducts.length; i++)
				{
					need2ReCalculateStoreHM.put(cartProducts[i].getString("cart_pid")+"|"+cartProducts[i].getString("cart_product_type")+"|"+cartProducts[i].getString("cart_quantity"),cartProducts[i].getString("cart_pid")+"|"+cartProducts[i].getString("cart_product_type"));
				}
				if ( StringUtil.getInt( porder.getString("handle_status") )==HandStatusleKey.NORMAL&&oldOrder.get("product_status", 0)!=ProductStatusKey.UNKNOWN&&ps_id==oldOrder.get("ps_id", 0l) )
				{
					//商品唯一标识：pid+product_type+quantity
					//抹掉不需要重新计算库存商品
					for (int i=0; i<oldOrderItems.length; i++)
					{
						//缺货肯定要重新计算
						if ( oldOrderItems[i].get("lacking", 0)==ProductStatusKey.STORE_OUT)
						{
							continue;
						}
						
						//如果是定制商品，需要做进一步判断
						if ( oldOrderItems[i].get("product_type",0)==ProductTypeKey.UNION_CUSTOM )
						{
							DBRow oldCustomProducts[] = fpm.getProductsCustomInSetBySetPid(oldOrderItems[i].get("pid",0l));
							DBRow newCustomProducts[] = customCart.getFinalDetailProducts(StringUtil.getSession(request), oldOrderItems[i].get("pid",0l));
							
							if (!customProductNotChanged(oldCustomProducts, newCustomProducts))
							{
								continue;
							}
						}
	
						need2ReCalculateStoreHM.remove( oldOrderItems[i].getString("pid")+"|"+oldOrderItems[i].getString("product_type")+"|"+oldOrderItems[i].getString("quantity") );
					}
				}
	
				/**
				 * 插入订单商品
				 * 插入商品前，先把原来的删掉
				 * 
				 * 因为有可能出现修改抄单的情况，为了保证库存正确，所以需要检测该订单是否已经抄单，如果曾经抄单，需要把现有商品的数量返回给现在的库存（注意区分普通商品和组合商品）
				 * 为了避免一些复杂情况，订单管理页面已经限制发货后，不能做操作
				 */
				
				this.stockback(cartProducts,StringUtil.getInt( porder.getString("handle_status") ),ps_id, oldOrder, oldOrderItems,StringUtil.getSession(request));
				//重新计算购物车数据后，获得详细信息
				cart.flush(StringUtil.getSession(request));
				cartProducts = cart.getDetailProducts();
				//退回库存后，清掉旧数据
				fom.delPOrderItem(oid);
	
				//缺货商品清单
				ArrayList lackingProductList = new ArrayList();
	
				for (int i=0; i<cartProducts.length; i++)
				{
					/**
					 * 正常抄单才减掉库存，疑问抄单不减少，以免影不正常订单情况影响库存
					 * 缺货抄单需要减掉库存，是因为可以使库存出现负值，使仓库人员能知道
					 * 
					 * 重复抄单还需要过滤掉部分商品
					 */
					////system.out.println(cartProducts[i].getString("cart_pid"));
					if (StringUtil.getInt( porder.getString("handle_status") )==HandStatusleKey.NORMAL&&need2ReCalculateStoreHM.containsValue(cartProducts[i].getString("cart_pid")+"|"+cartProducts[i].getString("cart_product_type")))
					{
						////system.out.println("["+DateUtil.NowStr()+"] [扣库存] "+cartProducts[i].getString("p_name"));
	
						/**
						 * 减少库存区分普通商品、标准套装、标准套装拼装、定制套装
						 * 普通商品：直接减少库存
						 * 标准套装：
						 *	1、标准套装够库存，直接减少库存
						 *  2、标准套装满足部分库存，散件满足部分库存，标准套装和散件分别减少库存
						 *  3、标准套装没库存，散件拼装有库存，减少散件库存
						 *  4、标准套装没库存，散件拼装没库存，减少标准套装库存
						 * 注意：当标准套装库存不够，散件够，需要拆分成虚拟标准套装（即拼装成的） 
						 * 
						 * 定制套装：套装中的散件减少库存
						 */
	
							//先把手工套装转换为标准套装
							//为了让系统重新判断套装是否需要拆分
							if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL)
							{
								cartProducts[i].add("cart_product_type", ProductTypeKey.UNION_STANDARD);
							}
	
							//进出库日志
							ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
							deInProductStoreLogBean.setAccount( (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAccount() );
							deInProductStoreLogBean.setOid(oid);
//							deInProductStoreLogBean.setOperation("抄单 提取 库存");
							
							if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.NORMAL)
							{
								int lacking = ProductStatusKey.IN_STORE;		//防止购物车有多个普通商品的时候，其中一个缺货，其他也被标记为缺货
	
								//确保两个线程同时检查库存的时候有先后顺序
								synchronized(String.valueOf(ps_id)+"_"+cartProducts[i].getString("cart_pid"))
								{
									//检查库存
									//orderIsLacking
									DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,StringUtil.getLong(cartProducts[i].getString("cart_pid")));
									
									if (detailPro==null)	//	商品还没进库，当然缺货
									{
										orderIsLacking = true;
										lacking = ProductStatusKey.STORE_OUT;
									}
									else
									{
										if (detailPro.get("store_count", 0f)<StringUtil.getFloat(cartProducts[i].getString("cart_quantity")))
										{
											orderIsLacking = true;
											lacking = ProductStatusKey.STORE_OUT;
										}
									}
									
									//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), StrUtil.getFloat(cartProducts[i].getString("cart_quantity")));
								}
	
								DBRow orderItem = new DBRow();
								orderItem.add("oid",oid);
								orderItem.add("pid",cartProducts[i].getString("cart_pid"));
								orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
								orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
								orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
								orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
								orderItem.add("weight",cartProducts[i].get("weight", 0f));
								orderItem.add("name",cartProducts[i].getString("p_name"));
								orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
								orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
								orderItem.add("lacking",lacking);
								fom.addPOrderItem(orderItem);
	
								//如果缺货，插入缺货清单列表
								if (lacking==ProductStatusKey.STORE_OUT)
								{
									this.addLackingProductList(lackingProductList, oid, StringUtil.getLong(cartProducts[i].getString("cart_pid")), cartProducts[i].getString("p_name"), StringUtil.getFloat(cartProducts[i].getString("cart_quantity")));
								}
							}
							else if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD)
							{
								synchronized(String.valueOf(ps_id)+"_"+cartProducts[i].getString("cart_pid"))
								{
									boolean notBeCreateStorage = false;
									DBRow detailProductStorage = fpm.getDetailProductProductStorageByPcid( ps_id,StringUtil.getLong(cartProducts[i].getString("cart_pid")) );
									if (detailProductStorage==null)
									{
										notBeCreateStorage = true;//没建库
										detailProductStorage = new DBRow();
										detailProductStorage.add("store_count", 0);
									}
	
									//1、标准套装够库存，直接减少库存
									if (detailProductStorage.get("store_count", 0f)>=StringUtil.getFloat(cartProducts[i].getString("cart_quantity")))
									{
										DBRow orderItem = new DBRow();
										orderItem.add("oid",oid);
										orderItem.add("pid",cartProducts[i].getString("cart_pid"));
										orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
										orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
										orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
										orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
										orderItem.add("weight",cartProducts[i].get("weight", 0f));
										orderItem.add("name",cartProducts[i].getString("p_name"));
										orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
										orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
										orderItem.add("lacking",ProductStatusKey.IN_STORE);
										fom.addPOrderItem(orderItem);
	
										//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), StrUtil.getFloat(cartProducts[i].getString("cart_quantity")));
									}
									//2、标准套装满足部分库存，散件满足部分库存，标准套装和散件分别减少库存
									else
									{
										float needManualQuantity ;
										//计算需要散件的套数
										if (detailProductStorage.get("store_count", 0f)>0)
										{
											//除了标准套装提供的数量外，还需要瓶装多少套散件
											needManualQuantity = StringUtil.getFloat(cartProducts[i].getString("cart_quantity")) - detailProductStorage.get("store_count", 0f);
										}
										else
										{
											needManualQuantity = StringUtil.getFloat(cartProducts[i].getString("cart_quantity"));
										}
	
										//标准套装不够，检查散件是否够拼装
										boolean isLacking = false;
										DBRow productsInSet[] = fpm.getProductsInSetBySetPid( StringUtil.getLong(cartProducts[i].getString("cart_pid")) );
										for (int j=0; j<productsInSet.length; j++)
										{
											float productsInSetCount = productsInSet[j].get("quantity", 0f)*needManualQuantity;
											DBRow productStorage = fpm.getDetailProductStorageByRscIdPcid(ps_id,productsInSet[j].get("pid", 0l));
											if (productStorage==null||productStorage.get("store_count", 0f)<productsInSetCount)
											{
												isLacking = true;
												break;
											}
										}
										//散件也不够库存，并且商品已经在这个区域仓库建库，直接减掉标准套装库存
										if (isLacking&&notBeCreateStorage==false)
										{
											DBRow orderItem = new DBRow();
											orderItem.add("oid",oid);
											orderItem.add("pid",cartProducts[i].getString("cart_pid"));
											orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
											orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
											orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
											orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
											orderItem.add("weight",cartProducts[i].get("weight", 0f));
											orderItem.add("name",cartProducts[i].getString("p_name"));
											orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
											orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
											orderItem.add("lacking",ProductStatusKey.STORE_OUT);
											fom.addPOrderItem(orderItem);
											
											//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), StrUtil.getFloat(cartProducts[i].getString("cart_quantity")));
											
											//如果缺货，插入缺货清单列表
											this.addLackingProductList(lackingProductList, oid, StringUtil.getLong(cartProducts[i].getString("cart_pid")), cartProducts[i].getString("p_name"), StringUtil.getFloat(cartProducts[i].getString("cart_quantity")));
										}
										//散件够库存
										else
										{
											//实际需要标准套装数-瓶装提供的数目
											float needUnionStandardCount = StringUtil.getFloat(cartProducts[i].getString("cart_quantity")) - needManualQuantity;
	
											//标准套装需要的数目
											if (needUnionStandardCount>0)
											{
												DBRow orderItem = new DBRow();
												orderItem.add("oid",oid);
												orderItem.add("pid",cartProducts[i].getString("cart_pid"));
												orderItem.add("quantity",needUnionStandardCount);
												orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
												orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
												orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
												orderItem.add("weight",cartProducts[i].get("weight", 0f));
												orderItem.add("name",cartProducts[i].getString("p_name"));
												orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
												orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
												orderItem.add("lacking",ProductStatusKey.IN_STORE);
												fom.addPOrderItem(orderItem);
												
												//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,StrUtil.getLong(cartProducts[i].getString("cart_pid")), needUnionStandardCount);
											}
	
											//插入散件套装
											DBRow orderItem = new DBRow();
											orderItem.add("oid",oid);
											orderItem.add("pid",cartProducts[i].getString("cart_pid"));
											orderItem.add("quantity",needManualQuantity);
											orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
											orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
											orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
											orderItem.add("weight",cartProducts[i].get("weight", 0f));
											orderItem.add("name",cartProducts[i].getString("p_name"));
											orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
											orderItem.add("product_type",ProductTypeKey.UNION_STANDARD_MANUAL);
	
											if (isLacking)
											{
												orderItem.add("lacking",ProductStatusKey.STORE_OUT);		
											}
											else
											{
												orderItem.add("lacking",ProductStatusKey.IN_STORE);
											}
	
											////system.out.println(isLacking+" | "+orderItem.getString("lacking"));
											
											fom.addPOrderItem(orderItem);
											
											//减掉散件拼装的数量
											for (int j=0; j<productsInSet.length; j++)
											{
												float productsInSetCount = productsInSet[j].get("quantity", 0f)*needManualQuantity;
												//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsInSet[j].get("pc_id",0l), productsInSetCount);
												
												DBRow productStorage = fpm.getDetailProductStorageByRscIdPcid(ps_id,productsInSet[j].get("pid", 0l));
												if (productStorage==null||productStorage.get("store_count", 0f)<productsInSetCount)
												{
													//如果缺货，插入缺货清单列表
													this.addLackingProductList(lackingProductList, oid, StringUtil.getLong(productsInSet[j].getString("pc_id")), productsInSet[j].getString("p_name"), productsInSetCount);
												}
											}
										}
	
										//只有当散件也缺货的时候，才把订单也标记为缺货
										if (isLacking)
										{
											orderIsLacking = isLacking;
										}
									}
								}
							}
							else  //定制 
							{
								int lacking = 0;		//防止购物车有多个普通商品的时候，其中一个缺货，其他也被标记为缺货
								
								//先把旧的定制商品删掉
								fpm.delProductCustomUnionBySetPid(StringUtil.getLong(cartProducts[i].getString("cart_pid")));
								
								DBRow productsCustomInSet[] = customCart.getFinalDetailProducts(StringUtil.getSession(request), StringUtil.getLong(cartProducts[i].getString("cart_pid")));
								for (int j=0; j<productsCustomInSet.length; j++)
								{
									synchronized(String.valueOf(ps_id)+"_"+productsCustomInSet[j].getString("pc_id"))
									{
										//检查库存
										DBRow detailPro = fpm.getDetailProductProductStorageByPcid(ps_id,productsCustomInSet[j].get("pc_id", 0l));
										if (detailPro==null)	//	商品还没进库，当然缺货
										{
											orderIsLacking = true;
											lacking = ProductStatusKey.STORE_OUT;
										}
										else
										{
											if (detailPro.get("store_count", 0f)< StringUtil.getFloat(productsCustomInSet[j].getString("cart_quantity"))*StringUtil.getFloat(cartProducts[i].getString("cart_quantity")) )
											{
												orderIsLacking = true;
												lacking = ProductStatusKey.STORE_OUT;
												
												//如果缺货，插入缺货清单列表
												this.addLackingProductList(lackingProductList, oid, productsCustomInSet[j].get("pc_id", 0l), productsCustomInSet[j].getString("p_name"), StringUtil.getFloat(productsCustomInSet[j].getString("cart_quantity"))*StringUtil.getFloat(cartProducts[i].getString("cart_quantity")));
											}
										}
										//fpm.deIncProductStoreCountByPcid(deInProductStoreLogBean,ps_id,productsCustomInSet[j].get("pc_id", 0l), StrUtil.getFloat(productsCustomInSet[j].getString("cart_quantity"))*StrUtil.getFloat(cartProducts[i].getString("cart_quantity")));
										
										//插入到定制表
										DBRow customPro = new DBRow();
										customPro.add("set_pid",cartProducts[i].getString("cart_pid"));
										customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
										customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
										fpm.addProductCustomUnion(customPro);
									}
								}
	
								DBRow orderItem = new DBRow();
								orderItem.add("oid",oid);
								orderItem.add("pid",cartProducts[i].getString("cart_pid"));
								orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
								orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
								orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
								orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
								orderItem.add("weight",cartProducts[i].get("weight", 0f));
								orderItem.add("name",cartProducts[i].getString("p_name"));
								orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
								orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
								orderItem.add("lacking",lacking);
								
								fom.addPOrderItem(orderItem);
							}
						}
					else
					{
						//除了正常抄单和缺货抄单外，其他抄单都不需要处理库存，直接插入数据即可
						DBRow orderItem = new DBRow();
						orderItem.add("oid",oid);
						orderItem.add("pid",cartProducts[i].getString("cart_pid"));
						orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
						orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
						orderItem.add("unit_price",cartProducts[i].get("unit_price", 0d));
						orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
						orderItem.add("weight",cartProducts[i].get("weight", 0f));
						orderItem.add("name",cartProducts[i].getString("p_name"));
						orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
						orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
						orderItem.add("lacking",ProductStatusKey.IN_STORE);//默认商品未扣库存，新抄单因传入的参数为正常抄单，可能永不到
						fom.addPOrderItem(orderItem);
						
						/**
						 * 如果是定制商品，还需要插入定制商品
						 * 一种特殊情况：第一次抄单，抄的是定制商品，但是是疑问抄单，如果不插入定制商品，则重新抄单修改定制商品会出错
						 */
						if ( StringUtil.getLong(cartProducts[i].getString("cart_product_type"))==ProductTypeKey.UNION_CUSTOM )
						{
							
							//先把旧的定制商品删掉
							fpm.delProductCustomUnionBySetPid(StringUtil.getLong(cartProducts[i].getString("cart_pid")));
							DBRow productsCustomInSet[] = customCart.getFinalDetailProducts(StringUtil.getSession(request), StringUtil.getLong(cartProducts[i].getString("cart_pid")));
							for (int j=0; j<productsCustomInSet.length; j++)
							{
								//插入到定制表
								DBRow customPro = new DBRow();
								customPro.add("set_pid",cartProducts[i].getString("cart_pid"));
								customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
								customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
								fpm.addProductCustomUnion(customPro);
							}
						}
					}
					
				}
//				cart.clearCart(StrUtil.getSession(request));				//清空购物车
				
				//插入缺货商品列表
				this.addOrderLackingList(oid, lackingProductList);
				
				
				/**
				 * 正常抄单才检查是否缺货(只有正常抄单，因为疑问状态抄单，不需要减少库存)
				 */
				if (porder.getString("handle_status").equals(String.valueOf( HandStatusleKey.NORMAL )))
				{
					if (orderIsLacking)
					{
						porder.add("product_status",ProductStatusKey.STORE_OUT);
						record_order_note += " - 缺货";			
						
						//如果这个时候订单处于已打印，需要把订单改为 待打印
						if ( oldOrder.get("handle",0)==HandleKey.PRINTED )
						{
							porder.add("handle",HandleKey.WAIT4_PRINT);
						}
					}
					else
					{
						porder.add("product_status",ProductStatusKey.IN_STORE);
					}
				}
				else
				{
					/**
					 * 疑问抄单会把之前库存退掉，并且不做库存计算，所以需要把货物状态标记为 未知！
					 */
					porder.add("product_status",ProductStatusKey.UNKNOWN);
				}
	
				/**
				 * 只有正常抄单才能标记为 待打印
				 * 不管是有货还是缺货
				 */
				if ( oldOrder.get("handle",0)==HandleKey.WAIT4_RECORD&&porder.getString("handle_status").equals(String.valueOf( HandStatusleKey.NORMAL )) )
				{
//					porder.add("handle",HandleKey.WAIT4_PRINT);
					//此处添加地址验证
//					if(oldOrder.getString("address_country").toLowerCase().equals("united states"))
//					{
//						//收货地址是美国就验证
//						if(!oldOrder.getString("address_street").equals("")&&!oldOrder.getString("address_zip").equals(""))
//						{
//							ordersMgrQLL.addressValidateFedexJMS(oid,oldOrder.getString("address_street"),oldOrder.getString("address_zip"));
//						}
//					}
					
					if(porder.getString("address_country").toLowerCase().equals("united states"))
					{
						//收货地址是美国就验证
						if(!porder.getString("address_street").equals("")&&!porder.getString("address_zip").equals(""))
						{
							ordersMgrQLL.addressValidateFedexJMS(oid,porder.getString("address_street"),porder.getString("address_zip"));
						}
					}
				}
	
				//如果PAYPAL进来的国家不对，这里需要抄国家
				if (ccid>0)
				{
					DBRow detailCountryCode = fom.getDetailCountryCodeByCcid(ccid);
					porder.add("address_country",detailCountryCode.getString("c_country"));
					porder.add("ccid",ccid);
				}
	
				//订单原来的状态为：正常、缺货、疑问，在抄单的时候才修改状态
				//这个逻辑需要修改下
	//			if (oldOrder.get("handle_status", 0)!=HandStatusleKey.NORMAL&&oldOrder.get("handle_status", 0)!=HandStatusleKey.LACK&&oldOrder.get("handle_status", 0)!=HandStatusleKey.DOUBT)
	//			{
	//				porder.remove("handle_status");
	//			}
	
				//如果是第一次抄单，则记录抄单时间和抄单人
				if (oldOrder.getString("record_account").equals(""))
				{
					porder.add("record_account",adminLoggerBean.getAccount());
					porder.add("record_date",DateUtil.NowStr());
				}
	
				porder.add("ps_id",ps_id);
				porder.add("quantity",cart.getSumQuantity());
				porder.add("inv_dog",inv_dog);
				porder.add("inv_tv",inv_tv);
				porder.add("inv_uv",inv_uv);
				porder.add("inv_rfe",inv_rfe);
				porder.add("inv_di_id",inv_di_id);
				porder.add("delivery_note",delivery_note);
				porder.add("product_type",product_type);
				porder.add("recom_express",recom_express);
				porder.add("pro_id",pro_id);
	
				fom.modPOrder(oid,porder);
	
				//如果状态为已打印，
				if ( oldOrder.get("handle", 0)==HandleKey.PRINTED && ( (oldOrder.get("handle_status", 0)==HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("handle_status"))==HandStatusleKey.NORMAL)||(oldOrder.get("handle_status", 0)!=HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("handle_status"))==HandStatusleKey.NORMAL)||(oldOrder.get("handle_status", 0)==HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("handle_status"))!=HandStatusleKey.NORMAL) ) )
				{
					String t_note = "";
					long t_target_admin = 0;//目标管理员0，代表发送给整个角色执行
					long t_target_group = StringUtil.getLong( systemConfig.getStringConfigValue("order_task_operator_id") );//hard code
					int t_operationsA[];
					
					/**
					 * 进来只有三种情况：
					 * 正常 -> 正常
					 * 正常 -> 疑问
					 * 疑问 -> 正常
					 */
					
					//没有切换仓库的情况下
					if (oldOrder.get("ps_id", 0l)==ps_id)
					{
						if ( StringUtil.getInt(porder.getString("handle_status"))==HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("product_status"))==ProductStatusKey.IN_STORE )
						{
							//正常/有货需要重新打印
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY,OrderTaskKey.T_OPERATION_RE_PRINT};
						}
						else
						{
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY};
						}
						
						taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_RECORD, t_operationsA, oid, t_note, t_target_admin, t_target_group,oldOrder.get("ps_id", 0l));
					}
					else
					{
						if ( StringUtil.getInt(porder.getString("handle_status"))==HandStatusleKey.NORMAL&&StringUtil.getInt(porder.getString("product_status"))==ProductStatusKey.IN_STORE )
						{
							//旧仓库撕毁
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY};
							taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_RECORD, t_operationsA, oid, t_note, t_target_admin, t_target_group,oldOrder.get("ps_id", 0l));
							
							//新仓库重打
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_RE_PRINT};
							taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_RECORD, t_operationsA, oid, t_note, t_target_admin, t_target_group,ps_id);
						}
						else
						{
							//旧仓库撕毁
							t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY};
							taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_RECORD, t_operationsA, oid, t_note, t_target_admin, t_target_group,oldOrder.get("ps_id", 0l));
						}
					}
				}
	
				//增加相关备注
				int trace_type = 0;
				
				if ( StringUtil.getInt(request,"handle_status")==HandStatusleKey.DOUBT )
				{
					trace_type = TracingOrderKey.RECORD_DOUBT_ORDER;
				}
				else if ( StringUtil.getInt(request,"handle_status")==HandStatusleKey.DOUBT_ADDRESS )
				{
					trace_type = TracingOrderKey.RECORD_DOUBT_ADDRESS_ORDER;
				}
				else if ( StringUtil.getInt(request,"handle_status")==HandStatusleKey.DOUBT_PAY )
				{
					trace_type = TracingOrderKey.RECORD_DOUBT_PAY_ORDER;
				}
				else
				{
					//再判断是否缺货抄单
					if ( porder.get("product_status",0)==ProductStatusKey.STORE_OUT )
					{
						trace_type = TracingOrderKey.RECORD_LACKING_ORDER;
					}
					else
					{
						trace_type = TracingOrderKey.RECORD_NORMAL_ORDER;
					}
				}
				
				this.addPOrderNotePrivate(oid, record_order_note,trace_type, StringUtil.getSession(request),0);
	
				//计算订单商品成本和运费成本
	
				
				
				//根据外部参数，决定发送邮件通知类型
				//不再发邮件
	//			if ( order_error_email==1 )
	//			{
	//				sendMailForNotModel(oid);//无型号邮件
	//			}
	//			else if ( order_error_email==2 )
	//			{
	//				sendMailForDoubtModel(oid);//疑问型号邮件
	//			}
	
				//通知该订单是缺货抄单
				if (porder.get("product_status",0)==ProductStatusKey.STORE_OUT)
				{
					return(ProductStatusKey.STORE_OUT);
				}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"recordOrder",log);
		}
		
		return(HandStatusleKey.NORMAL);
	}
	
	
	/**
	 * 最新抄单
	 * @param request
	 * @throws Exception
	 */
	public void recordOrderTrue(HttpServletRequest request)
		throws Exception
	{
		AdminMgr am = new AdminMgr();
		AdminLoginBean adminLoggerBean = am.getAdminLoginBean(StringUtil.getSession(request));
		

		String record_order_note = StringUtil.getString(request,"record_order_note");
		long oid = StringUtil.getLong(request,"oid");
		long ccid = StringUtil.getLong(request,"ccid");
		long pro_id = StringUtil.getLong(request,"pro_id");
		
		String tel = StringUtil.getString(request,"");
		String handle_status = StringUtil.getString(request,"");
		String address_name = StringUtil.getString(request,"");
		String address_city = StringUtil.getString(request,"");
		String address_zip = StringUtil.getString(request,"");
		String address_street = StringUtil.getString(request,"");
		String address_state = StringUtil.getString(request,"");
		
		DBRow porder = new DBRow();
		porder.add("tel",tel);
		porder.add("handle_status",handle_status);
		porder.add("address_name",address_name);
		porder.add("address_city",address_city);
		porder.add("address_zip",address_zip);
		porder.add("address_street",address_street);
		porder.add("address_state",address_state);
		
		
		if(pro_id>0)
		{
			DBRow province = fpm.getDetailProvinceByProId(pro_id);;//从新获得发送区的区域
			porder.add("address_state",province.getString("pro_name")) ;//区域名称
		}
		
		DBRow oldOrder = fom.getDetailPOrderByOid(oid);
		DBRow oldOrderItems[] = fom.getPOrderItemsByOid(oid);
		DBRow cartProducts[] = cart.getSimpleProducts(StringUtil.getSession(request));
		
		long ps_id = productMgr.getPriorDeliveryWarehouse(ccid, pro_id);//最优发货
		//加载购物车
		cart.flush(StringUtil.getSession(request));
		cartProducts = cart.getDetailProducts();
		//清掉旧数据
		fom.delPOrderItem(oid);
		
		for (int i=0; i<cartProducts.length; i++)
		{
			DBRow orderItem = new DBRow();
			orderItem.add("oid",oid);
			orderItem.add("pid",cartProducts[i].getString("cart_pid"));
			float quantity = Float.parseFloat(cartProducts[i].getString("cart_quantity"));
			orderItem.add("iid",cartProducts[i].getString("order_item_id"));
			orderItem.add("quantity",quantity);//购买数量
			orderItem.add("wait_quantity",cartProducts[i].get("wait_quantity",0f));//代发货数量
			orderItem.add("catalog_id",cartProducts[i].get("catalog_id",0l));
			orderItem.add("unit_price",cartProducts[i].get("unit_price",0d));
			orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
			orderItem.add("weight",cartProducts[i].get("weight", 0f));
			orderItem.add("name",cartProducts[i].getString("p_name"));
			orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
			orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
			orderItem.add("lacking",ProductStatusKey.UNKNOWN);
			orderItem.add("prefer_ps_id",ps_id);
			orderItem.add("modality",1);//1代表原状代发形式
			long plan_ps_id = ps_id; 
			
			DBRow storageTransd = fpm.getStorageTransd(ps_id,cartProducts[i].get("catalog_id", 0l));
			if(storageTransd!=null)
			{
				plan_ps_id = storageTransd.get("d_sid",0l);
				orderItem.add("modality",storageTransd.get("modality_id",0));
			}
			orderItem.add("plan_ps_id",plan_ps_id);
			fom.addPOrderItemHasId(orderItem);
			
		}
		
		porder.add("product_status",ProductStatusKey.UNKNOWN);
		

		//如果PAYPAL进来的国家不对，这里需要抄国家
		if (ccid>0)
		{
			DBRow detailCountryCode = fom.getDetailCountryCodeByCcid(ccid);
			porder.add("address_country",detailCountryCode.getString("c_country"));
			porder.add("ccid",ccid);
		}


		//如果是第一次抄单，则记录抄单时间和抄单人
		if (oldOrder.getString("record_account").equals(""))
		{
			porder.add("record_account",adminLoggerBean.getAccount());
			porder.add("record_date",DateUtil.NowStr());
		}

		porder.add("quantity",cart.getSumQuantity());

		porder.add("pro_id",pro_id);

		fom.modPOrder(oid,porder);

		//增加相关备注
		int trace_type = 0;
		
		if ( StringUtil.getInt(request,"handle_status")==HandStatusleKey.DOUBT )
		{
			trace_type = TracingOrderKey.RECORD_DOUBT_ORDER;
		}
		else if ( StringUtil.getInt(request,"handle_status")==HandStatusleKey.DOUBT_ADDRESS )
		{
			trace_type = TracingOrderKey.RECORD_DOUBT_ADDRESS_ORDER;
		}
		else if ( StringUtil.getInt(request,"handle_status")==HandStatusleKey.DOUBT_PAY )
		{
			trace_type = TracingOrderKey.RECORD_DOUBT_PAY_ORDER;
		}
		else
		{
			//再判断是否缺货抄单
			if ( porder.get("product_status",0)==ProductStatusKey.STORE_OUT )
			{
				trace_type = TracingOrderKey.RECORD_LACKING_ORDER;
			}
			else
			{
				if(oldOrder.getString("record_account").equals(""))//第一次抄单，记录正常抄单
				{
					trace_type = TracingOrderKey.RECORD_NORMAL_ORDER;
				}
				else
				{
					trace_type = TracingOrderKey.RECORD_UPDATE_ORDER;//非第一次抄单，认为是修改订单
				}
					
			}
		}
		
		this.addPOrderNotePrivate(oid, record_order_note,trace_type, StringUtil.getSession(request),0);
		
		cart.clearCart(StringUtil.getSession(request));
		
		changeOrderHandle(oid);//检查是货物全上运单
	}
	
	/**
	 * 累积缺货商品
	 * @param lackingProductList
	 * @param oid
	 * @param pid
	 * @param p_name
	 * @param quantity
	 * @throws Exception 
	 * @throws  
	 */
	private void addLackingProductList(ArrayList lackingProductList,long oid,long pid,String p_name,float quantity ) 
		throws  Exception
	{
		boolean isDuplicate = false;
		long tmpPid;
		float tmpQuantity;
		DBRow tmpRow;
		
		//先检查是否有重复
		for (int i=0; i<lackingProductList.size(); i++)
		{
			tmpRow = (DBRow)lackingProductList.get(i);
			tmpPid = StringUtil.getLong(tmpRow.getString("pid"));
			tmpQuantity = StringUtil.getFloat(tmpRow.getString("quantity"));
			//重复就修改数量
			if (tmpPid==pid)
			{
				tmpQuantity += quantity;
				tmpRow.add("quantity", tmpQuantity);
				lackingProductList.set(i, tmpRow);
				isDuplicate = true;
			}
		}
		//没有重复，新增
		if (isDuplicate==false)
		{
			DBRow lackingProduct = new DBRow();
			lackingProduct.add("oid", oid);
			lackingProduct.add("pid", pid);
			lackingProduct.add("p_name", p_name);
			lackingProduct.add("quantity", quantity);
			lackingProductList.add(lackingProduct);
		}
	}

	public void modPOrderProductInfo(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request,"oid");
			String ems_id = StringUtil.getString(request,"ems_id");
			DBRow porder = new DBRow();
			porder.add("ems_id",ems_id);
			
			fom.modPOrder(oid,porder);
			
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			OrderIndexMgr.getInstance().updateIndex(oid, detailOrder.getString("address_name"), detailOrder.getString("client_id"), detailOrder.getString("txn_id"), detailOrder.getString("auction_buyer_id"), detailOrder.getString("item_number"), detailOrder.getString("ems_id"), detailOrder.getString("address_country"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modPOrderProductInfo",log);
		}
	}

	public void modPOrderDeliveryInfo(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request,"oid");//递送信息修改，不修改国家。国家通过单独函数修改
			
			long pro_id = StringUtil.getLong(request,"pro_id");
			String address_state = StringUtil.getString(request,"address_state");//区域缩写
			String tel = StringUtil.getString(request,"");
			String address_name = StringUtil.getString(request,"");
			String address_street = StringUtil.getString(request,"");
			String address_city = StringUtil.getString(request,"");
			String address_zip = StringUtil.getString(request,"");
			
			DBRow porder = new DBRow();
			
			porder.add("tel",tel);
			porder.add("address_name",address_name);
			porder.add("address_street",address_street);
			porder.add("address_city",address_city);
			porder.add("address_zip",address_zip);
			
			if(pro_id!=-1)//非手动输入区域信息
			{
				DBRow province = fpm.getDetailProvinceByProId(pro_id);;//从新获得发送区的区域
				address_state = province.getString("pro_name");//区域编码
			}
			
			porder.add("address_state",address_state);
			porder.add("pro_id",pro_id);
			
			fom.modPOrder(oid,porder);
			
			//判断是否进行再次验证
			DBRow rowTemp = fom.getDetailPOrderByOid(oid);
			
			//只要是地址有修改过就就再次验证
			ordersMgrQLL.addressValidateFedexJMS(oid,StringUtil.getString(request, "address_street"),StringUtil.getString(request, "address_zip"));
		 
			/////////
			
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);

			if ( detailOrder.get("handle",0)==HandleKey.PRINTED&&detailOrder.get("handle_status", 0)==HandStatusleKey.NORMAL )
			{
				AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
				
				String t_note = "";
				long t_target_admin = 0;//目标管理员0，代表发送给整个角色执行
				long t_target_group = StringUtil.getLong( systemConfig.getStringConfigValue("order_task_operator_id") );//hard code
				int t_operationsA[] = new int[]{OrderTaskKey.T_OPERATION_DESTROY,OrderTaskKey.T_OPERATION_RE_PRINT};
				
				taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_MODIFY_ADDRESS, t_operationsA, oid, t_note, t_target_admin, t_target_group,detailOrder.get("ps_id", 0));
				
				//把旧运单号清掉
//				DBRow emptyEMSId = new DBRow();
//				emptyEMSId.add("ems_id","");
//				fom.modPOrder(oid, emptyEMSId);
			}
			
			OrderIndexMgr.getInstance().updateIndex(oid, detailOrder.getString("address_name"), detailOrder.getString("client_id"), detailOrder.getString("txn_id"), detailOrder.getString("auction_buyer_id"), detailOrder.getString("item_number"), detailOrder.getString("ems_id"), detailOrder.getString("address_country"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modPOrderDeliveryInfo",log);
		}
	}
	
	public void modPOrderDeliveryInfo4LoadTest(HttpServletRequest request)
		throws Exception
	{
		this.modPOrderDeliveryInfo(request);
	}	

	/**
	 * 
	 * @param search_field  0 - 精确匹配搜索，1-模糊匹配
	 * @param value
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchOrders(int search_field,String value,int search_mode,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			
			int page_count = systemConfig.getIntConfigValue("page_count");
			row = OrderIndexMgr.getInstance().getSearchResults(value.toLowerCase(),search_mode,page_count,pc);
//			if (search_field==0)
//			{
//				String searchFields[] = new String[]{
//						"address_name",
//						"client_id",
//						"orderid",
//						"txnid",
//						"auctionbuyer",
//						"itemnumber",
//						"ems_id",
//						"country"
//						};
//				
//				row = OrderIndexMgr.getInstance().getMultiplFieldsExactSearchResults(searchFields,value,pc);
//			}
//			else
//			{
				
//				row = OrderIndexMgr.getInstance().mergeSearch("merge_field",value, pc);
//			}

			return(row);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getSearchOrders",log);
		}
	}

	public DBRow getDetailPOrderByOid(long oid)
		throws Exception
	{
		try
		{
			return(fom.getDetailPOrderByOid(oid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailPOrderByOid",log);
		}
	}
	
	public DBRow getDetailOrderDeliveryInfoByOid(long oid)
		throws Exception
	{
		try
		{
			return(fom.getDetailOrderDeliveryInfoByOid(oid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailOrderDeliveryInfoByOid",log);
		}
	}
	
	/**
	 * 打印更新运单号
	 * @param request
	 * @throws Exception
	 */
	public void printUpdateEMS(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request,"oid");
			String ems_id = StringUtil.getString(request,"ems_id");
			String mail_piece_shape = StringUtil.getString(request,"mail_piece_shape");
			
			DBRow porder = new DBRow();
			porder.add("ems_id",ems_id);
			porder.add("mail_piece_shape",mail_piece_shape);
			
			fom.modPOrder(oid,porder);
			
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			OrderIndexMgr.getInstance().updateIndex(oid, detailOrder.getString("address_name"), detailOrder.getString("client_id"), detailOrder.getString("txn_id"), detailOrder.getString("auction_buyer_id"), detailOrder.getString("item_number"), detailOrder.getString("ems_id"), detailOrder.getString("address_country"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"printUpdateEMS",log);
		}
	}
	
	/**
	 * 打印更新运单号
	 * @param request
	 * @throws Exception
	 */
	public void printUpdateWayBillNumber(HttpServletRequest request)
		throws Exception
	{
		try
		{
			
			long oid = StringUtil.getLong(request,"oid");
			String waybill_no = StringUtil.getString(request, "waybill_no");
			String mail_piece_shape = StringUtil.getString(request, "mail_piece_shape");
			
			DBRow porder = new DBRow();
			porder.add("ems_id", waybill_no);
			porder.add("mail_piece_shape", mail_piece_shape);
			
			fom.modPOrder(oid,porder);
			
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			OrderIndexMgr.getInstance().updateIndex(oid, detailOrder.getString("address_name"), detailOrder.getString("client_id"), detailOrder.getString("txn_id"), detailOrder.getString("auction_buyer_id"), detailOrder.getString("item_number"), detailOrder.getString("ems_id"), detailOrder.getString("address_country"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"printUpdateWayBillNumber",log);
		}
	}
	
	/**
	 * 运单上传trackingNumber
	 * @param oid
	 * @param tracking_number
	 * @throws Exception
	 */
	public void printUpdateWayBillNumber(long oid,String tracking_number)
		throws Exception
	{
		try
		{
			DBRow paraOrder = new DBRow();
			paraOrder.add("ems_id",tracking_number);
			
			fom.modPOrder(oid,paraOrder);
			
			
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);//trackingNumber累加记录后再获得一次构建索引
			
			OrderIndexMgr.getInstance().updateIndex(oid, detailOrder.getString("address_name"), detailOrder.getString("client_id"), detailOrder.getString("txn_id"), detailOrder.getString("auction_buyer_id"), detailOrder.getString("item_number"), detailOrder.getString("ems_id"), detailOrder.getString("address_country"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"printUpdateWayBillNumber",log);
		}
	}

	public void printUpdateHandle(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String print_date = DateUtil.NowStr();
			
			String printType = StringUtil.getString(request,"printType");
			long oid = StringUtil.getLong(request,"oid");
			String ems_id = StringUtil.getString(request,"ems_id");
			long sc_id = StringUtil.getLong(request,"sc_id");
			long ccid = StringUtil.getLong(request,"ccid");
			String mail_piece_shape = StringUtil.getString(request,"mail_piece_shape");
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminInfo = am.getAdminLoginBean( request.getSession(true) );
			
			DBRow porder = new DBRow();
			porder.add("ems_id",ems_id);
			porder.add("handle",HandleKey.PRINTED);
			//记录打印人和打印时间
			porder.add("print_date",print_date);
			porder.add("print_manager",adminInfo.getAccount());
			porder.add("mail_piece_shape",mail_piece_shape);
			
			//记录订单商品成本、运费成本和运输公司
			//成本为货币转换后的金额
			//把错误屏蔽一下
			try
			{
				OrderCostBean orderCostBean = this.getOrderCost(oid, sc_id, ccid);
				porder.add("product_cost", orderCostBean.getProductCost() );
				porder.add("shipping_cost",orderCostBean.getShippingCost());
				porder.add("shipping_name",orderCostBean.getExpressName());
				porder.add("total_cost",orderCostBean.getTotalCost());
				porder.add("total_weight",orderCostBean.getWeight());
				porder.add("mc_gross_rmb",orderCostBean.getRmbMcGross());
				porder.add("sc_id",sc_id);
			}
			catch (WeightOutSizeException e) 
			{
			}
			catch (WeightCrossException e) 
			{
			}
			catch (CountryOutSizeException e) 
			{
			}
			
			fom.modPOrder(oid,porder);
			
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			//如果是合并订单，把所有子订单的状态也要修改
			if ( detailOrder.get("parent_oid", 0l)==-1 )
			{
				DBRow sonOrders[] = fom.getSonOrders(oid, null);
				for (int i=0; i<sonOrders.length; i++)
				{
					DBRow sonOrder = new DBRow();
					sonOrder.add("handle",HandleKey.PRINTED);
					sonOrder.add("print_date",print_date);
					sonOrder.add("print_manager",adminInfo.getAccount());
					fom.modPOrder(sonOrders[i].get("oid", 0l),sonOrder);
				}
			}
			else
			{
				
			}

			OrderIndexMgr.getInstance().updateIndex(oid, detailOrder.getString("address_name"), detailOrder.getString("client_id"), detailOrder.getString("txn_id"), detailOrder.getString("auction_buyer_id"), detailOrder.getString("item_number"), detailOrder.getString("ems_id"), detailOrder.getString("address_country"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"printUpdateStatus",log);
		}
	}

	public void printUpdateHandleStatus(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request,"oid");
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			DBRow porder = new DBRow();
			porder.add("handle_status","1");
			porder.add("note",detailOrder.getString("note")+"\n"+"打印疑问");
			fom.modPOrder(oid,porder);			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"printUpdateHandleStatus",log);
		}
	}
	
	public void delOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request,"oid");
			DBRow oldOrder = fom.getDetailPOrderByOid(oid);
			DBRow oldOrderItems[] = fom.getPOrderItemsByOid(oid);
			
			//商品状态除了：未知（正常待抄单），都要退库存
			if ( oldOrder.get("product_status", 0)!=ProductStatusKey.UNKNOWN )
			{
				this.stockback(null,HandStatusleKey.NORMAL,0,oldOrder, oldOrderItems,StringUtil.getSession(request));
			}
			fom.delPOrder(oid);
			fom.delPOrderItem(oid);
			
			OrderIndexMgr.getInstance().deleteIndex(oid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delOrder",log);
		}
	}
	
	public String getPrintedOrderReplyMail(String business)
		throws Exception
	{
		//获得回复邮件地址
		String s = systemConfig.getStringConfigValue("sendMailReplyMail");
		String items[] = s.split("\\|");
		String replyMail;
		
		DBRow sender = new DBRow();
		for (int i=0; i<items.length; i++)
		{
			sender.add(items[i].split(":")[0],items[i].split(":")[1]);
		}
		
		if ( sender.getString(business).equals("") )
		{
			replyMail = sender.getString("default");
		}
		else
		{
			replyMail = sender.getString(business);
		}
		
		return(replyMail);
	}

	/**
	 * 存在关联订单
	 * @param request
	 * @return
	 */
	private boolean haveParentOrder(HttpServletRequest request)
	{
		String parent_txn_id = StringUtil.getString(request, "parent_txn_id");
		
		if (parent_txn_id.equals(""))
		{
			return(false);
		}
		else
		{
			return(true);
		}
	}
	
	/**
	 * 投诉订单
	 * @param request
	 * @return
	 */
	private boolean isDisputeOrder(HttpServletRequest request)
	{
		String txn_type = StringUtil.getString(request, "txn_type");
		
		if (txn_type.equals("new_case"))
		{
			return(true);			
		}
		else
		{
			return(false);
		}
	}
	
	/**
	 * 封装特殊订单那数据，用来插入到特殊订单
	 * @param request
	 * @return
	 */
	private DBRow getSpecialOrder(HttpServletRequest request)
	{
		String txn_id = StringUtil.getString(request, "txn_id");
		String parent_txn_id = StringUtil.getString(request, "parent_txn_id");
		
		String payment_status = StringUtil.getString(request,"payment_status");
		String payment_type = StringUtil.getString(request,"payment_type");
		String reason_code = StringUtil.getString(request,"reason_code");
		String case_id = StringUtil.getString(request,"case_id");
		String case_type = StringUtil.getString(request,"case_type");
		
		DBRow specialOrder = new DBRow();
		
		specialOrder.add("payment_status",payment_status);
		specialOrder.add("payment_type",payment_type);
		specialOrder.add("reason_code",reason_code);
		specialOrder.add("case_id",case_id);
		specialOrder.add("case_type",case_type);
		
		if (!parent_txn_id.equals(""))
		{
			specialOrder.add("txn_id",parent_txn_id);
		}
		else
		{
			specialOrder.add("txn_id",txn_id);
		}
		
		specialOrder.add("post_date", DateUtil.NowStr());
		
		return(specialOrder);
	}
	
	/**
	 * 把主订单标记为有投诉
	 * 一边飙红
	 * @param request
	 * @throws Exception
	 */
	private void markDisputeOrder(HttpServletRequest request)
		throws Exception
	{
		String txn_id = StringUtil.getString(request, "txn_id");
		
		try
		{
			DBRow order = new DBRow();
			order.add("dispute_flag", 1);
			
			fom.modPOrder(txn_id, order);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"markDisputeOrder",log);
		}
	}
	
	/**
	 * 把订单状态修改为最后的状态
	 * @param request
	 * @throws Exception
	 */
	private void updateOrderPaymentStatus(String txn_id,String payment_status)
		throws Exception
	{	
		try
		{
			DBRow order = new DBRow();
			order.add("payment_status", payment_status);
			
			fom.modPOrder(txn_id, order);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateOrderPaymentStatus",log);
		}
	}

	/**
	 * 通过txn_id获得关联订单
	 * @param txn_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRelationOrders(String txn_id)
		throws Exception
	{
		try
		{
			return(fom.getRelationOrders(txn_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getRelationOrders",log);
		}
	}
	
	public String uploadOrderAppend(HttpServletRequest request,String filename)
		throws Exception
	{
		String msg = "";
	
		try
		{	
			String permitFile = "doc,xls";
			
			TUpload upload = new TUpload();
			
			upload.setFileName(filename);
			upload.setRootFolder("/administrator/order_append/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			if (flag==2)
			{
				msg = "只能上传:"+permitFile;
			}
			else if (flag==1)
			{
				msg = "上传出错";
			}
			else
			{
				msg = upload.getFileName();
				
				//给主订单做附件标志
				DBRow order = new DBRow();
				order.add("append_name",msg);
				fom.modPOrder(StringUtil.getLong(filename), order);
			}
			
			return(msg);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"uploadOrderAppend",log);
		}
	}
	
	public void delOrderAppend(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long oid = StringUtil.getLong(request, "oid");
			String append_name = StringUtil.getString(request, "append_name");
			
			FileUtil.delFile(Environment.getHome().replace("\\", "/") + "."	+ "/administrator/order_append/" + append_name);
			DBRow order = new DBRow();
			order.add("append_name", "");
			fom.modPOrder(oid, order);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"delOrderAppend",log);
		}		
	}
	
	public DBRow getStatOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try 
		{
			DBRow out = new DBRow();
			DBRow orders[] = fom.getStatOrdersCount( st, en, business, product_type_int);
			for (int i=0; i<orders.length; i++)
			{
				out.add(orders[i].getString("d"), orders[i].get("c",0));
			}
			
			return(out);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getStatOrdersCount",log);
		}		
	}

	public DBRow getStatNormalOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try 
		{
			DBRow out = new DBRow();
			DBRow orders[] = fom.getStatNormalOrdersCount( st, en, business, product_type_int);
			for (int i=0; i<orders.length; i++)
			{
				out.add(orders[i].getString("d"), orders[i].get("c",0));
			}
			
			return(out);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getStatNormalOrdersCount",log);
		}		
	}
	
	public DBRow getStatPendingOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try 
		{
			DBRow out = new DBRow();
			DBRow orders[] = fom.getStatPendingOrdersCount( st, en, business, product_type_int);
			for (int i=0; i<orders.length; i++)
			{
				out.add(orders[i].getString("d"), orders[i].get("c",0));
			}
			return(out);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getStatPendingOrdersCount",log);
		}		
	}

	public DBRow getStatArchiveOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try 
		{
			DBRow out = new DBRow();
			DBRow orders[] = fom.getStatArchiveOrdersCount( st, en, business, product_type_int);
			for (int i=0; i<orders.length; i++)
			{
				out.add(orders[i].getString("d"), orders[i].get("c",0));
			}
			return(out);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getStatArchiveOrdersCount",log);
		}		
	}
	
	public DBRow getStatPrintedOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try 
		{
			DBRow out = new DBRow();
			DBRow orders[] = fom.getStatPrintedOrdersCount( st, en, business, product_type_int);
			for (int i=0; i<orders.length; i++)
			{
				out.add(orders[i].getString("d"), orders[i].get("c",0));
			}
			return(out);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getStatPrintedOrdersCount",log);
		}		
	}
	
	
	public DBRow getStatDeliveryOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try 
		{
			DBRow out = new DBRow();
			DBRow orders[] = fom.getStatDeliveryOrdersCount( st, en, business, product_type_int);
			for (int i=0; i<orders.length; i++)
			{
				out.add(orders[i].getString("d"), orders[i].get("c",0));
			}
			return(out);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getStatDeliveryOrdersCount",log);
		}		
	}
	
	public DBRow getStatNotPrintOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try 
		{
			DBRow out = new DBRow();
			DBRow orders[] = fom.getStatNotPrintOrdersCount( st, en, business, product_type_int);
			for (int i=0; i<orders.length; i++)
			{
				out.add(orders[i].getString("d"), orders[i].get("c",0));
			}
			return(out);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getStatNotPrintOrdersCount",log);
		}		
	}
	
	public DBRow[] getTimeRangeOrders(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try
		{
			return(fom.getTimeRangeOrders( st, en, business, product_type_int));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getTimeRangeOrders",log);
		}
	}
	
	public DBRow[] getAllInvoiceTemplates()
		throws Exception
	{
		try
		{
			return(fom.getAllInvoiceTemplates());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllInvoiceTemplates",log);
		}
	}
	
	private String getInvoiceType(String sc,int s)
	{	 
			 String a[] = sc.split(",");
			 
			 for (int i=0; i<a.length; i++)
			 {
				 String b[] = a[i].split("=");
					 
				 if ( Integer.parseInt(b[1])==s )
				 {
					 return(b[0]);
				 }
			 }
			 
			 return("");
	}
	
	public DBRow[] getInvoiceTemplatesByPsId(long ps_id)
		throws Exception
	{
		try
		{
			DBRow rows[] = fom.getInvoiceTemplatesByPsId(ps_id);
				
			return(rows);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getInvoiceTemplatesByPsId",log);
		}
	}

	public void addDelivererInfo(HttpServletRequest request)
		throws Exception
	{
		
		try
		{
			String CompanyName = StringUtil.getString(request, "CompanyName");
			String AddressLine1 = StringUtil.getString(request, "AddressLine1");
			String AddressLine2 = StringUtil.getString(request, "AddressLine2");
			String AddressLine3 = StringUtil.getString(request, "AddressLine3");
			String City = StringUtil.getString(request, "City");
			String DivisionCode = StringUtil.getString(request, "DivisionCode");
			String PostalCode = StringUtil.getString(request, "PostalCode");
			String CountryCode = StringUtil.getString(request, "CountryCode");
			String CountryName = StringUtil.getString(request, "CountryName");
			String PhoneNumber = StringUtil.getString(request, "PhoneNumber");
			
			DBRow delivererInfo = new DBRow();
			
			delivererInfo.add("CompanyName", CompanyName);
			delivererInfo.add("AddressLine1", AddressLine1);
			delivererInfo.add("AddressLine2", AddressLine2);
			delivererInfo.add("AddressLine3", AddressLine3);
			delivererInfo.add("City", City);
			delivererInfo.add("DivisionCode", DivisionCode);
			delivererInfo.add("PostalCode", PostalCode);
			delivererInfo.add("CountryCode", CountryCode);
			delivererInfo.add("CountryName", CountryName);
			delivererInfo.add("PhoneNumber", PhoneNumber);
			
			fom.addDelivererInfo(delivererInfo);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addDelivererInfo",log);
		}
	}
	
	public void modDelivererInfo(HttpServletRequest request)
		throws Exception
	{
		
		try
		{
			long di_id = StringUtil.getLong(request, "di_id");
			
			String CompanyName = StringUtil.getString(request, "CompanyName");
			String AddressLine1 = StringUtil.getString(request, "AddressLine1");
			String AddressLine2 = StringUtil.getString(request, "AddressLine2");
			String AddressLine3 = StringUtil.getString(request, "AddressLine3");
			String City = StringUtil.getString(request, "City");
			String DivisionCode = StringUtil.getString(request, "DivisionCode");
			String PostalCode = StringUtil.getString(request, "PostalCode");
			String CountryCode = StringUtil.getString(request, "CountryCode");
			String CountryName = StringUtil.getString(request, "CountryName");
			String PhoneNumber = StringUtil.getString(request, "PhoneNumber");
			
			DBRow delivererInfo = new DBRow();
			
			delivererInfo.add("CompanyName", CompanyName);
			delivererInfo.add("AddressLine1", AddressLine1);
			delivererInfo.add("AddressLine2", AddressLine2);
			delivererInfo.add("AddressLine3", AddressLine3);
			delivererInfo.add("City", City);
			delivererInfo.add("DivisionCode", DivisionCode);
			delivererInfo.add("PostalCode", PostalCode);
			delivererInfo.add("CountryCode", CountryCode);
			delivererInfo.add("CountryName", CountryName);
			delivererInfo.add("PhoneNumber", PhoneNumber);
			
			fom.modDelivererInfo(di_id,delivererInfo);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modDelivererInfo",log);
		}
	}
	
	public void delDelivererInfo(HttpServletRequest request)
		throws DeliveryInfoBeUsedException,Exception
	{
		
		try
		{
			long di_id = StringUtil.getLong(request, "di_id");
			
			if (fom.getInvoiceTemplatesByDiid(di_id, null).length>0)
			{
				throw new DeliveryInfoBeUsedException();
			}
			
			fom.delDelivererInfo(di_id);
		} 
		catch (DeliveryInfoBeUsedException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delDelivererInfo",log);
		}
	}

	public DBRow[] getAllDelivererInfo()
		throws Exception
	{
		try
		{
			return(fom.getAllDelivererInfo());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllDelivererInfo",log);
		}
	}
	
	public DBRow getRandomDelivererInfo(long ps_id)
		throws Exception
	{
		try
		{
			DBRow rows[] = fom.getAllDelivererInfoByPsid(ps_id);//fom.getAllDelivererInfoByRandGroup( rand_group);
			
			Random rand = new Random();
			int index = rand.nextInt(rows.length);
			
			return(rows[index]);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getRandomDelivererInfo",log);
		}
	}

	public DBRow getRandomDelivererInfoByPsId(long ps_id)
		throws Exception
	{
		try
		{
			DBRow rows[] = fom.getAllDelivererInfoByPsid( ps_id);
			
			Random rand = new Random();
			int index = rand.nextInt(rows.length);
			
			return(rows[index]);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getRandomDelivererInfoByPsId",log);
		}
	}
	
	public void addInvoiceTemplates(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long invoice_id = StringUtil.getLong(request, "invoice_id");
			String dog = StringUtil.getString(request, "dog");
			String rfe = StringUtil.getString(request, "rfe");
			String uv = StringUtil.getString(request, "uv");
			String tv = StringUtil.getString(request, "tv");
			
			DBRow invoiceTemplates = new DBRow();
			
			invoiceTemplates.add("dog", dog);
			invoiceTemplates.add("rfe", rfe);
			invoiceTemplates.add("uv", uv);
			invoiceTemplates.add("tv", tv);
			invoiceTemplates.add("invoice_id", invoice_id);
			
			fom.addInvoiceTemplates(invoiceTemplates);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addInvoiceTemplates",log);
		}
	}

	public void modInvoiceTemplates(HttpServletRequest request)
		throws Exception
	{
		
		try
		{
			String invoice_ids[] = request.getParameterValues("invoice_ids");
			
			for (int i=0; invoice_ids!=null&&i<invoice_ids.length; i++)
			{
				DBRow newRow = new DBRow();
				
				newRow.add("dog",StringUtil.getString(request, "dog_"+invoice_ids[i]));
				newRow.add("rfe",StringUtil.getString(request, "rfe_"+invoice_ids[i]));
				newRow.add("uv",StringUtil.getString(request, "uv_"+invoice_ids[i]));
				newRow.add("tv",StringUtil.getString(request, "tv_"+invoice_ids[i]));
				newRow.add("di_id",StringUtil.getString(request, "di_id_"+invoice_ids[i]));
				newRow.add("ps_id",StringUtil.getString(request, "ps_id_"+invoice_ids[i]));
				
				fom.modInvoiceTemplates(Long.parseLong(invoice_ids[i]), newRow);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modInvoiceTemplates",log);
		}
	}

	public void delInvoiceTemplatesByInvoiceId(HttpServletRequest request)
		throws InvoiceTemplateBeUsedException,Exception
	{
		try
		{
			long invoice_id = StringUtil.getLong(request,"invoice_id");	
			
			//先检查发票模板是否已经被使用了
			if (fom.getOrdersByInvoiceId(invoice_id, null).length>0)
			{
				throw new InvoiceTemplateBeUsedException();
			}
			
			fom.delInvoiceTemplatesByInvoiceId(invoice_id);
		} 
		catch (InvoiceTemplateBeUsedException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delInvoiceTemplatesByInvoiceId",log);
		}
	}
	
	public DBRow getDetailDelivererInfo(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long di_id = StringUtil.getLong(request, "di_id");
			return(fom.getDetailDelivererInfo(di_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailDelivererInfo",log);
		}
	}
	
	public DBRow getDetailDelivererInfo(long di_id)
		throws Exception
	{
		try
		{
			return(fom.getDetailDelivererInfo(di_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailDelivererInfo",log);
		}
	}
	
	public DBRow getDetailInvoiceTemplate(long invoice_id)
		throws Exception
	{
		try
		{
			return(fom.getDetailInvoiceTemplate(invoice_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailInvoiceTemplate",log);
		}
	}

	public void updateOrderSource()
		throws Exception
	{
		
		try
		{
			fom.updateOrderSource();
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateOrderSource",log);
		}
	}
	
	public HashMap getAllCountryCodeHM()
		throws Exception
	{
		
		try
		{
			HashMap hm = new HashMap();
			DBRow country[] = fom.getAllCountryCode();
			
			for (int i=0; i<country.length; i++)
			{
				hm.put(country[i].getString("c_country").toLowerCase(), country[i].getString("c_code"));
			}
			
			return(hm);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllCountryCodeHM",log);
		}
	}
		
	public DBRow[] getAllCountryCode()
		throws Exception
	{
		try
		{
			return(fom.getAllCountryCode());
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllCountryCode",log);
		}
	}

	public DBRow getDetailCountryCodeByCcid(long ccid)
		throws Exception
	{
		try
		{
			return(fom.getDetailCountryCodeByCcid(ccid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailCountryCodeByCcid",log);
		}
	}
	
	public DBRow getDetailCountryCodeByCountry(String country)
		throws Exception
	{
		try
		{
			return(fom.getDetailCountryCodeByCountry(country));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailCountryCodeByCountry",log);
		}
	}
	
	//转换国家代码
	public void convertCountryCode()
		throws Exception
	{
		fom.convertCountryCode();
	}
	
	/**
	 * 添加订单跟进记录
	 * @param request
	 * @throws Exception
	 */
	public void addPOrderNote(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			int trace_type = StringUtil.getInt(request, "trace_type");
			String note = StringUtil.getString(request, "note");
			addPOrderNotePrivate(oid,note,trace_type,StringUtil.getSession(request),0);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addPOrderNote",log);
		}
	}
	

	/**
	 * 增加订单跟进记录公共类
	 * @param oid
	 * @param note
	 * @param trace_type  跟踪类型
	 * @param ses
	 * @param rel_id      相关信息连接ID
	 * @throws Exception
	 */
	public void addPOrderNotePrivate(long oid,String note,int trace_type,HttpSession ses,long rel_id)
		throws Exception
	{
		try
		{
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(ses);
			if (adminLoggerBean==null)
			{
				adminLoggerBean = new AdminLoginBean();
				adminLoggerBean.setAdgid(0);
				adminLoggerBean.setAdid(0);
				adminLoggerBean.setAccount("");
			}
			
			DBRow porder = fom.getDetailPOrderByOid(oid);

			DBRow delivererInfo = new DBRow();

			delivererInfo.add("oid", oid);
			delivererInfo.add("note", note);
			delivererInfo.add("account", adminLoggerBean.getAccount());
			delivererInfo.add("adid", adminLoggerBean.getAdid());
			delivererInfo.add("trace_type", trace_type);
			delivererInfo.add("post_date", DateUtil.NowStr());
			delivererInfo.add("rel_id", rel_id);
			delivererInfo.add("trace_child_type",porder.get("internal_tracking_status",0));

			fom.addPOrderNote(delivererInfo);
			
			//记录跟进人
			DBRow paraOrder = new DBRow();
			paraOrder.add("trace_type",trace_type);
			paraOrder.add("trace_date",DateUtil.NowStr());
			paraOrder.add("trace_operator_role_id",adminLoggerBean.getAdgid());
			paraOrder.add("trace_pre_operator_role_id",porder.get("trace_operator_role_id",0l));
			fom.modPOrder(oid,paraOrder);
			
			
			//检测跟踪任务
			this.validateTraceService(adminLoggerBean.getAdgid(),oid,trace_type);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addPOrderNotePrivate",log);
		}
	}
	
	
	public void addPOrderNotePrivate(long oid,String note,int trace_type,AdminLoginBean adminLoggerBean,long rel_id)
		throws Exception
	{
		try
		{
//			AdminMgr adminMgr = new AdminMgr();
//			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(ses);
			if (adminLoggerBean==null)
			{
				adminLoggerBean = new AdminLoginBean();
				adminLoggerBean.setAdgid(0);
				adminLoggerBean.setAdid(0);
				adminLoggerBean.setAccount("");
			}
	
			DBRow delivererInfo = new DBRow();
	
			delivererInfo.add("oid", oid);
			delivererInfo.add("note", note);
			delivererInfo.add("account", adminLoggerBean.getAccount());
			delivererInfo.add("adid", adminLoggerBean.getAdid());
			delivererInfo.add("trace_type", trace_type);
			delivererInfo.add("post_date", DateUtil.NowStr());
			delivererInfo.add("rel_id", rel_id);
	
			fom.addPOrderNote(delivererInfo);
			
			//检测跟踪任务
			this.validateTraceService(adminLoggerBean.getAdgid(),oid,trace_type);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addPOrderNotePrivate",log);
		}
	}

	public DBRow[] getPOrderNoteByOid(long oid)
		throws Exception
	{
		try
		{
			return(fom.getPOrderNoteByOid(oid,null));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPOrderNoteByOid",log);
		}
	}
	
	/**
	 * 修改递送信息的国家信息
	 * @param request
	 * @throws Exception
	 */
	public void modDeliveryCountry(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request,"oid");
			long ccid = StringUtil.getLong(request,"ccid");
			
			DBRow detailCountryCode = fom.getDetailCountryCodeByCcid(ccid);
			
			DBRow porder = new DBRow();
			
			porder.add("ccid",ccid);
			porder.add("address_country",detailCountryCode.getString("c_country"));
			
			fom.modPOrder(oid,porder);
	
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			OrderIndexMgr.getInstance().updateIndex(oid, detailOrder.getString("address_name"), detailOrder.getString("client_id"), detailOrder.getString("txn_id"), detailOrder.getString("auction_buyer_id"), detailOrder.getString("item_number"), detailOrder.getString("ems_id"), detailOrder.getString("address_country"));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modDeliveryCountry",log);
		}
	}

	public int getOrderCountByClientId(String client_id)
		throws Exception
	{
		try
		{
			
			return(fom.getOrderCountByClientId(client_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderCountByClientId",log);
		}
	}

	public double getSumMcGrossByClientId(String client_id)
		throws Exception
	{
		try
		{
			
			return(fom.getSumMcGrossByClientId(client_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSumMcGrossByPayerEmail",log);
		}
	}

	public DBRow[] getPOrderItemsByOid(long oid)
		throws Exception
	{
		try
		{
			return(fom.getPOrderItemsByOid(oid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPOrderItemsByOid",log);
		}
	}
	
	public void initCartFromOrder(HttpServletRequest request,long oid)
		throws Exception
	{
		try
		{
			cart.clearCart(StringUtil.getSession(request));
			customCart.clearCart(StringUtil.getSession(request));
			
			DBRow orderItems[] = fom.getPOrderItemsByOid(oid);
			for (int i=0; i<orderItems.length; i++)
			{
				cart.put2Cart(StringUtil.getSession(request),orderItems[i].get("iid",0l),orderItems[i].get("pid", 0l), orderItems[i].get("quantity", 0f),orderItems[i].get("product_type", 0),orderItems[i].get("wait_quantity",0f));
				//如果是定制商品，还需要初始化定制商品的session
				if (orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_CUSTOM)
				{
					DBRow inSetProducts[] = fpm.getProductsCustomInSetBySetPid(orderItems[i].get("pid", 0l));
					for (int j=0; j<inSetProducts.length; j++)
					{
						customCart.put2Cart(StringUtil.getSession(request), orderItems[i].get("pid", 0l), inSetProducts[j].get("pc_id", 0l), inSetProducts[j].get("quantity", 0f));
					}
				}
			}
			
			customCart.copy2FianlSession(StringUtil.getSession(request));
			
			//订单明细为空或者明细数组长度为0，根据COI添加Session购物车
			if ((orderItems ==null||orderItems.length==0)&&oid!=0)
			{
				DBRow trade = tradeMgrZJ.getCreateOrderTradeByOid(oid);
				
				if (trade!=null)
				{
					DBRow[] tradeItems = tradeMgrZJ.getTradeItemsByTradeId(trade.get("trade_id",0l));
					
					for (int i = 0; i < tradeItems.length; i++) 
					{
						String mpn = tradeItems[i].getString("mpn");
						String sku = tradeItems[i].getString("sku");
						
						boolean canSearchProduct = true;
						if (!mpn.equals(""))
						{
							String[] pcodes = mpn.split(",");
							for (int j = 0; j < pcodes.length; j++) 
							{
								DBRow product = productMgr.getDetailProductByPname(pcodes[j]);
								
								//COI根据商品名无法找到商品，按照商品条码再搜索一次
								 //商品条码可不惟一 zyj
							/*	if(product == null)
								{
									product = productMgr.getDetailProductByPcode(pcodes[j]);//(ZJ)
								}*/
								
								//COI未查到商品或查到商品是不许抄单，清空购物车，整张订单的所有COI商品不添加
								if(product == null)
								{
									canSearchProduct = false;
									
									//记录MPN未找到对应商品的条码
									DBRow codeMiss = new DBRow();
									codeMiss.add("miss_oid",oid);
									codeMiss.add("miss_code",pcodes[j]);
									
									fom.addCodeMiss(codeMiss);
								}
							}
							
							if (canSearchProduct)
							{
								tradeItems[i].add("use","mpn");
							}
						}
						
						if(!sku.equals("")&&canSearchProduct==false)
						{
							String[] pcodes = sku.split(",");
							canSearchProduct = true;
							
							for (int j = 0; j < pcodes.length; j++)
							{
								DBRow product = productMgr.getDetailProductByPname(pcodes[j]);
								
								//COI根据商品名无法找到商品，按照商品条码再搜索一次
								 //商品条码可不惟一 zyj
							/*	if(product == null)
								{
									product = productMgr.getDetailProductByPcode(pcodes[j]);//(ZJ)
								}*/
								
								if(product == null)
								{
									canSearchProduct = false;
									
									//MPN无法使用，使用SKU时，记录SKU分割后找不到对应商品的条码
									DBRow codeMiss = new DBRow();
									codeMiss.add("miss_oid",oid);
									codeMiss.add("miss_code",pcodes[j]);
									
									fom.addCodeMiss(codeMiss);
								}
							}
							
							if (canSearchProduct)
							{
								tradeItems[i].add("use","sku");
							}
						}
					}
					
					//判断是否可以添加购物车
					boolean canSaveCart = true;
					for (int i = 0; i < tradeItems.length; i++) 
					{
						String use = tradeItems[i].getString("use");
						String mpn = tradeItems[i].getString("mpn");
						String sku = tradeItems[i].getString("sku");
						
						String[] pcodes = null;
						if(use.equals("mpn"))
						{
							pcodes = mpn.split(",");
						}
						else if(use.equals("sku"))
						{
							pcodes = sku.split(",");
						}
						else
						{
							canSaveCart = false;
						}
						
						if(canSaveCart)
						{
							for (int j = 0; j < pcodes.length; j++) 
							{
								DBRow product = fpm.getDetailProductByPname(pcodes[j]);
								 //商品条码可不惟一 zyj	
								/*if(product == null)
								{
									product = fpm.getDetailProductByPcode(pcodes[j]);//(ZJ)
								}*/
								
								if (product==null||product.get("alive", 0)==0)
								{
									canSaveCart = false;
								}
								else
								{
									float length = product.get("length",0f);
									float width = product.get("width",0f);
									float heigth = product.get("heigth",0f);
									float weight = product.get("weight",0f);
									
									double unit_price = product.get("unit_price",0d);
									
									if(!(length>0&&width>0&&heigth>0&&unit_price>0&&weight>0))
									{
										canSaveCart = false;
									}
								}
							}
						
						}
					}
					
					if (canSaveCart==true) 
					{
						for (int i = 0; i < tradeItems.length; i++) 
						{
							String use = tradeItems[i].getString("use");
							String mpn = tradeItems[i].getString("mpn");
							String sku = tradeItems[i].getString("sku");
							float quantity = tradeItems[i].get("quantity",0f);
							
							String[] pcodes = null;
							if(use.equals("mpn"))
							{
								pcodes = mpn.split(",");
							}
							else if(use.equals("sku"))
							{
								pcodes = sku.split(",");
							}
							
							for (int j = 0; j < pcodes.length; j++) 
							{
								DBRow product = fpm.getDetailProductByPname(pcodes[j]);
								 //商品条码可不惟一 zyj
							/*	if(product == null)
								{
									product = fpm.getDetailProductByPcode(pcodes[j]);//(ZJ)
								}
								*/
								int product_type = 0;
								
								if (product.get("union_flag", 0)==0)
								{
									product_type = ProductTypeKey.NORMAL;
								}
								else
								{
									product_type = ProductTypeKey.UNION_STANDARD;
								}
								cart.put2Cart(StringUtil.getSession(request),0,product.get("pc_id",0l),quantity,product_type,quantity);
							}
						}
					}
				
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"initCartFromOrder",log);
		}
	}

	/**
	 * 合并订单
	 * @param parent_oid
	 * @param oid
	 * @throws Exception
	 */
	public void mergeOrder(HttpServletRequest request)
		throws MergeOrderNotCancelException,LackingOrderCanBeMergePrintedOrderException,MergeOrderStatusConflictException,MergeOrderNotSelfException,MergeOrderNotExistException,MergeOrderStatusIncorrectException,Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			long parent_oid = StringUtil.getLong(request, "parent_oid");
			long oid = StringUtil.getLong(request, "oid");
			
			if (parent_oid==oid)
			{
				throw new MergeOrderNotSelfException();
			}

			DBRow parentOrder = fom.getDetailPOrderByOid(parent_oid);
			DBRow order = fom.getDetailPOrderByOid(oid);

			if (parentOrder==null)
			{
				throw new MergeOrderNotExistException();
			}
			
			//发货仓库不同，不能合并
			if (parentOrder.get("ps_id",0l)!=order.get("ps_id",0l))
			{
				throw new MergeOrderStorageNotMatchException();
			}

			//不能把已打印订单合并到待打印订单
			if (parentOrder.get("handle",0)==HandleKey.WAIT4_PRINT&&order.get("handle",0)==HandleKey.PRINTED)
			{
				throw new MergeOrderStatusConflictException();
			}

			//不能把缺货订单合并到已打印订单上
			if (parentOrder.get("handle",0)==HandleKey.PRINTED&&order.get("product_status",0)==ProductStatusKey.STORE_OUT)
			{
				throw new LackingOrderCanBeMergePrintedOrderException();
			}

			/**
			 * 能合并订单状态：
			 * 订单属性：正常
			 * 货物状态：有货或缺货
			 * 处理状态：待打印、已打印
			 */
			if ( parentOrder.get("handle_status",0)==HandStatusleKey.NORMAL&&parentOrder.get("product_status",0)!=ProductStatusKey.UNKNOWN&&parentOrder.get("handle",0)!=HandleKey.DELIVERIED&&parentOrder.get("handle",0)!=HandleKey.WAIT4_RECORD && order.get("handle_status",0)==HandStatusleKey.NORMAL&&order.get("product_status",0)!=ProductStatusKey.UNKNOWN&&order.get("handle",0)!=HandleKey.DELIVERIED&&order.get("handle",0)!=HandleKey.WAIT4_RECORD )	
			{
				//还要修改为归档状态
				DBRow sonOrder = new DBRow();
				sonOrder.add("parent_oid",parent_oid);
				sonOrder.add("handle_status",HandStatusleKey.ARCHIVE);
				fom.modPOrder(oid, sonOrder);

				/**
				 * 父订单做一个标记
				 * <0代表含有子订单，-1有子订单，-2子订单缺货
				 */
				DBRow fatherOrder = new DBRow();
				if (order.get("product_status",0)==ProductStatusKey.STORE_OUT)
				{
					fatherOrder.add("parent_oid",-2);
				}
				else
				{
					//原来已经包含缺货子订单，就不要改变状态
					if (parentOrder.get("parent_oid",0)!=-2)
					{
						fatherOrder.add("parent_oid",-1);
					}
				}
				fom.modPOrder(parent_oid, fatherOrder);

				//如果是已打印订单，需要触发 订单合并任务
				if ( parentOrder.get("handle",0)==HandleKey.PRINTED&&parentOrder.get("handle_status", 0)==HandStatusleKey.NORMAL )
				{
					
					String t_note = "";
					long t_target_admin = 0;//目标管理员0，代表发送给整个角色执行
					long t_target_group = StringUtil.getLong( systemConfig.getStringConfigValue("order_task_operator_id") );//hard code
					int t_operationsA[];
					
					//父有货撕单重打，没货撕单
					if (parentOrder.get("parent_oid", 0)!=-2&&fatherOrder.get("parent_oid", 0)!=-2)
					{
						t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY,OrderTaskKey.T_OPERATION_RE_PRINT};
					}
					else
					{
						t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY};//这种情况好像不可能出现
					}
					
					taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_MERGE, t_operationsA, parent_oid, t_note, t_target_admin, t_target_group,parentOrder.get("ps_id", 0l));
				}
				//如果被合并子订单已打印，需要斯单，但是不重打（合并两个已打印订单）
				if ( order.get("handle",0)==HandleKey.PRINTED&&order.get("handle_status", 0)==HandStatusleKey.NORMAL )
				{
					
					String t_note = "";
					long t_target_admin = 0;//目标管理员0，代表发送给整个角色执行
					long t_target_group = StringUtil.getLong( systemConfig.getStringConfigValue("order_task_operator_id") );//hard code
					int t_operationsA[] = new int[]{OrderTaskKey.T_OPERATION_DESTROY};
					
					taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_MERGE, t_operationsA, oid, t_note, t_target_admin, t_target_group,order.get("ps_id", 0l));
				}

				//增加子订单备注
				this.addPOrderNotePrivate(oid, "合并订单",TracingOrderKey.OTHERS, StringUtil.getSession(request),parent_oid);
				//增加父订单备注
				this.addPOrderNotePrivate(parent_oid, "合并订单",TracingOrderKey.OTHERS, StringUtil.getSession(request),oid);
			}
			else
			{
				throw new MergeOrderStatusIncorrectException();
			}
		}
		catch (MergeOrderNotSelfException e) 
		{
			throw e;
		}
		catch (MergeOrderNotExistException e) 
		{
			throw e;
		}
		catch (MergeOrderStatusIncorrectException e) 
		{
			throw e;
		}
		catch (MergeOrderStatusConflictException e) 
		{
			throw e;
		}
		catch (LackingOrderCanBeMergePrintedOrderException e) 
		{
			throw e;
		}
		catch (MergeOrderStorageNotMatchException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"mergeOrder",log);
		}
	}

	/**
	 * 验证需要合并的两个订单的收件人是否相同
	 * @param parent_oid
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public boolean validateMergeorderDeliveryInfo(long parent_oid,long oid)
		throws Exception
	{
		try
		{
			DBRow parentOrder = fom.getDetailPOrderByOid(parent_oid);
			DBRow order = fom.getDetailPOrderByOid(oid);
			
			if (parentOrder==null)
			{
				return(false);
			}
			
			String parentOrderMD5Verify = parentOrder.getString("address_name")+parentOrder.getString("address_street")+parentOrder.getString("address_city")+parentOrder.getString("address_zip")+parentOrder.getString("tel");
			String orderMD5Verify = order.getString("address_name")+order.getString("address_street")+order.getString("address_city")+order.getString("address_zip")+order.getString("tel");
			
			if (parentOrderMD5Verify.equals(orderMD5Verify))
			{
				return(true);
			}
			else
			{
				return(false);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"validateMergeorderDeliveryInfo",log);
		}
		
	}
	
	/**
	 * 获得子订单所有商品
	 * @param parent_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSonOrderItemsByParentOid(long parent_oid)
		throws Exception
	{
		try
		{
			return(fom.getSonOrderItemsByParentOid(parent_oid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSonOrderItemsByParentOid",log);
		}
	}
	
	/**
	 * 获得所有子订单配送备注
	 * @param parent_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSonOrderDeliveryNoteByParentOid(long parent_oid)
		throws Exception
	{
		try
		{
			return(fom.getSonOrderDeliveryNoteByParentOid(parent_oid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSonOrderDeliveryNoteByParentOid",log);
		}
	}

	/**
	 * 取消合并
	 * @param request
	 * @throws Exception
	 */
	public void undoMergeOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			long parent_oid = StringUtil.getLong(request, "parent_oid");
			long oid = StringUtil.getLong(request, "oid");
			
			DBRow sonOrder = new DBRow();
			sonOrder.add("parent_oid",0);
			sonOrder.add("handle_status",HandStatusleKey.NORMAL);
			fom.modPOrder(oid, sonOrder);
			
			//如果父订单没有子订单了，则把父订单标记消除
			DBRow sonOrders[] = fom.getSonOrders(parent_oid, null);
			boolean haveLackingOrder = false;
			for (int i=0; i<sonOrders.length; i++)
			{
				if (sonOrders[i].get("product_status", 0)==ProductStatusKey.STORE_OUT)
				{
					haveLackingOrder = true;
				}
			}
			
			if (sonOrders.length==0)
			{
				DBRow fatherOrder = new DBRow();
				fatherOrder.add("parent_oid",0);
				fom.modPOrder(parent_oid, fatherOrder);
			}
			else if (!haveLackingOrder)//没有包含缺货子订单
			{
				DBRow fatherOrder = new DBRow();
				fatherOrder.add("parent_oid",-1);
				fom.modPOrder(parent_oid, fatherOrder);
			}

			DBRow parentOrder = fom.getDetailPOrderByOid(parent_oid);
			//父订单已打印，需要触发任务
			if ( parentOrder.get("handle",0)==HandleKey.PRINTED&&parentOrder.get("handle_status", 0)==HandStatusleKey.NORMAL )
			{
				//假如父订单已经打印，触发订单任务～
				String t_note = "";
				long t_target_admin = 0;//目标管理员0，代表发送给整个角色执行
				long t_target_group = StringUtil.getLong( systemConfig.getStringConfigValue("order_task_operator_id") );//hard code
				int t_operationsA[];
				
				//父有货撕单重打，没货撕单
				if (parentOrder.get("parent_oid", 0)!=-2)
				{
					t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY,OrderTaskKey.T_OPERATION_RE_PRINT};
				}
				else
				{
					t_operationsA = new int[]{OrderTaskKey.T_OPERATION_DESTROY};
				}
				
				taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_UNMERGE, t_operationsA, parent_oid, t_note, t_target_admin, t_target_group,parentOrder.get("ps_id",0l));
				
				//解合后，子订单退库存，状态变成：疑问-未知-待抄单
				//回退库存
				DBRow curOrder = fom.getDetailPOrderByOid(oid);
				DBRow curOrderItems[] = fom.getPOrderItemsByOid(oid);
				this.stockback(null,HandStatusleKey.NORMAL,0,curOrder, curOrderItems,StringUtil.getSession(request));
				//回退库存后再改订单状态，否则回退不成功
				DBRow curOrderNewStatus = new DBRow();
				curOrderNewStatus.add("handle_status", HandStatusleKey.DOUBT);
				curOrderNewStatus.add("product_status", ProductStatusKey.UNKNOWN);
				curOrderNewStatus.add("handle", HandleKey.WAIT4_RECORD);
				fom.modPOrder(oid, curOrderNewStatus);
			}

			//增加子订单备注
			this.addPOrderNotePrivate(oid, "解除合并", TracingOrderKey.OTHERS,StringUtil.getSession(request),parent_oid);
			//增加父订单备注
			this.addPOrderNotePrivate(parent_oid, "解除合并", TracingOrderKey.OTHERS,StringUtil.getSession(request),oid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"undoMergeOrder",log);
		}
	}

	/**
	 * 通过父订单号获得所有子订单
	 * @param parent_oid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSonOrders(long parent_oid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getSonOrders(parent_oid,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getSonOrders",log);
		}
	}	

	/**
	 * 取消订单
	 * @param request
	 * @throws Exception
	 */
	public void cancelOrder(HttpServletRequest request)
		throws MergeOrderNotCancelException,Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
			long oid = StringUtil.getLong(request, "oid");
			DBRow oldOrder = fom.getDetailPOrderByOid(oid);
			DBRow oldOrderItems[] = fom.getPOrderItemsByOid(oid);
			String note = StringUtil.getString(request, "note");
			
			long[] oids = new long[1];
			oids[0] = oid;
			DBRow[] wayBillOrders = floorWayBillOrderMgrZJ.getWayBillOrdersByOids(oids);
			if(wayBillOrders!=null&&wayBillOrders.length>0)
			{
				throw new OrderHasWayBillNotCancelException();
			}
			
//			//有合并订单都不能直接取消，要先取消合并
//			if ( oldOrder.get("parent_oid", 0l)!=0 )
//			{
//				throw new MergeOrderNotCancelException();
//			}
			
			//回退库存
			//this.stockback(null,HandStatusleKey.NORMAL,0,oldOrder, oldOrderItems,StrUtil.getSession(request));

			DBRow order = new DBRow();
			order.add("handle_status",HandStatusleKey.CANCEL);
			order.add("product_status",ProductStatusKey.UNKNOWN);//把货物状态标记为未知
			fom.modPOrder(oid, order);
			
//			if ( oldOrder.get("handle",0)==HandleKey.PRINTED )
//			{
//				String t_note = "";
//				long t_target_admin = 0;//目标管理员0，代表发送给整个角色执行
//				long t_target_group = StrUtil.getLong( systemConfig.getStringConfigValue("order_task_operator_id") );//hard code
//				int t_operationsA[] = new int[]{OrderTaskKey.T_OPERATION_DESTROY};
//				
//				taskMgr.createOrderTask(adminLoggerBean, OrderTaskKey.T_REASON_CNACLE, t_operationsA, oid, t_note, t_target_admin, t_target_group,oldOrder.get("ps_id",0l));
//			}
			
			//增加相关备注
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.OTHERS,StringUtil.getSession(request),0);
		}
		catch(OrderHasWayBillNotCancelException e)
		{
			throw e;
		}
		catch (MergeOrderNotCancelException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelOrder",log);
		}
	}

	/**
	 * 归档(不再使用)
	 * @param request
	 * @throws Exception
	 */
	public void archiveOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			
			DBRow order = new DBRow();
			order.add("handle_status",HandStatusleKey.ARCHIVE);
			fom.modPOrder(oid, order);
			
			//增加相关备注
			this.addPOrderNotePrivate(oid, note,TracingOrderKey.OTHERS, StringUtil.getSession(request),0);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"archiveOrder",log);
		}
	}
	
	public DBRow getDetailPOrderNoteByOnid(long on_id)
		throws Exception
	{
		try
		{
			return(fom.getDetailPOrderNoteByOnid(on_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailPOrderNoteByOnid",log);
		}
	}
	
	/**
	 * 获得待打印订单
	 * @param st
	 * @param en
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWait4PrintOrders(long ps_id,String business,long product_type,String st,String en,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getWait4PrintOrders(  ps_id,business,product_type,st, en, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getWait4PrintOrders",log);
		}
	}

	/**
	 * 
	 * @param print_date
	 * @param psid
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderProductsByDatePsidPscidCatalogid(String print_date,long psid,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> outAl = new ArrayList<DBRow>();
			
			DBRow order_products_n_unions[] = fom.getStatLeavingStorageOrderProductsByDatePsidPscidCatalogid( print_date, psid, pscid, catalog_id);
			
			//获得手工拼装商品
			DBRow union_manuals[] = fom.getStatLeavingStorageOrderStandardManualProductsByDatePsid(print_date,psid);
			for (int i=0; i<union_manuals.length; i++)
			{
				DBRow union_manual_products[] = fom.getStatLeavingStorageOrderStandardManualProductsBySetPidPscidCatalogid(union_manuals[i].get("pid", 0l),pscid,catalog_id);
				for (int j=0; j<union_manual_products.length; j++)
				{
					DBRow union_manual_product = new DBRow();
					union_manual_product.add("name",union_manual_products[j].getString("p_name"));
					union_manual_product.add("quantity",union_manual_products[j].get("quantity", 0f)*union_manuals[i].get("quantity", 0f));
					union_manual_product.add("store_station",union_manual_products[j].getString("store_station"));
					union_manual_product.add("store_count",union_manual_products[j].get("store_count",0f));
					
					outAl.add(union_manual_product);
				}
			}

			//获得定制商品
			DBRow union_customs[] = fom.getStatLeavingStorageOrderCustomProductsByDatePsid(print_date,psid);
			for (int i=0; i<union_customs.length; i++)
			{
				DBRow union_custom_products[] = fom.getStatLeavingStorageOrderCustomProductsBySetPidPscidCatalogid(union_customs[i].get("pid", 0l),pscid,catalog_id);
				for (int j=0; j<union_custom_products.length; j++)
				{
					DBRow union_custom_product = new DBRow();
					union_custom_product.add("name",union_custom_products[j].getString("p_name"));
					union_custom_product.add("quantity",union_custom_products[j].get("quantity", 0f)*union_customs[i].get("quantity", 0f));
					union_custom_product.add("store_station",union_custom_products[j].getString("store_station"));
					union_custom_product.add("store_count",union_custom_products[j].get("store_count",0f));
					
					outAl.add(union_custom_product);
				}
			}

			//重新封装所有商品数据
			for (int i=0; i<order_products_n_unions.length; i++)
			{
				outAl.add(order_products_n_unions[i]);
			}

			//合并重复商品数量
			HashMap mergeProducts = new HashMap();
			for (int i=0; i<outAl.size(); i++)
			{
				DBRow productTmp = (DBRow)outAl.get(i);
				String name = productTmp.getString("name");
				
				if ( mergeProducts.containsKey(name)==false )
				{
					mergeProducts.put(name,productTmp);
				}
				else
				{
					DBRow oldProductTmp = (DBRow)mergeProducts.get(name);
					float oldQuantity = StringUtil.getFloat(oldProductTmp.getString("quantity"));
					
					//重新合并数量
					oldProductTmp.add("quantity", oldQuantity+StringUtil.getFloat(productTmp.getString("quantity")));
					mergeProducts.put(name, oldProductTmp);
				}
			}
			
			outAl.clear();
			Iterator iter = mergeProducts.entrySet().iterator(); 
			while (iter.hasNext()) 
			{ 
				Map.Entry entry = (Map.Entry) iter.next(); 
				Object val = entry.getValue(); 
				outAl.add((DBRow)val);
			} 
			
			return( (DBRow[])outAl.toArray(new DBRow[0]) );
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getStatLeavingStorageOrderProductsByDatePsidPscidCatalogid",log);
		}
	}

	/**
	 * 
	 * @param print_date
	 * @param psid
	 * @param pscid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderPCByDatePsidPscid(String print_date,long psid,long pscid)
		throws Exception
	{
		try
		{
			ArrayList existCatalog = new ArrayList();
			ArrayList<DBRow> outAl = new ArrayList<DBRow>();
			
			//手工拼装商品
			DBRow union_manuals[] = fom.getStatLeavingStorageOrderStandardManualProductsByDatePsid(print_date,psid);
			for (int i=0; i<union_manuals.length; i++)
			{
				DBRow catalog2[] = fom.getStatLeavingStorageOrderStandardManualPCByDatePsidPscid(union_manuals[i].get("pid", 0l),pscid);
				for (int j=0; j<catalog2.length; j++)
				{
					if (!existCatalog.contains(catalog2[j].get("catalog_id",0l)))
					{
						DBRow union_manual_product_catalog = new DBRow();
						union_manual_product_catalog.add("catalog_id",catalog2[j].get("catalog_id",0l));
						outAl.add(union_manual_product_catalog);
						
						existCatalog.add(catalog2[j].get("catalog_id",0l));
					}
				}
			}

			//定制商品
			DBRow union_customs[] = fom.getStatLeavingStorageOrderCustomProductsByDatePsid(print_date,psid);
			for (int i=0; i<union_customs.length; i++)
			{
				DBRow catalog3[] = fom.getStatLeavingStorageOrderCustomPCByDatePsidPscid(union_customs[i].get("pid", 0l),pscid);
				for (int j=0; j<catalog3.length; j++)
				{
					if (!existCatalog.contains(catalog3[j].get("catalog_id",0l)))
					{
						DBRow union_manual_product_catalog = new DBRow();
						union_manual_product_catalog.add("catalog_id",catalog3[j].get("catalog_id",0l));
						outAl.add(union_manual_product_catalog);
						
						existCatalog.add(catalog3[j].get("catalog_id",0l));
					}
				}
			}

			DBRow catalog1[] = fom.getStatLeavingStorageOrderPCByDatePsidPscid( print_date, psid, pscid);
			for (int i=0; i<catalog1.length; i++)
			{
				if (!existCatalog.contains(catalog1[i].get("catalog_id",0l)))
				{
					outAl.add(catalog1[i]);
				}
			}
			
			return( (DBRow[])outAl.toArray(new DBRow[0]) );
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getStatLeavingStorageOrderPCByDatePsidPscid",log);
		}
	}
	
	/**
	 * 通过视图获得当天出库商品所属商品分类
	 * @param print_date
	 * @param psid
	 * @param pscid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderPCByDatePsidPscidView(String print_date,long psid,long pscid)
		throws Exception
	{
		try
		{
			return(fom.getStatLeavingStorageOrderPCByDatePsidPscidView(print_date, psid, pscid));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getStatLeavingStorageOrderPCByDatePsidPscidView",log);
		}
		
	}
	
	/**
	 * 通过视图获得当天出库所有商品
	 * @param print_date
	 * @param psid
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderProductsByDatePsidPscidCatalogidView(String print_date,long psid,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			return(fom.getStatLeavingStorageOrderProductsByDatePsidPscidCatalogidView( print_date, psid, pscid, catalog_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getStatLeavingStorageOrderProductsByDatePsidPscidCatalogidView",log);
		}
		
	}

	/***
	 * 获得所有缺货商品分类
	 * @param pscid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLackingProductsPCByPscid(long pscid)
		throws Exception
	{
		try
		{
			return(fom.getStatLackingProductsPCByPscid(pscid));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getStatLackingProductsPCByPscid",log);
		}
	}

	/**
	 * 分类下的缺货商品
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLackingProductsByPscidCatalogid(long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			return(fom.getStatLackingProductsByPscidCatalogid( pscid, catalog_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getStatLackingProductsByPscidCatalogid",log);
		}
	}
	
	/**
	 * 获得所有帐号待打印订单总数
	 * @param ps_id
	 * @param st
	 * @param en
	 * @return
	 * @throws Exception
	 */
	public int getWait4PrintOrdersCount(long ps_id,String st,String en)
		throws Exception
	{
		try
		{
			return(fom.getWait4PrintOrdersCount( ps_id, st, en));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWait4PrintOrdersCount",log);
		}
	}

	/**
	 * 检查订单是否打印成功
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public boolean checkOrderPrinted(HttpServletRequest request)
		throws Exception
	{
		//打印状态变成已打印，运单号已经记录
		long oid = StringUtil.getLong(request, "oid");
		
		try
		{
			DBRow detail = fom.getDetailPOrderByOid(oid);
			
			if (detail.get("handle", 0)==HandleKey.PRINTED&&detail.getString("ems_id").equals("")==false)
			{
				return(true);
			}
			else
			{
				return(false);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"checkOrderPrinted",log);
		}
		
	}
	
	
	//登录首页的统计数据
	
	
	
	
	public DBRow[] getProfileWait4HandleOrders(String st,String en)
		throws Exception
	{
		try
		{
			return(fom.getProfileWait4HandleOrders( st, en));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileWait4HandleOrders",log);
		}
	}

	public DBRow[] getProfileLackingOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfileLackingOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileLackingOrders",log);
		}
	}
		
	public DBRow[] getProfileWait4PrintOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfileWait4PrintOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileWait4PrintOrders",log);
		}
	}
			
	public DBRow[] getProfileDoubtOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfileDoubtOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileDoubtOrders",log);
		}
	}
	
	public DBRow[] getProfileDoubtAddressOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfileDoubtAddressOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileDoubtAddressOrders",log);
		}
	}
	
	public DBRow[] getProfileDoubtCostOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfileDoubtCostOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileDoubtCostOrders",log);
		}
	}
	
	public DBRow[] getProfileDoubtPayOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfileDoubtPayOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileDoubtPayOrders",log);
		}
	}
		
	public DBRow[] getProfilePrintedOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfilePrintedOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfilePrintedOrders",log);
		}
	}
		
	public DBRow[] getProfileDate(String st,String en)
		throws Exception
	{
		try
		{
			return(fom.getProfileDate( st, en));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileDate",log);
		}
	}
	
	public DBRow[] getProfileDisputeRefundOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfileDisputeRefundOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileDisputeRefundOrders",log);
		}
	}
	
	public DBRow[] getProfileDisputeWarrantyOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfileDisputeWarrantyOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileDisputeWarrantyOrders",log);
		}
	}
	
	public DBRow[] getProfileOtherDisputingOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getProfileOtherDisputingOrders( st, en, ps_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProfileOtherDisputingOrders",log);
		}
	}
	
	/**
	 * 用日期作为Key，重新封装结果集
	 * @param rows
	 * @return
	 */
	public DBRow getDataByDate(DBRow rows[])
	{
		DBRow result = new DBRow();
		
		for (int i=0; i<rows.length; i++)
		{
			result.add(rows[i].getString("date"), rows[i]);
		}
		
		return(result);
	}
	
	/**
	 * 批量更新旧订单递送时间
	 * @param year
	 * @param month
	 * @throws Exception
	 */
	public void updateDeliveryDate(int year,int month)
		throws Exception
	{
		try
		{
			DBRow orders[] = fom.getOrdersByYearMonth(year, month);
			
			for (int i=0; i<orders.length; i++)
			{
				  DBRow shipProducts[] = fpm.getShipProductsByName(orders[i].getString("oid"));
				  if (shipProducts.length>0)
				  {
					DBRow delivery = new DBRow();
					delivery.add("delivery_date",shipProducts[0].getString("post_date"));
					delivery.add("delivery_account",shipProducts[0].getString("delivery_operator"));
					fom.modPOrder(orders[i].get("oid",0l), delivery);
					////system.out.println(orders[i].get("oid",0l));
				  }
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateDeliveryDate",log);
		}
	}

	/**
	 * 计算订单商品和运费成本
	 * @param oid
	 * @param sc_id 为0不计算运费
	 * @param ccid
	 * @return
	 * @throws WeightOutSizeException
	 * @throws WeightCrossException
	 * @throws CountryOutSizeException
	 * @throws LostMoneyException
	 * @throws Exception
	 */
	public OrderCostBean getOrderCost(long oid,long sc_id,long ccid)
		throws ProvinceOutSizeException,WeightOutSizeException,WeightCrossException,CountryOutSizeException,Exception
	{
		try
		{
			double product_cost = 0;//商品成本
			float weight = 0;		//商品重量
			double total_mc_gross = 0;//订单总成本

			//注意子订单、注意乘以商品数量
			//普通商品、标准套装、手工套装、定制商品

			DBRow orderItems[] = fom.getPOrderItemsByOid(oid);
			for (int i=0; i<orderItems.length; i++)
			{
				//普通商品和标准套装
				if ( orderItems[i].get("product_type", 0)==ProductTypeKey.NORMAL||orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD )
				{
					//log.info( orderItems[i].getString("name")+" - "+orderItems[i].get("weight", 0f)*orderItems[i].get("quantity", 0f) );
					//log.info( orderItems[i].getString("name")+" - "+MoneyUtil.mul(orderItems[i].get("unit_price", 0d), orderItems[i].get("quantity", 0d)) );
					product_cost += MoneyUtil.mul(orderItems[i].get("unit_price", 0d), orderItems[i].get("quantity", 0d));
					weight += orderItems[i].get("weight", 0f)*orderItems[i].get("quantity", 0f);
				}
				else if ( orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//手工拼装
				{
					DBRow productsInSet[] = fpm.getProductsInSetBySetPid( orderItems[i].get("pid",0l) );
					for (int j=0; j<productsInSet.length; j++)
					{
						//log.info( orderItems[i].getString("name")+" - "+productsInSet[j].get("weight", 0f)*orderItems[i].get("quantity", 0f)*productsInSet[j].get("quantity", 0f) );
						//log.info( orderItems[i].getString("name")+" - "+MoneyUtil.mul(productsInSet[j].get("unit_price", 0d),orderItems[i].get("quantity", 0d)*productsInSet[j].get("quantity", 0d)) );
						product_cost += MoneyUtil.mul(productsInSet[j].get("unit_price", 0d),orderItems[i].get("quantity", 0d)*productsInSet[j].get("quantity", 0d));
					}
					
					//手工拼装的重量直接使用标准套装的重量
					weight += orderItems[i].get("weight", 0f)*orderItems[i].get("quantity", 0f);
				}
				else	//定制
				{
					DBRow customProducts[] = fpm.getProductsCustomInSetBySetPid(orderItems[i].get("pid",0l));
					for (int j=0; j<customProducts.length; j++)
					{
						//log.info( orderItems[i].getString("name")+" - "+customProducts[j].get("weight", 0f)*orderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f) );
						//log.info( orderItems[i].getString("name")+" - "+MoneyUtil.mul(customProducts[j].get("unit_price", 0d), orderItems[i].get("quantity", 0d)*customProducts[j].get("quantity", 0d)) );
						product_cost += MoneyUtil.mul(customProducts[j].get("unit_price", 0d), orderItems[i].get("quantity", 0d)*customProducts[j].get("quantity", 0d));
						weight += customProducts[j].get("weight", 0f)*orderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f);
					}
				}
			}
			orderItems = null;

			//检查是否有子订单，有子订单做相同计算
			DBRow sonOrderItems[] = fom.getSonOrderItemsByParentOid(oid);
			for (int i=0; i<sonOrderItems.length; i++)
			{
				//普通商品和标准套装
				if ( sonOrderItems[i].get("product_type", 0)==ProductTypeKey.NORMAL||sonOrderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD )
				{
					//log.info( sonOrderItems[i].getString("name")+" - "+sonOrderItems[i].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f) );
					//log.info( sonOrderItems[i].getString("name")+" - "+MoneyUtil.mul(sonOrderItems[i].get("unit_price", 0d), sonOrderItems[i].get("quantity", 0d)) );
					product_cost += MoneyUtil.mul(sonOrderItems[i].get("unit_price", 0d), sonOrderItems[i].get("quantity", 0d));
					weight += sonOrderItems[i].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f);
				}
				else if ( sonOrderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//手工拼装
				{
					DBRow productsInSet[] = fpm.getProductsInSetBySetPid( sonOrderItems[i].get("pid",0l) );
					for (int j=0; j<productsInSet.length; j++)
					{
						//log.info( sonOrderItems[i].getString("name")+" - "+productsInSet[j].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f)*productsInSet[j].get("quantity", 0f) );
						//log.info( sonOrderItems[i].getString("name")+" - "+MoneyUtil.mul(productsInSet[j].get("unit_price", 0d),sonOrderItems[i].get("quantity", 0d)*productsInSet[j].get("quantity", 0d)) );
						product_cost += MoneyUtil.mul(productsInSet[j].get("unit_price", 0d),sonOrderItems[i].get("quantity", 0d)*productsInSet[j].get("quantity", 0d));
					}
					
					weight += sonOrderItems[i].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f);
				}
				else	//定制
				{
					DBRow customProducts[] = fpm.getProductsCustomInSetBySetPid(sonOrderItems[i].get("pid",0l));
					for (int j=0; j<customProducts.length; j++)
					{
						//log.info( sonOrderItems[i].getString("name")+" - "+customProducts[j].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f) );
						//log.info( sonOrderItems[i].getString("name")+" - "+MoneyUtil.mul(customProducts[j].get("unit_price", 0d), sonOrderItems[i].get("quantity", 0d)*customProducts[j].get("quantity", 0d)) );
						product_cost += MoneyUtil.mul(customProducts[j].get("unit_price", 0d), sonOrderItems[i].get("quantity", 0d)*customProducts[j].get("quantity", 0d));
						weight += customProducts[j].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f);
					}
				}
			}
			
			sonOrderItems = null;
			//log.info("------------------------");

			////system.out.println("->"+product_cost);

			DBRow detailorder = fom.getDetailPOrderByOid(oid);
			total_mc_gross += detailorder.get("mc_gross", 0d);
			//如果有子订单，需要把子订单金额合并
			DBRow sonOrders[] = fom.getSonOrders(oid, null);
			for (int i=0; i<sonOrders.length; i++)
			{
				total_mc_gross += sonOrders[i].get("mc_gross", 0d);
			}

			//需要乘以一个重量修正系数
			float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
			weight = weight*weight_cost_coefficient;

			////system.out.println("Weight:"+weight+" Cost:"+product_cost+" OrderCost:"+total_mc_gross);
			
//			if (detailorder.getString("mc_currency").equals("USD")==false)
//			{
//				//system.out.println(detailorder.getString("mc_currency")+" - "+currency);
//			}
			////system.out.println("detailorder.getString(mc_currency):"+detailorder.getString("mc_currency"));
			
			ShippingInfoBean shippingInfoBean;
			if (sc_id>0&&ccid>0)
			{
				//计算运费
				shippingInfoBean = expressMgr.getShippingFee(sc_id, weight, ccid,detailorder.get("pro_id", 0l));//如果父快递公司不能配送，需要继续查找子快递公司
			}
			else
			{
				shippingInfoBean = new ShippingInfoBean();
				shippingInfoBean.setShippingFee(0);
				shippingInfoBean.setCompanyName("No Name");
			}

			OrderCostBean orderCostBean = new OrderCostBean();
			if (shippingInfoBean!=null)
			{
				double currency = systemConfig.getDoubleConfigValue( detailorder.getString("mc_currency") );//获得汇率
				
				orderCostBean.setProductCost(this.getProductMergeCost(product_cost));								//商品成本
				orderCostBean.setShippingCost(this.getShippingMergeCost(shippingInfoBean.getShippingFee()));			//运费成本
				orderCostBean.setTotalCost( this.getOrderMergeCost(detailorder.getString("order_source"),this.getProductMergeCost(product_cost), this.getShippingMergeCost(shippingInfoBean.getShippingFee())) );		//订单总成本		
				orderCostBean.setMcGross(total_mc_gross);															//订单总金额
				orderCostBean.setMcCurrency(detailorder.getString("mc_currency"));									//货币类型
				orderCostBean.setWeight(weight);																	//订单总重
				orderCostBean.setExpressName(shippingInfoBean.getCompanyName());									//快递公司
				orderCostBean.setRmbMcGross( MoneyUtil.round(MoneyUtil.mul(total_mc_gross, currency), 2) );
			}

			return(orderCostBean);
		}
		catch (CountryOutSizeException e)//国家不到达
		{
			throw e;
		}
		catch (ProvinceOutSizeException e)
		{
			throw e;
		}
		catch (WeightCrossException e)//重量数据段交叉
		{
			throw e;
		}
		catch (WeightOutSizeException e)//重量超出计算范围
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderCost",log);
		}
	}
	
	/**
	 * 计算订单商品和运费成本(最新的，每个商品的价格重量都要现从商品表里抓取)
	 * @param oid
	 * @param sc_id 为0不计算运费
	 * @param ccid
	 * @return
	 * @throws WeightOutSizeException
	 * @throws WeightCrossException
	 * @throws CountryOutSizeException
	 * @throws LostMoneyException
	 * @throws Exception
	 */
	public OrderCostBean getNowOriginalOrderCost(long oid,long sc_id,long ccid)
		throws ProvinceOutSizeException,WeightOutSizeException,WeightCrossException,CountryOutSizeException,Exception
	{
		try
		{
			double product_cost = 0;//商品成本
			float weight = 0;		//商品重量
			double total_mc_gross = 0;//订单总成本

			//注意子订单、注意乘以商品数量
			//普通商品、标准套装、手工套装、定制商品
			DBRow orderItems[] = fom.getPOrderItemsByOid(oid);
			for (int i=0; i<orderItems.length; i++)
			{
				//普通商品和标准套装
				if ( orderItems[i].get("product_type", 0)==ProductTypeKey.NORMAL||orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD )
				{
					//log.info( orderItems[i].getString("name")+" - "+orderItems[i].get("weight", 0f)*orderItems[i].get("quantity", 0f) );
					//log.info( orderItems[i].getString("name")+" - "+MoneyUtil.mul(orderItems[i].get("unit_price", 0d), orderItems[i].get("quantity", 0d)) );
					DBRow product = fpm.getDetailProductByPcid(orderItems[i].get("pid",0l));
					product_cost += MoneyUtil.mul(product.get("unit_price", 0d), orderItems[i].get("quantity", 0d));
					weight += product.get("weight", 0f)*orderItems[i].get("quantity", 0f);
				}
				else if ( orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//手工拼装
				{
					DBRow productsInSet[] = fpm.getProductsInSetBySetPid( orderItems[i].get("pid",0l) );
					for (int j=0; j<productsInSet.length; j++)
					{
						//log.info( orderItems[i].getString("name")+" - "+productsInSet[j].get("weight", 0f)*orderItems[i].get("quantity", 0f)*productsInSet[j].get("quantity", 0f) );
						//log.info( orderItems[i].getString("name")+" - "+MoneyUtil.mul(productsInSet[j].get("unit_price", 0d),orderItems[i].get("quantity", 0d)*productsInSet[j].get("quantity", 0d)) );
						product_cost += MoneyUtil.mul(productsInSet[j].get("unit_price", 0d),orderItems[i].get("quantity", 0d)*productsInSet[j].get("quantity", 0d));
						weight += productsInSet[j].get("weight", 0f)*orderItems[i].get("quantity", 0f)*productsInSet[j].get("quantity", 0d);
					}
					
					//手工拼装的重量直接使用标准套装的重量
					
				}
				else	//定制
				{
					DBRow customProducts[] = fpm.getProductsCustomInSetBySetPid(orderItems[i].get("pid",0l));
					for (int j=0; j<customProducts.length; j++)
					{
						//log.info( orderItems[i].getString("name")+" - "+customProducts[j].get("weight", 0f)*orderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f) );
						//log.info( orderItems[i].getString("name")+" - "+MoneyUtil.mul(customProducts[j].get("unit_price", 0d), orderItems[i].get("quantity", 0d)*customProducts[j].get("quantity", 0d)) );
						product_cost += MoneyUtil.mul(customProducts[j].get("unit_price", 0d), orderItems[i].get("quantity", 0d)*customProducts[j].get("quantity", 0d));
						weight += customProducts[j].get("weight", 0f)*orderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f);
					}
				}
			}
			orderItems = null;

			//检查是否有子订单，有子订单做相同计算
			DBRow sonOrderItems[] = fom.getSonOrderItemsByParentOid(oid);
			for (int i=0; i<sonOrderItems.length; i++)
			{
				//普通商品和标准套装
				if ( sonOrderItems[i].get("product_type", 0)==ProductTypeKey.NORMAL||sonOrderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD )
				{
					//log.info( sonOrderItems[i].getString("name")+" - "+sonOrderItems[i].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f) );
					//log.info( sonOrderItems[i].getString("name")+" - "+MoneyUtil.mul(sonOrderItems[i].get("unit_price", 0d), sonOrderItems[i].get("quantity", 0d)) );
					DBRow sonProduct = fpm.getDetailProductByPcid(sonOrderItems[i].get("pid",0l));
					product_cost += MoneyUtil.mul(sonProduct.get("unit_price", 0d), sonOrderItems[i].get("quantity", 0d));
					weight += sonProduct.get("weight", 0f)*sonOrderItems[i].get("quantity", 0f);
				}
				else if ( sonOrderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//手工拼装
				{
					DBRow productsInSet[] = fpm.getProductsInSetBySetPid( sonOrderItems[i].get("pid",0l) );
					for (int j=0; j<productsInSet.length; j++)
					{
						//log.info( sonOrderItems[i].getString("name")+" - "+productsInSet[j].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f)*productsInSet[j].get("quantity", 0f) );
						//log.info( sonOrderItems[i].getString("name")+" - "+MoneyUtil.mul(productsInSet[j].get("unit_price", 0d),sonOrderItems[i].get("quantity", 0d)*productsInSet[j].get("quantity", 0d)) );
						product_cost += MoneyUtil.mul(productsInSet[j].get("unit_price", 0d),sonOrderItems[i].get("quantity", 0d)*productsInSet[j].get("quantity", 0d));
						weight += productsInSet[j].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f)*productsInSet[j].get("quantity", 0d);
					}
					
					
				}
				else	//定制
				{
					DBRow customProducts[] = fpm.getProductsCustomInSetBySetPid(sonOrderItems[i].get("pid",0l));
					for (int j=0; j<customProducts.length; j++)
					{
						//log.info( sonOrderItems[i].getString("name")+" - "+customProducts[j].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f) );
						//log.info( sonOrderItems[i].getString("name")+" - "+MoneyUtil.mul(customProducts[j].get("unit_price", 0d), sonOrderItems[i].get("quantity", 0d)*customProducts[j].get("quantity", 0d)) );
						product_cost += MoneyUtil.mul(customProducts[j].get("unit_price", 0d), sonOrderItems[i].get("quantity", 0d)*customProducts[j].get("quantity", 0d));
						weight += customProducts[j].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f);
					}
				}
			}
			
			sonOrderItems = null;
			//log.info("------------------------");

			////system.out.println("->"+product_cost);

			DBRow detailorder = fom.getDetailPOrderByOid(oid);
			total_mc_gross += detailorder.get("mc_gross", 0d);
			//如果有子订单，需要把子订单金额合并
			DBRow sonOrders[] = fom.getSonOrders(oid, null);
			for (int i=0; i<sonOrders.length; i++)
			{
				total_mc_gross += sonOrders[i].get("mc_gross", 0d);
			}

			//需要乘以一个重量修正系数
//			float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
//			weight = weight*weight_cost_coefficient;

			////system.out.println("Weight:"+weight+" Cost:"+product_cost+" OrderCost:"+total_mc_gross);
			
//			if (detailorder.getString("mc_currency").equals("USD")==false)
//			{
//				//system.out.println(detailorder.getString("mc_currency")+" - "+currency);
//			}
			////system.out.println("detailorder.getString(mc_currency):"+detailorder.getString("mc_currency"));
			
//			ShippingInfoBean shippingInfoBean;
//			if (sc_id>0&&ccid>0)
//			{
//				//计算运费
//				shippingInfoBean = expressMgr.getShippingFee(sc_id, weight, ccid,detailorder.get("pro_id", 0l));//如果父快递公司不能配送，需要继续查找子快递公司
//			}
//			else
//			{
//				shippingInfoBean = new ShippingInfoBean();
//				shippingInfoBean.setShippingFee(0);
//				shippingInfoBean.setCompanyName("No Name");
//			}

			OrderCostBean orderCostBean = new OrderCostBean();
			
				double currency = systemConfig.getDoubleConfigValue( detailorder.getString("mc_currency") );//获得汇率
				
				orderCostBean.setProductCost(product_cost);								//商品成本
//				orderCostBean.setShippingCost(shippingInfoBean.getShippingFee());			//运费成本
//				orderCostBean.setTotalCost( this.getOrderMergeCost(detailorder.getString("order_source"),this.getProductMergeCost(product_cost), this.getShippingMergeCost(shippingInfoBean.getShippingFee())) );		//订单总成本		
//				orderCostBean.setMcGross(total_mc_gross);															//订单总金额
//				orderCostBean.setMcCurrency(detailorder.getString("mc_currency"));									//货币类型
				orderCostBean.setWeight(weight);																	//订单总重
//				orderCostBean.setExpressName(shippingInfoBean.getCompanyName());									//快递公司
//				orderCostBean.setRmbMcGross( MoneyUtil.round(MoneyUtil.mul(total_mc_gross, currency), 2) );
			

			return(orderCostBean);
		}
		catch (CountryOutSizeException e)//国家不到达
		{
			throw e;
		}
		catch (ProvinceOutSizeException e)
		{
			throw e;
		}
		catch (WeightCrossException e)//重量数据段交叉
		{
			throw e;
		}
		catch (WeightOutSizeException e)//重量超出计算范围
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderCost",log);
		}
	}

	/**
	 * 获得一个订单的总支付金额(原始货币未作转换)
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public double getOrgOrderTotalMcGross(long oid)
		throws Exception
	{
		try
		{
			double total_mc_gross = 0;
			//先获得本订单的支付金额
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			total_mc_gross = detailOrder.get("mc_gross", 0d) ;
			
			//累加补钱订单支付金额
			DBRow additionMoneyOrders[] = fom.getAdditionMoneyOrders(oid, null);
			for (int i=0; i<additionMoneyOrders.length; i++)
			{
				total_mc_gross = MoneyUtil.add(total_mc_gross, additionMoneyOrders[i].get("mc_gross", 0d)) ;
			}
			
			//子订单金额也需要累加
			DBRow sunOrders[] = fom.getSonOrders(oid, null);
			for (int i=0; i<sunOrders.length; i++)
			{
				total_mc_gross = MoneyUtil.add(total_mc_gross, sunOrders[i].get("mc_gross", 0d)) ;
				
				//子订单的补钱订单也要累加
				DBRow sunOrdersAdditionMoneyOrders[] = fom.getAdditionMoneyOrders(sunOrders[i].get("oid", 0l), null);
				for (int ii=0; ii<sunOrdersAdditionMoneyOrders.length; ii++)
				{
					total_mc_gross = MoneyUtil.add(total_mc_gross, sunOrdersAdditionMoneyOrders[ii].get("mc_gross", 0d)) ;
				}
			}
			
			return(total_mc_gross);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrgOrderTotalMcGross",log);
		}
	}
	
	/**
	 * 获得订单总支付美元
	 * 不管订单使用什么货币，最终转换成美元
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public double getUSDOrderTotalMcGross(long oid)
		throws Exception
	{
		try
		{
			double total_mc_gross = getOrgOrderTotalMcGross(oid);
			DBRow detailorder = fom.getDetailPOrderByOid(oid);
			double currency = systemConfig.getDoubleConfigValue( detailorder.getString("mc_currency") );//获得汇率
			double usd_currency = systemConfig.getDoubleConfigValue("USD");//获得汇率
			
			//先把它转换成人民币，然后再转换成美元
			double rmb_mc_gross = MoneyUtil.mul(total_mc_gross, currency);
			
			////system.out.println(total_mc_gross+"-"+currency+"-"+rmb_mc_gross+"-"+usd_currency);
			
			return( MoneyUtil.div(rmb_mc_gross, usd_currency) );
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getUSDOrderTotalMcGross",log);
		}
	}
	
	/**
	 * 比较订单成本，是否亏本
	 * @param oid
	 * @param sc_id
	 * @param ccid
	 * @return  返回订单总重量
	 * @throws CurrencyNotExistException
	 * @throws WeightOutSizeException
	 * @throws WeightCrossException
	 * @throws CountryOutSizeException
	 * @throws LostMoneyException
	 * @throws Exception
	 */
	public float compareOrderCost(long oid,long sc_id,long ccid)
		throws CurrencyNotExistException,WeightOutSizeException,WeightCrossException,CountryOutSizeException,LostMoneyException,Exception
	{
			OrderCostBean orderCostBean = this.getOrderCost(oid, sc_id, ccid);
			double currency = systemConfig.getDoubleConfigValue( orderCostBean.getMcCurrency() );//获得汇率

			////system.out.println("orderCostBean.getMcCurrency():"+orderCostBean.getMcCurrency());
			////system.out.println("currency:"+currency);

			//汇率不存在
			if (currency==0)
			{
				throw new CurrencyNotExistException();
			}

			double total_cost = orderCostBean.getTotalCost();
			////system.out.println(orderCostBean.getMcCurrency()+"("+currency+"):"+MoneyUtil.div(total_cost, currency, 2));
			
			//已发货订单重新打印不需要 比较！！
			if (!withoutVerifyCost(oid) && MoneyUtil.div(total_cost, currency, 2)>orderCostBean.getMcGross())
			{
				throw new LostMoneyException();
			}
			
			return(orderCostBean.getWeight());
	}
	
	/**
	 * 计算订单商品合并成本
	 * @param product_cost
	 * @return
	 * @throws Exception
	 */
	public double getProductMergeCost(double product_cost)
		throws Exception
	{
		double product_cost_coefficient = systemConfig.getDoubleConfigValue( "product_cost_coefficient" );
		return( MoneyUtil.mul(product_cost, product_cost_coefficient) );
	}
	
	/**
	 * 计算订单运费合并成本
	 * @param product_cost
	 * @return
	 * @throws Exception
	 */
	public double getShippingMergeCost(double shipping_cost)
		throws Exception
	{
		double shipping_cost_coefficient = 1;//systemConfig.getDoubleConfigValue( "shipping_cost_coefficient" );
		return( MoneyUtil.mul(shipping_cost, shipping_cost_coefficient) );
	}
	
	/**
	 * 计算订单最后总成本
	 * @param product_cost
	 * @param shipping_cost
	 * @return
	 * @throws Exception
	 */
	public double getOrderMergeCost(String order_source,double product_cost,double shipping_cost)
		throws Exception
	{
		double min_order_profit = 0;
		
//		if (order_source.equals(OrderMgr.ORDER_SOURCE_DIRECTPAY))
//		{
//			min_order_profit = systemConfig.getDoubleConfigValue( "min_order_profit_directpay" );
//		}
//		else if (order_source.equals(OrderMgr.ORDER_SOURCE_MANUAL))
//		{
//			min_order_profit = systemConfig.getDoubleConfigValue( "min_order_profit_manual" );
//		}
//		else if (order_source.equals(OrderMgr.ORDER_SOURCE_WEBSITE))
//		{
//			min_order_profit = systemConfig.getDoubleConfigValue( "min_order_profit_website" );
//		}
//		else//ebay
//		{
//			min_order_profit = systemConfig.getDoubleConfigValue( "min_order_profit_ebay" );
//		}
		
		double total_cost = MoneyUtil.mul( MoneyUtil.add(product_cost, shipping_cost), MoneyUtil.add(1,min_order_profit));
		return(total_cost);
	}

	/**
	 * 是否需要验证成本
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public boolean withoutVerifyCost(long oid)
		throws Exception
	{
		try
		{
			DBRow order = fom.getDetailPOrderByOid(oid);
			
			if ( order.get("handle", 0)==HandleKey.DELIVERIED||order.get("verify_cost", 0)==1 )//已发货和已审核通过
			{
				return(true);
			}
			else
			{
				return(false);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"need2VerifyCost",log);
		}
	}
	
	/**
	 * 把订单标记为疑问成本和待审核
	 * @param request
	 * @throws Exception
	 */
	public void markDoubtCost(HttpServletRequest request)
		throws Exception
	{
		try
		{
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			long oid = StringUtil.getLong(request, "oid");
			long sc_id = StringUtil.getLong(request, "sc_id");
			long ccid = StringUtil.getLong(request, "ccid");
			String note = StringUtil.getString(request, "note");
			
			OrderCostBean orderCostBean = this.getOrderCost(oid, sc_id, ccid);
			DBRow order = new DBRow();
			order.add("handle", HandleKey.VERIFY_COST);//疑问成本

			order.add("product_cost", orderCostBean.getProductCost() );
			order.add("shipping_cost",orderCostBean.getShippingCost());
			order.add("shipping_name",orderCostBean.getExpressName());
			order.add("total_cost",orderCostBean.getTotalCost());
			order.add("total_weight",orderCostBean.getWeight());
			order.add("mc_gross_rmb",orderCostBean.getRmbMcGross());
			
			fom.modPOrder(oid, order);
			
			this.validateTraceService(adminLoggerBean.getAdgid(),oid, TracingOrderKey.ORDER_COST_VERIFY);
			this.addPOrderNotePrivate(oid, note,TracingOrderKey.ORDER_COST_VERIFY, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markDoubtCost",log);
		}
	}
	
	/**
	 * 订单成本审核通过
	 * @param request
	 * @throws Exception
	 */
	public void verifyOrderCost(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String verify_note = StringUtil.getString(request, "verify_note");
			
			DBRow order = new DBRow();
			order.add("handle", HandleKey.WAIT4_PRINT);
			order.add("verify_cost", 1);//审核通过
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, verify_note,TracingOrderKey.ORDER_COST_VERIFY, StringUtil.getSession(request),0);//增加备注
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"verifyOrderCost",log);
		}
	}
	
	/**
	 * 把所有已经发货的复合订单的子订单也变成发货状态 
	 * @param request
	 * @throws Exception
	 */
	public void updateNotDeliverySonOrders(HttpServletRequest request)
		throws Exception
	{
		try
		{
			DBRow orders[] = fom.getAllDeliveriedMergeOrders();
			
			for (int i=0; i<orders.length; i++)
			{
				DBRow sonOrders[] = fom.getSonOrders(orders[i].get("oid", 0l), null);
				
				for (int j=0; j<sonOrders.length; j++)
				{
					DBRow row = new DBRow();
					row.add("handle", 4);
					fom.modPOrder(sonOrders[j].get("oid", 0l), row);
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateNotDeliverySonOrders",log);
		}
	}

	//---------- 订单跟踪任务 --------------
	
	/**
	 * 根据订单的状态，开启、更新、关闭跟踪服务
	 * 对当前处理订单历遍检查所有订单任务
	 * @param handle
	 * @param handle_status
	 * @param product_status
	 * @return    
	 * @throws Exception
	 */
	public void validateTraceService(long role_id,long oid,int trace_type)
		throws Exception 
	{
		try
		{
			ArrayList<String> orderTraceTasks = new ArrayList<String>();
			orderTraceTasks.add("DoubtOrderTask");//所有订单跟进服务任务
			orderTraceTasks.add("LackingProductOrderTask");
			orderTraceTasks.add("DoubtPayOrderTask");
			orderTraceTasks.add("DoubtAddressOrderTask");
			orderTraceTasks.add("VerifyCostOrderTask");
			orderTraceTasks.add("PrintDoubtOrderTask");
			orderTraceTasks.add("DisputeRefundingOrderTask");
			orderTraceTasks.add("DisputeWarrantyingOrderTask");
			orderTraceTasks.add("OtherDisputeOrderTask");

			DBRow detailOrder = fom.getDetailPOrderByOid(oid);

			int handle_status = detailOrder.get("handle_status",0);
			int after_service_status = detailOrder.get("after_service_status", 0);
			int handle = detailOrder.get("handle", 0);
			int product_status = detailOrder.get("product_status", 0);
			int trace_flag = detailOrder.get("trace_flag", 0);
			long trace_pre_operator_role_id = detailOrder.get("trace_pre_operator_role_id", 0l);
			long trace_operator_role_id = detailOrder.get("trace_operator_role_id", 0l);
			String oldTraceTaskName = detailOrder.getString("trace_task_name");

			boolean haveOrderTraceTask = false;

			for (int i=0; i<orderTraceTasks.size(); i++)
			{
				OrderTraceFather orderTraceTask = (OrderTraceFather)MvcUtil.getBeanFromContainer(orderTraceTasks.get(i).toString());

				if ( orderTraceTask.need2CreateTask(handle, after_service_status,handle_status, product_status,oldTraceTaskName,trace_flag, trace_operator_role_id,trace_pre_operator_role_id,role_id) )//检查是否需要创建该订单任务
				{
					updateTraceService(orderTraceTask.getClass().getSimpleName(),oldTraceTaskName,role_id,orderTraceTask.getNextOperatorRoleID(),oid,trace_flag,detailOrder.getString("trace_date"),orderTraceTask.getNextTraceDate(), trace_type);
					haveOrderTraceTask = true;
					break;
				}
			}
			
			//订单不再需要跟踪服务，关闭跟踪服务
			if (!haveOrderTraceTask)
			{
//				closeTraceService(oid,trace_flag);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"validateTraceService",log);
		}
	}
	
	
	/**
	 * 更新跟踪服务
	 * 
	 * @param trace_pre_operator_role_id	上一次执行任务角色	
	 * @param trace_operator_role_id		执行该任务的角色
	 * @param oid							任务相关订单号
	 * @param trace_flag					1-活动任务，0-关闭任务
	 * @param old_trace_date				任务应该执行日期
	 * @param new_trace_date				任务实际执行日期
	 * @param trace_type					任务类型
	 * @throws Exception
	 */
	private void updateTraceService(String currentTraceTaskName,String oldTraceTaskName,long trace_pre_operator_role_id,long trace_operator_role_id,long oid,int trace_flag,String old_trace_date,String new_trace_date,int trace_type)
		throws Exception
	{
		try
		{
			TDate tDate = new TDate(); 
			String now_date = DateUtil.NowStr();
			DBRow order = new DBRow();

			//同一个任务，如果当前执行时间小于任务计划时间并且没有发生任务切换，不对任务记录做任何更新
			if (currentTraceTaskName.equals(oldTraceTaskName)&&tDate.getDateTime(now_date)<tDate.getDateTime(old_trace_date))
			{
			}
			else
			{
				//跟踪服务已经被打开
				if (trace_flag==1&&currentTraceTaskName.equals(oldTraceTaskName))
				{
					/**
					 * 任务开始执行
					 * 上一次执行角色ID为当前执行任务角色ID
					 * 任务执行角色ID为 任务类计算后赋予
					 */
					//检测是否超时处理，超时处理需要记录
					//多角色参与的任务，当最后一个角色完成操作，才去判断是否超时
					if ( new_trace_date!=null&&trace_operator_role_id!=-1&&tDate.getDateTime(now_date)>tDate.getDateTime(old_trace_date) )
					{
						DBRow traceDelay = new DBRow();
						traceDelay.add("oid", oid);
						traceDelay.add("trace_date", old_trace_date);
						traceDelay.add("delay_date", now_date);
						traceDelay.add("trace_type", trace_type);
						fom.addTraceDelay(traceDelay);
					}
					
					//多角色任务，new_trace_date有可能为null
					if (new_trace_date!=null)
					{
//						order.add("trace_date",new_trace_date);
					}
					if (trace_operator_role_id!=-1)
					{
						order.add("trace_operator_role_id",trace_operator_role_id);
					}
					order.add("trace_pre_operator_role_id",trace_pre_operator_role_id);
					order.add("trace_type",trace_type);
				}
				/**
				 * 第一次创建订单任务
				 * 上一次执行角色ID为0（因为第一次创建，还没被执行）
				 * 任务执行角色ID为 任务类计算后赋予
				 */
				else
				{
					order.add("trace_operator_role_id",trace_operator_role_id);
					order.add("trace_pre_operator_role_id",0);//第一次创建任务，暂无执行人，所以ID为0
					order.add("trace_type",trace_type);
//					order.add("trace_date",new_trace_date);
					order.add("trace_task_name",currentTraceTaskName);
					order.add("trace_flag",1);//打开任务
				}

				fom.modPOrder(oid, order);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateTraceService",log);
		}
	}
	
	/**
	 * 关闭跟踪服务
	 * @param oid
	 * @throws Exception
	 */
	private void closeTraceService(long oid,int trace_flag)
		throws Exception
	{
		try
		{
			if (trace_flag==1)
			{
				DBRow order = new DBRow();
				order.add("trace_flag",0);//关闭任务开关
				order.add("trace_task_name","");//清空订单任务名称
				fom.modPOrder(oid, order);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"closeTraceService",log);
		}
	}

	/**
	 * 获得需要跟进疑问订单数
	 * @return
	 * @throws Exception
	 */
	public int getTraceDoubtOrderCount(String st,String en,int tracking_day)
		throws Exception
	{
		try
		{
			return(fom.getTraceDoubtOrderCount(st,en,tracking_day));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceDoubtOrderCount",log);
		}
	}
	
	/**
	 * 获得需要跟进疑问地址订单数
	 * @return
	 * @throws Exception
	 */
	public int getTraceDoubtAddressOrderCount(String st,String en,int doubt_address_day)
		throws Exception
	{
		try
		{
			return(fom.getTraceDoubtAddressOrderCount(st,en,doubt_address_day));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceDoubtAddressOrderCount",log);
		}
	}

	/**
	 * 获得需要跟进疑问付款订单数
	 * @return
	 * @throws Exception
	 */
	public int getTraceDoubtPayOrderCount(String st,String en,int doubt_pay_day)
		throws Exception
	{
		try
		{
			return(fom.getTraceDoubtPayOrderCount(st,en,doubt_pay_day));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceDoubtPayOrderCount",log);
		}
	}
		
	/**
	 * 获得需要跟进缺货订单数
	 * @return
	 * @throws Exception
	 */
	public int getTraceLackingOrderCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			return(fom.getTraceLackingOrderCount( trace_operator_role_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceLackingOrderCount",log);
		}
	}

	/**
	 * 获得需要跟进疑问订单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTraceDoubtOrders(PageCtrl pc)
		throws Exception
	{
		try
		{
			TDate tDate = new TDate();
			tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
			String st = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			String en = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
			
			int doubtOrderTracgingDay = Integer.valueOf(systemConfig.getStringConfigValue("doubt_order_handle_period"));
			
			return(fom.getTraceDoubtOrders(st,en,doubtOrderTracgingDay, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceDoubtOrders",log);
		}
	}
	
	/**
	 * 获得需要跟进疑问地址订单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTraceDoubtAddressOrders(PageCtrl pc)
		throws Exception
	{
		try
		{
			int doubtAddressOrderTracgingDay = Integer.valueOf(systemConfig.getStringConfigValue("doubt_order_address_period"));
			
			TDate tDate = new TDate();
			tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
			String st = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			String en = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
			
			return(fom.getTraceDoubtAddressOrders(st,en,doubtAddressOrderTracgingDay,pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceDoubtAddressOrders",log);
		}
	}
	
	/**
	 * 获得需要跟进疑问付款订单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTraceDoubtPayOrders(PageCtrl pc)
		throws Exception
	{
		try
		{
			TDate tDate = new TDate();
			tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
			String st = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			String en = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
			
			int doubtPayOrderTracgingDay = Integer.valueOf(systemConfig.getStringConfigValue("doubt_order_pay_period"));
			
			return(fom.getTraceDoubtPayOrders(st,en,doubtPayOrderTracgingDay,pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceDoubtPayOrders",log);
		}
	}
	
	/**
	 * 获得需要跟进缺货订单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTraceLackingOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getTraceLackingOrders(  trace_operator_role_id,pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceLackingOrders",log);
		}
	}
	
	/**
	 * 获得打印疑问订单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTracePrintDoubtOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getTracePrintDoubtOrders(trace_operator_role_id, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTracePrintDoubtOrders",log);
		}
	}

	/**
	 * 获得需要跟进成本审核订单数
	 * @return
	 * @throws Exception
	 */
	public int getVerifyCostOrderCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			return(fom.getVerifyCostOrderCount(trace_operator_role_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getVerifyCostOrderCount",log);
		}
	}
	
	/**
	 * 打印疑问
	 * @return
	 * @throws Exception
	 */
	public int getPrintDoubtOrderCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			return(fom.getPrintDoubtOrderCount(trace_operator_role_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getPrintDoubtOrderCount",log);
		}
	}
	
	/**
	 * 获得需要跟进成本审核订单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getVerifyCostOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getVerifyCostOrders( trace_operator_role_id,pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getVerifyCostOrders",log);
		}
	}
	
	public int getDisputeRefundCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			return(fom.getDisputeRefundCount(trace_operator_role_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDisputeRefundCount",log);
		}
	}
	
	public int getDisputeWarrantyCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			return(fom.getDisputeWarrantyCount(trace_operator_role_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDisputeWarrantyCount",log);
		}
	}
	
	public int getOtherDisputeCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			return(fom.getOtherDisputeCount(trace_operator_role_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOtherDisputeCount",log);
		}
	}
	
	//---------- 订单跟踪任务 --------------
	
	
	
	/**
	 * 5位纯数字
	 * @return
	 */
	public boolean validateUSAZipCode(String zipCode)
	{
		try
		{
			Long.parseLong(zipCode);
		}
		catch (NumberFormatException e) 
		{
			return(false);
		}
		
		if (zipCode.length()!=5)
		{
			return(false);
		}
		
		return(true);
	}

	/**
	 * 打印发现地址问题，提交给客服处理
	 * @throws Exception
	 */
	public void printVerifyAddress(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String verify_address_note = StringUtil.getString(request, "verify_address_note");

			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			DBRow orderItems[] = fom.getPOrderItemsByOid(oid);
			
			//回退库存
			this.stockback(null,HandStatusleKey.NORMAL,0,detailOrder, orderItems,StringUtil.getSession(request));
			
			//变为疑问状态，并且库存状态改为未知
			DBRow order = new DBRow();
			order.add("handle_status", HandStatusleKey.PRINT_DOUBT);
			order.add("product_status",ProductStatusKey.UNKNOWN);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, verify_address_note, TracingOrderKey.PRINT_DOUBT, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"printVerifyAddress",log);
		}
	}
	

	/**
	 * 获得订单估算重量
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public float getOrderWeightByOid(long oid)
		throws Exception
	{
		try
		{
			float weight = 0;		//商品重量

			//注意子订单、注意乘以商品数量
			//普通商品、标准套装、手工套装、定制商品

			DBRow orderItems[] = fom.getPOrderItemsByOid(oid);
			for (int i=0; i<orderItems.length; i++)
			{
				//普通商品和标准套装
				if ( orderItems[i].get("product_type", 0)==ProductTypeKey.NORMAL||orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD )
				{
					weight += orderItems[i].get("weight", 0f)*orderItems[i].get("quantity", 0f);
				}
				else if ( orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//手工拼装
				{
					//手工拼装的重量直接使用标准套装的重量
					weight += orderItems[i].get("weight", 0f)*orderItems[i].get("quantity", 0f);
				}
				else	//定制
				{
					DBRow customProducts[] = fpm.getProductsCustomInSetBySetPid(orderItems[i].get("pid",0l));
					for (int j=0; j<customProducts.length; j++)
					{
						weight += customProducts[j].get("weight", 0f)*orderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f);
					}
				}
			}

			//检查是否有子订单，有子订单做相同计算
			DBRow sonOrderItems[] = fom.getSonOrderItemsByParentOid(oid);
			for (int i=0; i<sonOrderItems.length; i++)
			{
				//普通商品和标准套装
				if ( sonOrderItems[i].get("product_type", 0)==ProductTypeKey.NORMAL||sonOrderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD )
				{
					weight += sonOrderItems[i].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f);
				}
				else if ( sonOrderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//手工拼装
				{
					weight += sonOrderItems[i].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f);
				}
				else	//定制
				{
					DBRow customProducts[] = fpm.getProductsCustomInSetBySetPid(sonOrderItems[i].get("pid",0l));
					for (int j=0; j<customProducts.length; j++)
					{
						weight += customProducts[j].get("weight", 0f)*sonOrderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f);
					}
				}
			}
			float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
			weight = weight*weight_cost_coefficient;
			
			return( Float.parseFloat(String.valueOf(MoneyUtil.round(weight, 2))) );
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderWeightByOid",log);
		}
	}

	/**
	 * 获得所有被监控状态订单数量
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getMonitorTraceCount(HttpServletRequest request)
		throws Exception
	{
		try
		{
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean(StringUtil.getSession(request));
			
			TDate tDate = new TDate();
			tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
			String st = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			String en = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
			
			//进单数字
			int waitRecordOrderCount = this.getWaitRecordOrderCount(st,en);
			int outBoundingCount = this.getOutBoundingCount(st,en);
			
			
			//需跟进订单数字
			int doubtOrderTracgingDay = Integer.valueOf(systemConfig.getStringConfigValue("doubt_order_handle_period")); 
			int doubtAddressOrderTracgingDay = Integer.valueOf(systemConfig.getStringConfigValue("doubt_order_address_period"));
			int doubtPayOrderTracgingDay = Integer.valueOf(systemConfig.getStringConfigValue("doubt_order_pay_period"));
			
			int doubtOrderCount = this.getTraceDoubtOrderCount(st,en,doubtOrderTracgingDay);
			int doubtAddressOrderCount = this.getTraceDoubtAddressOrderCount(st,en,doubtAddressOrderTracgingDay);
			int doubtPayOrderCount = this.getTraceDoubtPayOrderCount(st,en,doubtPayOrderTracgingDay);
			//需跟进快递运输
			int delivery_exception_day = Integer.valueOf(systemConfig.getStringConfigValue("delivery_exception_day"));
			int customs_exception_day = Integer.valueOf(systemConfig.getStringConfigValue("customs_exception_day"));
			int overdue_deliveryed_day = Integer.valueOf(systemConfig.getStringConfigValue("overdue_deliveryed_day"));
			int notyet_deliveryed_day = Integer.valueOf(systemConfig.getStringConfigValue("notyet_deliveryed_day"));
			
			int deliveryExceptionCount = this.getDeliveryExceptionCount(st,en,delivery_exception_day);
			int customsExceptionCount = this.getCustomsExceptionCount(st,en,customs_exception_day);
			int notyetDeliveryedCount = this.getNotyetDeliveryedCount(st,en,notyet_deliveryed_day,overdue_deliveryed_day);
			
			//需跟进发货
			int stockout_day = Integer.valueOf(systemConfig.getStringConfigValue("waybill_stockout_period"));
			int overdue_hour = Integer.valueOf(systemConfig.getStringConfigValue("waybill_overdue_send_period"));
			int overdue_track_day = Integer.valueOf(systemConfig.getStringConfigValue("waybill_overdue_track_day"));
			
			int stockoutWaybillCount = floorWayBillOrderMgrZJ.getStockoutWaybillOrderCount(stockout_day);
			int overdueWaybillCount = floorWayBillOrderMgrZJ.getOverdueSendWaybillOrderCount(overdue_hour,overdue_track_day);
		
			
			
			
			int lackingOrderCount = this.getTraceLackingOrderCount(adminLoggerBean.getAdgid());
			int verifyCostOrderCount = this.getVerifyCostOrderCount(adminLoggerBean.getAdgid());
			int printDoubtOrderCount = this.getPrintDoubtOrderCount(adminLoggerBean.getAdgid());
			int disputerefundcount = this.getDisputeRefundCount(adminLoggerBean.getAdgid());
			int disputewarrantycount = this.getDisputeWarrantyCount(adminLoggerBean.getAdgid());
			int otherdisputecount = this.getOtherDisputeCount(adminLoggerBean.getAdgid());

			DBRow result = new DBRow();
			result.add("doubtordercount", doubtOrderCount);
			result.add("doubtaddressordercount", doubtAddressOrderCount);
			result.add("doubtpayordercount", doubtPayOrderCount);
			result.add("lackingordercount", lackingOrderCount);
			result.add("verifycostordercount", verifyCostOrderCount);
			result.add("printdoubtordercount", printDoubtOrderCount);
			
			result.add("disputerefundcount", disputerefundcount);
			result.add("disputewarrantycount", disputewarrantycount);
			result.add("otherdisputecount", otherdisputecount);
			
			
			result.add("waitrecordordercount",waitRecordOrderCount);
			result.add("outboundingcount",outBoundingCount);
			
			result.add("deliveryexceptioncount",deliveryExceptionCount);
			result.add("customsexceptioncount",customsExceptionCount);
			result.add("notyetdeliveryedcount",notyetDeliveryedCount);
			
			result.add("stockoutwaybillcount",stockoutWaybillCount);
			result.add("overduewaybillcount",overdueWaybillCount);
			
			return(result);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getMonitorTraceCount",log);
		}
	}
	
	/**
	 * 需跟进清关例外订单
	 */
	public DBRow[] getCustomExceptionOrder(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			TDate tDate = new TDate();
			tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
			
			String st = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			String en = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
			
			int customs_exception_day = Integer.valueOf(systemConfig.getStringConfigValue("customs_exception_day"));
			
			return fom.getCustomsExceptionOrders(st, en, customs_exception_day, pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getCustomExceptionOrder",log);
		}
	}

	/**
	 * 获得需跟进派送例外订单
	 */
	public DBRow[] getDeliveryExceptionOrder(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			TDate tDate = new TDate();
			tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
			
			String st = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			String en = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
			
			int delivery_exception_day = Integer.valueOf(systemConfig.getStringConfigValue("delivery_exception_day"));
			
			return fom.getDeliveryExceptionOrders(st, en, delivery_exception_day, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDeliveryExceptionOrder",log);
		}
	}

	/**
	 * 需跟进未妥投订单
	 */
	public DBRow[] getNotYetDeliveryedOrder(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			int notyet_deliveryed_day = Integer.valueOf(systemConfig.getStringConfigValue("notyet_deliveryed_day"));
			int overdue_deliveryed_day = Integer.valueOf(systemConfig.getStringConfigValue("overdue_deliveryed_day"));
			
			TDate tDate = new TDate();
			tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
			
			String st = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			String en = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
			
			return fom.getNotyetDeliveryedOrders(st, en, notyet_deliveryed_day,overdue_deliveryed_day,pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getNotYetDeliveryedOrder",log);
		}
	}
	
	/**
	 * 需跟进清关例外订单数量
	 * @param st
	 * @param en
	 * @param customs_exception_day
	 * @return
	 * @throws Exception
	 */
	 public int getCustomsExceptionCount(String st,String en,int customs_exception_day)
	 	throws Exception
	 {
		 try 
		 {
			return fom.getCustomsExceptionCount(st,en,customs_exception_day);
		 }
		 catch (Exception e) 
		 {
			throw new SystemException(e,"getCustomsExceptionCount",log);
		 }
	 }
	
	 /**
	  * 获得未妥投的订单数量
	  * @param st
	  * @param en
	  * @param notyet_deliveryed_day
	  * @return
	  * @throws Exception
	  */
	 public int getNotyetDeliveryedCount(String st,String en,int notyet_deliveryed_day,int overdue_deliveryed_day)
	 	throws Exception
	 {
		 try 
		 {
			return fom.getNotyetDeliveryedCount(st,en,notyet_deliveryed_day,overdue_deliveryed_day);
		 }
		 catch (Exception e) 
		 {
			throw new SystemException(e,"getNotyetDeliveryedCount",log);
		 }
	 }
	 
	/**
	 * 需跟进派送例外订单数量
	 * @param st
	 * @param en
	 * @param delivery_exception_day
	 * @return
	 * @throws Exception
	 */
	public int getDeliveryExceptionCount(String st,String en,int delivery_exception_day)
		throws Exception
	{
		try 
		{
			return fom.getDeliveryExceptionCount(st,en,delivery_exception_day);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDeliveryExceptionCount",log);
		}
	}
	
	public int getWaitRecordOrderCount(String input_st_date,String input_en_date)
		throws Exception
	{
		try 
		{
			return fom.getWaitRecordOrderCount(input_st_date,input_en_date);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWaitRecordOrderCount",log);
		}
	}
	
	public int getOutBoundingCount(String st,String en)
		throws Exception
	{
		try 
		{
			return fom.getOutboundingCount(st,en);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOutBoundingCount",log);
		}
	}
	
	/**
	 * 超时处理订单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTraceDelay(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getTraceDelay( pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceDelay",log);
		}
	}
	
	/**
	 * 把长期处于疑问状态的订单挂起
	 * @param request
	 * @throws Exception
	 */
	public void hangUpLongDoubtOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String hang_up_note = StringUtil.getString(request, "note");
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			DBRow orderItems[] = fom.getPOrderItemsByOid(oid);
			
			//回退库存
			this.stockback(null,HandStatusleKey.NORMAL,0,detailOrder, orderItems,StringUtil.getSession(request));
			
			//变为疑问状态，并且库存状态改为未知
			DBRow order = new DBRow();
			order.add("handle_status", HandStatusleKey.HANG_UP);
			order.add("product_status",ProductStatusKey.UNKNOWN);
			order.add("trace_flag",1);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, hang_up_note, TracingOrderKey.OTHERS, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"hangUpLongDoubtOrder",log);
		}
	}
	
	/**
	 * 判断订单是否已经信用卡验证
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public boolean hasBeCreditVerify(long oid)
		throws Exception
	{
		try
		{
			DBRow rows[] = fom.getPOrderNoteByOidTraceType(oid, TracingOrderKey.CREDIT_VERIFY, null);
			
			if (rows.length>0)
			{
				return(true);
			}
			else
			{
				return(false);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"hasBeCreditVerify",log);
		}
	}

	/**
	 * 更新旧订单的快递方式
	 * @param oid
	 * @throws Exception
	 */
	public void updateOldOrderShippingWays(long ps_id,int year,int month)
		throws Exception
	{
		try
		{
			DBRow orders[] = fom.getOrdersByYearMonthRange(ps_id,year, month, null);
			
			for (int i=0; i<orders.length; i++)
			{
				if (orders[i].getString("shipping_name").equals("")==false)
				{
					continue;
				}
				
				DBRow shippingName = new DBRow();
				
				if ( orders[i].getString("ems_id").toLowerCase().indexOf("cn")>=0 )
				{
					shippingName.add("shipping_name", "EMS");
				}
				else if ( orders[i].getString("ems_id").length()==10||orders[i].getString("ems_id").length()==27||orders[i].getString("ems_id").length()==30||orders[i].getString("ems_id").equals("") )//暂时把usps也算是DHL
				{
					shippingName.add("shipping_name", "DHL");
				}
				else 
				{
					continue;
				}
				
				fom.modPOrder(orders[i].get("oid", 0l), shippingName);
				////system.out.println(orders[i].get("oid", 0l));
			}
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateOldOrderShippingWays",log);
		}
	}
	
	/**
	 * 更新旧订单成本
	 * @param ps_id
	 * @param year
	 * @param month
	 * @throws Exception
	 */
	public void updateOldOrderCost(int month)
		throws Exception
	{
		try
		{
			fom.updateOldOrderCost(month);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateOldOrderCost",log);
		}
	}
	
	/**
	 * 临时更新一些美国订单打印问题
	 */
	public void updateOldOrderCostTMP()
	throws Exception
{
	try
	{
		fom.updateOldOrderCostTMP();
	}
	catch (Exception e) 
	{
		throw new SystemException(e,"updateOldOrderCostTMP",log);
	}
}

	/**
	 * 批量增加缺货列表
	 * @param oid
	 * @param lackingList  缺货商品
	 * @throws Exception
	 */
	private void addOrderLackingList(long oid,ArrayList lackingList)
		throws Exception
	{
		try
		{
			if (lackingList.size()>0)
			{
				fom.delOrderLackingListByOid(oid);
				
				for (int i=0; i<lackingList.size(); i++)
				{
					fom.addOrderLackingList( (DBRow)lackingList.get(i) );				
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addOrderLackingList",log);
		}
	}
	
	/**
	 * 获得缺货清单
	 * @param oid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOrderLackingProductListByOid(long oid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getOrderLackingProductListByOid(oid,pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getOrderLackingProductListByOid",log);
		}
	}

	/**
	 * 补钱订单
	 * @param request
	 * @throws Exception
	 */
	public void additionMoneyOrder(HttpServletRequest request)
		throws NotNormalOrderException,OrderNotExistException,Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			long addition_money_oid = StringUtil.getLong(request, "addition_money_oid");

			DBRow detailOrder = fom.getDetailPOrderByOid(addition_money_oid);
			DBRow orderItems[] = fom.getPOrderItemsByOid(addition_money_oid);
			
			if ( detailOrder==null )
			{
				throw new OrderNotExistException();
			}

			if (detailOrder.get("handle_status", 0)!=HandStatusleKey.NORMAL)
			{
				throw new NotNormalOrderException();
			}
			
			//回退库存
			this.stockback(null,HandStatusleKey.NORMAL,0,detailOrder, orderItems,StringUtil.getSession(request));
			
			DBRow order = new DBRow();
			order.add("handle_status", HandStatusleKey.ADDITION_MONEY);
			order.add("product_status",ProductStatusKey.UNKNOWN);
			order.add("handle", HandleKey.WAIT4_RECORD);
			order.add("addition_money_oid", oid);
			fom.modPOrder(addition_money_oid, order);
			
			this.addPOrderNotePrivate(oid, "补钱订单", TracingOrderKey.OTHERS, StringUtil.getSession(request),addition_money_oid);
			this.addPOrderNotePrivate(addition_money_oid, "补钱到", TracingOrderKey.OTHERS, StringUtil.getSession(request),oid);
		}
		catch (NotNormalOrderException e) 
		{
			throw e;
		}
		catch (OrderNotExistException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"additionMoneyOrder",log);
		}
	}	
	
	/**
	 * 取消补钱
	 * @param request
	 * @throws OrderNotExistException
	 * @throws Exception
	 */
	public void unDoAdditionMoneyOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			
			DBRow order = new DBRow();
			order.add("handle_status", HandStatusleKey.NORMAL);
			order.add("addition_money_oid", 0);
			fom.modPOrder(oid, order);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"unDoAdditionMoneyOrder",log);
		}
	}

	//============== 质保处理 ================
	
	/**
	 * 正常退款中
	 * @param request
	 * @throws Exception
	 */
	public long markNormalRefunding(HttpServletRequest request)
		throws OperationNotPermitException,Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			int return_product_flag = StringUtil.getInt(request, "return_product_flag");
			long rp_id = 0;
			
			DBRow order = new DBRow();
			order.add("after_service_status", AfterServiceKey.NORMAL_REFUNDING);
			fom.modPOrder(oid, order);
			
			if (return_product_flag==1)//需要退货
			{
				rp_id = ((OrderMgrIFace)AopContext.currentProxy()).addReturnProducts(request,true);//第二个参数为true表示直接发送邮件
			}
			////system.out.println(return_product_flag);
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.NORMAL_REFUNDING, StringUtil.getSession(request),rp_id);
			cartReturn.clearCart(StringUtil.getSession(request));			
			return (rp_id);
		}
		catch (OperationNotPermitException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markNormalRefunding",log);
		}
	}
	/**
	 * 退货完成后发送邮件给顾客	 
	 * @param oid
	 * @param rp_id
	 * @throws Exception
	 */
	public void sendMail(long oid,long rp_id)
	throws Exception
	{
		try
		{
			//发送邮件通知顾客
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			
			ReturnVoiceEmailPageString returnVoiceEmailPageString = new ReturnVoiceEmailPageString(rp_id,oid);
			User user = new User(returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getPassWord(),returnVoiceEmailPageString.getHost(),returnVoiceEmailPageString.getPort(),returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getAuthor(),returnVoiceEmailPageString.getRePly());
	        MailAddress mailAddress = new MailAddress(detailOrder.getString("client_id"));
	        MailBody mailBody = new MailBody(returnVoiceEmailPageString,true,null);
	
	        Mail mail = new Mail(user);
	        mail.setAddress(mailAddress);
	        mail.setMailBody(mailBody);
	        mail.send();
		}
		catch(Exception e)
		{
			throw new SystemException(e,"sendMail(oid,rp_id)error:",log);
		}
	}
	
	/**
	 * 争议退款中
	 * @param request
	 * @throws Exception
	 */
	public void markDisputeRefunding(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.DISPUTE_REFUNDING);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.DISPUTE_REFUNDING, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markDisputeRefunding",log);
		}
	}

	/**
	 * 已经部分退款
	 * @param request
	 * @throws Exception
	 */
	
	public void markPartRefunded(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.PART_REFUNDED);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.PART_REFUNDED, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markPartRefunded",log);
		}
	}
	
	/**
	 * 已全部退款
	 * @param request
	 * @throws Exception
	 */
	public void markAllRefunded(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.ALL_REFUNDED);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.ALL_REFUNDED, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markAllRefunded",log);
		}
	}

	/**
	 * 普通质保
	 * @param request
	 * @throws Exception
	 */
	public long markNormalWarrantying(HttpServletRequest request)
		throws OperationNotPermitException,Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			int return_product_flag = StringUtil.getInt(request, "return_product_flag");
			long rp_id = 0;
			
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.NORMAL_WARRANTYING);
			fom.modPOrder(oid, order);
			
			if (return_product_flag==1)
			{
				rp_id = ((OrderMgrIFace)AopContext.currentProxy()).addReturnProducts(request,false);//第二个参数为false表示不直接发送邮件
			}
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.NORMAL_WARRANTYING, StringUtil.getSession(request),rp_id);
			cartReturn.clearCart(StringUtil.getSession(request));
			return (rp_id);
		}
		catch (OperationNotPermitException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markNormalWarrantying",log);
		}
	}
	
	/**
	 * 争议质保
	 * @param request
	 * @throws Exception
	 */
	
	public void markDisputeWarrantying(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.DISPUTE_WARRANTYING);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.DISPUTE_WARRANTYING, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markDisputeWarrantying",log);
		}
	}
	
	/**
	 * 已质保
	 * @param request
	 * @throws Exception
	 */
	
	public void markWarrantied(HttpServletRequest request)
		throws OrderNotExistException,Exception
	{
		try
		{
//			long oid = StrUtil.getLong(request, "oid");
//			long addition_money_oid = StrUtil.getLong(request, "addition_money_oid");
//			
//			if ( fom.getDetailPOrderByOid(addition_money_oid)==null )
//			{
//				throw new OrderNotExistException();
//			}
//			
//			DBRow order = new DBRow();
//			
//			order.add("after_service_status", AfterServiceKey.WARRANTIED);
//			fom.modPOrder(oid, order);
//			
//			DBRow warrantyOrder = new DBRow();
//			warrantyOrder.add("manual_order_type",1);//质保订单
//			warrantyOrder.add("manual_order_rel_oid", addition_money_oid);//质保单号
//			fom.modPOrder(addition_money_oid, warrantyOrder);
//			
//			this.addPOrderNotePrivate(oid, "创建有偿质保订单", TracingOrderKey.WARRANTIED, StrUtil.getSession(request),addition_money_oid);
//			this.addPOrderNotePrivate(addition_money_oid, "有偿质保订单", TracingOrderKey.WARRANTIED, StrUtil.getSession(request),oid);
			
			long oid = StringUtil.getLong(request, "oid");
			
			DBRow order = new DBRow();
			order.add("after_service_status", AfterServiceKey.WARRANTIED);
			fom.modPOrder(oid, order);
			
			long dev_oid = this.saveWarrantyOrder(request);
		}
		catch (OrderNotExistException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markWarrantied",log);
		}
	}

	/**
	 * 手动关联有偿质保单
	 * @param request
	 * @throws OrderNotExistException
	 * @throws Exception
	 */
	public void linkWarrantied(HttpServletRequest request)
		throws OrderNotExistException,Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			long addition_money_oid = StringUtil.getLong(request, "addition_money_oid");
			long wo_id = StringUtil.getLong(request, "wo_id");
			
			if ( fom.getDetailPOrderByOid(addition_money_oid)==null )
			{
				throw new OrderNotExistException();
			}
			
			DBRow warrantyOrder = new DBRow();
			warrantyOrder.add("dev_oid",addition_money_oid);//质保订单
			fom.modWarrantyOrder(wo_id, warrantyOrder);
			
			this.addPOrderNotePrivate(oid, "创建有偿质保订单", TracingOrderKey.WARRANTIED, StringUtil.getSession(request),addition_money_oid);
			this.addPOrderNotePrivate(addition_money_oid, "有偿质保订单", TracingOrderKey.WARRANTIED, StringUtil.getSession(request),oid);
		}
		catch (OrderNotExistException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"linkWarrantied",log);
		}
	}

	/**
	 * 其他争议
	 * @param request
	 * @throws Exception
	 */
	public void markOtherDispute(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.OTHER_DISPUTING);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.OTHER_DISPUTING, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markOtherDispute",log);
		}
	}
	
	/**
	 * 撤销退款
	 * @param request
	 * @throws Exception
	 */
	public void cancelRefund(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			
			DBRow order = new DBRow();
			order.add("after_service_status", 0);//重置回正常状态
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.CANCEL_REFUND, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelRefund",log);
		}
	}

	/**
	 * 撤销质保
	 * @param request
	 * @throws Exception
	 */
	public void cancelWarranty(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
			
			DBRow order = new DBRow();
			order.add("after_service_status",0);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.CANCEL_WARRANTY, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelWarranty",log);
		}
	}
		
	/**
	 * 撤销争议
	 * @param request
	 * @throws Exception
	 */
	public void cancelDispute(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");

			DBRow order = new DBRow();
			order.add("after_service_status", 0);
			fom.modPOrder(oid, order);

			this.addPOrderNotePrivate(oid, note, TracingOrderKey.CANCEL_DISPUTE, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelDispute",log);
		}
	}
			
	public DBRow[] getTraceDisputeRefundOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getTraceDisputeRefundOrders( trace_operator_role_id, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceDisputeRefundOrders",log);
		}
	}
		
	public DBRow[] getTraceDisputeWarrantyOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getTraceDisputeWarrantyOrders( trace_operator_role_id, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceDisputeWarrantyOrders",log);
		}
	}
	
	public DBRow[] getTraceOtherDisputeOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getTraceOtherDisputeOrders(  trace_operator_role_id,pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getTraceOtherDisputeOrders",log);
		}
	}
			
	public void markFailureRefund(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");

			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.FAILURE_REFUND);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.FAILURE_REFUND, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markFailureRefund",log);
		}
	}

	public long markFreeWarranty(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
	
			DBRow order = new DBRow();
			order.add("after_service_status", AfterServiceKey.FREE_WARRANTIED);
			fom.modPOrder(oid, order);
			
			long dev_oid = this.saveWarrantyOrder(request);
			
			this.addPOrderNotePrivate(oid, "创建无偿质保订单",TracingOrderKey.FREE_WARRANTIED, StringUtil.getSession(request),dev_oid);
			this.addPOrderNotePrivate(dev_oid, "无偿质保订单",TracingOrderKey.FREE_WARRANTIED, StringUtil.getSession(request),oid);
			
			return(dev_oid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markFreeWarranty",log);
		}
	}
	
	public long markFeeShippingFeeWarranty(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			
			DBRow order = new DBRow();
			order.add("after_service_status", AfterServiceKey.FREE_SHIPPING_FEE_WARRANTY);
			fom.modPOrder(oid, order);
			
			long dev_oid = this.saveWarrantyOrder(request);
			this.addPOrderNotePrivate(oid, "创建免运费质保订单",TracingOrderKey.FREE_SHIPPING_FEE_WARRANTY, StringUtil.getSession(request),dev_oid);
			this.addPOrderNotePrivate(dev_oid, "免运费质保订单",TracingOrderKey.FREE_SHIPPING_FEE_WARRANTY, StringUtil.getSession(request),oid);
			
			return(oid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markFeeShippingFeeWarranty",log);
		}
	}
	
	public void markFailureWarranty(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
	
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.FAILURE_WARRANTY);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.FAILURE_WARRANTY, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markFailureWarranty",log);
		}
	}

	public void markFailureDispute(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
	
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.FAILURE_DISPUTING);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.FAILURE_DISPUTING, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markFailureDispute",log);
		}
	}


	public void markBadRefund(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
	
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.BAD_REFUND);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.BAD_REFUND, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markBadRefund",log);
		}
	}

	public void markBadWarranty(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
	
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.BAD_WARRANTY);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.BAD_WARRANTY, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markBadWarranty",log);
		}
	}

	public void markBadDispute(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			String note = StringUtil.getString(request, "note");
	
			DBRow order = new DBRow();
			
			order.add("after_service_status", AfterServiceKey.BAD_DISPUTING);
			fom.modPOrder(oid, order);
			
			this.addPOrderNotePrivate(oid, note, TracingOrderKey.BAD_DISPUTING, StringUtil.getSession(request),0);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markBadDispute",log);
		}
	}
	

	/**
	 * 检查主订单发货仓库是否跟子订单一致
	 * @param request
	 * @throws Exception
	 */
	public void checkDeliveryStorage(HttpServletRequest request)
		throws DeliveryStorageNotSameException,Exception
	{
		try
		{
			long ps_id = StringUtil.getLong(request, "ps_id");
			int parentid = StringUtil.getInt(request, "parentid");
			long oid = StringUtil.getLong(request, "oid");
			
			//有子订单
			if ( parentid<0 )
			{
				DBRow sonOrders[] = fom.getSonOrders(oid, null);
				
				for (int i=0; i<sonOrders.length; i++)
				{
					if (ps_id!=sonOrders[i].get("ps_id", 0l))
					{
						throw new DeliveryStorageNotSameException();
					}
				}
			}
		}
		catch (DeliveryStorageNotSameException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"checkDeliveryStorage",log);
		}
	}
	
	/**
	 * 获得订单最小关税的关税代码
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public String getHsCode(long oid)
		throws Exception
	{
		try
		{
			HashMap<String, DBRow> catalog = new HashMap<String, DBRow>();
			
			DBRow orderItems[] = fom.getPOrderItemsByOid(oid);
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			//先找到订单所有商品所属的所有父类
			//把父类的tax起来
			for (int i=0; i<orderItems.length; i++)
			{
				DBRow fathers[] = tree.getAllFather(orderItems[i].get("catalog_id", 0l));
				
				if (catalog.containsKey(fathers[0].get("id", 0l))==false)
				{
					catalog.put(fathers[0].getString("id"), fathers[0]);
				}
			}
			//去除该订单包含的最低tax
			Set<String> keys = catalog.keySet();
			String maxTaxId=null;
			float tmpTax = 0;
			int i = 0;
			for(String key:keys)
			{   
				DBRow c = (DBRow)catalog.get(key);
				if (c.getString("hs_code").equals(""))
				{
					continue;
				}
				if (i++==0)
				{
					maxTaxId = c.getString("hs_code");
					tmpTax = c.get("tax",0f);
				}
				if (tmpTax>c.get("tax",0f))
				{
					maxTaxId = c.getString("hs_code");
					tmpTax = c.get("tax",0f);
				}
			}
			
			return(maxTaxId);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"Exception",log);
		}
	}
	
	/**
	 * 重新计算提交给快递服务器的重量
	 * @param weight
	 * @return
	 * @throws Exception 
	 */
	public float calculateWayBillWeight(long sc_id,float weight) 
		throws Exception
	{
		//因为传进来的重量是经过修正的，所以要先转换成实际重量，再减少一点
//		float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
		float print_weight_coefficient = expressMgr.getDetailCompany(sc_id).get("print_weight_discount", 0f);
		////system.out.println("0> "+weight);
//		weight = weight/weight_cost_coefficient;
		////system.out.println("1> "+weight);
		weight = weight*print_weight_coefficient;
		////system.out.println("2> "+weight);
		
		return(weight);
	}

	/**
	 * 订单实际重量
	 * @param weight
	 * @return
	 * @throws Exception
	 */
	public float calculateWayBillWeight4ST(float weight) 
		throws Exception
	{
		//因为传进来的重量是经过修正的，所以要先转换成实际重量，再减少一点
		float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
		
		weight = weight/weight_cost_coefficient;
		
		return(weight);
	}
	
	/**
	 * 修改订单邮编
	 * @param oid
	 * @param address_zip
	 * @throws Exception
	 */
	public void modAddressZip(long oid,String address_zip)
		throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("address_zip",address_zip);
			fom.modPOrder(oid,row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modAddressZip",log);
		}
	}

	/**
	 * 创建退货单
	 * @param request
	 * @throws Exception
	 */
	public long addReturnProducts(HttpServletRequest request,boolean flag)
		throws Exception
	{
		try
		{
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminInfo = am.getAdminLoginBean( StringUtil.getSession(request) );
			long rp_id = StringUtil.getLong(request, "rp_id");
			long oid = StringUtil.getLong(request, "oid");
			long ps_id = StringUtil.getLong(request, "ps_id");
			
			if (rp_id==0) //rp_id>0是修改，所以无需插入退货主体信息
			{
				//首先插入退货单主信息
				int return_product_reason = StringUtil.getInt(request, "return_product_reason");
				long create_user = adminInfo.getAdid();
				String create_date = DateUtil.NowStr();
				String handle_user = "";
				String handle_date = create_date;
				
				DBRow returnProductInfo = new DBRow();
				returnProductInfo.add("create_user", create_user);
				returnProductInfo.add("create_date", create_date);
				returnProductInfo.add("handle_user", handle_user);
				returnProductInfo.add("handle_date", handle_date);
				returnProductInfo.add("oid", oid);
				returnProductInfo.add("ps_id", ps_id);
				returnProductInfo.add("status", ReturnProductKey.WAITING);
				returnProductInfo.add("handle_date", handle_date);
				returnProductInfo.add("return_product_reason", return_product_reason);
				rp_id = fom.addReturnInfoByRpId(returnProductInfo);
			}
			else
			{
				DBRow returnProductInfo = new DBRow();
				returnProductInfo.add("ps_id", ps_id);
				returnProductInfo.add("status",ReturnProductKey.WAITINGPRODUCT);
				returnProductInfo.add("product_status",ReturnProductStatusKey.Waiting);
				fom.modReturnInfoByRpId(rp_id, returnProductInfo);
			}

			/**
			 * 插入散件的时候，先插入到数据池，如果有重复商品，则累加数量
			 * pid做主键
			 */
			HashMap<String, DBRow> tmpDataPool = new HashMap<String, DBRow>();
			
			//先清空原来的退货商品记录
			fom.delReturnProductItemsByRpId(rp_id);
			fom.delReturnProductSubItemsByRpId(rp_id);
			
			//插入退货主商品
			cartReturn.flush(StringUtil.getSession(request));
			DBRow returnProducts[] = cartReturn.getDetailProducts();
			for (int i=0; i<returnProducts.length; i++)
			{
				DBRow returnProduct = new DBRow();
				returnProduct.add("p_name", returnProducts[i].getString("p_name"));
				returnProduct.add("pid", returnProducts[i].getString("cart_pid"));
				returnProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
				returnProduct.add("unit_name", returnProducts[i].getString("unit_name"));
				returnProduct.add("product_type", returnProducts[i].getString("cart_product_type"));
				returnProduct.add("rp_id", rp_id);
				long rpi_id = fom.addReturnProductItems(returnProduct);
				
				//分解套装成散件
				if (returnProducts[i].getString("cart_product_type").equals(String.valueOf(ProductTypeKey.UNION_STANDARD)))
				{
					DBRow returnSubProducts[] = fpm.getProductsInSetBySetPid(returnProducts[i].get("pc_id", 0l));
					for (int j=0; j<returnSubProducts.length; j++)
					{
						if (tmpDataPool.containsKey(returnSubProducts[j].getString("pc_id")))
						{
							DBRow returnSubProduct =  tmpDataPool.get(returnSubProducts[j].getString("pc_id"));
							returnSubProduct.add("quantity", StringUtil.getFloat(returnSubProduct.getString("quantity"))+returnSubProducts[j].get("quantity", 0f)*StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
							tmpDataPool.put(returnSubProducts[j].getString("pc_id"), returnSubProduct);
						}
						else
						{
							DBRow returnSubProduct = new DBRow();
							returnSubProduct.add("p_name", returnSubProducts[j].getString("p_name"));
							returnSubProduct.add("pid", returnSubProducts[j].getString("pc_id"));
							returnSubProduct.add("quantity", returnSubProducts[j].get("quantity", 0f)*StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
							returnSubProduct.add("unit_name", returnSubProducts[j].getString("unit_name"));
							returnSubProduct.add("rpi_id", rpi_id);
							
							tmpDataPool.put(returnSubProducts[j].getString("pc_id"), returnSubProduct);
						}
					}
				}
				else
				{
					//普通商品直接插入
					if (tmpDataPool.containsKey(returnProducts[i].getString("cart_pid")))
					{
						DBRow returnSubProduct =  tmpDataPool.get(returnProducts[i].getString("cart_pid"));
						returnSubProduct.add("quantity", StringUtil.getFloat(returnSubProduct.getString("quantity"))+StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
						tmpDataPool.put(returnProducts[i].getString("cart_pid"), returnSubProduct);
					}
					else
					{
						DBRow returnSubProduct = new DBRow();
						returnSubProduct.add("p_name", returnProducts[i].getString("p_name"));
						returnSubProduct.add("pid", returnProducts[i].getString("cart_pid"));
						returnSubProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
						returnSubProduct.add("unit_name", returnProducts[i].getString("unit_name"));
						returnSubProduct.add("rpi_id", rpi_id);
						
						tmpDataPool.put(returnProducts[i].getString("cart_pid"), returnSubProduct);
					}
				}
				
			}
			
			if(returnProducts.length>0)
			{
				DBRow para = new DBRow();
				para.add("status",ReturnProductKey.WAITINGPRODUCT);
				fom.modReturnInfoByRpId(rp_id, para);
			}

			//最后插入散件
			Set<String> keys = tmpDataPool.keySet();   
			for(String key:keys)
			{
				fom.addReturnProductSubItems(tmpDataPool.get(key));
			}

			//清空退货购物车
			cartReturn.clearCart(StringUtil.getSession(request));
			
			//发送邮件通知顾客,当正常质保的时候则不直接发送邮件给客户
			if(flag)
			{
				DBRow detailOrder = fom.getDetailPOrderByOid(oid);
				
				ReturnVoiceEmailPageString returnVoiceEmailPageString = new ReturnVoiceEmailPageString(rp_id,oid);
				User user = new User(returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getPassWord(),returnVoiceEmailPageString.getHost(),returnVoiceEmailPageString.getPort(),returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getAuthor(),returnVoiceEmailPageString.getRePly());
	            MailAddress mailAddress = new MailAddress(detailOrder.getString("client_id"));
	            MailBody mailBody = new MailBody(returnVoiceEmailPageString,true,null);
	
	            Mail mail = new Mail(user);
	            mail.setAddress(mailAddress);
	            mail.setMailBody(mailBody);
	            mail.send();
			}
			return(rp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addReturnProducts",log);
		}
	}
	
	/**
	 * 通过退货单号，获得退货单主商品信息
	 * @param rp_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnProductItemsByRpId(long rp_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getReturnProductItemsByRpId( rp_id, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getReturnProductItemsByRpId",log);
		}
	}

	/**
	 * 通过退货单号，获得退货单散件商品信息
	 * @param rp_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnProductSubItemsByRpId(long rp_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getReturnProductSubItemsByRpId( rp_id, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getReturnProductSubItemsByRpId",log);
		}
	}
	
	/**
	 * 退货单详细信息
	 * @param rp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailReturnProductByRpId(long rp_id)
		throws Exception
	{
		try
		{
			return(fom.getDetailReturnProductByRpId(rp_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailReturnProductByRpId",log);
		}
	}
	
	/**
	 * 初始化退货购物车
	 * @param request
	 * @param rp_id
	 * @throws Exception
	 */
	public void initCartReturn(HttpServletRequest request,long rp_id)
		throws Exception
	{
		try
		{
			cartReturn.clearCart(StringUtil.getSession(request));
			
			DBRow returnProductItems[] = fom.getReturnProductItemsByRpId( rp_id,null);
			for (int i=0; i<returnProductItems.length; i++)
			{
				cartReturn.put2Cart(StringUtil.getSession(request),returnProductItems[i].get("pid", 0l), returnProductItems[i].get("quantity", 0f),returnProductItems[i].get("product_type", 0));
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"initCartReturn",log);
		}
	}
	
	/**
	 * 获得所有退货单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllReturnProduct(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getAllReturnProduct(pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllReturnProduct",log);
		}
	}
	
	/**
	 * 通过退货单号，搜索退货单
	 * @param rp_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchReturnProductByRpId(long rp_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.searchReturnProductByRpId( rp_id, pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"searchReturnProductByRpId",log);
		}
	}
	
	/**
	 * 登记退货
	 * @param request
	 * @throws Exception
	 */
	public void sigReturnProducts(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long rp_id = StringUtil.getLong(request,"rp_id");
			String rpsi_id[] = request.getParameterValues("rpsi_id");
			String note = StringUtil.getString(request, "note");
			float quality_nor,quality_damage,quality_package_damage;
			long pid;

			DBRow detailReturnInfo = fom.getDetailReturnProductByRpId(rp_id);
			
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true) );
			
			for (int i=0; i<rpsi_id.length; i++)
			{
				quality_nor = StringUtil.getFloat(request, rpsi_id[i]+"_nor");
				quality_damage = StringUtil.getFloat(request, rpsi_id[i]+"_damage");
				quality_package_damage = StringUtil.getFloat(request, rpsi_id[i]+"_package_damage");
				pid = StringUtil.getLong(request, rpsi_id[i]+"_pid");
				
				DBRow returnSubProductItem = new DBRow();
				returnSubProductItem.add("quality_nor", quality_nor);
				returnSubProductItem.add("quality_damage", quality_damage);
				returnSubProductItem.add("quality_package_damage", quality_package_damage);
				fom.modReturnProductSubItemsByRpsiId(StringUtil.getLong(rpsi_id[i]), returnSubProductItem);
				
				DBRow detailProduct = fpm.getDetailProductByPcid(pid);
				//完好商品进库
				if (quality_nor>0)
				{
					ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
					inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
					inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
					inProductStoreLogBean.setOid(rp_id);
					inProductStoreLogBean.setBill_type(ProductStoreBillKey.RETURN_ORDER);
					inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_RETURN);
					
					fpm.incProductStoreCountByPcid(inProductStoreLogBean, detailReturnInfo.get("ps_id", 0l), pid, quality_nor,0);
					//记录入库日志
					ProductMgr productMgr = (ProductMgr)MvcUtil.getBeanFromContainer("productMgr");				
					TDate td = new TDate();
					long datemark = td.getDateTime();
					
					productMgr.addInProductLog(detailProduct.getString("p_code"), detailReturnInfo.get("ps_id", 0l), quality_nor, adminLoggerBean.getAdid(), datemark, 1, InOutStoreKey.IN_STORE_RETURN,detailReturnInfo.get("oid", 0l));
				}
				//功能残损进库
				if (quality_damage>0)
				{
					//增加残损件数目
					fpm.incProductDamagedCountByPcid(adminLoggerBean.getPs_id(),pid,quality_damage);
					
					//记录残损件入库日志
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(rp_id);
					inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.RETURN_ORDER);
					inProductDamagedCountLogBean.setPs_id(detailReturnInfo.get("ps_id", 0l));
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN);
					inProductDamagedCountLogBean.setQuantity(quality_damage);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(detailProduct.get("pc_id",0l));
					productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
				}
				//包装残损进库
				if (quality_package_damage>0)
				{
					//增加残损件数目
					fpm.incProductPackageDamagedCountByPcid(adminLoggerBean.getPs_id(),pid,quality_package_damage);
					
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(rp_id);
					inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.RETURN_ORDER);
					inProductDamagedCountLogBean.setPs_id(detailReturnInfo.get("ps_id", 0l));
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN);
					inProductDamagedCountLogBean.setQuantity(quality_package_damage);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(detailProduct.get("pc_id",0l));
					productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
				}
			}

			//更新退货单状态
			DBRow returnInfo = new DBRow();
			returnInfo.add("status", ReturnProductKey.FINISH);
			returnInfo.add("handle_user", adminLoggerBean.getAdid());
			returnInfo.add("handle_date", DateUtil.NowStr());
			returnInfo.add("note", note);
			returnInfo.add("product_status",ReturnProductStatusKey.GetProduct);
			fom.modReturnInfoByRpId(rp_id,returnInfo);
			//对相关订单增加备注
			FloorCatalogMgr fcm = (FloorCatalogMgr)MvcUtil.getBeanFromContainer("floorCatalogMgr");
			String orderNote = fcm.getDetailProductStorageCatalogById(adminLoggerBean.getPs_id()).getString("title")+" warehouse 已经收到退件";
			this.addPOrderNotePrivate(detailReturnInfo.get("oid", 0l), orderNote, TracingOrderKey.RETURN_PRODUCT, StringUtil.getSession(request),rp_id);
			
			//发送确认邮件通知顾客
			DBRow detailOrder = fom.getDetailPOrderByOid(detailReturnInfo.get("oid",0l));
			ReturnReceiveEmailPageString returnReceiveEmailPageString = new ReturnReceiveEmailPageString(rp_id,detailReturnInfo.get("oid",0l));
			User user = new User(returnReceiveEmailPageString.getUserName(),returnReceiveEmailPageString.getPassWord(),returnReceiveEmailPageString.getHost(),returnReceiveEmailPageString.getPort(),returnReceiveEmailPageString.getUserName(),returnReceiveEmailPageString.getAuthor(),returnReceiveEmailPageString.getRePly());
            MailAddress mailAddress = new MailAddress(detailOrder.getString("client_id"));
            MailBody mailBody = new MailBody(returnReceiveEmailPageString,true,null);
            
            Mail mail = new Mail(user);
            mail.setAddress(mailAddress);
            mail.setMailBody(mailBody);
            mail.send();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"sigReturnProducts",log);
		}
	}

	/**
	 * 取消退货 
	 * @param request
	 * @throws Exception
	 */
	public void cancelReturn(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long rp_id = StringUtil.getLong(request,"rp_id");
			
			DBRow returnInfo = new DBRow();
			returnInfo.add("status", ReturnProductKey.CANCEL);
			fom.modReturnInfoByRpId(rp_id, returnInfo);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelReturn",log);
		}
	}
	
	/**
	 * 获得所有退货原因
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllReturnProductReasons()
		throws Exception
	{
		try
		{
			String reasonStr = systemConfig.getStringConfigValue("return_product_reason");
			ArrayList reasonAL = new ArrayList();
			String reasonA[] = reasonStr.split("\n");
			
			for (int i=0; i<reasonA.length; i++)
			{
				DBRow row = new DBRow();
				row.add("id", reasonA[i].split("=")[0]);
				row.add("value", reasonA[i].split("=")[1]);
				
				reasonAL.add(row);
			}
			
			return((DBRow[])reasonAL.toArray(new DBRow[0]));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllReturnProductReasons",log);
		}
	}
	
	/**
	 * 获得退货原因
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public String getReturnProductReasonById(int id)
		throws Exception
	{
		try
		{
			String reasonStr = systemConfig.getStringConfigValue("return_product_reason");
			String reasonA[] = reasonStr.split("\n");
			
			for (int i=0; i<reasonA.length; i++)
			{
				if ( StringUtil.getInt(reasonA[i].split("=")[0])==id )
				{
					return(reasonA[i].split("=")[1]);
				}
			}
			
			return("");
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getReturnProductReasonById",log);
		}
	}
	
	/**
	 * 获得所有差评原因
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllBadFeedBackReasons()
		throws Exception
	{
		try
		{
			String reasonStr = systemConfig.getStringConfigValue("bad_feedback_reason");
			ArrayList reasonAL = new ArrayList();
			String reasonA[] = reasonStr.split("\n");
			
			for (int i=0; i<reasonA.length; i++)
			{
				DBRow row = new DBRow();
				row.add("id", reasonA[i].split("=")[0]);
				row.add("value", reasonA[i].split("=")[1]);
				
				reasonAL.add(row);
			}
			
			return((DBRow[])reasonAL.toArray(new DBRow[0]));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllBadFeedBackReasons",log);
		}
	}
	
	/**
	 * 获得详细差评原因
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public String getBadFeedBackReasonById(int id)
		throws Exception
	{
		try
		{
			String reasonStr = systemConfig.getStringConfigValue("bad_feedback_reason");
			String reasonA[] = reasonStr.split("\n");
			
			for (int i=0; i<reasonA.length; i++)
			{
				if ( StringUtil.getInt(reasonA[i].split("=")[0])==id )
				{
					return(reasonA[i].split("=")[1]);
				}
			}
			
			return("");
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getBadFeedBackReasonById",log);
		}
	}
	
	/**
	 * 登记差评
	 * @param request
	 * @throws Exception
	 */
	public void markBadFeedBack(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			int status = StringUtil.getInt(request, "status");
			String badFeedbackReasons[] = request.getParameterValues("bad_feedback_reason");
			
			//先把订单标记为差评订单
			DBRow order = new DBRow();
			order.add("bad_feedback_flag",status);
			fom.modPOrder(oid, order);
			//插入差评原因关系
			for (int i=0; badFeedbackReasons!=null&&i<badFeedbackReasons.length; i++)
			{
				DBRow badFeedbackReason = new DBRow();
				badFeedbackReason.add("oid", oid);
				badFeedbackReason.add("bf_id", badFeedbackReasons[i]);
				fom.addBadFeedBack(badFeedbackReason);
			}
			
			//添加任务。(代替以前的工作流)
			scheduleMgrZr.addScheduleByWorkFlowOrder(request);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"markBadFeedBack",log);
		}
	}
	
	/**
	 * 获得订单差评原因
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBadFeedBacksByOid(long oid)
		throws Exception
	{
		try
		{
			DBRow badFeedBacks[] = fom.getBadFeedBacksByOid(oid);
			
			for (int i=0; i<badFeedBacks.length; i++)
			{
				//附加上差评原因
				badFeedBacks[i].add("name", this.getBadFeedBackReasonById(StringUtil.getInt(badFeedBacks[i].getString("bf_id"))) );
			}
			
			return(badFeedBacks);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getBadFeedBacksByOid",log);
		}
	}
	
	/**
	 * 把超过30天没处理的退货单取消掉
	 * @throws Exception
	 */
	public void cancelExpireReturnInfo()
		throws Exception
	{
		try
		{
			DBRow returnList[] = fom.getExpireReturnInfo(100);//取消过期60天
			for (int i=0; i<returnList.length; i++)
			{
				DBRow status = new DBRow();
				status.add("status", ReturnProductKey.CANCEL);
				fom.modReturnInfoByRpId(returnList[i].get("rp_id", 0l), status);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"cancelExpireReturnInfo",log);
		}
	}
	
	/**
	 * 临时更新订单快递公司ID
	 * @throws Exception
	 */
	public void updateOrderScIdTMP()
		throws Exception
	{
		try
		{
			fom.updateOrderScIdTMP();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateOrderScIdTMP",log);
		}
	}
	
	/**
	 * 临时更新发货快递公司ID
	 * @throws Exception
	 */
	public void updateShippingScIdTMP()
		throws Exception
	{
		try
		{
			fom.updateShippingScIdTMP();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateShippingScIdTMP",log);
		}
	}
	
	/**
	 * 修改订单ebay卖家ID
	 * @param oid
	 * @param seller_id
	 * @throws Exception
	 */
	public void modSellerId(long oid,String seller_id)
		throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("seller_id",seller_id);
			fom.modPOrder(oid,row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modSellerId",log);
		}
	}
	
	public void modBuyerId(long oid,String buyer_id)
		throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("auction_buyer_id",buyer_id);
			fom.modPOrder(oid,row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modBuyerIdz",log);
		}
	}
	
	public void updateSellerID()
		throws Exception
	{
		try
		{
			fom.updateSellerID();
		}
		catch (Exception e)
		{
			throw new Exception("updateSellerID(row) error:" + e);
		}
	}
		
	/**
	 * 获得所有国家，但排除当前仓库所在国家
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllCountrysRejPsid(long ps_id)
		throws Exception
	{
		try
		{
			return(fom.getAllCountrysRejPsid(ps_id));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllCountrysRejPsid",log);
		}
	}
	
	/**
	 * 修改退货单，退货仓库
	 * @param request
	 * @throws Exception
	 */
	public void modReturnProductWareHouse(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long rp_id = StringUtil.getLong(request, "rp_id");
			long ps_id  = StringUtil.getLong(request, "ps_id");
			
			DBRow newWareHouse = new DBRow();
			newWareHouse.add("ps_id", ps_id);
			fom.modReturnInfoByRpId(rp_id, newWareHouse);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modReturnProductWareHouse",log);
		}
	}
	
	/**
	 * 更新订单售后服务状态
	 * @throws Exception
	 */
	public void updateAfterServiceStatus()
		throws Exception
	{
		try
		{
			fom.updateAfterServiceStatus();
		}
		catch (Exception e)
		{
			throw new Exception("updateAfterServiceStatus(row) error:" + e);
		}
	}
	
	/**
	 * 创建质保订单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addWarrantyOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			return(0);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addWarrantyOrder",log);
		}
	}
	
	/**
	 * 获得所有质保订单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllWarrantyOrders(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getAllWarrantyOrders(pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllWarrantyOrders",log);
		}
	}
		

	/**
	 * 保存质保单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long saveWarrantyOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
				long devOid = 0;
				
				int type = StringUtil.getInt(request,"type");//1-有偿质保 2-无偿质保 3-免运费质保
				String business = StringUtil.getString(request, "business");
				float quantity = StringUtil.getFloat(request, "quantity");
				double mc_gross = StringUtil.getDouble(request, "mc_gross");
				String mc_currency = StringUtil.getString(request, "mc_currency");
				long oid = StringUtil.getLong(request, "oid");
				long pro_id = StringUtil.getLong(request, "pro_id");
				int reason = StringUtil.getInt(request, "reason");
				
				long ccid = StringUtil.getLong(request, "ccid");
				String address_name = StringUtil.getString(request, "address_name");
				String address_street = StringUtil.getString(request, "address_street");
				String address_city = StringUtil.getString(request, "address_city");
				String address_state = StringUtil.getString(request, "address_state");
				String address_zip = StringUtil.getString(request, "address_zip");
				String tel = StringUtil.getString(request, "tel");
				
				
				AdminLoginBean adminInfo = new AdminMgr().getAdminLoginBean( StringUtil.getSession(request) );
				
				DBRow porder = new DBRow();
				porder.add("business",business);
				porder.add("quantity",quantity);
				porder.add("mc_gross",mc_gross);
				porder.add("mc_currency",mc_currency);
				porder.add("reason",reason);
				porder.add("oid",oid);
				porder.add("creater",adminInfo.getAdid());
				porder.add("create_date",DateUtil.NowStr());
				porder.add("dev_oid",0);
				porder.add("pro_id",pro_id);
				porder.add("ccid",ccid);
				porder.add("address_name",address_name);
				porder.add("address_street",address_street);
				porder.add("address_city",address_city);
				porder.add("address_city",address_city);
				porder.add("address_state",address_state);
				porder.add("address_zip",address_zip);
				porder.add("tel",tel);
				
				if (type==3||type==2)
				{
					porder.add("pay_type",PayTypeKey.Free);
				}
				else
				{
					porder.add("pay_type",PayTypeKey.PayPal);
				}
				
				long wo_id = fom.addWarrantyOrder(porder);
				
				//生成发货订单
				if (type==3||type==2)
				{
					DBRow orgOrder = fom.getDetailPOrderByOid(oid);
					
					DBRow devOrder = new DBRow();
					devOrder.add("business",business);
					devOrder.add("quantity",quantity);
					devOrder.add("mc_gross",mc_gross);
					devOrder.add("mc_currency",mc_currency);
					devOrder.add("post_date",DateUtil.NowStr());
					devOrder.add("client_id",orgOrder.getString("client_id"));
					devOrder.add("auction_buyer_id",orgOrder.getString("auction_buyer_id"));
					devOrder.add("order_source",OrderMgr.ORDER_SOURCE_WANRRANTY);
					devOrder.add("handle",HandleKey.WAIT4_RECORD);
					devOrder.add("handle_status",HandStatusleKey.DOUBT);
					devOrder.add("pay_type",PayTypeKey.Free);
					
					
					devOrder.add("pro_id",pro_id);
					
					if(pro_id!=-1)//省份并非other
					{
						DBRow detailProvince = fpm.getDetailProvinceByProId(pro_id);
						if(detailProvince!=null)
						{
							address_state = detailProvince.getString("pro_name");
						}
					}

					devOrder.add("ccid",ccid);
					
					DBRow detailCountry = fom.getDetailCountryCodeByCcid(StringUtil.getLong(porder.getString("ccid")));
					
					devOrder.add("address_country",detailCountry.getString("c_country"));
					
					devOrder.add("address_name",address_name);
					devOrder.add("address_street",address_street);
					devOrder.add("address_city",address_city);
					devOrder.add("address_city",address_city);
					devOrder.add("address_state",address_state);
					devOrder.add("address_zip",address_zip);
					devOrder.add("tel",tel);
					
					devOid = fom.addPOrder(devOrder);
					//质保发货订单地址验证
					ordersMgrQLL.addressValidateFedexJMS(devOid,devOrder.getString("address_street"),devOrder.getString("address_zip"));
					
					OrderIndexMgr.getInstance().addIndex("",orgOrder.getString("client_id"),devOid,"",orgOrder.getString("auction_buyer_id"),"","","");
					//记录质保单号
					DBRow modWarrantyOrder = new DBRow();
					modWarrantyOrder.add("dev_oid",devOid);
					fom.modWarrantyOrder(wo_id, modWarrantyOrder);
				}
				
				fom.delWarrantyOrderItemsByWoId(wo_id);
				//重新计算购物车数据后，获得详细信息
				cartWarranty.flush(StringUtil.getSession(request));
				DBRow cartProducts[] = cartWarranty.getDetailProducts();
				
				long ps_id = productMgr.getPriorDeliveryWarehouse(ccid, pro_id);//最优发货
				
				for (int i=0; i<cartProducts.length; i++)
				{
							if (cartProducts[i].get("cart_product_type", 0)==CartWarranty.UNION_CUSTOM)
							{
								//先把旧的定制商品删掉
								fpm.delProductCustomUnionBySetPid(StringUtil.getLong(cartProducts[i].getString("cart_pid")));
								DBRow productsCustomInSet[] = customCartWarranty.getFinalDetailProducts(StringUtil.getSession(request), StringUtil.getLong(cartProducts[i].getString("cart_pid")));
								for (int j=0; j<productsCustomInSet.length; j++)
								{
										//插入到定制表
										DBRow customPro = new DBRow();
										customPro.add("set_pid",cartProducts[i].getString("cart_pid"));
										customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
										customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
										fpm.addProductCustomUnion(customPro);
								}
							}

							DBRow orderItem = new DBRow();
							orderItem.add("name",cartProducts[i].getString("p_name"));
							orderItem.add("pid",cartProducts[i].getString("cart_pid"));
							orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
							orderItem.add("wo_id",wo_id);
							orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
							orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
							fom.addWarrantyOrderItems(orderItem);
							
							//生成发货订单
							if (type==3||type==2)
							{
								DBRow devOrderItems = new DBRow();

								devOrderItems.add("pid", cartProducts[i].getString("cart_pid"));
								devOrderItems.add("wait_quantity", cartProducts[i].getString("cart_quantity"));//保存质保单时，质保单购物车里的数量就是待发数量
								devOrderItems.add("quantity", cartProducts[i].getString("cart_quantity"));
								devOrderItems.add("catalog_id", cartProducts[i].getString("catalog_id"));
								devOrderItems.add("unit_price", cartProducts[i].get("unit_price",0d));
								devOrderItems.add("gross_profit", cartProducts[i].get("gross_profit",0d));
								devOrderItems.add("weight", cartProducts[i].get("weight",0f));
								devOrderItems.add("unit_name", cartProducts[i].getString("unit_name"));
								devOrderItems.add("name", cartProducts[i].getString("p_name"));
								devOrderItems.add("product_type", cartProducts[i].getString("cart_product_type"));
								devOrderItems.add("oid", devOid);
								
								devOrderItems.add("prefer_ps_id",ps_id);
								devOrderItems.add("modality",1);//1代表原状
								long plan_ps_id = ps_id; 
								
								DBRow storageTransd = fpm.getStorageTransd(ps_id,cartProducts[i].get("catalog_id", 0l));
								if(storageTransd!=null)
								{
									plan_ps_id = storageTransd.get("d_sid",0l);
									devOrderItems.add("modality",storageTransd.get("modality_id",0));
								}
								devOrderItems.add("plan_ps_id",plan_ps_id);
								
								fom.addPOrderItem(devOrderItems);
							}
				}
				cartWarranty.clearCart(StringUtil.getSession(request));				//清空购物车
				
				return(devOid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"saveQuoteProduct",log);
		}
		
	}
	
	/**
	 * 获得质保订单的商品
	 * @param woid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWarrantyOrderItemsByWoId(long woid)
		throws Exception
	{
		try
		{
			return(fom.getWarrantyOrderItemsByWoId(woid));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWarrantyOrderItemsByWoId",log);
		}
	}
		
	/**
	 * 通过客户邮件，获得质保订单列表
	 * @param email
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWarrantyOrdersByPayerEmail(String email,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fom.getWarrantyOrdersByPayerEmail(email,pc));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWarrantyOrdersByPayerEmail",log);
		}
	}
		
	/**
	 * 通过原始订单号，获得质保订单列表
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWarrantyOrdersByOid(long oid)
		throws Exception
	{
		try
		{
			return(null);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWarrantyOrdersByOid",log);
		}
	}
	
	/**
	 * 通过质保单号，获得质保订单
	 * @param woid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWarrantyOrdersByWoid(long woid)
		throws Exception
	{
		try
		{
			return(null);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWarrantyOrdersByWoid",log);
		}
	}
	
	/**
	 * 获得质保原因
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public String getWarrantyReasonById(int id)
		throws Exception
	{
		try
		{
			String reasonStr = systemConfig.getStringConfigValue("warranty_reason");
			String reasonA[] = reasonStr.split("\n");
			
			for (int i=0; i<reasonA.length; i++)
			{
				if ( StringUtil.getInt(reasonA[i].split("=")[0])==id )
				{
					return(reasonA[i].split("=")[1]);
				}
			}
			
			return("");
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWarrantyReasonById",log);
		}
	}
	
	/**
	 * 获得所有质保原因
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllWarrantyReasons()
		throws Exception
	{
		try
		{
			String reasonStr = systemConfig.getStringConfigValue("warranty_reason");
			ArrayList reasonAL = new ArrayList();
			String reasonA[] = reasonStr.split("\n");
			
			for (int i=0; i<reasonA.length; i++)
			{
				DBRow row = new DBRow();
				row.add("id", reasonA[i].split("=")[0]);
				row.add("value", reasonA[i].split("=")[1]);
				
				reasonAL.add(row);
			}
			
			return((DBRow[])reasonAL.toArray(new DBRow[0]));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllWarrantyReasons",log);
		}
	}
	
	/**
	 * 获得订单发货详细信息
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailShipping(long oid)
		throws Exception
	{
		try
		{
			return(fom.getDetailShipping(oid));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailShipping",log);
		}
	}
	
	/**
	 * 返回待发货数不为0的订单明细
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] waitQuantityNotZero(long oid)
		throws Exception
	{
		return (fom.waitQuantityNotZero(oid));
	}
	
	/**
	 * 订单标记缺货
	 */
	public void orderLacking(HttpServletRequest request)
		throws Exception
	{
		long ps_id = StringUtil.getLong(request,"ps_id");
		PreCalcuOrderBean preCalcuOrderBean = productMgr.preCalcuOrder(request, ps_id);
		DBRow wayBillItems[] = preCalcuOrderBean.getResult();
		
		for (int i = 0; i < wayBillItems.length; i++) 
		{
			if(wayBillItems[i].get("lacking",0)==ProductStatusKey.STORE_OUT)
			{
				//修改订单明细为缺货
				long iid = wayBillItems[i].get("order_item_id",0l);
				DBRow itemPara =  new DBRow();
				itemPara.add("lacking_ps_id",ps_id);
				itemPara.add("lacking",ProductStatusKey.STORE_OUT);
				fom.modPOrderItem(iid,itemPara);
				
				//修改订单为缺货
				DBRow orderItem = fom.getDetailOrderItem(iid);
				DBRow paraOrder = new DBRow();
				paraOrder.add("product_status",ProductStatusKey.STORE_OUT);
				paraOrder.add("handle",HandleKey.WAIT4_RECORD);
				fom.modPOrder(orderItem.get("oid",0l),paraOrder);
			}
		}
	}
	
	/**
	 * 订单全部上运单
	 * @param oid
	 * @throws Exception
	 */
	public void orderShipment(long oid)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add("handle",HandleKey.ALLOUTBOUND);
		
		fom.modPOrder(oid,para);
	}
	
	public void orderNoShipment(long oid)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add("handle",HandleKey.OUTBOUNDING);
	}
	
	/**
	 * 运单操作后，对订单状态的修改
	 * @param oid
	 * @throws Exception
	 */
	public void changeOrderHandle(long oid)
		throws Exception
	{
		DBRow[] wait_order_items = fom.waitQuantityNotZero(oid);
		DBRow[] no_ship_order_items = fom.noShipmentItems(oid);
		
		DBRow[] order_items = fom.getPOrderItemsByOid(oid);
		
		DBRow para = new DBRow(); 
		if(no_ship_order_items.length==order_items.length)//未上运单数与订单明细数相等，待出库
		{
			para.add("handle",HandleKey.WAIT4_OUTBOUND);
		}
		else
		{
			if(wait_order_items==null||wait_order_items.length==0)//订单明细上待发货数不等于0的没有，全部出库
			{
				para.add("handle",HandleKey.ALLOUTBOUND);
			}
			else//出库中
			{
				para.add("handle",HandleKey.OUTBOUNDING);
			}
		}
		
		fom.modPOrder(oid,para);
	}
	
	public DBRow[] statisticsOrder(String st,String en,String p_name,long product_line_id,long catalog_id,long ca_id,long ccid,long pro_id,int cost,int cost_type,int unfinished,PageCtrl pc)
		throws Exception
	{
		try 
		{
			DBRow product = fpm.getDetailProductByPname(p_name);
			long pc_id = 0;
			if(product !=null)
			{
				pc_id = product.get("pc_id",0l);
			}
			
			return (fom.statisticsOrder(st, en, pc_id, product_line_id, catalog_id, ca_id, ccid, pro_id, cost, cost_type, unfinished, pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"statisticsOrder",log);
		}
		
	}
	
	public DBRow getDetailOrderItemByIid(long iid)
		throws Exception
	{
		return (fom.getDetailOrderItem(iid));
	}
		
	
	
	public void setFom(FloorOrderMgr fom)
	{
		this.fom = fom;
	}

	public void setSystemConfig(SystemConfig systemConfig)
	{
		this.systemConfig = systemConfig;
	}
	
	public void setCart(Cart cart)
	{
		this.cart = cart;
	}

	public void setFpm(FloorProductMgr fpm)
	{
		this.fpm = fpm;
	}

	public void setTaskMgr(TaskMgr taskMgr)
	{
		this.taskMgr = taskMgr;
	}

	public void setCustomCart(CustomCart customCart)
	{
		this.customCart = customCart;
	}

	
	public void setExpressMgr(ExpressMgr expressMgr)
	{
		this.expressMgr = expressMgr;
	}
	
	public void setCartReturn(CartReturn cartReturn)
	{
		this.cartReturn = cartReturn;
	}

	public void setCartWarranty(CartWarranty cartWarranty) 
	{
		this.cartWarranty = cartWarranty;
	}

	public void setCustomCartWarranty(CustomCartWarranty customCartWarranty)
	{
		this.customCartWarranty = customCartWarranty;
	}
	
	
	
	public static void main(String args[])
		throws Exception
	{
		
//	    TextPara tp = new TextPara();
//	    tp.putPara("hl","en");
//	    tp.putPara("q","1"+currency+"%3D%3FRMB");
//	    
//	    WebRequest wr = new WebRequest();
//	    wr.setPort(80);
//	    wr.setPara(tp);
//	    wr.setProtocol(WebRequest.HTTP_PROTOCOL);
//	    wr.setRequestURI("/ig/calculator");
//	    wr.setServer("www.google.com");
//	    wr.commitGET();
//	   
//	  
//	    String str = wr.getServerResponse();
//	    //system.out.println(str);
		
//		String a = "{lhs: \"1 Norwegian krone\",rhs: \"1.17755009 Chinese yuan\",error: \"\",icc: true}";
//		
//		String aa[] = a.split(",");
//		aa = aa[1].split(":");
//		aa = aa[1].trim().substring(1,aa[1].trim().length()-1).split(" ");
//		//system.out.println(aa[0]);
		
//		String custom = "xp@126.com,http://www.163.com";
//		String tmpA[] =  custom.split(",");
//		
//		//system.out.println(tmpA[0]);
//
//		if (tmpA.length>1)
//		{
//			//system.out.println("vvme_webservice_url:"+tmpA[1].substring(0,tmpA[1].lastIndexOf(":")));
//		}
		
		String s = "payment=paypaldp|payer_phone=123-455-3452";
		////system.out.println( s.split("payer_phone=")[1] );
		
		
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setFloorWayBillOrderMgrZJ(
			FloorWayBillOrderMgrZJ floorWayBillOrderMgrZJ) {
		this.floorWayBillOrderMgrZJ = floorWayBillOrderMgrZJ;
	}

	public void setTradeMgrZJ(TradeMgrIFaceZJ tradeMgrZJ) {
		this.tradeMgrZJ = tradeMgrZJ;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
	
}













