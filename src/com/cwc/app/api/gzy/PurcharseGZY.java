package com.cwc.app.api.gzy;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.gzy.FloorPurchaseGZY;
import com.cwc.app.iface.gzy.PurcharseIFaceGZY;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class PurcharseGZY implements PurcharseIFaceGZY {
	static Logger log = Logger.getLogger("ACTION");
	private FloorPurchaseGZY flp;
	public void setFlp(FloorPurchaseGZY flp) {
		this.flp = flp;
	}
	//根据productlineId获取供应商信息
	public DBRow[] getSupplierByProductline(long productLineId)
			throws Exception {
		try {
			return flp.getSupplierByProductline(productLineId);
		} catch (Exception e) {
			throw new SystemException(e,"getSupplierByProductline",log);
		}
	}
	public long addPurchase(HttpServletRequest request) throws Exception {
		try 
		{
			String supplier=StringUtil.getString(request,"supplier");
			long ps_id=StringUtil.getLong(request,"ps_id");
			String delivery_address = StringUtil.getString(request,"delivery_address");
			String delivery_linkman = StringUtil.getString(request,"delivery_linkman");
			String linkman_phone = StringUtil.getString(request,"linkman_phone");
			
			String purchase_date=DateUtil.NowStr();//交易日期
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			String proposer=adminLoggerBean.getAccount();

			TDate tDate = new TDate();
			tDate.addDay(+1);

			String updatetime=purchase_date;
			
			DBRow dbrow = new DBRow();

			dbrow.add("supplier",supplier);
			dbrow.add("ps_id",ps_id);
			dbrow.add("purchase_date",purchase_date);
			dbrow.add("proposer",proposer);
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			dbrow.add("updatetime",updatetime);
			dbrow.add("delivery_address",delivery_address);
			dbrow.add("delivery_linkman",delivery_linkman);
			dbrow.add("linkman_phone",linkman_phone);
			
			return (flp.addPurchase(dbrow));
		} 
		catch(RuntimeException e) 
		{
			throw new SystemException(e,"addPurchase",log);
		}
	}
}
