package com.cwc.app.api.gzy;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.gzy.FloorSupplierGZY;
import com.cwc.app.iface.gzy.SupplierIFaceGZY;
import com.cwc.app.iface.zr.AccountMgrIfaceZr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class SupplierGZY implements SupplierIFaceGZY{
	
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorSupplierGZY fls; 

	private AccountMgrIfaceZr accountMgrZr ;
	
	public void setFls(FloorSupplierGZY fls) {
		this.fls = fls;
	}
	
	public void setAccountMgrZr(AccountMgrIfaceZr accountMgrZr)
	{
		this.accountMgrZr = accountMgrZr;
	}

	//获取供应商详细信息
	public DBRow getDetailSupplier(long supplierid) throws Exception {
		try {
			return fls.getDetailSupplier(supplierid);
		} catch (Exception e) {
			throw new SystemException(e,"getDetailSupplier",log);
		}
	}

	//获取供应商信息列表
	public DBRow[] getSupplierInfoList(PageCtrl pc) throws Exception {
		try {
			return fls.getSupplierInfoList(pc);
		} catch (Exception e) {
			throw new SystemException(e,"getSupplierInfoList",log);
		}
		
	}
	//根据查询关键字获取供应商的信息
	public DBRow[] getSearchSupplier(String key, PageCtrl pc) throws Exception {
		try {
			return fls.getSearchSupplier(key, pc);
		} catch (Exception e) {
			throw new SystemException(e,"getSearchSupplier",log);
		}
	}
	
	//增加供应商信息
	public void addSupplier(HttpServletRequest request) throws Exception {
		try {
			String supName = StringUtil.getString(request, "sup_name");
			long nationId = StringUtil.getLong(request, "nation");
			long productLineId = StringUtil.getLong(request,"product_line_id");
			long provinceId = StringUtil.getLong(request,"province");
			String linkman = StringUtil.getString(request, "linkman");
			String address = StringUtil.getString(request, "address");
			String legalName = StringUtil.getString(request, "legal_name");
			String phone = StringUtil.getString(request, "phone");
			String otherContacts = StringUtil.getString(request, "other_contacts");
			String bankInformation = StringUtil.getString(request, "bank_information");			
			String address_houme_number = StringUtil.getString(request, "address_houme_number");
			String address_street = StringUtil.getString(request, "address_street");
			String city_input = StringUtil.getString(request, "city_input");
			String zip_code = StringUtil.getString(request, "zip_code");
			DBRow supplier = new DBRow();
			if(-1 == provinceId){
				String address_pro_input = StringUtil.getString(request, "address_pro_input");
				supplier.add("address_pro_input", address_pro_input);
			}else{
				supplier.add("address_pro_input", "");
			}
			supplier.add("sup_name", supName);
			supplier.add("nation_id", nationId);
			supplier.add("product_line_id", productLineId);
			supplier.add("province_id", provinceId);
			supplier.add("linkman", linkman);
			supplier.add("address", address);
			supplier.add("legal_name", legalName);
			supplier.add("phone", phone);
			supplier.add("other_contacts", otherContacts);
			supplier.add("bank_information", bankInformation);
			supplier.add("address_houme_number", address_houme_number);
			supplier.add("address_street", address_street);
			supplier.add("city_input", city_input);
			supplier.add("zip_code", zip_code);
			long supplierId = fls.addSupplier(supplier);
			accountMgrZr.addAccount(supplierId,request);
		} catch (Exception e) {
			throw new SystemException(e,"addSupplier",log);
		}
		
	}
	//修改供应商信息
	public void modSupplier(HttpServletRequest request) throws Exception {
		try {
			String supName = StringUtil.getString(request, "sup_name");
			long id = StringUtil.getLong(request, "sid");
			long nationId = StringUtil.getLong(request, "nation");
			long productLineId = StringUtil.getLong(request,"product_line_id");
			long provinceId = StringUtil.getLong(request,"province");
			String linkman = StringUtil.getString(request, "linkman");
			String address = StringUtil.getString(request, "address");
			String legalName = StringUtil.getString(request, "legal_name");
			String phone = StringUtil.getString(request, "phone");
			String otherContacts = StringUtil.getString(request, "other_contacts");
			String bankInformation = StringUtil.getString(request, "bank_information");
			String pwd = StringUtil.getString(request, "password");
			String address_houme_number = StringUtil.getString(request, "address_houme_number");
			String address_street = StringUtil.getString(request, "address_street");
			String city_input = StringUtil.getString(request, "city_input");
			String zip_code = StringUtil.getString(request, "zip_code");
			DBRow supplier = new DBRow();
			if(-1 == provinceId){
				String address_pro_input = StringUtil.getString(request, "address_pro_input");
				supplier.add("address_pro_input", address_pro_input);
			}else{
				supplier.add("address_pro_input", "");
			}
			if (!pwd.trim().equals("")) {
				pwd = StringUtil.getMD5(pwd);
				supplier.add("password", pwd);
			}
			supplier.add("sup_name", supName);
			supplier.add("nation_id", nationId);
			supplier.add("product_line_id", productLineId);
			supplier.add("province_id", provinceId);
			supplier.add("linkman", linkman);
			supplier.add("address", address);
			supplier.add("legal_name", legalName);
			supplier.add("phone", phone);
			supplier.add("other_contacts", otherContacts);
			if(bankInformation != null && bankInformation.length() > 0 ){
				supplier.add("bank_information", bankInformation);
			}
			supplier.add("address_houme_number", address_houme_number);
			supplier.add("address_street", address_street);
			supplier.add("city_input", city_input);
			supplier.add("zip_code", zip_code);
			fls.modSupplier(id,supplier);
		} catch (Exception e){
			throw new SystemException(e,"modSupplier",log);
		}
	}

	//根据条件获得供应商的信息
	public DBRow[] getSupplierInfoListByCondition(HttpServletRequest request,PageCtrl pc) 
		throws Exception 
	{
			try 
			{
				//三个查询条件
				long nationId = StringUtil.getLong(request, "country");
				long provinceId = StringUtil.getLong(request, "province_id");
				long productLineId = StringUtil.getLong(request,"product_line_id");
				
				return fls.getSupplierInfoListByCondition(nationId,provinceId,productLineId,pc);
			}
			catch (Exception e) 
			{
				throw new SystemException(e,"getSupplierInfoListByCondition",log);
			}
	}

}
