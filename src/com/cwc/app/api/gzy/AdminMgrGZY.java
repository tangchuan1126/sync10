package com.cwc.app.api.gzy;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSON;

import org.apache.log4j.Logger;

import seamoonotp.seamoonapi;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.AccountOrPwdIncorrectException;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.app.exception.admin.OldPwdIncorrectException;
import com.cwc.app.exception.admin.RoleHaveAdminException;
import com.cwc.app.exception.admin.RoleIsExistException;
import com.cwc.app.exception.admin.SuperAdminException;
import com.cwc.app.exception.admin.SuperRoleException;
import com.cwc.app.exception.admin.VerifyCodeIncorrectException;
import com.cwc.app.floor.api.gzy.FloorAdminMgrGZY;
import com.cwc.app.floor.api.zr.FloorFileMgrZr;
import com.cwc.app.iface.gzy.AdminMgrIFaceGZY;
import com.cwc.app.key.BatchTypeKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.authentication.AdminAuthenCenter;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.json.JsonObject;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;
import com.cwc.verifycode.LoginLicence;

/**
 * 该类用于对管理员后台的一些操作
 * 
 * @author TurboShop
 * 
 *         TurboShop.cn all rights reserved.
 */
public class AdminMgrGZY implements AdminMgrIFaceGZY {
	static Logger log = Logger.getLogger("ACTION");
	private FloorAdminMgrGZY fam;
	private FloorFileMgrZr floorFileMgrZr;

	public void setFam(FloorAdminMgrGZY fam) {
		this.fam = fam;
	}
	


	/**
	 * 增加管理员
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void addAdmin(HttpServletRequest request)
			throws AdminIsExistException, Exception {
		try {	
			String account = StringUtil.getString(request, "account");
			String pwd = StringUtil.getString(request, "pwd");

			long ps_id = StringUtil.getLong(request, "ps_id");
			String employe_name = StringUtil.getString(request,
					"employe_name");
			String email = StringUtil.getString(request, "email");
			String skype = StringUtil.getString(request, "skype");

			String Entrytime = StringUtil.getString(request, "entrytime");// 入职时间
			String proQQ = StringUtil.getString(request, "QQ");// QQ
			String proMSN = StringUtil.getString(request, "msn");
			String mobilePhone = StringUtil.getString(request, "mobile");
			String attGRL = StringUtil.getString(request,"arrGRL");
			long adgid = StringUtil.getLong(request,"adgid");
			String areaId = StringUtil.getString(request,"areaId");
			
			if (fam.getDetailAdminByAccount(account) == null) {
				DBRow row = new DBRow();
				row.add("post_date", DateUtil.NowStr());
				row.add("account", account);
				row.add("ps_id", ps_id);
				row.add("pwd", StringUtil.getMD5(pwd));
				row.add("llock", 0);
				row.add("employe_name", employe_name);
				row.add("email", email);
				row.add("skype", skype);
				row.add("msn", proMSN);
				row.add("Entrytime", DateUtil.FormatDatetime(Entrytime));
				row.add("proQQ", proQQ);
				row.add("mobilePhone", mobilePhone);
				row.add("adgid", adgid);
				row.add("areaId", areaId);
				
				long id = fam.addAdmin(row);
				String[] items = null;
				 DBRow drow = new DBRow();				
		         if(attGRL != null){
		       	  items = attGRL.split(",");
			          for(int i = 0 ; i<items.length ; i++){
			        	  String[] grl = items[i].split("-");
			        	  long group_id = Long.parseLong(grl[0]);
			        	  long role_id = Long.parseLong(grl[1]);
			        	  long filter_lid = Long.parseLong(grl[2]);
			        	 	drow.add("adid", id);
						  	drow.add("adgid", group_id);
						  	drow.add("AreaId", filter_lid);
						  	drow.add("proJsId", role_id);
						  	fam.addAdminGRL(drow);
			          }
		         }				
			} else {
				throw new AdminIsExistException();
			}
		} catch (AdminIsExistException e) {
			throw new AdminIsExistException();
		} catch (Exception e) {
			throw new SystemException(e, "addAdmin", log);
		}
	}

	/**
	 * 通过帐号获得管理员详细信息
	 * 
	 * @param account
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDetailAdminByAccount(PageCtrl pc, String account)
			throws Exception {
		try {
			return (fam.getActionsByPage(pc, account));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailAdminByAccount", log);
		}
	}

	/**
	 * 通过高级查询
	 * 
	 * @param account
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getadvanSearch(PageCtrl pc, HttpServletRequest request)
			throws Exception {
		try 
		{
			//四个查询条件
			long proPsId = StringUtil.getLong(request, "proPsId"); //仓库
			String proJsId = StringUtil.getString(request,"proJsId");//角色
			long proAdgid = StringUtil.getLong(request, "proAdgid"); //部门
			long filter_lid = StringUtil.getLong(request,"filter_lid");//办公地点
			int	lock_state	= StringUtil.getInt(request,"lock_state");
			
			return (fam.getadvanSearch(proPsId,proJsId,proAdgid,filter_lid,lock_state,pc));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "getadvanSearch", log);
		}
	}

	/**
	 * 删除管理员
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void delAdmin(HttpServletRequest request)
			throws SuperAdminException, Exception {
		try {
			String account = StringUtil.getString(request, "account");
			if (account.equals("admin")) {
				throw new SuperAdminException();
			}

			long adid = StringUtil.getLong(request, "adid");
			fam.delAdmin(adid);
		} catch (SuperAdminException e) {
			throw new SuperAdminException();
		} catch (Exception e) {
			throw new SystemException(e, "delAdmin", log);
		}
	}

	/**
	 * 锁定管理员
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void lockAdmin(HttpServletRequest request)
			throws SuperAdminException, Exception {
		try {
			String account = StringUtil.getString(request, "account");
			if (account.equals("admin")) {
				throw new SuperAdminException();
			}

			long adid = StringUtil.getLong(request, "adid");
			DBRow newInfo = new DBRow();
			newInfo.add("llock", "1");
			fam.modifyAdmin(adid, newInfo);
		} catch (SuperAdminException e) {
			throw new SuperAdminException();
		} catch (Exception e) {
			throw new SystemException(e, "lockAdmin", log);
		}
	}

	/**
	 * 解锁管理员
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void unLockAdmin(HttpServletRequest request) throws Exception {
		try {
			long adid = StringUtil.getLong(request, "adid");
			DBRow newInfo = new DBRow();
			newInfo.add("llock", "0");
			fam.modifyAdmin(adid, newInfo);
		} catch (Exception e) {
			throw new SystemException(e, "unLockAdmin", log);
		}
	}

	/**
	 * 获得管理员详细信息
	 * 
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailAdmin(long adid) throws Exception {
		try {
			return (fam.getDetailAdmin(adid));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailAdmin", log);
		}
	}

	/**
	 * 获得所有管理员
	 * 
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAdmin(PageCtrl pc) throws Exception {
		try {
			return (fam.getAllAdmin(pc));
		} catch (Exception e) {
			throw new SystemException(e, "getAllAdmin", log);
		}
	}

	/**
	 * 获得所有角色
	 * 
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAdminGroup(PageCtrl pc) throws Exception {
		try {
			return (fam.getAllAdminGroup(pc));
		} catch (Exception e) {
			throw new SystemException(e, "getAllAdminGroup", log);
		}
	}

	/**
	 * 根据用户ID获取部门角色关系信息
	 * 
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminGroupRelation(long adid) throws Exception{
		try {
			return  (fam.getAdminGroupRelation(adid));
		} catch (Exception e) {
			throw new SystemException(e, "getAdminGroupRelation", log);
		}
	}
	/**
	 * 获得角色详细信息
	 * 
	 * @param adgid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailAdminGroup(long adgid) throws Exception {
		try {
			return (fam.getDetailAdminGroup(adgid));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailAdminGroup", log);
		}
	}

	/**
	 * 修改管理员密码
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void modAdminPwd(HttpServletRequest request)
			throws OldPwdIncorrectException, Exception {
		try {
			HttpSession ses = StringUtil.getSession(request);
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(ses);
			long adid = adminLoggerBean.getAdid();
			String pwd = StringUtil.getString(request, "pwd");
			String oldpwd = StringUtil.getString(request, "oldpwd");

			DBRow detailAdmin = fam.getDetailAdmin(adid);
			if (detailAdmin.getString("pwd").equals(StringUtil.getMD5(oldpwd))) {
				DBRow newInfo = new DBRow();
				newInfo.add("pwd", StringUtil.getMD5(pwd));
				fam.modifyAdmin(adid, newInfo);

				// 重新登录
				this.adminLogout(request);
			} else {
				throw new OldPwdIncorrectException();
			}
		} catch (OldPwdIncorrectException e) {
			throw new OldPwdIncorrectException();
		} catch (Exception e) {
			throw new SystemException(e, "modAdminPwd", log);
		}
	}

	/**
	 * 管理员登陆
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public void adminLogin(HttpServletRequest request,
			HttpServletResponse response)
			throws AccountOrPwdIncorrectException,
			VerifyCodeIncorrectException, AccountNotPermitLoginException,
			Exception {
		try {
			String pwd = StringUtil.getString(request, "pwd");
			String account = StringUtil.getString(request, "account");
			String licence = StringUtil.getString(request, "licence");
			int remember = StringUtil.getInt(request, "remember");
			int loginType = StringUtil.getInt(request, "loginType");// =1，sso登录无需验证验证码

			adminLogin(request, response, account, pwd, licence, remember,
					loginType, null);
		} catch (AccountOrPwdIncorrectException e) {
			throw e;
		} catch (VerifyCodeIncorrectException e) {
			throw e;
		} catch (AccountNotPermitLoginException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "adminLogin", log);
		}
	}

	/**
	 * 帐号登录接口
	 * 
	 * @param request
	 * @param response
	 * @param account
	 * @param pwd
	 * @param licence
	 * @param remember
	 *            //浏览器是否记录登录帐号 1-记录
	 * @param loginType
	 *            //1-SSO登录
	 * @throws AccountOrPwdIncorrectException
	 * @throws VerifyCodeIncorrectException
	 * @throws AccountNotPermitLoginException
	 * @throws Exception
	 */
	public void adminLogin(HttpServletRequest request,
			HttpServletResponse response, String account, String pwd,
			String licence, int remember, int loginType, DBRow attach)
			throws AccountOrPwdIncorrectException,
			VerifyCodeIncorrectException, AccountNotPermitLoginException,
			Exception {
		HttpSession ses = StringUtil.getSession(request);
		String login_date = DateUtil.NowStr();

		if (loginType == 1
				|| licence.equals(LoginLicence.getTheLicence(request))) {
			DBRow detailAdmin = fam.getDetailAdminByAccount(account);

			// //system.out.println("-->"+detailAdmin.getString("pwd")+" - "+detailAdmin.getString("pwd"));

			if (detailAdmin != null
					&& detailAdmin.getString("pwd").equals(
							StringUtil.getMD5(pwd))) {
				// 先检查下角色是否存在，因为删除角色并不会删除帐号
				// 如果角色不存在，则不让登录，以免出错
				if (fam.getDetailRoleByAdgid(detailAdmin.get("adgid", 0l)) == null) {
					throw new AccountOrPwdIncorrectException();
				}

				if (detailAdmin.get("llock", 0) == 1) {
					throw new AccountNotPermitLoginException();
				}

				/**
				 * 如果是SSO登录，需要特别处理，手工创建一个adminLoggerBean，设置正确登录入口（正常情况，
				 * 该操作在AdminAuthorizationFilter执行）
				 */
				if (loginType == 1) {
					AdminLoginBean adminLoggerBean = new AdminLoginBean();
					adminLoggerBean.setIsLoginRightPath();
					StringUtil.getSession(request).setAttribute(
							Config.adminSesion, adminLoggerBean);
				}

				// 登录后把islogin标记下，才算真正登录
				AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));

				if (adminLoggerBean == null)// 有时adminLoggerBean为null
				{
					throw new VerifyCodeIncorrectException();
				}

				adminLoggerBean.setAccount(account);
				adminLoggerBean.setAdgid(detailAdmin.get("adgid", 0l));
				adminLoggerBean.setAdid(detailAdmin.get("adid", 0l));
				adminLoggerBean.setPs_id(detailAdmin.get("ps_id", 0l));
				adminLoggerBean.setEmail(detailAdmin.getString("email"));
				adminLoggerBean.setLoginDate(login_date);
				adminLoggerBean.setAttach(attach);

				adminLoggerBean.setIsLogin();
				ses.setAttribute(Config.adminSesion, adminLoggerBean);
				// 记住登陆帐号
				if (remember == 1) {
					StringUtil.setCookie(response, "account", account,
							60 * 60 * 24 * 365);
				} else {
					StringUtil.setCookie(response, "account", account, 0);
				}

				DBRow newInfo = new DBRow();
				newInfo.add("last_login_date", login_date);
				fam.modifyAdmin(detailAdmin.get("adid", 0l), newInfo);
			} else {
				throw new AccountOrPwdIncorrectException();
			}
		} else {
			throw new VerifyCodeIncorrectException();
		}
	}

	/**
	 * 获得管理员菜单树权限
	 * 
	 * @param adgid
	 * @return
	 * @throws Exception
	 */
	private ArrayList getPermitPage(long adgid) throws Exception {
		try {
			ArrayList al = new ArrayList();
			DBRow permitPages[] = fam.getControlTreeByAdgid(adgid);

			for (int i = 0; i < permitPages.length; i++) {
				String uri = permitPages[i].getString("link");
				if (uri.indexOf("?") >= 0) {
					int l2 = uri.indexOf("?");
					uri = uri.substring(0, l2);
				}

				al.add(uri);
			}

			return (al);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitPage", log);
		}
	}

	/**
	 * 管理员推出登录
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void adminLogout(HttpServletRequest request) throws Exception {
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		adminLoggerBean.setNotLogin();
		adminLoggerBean.setAttach(null);
		StringUtil.getSession(request).setAttribute(Config.adminSesion,
				adminLoggerBean);
	}

	/**
	 * 
	 * @param response
	 * @param request
	 * @throws Exception
	 */
	public void markLocal(HttpServletResponse response,
			HttpServletRequest request) throws Exception {
		String local = StringUtil.getString(request, "local");
		local = local.toLowerCase();
		StringUtil.setCookie(response, "turbocwcshop_local", local,
				3600 * 24 * 365);
	}

	/**
	 * 判断用户是否从正确后台路径登录
	 * 
	 * @param response
	 * @param request
	 * @throws Exception
	 */
	public void isRightAdminPath(HttpServletResponse response,
			HttpServletRequest request) throws Exception {
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));

		if (adminLoggerBean == null || !adminLoggerBean.isLoginRightPath()) {
			response.sendRedirect("../errorAdminPath");
			return;
		}
	}

	/**
	 * 获得管理菜单
	 * 
	 * @param parentid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getControlTreeByParentid(long parentid) throws Exception {
		try {
			return (fam.getControlTreeByParentid(parentid));
		} catch (Exception e) {
			throw new SystemException(e, "getControlTreeByParentid", log);
		}
	}

	/**
	 * 获得管理菜单，按sort排序
	 * 
	 * @param parentid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getControlTreeByParentidOrderBySort(long parentid)
			throws Exception {
		try {
			return (fam.getControlTreeByParentidOrderBySort(parentid));
		} catch (Exception e) {
			throw new SystemException(e, "getControlTreeByParentidOrderBySort",
					log);
		}
	}

	/**
	 * 通过方法名获得权限详细信息
	 * 
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailAuthenticationActionByAction(String action)
			throws Exception {
		try {
			return (fam.getDetailAuthenticationActionByAction(action));
		} catch (Exception e) {
			throw new SystemException(e,
					"getDetailAuthenticationActionByAction", log);
		}
	}

	/**
	 * 通过连接获得管理菜单相信信息
	 * 
	 * @param link
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailControlTreeByLink(String link) throws Exception {
		try {
			return (fam.getDetailControlTreeByLink(link));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailControlTreeByLink", log);
		}
	}

	/**
	 * 通过ID获得页面树详细
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailControlTreeById(long id) throws Exception {
		try {
			return (fam.getDetailControlTreeById(id));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailControlTreeById", log);
		}
	}

	/**
	 * 获得树状控制页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getControlTree() throws Exception {
		try {
			Tree tree = new Tree(ConfigBean.getStringValue("control_tree"));
			DBRow rows[] = tree.getTree();
			return (rows);
		} catch (Exception e) {
			throw new SystemException(e, "getControlTree", log);
		}
	}

	/**
	 * 获得页面资源允许的角色ID
	 * 
	 * @param page
	 * @return
	 * @throws Exception
	 */
	public ArrayList getPermitPageAdgid(String page) throws Exception {
		ArrayList out = new ArrayList();

		try {
			DBRow rows[] = fam.getPermitPageAdgid(page);

			for (int i = 0; i < rows.length; i++) {
				out.add(rows[i].getString("adgid"));
			}

			return (out);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitPageAdgid", log);
		}
	}

	/**
	 * 获得页面资源允许的管理员ID(扩展权限)
	 * 
	 * @param page
	 * @return
	 * @throws Exception
	 */
	public ArrayList getPermitPageAdid(String page) throws Exception {
		ArrayList out = new ArrayList();

		try {
			DBRow rows[] = fam.getPermitPageAdid(page);

			for (int i = 0; i < rows.length; i++) {
				out.add(rows[i].getString("adid"));
			}

			return (out);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitPageAdid", log);
		}
	}

	/**
	 * 获得 action资源允许的角色ID
	 * 
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public ArrayList getPermitActionAdgid(String action) throws Exception {
		ArrayList out = new ArrayList();

		try {
			DBRow rows[] = fam.getPermitActionAdgid(action);

			for (int i = 0; i < rows.length; i++) {
				out.add(rows[i].getString("adgid"));
			}

			return (out);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitActionAdgid", log);
		}
	}

	/**
	 * 获得 action资源允许的管理员ID(扩展权限)
	 * 
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public ArrayList getPermitActionAdid(String action) throws Exception {
		ArrayList out = new ArrayList();

		try {
			DBRow rows[] = fam.getPermitActionAdid(action);

			for (int i = 0; i < rows.length; i++) {
				out.add(rows[i].getString("adid"));
			}

			return (out);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitActionAdid", log);
		}
	}

	/**
	 * 增加角色
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void addRole(HttpServletRequest request)
			throws RoleIsExistException, Exception {
		try
		{
			String name = StringUtil.getString(request, "name");
			String description = StringUtil.getString(request,"description");
			
			if (fam.getDetailRoleByName(name) != null) {
				throw new RoleIsExistException();
			}

			DBRow role = new DBRow();
			role.add("description",description);
			role.add("name", name);

			fam.addRole(role);
		} catch (RoleIsExistException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "addRole", log);
		}
	}

	/**
	 * 增加角色-资源页面映射
	 * 
	 * @param adgid
	 * @param ataid
	 * @throws Exception
	 */
	private void addRoleControlPageMap(long adgid, String ctid[])
			throws Exception {
		try {
			for (int i = 0; ctid != null && i < ctid.length; i++) {
				DBRow roleControlPageMap = new DBRow();
				roleControlPageMap.add("adgid", adgid);
				roleControlPageMap.add("ctid", ctid[i]);
				fam.addRoleControlPageMap(roleControlPageMap);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addRoleControlPageMap", log);
		}
	}

	/**
	 * 增加管理员页面扩展权限
	 * 
	 * @param adid
	 * @param ctid
	 * @throws Exception
	 */
	private void addAdminControlPageMap(long adid, String ctid[])
			throws Exception {
		try {
			for (int i = 0; ctid != null && i < ctid.length; i++) {
				DBRow roleControlPageMap = new DBRow();
				roleControlPageMap.add("adid", adid);
				roleControlPageMap.add("ctid", ctid[i]);
				fam.addAdminControlPageMap(roleControlPageMap);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addAdminControlPageMap", log);
		}
	}

	/**
	 * 增加角色-action映射
	 * 
	 * @param adgid
	 * @param ataid
	 * @throws Exception
	 */
	private void addRoleActionMap(long adgid, String ataid[]) throws Exception {
		try {
			for (int i = 0; ataid != null && i < ataid.length; i++) {
				DBRow roleActionMap = new DBRow();
				roleActionMap.add("adgid", adgid);
				roleActionMap.add("ataid", ataid[i]);
				fam.addRoleActionMap(roleActionMap);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addRoleActionMap", log);
		}
	}

	/**
	 * 增加管理员扩展权限
	 * 
	 * @param adid
	 * @param ataid
	 * @throws Exception
	 */
	private void addAdminActionMap(long adid, String ataid[]) throws Exception {
		try {
			for (int i = 0; ataid != null && i < ataid.length; i++) {
				DBRow roleActionMap = new DBRow();
				roleActionMap.add("adid", adid);
				roleActionMap.add("ataid", ataid[i]);
				fam.addAdminActionMap(roleActionMap);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addAdminActionMap", log);
		}
	}

	/**
	 * 修改角色
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void modifyRole(HttpServletRequest request)
			throws RoleIsExistException, Exception {
		try 
		{
			long adgid = StringUtil.getLong(request, "adgid");
			String name = StringUtil.getString(request, "name");
			String index_page = StringUtil.getString(request, "index_page");
			String description = StringUtil.getString(request,"description");

			DBRow existRole = fam.getDetailRoleByName(name);

			if (existRole != null && existRole.get("adgid", 0l) != adgid) {
				throw new RoleIsExistException();
			}

			DBRow role = new DBRow();
			role.add("name", name);
			role.add("index_page", index_page);

			fam.modifyRole(adgid, role);
		} catch (RoleIsExistException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "modifyRole", log);
		}
	}

	/**
	 * 对角色进行页面资源和action权限设置
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void grantRolePageActionRights(HttpServletRequest request)
			throws SuperRoleException, Exception {
		try {
			long adgid = StringUtil.getLong(request, "adgid");

			// 不允许修改超级管理员权限
			if (adgid == 10000) {
				throw new SuperRoleException();
			}

			String ataid[] = request.getParameterValues("ataid");
			String ctid[] = request.getParameterValues("ctid");

			if (adgid > 0) {
				// 先删除现有群组权限映射
				fam.delRoleActionMapByAdgid(adgid);
				fam.delRoleControlPageMapByAdgid(adgid);

				this.addRoleActionMap(adgid, ataid);
				this.addRoleControlPageMap(adgid, ctid);
			}

			// 清空鉴权映射内存缓存数据
			AdminAuthenCenter.clearPermitPageHM();
			AdminAuthenCenter.clearPermitActionHM();
		} catch (SuperRoleException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "grantRolePageActionRights", log);
		}
	}

	public void grantAdminPageActionRights(HttpServletRequest request)
			throws SuperRoleException, Exception {
		try {
			long adid = StringUtil.getLong(request, "adid");

			String admin_ataid[] = request.getParameterValues("admin_ataid");
			String admin_ctid[] = request.getParameterValues("admin_ctid");

			if (adid > 0) {
				// 先删除现有群组权限映射
				fam.delAdminActionMapByAdid(adid);
				fam.delAdminControlPageMapByAdid(adid);

				this.addAdminActionMap(adid, admin_ataid);
				this.addAdminControlPageMap(adid, admin_ctid);
			}

			// 清空鉴权映射内存缓存数据
			AdminAuthenCenter.clearPermitPageHM();
			AdminAuthenCenter.clearPermitActionHM();
		} catch (SuperRoleException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "grantAdminPageActionRights", log);
		}
	}

	/**
	 * 删除角色
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void delRole(HttpServletRequest request)
			throws RoleHaveAdminException, SuperRoleException, Exception {
		try {
			long adgid = StringUtil.getLong(request, "adgid");

			// 不能删除超级管理员角色
			if (adgid == 10000) {
				throw new SuperRoleException();
			}

			if (fam.getAdminByAdgid(adgid, null).length > 0) {
				throw new RoleHaveAdminException();
			}

			fam.delRole(adgid);
			// 把角色相关的action和页面资源映射同时删除
			fam.delRoleActionMapByAdgid(adgid);
			fam.delRoleControlPageMapByAdgid(adgid);
		} catch (RoleHaveAdminException e) {
			throw e;
		} catch (SuperRoleException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "delRole", log);
		}
	}

	/**
	 * 通过角色名称获得角色详细信息
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailRoleByName(String name) throws Exception {
		try {
			return (fam.getDetailRoleByName(name));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailRoleByName", log);
		}
	}

	/**
	 * 通过角色ID获得角色详细名称
	 * 
	 * @param adgid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailRoleByAdgid(long adgid) throws Exception {
		try {
			return (fam.getDetailRoleByAdgid(adgid));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailRoleByAdgid", log);
		}
	}

	/**
	 * 通过page id获得其下所有action
	 * 
	 * @param pc
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getActionsByPage(PageCtrl pc, long id) throws Exception {
		try {
			return (fam.getActionsByPage(pc, id));
		} catch (Exception e) {
			throw new SystemException(e, "getActionsByPage", log);
		}
	}

	/**
	 * 
	 * @param adgid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminByAdgid(long adgid, PageCtrl pc) throws Exception {
		try {
			return (fam.getAdminByAdgid(adgid, pc));
		} catch (Exception e) {
			throw new SystemException(e, "getAdminByAdgid", log);
		}
	}

	/**
	 * 修改管理员
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void modAdmin(HttpServletRequest request) throws Exception {
		try {
			long adid = StringUtil.getLong(request, "adid");
			String pwd = StringUtil.getString(request, "pwd");
			String employe_name = StringUtil.getString(request,"employe_name");
			long ps_id = StringUtil.getLong(request, "ps_id");
			String proEntryTime = StringUtil.getString(request, "entrytime");

			String email = StringUtil.getString(request, "email");
			String skype = StringUtil.getString(request, "skype");
			String msn = StringUtil.getString(request, "msn");
			String qq = StringUtil.getString(request, "QQ");
			String mobilePhone = StringUtil.getString(request, "mobile");
			String attGRL = StringUtil.getString(request,"arrGRL");
			long adgid = StringUtil.getLong(request,"adgid");
			String filePath = StringUtil.getString(request,"filePath");
			
			String areaId = StringUtil.getString(request,"areaId");
			 //保存凭证文件
			String fileNames = StringUtil.getString(request, "file_names");
			String realFileNames = "";
			if(fileNames!=null && fileNames.trim().length()>0)
			{
				String sn = StringUtil.getString(request, "sn");
				String path = StringUtil.getString(request, "path");
				realFileNames = this.handleAdminFile(sn, path, fileNames, adid);
			}
			DBRow admin = new DBRow();
			if (!pwd.trim().equals("")) {
				pwd = StringUtil.getMD5(pwd);
				admin.add("pwd", pwd);
			}
			admin.add("employe_name", employe_name);
			admin.add("ps_id", ps_id);
			admin.add("email", email);
			admin.add("skype", skype);
			admin.add("msn", msn);
			admin.add("proQQ", qq);
			admin.add("mobilePhone", mobilePhone);
			admin.add("Entrytime", proEntryTime);
			admin.add("AreaId", areaId);
			if(fileNames!=null && fileNames.trim().length()>0){
			 admin.add("file_path", realFileNames);
			}else{
				admin.add("file_path", filePath);
			}
			admin.add("adgid", adgid);
			fam.modifyAdmin(adid, admin);
			String[] items = null;
			 DBRow drow = new DBRow();			
	          if(attGRL.length()!= 0 && !attGRL.equals("")){	  
	        	  fam.deleteAdminRelation(adid);
	        	  items = attGRL.split(",");
		          for(int i = 0 ; i<items.length ; i++){
		        	  String[] grl = items[i].split("-");
		        	  long group_id = Long.parseLong(grl[0]);
		        	  long role_id = Long.parseLong(grl[1]);
		        	  long filter_lid = Long.parseLong(grl[2]);
		        	  	drow.add("adid", adid);
					  	drow.add("adgid", group_id);
					  	drow.add("AreaId", filter_lid);
					  	drow.add("proJsId", role_id);
					  	fam.addAdminGRL(drow);
		          }		          
	          }	
		} catch (Exception e) {
			throw new SystemException(e, "modAdmin", log);
		}
	}
	public String handleAdminFile(String sn, String path, String fileNames, long adminId) throws Exception 
	{
		try{
			//重新命名图片文件A_admin_adminId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			String realFileNames = "";
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false );
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							StringBuffer realFileName = new StringBuffer(sn);
							if(0 != adminId)
							{
								realFileName.append("_").append(adminId);
							}
							realFileName.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorFileMgrZr.getIndexOfFileByFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							realFileNames = realFileName.toString();
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
						}
					}
				}
			}
			return realFileNames;
		}catch (Exception e) {
			throw new SystemException(e, "handleAdminFile", log);
		}
	}
	
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	
	public DBRow getDetailAdminByAccount(String account) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 更改用户的口令
	 */
//	@Override
//	public void updateAdminRegisterToken(HttpServletRequest request) throws Exception{
//		try 
//		{
//			String register_token_str	= StrUtil.getString(request, "register_token_str");
//			String register_token		= "";
//			long adid					= StrUtil.getLong(request, "userAdid");
//			DBRow adminRow				= fam.getDetailAdmin(adid);
//			String sninfo				= adminRow.getString("register_token");
//			seamoonapi sc				= new seamoonapi();
//			
//			String str					= sc.passwordsyn(sninfo,register_token_str);
//			if (str.length() > 3 )
//			{
//				//system.out.println("同步成功");  
//				//system.out.println(str);
//				DBRow row				= new DBRow();
//				row.add("register_token", str);
//				fam.updateAdminRegisterToken(row, adid);
//			}
//			else if(str.equals("-1"))
//			{
//				//system.out.println("SN字符串有错");
//			}
//			else if(str.equals("0"))
//			{
//				//system.out.println("同步密码错误");
//			}	 
//			else  
//			{
//				//system.out.println("未知错误");
//			}
//			
//		} catch (Exception e) {
//			throw new SystemException(e, "updateAdminRegisterToken", log);
//		}
//		
//		
//	}
	
	
	
	/**
	 * 登记口令卡信息
	 * 1:卡号不存在
	 * 2：密码验证通过
	 * 3：密码验证不通过
	 * 4：卡号已经注册
	 * 5: 注册成功
	 * 6：密码同步成功
	 * 7：密码同步未成功
	 */
	@Override
	public int checkAdminAndUpdateToken(HttpServletRequest request) throws Exception{
		
		try {
			int reInt				= 0;
			String reStr			= "";
			//先验证此卡号是否存在，不存在则返回提示，存在确认卡号与密钥是否对应
			String snNo				= StringUtil.getString(request, "token_sn");
			DBRow snInfo			= fam.checkTokenSn(snNo);
			if(null == snInfo){
				reStr				= "此卡号不存在，请确认......";
				reInt				= 1;
			}else{
				long adid			= StringUtil.getLong(request, "userAdid");
				DBRow[] admins			= fam.getAdminsBySn(snNo);
				//如果此卡号已注册,同步，没注册则注册
				if(admins.length > 0){
					String snSnInfo		= StringUtil.getString(request, "token_sn_sninfo");
					seamoonapi sc		= new seamoonapi();
				    String str			= sc.checkpassword(snInfo.getString("sninfo"),snSnInfo);
					if (str.length()>3)
					{
						reStr			= "密码验证通过.......";
						reInt			= 2;
						String str2		= sc.passwordsyn(snInfo.getString("sninfo"),snSnInfo);
						if (str2.length()>3)
						{
							//验证通过，更改口令密码
							DBRow snRow		= new DBRow();
							snRow.add("sninfo", str2);
							fam.updateSnSninfoToken(snRow, snNo);
							DBRow adminInfo	= new DBRow();
							adminInfo.add("register_token", snNo);
							fam.updateAdminRegisterToken(adminInfo, adid);
							reStr			= "密码同步成功.......";
							reInt			= 6;
						}else{
							reStr			= "密码同步未成功.......";
							reInt			= 7;
						}
						
					}else{
						reStr			= "验证未通过......";
						reInt			= 3;
					}
				}else{
					//新注册
					DBRow adminInfo	= new DBRow();
					adminInfo.add("register_token", snNo);
					fam.updateAdminRegisterToken(adminInfo, adid);
					reStr				= "注册成功...";
					reInt				= 5;
				}
			}
			return reInt;
		} catch (Exception e) {
			throw new SystemException(e, "checkAdminAndUpdateToken", log);
		}
		
	}
	
		//根据员工真实名获取管理员信息
	public  DBRow getAdminByEmployeName(String Employe_name) 
		throws Exception
	{
		try 
		{
			return (fam.getAdminByEmployeName(Employe_name));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getAdminByEmployeName(Employe_name)",log);
		}
	}



	public void setFloorFileMgrZr(FloorFileMgrZr floorFileMgrZr) {
		this.floorFileMgrZr = floorFileMgrZr;
	}
	
}
