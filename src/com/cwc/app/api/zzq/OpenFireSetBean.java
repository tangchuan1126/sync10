package com.cwc.app.api.zzq;

public class OpenFireSetBean {

	private String systemNotifyAdid;
	private String systemNofiyPassword ;
	private String address;
	private String port;
	private String domainName;
	private String allDomainName;
	
	
	
	public String getSystemNotifyAdid() {
		return systemNotifyAdid;
	}
	public void setSystemNotifyAdid(String systemNotifyAdid) {
		this.systemNotifyAdid = systemNotifyAdid;
	}
	public String getSystemNofiyPassword() {
		return systemNofiyPassword;
	}
	public void setSystemNofiyPassword(String systemNofiyPassword) {
		this.systemNofiyPassword = systemNofiyPassword;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getAllDomainName() {
		return allDomainName;
	}
	public void setAllDomainName(String allDomainName) {
		this.allDomainName = allDomainName;
	}
	
	
	
}
