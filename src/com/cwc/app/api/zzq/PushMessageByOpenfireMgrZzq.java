package com.cwc.app.api.zzq;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import org.jivesoftware.smackx.OfflineMessageManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;

import com.cwc.app.api.zr.AndroidPushMessge;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.iface.zzq.PushMessageByOpenfireMgrIfaceZzq;
import com.cwc.db.DBRow;
import com.cwc.service.pushMessageByOpenfire.action.PushMessageByOpenfireAction;

/**
 * @author Zhengziqi 2015年1月28日
 *
 */
public class PushMessageByOpenfireMgrZzq implements
		PushMessageByOpenfireMgrIfaceZzq  {

	private final static String SystemNotifyMsgAppend="[SystemNotifyMsgAppend]";
	
	private String systemNotifyAdid;
	private String systemNofiyPassword ;
	private String address;
	private String port;
	private String domainName;
	private String allDomainName;
	
	
	private FloorAdminMgr fam;
	private OpenFireSetBean openFireSetBean ;
	
	
	 
	public void init(){
	    systemNotifyAdid = openFireSetBean.getSystemNotifyAdid() ;
	    systemNofiyPassword = openFireSetBean.getSystemNofiyPassword() ;
	    address = openFireSetBean.getAddress() ;
	    port = openFireSetBean.getPort() ;
	    domainName = openFireSetBean.getDomainName() ;
	    allDomainName =  openFireSetBean.getAllDomainName() ;
	}
	/**
	 *
	 * 轮训拉取当前用户数据，包括会话(conversation, msgType=1)、通知(notice, msgType=2)、在线状态(presence)
	 * 2015年1月29日
	 * @param account
	 * @param delTemp 是否删除tempStack
	 * @param delHistroy 是否删除historyStack
	 * @param chatId 删除指定用户 与当前用户的历史缓存
	 * @param msgType 删除指定类型的历史缓存，默认是会话(conversation, msgType=1)
	 * @return
	 * @throws Exception
	 *
	 */
	@Override
	public JSONArray roundRobin(String account, String delTemp,
			String delHistroy, String chatId, String msgType) throws Exception {
		Map<String, Object> conversationStack = PushMessageByOpenfireAction
				.getConversationStack();
		Map<String, Object> historyStack = PushMessageByOpenfireAction
				.getHistoryStack();
		Map<String, Object> tempStack = PushMessageByOpenfireAction
				.getTempStack();

		// delete tempStack till front-end receive message
		if (StringUtils.equals(delTemp, "true")) {
			tempStack.remove(account);
		}
		// delete historyStack till user see the message (conversation)
		if (StringUtils.equals(delHistroy, "true")
				&& StringUtils.isNotBlank(chatId)
				&& historyStack.containsKey(account)) {
			msgType = StringUtils.isNotBlank(msgType) ? msgType
					: PushMessageByOpenfireAction.CONVERSATION_TYPE_NO;
			JSONArray array = (JSONArray) historyStack.get(account);
			JSONArray newArray = new JSONArray();
			for (int i = 0; i < array.size(); i++) {
				JSONObject object = (JSONObject) array.get(i);
				if (!(StringUtils.equals(object.get("userid").toString(),
						chatId) && StringUtils.equals(object.get("msgType")
						.toString(), msgType))) {
					newArray.add(object);
				}
			}
			if (newArray.size() > 0) {
				historyStack.put(account, newArray);
			} else {
				historyStack.remove(account);
			}
		}
		// shift conversation info into tempStack from conversationStack
		if (conversationStack.containsKey(account)
				&& conversationStack.get(account) != null) {
			JSONArray reply = (JSONArray) conversationStack.get(account);
			PushMessageByOpenfireAction.setTempStack(resetConversationStack(null, reply, account, tempStack));
			// stock historyStack
			// do not need presence info
			JSONArray newArray = new JSONArray();
			for (int i = 0; i < reply.size(); i++) {
				JSONObject object = (JSONObject) reply.get(i);
				// presence info has no use now, delete it
				if (!StringUtils.equals(object.get("type").toString(),
						"presence")) {
					newArray.add(object);
				}
			}
			PushMessageByOpenfireAction.setHistoryStack(resetConversationStack(null, newArray, account, historyStack));
			conversationStack.remove(account);
		}
		// return conversations in tempStack
		JSONArray reply = new JSONArray();
		if (tempStack.containsKey(account) && tempStack.get(account) != null
				&& ((JSONArray) tempStack.get(account)).size() > 0) {
			reply = (JSONArray) tempStack.get(account);
		}
		return reply;
	}

	/**
	 *
	 * 当前用户向指定用户发送会话消息
	 * 固定为会话类型(conversation, msgType=1)
	 * 2015年1月29日
	 * @param account 用于从缓存中获取当前用户的连接
	 * @param chatId 指定用户
	 * @param msg 会话消息内容
	 * @return
	 * @throws Exception
	 *
	 */
	@Override
	public Boolean sendMsg(String account, String chatId, String msg)
			throws Exception {
		if (!StringUtils.contains(chatId, this.domainName)) {
			chatId += this.allDomainName;
		}
		Message message = new Message();
		message.setProperty("messageType", "1");
		message.setBody(msg);
		Boolean isSuccess = false;
		
		XMPPConnection con = aquireConnection(account);
		if (con != null && StringUtils.isNotBlank(chatId)) {
			sendMessage(con, message, chatId);
			isSuccess = true;
		}
		return isSuccess;
	}

	/**
	 *
	 * 初始化联系人列表
	 * 2015年1月29日
	 * @param account 用于从缓存中获取当前用户的连接
	 * @return
	 * @throws Exception
	 *
	 */
	@Override
	public JSONObject aquireContacts(String account) throws Exception {
		XMPPConnection con = aquireConnection(account);
		if (con != null) {
			JSONArray array = new JSONArray();
			Roster roster = con.getRoster();
			// obtain contacts group
			Collection<RosterGroup> rosterGroups = roster.getGroups();
			JSONObject rosterGroupObject;
			Collection<RosterEntry> rosterEntries;
			JSONArray entryJSONArray;
			int id = 0;
			
			for (RosterGroup rosterGroup : rosterGroups) {
				entryJSONArray = new JSONArray();
				JSONObject entryJSONObject;
				Presence presence;
				int i=0;
				rosterEntries = rosterGroup.getEntries();
				
				for (RosterEntry rosterEntry : rosterEntries) {
					i++;
					// 名称、帐号、在线状态
					entryJSONObject = new JSONObject();
					entryJSONObject.put("username", rosterEntry.getName());
					entryJSONObject.put("id",
							StringUtils.split(rosterEntry.getUser(), "@")[0]);
					entryJSONObject.put("icons", "");
					presence = roster.getPresence(rosterEntry.getUser());
					entryJSONObject.put("status", presence.getType());

					entryJSONArray.add(entryJSONObject);
				}
				////system.out.println(rosterGroup.getName()+":"+i);
				rosterGroupObject = new JSONObject();
				rosterGroupObject.put("groupName", rosterGroup.getName());
				rosterGroupObject.put("contacts", entryJSONArray);
				rosterGroupObject.put("groupId", id++);
				array.add(rosterGroupObject);
			}
			JSONObject dataJSONObject = new JSONObject();
			dataJSONObject.put("groups", array);
			dataJSONObject.put("groupNumber", array.size());

			JSONObject currentUser = new JSONObject();
			currentUser.put("userid", account);
			AccountManager accountManager = con.getAccountManager();
			currentUser.put("username",
					accountManager.getAccountAttribute("name"));
			currentUser.put("statuc", "");
			currentUser.put("icon", "");
			dataJSONObject.put("currentUser", currentUser);
			return dataJSONObject;
		}
		return null;
	}

	/**
	 *
	 * 登出，可以使用adid或account作为参数
	 * 2015年1月29日
	 * @param adid
	 * @param isAdid 为false时,第一个参数为account值
	 * @return
	 * @throws Exception
	 *
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Boolean logout(String adid, Boolean isAdid) throws Exception {
		String account;
		if (isAdid) {
			DBRow adminRow = fam.getDetailAdmin(Long.valueOf(adid));
			account = adminRow.getString("account");
		} else {
			account = adid;
		}
		Boolean isSuccess = false;
		XMPPConnection con = aquireConnection(account);
		if (con != null) {
			con.disconnect();
			Map<String, Object> connectionStack = PushMessageByOpenfireAction.getConnectionStack();
			connectionStack.remove(account);
			PushMessageByOpenfireAction.setConnectionStack(connectionStack);
			isSuccess = true;
		}
		return isSuccess;
	}

	/**
	 *
	 * 首次登录，会将连接保存在缓存中；再次登录，会检查缓存中是否已存在该连接
	 * 2015年1月29日
	 * @param adid 可以使用adid或account作为参数
	 * @param password 首次登录时必须，密码为明文
	 * @param isAdid 为false时,第一个参数为account值
	 * @param isReceiveMsg 是否拉取信息
	 * @param isAnonymously 安卓推送通知时，使用匿名登录
	 * @return
	 * @throws Exception
	 *
	 */
	@Override
	public Boolean login(String adid, String password, Boolean isAdid,
			Boolean isReceiveMsg, Boolean isAnonymously) throws Exception {
		String account;
		if (isAdid) {
			DBRow adminRow = fam.getDetailAdmin(Long.valueOf(adid));
			account = adminRow.getString("account");
		} else {
			account = adid;
		}

		Map<String, Object> connectionStack = PushMessageByOpenfireAction
				.getConnectionStack();
		Map<String, Object> historyStack = PushMessageByOpenfireAction
				.getHistoryStack();
		Map<String, Object> tempStack = PushMessageByOpenfireAction
				.getTempStack();

		XMPPConnection con = aquireConnection(account);
		if( con == null || !con.isAuthenticated()){
			ConnectionConfiguration config = new ConnectionConfiguration(this.address, Integer.valueOf(this.port));
			con = new XMPPConnection(config);
			con.connect();
			if(isAnonymously){
				con.loginAnonymously();
			}else{
				con.login(account, password, this.domainName);
			}
			// add a listener to receive all messages
			addListener(con);
			// listening contacts presence changes
			addRosterListener(con);

			connectionStack.put(account, con);
		}
		if (isReceiveMsg) {
			// if historyStack exist, add historyStack to tempStack
			if (historyStack.containsKey(account)) {
				JSONArray history = (JSONArray) historyStack.get(account);
				PushMessageByOpenfireAction.setTempStack(resetConversationStack(null, history, account, tempStack));
			}
			// aquire offline message
			OfflineMessageManager offlineManager = new OfflineMessageManager(
					con);
			offlineManager.deleteMessages();// 通知服务器删除离线消息
		}
		return con.isAuthenticated();
	}

	/**
	 *
	 * 向多个用户发送会话消息或者通知消息
	 * 2015年1月29日
	 * @param adid 必须是adid, adid=0时使用系统账户发送消息
	 * @param chatIds 发送的目标用户，用","分割
	 * @param msg 消息内容
	 * @param msgType 包括会话(conversation, msgType=1)、通知(notice, msgType=2)，默认msgType=1
	 * @return
	 * @throws Exception
	 *
	 */
	@Override
	public Boolean sendNotice(long adid, String chatIds, String msg,
			String msgType) throws Exception {
		String account;
		if (adid < 1l) {
			adid = Long.parseLong(systemNotifyAdid);
		} 
		DBRow adminRow = fam.getDetailAdmin(adid);
		account = adminRow.getString("account");
		msgType = StringUtils.isNotBlank(msgType)?msgType:PushMessageByOpenfireAction.CONVERSATION_TYPE_NO;
		
		Boolean issuccess = false;
		List<String> list = Arrays.asList(chatIds.split(","));
		for (String chatId : list) {
			String toUser = chatId + this.allDomainName;
			Message message = new Message();
			message.setProperty("messageType", msgType);
			message.setBody(msg);

			XMPPConnection con = aquireConnection(account);
			if (con != null && StringUtils.isNotBlank(chatId)) {
				sendMessage(con, message, toUser);
				issuccess = true;
			}
		}
		return issuccess;
	}

	
	/**
	 *
	 * 发送消息
	 * 2015年1月29日
	 * @param con
	 * @param message
	 * @param chatId
	 * @throws XMPPException 
	 *
	 */
	private void sendMessage(XMPPConnection con, Message message, String chatId)
			throws XMPPException {
		if (!con.isConnected()) {
			con.connect();
		}
		ChatManager chatmanager = con.getChatManager();
		Chat newChat = chatmanager.createChat(chatId, new MessageListener() {
			public void processMessage(Chat chat, Message message) {
			}
		});

		DeliveryReceiptManager.addDeliveryReceiptRequest(message);

		newChat.sendMessage(message);
	}

	
	/**
	 *
	 * 在线状态监听, 状态变更会存进ConversationStack里
	 * 2015年1月29日
	 * @param con
	 *
	 */
	private void addRosterListener(XMPPConnection con) {
		Roster roster = con.getRoster();
		roster.addRosterListener(new RosterListener() {
			@Override
			public void presenceChanged(Presence presence) {
				if (StringUtils.isNotBlank(presence.getFrom())
						&& StringUtils.isNotBlank(presence.getTo())) {
					String fromId = StringUtils.split(presence.getFrom(), "@")[0];
					String adid = StringUtils.split(presence.getTo(), "@")[0];
					String status = presence.getType().name();
					Date date = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat(
							"yyyy-MM-dd hh:mm:ss");
					String dateStr = sdf.format(date);

					JSONObject statusChange = buildJSONObject(
							PushMessageByOpenfireAction.PRESENCE_TYPE, dateStr,
							fromId, "", "", status);
					Map<String, Object> stack = resetConversationStack(statusChange, null, adid,
							PushMessageByOpenfireAction.getConversationStack());
					PushMessageByOpenfireAction.setConversationStack(stack);
				} 

			}

			@Override
			public void entriesUpdated(Collection<String> arg0) {
			}

			@Override
			public void entriesAdded(Collection<String> arg0) {
			}

			@Override
			public void entriesDeleted(Collection<String> arg0) {
			}
		});
	}

	/**
	 *
	 * 消息监听，包括会话(conversation, msgType=1)、通知(notice, msgType=2)、安卓端通知(notice, msgType=0)
	 * 消息会存进ConversationStack里
	 * 2015年1月29日
	 * @param con
	 *
	 */
	private void addListener(XMPPConnection con) {
		PacketFilter filterMessage = new PacketTypeFilter(Message.class);
		PacketListener myListener = new PacketListener() {
			public void processPacket(Packet packet) {
				String userId = StringUtils.split(packet.getTo(), "@")[0];
				String fromId = StringUtils.split(packet.getFrom(), "@")[0];
				Message message = (Message) packet;
				String msg = message.getBody();
				String msgType = (message.getProperty("messageType") != null) ? message
						.getProperty("messageType").toString() : "";
				String msgTypeDetail = null;
				//不处理安卓端通知
				if(!StringUtils.equals(msgType, "0")){
					msgTypeDetail = (StringUtils.equals(msgType, "1"))?PushMessageByOpenfireAction.CONVERSATION_TYPE:PushMessageByOpenfireAction.NOTICE_TYPE;		
					Date date = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat(
							"yyyy-MM-dd hh:mm:ss");
					String dateStr = sdf.format(date);
					JSONObject conversation = buildJSONObject(msgTypeDetail,
							dateStr, fromId, msg, msgType, "");
					Map<String, Object> stack = resetConversationStack(conversation, null, userId,
							PushMessageByOpenfireAction.getConversationStack());
					PushMessageByOpenfireAction.setConversationStack(stack);
				}
			}
		};
		// register the listener to the connection
		con.addPacketListener(myListener, filterMessage);
	}

	private Map<String, Object> resetConversationStack(JSONObject conversation,
			JSONArray conversationArray, String userId,
			Map<String, Object> stack) {
		JSONArray array = new JSONArray();
		if (stack.containsKey(userId) && stack.get(userId) != null) {
			array = (JSONArray) stack.get(userId);
		}
		if (conversation != null) {
			array.add(conversation);
			stack.put(userId, array);
		}
		if (conversationArray != null && conversationArray.size() > 0) {
			array.addAll(conversationArray);
			stack.put(userId, array);
		}
		return stack;
	}

	private JSONObject buildJSONObject(String type, String time, String userid,
			String msg, String msgType, String status) {
		JSONObject object = new JSONObject();
		object.put("type", type);
		object.put("time", time);
		object.put("userid", userid);
		object.put("msg", msg);
		object.put("msgType", msgType);
		object.put("status", status);
		return object;
	}
	
	/**
	 *
	 * 获取XMPPConnection
	 * 2015年1月29日
	 * @param account
	 * @return
	 *
	 */
	private XMPPConnection aquireConnection(String account){
		Map<String, Object> connectionStack = PushMessageByOpenfireAction.getConnectionStack();
		XMPPConnection con = null;
		if (connectionStack.containsKey(account)
				&& connectionStack.get(account) != null) {
			con = ((XMPPConnection) connectionStack.get(account));
			if (!con.isConnected()) {
				try {
					con.connect();
				} catch (XMPPException e) {
					e.printStackTrace();
				}
			}
		}
		return con;
	}

	@Override
	public boolean sysSendNotice(String accounts, AndroidPushMessge pushMessage)
			throws Exception {

		try {

			/*DBRow adminRow = fam.getDetailAdmin(Long.parseLong(systemNotifyAdid));
			XMPPConnection con = getConnection(adminRow.getString("account"),systemNofiyPassword, adminRow.get("adid", 0l));*/
			/******start author TW********/
			Map<String, Object> connectionStack = PushMessageByOpenfireAction
					.getConnectionStack();
			XMPPConnection con= (XMPPConnection) connectionStack.get("anonymously");
			if(con==null){
				ConnectionConfiguration configuration=new ConnectionConfiguration(this.address, Integer.valueOf(this.port));
				configuration.setCompressionEnabled(true);
				configuration.setSASLAuthenticationEnabled(false);
				configuration.setSelfSignedCertificateEnabled(true);
				con=new XMPPConnection(configuration);
				connectionStack.put("anonymously", con);
				con.connect();
				con.loginAnonymously();
			}
			if( !con.isConnected()){
				connectionStack.remove("anonymously");
				ConnectionConfiguration configuration=new ConnectionConfiguration(this.address, Integer.valueOf(this.port));
				configuration.setCompressionEnabled(true);
				configuration.setSASLAuthenticationEnabled(false);
				configuration.setSelfSignedCertificateEnabled(true);
				con=new XMPPConnection(configuration);
				connectionStack.put("anonymously", con);
				con.connect();
				con.loginAnonymously();
			}

	        
			/******end*****/
			List<String> list = Arrays.asList(accounts.split(","));
			for (String chatId : list) {
				String toUser = chatId + this.allDomainName;
				Message message = new Message();
				message.setProperty("messageType", pushMessage.getMessageType() );
				message.setBody(SystemNotifyMsgAppend + pushMessage.getData().toString());
				sendMessage(con, message, toUser);
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			////system.out.println("-------------------" + e);
		}
		return false;
	}

	/**
	 * 获取一个Connecton的lianj
	 * 
	 * @param adid
	 * @param password
	 * @return 
	 * @throws Exception
	 */
	private XMPPConnection getConnection(String account, String password,
			long adid) throws Exception {
		Map<String, Object> connectionStack = PushMessageByOpenfireAction
				.getConnectionStack();
		XMPPConnection returnConnection = null;
		if (connectionStack != null
				&& connectionStack.containsKey(account)
				&& connectionStack.get(account) != null
				&& ((XMPPConnection) connectionStack.get(account))
						.isConnected()) {
			returnConnection = (XMPPConnection) connectionStack.get(account);
		} else {
			login(String.valueOf(adid), password, true, false, true);
			returnConnection = (XMPPConnection) connectionStack.get(account);
		}
		return returnConnection;
	}

	public void setOpenFireSetBean(OpenFireSetBean openFireSetBean) {
		this.openFireSetBean = openFireSetBean;
	}
	public void setFam(FloorAdminMgr fam) {
		this.fam = fam;
	}
	
	
	
}
