package com.cwc.app.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import seamoonotp.seamoonapi;
import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.AccountOrPwdIncorrectException;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.app.exception.admin.OldPwdIncorrectException;
import com.cwc.app.exception.admin.RoleHaveAdminException;
import com.cwc.app.exception.admin.RoleIsExistException;
import com.cwc.app.exception.admin.SuperAdminException;
import com.cwc.app.exception.admin.SuperRoleException;
import com.cwc.app.exception.admin.TokenCheckNotPassException;
import com.cwc.app.exception.admin.VerifyCodeIncorrectException;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.gzy.FloorAdminMgrGZY;
import com.cwc.app.floor.api.zj.FloorTitleMgrZJ;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.key.ControlTypeKey;
import com.cwc.app.lucene.zyj.AdminIndexMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.authentication.AdminAuthenCenter;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;
import com.cwc.util.Tree;

/**
 * 该类用于对管理员后台的一些操作
 * 
 * @author TurboShop
 *
 *         TurboShop.cn all rights reserved.
 */
public class AdminMgr implements AdminMgrIFace {
	static Logger log = Logger.getLogger("ACTION");
	private FloorAdminMgr fam;
	private FloorAdminMgrGZY floorAdminMgrGZY;
	private FloorTitleMgrZJ floorTitleMgrZJ;
	private FloorCatalogMgr floorCatalogMgr;
	private SystemConfigIFace systemConfig;
	
	/**
	 * 增加管理员
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void addAdmin(HttpServletRequest request) throws AdminIsExistException, Exception {
		try {
			String account = StringUtil.getString(request, "account");
			String pwd = StringUtil.getString(request, "pwd");
			long adgid = StringUtil.getLong(request, "adgid");
			String employe_name = StringUtil.getString(request, "employe_name");
			String employe_summary = StringUtil.getString(request, "employe_summary");
			long ps_id = StringUtil.getLong(request, "ps_id");
			
			String email = StringUtil.getString(request, "email");
			String skype = StringUtil.getString(request, "skype");
			String msn = StringUtil.getString(request, "msn");
			
			if (fam.getDetailAdminByAccount(account) == null) {
				DBRow row = new DBRow();
				row.add("post_date", DateUtil.NowStr());
				row.add("account", account);
				row.add("adgid", adgid);
				row.add("ps_id", ps_id);
				row.add("pwd", StringUtil.getMD5(pwd));
				row.add("llock", 0);
				row.add("employe_name", employe_name);
				row.add("employe_summary", employe_summary);
				row.add("email", email);
				row.add("skype", skype);
				row.add("msn", msn);
				
				fam.addAdmin(row);
			} else {
				throw new AdminIsExistException();
			}
		} catch (AdminIsExistException e) {
			throw new AdminIsExistException();
		} catch (Exception e) {
			throw new SystemException(e, "addAdmin", log);
		}
	}
	
	/**
	 * 删除管理员
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void delAdmin(HttpServletRequest request) throws SuperAdminException, Exception {
		try {
			String account = StringUtil.getString(request, "account");
			if (account.equals("admin")) {
				throw new SuperAdminException();
			}
			
			long adid = StringUtil.getLong(request, "adid");
			fam.delAdmin(adid);
		} catch (SuperAdminException e) {
			throw new SuperAdminException();
		} catch (Exception e) {
			throw new SystemException(e, "delAdmin", log);
		}
	}
	
	/**
	 * 锁定管理员
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void lockAdmin(HttpServletRequest request) throws SuperAdminException, Exception {
		try {
			String account = StringUtil.getString(request, "account");
			if (account.equals("admin")) {
				throw new SuperAdminException();
			}
			
			long adid = StringUtil.getLong(request, "adid");
			DBRow newInfo = new DBRow();
			newInfo.add("llock", "1");
			fam.modifyAdmin(adid, newInfo);
		} catch (SuperAdminException e) {
			throw new SuperAdminException();
		} catch (Exception e) {
			throw new SystemException(e, "lockAdmin", log);
		}
	}
	
	/**
	 * 解锁管理员
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void unLockAdmin(HttpServletRequest request) throws Exception {
		try {
			long adid = StringUtil.getLong(request, "adid");
			DBRow newInfo = new DBRow();
			newInfo.add("llock", "0");
			fam.modifyAdmin(adid, newInfo);
		} catch (Exception e) {
			throw new SystemException(e, "unLockAdmin", log);
		}
	}
	
	/**
	 * 获得管理员详细信息
	 * 
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailAdmin(long adid) throws Exception {
		try {
			return (fam.getDetailAdmin(adid));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailAdmin", log);
		}
	}
	
	/**
	 * 获得所有管理员
	 * 
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAdmin(PageCtrl pc) throws Exception {
		try {
			return (fam.getAllAdmin(pc));
		} catch (Exception e) {
			throw new SystemException(e, "getAllAdmin", log);
		}
	}
	
	/**
	 * 获得所有角色
	 * 
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAdminGroup(PageCtrl pc) throws Exception {
		try {
			return (fam.getAllAdminGroup(pc));
		} catch (Exception e) {
			throw new SystemException(e, "getAllAdminGroup", log);
		}
	}
	
	/**
	 * 获得角色详细信息
	 * 
	 * @param adgid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailAdminGroup(long adgid) throws Exception {
		try {
			return (fam.getDetailAdminGroup(adgid));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailAdminGroup", log);
		}
	}
	
	/**
	 * 通过帐号获得管理员详细信息
	 * 
	 * @param account
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailAdminByAccount(String account) throws Exception {
		try {
			return (fam.getDetailAdminByAccount(account));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailAdminByAccount", log);
		}
	}
	
	/**
	 * 修改管理员密码
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void modAdminPwd(HttpServletRequest request) throws OldPwdIncorrectException, Exception {
		try {
			HttpSession ses = StringUtil.getSession(request);
			AdminLoginBean adminLoggerBean = getAdminLoginBean(ses);
			long adid = adminLoggerBean.getAdid();
			String pwd = StringUtil.getString(request, "pwd");
			String oldpwd = StringUtil.getString(request, "oldpwd");
			
			DBRow detailAdmin = fam.getDetailAdmin(adid);
			if (detailAdmin.getString("pwd").equals(StringUtil.getMD5(oldpwd))) {
				DBRow newInfo = new DBRow();
				newInfo.add("pwd", StringUtil.getMD5(pwd));
				fam.modifyAdmin(adid, newInfo);
				
				// 重新登录
				this.adminLogout(request);
			} else {
				throw new OldPwdIncorrectException();
			}
		} catch (OldPwdIncorrectException e) {
			throw new OldPwdIncorrectException();
		} catch (Exception e) {
			throw new SystemException(e, "modAdminPwd", log);
		}
	}
	
	/**
	 * 管理员登陆
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public void adminLogin(HttpServletRequest request, HttpServletResponse response)
			throws AccountOrPwdIncorrectException, VerifyCodeIncorrectException, AccountNotPermitLoginException,
			TokenCheckNotPassException, Exception {
		try {
			String pwd = StringUtil.getString(request, "pwd");
			String account = StringUtil.getString(request, "account");
			String licence = StringUtil.getString(request, "licence");
			int remember = StringUtil.getInt(request, "remember");
			int loginType = StringUtil.getInt(request, "loginType");// =1，sso登录无需验证验证码
			
			if (3 == loginType) {
				adminLogin(loginType, request, response);
			} else {
				adminLogin(request, response, account, pwd, licence, remember, loginType, null);
			}
		} catch (AccountOrPwdIncorrectException e) {
			throw e;
		} catch (VerifyCodeIncorrectException e) {
			throw e;
		} catch (AccountNotPermitLoginException e) {
			throw e;
		} catch (TokenCheckNotPassException e) {
			throw e;
		} catch (AdminIsExistException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "adminLogin", log);
		}
	}
	
	@Override
	public void adminLogin(int loginType, HttpServletRequest request, HttpServletResponse response)
			throws AccountOrPwdIncorrectException, VerifyCodeIncorrectException, AccountNotPermitLoginException,
			AdminIsExistException, TokenCheckNotPassException, Exception {
		
		int isThirdWitness = StringUtil.getInt(request, "isThirdWitness");// 1, 2
		String userName = StringUtil.getString(request, "userName");
		String tokenSn = StringUtil.getString(request, "tokenSn");
		String witnessUserName = StringUtil.getString(request, "witnessUserName");
		String witnessTokenSn = StringUtil.getString(request, "witnessTokenSn");
		int remember = StringUtil.getInt(request, "remember");
		String pwd = StringUtil.getString(request, "pwd");
		String pwd_user = StringUtil.getString(request, "pwd_user");
		HttpSession ses = StringUtil.getSession(request);
		String login_date = DateUtil.NowStr();
		if (1 == isThirdWitness) {
			DBRow detailAdmin1 = fam.getDetailAdminByAccount(witnessUserName);// 第三见证人
			DBRow detailAdmin2 = fam.getDetailAdminByAccount(userName);
			if (null != detailAdmin1 && null != detailAdmin2) {
				if (detailAdmin2.getString("pwd").equals(StringUtil.getMD5(pwd_user))) {
					String sn1 = detailAdmin1.getString("register_token");
					DBRow tokenInfo1 = floorAdminMgrGZY.checkTokenSn(sn1);
					String sninfo1 = "";
					if (null != tokenInfo1) {
						sninfo1 = tokenInfo1.getString("sninfo");
					}
					if (!"".equals(sninfo1)) {
						seamoonapi sc1 = new seamoonapi();
						String checkStr1 = sc1.checkpassword(sninfo1, witnessTokenSn);
						if (checkStr1.length() > 3) {
							if (fam.getDetailRoleByAdgid(detailAdmin2.get("adgid", 0l)) == null) {
								throw new AccountOrPwdIncorrectException();
							}
							if (detailAdmin2.get("llock", 0) == 1) {
								throw new AccountNotPermitLoginException();
							}
							if (loginType == 1 || loginType == 3) {
								AdminLoginBean adminLoggerBean1 = new AdminLoginBean();
								adminLoggerBean1.setIsLoginRightPath();
								StringUtil.getSession(request).setAttribute(Config.adminSesion, adminLoggerBean1);
							}
							AdminLoginBean adminLoggerBean1 = this.getAdminLoginBean(StringUtil.getSession(request));
							if (adminLoggerBean1 == null)// 有时adminLoggerBean为null
							{
								throw new VerifyCodeIncorrectException();
							}
							adminLoggerBean1.setAccount(userName);
							adminLoggerBean1.setAdgid(detailAdmin2.get("adgid", 0l));
							adminLoggerBean1.setAdid(detailAdmin2.get("adid", 0l));
							adminLoggerBean1.setPs_id(detailAdmin2.get("ps_id", 0l));
							adminLoggerBean1.setEmail(detailAdmin2.getString("email"));
							adminLoggerBean1.setLoginDate(login_date);
							adminLoggerBean1.setAttach(null);
							adminLoggerBean1.setStorage_name(detailAdmin2.getString("title"));
							adminLoggerBean1.setEmploye_name(detailAdmin2.getString("employe_name"));
							adminLoggerBean1.setTitles(floorTitleMgrZJ.getTitlesByAdid(detailAdmin2.get("adid", 0l)));
							adminLoggerBean1.setIsLogin();
							ses.setAttribute(Config.adminSesion, adminLoggerBean1);
							if (remember == 1) {
								StringUtil.setCookie(response, "account", userName, 60 * 60 * 24 * 365);
							} else {
								StringUtil.setCookie(response, "account", userName, 0);
							}
							DBRow newInfo = new DBRow();
							newInfo.add("last_login_date", login_date);
							fam.modifyAdmin(detailAdmin2.get("adid", 0l), newInfo);
						} else {
							throw new TokenCheckNotPassException();
						}
					} else {
						throw new TokenCheckNotPassException();
					}
				} else {
					
					throw new AccountOrPwdIncorrectException();
				}
			} else {
				throw new AdminIsExistException();
			}
		} else {
			DBRow detailAdmin = fam.getDetailAdminByAccount(userName);
			if (null == detailAdmin) {
				throw new AdminIsExistException();
			} else {
				String sn = detailAdmin.getString("register_token");
				DBRow tokenInfo = floorAdminMgrGZY.checkTokenSn(sn);
				String sninfo = "";
				if (null != tokenInfo) {
					sninfo = tokenInfo.getString("sninfo");
				}
				if (!"".equals(sninfo)) {
					seamoonapi sc = new seamoonapi();
					String checkStr = sc.checkpassword(sninfo, tokenSn);
					if (checkStr.length() > 3) {
						if (fam.getDetailRoleByAdgid(detailAdmin.get("adgid", 0l)) == null) {
							throw new AccountOrPwdIncorrectException();
						}
						if (detailAdmin.get("llock", 0) == 1) {
							throw new AccountNotPermitLoginException();
						}
						if (loginType == 1 || loginType == 3) {
							AdminLoginBean adminLoggerBean = new AdminLoginBean();
							adminLoggerBean.setIsLoginRightPath();
							StringUtil.getSession(request).setAttribute(Config.adminSesion, adminLoggerBean);
						}
						AdminLoginBean adminLoggerBean = this.getAdminLoginBean(StringUtil.getSession(request));
						if (adminLoggerBean == null)// 有时adminLoggerBean为null
						{
							throw new VerifyCodeIncorrectException();
						}
						adminLoggerBean.setAccount(userName);
						adminLoggerBean.setAdgid(detailAdmin.get("adgid", 0l));
						adminLoggerBean.setAdid(detailAdmin.get("adid", 0l));
						adminLoggerBean.setPs_id(detailAdmin.get("ps_id", 0l));
						adminLoggerBean.setEmail(detailAdmin.getString("email"));
						adminLoggerBean.setStorage_name(detailAdmin.getString("title"));
						adminLoggerBean.setLoginDate(login_date);
						adminLoggerBean.setAttach(null);
						adminLoggerBean.setEmploye_name(detailAdmin.getString("employe_name"));
						adminLoggerBean.setIsLogin();
						ses.setAttribute(Config.adminSesion, adminLoggerBean);
						if (remember == 1) {
							StringUtil.setCookie(response, "account", userName, 60 * 60 * 24 * 365);
						} else {
							StringUtil.setCookie(response, "account", userName, 0);
						}
						DBRow newInfo = new DBRow();
						newInfo.add("last_login_date", login_date);
						fam.modifyAdmin(detailAdmin.get("adid", 0l), newInfo);
					} else {
						throw new TokenCheckNotPassException();
					}
				} else {
					throw new TokenCheckNotPassException();
				}
			}
		}
	}
	
	/**
	 * 帐号登录接口
	 * 
	 * @param request
	 * @param response
	 * @param account
	 * @param pwd
	 * @param licence
	 * @param remember //浏览器是否记录登录帐号 1-记录
	 * @param loginType //1-SSO登录
	 * @throws AccountOrPwdIncorrectException
	 * @throws VerifyCodeIncorrectException
	 * @throws AccountNotPermitLoginException
	 * @throws Exception
	 */
	public void adminLogin(HttpServletRequest request, HttpServletResponse response, String account, String pwd,
			String licence, int remember, int loginType, DBRow attach) throws AccountOrPwdIncorrectException,
			VerifyCodeIncorrectException, AccountNotPermitLoginException, Exception {
		HttpSession ses = StringUtil.getSession(request);
		String login_date = DateUtil.NowStr();
		
		// ||licence.equals( LoginLicence.getTheLicence(request) )
		// {
		DBRow detailAdmin = fam.getDetailAdminByAccount(account);
		
		// //system.out.println("-->"+detailAdmin.getString("pwd")+" - "+detailAdmin.getString("pwd"));
		
		if (detailAdmin != null && detailAdmin.getString("pwd").equals(StringUtil.getMD5(pwd))) {
			// 先检查下角色是否存在，因为删除角色并不会删除帐号
			// 如果角色不存在，则不让登录，以免出错
			if (fam.getDetailRoleByAdgid(detailAdmin.get("adgid", 0l)) == null) {
				throw new AccountOrPwdIncorrectException();
			}
			
			if (detailAdmin.get("llock", 0) == 1) {
				throw new AccountNotPermitLoginException();
			}
			
			/**
			 * 如果是SSO登录，需要特别处理，手工创建一个adminLoggerBean，设置正确登录入口（正常情况，该操作在AdminAuthorizationFilter执行）
			 */
			if (loginType == 1) {
				AdminLoginBean adminLoggerBean = new AdminLoginBean();
				adminLoggerBean.setIsLoginRightPath();
				StringUtil.getSession(request).setAttribute(Config.adminSesion, adminLoggerBean);
			}
			
			// 登录后把islogin标记下，才算真正登录
			AdminLoginBean adminLoggerBean = this.getAdminLoginBean(StringUtil.getSession(request));
			
			if (adminLoggerBean == null)// 有时adminLoggerBean为null
			{
				throw new VerifyCodeIncorrectException();
			}
			
			adminLoggerBean.setAccount(account);
			adminLoggerBean.setAdgid(detailAdmin.get("adgid", 0l));
			adminLoggerBean.setAdid(detailAdmin.get("adid", 0l));
			adminLoggerBean.setPs_id(detailAdmin.get("ps_id", 0l));
			adminLoggerBean.setEmail(detailAdmin.getString("email"));
			adminLoggerBean.setLoginDate(login_date);
			adminLoggerBean.setAttach(attach);
			adminLoggerBean.setStorage_name(detailAdmin.getString("title"));
			adminLoggerBean.setEmploye_name(detailAdmin.getString("employe_name"));
			adminLoggerBean.setTitles(floorTitleMgrZJ.getTitlesByAdid(detailAdmin.get("adid", 0l)));
			
			adminLoggerBean.setIsLogin();
			ses.setAttribute(Config.adminSesion, adminLoggerBean);
			// 记住登陆帐号
			if (remember == 1) {
				StringUtil.setCookie(response, "account", account, 60 * 60 * 24 * 365);
			} else {
				StringUtil.setCookie(response, "account", account, 0);
			}
			
			DBRow newInfo = new DBRow();
			newInfo.add("last_login_date", login_date);
			fam.modifyAdmin(detailAdmin.get("adid", 0l), newInfo);
		} else {
			throw new AccountOrPwdIncorrectException();
		}
		// }
		// else
		// {
		// throw new VerifyCodeIncorrectException();
		// }
	}
	
	/**
	 * 获得管理员菜单树权限
	 * 
	 * @param adgid
	 * @return
	 * @throws Exception
	 */
	private ArrayList getPermitPage(long adgid) throws Exception {
		try {
			ArrayList al = new ArrayList();
			DBRow permitPages[] = fam.getControlTreeByAdgid(adgid);
			
			for (int i = 0; i < permitPages.length; i++) {
				String uri = permitPages[i].getString("link");
				if (uri.indexOf("?") >= 0) {
					int l2 = uri.indexOf("?");
					uri = uri.substring(0, l2);
				}
				
				al.add(uri);
			}
			
			return (al);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitPage", log);
		}
	}
	
	private Long safeLong(Object v) {
		return (v == null) ? 0L : Long.valueOf(v.toString());
	}
	
	private Boolean safeBoolean(Object v) {
		return (v == null) ? false : (Boolean) v;
	}
	
	/**
	 * 从session获取AdminLoginBean
	 * @param HttpSession
	 * @return AdminLoginBean
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public AdminLoginBean getAdminLoginBean(HttpSession session){
		
		AdminLoginBean adminBean = null;
		
		if(session.getAttribute(Config.adminSesion) != null){
			
			Object param = session.getAttribute(Config.adminSesion);
			
			if(param instanceof AdminLoginBean){
				
				adminBean = (AdminLoginBean)param;
				
			}else if(param instanceof Map){
				
				adminBean = new AdminLoginBean();
				
				@SuppressWarnings("unchecked")
				Map<String,Object> adminMap = (Map<String,Object>)param;
				
				adminBean.setAccount(adminMap.get("account") == null ? null : adminMap.get("account").toString());
				adminBean.setAdid(adminMap.get("adid") == null ? null : Long.valueOf(adminMap.get("adid").toString()));
				adminBean.setCity(adminMap.get("city") == null ? null : Long.valueOf(adminMap.get("city").toString()));
				adminBean.setEmail(adminMap.get("email") == null ? null : adminMap.get("email").toString());
				adminBean.setEmploye_name(adminMap.get("employe_name") == null ? null : adminMap.get("employe_name").toString());
				adminBean.setLoginDate(adminMap.get("loginDate") == null ? null : adminMap.get("loginDate").toString());
				adminBean.setProvince(adminMap.get("province") == null ? null : Long.valueOf(adminMap.get("province").toString()));
				adminBean.setPs_id(adminMap.get("ps_id") == null ? null : Long.valueOf(adminMap.get("ps_id").toString()));
				adminBean.setAdministrator(adminMap.get("administrator") == null ? null : Boolean.valueOf(adminMap.get("administrator").toString()));
				
				adminBean.setCorporationId(adminMap.get("corporationId") == null ? 0 : Long.valueOf(adminMap.get("corporationId").toString()));
				adminBean.setCorporationType(adminMap.get("corporationType") == null ? 0 : Long.valueOf(adminMap.get("corporationType").toString()));
				
				if(Boolean.valueOf(adminMap.get("login") == null ? null : adminMap.get("login").toString())){
					
					adminBean.setIsLogin();
				}
				
				if(Boolean.valueOf(adminMap.get("loginRightPath") == null ? null : adminMap.get("loginRightPath").toString())){
					
					adminBean.setIsLoginRightPath();
				}
				
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> titles = adminMap.containsKey("titles") ? (List<Map<String,Object>>)adminMap.get("titles") : null;
				
				if(titles != null){
					
					DBRow[] titlesArray = new DBRow[titles.size()];
					
					for(int i=0; i<titlesArray.length; i++){
						
						titlesArray[i] = new DBRow();
						
						for(@SuppressWarnings("rawtypes") Map.Entry e: titles.get(i).entrySet()){
							
							titlesArray[i].addObject((String)e.getKey(), e.getValue());
						}
					}
					
					adminBean.setTitles(titlesArray);
				}
				
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> depts = adminMap.containsKey("department") ? (List<Map<String,Object>>)adminMap.get("department") : null;
				
				if(depts != null){
					
					DBRow[] deptsArray = new DBRow[depts.size()];
					
					for(int i=0; i<deptsArray.length; i++){
						
						deptsArray[i] = new DBRow();
						
						for(@SuppressWarnings("rawtypes") Map.Entry e: depts.get(i).entrySet()){
							
							deptsArray[i].addObject((String)e.getKey(), e.getValue());
						}
					}
					
					adminBean.setDepartment(deptsArray);
				}
				
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> wares = adminMap.containsKey("warehouse") ? (List<Map<String,Object>>)adminMap.get("warehouse") : null;
				
				if(wares != null){
					
					DBRow[] waresArray = new DBRow[wares.size()];
					
					for(int i=0; i<waresArray.length; i++){
						
						waresArray[i] = new DBRow();
						
						for(@SuppressWarnings("rawtypes") Map.Entry e: wares.get(i).entrySet()){
							
							waresArray[i].addObject((String)e.getKey(), e.getValue());
						}
					}
					
					adminBean.setWarehouse(waresArray);
				}
			}
		}
		
		return adminBean;
	}
	/*public AdminLoginBean getAdminLoginBean(HttpSession ses)
	{
		if ( ses.getAttribute(Config.adminSesion)!=null )
		{
			Object v = ses.getAttribute(Config.adminSesion);
			if (v instanceof AdminLoginBean) {
				return (AdminLoginBean) v;
			} else if (v instanceof Map) {
				Map<String, Object> m = (Map<String, Object>) v;
				AdminLoginBean alb = new AdminLoginBean();
				try {
					alb.setAccount((String) m.get("account"));
					alb.setAdgid(safeLong(m.get("adgid")));
					alb.setAdid(safeLong(m.get("adid")));
					alb.setCity(safeLong(m.get("city")));
					alb.setEmail((String) m.get("email"));
					alb.setEmploye_name((String) m.get("employe_name"));
					alb.setStorage_name((String) m.get("storage_name"));
					if (safeBoolean(m.get("login")))
						alb.setIsLogin();
					if (safeBoolean(m.get("loginRightPath")))
						alb.setIsLoginRightPath();
					alb.setLoginDate((String) m.get("loginDate"));
					alb.setProvince(safeLong(m.get("province")));
					alb.setPs_id(safeLong(m.get("ps_id")));
					
					Map<String, Object> attach = m.containsKey("attach") ? (Map<String, Object>) m.get("attach") : null;
					if (attach != null) {
						DBRow attachBean = new DBRow();
						attachBean.putAll(attach);
						alb.setAttach(attachBean);
					}
					
					List<Map<String, Object>> titles = m.containsKey("titles") ? (List<Map<String, Object>>) m
							.get("titles") : null;
					if (titles != null) {
						DBRow[] titlesArray = new DBRow[titles.size()];
						for (int i = 0; i < titlesArray.length; i++) {
							titlesArray[i] = new DBRow();
							for (Map.Entry e : titles.get(i).entrySet()) {
								titlesArray[i].addObject((String) e.getKey(), e.getValue());
							}
						}
						alb.setTitles(titlesArray);
					}
					
					return alb;
				} catch (Exception e) {
					e.printStackTrace();
					
				}
				
			}
			return null;
		} else {
			return (null);
		}
	}*/
	
	/**
	 * 管理员推出登录
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void adminLogout(HttpServletRequest request) throws Exception {
		AdminLoginBean adminLoggerBean = this.getAdminLoginBean(StringUtil.getSession(request));
		adminLoggerBean.setNotLogin();
		adminLoggerBean.setAttach(null);
		StringUtil.getSession(request).setAttribute(Config.adminSesion, adminLoggerBean);
	}
	
	/**
	 * 
	 * @param response
	 * @param request
	 * @throws Exception
	 */
	public void markLocal(HttpServletResponse response, HttpServletRequest request) throws Exception {
		String local = StringUtil.getString(request, "local");
		local = local.toLowerCase();
		StringUtil.setCookie(response, "turbocwcshop_local", local, 3600 * 24 * 365);
	}
	
	/**
	 * 判断用户是否从正确后台路径登录
	 * 
	 * @param response
	 * @param request
	 * @throws Exception
	 */
	public void isRightAdminPath(HttpServletResponse response, HttpServletRequest request) throws Exception {
		AdminLoginBean adminLoggerBean = this.getAdminLoginBean(StringUtil.getSession(request));
		
		if (adminLoggerBean == null || !adminLoggerBean.isLoginRightPath()) {
			response.sendRedirect("../errorAdminPath");
			return;
		}
	}
	
	/**
	 * 获得管理菜单
	 * 
	 * @param parentid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getControlTreeByParentid(long parentid) throws Exception {
		try {
			return (fam.getControlTreeByParentid(parentid));
		} catch (Exception e) {
			throw new SystemException(e, "getControlTreeByParentid", log);
		}
	}
	
	/**
	 * 获得管理菜单，按sort排序
	 * 
	 * @param parentid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getControlTreeByParentidOrderBySort(long parentid, int control_type) throws Exception {
		try {
			return (fam.getControlTreeByParentidOrderBySort(parentid, control_type));
		} catch (Exception e) {
			throw new SystemException(e, "getControlTreeByParentidOrderBySort", log);
		}
	}
	
	public JSONArray getLeftTree() throws Exception {
		DBRow[] parentLefts = fam.getControlTreeByParentidOrderBySort(0, ControlTypeKey.Left);
		
		JSONArray left = new JSONArray();
		
		for (int i = 0; i < parentLefts.length; i++) {
			String title = parentLefts[i].getString("title");
			String url = parentLefts[i].getString("link");
			String ico = parentLefts[i].getString("ico");
			long id = parentLefts[i].get("id", 0l);
			
			JSONArray children = new JSONArray();
			
			DBRow[] sonLefts = fam.getControlTreeByParentidOrderBySort(id, ControlTypeKey.Left);
			for (int j = 0; j < sonLefts.length; j++) {
				String sonTitle = sonLefts[j].getString("title");
				String sonUrl = sonLefts[j].getString("link");
				String sonIco = sonLefts[j].getString("ico");
				long sonId = sonLefts[j].get("id", 0l);
				
				JSONObject sonLeft = new JSONObject();
				sonLeft.put("title", sonTitle);
				sonLeft.put("url", sonUrl);
				sonLeft.put("ico", sonIco);
				sonLeft.put("id", sonId);
				
				children.put(sonLeft);
			}
			
			JSONObject parentLeft = new JSONObject();
			parentLeft.put("title", title);
			parentLeft.put("url", url);
			parentLeft.put("ico", ico);
			parentLeft.put("children", children);
			parentLeft.put("id", id);
			
			left.put(parentLeft);
		}
		
		return left;
	}
	
	/**
	 * @author zhanjie
	 * @param adid 登录人ID
	 * @param control_type 需要的权限类型
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminControlTreeByAdid(long adid, int control_type) throws Exception {
		try {
			return fam.getAdminControlTreeByAdid(adid, control_type);
		} catch (Exception e) {
			throw new SystemException(e, "getAdminControlTreeByAdid", log);
		}
	}
	
	/**
	 * 通过方法名获得权限详细信息
	 * 
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailAuthenticationActionByAction(String action) throws Exception {
		try {
			return (fam.getDetailAuthenticationActionByAction(action));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailAuthenticationActionByAction", log);
		}
	}
	
	/**
	 * 通过连接获得管理菜单相信信息
	 * 
	 * @param link
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailControlTreeByLink(String link) throws Exception {
		try {
			return (fam.getDetailControlTreeByLink(link));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailControlTreeByLink", log);
		}
	}
	
	/**
	 * 通过ID获得页面树详细
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailControlTreeById(long id) throws Exception {
		try {
			return (fam.getDetailControlTreeById(id));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailControlTreeById", log);
		}
	}
	
	/**
	 * 获得树状控制页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getControlTree() throws Exception {
		try {
			Tree tree = new Tree(ConfigBean.getStringValue("control_tree"));
			DBRow rows[] = tree.getTree();
			return (rows);
		} catch (Exception e) {
			throw new SystemException(e, "getControlTree", log);
		}
	}
	
	public DBRow[] getControlTreeAllByControlTypeSort(int control_type) throws Exception {
		try {
			Tree tree = new Tree(ConfigBean.getStringValue("control_tree"), "control_type=" + control_type);
			
			return tree.getTree();
		} catch (Exception e) {
			throw new SystemException(e, "getControlTreeAllByControlTypeSort", log);
		}
	}
	
	/**
	 * 获得页面资源允许的角色ID
	 * 
	 * @param page
	 * @return
	 * @throws Exception
	 */
	public ArrayList getPermitPageAdgid(String page) throws Exception {
		ArrayList out = new ArrayList();
		
		try {
			DBRow rows[] = fam.getPermitPageAdgid(page);
			
			for (int i = 0; i < rows.length; i++) {
				out.add(rows[i].getString("adgid"));
			}
			
			return (out);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitPageAdgid", log);
		}
	}
	
	/**
	 * 获得页面资源允许的管理员ID(扩展权限)
	 * 
	 * @param page
	 * @return
	 * @throws Exception
	 */
	public ArrayList getPermitPageAdid(String page) throws Exception {
		ArrayList out = new ArrayList();
		
		try {
			
			DBRow rows[] = fam.getPermitPageAdid(page);
			
			for (int i = 0; i < rows.length; i++) {
				out.add(rows[i].getString("adid"));
			}
			
			return (out);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitPageAdid", log);
		}
	}
	
	/**
	 * 获得 action资源允许的角色ID
	 * 
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public ArrayList getPermitActionAdgid(String action) throws Exception {
		ArrayList out = new ArrayList();
		
		try {
			DBRow rows[] = fam.getPermitActionAdgid(action);
			
			for (int i = 0; i < rows.length; i++) {
				out.add(rows[i].getString("adgid"));
			}
			
			return (out);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitActionAdgid", log);
		}
	}
	
	/**
	 * 获得 action资源允许的管理员ID(扩展权限)
	 * 
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public ArrayList getPermitActionAdid(String action) throws Exception {
		ArrayList out = new ArrayList();
		
		try {
			DBRow rows[] = fam.getPermitActionAdid(action);
			
			for (int i = 0; i < rows.length; i++) {
				out.add(rows[i].getString("adid"));
			}
			
			return (out);
		} catch (Exception e) {
			throw new SystemException(e, "getPermitActionAdid", log);
		}
	}
	
	/**
	 * 增加角色
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void addRole(HttpServletRequest request) throws RoleIsExistException, Exception {
		try {
			String name = StringUtil.getString(request, "name");
			
			if (fam.getDetailRoleByName(name) != null) {
				throw new RoleIsExistException();
			}
			
			String description = StringUtil.getString(request, "description");
			
			DBRow role = new DBRow();
			role.add("description", description);
			role.add("name", name);
			
			fam.addRole(role);
		} catch (RoleIsExistException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "addRole", log);
		}
	}
	
	/**
	 * 增加角色-资源页面映射
	 * 
	 * @param adgid
	 * @param ataid
	 * @throws Exception
	 */
	private void addRoleControlPageMap(long adgid, String ctid[]) throws Exception {
		try {
			for (int i = 0; ctid != null && i < ctid.length; i++) {
				DBRow roleControlPageMap = new DBRow();
				roleControlPageMap.add("adgid", adgid);
				roleControlPageMap.add("ctid", ctid[i]);
				fam.addRoleControlPageMap(roleControlPageMap);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addRoleControlPageMap", log);
		}
	}
	
	/**
	 * 增加管理员页面扩展权限
	 * 
	 * @param adid
	 * @param ctid
	 * @throws Exception
	 */
	private void addAdminControlPageMap(long adid, String ctid[]) throws Exception {
		try {
			for (int i = 0; ctid != null && i < ctid.length; i++) {
				DBRow roleControlPageMap = new DBRow();
				roleControlPageMap.add("adid", adid);
				roleControlPageMap.add("ctid", ctid[i]);
				fam.addAdminControlPageMap(roleControlPageMap);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addAdminControlPageMap", log);
		}
	}
	
	/**
	 * 增加角色-action映射
	 * 
	 * @param adgid
	 * @param ataid
	 * @throws Exception
	 */
	private void addRoleActionMap(long adgid, String ataid[]) throws Exception {
		try {
			for (int i = 0; ataid != null && i < ataid.length; i++) {
				DBRow roleActionMap = new DBRow();
				roleActionMap.add("adgid", adgid);
				roleActionMap.add("ataid", ataid[i]);
				fam.addRoleActionMap(roleActionMap);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addRoleActionMap", log);
		}
	}
	
	/**
	 * 增加管理员扩展权限
	 * 
	 * @param adid
	 * @param ataid
	 * @throws Exception
	 */
	private void addAdminActionMap(long adid, String ataid[]) throws Exception {
		try {
			for (int i = 0; ataid != null && i < ataid.length; i++) {
				DBRow roleActionMap = new DBRow();
				roleActionMap.add("adid", adid);
				roleActionMap.add("ataid", ataid[i]);
				fam.addAdminActionMap(roleActionMap);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addAdminActionMap", log);
		}
	}
	
	/**
	 * 修改角色
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void modifyRole(HttpServletRequest request) throws RoleIsExistException, Exception {
		try {
			long adgid = StringUtil.getLong(request, "adgid");
			String name = StringUtil.getString(request, "name");
			String index_page = StringUtil.getString(request, "index_page");
			String description = StringUtil.getString(request, "description");
			
			DBRow existRole = fam.getDetailRoleByName(name);
			
			if (existRole != null && existRole.get("adgid", 0l) != adgid) {
				throw new RoleIsExistException();
			}
			
			DBRow role = new DBRow();
			role.add("description", description);
			role.add("name", name);
			role.add("index_page", index_page);
			
			fam.modifyRole(adgid, role);
		} catch (RoleIsExistException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "modifyRole", log);
		}
	}
	
	/**
	 * 对角色进行页面资源和action权限设置
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void grantRolePageActionRights(HttpServletRequest request) throws SuperRoleException, Exception {
		try {
			long adgid = StringUtil.getLong(request, "adgid");
			
			// 不允许修改超级管理员权限
			if (adgid == 10000) {
				throw new SuperRoleException();
			}
			
			String ataid[] = request.getParameterValues("ataid");
			String ctid[] = request.getParameterValues("ctid");
			
			if (adgid > 0) {
				// 先删除现有群组权限映射
				fam.delRoleActionMapByAdgid(adgid);
				fam.delRoleControlPageMapByAdgid(adgid);
				
				this.addRoleActionMap(adgid, ataid);
				this.addRoleControlPageMap(adgid, ctid);
			}
			
			// 清空鉴权映射内存缓存数据
			AdminAuthenCenter.clearPermitPageHM();
			AdminAuthenCenter.clearPermitActionHM();
		} catch (SuperRoleException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "grantRolePageActionRights", log);
		}
	}
	
	public void grantAdminPageActionRights(HttpServletRequest request) throws SuperRoleException, Exception {
		try {
			long adid = StringUtil.getLong(request, "adid");
			
			String admin_ataid[] = request.getParameterValues("admin_ataid");
			String admin_ctid[] = request.getParameterValues("admin_ctid");
			
			if (adid > 0) {
				// 先删除现有群组权限映射
				fam.delAdminActionMapByAdid(adid);
				fam.delAdminControlPageMapByAdid(adid);
				
				this.addAdminActionMap(adid, admin_ataid);
				this.addAdminControlPageMap(adid, admin_ctid);
			}
			
			// 清空鉴权映射内存缓存数据
			AdminAuthenCenter.clearPermitPageHM();
			AdminAuthenCenter.clearPermitActionHM();
		} catch (SuperRoleException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "grantAdminPageActionRights", log);
		}
	}
	
	/**
	 * 删除角色
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void delRole(HttpServletRequest request) throws RoleHaveAdminException, SuperRoleException, Exception {
		try {
			long adgid = StringUtil.getLong(request, "adgid");
			
			// 不能删除超级管理员角色
			if (adgid == 10000) {
				throw new SuperRoleException();
			}
			
			if (fam.getAdminByAdgid(adgid, null).length > 0) {
				throw new RoleHaveAdminException();
			}
			
			fam.delRole(adgid);
			// 把角色相关的action和页面资源映射同时删除
			fam.delRoleActionMapByAdgid(adgid);
			fam.delRoleControlPageMapByAdgid(adgid);
		} catch (RoleHaveAdminException e) {
			throw e;
		} catch (SuperRoleException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "delRole", log);
		}
	}
	
	/**
	 * 通过角色名称获得角色详细信息
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailRoleByName(String name) throws Exception {
		try {
			return (fam.getDetailRoleByName(name));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailRoleByName", log);
		}
	}
	
	/**
	 * 通过角色ID获得角色详细名称
	 * 
	 * @param adgid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailRoleByAdgid(long adgid) throws Exception {
		try {
			return (fam.getDetailRoleByAdgid(adgid));
		} catch (Exception e) {
			throw new SystemException(e, "getDetailRoleByAdgid", log);
		}
	}
	
	/**
	 * 通过page id获得其下所有action
	 * 
	 * @param pc
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getActionsByPage(PageCtrl pc, long id) throws Exception {
		try {
			return (fam.getActionsByPage(pc, id));
		} catch (Exception e) {
			throw new SystemException(e, "getActionsByPage", log);
		}
	}
	
	/**
	 * 
	 * @param adgid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminByAdgid(long adgid, PageCtrl pc) throws Exception {
		try {
			return (fam.getAdminByAdgid(adgid, pc));
		} catch (Exception e) {
			throw new SystemException(e, "getAdminByAdgid", log);
		}
	}
	
	/**
	 * 根据部门ID获取账号
	 * @param 部门ID
	 * @return 账号列表
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountByDeptId(long adgid) throws Exception {
		
		try {
			
			return fam.getAccountByDeptId(adgid);
			
		} catch (Exception e) {
			throw new SystemException(e, "getAccountByDeptId", log);
		}
	}
	
	/**
	 * 修改管理员
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void modAdmin(HttpServletRequest request) throws Exception {
		try {
			String pwd = StringUtil.getString(request, "pwd");
			String employe_name = StringUtil.getString(request, "employe_name");
			String employe_summary = StringUtil.getString(request, "employe_summary");
			long adgid = StringUtil.getLong(request, "adgid");
			long adid = StringUtil.getLong(request, "adid");
			long ps_id = StringUtil.getLong(request, "ps_id");
			
			String email = StringUtil.getString(request, "email");
			String skype = StringUtil.getString(request, "skype");
			String msn = StringUtil.getString(request, "msn");
			
			DBRow admin = new DBRow();
			if (!pwd.trim().equals("")) {
				pwd = StringUtil.getMD5(pwd);
				admin.add("pwd", pwd);
			}
			
			admin.add("employe_name", employe_name);
			admin.add("employe_summary", employe_summary);
			admin.add("adgid", adgid);
			admin.add("ps_id", ps_id);
			admin.add("email", email);
			admin.add("skype", skype);
			admin.add("msn", msn);
			
			fam.modifyAdmin(adid, admin);
		} catch (Exception e) {
			throw new SystemException(e, "modAdmin", log);
		}
	}
	
	/**
	 * zyj 索引搜索账号
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchAdmins(String search_key, int search_mode, PageCtrl pc) throws Exception {
		try {
			if (!search_key.equals("") && !search_key.equals("\"")) {
				search_key = search_key.replaceAll("\"", "");
				search_key = search_key.replaceAll("'", "");
				search_key = search_key.replaceAll("\\*", "");
				// search_key +="*";这个索引是IK分词器，不要最后+*
			}
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			return AdminIndexMgr.getInstance().getSearchResults(search_key.toLowerCase(), search_mode, page_count, pc);
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 根据仓库id和分组id 查询人员列表 zwb
	 * 
	 * @param ps_id
	 * @param group_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminsByPsIdAndGId(long ps_id, long group_id) throws Exception {
		try {
			return this.fam.getAdminsByPsIdAndGId(ps_id, group_id);
		} catch (Exception e) {
			throw new SystemException(e, "getAdminsByPsIdAndGId", log);
		}
	}
	
	/**
	 * 根据仓库ID和分组ID，返回人员ids和names
	 * 
	 * @param ps_id
	 * @param group_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月8日 下午12:10:07
	 */
	public DBRow getAdminIdsNamesByPsIdAndGId(long ps_id, long group_id) throws Exception {
		try {
			DBRow row = new DBRow();
			DBRow[] rows = getAdminsByPsIdAndGId(ps_id, group_id);
			String ids = "";
			String names = "";
			for (int i = 0; i < rows.length; i++) {
				if (i == rows.length - 1) {
					ids += rows[i].get("adid", 0l);
					names += rows[i].getString("employe_name");
				} else {
					ids += rows[i].get("adid", 0l) + ",";
					names += rows[i].getString("employe_name") + ",";
				}
			}
			row.add("adids", ids);
			row.add("adnames", names);
			return row;
		} catch (Exception e) {
			throw new SystemException(e, "getAdminIdsNamesByPsIdAndGId", log);
		}
	}
	
	public void setFam(FloorAdminMgr fam) {
		this.fam = fam;
	}
	
	public void setFloorAdminMgrGZY(FloorAdminMgrGZY floorAdminMgrGZY) {
		this.floorAdminMgrGZY = floorAdminMgrGZY;
	}
	
	public void setFloorTitleMgrZJ(FloorTitleMgrZJ floorTitleMgrZJ) {
		this.floorTitleMgrZJ = floorTitleMgrZJ;
	}
	
	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	
	@Override
	public DBRow[] getAdminsEmployeName(String employe_name) throws Exception {
		return this.fam.getAdminsEmployeName(employe_name);
	}
	
}
