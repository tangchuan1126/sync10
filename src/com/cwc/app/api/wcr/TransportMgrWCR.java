package com.cwc.app.api.wcr;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.api.zj.TransportMgrZJ;
import com.cwc.app.api.zyj.TransportMgrZyj;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuB2BOrderBean;
import com.cwc.app.exception.b2b.B2BOrderDetailWaitCountNotEnoughException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.wcr.FloorTransportMgrWCR;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.ll.FreightMgrIFaceLL;
import com.cwc.app.iface.wcr.TransportMgrIFaceWCR;
import com.cwc.app.iface.zj.ConfigChangeMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.app.key.B2BOrderConfigChangeKey;
import com.cwc.app.key.B2BOrderStatusKey;
import com.cwc.app.key.ClearanceKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.DeclarationKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.TransportCertificateKey;
import com.cwc.app.key.TransportLogTypeKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportProductFileKey;
import com.cwc.app.key.TransportQualityInspectionKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.key.TransportTagKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.app.util.DateUtil;
import com.cwc.util.StringUtil;
import com.cwc.app.util.TDate;

public class TransportMgrWCR implements TransportMgrIFaceWCR {
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorTransportMgrWCR floorTransportMgrWCR;
	
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private FloorTransportMgrLL floorTransportMgrLL;
	private FreightMgrIFaceLL freightMgrLL;
	private FloorCatalogMgr floorCatalogMgr;
	private FloorB2BOrderMgrZyj floorB2BOrderMgrZyj;
	private ProductStoreMgrIFaceZJ productStoreMgrZJ;
	private ConfigChangeMgrIFaceZJ configChangeMgrZJ;
	private TransportMgrZJ transportMgrZJ;
	
	private TransportMgrZyj transportMgrZyj;
	private SystemConfigIFace systemConfig;
	
	
	public void setFloorTransportMgrWCR(FloorTransportMgrWCR floorTransportMgrWCR) {
		this.floorTransportMgrWCR = floorTransportMgrWCR;
	}
	
	
	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}
	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}
	public void setFreightMgrLL(FreightMgrIFaceLL freightMgrLL) {
		this.freightMgrLL = freightMgrLL;
	}
	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
	public void setFloorB2BOrderMgrZyj(FloorB2BOrderMgrZyj floorB2BOrderMgrZyj) {
		this.floorB2BOrderMgrZyj = floorB2BOrderMgrZyj;
	}
	public void setProductStoreMgrZJ(ProductStoreMgrIFaceZJ productStoreMgrZJ) {
		this.productStoreMgrZJ = productStoreMgrZJ;
	}
	public void setConfigChangeMgrZJ(ConfigChangeMgrIFaceZJ configChangeMgrZJ) {
		this.configChangeMgrZJ = configChangeMgrZJ;
	}
	public void setTransportMgrZJ(TransportMgrZJ transportMgrZJ) {
		this.transportMgrZJ = transportMgrZJ;
	}

	public void setTransportMgrZyj(TransportMgrZyj transportMgrZyj) {
		this.transportMgrZyj = transportMgrZyj;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}


	public DBRow[] fillterTransport(long psid, PageCtrl pc, int status, int declaration, int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id, int qualityInspection, int productFile, int tag, int tagThird) throws Exception {
		try
		{
			return floorTransportMgrWCR.fillterTransport(psid, pc, status, declaration, clearance, invoice, drawback, day, stock_in_set, create_account_id, qualityInspection, productFile, tag, tagThird);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	public DBRow[] getDefTransport(long psid, PageCtrl pc, int status, int declaration, int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id, int qualityInspection, int productFile, int tag, int tagThird) throws Exception {
		try
		{
			return floorTransportMgrWCR.getDefaultTransport(psid, pc, status, declaration, clearance, invoice, drawback, day, stock_in_set, create_account_id, qualityInspection, productFile, tag, tagThird);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}

	public DBRow getTransportForDefault(long psid, long title_id, long order_id) throws Exception {
		try
		{
			return floorTransportMgrWCR.getTransportForDefault(psid, title_id, order_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * B2B订单生成运单
	 */
	public long addB2BOrderTransport(HttpServletRequest request) throws Exception {
		try
		{
			long b2b_oid	= StringUtil.getLong(request, "b2b_oid");
			DBRow b2BOrderRow	= floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			long title_id = b2BOrderRow.get( "title_id",0l);
			AdminLoginBean adminLoginBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			
			//提货地址
			long send_psid = StringUtil.getLong(request, "ps_id");
			DBRow sendProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
			
			//运输设置
			long fr_id = StringUtil.getLong(request,"fr_id");
			String transport_waybill_name = StringUtil.getString(request,"transport_waybill_name");
			String transport_waybill_number = StringUtil.getString(request,"transport_waybill_number");
			int transportby = StringUtil.getInt(request,"transportby");
			String carriers = StringUtil.getString(request,"carriers");
			String transport_send_place = StringUtil.getString(request,"transport_send_place");
			String transport_receive_place = StringUtil.getString(request,"transport_receive_place");
			
			//主流程
			String adminUserIdsB2BOrder			= StringUtil.getString(request, "adminUserIdsB2BOrder");
			String adminUserNamesB2BOrder		= StringUtil.getString(request, "adminUserNamesB2BOrder");
			int needMailB2BOrder				= StringUtil.getInt(request, "needMailB2BOrder");
			int needMessageB2BOrder				= StringUtil.getInt(request, "needMessageB2BOrder");
			int needPageB2BOrder				= StringUtil.getInt(request, "needPageB2BOrder");
			
			//出口报关	
			int declaration					= StringUtil.getInt(request, "declaration");
			int declaration_responsible = declaration==1?0:(declaration==7?1:(declaration==8?2:0));
			declaration = (declaration==1?1:2);
			
			String adminUserIdsDeclaration	= StringUtil.getString(request, "adminUserIdsDeclaration");
			String adminUserNamesDeclaration= StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration			= StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration		= StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration			= StringUtil.getInt(request, "needPageDeclaration");
			
			//进口清关
			int clearance					= StringUtil.getInt(request, "clearance");
			int clearance_responsible = clearance==1?0:(clearance==7?1:(clearance==8?2:0));
			clearance = (clearance==1?1:2);
			
			String adminUserIdsClearance	= StringUtil.getString(request, "adminUserIdsClearance");
			String adminUserNamesClearance	= StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance			= StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance		= StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance			= StringUtil.getInt(request, "needPageClearance");
			
			//商品图片
			int productFile							= StringUtil.getInt(request, "product_file");
			int productFile_responsible = (productFile==7?1:(productFile==8?2:0));
			productFile = 2;
			
			String adminUserIdsProductFile	= StringUtil.getString(request, "adminUserIdsProductFile");
			String adminUserNamesProductFile= StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile			= StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile		= StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile			= StringUtil.getInt(request, "needPageProductFile");
			
			//内部标签	
			int tag							= StringUtil.getInt(request, "tag");
			int tag_responsible = (tag==1?0:(tag==7?1:(tag==8?2:0)));
			tag = (tag==1?1:2);
			
			String adminUserIdsTag			= StringUtil.getString(request, "adminUserIdsTag");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag					= StringUtil.getInt(request, "needMailTag");
			int needMessageTag				= StringUtil.getInt(request, "needMessageTag");
			int needPageTag					= StringUtil.getInt(request, "needPageTag");
			
			//第三方标签	
			int tag_third					= StringUtil.getInt(request, "tag_third");
			int tag_third_responsible = (tag_third==1?0:(tag_third==7?1:(tag_third==8?2:0)));
			tag_third = (tag_third==1?1:2);
			
			String adminUserIdsTagThird		= StringUtil.getString(request, "adminUserIdsTagThird");
			String adminUserNamesTagThird	= StringUtil.getString(request, "adminUserNamesTagThird");
			int needMailTagThird			= StringUtil.getInt(request, "needMailTagThird");
			int needMessageTagThird			= StringUtil.getInt(request, "needMessageTagThird");
			int needPageTagThird			= StringUtil.getInt(request, "needPageTagThird");
			
			//运费
			
			int stock_in_set				= StringUtil.getInt(request, "stock_in_set");
			int stock_in_set_responsible = (stock_in_set==1?0:(stock_in_set==7?1:(stock_in_set==8?2:0)));
			stock_in_set = (stock_in_set==1?1:2);
			
			String adminUserIdsStockInSet	= StringUtil.getString(request, "adminUserIdsStockInSet");
			String adminUserNamesStockInSet	= StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet			= StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet		= StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet			= StringUtil.getInt(request, "needPageStockInSet");
			
			//单证
			int certificate					= StringUtil.getInt(request, "certificate");
			int certificate_responsible = (certificate==1?0:(certificate==7?1:(certificate==8?2:0)));
			certificate = (certificate==1?1:2);
			
			String adminUserIdsCertificate	= StringUtil.getString(request, "adminUserIdsCertificate");
			String adminUserNamesCertificate= StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate			= StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate		= StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate			= StringUtil.getInt(request, "needPageCertificate");
			
			//质检
			int quality_inspection 			= StringUtil.getInt(request, "quality_inspection");
			int quality_inspection_responsible = (quality_inspection==1?0:(quality_inspection==7?1:(quality_inspection==8?2:0)));
			quality_inspection = (quality_inspection==1?1:2);
			
			String adminUserIdsQualityInspection	= StringUtil.getString(request, "adminUserIdsQualityInspection");
			String adminUserNamesQualityInspection	= StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection	= StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection= StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection	= StringUtil.getInt(request, "needPageQualityInspection");
			
			DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(b2BOrderRow.get("receive_psid", 0L));
			DBRow transportRow = new DBRow();
			//收货信息
			transportRow.add("receive_psid",b2BOrderRow.get("receive_psid", 0L));
			transportRow.add("deliver_ccid",b2BOrderRow.get("deliver_ccid", 0L));
			transportRow.add("deliver_pro_id",b2BOrderRow.get("deliver_pro_id", 0L));
			transportRow.add("deliver_city",b2BOrderRow.getString("deliver_city"));
			transportRow.add("deliver_house_number",b2BOrderRow.getString("deliver_house_number"));
			transportRow.add("deliver_street",b2BOrderRow.getString("deliver_street"));
			transportRow.add("deliver_zip_code",b2BOrderRow.getString("deliver_zip_code"));
			transportRow.add("transport_linkman",b2BOrderRow.getString("b2b_order_linkman"));
			transportRow.add("transport_linkman_phone",b2BOrderRow.getString("b2b_order_linkman_phone"));
			if(!"".equals(b2BOrderRow.getString("b2b_order_receive_date")))
			{
				transportRow.add("transport_receive_date", b2BOrderRow.getString("b2b_order_receive_date"));
			}
			transportRow.add("address_state_deliver", b2BOrderRow.getString("address_state_deliver"));

			
			//提货信息
			transportRow.add("send_psid",send_psid);
			transportRow.add("send_city",sendProductStorage.getString("send_city"));
			transportRow.add("send_house_number",sendProductStorage.getString("send_house_number"));
			transportRow.add("send_street",sendProductStorage.getString("send_street"));
			transportRow.add("send_ccid",sendProductStorage.get("send_nation", 0L));
			transportRow.add("send_pro_id",sendProductStorage.get("send_pro_id", 0L));
			transportRow.add("send_zip_code",sendProductStorage.getString("send_zip_code"));
			transportRow.add("send_name",sendProductStorage.getString("send_contact"));
			transportRow.add("send_linkman_phone",sendProductStorage.getString("send_phone"));
			if(!"".equals(b2BOrderRow.getString( "b2b_order_out_date")))
			{
				transportRow.add("transport_out_date", b2BOrderRow.getString( "b2b_order_out_date"));
			}
			transportRow.add("address_state_send", sendProductStorage.getString("send_pro_input"));
			transportRow.add("transport_address", "");
			//其他信息(ETA,备注)
			transportRow.add("remark", b2BOrderRow.getString( "remark"));
			transportRow.add("title_id",title_id);
			
			long logingUserId = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			String loginUserName = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name();
			
			//主流程信息
			transportRow.add("transport_status",TransportOrderKey.PACKING);//B2B创建的转运单都是装箱中
			transportRow.add("transport_date",DateUtil.NowStr());
			transportRow.add("create_account_id",logingUserId);//转运单的创建者
			transportRow.add("create_account",loginUserName);//转运单的创建者
			transportRow.add("updatedate",DateUtil.NowStr());
			transportRow.add("updateby",logingUserId);//转运单的创建者
			transportRow.add("updatename",loginUserName);//转运单的创建者
			
			//提货与收货仓库类型
			transportRow.add("target_ps_type",b2BOrderRow.get("storage_type",0));
			transportRow.add("from_ps_type",sendProductStorage.get("storage_type",0));
			
			//运输设置
			transportRow.add("fr_id",fr_id);
			transportRow.add("transport_waybill_name",transport_waybill_name);
			transportRow.add("transport_waybill_number",transport_waybill_number);
			transportRow.add("transportby",transportby);
			transportRow.add("carriers",carriers);
			transportRow.add("transport_send_place",transport_send_place);
			transportRow.add("transport_receive_place",transport_receive_place);
			
			//流程信息
			transportRow.add("declaration", declaration);
			transportRow.add("clearance", clearance);
			transportRow.add("product_file", TransportProductFileKey.PRODUCTFILE);
			transportRow.add("tag", tag);
			transportRow.add("tag_third", tag_third);
			transportRow.add("stock_in_set", stock_in_set);
			transportRow.add("certificate", certificate);
			transportRow.add("quality_inspection", quality_inspection);
			
			transportRow.add("declaration_responsible", declaration_responsible);
			transportRow.add("clearance_responsible", clearance_responsible);
			transportRow.add("product_file_responsible", productFile_responsible);
			transportRow.add("tag_responsible", tag_responsible);
			transportRow.add("tag_third_responsible", tag_third_responsible);
			transportRow.add("stock_in_set_responsible", stock_in_set_responsible);
			transportRow.add("certificate_responsible", certificate_responsible);
			transportRow.add("quality_inspection_responsible", quality_inspection_responsible);
			
			
			long transport_id = transportMgrZJ.addTransport(transportRow);
			

			//保留分配库存
			DBRow[] cartWaybillB2BProducts = new DBRow[0]; //cartWaybillB2BMgrZJ.getCartWaybillB2bProduct();
			
			PreCalcuB2BOrderBean preCalcuB2BOrderBean = productStoreMgrZJ.calcuOrderB2B(StringUtil.getSession(request), send_psid, title_id);
			
			if (preCalcuB2BOrderBean.getOrderStatus()==B2BOrderStatusKey.Enough||(preCalcuB2BOrderBean.getOrderStatus()==B2BOrderStatusKey.ConfigNotEnough&&preCalcuB2BOrderBean.getOrderConfigChange()==B2BOrderConfigChangeKey.Can))
			{
				cartWaybillB2BProducts = preCalcuB2BOrderBean.getResult();
			}
			
			ArrayList<DBRow> containerTypeCountList = new ArrayList<DBRow>();
			ArrayList<DBRow> configChangeItems = new ArrayList<DBRow>();
			for (int i = 0; i < cartWaybillB2BProducts.length; i++) 
			{
				int item_configChange = cartWaybillB2BProducts[i].get("order_item_configChange",0);
				int cci_type = cartWaybillB2BProducts[i].get("cci_type",0);
				long cci_type_id = cartWaybillB2BProducts[i].get("cci_type_id",0l);
				int cci_count = cartWaybillB2BProducts[i].get("cci_count",0);
				int cci_product_count = cartWaybillB2BProducts[i].get("cci_product_count",0);
				
				long pc_id = cartWaybillB2BProducts[i].get("pc_id",0l);
				int product_count = cartWaybillB2BProducts[i].get("product_count", 0);
				String lot_number = cartWaybillB2BProducts[i].getString("lot_number");
				
				long clp_type_id = cartWaybillB2BProducts[i].get("clp_type_id",0l);
				int clp_count = cartWaybillB2BProducts[i].get("clp_count",0);
				if (clp_count>0) 
				{
					
					DBRow clpRow = new DBRow();
					clpRow.add("container_type",ContainerTypeKey.CLP);
					clpRow.add("container_type_id",clp_type_id);
					
					int con_quantity = clp_count;
					
					if (cci_type==ContainerTypeKey.CLP&&cci_type_id==clp_type_id&&item_configChange==B2BOrderConfigChangeKey.Can) 
					{
						con_quantity = clp_count - cci_count;
						product_count = product_count - cci_product_count;
						
						DBRow configChangeItem = new DBRow();
						configChangeItem.add("cci_type",cci_type);
						configChangeItem.add("cci_type_id",cci_type_id);
						configChangeItem.add("cci_count",cci_count);
						configChangeItem.add("cci_product_count",cci_product_count);
						configChangeItem.add("cci_pc_id",pc_id);
						
						configChangeItems.add(configChangeItem);
					}
					
					clpRow.add("con_quantity",con_quantity);
					
					if (con_quantity>0)
					{
						containerTypeCountList.add(clpRow);
					}
				}
				
				/*
				long blp_type_id = cartWaybillB2BProducts[i].get("blp_type_id",0l);
				int blp_count = cartWaybillB2BProducts[i].get("blp_count",0);
				if (blp_count>0)
				{
					DBRow blpRow = new DBRow();
					blpRow.add("container_type",ContainerTypeKey.BLP);
					blpRow.add("container_type_id",blp_type_id);
					blpRow.add("con_quantity",blp_count);
					
					int con_quantity = blp_count;
					
					if (cci_type==ContainerTypeKey.BLP&&cci_type_id==clp_type_id&&item_configChange==B2BOrderConfigChangeKey.Can) 
					{
						con_quantity = blp_count - cci_count;
						product_count = product_count - cci_product_count;
						
						DBRow configChangeItem = new DBRow();
						configChangeItem.add("cci_type",cci_type);
						configChangeItem.add("cci_type_id",cci_type_id);
						configChangeItem.add("cci_count",cci_count);
						configChangeItem.add("cci_product_count",cci_product_count);
						configChangeItem.add("cci_pc_id",pc_id);
						
						configChangeItems.add(configChangeItem);
					}
					
					blpRow.add("con_quantity",con_quantity);
					
					if (con_quantity>0)
					{
						containerTypeCountList.add(blpRow);
					}
				}
				
				long ilp_type_id = cartWaybillB2BProducts[i].get("ilp_type_id",0l);
				int ilp_count = cartWaybillB2BProducts[i].get("ilp_count",0);
				if (ilp_count>0)
				{
					DBRow ilpRow = new DBRow();
					ilpRow.add("container_type",ContainerTypeKey.ILP);
					ilpRow.add("container_type_id",ilp_type_id);
					ilpRow.add("con_quantity",ilp_count);
					
					int con_quantity = ilp_count;
					
					if (cci_type==ContainerTypeKey.ILP&&cci_type_id==clp_type_id&&item_configChange==B2BOrderConfigChangeKey.Can) 
					{
						con_quantity = ilp_count - cci_count;
						product_count = product_count - cci_product_count;
						
						DBRow configChangeItem = new DBRow();
						configChangeItem.add("cci_type",cci_type);
						configChangeItem.add("cci_type_id",cci_type_id);
						configChangeItem.add("cci_count",cci_count);
						configChangeItem.add("cci_product_count",cci_product_count);
						configChangeItem.add("cci_pc_id",pc_id);
						
						configChangeItems.add(configChangeItem);
					}
					
					ilpRow.add("con_quantity",con_quantity);
					
					if (con_quantity>0)
					{
						containerTypeCountList.add(ilpRow);
					}
					containerTypeCountList.add(ilpRow);
				}
				*///去掉ILP和BLP
				
				int orignail_count = cartWaybillB2BProducts[i].get("orignail_count",0);
				if (orignail_count>0)
				{
					DBRow orignailRow = new DBRow();
					orignailRow.add("container_type",ContainerTypeKey.Original);
					orignailRow.add("container_type_id",0);
					orignailRow.add("con_quantity",orignail_count);
					containerTypeCountList.add(orignailRow);
				}
				
				
				DBRow[] containerTypes = containerTypeCountList.toArray(new DBRow[0]);
				containerTypeCountList.clear();
				
				productStoreMgrZJ.reserveProductStore(send_psid,pc_id,title_id,product_count,transport_id,ProductStoreBillKey.TRANSPORT_ORDER,lot_number,adminLoginBean,containerTypes);
				
				if (preCalcuB2BOrderBean.getOrderConfigChange()==B2BOrderConfigChangeKey.Can)
				{
					configChangeMgrZJ.addConfigChangeForSystemBill(send_psid, title_id, adminLoginBean,ProductStoreBillKey.TRANSPORT_ORDER,transport_id,configChangeItems.toArray(new DBRow[0]));
					
					DBRow para = new DBRow();
					para.add("transport_status",TransportOrderKey.CONFIGCHANGEING);
					
					floorTransportMgrZJ.modTransport(transport_id, para);
				}
			}
			
			//添加转运单详细，从session中获取值
			this.addB2BTransportItemBySession(transport_id, request);
			
			//设置运费
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), "创建转运单:单号"+transport_id,adminLoginBean.getAdid(),adminLoginBean.getEmploye_name(),TransportLogTypeKey.Goods,TransportOrderKey.READY);//类型1跟进记录
			freightMgrLL.transportSetFreight(transport_id, request);
			
			//添加转运单索引
			transportMgrZJ.editTransportIndex(transport_id, "add");
			
			//修改b2b订单
			transportMgrZJ.updateB2BOrder(b2b_oid);
			
			//转运单发送邮件的内容不同，需要区分是供应商还是仓库，组织内容时需要传采购单的Id，转运单传0
			this.addSculde(transport_id, 0L, sendProductStorage.getString("title"),receiveProductStorage.getString("title"),b2BOrderRow.getString("b2b_order_receive_date"), logingUserId, loginUserName, request, 
					adminUserIdsB2BOrder, adminUserNamesB2BOrder, needMailB2BOrder, needMessageB2BOrder, needPageB2BOrder,
					declaration, adminUserNamesDeclaration, adminUserIdsDeclaration, needPageDeclaration, needMailDeclaration, needMessageDeclaration,
					clearance, adminUserNamesClearance, adminUserIdsClearance, needPageClearance, needMailClearance, needMessageClearance, 
					TransportProductFileKey.PRODUCTFILE,adminUserNamesProductFile,adminUserIdsProductFile,needPageProductFile,needMailProductFile,needMessageProductFile,
					tag,adminUserNamesTag,adminUserIdsTag,needPageTag,needMailTag,needMessageTag,
					stock_in_set,adminUserNamesStockInSet,adminUserIdsStockInSet,needPageStockInSet,needMailStockInSet,needMessageStockInSet, 
					certificate,adminUserNamesCertificate,adminUserIdsCertificate,needPageCertificate,needMailCertificate,needMessageCertificate,
					quality_inspection,adminUserNamesQualityInspection,adminUserIdsQualityInspection,needPageQualityInspection,needMailQualityInspection,needMessageQualityInspection
					,tag_third,adminUserIdsTagThird,adminUserNamesTagThird,needMailTagThird,needMessageTagThird,needPageTagThird);
			
			return transport_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addB2BOrderTransport(HttpServletRequest request)",log);
		}
	}
	
	public void addSculde(long transport_id,long purchase_id,String cata_name_send,String cata_name_deliver,String eta,long logingUserId,String loginUserName,HttpServletRequest request,
			String adminUserIdsTransport, String adminUserNamesTransport, int needMailTransport, int needMessageTransport, int needPageTransport,
			int declaration,String adminUserNamesDeclaration,String adminUserIdsDeclaration,int needPageDeclaration,int needMailDeclaration,int needMessageDeclaration,
			int clearance,String adminUserNamesClearance,String adminUserIdsClearance,int needPageClearance,int needMailClearance,int needMessageClearance,
			int product_file,String adminUserNamesProductFile,String adminUserIdsProductFile,int needPageProductFile,int needMailProductFile,int needMessageProductFile,
			int tag,String adminUserNamesTag,String adminUserIdsTag,int needPageTag,int needMailTag,int needMessageTag,
			int stock_in_set,String adminUserNamesStockInSet,String adminUserIdsStockInSet,int needPageStockInSet,int needMailStockInSet,int needMessageStockInSet,
			int certificate,String adminUserNamesCertificate,String adminUserIdsCertificate,int needPageCertificate,int needMailCertificate,int needMessageCertificate,
			int quality_inspection,String adminUserNamesQualityInspection,String adminUserIdsQualityInspection,int needPageQualityInspection,int needMailQualityInspection,int needMessageQualityInspection
			,int tag_third,String adminUserIdsTagThird,String adminUserNamesTagThird,int needMailTagThird,int needMessageTagThird,int needPageTagThird)
			throws Exception
		{
			String sendContent = transportMgrZJ.handleSendContent(transport_id, purchase_id, declaration, adminUserNamesDeclaration,
					clearance, adminUserNamesClearance, product_file, adminUserNamesProductFile,
					tag, adminUserNamesTag, stock_in_set, adminUserNamesStockInSet, 
					certificate, adminUserNamesCertificate, quality_inspection, adminUserNamesQualityInspection,cata_name_send,cata_name_deliver,eta);
			
			//转运单主流程任务
			if (!"".equals(adminUserIdsTransport))
				transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), "",logingUserId,
					adminUserIdsTransport, "", 
					transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[主流程]", 
					sendContent,
					needPageTransport, needMailTransport, needMessageTransport, request, true, 4,
					TransportOrderKey.READY,loginUserName, "创建转运单"+transport_id+"[备货中]");
			//报关
			if(DeclarationKey.DELARATION == declaration)
			{
				if (!"".equals(adminUserIdsDeclaration))
					transportMgrZyj.addTransportScheduleOneProdure("", "",logingUserId,
							adminUserIdsDeclaration, "", 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[报关流程]", 
							sendContent,
							needPageDeclaration, needMailDeclaration, needMessageDeclaration, request, false, 7,
							DeclarationKey.DELARATION,loginUserName, "创建转运单"+transport_id+"[需要报关]");
			}
			
			//清关
			if(ClearanceKey.CLEARANCE == clearance)
			{
				if (!"".equals(adminUserIdsClearance))
					transportMgrZyj.addTransportScheduleOneProdure("", "", logingUserId,
							adminUserIdsClearance, "", 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[清关流程]",
							sendContent,
							needPageClearance, needMailClearance, needMessageClearance, request, false, 6,
							ClearanceKey.CLEARANCE, loginUserName, "创建转运单"+transport_id+"[需要清关]");
			}
			//商品图片
			if(TransportProductFileKey.PRODUCTFILE == product_file)
			{
				TDate tdateProductFile = new TDate();
				tdateProductFile.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_product_file_period")));
				String endTimeProductFile = tdateProductFile.formatDate("yyyy-MM-dd HH:mm:ss");
				if (!"".equals(adminUserIdsProductFile))
					transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeProductFile, logingUserId,
							adminUserIdsProductFile, "", 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[商品图片流程]",
							sendContent,
							needPageProductFile, needMailProductFile, needMessageProductFile, request, true, 10,
							TransportProductFileKey.PRODUCTFILE, loginUserName, "创建转运单"+transport_id+"[商品文件上传中]");
			}
			//内部标签
			if(TransportTagKey.TAG == tag) 
			{
				TDate tdateTag = new TDate();
				tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_tag_period")));
				String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
				//添加
				if (!"".equals(adminUserIdsTag))
					transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeTag, logingUserId,
							adminUserIdsTag, "", 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[内部标签流程]",
							sendContent,
							needPageTag, needMailTag, needMessageTag, request, true, 8,
							TransportTagKey.TAG, loginUserName, "创建转运单"+transport_id+"[制签中]");
			}
			
			//第三方标签
			if(TransportTagKey.TAG == tag_third) 
			{
				TDate tdateTagThird = new TDate();
				tdateTagThird.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_tag_period")));
				String endTimeTagThird = tdateTagThird.formatDate("yyyy-MM-dd HH:mm:ss");
				//添加
				if (!"".equals(adminUserIdsTagThird))
					transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeTagThird, logingUserId,
							adminUserIdsTagThird, "", 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[第三方标签流程]",
							sendContent,
							needPageTagThird, needMailTagThird, needMessageTagThird, request, true, TransportLogTypeKey.THIRD_TAG,
							TransportTagKey.TAG, loginUserName, "创建转运单"+transport_id+"[制签中]");
			}
			
			//运费
			if(TransportStockInSetKey.SHIPPINGFEE_SET == stock_in_set)
			{
				//添加
				if (!"".equals(adminUserIdsStockInSet))
					transportMgrZyj.addTransportScheduleOneProdure("", "", logingUserId,
							adminUserIdsStockInSet, "", 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[运费流程]",
							sendContent,
							needPageStockInSet, needMailStockInSet, needMessageStockInSet, request, false, 5,
							TransportStockInSetKey.SHIPPINGFEE_SET, loginUserName, "创建转运单"+transport_id+"[需要运费]");
			}
			
			//单证
			if(TransportCertificateKey.CERTIFICATE == certificate)
			{
				TDate tdateCertificate = new TDate();
				tdateCertificate.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_certificate_period")));
				String endTimeCertificate = tdateCertificate.formatDate("yyyy-MM-dd HH:mm:ss");
				if (!"".equals(adminUserIdsCertificate))
					transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeCertificate, logingUserId,
							adminUserIdsCertificate, "", 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[单证流程]",
							sendContent,
							needPageCertificate, needMailCertificate, needMessageCertificate, request, true, 9,
							TransportCertificateKey.CERTIFICATE, loginUserName, "创建转运单"+transport_id+"[单证采集中]");
			}
			
			//质检
			if(TransportQualityInspectionKey.NEED_QUALITY == quality_inspection)
			{
				TDate tdateQuality = new TDate();
				tdateQuality.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_quality_inspection")));
				String endTimeQuality = tdateQuality.formatDate("yyyy-MM-dd HH:mm:ss");
				if (!"".equals(adminUserIdsQualityInspection))
					transportMgrZyj.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeQuality, logingUserId,
							adminUserIdsQualityInspection, "", 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[质检流程]",
							sendContent,
							needPageQualityInspection, needMailQualityInspection, needMessageQualityInspection, request, true, 11,
							TransportQualityInspectionKey.NEED_QUALITY, loginUserName, "创建转运单"+transport_id+"[需要质检]");
			}
		}
	
	/**
	 * 将session中的选择的订单明细保存成转运单明细
	 * @param transport_id
	 * @param request
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void addB2BTransportItemBySession(long transport_id, HttpServletRequest request) throws Exception
	{
		try
		{
			
			HttpSession session = StringUtil.getSession(request);
			ArrayList<DBRow> sessionTransportItems = null;
			if(null != session.getAttribute(Config.cartWaybillB2B))
			{
				sessionTransportItems = (ArrayList)session.getAttribute(Config.cartWaybillB2B);
			}
			if(null != sessionTransportItems)
			{
				for (int i = 0; i < sessionTransportItems.size(); i++) 
				{
					DBRow row = null;
					
					Object sessionValue = sessionTransportItems.get(i);
					if (sessionValue instanceof Map)
					{
						Map<String,Object> valueMap = (Map<String,Object>)sessionValue;
						row = DBRowUtils.mapConvertToDBRow(valueMap);
					}
					else if (sessionValue instanceof DBRow)
					{
						row = sessionTransportItems.get(i);
					}
					
					
					if(null != row)
					{
						long b2b_detail_id = row.get("b2b_detail_id", 0L);
						int product_count = row.get("product_count",0);
						DBRow b2BOrderItemRow	= floorB2BOrderMgrZyj.findB2BOrderItemByItemId(b2b_detail_id);
						DBRow transportDetail = new DBRow();
						transportDetail.add("transport_pc_id",b2BOrderItemRow.get("b2b_pc_id", 0L));
						transportDetail.add("transport_delivery_count",product_count);
						transportDetail.add("transport_backup_count",0);
						transportDetail.add("transport_count",product_count);
						transportDetail.add("transport_id",transport_id);
						transportDetail.add("transport_p_name",b2BOrderItemRow.getString("b2b_p_name"));
						transportDetail.add("transport_p_code",b2BOrderItemRow.getString("b2b_p_code"));
						transportDetail.add("transport_volume",b2BOrderItemRow.get("b2b_volume", 0d));
						transportDetail.add("transport_weight",b2BOrderItemRow.get("b2b_weight", 0d));
						transportDetail.add("lot_number", b2BOrderItemRow.getString("lot_number"));
						if(!"".equals(b2BOrderItemRow.getString("clp_type_id")))
						{
							transportDetail.add("clp_type_id",b2BOrderItemRow.getString("clp_type_id"));
						}
						if(!"".equals(b2BOrderItemRow.getString("blp_type_id")))
						{
							transportDetail.add("blp_type_id",b2BOrderItemRow.getString("blp_type_id"));
						}
						transportDetail.add("transport_send_price",b2BOrderItemRow.get("b2b_send_price", 0d));
						transportDetail.add("transport_union_count",b2BOrderItemRow.get("b2b_union_count", 0F));
						transportDetail.add("transport_normal_count",b2BOrderItemRow.get("b2b_normal_count", 0F));
						transportDetail.add("b2b_detail_id", b2b_detail_id);
						
						//更新B2B订单明细上的待上运单数量
						transportMgrZJ.updateb2bOrderDetaiWaitCount(b2b_detail_id, product_count);
						
						transportMgrZJ.addTransportDetail(transportDetail);
					}
				}
			}
		}
		catch(B2BOrderDetailWaitCountNotEnoughException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addTransportItemBySession(HttpServletRequest request)",log);
		}
	}

	public DBRow[] fillterTransportDefault(long send_psid, long receive_psid, PageCtrl pc, int status, int declaration, int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id) throws Exception {
		try
		{
			return floorTransportMgrWCR.fillterTransportDefault(send_psid, receive_psid, pc, status, declaration, clearance, invoice, drawback, day, stock_in_set, create_account_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
}