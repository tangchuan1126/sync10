package com.cwc.app.api.wcr;

import com.cwc.app.floor.api.zj.FloorLPTypeMgrZJ;
import com.cwc.app.floor.api.zj.FloorOutListDetailMgrZJ;
import com.cwc.app.iface.wcr.OutboundOrderMgrIFaceWCR;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.db.DBRow;

public class OutboundOrderMgrWCR implements OutboundOrderMgrIFaceWCR {
	private FloorOutListDetailMgrZJ floorOutListDetailMgrZJ;
	private FloorLPTypeMgrZJ floorLPTypeMgrZJ;

	public void setFloorOutListDetailMgrZJ(FloorOutListDetailMgrZJ floorOutListDetailMgrZJ) {
		this.floorOutListDetailMgrZJ = floorOutListDetailMgrZJ;
	}
	public void setFloorLPTypeMgrZJ(FloorLPTypeMgrZJ floorLPTypeMgrZJ) {
		this.floorLPTypeMgrZJ = floorLPTypeMgrZJ;
	}

	public DBRow[] getOutListDetail(long pc_id, int system_bill_type, long system_bill_id, int container_type, long clp_type_id) throws Exception {
		try {
			return floorOutListDetailMgrZJ.getOutListDetailByContainerType(	pc_id, system_bill_type, system_bill_id, container_type, clp_type_id);
		} catch (Exception e) {
			throw new Exception("OutboundOrderMgrWCR getOutListDetail error:" + e.getMessage());
		}
	}
	
	public int getOutListDetailOriginalCount(long pc_id, int system_bill_type, long system_bill_id) throws Exception {
		int rt =0;
		try {
			////system.out.println("pc_id:"+pc_id+",system_bill_type:"+system_bill_type+",system_bill_id:"+system_bill_id);
			rt = floorOutListDetailMgrZJ.getOutListDetailOriginalCount(pc_id, system_bill_type, system_bill_id);
		} catch (Exception e) {
			rt=0;
			throw new Exception("OutboundOrderMgrWCR getOutListDetailOriginalCount error:" + e.getMessage());
		}
		return rt;
	}
	
	public String getLPName(int container_type, long lp_type_id) throws Exception  {
		if (ContainerTypeKey.CLP== container_type) {
			DBRow clpType = floorLPTypeMgrZJ.getDetailCLP(lp_type_id);
			String CLPName = "";
			if (clpType!=null) {
				CLPName = clpType.get("clp_name", "");
			}
			return CLPName;
		}
		/*
		if (ContainerTypeKey.BLP== container_type) {
			DBRow blpType = floorLPTypeMgrZJ.getDetailBLPType(lp_type_id);
			String BLPName = "";
			if (blpType!=null) {
				BLPName = blpType.get("box_name", "");
			}
			return BLPName;
		}
		if (ContainerTypeKey.ILP== container_type) {
			DBRow ilpType = floorLPTypeMgrZJ.getDetailILPType(lp_type_id);
			String ILPName = "";
			if (ilpType != null) {
				ILPName = ilpType.getString("ibt_total_length")+"X"+ilpType.getString("ibt_total_width")+"X"+ilpType.getString("ibt_total_height");
			}
			return ILPName;
		}
		*///去掉ILP和BLP
		return "";
	}
}