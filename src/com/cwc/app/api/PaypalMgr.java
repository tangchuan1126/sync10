package com.cwc.app.api;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.PaypalMgrIFace;
import com.cwc.app.prehandle.OrderPreHandle;
import com.cwc.app.prehandle.order.OrderPreHandleFather;
import com.cwc.app.util.ConfigBean;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class PaypalMgr implements PaypalMgrIFace
{
	static Logger log = Logger.getLogger("ACTION");
	static Logger paypallog = Logger.getLogger("PAYPAL");
	
	private OrderMgrIFace orderMgr;
	private OrderPreHandle orderPreHandle;			//加载订单预处理实体类

	public void paypalNotify(HttpServletRequest request)
		throws SystemException,Exception
	{
		String str = null;
		String paramName;
		String paramValue;
		
		/**
		 * 在做任何处理前，先把paypal触发过来的参数打印出来
		 * 这样即使后面出现了问题，也能追踪到PAYPAL是否发过来订单，或者追踪系统没有的订单
		 */
		
		paypallog.info("================== DEBUG  "+StringUtil.getString(request, "payment_status")+" start ==================");
		Enumeration enu = request.getParameterNames();
		String key;
		StringBuffer backSb = new StringBuffer("cmd=_notify-validate");
		paypallog.info("request toString:"+request.toString());
		while( enu.hasMoreElements() )
		{

			paramName = (String)enu.nextElement();
			paramValue = request.getParameter(paramName);//new String(.getBytes("ISO-8859-1"),"utf-8");
			
			backSb.append("&");
			backSb.append(paramName);
			backSb.append("=");
			
			try
			{
				backSb.append(URLEncoder.encode(paramValue,"utf-8"));
			} 
			catch (UnsupportedEncodingException e)
			{
				throw new SystemException(e,"PaypalNotify UnsupportedEncodingException",log);
			}
			paypallog.info(paramName+" = "+paramValue);
		}
		paypallog.info("================== DEBUG "+StringUtil.getString(request, "payment_status")+" END ==================");
		paypallog.info("");
		paypallog.info("");
		
		str = backSb.toString();
		
		
		//把参数重新发给paypal验证
		URLConnection uc=null;
		String res=null;
		String txn_id = StringUtil.getString(request,"txn_id");
		try 
		{
			URL u = new URL(ConfigBean.getStringValue("paypal_action_url"));
			uc = u.openConnection();
			uc.setDoOutput(true);
			uc.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			PrintWriter pw = new PrintWriter(uc.getOutputStream());
			pw.println(str);
			pw.flush();
			pw.close();
	
			//paypal告诉你验证的结果
			BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			res = in.readLine();	
			in.close();
		} 
		catch (Exception e) 
		{
			paypallog.error("txn_id:"+txn_id+"Communicating with PAYPAL IPN Verifiy Processing Error");
			throw e;
		}
    

		if (res==null)
		res="Null";

		try 
		{
            if (!res.trim().toLowerCase().equals("verified"))
			{
				paypallog.info(txn_id + "Paypal IPN did NOT Verified, Result:" + res);
				throw new Exception();
			}
            paypallog.info(txn_id + "Paypal IPN Verified, Result:" + res);
			request.setAttribute("sender_status",res.trim().toLowerCase());//paypal验证参数请求的结果

			if (isPermit(request))
			{
				
					long oid = orderMgr.addPOrder(request);
					//插入订单后，对订单预处理
					orderPreHandle(oid);
     		}
			else
			{

				paypallog.info("================== Empty txn_id Request start ==================");
				Enumeration en = request.getParameterNames();
				while( en.hasMoreElements() )
				{
					key = (String)en.nextElement();
					paypallog.info(key+" = "+StringUtil.getString(request,key));
				}
				paypallog.info("================== Empty txn_id Request end ==================");

			}
				
		}

		catch (Exception e) 
		{
			paypallog.info("=============" + txn_id + " Insert Failuer Start" + "============");

			Enumeration en = request.getParameterNames();
			while( en.hasMoreElements() )
			{
				key = (String)en.nextElement();
				paypallog.info(key+" = "+StringUtil.getString(request,key));
			}
			
			paypallog.info("===========" + txn_id + " Insert Failuer End" + "==============");
			
			throw new SystemException(e,"paypalNotify",log);
		}
	}

	/**
	 * 新进订单后，预处理数据中心
	 * @param method
	 * @param returnValue
	 */
	private void orderPreHandle(long oid)
	{
		try
		{         
			List<OrderPreHandleFather> orderPreHandleClassList = orderPreHandle.getOrderPreHandleClass();
			for(OrderPreHandleFather oph : orderPreHandleClassList)
			{
				if (oph!=null)
				{
					oph.perform( oid );
				}
			}
		}
		catch (Exception e) 
		{
			log.error("paypalNotify.orderPreHandle error:"+e);
		}
	}
	
	private boolean isPermit(HttpServletRequest request)
	{
		String txn_id = StringUtil.getString(request,"txn_id");
		
		if (txn_id.trim().equals(""))
		{
			return(false);
		}
		else
		{
			return(true);
		}
	}

//	public static void main(String args[])
//		throws Exception
//	{
//		for (int i=0; i<3; i++)
//		{
//			//system.out.print("i:"+i+" ");
//			for (int j=0; j<3; j++)
//			{
//				//system.out.print("j:"+j+" ");
//				for (int k=0; k<3; k++)
//				{
//					//system.out.print("k:"+k+" ");
//					break;
//				}
//				//system.out.println("");
//			}
//			//system.out.println("");
//		}
//	}
	
	public void setOrderPreHandle(OrderPreHandle orderPreHandle)
	{
		this.orderPreHandle = orderPreHandle;
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}
	
		
	public static void main(String[] args) 
		throws Exception
	{
		  FileInputStream input = new FileInputStream("D:/baowen/change.log");
		  BufferedReader in = new BufferedReader(new InputStreamReader(input)); 
		  BufferedWriter outw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("D:/baowen/t.txt")));
		  int i = 1;
		  while(true)
		  {
			  String s = in.readLine();
			  if(s != null)
			  {
			    if(s.length() > 40)
			    {
			     outw.write( "str +="+"\"&"+s.substring(40).replace(" ", "")+"\";");outw.newLine();
			    }
			  }
		   }
		 
	}
}
