package com.cwc.app.api;

import javax.xml.bind.annotation.XmlEnumValue;

public enum EquipmentType {
	//license plate needed only
    @XmlEnumValue("Bobtail Truck")
    BOBTAIL_TRUCK,

    //license plate needed only
    @XmlEnumValue("Box Truck")
    BOX_TRUCK,

    //license plate needed only
    @XmlEnumValue("Car")
    CAR,

    //lp+containerNos
    @XmlEnumValue("Tractor + Container")
    TRACTOR_PLUS_CONTAINER,

    //lp+trailers
    @XmlEnumValue("Tractor + Trailer")
    TRACTOR_PLUS_TRAILER,

    @XmlEnumValue("Tractor + Flatbed")
    TRACTOR_PLUS_FLATBED,

    @XmlEnumValue("Other")
    OTHER,

    @XmlEnumValue("Tractor Only")
    TRACTOR_ONLY
}
