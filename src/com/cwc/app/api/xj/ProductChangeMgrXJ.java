 package com.cwc.app.api.xj;

 


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.productChange.SameProductAndTypeException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.xj.FloorProductChangeMgrXJ;
import com.cwc.app.iface.xj.ProductChangeMgrIFaceXJ;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;



public class ProductChangeMgrXJ  implements ProductChangeMgrIFaceXJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorProductChangeMgrXJ floorProductChangeMgrXJ;
	private FloorProductMgr floorProductMgr;
	/**
	 * 向主表和详细表里添加数据
	 */
	@Override
	public long addProductChange(HttpServletRequest request) throws Exception {
		
		try
		{
			
			//获得创建人
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long pc_id = StringUtil.getLong(request, "pc_id");
			long type = StringUtil.getLong(request, "type");
			long ps_id =StringUtil.getLong(request, "ps_id");
			float quantity = StringUtil.getFloat(request, "quantity");
			long adminid =  adminLoggerBean.getAdid() ;
			String create_time = DateUtil.NowStr();
			DBRow result = floorProductChangeMgrXJ.findProductChangeByPsId(ps_id);
			long id = 0;
			long itemid = 0;
			if(result==null){
				DBRow row = new DBRow();
				row.add("ps_id", ps_id);
				row.add("adid",adminid );
				row.add("is_open", 1);
				row.add("create_time",create_time );
				id = floorProductChangeMgrXJ.addProductChange(row);
				DBRow rowItem = new DBRow();
				rowItem.add("cid", id);
				rowItem.add("pc_id", pc_id);
	  		    rowItem.add("pc_num",quantity);
				rowItem.add("type",type);
				rowItem.add("create_time",create_time);
				itemid=floorProductChangeMgrXJ.addProductChangeItem(rowItem);
			}else{
				DBRow rowItem = new DBRow();
				rowItem.add("cid", result.get("cid", 0));
				rowItem.add("pc_id", pc_id);
	  		    rowItem.add("pc_num",quantity);
				rowItem.add("type",type);
				rowItem.add("create_time",create_time);
				itemid=floorProductChangeMgrXJ.addProductChangeItem(rowItem);
			}
			return itemid;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProductChange",log);
		}
		
	}
	/**
	 * 根据主表id查询详细表记录
	 */
	@Override
	public DBRow[] findProductChangeItems(long cid,PageCtrl pc) throws Exception {
		try
		{
		   return floorProductChangeMgrXJ.findProductChangeItems(cid,pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProductChangeItems",log);
		}
	}
	/**
	 * 修改详细表记录
	 */
	@Override
	public void modProductChangeDetail(HttpServletRequest request) throws Exception {
		try
		{
			long itemid = StringUtil.getLong(request,"id");//grid默认参数是ID
			Map parameter = request.getParameterMap();
			DBRow para = new DBRow();
			long pc_num=0;
			DBRow row =  floorProductChangeMgrXJ.findProductChangeItemByItemId(itemid);
			long cid = row.get("cid", 01);
			long type = row.get("type", 0);
			long pc_id = row.get("pc_id", 01);
			if(parameter.containsKey("p_name"))
			{
				String p_name = StringUtil.getString(request,"p_name");
				DBRow product = floorProductMgr.getDetailProductByPname(p_name);
				if(product == null)
				{
					throw new ProductNotExistException();
				}
				pc_id=product.get("pc_id",0l);
				para.add("pc_id",pc_id);
				DBRow item = floorProductChangeMgrXJ.findProductChangeItem(pc_id,type,cid);
				if(item!=null)
				{
					throw new SameProductAndTypeException();
				}
			}
			if(parameter.containsKey("pc_num"))
			{
				pc_num = StringUtil.getLong(request,"pc_num");
				para.add("pc_num",pc_num);
			}
			if(parameter.containsKey("type"))
			{
				type = StringUtil.getLong(request,"type");
				para.add("type",type);
				DBRow item = floorProductChangeMgrXJ.findProductChangeItem(pc_id,type,cid);
				if(item!=null)
				{
					throw new SameProductAndTypeException();
				}
			}
			
		    floorProductChangeMgrXJ.modProductChangeDetail(itemid,para);
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(SameProductAndTypeException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProductChangeItems",log);
		}
		
	}
	/**
	 * 删除详细表记录
	 */
	@Override
	public void delProductChangeDetail(HttpServletRequest request) throws Exception {
		
		try {
			long itemid = StringUtil.getLong(request,"id");
			floorProductChangeMgrXJ.delProductChangeDetail(itemid);
		} catch (Exception e) {
			throw new SystemException(e,"delProductChange",log);
		}
		
	}
	/**
	 * 添加详细表记录
	 */
	@Override
	public void addProductChangeDetail(HttpServletRequest request)
			throws Exception {
		try
		{
			
			long pc_num = StringUtil.getLong(request,"pc_num");
			long cid = StringUtil.getLong(request,"cid");
			String p_name = StringUtil.getString(request,"p_name");
			String create_time = DateUtil.NowStr();
			long type = StringUtil.getLong(request,"type");
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			long pc_id =0;
			if(product==null)
			{
				throw new ProductNotExistException();
			}else{
				pc_id = product.get("pc_id", 0);
				DBRow item = floorProductChangeMgrXJ.findProductChangeItem(pc_id,type,cid);
				if(item!=null)
				{
					throw new SameProductAndTypeException();
				}
			}
			
			DBRow rowItem = new DBRow();
			rowItem.add("pc_id", pc_id);
			rowItem.add("cid",cid);
  		    rowItem.add("pc_num",pc_num);
			rowItem.add("type",type);
			rowItem.add("create_time",create_time);
			floorProductChangeMgrXJ.addProductChangeItem(rowItem);
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch(SameProductAndTypeException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProductChangeDetail",log);
		}
		
	}
	/**
	 * 查询主表记录
	 */
	@Override
	public DBRow[] findProductChange(PageCtrl pc,String ps_id,long is_open,String startTime,String endTime) throws Exception {
		try {
			return floorProductChangeMgrXJ.findProductChange(pc,ps_id,is_open,startTime, endTime);
		} catch (Exception e) {
			throw new SystemException(e,"findProductChange",log);
		}
	}
	public void setfloorProductChangeMgrXJ(FloorProductChangeMgrXJ floorProductChangeMgrXJ) {
		this.floorProductChangeMgrXJ = floorProductChangeMgrXJ;
	}
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
	public void setFloorProductChangeMgrXJ(
			FloorProductChangeMgrXJ floorProductChangeMgrXJ) {
		this.floorProductChangeMgrXJ = floorProductChangeMgrXJ;
	}
	
}
