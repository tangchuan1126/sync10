package com.cwc.app.api.xj;




import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.xj.FloorCheckInMgrXj;
import com.cwc.app.floor.api.zj.FloorEquipmentMgr;
import com.cwc.app.floor.api.zj.FloorSpaceResourcesRelationMgr;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.xj.CheckInMgrIfaceXj;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.app.key.EquipmentTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;


public class CheckInMgrXj implements CheckInMgrIfaceXj{
 
 
    private FloorCheckInMgrXj floorCheckInMgrXj;
	private CheckInMgrIfaceZwb checkInMgrZwb;
	private FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr;
	private FloorEquipmentMgr floorEquipmentMgr;
	private YMSMgrAPI ymsMgrAPI;
	private CheckInMgrIFaceZyj checkInMgrZyj;
    static Logger log = Logger.getLogger("ACTION");

 
	
    CheckInTractorOrTrailerTypeKey checkInTractorOrTrailerTypeKey = new CheckInTractorOrTrailerTypeKey();
    ModuleKey moduleKey = new ModuleKey();

	@Override
	public DBRow[] rtOccupiedSpot(long ps_id, long spot_area,int spotStatus, PageCtrl pc)
			throws Exception {
		try{
			return this.floorCheckInMgrXj.returnSpot(ps_id, spot_area, spotStatus,1, pc);
		}catch(Exception e){
			throw new SystemException(e,"rtOccupiedSpot",log);
		}
	}
	@Override
	public void batchModifySpotEquipment(long adminId, String equipment_ids,int  resource_type, long resource_id) throws Exception{
		try{
			String[] equipment_id = equipment_ids.split(",");
			for (int i = 0; i < equipment_id.length; i++) {
				this.modifySpotEquipment(adminId, Long.parseLong(equipment_id[i]), resource_type, resource_id);
			}
			
		}catch(Exception e){
			throw new SystemException(e,"batchModifySpotEquipment",log);
		}
	}
	
	/**
	 * @param adid
	 * @param equipment_id
	 * @param resource_type
	 * @param resource_id
	 * @throws Exception
	 * 
	 */
	@Override
	public void modifySpotEquipment(long adid,long equipment_id,int resource_type,long  resource_id)throws Exception {
			
		try{
			String now = DateUtil.NowStr();
			DBRow resource = null; 
			DBRow equipment = this.ymsMgrAPI.getDetailEquipment(equipment_id);
			DBRow spaceResourceRelation = this.ymsMgrAPI.getSpaceResorceRelation(SpaceRelationTypeKey.Equipment, equipment_id);
			String data = "";
			if (spaceResourceRelation !=null)
			{
				long resources_id = spaceResourceRelation.get("resources_id",0l);
				int resources_type = spaceResourceRelation.get("resources_type",0);
						
				if (resources_type == OccupyTypeKey.DOOR) 
				{
					
					resource = this.checkInMgrZwb.findDoorById(resources_id);
					data = " Door【"+resource.get("doorId", "") +"】" ;
					this.checkInMgrZwb.addCheckInLog(adid, ""+checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipment.get("equipment_type", 0))+" 【"+equipment.get("equipment_number", "")+"】 Released ", equipment.get("check_in_entry_id", 0l), CheckInLogTypeKey.ANDROID_PATROL,now,data);  //添加log

				}
				else if (resources_type==OccupyTypeKey.SPOT)
				{
					resource = this.checkInMgrZwb.findStorageYardControl(resources_id);
					data = " Spot【"+resource.get("yc_no", "") +"】" ;
					this.checkInMgrZwb.addCheckInLog(adid, ""+checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipment.get("equipment_type", 0))+" 【"+equipment.get("equipment_number", "")+"】 Released ", equipment.get("check_in_entry_id", 0l), CheckInLogTypeKey.ANDROID_PATROL,now,data);  //添加log

				}
			}		
			
			this.ymsMgrAPI.operationSpaceRelation(resource_type, resource_id, SpaceRelationTypeKey.Equipment, equipment_id, ModuleKey.CHECK_IN, equipment.get("check_in_entry_id", 0l), OccupyStatusTypeKey.OCUPIED, adid);
			DBRow[] tasks = this.floorCheckInMgrXj.findTaskByEquipmentId(equipment_id);
			for(DBRow task : tasks){
				DBRow  taskResource= this.ymsMgrAPI.getSpaceResorceRelation(SpaceRelationTypeKey.Task, task.get("dlo_detail_id", 0l));
				if(taskResource.get("resources_type", 0)==OccupyTypeKey.SPOT){
					this.ymsMgrAPI.operationSpaceRelation(resource_type, resource_id, SpaceRelationTypeKey.Task, task.get("dlo_detail_id", 0l), ModuleKey.CHECK_IN, equipment.get("check_in_entry_id", 0l), OccupyStatusTypeKey.OCUPIED, adid);

				}

			}
			
			DBRow originalResource = new DBRow();
			originalResource.add("original_resource_id", null);
			originalResource.add("original_resource_type", null);
			this.ymsMgrAPI.updateEquipment(equipment_id, originalResource);
			if(resource_type==OccupyTypeKey.SPOT){
				resource = this.checkInMgrZwb.findStorageYardControl(resource_id);
				data = " Spot【"+resource.get("yc_no", "") +"】" ;
				this.checkInMgrZwb.addCheckInLog(adid, ""+checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipment.get("equipment_type", 0))+" 【"+equipment.get("equipment_number", "")+"】 Occupied ", equipment.get("check_in_entry_id", 0l), CheckInLogTypeKey.ANDROID_PATROL,now,data);  //添加log
			}else if(resource_type==OccupyTypeKey.DOOR){
				resource = this.checkInMgrZwb.findDoorById(resource_id);
				data = " Door【"+resource.get("doorId", "") +"】" ;
				this.checkInMgrZwb.addCheckInLog(adid, ""+checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipment.get("equipment_type", 0))+" 【"+equipment.get("equipment_number", "")+"】 Occupied ", equipment.get("check_in_entry_id", 0l), CheckInLogTypeKey.ANDROID_PATROL,now,data);  //添加log
			}
			

			
			
		}catch(Exception e){
			throw new SystemException(e,"modifySpotEquipment",log);
		}
		
	}

	//释放资源
			
	@Override
	public void delSpaceResourcesRelation(int relation_type ,long relation_id ,long adid) throws Exception  {
		try{
			String now = DateUtil.NowStr();
			DBRow resource = null; 
			String remark ="";
			if(relation_type==SpaceRelationTypeKey.Equipment){
				DBRow equipment = this.ymsMgrAPI.getDetailEquipment(relation_id);
				remark=checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipment.get("equipment_type", 0))+" 【"+equipment.get("equipment_number", "")+"】 Released ";
			}else if(relation_type==SpaceRelationTypeKey.Task){
				DBRow task = this.checkInMgrZwb.selectDetailByDetailId(relation_id);
				remark=moduleKey.getModuleName(task.get("number_type", 0))+" 【"+task.get("number", "")+"】 Released ";
			}
			
			DBRow spaceResourceRelation = this.ymsMgrAPI.getSpaceResorceRelation(relation_type, relation_id);
			String data = "";
			if (spaceResourceRelation !=null)
			{
				long resources_id = spaceResourceRelation.get("resources_id",0l);
				int resources_type = spaceResourceRelation.get("resources_type",0);
						
				if (resources_type == OccupyTypeKey.DOOR) 
				{
					
					resource = this.checkInMgrZwb.findDoorById(resources_id);
					data = " Door【"+resource.get("doorId", "") +"】" ;
					this.checkInMgrZwb.addCheckInLog(adid, remark, spaceResourceRelation.get("module_id", 0l), CheckInLogTypeKey.ANDROID_PATROL,now,data);  //添加log

				}
				else if (resources_type==OccupyTypeKey.SPOT)
				{
					resource = this.checkInMgrZwb.findStorageYardControl(resources_id);
					data = " Spot【"+resource.get("yc_no", "") +"】" ;
					this.checkInMgrZwb.addCheckInLog(adid, remark, spaceResourceRelation.get("module_id", 0l), CheckInLogTypeKey.ANDROID_PATROL,now,data);  //添加log

				}
				DBRow originalResource = new DBRow();
				originalResource.add("original_resource_id", resources_id);
				originalResource.add("original_resource_type", resources_type);
				this.ymsMgrAPI.updateEquipment(relation_id, originalResource);
			}		
			this.floorSpaceResourcesRelationMgr.delSpaceResourcesRelation(relation_type, relation_id);
			
		}catch(Exception e){
			throw new SystemException(e,"delSpaceResourcesRelation",log);
		}
	}
	@Override
	public long createEntryId(HttpServletRequest request) throws Exception  {
		try{
			long resource_id=StringUtil.getLong(request,"resource_id");
			int resource_type=StringUtil.getInt(request,"resource_type");
			int equipment_type=StringUtil.getInt(request,"equipment_type");
			String equipment_number=StringUtil.getString(request,"equipment_number").toUpperCase();
			String phone_equipment_number=StringUtil.convertStr2PhotoNumber(equipment_number);
			EquipmentTypeKey equipmentTypeKey = new EquipmentTypeKey();
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");  
			HttpSession ses = StringUtil.getSession(request);
		    AdminLoginBean adminLoginBean  =  adminMgr.getAdminLoginBean(ses);
			long createManId = adminLoginBean.getAdid();
			long ps_id = adminLoginBean.getPs_id();
			String date =  DateUtil.NowStr();
			int flag = StringUtil.getInt(request,"flag");
			DBRow row = new DBRow();
			row.add("ps_id", ps_id);
			row.add("creator", createManId);
			row.add("create_time", date);
			row.add("gate_check_in_time", date);
			long mainId=this.floorCheckInMgrXj.createEntryId(row);
			String resource_name= checkInMgrZyj.windowCheckInHandleOccupyResourceName(resource_type,resource_id );
			long yc_id = 0;
			long doorId = 0;
			if(resource_type==OccupyTypeKey.SPOT){
				yc_id = resource_id;
			}else{
				doorId = resource_id;
			}
			OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
			//添加设备和资源
			String data="";
			
			DBRow equipment = new DBRow();
			equipment.add("equipment_type",equipment_type);
			equipment.add("check_in_entry_id",mainId);
			equipment.add("check_in_time",DateUtil.NowStr());
			equipment.add("equipment_number",equipment_number);
			equipment.add("phone_equipment_number",phone_equipment_number);
			if(equipment_type==EquipmentTypeKey.Container){
				equipment.add("equipment_purpose",CheckInLiveLoadOrDropOffKey.DROP);
			}else{
				equipment.add("equipment_purpose",CheckInLiveLoadOrDropOffKey.LIVE);

			}
			
			equipment.add("equipment_status", CheckInMainDocumentsStatusTypeKey.UNPROCESS);
			equipment.add("patrol_create",1);
			equipment.add("patrol_lost",0);
			equipment.add("rel_type",CheckInMainDocumentsRelTypeKey.PATROL);
			equipment.add("ps_id", ps_id);
			long equipment_id=this.floorEquipmentMgr.addEquipment(equipment);
			this.ymsMgrAPI.operationSpaceRelation(resource_type, resource_id, SpaceRelationTypeKey.Equipment, equipment_id, ModuleKey.CHECK_IN, mainId, OccupyStatusTypeKey.OCUPIED, createManId);
			data+=equipmentTypeKey.getEnlishEquipmentTypeValue(equipment_type)+":"+equipment_number+",";
			data+=occupyTypeKey.getOccupyTypeKeyName(resource_type)+":"+resource_name+",";
				
			data += checkInMgrZwb.gateCheckInAddNotice(mainId, 0, doorId, yc_id, adminLoginBean, "", 0, equipment_number, equipment_type); 
			if(flag==1){
				this.checkInMgrZwb.addCheckInLog(createManId, "Created Entry ID【"+mainId +"】 ", mainId, CheckInLogTypeKey.ANDROID_PATROL,date,data);  //添加log

			}else{
				this.checkInMgrZwb.addCheckInLog(createManId, "Created Entry ID【"+mainId +"】 ", mainId, CheckInLogTypeKey.ANDROID_PATROL,date,data);  //添加log

			}


			this.checkInMgrZwb.editCheckInIndex(mainId, "add");
			return mainId;
		}catch(Exception e){
			throw new SystemException(e,"createEntryId",log);
		}
	}

	@Override
	public DBRow[] verifySpot(long ps_id,PageCtrl pc) throws Exception {
		try{
			return this.floorSpaceResourcesRelationMgr.noSpaceRelationEquipment(ps_id,pc);
		}catch(Exception e){
			throw new SystemException(e,"verifySpot",log);
		}
	}

	@Override
	public long checkInPatrolApprove(HttpServletRequest request) throws Exception {
		try{
			//创建人
			long ps_id=StringUtil.getLong(request,"ps_id");
			long len=StringUtil.getLong(request,"len");
			String equipment_ids[] = request.getParameterValues("equipment_ids");
// 			AdminLoginBean adminLoginBean  = (AdminLoginBean)request.getSession().getAttribute(Config.adminSesion);
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");  
			HttpSession ses = StringUtil.getSession(request);
		    AdminLoginBean adminLoginBean  =  adminMgr.getAdminLoginBean(ses);
			long createManId = adminLoginBean.getAdid();
			long pa_id = 0;
	//		DBRow[] rows = this.floorCheckInMgrXj.verifySpot(ps_id);
				DBRow row = new DBRow();
				row.add("commit_adid", createManId);
				row.add("post_date", DateUtil.NowStr());
				row.add("difference_count", equipment_ids.length);
				row.add("difference_approve_count", 0);
				row.add("ps_id", ps_id);
				row.add("approve_status", 1);
				pa_id=this.floorCheckInMgrXj.addCheckInPatrolApprove(row);
				for (int i = 0; i < equipment_ids.length; i++) {
					DBRow statusRow = this.floorCheckInMgrXj.findGateCheckInById(Long.parseLong(equipment_ids[i]));
					DBRow detail = new DBRow();
					detail.add("pa_id", pa_id);
					detail.add("ps_id", ps_id);
					detail.add("equipment_id", equipment_ids[i]);
					detail.add("approve_status", 1);
//					detail.add("trailer_status", statusRow.get("status", 0));
//					detail.add("tractor_status", statusRow.get("tractor_status", 0));
	//				detail.add("doubt_yc_id", statusRow.get("doubt_yc_id", null));
	//				detail.add("doubt_door_id", statusRow.get("doubt_door_id", null));
					this.floorCheckInMgrXj.addCheckInPatrolApproveDetails(detail);
					DBRow patrol_lost =  new DBRow();
					patrol_lost.add("patrol_lost", 1);
					this.floorEquipmentMgr.updateEquipment(Long.parseLong(equipment_ids[i]),patrol_lost);
				}
				if(len==equipment_ids.length){
					this.AgainPatrolSpot(ps_id, 0);
					this.AgainPatrolDock(ps_id, 0);
				}
				
			
			return pa_id;
		}catch(Exception e){
			throw new SystemException(e,"checkInPatrolApprove",log);
		}
		
	}
	@Override
	public long AndroidcheckInPatrolApprove(HttpServletRequest request) throws Exception {
		try{
			//创建人
// 			AdminLoginBean adminLoginBean  = (AdminLoginBean)request.getSession().getAttribute(Config.adminSesion);
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");  
			HttpSession ses = StringUtil.getSession(request);
		    AdminLoginBean adminLoginBean  =  adminMgr.getAdminLoginBean(ses);
 			long ps_id = adminLoginBean.getPs_id();
			String equipment_id = StringUtil.getString(request,"equipment_id");
			String[] equipment_ids = equipment_id.split(",");
			long len=StringUtil.getLong(request,"all_length");
			long createManId = adminLoginBean.getAdid();
			long pa_id = 0;
	//		DBRow[] rows = this.floorCheckInMgrXj.verifySpot(ps_id);
				DBRow row = new DBRow();
				row.add("commit_adid", createManId);
				row.add("post_date", DateUtil.NowStr());
				row.add("difference_count", equipment_ids.length);
				row.add("difference_approve_count", 0);
				row.add("ps_id", ps_id);
				row.add("approve_status", 1);
				pa_id=this.floorCheckInMgrXj.addCheckInPatrolApprove(row);
				for (int i = 0; i < equipment_ids.length; i++) {
				//	DBRow statusRow = this.floorCheckInMgrXj.findGateCheckInById(Long.parseLong(equipment_ids[i]));
					DBRow detail = new DBRow();
					detail.add("pa_id", pa_id);
					detail.add("ps_id", ps_id);
					detail.add("equipment_id", equipment_ids[i]);
					detail.add("approve_status", 1);
//					detail.add("trailer_status", statusRow.get("status", 0));
//					detail.add("tractor_status", statusRow.get("tractor_status", 0));
	//				detail.add("doubt_yc_id", statusRow.get("doubt_yc_id", null));
	//				detail.add("doubt_door_id", statusRow.get("doubt_door_id",null));
					this.floorCheckInMgrXj.addCheckInPatrolApproveDetails(detail);
					DBRow patrol_lost =  new DBRow();
					patrol_lost.add("patrol_lost", 1);
					this.floorEquipmentMgr.updateEquipment(Long.parseLong(equipment_ids[i]),patrol_lost);
				}
				
				if(len==equipment_ids.length){
					this.AgainPatrolSpot(ps_id, 0);
					this.AgainPatrolDock(ps_id, 0);
				}
			return pa_id;
		}catch(Exception e){
			throw new SystemException(e,"AndroidcheckInPatrolApprove",log);
		}
		
	}
	@Override
	public DBRow[] findCheckInPatrolApprove(long ps_id, long approve_status,PageCtrl pc)
			throws Exception {
		try{
			return this.floorCheckInMgrXj.findCheckInPatrolApprove(ps_id,approve_status,pc);
		}catch(Exception e){
			throw new SystemException(e,"findCheckInPatrolApprove",log);
		}
	}
	@Override
	public DBRow[] findCheckInPatrolApproveDetails(long ps_id, long pa_id,
			PageCtrl pc) throws Exception {
		try{
			return this.floorCheckInMgrXj.findCheckInPatrolApproveDetails(ps_id,pa_id,pc);
		}catch(Exception e){
			throw new SystemException(e,"findCheckInPatrolApprove",log);
		}
	}
	@Override
	public long ConfirmPatrolSpot(long yc_id,long dlo_id,long area_id) throws Exception {
		try{
			String date = DateUtil.NowStr();
			DBRow yardRow =  new DBRow();
			yardRow.add("patrol_time", date);
			long id = this.floorCheckInMgrXj.modStorageParking(yc_id, yardRow);
			if(area_id>0){
				int count=this.floorCheckInMgrXj.findStorageParkingByAreaId(area_id);
				if(count==0){
					this.floorCheckInMgrXj.modStorageArea(area_id, yardRow);	
				}
				
			}
			return id;
		}catch(Exception e){
			throw new SystemException(e,"ConfirmPatrolSpot",log);
		}
		
	}

	@Override
	public long ConfirmPatrolDock(long sd_id,long dlo_id,long area_id) throws Exception {
		try{
			String date = DateUtil.NowStr();
			DBRow doorRow =  new DBRow();
			doorRow.add("patrol_time", date);
			long id=this.floorCheckInMgrXj.modStorageDoor(sd_id, doorRow);
			if(area_id>0){
				int count=this.floorCheckInMgrXj.findStorageDoorByAreaId(area_id);
				if(count==0){
					this.floorCheckInMgrXj.modStorageArea(area_id, doorRow);
				}
			}
			return id;
		}catch(Exception e){
			throw new SystemException(e,"ConfirmPatrolDock",log);
		}
	}
	
	@Override
	public DBRow confirmPatrolResource(long resource_id, int resource_type,long area_id,AdminLoginBean adminLoginBean) throws Exception {
			String date = DateUtil.NowStr();
			DBRow row =  new DBRow();
			row.add("patrol_time", date);
			DBRow result = new DBRow();
			DBRow[] needPatrol=null;
			DBRow[] finishPatrol=null;
			int count= 0;
	//		long resourceCount =0;
			if(resource_type==OccupyTypeKey.SPOT){
				this.floorCheckInMgrXj.modStorageParking(resource_id, row);
				if(area_id==0){
					DBRow spotRow =this.floorCheckInMgrXj.findStorageYardControl(resource_id);
					count=this.floorCheckInMgrXj.findStorageParkingByAreaId(spotRow.get("area_id", 0l));
					if(count==0){
						   this.floorCheckInMgrXj.modStorageArea(spotRow.get("area_id", 0l), row);	
					}
				}else{
				    count=this.floorCheckInMgrXj.findStorageParkingByAreaId(area_id);
				    if(count==0){
						   this.floorCheckInMgrXj.modStorageArea(area_id, row);	
					}
				}
				
				needPatrol=	this.returnSpot(adminLoginBean.getPs_id(), area_id, 0,1, null);
				finishPatrol=	this.returnSpot(adminLoginBean.getPs_id(), area_id, 0,2, null);
	//			resourceCount = this.floorCheckInMgrXj.findStoragePatrolTimeByPsId(adminLoginBean.getPs_id(),3);
			}else{
				this.floorCheckInMgrXj.modStorageDoor(resource_id, row);
				if(area_id==0){
					DBRow doorRow =this.floorCheckInMgrXj.findDoorById(resource_id);
					count=this.floorCheckInMgrXj.findStorageDoorByAreaId(doorRow.get("area_id", 0l));
					if(count==0){
						   this.floorCheckInMgrXj.modStorageArea(doorRow.get("area_id", 0l), row);	
					}
				}else{
					count=this.floorCheckInMgrXj.findStorageDoorByAreaId(area_id);
					if(count==0){
						   this.floorCheckInMgrXj.modStorageArea(area_id, row);	
					}
				}
				
				needPatrol=	this.returnDoor(adminLoginBean.getPs_id(), area_id, 0,1, null);
				finishPatrol=	this.returnDoor(adminLoginBean.getPs_id(), area_id, 0,2, null);
	//			resourceCount = this.floorCheckInMgrXj.findStoragePatrolTimeByPsId(adminLoginBean.getPs_id(),2);
			}
			long needPatrolArea = this.findStoragePatrolTimeByPsId(adminLoginBean.getPs_id());
			
			result.add("count", needPatrolArea);
			result.add("needPatrol", needPatrol);
			result.add("finishPatrol", finishPatrol);
	//		result.add("resourceCount", resourceCount);
			if(needPatrolArea==0){
				this.ghostCtnrAutoCheckOut(adminLoginBean);
			}
			DBRow[] patrol_start_time = this.floorCheckInMgrXj.getPatrolStartTime(adminLoginBean.getPs_id());
			if(patrol_start_time.length==1){
				this.updateCtnrStatus(adminLoginBean.getPs_id());
			}
			return result;
	}
	public void ghostCtnrAutoCheckOut(AdminLoginBean adminLoginBean) throws Exception{
		
		DBRow[] patrol_start_time = this.floorCheckInMgrXj.getPatrolStartTime(adminLoginBean.getPs_id());
		String time ="";
		if(patrol_start_time.length>0){
			time = patrol_start_time[0].get("patrol_time","");
		}
		DBRow[] ghostCtnrs=this.floorCheckInMgrXj.ghostCtnrSecondConfirm(time);
		for(DBRow ghostCtnr : ghostCtnrs){
			this.verifyCheckOutSingle(ghostCtnr.get("equipment_id", 0), "", adminLoginBean);
		}
		
	}
	@Override
	public long AndroidConfirmPatrolSpot(long yc_id,long dlo_id,long area_id) throws Exception {
		try{
				String date = DateUtil.NowStr();
				DBRow yardRow =  new DBRow();
				yardRow.add("patrol_time", date);
				this.floorCheckInMgrXj.modStorageParking(yc_id, yardRow);
				if(area_id==0){
					DBRow spotRow =this.floorCheckInMgrXj.findStorageYardControl(yc_id);
					area_id = spotRow.get("area_id", 0l);
				}
				int count=this.floorCheckInMgrXj.findStorageParkingByAreaId(area_id);
				if(count==0){
				   this.floorCheckInMgrXj.modStorageArea(area_id, yardRow);	
				}
			return 1;
		}catch(Exception e){
			throw new SystemException(e,"ConfirmPatrolSpot",log);
		}
		
	}

	@Override
	public long AndroidConfirmPatrolDock(long sd_id,long dlo_id,long area_id) throws Exception {
		try{
			    String date = DateUtil.NowStr();
				DBRow yardRow =  new DBRow();
				yardRow.add("patrol_time", date);
				this.floorCheckInMgrXj.modStorageDoor(sd_id, yardRow);
				int count=this.floorCheckInMgrXj.findStorageDoorByAreaId(area_id);
				if(count==0){
				   this.floorCheckInMgrXj.modStorageArea(area_id, yardRow);	
				}
			return 1;
		}catch(Exception e){
			throw new SystemException(e,"ConfirmPatrolDock",log);
		}
	}
	@Override
	public DBRow AgainPatrolSpot(long ps_id, long area_id) throws Exception {
		try{
			DBRow row = new DBRow();
			row.add("patrol_time", null);
			this.floorCheckInMgrXj.AgainPatrolSpot(ps_id,  area_id,row);
			this.floorCheckInMgrXj.ClearAreaPatrolDoneTime(ps_id, 3, area_id, row);
			DBRow[] needPatrol =	this.returnSpot(ps_id, area_id, 0,1, null);
			DBRow[] finishPatrol =	this.returnSpot(ps_id, area_id, 0,2, null);
			DBRow result = new DBRow();
			result.add("needPatrol", needPatrol);
			result.add("finishPatrol", finishPatrol);
		    return result;
		}catch(Exception e){
			throw new SystemException(e,"AgainPatrolSpot",log);
		}
	}

	@Override
	public DBRow AgainPatrolDock(long ps_id, long area_id) throws Exception {
		try{
			DBRow row = new DBRow();
			row.add("patrol_time", null);
		    this.floorCheckInMgrXj.AgainPatrolDock(ps_id, area_id,row);
		    this.floorCheckInMgrXj.ClearAreaPatrolDoneTime(ps_id, 2, area_id, row);
		    DBRow[] needPatrol =	this.returnDoor(ps_id, area_id, 0,1, null);
			DBRow[] finishPatrol =	this.returnDoor(ps_id, area_id, 0,2, null);
			DBRow result = new DBRow();
			result.add("needPatrol", needPatrol);
			result.add("finishPatrol", finishPatrol);
		    return result;
		}catch(Exception e){
			throw new SystemException(e,"AgainPatrolDock",log);
		}
	}
	
	@Override
	public DBRow againPatrolResource(long resource_id, int resource_type,long area_id,AdminLoginBean adminLoginBean)
			throws Exception {
		DBRow row = new DBRow();
		row.add("patrol_time", null);
		int count = 0;
		if(resource_type==OccupyTypeKey.DOOR){
			this.floorCheckInMgrXj.modStorageDoor(resource_id, row);
			if(area_id==0){
				DBRow doorRow =this.floorCheckInMgrXj.findDoorById(resource_id);
				this.floorCheckInMgrXj.modStorageArea(doorRow.get("area_id", 0l), row);	
			}else{
				
				this.floorCheckInMgrXj.modStorageArea(area_id, row);	
				
			}
		}else{
			this.floorCheckInMgrXj.modStorageParking(resource_id, row);
			if(area_id==0){
				DBRow spotRow =this.floorCheckInMgrXj.findStorageYardControl(resource_id);
			    this.floorCheckInMgrXj.modStorageArea(spotRow.get("area_id", 0l), row);	
			}else{
			    
				this.floorCheckInMgrXj.modStorageArea(area_id, row);	
				
			}
			
		}
		
//		if(count!=0){
//			this.floorCheckInMgrXj.modStorageArea(area_id, row);	
//		}
		DBRow result = new DBRow();
		DBRow[] needPatrol=null;
		DBRow[] finishPatrol=null;
		if(resource_type==OccupyTypeKey.SPOT){
			needPatrol=	this.returnSpot(adminLoginBean.getPs_id(), area_id, 0,1, null);
			finishPatrol=	this.returnSpot(adminLoginBean.getPs_id(), area_id, 0,2, null);
		}else{
			needPatrol=	this.returnDoor(adminLoginBean.getPs_id(), area_id, 0,1, null);
			finishPatrol=	this.returnDoor(adminLoginBean.getPs_id(), area_id, 0,2, null);
		}
		result.add("needPatrol", needPatrol);
		result.add("finishPatrol", finishPatrol);
		return result;
	}
	@Override
	public long ClearAreaPatrolDoneTime(long ps_id,long area_type ,long area_id) throws Exception {
		try{
			DBRow row = new DBRow();
			row.add("patrol_time", null);
			return this.floorCheckInMgrXj.ClearAreaPatrolDoneTime(ps_id,area_type ,area_id,row);
		}catch(Exception e){
			throw new SystemException(e,"ClearAreaPatrolDoneTime",log);
		}
	}
	@Override
	public long findStoragePatrolTimeByPsId(long ps_id) throws Exception {
		try{
			return this.floorCheckInMgrXj.findStoragePatrolTimeByPsId(ps_id);
		}catch(Exception e){
			throw new SystemException(e,"findStoragePatrolTimeByPsId",log);
		}
	}
 
	@Override
	public void verifyCheckOut(HttpServletRequest request) throws Exception {
		try{
			//创建人
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long adminId = adminLoginBean.getAdid();
			long ps_id = adminLoginBean.getPs_id();
			String createTime = DateUtil.NowStr();
			String pad_ids[] = request.getParameterValues("pad_ids");
			long pa_id = StringUtil.getLong(request,"pa_id");
			if (pad_ids!=null&&pad_ids.length>0)
			{
		//		long ps_id = StringUtil.getLong(request,"ps_id");
				

				//更新差异原因
				//标记为已经审核
				for (int i = 0; i < pad_ids.length; i++) {
					long padId = Long.parseLong(pad_ids[i]);
					DBRow result = this.floorCheckInMgrXj.findCheckInPatrolApproveDetailsByPadId(padId);
				
				/**		DBRow row = new DBRow();
					DBRow note = new DBRow();
					String data ="";
					if(equipmentRow.get("rel_type", 0l)==1){
						row.add("status", CheckInMainDocumentsStatusTypeKey.LEFT);
						row.add("tractor_status", CheckInMainDocumentsStatusTypeKey.LEFT);
						note.add("trailer_status", CheckInMainDocumentsStatusTypeKey.LEFT);
						note.add("tractor_status", CheckInMainDocumentsStatusTypeKey.LEFT);
						data+="Status: Delivery:Left";
					}else{
						row.add("status", CheckInMainDocumentsStatusTypeKey.LEFT);
						row.add("tractor_status", CheckInMainDocumentsStatusTypeKey.LEFT);
						note.add("trailer_status", CheckInMainDocumentsStatusTypeKey.LEFT);
						note.add("tractor_status", CheckInMainDocumentsStatusTypeKey.LEFT);
						data+="Status: Pick Up:Left";
					}
				    if(mainRow.get("doubt_yc_id", 0)>0){
						row.add("doubt_yc_id", null);
					}
					if(mainRow.get("doubt_door_id", 0)>0){
						row.add("doubt_door_id", null);
					}
				    row.add("check_out_time", DateUtil.NowStr());
					this.floorCheckInMgrXj.modCheckIn(result.get("entry_id", 0l), row);
					String noteApprove = StringUtil.getString(request, "note_"+pad_ids[i]);
					if(!noteApprove.equals("")){
						note.add("note",noteApprove );
					}
					note.add("approve_status", 2);
					this.floorCheckInMgrXj.modCheckInPatrolApproveDetails(padId,note);
					this.checkInMgrZwb.addCheckInLog(adminId, "Modified Entry ID【"+mainRow.get("dlo_id", 0l) +"】 Status", mainRow.get("dlo_id", 01), CheckInLogTypeKey.ANDROID_PATROL,createTime,data); */ //添加log
					DBRow row = new DBRow();
					DBRow note = new DBRow();
					row.add("check_out_time", createTime);
					row.add("equipment_status", CheckInMainDocumentsStatusTypeKey.LEFT);
					this.ymsMgrAPI.updateEquipment(result.get("equipment_id", 0l), row);
					String noteApprove = StringUtil.getString(request, "note_"+pad_ids[i]);
					if(!noteApprove.equals("")){
						note.add("note",noteApprove );
					}
					note.add("approve_status", 2);
					this.floorCheckInMgrXj.modCheckInPatrolApproveDetails(padId,note);
					DBRow equipmentRow = this.ymsMgrAPI.getDetailEquipment(result.get("equipment_id", 0l));
					this.checkInMgrZwb.addCheckInLog(adminId, checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipmentRow.get("equipment_type", 0))+"【 "+equipmentRow.get("equipment_number", "")+" 】 Check Out", equipmentRow.get("check_in_entry_id", 01), CheckInLogTypeKey.ANDROID_PATROL,createTime,""); //添加log

					int[] numberStatus = new int[0];
					DBRow[] tasks = this.checkInMgrZwb.getTaskByEquimentId(result.get("equipment_id", 0l), numberStatus);
					for (int j = 0; j < tasks.length; j++) {
						DBRow spaceResourceRelation = this.ymsMgrAPI.getSpaceResorceRelation(SpaceRelationTypeKey.Task, tasks[j].get("dlo_detail_id", 0l));
						if(spaceResourceRelation!=null){
							DBRow doorRow =  new DBRow();
							doorRow.add("rl_id", spaceResourceRelation.get("resources_id",0l));
							doorRow.add("occupancy_type", spaceResourceRelation.get("resources_type",0l));
							doorRow.add("is_forget_task", 1);
							this.checkInMgrZwb.updateDetailByIsExist(tasks[j].get("dlo_detail_id", 0l), doorRow);
							this.delSpaceResourcesRelation(SpaceRelationTypeKey.Task, tasks[j].get("dlo_detail_id", 0l), adminId);
						}
						
					}
					
				}
				
			}
			int notApproveCount = this.floorCheckInMgrXj.getNotApproveCountByPaId(pa_id);//检测是否全部已经审核完成
			int approveCount = this.floorCheckInMgrXj.getApproveCountByPaId(pa_id);
			
			DBRow approve = new DBRow();
			if (notApproveCount==0)
			{
				approve.add("approve_status",2);//已审核
				approve.add("approve_adid",adminId);//审核人
				approve.add("approve_date",DateUtil.NowStr());//时间
				approve.add("difference_approve_count",approveCount);//已审核数
			}
			else
			{
				approve.add("difference_approve_count",approveCount);//已审核数
			}
			this.floorCheckInMgrXj.modCheckInPatrolApprove(pa_id,approve);
			
		}catch(Exception e){
			throw new SystemException(e,"verifyCheckOut",log);
		}
		
	}
	@Override
	public void verifyCheckOutSingle(long equipment_id,String noteSingle,AdminLoginBean adminLoginBean) throws Exception {
		try{
			//创建人
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			long adminId = adminLoginBean.getAdid();
			String createTime = DateUtil.NowStr();
			
			
			DBRow equipmentRow = this.ymsMgrAPI.getDetailEquipment(equipment_id);
			DBRow row = new DBRow();
			row.add("equipment_status", CheckInMainDocumentsStatusTypeKey.LEFT);
			row.add("check_out_entry_id", equipmentRow.get("check_in_entry_id", 01));
			row.add("patrol_lost", 1);
			String data = "Reason:" +noteSingle;
			this.ymsMgrAPI.updateEquipment(equipment_id, row);
			this.checkInMgrZwb.addCheckInLog(adminId, checkInTractorOrTrailerTypeKey.getContainerTypeKeyValue(equipmentRow.get("equipment_type", 0))+"【 "+equipmentRow.get("equipment_number", "")+" 】 Check Out", equipmentRow.get("check_in_entry_id", 01), CheckInLogTypeKey.ANDROID_PATROL,createTime,data); //添加log

		}catch(Exception e){
			throw new SystemException(e,"verifyCheckOut",log);
		}
		
	}

	public long modStorageDoor(long door_id,long door_status,int associate_type,long associate_id,String patrol_time) throws Exception{
		try{
			DBRow row = new DBRow();
			row.add("occupied_status", door_status);
			row.add("associate_type", associate_type);
			row.add("associate_id", associate_id);
			row.add("patrol_time", patrol_time);
			return this.floorCheckInMgrXj.modStorageDoor(door_id, row);
			
		}catch(Exception e){
			throw new SystemException(e,"modStorageDoor",log);
		}
	}
	@Override
	public DBRow[] rtOccupiedDoor(long ps_id, long zone_area,int doorStatus,PageCtrl pc) throws Exception {
		try{
			return floorCheckInMgrXj.returnDoor(ps_id, zone_area, doorStatus,1, pc);
		}catch(Exception e){
			throw new SystemException(e,"rtOccupiedSpot",log);
		}
	}
	

	//将另外数据置为疑问数据
	@Override
	public void verifyData(long adminId,long ps_id,long mainId,String license_plate,String trailerNo,int flag,int doubt) throws Exception {
		try{
				
					DBRow[] results=null;
					if(doubt==1){
//	xujia					results=this.floorCheckInMgrXj.findMainMesByCondition(ps_id, 0, license_plate, trailerNo);
					}else{
//	xujia					results=this.floorCheckInMgrXj.findDockMainMesByCondition(ps_id, 0, license_plate, trailerNo);
					}
					//创建时间
					//	String createTime = DateUtil.NowStr();
					for(DBRow result : results){
						DBRow row = new DBRow();
						long dlo_id=result.get("dlo_id", 01);
						
						if(dlo_id!=mainId){
							
								if(doubt==1){
									int ycId = result.get("yc_id", -1);
//									int doubtYcId = result.get("doubt_yc_id", -1);
//									if(ycId==-1){
//										ycId = doubtYcId;
//									}
									row.add("doubt_yc_id", ycId);
									row.add("yc_id", null);
									this.floorCheckInMgrXj.updateParkingStatus(ycId, 1);
								}else{
									int doorId = result.get("sd_id", -1);
//									int doubtDoorId = result.get("doubt_door_id", -1);
//									if(doorId==-1){
//										doorId = doubtDoorId;
//									}
									row.add("doubt_door_id", doorId);
									row.add("door_id", null);
									DBRow detail = new DBRow();
									detail.add("occupancy_status",3);
									this.floorCheckInMgrXj.updateDetailByDoorIdAndDloId(dlo_id, doorId, detail);
								}
								//				status="Delivery:Closed  Left";
							
							this.floorCheckInMgrXj.modCheckIn(dlo_id, row);
//						String data="";
//					    data+="Status:"+status;
//						if(flag==1){
//							addCheckInLog(adminId, "Modified Entry ID【"+dlo_id +"】 Status", dlo_id, CheckInLogTypeKey.ANDROID_PATROL,createTime,data);  //添加log
//
//						}else{
//							addCheckInLog(adminId, "Modified Entry ID【"+dlo_id +"】 Status", dlo_id, CheckInLogTypeKey.ANDROID_PATROL,createTime,data);  //添加log
//
//						}
					}
					
					
				}
				
		
			
		}catch(Exception e){
			throw new SystemException(e,"verifyData",log);
		}
		
	}
	
	@Override
	public DBRow[] getSearchCheckInCTNRJSON(HttpServletRequest request,String trailerNo)
			throws Exception {
		try{
			// 获得创建人
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");  
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long ps_id=adminLoginBean.getPs_id();
			int flag = StringUtil.getInt(request, "flag");
			return this.floorCheckInMgrXj.getSearchCheckInCTNRJSON(ps_id,trailerNo,flag);
	}catch(Exception e){
		throw new SystemException(e,"getSearchCheckInCTNRJSON",log);
	}
	
	}

	@Override
	public DBRow[] getSearchCheckInLicensePlateJSON(HttpServletRequest request,String license_plate) throws Exception {
		try{

			//获得创建人
//			AdminLoginBean adminLoginBean  = (AdminLoginBean)request.getSession().getAttribute(Config.adminSesion);
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");  
			HttpSession ses = StringUtil.getSession(request);
		    AdminLoginBean adminLoginBean  =  adminMgr.getAdminLoginBean(ses);
			long ps_id = adminLoginBean.getPs_id();
			int flag = StringUtil.getInt(request, "flag");
			return this.floorCheckInMgrXj.getSearchCheckInLicensePlateJSON(ps_id,license_plate,flag);
		}catch(Exception e){
			throw new SystemException(e,"getSearchCheckInLicensePlateJSON",log);
		}
	}
	@Override
	public DBRow[] returnSpot(long ps_id, long area_id, int spot_status,int is_finish, PageCtrl pc) throws Exception {
		try{
			DBRow[] rows=floorCheckInMgrXj.returnSpot(ps_id, area_id, spot_status,is_finish, pc);
			
			for (int i = 0; i < rows.length; i++) {
					DBRow[] equipments = this.floorCheckInMgrXj.getEquipmentBySpotId(rows[i].get("resource_id", 0));
					List<DBRow> tractors = new ArrayList<DBRow>();
					List<DBRow> trailers = new ArrayList<DBRow>();
					for (int j = 0; j < equipments.length; j++) {
						if(equipments[j].get("equipment_type", 0)>0){
							if(equipments[j].get("equipment_type", 0)==CheckInTractorOrTrailerTypeKey.TRACTOR){
								DBRow tractor= new DBRow();
								tractor.add("equipment_number", equipments[j].get("equipment_number", ""));
								tractor.add("equipment_id", equipments[j].get("equipment_id", 0l));
								tractor.add("check_in_entry_id", equipments[j].get("check_in_entry_id", 0l));
								tractors.add(tractor);
							}
							if(equipments[j].get("equipment_type", 0)==CheckInTractorOrTrailerTypeKey.TRAILER){
								DBRow trailer= new DBRow();
								trailer.add("equipment_number", equipments[j].get("equipment_number", ""));
								trailer.add("equipment_id", equipments[j].get("equipment_id", 0l));
								trailer.add("check_in_entry_id", equipments[j].get("check_in_entry_id", 0l));
								trailers.add(trailer);
								
							}
						}
						
					}
					if(equipments.length>0){
						rows[i].add("occupy_status", OccupyStatusTypeKey.OCUPIED);	
					}
					
					rows[i].add("tractors", tractors.toArray(new DBRow[0]));
					rows[i].add("trailers", trailers.toArray(new DBRow[0]));
				
				rows[i].add("resource_type", OccupyTypeKey.SPOT);
				
			}
			
			return rows;
		}catch(Exception e){
			throw new SystemException(e,"returnSpot",log);
		}
	}
	@Override
	public DBRow[] returnDoor(long ps_id, long area_id, int door_status,int is_finish, PageCtrl pc) throws Exception {
		try{
			DBRow[] rows=floorCheckInMgrXj.returnDoor(ps_id, area_id, door_status,is_finish, pc);
			for (int i = 0; i < rows.length; i++) {
					DBRow[] equipments = this.floorCheckInMgrXj.getEquipmentByDoorId(rows[i].get("resource_id", 0));
					List<DBRow> tractors = new ArrayList<DBRow>();
					List<DBRow> trailers = new ArrayList<DBRow>();
					for (int j = 0; j < equipments.length; j++) {
						if(equipments[j].get("equipment_type", 0)>0){
							if(equipments[j].get("equipment_type", 0)==CheckInTractorOrTrailerTypeKey.TRACTOR){
								DBRow tractor= new DBRow();
								tractor.add("equipment_number", equipments[j].get("equipment_number", ""));
								tractor.add("equipment_id", equipments[j].get("equipment_id", 0l));
								tractor.add("check_in_entry_id", equipments[j].get("check_in_entry_id", 0l));
								tractors.add(tractor);
							}
							if(equipments[j].get("equipment_type", 0)==CheckInTractorOrTrailerTypeKey.TRAILER){
								DBRow trailer= new DBRow();
								trailer.add("equipment_number", equipments[j].get("equipment_number", ""));
								trailer.add("equipment_id", equipments[j].get("equipment_id", 0l));
								trailer.add("check_in_entry_id", equipments[j].get("check_in_entry_id", 0l));
								trailers.add(trailer);
								
							}
						}
						
					}
					if(equipments.length>0){
						rows[i].add("occupy_status", OccupyStatusTypeKey.OCUPIED);	
					}
					rows[i].add("tractors", tractors.toArray(new DBRow[0]));
					rows[i].add("trailers", trailers.toArray(new DBRow[0]));
				
				rows[i].add("resource_type", OccupyTypeKey.DOOR);
				
			}
			return rows;
		}catch(Exception e){
			throw new SystemException(e,"returnDoor",log);
		}
	}
	@Override
	public DBRow findResourceByEquipment(long ps_id, String license_plate,
			String trailerNo) throws Exception {
		try{
			DBRow[] rows =this.floorSpaceResourcesRelationMgr.findEquipmentWithResource(ps_id, license_plate, trailerNo);
			if(rows.length>0){
				return  rows[0];
			}else{
				return null;
			}
			
		}catch(Exception e){
			throw new SystemException(e,"returnDoor",log);
		}
	}
	
	@Override
	public DBRow[] getInYardEquipmentByEquipmentNumberLast (long ps_id,String equipment_number)throws Exception{
		try{
			DBRow[] rows = this.ymsMgrAPI.getInYardEquipmentByEquipmentNumberLast(ps_id,equipment_number,CheckInMainDocumentsStatusTypeKey.LEFT);
			  if(rows != null && rows.length>0){
				  for (int i = 0; i < rows.length; i++) {
			    	  if(rows[i].get("resources_type", 0)==OccupyTypeKey.SPOT){
			    		  DBRow spot=this.checkInMgrZwb.findStorageYardControl(rows[i].get("resources_id", 0));
			    		  rows[i].add("location", "Spot "+spot.get("yc_no", ""));
			    	  }
			    	  if(rows[i].get("resources_type", 0)==OccupyTypeKey.DOOR){
			    		  DBRow door=this.checkInMgrZwb.findDoorById(rows[i].get("resources_id", 0));
			    		  rows[i].add("location", "Door "+door.get("doorId", ""));
			    	  }
					
			      } 
			  }
			      
			return rows;
		}catch(Exception e){
			throw new SystemException(e,"getInYardEquipmentByEquipmentNumberLast",log);
		}
	}
	@Override
	public DBRow[] getEquipmentByDoorId(long door_id) throws Exception {
		try{
			return this.floorCheckInMgrXj.getEquipmentByDoorId(door_id);
		}catch(Exception e){
			throw new SystemException(e,"getEquipmentByDoorId",log);
		}
	}
	@Override
	public DBRow[] getEquipmentBySpotId(long yc_id) throws Exception {
		try{
			return this.floorCheckInMgrXj.getEquipmentBySpotId(yc_id);
		}catch(Exception e){
			throw new SystemException(e,"getEquipmentByDoorId",log);
		}
	}
	@Override
	public void updateEquipment(HttpServletRequest request) throws Exception {
		
		long equipment_id =  StringUtil.getLong(request, "equipment_id");
		String equipment_number = StringUtil.getString(request, "equipment_number");
		DBRow row = new DBRow();
		row.add("equipment_number", equipment_number);
		this.ymsMgrAPI.updateEquipment(equipment_id, row);
		AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");  
		HttpSession ses = StringUtil.getSession(request);
	    AdminLoginBean adminLoginBean  =  adminMgr.getAdminLoginBean(ses);
		long createManId = adminLoginBean.getAdid();
		EquipmentTypeKey  equipmentTypeKey = new EquipmentTypeKey();
		DBRow equipment = this.ymsMgrAPI.getDetailEquipment(equipment_id);
		String data = equipmentTypeKey.getEnlishEquipmentTypeValue(equipment.get("equipment_type", 0))+":"+equipment_number;
		this.checkInMgrZwb.addCheckInLog(createManId, "Update Equipment NO.", equipment.get("check_in_entry_id", 0), CheckInLogTypeKey.PATROL, DateUtil.NowStr(), data);
	
	}
	public String byteArrayToString(byte[] a){
		StringBuffer sb = new StringBuffer();
		for (int i : a) {
			sb.append((char)i);
		}
		return sb.toString();
	}
	@Override
	public void updateCtnrStatus(long ps_id) throws Exception {
		DBRow[] rows = this.floorSpaceResourcesRelationMgr.noSpaceRelationEquipment(ps_id, null);
		for(DBRow row:rows){
			DBRow status = new DBRow();
			status.add("patrol_lost",2);
			this.floorCheckInMgrXj.updateCtnrStatus(row.get("equipment_id", 0), status);
		}
		
	
	}
    public void setFloorCheckInMgrXj(FloorCheckInMgrXj floorCheckInMgrXj) {
			this.floorCheckInMgrXj = floorCheckInMgrXj;
	}
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	public void setFloorSpaceResourcesRelationMgr(
			FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr) {
		this.floorSpaceResourcesRelationMgr = floorSpaceResourcesRelationMgr;
	}
	public void setYmsMgrAPI(YMSMgrAPI ymsMgrAPI) {
		this.ymsMgrAPI = ymsMgrAPI;
	}
	public void setFloorEquipmentMgr(FloorEquipmentMgr floorEquipmentMgr) {
		this.floorEquipmentMgr = floorEquipmentMgr;
	}
	public void setCheckInMgrZyj(CheckInMgrIFaceZyj checkInMgrZyj) {
		this.checkInMgrZyj = checkInMgrZyj;
	}



	


}

