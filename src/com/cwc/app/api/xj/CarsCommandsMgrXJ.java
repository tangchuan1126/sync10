 package com.cwc.app.api.xj;

 

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;



import com.cwc.app.iface.xj.CarsCommandsMgrIFaceXJ;
import com.cwc.app.util.Constant;
import com.cwc.app.util.ConstantC;
import com.cwc.app.util.PointD;
import com.cwc.util.StringUtil;
import com.googlecode.jsonplugin.JSONUtil;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;



public class CarsCommandsMgrXJ  implements CarsCommandsMgrIFaceXJ {

	private ConstantC constantC;
	//mongo---------------------------------------------------------
	static private Mongo  mongo ;
	public DB db;
	private DBCollection history;
	private DBCollection lastHistory;
	private DBCollection cmd;
//	static { 
//		try 
//		{
////			mongo = new Mongo("localhost",27017);
//			mongo = new Mongo(constantC.mongodbHost,27017);
//		}
//		catch (UnknownHostException e) 
//		{
//			e.printStackTrace();
//		}
//		db=mongo.getDB(ConstantC.mongodbData);
//		auth(db);
//		history=db.getCollection(Constant.historyName);
//    	lastHistory=db.getCollection(Constant.lasHistoryName); 
//    	cmd=db.getCollection(Constant.command); 
//	} 
	
	private void auth(DB db){
		/*if(!db.isAuthenticated()){
			char[] pwd_char = constantC.mongodbPassword.toCharArray(); 
			
			boolean auth = db.authenticate(constantC.mongodbUser,pwd_char);
			if(!auth){ 
				//throw new RuntimeException(); 
			}
		}*/
	}
	//mongo---------------------------------------------------------
	
	public void initCollection()
	throws Exception
	{
		try 
		{
			mongo = new Mongo(constantC.mongodbHost,27017);
		}
		catch (UnknownHostException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db=mongo.getDB(constantC.mongodbData);
		auth(db);
		history=db.getCollection(Constant.historyName);
    	lastHistory=db.getCollection(Constant.lasHistoryName); 
    	cmd=db.getCollection(Constant.command); 
	}
	
	@Override
	public String queryHistory(HttpServletRequest request) {
		long asset_id = StringUtil.getLong(request, "asset_id");
		String starttime = StringUtil.getString(request, "startTime");
		String endtime = StringUtil.getString(request, "endTime");
		int timeZone = StringUtil.getInt(request, "timeZone");
		//转换为本地时间
    	int offset = Calendar.getInstance().getTimeZone().getOffset(0);
		long tzOffset = timeZone*60*60*1000;
		long stl = constantC.timeToLong(starttime) - tzOffset + offset; 
		long etl = constantC.timeToLong(endtime) - tzOffset + offset;

		DBObject queryCondition = new BasicDBObject();
		queryCondition.put("aid", asset_id + "");
		queryCondition.put("gt", new BasicDBObject("$gt", stl).append("$lte", etl));
		queryCondition.put("isl", true);
		DBCursor dbCursor = history.find(queryCondition).sort((DBObject) new BasicDBObject().put("gt", "1"));
		List<DBObject> list = new ArrayList<DBObject>();
		while (dbCursor.hasNext()) {
			DBObject db = dbCursor.next();
			String time = constantC.longToTime(Long.parseLong(db.get("gt").toString()));
			db.put("gt", time);
			String t = constantC.longToTime(Long.parseLong(db.get("t").toString()));
			db.put("t", t);
			db.put("sta", constantC.getStatus(db.get("sta").toString()));

			double lon = Double.parseDouble(db.get("lon").toString());
			double lat = Double.parseDouble(db.get("lat").toString());
			PointD p = constantC.changeMap(lon, lat, ConstantC.GOOGLEMAP);
			db.put("lon", p.getX());
			db.put("lat", p.getY());

			db.removeField("_id");
			list.add(db);
		}
		String result = null;
		try {
			result = JSONUtil.serialize(list);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public String getLastPos(String idStr) {
		DBObject queryCondition = new BasicDBObject();        
        BasicDBList values = new BasicDBList();  
        String[] ids = idStr.split(",");
        for(int i=0;i<ids.length;i++){
        	values.add(ids[i]);
        }  
        queryCondition.put("aid", new BasicDBObject("$in", values));  
        
        DBCursor dbCursor = lastHistory.find(queryCondition); 
        List<DBObject>  list=new ArrayList<DBObject>();
        while(dbCursor.hasNext()){
        	DBObject db=dbCursor.next();
        	String time=constantC.longToTime(Long.parseLong(db.get("gt").toString()));
        	db.put("gt_format", time);
        	String t=constantC.longToTime(Long.parseLong(db.get("t").toString()));
        	db.put("t_format", t); 
        	db.put("sta",constantC.getStatus( db.get("sta").toString()));
        	
        	double lon=Double.parseDouble(db.get("lon").toString());
        	double lat=Double.parseDouble(db.get("lat").toString());
        	PointD p=constantC.changeMap(lon, lat, ConstantC.GOOGLEMAP);
        	db.put("lon", p.getX());
        	db.put("lat", p.getY());
        	db.removeField("_id");
        	list.add(db);
        }
        String result=null;
		try {
			result = JSONUtil.serialize(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return result;
	}


	public void insertCmd(String assetid,String name,String call,String loctype,int cmdtype,Map params) throws Exception {
		
		BasicDBObject data = new BasicDBObject();
		String id = constantC.getUId();
		data.put("id", id);
		data.put("aid", assetid);
		data.put("cal", call);
		data.put("nam", name);
		data.put("lty", loctype);
		data.put("cty", cmdtype);
		data.put("par", params);
		data.put("t", System.currentTimeMillis());
		cmd.insert(data);
	}


	public void setConstantC(ConstantC constantC) {
		this.constantC = constantC;
	}
}
