package com.cwc.app.api;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.catalog.HaveProductException;
import com.cwc.app.exception.catalog.HaveRelationStorageException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.tjh.FloorProductLineMgrTJH;
import com.cwc.app.floor.api.zj.FloorProductCatalogMgrZJ;
import com.cwc.app.iface.CatalogMgrIFace;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.*;
import com.cwc.exception.SystemException;
import com.cwc.util.*;

public class CatalogMgr implements CatalogMgrIFace {
	
	static Logger log = Logger.getLogger("ACTION");

	private FloorCatalogMgr fcm;
	
	private FloorProductMgr fpm;
	
	private FloorProductCatalogMgrZJ floorProductCatalogMgrZJ;
	
	private FloorProductLineMgrTJH floorProductLineMgr;
	
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	
	private ProductMgrIFaceZJ productMgrZJ;
	
	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}

	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}
	
	public void setFcm(FloorCatalogMgr fcm) {
		this.fcm = fcm;
	}
	
	public void setFpm(FloorProductMgr fpm) {
		this.fpm = fpm;
	}

	public DBRow[] getAllProductCatalog() throws Exception {
		return fcm.getAllCatalog();
	}

	public void setFloorProductCatalogMgrZJ( FloorProductCatalogMgrZJ floorProductCatalogMgrZJ) {
		this.floorProductCatalogMgrZJ = floorProductCatalogMgrZJ;
	}

	public void setFloorProductLineMgr(FloorProductLineMgrTJH floorProductLineMgr) {
		this.floorProductLineMgr = floorProductLineMgr;
	}
	
	public DBRow[] getProductStorageCatalogTree()
		throws Exception
	{
		try
		{
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			
			DBRow rows[] = tree.getTree();
			return(rows);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductStorageCatalogTree",log);
		}
	}
	
	/**
	 * 允许发货仓库
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductDevStorageCatalogTree()
		throws Exception
	{
		try
		{
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"),"dev_flag=1");
			DBRow rows[] = tree.getTree();
			return(rows);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductDevStorageCatalogTree",log);
		}
	}

	/**
	 * 允许退货仓库
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductReturnStorageCatalogTree()
		throws Exception
	{
		try
		{
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"),"return_flag=1");
			DBRow rows[] = tree.getTree();
			return(rows);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductReturnStorageCatalogTree",log);
		}
	}

	public long addProductStorageCatalog(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String title = StringUtil.getString(request,"title");
			
//			String contact = StrUtil.getString(request,"contact");
//			String phone = StrUtil.getString(request,"phone");
			long parentid = StringUtil.getLong(request,"parentid");
			long ccid = StringUtil.getLong(request,"ccid");
			long pro_id = StringUtil.getLong(request,"pro_id");
//			String city = StrUtil.getString(request,"city");
			
//			String send_address1 = StrUtil.getString(request,"send_address1");
//			String send_address2 = StrUtil.getString(request,"send_address2");
//			String send_address3 = StrUtil.getString(request,"send_address3");
//			String address = send_address1+send_address2+send_address3;
//			String send_zip_code = StrUtil.getString(request,"send_zip_code");
			
//			String deliver_address1 = StrUtil.getString(request,"deliver_address1");
//			String deliver_address2 = StrUtil.getString(request,"deliver_address2");
//			String deliver_address3 = StrUtil.getString(request,"deliver_address3");
//			String deliver_address = deliver_address1+deliver_address2+deliver_address3;
//			String deliver_zip_code = StrUtil.getString(request,"deliver_zip_code");
			
			DBRow sonCatalog[] = getProductStorageCatalogByParentId(parentid,null);
			int sort;

			if ( sonCatalog.length==0 )
			{
				sort = 1;
			}
			else
			{
				sort = sonCatalog[sonCatalog.length-1].get("sort",0) + 1;
			}

			DBRow curCatalog = new DBRow();
			curCatalog.add("title",title);
			curCatalog.add("parentid",parentid);
			curCatalog.add("sort",sort);
//			curCatalog.add("address",address);
//			curCatalog.add("contact",contact);
//			curCatalog.add("phone",phone);
			curCatalog.add("native",ccid);
			curCatalog.add("pro_id",pro_id);
//			curCatalog.add("city",city);
			
//			curCatalog.add("send_address1",send_address1);
//			curCatalog.add("send_address2",send_address2);
//			curCatalog.add("send_address3",send_address3);
//			curCatalog.add("send_zip_code",send_zip_code);
			
//			curCatalog.add("deliver_address1",deliver_address1);
//			curCatalog.add("deliver_address2",deliver_address2);
//			curCatalog.add("deliver_address3",deliver_address3);
//			curCatalog.add("deliver_zip_code",deliver_zip_code);
//			curCatalog.add("deliver_address",deliver_address);
			
			long ps_id = fcm.addProductStorageCatalog(curCatalog);
				
			DBRow[] products = fpm.getAllProducts(null);
			
			for (int i = 0; i < products.length; i++) 
			{
				DBRow row = new DBRow();
				row.add("post_date",DateUtil.NowStr());
				row.add("cid",ps_id);
				row.add("pc_id",products[i].get("pc_id",0l));
				row.add("store_count",0);
				row.add("store_count_alert",0);
				fpm.addProductStorage(row);
			}
			
			return(ps_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProductStorageCatalog",log);
		}
	}

	public int modProductStorageCatalog(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long id = StringUtil.getLong(request,"id");
			String title = StringUtil.getString(request,"title");
			String address = StringUtil.getString(request,"address");
			String contact = StringUtil.getString(request,"contact");
			String phone = StringUtil.getString(request,"phone");
			long ccid = StringUtil.getLong(request,"ccid");
			
			DBRow catalog = new DBRow();
			catalog.add("title", title);
			catalog.add("address", address);
			catalog.add("contact", contact);
			catalog.add("phone", phone);
			catalog.add("native",ccid); 

			fcm.modProductStorageCatalog(id,catalog);	
			return(0);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modProductStorageCatalog",log);
		}
	}

	/**
	 * 货架有商品，禁止删除货架
	 * @param request
	 * @throws Exception
	 */
	public void delProductStorageCatalog(HttpServletRequest request)
		throws HaveRelationStorageException,Exception
	{
		try
		{
			long id = StringUtil.getLong(request,"id");

			if ( id>0 )
			{
				//获得该分类下所有子类
				Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
				ArrayList al = tree.getAllSonNode(id);
				al.add(String.valueOf(id));
				
				//先检测该分类下是否有商品，存在商品不能删除
				//该检查包括该分类下的所有子类
				if (fpm.getProductsInCids((String[])al.toArray(new String[0]), null).length>0)
				{
					throw new HaveRelationStorageException();
				}

				for ( int i=0; i<al.size(); i++ )
				{
					fcm.delProductStorageCatalog(StringUtil.getLong(al.get(i).toString()));
					fpm.delStorageCountryBySid(StringUtil.getLong(al.get(i).toString()));
					fpm.delStorageProvinceBySid(StringUtil.getLong(al.get(i).toString()));
				}				
			}
		}
		catch (HaveRelationStorageException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delProductStorageCatalog",log);
		}
	}
	
	public DBRow[] getAllFatherCatalog(long cid)
		throws Exception 
	{
		try
		{
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			
			return(tree.getAllFatherSQL(cid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllFatherCatalog",log);
		}
	}

	public DBRow[] getProductStorageCatalogByParentId(long parentid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getProductStorageCatalogByParentId(parentid,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductStorageCatalogByParentId",log);
		}
	}
	
//	public DBRow[] getProductStorageCatalogByParentIdByCount(long parentid,int count)
//		throws Exception
//	{
//		try
//		{
//			return(fcm.getProductStorageCatalogByParentIdByCount(parentid,count));
//		}
//		catch (Exception e)
//		{
//			throw new SystemException(e,"getProductStorageCatalogByParentIdByCount",log);
//		}
//	}
	
	public DBRow getDetailProductStorageCatalogById(long id)
		throws Exception
	{
		try
		{
			return(fcm.getDetailProductStorageCatalogById(id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductStorageCatalogById",log);
		}
	}
	
	public DBRow getProductStorageDetailCatalogByTitle(String title)
		throws Exception
	{ 
		try
		{
			return(fcm.getProductStorageDetailCatalogByTitle(title));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductStorageDetailCatalogByTitle",log);
		}
	}
	
	
	public DBRow[] geAllFather(long cid) 
		throws Exception
	{
		try
		{
			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			return(tree.getAllFatherSQL(cid));	
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"geAllFather",log);
		}
	}
	
	public DBRow[] getProductCatalogTree()
		throws Exception
	{
		try
		{
			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
			DBRow rows[] = tree.getTree();
			return(rows);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductCatalogTree",log);
		}
	}
	
	public DBRow getProductLineByName(String title) throws Exception {
		
		try {
			
			return floorProductLineMgr.getProductLineByName(title);
			
		} catch (Exception e) {
			throw new SystemException(e,"getProductLineByName",log);
		}
	}
	
	/**
	 * 添加产品分类[不能和产品线重名]
	 * 
	 * 
	 * */
	public DBRow addProductCatalog(HttpServletRequest request) throws Exception {
		
		try {
			
			String title = StringUtil.getString(request,"title");
			
			DBRow resultRow = new DBRow();
			
			//产品分类和产品线都不可重名
			if(0 == fcm.getProductCatalogsByName(title).length && null == floorProductLineMgr.getProductLineByName(title)){
				
				long parentid = StringUtil.getLong(request,"parentid");
				DBRow sonCatalog[] = getProductCatalogByParentId(parentid,null);
				
				int sort;
		
				if ( sonCatalog.length==0 ) {
					
					sort = 1;
				} else {
					
					sort = sonCatalog[sonCatalog.length-1].get("sort",0) + 1;
				}
				AdminMgr am = new AdminMgr();
				AdminLoginBean adminLoggerBean = am.getAdminLoginBean(request.getSession(true));
				DBRow curCatalog = new DBRow();
				curCatalog.add("title",title);
				curCatalog.add("parentid",parentid);
				curCatalog.add("sort",sort);
				curCatalog.add("creator",adminLoggerBean.getAdid());
				curCatalog.add("create_time",DateUtil.NowStr());
				
				// 一级分类以外 的产品分类时，productLine和一级分类的一样
				long productLineTitle = 0;
				if (parentid > 0){
					// 查询父级分类的产品线
					String sql="select pc.product_line_id as product_line_id from product_catalog pc where pc.id="+parentid;
					DBRow LineIdRows[] = getInfoBySql(sql);
					for (int i=0; i<LineIdRows.length; i++) {
						productLineTitle = LineIdRows[0].get("product_line_id",0l);
					}
				} else {
					productLineTitle = StringUtil.getLong(request, "productLineTitle");
				}
				
				if(productLineTitle > 0){
					curCatalog.add("product_line_id",productLineTitle);
				}
				
				//如果父分类有商品,则全部修改为子分类
				long categoryId = fcm.addProductCatalog(curCatalog);
				
				DBRow[] product = fcm.getProductByCategory(parentid == 0 ? -1 : parentid);
				
				for(DBRow one : product){
					
					DBRow param = new DBRow();
					param.put("catalog_id", categoryId);
					
					fcm.updateProduct(one.get("pc_id",0),param);
					
					productMgrZJ.updateCustomerTitleAndCategoryLineRelation(one.get("pc_id",0), parentid);
				}
				
				//fcm.updateProductCategoryByCategory(parentid,categoryId);
					
				floorProductCatalogMgrZJ.callProductCatalogChildList(0);//调用存储过程重算pc_child_list
				
				resultRow.add("flag", "true");
			} else {
				resultRow.add("flag", "false");
			}
			
			return resultRow;
			
		} catch (Exception e) {
			throw new SystemException(e,"addProductCatalog",log);
		}
	}
	
	private DBUtilAutoTran dbUtilAutoTran;
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public DBRow[] getInfoBySql(String sql) throws Exception
	{
		try
		{
			DBRow infoRows[] = dbUtilAutoTran.selectMutliple(sql);
			return(infoRows);
		}
		catch (Exception e)
		{
			log.error("CatalogMgr.getInfoBySql(sql) error:" + e);
			throw new Exception("getInfoBySql(sql) error:" + e);
		}
	}
	
	public int modProductCatalog(HttpServletRequest request)
		throws Exception
	{
		try
		{
			
			long id = StringUtil.getLong(request,"id");
			String title = StringUtil.getString(request, "title");
			DBRow rowByName = getDetailProductCatalogByName(title);
			boolean name_available =true;
			if(rowByName==null){
			name_available = true;
			}else if(id==rowByName.get("id", 0)){
			name_available = true;
			}else{
			name_available = false;
			}
			if(name_available){
			DBRow catalog = new DBRow();
			catalog.add("title", title);
			fcm.modProductCatalog(id,catalog);
			}
			return(0);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modProductCatalog",log);
		}
	}
	
	public DBRow delProductCatalog(HttpServletRequest request)
		throws HaveProductException,Exception
	{
		try
		{
			DBRow resultRow = new DBRow();
			long id = StringUtil.getLong(request,"id");
	
			if ( id>0 )
			{
				//获得该分类下所有子类
				Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
				ArrayList al = tree.getAllSonNode(id);
				al.add(String.valueOf(id));
				
				//先检测该分类下是否有商品，存在商品不能删除
				//该检查包括该分类下的所有子类
				if (fpm.getProductInCids((String[])al.toArray(new String[0]),0, null).length>0)
				{
					resultRow.add("flag", "false");
					return resultRow;
					//throw new HaveProductException();
				}

				for ( int i=0; i<al.size(); i++ )
				{
					fcm.delProductCatalog(StringUtil.getLong(al.get(i).toString()));
					//删除分类，也删除title和分类的关系
					//floorProprietaryMgrZyj.deleteProprietaryProductCatagoryById(0L, 0L, Long.parseLong(al.get(i).toString()));
				}
				
				floorProductCatalogMgrZJ.callProductCatalogChildList(0);//调用存储过程重算pc_child_list
				resultRow.add("flag", "true");
			} else{
				resultRow.add("flag", "false");
			}
			return resultRow;
		}
//		catch (HaveProductException e)
//		{
//			throw e;
//		}
		catch (Exception e)
		{
			throw new SystemException(e,"delProductCatalog",log);
		}
	}
	
	public DBRow[] getProductCatalogByParentId(long parentid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getProductCatalogByParentId(parentid,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductCatalogByParentId",log);
		}
	}
	
	public DBRow getDetailProductCatalogById(long id)
		throws Exception
	{
		try
		{
			return(fcm.getDetailProductCatalogById(id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductCatalogById",log);
		}
	}
	
	public DBRow getDetailProductCatalogByName(String name)
	throws Exception
	{
		try
		{
			return(fcm.getDetailProductCatalogByName(name));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductCatalogById",log);
		}
	}
	
	/**
	 * 标记为可以发货
	 * @param request
	 * @throws Exception
	 */
	public void markDevFlag(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long id = StringUtil.getLong(request, "id");
			DBRow sc = new DBRow();
			sc.add("dev_flag", 1);
			fcm.modProductStorageCatalog(id, sc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"markDevFlag",log);
		}
	}
	
	/**
	 * 标记为不可以发货
	 * @param request
	 * @throws Exception
	 */
	public void unMarkDevFlag(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long id = StringUtil.getLong(request, "id");
			DBRow sc = new DBRow();
			sc.add("dev_flag", 0);
			fcm.modProductStorageCatalog(id, sc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"unMarkDevFlag",log);
		}
	}
	
	/**
	 * 标记为可以退货
	 * @param request
	 * @throws Exception
	 */
	public void markReturnFlag(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long id = StringUtil.getLong(request, "id");
			DBRow sc = new DBRow();
			sc.add("return_flag", 1);
			fcm.modProductStorageCatalog(id, sc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"markReturnFlag",log);
		}
	}
	
	/**
	 * 标记为不可以退货
	 * @param request
	 * @throws Exception
	 */
	public void unMarkReturnFlag(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long id = StringUtil.getLong(request, "id");
			DBRow sc = new DBRow();
			sc.add("return_flag", 0);
			fcm.modProductStorageCatalog(id, sc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"unMarkReturnFlag",log);
		}
	}
	
	/**
	 * 标记为可以存放样品
	 * @param request
	 * @throws Exception
	 */
	public void markSampleFlag(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long id = StringUtil.getLong(request, "id");
			DBRow sc = new DBRow();
			sc.add("sample_flag", 1);
			fcm.modProductStorageCatalog(id, sc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"markSampleFlag",log);
		}
	}
	
	/**
	 * 标记为不可以存放样品
	 * @param request
	 * @throws Exception
	 */
	public void unMarkSampleFlag(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long id = StringUtil.getLong(request, "id");
			DBRow sc = new DBRow();
			sc.add("sample_flag", 0);
			fcm.modProductStorageCatalog(id, sc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"unMarkSampleFlag",log);
		}
	}
			
	public DBRow[] getProductDevStorageCatalogByParentId(long parentid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getProductDevStorageCatalogByParentId(parentid,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductDevStorageCatalogByParentId",log);
		}
	}
	
	/**
	 * 收货国家当地仓库
	 * @param parentid
	 * @param nativ
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNativeProductDevStorageCatalogByParentId(long parentid,long nativ,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getNativeProductDevStorageCatalogByParentId( parentid, nativ, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductDevStorageCatalogByParentId",log);
		}
	}	

	/**
	 * 收货国家海外仓库
	 * @param parentid
	 * @param nativ
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNonNativeProductDevStorageCatalogByParentId(long parentid,long nativ,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fcm.getNonNativeProductDevStorageCatalogByParentId( parentid, nativ, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductDevStorageCatalogByParentId",log);
		}
	}	
			
	/**
	 * 修改仓库所属国家
	 * @param request
	 * @throws Exception
	 */
	public void modProductStorageCatalogCountry(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long id = StringUtil.getLong(request,"id");
			long ccid = StringUtil.getLong(request,"ccid");
			String title = StringUtil.getString(request,"title");
			String contact = StringUtil.getString(request,"contact");
			String phone = StringUtil.getString(request,"phone");
			String city = StringUtil.getString(request,"city");
			
			long pro_id = StringUtil.getLong(request,"pro_id");
			
			String send_address1 = StringUtil.getString(request,"send_address1");
			String send_address2 = StringUtil.getString(request,"send_address2");
			String send_address3 = StringUtil.getString(request,"send_address3");
			String address = send_address1+send_address2+send_address3;
			String send_zip_code = StringUtil.getString(request,"send_zip_code");
			
			String deliver_address1 = StringUtil.getString(request,"deliver_address1");
			String deliver_address2 = StringUtil.getString(request,"deliver_address2");
			String deliver_address3 = StringUtil.getString(request,"deliver_address3");
			String deliver_address = deliver_address1+deliver_address2+deliver_address3;
			String deliver_zip_code = StringUtil.getString(request,"deliver_zip_code");
			
			DBRow catalog = new DBRow();
			catalog.add("native",ccid); 
			catalog.add("title",title);
			catalog.add("address",address);
			catalog.add("deliver_address",deliver_address);
			catalog.add("contact",contact);
			catalog.add("phone",phone);
			
			catalog.add("pro_id",pro_id);
			catalog.add("city",city);
			
			catalog.add("send_address1",send_address1);
			catalog.add("send_address2",send_address2);
			catalog.add("send_address3",send_address3);
			catalog.add("send_zip_code",send_zip_code);
			
			catalog.add("deliver_address1",deliver_address1);
			catalog.add("deliver_address2",deliver_address2);
			catalog.add("deliver_address3",deliver_address3);
			catalog.add("deliver_zip_code",deliver_zip_code);
			catalog.add("deliver_address",deliver_address);
	
			fcm.modProductStorageCatalog(id,catalog);	
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modProductStorageCatalogCountry",log);
		}
	}
	
	/*************************************************************************************/
	
	private void updateCustomerTitleAndCategoryLine(long beforeLineId,long categoryId) throws Exception{
		
		//清除产品线与title的关系
		fcm.deleteTitleProductLineByline(beforeLineId);
		
		proprietaryMgrZyj.customerTitleAndCategory(categoryId);
		
		//查询此分类的产品线
		DBRow category = fcm.getProductCategory(categoryId);
		
		if(category.get("product_line_id",0L)!=0){
			
			proprietaryMgrZyj.customerTitleAndLine(category.get("product_line_id",0));
		}
	}

	/*************************************************************************************/
	
	/**
	 * 基础数据>>产品线转化产品分类
	 * 
	 * @author subin
	 */
	public DBRow addProductCatalogByTransfer(HttpServletRequest request) throws Exception {
		
		try{
			
			DBRow resultRow = new DBRow();
			//要转化的产品线名称
			String title = StringUtil.getString(request,"title");
			//要转化的产品线
			long oldProductLineId = StringUtil.getLong(request,"oldProductLineId");
			//选择的产品线
			long productLineTitle = StringUtil.getLong(request, "productLineTitle");
			
			//验证此产品线的名称在产品分类中是否存在
			if(0 == fcm.getProductCatalogsByName(title).length){
				
				//删除产品线
				floorProductLineMgr.deleteProductLine(oldProductLineId);
				
				//查询排序
				DBRow sort = fcm.getProductCatalogSort(0l);
				
				DBRow curCatalog = new DBRow();
				curCatalog.add("title",title);
				curCatalog.add("parentid",0l);
				curCatalog.add("sort",sort.get("sort",1L));
				curCatalog.add("product_line_id",productLineTitle);
				
				//添加产品分类
				long id = fcm.addProductCatalog(curCatalog);
				
				//更新产品分类下的所有分类的信息//1.更新parentid//2.更新product_line_id
				DBRow[] productCategory = fcm.getProductCatalogByLineId(oldProductLineId);
				
				for(DBRow one : productCategory){
					
					DBRow param = new DBRow();
					param.put("product_line_id",productLineTitle);
					
					if(one.get("parentid",0l) == 0l){
						
						param.put("parentid",id);
					}
					
					fcm.updateCategoryProductLine(one.get("id",0l),param);
				}
				
				//调用存储过程重算pc_child_list
				floorProductCatalogMgrZJ.callProductCatalogChildList(0);
				
				//修改转化后影响的title关系
				this.updateCustomerTitleAndCategoryLine(oldProductLineId, id);
				
				resultRow.add("flag", "true");
			}else{
				
				resultRow.add("flag", "false");
			}
			
			return resultRow;
			
		} catch (Exception e){
			throw new SystemException(e,"addProductCatalogByTransfer",log);
		}
	}
	
	/**
	 * 基础数据>>更新产品分类的产品线
	 * 
	 * 
	 * @author subin
	 */
	public void updateCategoryProductLine(HttpServletRequest request) throws Exception{
		
		//分类ID
		long categoryId = StringUtil.getLong(request,"categoryId");
		//产品线ID
		String productLineId = StringUtil.getString(request,"productLineTitle","");
		
		//查询此分类
		DBRow catalog = fcm.getDetailProductCatalogById(categoryId);

		//原产品线与现产品线是否相同
		if (!catalog.get("product_line_id","").equals(productLineId)){
			
			//修改当前产品分类下面所有产品分类的product_line_id
			DBRow param = new DBRow();
			param.put("product_line_id", productLineId.equals("") ? null : productLineId);
			
			//更新一级
			fcm.updateCategoryProductLine(categoryId,param);
			
			DBRow[] subCategory = fcm.getSubProductCatalogByParentId(categoryId);
			
			for(DBRow one:subCategory){
				
				//更新二级
				fcm.updateCategoryProductLine(one.get("id",0L),param);
				
				DBRow[] subSubCategory = fcm.getSubProductCatalogByParentId(one.get("id",0L));
				
				for(DBRow two:subSubCategory){
					
					//更新三级
					fcm.updateCategoryProductLine(two.get("id",0L),param);
				}
			}
		}
		
		//产品线改变后影响的产品线与title的关系
		this.updateCustomerTitleAndLine(catalog.get("product_line_id",0L), categoryId);
	}
	
	/*******************************************************************************************/
	
	private void updateCustomerTitleAndLine(long beforeLineId,long categoryId) throws Exception {
		
		//比较分类原产品线与现产品线是否一样
		DBRow category = fcm.getProductCatalogById(categoryId);
		
		//判断原产品线与现在产品线是否相同
		if(category.get("product_line_id",0L) != beforeLineId){
			
			//原产品线与title的关系
			proprietaryMgrZyj.customerTitleAndLine(beforeLineId);
		
			//现产品线与title的关系
			proprietaryMgrZyj.customerTitleAndLine(category.get("product_line_id",0));
		}
	}
	
	/*******************************************************************************************/
	
	/**
	 *1.影响此分类原来的产品线与title的关系
	 *2.影响此分类现在的产品线与title的关系
	 *
	 *@param long[原产品线ID,分类ID]
	 *@author subin
	*/
	@SuppressWarnings("unused")
	private void updateProductLineAndTitle(long beforeLineId,long categoryId) throws Exception {
		
		//比较分类原产品线与现产品线是否一样
		DBRow category = fcm.getProductCatalogById(categoryId);
		
		//判断原产品线与现在产品线是否相同
		if(category.get("product_line_id",0L) != beforeLineId){
			
			//原产品线与title的关系
			this.beforeLineAndTitleRelation(beforeLineId);
		
			//现产品线与title的关系
			this.nowLineAndTitleRelation(categoryId);
		}
	}
	
	//原产品线与title的关系
	private void beforeLineAndTitleRelation(long beforeLineId) throws Exception{
		
		if(beforeLineId != 0){
			
			//查询产品线下产品分类与title的关系
			DBRow[] title1 = fcm.getTitleForLineCategoryRelation(beforeLineId);
			//查询产品线与title的关系
			DBRow[] title2 = fcm.getTitleForLineByLineId(beforeLineId);
			//对比title
			for(DBRow one:title1){
				
				for(DBRow two:title2){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						two.put("exist",1);
						break;
					}
				}
			}
			//删除不存在的title
			for(DBRow one:title2){
				
				if(one.get("exist",0)==0){
					
					fcm.deleteTitleProductLine(beforeLineId,one.get("title",-1L));
				}
			}
		}
	}
	
	//现产品线与title的关系
	private void nowLineAndTitleRelation(long categoryId) throws Exception{
		
		//查询产品线
		DBRow category = fcm.getProductCategory(categoryId);
		
		//如果存在产品线
		if(category.get("product_line_id",0L) != 0){
			
			//查询此产品线的title
			DBRow[] title1 = fcm.getTitleForLine(categoryId);
			//查询此分类的title
			DBRow[] title2 = fcm.getTitleForCategory(categoryId);
			//对比title
			for(DBRow one:title1){
				
				for(DBRow two:title2){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						two.put("exist",1);
						break;
					}
				}
			}
			//添加不存在的title
			for(DBRow one:title2){
				
				if(one.get("exist",0)==0){
					
					DBRow row = new DBRow();
					row.put("tpl_title_id", one.get("title",-1L));
					row.put("tpl_product_line_id", category.get("product_line_id",0L));
					
					fcm.insertTitleProductLine(row);
				}
			}
		}
	}
	
	/**
	* 产品线转分类: 产品线+其下分类不能大于3级
	* 
	* A:影响此产品线与title的关系(清除产品线与title的关系)
	* 
	* B:影响转为分类后,分类与title的关系(原来产品线与title的关系==现在分类与title的关系)
	* 
	* C:影响转为分类后,分类所属产品线与title的关系
	* 
	* @param long[转换前产品线ID,转化后的分类ID]
	* @author subin
	*/
	@SuppressWarnings("unused")
	private void lineToCategoryAffect(long beforeLineId,long categoryId) throws Exception{
		
		//查询产品线与title的关系
		DBRow[] title1 = fcm.getTitleForLineByLineId(beforeLineId);
		
		//清除产品线与title的关系
		fcm.deleteTitleProductLineByline(beforeLineId);
		
		//关联现在分类与title的关系
		for(DBRow one : title1){
			
			DBRow row = new DBRow();
			row.put("tpc_title_id",one.get("title",0L));
			row.put("tpc_product_catalog_id", categoryId);
			
			fcm.insertTitleProductCatalog(row);
		}
		
		//查询此分类的产品线
		DBRow category = fcm.getProductCategory(categoryId);
		
		if(category.get("product_line_id",0L)!=0){
			
			DBRow[] title2 = fcm.getTitleForLineByLineId(category.get("product_line_id",0L));
			
			//对比title
			for(DBRow one:title1){
				
				for(DBRow two:title2){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						one.put("exist", true);
						break;
					}
				}
			}
			//添加不存在的title
			for(DBRow one:title1){
				
				if(!one.get("exist",false)){
					
					DBRow row = new DBRow();
					row.put("tpl_title_id", one.get("title",0L));
					row.put("tpl_product_line_id", category.get("product_line_id",0L));
					
					fcm.insertTitleProductLine(row);
				}
			}
		}
	}
	
	/*************************************************************************************/
	
	/**
	 * 产品分类转化产品线
	 * 
	 */
	public DBRow addProductLineByTransfer(HttpServletRequest request) throws Exception {
		try{
			DBRow resultRow = new DBRow();
			String name = StringUtil.getString(request ,"productCategoryNameByTransfer");
			if(null == floorProductLineMgr.getProductLineByName(name)){
				// 保存新产品线
				DBRow params = new DBRow();
				params.add("name", name);
				long newProductLineId = floorProductLineMgr.addProductLine(params);
				
				// 修改成产品线的产品分类的下一级产品分类信息的变更（父节点=0、产品线）
				long oldProductCategoryId = StringUtil.getLong(request, "productCategoryIdByTransfer");
				ArrayList<DBRow> catalogList = getAllCategoryByParentId(oldProductCategoryId);
				if(!CollectionUtils.isEmpty(catalogList)){
					for (DBRow dbRow : catalogList) {
						if(dbRow.get("parentid",0l) == oldProductCategoryId){
							dbRow.add("parentid",0l);
						}
						dbRow.add("product_line_id", newProductLineId);
						fcm.updateCategoryProductLine(dbRow.get("id",0l),dbRow);
					}
				}
				
				// 删除修改成产品线的产品分类
				fcm.delProductCatalog(oldProductCategoryId);
				
				floorProductCatalogMgrZJ.callProductCatalogChildList(0);//调用存储过程重算pc_child_list
				resultRow.add("flag", "true");
			}else{
				resultRow.add("flag", "false");
			}
			return resultRow;
		} catch (Exception e){
			throw new SystemException(e,"addProductLineByTransfer",log);
		}
	}
	
	/**
	 * 查询当前产品分类下所有产品分类
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public ArrayList<DBRow> getAllCategoryByParentId(long id)throws Exception{
		ArrayList<DBRow> categoryDBRowList = new ArrayList<DBRow>();
		String sql;
		int pos = 0;
		try{
			while(true){
				sql = "select * from product_catalog where parentid=" + id;
				DBRow rows[] = dbUtilAutoTran.selectMutliple(sql);
				for(int i=0;i<rows.length;i++) {
					categoryDBRowList.add(rows[i]);
				}
				if(pos>categoryDBRowList.size()-1){
					break;
				}
				id  = categoryDBRowList.get(pos++).get("id", 0l);
				if(id <= 0l){
					break;
				}
			}
		}catch (Exception e){
			throw new Exception("getAllCategoryByParentId(" + id + ") error:" + e);
		}
		return(categoryDBRowList);
	}
}