package com.cwc.app.api;

import java.util.HashMap;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;

/**
 * 订单锁
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class OrderLock
{
	static Logger log = Logger.getLogger("ACTION");
	
	public static int RECORD = 1;	//抄单
	public static int MODIFY = 2;	//修改
	public static int PRINT = 3;	//打印
	
	public static int timeout = 1000 * 60  * 3;	//订单锁超时时间(默认3分钟,单位为毫秒)
	
	//存放被锁定的订单信息
	static HashMap lockedOrders = new HashMap();

	/**
	 * 获得订单锁信息
	 * @param oid
	 * @return
	 */
	private static DBRow getLockerInfo(long oid)
	{
		return((DBRow)lockedOrders.get( String.valueOf(oid) ));
	}
	
	public static String getMsg(String operate_type)
	{
		int operate_type_int = Integer.parseInt(operate_type);
		if (operate_type_int == OrderLock.RECORD)
		{
			return("抄单");
		}
		else if (operate_type_int == OrderLock.MODIFY)
		{
			return("修改订单");
		}
		else
		{
			return("打印订单");
		}
	}
	
	/**
	 * 判断订单是否被锁定
	 * @param oid
	 * @return
	 */
	private static boolean orderIsLocked(long oid,long adid)
	{
		String oidStr = String.valueOf(oid);
		
		if ( lockedOrders.containsKey(oidStr) )
		{
			DBRow orderLocker = getLockerInfo(oid);
			String timestamp = orderLocker.getString("timestamp");
			long operator_id = Long.parseLong(orderLocker.getString("operator_id"));

			long lockerTimestamp = Long.parseLong(timestamp);
			long nowTime = System.currentTimeMillis();
			
			//订单如果已经被锁定，还要检查锁定是否超时，如果锁定人是自己，则不算锁定
			if ( operator_id==adid || nowTime-lockerTimestamp>=timeout )
			{
				////system.out.println("====== Not Locked time:"+(nowTime-lockerTimestamp));
				return(false);
			}
			else
			{
				////system.out.println("====== Locked time:"+(nowTime-lockerTimestamp));
				return(true);
			}
		}
		else
		{
			return(false);
		}
	}

	/**
	 * 锁定订单
	 * @param session
	 * @param oid
	 * @param operate_type
	 * @return	锁定成功返回null，锁定失败，返回现在锁的信息
	 */
	public static synchronized DBRow lockOrder(HttpSession session,long oid,int operate_type)
	{
		try
		{
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
			
			if ( orderIsLocked(oid,adminLoggerBean.getAdid()) )
			{
				////system.out.println("====== Lock order false!");
				return(getLockerInfo(oid));
			}
			else
			{
				//封装订单锁信息
				DBRow orderLocker = new DBRow();
				orderLocker.add("oid",oid);
				orderLocker.add("operator",adminLoggerBean.getAccount());
				orderLocker.add("operator_id",adminLoggerBean.getAdid());
				orderLocker.add("operate_type",operate_type);
				orderLocker.add("timestamp",String.valueOf(System.currentTimeMillis()));
				
				lockedOrders.put(String.valueOf(oid),orderLocker);
				
				////system.out.println("====== Lock order true!");
			}
		}
		catch (Exception e)
		{
			log.error("lockOrder error:"+e);
		}
		
		
		return(null);
	}
	
	/**
	 * 解锁订单
	 * @param session
	 * @param oid
	 * @param operate_type
	 * @return
	 */
	public static synchronized DBRow unlockOrder(HttpSession session,long oid)
	{
		try
		{
			AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(session);
			
			DBRow orderLocker = getLockerInfo(oid);
			if (orderLocker!=null)
			{
				//执行人只能解锁自己锁定的订单
				if ( Long.parseLong(orderLocker.getString("operator_id"))==adminLoggerBean.getAdid() )
				{
					////system.out.println("====== unLock order true!");
					lockedOrders.remove(String.valueOf(oid));
				}
			}
		}
		catch (Exception e)
		{
			log.error("unlockOrder error:"+e);
		}

		return(null);
	}
	
	/**
	 * 清空当前用户占有的所有订单锁
	 * @param session
	 */
	public static void cleanMyOrderlocker(HttpSession session)
	{
		Set<String> keys = lockedOrders.keySet();
		
		for(String key:keys)
		{  
			unlockOrder(session,Long.parseLong(key));
		}  
	}
	
	
	public static void main(String args[])
	{
		DBRow rrr = new DBRow();
		rrr.add("type", 3);
		
		////system.out.println(rrr.getString("type"));
	}
	
	
	
}







