package com.cwc.app.api;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;


import com.cwc.app.api.mgr.ProductStoreMgrAPI;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuB2BOrderBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.bcs.PickUpCountMoreException;
import com.cwc.app.iface.zj.CartWaybillB2BMgrIFaceZJ;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;

public class ProductStoreMgr implements ProductStoreMgrIFaceZJ {

	static Logger log = Logger.getLogger("ACTION");
	private CartWaybillB2BMgrIFaceZJ cartWaybillB2BMgrZJ;
	
	private ProductStoreMgrAPI productStoreMgrAPI;
	
	/**
	 * 预保留库存
	 * 基于neo4j图数据库
	 * 保存小拣货单明细
	 */
	public void reserveProductStore(long ps_id, long pc_id, long title_id,float quantity, long system_bill_id, int system_type_id,String lot_number,AdminLoginBean adminLoggerBean,DBRow[] containerTypes)
		throws Exception 
	{
		try 
		{
			productStoreMgrAPI.reserveProductStore(ps_id, pc_id, title_id, quantity, system_bill_id, system_type_id, lot_number, adminLoggerBean, containerTypes);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"reserveProductStore",log);
		}
	}
	
	/**
	 * 预保留库存
	 * 基于neo4j图数据库
	 * 保存小拣货单明细
	 */
	public void reserveProductStoreSub(long ps_id, long pc_id, long title_id,long system_bill_id, int system_type_id,String lot_number,AdminLoginBean adminLoggerBean,int container_type,long container_type_id,int con_quantity)
		throws Exception
	{
		try 
		{
			productStoreMgrAPI.reserveProductStoreSub(ps_id, pc_id, title_id, system_bill_id, system_type_id, lot_number, adminLoggerBean, container_type, container_type_id, con_quantity);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"reserveProductStoreSub",log);
		}
	}
	
	/**
	 * 拣货单异常，再次预保留
	 * @param ps_id
	 * @param pc_id
	 * @param title_id
	 * @param container_type
	 * @param container_type_id
	 * @param con_quantity
	 * @param system_bill_id
	 * @param system_type_id
	 * @param lot_number
	 * @param adminLoggerBean
	 * @return 返回大拣货单明细
	 * @throws Exception
	 */
	public DBRow[] reserveProductStorePickException(long ps_id,long pc_id,long title_id,int container_type,long container_type_id,int con_quantity,long system_bill_id, int system_type_id,String lot_number,AdminLoginBean adminLoggerBean)
		throws Exception 
	{
		try 
		{
			return productStoreMgrAPI.reserveProductStorePickException(ps_id, pc_id, title_id, container_type, container_type_id, con_quantity, system_bill_id, system_type_id, lot_number, adminLoggerBean);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"reserveProductStorePickException",log);
		}
	}
	
	
	public void addOutListDetail(long pc_id,long slc_id,int quantity,long system_bill_id,int system_bill_type,long adid,long ps_location_id,int from_container_type,long from_container_type_id,long from_con_id,int pick_container_type,long pick_container_type_id,long pick_con_id,long title_id)
		throws Exception
	{
		try 
		{
			productStoreMgrAPI.addOutListDetail(pc_id, slc_id, quantity, system_bill_id, system_bill_type, adid, ps_location_id, from_container_type, from_container_type_id, from_con_id, pick_container_type, pick_container_type_id, pick_con_id, title_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addOutListDetail",log);
		}
	}
	
	public DBRow addOutStorebillOrderDetail(long pc_id,long slc_id,int quantity,long system_bill_id,int system_bill_type,long adid,long ps_location_id,int from_container_type,long from_container_type_id,long from_con_id,int pick_container_type,long pick_container_type_id,long pick_con_id)
		throws Exception
	{
		try 
		{
			return productStoreMgrAPI.addOutStorebillOrderDetail(pc_id, slc_id, quantity, system_bill_id, system_bill_type, adid, ps_location_id, from_container_type, from_container_type_id, from_con_id, pick_container_type, pick_container_type_id, pick_con_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addOutStorebillOrderDetail",log);
		}
	}
	
	/**
	 * B2B订单仓库预计算
	 * @param cartItems
	 * @param ps_id
	 * @param title_id
	 * @return
	 * @throws Exception
	 */
	public PreCalcuB2BOrderBean calcuOrderB2B(HttpSession session,long ps_id,long title_id)
		throws Exception
	{
		try
		{
			cartWaybillB2BMgrZJ.flush(session);
			DBRow[] cartItems = cartWaybillB2BMgrZJ.getCartWaybillB2bProduct();
			
			return productStoreMgrAPI.calcuOrderB2B(cartItems, ps_id, title_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"calcuOrderB2B",log);
		}
	}
	
	/**
	 * 审核容器，单纯的copy容器树
	 * @param ps_id
	 * @param con_id
	 * @throws Exception
	 */
	public void differentContainerApproveCopyTree(long ps_id,long con_id,long slc_id,long pc_id,long title_id,String lot_number,long time_number)
		throws Exception
	{
		try 
		{
			productStoreMgrAPI.differentContainerApproveCopyTree(ps_id, con_id, slc_id, pc_id, title_id, lot_number, time_number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"differentContainerApproveCopyTree",log);
		}
	}
	
	/**
	 * 库存审核记录日志
	 * @param ps_id
	 * @param slc_id
	 * @param pc_id
	 * @param quantity
	 * @param con_id
	 * @param title_id
	 * @param physicalOperation
	 * @param operation
	 * @param system_bill_type
	 * @param system_bill_id
	 * @param lot_number
	 * @param machine
	 * @param serial_number
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void differentContainerApprove(long ps_id,long slc_id,long pc_id,int quantity,long con_id,long title_id,int physicalOperation,int operation,int system_bill_type,long system_bill_id,String lot_number,String machine,String serial_number,AdminLoginBean adminLoggerBean)
			throws Exception
	{
		productStoreMgrAPI.differentContainerApprove(ps_id, slc_id, pc_id, quantity, con_id, title_id, physicalOperation, operation, system_bill_type, system_bill_id, lot_number, machine, serial_number, adminLoggerBean);
	}
	
	public PreCalcuB2BOrderBean calcuB2bOrderOptimalWareHouse(long b2b_oid,long optimal_psid,HttpSession session)
		throws Exception
	{
		try 
		{
			cartWaybillB2BMgrZJ.flush(session);
			DBRow[] cartItems = cartWaybillB2BMgrZJ.getCartWaybillB2bProduct();
			
			return productStoreMgrAPI.calcuB2bOrderOptimalWareHouse(b2b_oid, optimal_psid, cartItems);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"calcuB2bOrderOptimalWareHouse",log);
		}
	}
	
	/**
	 * 计算B2B订单非最优仓库
	 */
	public ArrayList<PreCalcuB2BOrderBean> calcuB2BOrderNotOptimalWareHouse(long b2b_oid,long optimal_psid,HttpSession session)
		throws Exception
	{
		try
		{
			cartWaybillB2BMgrZJ.flush(session);
			DBRow[] cartItems = cartWaybillB2BMgrZJ.getCartWaybillB2bProduct();
			
			return productStoreMgrAPI.calcuB2BOrderNotOptimalWareHouse(b2b_oid, optimal_psid, cartItems);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"calcuB2BOrder",log);
		}
	}
	
	
	/**
	 * 为MongoDB构建商品对象（商品分类路径与产品线）
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public HashMap<String,Object> productMongoDB(long pc_id)
		throws Exception
	{
		return productStoreMgrAPI.productMongoDB(pc_id);
	}
	
	/**
	 * 拣货
	 * @param ps_location_id
	 * @param quantity
	 * @throws Exception
	 */
	public void pickUpPhysical(long ps_id,long slc_id,long pc_id,float quantity,AdminLoginBean adminLoggerBean,long out_id,String serial_number,String machine,String foType,int from_type,long from_type_id,long from_con_id,int pick_type,long pick_type_id,long pick_con_id,long opation_con_id)
		throws Exception
	{
		try 
		{
			productStoreMgrAPI.pickUpPhysical(ps_id, slc_id, pc_id, quantity, adminLoggerBean, out_id, serial_number, machine, foType, from_type,from_type_id,from_con_id,pick_type,pick_type_id,pick_con_id,opation_con_id);
		} 
		catch (PickUpCountMoreException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"pickUpPhysical",log);
		}
	}
	
	public DBRow[] pickUpException(long ps_id,long slc_id,long pc_id,float error_quantity,long out_id,String serial_number,String machine,String foType,int from_type,long from_type_id,long from_con_id,int pick_type,long pick_type_id,long pick_con_id,long error_con_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try
		{
			return productStoreMgrAPI.pickUpException(ps_id, slc_id, pc_id, error_quantity, out_id, serial_number, machine, foType, from_type, from_type_id, from_con_id, pick_type, pick_type_id, pick_con_id, error_con_id, adminLoggerBean);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"pickUpException",log);
		}
	}
	
	/**
	 * 放货基于图数据库
	 */
	public void putProductToLocation(long ps_id,long pc_id,float quantity,long system_bill_id,int system_bill_type,AdminLoginBean adminLoggerBean,String location,String serial_number,long lp_id,long title_id,String lot_number,String machine,ProductStoreLogBean inProductStoreLogBean)
		throws Exception
	{
		try 
		{
			productStoreMgrAPI.putProductToLocation(ps_id, pc_id, quantity, system_bill_id, system_bill_type, adminLoggerBean, location, serial_number, lp_id, title_id, lot_number, machine, inProductStoreLogBean);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"putProductToLocation",log);
		}
	}
	
	/**
	 * ConfigChange放货（放货后同一事务内为原单据保留）
	 * @param ps_id
	 * @param pc_id
	 * @param quantity
	 * @param cc_id
	 * @param adminLoggerBean
	 * @param location
	 * @param serial_number
	 * @param lp_id
	 * @param title_id
	 * @param lot_number
	 * @param machine
	 * @param inProductStoreLogBean
	 * @throws Exception
	 */
	public void putProductToLocationForConfigChange(long ps_id,long pc_id,float quantity,long cc_id,AdminLoginBean adminLoggerBean,String location,String serial_number,long lp_id,long title_id,String lot_number,String machine,ProductStoreLogBean inProductStoreLogBean) 
		throws Exception
	{
		productStoreMgrAPI.putProductToLocationForConfigChange(ps_id, pc_id, quantity, cc_id, adminLoggerBean, location, serial_number, lp_id, title_id, lot_number, machine, inProductStoreLogBean);
		
	}
	
	/**
	 * 获得容器内装商品数量
	 * @param containter_type
	 * @param container_type_id
	 * @return
	 * @throws Exception
	 */
	public int getLPTypeBuiltInProductCount(int containter_type,long container_type_id)
		throws Exception
	{
		try 
		{
			return productStoreMgrAPI.getLPTypeBuiltInProductCount(containter_type, container_type_id);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getLPTypeBuiltInProductCount",log);
		}
	}
	
	/**
	 * 获得容器类型的可用数，类型为原样时，获得的是TLP内的散件数量
	 * @param ps_id
	 * @param pc_id
	 * @param title_id
	 * @param container_type
	 * @param container_type_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public int getContainerStoreAvailableCount(long ps_id,long pc_id,long title_id,int container_type,long container_type_id,String lot_number)
			throws Exception
	{
		try 
		{
			return productStoreMgrAPI.getContainerStoreAvailableCount(ps_id, pc_id, title_id, container_type, container_type_id, lot_number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getContainerStoreAvailableCount",log);
		}
	}
	
	/**
	 * 现在临时库内查询，如没有容器树，再到操作人的所属仓库查找
	 */
	public String containerLoadProduct(long ps_id,long con_id)
			throws Exception
	{
		try 
		{
			return productStoreMgrAPI.containerLoadProduct(ps_id, con_id);
		}
		catch (ContainerNotFoundException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"containerLoadProduct",log);
		}
	}
	
	/**
	 * 获得库存，根据商品ID进行GroupBy
	 */
	public DBRow[] getProductStoreCount(String ps_id, long slc_area, long slc_id,String titleIds, long pc_id, String lot_number, String catalogs,long product_line)
		throws Exception 
	{
		try
		{
			return productStoreMgrAPI.getProductStoreCount(ps_id, slc_area, slc_id, titleIds, pc_id, lot_number, catalogs, product_line);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStoreCount",log);
		}
	}
	
	/**
	 * 根据商品ID，title_id进行GroupBy
	 */
	public DBRow[] getProductStoreCountGroupByTitle(String ps_id, long slc_area, long slc_id,String titleIds, long pc_id, String lot_number, String catalogs,long product_line)
		throws Exception 
	{
		try
		{
			return productStoreMgrAPI.getProductStoreCountGroupByTitle(ps_id, slc_area, slc_id, titleIds, pc_id, lot_number, catalogs, product_line);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStoreCountGroupByTitle",log);
		}
	}		
	
	/**
	 * 根据商品ID，title_id，lot_number进行GroupBy
	 */
	public DBRow[] getProductStoreCountGroupByLotNumber(long ps_id,long title_id,long pc_id, String lot_number)
		throws Exception 
	{
		try
		{
			return productStoreMgrAPI.getProductStoreCountGroupByLotNumber(ps_id, title_id, pc_id, lot_number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStoreCountGroupByLotNumber",log);
		}
	}
	
	/**
	 * 根据商品ID，title_id，slc_id进行GroupBy
	 */
	public DBRow[] getProductStoreCountGroupBySlc(long ps_id, long title_id,long pc_id, String lot_number) 
		throws Exception 
	{
		try 
		{
			return productStoreMgrAPI.getProductStoreCountGroupBySlc(ps_id, title_id, pc_id, lot_number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStoreCountGroupBySlc",log);
		}
	}

	@Override
	public DBRow[] getProductStoreCountGroupByContainer(long ps_id,long slc_id,int container_type,long title_id,long pc_id, String lot_number)
		throws Exception 
	{
		try
		{
			return productStoreMgrAPI.getProductStoreCountGroupByContainer(ps_id, slc_id, container_type, title_id, pc_id, lot_number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStoreCountGroupByContainer",log);
		}
	}
	
	public DBRow[] getProductStoreContainerCount(long ps_id,long slc_id,long title_id,long pc_id,String lot_number)
		throws Exception
	{
		try
		{
			return productStoreMgrAPI.getProductStoreContainerCount(ps_id, slc_id, title_id, pc_id, lot_number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getProductStoreContainerCount",log);
		}
	}
	
	public boolean hasInnerContainer(long ps_id,long con_id)
		throws Exception
	{
		try 
		{
			return productStoreMgrAPI.hasInnerContainer(ps_id, con_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"hasInnerContainer",log);
		}
	}
	
	public HashMap<String,Object> getContainerInnerTree(long ps_id,long con_id,long pc_id)
		throws Exception
	{
		return productStoreMgrAPI.getContainerInnerTree(ps_id, con_id, pc_id);
	}

	
	public void copyTempContainerToContainer(long con_id) 
		throws Exception 
	{
		try
		{
			productStoreMgrAPI.copyTempContainerToContainer(con_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"copyTempContainerToContainer",log);
		}
	}
	
	public DBRow[] productCountOfContainer(long ps_id,long con_id,long title_id,long pc_id)
		throws Exception
	{
		try
		{
			return productStoreMgrAPI.productCountOfContainer(ps_id, con_id, title_id, pc_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"productCountOfContainer",log);
		}
	}

	public void setCartWaybillB2BMgrZJ(CartWaybillB2BMgrIFaceZJ cartWaybillB2BMgrZJ) {
		this.cartWaybillB2BMgrZJ = cartWaybillB2BMgrZJ;
	}

	public void setProductStoreMgrAPI(ProductStoreMgrAPI productStoreMgrAPI) {
		this.productStoreMgrAPI = productStoreMgrAPI;
	}
}
