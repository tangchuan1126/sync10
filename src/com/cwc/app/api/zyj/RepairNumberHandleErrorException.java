package com.cwc.app.api.zyj;

public class RepairNumberHandleErrorException  extends Exception {
	
	public RepairNumberHandleErrorException() 
	{
		super();
	}
	
	public RepairNumberHandleErrorException(String inMessage)
	{
		super(inMessage);
	}
}
