package com.cwc.app.api.zyj;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.api.zr.androidControl.AndroidPermissionUtil;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.checkin.CheckInAllTasksClosedException;
import com.cwc.app.exception.checkin.CheckInMustHasTasksException;
import com.cwc.app.exception.checkin.CheckInMustNotHaveTasksException;
import com.cwc.app.exception.checkin.CheckInTaskCanntDeleteException;
import com.cwc.app.exception.checkin.CheckInTaskHaveDeletedException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToEntryException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToOthersException;
import com.cwc.app.exception.checkin.CheckinTaskNotFoundException;
import com.cwc.app.exception.checkin.DoorHasUsedException;
import com.cwc.app.exception.checkin.DoorNotFindException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.EquipmentHasLoadedOrdersException;
import com.cwc.app.exception.checkin.EquipmentNotFindException;
import com.cwc.app.exception.checkin.EquipmentSameToEntryException;
import com.cwc.app.exception.checkin.ResourceHasUsedException;
import com.cwc.app.exception.checkin.SpotHasUsedException;
import com.cwc.app.exception.checkin.SpotNotFindException;
import com.cwc.app.exception.checkin.TaskNoAssignWarehouseSupervisorException;
import com.cwc.app.floor.api.wfh.FloorCheckInMgrWfh;
import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.floor.api.zyj.FloorCheckInMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.AdminMgrIFaceZJ;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.iface.zr.CheckInMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.ScheduleModel;
import com.cwc.app.iface.zr.StorageDoorMgrIfaceZr;
import com.cwc.app.iface.zr.StorageYardControlIfaceZr;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;
import com.cwc.app.iface.zyj.CheckInMgrIFaceZyj;
import com.cwc.app.iface.zyj.OrderSystemMgrIFace;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInOrderComeStatusKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.app.key.ContainerImportStatusKey;
import com.cwc.app.key.EquipmentTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.PlateLevelProductInventaryExceptionKey;
import com.cwc.app.key.PlateStatusInOrOutBoundKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.key.WaitingTypeKey;
import com.cwc.app.key.WmsOrderTypeKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.lucene.zyj.PlateInventaryIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.XmlUtil;
import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONException;
import com.fr.base.core.json.JSONObject;

public class CheckInMgrZyj implements CheckInMgrIFaceZyj{

	static Logger log = Logger.getLogger("ACTION");
	private AdminMgrIFaceZJ adminMgrZJ;
	private FloorCheckInMgrZyj floorCheckInMgrZyj;
	private SQLServerMgrIFaceZJ sqlServerMgrZJ;
	private SystemConfigIFace systemConfig;
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	private FloorShipToMgrZJ floorShipToMgrZJ;
	private YMSMgrAPI ymsMgrAPI;
	private TransactionTemplate sqlServerTransactionTemplate;
	private StorageDoorMgrIfaceZr storageDoorMgrZr;
	private StorageYardControlIfaceZr storageYardControlZr;
	private FloorCheckInMgrZwb floorCheckInMgrZwb;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private OrderSystemMgrIFace orderSystemMgr;
	private FloorCheckInMgrWfh floorCheckInMgrWfh;
	
	public String checkInImportContainers(HttpServletRequest request)throws Exception
	{
		InputStream inputStream = request.getInputStream();                       //用二进制流接收数据
		String xml = getPost(inputStream);
//			xml = xml.replaceAll("\"","\\\"");
		DBRow row = XmlUtil.initDataXmlStringToDBRow(xml,new String[]{"Sheet","Account","Password"});
		String sheet  = row.getString("sheet");
		String account = row.getString("account");
		String password = row.getString("password");
		
		DBRow login = adminMgrZJ.MachineLogin(account, password);
		
		String result = "";
		
		if(login != null)
		{
			AdminLoginBean adminLoggerBean =  AndroidPermissionUtil.androidSetSession(login, request);

			switch (sheet)
			{
				case "SamSungImportContainer":
					result = handleContainerInfo(xml, adminLoggerBean);
					break;
			}
		}
		else
		{
			result = "LoginError";
		}
		
		return result;
	}
	
	private String getPost(InputStream inputStream)throws Exception
	{
		BufferedInputStream input = null;                                       //输入流,用于接收请求的数据
		byte[] buffer = new byte[1024];                                             //数据缓冲区
		int count = 0;                                                            //每个缓冲区的实际数据长度
		ByteArrayOutputStream streamXML = new ByteArrayOutputStream();            //请求数据存放对象
		byte[] iXMLData = null;   
		try 
		{
			input = new BufferedInputStream(inputStream);
			while ((count = input.read(buffer)) != -1)
			{
			    streamXML.write(buffer, 0, count);
			} 
		 }
		 catch (Exception e)
		 {
			e.printStackTrace();
		 }
		 finally
		 {
			if(input != null)
			{
			    try 
			    {
			     input.close();
			    }
			    catch (Exception f)
			    {
			    	f.printStackTrace();
			    }
			 }
		}
		iXMLData = streamXML.toByteArray();  
		
		return (new String(iXMLData));
	}
	
	
	/**
	 * 导入container信息
	 * @param rwb
	 * @return
	 * @throws Exception
	 */
	public String handleContainerInfo(String xml, AdminLoginBean adminLoggerBean)throws Exception
	{
		String result;
		try 
		{
			String[] nodeNames = {"Container","LoadID","ModelSize","Qty","ShipOutDate","CustomerID","BOL","StorageLocation","AccountID"}; 
			DBRow containerRow = XmlUtil.initDataXmlHasSpaceStringToDBRow(xml, nodeNames);
			String container = containerRow.getString("Container");
			String load = containerRow.getString("LoadID");
			String model_size = containerRow.getString("ModelSize");
			int qty = containerRow.get("Qty",0);
			String ship_out_date = containerRow.getString("ShipOutDate");
			String customer_id = containerRow.getString("CustomerID").trim().toUpperCase();
			String bol = containerRow.getString("BOL");
			String storage_location = containerRow.getString("StorageLocation");
			String account_id = containerRow.getString("AccountID");
		//	//system.out.println(po_no+","+container_no+","+customer_id+","+appointment_time);
			
			DBRow row = new DBRow();
			ship_out_date = dateFormat(ship_out_date);
	//		int[] status= new int[]{ContainerImportStatusKey.IMPORTED};
			DBRow[] ctnr=floorCheckInMgrZyj.getImportRepeatByLoadNo(load);
			if(ctnr.length>0){
				result = "Load ID is repeat";
			}
			else if(StrUtil.isBlank(ship_out_date))
			{
				result = "Ship Out Date format error.";
			}
			else
			{
				row.add("import_user", adminLoggerBean.getAdid());
				row.add("import_time", DateUtil.NowStr());
				row.add("status", ContainerImportStatusKey.IMPORTED);
				row.add("container_no", container);
				row.add("po_no",load );
				row.add("model_size", model_size);
				row.add("qty",qty );
				row.add("ship_out_date", ship_out_date);
				row.add("customer_id",customer_id );
				row.add("bol", bol);
				row.add("storage_location",storage_location );
				row.add("account_id",account_id );
				floorCheckInMgrZyj.addImportContainer(row);
				result = "Success";
			}
			////system.out.println("result:"+result);
		} 
		catch (Exception e) 
		{
			result = "System Error";
		}
		
		return result;
	}
	
	/**
	 * 批量添加plate
	 * @param dlo_detail_id
	 * @param ic_id
	 * @param plates
	 * @return
	 * @throws Exception
	 */
	public void batchAddContainerPlatesReceived(int dlo_detail_key,long dlo_detail_id, long ic_id, DBRow[] plates
			, long adid, String receive_time, long ps_id) throws Exception
	{
		try
		{
			if(dlo_detail_id > 0 && plates.length > 0)
			{
				for (int i = 0; i < plates.length; i++) 
				{
					String plate_no = plates[i].getString("pallet_number");
					long staging_area = plates[i].get("area", 0L);
					int box_number = plates[i].get("box_number", 0);
					double weight = plates[i].get("weight", 0d);
					if(!StrUtil.isBlank(plate_no))
					{
						addContainerPlateReceived(ic_id, plate_no, staging_area,box_number,dlo_detail_key, dlo_detail_id, adid, receive_time, weight, ps_id);
					}
				}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"batchAddContainerPlatesReceived",log);
		}
	}
	
	/**
	 * 添加containerPlate
	 * @param ic_id
	 * @param plate_no
	 * @param staging_area
	 * @throws Exception
	 */
	public void addContainerPlateReceived(long ic_id, String plate_no, long staging_area, int box_number
			,int receive_order_type, long dlo_detail_id, long adid, String receive_time, double weight, long ps_id) throws Exception
	{
		try
		{
			if(!StrUtil.isBlank(plate_no))
			{
				DBRow row = new DBRow();
				row.add("ic_id", ic_id);
				row.add("plate_no", plate_no);
				row.add("staging_area", staging_area);
				row.add("box_number", box_number);
				row.add("weight", weight);
 				long plate_id = floorCheckInMgrZyj.addImportContainerPlate(row);
 				DBRow icRow = floorCheckInMgrZyj.findImportContainerByIcid(ic_id);
 				String customer = "";
 				if(null != icRow)
 				{
 					customer = icRow.getString("customer_id");
 				}
 				addContainerPlateReceivedLog(plate_no, staging_area, box_number,receive_order_type, dlo_detail_id, ModuleKey.CHECK_IN_SAMSUNG_PLATE,plate_id, adid, receive_time, weight, 0L, customer, ps_id);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addContainerPlateReceived",log);
		}
	}
	
	/**
	 * 添加三星的托盘日志
	 * @throws Exception
	 */
	public void addContainerPlateReceivedLog(String plate_no, long staging_area, int box_number,int receive_order_type, long receive_order
			,int associate_type, long associate_order, long receive_adid, String receive_time, double weight, long customer_key, String customer_id, long ps_id)throws Exception
	{
		try
		{
				insertPlateLevelProductInventary(plate_no, customer_key, customer_id, box_number
						, staging_area, associate_type, associate_order
						, receive_adid, receive_time, receive_order_type, receive_order, weight, ps_id);
				
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addContainerPlateReceivedLog",log);
		}
	}
	
	
	/**
	 * 整体提货
	 * @param ic_id
	 * @param ship_entryd_id
	 * @throws Exception
	 */
	public void updateBatchContainerPlatesShipByIcid(long ic_id, long ship_entryd_id) throws Exception
	{
		try
		{
			if(ic_id > 0)
			{
				DBRow update = new DBRow();
				update.add("ship_entryd_id", ship_entryd_id);
				floorCheckInMgrZyj.updateContainerPlateBatch(ic_id, update);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateBatchContainerPlatesShipByIcid",log);
		}
	}
	
	
	/**
	 *单个plate提货
	 * @param icp_id
	 * @param ship_entryd_id
	 * @throws Exception
	 */
	public void updateContainerPlateShipByIcpid(long icp_id, long dlo_detail_id , DBRow loginRow ) throws Exception
	{
		try
		{
			if(icp_id > 0 && dlo_detail_id > 0)
			{
				DBRow update = new DBRow();
				update.add("ship_entryd_id", dlo_detail_id);
				floorCheckInMgrZyj.updateContainerPlateByIcpId(icp_id, update);
				 
				updatePlateShippedInfoByAssociate(ModuleKey.CHECK_IN_SAMSUNG_PLATE, icp_id
						, loginRow.get("adid", 0L), DateUtil.NowStr(), ModuleKey.CHECK_IN_ENTRY_DETAIL, dlo_detail_id, 0L);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateContainerPlateShipByIcpid",log);
		}
	}
	
	
	/**
	 *单个plate提货
	 * @param plateNo
	 * @param ship_entryd_id
	 * @throws Exception
	 */
	public void updateContainerPlateShipByPlateNo(String plateNo, long ship_entryd_id) throws Exception
	{
		try
		{
			if(!StrUtil.isBlank(plateNo) && ship_entryd_id > 0)
			{
				DBRow update = new DBRow();
				update.add("ship_entryd_id", ship_entryd_id);
				floorCheckInMgrZyj.updateContainerPlateByPlateNo(plateNo, update);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateContainerPlateShipByPlateNo",log);
		}
	}
	
	/**ContainerImportStatusKey
	 * 更新导入的containerInfo的状态
	 * @param status
	 * @param icid
	 * @throws Exception
	 */
	public void updateContainerInfoStatus(int status, long icid) throws Exception
	{
		try
		{
			if(icid > 0 && status > 0)
			{
				DBRow update = new DBRow();
				update.add("status", status);
				floorCheckInMgrZyj.updateContainerInfoByIcid(icid, update);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateContainerInfoStatus",log);
		}
	}
	
	
	/**
	 * 通过entryID查询container信息
	 * @param entryId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerInfosByEntryId(long entryId, PageCtrl pc) throws Exception
	{
		try
		{
			return floorCheckInMgrZyj.findContainerInfosByEntryId(entryId, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findContainerInfosByEntryId",log);
		}
	}
	
	
	/**
	 * 通过ContainerInfoID查询Plates
	 * @param containerNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlatesByIcid(long ic_id, PageCtrl pc)throws Exception
	{
	
		try
		{
			return floorCheckInMgrZyj.findPlatesByIcid(ic_id, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findPlatesByIcid",log);
		}
	}

	/**
	 * 通过ContainerNo，查询container信息
	 * @param containerNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerInfoByNo(String containerNo,int[] status, PageCtrl pc)throws Exception
	{
		try
		{
			if(!StrUtil.isBlank(containerNo))
			{
				return floorCheckInMgrZyj.findContainerInfoByNo(containerNo, status, null);
			}
			return new DBRow[0];
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findPlatesByIcid",log);
		}
	}
	
	/**
	 * 导数据时，判断日期格式
	 * @param format
	 * @return
	 */
	private String dateFormat(String format) {
		String str = "";
		Date date = null;
		 
			try {
				date = new SimpleDateFormat("yyyy/MM/dd").parse(format);
			} catch (ParseException e) {
				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(format);
				} catch (ParseException e1) {
					str = "";
				}
			}
		 
		if(null != date)
		{
			str = DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", date);
		}
		return str;
	}
	
	/**
	 * 查询容器哪些staging有哪个托盘
	 * @param icid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findStagingPlatesByIcid(long icid, PageCtrl pc)throws Exception
	{
		try
		{
			DBRow[] stagings = floorCheckInMgrZyj.findStagingAreasByIcid(icid, pc);
			for (int i = 0; i < stagings.length; i++)
			{
				long stagins = stagings[i].get("staging_area", 0L);
				DBRow[] plates = floorCheckInMgrZyj.findPlatesByIcStagingAreaId(icid, stagins, pc);
				stagings[i].add("plates", plates);
			}
			return stagings;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findStagingPlatesByIcid",log);
		}
	}
	
	/**
	 * 通过entryID查询pono信息
	 * @param entryId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPonoInfosByEntryId(long entryId, PageCtrl pc) throws Exception
	{
		try
		{
			return floorCheckInMgrZyj.findPonoInfosByEntryId(entryId, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findStagingPlatesByIcid",log);
		}
	}
	/**
	 * 报表    查询出入库状态
	 * @return
	 * @throws SystemException 
	 */
	@Override
	public DBRow[] findPlateNoInOutInfo(String startTime,String endTime, long ps_id_in) throws SystemException{
		try{
			DBRow[] mains = floorCheckInMgrZyj.findPlateNoInOutInfo(1, startTime, endTime,ps_id_in, null);
			DBRow[] rows = new DBRow[mains.length];
			int i = 0;
			for (DBRow row : mains) {
				DBRow data = new DBRow();
				rows[i] = data;
				String out = row.get("dlo_id_out", "");
				if(out.equals("")){
					data.add("Status", "OnHand");
				}else{
					data.add("Status", "Shipped");
				}
	//			PlateNo , Location, InTime, OutTime, ContainerNo, PONo, Entry In, Entry Out, ps_id_in, ps_title_in
				data.add("PlateNo", row.getString("plate_no",""));
				data.add("Location", row.getString("location_name",""));
				data.add("InTime", row.getString("finish_time_in",""));
				data.add("OutTime", row.getString("finish_time_out",""));
				data.add("PONo", row.getString("pono",""));
				data.add("EntryIn", row.getString("dlo_id_in",""));
				data.add("EntryOut", row.getString("dlo_id_out",""));
				data.add("PsIdIn", row.getString("ps_id_in"));
				data.add("PsTitleIn", row.getString("ps_title_in"));
//				data.add("PsIdOut", row.getString("ps_id_out"));
//				data.add("PsTitleOut", row.getString("ps_title_out"));
				
				data.add("ctnr", row.getString("ctnr",""));
				++i;
			}
			return rows;
		}catch (Exception e){
			throw new SystemException(e,"findPlateNoInOutInfo",log);
		}
	}
	
	/**
	 * filter托盘库存
	 * @param type
	 * @param startTime
	 * @param endTime
	 * @param ps_id_in
	 * @param status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchPlatesLevelInventary(int type, String startTime, String endTime, long ps_id_in, int status,long entry_id_in, long entry_id_out, long ic_id_in, long ic_id_out, PageCtrl pc)throws Exception
	{
		try
		{
			DBRow[] plates = floorCheckInMgrZyj.searchPlatesLevelInventary(type, startTime, endTime, ps_id_in, status,entry_id_in,entry_id_out,ic_id_in,ic_id_out, pc);
			for (DBRow row : plates) {
				String out = row.get("dlo_id_out", "");
				if(out.equals("")){
					row.add("status", "OnHand");
					row.add("status_key", PlateStatusInOrOutBoundKey.ON_HAND);
				}else{
					row.add("status", "Shipped");
					row.add("status_key", PlateStatusInOrOutBoundKey.SHIPPED);
				}
				//通过entry或者icid获取进出plateno
				int entryInIn = floorCheckInMgrZyj.findPlateCountByEntryIdIn(row.get("dlo_id_in", 0L));
				int entryInOut = floorCheckInMgrZyj.findPlateCountByEntryIdOut(row.get("dlo_id_in", 0L));
				int entryOutIn = floorCheckInMgrZyj.findPlateCountByEntryIdIn(row.get("dlo_id_out", 0L));
				int entryOutOut = floorCheckInMgrZyj.findPlateCountByEntryIdOut(row.get("dlo_id_out", 0L));
				int icIn = floorCheckInMgrZyj.findPlateCountByIcidIn(row.get("ic_id", 0L));
				//floorCheckInMgrZyj.findPlateCountByEntryDetailIdOut(row.get("ship_entryd_id", 0L));
				int icOut = floorCheckInMgrZyj.findPlateCountByIcidOut(row.get("ic_id", 0L));
				row.add("entry_in_in", entryInIn);
				row.add("entry_in_out", entryInOut);
				row.add("entry_out_in", entryOutIn);
				row.add("entry_out_out", entryOutOut);
				row.add("ic_in_cn", icIn);
				row.add("ic_out_cn", icOut);
			}
			return plates;
		}catch (Exception e){
			throw new SystemException(e,"searchPlatesLevelInventary",log);
		}
	}
	
	public double getDiffDateOrhhmmss(String strStartDate, String strEndDate,String rtnType)throws ParseException 
	{
		
		  TDate dateSt = new TDate(strStartDate);
		  TDate dateEn = new TDate(strEndDate);
		  long startT = fromDateStringToLong(dateSt.formatDate("yyyy-MM-dd HH:mm:ss")); //定义上机时间
		  long endT = fromDateStringToLong(dateEn.formatDate("yyyy-MM-dd HH:mm:ss"));  //定义下机时间
		  double ss=(endT-startT)/(1000); //共计秒数
		  double mm = ss/60;   //共计分钟数
		  double hh= mm/60;  //共计小时数
		  double dd= hh/24;   //共计天数
		  if(rtnType.equals("ss"))
		  {
			  return floatRemain1(ss);
		  }
		  else if(rtnType.equals("hh"))
		  {
			  return floatRemain1(hh);
		  }
		  else if(rtnType.equals("mm"))
		  {
			  return floatRemain1(mm);
		  }
		  else if(rtnType.equals("dd"))
		  {
			  return floatRemain1(dd);
		  }
		  else
		  {
			  return floatRemain1(dd);
		  }
	}
	public float floatRemain1(double day_count)
	{
		BigDecimal b = new BigDecimal(day_count);  
		float f1 =  b.setScale(1, BigDecimal.ROUND_HALF_UP).floatValue();  
		return f1;
	}
	private long fromDateStringToLong(String strDate) { // 此方法计算时间毫秒
		Date date = null; // 定义时间类型
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try 
		{
			date = inputFormat.parse(strDate); // 将字符型转换成日期型
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return date.getTime(); // 返回毫秒数
	}
	
	@Override
	public int findPlateCountByEntryIdIn(long entry_id) throws Exception {
		try{
			return floorCheckInMgrZyj.findPlateCountByEntryIdIn(entry_id);
		}catch (Exception e){
			throw new SystemException(e,"findPlateCountByEntryIdIn",log);
		}
	}

	@Override
	public int findPlateCountByEntryIdOut(long entry_id) throws Exception {
		try{
			return floorCheckInMgrZyj.findPlateCountByEntryIdOut(entry_id);
		}catch (Exception e){
			throw new SystemException(e,"findPlateCountByEntryIdOut",log);
		}
	}

	@Override
	public int findPlateCountByIcidIn(long ic_id) throws Exception {
		try{
			return floorCheckInMgrZyj.findPlateCountByIcidIn(ic_id);
		}catch (Exception e){
			throw new SystemException(e,"findPlateCountByIcidIn",log);
		}
	}

	@Override
	public int findPlateCountByIcidOut(long ic_id) throws Exception {
		try{
			return floorCheckInMgrZyj.findPlateCountByIcidOut(ic_id);
		}catch (Exception e){
			throw new SystemException(e,"findPlateCountByIcidOut",log);
		}
	}
	
	@Override
	public int findPlateCountByEntryDetailIdOut(long dlod_id) throws Exception
	{
		try{
			return floorCheckInMgrZyj.findPlateCountByEntryDetailIdOut(dlod_id);
		}catch(Exception e){
			 throw new Exception("findPlateCountByEntryDetailIdOut"+e);
		}
	}
	
	/**
	 * 从wms下载load信息，存在本地
	 * @param loadNo
	 * @param companyId
	 * @param status_one
	 * @throws Exception
	 */
	public DBRow[] findLoadInfosWmsAndInsertLocalForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID, String status_one, long adid, HttpServletRequest request) throws Exception
	{
		try{
			//sqlServer返回数据
			DBRow[] loadOrderPlates = sqlServerMgrZJ.findOrdersInfoByOrderTypeForConsolidate(order_type, order_no, CompanyID, CustomerID, status_one, adid, request);
//			//system.out.println(StringUtil.convertDBRowArrayToJsonString(loadOrderPlates));
			
			DBRow[] results = new DBRow[loadOrderPlates.length];//返回父级值（load或order）
			//遍历Load或Order，保存在Mysql，并组织返回数据
			for (int i = 0; i < loadOrderPlates.length; i++) 
			{
				DBRow masterBols = loadOrderPlates[i];
				DBRow[] orders = (DBRow[])masterBols.get("load_orders", new DBRow[0]);
				if(orders.length > 0)
				{
					results[i] = findLoadInfosInsertAndCheckExist(masterBols, adid);
					long load_id = results[i].get("load_id", 0L);
					DBRow[] resultOrders = new DBRow[orders.length];
					for (int j = 0; j < orders.length; j++) 
					{
						resultOrders[j] = findLoadOrdersInsertAndCheckExist(orders[j], load_id, adid);
						long order_id = resultOrders[j].get("order_id", 0L);
						DBRow[] orderPlates = (DBRow[])orders[j].get("order_plates", new DBRow[0]);
						DBRow[] resultPlates = findLoadOrderPlatesInsertAndCheckExist(orderPlates, order_id, adid);
						resultOrders[j].add("order_plates", resultPlates);
					}
					results[i].add("load_orders", resultOrders);
				}
				else
				{
					results[i] = findLoadOrdersInsertAndCheckExist(masterBols, 0, adid);
					long order_id = results[i].get("order_id", 0L);
					DBRow[] order_plates = (DBRow[])masterBols.get("order_plates", new DBRow[0]);
					DBRow[] resultPlates = findLoadOrderPlatesInsertAndCheckExist(order_plates, order_id, adid);
					results[i].add("order_plates", resultPlates);
				}
			}
			
//			//system.out.println(StringUtil.convertDBRowArrayToJsonString(results));
			
			return results;
		}catch(Exception e){
			 throw new Exception("findLoadInfosAndInsertLocal"+e);
		}
	}
	
	/**
	 * 添加consolidate load，如果存在，直接返回
	 * @param load
	 * @return
	 * @throws Exception
	 */
	private DBRow findLoadInfosInsertAndCheckExist(DBRow load, long adid) throws Exception
	{
		try
		{
			DBRow[] searchRows = floorCheckInMgrZyj.findMasterBolsByLoadNo(
					load.getString("LoadNo"), load.get("MasterBOLNo", 0L), load.getString("CompanyID")
					, load.getString("CustomerID"), null, 0L, null);
			if(0 == searchRows.length)
			{
				DBRow row = new DBRow();
				row.add("load_no", load.getString("LoadNo"));
				row.add("master_bol_no", load.getString("MasterBOLNo"));
				row.add("company_id", load.getString("CompanyID"));
				row.add("customer_id", load.getString("CustomerID"));
				row.add("account_id", load.getString("AccountID"));
				row.add("status", load.getString("Status"));
				row.add("shipto_id", load.getString("ShipToID"));
				row.add("pallet_type_id", load.getString("PalletTypeID"));
				row.add("case_total", load.getString("case_total"));
				row.add("item_total", load.getString("item_total"));
				row.add("pallet_total", load.getString("pallet_total"));
				row.add("create_user", adid);
				row.add("create_time", DateUtil.NowStr());
				long load_id = floorCheckInMgrZyj.insertConsolidateMasterBols(row);
				row.add("load_id", load_id);
				return row;
			}
			else
			{
				return searchRows[0];
			}
		}catch(Exception e){
			 throw new Exception("findLoadInfosInsert"+e);
		}
	}
	
	private DBRow findLoadOrdersInsertAndCheckExist(DBRow order, long load_id, long adid) throws Exception
	{
		try
		{
			DBRow[] searchRows = floorCheckInMgrZyj.findConsolidateOrdersByOrderNo(
					0L, order.get("OrderNo", 0L), order.getString("CompanyID"), order.getString("CustomerID")
					, order.getString("PONo"), null, 0L, 0L, null);
			if(0 == searchRows.length)
			{
				DBRow row = new DBRow();
				row.add("load_id", load_id);
				row.add("order_no", order.getString("OrderNo"));
				row.add("po_no", order.getString("PONo"));
				row.add("master_bol_no", order.getString("MasterBOLNo"));
				row.add("load_no", order.getString("LoadNo"));
				row.add("company_id", order.getString("CompanyID"));
				row.add("customer_id", order.getString("CustomerID"));
				row.add("account_id", order.getString("AccountID"));
				row.add("status", order.getString("Status"));
				row.add("shipto_id", order.getString("ShipToID"));
				row.add("picking_type", order.getString("PickingType"));
				row.add("staging_area_id", order.getString("StagingAreaID"));
				row.add("pallet_type_id", order.getString("PalletTypeID"));
				row.add("case_total", order.getString("case_total"));
				row.add("item_total", order.getString("item_total"));
				row.add("pallet_total", order.getString("pallet_total"));
				row.add("create_user", adid);
				row.add("create_time", DateUtil.NowStr());
				long order_id = floorCheckInMgrZyj.insertConsolidateOrders(row);
				row.add("order_id", order_id);
				return row;
			}
			else
			{
				return searchRows[0];
			}
		}catch(Exception e){
			 throw new Exception("findLoadOrdersInsert"+e);
		}
	}
	
	private DBRow[] findLoadOrderPlatesInsertAndCheckExist(DBRow[] plates, long order_id, long adid) throws Exception
	{
		try
		{
			List<DBRow> rePlates = new ArrayList<DBRow>();
			//DBRow[] rows = new DBRow[plates.length];
			for (int i = 0; i < plates.length; i++) 
			{
				DBRow plate = plates[i];
				
				DBRow[] searchRows = floorCheckInMgrZyj.findConsolidateOrderPlatesByOrderNo(
						plate.get("OrderNo", 0L), plate.getString("CompanyID"), plate.getString("PlateNo"), plate.get("LineNo", 0), 
						plate.get("OrderNo", 0L), plate.getString("CompanyID"), plate.getString("PlateNo"), plate.get("LineNo", 0), 
						plate.getString("ItemID"),plate.getString("LotNo"),plate.getString("SupplierID"), 0L, 0L, null);
				if(0 == searchRows.length)
				{
					DBRow row = new DBRow();
					row.add("order_id", order_id);
					row.add("lot_number", plate.getString("LotNo"));
					row.add("supplier_id", plate.getString("SupplierID"));
					row.add("item_id", plate.getString("ItemID"));//?
					row.add("order_no", plate.getString("OrderNo"));
					row.add("company_id", plate.getString("CompanyID"));
					row.add("line_no", plate.getString("LineNo"));
					row.add("plate_no", plate.getString("PlateNo"));
					row.add("shipped_qty", plate.getString("ShippedQty"));
					row.add("shipped_case", plate.getString("case_sum_qty"));
					
					row.add("order_no_from", plate.getString("OrderNo"));
					row.add("company_id_from", plate.getString("CompanyID"));
					row.add("line_no_from", plate.getString("LineNo"));
					row.add("plate_no_from", plate.getString("PlateNo"));
					row.add("shipped_qty_changed", plate.getString("ShippedQty"));
					row.add("shipped_case_changed", plate.getString("case_sum_qty"));
					row.add("create_user", adid);
					row.add("create_time", DateUtil.NowStr());
					long plate_id = floorCheckInMgrZyj.insertConsolidateOrderPlates(row);
					row.add("plate_id", plate_id);
					rePlates.add(row);
				}
				else
				{
					for (int j = 0; j < searchRows.length; j++) 
					{
						rePlates.add(searchRows[j]);
					}
				}
			}
			return rePlates.toArray(new DBRow[0]);
		
		}catch(Exception e){
			 throw new Exception("findLoadOrderPlatesInsertAndCheckExist"+e);
		}
	}
	
	/**
	 * 判断entry是否有托盘进出
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow checkEntryHasPlateInOrOut(long entry_id) throws Exception
	{
		try
		{
			DBRow result = new DBRow();
			int entryIn = floorCheckInMgrZyj.findPlateCountByEntryIdIn(entry_id);
			int entryOut = floorCheckInMgrZyj.findPlateCountByEntryIdOut(entry_id);
			result.add("entry_in_cn", entryIn);
			result.add("entry_out_cn", entryOut);
			return result;
		}catch(Exception e){
			 throw new Exception("checkEntryHasPlateInOrOut"+e);
		}
	}
	
	/**
	 * 根据icid更新导入container信息表
	 * @param icid
	 * @param row
	 * @throws Exception
	 */
	public void updateContainerInfoByIcid(long icid, DBRow row) throws Exception
	{
		try
		{
			this.floorCheckInMgrZyj.updateContainerInfoByIcid(icid, row);;
		}
		catch(Exception e)
		{
			 throw new Exception("updateContainerInfoByIcid"+e);
		}
	}
/**
	 * 从本地查询数据
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @param status_one
	 * @param adid
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadInfosLocalForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID, String status_one, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			String[] status = null;
			if(!StrUtil.isBlank(status_one))
			{
				status = new String[1];
				status[0] = status_one;
			}
			else
			{
				status = sqlServerMgrZJ.getStatusExceptClosed(WmsOrderTypeKey.PICKUP, adid, request);
						
			}
			DBRow[] localRows = new DBRow[0];
			if(!StringUtil.isBlank(order_no) && 0 != order_type)
			{
				if(WmsOrderTypeKey.LOAD_NO == order_type)
				{
					localRows = floorCheckInMgrZyj.findLoadInfosByLoadNo(order_no, CompanyID, CustomerID, status);
					//localRows.length 》 0 ，有masterBol
					if(localRows.length > 0)
					{
						for (int i = 0; i < localRows.length; i++) 
						{
							DBRow[] orderRows = floorCheckInMgrZyj.findOrderInfosByLoadId(localRows[i].get("load_id", 0L));
							for (int j = 0; j < orderRows.length; j++)
							{
								DBRow[] plateRows = floorCheckInMgrZyj.findPlateInfosByOrderId(orderRows[j].get("order_id", 0L));
								orderRows[j].add("order_plates", plateRows);
							}
							localRows[i].add("load_orders", orderRows);
						}
					}
					//通过loadno查询order表
					else
					{
						localRows = floorCheckInMgrZyj.findOrderInfosByLoadNo(order_no, CompanyID, CustomerID, status);
						for (int i = 0; i < localRows.length; i++) 
						{
							DBRow[] plateRows = floorCheckInMgrZyj.findPlateInfosByOrderId(localRows[i].get("order_id", 0L));
							localRows[i].add("order_plates", plateRows);
						}
					}
				}
				else if(WmsOrderTypeKey.MASTER_BOL_NO == order_type)
				{
					if(!StringUtil.isBlankAndCanParseLong(order_no))
					{
						localRows = floorCheckInMgrZyj.findLoadInfosByMasterBolNo(Long.parseLong(order_no), CompanyID, CustomerID, status);
						for (int i = 0; i < localRows.length; i++) 
						{
							DBRow[] orderRows = floorCheckInMgrZyj.findOrderInfosByLoadId(localRows[i].get("load_id", 0L));
							for (int j = 0; j < orderRows.length; j++)
							{
								DBRow[] plateRows = floorCheckInMgrZyj.findPlateInfosByOrderId(orderRows[j].get("order_id", 0L));
								orderRows[j].add("order_plates", plateRows);
							}
							localRows[i].add("load_orders", orderRows);
						}
					}
				}
				else if(WmsOrderTypeKey.ORDER == order_type)
				{
					if(!StringUtil.isBlankAndCanParseLong(order_no))
					{
						DBRow[] orderRows = floorCheckInMgrZyj.findOrderInfosByOrderNo(Long.parseLong(order_no), CompanyID, CustomerID, status);
						for (int j = 0; j < orderRows.length; j++)
						{
							DBRow[] plateRows = floorCheckInMgrZyj.findPlateInfosByOrderId(orderRows[j].get("order_id", 0L));
							orderRows[j].add("order_plates", plateRows);
						}
					}
				}
			}
			return localRows;
		}catch(Exception e){
			 throw new Exception("findLoadInfosLocalOrWmsForConsolidate"+e);
		}
	}
	
	//findLoadInfosAndInsertLocal
	//findOrdersInfoByOrderTypeForConsolidate
	/**
	 * 判断从本地或者wms查询数据
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @param status_one
	 * @param adid
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadInfosLocalOrWmsForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID, String status_one, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			DBRow[] localRows = findLoadInfosLocalForConsolidate(order_type, order_no, CompanyID, CustomerID, status_one, adid, request);
			if(localRows.length == 0)
			{
				localRows = findLoadInfosWmsAndInsertLocalForConsolidate(order_type, order_no, CompanyID, CustomerID, status_one, adid, request);
			}
//			//system.out.println(StringUtil.convertDBRowArrayToJsonString(localRows));
			return localRows;
		}
		catch(Exception e){
			 throw new Exception("findLoadInfosLocalOrWmsForConsolidate"+e);
		}
	}
	
	/**
	 * 通过plate_id删除Plate
	 * @param plate_id
	 * @throws Exception
	 */
	public void deleteOrderPlateInfoByPlateId(long plate_id) throws Exception
	{
		try
		{
			floorCheckInMgrZyj.deleteConsolidatePlateByOrderNoPlateId(plate_id);
		}
		catch(Exception e){
			 throw new Exception("deleteOrderPlateInfoByPlateId"+e);
		}
	}
	
	/**
	 * 更新托盘上的信息
	 * @param plate_id
	 * @param row
	 * @throws Exception
	 */
	public void updateOrderPlateInfoByPlateId(long plate_id, DBRow row) throws Exception
	{
		try
		{
			floorCheckInMgrZyj.updateConsolidatePlateByOrderNoPlateId(plate_id, row);
		}
		catch(Exception e){
			 throw new Exception("deleteOrderPlateInfoByPlateId"+e);
		}
	}
	
	/*****************************************************plate level product inventary*************************************************/
	/**
	 * 添加托盘货库存
	 * @param plate_no
	 * @param customer_id
	 * @param customer
	 * @param box_count
	 * @param staging_area_id
	 * @param staging_area_name
	 * @param associate_order_type
	 * @param associate_order
	 * @param receive_user
	 * @param receive_time
	 * @param receive_order_type
	 * @param receive_order
	 * @return
	 * @throws Exception
	 */
	public DBRow insertPlateLevelProductInventary(String plate_no, long customer_key, String customer_id, double box_count
			, long staging_area_id, int associate_order_type, long associate_order
			, long receive_user, String receive_time, int receive_order_type, long receive_order, double weight, long ps_id) throws Exception
	{
		try
		{
			DBRow result = new DBRow();
			DBRow[] searchRows = floorCheckInMgrZyj.findPlateByPlateNoReceiveTypeNo(receive_order_type, receive_order, plate_no);
			if(0 == searchRows.length)
			{
				DBRow row = new DBRow();
				row.add("plate_no", plate_no);
				row.add("customer_key", customer_key);
				row.add("customer_id", customer_id);
				row.add("box_count", box_count);
				row.add("associate_order_type", associate_order_type);
				row.add("associate_order", associate_order);
				row.add("staging_area_id", staging_area_id);
				row.add("receive_order_type", receive_order_type);
				row.add("receive_order", receive_order);
				if(!StrUtil.isBlank(receive_time))
				{
					row.add("receive_time", receive_time);
				}
				row.add("weight", weight);
				row.add("receive_user", receive_user);
				row.add("ps_id", ps_id);
				floorCheckInMgrZyj.insertPlateLevelProductInventary(row);
				result.add("message", PlateLevelProductInventaryExceptionKey.SUCCESS);
			}
			else
			{
				result.add("message", PlateLevelProductInventaryExceptionKey.PLATE_EXIST);
			}
			return result;
		}catch(Exception e){
			 throw new Exception("insertPlateLevelProductInventary"+e);
		}
	}
	
	/**
	 * 更新提货信息
	 * @param plate_inventary_id
	 * @param shipped_user
	 * @param shipped_time
	 * @param shipped_order_type
	 * @param shipped_order
	 * @throws Exception
	 */
	public void updatePlateLevelProductInventary(long plate_inventary_id
			, long shipped_user, String shipped_time, int shipped_order_type, long shipped_order)throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("shipped_user", shipped_user);
			row.add("shipped_time", shipped_time);
			row.add("shipped_order_type", shipped_order_type);
			row.add("shipped_order", shipped_order);
			floorCheckInMgrZyj.updatePlateLevelProductInventaryById(plate_inventary_id, row);
		}catch(Exception e){
			 throw new Exception("updatePlateLevelProductInventary"+e);
		}
	}
	
	/**
	 * 更新托盘位置
	 * @param statgingAreaId
	 * @throws Exception
	 */
	public void updatePlateLevelProductInventaryStaging(long plate_inventary_id,long statgingAreaId) throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("staging_area_id", statgingAreaId);
			floorCheckInMgrZyj.updatePlateLevelProductInventaryById(plate_inventary_id, row);
		}catch(Exception e){
			 throw new Exception("updatePlateLevelProductInventary"+e);
		}
	}
	
	//searchPlatesLevelInventary
	/**
	 * 查询托盘信息
	 * @param receiveType
	 * @param receiveOrder
	 * @param shippedType
	 * @param shippedOrder
	 * @param ps_id
	 * @param startTime
	 * @param endTime
	 * @param plateNo
	 * @param plate_inventary_id
	 * @param associateType
	 * @param associateOrder
	 * @param timeType
	 * @param plateStatus
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventary(int receiveType, long receiveOrder, int shippedType, long shippedOrder
			,long ps_id, String startTime, String endTime, String plateNo, long plate_inventary_id
			, int associateType, long associateOrder, int timeType, int plateStatus, PageCtrl pc)throws Exception
	{
		try
		{
			DBRow[] searchRows = floorCheckInMgrZyj.findPlateLevelProductInventarys(
					receiveType, receiveOrder, shippedType, shippedOrder
					, ps_id, startTime, endTime, plateNo, plate_inventary_id
					, associateType, associateOrder, timeType, plateStatus, pc);
			
			for (int i = 0; i < searchRows.length; i++)
			{
				DBRow searchRow = searchRows[i];
				long ps_id_plate = searchRow.get("ps_id", 0L);
				DBRow psRow = floorCheckInMgrZyj.findStorageById(ps_id_plate);
				String ps_title = "";
				if(null != psRow)
				{
					ps_title = psRow.getString("title");
				}
				searchRows[i].add("ps_title", ps_title);
				long staging_id = searchRow.get("staging_area_id", 0L);
				DBRow stagingRow = floorCheckInMgrZyj.findStagingAreaByID(staging_id);
				String staging_name = "";
				if(null != stagingRow)
				{
					staging_name = stagingRow.getString("location_name");
				}
				searchRows[i].add("staging_name", staging_name);
				long out = searchRows[i].get("shipped_order", 0L);
				if(out > 0){
					searchRows[i].add("status", "Shipped");
					searchRows[i].add("status_key", PlateStatusInOrOutBoundKey.SHIPPED);
				}else{
					searchRows[i].add("status", "OnHand");
					searchRows[i].add("status_key", PlateStatusInOrOutBoundKey.ON_HAND);
				}
				
				
				LinkedHashMap<String, String> receiveInfoMap = plateInventaryHandleReceiveInfo(searchRow.get("receive_order_type", 0), searchRow.get("receive_order", 0L), searchRows[i]);
				LinkedHashMap<String, String> shippedInfoMap = plateInventaryHandleShippedInfo(searchRow.get("shipped_order_type", 0), searchRow.get("shipped_order", 0L), searchRows[i]);;
				LinkedHashMap<String, String> associateMap   = plateInventaryHandleAssociateInfo(searchRow.get("associate_order_type", 0), searchRow.get("associate_order", 0L), searchRows[i]);
				searchRows[i].add("receive_info", receiveInfoMap);
				searchRows[i].add("shipped_info", shippedInfoMap);
				searchRows[i].add("associate_info", associateMap);
				
			}
			return searchRows;
		}catch(Exception e){
			 throw new Exception("findPlateLevelProductInventary"+e);
		}
	}
	
//	sql.append("select icp.plate_no, sll.location_name, ddi.finish_time finish_time_in, ddo.finish_time finish_time_out");
//	sql.append(" , ddi.dlo_id dlo_id_in, ddo.dlo_id dlo_id_out , dmi.ps_id ps_id_in, psi.title ps_title_in, icp.ship_entryd_id");
//	sql.append(" , ici.container_no ctnr, ici.po_no pono, ici.customer_id, icp.box_number
	//, ici.ic_id, ici.bol, ici.qty, ici.recevie_qty, ici.ship_out_date");
//	sql.append(" , dmi.gate_liscense_plate gate_liscense_plate_in, dmi.gate_driver_name gate_driver_name_in
	//, dmi.gate_driver_liscense gate_driver_liscense_in, dmi.company_name company_name_in");
//	sql.append(" , dmo.gate_liscense_plate gate_liscense_plate_out, dmo.gate_driver_name gate_driver_name_out
	//, dmo.gate_driver_liscense gate_driver_liscense_out, dmo.company_name company_name_out");
	/**
	 *  处理plate进的相关信息
	 * @param orderType
	 * @param order
	 * @param plate
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> plateInventaryHandleReceiveInfo(int orderType, long order, DBRow plate) throws Exception
	{
		LinkedHashMap<String,String> map = new LinkedHashMap<String, String>();
		switch(orderType)
		{
			case ModuleKey.CHECK_IN_ENTRY_DETAIL:
				DBRow row = findEntrySomeInfoByDetailId(order);
				String entry = row.getString("dlo_id")+"( I : "+row.getString("entry_in_cn")
						+" ,O : "+row.getString("entry_out_cn")+" )";
				map.put("Entry", entry);
				String receive_time = new TDate().getFormateTime(plate.getString("receive_time"), "yyyy-MM-dd HH:mm");
				map.put("DeliveryTime", receive_time);
				map.put("LP", row.getString("gate_liscense_plate"));
				map.put("DriverName", row.getString("gate_driver_name"));
				map.put("DriverLicense", row.getString("gate_driver_liscense"));
				map.put("Carrier", row.getString("company_name"));
				map.put("Storage", plate.getString("ps_title"));
				map.put("display_filed", "7");
				map.put("I", row.getString("entry_in_cn"));
				map.put("O", row.getString("entry_out_cn"));
				map.put("EntryID", row.getString("dlo_id"));
				map.put("Receive", row.getString("dlo_id"));
		}
		return map;
	}
	
	/**
	 *  处理plate出库的相关信息
	 * @param orderType
	 * @param order
	 * @param plate
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> plateInventaryHandleShippedInfo(int orderType, long order, DBRow plate) throws Exception
	{
		LinkedHashMap<String,String> map = new LinkedHashMap<String, String>();
		switch(orderType)
		{
			case ModuleKey.CHECK_IN_ENTRY_DETAIL:
				DBRow row = findEntrySomeInfoByDetailId(order);
				String entry = row.getString("dlo_id")+"( I : "+row.getString("entry_in_cn")
						+" ,O : "+row.getString("entry_out_cn")+" )";
				map.put("Entry", entry);
				String receive_time = new TDate().getFormateTime(plate.getString("Shipped_time"), "yyyy-MM-dd HH:mm");
				map.put("DeliveryTime", receive_time);
				map.put("LP", row.getString("gate_liscense_plate"));
				map.put("DriverName", row.getString("gate_driver_name"));
				map.put("DriverLicense", row.getString("gate_driver_liscense"));
				map.put("Carrier", row.getString("company_name"));
				map.put("display_filed", "6");
				map.put("I", row.getString("entry_in_cn"));
				map.put("O", row.getString("entry_out_cn"));
				map.put("EntryID", row.getString("dlo_id"));
				map.put("Shipped", row.getString("dlo_id"));
		}
		return map;
	}
	
	/**
	 *  处理plate相关信息
	 * @param orderType
	 * @param order
	 * @param plate
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> plateInventaryHandleAssociateInfo(int orderType, long order, DBRow plate) throws Exception
	{
		LinkedHashMap<String,String> map = new LinkedHashMap<String, String>();
		switch(orderType)
		{
			case ModuleKey.CHECK_IN_SAMSUNG_PLATE:
				DBRow row = findImportConatinerInfoByPlateId(order);
				String ctnr = row.getString("ctnr")+"( I : "+row.getString("ic_in_cn")
						+" ,O : "+row.getString("ic_out_cn")+" )";
				map.put("CTNR", ctnr);
				map.put("PONo", row.getString("pono"));
				map.put("Customer", plate.getString("customer_id"));
				map.put("BOL", row.getString("bol"));
				map.put("ExpectedQty", row.getString("qty"));
				map.put("ETD", row.getString("ship_out_date"));
				map.put("display_filed", "6");
				map.put("I", row.getString("ic_in_cn"));
				map.put("O", row.getString("ic_out_cn"));
				map.put("ctnr", row.getString("ctnr"));
				map.put("Associate", row.getString("ctnr")+" "+row.getString("bol"));
		}
		return map;
	}
	
	/**
	 * 通过entry明细id获取entry
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findEntrySomeInfoByDetailId(long dlo_detail_id) throws Exception
	{
		try
		{
			DBRow row = floorCheckInMgrZyj.findEntrySomeInfoByDetailId(dlo_detail_id);
			long dlo_id = 0L;
			if(null != row)
			{
				dlo_id = row.get("dlo_id", 0L);
			}
			//通过entry获取进出plateno
			int entryIn = floorCheckInMgrZyj.findPlateCountByEntryIdIn(dlo_id);
			int entryOut = floorCheckInMgrZyj.findPlateCountByEntryIdOut(dlo_id);
			row.add("entry_in_cn", entryIn);
			row.add("entry_out_cn", entryOut);
			return row;
		}catch(Exception e){
			 throw new Exception("findEntrySomeInfoByDetailId"+e);
		}
	}
	
	/**
	 * 通过导入的container的托盘id查询container信息
	 * @param icp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findImportConatinerInfoByPlateId(long icp_id) throws Exception
	{
		try
		{
			DBRow row = floorCheckInMgrZyj.findContainerInfoByIcpId(icp_id);
			long ic_id = 0L;
			if(null != row)
			{
				ic_id = row.get("ic_id", 0L);
			}
			int icIn = floorCheckInMgrZyj.findPlateCountByIcidIn(ic_id);
			int icOut = floorCheckInMgrZyj.findPlateCountByIcidOut(ic_id);
			row.add("ic_in_cn", icIn);
			row.add("ic_out_cn", icOut);
			return row;
		}catch(Exception e){
			 throw new Exception("findImportConatinerInfoByPlateId"+e);
		}
	}
	
	
	/**
	 * 通过主键查询：plate_level_product_inventary
	 * @param pi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findPlateLevelProductInventaryById(long pi_id) throws Exception
	{
		try
		{
			return floorCheckInMgrZyj.findPlateLevelProductInventaryById(pi_id);
		}catch(Exception e){
			 throw new Exception("findPlateLevelProductInventaryById"+e);
		}
	}
	
	
	/**
	 * 托盘商品库存索引
	 * @param search_key
	 * @param search_mode
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchPlateProductInventarys(String search_key,int search_mode,PageCtrl pc)throws Exception 
	{
		try 
		{
			if(!search_key.equals("")&&!search_key.equals("\""))
			{
				search_key = search_key.replaceAll("\"","");
				search_key = search_key.replaceAll("'","");
				search_key = search_key.replaceAll("\\*","");
			}
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			return PlateInventaryIndexMgr.getInstance().getSearchResults(search_key.toLowerCase(),search_mode,page_count,pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	
	/**
	 * 通过索引查询托盘信息
	 * @param receiveType
	 * @param receiveOrder
	 * @param shippedType
	 * @param shippedOrder
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryAllInfoByOrderType(int receiveType, long receiveOrder, int shippedType, long shippedOrder, PageCtrl pc)throws Exception
	{
		try
		{
			DBRow[] searchRows = findPlateLevelProductInventaryInfoByIndex(receiveType, receiveOrder, shippedType, shippedOrder, 2, pc);
			
			for (int i = 0; i < searchRows.length; i++)
			{
				DBRow searchRow = searchRows[i];
				long ps_id_plate = searchRow.get("ps_id", 0L);
				DBRow psRow = floorCheckInMgrZyj.findStorageById(ps_id_plate);
				String ps_title = "";
				if(null != psRow)
				{
					ps_title = psRow.getString("title");
				}
				searchRows[i].add("ps_title", ps_title);
				long staging_id = searchRow.get("staging_area_id", 0L);
				DBRow stagingRow = floorCheckInMgrZyj.findStagingAreaByID(staging_id);
				String staging_name = "";
				if(null != stagingRow)
				{
					staging_name = stagingRow.getString("location_name");
				}
				searchRows[i].add("staging_name", staging_name);
				long out = searchRows[i].get("shipped_order", 0L);
				if(out > 0){
					searchRows[i].add("status", "Shipped");
					searchRows[i].add("status_key", PlateStatusInOrOutBoundKey.SHIPPED);
				}else{
					searchRows[i].add("status", "OnHand");
					searchRows[i].add("status_key", PlateStatusInOrOutBoundKey.ON_HAND);
				}
				
				
				LinkedHashMap<String, String> receiveInfoMap = plateInventaryHandleReceiveInfo(searchRow.get("receive_order_type", 0), searchRow.get("receive_order", 0L), searchRows[i]);
				LinkedHashMap<String, String> shippedInfoMap = plateInventaryHandleShippedInfo(searchRow.get("shipped_order_type", 0), searchRow.get("shipped_order", 0L), searchRows[i]);;
				LinkedHashMap<String, String> associateMap   = plateInventaryHandleAssociateInfo(searchRow.get("associate_order_type", 0), searchRow.get("associate_order", 0L), searchRows[i]);
				searchRows[i].add("receive_info", receiveInfoMap);
				searchRows[i].add("shipped_info", shippedInfoMap);
				searchRows[i].add("associate_info", associateMap);
				
			}
			return searchRows;
		}catch(Exception e){
			 throw new Exception("findPlateLevelProductInventary"+e);
		}
	}

	/**
	 * 通过单据类型，查询库存
	 * @param receiveType
	 * @param receiveOrder
	 * @param shippedType
	 * @param shippedOrder
	 * @param associateType
	 * @param associateOrder
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryInfoByIndex(int receiveType, long receiveOrder
			, int shippedType, long shippedOrder, int ReceiveOrShipOrNot, PageCtrl pc)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			DBRow[] rows = new DBRow[0];
			sql.append("select pli.* from plate_level_product_inventary pli");
			if(receiveType == ModuleKey.CHECK_IN_ENTRY)
			{
				sql.append(" left join door_or_location_occupancy_details ddi on pli.receive_order = ddi.dlo_detail_id");
				sql.append(" and pli.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			}
			if(shippedType == ModuleKey.CHECK_IN_ENTRY)
			{
				sql.append(" left join door_or_location_occupancy_details ddo on pli.receive_order = ddo.dlo_detail_id");
				sql.append(" and pli.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			}
			//where 
			sql.append(" where 1=1");
			if(receiveType == ModuleKey.CHECK_IN_ENTRY)
			{
				if(1==ReceiveOrShipOrNot)
				{
					sql.append(" and (ddi.dlo_id = ").append(receiveOrder);
					
				}
				else if(2==ReceiveOrShipOrNot)
				{
					sql.append(" and ddi.dlo_id = ").append(receiveOrder);
				}
			}
			if(shippedType == ModuleKey.CHECK_IN_ENTRY)
			{
				if(1==ReceiveOrShipOrNot)
				{
					sql.append(" or ddo.dlo_id = ").append(shippedOrder).append(")");
				}
				else if(2==ReceiveOrShipOrNot)
				{
					sql.append(" and ddo.dlo_id = ").append(shippedOrder);
				}
			}
			
			return floorCheckInMgrZyj.findPlateLevelProductInventarysBySql(sql, pc);
		}
		catch(Exception e){
			 throw new Exception("findPlateLevelProductInventaryInfoByOrderType"+e);
		}
	}
	
	/**
	 * 通过关联信息更新托盘
	 * @param associateType
	 * @param associateOrder
	 * @param shipped_user
	 * @param shipped_time
	 * @param shipped_order_type
	 * @param shipped_order
	 * @param statgingAreaId
	 * @throws Exception
	 */
	public void updatePlateShippedInfoByAssociate(int associateType, long associateOrder
			, long shipped_user, String shipped_time, int shipped_order_type, long shipped_order, long statgingAreaId) throws Exception
	{
		try
		{
			if(ModuleKey.CHECK_IN_SAMSUNG_PLATE == associateType)
			{
				DBRow row = new DBRow();
				if(shipped_user > 0)
				{
					row.add("shipped_user", shipped_user);
				}
				if(!StrUtil.isBlank(shipped_time))
				{
					row.add("shipped_time", shipped_time);
				}
				if(shipped_order_type > 0 && shipped_order > 0)
				{
					row.add("shipped_order_type", shipped_order_type);
					row.add("shipped_order", shipped_order);
				}
				if(statgingAreaId > 0)
				{
					row.add("staging_area_id", statgingAreaId);
				}
				floorCheckInMgrZyj.updatePlateLevelProductInventaryByAssociate(associateType, associateOrder, row);
			}
		}
		catch(Exception e){
			 throw new Exception("updatePlateShippedInfoByAssociate"+e);
		}
	}
	
	/**
	 * 添加account到ShipTo中
	 * @throws Exception
	 */
	public void addAllWmsAccountIDToOSO() throws Exception
	{
		try
		{
			DBRow[] rows = sqlServerMgrZJ.findAllAccountIDs();
			for (int i = 0; i < rows.length; i++)
			{
				String AccountID = rows[i].getString("AccountID");
				DBRow accounts = floorShipToMgrZJ.getDetailShipTo(AccountID.toUpperCase());
						//floorCheckInMgrZyj.findAccountInfoByAccount(AccountID.toUpperCase());
				if(null == accounts)
				{
					DBRow accountRow = new DBRow();
					accountRow.add("ship_to_name", AccountID.toUpperCase());
					floorShipToMgrZJ.addShipTo(accountRow);
				}
			}
		}
		catch(Exception e){
			 throw new Exception("addAllWmsAccountIDToOSO"+e);
		}
	}
	
	
	/**
	 * 添加Supplier到title中
	 * @throws Exception
	 */
	public void addAllWmsSupplierIDToOSO() throws Exception
	{
		try
		{
			DBRow[] rows = sqlServerMgrZJ.findAllSupplierIDs();
			for (int i = 0; i < rows.length; i++)
			{
				String SupplierID = rows[i].getString("SupplierID").toUpperCase();
				DBRow title = proprietaryMgrZyj.findProprietaryByTitleName(SupplierID);
				if(title == null)
				{
					proprietaryMgrZyj.addProprietaryOrProprietaryAdmin(SupplierID.toUpperCase(), 0L);
				}
			}
		}
		catch(Exception e){
			 throw new Exception("addAllWmsSupplierIDToOSO"+e);
		}
	}
	
	/**
	 * 添加Customer到AccountID中
	 * @throws Exception
	 */
	public void addAllWmsCustomerIDToOSO() throws Exception
	{
		try
		{
			DBRow[] rows = sqlServerMgrZJ.findAllCustomerIDs();
			for (int i = 0; i < rows.length; i++)
			{
				String CustomerID = rows[i].getString("CustomerID");
//				//加张表添加account
				DBRow[] customers = floorCheckInMgrZyj.findCustomerInfoByCustomer(CustomerID.toUpperCase());
				
				//加张表添加account
//				DBRow[] accounts = floorCheckInMgrZyj.findAccountInfoByAccount(CustomerID.toUpperCase());
				
				if(customers.length == 0)
				{
					DBRow customerRow = new DBRow();
					customerRow.add("customer_id", CustomerID.toUpperCase());
					floorCheckInMgrZyj.insertCustomer(customerRow);
//					DBRow accountRow = new DBRow();
//					accountRow.add("account_id", CustomerID.toUpperCase());
//					floorCheckInMgrZyj.insertAccount(accountRow);
				}
			}
		}
		catch(Exception e){
			 throw new Exception("addAllWmsCustomerIDToOSO"+e);
		}
	}
	
	/**
	 * 通过shippedInfo更新托盘
	 * @param shippedOrderType
	 * @param shippedOrder
	 * @throws Exception
	 * @Date   2014年10月22日
	 */
	public void clearPlateLevelProductInventaryShipped(int shippedOrderType, long shippedOrder) throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("shipped_user", 0L);
			row.add("shipped_time", null);
			row.add("shipped_order_type", 0);
			row.add("shipped_order", 0L);
			floorCheckInMgrZyj.updatePlateLevelProductInventaryByShipped(shippedOrderType, shippedOrder, row);
		}
		catch(Exception e){
			 throw new Exception("clearPlateLevelProductInventaryShipped"+e);
		}
	}
	
	
	/**
	 * 通过entryin prupose查询设备
	 * @param equipment_purpose
	 * @param check_in_entry
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:07:11
	 */
	public DBRow[] checkInWindowFindEquipmentByEntryInIdpurpose(int equipment_purpose, long check_in_entry, int equipment_type) throws Exception
	{
		 try
		 {
			 EquipmentTypeKey equipmentTypeKey = new EquipmentTypeKey();
			 DBRow[] rows = floorCheckInMgrZyj.findEquipmentReOccupyByEntryInIdpurpose(equipment_purpose, check_in_entry, equipment_type);
			 for (int i = 0; i < rows.length; i++) {
				 rows[i].add("equipment_type_value", equipmentTypeKey.getEnlishEquipmentTypeValue(rows[i].get("equipment_type", 0)));
			}
			 return rows;
		 }
		 catch(Exception e){
			 throw new Exception("checkInWindowFindEquipmentByEntryInIdpurpose"+e);
		}
	}
	
	
	/**
	 * 通过entryOut prupose查询设备
	 * @param equipment_purpose
	 * @param check_in_entry
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:07:11
	 */
	public DBRow[] checkInWindowFindEquipmentByEntryOutIdpurpose(int equipment_purpose, long check_in_entry, int equipment_type) throws Exception
	{
		 try
		 {
			 return floorCheckInMgrZyj.findEquipmentReOccupyByEntryOutIdpurpose(equipment_purpose, check_in_entry, equipment_type);
		 }
		 catch(Exception e){
			 throw new Exception("checkInWindowFindEquipmentByEntryOutIdpurpose"+e);
		 }
	}
	
	
	/**
	 * 添加设备
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:13:10
	 */
	public long checkInWindowAddEquipment(HttpServletRequest request)  throws Exception
	{
		try
		{
			int equipment_type		= StringUtil.getInt(request, "equipment_type");
			long check_in_entry_id	= StringUtil.getLong(request, "check_in_entry_id");
			String equipment_number	= StringUtil.getString(request, "equipment_number");
			int equipment_purpose	= StringUtil.getInt(request, "equipment_purpose");
			String seal_delivery	= StringUtil.getString(request, "seal_delivery");
			String seal_pick_up		= StringUtil.getString(request, "seal_pick_up");
			
			DBRow row = new DBRow();
			row.add("equipment_type", equipment_type);
			row.add("check_in_entry_id", check_in_entry_id);
			row.add("check_in_time", DateUtil.NowStr());
			row.add("equipment_number", equipment_number);
			row.add("equipment_purpose", equipment_purpose);
			row.add("seal_delivery", seal_delivery);
			row.add("seal_pick_up", seal_pick_up);
			row.add("equipment_status", CheckInMainDocumentsStatusTypeKey.UNPROCESS);
			
			return floorCheckInMgrZyj.addEquipment(row);
		}
		catch(Exception e){
			throw new Exception("checkInWindowAddEquipment"+e);
		}
	}
	
	/**
	 * 更新设备，有task已经有明细，不能修改资源；修改设备处理task
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午6:48:56
	 */
	public DBRow updateEquipmentById(HttpServletRequest request) throws Exception
	{
		//PickUp设备是否是当前entry的其他用途设备
		long equipment_id		= StringUtil.getLong(request, "equipment_id");
		long check_in_entry_id	= StringUtil.getLong(request, "check_in_entry_id");
		int is_check_success	= StringUtil.getInt(request, "is_check_success");
		String equipment_number	= StringUtil.getString(request, "equipment_number");
		int equipment_purpose	= StringUtil.getInt(request, "equipment_purpose");
		boolean flag 			= false;
		DBRow equipment			= null;
		AdminMgr adminMgr 		= new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		long adid				= adminLoginBean.getAdid();
		String adidName			= adminLoginBean.getEmploye_name();
		long ps_id				= adminLoginBean.getPs_id();
		String resource_name	= "";
		if(equipment_id > 0)
		{
			equipment			= findEquipmentReOccupyById(equipment_id);
			//如果设备已经出去了，不能修改
			if(!StrUtil.isBlank(equipment.getString("check_out_time"))
//					|| equipment.get("equipment_status", 0)==CheckInMainDocumentsStatusTypeKey.INYARD
					|| equipment.get("equipment_status", 0)==CheckInMainDocumentsStatusTypeKey.LEFT)
				throw new EquipmentHadOutException("\""+equipment.getString("equipment_number")+"\"");
			long before_equipment_id= StringUtil.getLong(request, "before_equipment_id");
			resource_name= windowCheckInHandleOccupyResourceName(equipment.get("resources_type", 0), equipment.get("resources_id",0L));
			//如果是pickup添加设备
			//PickUp时验证设备号重不重，重的话要提示跟哪些重，要不要left那些设备
			DBRow sameInfo = windowCheckInSameCtnrPickUp(equipment_number, equipment_id,check_in_entry_id, adminLoginBean, is_check_success);
			if(null != sameInfo)
			{
				return sameInfo;
			}
			flag = windowCheckInEntryAddPickUpCtnr(equipment_id,before_equipment_id, check_in_entry_id, adid,adidName,resource_name, equipment_number, adminLoginBean.getPs_id(),equipment_purpose);
		}
		if(!flag)
		{
			int equipment_type		= StringUtil.getInt(request, "equipment_type");
			String seal_delivery	= StringUtil.getString(request, "seal_delivery");
			String seal_pick_up		= StringUtil.getString(request, "seal_pick_up");
			int occupy_type			= StringUtil.getInt(request, "occupy_type");
			long occupy_id			= StringUtil.getLong(request, "occupy_id");
			
			int occupy_status		= OccupyStatusTypeKey.RESERVERED;
			if(occupy_type == OccupyTypeKey.SPOT)
			{
				occupy_status = OccupyStatusTypeKey.OCUPIED;
			}
			seal_delivery = StrUtil.isBlank(seal_delivery)?"NA":seal_delivery;
			seal_pick_up = StrUtil.isBlank(seal_pick_up)?"NA":seal_pick_up;
			//entry主单据
			DBRow mRow=floorCheckInMgrZwb.findGateCheckInById(check_in_entry_id);
			DBRow row = new DBRow();
			row.add("equipment_type", equipment_type);
			row.add("equipment_number", equipment_number);
			row.add("equipment_purpose", equipment_purpose);
			row.add("seal_delivery", seal_delivery);
			row.add("seal_pick_up", seal_pick_up);
			row.add("phone_equipment_number", StringUtil.convertStr2PhotoNumber(equipment_number));
			CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
			
			if(0==equipment_id)
			{
				//TODO 处理设备number重复的问题，如果添加新的，需要把之前的left并释放资源
				DBRow equipmentNumberSameRow = windowCheckInSameCtnr(equipment_number, check_in_entry_id, adminLoginBean,null,is_check_success,"A");
				if(null != equipmentNumberSameRow)
				{
					return equipmentNumberSameRow;
				}
				row.add("check_in_entry_id", check_in_entry_id);
				row.add("equipment_status", CheckInMainDocumentsStatusTypeKey.UNPROCESS);
				row.add("ps_id", ps_id);
				resource_name= windowCheckInHandleOccupyResourceName(occupy_type, occupy_id);
				row.add("check_in_time", null!=mRow?mRow.getString("gate_check_in_time"):"");
				row.add("check_in_window_time",  !StrUtil.isBlank(mRow.getString("window_check_in_time"))?mRow.getString("window_check_in_time"):DateUtil.NowStr());
				equipment_id = floorCheckInMgrZyj.addEquipment(row);
				//TODO 如果是liveLoad设备处理所有设备
				windowCheckInHandleLiveLoadResources(check_in_entry_id, equipment_purpose, occupy_type, occupy_id, occupy_status, resource_name, adid);
				windowCheckInAddEquipmentLog("create", equipment_type, equipment_number, check_in_entry_id
						, adid, adidName, occupy_type, resource_name, seal_delivery, seal_pick_up, "");
				
			}
			else
			{
				//TODO 处理设备number重复的问题
				DBRow equipmentNumberSameRow = windowCheckInSameCtnr(equipment_number, check_in_entry_id, adminLoginBean, equipment,is_check_success,"U");
				if(null != equipmentNumberSameRow)
				{
					return equipmentNumberSameRow;
				}
//				windowCheckInCheckEquipmentDelete(equipment_id, check_in_entry_id, equipment);
				//设备之后占用门，不能修改门名称 
//				if(!equipment_number.equals(equipment.getString("equipment_number"))
//						&& equipment.get("occupy_status", 0)==OccupyStatusTypeKey.OCUPIED
//						&& equipment.get("resources_type", 0) == OccupyTypeKey.DOOR)
//					throw new EquipmentOccupyResourcesException();
				
				row.add("check_in_window_time",  !StrUtil.isBlank(mRow.getString("window_check_in_time"))?mRow.getString("window_check_in_time"):DateUtil.NowStr());
				if(equipment_purpose == CheckInLiveLoadOrDropOffKey.PICK_UP)
				{
					row.remove("equipment_type");
					row.remove("equipment_purpose");
					row.remove("equipment_number");
					row.remove("seal_delivery");
					row.remove("seal_pick_up");
					row.remove("phone_equipment_number");
				}
				if(equipment.get("resources_type", 0) == occupy_type)
				{
					occupy_status = equipment.get("occupy_status", 0);
				}
				floorCheckInMgrZyj.updateEquipment(equipment_id, row);
				//TODO 如果是liveLoad设备处理所有设备
				windowCheckInHandleLiveLoadResources(check_in_entry_id, equipment_purpose, occupy_type, occupy_id, occupy_status, resource_name, adid);
				windowCheckInAddEquipmentLog("update", equipment_type, equipment_number, check_in_entry_id
						, adid, adidName, occupy_type, resource_name, seal_delivery, seal_pick_up, "");
			}
			
			//更新entry下没有windowCheckIn时间的设备  	2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
	//		windowCheckInUpdateMainTimeEquipment(check_in_entry_id, adid);
			
			OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
			if(!ymsMgrAPI.resourceJudgeCanUse(ModuleKey.CHECK_IN, check_in_entry_id, occupy_type, occupy_id))
				throw new ResourceHasUsedException(occupyTypeKey.getOccupyTypeKeyName(occupy_type)+":"+resource_name);
			
			ymsMgrAPI.operationSpaceRelation(occupy_type, occupy_id, SpaceRelationTypeKey.Equipment, equipment_id
					, ModuleKey.CHECK_IN, check_in_entry_id, occupy_status, adid);
			//updateIndex
			checkInMgrZwb.editCheckInIndex(check_in_entry_id, "update");
		}
		return findEquipmentReOccupyById(equipment_id);
	}
	
	/**
	 * liveLoad的设备资源保持一致
	 * @param entry_id
	 * @param purpose
	 * @param resource_type
	 * @param resource_id
	 * @param occupy_status
	 * @param resource_name
	 * @param adid
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月20日 下午2:45:53
	 */
	public void windowCheckInHandleLiveLoadResources(long entry_id,int purpose, int resource_type, long resource_id, int occupy_status, String resource_name, long adid) throws Exception
	{
		if(purpose == CheckInLiveLoadOrDropOffKey.LIVE)
		{
			DBRow[] equipments = checkInWindowFindEquipmentByEntryInIdpurpose(purpose, entry_id, 0);
			for (int i = 0; i < equipments.length; i++)
			{
				if(equipments[i].get("equipment_status", 0) != CheckInMainDocumentsStatusTypeKey.LEFT){
					if(resource_id == 0 && equipments[i] != null && equipments[i].get("resources_id", 0L) != 0L)
					{
						ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipments[i].get("equipment_id", 0L), adid);
					}
					else
					{
						OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
						if(resource_id > 0)
						{
							if(!ymsMgrAPI.resourceJudgeCanUse(ModuleKey.CHECK_IN, entry_id, resource_type, resource_id))
								throw new ResourceHasUsedException(occupyTypeKey.getOccupyTypeKeyName(resource_type)+":"+resource_name);
							ymsMgrAPI.operationSpaceRelation(resource_type, resource_id, SpaceRelationTypeKey.Equipment, equipments[i].get("equipment_id",0L)
									, ModuleKey.CHECK_IN, entry_id, occupy_status, adid);
						}
					}
				}
				
			}
		}
	}
	
	
	/**
	 * 自动使设备离开，释放设备和task的资源
	 * @param equipmentRow
	 * @param adid
	 * @param adidName
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月16日 下午8:05:27
	 */
	public void windowCheckInAutoEquipmentLeftReleaseResources(DBRow equipmentRow,long adid, String adidName, long entry_id) throws Exception
	{
		OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
		CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
		//更新设备时间和状态
		DBRow updateRow = new DBRow();
		updateRow.add("check_out_time", DateUtil.NowStr());
		updateRow.add("equipment_status", CheckInMainDocumentsStatusTypeKey.LEFT);
		updateRow.add("check_out_entry_id", equipmentRow.get("check_in_entry_id", 0L));
		updateRow.add("patrol_lost", 1);
		floorCheckInMgrZyj.updateEquipment(equipmentRow.get("equipment_id", 0L), updateRow);
		// 要释放的资源记回task表
		checkInMgrZwb.checkOutReturnTheTask(equipmentRow.get("equipment_id", 0L));
		//  查看task的状态，如果为processing，unprocess。更新details表中的is_forget_task为true
		checkInMgrZwb.checkOutTaskStatus(false,adid,0,equipmentRow.get("check_in_entry_id",0L),entry_id,equipmentRow.get("equipment_id", 0L));
		
//		boolean isAndroid ,long adid, HttpServletRequest request, long isSearch,long check_in_entry_id, long entry_id, long equipment_id
		// 调用释放资源方法
		ymsMgrAPI.checkInWindowEquipmentTaskReleaseResources(equipmentRow.get("check_in_entry_id",0L), equipmentRow.get("equipment_id", 0L), adid);
		
		//** 记录equipment checkout日志   wfh
		String equipment_log = equipmentRow.get("equipment_type_value", "")+": 【"+equipmentRow.get("equipment_number", "")+"】";
		equipment_log += ",-Release:"+occupyTypeKey.getOccupyTypeKeyName(equipmentRow.get("resources_type", 0))+"【"+equipmentRow.get("resources_name", "")+"】";
		equipment_log += ",-Check Out By: E"+entry_id;
		checkInMgrZwb.addCheckInLog(adid, "Check Out Equipment", equipmentRow.get("check_in_entry_id", 0L), CheckInLogTypeKey.GATEOUT, DateUtil.NowStr(),equipment_log);
		
		
		//给添加设备的entry添加日志   wfh
		String equipmentLog = "Trailer【"+equipmentRow.get("equipment_number", "")+"】";
		equipmentLog += ",-Release:"+occupyTypeKey.getOccupyTypeKeyName(equipmentRow.get("resources_type", 0))+"【"+equipmentRow.get("resources_name", "")+"】";
		equipmentLog += ",-Check Out Entry: E"+equipmentRow.get("entry_id", 0L);
		checkInMgrZwb.addCheckInLog(adid, "Check Out Equipment", entry_id, CheckInLogTypeKey.WINDOW, DateUtil.NowStr(),equipmentLog);
	}
	
	/**
	 * 验证设备名称是否存在
	 * @param equipment_number
	 * @param check_in_entry_id
	 * @param adminLoginBean
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月16日 下午3:28:57
	 */
	public DBRow windowCheckInSameCtnr(String equipment_number, long check_in_entry_id, AdminLoginBean adminLoginBean,DBRow requipmentPre,int is_check_success,String au) throws Exception
	{
		long adid		= adminLoginBean.getAdid();
		String adidName = adminLoginBean.getEmploye_name();
		DBRow result = new DBRow();
		List<DBRow> equipmentNotThisEntry = new ArrayList<DBRow>();
		List<DBRow> equipmentYesThisEntry = new ArrayList<DBRow>();
		DBRow[] inYardSameCntr = findInYardDropEquipmentsByNumber(equipment_number, adminLoginBean.getPs_id(),CheckInTractorOrTrailerTypeKey.TRAILER);
		for (int i = 0; i < inYardSameCntr.length; i++)
		{
			DBRow rowEq = new DBRow();
			rowEq.add("equipment_id", inYardSameCntr[i].get("equipment_id", 0L));
			rowEq.add("equipment_number", inYardSameCntr[i].getString("equipment_number"));
			EquipmentTypeKey equipmentTypeKey = new EquipmentTypeKey();
			rowEq.add("equipment_type_value", equipmentTypeKey.getEnlishEquipmentTypeValue(inYardSameCntr[i].get("equipment_type", 0)));
			
			int re_type = inYardSameCntr[i].get("resources_type", 0);
			OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
			rowEq.add("resources_name","");
			if(re_type == OccupyTypeKey.DOOR)
			{
				rowEq.add("resources_name", inYardSameCntr[i].getString("doorId"));
			}
			else if(re_type == OccupyTypeKey.SPOT)
			{
				rowEq.add("resources_name", inYardSameCntr[i].getString("yc_no"));
			}
			rowEq.add("resources_type", re_type);
			rowEq.add("resources_type_value", occupyTypeKey.getOccupyTypeKeyName(re_type));
			OccupyStatusTypeKey occupyStatusTypeKey = new OccupyStatusTypeKey();
			rowEq.add("occupy_status_value", occupyStatusTypeKey.getOccupyStatusValue(inYardSameCntr[i].get("occupy_status",0)));
			rowEq.add("entry_id", inYardSameCntr[i].get("check_in_entry_id", 0L));
			rowEq.add("check_in_entry_id", inYardSameCntr[i].get("check_in_entry_id", 0L));
			rowEq.add("seal_delivery", inYardSameCntr[i].getString("seal_delivery"));
			rowEq.add("seal_pick_up", inYardSameCntr[i].getString("seal_pick_up"));
			rowEq.add("equipment_type", inYardSameCntr[i].get("equipment_type",0));

			if(inYardSameCntr[i].get("check_in_entry_id", 0L) != check_in_entry_id)
			{
				equipmentNotThisEntry.add(rowEq);
			}
			else
			{
				if(au.equals("A"))
				{
					equipmentYesThisEntry.add(rowEq);
				}
				else
				{
					if(requipmentPre.get("equipment_id", 0L) != inYardSameCntr[i].get("equipment_id", 0L))
					{
						equipmentYesThisEntry.add(rowEq);
					}
				}
			}
		}
		result.add("equipment_this_entry", equipmentYesThisEntry.toArray(new DBRow[0]));
		result.add("equipment_not_entry", equipmentNotThisEntry.toArray(new DBRow[0]));
		result.add("equipment_this_entry_len", equipmentYesThisEntry.size());
		result.add("equipment_not_entry_len", equipmentNotThisEntry.size());
		if(result.get("equipment_this_entry_len", 0) > 0)
		{
			throw new EquipmentSameToEntryException();
		}
		if(result.get("equipment_this_entry_len", 0) > 0 || result.get("equipment_not_entry_len", 0) > 0)
		{
			result.add("flag", YesOrNotKey.NO);
		}
		else
		{
			result.add("flag", YesOrNotKey.YES);
		}
		
		
		if(result.get("flag", 0) == YesOrNotKey.NO)
		{
			//将其他ctnr left
			if(is_check_success == YesOrNotKey.YES)
			{
				DBRow[] equipmentSames = (DBRow[])result.get("equipment_not_entry", new DBRow[0]);
				for (int i = 0; i < equipmentSames.length; i++) {
					windowCheckInAutoEquipmentLeftReleaseResources(equipmentSames[i],adid,adidName, check_in_entry_id);
				}
				result = windowCheckInSameCtnr(equipment_number, check_in_entry_id, adminLoginBean,requipmentPre,is_check_success,au);
				if(null!=result&&result.get("flag", 0) == YesOrNotKey.NO)
				{
					if(result.get("equipment_this_entry_len", 0) > 0){
						String ctnr_same = ((DBRow[])result.get("equipment_this_entry", new DBRow[0]))[0].getString("equipment_number");
						if("U".equals(au) && !ctnr_same.equals(requipmentPre.getString("equipment_number"))){
							return result;
						}
						else if("A".equals(au))
						{
							return result;
						}
					}
				}
			}
			else
			{
				return result;
			}
		}
		
		
//		//system.out.println("len:"+result.get("equipment_this_entry_len", 0) +","+ result.get("equipment_not_entry_len", 0)+","+result.get("flag", 0));
		
		return null;
	}
	
	/**
	 * 验证pickUpCtnr，释放资源
	 * @param number
	 * @param equipment_id
	 * @param adminLoginBean
	 * @param is_check_success
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月16日 下午10:25:31
	 */
	public DBRow windowCheckInSameCtnrPickUp(String number,long equipment_id,long entry_id, AdminLoginBean adminLoginBean, int is_check_success) throws Exception
	{
		long adid		= adminLoginBean.getAdid();
		String adidName = adminLoginBean.getEmploye_name();
		DBRow result = new DBRow();
		List<DBRow> equipmentNotThisEquipment = new ArrayList<DBRow>();
		DBRow[] inYardSameCntr = findInYardDropEquipmentsByNumber(number, adminLoginBean.getPs_id(),CheckInTractorOrTrailerTypeKey.TRAILER);
		for (int i = 0; i < inYardSameCntr.length; i++)
		{
			DBRow rowEq = new DBRow();
			rowEq.add("equipment_id", inYardSameCntr[i].get("equipment_id", 0L));
			rowEq.add("equipment_number", inYardSameCntr[i].getString("equipment_number"));
			EquipmentTypeKey equipmentTypeKey = new EquipmentTypeKey();
			rowEq.add("equipment_type_value", equipmentTypeKey.getEnlishEquipmentTypeValue(inYardSameCntr[i].get("equipment_type", 0)));
			
			int re_type = inYardSameCntr[i].get("resources_type", 0);
			OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
			rowEq.add("resources_name","");
			if(re_type == OccupyTypeKey.DOOR)
			{
				rowEq.add("resources_name", inYardSameCntr[i].getString("doorId"));
			}
			else if(re_type == OccupyTypeKey.SPOT)
			{
				rowEq.add("resources_name", inYardSameCntr[i].getString("yc_no"));
			}
			rowEq.add("resources_type", re_type);
			rowEq.add("resources_type_value", occupyTypeKey.getOccupyTypeKeyName(re_type));
			OccupyStatusTypeKey occupyStatusTypeKey = new OccupyStatusTypeKey();
			rowEq.add("occupy_status_value", occupyStatusTypeKey.getOccupyStatusValue(inYardSameCntr[i].get("occupy_status",0)));
			rowEq.add("entry_id", inYardSameCntr[i].get("check_in_entry_id", 0L));
			rowEq.add("check_in_entry_id", inYardSameCntr[i].get("check_in_entry_id", 0L));
			rowEq.add("seal_delivery", inYardSameCntr[i].getString("seal_delivery"));
			rowEq.add("seal_pick_up", inYardSameCntr[i].getString("seal_pick_up"));
			rowEq.add("equipment_type", inYardSameCntr[i].get("equipment_type",0));	//zhangrui 2015-01-16 添加 equipment_type

			
			if(inYardSameCntr[i].get("equipment_id", 0L) != equipment_id
					&& inYardSameCntr[i].get("check_in_entry_id", 0L) != entry_id)
			{
				equipmentNotThisEquipment.add(rowEq);
			}
		}
		result.add("equipment_not_entry", equipmentNotThisEquipment.toArray(new DBRow[0]));
		result.add("equipment_not_entry_len", equipmentNotThisEquipment.size());
		if(result.get("equipment_not_entry_len", 0) > 0)
		{
			result.add("flag", YesOrNotKey.NO);
		}
		else
		{
			result.add("flag", YesOrNotKey.YES);
		}
		
		if(result.get("flag", 0) == YesOrNotKey.NO)
		{
			//将其他ctnr left
			if(is_check_success == YesOrNotKey.YES)
			{
				DBRow[] equipmentSames = (DBRow[])result.get("equipment_not_entry", new DBRow[0]);
				for (int i = 0; i < equipmentSames.length; i++) {
					if(equipmentSames[i].get("check_in_entry_id", 0L) != entry_id)
					windowCheckInAutoEquipmentLeftReleaseResources(equipmentSames[i],adid,adidName,entry_id);
				}
				result = windowCheckInSameCtnrPickUp(number, equipment_id,entry_id, adminLoginBean,is_check_success);
				if(null!=result && result.get("flag", 0) == YesOrNotKey.NO)
				{
					return result;
				}
			}
			else
			{
				return result;
			}
		}
		return null;
	}
	
	/**判断是不是pickup ctnr，是更改check_out_entry_id
	 * @param equipment_id
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月29日 下午2:57:39
	 */
	public boolean windowCheckInEntryAddPickUpCtnr(long equipment_id,long before_equipment_id, long entry_id, long adid, String adidName
			,String resource_name, String equipment_number, long ps_id, int equipment_purpose) throws Exception
	{
		//PickUp设备，换设备的问题
		if(equipment_id != 0)
		{
			DBRow equipment = findEquipmentReOccupyById(equipment_id);
			if(before_equipment_id != 0 && equipment_id != before_equipment_id)
			{
				DBRow equipment_before = findEquipmentReOccupyById(before_equipment_id);
				DBRow row = new DBRow();
				row.add("check_out_entry_id", null);
				floorCheckInMgrZyj.updateEquipment(before_equipment_id, row);
				windowCheckInAddEquipmentLog("update", equipment_before.get("equipment_type", 0), equipment_before.getString("equipment_number"), entry_id
						, adid, adidName, equipment_before.get("resources_type", 0), resource_name, equipment_before.getString("seal_delivery")
						, equipment_before.getString("seal_pick_up"), " Cancel PickUp CTNR : "+equipment_before.getString("equipment_number"));
				windowCheckInAddEquipmentLog("update", equipment_before.get("equipment_type", 0), equipment_before.getString("equipment_number"), equipment_before.get("check_in_entry_id", 0L)
						, adid, adidName, equipment_before.get("resources_type", 0), resource_name, equipment_before.getString("seal_delivery")
						, equipment_before.getString("seal_pick_up"), " Cancel PickUp CTNR : "+equipment_before.getString("equipment_number")+" By "+entry_id);
				//更新entry下没有windowCheckIn时间的设备				2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
	//			windowCheckInUpdateMainTimeEquipment(entry_id, adid);
				if(before_equipment_id == 0)
				return true;
			}
			long equip_in_entry = equipment.get("check_in_entry_id", 0L);
			if(equip_in_entry != entry_id)
			{
				DBRow[] equipmentInYards = findInYardDropEquipmentsByNumber(equipment_number, ps_id,CheckInTractorOrTrailerTypeKey.TRAILER);
				if(equipmentInYards.length == 0)
				{
					throw new EquipmentNotFindException();
				}
				
				long equip_out_entry = equipment.get("check_out_entry_id", 0L);
				if(equip_out_entry == 0 || equip_out_entry == entry_id)
				{
					//验证设备是否可以带走
					DBRow row = new DBRow();
					row.add("check_out_entry_id", entry_id);
					floorCheckInMgrZyj.updateEquipment(equipment_id, row);
					windowCheckInAddEquipmentLog("update", equipment.get("equipment_type", 0), equipment.getString("equipment_number"), entry_id
							, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery")
							, equipment.getString("seal_pick_up"), " PickUp CTNR : "+equipment.getString("equipment_number"));
					
					windowCheckInAddEquipmentLog("update", equipment.get("equipment_type", 0), equipment.getString("equipment_number"), equip_in_entry
							, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery")
							, equipment.getString("seal_pick_up"), " PickUp CTNR : "+equipment.getString("equipment_number")+" By "+entry_id);
					
					//更新entry下没有windowCheckIn时间的设备			2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
			//		windowCheckInUpdateMainTimeEquipment(entry_id, adid);		
					return true;
				}
//				else
//				{
//					throw new EquipmentHasDecidedPickedException(equipment.get("check_out_entry_id", 0L)+"");
//				}
			}
//			else
//			{
//				throw new EquipmentPickUpNotInThisEntryException();
//			}
		}
		return false;
	}
	
	
	/**
	 * 删除设备及关系
	 * @param id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午9:59:12
	 */
	public int deleteEquipmentById(HttpServletRequest request) throws Exception
	{
		int isCheckSuccess = StringUtil.getInt(request, "is_check_success");
		long equipment_id = StringUtil.getLong(request, "id");
		DBRow equipment = findEquipmentReOccupyById(equipment_id);
		long entry_id = equipment.get("check_in_entry_id", 0L);
		if(!StrUtil.isBlank(equipment.getString("check_out_time"))
				|| equipment.get("equipment_status", 0)==CheckInMainDocumentsStatusTypeKey.INYARD
				|| equipment.get("equipment_status", 0)==CheckInMainDocumentsStatusTypeKey.LEFT) 
			throw new EquipmentHadOutException("\""+equipment.getString("equipment_number")+"\"");
		
		AdminMgr adminMgr		= new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		long adid				= adminLoginBean.getAdid();
		String adidName			= adminLoginBean.getEmploye_name();
		if(equipment.get("check_out_entry_id", 0L) != 0 && equipment.get("check_in_entry_id", 0L) != equipment.get("check_out_entry_id", 0L))
		{
			String resource_name = windowCheckInHandleOccupyResourceName(equipment.get("resources_type", 0), equipment.get("resources_id",0L));
			DBRow row = new DBRow();
			row.add("check_out_entry_id", null);
			floorCheckInMgrZyj.updateEquipment(equipment_id, row);
			windowCheckInAddEquipmentLog("update", equipment.get("equipment_type", 0), equipment.getString("equipment_number"), equipment.get("check_in_entry_id", 0L)
					, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery")
					, equipment.getString("seal_pick_up"), " Cancel PickUp CTNR : "+equipment.getString("equipment_number") +" By "+entry_id);
			windowCheckInAddEquipmentLog("update", equipment.get("equipment_type", 0), equipment.getString("equipment_number"), entry_id
					, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery")
					, equipment.getString("seal_pick_up"), " Cancel PickUp CTNR : "+equipment.getString("equipment_number"));
			return YesOrNotKey.YES;
		}
		if(isCheckSuccess != YesOrNotKey.YES)
		{
			//在不能删除的时候，判断是否能删除，能删除就删除
			isCheckSuccess = windowCheckInCheckEquipmentDelete(equipment_id, equipment.get("check_in_entry_id", 0L), equipment);
		}
		if(isCheckSuccess == YesOrNotKey.YES)
		{
			//如果是本单据添加的设备可以删除，否则不能删除。非本单据的设备，将out_entry处理
			String resource_name = windowCheckInHandleOccupyResourceName(equipment.get("resources_type", 0), equipment.get("resources_id",0L));
			windowCheckInAddEquipmentLog("delete", equipment.get("equipment_type", 0), equipment.getString("equipment_number"), equipment.get("check_in_entry_id", 0L)
					, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery")
					, equipment.getString("seal_pick_up"), "");
			ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipment_id, adid);
			DBRow[] tasks = floorCheckInMgrZyj.findTasksByEquipmentId(equipment_id);
			if(tasks.length > 0)
			{
				for (int i = 0; i < tasks.length; i++)
				{
					long detail_id = tasks[i].get("dlo_detail_id", 0L);
					ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Task, detail_id, adid);
					//处理是否删除业务单据
					orderSystemMgr.deleteOrNotOrderSystemByDetailInfos(detail_id);
					detCheckinWindow(request, detail_id);
				}
			}
			floorCheckInMgrZyj.deleteEquipment(equipment_id);
			CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
			checkInMgrZwb.editCheckInIndex(entry_id, "update");
		}
		//更新entry下没有windowCheckIn时间的设备			2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
	//	windowCheckInUpdateMainTimeEquipment(entry_id, adid);
		return isCheckSuccess;
	}
	
	/**
	 * 更新主单据和设备的windowCheckIn时间   2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
	 * @param entry_id
	 * @param adid
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月13日 下午8:00:16
	 */
	public void windowCheckInUpdateMainTimeEquipment(long entry_id, long adid) throws Exception
	{
//		if(whetherWriteWindowCheckInTime(entry_id))xujia  只要window操作过
//		{
			DBRow mRow		= floorCheckInMgrZwb.findGateCheckInById(entry_id);
			DBRow mainRow = new DBRow();
			//更新entry下没有windowCheckIn时间的设备
			DBRow equipmentEntry = new DBRow();
			if(mRow.getString("window_check_in_time").equals("")){
				mainRow.add("window_check_in_time",DateUtil.NowStr());
				mainRow.add("window_check_in_operator",adid);
				equipmentEntry.add("check_in_window_time", DateUtil.NowStr());
			}
			mainRow.add("window_check_in_operate_time",DateUtil.NowStr());
			floorCheckInMgrZwb.modCheckIn(entry_id,mainRow);
			
			floorCheckInMgrZyj.updateEntryEquipmentNoWindowCheckInTime(equipmentEntry, entry_id);
//		}
	}	
	
	/**
	 * 删除task，同时删除window和warehouse任务
	 * @param request
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月29日 下午6:03:52
	 */
	public long detCheckinWindow(HttpServletRequest request,long detail_id)throws Exception
	{
	    floorCheckInMgrZwb.detOccupancyDetails(detail_id);//删除子单据
		//删除 该单据的window任务
		DBRow[] shRow=this.floorCheckInMgrZwb.selectSchedule(detail_id, ProcessKey.CHECK_IN_WINDOW);
		if(shRow.length>0 && shRow!=null){
			for(int i=0;i<shRow.length;i++){
				long schedule_id=shRow[i].get("schedule_id", 0l);
				this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, request);
			}
		}
		//删除warehouse任务
		DBRow[] ware1 = floorCheckInMgrZwb.selectSchedule(detail_id, ProcessKey.CHECK_IN_WAREHOUSE);
		for (int i = 0; i < ware1.length; i++) {
			long schedule_id=ware1[i].get("schedule_id", 0l);
			this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, request);
		}
		DBRow[] ware2 = floorCheckInMgrZwb.selectSchedule(detail_id, ProcessKey.CHECK_IN_ANDROID_WAREHOUSE);
		for (int i = 0; i < ware2.length; i++) {
			long schedule_id=ware2[i].get("schedule_id", 0l);
			this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, request);
		}
		//gate schedule
		DBRow[] gate = floorCheckInMgrZwb.selectSchedule(detail_id, ProcessKey.GateHasTaskNotifyWindow);
		for (int i = 0; i < gate.length; i++) {
			long schedule_id=gate[i].get("schedule_id", 0l);
			this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, request);
		}
	   return 01;
   }
	
	
	/**
	 * 删除task，同时删除window和warehouse任务
	 * @param request
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月29日 下午6:03:52
	 */
	public long deleteTaskAndScheduleLog(AdminLoginBean adminLoginBean,long detail_id)throws Exception
	{
	   floorCheckInMgrZwb.detOccupancyDetails(detail_id);//删除子单据
		//删除 该单据的window任务
		DBRow[] shRow=this.floorCheckInMgrZwb.selectSchedule(detail_id, ProcessKey.CHECK_IN_WINDOW);
		if(shRow.length>0 && shRow!=null){
			for(int i=0;i<shRow.length;i++){
				long schedule_id=shRow[i].get("schedule_id", 0l);
				this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, adminLoginBean);
			}
		}
		//删除warehouse任务
		DBRow[] ware1 = floorCheckInMgrZwb.selectSchedule(detail_id, ProcessKey.CHECK_IN_WAREHOUSE);
		for (int i = 0; i < ware1.length; i++) {
			long schedule_id=ware1[i].get("schedule_id", 0l);
			this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, adminLoginBean);
		}
		DBRow[] ware2 = floorCheckInMgrZwb.selectSchedule(detail_id, ProcessKey.CHECK_IN_ANDROID_WAREHOUSE);
		for (int i = 0; i < ware2.length; i++) {
			long schedule_id=ware2[i].get("schedule_id", 0l);
			this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, adminLoginBean);
		}
		DBRow[] shRows1=floorCheckInMgrZwb.selectSchedule(detail_id, ProcessKey.GateNotifyWareHouse);
		if(shRows1.length>0 && shRows1!=null){
			scheduleMgrZr.deleteScheduleByScheduleId(shRows1[0].get("schedule_id", 0l), adminLoginBean);
		}
		//gate schedule
		DBRow[] gate = floorCheckInMgrZwb.selectSchedule(detail_id, ProcessKey.GateHasTaskNotifyWindow);
		for (int i = 0; i < gate.length; i++) {
			long schedule_id=gate[i].get("schedule_id", 0l);
			this.scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, adminLoginBean);
		}
	   return 01;
   }
	
	/**
	 *TODO 验证是否可以删除
	 * @param equipment_id
	 * @param entry_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月29日 上午9:35:39
	 */
	public int windowCheckInCheckEquipmentDelete(long equipment_id, long entry_id, DBRow equipment) throws Exception
	{
		DBRow[] tasks = floorCheckInMgrZyj.findTasksByEquipmentId(equipment_id);
		if(tasks.length > 0)
		{
			for (int i = 0; i < tasks.length; i++)
			{
				long detail_id = tasks[i].get("dlo_detail_id", 0L);
				int loadDetailCount = floorCheckInMgrZwb.loadDoneConut(detail_id);
				if(loadDetailCount > 0)
				{
					throw new EquipmentHasLoadedOrdersException();
				}
			}
			return YesOrNotKey.NO;
		}
		else
		{
			return YesOrNotKey.YES;
		}
	}
	
	
	/**
	 * 通过entry查询设备列表
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月26日 下午8:37:22
	 */
	public DBRow[] windowCheckInFindEquipments(long entry_id) throws Exception
	{
		try
		{
			DBRow[] rows = null;
			DBRow[] t1 = checkInWindowFindEquipmentByEntryInIdpurpose(CheckInLiveLoadOrDropOffKey.LIVE, entry_id, CheckInTractorOrTrailerTypeKey.TRAILER);
			DBRow[] t2 = checkInWindowFindEquipmentByEntryInIdpurpose(CheckInLiveLoadOrDropOffKey.DROP, entry_id, CheckInTractorOrTrailerTypeKey.TRAILER);
			DBRow[] t3 = checkInWindowFindEquipmentByEntryInIdpurpose(0, entry_id, CheckInTractorOrTrailerTypeKey.TRACTOR);
			if(t1.length > 0 || t2.length > 0)
			{
				rows = new DBRow[t1.length+t2.length];
				System.arraycopy(t1, 0, rows, 0, t1.length);
				System.arraycopy(t2, 0, rows, t1.length, t2.length);
			}
			if(rows == null || rows.length == 0)
			{
				rows = t3;
			}
			return rows;
		}
		catch(Exception e){
			throw new Exception("deleteEquipmentById"+e);
		}
	}
	
	
	/**
	 * 通过entryId查询entry的task所在的门或停车位
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月26日 下午9:09:45
	 */
	public DBRow[] findTasksOccupysEquipmentsByEntryId(long entry_id)throws Exception
	{
		try
		{
			CheckInChildDocumentsStatusTypeKey statusKey = new CheckInChildDocumentsStatusTypeKey();
			EquipmentTypeKey equipmentTypeKey = new EquipmentTypeKey();
			DBRow[] rows = floorCheckInMgrZyj.findTasksOccupysEquipmentsByEntryId(entry_id);
			for (int i = 0; i < rows.length; i++) {
				rows[i].add("number_status_value", statusKey.getContainerTypeKeyValue(rows[i].get("number_status", 0)));
				if(!StrUtil.isBlank(rows[i].get("appointment_date", ""))){
					rows[i].add("appointment_date", DateUtil.showLocalTime(rows[i].get("appointment_date", ""), rows[i].get("ps_id", 0l)));

				}
				rows[i].add("equipment_type_value", rows[i].get("equipment_type", 0)>0?equipmentTypeKey.getEnlishEquipmentTypeValue(rows[i].get("equipment_type", 0)):"");
				if(rows[i].get("srr_id", 0L) == 0L)
				{
					int occupy_type = rows[i].get("occupancy_type", 0);
					long occupy_id	= rows[i].get("rl_id", 0L);
					String occupy_name = windowCheckInHandleOccupyResourceName(occupy_type, occupy_id);
					rows[i].add("occupy_name", occupy_name);
				}
				//查询通知
				CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
				DBRow[] notices = checkInMgrZwb.windowFindSchedule(rows[i].get("dlo_detail_id", 0L), new int[]{ProcessKey.CHECK_IN_WINDOW,ProcessKey.GateNotifyWareHouse});
				String note = "";
	 			String notice_name = "";
	 			String notice_ids = "";
	 			int email=0;
	 			int message=0;
	 			int page=0;
				if(notices.length > 0)
				{
					email=notices[0].get("sms_email_notify", 0);
		 			message=notices[0].get("sms_short_notify", 0);
		 			page=notices[0].get("schedule_is_note", 0);
		 			String[] notes = notices[0].getString("schedule_detail").split("Note :");
		 			
		 			if(notes.length > 1)
		 			{
		 				note = notes[1];
		 				if(note.startsWith(" "))
		 				{
		 					note = note.substring(1);
		 				}
		 			}
					for (int j = 0; j < notices.length; j++) {
							notice_name += (","+notices[j].getString("employe_name"));
			 				notice_ids  += (","+notices[j].getString("adids"));
					}
				}
//				//system.out.println("noticeIds:"+notice_ids);
//				//system.out.println("notice_name:"+notice_name);
				
				notice_ids = notice_ids.length()>0?notice_ids.substring(1):"";
				notice_name = notice_name.length()>0?notice_name.substring(1):"";
				rows[i].add("email", email);
				rows[i].add("message", message);
				rows[i].add("page", page);
				rows[i].add("note", note);
				rows[i].add("notice_names", notice_name);
				rows[i].add("notice_ids", notice_ids);
				rows[i].add("zone_id",rows[i].get("area_id", 0L));
			}
			return rows;
		}
		catch(Exception e){
			throw new Exception("findTasksOccupysEquipmentsByEntryId"+e);
		}
	}
	
	
	/**
	 * windowCheckIn修改信息
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月26日 下午9:16:40
	 */
	public void windowCheckInMain(final HttpServletRequest request)throws Exception
	{
		sqlServerTransactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(
					TransactionStatus txStat) {
				try
				{
					String str				= StringUtil.getString(request,"str");//添加或修改的子单据数据
					JSONObject json			= new JSONObject(str);
					//获得当前登录账号信息,操作时间
					AdminMgr adminMgr		= new AdminMgr();
					AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
					//处理task
					windowCheckInHandleTasks(json,adminLoginBean, request);
					//处理主单据
					windowCheckInHandleEntrys(json, adminLoginBean);
				}
				catch(DoorHasUsedException e1){
					throw new DoorHasUsedException();
				} catch (SpotHasUsedException e) {
					throw new SpotHasUsedException();
				} catch (CheckInTaskCanntDeleteException e) {
					throw new CheckInTaskCanntDeleteException();
				} catch (CheckInTaskRepeatToEntryException e) {
					throw new CheckInTaskRepeatToEntryException();
				} catch (CheckInTaskRepeatToOthersException e) {
					throw new CheckInTaskRepeatToOthersException();
				} catch (CheckInMustHasTasksException e) {
					throw new CheckInMustHasTasksException();
				} catch (CheckInAllTasksClosedException e) {
					throw new CheckInAllTasksClosedException();
				} catch (CheckInMustNotHaveTasksException e) {
					throw new CheckInMustNotHaveTasksException();
				}catch (CheckInTaskHaveDeletedException e) {
					throw new CheckInTaskHaveDeletedException();
				}catch (EquipmentHasLoadedOrdersException e) {
					throw new EquipmentHasLoadedOrdersException();
				} catch(EquipmentNotFindException e) {
					throw new EquipmentNotFindException();
				} catch(DoorNotFindException e) {
					throw new DoorNotFindException();
				} catch(SpotNotFindException e) {
					throw new SpotNotFindException();
				} catch(CheckinTaskNotFoundException e) {
					throw new CheckinTaskNotFoundException();
				}catch(Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
	}
	
	/**
	 * 处理tasks
	 * @param json
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午4:17:04
	 */
	private DBRow windowCheckInHandleTasks(JSONObject json, AdminLoginBean adminLoginBean,HttpServletRequest request)throws Exception
	{
		DBRow result		= new DBRow();
		long mainId			= json.getLong("mainId");
		JSONArray items		= json.getJSONArray("items");
		long adid			= adminLoginBean.getAdid();
		String adidName		= adminLoginBean.getEmploye_name();
		
		for(int i=0;i<items.length();i++)
	    {
			DBRow row			= windowCheckInSubmitJsonToDBRow(items.getJSONObject(i), mainId, adminLoginBean);
			windowCheckInCheckTaskNumber(row);//同一单据在同一设备同一资源上不能重
			String number_order_status = row.getString("number_order_status");
			int order_system_type =  row.get("order_system_type", 0);
	    	long detailId		= row.get("dlo_detail_id", 0L);
	    	if(detailId > 0)
	    	{
	    		windowCheckInHandleTaskCheck(detailId, mainId);//关了的单据，或者已经开始load的单据不能修改
	    	}
	    	int number_type		= row.get("number_type", 0);
	    	String number		= row.getString("number");
	    	String notePersonIds= row.getString("ids");
	    	int resource_type	= row.get("occupancy_type", 0);
	    	long resource_id	= row.get("rl_id", 0L);
	    	String resource_name= windowCheckInHandleOccupyResourceName(resource_type, resource_id);
	    	long equipment_id	= row.get("equipment_id", 0L);
	    	DBRow equipment		= findEquipmentReOccupyById(equipment_id);
	    	//task日志内容
	    	String context		= windowCheckInHandleTaskContext(number_type, number, resource_type, resource_name
	    							, row.getString("note"), equipment.get("equipment_type", 0), equipment.getString("equipment_number"));
	    	
	    	//根据主单据id 和子单据id 查询子单据 是否存在
	    	String loadData="";//将load的MasterBolNo和OrderNo写到checkin明细表中
	    	if(number_type==ModuleKey.CHECK_IN_LOAD){
	    		loadData = sqlServerMgrZJ.findMasterBolNoOrderNoStrByLoadNo(number,adid);
	    	}
	    	row.add("master_order_nos", loadData);
	    	row.remove("yj"); row.remove("dx"); row.remove("ym"); row.remove("ids"); row.remove("dlo_detail_id");row.remove("note");
	    	row.remove("ra");row.remove("occupancy_type");row.remove("rl_id");row.remove("number_order_status");row.remove("order_system_type");
	    	if(detailId > 0)
	    	{
	    		//处理资源和设备
	    		DBRow taskHandle = windowCheckInHandleResources(detailId, detailId, number_type, number,resource_type, resource_id, equipment_id, mainId, adid, request);
	    		row.append(taskHandle);
	    		floorCheckInMgrZwb.updateDetailByIsExist(detailId,row);	
	    		//添加日志
	    		windowCheckInAddTaskLog("update",number,number_type , mainId, adid,adidName,resource_type, resource_name,notePersonIds,equipment.getString("seal_delivery"),equipment.getString("seal_pick_up"), equipment.get("equipment_type", 0), equipment.getString("equipment_number"));
	    		//删除通知，先查询通知
	    		deleteTaskWindowScheduleByDetailId(detailId, request);
	    		
	    	}
	    	else
	    	{
	    		row.add("number_status",1);
	    		//处理资源和设备
	    		detailId=floorCheckInMgrZwb.addOccupancyDetails(row);
	    		windowCheckInHandleResources(detailId, 0L, number_type, number,resource_type, resource_id, equipment_id, mainId, adid, request);
	    		windowCheckInAddTaskLog("add",number,number_type, mainId, adid,adidName,resource_type, resource_name,notePersonIds,equipment.getString("seal_delivery"),equipment.getString("seal_pick_up"), equipment.get("equipment_type", 0), equipment.getString("equipment_number"));
	    	}
	    	
	    	windowCheckInHandleTaskEquipmentRelType(equipment_id, mainId);
	    	//处理单据
	    	DBRow orderSystem = orderSystemMgr.addOrUpdateOrderSystems(detailId, adminLoginBean.getAdid(), number_order_status, order_system_type, adminLoginBean);
	    	if(null!=orderSystem)
	    	{
	    		DBRow updateDetail = new DBRow();
	    		updateDetail.add("lr_id", orderSystem.get("lr_id",0));
	    		floorCheckInMgrZwb.updateDetailByIsExist(detailId,updateDetail);	
	    	}
	    	//回写wms
	    	updateReceiptsByCtnOrBols(mainId);
	    	CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
	    	checkInMgrZwb.editCheckInIndex(mainId, "update");
	    	//发通知，上面删除，此处添加
	    	if(!notePersonIds.equals("")){
//	    		scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(),"",adid,notePersonIds,"",detailId, ModuleKey.CHECK_IN, emailTitle, context, ym, yj, dx,request,true,ProcessKey.CHECK_IN_WINDOW);			    	
	    		scheduleMgrZr.addSchedule(ScheduleModel.getCheckInScheduleModel(mainId, detailId,ProcessKey.CHECK_IN_WINDOW, adid, notePersonIds,context,number,number_type));
	    	}
	    	CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr)MvcUtil.getBeanFromContainer("proxyCheckInMgrZr");  
	    	DBRow[] schedules = this.floorCheckInMgrZyj.findWindowSchedule(mainId,detailId);
	    	if (schedules != null && schedules.length > 0) {
				for (DBRow temp : schedules) {
					long schedule_id = temp.get("schedule_id", 0l);
					checkInMgrZr.finishSchedule(schedule_id, adid);
				}
			}
	    	
	    }
		
		return result;//result没有属性
	}
	
	/**
	 * check单据是否重
	 * @param row{number,number_type,occupancy_type,rl_id,equipment_id,dlo_id,dlo_detail_id,company_id}
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月19日 下午12:26:09
	 */
	public void windowCheckInCheckTaskNumber(DBRow row) throws Exception
	{
		String number = row.getString("number");
		int number_type = row.get("number_type", 0);
		int occupancy_type = row.get("occupancy_type", 0);
		long rl_id = row.get("rl_id", 0L);
		long equipment_id = row.get("equipment_id", 0L);
		long entry_id = row.get("dlo_id", 0L);
		long dlo_detail_id = row.get("dlo_detail_id", 0L);
		String company_id = row.getString("company_id");
		long receipt_no = row.get("receipt_no", 0L);
		
		DBRow[] findTasks = floorCheckInMgrZyj.findTasksByTaskNumberEquipmentOccupys(number, number_type, equipment_id, occupancy_type, rl_id, entry_id, company_id,receipt_no);
		if(findTasks.length == 1)
		{
			if(dlo_detail_id == 0)
			{
				throw new CheckInTaskRepeatToEntryException();
			}
			else
			{
				if(dlo_detail_id != findTasks[0].get("dlo_detail_id", 0L))
				{
					throw new CheckInTaskRepeatToEntryException();
				}
			}
		}
		else if(findTasks.length > 1)
		{
			throw new CheckInTaskRepeatToEntryException();
		}
	}
	
	/**
	 * 回写receipts的inYard时间
	 * @param entryId
	 * @throws Exception
	 */
	public void updateReceiptsByCtnOrBols(long entryId)throws Exception
	{
		
		DBRow[] bols = floorCheckInMgrZwb.findEntryDetailByEntryIdType(entryId, ModuleKey.CHECK_IN_BOL);
		DBRow entryRow = floorCheckInMgrZwb.findGateCheckInById(entryId);
		for (int i = 0; i < bols.length; i++) 
		{
			String bol = bols[i].getString("number");
			String gateCheckInTime = null!=entryRow?entryRow.getString("gate_check_in_time"):"";
			String gateCheckInDate = DateUtil.showLocalTime(gateCheckInTime, null!=entryRow?entryRow.get("ps_id", 0L):0L);
			sqlServerMgrZJ.updateReceiptInYardDateByBolOrCtnrNo(gateCheckInDate
				, bol, null, bols[i].getString("supplier_id"),bols[i].getString("company_id"),0L, null);
		}
		DBRow[] ctns = floorCheckInMgrZwb.findEntryDetailByEntryIdType(entryId, ModuleKey.CHECK_IN_CTN);
				//.findContainerNoDetailsByEntryId(entryId);
		for (int i = 0; i < ctns.length; i++) 
		{
			String ctn = ctns[i].getString("number");
			String gateCheckInTime = null!=entryRow?entryRow.getString("gate_check_in_time"):"";
			String gateCheckInDate = DateUtil.showLocalTime(gateCheckInTime, null!=entryRow?entryRow.get("ps_id", 0L):0L);
			sqlServerMgrZJ.updateReceiptInYardDateByBolOrCtnrNo(gateCheckInDate
				, null, ctn, ctns[i].getString("supplier_id"),ctns[i].getString("company_id"), 0L, null);
		}
	}
	
	/**
	 * 编辑、删除task需要做的验证
	 * 关了或者已load，不能修改和删除
	 * @param detail_id
	 * @param entry_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月1日 下午1:48:12
	 */
	public void windowCheckInHandleTaskCheck(long detail_id, long entry_id) throws Exception
	{
		DBRow task = findTaskEquipmentResourceByDetailId(detail_id);
		if(!(task.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.UNPROCESS 
				|| task.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.PROCESSING))
		{
			throw new CheckInAllTasksClosedException();
		}
		else
		{
			int loadDetailCount = floorCheckInMgrZwb.loadDoneConut(detail_id);
			if(loadDetailCount > 0)
			{
				throw new EquipmentHasLoadedOrdersException();
			}
		}
		if(task.get("equipment_status", 0) == CheckInMainDocumentsStatusTypeKey.LEFT)
			throw new EquipmentHadOutException();
	}
	
	/**
	 * 删除任务
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月1日 下午2:38:22
	 */
	public void windowCheckInDeleteTask1(HttpServletRequest request) throws Exception
	{
		long entry_id = StringUtil.getLong(request, "entry_id");
		long detail_id = StringUtil.getLong(request, "detail_id");
		DBRow task = findTaskEquipmentResourceByDetailId(detail_id);
		if(null == task) throw new CheckinTaskNotFoundException();
		windowCheckInHandleTaskCheck(detail_id, entry_id);
		//如果是本单据添加的设备可以删除，否则不能删除。非本单据的设备，将out_entry处理
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		long adid				= adminLoginBean.getAdid();
		String adidName			= adminLoginBean.getEmploye_name();
		//重算设备的rel_type
		String resource_name= windowCheckInHandleOccupyResourceName(task.get("resources_type", 0), task.get("resources_id", 0));
		windowCheckInAddTaskLog("delete",task.getString("number"),task.get("number_type",0), entry_id, adid,adidName
				,task.get("resources_type", 0), resource_name,"",task.getString("seal_delivery")
				,task.getString("seal_pick_up"), task.get("equipment_type", 0), task.getString("equipment_number"));
		ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Task, detail_id, adid);
		//处理是否删除业务单据
		orderSystemMgr.deleteOrNotOrderSystemByDetailInfos(detail_id);
		detCheckinWindow(request, detail_id);
		//更新entry下没有windowCheckIn时间的设备   2015-1-29 只在check in 与waiting时添加windowCheckIn的时间
	//	windowCheckInUpdateMainTimeEquipment(entry_id, adid);
		//删除task
		windowCheckInHandleTaskEquipmentRelType(task.get("equipment_id",0L), entry_id);
		CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
		checkInMgrZwb.editCheckInIndex(entry_id, "update");
	}
	
	
	/**
	 * 处理设备的rel_type
	 * @param equipment_id
	 * @param entry_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月1日 下午1:43:59
	 */
	public void windowCheckInHandleTaskEquipmentRelType(long equipment_id, long entry_id) throws Exception
	{
		//提货task数量
		int[] pickNumberType = {ModuleKey.CHECK_IN_LOAD, ModuleKey.CHECK_IN_ORDER, ModuleKey.CHECK_IN_PONO, ModuleKey.CHECK_IN_PICKUP_ORTHERS};
		int pick_up_count = floorCheckInMgrZyj.findEntryTasksByEntryIdNumberTypes(entry_id, pickNumberType, equipment_id).length;
		//送货task数量
		int[] deliveryNumberType = {ModuleKey.CHECK_IN_BOL, ModuleKey.CHECK_IN_CTN, ModuleKey.CHECK_IN_DELIVERY_ORTHERS};
		int delivery_count = floorCheckInMgrZyj.findEntryTasksByEntryIdNumberTypes(entry_id, deliveryNumberType, equipment_id).length;
		//计算rel_type
		int rel_type = 0;
		if(pick_up_count > 0 && delivery_count > 0)
		{
			rel_type = CheckInMainDocumentsRelTypeKey.BOTH;
		}
		else if(pick_up_count > 0)
		{
			rel_type = CheckInMainDocumentsRelTypeKey.PICK_UP;
		}
		else if(delivery_count > 0)
		{
			rel_type = CheckInMainDocumentsRelTypeKey.DELIVERY;
		}
		DBRow row = new DBRow();
		row.add("rel_type", rel_type);
		floorCheckInMgrZyj.updateEquipment(equipment_id, row);
	}
	
	/**
	 * 通过entry明细ID，查询task设备资源
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月2日 下午3:20:33
	 */
	public DBRow findTaskEquipmentResourceByDetailId(long detail_id) throws Exception
	{
		DBRow row = floorCheckInMgrZyj.findTaskEquipmentResourceByDetailId(detail_id);
		if(null == row) throw new CheckinTaskNotFoundException();
		if(row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.PARTIALLY
				|| row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.EXCEPTION
				|| row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.CLOSE)
		{
			throw new CheckInAllTasksClosedException();
		}
		long resources_id	= row.get("resources_id", 0L);
		int resources_type	= row.get("resources_type", 0);
		if(resources_id == 0)
		{
			resources_id	= row.get("rl_id", 0L);
			resources_type	= row.get("occupancy_type", 0);
		}
		row.add("resources_type", resources_type);
		row.add("resources_id", resources_id);
		String resource_name = windowCheckInHandleOccupyResourceName(resources_type, resources_id);
		row.add("resources_name", resource_name);
		
		
		//查询通知
		CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
		DBRow[] notices = checkInMgrZwb.windowFindSchedule(row.get("dlo_detail_id", 0L), new int[]{ProcessKey.CHECK_IN_WINDOW,ProcessKey.GateNotifyWareHouse});
		String notice_name = "";
		String notice_ids = "";
		if(notices.length > 0)
		{
			for (int j = 0; j < notices.length; j++) {
				notice_name += (","+notices[j].getString("employe_name"));
 				notice_ids  += (","+notices[j].getString("adids"));
			}
		}
		notice_ids = notice_ids.length()>0?notice_ids.substring(1):"";
		notice_name = notice_name.length()>0?notice_name.substring(1):"";
		row.add("notice_names", notice_name);
		row.add("notice_ids", notice_ids);
		return row;
	}
	
	/**
	 * 处理明细占用的资源和设备
	 * @param detailIdNow
	 * @param detailIdBef
	 * @param number_type
	 * @param number
	 * @param resource_type
	 * @param resource_id
	 * @param equipment_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午6:26:42
	 */
	public DBRow windowCheckInHandleResources(long detailIdNow, long detailIdBef, int number_type, String number
			,int resource_type, long resource_id, long equipment_id, long mainId,long adid, HttpServletRequest request) throws Exception
	{
		DBRow result = new DBRow();
		//更新
		if(detailIdNow == detailIdBef)
		{
			//单据号变了，子单据状态需要改,门需要改；如果门变了，需要改变门状态，子单据
			DBRow detailRowExist = findTaskEquipmentResourceByDetailId(detailIdNow);
			//在其他界面删除了
			if(null == detailRowExist) throw new CheckInTaskHaveDeletedException();
			
			long occupiedTypeId	= detailRowExist.get("rl_id", 0L);
			int occupiedTypeKey	= detailRowExist.get("occupancy_type", 0);
			String numberExist	= detailRowExist.getString("number");
			int numberTypeExist	= detailRowExist.get("number_type", 0);
			long equipmentExist	= detailRowExist.get("equipment_id", 0L);
			
			//单号或者门或者设备改变了，如果没有单据明细，将单据状态为未处理，门占用状态改成预留，将wareHouse任务删除；有单据明细提示不能修改，让warehouse处理后才能修改
			if(!numberExist.equals(number) || numberTypeExist != number_type || equipmentExist != equipment_id)
//					|| occupiedTypeId != resource_id || occupiedTypeKey != resource_type
			{
				//单据或者门改变，将单据状态为未处理，门占用状态改成预留
				result.add("number_status", CheckInChildDocumentsStatusTypeKey.UNPROCESS);//改子单据状态
				//删除warehouse任务，通知
				deleteTaskWarehouseScheduleByDetailId(detailIdBef, request);
			}
			if(occupiedTypeId != resource_id || occupiedTypeKey != resource_type)
			{
				deleteTaskWarehouseScheduleByDetailId(detailIdBef, request);
				ymsMgrAPI.operationSpaceRelation(resource_type, resource_id, SpaceRelationTypeKey.Task, detailIdNow
						, ModuleKey.CHECK_IN, mainId, OccupyStatusTypeKey.RESERVERED, adid);
			}
		}
		else
		{
			ymsMgrAPI.operationSpaceRelation(resource_type, resource_id, SpaceRelationTypeKey.Task, detailIdNow
					, ModuleKey.CHECK_IN, mainId, OccupyStatusTypeKey.RESERVERED, adid);
		}
		return result;
	}
	
	
	
	
	
	
	
	/**
	 * 处理主单据
	 * @param json
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午4:17:04
	 */
	private void windowCheckInHandleEntrys(JSONObject json, AdminLoginBean adminLoginBean)throws Exception
	{
		long mainId				= StringUtil.getJsonLong(json, "mainId");
		//TODO 加上报错？
		String appointmentdate	= StringUtil.getJsonString(json, "appointmentdate");
		if(StrUtil.isBlankNullUndefined(appointmentdate))
		{
			appointmentdate	= StringUtil.getJsonString(json, "appointment_date");
		}
		int waitingOrNot		= StringUtil.getJsonInt(json, "waiting");
		int checkin				= StringUtil.getJsonInt(json, "checkin");
		int is_check_in				= StringUtil.getJsonInt(json, "is_check_in");
		//必须有task或有pickup设备才能waiting或window
		if((floorCheckInMgrZyj.findTaskCountByEntryId(mainId)==0 && checkInWindowFindEquipmentByEntryOutIdpurpose(CheckInLiveLoadOrDropOffKey.PICK_UP, mainId, 0).length==0) && (waitingOrNot==1 || checkin==1))
		{
			throw new CheckInMustHasTasksException();
		}
		
		//所有未关闭的task必须得有window--->warehouse的通知人才可以进行check in  |  waiting    wfh
		if(checkin==1 || waitingOrNot ==1){
			DBRow[] schdule_count = floorCheckInMgrWfh.findScheduleCountByMainId(mainId, ProcessKey.CHECK_IN_WINDOW);
			DBRow[] task_count = floorCheckInMgrWfh.findAllTaskCountByMainId(mainId);
			if(task_count.length!=0&&schdule_count.length==0)
				throw new TaskNoAssignWarehouseSupervisorException();
			for (DBRow task : task_count) {
				long detail_id = task.get("dlo_detail_id", 0L);
				boolean flag = true;
				for(DBRow schedule:schdule_count){
					long associateId = schedule.get("associate_id", 0L);
					if(associateId==detail_id){
						flag = false;
						break;
					}
				}
				if(flag){
					throw new TaskNoAssignWarehouseSupervisorException();
				}
			}
		}
	
		//完成schedule
		CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr)MvcUtil.getBeanFromContainer("proxyCheckInMgrZr");  
    	DBRow[] schedules = this.floorCheckInMgrZyj.findWindowSchedule(mainId,0);
    	if (schedules != null && schedules.length > 0) {
			for (DBRow temp : schedules) {
				long schedule_id = temp.get("schedule_id", 0l);
				checkInMgrZr.finishSchedule(schedule_id, adminLoginBean.getAdid());
			}
		}
		//提货task数量
		int[] pickNumberType = {ModuleKey.CHECK_IN_LOAD, ModuleKey.CHECK_IN_ORDER, ModuleKey.CHECK_IN_PONO, ModuleKey.CHECK_IN_PICKUP_ORTHERS};
		int pick_up_count = floorCheckInMgrZyj.findEntryTasksByEntryIdNumberTypes(mainId, pickNumberType, 0L).length;
		//送货task数量
//		int[] deliveryNumberType = {ModuleKey.CHECK_IN_BOL, ModuleKey.CHECK_IN_CTN, ModuleKey.CHECK_IN_DELIVERY_ORTHERS};
//		int delivery_count = floorCheckInMgrZyj.findEntryTasksByEntryIdNumberTypes(mainId, deliveryNumberType).length;
		//计算rel_type
//		int rel_type = 0;
//		if(pick_up_count > 0 && delivery_count > 0)
//		{
//			rel_type = CheckInMainDocumentsRelTypeKey.BOTH;
//		}
//		else if(pick_up_count > 0)
//		{
//			rel_type = CheckInMainDocumentsRelTypeKey.PICK_UP;
//		}
//		else if(delivery_count > 0)
//		{
//			rel_type = CheckInMainDocumentsRelTypeKey.DELIVERY;
//		}
		DBRow mRow		= floorCheckInMgrZwb.findGateCheckInById(mainId);
		
		//***********  添加处理 waiting wfh
		int waitingType = WaitingTypeKey.NotWaiting;
		if(waitingOrNot==1){ //等于1 进行处理
			DBRow[] tasks = floorCheckInMgrWfh.findTaskAppointmentByMainId(mainId); //查找所有的task
		//	DBRow[] tractor = floorCheckInMgrWfh.findTractorByEntryId(mainId, 1); //只查找车头的checkIn 时间
			//得到 预约时间中最早的时间
			Date earlyWaitTime = null;
			for (DBRow row : tasks) {
				String time = row.get("appointment_date", "");
				if(!StringUtil.isBlank(time)){
					earlyWaitTime = DateUtil.formateDate(time);
					break;
				}
			}
			String checkTime = mRow.get("create_time", "");
			Date checkInTime = null;
			if(!StringUtil.isBlank(checkTime)){
				//得到 checkInTime
				checkInTime = DateUtil.formateDate(checkTime);
			}
			//如果最早的预约时间为空 那么这个entry下的task都没有预约时间
			if(earlyWaitTime!=null){
				long min = (checkInTime.getTime()-earlyWaitTime.getTime())/1000/60;
				if(min>=30){ //晚60min 司机签到时间预约时间中最早的时间（某一条）晚60分钟以上算lateWaiting
					waitingType = WaitingTypeKey.LateWaiting;
				}else if(min<=-30){ //早120min 司机签到时间比最早的预约时间（全部的）早2个小时以上 earlyWaiting
					waitingType = WaitingTypeKey.EarlyWaiting;
				}else{
					waitingType = WaitingTypeKey.WarehouseWaitng;
				}
			}else{
				waitingType = WaitingTypeKey.NoAppointment;
			}
		}
		//***********   end
		
		
		DBRow mainRow	= mRow;
		if(!StrUtil.isBlankNullUndefined(appointmentdate)){
			mainRow.add("appointment_time", DateUtil.showUTCTime(appointmentdate,adminLoginBean.getPs_id()));
		}else{
			mainRow.add("appointment_time", null);
		}
//		if(mRow.getString("window_check_in_time").equals("")){
//			mainRow.add("window_check_in_time",DateUtil.NowStr());
//			mainRow.add("window_check_in_operator",adminLoginBean.getAdid());
//		}
//		mainRow.add("window_check_in_operate_time",DateUtil.NowStr());
		mainRow.add("dlo_id", mainId); 
//		mainRow.add("rel_type", rel_type);
		mainRow.add("load_count",pick_up_count);
		mainRow.add("waiting", waitingType);
		//更新优先级  priority
		int priority = StringUtil.getJsonInt(json, "priority");
		mainRow.add("priority", priority);
		
	    floorCheckInMgrZwb.modCheckIn(mainId,mainRow);
	    //更新entry下没有windowCheckIn时间的设备
//	    if(checkin == 1 || waitingOrNot == 1)
//	    {
	    //2015-1-29  只有check in 2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
	    if(waitingType==WaitingTypeKey.WarehouseWaitng ||  checkin==1 ){
	    	windowCheckInUpdateMainTimeEquipment(mainId, adminLoginBean.getAdid());
	    }
//	    }
	}
	
	
	/**
	 * windowCheckIn将json转成DBRow
	 * @param json
	 * @param mainId
	 * @return
	 * @throws JSONException
	 * @author:zyj
	 * @date: 2014年11月17日 上午11:07:52
	 */
	public DBRow windowCheckInSubmitJsonToDBRow(JSONObject json, long mainId, AdminLoginBean adminLoginBean) throws Exception
	{
		if(null != json)
		{
			DBRow row = new DBRow();
			
			String number			= StringUtil.getJsonString(json, "number").toUpperCase();//load
	    	String doorId			= StringUtil.getJsonString(json, "doorId");//门id
	    	String ids				= StringUtil.getJsonString(json, "tongzhi");//要通知的人 名字 字符串				   
	    	String detailId			= StringUtil.getJsonString(json, "detailId");
	    	String zoneId			= StringUtil.getJsonString(json, "zoneId");
	    	if(StrUtil.isBlankNullUndefined(zoneId))
	    	{
	    		zoneId = "0";
	    	}
	    	String youjian			= StringUtil.getJsonString(json, "youjian");    //邮件
	    	String duanxin			= StringUtil.getJsonString(json, "duanxin");    //短信
	    	String yemian			= StringUtil.getJsonString(json, "yemian");      //页面
	    	String beizhu			= StringUtil.getJsonString(json, "beizhu");       //备注
	    	String number_type		= StringUtil.getJsonString(json, "number_type");
	    	String customer_id		= StringUtil.getJsonString(json, "customer_id"); 
	    	String company_id		= StringUtil.getJsonString(json, "company_id"); 
	    	String account_id		= StringUtil.getJsonString(json, "account_id");
	    	String order_no			= StringUtil.getJsonString(json, "order_no");
	    	String po_no			= StringUtil.getJsonString(json, "po_no");
	    	String supplier_id		= StringUtil.getJsonString(json, "supplier_id"); 
	    	String staging_area_id	= StringUtil.getJsonString(json, "staging_area_id"); 
	    	String freight_term		= StringUtil.getJsonString(json, "freight_term"); 
	    	String occupancy_type	= StringUtil.getJsonString(json, "occupancy_type");
			long equipment_id		= StringUtil.getJsonLong(json, "container_no");
			int ra					= StringUtil.getJsonInt(json, "ra");//0:pickup, 1:delivery
			String appointment_date	= StringUtil.getJsonString(json, "appointment_date");
			String receiptNo		= StringUtil.getJsonString(json, "receipt_no");
			long receipt_no			= !StrUtil.isBlankNullUndefined(receiptNo)?json.getLong("receipt_no"):0L;
			
			String number_order_status=  StringUtil.getJsonString(json, "number_order_status");
			String order_system_type=StringUtil.getJsonString(json, "order_system_type");
			DBRow main = this.floorCheckInMgrZwb.findGateCheckInById(mainId);
			String gate_check_in_time  = main.getString("gate_check_in_time");
			if(!StrUtil.isBlankNullUndefined(appointment_date))
			{
				row.add("appointment_date", DateUtil.showUTCTime(appointment_date,adminLoginBean.getPs_id()));
				long diffTime = (DateUtil.getDate2LongTime(gate_check_in_time)-DateUtil.getDate2LongTime(DateUtil.showUTCTime(appointment_date, adminLoginBean.getPs_id())))/1000/60;
				if(diffTime >= 30){												//60
					row.add("come_status",CheckInOrderComeStatusKey.TooLate);
				}else if(diffTime <= -30){										//-120
					row.add("come_status",CheckInOrderComeStatusKey.TooEarly);
				}else{
					row.add("come_status",CheckInOrderComeStatusKey.OnTime);
				}
			}else{
				row.add("come_status",CheckInOrderComeStatusKey.NoAppointment);
				row.add("appointment_date",null);
			}
	    	
	    	row.add("dlo_id",mainId);
	    	row.add("dlo_detail_id", detailId);
	    	row.add("occupancy_type",occupancy_type);
	    	row.add("rl_id",doorId);
	    	row.add("zone_id",zoneId);
			
	    	int bill_type=0;   
	    	if(!"".equals(number_type)){
	    		bill_type=Integer.parseInt(number_type);
	    	}
	    			
	    	row.add("number_type",bill_type);
	    	row.add("number",number);
	    	row.add("equipment_id", equipment_id);
	    	row.add("customer_id",customer_id);
	    	row.add("company_id",company_id);
	    	row.add("account_id",account_id);
	    	row.add("order_no",order_no);
	    	row.add("po_no",po_no);
	    	row.add("supplier_id",supplier_id);
	    	row.add("staging_area_id",staging_area_id);
	    	row.add("freight_term",freight_term);
			row.add("note", beizhu);
			row.add("number_order_status",number_order_status);
			row.add("order_system_type", order_system_type);
			row.add("receipt_no", receipt_no);
			
	    	Boolean yj=true;
	    	Boolean dx=true;
	    	Boolean ym=true;
	    	if(youjian.equals("0")){
	    		yj=false;
	    	}
	    	if(duanxin.equals("0")){
	    		dx=false;
	    	}
	    	if(yemian.equals("0")){
	    		ym=false;
	    	}
	    	
	    	row.add("yj", yj==true?1:2);
	    	row.add("dx", dx==true?1:2);
	    	row.add("ym", ym==true?1:2);
	    	row.add("ids", ids);
			row.add("ra", ra);
	    	
			return row;
		}
		return null;
	}
	
	/**
	 * 通过资源类型和资源ID，获取资源名称
	 * @param resourceType
	 * @param resourceId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午4:47:47
	 */
	@Override
	public String windowCheckInHandleOccupyResourceName(int resourceType, long resourceId) throws Exception
	{
		String resourceName = "";
		if(resourceId == 0)
		{
			return resourceName;
		}
		if(resourceType == OccupyTypeKey.DOOR)
		{
			DBRow door = storageDoorMgrZr.findDoorDetaiByDoorId(resourceId);
			if(door == null)
			{
				throw new DoorNotFindException();
			}
			else
			{
				resourceName = door.getString("doorId");
			}
		}
		else if(resourceType == OccupyTypeKey.SPOT)
		{
			DBRow spot = storageYardControlZr.findSpotDetaiBySpotId(resourceId);
			if(spot == null)
			{
				throw new SpotNotFindException();
			}
			else 
			{
				resourceName = spot.getString("yc_no");
			}
		}
		return resourceName;
	}
	
	/**
	 * 通过设备ID，获取设备信息
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午5:10:52
	 */
//	public DBRow windowCheckInHandleEquipmentTypeName(long equipment_id) throws Exception
//	{
//		DBRow row = ymsMgrAPI.getDetailEquipment(equipment_id);
//		if(null == row)
//		{
//			throw new EquipmentNotFindException();
//		}
//		return row;
//	}
	
	
	/**
	 * 通过设备ID，查询设备及占用资源信息
	 */
	public DBRow findEquipmentReOccupyById(long equipment_id) throws Exception
	{
		DBRow row = floorCheckInMgrZyj.findEquipmentReOccupyById(equipment_id);
		if(null == row)
		{
			throw new EquipmentNotFindException();
		}
		long resources_id	= row.get("resources_id", 0L);
		int resources_type	= row.get("resources_type", 0);
		String resource_name = windowCheckInHandleOccupyResourceName(resources_type, resources_id);
		row.add("resources_name", resource_name);
		return row;
	}
	
	
	
	
	/**
	 * 处理window的Task日志内容
	 * @param bill_type
	 * @param number
	 * @param occupancy_type
	 * @param occupyName
	 * @param note
	 * @param equipmentType
	 * @param equipmentName
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午4:59:54
	 */
	private String windowCheckInHandleTaskContext(int bill_type, String number,int occupancy_type, String occupyName
			, String note, int equipmentType, String equipmentName) throws Exception
	{
		CheckInMainDocumentsRelTypeKey pkDv = new CheckInMainDocumentsRelTypeKey();
		ModuleKey moduleKey 				= new ModuleKey();
		OccupyTypeKey occupyTypeKey			= new OccupyTypeKey();
		CheckInTractorOrTrailerTypeKey eqt  = new CheckInTractorOrTrailerTypeKey();
		
		String str = pkDv.getCheckInMainDocumentsRelTypeKeyValue(bill_type)
				  +" 【 " + moduleKey.getModuleName(bill_type)+" : " + number+" 】 To 【"
				  +occupyTypeKey.getOccupyTypeKeyName(occupancy_type)+" : "+occupyName+" 】 "
				  +"In 【 "+eqt.getContainerTypeKeyValue(equipmentType)+" : "+equipmentName+" 】";
		if(!StrUtil.isBlank(note))
		{
			str += " Note : "+note;
		}
		return str;
	}
	
	/**
	 * 操作task日志
	 * @param type
	 * @param number
	 * @param number_type
	 * @param main_id
	 * @param adid
	 * @param adidName
	 * @param occupyType
	 * @param occupyName
	 * @param ids
	 * @param in_seal
	 * @param out_seal
	 * @param equipType
	 * @param equip
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月2日 上午11:32:23
	 */
	public void windowCheckInAddTaskLog(String type,String number,int number_type,long main_id,long adid, String adidName
			,int occupyType,String occupyName,String ids,String in_seal,String out_seal,int equipType, String equip)throws Exception
	{
		ModuleKey moduleKey = new ModuleKey();
		OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
		CheckInTractorOrTrailerTypeKey eqt  = new CheckInTractorOrTrailerTypeKey();
		String data="In 【"+eqt.getContainerTypeKeyValue(equipType)+" : "+equip+"】,";
		if(!occupyName.equals("")){
			data+= occupyTypeKey.getOccupyTypeKeyName(occupyType) +":"+occupyName+",";
		}
		if(!ids.equals("")){
			String operator="";
			String[] str=ids.split(",");
			for(int i=0;i<str.length;i++){
				DBRow adidRow= floorCheckInMgrZyj.findAdminById(Long.parseLong(str[i]));
				String name=adidRow.get("employe_name","");
				if(i==str.length-1){
					operator+=name;
				}else{
					operator+=name+"、";	
				}
			}
			data+="Supervisor:"+operator+",";
		}

		data+="Delivery Seal:"+in_seal+",";

		data+="PickUp Seal:"+out_seal+",";
		if(!data.equals("")){
			data=data.substring(0, data.length()-1);
		}
		if(type.equals("add")){
			this.addCheckInLog(adid, "Create "+moduleKey.getModuleName(number_type)+"【"+number+"】", main_id, CheckInLogTypeKey.WINDOW,DateUtil.NowStr(),data);
		}else if(type.equals("update")){
			this.addCheckInLog(adid, "Update "+moduleKey.getModuleName(number_type)+"【"+number+"】", main_id, CheckInLogTypeKey.WINDOW,DateUtil.NowStr(),data);
		}else if(type.equals("delete")){
			this.addCheckInLog(adid, "Delete "+moduleKey.getModuleName(number_type)+"【"+number+"】", main_id, CheckInLogTypeKey.WINDOW,DateUtil.NowStr(),data);
		}else if(type.equals("release_main")){
			String content = "release "+occupyTypeKey.getOccupyTypeKeyName(occupyType) +":"+occupyName;
			this.addCheckInLog(adid, content, main_id, CheckInLogTypeKey.WINDOW,DateUtil.NowStr(),content);
		}
	}
	
	/**
	 * 操作设备日志
	 * @param type
	 * @param equipType
	 * @param equip
	 * @param main_id
	 * @param adid
	 * @param adidName
	 * @param occupyType
	 * @param occupyName
	 * @param in_seal
	 * @param out_seal
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月2日 上午11:31:44
	 */
	public void windowCheckInAddEquipmentLog(String type,int equipType, String equip, long main_id,long adid, String adidName
			,int occupyType,String occupyName,String in_seal,String out_seal, String data1)throws Exception
	{
		OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
		CheckInTractorOrTrailerTypeKey eqt  = new CheckInTractorOrTrailerTypeKey();
		String title = " "+eqt.getContainerTypeKeyValue(equipType)+" : 【 "+equip+" 】";
		String data= title+",";
		if(!occupyName.equals("")){
			data+= occupyTypeKey.getOccupyTypeKeyName(occupyType) +" : "+occupyName+",";
		}

		data+="Delivery Seal :"+in_seal+",";

		data+="PickUp Seal :"+out_seal+",";
		if(!StrUtil.isBlank(data1))
		{
			data = data1+","+data;
		}
		if(!data.equals("")){
			data=data.substring(0, data.length()-1);
		}
		if(type.equals("create")){
			this.addCheckInLog(adid, "Create "+title, main_id, CheckInLogTypeKey.WINDOW,DateUtil.NowStr(),data);
		}else if(type.equals("update")){
			this.addCheckInLog(adid, "Update "+title, main_id, CheckInLogTypeKey.WINDOW,DateUtil.NowStr(),data);
		}else if(type.equals("delete")){
			this.addCheckInLog(adid, "Delete "+title, main_id, CheckInLogTypeKey.WINDOW,DateUtil.NowStr(),data);
		}
	}
	
	
	public long addCheckInLog(long operator_id,String note,long dlo_id,long log_type,String operator_time,String data)throws Exception
	{
		DBRow row = new DBRow();
		row.add("operator_id", operator_id);
		row.add("note", note);
		row.add("dlo_id", dlo_id);
		row.add("log_type", log_type);
		row.add("operator_time",DateUtil.NowStr());
		row.add("data", data);
		return this.floorCheckInMgrZwb.addCheckInLog(row);
	}
	
	/**
	 * 删除明细的warehouse任务
	 * @param id
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月17日 下午6:08:21
	 */
	private void deleteTaskWarehouseScheduleByDetailId(long id, HttpServletRequest request) throws Exception
	{
		DBRow[] ware1 = floorCheckInMgrZwb.selectSchedule(id, ProcessKey.CHECK_IN_WAREHOUSE);
		for (int k = 0; k < ware1.length; k++) {
			long schedule_id=ware1[k].get("schedule_id", 0l);
			scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, request);
		}
		DBRow[] ware2 = floorCheckInMgrZwb.selectSchedule(id, ProcessKey.CHECK_IN_ANDROID_WAREHOUSE);
		for (int k = 0; k < ware2.length; k++) {
			long schedule_id=ware2[k].get("schedule_id", 0l);
			scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, request);
		}
	}
	
	/**
	 * 删除明细的warehouse任务
	 * @param id
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月17日 下午6:08:21
	 */
	private void deleteTaskWarehouseScheduleByDetailId(long id, AdminLoginBean adminLoginBean) throws Exception
	{
		DBRow[] ware1 = floorCheckInMgrZwb.selectSchedule(id, ProcessKey.CHECK_IN_WAREHOUSE);
		for (int k = 0; k < ware1.length; k++) {
			long schedule_id=ware1[k].get("schedule_id", 0l);
			scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, adminLoginBean);
		}
		DBRow[] ware2 = floorCheckInMgrZwb.selectSchedule(id, ProcessKey.CHECK_IN_ANDROID_WAREHOUSE);
		for (int k = 0; k < ware2.length; k++) {
			long schedule_id=ware2[k].get("schedule_id", 0l);
			scheduleMgrZr.deleteScheduleByScheduleId(schedule_id, adminLoginBean);
		}
	}
	
	/**
	 * 删除明细的window任务
	 * @param id
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月17日 下午6:08:21
	 */
	private void deleteTaskWindowScheduleByDetailId(long detailId, HttpServletRequest request) throws Exception
	{
		DBRow[] shRows=floorCheckInMgrZwb.selectSchedule(detailId, ProcessKey.CHECK_IN_WINDOW);
		if(shRows.length>0 && shRows!=null){
			scheduleMgrZr.deleteScheduleByScheduleId(shRows[0].get("schedule_id", 0l), request);
		}
		DBRow[] shRows1=floorCheckInMgrZwb.selectSchedule(detailId, ProcessKey.GateNotifyWareHouse);
		if(shRows1.length>0 && shRows1!=null){
			scheduleMgrZr.deleteScheduleByScheduleId(shRows1[0].get("schedule_id", 0l), request);
		}
	}
	
	/**
	 * 删除明细的window任务
	 * @param id
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月17日 下午6:08:21
	 */
	private void deleteTaskWindowScheduleByDetailId(long detailId, AdminLoginBean adminLoginBean) throws Exception
	{
		DBRow[] shRows=floorCheckInMgrZwb.selectSchedule(detailId, ProcessKey.CHECK_IN_WINDOW);
		if(shRows.length>0 && shRows!=null){
			scheduleMgrZr.deleteScheduleByScheduleId(shRows[0].get("schedule_id", 0l), adminLoginBean);
		}
		DBRow[] shRows1=floorCheckInMgrZwb.selectSchedule(detailId, ProcessKey.GateNotifyWareHouse);
		if(shRows1.length>0 && shRows1!=null){
			scheduleMgrZr.deleteScheduleByScheduleId(shRows1[0].get("schedule_id", 0l), adminLoginBean);
		}
	}
	
	
	/**
	 * 通过设备号查询在yardDrop设备
	 * @param number
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午9:38:48
	 */
	public DBRow[] findInYardDropEquipmentsByNumber(String number, long ps_id,int equipment_type) throws Exception
	{
		try
		{
			//设备及占用的资源，一设备多资源
			DBRow[] equipRes = floorCheckInMgrZyj.findInYardDropOffEquipmentByNumber(number, ModuleKey.CHECK_IN, ps_id,equipment_type);
			Map<Long, DBRow> resultMaps = new HashMap<Long, DBRow>();
			for (int i = 0; i < equipRes.length; i++)
			{
				long equipId = equipRes[i].get("equipment_id", 0L);
				int re_type = equipRes[i].get("resources_type", 0);
				long re_id = equipRes[i].get("resources_id", 0L);
				equipRes[i].add("resources_name",windowCheckInHandleOccupyResourceName(re_type, re_id));
				if(!resultMaps.containsKey(equipId))
				{
					List<DBRow> res = new ArrayList<DBRow>();
					DBRow re = new DBRow();
					re.add("resources_type", re_type);
					re.add("resources_id", re_id);
					re.add("resources_name",windowCheckInHandleOccupyResourceName(re_type, re_id));
					re.add("occupy_status", equipRes[i].get("occupy_status",0));
					res.add(re);
					equipRes[i].add("resources", res);
					resultMaps.put(equipId, equipRes[i]);
				}
				else
				{
					List<DBRow> res = (ArrayList<DBRow>)resultMaps.get(equipId).get("resources", new ArrayList<DBRow>());
					DBRow re = new DBRow();
					re.add("resources_type", re_type);
					re.add("resources_id", re_id);
					re.add("resources_name",windowCheckInHandleOccupyResourceName(re_type, re_id));
					re.add("occupy_status", equipRes[i].get("occupy_status",0));
					res.add(re);
				}
			}
			
			Set<Entry<Long, DBRow>> eqs = resultMaps.entrySet();
			List<DBRow> results = new ArrayList<DBRow>();
			for (Iterator iterator = eqs.iterator(); iterator.hasNext();) {
				Entry<Long, DBRow> entry = (Entry<Long, DBRow>) iterator.next();
				DBRow equipment = entry.getValue();
				List<DBRow> resources = (List<DBRow>) equipment.get("resources", new ArrayList<DBRow>());
				equipment.add("resources", resources.toArray(new DBRow[0]));
				results.add(equipment);
			}
			DBRow[] resultEqs = results.toArray(new DBRow[0]);
			return resultEqs;
		}
		catch(Exception e){
			throw new Exception("findInYardDropEquipmentsByNumber"+e);
		}
	}
	
	
	
	
	
	/**
	 * 查询inYardDropOffEquipment
	 * @param equipmentNumber
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午8:56:27
	 */
	public DBRow[] findInYardDropOffEquipmentByNumberNotThisEntry(String equipmentNumber, long ps_id, long entry_id) throws Exception
	{

		//设备及占用的资源，一设备多资源
		DBRow[] equipRes = floorCheckInMgrZyj.findInYardDropOffEquipmentByNumberNotThisEntry(equipmentNumber, ModuleKey.CHECK_IN, ps_id,entry_id);
//		Map<Long, DBRow> resultMaps = new HashMap<Long, DBRow>();
		for (int i = 0; i < equipRes.length; i++)
		{
			
			long equipId = equipRes[i].get("equipment_id", 0L);
			int re_type = equipRes[i].get("resources_type", 0);
			long re_id = equipRes[i].get("resources_id", 0L);
			equipRes[i].add("resources_type", re_type);
			equipRes[i].add("resources_id", re_id);
			equipRes[i].add("resources_name", windowCheckInHandleOccupyResourceName(re_type, re_id));
			equipRes[i].add("occupy_status", equipRes[i].get("occupy_status",0));
			equipRes[i].add("carrier", equipRes[i].get("carrier",""));
			
			
//			if(!resultMaps.containsKey(equipId))
//			{
//				List<DBRow> res = new ArrayList<DBRow>();
//				DBRow re = new DBRow();
//				re.add("resources_type", re_type);
//				re.add("resources_id", re_id);
//				re.add("resources_name", windowCheckInHandleOccupyResourceName(re_type, re_id));
//				re.add("occupy_status", equipRes[i].get("occupy_status",0));
//				res.add(re);
//				equipRes[i].add("resources", res);
//				resultMaps.put(equipId, equipRes[i]);
//			}
//			else
//			{
//				List<DBRow> res = (ArrayList<DBRow>)resultMaps.get(equipId).get("resources", new ArrayList<DBRow>());
//				DBRow re = new DBRow();
//				re.add("resources_type", re_type);
//				re.add("resources_id", re_id);
//				re.add("resources_name", windowCheckInHandleOccupyResourceName(re_type, re_id));
//				re.add("occupy_status", equipRes[i].get("occupy_status",0));
//				res.add(re);
//			}
		}
		
//		Set<Entry<Long, DBRow>> eqs = resultMaps.entrySet();
//		List<DBRow> results = new ArrayList<DBRow>();
//		for (Iterator iterator = eqs.iterator(); iterator.hasNext();) {
//			Entry<Long, DBRow> entry = (Entry<Long, DBRow>) iterator.next();
//			DBRow equipment = entry.getValue();
//			List<DBRow> resources = (List<DBRow>) equipment.get("resources", new ArrayList<DBRow>());
//			equipment.add("resources", resources.toArray(new DBRow[0]));
//			results.add(equipment);
//		}
//		DBRow[] resultEqs = results.toArray(new DBRow[0]);
		////system.out.println(StringUtil.convertDBRowArrayToJsonString(equipRes));
		return equipRes;
	
	}
	
	
	/**
	 * 通过设备号查询在yardDrop设备
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午9:38:48
	 */
	public DBRow[] findInYardDropEquipmentsByNumber(HttpServletRequest request) throws Exception
	{
		try
		{
			String number = StringUtil.getString(request, "number");
			AdminMgr adminMgr		= new AdminMgr();
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			return findInYardDropEquipmentsByNumber(number, adminLoginBean.getPs_id(),CheckInTractorOrTrailerTypeKey.TRAILER);
		}
		catch(Exception e){
			throw new Exception("findInYardDropEquipmentsByNumber"+e);
		}
	}
	
	/**
	 * 通过设备号查询在yardDrop设备，非本entry
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午9:38:48
	 */
	public DBRow[] findInYardDropEquipmentsByNumberNotThisEntry(HttpServletRequest request) throws Exception
	{
		try
		{
			String number = StringUtil.getString(request, "number");
			long entry_id = StringUtil.getLong(request, "entry_id");
			AdminMgr adminMgr		= new AdminMgr();
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			return findInYardDropOffEquipmentByNumberNotThisEntry(number, adminLoginBean.getPs_id(),entry_id);
		}
		catch(Exception e){
			throw new Exception("findInYardDropEquipmentsByNumber"+e);
		}
	}
	
	/**
	 * 通过设备ID查task
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月2日 上午9:27:22
	 */
	public DBRow[] findTasksByEquipmentId(long equipment_id) throws Exception
	{
		try
		{
			return floorCheckInMgrZyj.findTasksByEquipmentId(equipment_id);
		}
		catch(Exception e){
			throw new Exception("findTasksByEquipmentId"+e);
		}
	}
	
	/**
	 * 通过task添加order信息
	 * @param taskId
	 * @param adid
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 上午10:06:15
	 */
	public void addTaskOrdersByTaskId(long taskId, long adid) throws Exception
	{
		DBRow detailRow = findTaskEquipmentResourceByDetailId(taskId);
		//删除之前保存的task下的order
		floorCheckInMgrZyj.deleteWmsOrderInfosByTaskId(taskId);
		//获取task下最新的order
		DBRow[] orders = sqlServerMgrZJ.findOrdersByNumberAndType(detailRow.get("number_type", 0), detailRow.getString("number"), detailRow.getString("company_id") , detailRow.getString("customer_id"));
		for (int i = 0; i < orders.length; i++) 
		{
			DBRow row = new DBRow();
			row.add("order_no", orders[i].get("OrderNo", 0L));
			row.add("company_id", orders[i].getString("CompanyID"));
			row.add("customer_id", orders[i].getString("CustomerID"));
			row.add("po_no", orders[i].getString("PONo"));
			if(orders[i].get("MasterBOLNo", 0L) != 0)
			{
				row.add("load_no", orders[i].getString("LoadNo"));
			}
			row.add("master_bol_no", orders[i].get("MasterBOLNo", 0L));
			row.add("task_detail_id", taskId);
			row.add("status", orders[i].getString("Status"));
			row.add("ship_to_id", orders[i].getString("ShipToID"));
			row.add("ship_to_name", orders[i].getString("ShipToName"));
			row.add("ship_to_address1", orders[i].getString("ShipToAddress1"));
			row.add("ship_to_address2", orders[i].getString("ShipToAddress2"));
			row.add("ship_to_city", orders[i].getString("ShipToCity"));
			row.add("ship_to_state", orders[i].getString("ShipToState"));
			row.add("ship_to_zip_code", orders[i].getString("ShipToZipCode"));
			row.add("ship_to_country", orders[i].getString("ShipToCountry"));
			row.add("ship_to_contact", orders[i].getString("ShipToContact"));
			row.add("ship_to_phone", orders[i].getString("ShipToPhone"));
			row.add("ship_to_store_no", orders[i].getString("ShipToStoreNo"));
			row.add("freight_term", orders[i].getString("FreightTerm"));
			row.add("carrier_id", orders[i].getString("CarrierID"));
			row.add("picking_type", orders[i].getString("PickingType"));
			row.add("appointment_date", orders[i].getString("AppointmentDate"));
			row.add("staging_area_id", orders[i].getString("StagingAreaID"));
			row.add("pro_no", orders[i].getString("ProNo"));
			row.add("seals", orders[i].getString("Seals"));
			row.add("creator", adid);
			row.add("create_time", DateUtil.NowStr());
			floorCheckInMgrZyj.addWmsOrderInfo(row);
		}
	}
	
	/**
	 * 更改equipment类型
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月16日 下午1:09:23
	 */
	public void windowCheckInChangeEquipmentType(HttpServletRequest request) throws Exception
	{
		long equipment_id	= StringUtil.getLong(request, "equipment_id");
		int equipment_purpose	= StringUtil.getInt(request, "equipment_purpose");
		DBRow equipment		= findEquipmentReOccupyById(equipment_id);
		AdminMgr adminMgr 	= new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		long adid			= adminLoginBean.getAdid();
		String adidName		= adminLoginBean.getEmploye_name();
		String resource_name= windowCheckInHandleOccupyResourceName(equipment.get("resources_type", 0), equipment.get("resources_id",0L));
		
		DBRow row			= new DBRow();
		row.add("equipment_purpose", equipment_purpose);
		floorCheckInMgrZyj.updateEquipment(equipment_id, row);
		
		windowCheckInAddEquipmentLog("update", equipment_purpose, equipment.getString("equipment_number"), equipment.get("check_in_entry_id",0L)
				, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery"), equipment.getString("seal_pick_up"), "");
	}
	
	/**
	 * web端添加或者更新设备
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午4:19:19
	 */
	public DBRow webAddOrUpdateEquipment(HttpServletRequest request) throws Exception
	{
		long equipment_id		= StringUtil.getLong(request, "equipment_id");
		long check_in_entry_id	= StringUtil.getLong(request, "check_in_entry_id");
		int is_check_success	= StringUtil.getInt(request, "is_check_success");
		String equipment_number	= StringUtil.getString(request, "equipment_number");
		long before_equipment_id= StringUtil.getLong(request, "before_equipment_id");
		int equipment_type		= StringUtil.getInt(request, "equipment_type");
		int equipment_purpose	= StringUtil.getInt(request, "equipment_purpose");
		String seal_delivery	= StringUtil.getString(request, "seal_delivery");
		String seal_pick_up		= StringUtil.getString(request, "seal_pick_up");
		int occupy_type			= StringUtil.getInt(request, "occupy_type");
		long occupy_id			= StringUtil.getLong(request, "occupy_id");
		
		JSONObject json			= new JSONObject();
		json.put("equipment_id", equipment_id);
		json.put("check_in_entry_id", check_in_entry_id);
		json.put("is_check_success", is_check_success);
		json.put("equipment_number", equipment_number);
		json.put("before_equipment_id", before_equipment_id);
		json.put("equipment_type", equipment_type);
		json.put("equipment_purpose", equipment_purpose);
		json.put("seal_delivery", seal_delivery);
		json.put("seal_pick_up", seal_pick_up);
		json.put("occupy_type", occupy_type);
		json.put("occupy_id", occupy_id);
		
		AdminMgr adminMgr 		= new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		return addOrUpdateEquipment(json, adminLoginBean);
	}
	
	/**
	 * 通过Json更新或者添加设备
	 * @param json
	 * @param adminLoginBean
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午4:19:35
	 */
	public DBRow addOrUpdateEquipment(JSONObject json, AdminLoginBean adminLoginBean) throws EquipmentHadOutException,Exception
	{
		//PickUp设备是否是当前entry的其他用途设备
		long equipment_id		= StringUtil.getJsonLong(json, "equipment_id");
		long check_in_entry_id	= StringUtil.getJsonLong(json, "check_in_entry_id");
		int is_check_success	= StringUtil.getJsonInt(json, "is_check_success");
		String equipment_number	= StringUtil.getJsonString(json, "equipment_number");
		int equipment_purpose	= StringUtil.getJsonInt(json, "equipment_purpose");
		boolean flag 			= false;
		DBRow equipment			= null;
		long adid				= adminLoginBean.getAdid();
		String adidName			= adminLoginBean.getEmploye_name();
		long ps_id				= adminLoginBean.getPs_id();
		String resource_name	= "";
		if(equipment_id > 0) //修改设备
		{
			equipment			= findEquipmentReOccupyById(equipment_id);
			//如果设备已经出去了，不能修改
			if(!StrUtil.isBlank(equipment.getString("check_out_time"))
//					|| equipment.get("equipment_status", 0)==CheckInMainDocumentsStatusTypeKey.INYARD
					|| equipment.get("equipment_status", 0)==CheckInMainDocumentsStatusTypeKey.LEFT)
				throw new EquipmentHadOutException("\""+equipment.getString("equipment_number")+"\"");
			long before_equipment_id = StringUtil.getJsonLong(json, "before_equipment_id");
			resource_name= windowCheckInHandleOccupyResourceName(equipment.get("resources_type", 0), equipment.get("resources_id",0L));
			//如果是pickup添加设备
			//PickUp时验证设备号重不重，重的话要提示跟哪些重，要不要left那些设备
			DBRow sameInfo = windowCheckInSameCtnrPickUp(equipment_number, equipment_id,check_in_entry_id, adminLoginBean, is_check_success);
			if(null != sameInfo)
			{
				return sameInfo;
			}
			flag = windowCheckInEntryAddPickUpCtnr(equipment_id,before_equipment_id, check_in_entry_id, adid,adidName,resource_name,equipment_number, adminLoginBean.getPs_id(), equipment_purpose);
			
		}
		if(equipment_id == 0 && equipment_purpose == CheckInLiveLoadOrDropOffKey.PICK_UP)
		{
			DBRow[] equipmentInYards = findInYardDropOffEquipmentByNumberNotThisEntry(equipment_number, ps_id, check_in_entry_id);
			if(equipmentInYards.length == 0)
			{
				throw new EquipmentNotFindException();
			}
			equipment_id = equipmentInYards[0].get("equipment_id",0L);
			//验证设备是否可以带走
			DBRow row = new DBRow();
			row.add("check_out_entry_id", check_in_entry_id);
			floorCheckInMgrZyj.updateEquipment(equipmentInYards[0].get("equipment_id",0L), row);
			windowCheckInAddEquipmentLog("update", equipmentInYards[0].get("equipment_type", 0), equipmentInYards[0].getString("equipment_number"), check_in_entry_id
					, adid, adidName, equipmentInYards[0].get("resources_type", 0), resource_name, equipmentInYards[0].getString("seal_delivery")
					, equipmentInYards[0].getString("seal_pick_up"), " PickUp CTNR : "+equipmentInYards[0].getString("equipment_number"));
			//更新entry下没有windowCheckIn时间的设备 		2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
	//		windowCheckInUpdateMainTimeEquipment(check_in_entry_id, adid);
			
			flag = true;
		}
		CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
		if(!flag)
		{
			int equipment_type		= StringUtil.getJsonInt(json, "equipment_type");
			String seal_delivery	= StringUtil.getJsonString(json, "seal_delivery");
			String seal_pick_up		= StringUtil.getJsonString(json, "seal_pick_up");
			int occupy_type			= StringUtil.getJsonInt(json, "occupy_type");
			long occupy_id			= StringUtil.getJsonLong(json, "occupy_id");
			
			int occupy_status		= OccupyStatusTypeKey.RESERVERED;
			if(occupy_type == OccupyTypeKey.SPOT)
			{
				occupy_status = OccupyStatusTypeKey.OCUPIED;
			}
			seal_delivery = StrUtil.isBlank(seal_delivery)?"NA":seal_delivery;
			seal_pick_up = StrUtil.isBlank(seal_pick_up)?"NA":seal_pick_up;
			//entry主单据
			DBRow mRow=floorCheckInMgrZwb.findGateCheckInById(check_in_entry_id);
			DBRow row = new DBRow();
			row.add("equipment_type", equipment_type);
			row.add("equipment_number", equipment_number);
			row.add("equipment_purpose", equipment_purpose);
			row.add("seal_delivery", seal_delivery);
			row.add("seal_pick_up", seal_pick_up);
			row.add("phone_equipment_number", StringUtil.convertStr2PhotoNumber(equipment_number));
			
			if(0==equipment_id)
			{
				//TODO 处理设备number重复的问题，如果添加新的，需要把之前的left并释放资源
				DBRow equipmentNumberSameRow = windowCheckInSameCtnr(equipment_number, check_in_entry_id, adminLoginBean,null,is_check_success,"A");
				if(null != equipmentNumberSameRow)
				{
					return equipmentNumberSameRow;
				}
				row.add("check_in_entry_id", check_in_entry_id);
				row.add("equipment_status", CheckInMainDocumentsStatusTypeKey.UNPROCESS);
				row.add("ps_id", ps_id);
				resource_name= windowCheckInHandleOccupyResourceName(occupy_type, occupy_id);
				row.add("check_in_time", null!=mRow?mRow.getString("gate_check_in_time"):null);
				row.add("check_in_window_time",  !StrUtil.isBlank(mRow.getString("window_check_in_time"))?mRow.getString("window_check_in_time"):null);
				equipment_id = floorCheckInMgrZyj.addEquipment(row);
				//TODO 如果是liveLoad设备处理所有设备
				windowCheckInHandleLiveLoadResources(check_in_entry_id, equipment_purpose, occupy_type, occupy_id, occupy_status, resource_name, adid);
				windowCheckInAddEquipmentLog("create", equipment_type, equipment_number, check_in_entry_id
						, adid, adidName, occupy_type, resource_name, seal_delivery, seal_pick_up, "");
				
				
			}
			else
			{
				//TODO 处理设备number重复的问题
				DBRow equipmentNumberSameRow = windowCheckInSameCtnr(equipment_number, check_in_entry_id, adminLoginBean, equipment,is_check_success,"U");
				if(null != equipmentNumberSameRow)
				{
					return equipmentNumberSameRow;
				}
//				windowCheckInCheckEquipmentDelete(equipment_id, check_in_entry_id, equipment);
				//设备之后占用门，不能修改门名称 
//				if(!equipment_number.equals(equipment.getString("equipment_number"))
//						&& equipment.get("occupy_status", 0)==OccupyStatusTypeKey.OCUPIED
//						&& equipment.get("resources_type", 0) == OccupyTypeKey.DOOR)
//					throw new EquipmentOccupyResourcesException();
				
				row.add("check_in_window_time", !StrUtil.isBlank(mRow.getString("window_check_in_time"))?mRow.getString("window_check_in_time"):null);
				if(equipment_purpose == CheckInLiveLoadOrDropOffKey.PICK_UP)
				{
					row.remove("equipment_type");
					row.remove("equipment_purpose");
					row.remove("equipment_number");
					row.remove("seal_delivery");
					row.remove("seal_pick_up");
					row.remove("phone_equipment_number");
				}
				if(equipment.get("resources_type", 0) == occupy_type)
				{
					occupy_status = equipment.get("occupy_status", 0);
				}
				floorCheckInMgrZyj.updateEquipment(equipment_id, row);
				//TODO 如果是liveLoad设备处理所有设备
				windowCheckInHandleLiveLoadResources(check_in_entry_id, equipment_purpose, occupy_type, occupy_id, occupy_status, resource_name, adid);
				windowCheckInAddEquipmentLog("update", equipment_type, equipment_number, check_in_entry_id
						, adid, adidName, occupy_type, resource_name, seal_delivery, seal_pick_up, "");
				
			}
			
			//更新entry下没有windowCheckIn时间的设备   2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
		//	windowCheckInUpdateMainTimeEquipment(check_in_entry_id, adid);
			
			if(occupy_id == 0 && equipment != null && equipment.get("resources_id", 0L) != 0L)
			{
				ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipment.get("equipment_id", 0L), adid);
			}
			else
			{
				if(occupy_id > 0)
				{
					OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
					if(!ymsMgrAPI.resourceJudgeCanUse(ModuleKey.CHECK_IN, check_in_entry_id, occupy_type, occupy_id))
						throw new ResourceHasUsedException(occupyTypeKey.getOccupyTypeKeyName(occupy_type)+":"+resource_name);
					
					ymsMgrAPI.operationSpaceRelation(occupy_type, occupy_id, SpaceRelationTypeKey.Equipment, equipment_id
							, ModuleKey.CHECK_IN, check_in_entry_id, occupy_status, adid);
				}
			}
		}
		//updateIndex
		checkInMgrZwb.editCheckInIndex(check_in_entry_id, "update");
		return findEquipmentReOccupyById(equipment_id);
	}
	
	/**
	 * web端删除设备
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午4:35:26
	 */
	public int webDeleteEquipment(HttpServletRequest request) throws Exception
	{
		int isCheckSuccess = StringUtil.getInt(request, "is_check_success");
		long equipment_id = StringUtil.getLong(request, "id");
		JSONObject json = new JSONObject();
		json.put("is_check_success", isCheckSuccess);
		json.put("id", equipment_id);
		AdminMgr adminMgr		= new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		return deleteEquipment(json, adminLoginBean);
	}
	
	/**
	 * 删除设备
	 * @param json
	 * @param adminLoginBean
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午4:35:48
	 */
	public int deleteEquipment(JSONObject json, AdminLoginBean adminLoginBean)throws Exception
	{
		int isCheckSuccess = StringUtil.getJsonInt(json, "is_check_success");
		long equipment_id = StringUtil.getJsonLong(json, "id");
		DBRow equipment = findEquipmentReOccupyById(equipment_id);
		long check_in_entry_id = equipment.get("check_in_entry_id", 0L);
		long entry_id_check_out = equipment.get("check_out_entry_id", 0L);
		if(!StrUtil.isBlank(equipment.getString("check_out_time"))
//				|| equipment.get("equipment_status", 0)==CheckInMainDocumentsStatusTypeKey.INYARD
				|| equipment.get("equipment_status", 0)==CheckInMainDocumentsStatusTypeKey.LEFT) 
			throw new EquipmentHadOutException("\""+equipment.getString("equipment_number")+"\"");
		
		long adid				= adminLoginBean.getAdid();
		String adidName			= adminLoginBean.getEmploye_name();
		if(equipment.get("check_out_entry_id", 0L) != 0 && equipment.get("check_in_entry_id", 0L) != equipment.get("check_out_entry_id", 0L))
		{
			String resource_name = windowCheckInHandleOccupyResourceName(equipment.get("resources_type", 0), equipment.get("resources_id",0L));
			DBRow row = new DBRow();
			row.add("check_out_entry_id", null);
			floorCheckInMgrZyj.updateEquipment(equipment_id, row);
			windowCheckInAddEquipmentLog("update", equipment.get("equipment_type", 0), equipment.getString("equipment_number"), check_in_entry_id
					, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery")
					, equipment.getString("seal_pick_up"), " Cancel PickUp CTNR : "+equipment.getString("equipment_number")+" By "+entry_id_check_out);
			
			windowCheckInAddEquipmentLog("update", equipment.get("equipment_type", 0), equipment.getString("equipment_number"), entry_id_check_out
					, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery")
					, equipment.getString("seal_pick_up"), " Cancel PickUp CTNR : "+equipment.getString("equipment_number"));
			
			return YesOrNotKey.YES;
		}
		if(isCheckSuccess != YesOrNotKey.YES)
		{
			//在不能删除的时候，判断是否能删除，能删除就删除
			isCheckSuccess = windowCheckInCheckEquipmentDelete(equipment_id, equipment.get("check_in_entry_id", 0L), equipment);
		}
		if(isCheckSuccess == YesOrNotKey.YES)
		{
			//如果是本单据添加的设备可以删除，否则不能删除。非本单据的设备，将out_entry处理
			String resource_name = windowCheckInHandleOccupyResourceName(equipment.get("resources_type", 0), equipment.get("resources_id",0L));
			windowCheckInAddEquipmentLog("delete", equipment.get("equipment_type", 0), equipment.getString("equipment_number"), equipment.get("check_in_entry_id", 0L)
					, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery")
					, equipment.getString("seal_pick_up"), "");
			ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipment_id, adid);
			DBRow[] tasks = floorCheckInMgrZyj.findTasksByEquipmentId(equipment_id);
			if(tasks.length > 0)
			{
				for (int i = 0; i < tasks.length; i++)
				{
					long detail_id = tasks[i].get("dlo_detail_id", 0L);
					ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Task, detail_id, adid);
					//处理是否删除业务单据
					orderSystemMgr.deleteOrNotOrderSystemByDetailInfos(detail_id);
					windowCheckInAddTaskLog("delete",tasks[i].getString("number"),tasks[i].get("number_type",0), check_in_entry_id, adid,adidName
							,tasks[i].get("resources_type", 0), resource_name,"",tasks[i].getString("seal_delivery")
							,tasks[i].getString("seal_pick_up"), tasks[i].get("equipment_type", 0), tasks[i].getString("equipment_number"));
					deleteTaskAndScheduleLog(adminLoginBean, detail_id);
				}
			}
			floorCheckInMgrZyj.deleteEquipment(equipment_id);
			CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
			checkInMgrZwb.editCheckInIndex(check_in_entry_id, "update");
		}
		//更新entry下没有windowCheckIn时间的设备	 2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
	//	windowCheckInUpdateMainTimeEquipment(check_in_entry_id, adid);
		return isCheckSuccess;
	}
	
	/**
	 * web端windowCheckIn添加task
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午5:08:44
	 */
	public void webCheckInWindowAddOrUpdateTask(HttpServletRequest request) throws Exception
	{
		String str				= StringUtil.getString(request,"str");//添加或修改的子单据数据
		JSONObject json			= new JSONObject(str);
		//获得当前登录账号信息,操作时间
		AdminMgr adminMgr		= new AdminMgr();
		json.put("is_check_in", StringUtil.getInt(request,"is_check_in"));
		json.put("priority", StringUtil.getInt(request,"priority"));
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		checkInWindowAddOrUpdateTask(json, adminLoginBean);
	}
	
	/**
	 * 添加task
	 * @param json
	 * @param adminLoginBean
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午5:08:57
	 */
	public void checkInWindowAddOrUpdateTask(final JSONObject json, final AdminLoginBean adminLoginBean) throws Exception
	{
		sqlServerTransactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(
					TransactionStatus txStat) {
				try
				{
					//处理task
					windowCheckInHandleTasks(json,adminLoginBean);
					//处理主单据
					windowCheckInHandleEntrys(json, adminLoginBean);
				}
				catch(EquipmentHadOutException e){
					throw new EquipmentHadOutException();
				}catch(TaskNoAssignWarehouseSupervisorException e){
					throw new TaskNoAssignWarehouseSupervisorException();
				}catch(DoorHasUsedException e1){
					throw new DoorHasUsedException();
				} catch (SpotHasUsedException e) {
					throw new SpotHasUsedException();
				} catch (CheckInTaskCanntDeleteException e) {
					throw new CheckInTaskCanntDeleteException();
				} catch (CheckInTaskRepeatToEntryException e) {
					throw new CheckInTaskRepeatToEntryException();
				} catch (CheckInTaskRepeatToOthersException e) {
					throw new CheckInTaskRepeatToOthersException();
				} catch (CheckInMustHasTasksException e) {
					throw new CheckInMustHasTasksException();
				} catch (CheckInAllTasksClosedException e) {
					throw new CheckInAllTasksClosedException();
				} catch (CheckInMustNotHaveTasksException e) {
					throw new CheckInMustNotHaveTasksException();
				}catch (CheckInTaskHaveDeletedException e) {
					throw new CheckInTaskHaveDeletedException();
				}catch (EquipmentHasLoadedOrdersException e) {
					throw new EquipmentHasLoadedOrdersException();
				} catch(EquipmentNotFindException e) {
					throw new EquipmentNotFindException();
				} catch(DoorNotFindException e) {
					throw new DoorNotFindException();
				} catch(SpotNotFindException e) {
					throw new SpotNotFindException();
				} catch(CheckinTaskNotFoundException e) {
					throw new CheckinTaskNotFoundException();
				}catch(Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
	}
	
	/**
	 * 处理tasks
	 * @param json
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午4:17:04
	 */
	private DBRow windowCheckInHandleTasks(JSONObject json, AdminLoginBean adminLoginBean)throws Exception
	{
		DBRow result		= new DBRow();
		long mainId			= StringUtil.getJsonLong(json, "mainId");
		JSONArray items		= StringUtil.getJsonArrayFromJson(json, "items");
		long adid			= adminLoginBean.getAdid();
		String adidName		= adminLoginBean.getEmploye_name();
		for(int i=0;i<items.length();i++)
	    {
			DBRow row			= windowCheckInSubmitJsonToDBRow(items.getJSONObject(i), mainId, adminLoginBean);
			windowCheckInCheckTaskNumber(row);//同一单据在同一设备同一资源上不能重
			String number_order_status = row.getString("number_order_status");
			int order_system_type =  row.get("order_system_type", 0);
	    	long detailId		= row.get("dlo_detail_id", 0L);
	    	if(detailId > 0)
	    	{
	    		windowCheckInHandleTaskCheck(detailId, mainId);//关了的单据，或者已经开始load的单据不能修改
	    	}
	    	int number_type		= row.get("number_type", 0);
	    	String number		= row.getString("number");
	    	String notePersonIds= row.getString("ids");
	    	int resource_type	= row.get("occupancy_type", 0);
	    	long resource_id	= row.get("rl_id", 0L);
	    	String resource_name= windowCheckInHandleOccupyResourceName(resource_type, resource_id);
	    	long equipment_id	= row.get("equipment_id", 0L);
	    	DBRow equipment		= findEquipmentReOccupyById(equipment_id);
	    	int equipmentStatus = equipment.get("equipment_status", 0);
	    	if(equipmentStatus == CheckInMainDocumentsStatusTypeKey.LEFT)
	    		throw new EquipmentHadOutException();
	    	
	    	//task日志内容
	    	String context		= windowCheckInHandleTaskContext(number_type, number, resource_type, resource_name
	    							, row.getString("note"), equipment.get("equipment_type", 0), equipment.getString("equipment_number"));
	    	
	    	//根据主单据id 和子单据id 查询子单据 是否存在
	    	String loadData="";//将load的MasterBolNo和OrderNo写到checkin明细表中
	    	if(number_type==ModuleKey.CHECK_IN_LOAD){
	    		loadData = sqlServerMgrZJ.findMasterBolNoOrderNoStrByLoadNo(number,adid);
	    	}
	    	row.add("master_order_nos", loadData);
	    	row.remove("yj"); row.remove("dx"); row.remove("ym"); row.remove("ids"); row.remove("dlo_detail_id");row.remove("note");
	    	row.remove("ra");row.remove("occupancy_type");row.remove("rl_id");row.remove("number_order_status");row.remove("order_system_type");
	    	if(detailId > 0)
	    	{
	    		//处理资源和设备
	    		DBRow taskHandle = windowCheckInHandleResources(detailId, detailId, number_type, number,resource_type, resource_id, equipment_id, mainId, adminLoginBean);
	    		row.append(taskHandle);
	    		floorCheckInMgrZwb.updateDetailByIsExist(detailId,row);	
	    		//添加日志
	    		windowCheckInAddTaskLog("update",number,number_type , mainId, adid,adidName,resource_type, resource_name,notePersonIds,equipment.getString("seal_delivery"),equipment.getString("seal_pick_up"), equipment.get("equipment_type", 0), equipment.getString("equipment_number"));
	    		//删除通知，先查询通知
	    		deleteTaskWindowScheduleByDetailId(detailId, adminLoginBean);
	    		
	    	}
	    	else
	    	{
	    		row.add("number_status",1);
	    		row.add("create_time", DateUtil.NowStr());
	    		row.add("creator", adid);
	    		//处理资源和设备
	    		detailId=floorCheckInMgrZwb.addOccupancyDetails(row);
	    		//处理设备状态
	    		if(equipment.get("equipment_status", 0) == CheckInMainDocumentsStatusTypeKey.LEAVING 
	    				|| equipment.get("equipment_status", 0) == CheckInMainDocumentsStatusTypeKey.INYARD)
	    		{
	    			DBRow updateEquipment = new DBRow();
	    			updateEquipment.add("equipment_status", CheckInMainDocumentsStatusTypeKey.PROCESSING);
	    			floorCheckInMgrZyj.updateEquipment(equipment_id, updateEquipment);
	    		}
	    		windowCheckInHandleResources(detailId, 0L, number_type, number,resource_type, resource_id, equipment_id, mainId, adminLoginBean);
	    		windowCheckInAddTaskLog("add",number,number_type, mainId, adid,adidName,resource_type, resource_name,notePersonIds,equipment.getString("seal_delivery"),equipment.getString("seal_pick_up"), equipment.get("equipment_type", 0), equipment.getString("equipment_number"));
	    	}
	    	
	    	windowCheckInHandleTaskEquipmentRelType(equipment_id, mainId);
	    	//处理单据
	    	DBRow orderSystem = orderSystemMgr.addOrUpdateOrderSystems(detailId, adminLoginBean.getPs_id(), number_order_status, order_system_type, adminLoginBean);
	    	if(null!=orderSystem)
	    	{
	    		DBRow updateDetail = new DBRow();
	    		updateDetail.add("lr_id", orderSystem.get("lr_id",0));
	    		floorCheckInMgrZwb.updateDetailByIsExist(detailId,updateDetail);	
	    	}
	    	//回写wms
	    	updateReceiptsByCtnOrBols(mainId);
	    	CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
	    	checkInMgrZwb.editCheckInIndex(mainId, "update");
	    	//发通知，上面删除，此处添加
	    	if(!notePersonIds.equals("")){
//	    		scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(),"",adid,notePersonIds,"",detailId, ModuleKey.CHECK_IN, emailTitle, context, ym, yj, dx,request,true,ProcessKey.CHECK_IN_WINDOW);			    	
	    		scheduleMgrZr.addSchedule(ScheduleModel.getCheckInScheduleModel(mainId, detailId,ProcessKey.CHECK_IN_WINDOW, adid, notePersonIds,context,number,number_type));
	    	}
	    	CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr)MvcUtil.getBeanFromContainer("proxyCheckInMgrZr");  
	    	DBRow[] schedules = this.floorCheckInMgrZyj.findWindowSchedule(mainId,detailId);
	    	if (schedules != null && schedules.length > 0) {
				for (DBRow temp : schedules) {
					long schedule_id = temp.get("schedule_id", 0l);
					checkInMgrZr.finishSchedule(schedule_id, adid);
				}
			}
	    }
		
		return result;//result没有属性
	}
	
	
	public boolean windowCheckInCheckChange(DBRow before, DBRow now) throws Exception
	{
		boolean result = false;
		long detail_id = now.get("dlo_detail_id", 0L);
		if(detail_id > 0)
		{
			String number_bf		= before.getString("number");
			int number_type_bf		= before.get("number_type", 0);
			String company_id_bf	= before.getString("company_id");
			String customer_id_bf	= before.getString("customer_id");
			String supplier_id_bf	= before.getString("supplier_id");
			String account_id_bf	= before.getString("account_id");
			long equipment_id_bf	= before.get("equipment_id", 0L);
			long receipt_no_bf		= before.get("receipt_no", 0L);
			long lr_id_bf			= before.get("lr_id", 0L);
			int resources_type_bf	= before.get("resources_type", 0);
			long resources_id_bf	= before.get("resources_id", 0L);
			
			String number_nw		= now.getString("number");
			int number_type_nw		= now.get("number_type", 0);
			String company_id_nw	= now.getString("company_id");
			String customer_id_nw	= now.getString("customer_id");
			String supplier_id_nw	= now.getString("supplier_id");
			String account_id_nw	= now.getString("account_id");
			long equipment_id_nw	= now.get("equipment_id", 0L);
			long receipt_no_nw		= now.get("receipt_no", 0L);
			long lr_id_nw			= now.get("lr_id", 0L);
			int resources_type_nw	= now.get("resources_type", 0);
			long resources_id_nw	= now.get("resources_id", 0L);
			
			if(!number_bf.equals(number_nw) || number_type_bf != number_type_nw || !company_id_bf.equals(company_id_nw)
				|| !customer_id_bf.equals(customer_id_nw) || !supplier_id_bf.equals(supplier_id_nw) || !account_id_bf.equals(account_id_nw)
				|| equipment_id_bf != equipment_id_nw || receipt_no_bf != receipt_no_nw || lr_id_bf != lr_id_nw
				|| resources_type_bf != resources_type_nw || resources_id_bf != resources_id_nw)
			{
				result = true;
			}
		}
		return result;
	}
	
	
	
	
	
	/**
	 * 处理明细占用的资源和设备
	 * @param detailIdNow
	 * @param detailIdBef
	 * @param number_type
	 * @param number
	 * @param resource_type
	 * @param resource_id
	 * @param equipment_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午6:26:42
	 */
	public DBRow windowCheckInHandleResources(long detailIdNow, long detailIdBef, int number_type, String number
			,int resource_type, long resource_id, long equipment_id, long mainId,AdminLoginBean adminLoginBean) throws Exception
	{
		long adid = adminLoginBean.getAdid();
		DBRow result = new DBRow();
		//更新
		if(detailIdNow == detailIdBef)
		{
			//单据号变了，子单据状态需要改,门需要改；如果门变了，需要改变门状态，子单据
			DBRow detailRowExist = findTaskEquipmentResourceByDetailId(detailIdNow);
			//在其他界面删除了
			if(null == detailRowExist) throw new CheckInTaskHaveDeletedException();
			long occupiedTypeId	= detailRowExist.get("resources_id", 0L);
			int occupiedTypeKey	= detailRowExist.get("resources_type", 0);
			String numberExist	= detailRowExist.getString("number");
			int numberTypeExist	= detailRowExist.get("number_type", 0);
			long equipmentExist	= detailRowExist.get("equipment_id", 0L);
			
			//单号或者门或者设备改变了，如果没有单据明细，将单据状态为未处理，门占用状态改成预留，将wareHouse任务删除；有单据明细提示不能修改，让warehouse处理后才能修改
			if(!numberExist.equals(number) || numberTypeExist != number_type || equipmentExist != equipment_id)
//					|| occupiedTypeId != resource_id || occupiedTypeKey != resource_type
			{
				//单据或者门改变，将单据状态为未处理，门占用状态改成预留
				result.add("number_status", CheckInChildDocumentsStatusTypeKey.UNPROCESS);//改子单据状态
				//删除warehouse任务，通知
//				deleteTaskWarehouseScheduleByDetailId(detailIdBef, adminLoginBean);
			}
			if(occupiedTypeId != resource_id || occupiedTypeKey != resource_type)
			{
//				deleteTaskWarehouseScheduleByDetailId(detailIdBef, adminLoginBean);
				ymsMgrAPI.operationSpaceRelation(resource_type, resource_id, SpaceRelationTypeKey.Task, detailIdNow
						, ModuleKey.CHECK_IN, mainId, OccupyStatusTypeKey.RESERVERED, adid);
			}
			deleteTaskWarehouseScheduleByDetailId(detailIdBef, adminLoginBean);
		}
		else
		{
			ymsMgrAPI.operationSpaceRelation(resource_type, resource_id, SpaceRelationTypeKey.Task, detailIdNow
					, ModuleKey.CHECK_IN, mainId, OccupyStatusTypeKey.RESERVERED, adid);
		}
		return result;
	}
	
	/**
	 * web端删除task
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午5:17:46
	 */
	public void webWindowCheckinDeleteTask(HttpServletRequest request) throws Exception
	{
		long entry_id = StringUtil.getLong(request, "entry_id");
		long detail_id = StringUtil.getLong(request, "detail_id");
		JSONObject json = new JSONObject();
		json.put("entry_id", entry_id);
		json.put("detail_id", detail_id);
		AdminMgr adminMgr = new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		
		windowCheckInDeleteTask(json, adminLoginBean);
		
	}
	
	/**
	 * 删除task
	 * @param json
	 * @param adminLoginBean
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午5:18:04
	 */
	public void windowCheckInDeleteTask(JSONObject json, AdminLoginBean adminLoginBean) throws Exception
	{
		long entry_id = StringUtil.getJsonLong(json, "entry_id");
		long detail_id = StringUtil.getJsonLong(json, "detail_id");
		DBRow task = findTaskEquipmentResourceByDetailId(detail_id);
		if(null == task) throw new CheckinTaskNotFoundException();
		windowCheckInHandleTaskCheck(detail_id, entry_id);
		//如果是本单据添加的设备可以删除，否则不能删除。非本单据的设备，将out_entry处理
		long adid				= adminLoginBean.getAdid();
		String adidName			= adminLoginBean.getEmploye_name();
		//重算设备的rel_type
		String resource_name= windowCheckInHandleOccupyResourceName(task.get("resources_type", 0), task.get("resources_id", 0));
		windowCheckInAddTaskLog("delete",task.getString("number"),task.get("number_type",0), entry_id, adid,adidName
				,task.get("resources_type", 0), resource_name,"",task.getString("seal_delivery")
				,task.getString("seal_pick_up"), task.get("equipment_type", 0), task.getString("equipment_number"));
		ymsMgrAPI.releaseResourcesByRelation(SpaceRelationTypeKey.Task, detail_id, adid);
		//处理是否删除业务单据
		orderSystemMgr.deleteOrNotOrderSystemByDetailInfos(detail_id);
		deleteTaskAndScheduleLog(adminLoginBean, detail_id);
		//更新entry下没有windowCheckIn时间的设备    2015-1-29  只有check in 与waiting(warehouse waiting)时添加windowCheckIn的时间   wfh
//		windowCheckInUpdateMainTimeEquipment(entry_id, adid);
		//删除task
		windowCheckInHandleTaskEquipmentRelType(task.get("equipment_id",0L), entry_id);
		CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
		checkInMgrZwb.editCheckInIndex(entry_id, "update");
	}
	
	/**
	 * 更改equipment类型
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月16日 下午1:09:23
	 */
	public void webWindowCheckInChangeEquipmentType(HttpServletRequest request) throws Exception
	{
		long equipment_id		= StringUtil.getLong(request, "equipment_id");
		int equipment_purpose	= StringUtil.getInt(request, "equipment_purpose");
		JSONObject json			= new JSONObject();
		json.put("equipment_id", equipment_id);
		json.put("equipment_purpose", equipment_purpose);
		
		AdminMgr adminMgr 		= new AdminMgr();
		AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
		webWindowCheckInChangeEquipmentType(json, adminLoginBean);
	}
	
	/**
	 * 更改设备类型
	 * @param json
	 * @param adminLoginBean
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午7:54:36
	 */
	public void webWindowCheckInChangeEquipmentType(JSONObject json, AdminLoginBean adminLoginBean) throws Exception
	{
		long equipment_id		= StringUtil.getJsonLong(json, "equipment_id");
		int equipment_purpose	= StringUtil.getJsonInt(json, "equipment_purpose");
		DBRow equipment			= findEquipmentReOccupyById(equipment_id);
		long adid				= adminLoginBean.getAdid();
		String adidName			= adminLoginBean.getEmploye_name();
		String resource_name	= windowCheckInHandleOccupyResourceName(equipment.get("resources_type", 0), equipment.get("resources_id",0L));
		
		DBRow row				= new DBRow();
		row.add("equipment_purpose", equipment_purpose);
		floorCheckInMgrZyj.updateEquipment(equipment_id, row);
		
		windowCheckInAddEquipmentLog("update", equipment_purpose, equipment.getString("equipment_number"), equipment.get("check_in_entry_id",0L)
				, adid, adidName, equipment.get("resources_type", 0), resource_name, equipment.getString("seal_delivery"), equipment.getString("seal_pick_up"), "");
	}
	
	/**
	 * 通过设备ID查询task，占用资源，通知人
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月25日 上午11:28:56
	 */
	public DBRow[] findTaskEquipmentReSourcesByEquipmentId(long equipment_id) throws Exception
	{
		CheckInChildDocumentsStatusTypeKey statusKey = new CheckInChildDocumentsStatusTypeKey();
		EquipmentTypeKey equipmentTypeKey = new EquipmentTypeKey();
		DBRow[] rows = floorCheckInMgrZyj.findTasksByEquipmentId(equipment_id);
		for (int i = 0; i < rows.length; i++) {
			rows[i].add("number_status_value", statusKey.getContainerTypeKeyValue(rows[i].get("number_status", 0)));
			rows[i].add("equipment_type_value", rows[i].get("equipment_type", 0)>0?equipmentTypeKey.getEnlishEquipmentTypeValue(rows[i].get("equipment_type", 0)):"");
			if(rows[i].get("srr_id", 0L) == 0L)
			{
				int occupy_type = rows[i].get("occupancy_type", 0);
				long occupy_id	= rows[i].get("rl_id", 0L);
				String occupy_name = windowCheckInHandleOccupyResourceName(occupy_type, occupy_id);
				rows[i].add("occupy_name", occupy_name);
			}
			else
			{
				int occupy_type = rows[i].get("resources_type", 0);
				long occupy_id	= rows[i].get("resources_id", 0L);
				String occupy_name = windowCheckInHandleOccupyResourceName(occupy_type, occupy_id);
				rows[i].add("occupancy_type", occupy_type);
				rows[i].add("rl_id", occupy_id);
				rows[i].add("occupy_name", occupy_name);
			}
			//查询通知
			CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
			DBRow[] notices = checkInMgrZwb.windowFindSchedule(rows[i].get("dlo_detail_id", 0L), new int[]{ProcessKey.CHECK_IN_WINDOW,ProcessKey.GateNotifyWareHouse});
			String notice_name = "";
			String notice_ids = "";
			if(notices.length > 0)
			{
				for (int j = 0; j < notices.length; j++) {
						notice_name += (","+notices[j].getString("employe_name"));
		 				notice_ids  += (","+notices[j].getString("adids"));
				}
			}
			notice_ids = notice_ids.length()>0?notice_ids.substring(1):"";
			notice_name = notice_name.length()>0?notice_name.substring(1):"";
			rows[i].add("notice_names", notice_name);
			rows[i].add("notice_ids", notice_ids);
		}
		return rows;
	}
	
	/**
	 * 通过设备ID查询设备及tasks
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月25日 上午11:35:36
	 */
	public DBRow findEquipmentInfoAndEquipmentTasks(long equipment_id) throws Exception
	{
		DBRow equipment = findEquipmentReOccupyById(equipment_id);
		DBRow[] tasks = findTaskEquipmentReSourcesByEquipmentId(equipment_id);
		equipment.add("tasks", tasks);
		return equipment;
	}
	
	/**
	 * 查询supervisors
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月26日 下午12:32:40
	 */
	public DBRow findSupervisors(long ps_id, long group_id) throws Exception
	{
		long window_group_id = (group_id > 0)? group_id: 1000002;//默认warehouse 的supervizor分组
//		AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("proxyAdminMgr");
//		return adminMgr.getAdminIdsNamesByPsIdAndGId(ps_id, window_group_id);
//		String adids_default = supervisorDefault.getString("adids");
//		String adnames_default = supervisorDefault.getString("adnames");
		DBRow row = new DBRow();
		CheckInMgrIfaceZr checkInMgrZr = (CheckInMgrIfaceZr)MvcUtil.getBeanFromContainer("proxyCheckInMgrZr");  
		DBRow[] rows = checkInMgrZr.getPresenceOfSuperior(ps_id,window_group_id);
		String ids="";
		String names = "";
		for(int i=0;i<rows.length;i++){
			if(i==rows.length-1){
				ids+=rows[i].get("adid", 0l);
				names+=rows[i].getString("employe_name");
			}else{
				ids+=rows[i].get("adid", 0l)+",";
				names+=rows[i].getString("employe_name")+",";
			}
		}
		row.add("adids", ids);
		row.add("adnames", names);
		return row;
		
	}
	
	/**
	 * 通过entry明细ID，查询task设备资源，返回值类似于orderCheck
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月2日 下午3:20:33
	 */
	public DBRow findTaskEquipmentResourceByDetailIdSameCheck(long detail_id) throws Exception
	{
		ModuleKey moduleKey = new ModuleKey();
		DBRow row = floorCheckInMgrZyj.findTaskEquipmentResourceByDetailId(detail_id);
		if(null == row) throw new CheckinTaskNotFoundException();
		if(row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.PARTIALLY
				|| row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.EXCEPTION
				|| row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.CLOSE)
		{
			throw new CheckInAllTasksClosedException();
		}
		long resources_id	= row.get("resources_id", 0L);
		int resources_type	= row.get("resources_type", 0);
		if(resources_id == 0)
		{
			resources_id	= row.get("rl_id", 0L);
			resources_type	= row.get("occupancy_type", 0);
		}
		row.add("resources_type", resources_type);
		row.add("resources_id", resources_id);
		String resource_name = windowCheckInHandleOccupyResourceName(resources_type, resources_id);
		row.add("resources_name", resource_name);
		row.add("companyid", row.getString("company_id"));
		row.add("accountid", row.getString("account_id"));
		row.add("stagingareaid", row.getString("staging_area_id"));
		row.add("companyid", row.getString("company_id"));
		row.add("order_type", row.get("number_type", 0));
		row.add("receiptno", row.get("receipt_no",0L));
		row.add("supplierid", row.getString("supplier_id"));
		row.add("customerid", row.getString("customer_id"));
		if(!StrUtil.isBlank(row.getString("appointment_date"))){
			row.add("appointmentdate", DateUtil.showLocalTime(row.getString("appointment_date"),row.get("ps_id", 0l)));

		}
		row.add("order_type_value", moduleKey.getModuleName(row.get("number_type", 0)));
		//查询通知
		CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
		DBRow[] notices = checkInMgrZwb.windowFindSchedule(row.get("dlo_detail_id", 0L), new int[]{ProcessKey.CHECK_IN_WINDOW,ProcessKey.GateNotifyWareHouse});
		String notice_name = "";
		String notice_ids = "";
		if(notices.length > 0)
		{
			for (int j = 0; j < notices.length; j++) {
				notice_name += (","+notices[j].getString("employe_name"));
 				notice_ids  += (","+notices[j].getString("adids"));
			}
		}
		notice_ids = notice_ids.length()>0?notice_ids.substring(1):"";
		notice_name = notice_name.length()>0?notice_name.substring(1):"";
		row.add("notice_names", notice_name);
		row.add("notice_ids", notice_ids);
		return row;
	}
	
	/**
	 * task修改门、停车位或设备
	 * @param json
	 * @param adminLoginBean
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月27日 下午5:04:41
	 */
	public void windowCheckInTaskChangeResources(JSONObject json, AdminLoginBean adminLoginBean) throws Exception
	{
		long detail_id			= StringUtil.getJsonLong(json, "detail_id");
		int occupy_type			= StringUtil.getJsonInt(json, "occupy_type");
		long occupy_id			= StringUtil.getJsonLong(json, "occupy_id");
		long equipment_id_now	= StringUtil.getJsonLong(json, "equipment_id");
		DBRow row = floorCheckInMgrZyj.findTaskEquipmentResourceByDetailId(detail_id);
		if(null == row) throw new CheckinTaskNotFoundException();
		if(row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.PARTIALLY
				|| row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.EXCEPTION
				|| row.get("number_status", 0) == CheckInChildDocumentsStatusTypeKey.CLOSE)
		{
			throw new CheckInAllTasksClosedException();
		}
		DBRow checkRow = floorCheckInMgrZyj.findTaskEquipmentResourceByDetailId(detail_id);
		if(occupy_type > 0)
		{
			checkRow.add("occupancy_type", occupy_type);
		}
		if(occupy_id > 0)
		{
			checkRow.add("rl_id", occupy_id);
		}
		if(equipment_id_now > 0)
		{
			checkRow.add("equipment_id", equipment_id_now);
		}
		//同一单据在同一设备同一资源上不能重
		windowCheckInCheckTaskNumber(checkRow);
		
		long resources_id	= row.get("resources_id", 0L);
		int resources_type	= row.get("resources_type", 0);
		long equipment_id_before = row.get("equipment_id", 0L);
		int number_type		= row.get("number_type", 0);
		String number		= row.getString("number");
		long dlo_id			= row.get("dlo_id", 0L);
		String seal_delivery= row.getString("seal_delivery");
		String seal_pick_up = row.getString("seal_pick_up");
		int equipment_type  = row.get("equipment_type", 0);
		String equipment_number = row.getString("equipment_number");
		String resource_name= windowCheckInHandleOccupyResourceName(resources_type, resources_id);
		DBRow equipment		= findEquipmentReOccupyById(equipment_id_before);
		boolean a1 = false;
		boolean a2 = false;
		if(occupy_id > 0 && (occupy_type != resources_type || occupy_id != resources_id))
		{
			a1 = true;
			windowCheckInHandleResources(detail_id, detail_id, number_type, number, occupy_type, occupy_id, equipment_id_now, dlo_id, adminLoginBean);
			resource_name= windowCheckInHandleOccupyResourceName(occupy_type, occupy_id);
		}
		if(equipment_id_now > 0 && equipment_id_now != equipment_id_before)
		{
			a2 = true;
			equipment			= findEquipmentReOccupyById(equipment_id_now);
			equipment_number 	= equipment.getString("equipment_number");
		}
		if(a1 || a2)
		{
			//添加日志
    		windowCheckInAddTaskLog("update",number,number_type , dlo_id, adminLoginBean.getAdid(),adminLoginBean.getEmploye_name()
    				,occupy_type, resource_name,"",seal_delivery,seal_pick_up, equipment_type, equipment_number);
	    	//删除通知，先查询通知
    		String notePersonIds = fineTaskWarehouseSupervisors(detail_id).getString("notice_ids");
			deleteTaskWindowScheduleByDetailId(detail_id, adminLoginBean);
			deleteTaskWarehouseScheduleByDetailId(detail_id, adminLoginBean);
			
		    ModuleKey moduleKey = new ModuleKey();
		    OccupyTypeKey occupyTypeKey = new OccupyTypeKey();
		    CheckInMainDocumentsRelTypeKey checMainDocumentsRelTypeKey = new CheckInMainDocumentsRelTypeKey();
			String context=checMainDocumentsRelTypeKey.getCheckInMainDocumentsRelTypeKeyValue(orderSystemMgr.returnOrderTypeByDocType(number_type))
					+":【"+moduleKey.getModuleName(number_type)+":"+number+"】To "
    				+occupyTypeKey.getOccupyTypeKeyName(a1?occupy_type:resources_type)+":【"+resource_name +"】 In："+equipment_number;
			//更新
			DBRow updateDetail 	= new DBRow();
			if(a2)
			{
				updateDetail.add("equipment_id", equipment_id_now);
			}
			updateDetail.add("number_status",1);
			floorCheckInMgrZwb.updateDetailByIsExist(detail_id,updateDetail);	
			
			scheduleMgrZr.addSchedule(ScheduleModel.getCheckInScheduleModel(dlo_id, detail_id,ProcessKey.CHECK_IN_WINDOW
					, adminLoginBean.getAdid(), notePersonIds,context,number,number_type));
		}
		
	}
	
	
	
	public DBRow fineTaskWarehouseSupervisors(long task_id) throws Exception
	{
		DBRow row = new DBRow();
		CheckInMgrIfaceZwb checkInMgrZwb = (CheckInMgrIfaceZwb)MvcUtil.getBeanFromContainer("proxyCheckInMgrZwb");
		DBRow[] notices = checkInMgrZwb.windowFindSchedule(task_id, new int[]{ProcessKey.CHECK_IN_WINDOW,ProcessKey.GateNotifyWareHouse});
		String notice_name = "";
		String notice_ids = "";
		if(notices.length > 0)
		{
			for (int j = 0; j < notices.length; j++) {
				notice_name += (","+notices[j].getString("employe_name"));
 				notice_ids  += (","+notices[j].getString("adids"));
			}
		}
		notice_ids = notice_ids.length()>0?notice_ids.substring(1):"";
		notice_name = notice_name.length()>0?notice_name.substring(1):"";
		row.add("notice_names", notice_name);
		row.add("notice_ids", notice_ids);
		return row;
	}
	
	
	/**
	 * 查某个entry下未离开的设备及task信息
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月31日 上午11:25:44
	 */
	public DBRow[] getNotLeftEquipmentByEntryAndTotalTask(long entry_id) throws Exception
	{
		DBRow[] rows = floorCheckInMgrZyj.getNotLeftEquipmentByEntryAndTotalTask(entry_id);
		for (int i = 0; i < rows.length; i++)
		{
			long resources_id	= rows[i].get("resources_id", 0L);
			int resources_type	= rows[i].get("resources_type", 0);
			String resource_name= windowCheckInHandleOccupyResourceName(resources_type, resources_id);
			rows[i].add("resources_name", resource_name);
			rows[i].add("check_out_entry_id", rows[i].get("check_out_entry_id", 0L));
		}
		return rows;
	}
	
	/**
	 * 判断entry的所有task有没有发通知
	 * @param entryId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月5日 下午6:50:42
	 */
	public boolean whetherWriteWindowCheckInTime(long entryId) throws Exception
	{
		boolean ok = false;
		int taskCount = floorCheckInMgrZyj.findTaskCountByEntryId(entryId);
		int scheduleCount = scheduleMgrZr.findScheduleAssociateCountByAssociateMainIdType(ModuleKey.CHECK_IN, entryId, new int[]{ProcessKey.CHECK_IN_WINDOW,ProcessKey.GateNotifyWareHouse});
		if(taskCount == scheduleCount)
		{
			ok = true;
		}
		return ok;
	}
	
	/**
	 * 调整entry明细的数据
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年2月13日 下午4:57:09
	 */
	public void adjustReceiptNoInEntryDetailOrderSystem() throws Exception
	{
		DBRow[] entryDetails = floorCheckInMgrZwb.findAllNoReceiptNoEntryDetails();
		for (int i = 0; i < entryDetails.length; i++)
		{
			int number_type = entryDetails[i].get("number_type", 0);
			String number	= entryDetails[i].getString("number");
			String CompanyID= entryDetails[i].getString("company_id");
			String CustomerID=entryDetails[i].getString("customer_id");
			String SupplierID=entryDetails[i].getString("supplier_id");
			String bolNo = "";
			String containerNo = "";
			if(number_type == ModuleKey.CHECK_IN_BOL)
			{
				bolNo = number;
			}
			if(number_type == ModuleKey.CHECK_IN_CTN)
			{
				containerNo = number;
			}
			
			DBRow[] receipts = sqlServerMgrZJ.findReceiptsByBolNoOrContainerNo(bolNo, containerNo, CompanyID, CustomerID, SupplierID);
			long receiptNo = 0L;
			if(receipts.length == 1)
			{
				receiptNo = receipts[i].get("ReceiptNo", 0L);
			}
			else
			{
				log.error("entryDetail:"+entryDetails[i].get("dlo_detail_id", 0L)+",ReceiptNo handle fail.");
			}
			if(receiptNo > 0)
			{
				DBRow update = new DBRow();
				update.add("receipt_no", receiptNo);
				floorCheckInMgrZwb.updateDetail(entryDetails[i].get("dlo_detail_id", 0L), update);
				orderSystemMgr.updateOrderSystemById(entryDetails[i].get("lr_id", 0L), update);
			}
		}
	}
	
	
	public void setAdminMgrZJ(AdminMgrIFaceZJ adminMgrZJ) {
		this.adminMgrZJ = adminMgrZJ;
	}


	public void setFloorCheckInMgrZyj(FloorCheckInMgrZyj floorCheckInMgrZyj) {
		this.floorCheckInMgrZyj = floorCheckInMgrZyj;
	}

	public void setSqlServerMgrZJ(SQLServerMgrIFaceZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}

	public void setFloorShipToMgrZJ(FloorShipToMgrZJ floorShipToMgrZJ) {
		this.floorShipToMgrZJ = floorShipToMgrZJ;
	}

	public void setYmsMgrAPI(YMSMgrAPI ymsMgrAPI) {
		this.ymsMgrAPI = ymsMgrAPI;
	}

	public void setSqlServerTransactionTemplate(
			TransactionTemplate sqlServerTransactionTemplate) {
		this.sqlServerTransactionTemplate = sqlServerTransactionTemplate;
	}

	public void setStorageDoorMgrZr(StorageDoorMgrIfaceZr storageDoorMgrZr) {
		this.storageDoorMgrZr = storageDoorMgrZr;
	}

	public void setStorageYardControlZr(
			StorageYardControlIfaceZr storageYardControlZr) {
		this.storageYardControlZr = storageYardControlZr;
	}

	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setOrderSystemMgr(OrderSystemMgrIFace orderSystemMgr) {
		this.orderSystemMgr = orderSystemMgr;
	}
	
	public void setFloorCheckInMgrWfh(FloorCheckInMgrWfh floorCheckInMgrWfh) {
		this.floorCheckInMgrWfh = floorCheckInMgrWfh;
	}

}
