package com.cwc.app.api.zyj;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zyj.FloorProduresMgrZyj;
import com.cwc.app.iface.zyj.ProduresMgrZyjIFace;
import com.cwc.app.key.ProdureAddUpdateDelErrorKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ProduresMgrZyj implements ProduresMgrZyjIFace{
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorProduresMgrZyj floorProduresMgrZyj;

	/**
	 * 通过关联类型查询所有流程
	 * @param order_type
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] getProduresByOrderType(int order_type) throws Exception 
	{
		try
		{
			return floorProduresMgrZyj.getProduresByOrderType(order_type);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProduresByOrderType(int order_type)",log);
		}
	}

	/**
	 * 通过流程ID，阶段值数组，阶段状态数组查询阶段
	 * @param produres_id
	 * @param activities
	 * @param activitiesStatus
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] getProduresDetailsByProduresId(long produres_id,int[] activities, int[] activitiesStatus) throws Exception
	{
		try
		{
			return floorProduresMgrZyj.getProduresDetailsByProduresId(produres_id, activities, activitiesStatus);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProduresDetailsByProduresId(long produres_id,int[] activities)",log);
		}
	}
	
	/**
	 * 通过流程ID，是否显示查询通知信息
	 * @param produres_id
	 * @param is_display
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] getProduresNoticesByProduresId(long produres_id, int is_display) throws Exception
	{
		try
		{
			return floorProduresMgrZyj.getProduresNoticesByProduresId(produres_id, is_display);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProduresNoticesByProduresId(long produres_id, int is_display)",log);
		}
	}
	
	/**
	 * 通过流程ID获取流程信息
	 * @param produres_id
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow getProdureByProdureId(long produres_id) throws Exception
	{
		try
		{
			return floorProduresMgrZyj.getProdureByProdureId(produres_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProdureByProdureId(long produres_id)",log);
		}
	}
	
	/**
	 * 通过流程阶段ID详细详细
	 * @param produres_details_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProduresDetailsByProduresDetailId(long produres_details_id) throws Exception
	{
		try
		{
			return floorProduresMgrZyj.getProduresDetailsByProduresDetailId(produres_details_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProduresDetailsByProduresDetailId(long produres_details_id)",log);
		}
	}
	
	/**
	 * 通过通知ID获取通知详细
	 * @param notice_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProduresNoticeByNoticeId(long notice_id) throws Exception
	{
		try
		{
			return floorProduresMgrZyj.getProduresNoticeByNoticeId(notice_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProduresNoticeByNoticeId(long notice_id)",log);
		}
	}
	
	/**
	 * 通过流程ID，是否显示，通知类型查询
	 * @param produres_id
	 * @param is_display
	 * @param notice_type
	 * @return
	 * @throws Exception
	 */
	public DBRow getProduresNoticeByProduresIdIsDisplayType(long produres_id, int is_display, int notice_type) throws Exception
	{
		try
		{
			return floorProduresMgrZyj.getProduresNoticeByProduresIdIsDisplayType(produres_id, is_display, notice_type);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProduresNoticeByProduresIdIsDisplayType(long produres_id, int is_display, int notice_type) ",log);
		}
	}
	
	/**
	 * 通过流程阶段ID详细详细
	 * @param produres_details_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProduresDetailsByProduresIdActivity(long produres_id, int activity) throws Exception
	{
		try
		{
			return floorProduresMgrZyj.getProduresDetailsByProduresIdActivity(produres_id, activity);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProduresDetailsByProduresIdActivity(long produres_id, int activity)",log);
		}
	}
	
	/**
	 * 保存流程信息
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow addProdure(HttpServletRequest request) throws Exception
	{
		try
		{
			int order_type			= StringUtil.getInt(request, "order_type");
			
			String process_name		= StringUtil.getString(request, "process_name").trim();
			int process_key			= StringUtil.getInt(request, "process_key");
			String process_coloum	= StringUtil.getString(request, "process_coloum").trim();
			String process_coloum_page	= StringUtil.getString(request, "process_coloum_page").trim();
			String process_person_is_display	= StringUtil.getString(request, "process_person_is_display").trim();
			String process_person	= StringUtil.getString(request, "process_person").trim();
			String process_start_time_is_create	 = StringUtil.getString(request, "process_start_time_is_create").trim();
			String process_period	= StringUtil.getString(request, "process_period").trim();
			String process_note_is_display	= StringUtil.getString(request, "process_note_is_display").trim();
			String activity_is_need_display	= StringUtil.getString(request, "activity_is_need_display").trim();
			String process_note	= StringUtil.getString(request, "process_note").trim();
			
			
			//阶段
			List<DBRow> activitys	= new ArrayList<DBRow>();
			String[] activity_name	= request.getParameterValues("activity_name");
			String[] activity_val	= request.getParameterValues("activity_val");
			String[] val_status		= request.getParameterValues("val_status");
			String[] isThisActivityCheckedDefault	= request.getParameterValues("isThisActivityCheckedDefault");
			int activityCheckVal	= 0;
			for (int i = 0; i < activity_name.length; i++) 
			{
				DBRow activity	= new DBRow();
				activity.add("activity_name", activity_name[i]);
				activity.add("activity_val", activity_val[i]);
				activity.add("val_status", val_status[i]);
				activitys.add(activity);
				if(YesOrNotKey.YES == Integer.parseInt(isThisActivityCheckedDefault[i]))
				{
					activityCheckVal = Integer.parseInt(activity_val[i]);
				}
			}
			
			//通知
			List<DBRow> notices		= new ArrayList<DBRow>();
			String[] notice_name	= request.getParameterValues("notice_name");
			String[] notice_coloum_name	= request.getParameterValues("notice_coloum_name");     
			String[] notice_default_selected	= request.getParameterValues("notice_default_selected");
			String[] notice_type	= request.getParameterValues("notice_type");
			String[] notice_is_display	= request.getParameterValues("notice_is_display");
			for (int i = 0; i < notice_name.length; i++) 
			{
				DBRow notice	= new DBRow();
				notice.add("notice_name", notice_name[i]);
				notice.add("notice_coloum_name", notice_coloum_name[i]);
				notice.add("notice_default_selected", notice_default_selected[i]);
				notice.add("notice_type", notice_type[i]);
				notice.add("notice_is_display", notice_is_display[i]);
				notices.add(notice);
			}
			
			long produres_id	= 0L;
			//检查流程名，流程字段名，流程页面用的名，流程key值是否符合条件
			DBRow errorRowProdure	= this.checkProcesByOrderType(order_type, process_name, process_key, process_coloum, process_coloum_page);
			DBRow errorRowProDetail	= this.checkProdureDetails(activitys);
			DBRow errorRowProNotice = this.checkProdureNotices(notices);
			
			errorRowProdure.append(errorRowProDetail);
			errorRowProdure.append(errorRowProNotice);
			
			//检查阶段名，阶段值是否符合条件
			if(0 == errorRowProdure.get("flag_check_produres", 0) && 0 == errorRowProDetail.get("flag_check_produres_activity", 0) && 0 == errorRowProNotice.get("flag_check_produres_notice", 0))
			{
				//流程
				DBRow process = new DBRow();
				process.add("process_key", process_key);
				process.add("process_coloum", process_coloum);
				process.add("process_coloum_page", process_coloum_page);
				process.add("process_name", process_name);
				process.add("process_note_is_display", process_note_is_display);
				process.add("process_note", process_note);
				process.add("process_person_is_display", process_person_is_display);
				process.add("process_person", process_person);
				process.add("activity_is_need_display", activity_is_need_display);
				process.add("associate_order_type", order_type);
				process.add("process_start_time_is_create", process_start_time_is_create);
				process.add("process_period", process_period);
				process.add("activity_default_selected", activityCheckVal);//需要根据阶段默认选中
				produres_id = floorProduresMgrZyj.addProdure(process);
				//阶段
				for (int i = 0; i < activitys.size(); i++) 
				{
					DBRow activity	= activitys.get(i);
					activity.add("produres_id", produres_id);
					floorProduresMgrZyj.addProdureDetail(activity);
				}
				
				//通知
				for (int i = 0; i < notices.size(); i++) 
				{
					DBRow notice	= notices.get(i);
					notice.add("produres_id", produres_id);
					floorProduresMgrZyj.addProdureNotice(notice);
				}
			}
			else
			{
				return errorRowProdure;
			}
			return errorRowProdure;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProdure",log);
		}
	}
	
	/**
	 * 检查流程是否正确
	 * @param order_type
	 * @param process_name
	 * @param process_key
	 * @param process_coloum
	 * @param process_coloum_page
	 * @return
	 * @throws Exception
	 */
	public DBRow checkProcesByOrderType(int order_type, String process_name, int process_key, String process_coloum, String process_coloum_page) throws Exception
	{
		try
		{
			int flag = 0;//记录错误个数
			ProdureAddUpdateDelErrorKey errorKey = new ProdureAddUpdateDelErrorKey();
			DBRow errorRow = new DBRow();
			if(0 != floorProduresMgrZyj.findProdureByOrderTypeKeyColumn(order_type, process_key, "", "", ""))
			{
				flag ++;
				errorRow.add("process_key", errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.PROCESS_KEY_EXIST));
			}
			if(0 != floorProduresMgrZyj.findProdureByOrderTypeKeyColumn(order_type, 0, process_name, "", ""))
			{
				flag ++;
				errorRow.add("process_name", errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.PROCESS_NAME_EXIST));
			}
			if(0 != floorProduresMgrZyj.findProdureByOrderTypeKeyColumn(order_type, 0, "", process_coloum, ""))
			{
				flag ++;
				errorRow.add("process_coloum", errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.COLUMN_NAME_EXIST));
			}
			if(0 != floorProduresMgrZyj.findProdureByOrderTypeKeyColumn(order_type, 0, "", "", process_coloum_page))
			{
				flag ++;
				errorRow.add("process_coloum_page", errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.COLUMN_NAME_PAGE_EXIST));
			}
			errorRow.add("flag_check_produres", flag);
			return errorRow;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"checkProcesByOrderType()",log);
		}
	}
	
	
	public DBRow checkProdureDetails(List<DBRow> produre_details) throws Exception
	{
		try
		{
			int flag = 0;//记录错误个数
			ProdureAddUpdateDelErrorKey errorKey = new ProdureAddUpdateDelErrorKey();
			String activityNameError	= "";
			String activityValError		= "";
			DBRow errorRow = new DBRow();
			Set<String> activityNameSet	= new HashSet<String>();
			Set<String> activityValSet	= new HashSet<String>();
			for (int i = 0; i < produre_details.size(); i++) 
			{
				DBRow produre_detail	= produre_details.get(i);
				String activity_name	= produre_detail.getString("activity_name");
				String activity_val		= produre_detail.getString("activity_val");
				if(!activityNameSet.add(activity_name) && "".equals(activityNameError))
				{
					flag ++;
					activityNameError	= errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.ACTIVITY_NAME_REPEAT);
				}
				if(!activityValSet.add(activity_val) && "".equals(activityValError))
				{
					flag ++;
					activityNameError	= errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.ACTIVITY_VAL_REPEAT);
				}
			}
			errorRow.add("activity_name", activityNameError);
			errorRow.add("activity_val", activityValError);
			errorRow.add("flag_check_produres_activity", flag);
			return errorRow;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"checkProcesByOrderType()",log);
		}
	}
	
	public DBRow checkProdureNotices(List<DBRow> notices) throws Exception
	{
		try
		{
			int flag = 0;//记录错误个数
			ProdureAddUpdateDelErrorKey errorKey = new ProdureAddUpdateDelErrorKey();
			DBRow errorRow = new DBRow();
			String noticeNameError = "";
			String noticeColoumNameError = "";
			Set<String> noticeNameSet = new HashSet<String>();
			Set<String> noticeColoumNameSet = new HashSet<String>();
			for (int i = 0; i < notices.size(); i++)
			{
				DBRow notice = notices.get(i);
				String noticeName = notice.getString("notice_name");
				String noticeVal = notice.getString("notice_coloum_name");
				if(!noticeNameSet.add(noticeName) && "".equals(noticeNameError))
				{
					flag ++;
					noticeNameError = errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.NOTICE_NAME_REPEAT);
				}
				if(!noticeColoumNameSet.add(noticeVal) && "".equals(noticeColoumNameError))
				{
					flag ++;
					noticeColoumNameError = errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.NOTICE_COLUMN_REPEAT);
				}
			}
			
			errorRow.add("notice_name", noticeNameError);
			errorRow.add("notice_coloum_name", noticeColoumNameError);
			errorRow.add("flag_check_produres_notice", flag);
			return errorRow;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"checkProdureNotices(List<DBRow> notices)",log);
		}
	}
	
	public DBRow updateProdure(HttpServletRequest request) throws Exception
	{
		try
		{
			int order_type			= StringUtil.getInt(request, "order_type");
			long produre_id			= StringUtil.getLong(request, "produre_id");
			String process_name		= StringUtil.getString(request, "process_name").trim();
			int process_key			= StringUtil.getInt(request, "process_key");
			String process_coloum	= StringUtil.getString(request, "process_coloum").trim();
			String process_coloum_page	= StringUtil.getString(request, "process_coloum_page").trim();
			String process_person_is_display	= StringUtil.getString(request, "process_person_is_display").trim();
			String process_person	= StringUtil.getString(request, "process_person").trim();
			String process_start_time_is_create	 = StringUtil.getString(request, "process_start_time_is_create").trim();
			String process_period	= StringUtil.getString(request, "process_period").trim();
			String process_note_is_display	= StringUtil.getString(request, "process_note_is_display").trim();
			String activity_is_need_display	= StringUtil.getString(request, "activity_is_need_display").trim();
			String process_note	= StringUtil.getString(request, "process_note").trim();
			
			
			//阶段
			List<DBRow> activitys	= new ArrayList<DBRow>();
			String[] activity_name	= request.getParameterValues("activity_name");
			String[] activity_val	= request.getParameterValues("activity_val");
			String[] val_status		= request.getParameterValues("val_status");
			String[] isThisActivityCheckedDefault	= request.getParameterValues("isThisActivityCheckedDefault");
			int activityCheckVal	= 0;
			for (int i = 0; i < activity_name.length; i++) 
			{
				DBRow activity	= new DBRow();
				activity.add("activity_name", activity_name[i]);
				activity.add("activity_val", activity_val[i]);
				activity.add("val_status", val_status[i]);
				activitys.add(activity);
				if(YesOrNotKey.YES == Integer.parseInt(isThisActivityCheckedDefault[i]))
				{
					activityCheckVal = Integer.parseInt(activity_val[i]);
				}
			}
			
			//通知
			List<DBRow> notices		= new ArrayList<DBRow>();
			String[] notice_name	= request.getParameterValues("notice_name");
			String[] notice_coloum_name	= request.getParameterValues("notice_coloum_name");     
			String[] notice_default_selected	= request.getParameterValues("notice_default_selected");
			String[] notice_type	= request.getParameterValues("notice_type");
			String[] notice_is_display	= request.getParameterValues("notice_is_display");
			for (int i = 0; i < notice_name.length; i++) 
			{
				DBRow notice	= new DBRow();
				notice.add("notice_name", notice_name[i]);
				notice.add("notice_coloum_name", notice_coloum_name[i]);
				notice.add("notice_default_selected", notice_default_selected[i]);
				notice.add("notice_type", notice_type[i]);
				notice.add("notice_is_display", notice_is_display[i]);
				notices.add(notice);
			}
			
			//检查流程名，流程字段名，流程页面用的名，流程key值是否符合条件
			DBRow errorRowProdure	= this.checkProcesByOrderTypeProdure(order_type, produre_id, process_name, process_key, process_coloum, process_coloum_page);
			DBRow errorRowProDetail	= this.checkProdureDetails(activitys);
			DBRow errorRowProNotice = this.checkProdureNotices(notices);
			
			errorRowProdure.append(errorRowProDetail);
			errorRowProdure.append(errorRowProNotice);
			
			//检查阶段名，阶段值是否符合条件
			if(0 == errorRowProdure.get("flag_check_produres", 0) && 0 == errorRowProDetail.get("flag_check_produres_activity", 0) && 0 == errorRowProNotice.get("flag_check_produres_notice", 0))
			{
				//流程
				DBRow process = new DBRow();
				process.add("process_key", process_key);
				process.add("process_coloum", process_coloum);
				process.add("process_coloum_page", process_coloum_page);
				process.add("process_name", process_name);
				process.add("process_note_is_display", process_note_is_display);
				process.add("process_note", process_note);
				process.add("process_person_is_display", process_person_is_display);
				process.add("process_person", process_person);
				process.add("activity_is_need_display", activity_is_need_display);
				process.add("associate_order_type", order_type);
				process.add("process_start_time_is_create", process_start_time_is_create);
				process.add("process_period", process_period);
				process.add("activity_default_selected", activityCheckVal);//需要根据阶段默认选中
				floorProduresMgrZyj.updateProdure(process, produre_id);
				//阶段
				floorProduresMgrZyj.deleteProdureActivitysByProdureId(produre_id);
				for (int i = 0; i < activitys.size(); i++) 
				{
					DBRow activity	= activitys.get(i);
					activity.add("produres_id", produre_id);
					floorProduresMgrZyj.addProdureDetail(activity);
				}
				//通知
				floorProduresMgrZyj.deleteProdureNoticesByProdureId(produre_id);
				for (int i = 0; i < notices.size(); i++) 
				{
					DBRow notice	= notices.get(i);
					notice.add("produres_id", produre_id);
					floorProduresMgrZyj.addProdureNotice(notice);
				}
			}
			else
			{
				return errorRowProdure;
			}
			return errorRowProdure;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateProdure(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 检查流程是否正确
	 * @param order_type
	 * @param process_name
	 * @param process_key
	 * @param process_coloum
	 * @param process_coloum_page
	 * @return
	 * @throws Exception
	 */
	public DBRow checkProcesByOrderTypeProdure(int order_type, long produre_id, String process_name, int process_key, String process_coloum, String process_coloum_page) throws Exception
	{
		try
		{
			int flag = 0;//记录错误个数
			ProdureAddUpdateDelErrorKey errorKey = new ProdureAddUpdateDelErrorKey();
			DBRow errorRow = new DBRow();
			DBRow produreRowByKey = floorProduresMgrZyj.findProdureRowsByOrderTypeKeyColumn(order_type, process_key, "", "", "");
			if(null != produreRowByKey && produre_id != produreRowByKey.get("produres_id", 0L))
			{
				flag ++;
				errorRow.add("process_key", errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.PROCESS_KEY_EXIST));
			}
			produreRowByKey = floorProduresMgrZyj.findProdureRowsByOrderTypeKeyColumn(order_type, 0, process_name, "", "");
			if(null != produreRowByKey && produre_id != produreRowByKey.get("produres_id", 0L))
			{
				flag ++;
				errorRow.add("process_name", errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.PROCESS_NAME_EXIST));
			}
			produreRowByKey = floorProduresMgrZyj.findProdureRowsByOrderTypeKeyColumn(order_type, 0, "", process_coloum, "");
			if(null != produreRowByKey && produre_id != produreRowByKey.get("produres_id", 0L))
			{
				flag ++;
				errorRow.add("process_coloum", errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.COLUMN_NAME_EXIST));
			}
			produreRowByKey = floorProduresMgrZyj.findProdureRowsByOrderTypeKeyColumn(order_type, 0, "", "", process_coloum_page);
			if(null != produreRowByKey && produre_id != produreRowByKey.get("produres_id", 0L))
			{
				flag ++;
				errorRow.add("process_coloum_page", errorKey.getProdureAddUpdateDelErrorKey(ProdureAddUpdateDelErrorKey.COLUMN_NAME_PAGE_EXIST));
			}
			errorRow.add("flag_check_produres", flag);
			return errorRow;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"checkProcesByOrderTypeProdure",log);
		}
	}
	

	public void setFloorProduresMgrZyj(FloorProduresMgrZyj floorProduresMgrZyj) {
		this.floorProduresMgrZyj = floorProduresMgrZyj;
	}

	

}
