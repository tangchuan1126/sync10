package com.cwc.app.api.zyj;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.zyj.FloorTrackStatusMgrZyj;
import com.cwc.app.iface.zyj.TrackStatusMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class TrackStatusMgrZyj implements TrackStatusMgrZyjIFace{

	private static Logger log = Logger.getLogger("ACTION");
	private FloorTrackStatusMgrZyj floorTrackStatusMgrZyj;
	
	/**
	 * 暂时不用
	 */
	@Override
	public DBRow[] getTrackStatusListByShipCom(String shipCom) throws Exception {

		try {
			return floorTrackStatusMgrZyj.getTrackStatusListByShipCom(shipCom);
		} catch (Exception e) {
			throw new SystemException(e,"TrackStatusMgrZyj.getTrackStatusListByShipCom(shipCom)",log);
		}
	}

	/**
	 * 暂时不用
	 */
	@Override
	public DBRow[] getShipCompanyList() throws Exception {
		try {
			return floorTrackStatusMgrZyj.getShipCompanyList();
		} catch (Exception e) {
			throw new SystemException(e,"TrackStatusMgrZyj.getShipCompanyList()",log);
		}
	}
	
	/**
	 * 将物流的系统封装成一个Map，key为公司名称，value为物流信息
	 */
	@Override
	public Map<String, DBRow[]> getTrackStatusMapByShipCom() throws Exception {
		try {
			//将物流公司的名称作为Key，物流公司的信息作为value
			Map<String, DBRow[]> trackMap = new HashMap<String, DBRow[]>();
			//得到所有的物流公司名称
			DBRow[] shipComps = floorTrackStatusMgrZyj.getShipCompanyList();
			if(null != shipComps && shipComps.length >0){
				for (DBRow row : shipComps) {
					String compName		= row.getString("shipping_company");
					//根据物流公司的名称得到相关信息
					DBRow[] trackList	= floorTrackStatusMgrZyj.getTrackStatusListByShipCom(compName);
					trackMap.put(compName, trackList);
				}
			}
			return trackMap;
			
		} catch (Exception e) {
			throw new SystemException(e,"TrackStatusMgrZyj.getTrackStatusMapByShipCom()",log);
		}
		
	}
	
	/**
	 * 添加物流信息
	 */
	@Override
	public boolean addTrackStatus(HttpServletRequest request) throws Exception {
		try {
			String trackCode		= StringUtil.getString(request, "tracking_status_code");
			String trackDefinition	= StringUtil.getString(request, "tracking_definition");
			String shipComp			= StringUtil.getString(request, "shipping_company");
			int	trackStatus			= StringUtil.getInt(request, "tracking_status");
			
			DBRow trackRow = new DBRow();
			trackRow.add("tracking_status_code", trackCode);
			trackRow.add("tracking_definition", trackDefinition);
			trackRow.add("shipping_company", shipComp);
			trackRow.add("tracking_status", trackStatus);
			//如果添加的数据存在，则不允许添加
			if(null != floorTrackStatusMgrZyj.getTrackStatusByCodeComp(trackCode, shipComp)){
				return false;
			}
			return floorTrackStatusMgrZyj.addTrackStatus(trackRow);
		} catch (Exception e) {
			throw new SystemException(e,"TrackStatusMgrZyj.addTrackStatus(String trackCode, String trackDefinition, String shipComp, int trackStatus)",log);
		}
	}
	
	/**
	 * 更新物流信息
	 */
	@Override
	public boolean updateTrackStatus(HttpServletRequest request) throws Exception {
		try {
			
			int trackId				= StringUtil.getInt(request,"track_id");
			String trackCode		= StringUtil.getString(request, "tracking_status_code");
			String trackDefinition	= StringUtil.getString(request, "tracking_definition");
			String shipComp			= StringUtil.getString(request, "shipping_company");
			int	trackStatus			= StringUtil.getInt(request, "tracking_status");
			
			DBRow trackRow = new DBRow();
			trackRow.add("tracking_status_code", trackCode);
			trackRow.add("tracking_definition", trackDefinition);
			trackRow.add("shipping_company", shipComp);
			trackRow.add("tracking_status", trackStatus);
			//如果添加的数据存在，则不允许添加
			DBRow trackRowData = floorTrackStatusMgrZyj.getTrackStatusByCodeComp(trackCode, shipComp);
			if(null != trackRowData && trackRowData.get("track_id", 0) != trackId){
				return false;
			}
			return (0==floorTrackStatusMgrZyj.updateTrackStatus(trackId, trackRow))?false:true;
		} catch (Exception e) {
			throw new SystemException(e,"TrackStatusMgrZyj.updateTrackStatus(String trackCode, String trackDefinition, String shipComp, int trackStatus)",log);
		}
	}
	
	@Override
	public DBRow getTrackStatusById(HttpServletRequest request) throws Exception {
		try {
			String trackStatusId = StringUtil.getString(request,"trackStatusId");
			return floorTrackStatusMgrZyj.getTrackStatusById(trackStatusId);
		} catch (Exception e) {
			throw new SystemException(e,"TrackStatusMgrZyj.getTrackStatusById(id)",log);
		}
	}
	
	/**
	 * 删除
	 */
	@Override
	public void deleteById(HttpServletRequest request) throws Exception {
		try {
			int id	= StringUtil.getInt(request, "trackStatusId");
			floorTrackStatusMgrZyj.deleteById(id);
		} catch (Exception e) {
			throw new SystemException(e,"TrackStatusMgrZyj.deleteById(id)",log);
		}
	}
	
	public void setFloorTrackStatusMgrZyj(
			FloorTrackStatusMgrZyj floorTrackStatusMgrZyj) {
		this.floorTrackStatusMgrZyj = floorTrackStatusMgrZyj;
	}

	

	

	




	
}
