package com.cwc.app.api.zyj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.aop.framework.AopContext;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.ll.Utils;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.exception.deliveryOrder.RepeatProductException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.exception.transport.NoExistTransportDetailException;
import com.cwc.app.exception.transport.NoExistTransportOrderException;
import com.cwc.app.exception.transport.TransportLackException;
import com.cwc.app.exception.transport.TransportOrderDetailRepeatException;
import com.cwc.app.exception.transport.TransportStockInHandleErrorException;
import com.cwc.app.exception.transport.TransportStockOutHandleErrorException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zyj.FloorRepairOrderMgrZyj;
import com.cwc.app.floor.api.zyj.FloorStorageCatalogMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.ProductMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.ProductStoreEditLogMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zyj.ApplyFundsMgrZyjIFace;
import com.cwc.app.iface.zyj.RepairOrderMgrZyjIFace;
import com.cwc.app.key.ApproveRepairKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.InOutStoreKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.RepairCertificateKey;
import com.cwc.app.key.RepairClearanceKey;
import com.cwc.app.key.RepairDeclarationKey;
import com.cwc.app.key.RepairLogTypeKey;
import com.cwc.app.key.RepairOrderKey;
import com.cwc.app.key.RepairProductFileKey;
import com.cwc.app.key.RepairQualityInspectionKey;
import com.cwc.app.key.RepairStockInSetKey;
import com.cwc.app.key.RepairTagKey;
import com.cwc.app.lucene.zyj.RepairOrderIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

public class RepairOrderMgrZyj implements RepairOrderMgrZyjIFace{
	
	static Logger log = Logger.getLogger("ACTION");
	private AdminMgr adminMgr = new AdminMgr();
	private FloorRepairOrderMgrZyj floorRepairOrderMgrZyj;
	private SystemConfigIFace systemConfig;
	private FloorProductMgr floorProductMgr;
	private ProductMgrIFace productMgr;
	private FloorPurchaseMgr floorPurchaseMgr;
	private FloorCatalogMgr floorCatalogMgr;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private FloorStorageCatalogMgrZyj floorStorageCatalogMgrZyj;
	private ApplyFundsMgrZyjIFace applyFundsMgrZyj;
	private ProductStoreEditLogMgrIFaceZJ productStoreEditLogMgrZJ;
	
	
	/**
	 * 过滤返修单
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterRepairOrder(long send_psid, long receive_psid,PageCtrl pc, int status, int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id) 
		throws Exception 
	{
		try
		{
			return (floorRepairOrderMgrZyj.fillterRepairOrder(send_psid, receive_psid, pc, status, declaration, clearance, invoice, drawback,day,stock_in_set,create_account_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	
	/**
	 * 根据返修单号搜索返修单
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchTransportByNumber(String search_key,int search_mode,PageCtrl pc)
		throws Exception 
	{
		try 
		{
			if(!search_key.equals("")&&!search_key.equals("\""))
			{
				search_key = search_key.replaceAll("\"","");
				search_key = search_key.replaceAll("'","");
				search_key = search_key.replaceAll("\\*","");
//				search_key +="*";这个索引是IK分词器，不要最后+*
			}
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			return RepairOrderIndexMgr.getInstance().getSearchResults(search_key,search_mode,page_count,pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 需跟进的交货单
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReadyDelivery(long product_line_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			int need_track_ready_delivery_day = systemConfig.getIntConfigValue("repair_ready_period");//返修单备货跟进周期（天）
			
			return floorRepairOrderMgrZyj.getNeedTrackReadyDelivery(product_line_id, need_track_ready_delivery_day, pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getNeedTrackReadyDelivery",log);
		}
	}
	
	
	/**
	 * 根据仓库还有类型过滤需跟进的返修单发货
	 * @param ps_id
	 * @param cmd
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackSendRepairOrderByPsid(long ps_id,String cmd,PageCtrl pc)
		throws Exception
	{
		DBRow[] results = new DBRow[0];
		if(cmd.equals("ready_send_ps"))
		{
			int need_track_ready_transport_day = systemConfig.getIntConfigValue("repair_ready_period");//返修单备货跟进周期（天）
			results = floorRepairOrderMgrZyj.getNeedTrackSendRepairOrderByPsid(ps_id, need_track_ready_transport_day,RepairOrderKey.READY,pc);
		}
		else if(cmd.equals("packing_send_ps"))
		{
			int need_track_packing_transport_day = systemConfig.getIntConfigValue("repair_packing_period");//返修单装箱跟进周期（天）
			results = floorRepairOrderMgrZyj.getNeedTrackSendRepairOrderByPsid(ps_id,need_track_packing_transport_day,RepairOrderKey.PACKING,pc);
		}
		else if(cmd.equals("tag_send_ps"))
		{
			int tag_day = systemConfig.getIntConfigValue("repair_tag_period");							//转运制签周期
			results = floorRepairOrderMgrZyj.getNeedTrackTagRepairOrderByPs(ps_id, tag_day, pc);
		}
		else if(cmd.equals("product_file_send_ps"))
		{
			int product_file_day = systemConfig.getIntConfigValue("repair_product_file_period");			//转运实物图片周期
			results = floorRepairOrderMgrZyj.getNeedTrackProductFileRepairOrder(ps_id,product_file_day,pc);
		}
		else if(cmd.equals("quality_inspection_send_ps"))
		{
			int quality_inspection_day = systemConfig.getIntConfigValue("repair_quality_inspection");	//转运质检周期
			results = floorRepairOrderMgrZyj.getNeedTrackQualityInspectionRepairOrder(ps_id, quality_inspection_day, pc);
		}
		
		return results;
	}
	
	/**
	 * 根据仓库还有类型过滤需跟进的仓库收货
	 * @param ps_id
	 * @param cmd
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReceiveRepairOrderByPsid(long ps_id,String cmd,PageCtrl pc)
		throws Exception
	{
		DBRow[] results = new DBRow[0];
		if(cmd.equals("intransit_receive_ps"))
		{
			int need_track_intransit_day = systemConfig.getIntConfigValue("repair_intransit_period");//交货返修单运输中跟进周期（天）
			
			
			results = floorRepairOrderMgrZyj.getNeedTrackReceiveRepairOrderByPsid(ps_id,need_track_intransit_day,RepairOrderKey.INTRANSIT,pc);
		}
		else if(cmd.equals("alreadyRecive_receive_ps"))
		{
			int need_track_alreadyRecive_transport_hour = systemConfig.getIntConfigValue("repair_alreadyreceive_period");//交货返修单已收货跟进周期（时）
			
			results = floorRepairOrderMgrZyj.getNeedTrackReceiveRepairOrderByPsid(ps_id,need_track_alreadyRecive_transport_hour,RepairOrderKey.AlREADYRECEIVE,pc);
		}
		
		return results;
	}
	
	
	/**
	 * 获得返修的体积
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public float getRepairOrderVolume(long repair_order_id)
		throws Exception
	{
		try 
		{
			return floorRepairOrderMgrZyj.getRepairOrderVolume(repair_order_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getRepairOrderVolume",log);
		}
	}
	
	/**
	 * 获得返修单的总重量
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public float getRepairWeight(long repair_order_id)
		throws Exception
	{
		try 
		{
			return floorRepairOrderMgrZyj.getRepairWeight(repair_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getRepairWeight",log);
		}
	}
	
	/**
	 * 获得所有返修单出库金额
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public double getRepairSendPrice(long repair_order_id)
		throws Exception
	{
		try 
		{
			return floorRepairOrderMgrZyj.getRepairSendPrice(repair_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getRepairSendPrice",log);
		}
	}
	/**
	 * 获得返修单发货商品重量（商品数量以发货数计算）
	 */
	public float getRepairDetailsSendWeight(long repair_order_id)
		throws Exception
	{
		try {
			DBRow[] transportProducts = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);

			float weight = 0;
			for (int i = 0; i < transportProducts.length; i++) 
			{
				DBRow product = floorProductMgr.getDetailProductByPcid(transportProducts[i].get("repair_pc_id",0l));
				
				weight += product.get("weight",0f)*transportProducts[i].get("repair_send_count",0f);
			}
			
			return weight;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getRepairDetailsSendWeight",log);
		}
	}
	
	/**
	 * 根据返修单ID获得返修单详细
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailRepairOrderById(long repair_order_id) 
		throws Exception
	{
		try 
		{
			return (floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailRepairOrderById",log);
		}
	}
	/**
	 * 根据返修单ID获得返修单详细
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairOrderDetailByRepairId(long repair_order_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)//根据返修单ID获得返修单明细
		throws Exception
	{
		try 
		{
			return floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getRepairOrderDetailByRepairId",log);
		}
	}
	
	public DBRow[] getRepairOrderDetailByRepairId(long repair_order_id)
	throws Exception
	{
		try 
		{
			return (floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getRepairOrderDetailByRepairId",log);
		}
	}
	
	
	/**
	 * 根据返修明细ID获得返修明细
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getRepairDetailById(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long repair_detail_id = StringUtil.getLong(request,"repair_detail_id");
			
			return (floorRepairOrderMgrZyj.getRepairDetailById(repair_detail_id));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getRepairDetailById",log);
		}
	}
	
	
	/**
	 * 终止装箱
	 * @param request
	 * @throws Exception
	 */
	public void rebackRepair(HttpServletRequest request)
		throws Exception
	{
		long repair_order_id = StringUtil.getLong(request,"repair_order_id");
		DBRow repair = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
		
		DBRow[] repairDetails = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
		
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		//循环退回库存
		for (int i = 0; i < repairDetails.length; i++) 
		{
			//商品入库
			
			ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
			inProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
			inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			inProductStoreLogBean.setOid(repair.get("repair_order_id", 0l));
			inProductStoreLogBean.setBill_type(ProductStoreBillKey.TRANSPORT_ORDER);
			
			inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_TRANSPORT_RE);//返修中止
			
			DBRow[] unionId = floorProductMgr.getProductUnionsBySetPid(repairDetails[i].get("repair_pc_id",0l));
			//套装转换为散件
			if(repairDetails[i].get("repair_normal_count",0f)>0&&(unionId!=null&&unionId.length>0)) {
				for(int j=0;unionId!=null&&j<unionId.length;j++) {
					long pid = unionId[j].get("pid", 0l);
					float quantity = unionId[j].get("quantity", 0f);
					DBRow product = floorProductMgr.getDetailProductByPcid(pid);//为了获得商品ID
					if (product!=null) //以防商品被删除
					{
						floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,repair.get("send_psid",0l),product.get("pc_id", 0l),repairDetails[i].get("repair_normal_count",0f)*quantity,0);//库存添加
						
						TDate td = new TDate();
						long datemark = td.getDateTime();
						String pcode = product.getString("p_code");
						long adid = adminLoggerBean.getAdid();
						int type = InOutStoreKey.IN_STORE_TRANSPORT_RE;
						productMgr.addInProductLog(pcode,repair.get("send_psid",0l), repairDetails[i].get("repair_normal_count",0f)*quantity, adid,datemark,1, type);//入库日志
					}
				}
			}
			//散件入库
			if(repairDetails[i].get("repair_normal_count",0f)>0&&(unionId.length==0)) {
				DBRow product = floorProductMgr.getDetailProductByPcid(repairDetails[i].get("repair_pc_id",0l));//为了获得商品ID
				if (product!=null) //以防商品被删除
				{
					floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,repair.get("send_psid",0l),product.get("pc_id", 0l),repairDetails[i].get("repair_normal_count",0f),0);//库存添加
					
					TDate td = new TDate();
					long datemark = td.getDateTime();
					String pcode = product.getString("p_code");
					long adid = adminLoggerBean.getAdid();
					int type = InOutStoreKey.IN_STORE_TRANSPORT_RE;
					productMgr.addInProductLog(pcode,repair.get("send_psid",0l), repairDetails[i].get("repair_normal_count",0f), adid,datemark,1, type);//入库日志
				}
			}
			//入库整件个数
			if(repairDetails[i].get("repair_union_count",0f)>0) {
				DBRow product = floorProductMgr.getDetailProductByPcid(repairDetails[i].get("repair_pc_id",0l));//为了获得商品ID
				if (product!=null) //以防商品被删除
				{
					floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean,repair.get("send_psid",0l),product.get("pc_id", 0l),repairDetails[i].get("repair_union_count",0f),0);//库存添加
					
					TDate td = new TDate();
					long datemark = td.getDateTime();
					String pcode = product.getString("p_code");
					long adid = adminLoggerBean.getAdid();
					int type = InOutStoreKey.IN_STORE_TRANSPORT_RE;
					productMgr.addInProductLog(pcode,repair.get("send_psid",0l), repairDetails[i].get("repair_union_count",0f), adid,datemark,1, type);//入库日志
				}
			}
			
			DBRow detailpara = new DBRow();
			detailpara.add("repair_send_count",0f);
			detailpara.add("repair_union_count",0f);
			detailpara.add("repair_normal_count",0f);
			
			floorRepairOrderMgrZyj.modRepairDetail(repairDetails[i].get("repair_detail_id",0l), detailpara);
		}
		
		//返修单状态变为准备
		DBRow updatepara = new DBRow();
		updatepara.add("repair_status",RepairOrderKey.READY);
		floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, updatepara);
		
		this.insertLogs(repair_order_id, "停止装箱:单号"+repair_order_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods,"",RepairOrderKey.READY);
		
		
	}
	
	public void rebackRepairDamage(HttpServletRequest request) throws Exception
	{
		try 
		{
			long repair_order_id = StringUtil.getLong(request,"repair_order_id");
			DBRow repair = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			long send_psid = repair.get("send_psid", 0L);
			DBRow[] repairDetails = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			//循环退回库存
			for (int i = 0; i < repairDetails.length; i++) 
			{
				long pc_id = repairDetails[i].get("repair_pc_id",0L);
				double repair_total_count = repairDetails[i].get("repair_total_count",0.0);
				//库存日志
				ProductStoreLogBean deProductDamagedCountLogBean = new ProductStoreLogBean();
				deProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
				deProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
				deProductDamagedCountLogBean.setOid(repair_order_id);
				deProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
				deProductDamagedCountLogBean.setPs_id(send_psid);
				deProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_RE);
				deProductDamagedCountLogBean.setQuantity(repair_total_count);
				deProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				deProductDamagedCountLogBean.setPc_id(pc_id);
				productMgr.addProductStoreLogs(deProductDamagedCountLogBean);
				
				//残损商品入库
				DBRow product = floorProductMgr.getDetailProductByPcid(repairDetails[i].get("repair_pc_id",0L));//为了获得商品ID
				if (product!=null) //以防商品被删除
				{
					floorProductMgr.damageProductCountInStore(send_psid, pc_id, repair_total_count);
				}
				//将返修单详细还原成未装箱时的状态
				DBRow detailpara = new DBRow();
				detailpara.add("repair_send_count",0f);
				detailpara.add("repair_union_count",0f);
				detailpara.add("repair_normal_count",0f);
				floorRepairOrderMgrZyj.modRepairDetail(repairDetails[i].get("repair_detail_id",0l), detailpara);
			}
			//修改主单据状态
			DBRow repairOrder = new DBRow();
			repairOrder.add("repair_status", RepairOrderKey.READY);
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, repairOrder);
			
			this.insertLogs(repair_order_id, "装箱:停止装箱", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods,"",RepairOrderKey.READY);
			String is_need_notify_executer = StringUtil.getString(request, "is_need_notify_executer");
			scheduleMgrZr.addScheduleReplayExternal(repair_order_id , ModuleKey.REPAIR_ORDER ,
					4 , "返修单:"+repair_order_id+"装箱:停止装箱" , true, adminLoggerBean, "repair_intransit_period",is_need_notify_executer);
		}
			catch (Exception e) 
		{
			throw new SystemException(e, "rebackRepairDamage", log);
		}
	}
	
	/**
	 * 加跟进日志
	 * @param repair_order_id
	 * @param repair_content
	 * @param adid
	 * @param employe_name
	 * @param repair_type
	 * @param timeComplete
	 * @param stage
	 * @return
	 * @throws Exception
	 */
	private void insertLogs(long repair_order_id ,String repair_content , long adid ,String employe_name, int repair_type , String timeComplete ,int stage) throws Exception{
		try
		{
			floorRepairOrderMgrZyj.insertLogs(repair_order_id, repair_content, adid, employe_name, repair_type, timeComplete, stage);
		}
		catch (Exception e)
		{
			throw new SystemException(e, "insertLogs", log);
		}
	}
	
	/**
	 * 运输中的返修单中止运输
	 * @param request
	 * @throws Exception
	 */
	public void reStorageRepair(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long repair_order_id = StringUtil.getLong(request,"repair_order_id");
			
			DBRow[] repairDetails = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
			//运输中的停止运输，将实际装箱量修改为返修预计量，为了准备中取消回退库存
			for (int i = 0; i < repairDetails.length; i++)
			{
				DBRow para = new DBRow();
				para.add("repair_send_count",0);
				
				floorRepairOrderMgrZyj.modRepairDetail(repairDetails[i].get("repair_detail_id",0l), para);
			}
			
			DBRow repairpara = new DBRow();
			repairpara.add("repair_status",RepairOrderKey.PACKING);
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, repairpara);
//			batchMgrLL.batchStockOutDelete(repair_order_id, 3);
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogs(repair_order_id, "中止运输", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods,"",repairpara.get("repair_status",0));
			scheduleMgrZr.addScheduleReplayExternal(repair_order_id , ModuleKey.REPAIR_ORDER ,
					4 , "返修单:"+repair_order_id+"中止运输" , false, request, "repair_intransit_period");
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"reStorageRepair",log);
		}
	}
	
	/**
	 * 获得全部交货单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllDeliveryOrder(PageCtrl pc)
		throws Exception 
	{
		
		try 
		{
			return floorRepairOrderMgrZyj.getAllDeliveryOrder(pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 生成交货单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addDeliveryOrder(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long delivery_purchase_id = StringUtil.getLong(request,"purchase_id");
			String filename = StringUtil.getString(request,"filename");
			
			String waybill_number = StringUtil.getString(request,"waybill_number");
			String waybill_name = StringUtil.getString(request,"waybill_name");
			String arrival_eta = StringUtil.getString(request,"arrival_eta");
			
			
			
			
			DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(delivery_purchase_id);
			
			long delivery_supplier_id;
			try 
			{
				delivery_supplier_id = Long.parseLong(purchase.getString("supplier"));
			}
			catch (NumberFormatException e)
			{
				delivery_supplier_id = 0;
			}
			//StrUtil.getLong(request,"supplier_id");
			
			DBRow[] delivery_orders = floorRepairOrderMgrZyj.getDelveryOrdersByPurchaseId(delivery_purchase_id);
			String numbers = delivery_orders[delivery_orders.length-1].getString("delivery_order_number").split("-")[1];
			int number = Integer.parseInt(numbers);
			
			DBRow delivery_order = new DBRow();
			delivery_order.add("delivery_purchase_id", delivery_purchase_id);
			delivery_order.add("delivery_supplier_id",delivery_supplier_id);
			delivery_order.add("delivery_address",purchase.getString("delivery_address"));
			delivery_order.add("delivery_linkman",purchase.getString("delivery_linkman"));
			delivery_order.add("delivery_linkman_phone",purchase.getString("linkman_phone"));
			delivery_order.add("delivery_order_number","P"+delivery_purchase_id+"-"+number);
			delivery_order.add("delivery_date",DateUtil.NowStr());
			delivery_order.add("waybill_number",waybill_number);
			delivery_order.add("waybill_name",waybill_name);
			delivery_order.add("arrival_eta",arrival_eta);
			delivery_order.add("delivery_creat_account_id",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());
			delivery_order.add("delivery_creat_account",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name());
			
			
			long delivery_order_id = floorRepairOrderMgrZyj.addDelveryOrder(delivery_order);//创建交货单
			
			/**
			 * 根据采购单未到货创建交货单详细
			 */
			DBRow[] needDelivery;
			
			if(filename.equals(""))
			{
				needDelivery = floorPurchaseMgr.checkPurchaseStats(delivery_purchase_id);
			}
			else
			{
				needDelivery = excelshow(filename);
			}
				
				
			
			for (int i = 0; i < needDelivery.length; i++)
			{
				DBRow dbrow = new DBRow();//needDelivery[i];
				dbrow.add("product_id",needDelivery[i].get("product_id",0l));
				
				if(filename.equals(""))
				{
					dbrow.add("product_name",needDelivery[i].getString("purchase_name"));
					dbrow.add("product_barcode",needDelivery[i].getString("proudct_barcod"));
					dbrow.add("delivery_count",(needDelivery[i].get("purchase_count",0f)-needDelivery[i].get("reap_count",0f)));
				}
				else
				{
					dbrow.add("product_name",needDelivery[i].getString("product_name"));
					dbrow.add("product_barcode",needDelivery[i].getString("product_barcode"));
					dbrow.add("delivery_count",needDelivery[i].getString("delivery_count"));
					dbrow.add("delivery_box",needDelivery[i].getString("delivery_box"));
				}
				
				dbrow.add("delivery_reap_count",0f);
				dbrow.add("delivery_order_id",delivery_order_id);
				
				floorRepairOrderMgrZyj.addDeliveryOrderDetail(dbrow);
			}
			
			/**
			 *修改采购单为交货中状态 
			 */
//			DBRow para = new DBRow();
//			para.add("purchase_status",PurchaseKey.DELIVERYING);
//			floorPurchaseMgr.updatePurchase(delivery_purchase_id, para);
			
			return (delivery_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 将excel文件转换成DBRow[]
	 * @param filename
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] excelshow(String filename)
		throws Exception
	{
		try 
		{
			HashMap<String,String> filedName = new HashMap<String, String>();
			filedName.put("0","p_name");
			filedName.put("1","prefill_deliver_count");
			filedName.put("2","prefill_box");
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			String path = Environment.getHome() + "upl_imags_tmp/" + filename;
			
			InputStream is = new FileInputStream(path);
			Workbook rwb = Workbook.getWorkbook(is);

			Sheet rs = rwb.getSheet(0);
			int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
  
			  for(int i=1;i<rsRows;i++)
			  {
			   DBRow pro = new DBRow();
			   
			   
			   boolean result = false;//判断是否纯空行数据
			   for (int j=0;j<rsColumns;j++)
			   {
			    
				   if(j==0)
				   {
					   DBRow product = floorProductMgr.getDetailProductByPname(rs.getCell(j,i).getContents().trim());
					   if(product==null)//商品查不到，跳过循环，不添加入数组
					   {
						   continue;
					   }
					   pro.add("pc_id",product.get("pc_id",0l));
				   }
				   
				   pro.add(filedName.get(String.valueOf(j)),rs.getCell(j,i).getContents().trim());
				   
				    ArrayList proName=pro.getFieldNames();//DBRow 字段名
				    for(int p=0;p<proName.size();p++)
				    {
				    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
				    	{
				    		result = true;
				    	}
				    }
			   }
			   
			   if(result)//不是纯空行就加入到DBRow
			   {
				   al.add(pro);   
			   }
			  }
			  
			  return (al.toArray(new DBRow[0]));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"excelshow",log);
		}
	}
	
	/**
	 * 获得采购单需交货预填数
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseRepairDetailPrefill(long purchase_id)
		throws Exception
	{
		try {
			DBRow[] purchaseDetails = floorPurchaseMgr.getPurchaseDetail(purchase_id, null,null);
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			
			for (int i = 0; i < purchaseDetails.length; i++) 
			{
				long repair_pc_id = purchaseDetails[i].get("product_id",0l);
				
				DBRow product = floorProductMgr.getDetailProductByPcid(repair_pc_id);
				
				float order_count = purchaseDetails[i].get("order_count",0f);
				float backup_count = purchaseDetails[i].get("backup_count",0f);
				float reap_count = purchaseDetails[i].get("reap_count",0f);
				float purchase_count = purchaseDetails[i].get("purchase_count",0f);
				
				float transit_deliver_count = floorRepairOrderMgrZyj.getTransitDeliverCountForPurchase(purchase_id,repair_pc_id);
				float transit_backup_count = floorRepairOrderMgrZyj.getTransitBackupCountForPurchase(purchase_id,repair_pc_id);
				
				float prefill_backup_count = backup_count-transit_backup_count;
				float prefill_deliver_count = order_count-transit_deliver_count;
				
				float prefill_count = prefill_backup_count+prefill_deliver_count;
				
				if(prefill_count>0&&purchase_count>reap_count)
				{
					DBRow prefill = new DBRow();
					prefill.add("prefill_backup_count",prefill_backup_count);
					prefill.add("prefill_deliver_count",prefill_deliver_count);
					prefill.add("p_name",product.getString("p_name"));
					prefill.add("pc_id",repair_pc_id);
					
					list.add(prefill);
				}
			}
			
			return (list.toArray(new DBRow[0]));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getPurchaseRepairDetailPrefill",log);
		}
		
	}
	
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception {
		try {
			return floorRepairOrderMgrZyj.getAnalysis(st, en, analysisType, analysisStatus, day, pc);
		} catch (Exception e) {
			throw new SystemException(e,"getAnalysis",log);
		}
	}
	
	public DBRow getCountyById(String id) throws Exception {
		try {
			return floorRepairOrderMgrZyj.getCountyById(id);
		} catch (Exception e) {
			throw new SystemException(e,"getCountyById",log);
		}
	}
	
	public DBRow[] getAllAssetsCategory() throws Exception
	{
		try 
		{
			return floorRepairOrderMgrZyj.getAllAssetsCategory();
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getAllAssetsCategory",log);
		}
	}
	
	public DBRow[] getAllAdminGroup() throws Exception
	{
		try 
		{
			return floorRepairOrderMgrZyj.getAllAdminGroup();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllAssetsCategory",log);
		}
	}
	
	public DBRow[] getAllProductLineDefine() throws Exception
	{
		try 
		{
			return floorRepairOrderMgrZyj.getAllProductLineDefine();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAllAssetsCategory",log);
		}
	}
	
	public DBRow[] getAccountView() throws Exception
	{
		try 
		{
			return floorRepairOrderMgrZyj.getAccountView();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getAccountView",log);
		}
	}
	
	@Override
	public DBRow[] getRepairOrderLogs(long repair_order_id, int number)
			throws Exception
	{
		 try
		 {
			return floorRepairOrderMgrZyj.getRepairOrderLogs(repair_order_id, number); 
		 }catch (Exception e) {
			 throw new SystemException(e, "getRepairLogs", log);
		}
	}
	
	/**
	 * 根据当前登录者的Id，获取其上次创建的返修单的ID
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public String getRepairLastTimeCreateByAdid(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean	= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			DBRow row = floorRepairOrderMgrZyj.getRepairLastTimeCreateByAdid(adminLoggerBean.getAdid());
			if(null == row)
			{
				return "";
			}
			else
			{
				return row.getString("repair_order_id");
			}
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "getRepairLastTimeCreateByAdid", log);
		}
	}
	
	/**
	 * 增加返修单
	 * @param request
	 * @throws Exception
	 */
	public long addRepairOrder(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			//收货信息
			long receive_psid = StringUtil.getLong(request,"receive_psid");
			DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(receive_psid);
			long deliver_ccid = StringUtil.getLong(request,"deliver_ccid");
			long deliver_pro_id = StringUtil.getLong(request,"deliver_pro_id");			
			String deliver_city = StringUtil.getString(request,"deliver_city");
			String deliver_house_number = StringUtil.getString(request,"deliver_house_number");
			String deliver_street = StringUtil.getString(request,"deliver_street");
			String deliver_zip_code = StringUtil.getString(request,"deliver_zip_code");
			String deliver_name = StringUtil.getString(request,"deliver_name");
			String deliver_linkman_phone = StringUtil.getString(request,"deliver_linkman_phone");
			
			//提货地址
			long send_psid = StringUtil.getLong(request,"send_psid");
			DBRow sendProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
			long send_ccid = StringUtil.getLong(request,"send_ccid");
			long send_pro_id = StringUtil.getLong(request,"send_pro_id");
			String send_city = StringUtil.getString(request,"send_city");
			String send_house_number = StringUtil.getString(request,"send_house_number");
			String send_street = StringUtil.getString(request,"send_street");
			String send_zip_code = StringUtil.getString(request,"send_zip_code");
			String send_name = StringUtil.getString(request,"send_name");
			String send_linkman_phone = StringUtil.getString(request,"send_linkman_phone");
			String repair_receive_date = StringUtil.getString(request, "repair_receive_date");
			String remark = StringUtil.getString(request, "remark");
			
			//运输设置
			long fr_id = StringUtil.getLong(request,"fr_id");
			String repair_waybill_name = StringUtil.getString(request,"repair_waybill_name");
			String repair_waybill_number = StringUtil.getString(request,"repair_waybill_number");
			int repairby = StringUtil.getInt(request,"repairby");
			String carriers = StringUtil.getString(request,"carriers");
			String repair_send_place = StringUtil.getString(request,"repair_send_place");
			String repair_receive_place = StringUtil.getString(request,"repair_receive_place");
			
			//主流程
			String adminUserIdsTransport	= StringUtil.getString(request, "adminUserIdsTransport");
			String adminUserNamesTransport	= StringUtil.getString(request, "adminUserNamesTransport");
			int needMailTransport			= StringUtil.getInt(request, "needMailTransport");
			int needMessageTransport		= StringUtil.getInt(request, "needMessageTransport");
			int needPageTransport			= StringUtil.getInt(request, "needPageTransport");
			
			//出口报关	
			int declaration					= StringUtil.getInt(request, "declaration");
			String adminUserIdsDeclaration	= StringUtil.getString(request, "adminUserIdsDeclaration");
			String adminUserNamesDeclaration= StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration			= StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration		= StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration			= StringUtil.getInt(request, "needPageDeclaration");
			
			//进口清关
			int clearance					= StringUtil.getInt(request, "clearance");
			String adminUserIdsClearance	= StringUtil.getString(request, "adminUserIdsClearance");
			String adminUserNamesClearance	= StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance			= StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance		= StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance			= StringUtil.getInt(request, "needPageClearance");
			
			//商品图片
			int product_file				= StringUtil.getInt(request, "product_file");
			String adminUserIdsProductFile	= StringUtil.getString(request, "adminUserIdsProductFile");
			String adminUserNamesProductFile= StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile			= StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile		= StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile			= StringUtil.getInt(request, "needPageProductFile");
			
			//制签	
			int tag							= StringUtil.getInt(request, "tag");
			String adminUserIdsTag			= StringUtil.getString(request, "adminUserIdsTag");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag					= StringUtil.getInt(request, "needMailTag");
			int needMessageTag				= StringUtil.getInt(request, "needMessageTag");
			int needPageTag					= StringUtil.getInt(request, "needPageTag");
			
			//运费
			
			int stock_in_set				= StringUtil.getInt(request, "stock_in_set");
			String adminUserIdsStockInSet	= StringUtil.getString(request, "adminUserIdsStockInSet");
			String adminUserNamesStockInSet	= StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet			= StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet		= StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet			= StringUtil.getInt(request, "needPageStockInSet");
			
			//单证
			int certificate					= StringUtil.getInt(request, "certificate");
			String adminUserIdsCertificate	= StringUtil.getString(request, "adminUserIdsCertificate");
			String adminUserNamesCertificate= StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate			= StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate		= StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate			= StringUtil.getInt(request, "needPageCertificate");
			
			//质检
			int quality_inspection 			= StringUtil.getInt(request, "quality_inspection");
			String adminUserIdsQualityInspection	= StringUtil.getString(request, "adminUserIdsQualityInspection");
			String adminUserNamesQualityInspection	= StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection	= StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection= StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection	= StringUtil.getInt(request, "needPageQualityInspection");
			
			DBRow repair = new DBRow();
			//收货信息
			repair.add("receive_psid",receive_psid);
			repair.add("deliver_ccid",deliver_ccid);
			repair.add("deliver_pro_id",deliver_pro_id);
			repair.add("deliver_city",deliver_city);
			repair.add("deliver_house_number",deliver_house_number);
			repair.add("deliver_street",deliver_street);
			repair.add("deliver_zip_code",deliver_zip_code);
			repair.add("repair_address","");
			repair.add("repair_linkman",deliver_name);
			repair.add("repair_linkman_phone",deliver_linkman_phone);

			//提货信息
			repair.add("send_psid",send_psid);
			repair.add("send_city",send_city);
			repair.add("send_house_number",send_house_number);
			repair.add("send_street",send_street);
			repair.add("send_ccid",send_ccid);
			repair.add("send_pro_id",send_pro_id);
			repair.add("send_zip_code",send_zip_code);
			repair.add("send_name",send_name);
			repair.add("send_linkman_phone",send_linkman_phone);
			
			//其他信息(ETA,备注)
			if(!"".equals(repair_receive_date))
			{
				repair.add("repair_receive_date", repair_receive_date);
			}
			repair.add("remark", remark);
			
			long logingUserId = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			String loginUserName = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name();
			
			//主流程信息
			repair.add("repair_status",RepairOrderKey.READY);//创建的返修单都是准备中
			repair.add("repair_date",DateUtil.NowStr());
			repair.add("create_account_id",logingUserId);//返修单的创建者
			repair.add("create_account",loginUserName);//返修单的创建者
			repair.add("updatedate",DateUtil.NowStr());
			repair.add("updateby",logingUserId);//返修单的创建者
			repair.add("updatename",loginUserName);//返修单的创建者
			
			//提货与收货仓库类型
			repair.add("target_ps_type",receiveProductStorage.get("storage_type",0));
			repair.add("from_ps_type",sendProductStorage.get("storage_type",0));
			
			//运输设置
			repair.add("fr_id",fr_id);
			repair.add("repair_waybill_name",repair_waybill_name);
			repair.add("repair_waybill_number",repair_waybill_number);
			repair.add("repairby",repairby);
			repair.add("carriers",carriers);
			repair.add("repair_send_place",repair_send_place);
			repair.add("repair_receive_place",repair_receive_place);
			
			//流程信息
			repair.add("declaration", declaration);
			repair.add("clearance", clearance);
			repair.add("product_file", product_file);
			repair.add("tag", tag);
			repair.add("stock_in_set", stock_in_set);
			repair.add("certificate", certificate);
			repair.add("quality_inspection", quality_inspection);
			
			long repair_order_id = floorRepairOrderMgrZyj.addRepairOrder(repair);
			
			//添加返修单详细
			this.addPurchaseRepairDetail(0, repair_order_id, request);
			
			this.insertLogs(repair_order_id, "创建返修单:单号"+repair_order_id, (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid(), (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name(),RepairLogTypeKey.Goods,"",RepairOrderKey.READY);//类型1跟进记录
			
			
			this.repairOrderSetFreight(repair_order_id, request);
			
			//添加返修单索引
			this.editRepairOrderIndex(repair_order_id, "add");
			
			//返修单与交货单发送邮件的内容不同，需要区分是供应商还是仓库，组织内容时需要传采购单的Id，返修单传0
			addSculde(repair_order_id,0L,sendProductStorage.getString("title"),receiveProductStorage.getString("title"),repair_receive_date, logingUserId, loginUserName, request, 
					adminUserIdsTransport, adminUserNamesTransport, needMailTransport, needMessageTransport, needPageTransport,
					declaration, adminUserNamesDeclaration, adminUserIdsDeclaration, needPageDeclaration, needMailDeclaration, needMessageDeclaration,
					clearance, adminUserNamesClearance, adminUserIdsClearance, needPageClearance, needMailClearance, needMessageClearance, 
					product_file,adminUserNamesProductFile,adminUserIdsProductFile,needPageProductFile,needMailProductFile,needMessageProductFile,
					tag,adminUserNamesTag,adminUserIdsTag,needPageTag,needMailTag,needMessageTag,
					stock_in_set,adminUserNamesStockInSet,adminUserIdsStockInSet,needPageStockInSet,needMailStockInSet,needMessageStockInSet, 
					certificate,adminUserNamesCertificate,adminUserIdsCertificate,needPageCertificate,needMailCertificate,needMessageCertificate,
					quality_inspection,adminUserNamesQualityInspection,adminUserIdsQualityInspection,needPageQualityInspection,needMailQualityInspection,needMessageQualityInspection);
			
			return repair_order_id;
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 采购单交货返修单生成返修单明细
	 * @param purchase_id
	 * @param repair_order_id
	 * @throws Exception
	 */
	private void addPurchaseRepairDetail(long purchase_id,long repair_order_id,HttpServletRequest request)
		throws Exception
	{
		try 
		{
			floorRepairOrderMgrZyj.delRepairDetailByRepairId(repair_order_id);//批量删除返修明细
			
			String[] pcids = request.getParameterValues("pc_id");
			if (pcids==null)
			{
				pcids = new String[0];
			}
			for (int i = 0; i < pcids.length; i++) 
			{
				long pc_id = Long.parseLong(pcids[i]);
				
				float deliver_count = StringUtil.getFloat(request,pcids[i]+"_deliver_count");
				float backup_count = StringUtil.getFloat(request,pcids[i]+"_backup_count");
				String box = StringUtil.getString(request,pcids[i]+"_box");
				float count = deliver_count+backup_count;
				
				DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id, pc_id);
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				
				double send_price = product.get("unit_price",0d);
				if(purchaseDetail!=null)
				{
					send_price = purchaseDetail.get("price",0d);
				}
				
				float union_count = 0;
				float normal_count = 0;
				
				if(product.get("union_flag",0)==1)
				{
					union_count = count;
				}
				else
				{
					normal_count = count;
				}
				
				DBRow repairDetail = new DBRow();
				repairDetail.add("repair_pc_id",pc_id);
				repairDetail.add("repair_delivery_count",deliver_count);
				repairDetail.add("repair_backup_count",backup_count);
				repairDetail.add("repair_total_count",count);
				repairDetail.add("repair_order_id",repair_order_id);
				repairDetail.add("repair_p_name",product.getString("p_name"));
				repairDetail.add("repair_p_code",product.getString("p_code"));
				repairDetail.add("repair_volume",product.getString("volume"));
				repairDetail.add("repair_weight",product.getString("weight"));
				repairDetail.add("repair_box",box);
				
				repairDetail.add("repair_send_price",send_price);
				repairDetail.add("repair_union_count",union_count);
				repairDetail.add("repair_normal_count",normal_count);
				
				floorRepairOrderMgrZyj.addRepairOrderDetail(repairDetail);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addPurchaserepairDetail",log);
		}
	}
	
	/**
	 * 操作返修单索引
	 * @param repair_order_id 返修单号
	 * @param type		   操作类型(add,update)
	 * @throws Exception
	 */
	public void editRepairOrderIndex(long repair_order_id,String type)
		throws Exception
	{
		DBRow oldRepair = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);

		String sendStorage = "";
		DBRow sendProductStorageCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(oldRepair.get("send_psid",0l));
		sendStorage = sendProductStorageCatalog.getString("title");
		
		DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(oldRepair.get("receive_psid",0l));
		
		if(type.equals("add"))
		{
			RepairOrderIndexMgr.getInstance().addIndex(repair_order_id,sendStorage,receiveProductStorage.getString("title"),oldRepair.getString("repair_waybill_number"),oldRepair.getString("repair_waybill_name"),oldRepair.getString("carriers"));
		}
		else if(type.equals("update"))
		{
			RepairOrderIndexMgr.getInstance().updateIndex(repair_order_id,sendStorage,receiveProductStorage.getString("title"),oldRepair.getString("repair_waybill_number"),oldRepair.getString("repair_waybill_name"),oldRepair.getString("carriers"));
		}
		else if(type.equals("del"))
		{
			RepairOrderIndexMgr.getInstance().deleteIndex(repair_order_id);
		}
	}

	private void addSculde(long repair_order_id,long purchase_id,String cata_name_send,String cata_name_deliver,String eta,long logingUserId,String loginUserName,HttpServletRequest request,
			String adminUserIdsTransport, String adminUserNamesTransport, int needMailTransport, int needMessageTransport, int needPageTransport,
			int declaration,String adminUserNamesDeclaration,String adminUserIdsDeclaration,int needPageDeclaration,int needMailDeclaration,int needMessageDeclaration,
			int clearance,String adminUserNamesClearance,String adminUserIdsClearance,int needPageClearance,int needMailClearance,int needMessageClearance,
			int product_file,String adminUserNamesProductFile,String adminUserIdsProductFile,int needPageProductFile,int needMailProductFile,int needMessageProductFile,
			int tag,String adminUserNamesTag,String adminUserIdsTag,int needPageTag,int needMailTag,int needMessageTag,
			int stock_in_set,String adminUserNamesStockInSet,String adminUserIdsStockInSet,int needPageStockInSet,int needMailStockInSet,int needMessageStockInSet,
			int certificate,String adminUserNamesCertificate,String adminUserIdsCertificate,int needPageCertificate,int needMailCertificate,int needMessageCertificate,
			int quality_inspection,String adminUserNamesQualityInspection,String adminUserIdsQualityInspection,int needPageQualityInspection,int needMailQualityInspection,int needMessageQualityInspection
			)
			throws Exception
		{
			String sendContent = this.handleSendContent(repair_order_id, purchase_id, declaration, adminUserNamesDeclaration,
					clearance, adminUserNamesClearance, product_file, adminUserNamesProductFile,
					tag, adminUserNamesTag, stock_in_set, adminUserNamesStockInSet, 
					certificate, adminUserNamesCertificate, quality_inspection, adminUserNamesQualityInspection,cata_name_send,cata_name_deliver,eta);
			
			//转运单主流程任务
			this.addTransportScheduleOneProdure(DateUtil.NowStr(), "",logingUserId,
					adminUserIdsTransport, "", 
					repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[主流程]", 
					sendContent,
					needPageTransport, needMailTransport, needMessageTransport, request, true, 4,
					RepairOrderKey.READY,loginUserName, "创建返修单"+repair_order_id+"[备货中]");
			
			//报关
			if(RepairDeclarationKey.DELARATION == declaration)
			{
				this.addTransportScheduleOneProdure("", "",logingUserId,
						adminUserIdsDeclaration, "", 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[报关流程]", 
						sendContent,
						needPageDeclaration, needMailDeclaration, needMessageDeclaration, request, false, 7,
						RepairDeclarationKey.DELARATION,loginUserName, "创建返修单"+repair_order_id+"[需要报关]");
			}
			
			//清关
			if(RepairClearanceKey.CLEARANCE == clearance)
			{
				this.addTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsClearance, "", 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[清关流程]",
						sendContent,
						needPageClearance, needMailClearance, needMessageClearance, request, false, 6,
						RepairClearanceKey.CLEARANCE, loginUserName, "创建返修单"+repair_order_id+"[需要清关]");
			}
			//商品图片
			if(RepairProductFileKey.PRODUCTFILE == product_file)
			{
				TDate tdateProductFile = new TDate();
				tdateProductFile.addDay(Integer.parseInt(systemConfig.getStringConfigValue("repair_product_file_period")));
				String endTimeProductFile = tdateProductFile.formatDate("yyyy-MM-dd HH:mm:ss");
				
				this.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeProductFile, logingUserId,
						adminUserIdsProductFile, "", 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[商品图片流程]",
						sendContent,
						needPageProductFile, needMailProductFile, needMessageProductFile, request, true, 10,
						RepairProductFileKey.PRODUCTFILE, loginUserName, "创建返修单"+repair_order_id+"[商品文件上传中]");
			}
			//制签
			if(RepairTagKey.TAG == tag) 
			{
				TDate tdateTag = new TDate();
				tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("repair_tag_period")));
				String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
				//添加
				this.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeTag, logingUserId,
						adminUserIdsTag, "", 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[制签流程]",
						sendContent,
						needPageTag, needMailTag, needMessageTag, request, true, 8,
						RepairTagKey.TAG, loginUserName, "创建返修单"+repair_order_id+"[制签中]");
			}
			
			//运费
			if(RepairStockInSetKey.SHIPPINGFEE_SET == stock_in_set)
			{
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsStockInSet, "", 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[运费流程]",
						sendContent,
						needPageStockInSet, needMailStockInSet, needMessageStockInSet, request, false, 5,
						RepairStockInSetKey.SHIPPINGFEE_SET, loginUserName, "创建返修单"+repair_order_id+"[需要运费]");
			}
			
			//单证
			if(RepairCertificateKey.CERTIFICATE == certificate)
			{
				TDate tdateCertificate = new TDate();
				tdateCertificate.addDay(Integer.parseInt(systemConfig.getStringConfigValue("repair_certificate_period")));
				String endTimeCertificate = tdateCertificate.formatDate("yyyy-MM-dd HH:mm:ss");
				this.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeCertificate, logingUserId,
						adminUserIdsCertificate, "", 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[单证流程]",
						sendContent,
						needPageCertificate, needMailCertificate, needMessageCertificate, request, true, 9,
						RepairCertificateKey.CERTIFICATE, loginUserName, "创建返修单"+repair_order_id+"[单证采集中]");
			}
			
			//质检
			if(RepairQualityInspectionKey.NEED_QUALITY == quality_inspection)
			{
				TDate tdateQuality = new TDate();
				tdateQuality.addDay(Integer.parseInt(systemConfig.getStringConfigValue("repair_quality_inspection")));
				String endTimeQuality = tdateQuality.formatDate("yyyy-MM-dd HH:mm:ss");
				this.addTransportScheduleOneProdure(DateUtil.NowStr(), endTimeQuality, logingUserId,
						adminUserIdsQualityInspection, "", 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[质检流程]",
						sendContent,
						needPageQualityInspection, needMailQualityInspection, needMessageQualityInspection, request, true, 11,
						RepairQualityInspectionKey.NEED_QUALITY, loginUserName, "创建返修单"+repair_order_id+"[需要质检]");
			}
		}
	
	/**
	 * 添加任务
	 */
	private void addTransportScheduleOneProdure(String beginTime,
			String endTime, long adid, String executePersonIds,
			String joinPersonIds, long repair_order_id, int moduleId,
			String title, String content, int page, int mail,
			int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, String employeeName, String logContent) throws Exception {
		
		try
		{
			boolean needPage	= false;
			boolean needMail	= false;
			boolean needMessage	= false;
			if(2 == page){
				needPage = true;
			}
			if(2 == mail){
				needMail = true;
			}
			if(2 == message){
				needMessage = true;
			}
			//添加之前再确定一下，是否此流程的任务真的没有
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(repair_order_id, moduleId, processType);
			if(null == schedule){
				scheduleMgrZr.addScheduleByExternalArrangeNow(beginTime, endTime, 
						adid, executePersonIds, joinPersonIds,
						repair_order_id, moduleId, title, content,
						needPage, needMail, needMessage, request, isNow, processType);
				//添加日志
				this.insertLogs(repair_order_id, logContent, adid, employeeName, processType,"", activity);
			}
			
			
		}catch (Exception e)
		{
			throw new SystemException(e,"addTransportScheduleOneProdure",log);
		}
		
	}
	
	/**
	 * 构成内容
	 * @return
	 * @throws Exception
	 */
	public String handleSendContent(long repair_order_id, long purchase_id, int declaration, String declarationPerson,int clearance, String clearancePerson,int productFile, String productPerson,int tag, String tagPerson,int stockInSet, String stockPerson,int certificate, String certificatePerson,int quality, String qualityPerson,String cata_name_send,String cata_name_deliver,String eta) 
		throws Exception
	{		
		try
		{
			String rnStr= "";
			//获取提货仓库的名字
			if(0 == purchase_id)
			{
				rnStr += "提货仓库:"+cata_name_send+",";
			}
			//如果是交货单
			else
			{
				rnStr += "供应商:"+cata_name_send+",";
			}
			//获取收货仓库的名字
			rnStr += "收货仓库:"+cata_name_deliver+",";
			//获取eta
			if(!"".equals(eta))
			{
				rnStr += "预计到货时间:"+eta+",";
			}
			//返修货物的总体积，如果 有值，添加，此处待完善
			rnStr += "总体积:" +this.getRepairOrderVolume(repair_order_id);
			
			return rnStr;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"handleSendContent",log);
		}
	}
	
	public void repairOrderSetFreight(long repair_order_id,HttpServletRequest request) 
	throws Exception 
	{
		String[] tfc_ids = request.getParameterValues("tfc_id");
		String[] tfc_project_names = request.getParameterValues("tfc_project_name");
		String[] tfc_ways = request.getParameterValues("tfc_way");
		String[] tfc_units = request.getParameterValues("tfc_unit");
		String[] tfc_unit_prices = request.getParameterValues("tfc_unit_price");
		String[] tfc_unit_counts = request.getParameterValues("tfc_unit_count");
		String[] tfc_currency = request.getParameterValues("tfc_currency");
		String[] tfc_exchange_rate = request.getParameterValues("tfc_exchange_rate");
//		int stock_in_set = StrUtil.getInt(request,"stock_in_set");
		int changed = StringUtil.getInt(request,"changed",0);
		
		if(changed == 2) 
		{
			floorRepairOrderMgrZyj.deleteRepairOrderFreightCostByRepairId(String.valueOf(repair_order_id));
		}
		
		for(int i=0;tfc_ids!=null && i<tfc_ids.length;i++) 
		{
			DBRow row = new DBRow();
			row.add("tfc_id", tfc_ids[i]);
			row.add("repair_order_id", repair_order_id);
			row.add("tfc_project_name", tfc_project_names[i]);
			row.add("tfc_way", tfc_ways[i]);
			row.add("tfc_unit", tfc_units[i]);
			row.add("tfc_unit_price", tfc_unit_prices[i]);
			row.add("tfc_unit_count", tfc_unit_counts[i]);
			row.add("tfc_currency", tfc_currency[i]);
			row.add("tfc_exchange_rate", tfc_exchange_rate[i]);
			
			if(changed == 2 || row.getString("tfc_id").equals("") || row.getString("tfc_id").equals("0")) 
			{
				row.remove("tfc_id");
				floorRepairOrderMgrZyj.insertRepairOrderFreightCost(row);
			}
			else 
			{
				floorRepairOrderMgrZyj.updateRepairOrderFreightCost(row, Long.parseLong(tfc_ids[i]));
			}		
		}
//		DBRow transport = floorRepairOrderMgrZyj.getDetailTransportById(repair_order_id);
		
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
//		if(transport.get("transport_status",0)==RepairOrderKey.FINISH&&transport.get("stock_in_set",0)!=TransportStockInSetKey.SHIPPINGFEE_NOTSET)
//		{
//			batchMgrLL.batchTransportStockInChangeFreight(repair_order_id, adminLoggerBean);
//		}
		this.insertLogs(repair_order_id, "返修单设置运费:单号"+repair_order_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3,"",0);
	}
	
	@Override
	public void repairProductFileFollowUp(HttpServletRequest request)
			throws Exception
	{
		try
		{
			long repair_order_id = StringUtil.getLong(request, "repair_order_id");
			String context = "返修单:"+repair_order_id+StringUtil.getString(request, "context") ;
			String eta = StringUtil.getString(request, "eta");
			int productFile = StringUtil.getInt(request, "product_file");
			boolean isover = false; 
			if(productFile == RepairProductFileKey.FINISH){
				eta = null ;
				DBRow row = new DBRow();
				String repair_date = StringUtil.getString(request, "repair_date");
				TDate tDate = new TDate();
				TDate endDate = new TDate(repair_date);
				double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
				row.add("product_file_over", diffDay); // 商品图片减去开始时间
				row.add("product_file",RepairProductFileKey.FINISH);
				floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, row);
				isover = true; 
			}
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogs(repair_order_id, context, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.ProductShot, eta, productFile);
			scheduleMgrZr.addScheduleReplayExternal(repair_order_id, ModuleKey.REPAIR_ORDER,RepairLogTypeKey.ProductShot, context, isover, request, "repair_product_file_period");

		}catch (Exception e)
		{
			 throw new SystemException(e, "repairProductFileFollowUp", log);
		}
		
		
	}
	/**
	 * 通过返修单ID，日志类型，查询相应日志
	 */
	public DBRow[] getRepairLogsByRepairIdAndType(long repair, int[] types) throws Exception{
		try {
			return floorRepairOrderMgrZyj.getRepairLogsByRepairIdAndType(repair, types);
		} catch (Exception e) {
			throw new SystemException(e,"getRepairLogsByRepairIdAndType",log);
		}
	}
	
	@Override
	public void handleRepairDeclaration(HttpServletRequest request)
			throws Exception {
		try{
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			//重新命名图片文件T_declaration_transportId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			long file_with_id  = StringUtil.getLong(request, "repair_order_id");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class  = StringUtil.getInt(request, "declaration");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false ) && (file_with_class == RepairDeclarationKey.FINISH) ;
			StringBuffer fileNameSb = new StringBuffer(""); 
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request,"context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
							.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorRepairOrderMgrZyj.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							 
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
							this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						}
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.ExportCustoms, eta, file_with_class);
			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.ExportCustoms, null, file_with_class);
			}		 
			DBRow updateRepairRow = new DBRow();
			updateRepairRow.add("declaration", file_with_class);
			if(file_with_class == RepairDeclarationKey.FINISH){
				DBRow repairRow = floorRepairOrderMgrZyj.getDetailRepairOrderById(file_with_id);
			 	double diffDay = this.getCostTimeOfRepairProcess(file_with_id,RepairLogTypeKey.ExportCustoms,RepairDeclarationKey.DELARATING,repairRow.getString("repair_date"));
				updateRepairRow.add("declaration_over", diffDay);
			}
			floorRepairOrderMgrZyj.modRepairOrder(file_with_id, updateRepairRow);

			boolean isCompelte = (file_with_class == RepairDeclarationKey.FINISH);
			String scheduleContext = "返修单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.ExportCustoms,scheduleContext, isCompelte, request, "repair_declaration_period");
 
		}catch (Exception e) {
			throw new SystemException(e, "handleRepairDeclaration", log);
		}
	}
	/**
	 * 文件不能为重复的。就是说文件如果有了，那么就会覆盖掉原来数据库中的数据
	 * @param fileName
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param upload_adid
	 * @param upload_time
	 * @throws Exception
	 */
	// 在File表中添加一个字段
	private void addTransportFileLogs(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception{
		try{
 
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",upload_adid);
			file.add("upload_time",DateUtil.NowStr());
			floorRepairOrderMgrZyj.addFileNotExits(file); //// 都是插入的同一张表 所以用这个方法;
		}catch (Exception e) {
			 throw new SystemException(e, "addTransportLogs", log);
		}
	}
	/**
	 * 得到流程所花费的时间
	 * 查询日志中的记录的第一条日志作为开始的点。
	 * 如果没有那么就是用创建时间的作为开始的点
	 * @param repair_order_id
	 * @return
	 */
	private double getCostTimeOfRepairProcess(long repair_order_id , int repair_type , int activity_id , String repairCreateTime) throws Exception {
		try{
			DBRow firstLogClearance = floorRepairOrderMgrZyj.getFirstTransportIngLogTime(repair_order_id, repair_type, activity_id);
			String firstTime = "";
			if(firstLogClearance != null){
				firstTime = firstLogClearance.getString("repair_date");
			}
			if(firstTime.trim().length() < 1){
				firstTime = repairCreateTime;
			}
			TDate tDate = new TDate();
			TDate endDate = new TDate(firstTime);
			// 流程的完成时所需时间
		     double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");;
			return diffDay;
		}catch (Exception e) {
			throw new SystemException(e, "getCostTimeOfRepairProcess", log);
		}
	}
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	
	@Override
	public void handleRepairClearance(HttpServletRequest request) throws Exception {
		try{
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			//如果是完成那么是要记录文件路径,记录日志.更新主表的记录。
			// 移动文件的位置。如果是有待上传的文件
			// 文件的命名规范为 T_clearance_阶段状态_232323_index.jpg;
			// 清关文件支持多个
			 
			long file_with_id = StringUtil.getLong(request, "repair_order_id");
			int  file_with_class = StringUtil.getInt(request, "clearance");
			
			int file_with_type = StringUtil.getInt(request,"file_with_type");
		  
 			String sn = StringUtil.getString(request,"sn");
			String path = StringUtil.getString(request,"path");
 			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = ( fileNames.trim().length() > 0 ? true:false ) && (file_with_class == RepairClearanceKey.FINISH);
			StringBuffer fileNameSb = new StringBuffer();
			if(fileNames.trim().length() > 0 ){
				String[] fileNameArray =  fileNames.split(",");
				if(fileNameArray != null && fileNameArray.length > 0){
					for(String tempFileName : fileNameArray){
						StringBuffer context = new StringBuffer();
						context.append(StringUtil.getString(request,"context"));
						// 获取真实的名字,把文件该名称
						// 清关文件支持多个,文件名称T_clearance_transportId_index
						
						//1.移动文件
						String suffix = getSuffix(tempFileName);
						String  tempUrl =  baseTempUrl+tempFileName;
						
						StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
						.append("_").append(tempFileName.replace("."+suffix, ""));
						int indexFileName = floorRepairOrderMgrZyj.getIndexOfFilebyFileName(realFileName.toString());
						if(indexFileName != 0){
							realFileName.append("_").append(indexFileName);
						}
						realFileName.append(".").append(suffix);
						fileNameSb.append(",").append(realFileName);
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
					
						FileUtil.moveFile(tempUrl,url.toString());
						
						// file表上添加一条记录
						this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						
					}
					
				}
			}
			//添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				// 如果不是上传文件。那么是要有eta的
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.ImportCustoms, eta, file_with_class);

			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.ImportCustoms, null, file_with_class);
			}

			//更新主transport上的信息表示当前清关已经完成
			DBRow updateRepairRow = new DBRow();
			updateRepairRow.add("clearance", file_with_class);
			if(file_with_class == RepairClearanceKey.FINISH){
				DBRow repairRow = floorRepairOrderMgrZyj.getDetailRepairOrderById(file_with_id);
				updateRepairRow.add("clearance_over", this.getCostTimeOfRepairProcess(file_with_id,RepairLogTypeKey.ImportCustoms,RepairClearanceKey.CLEARANCEING,repairRow.getString("repair_date")));
			}
			floorRepairOrderMgrZyj.modRepairOrder(file_with_id, updateRepairRow);
					
			//任务的跟进
			boolean isCompelte = (file_with_class == RepairClearanceKey.FINISH);
			String scheduleContext = "返修单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.ImportCustoms, scheduleContext, isCompelte, request, "repair_clearance_period");
			 
		}catch (Exception e) {
			throw new SystemException(e, "handleRepairClearance", log);
		}
		
	}
	
	public void hanleRepairStockInSet(HttpServletRequest request)
	throws Exception {
		try{
			
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			//重新命名图片文件T_declaration_transportId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			long file_with_id  = StringUtil.getLong(request, "repair_order_id");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class  = StringUtil.getInt(request, "stock_in_set"); 
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false ) && (file_with_class ==  RepairStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH) ;
			StringBuffer fileNameSb = new StringBuffer(""); 
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request,"context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
							.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorRepairOrderMgrZyj.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							 
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
							this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						
						}
		
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Freight, eta, file_with_class);
			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Freight, null, file_with_class);
			}		 
			DBRow updateTransportRow = new DBRow();
			updateTransportRow.add("stock_in_set", file_with_class);
			if(file_with_class == RepairStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH){
				//如果是运费转账完成的话
				DBRow transportRow = floorRepairOrderMgrZyj.getDetailRepairOrderById(file_with_id);
			 
			 	double diffDay = this.getCostTimeOfRepairProcess(file_with_id,RepairLogTypeKey.Freight,RepairStockInSetKey.SHIPPINGFEE_SET,transportRow.getString("repair_date"));
		
				updateTransportRow.add("stock_in_set_over", diffDay);
			}
			floorRepairOrderMgrZyj.modRepairOrder(file_with_id, updateTransportRow);
		
			boolean isCompelte = (file_with_class == RepairStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH);
			String scheduleContext = "返修单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.Freight,scheduleContext, isCompelte, request, "repair_stock_in_set_period");
		}catch (Exception e) {
			 throw new SystemException(e, "hanleRepairStockInSet", log);
		}
	}
	public void hanleRepairQualityInspection(HttpServletRequest request)
	throws Exception {
	 try{
		 
		 
		 AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
		 
			//重新命名图片文件T_quality_inspection文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			long file_with_id  = StringUtil.getLong(request, "repair_order_id");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class  = StringUtil.getInt(request, "quality_inspection");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false )   ;
			StringBuffer fileNameSb = new StringBuffer(""); 
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request,"context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
							.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorRepairOrderMgrZyj.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							 
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
							this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						}
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.QualityControl, eta, file_with_class);
			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.QualityControl, null, file_with_class);
			}	
			
			DBRow updateTransportRow = new DBRow();
			updateTransportRow.add("quality_inspection", file_with_class);
			if( file_with_class == RepairQualityInspectionKey.FINISH ){
				//如果是报告完成的话那么就是整体完成
				DBRow transportRow = floorRepairOrderMgrZyj.getDetailRepairOrderById(file_with_id);
				//质检是用开始时间来计算的
			 	double diffDay = this.getDiffTimeByTransportCreateTime(transportRow.getString("repair_date"));
	
				updateTransportRow.add("quality_inspection_over", diffDay);
			}
			floorRepairOrderMgrZyj.modRepairOrder(file_with_id, updateTransportRow);
	
			boolean isCompelte = (file_with_class == RepairQualityInspectionKey.FINISH);
			String scheduleContext = "返修单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.QualityControl,scheduleContext, isCompelte, request, "repair_quality_inspection");
	 
	 }
	 catch (Exception e) 
	 {
		 throw new SystemException(e, "hanleRepairQualityInspection", log);
	 }
	}
	private double getDiffTimeByTransportCreateTime(String transport_date) throws Exception{
		try {
			TDate tDate = new TDate();
			TDate endDate;
	
			endDate = new TDate(transport_date);
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			return diffDay;
		} catch (Exception e) {
			 throw new SystemException(e, "getDiffTimeByTransportCreateTime", log);
		}
		
	}
	
	@Override
	public void handleRepairTag(HttpServletRequest request)
			throws Exception {
		try{
			
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			//重新命名图片文件T_tag_transportId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			long file_with_id  = StringUtil.getLong(request, "repair_order_id");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class  = StringUtil.getInt(request, "tag");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false ) && (file_with_class == RepairTagKey.FINISH) ;
			StringBuffer fileNameSb = new StringBuffer(""); 
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request,"context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
							.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorRepairOrderMgrZyj.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							 
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
							this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						
						}
	 
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Label, eta, file_with_class);
			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Label, null, file_with_class);
			}		 
			DBRow updateTransportRow = new DBRow();
			updateTransportRow.add("tag", file_with_class);
			if(file_with_class == RepairTagKey.FINISH){
				//如果是运费转账完成的话
				DBRow transportRow = floorRepairOrderMgrZyj.getDetailRepairOrderById(file_with_id);
			 
			 	double diffDay = this.getDiffTimeByTransportCreateTime(transportRow.getString("repair_date"));

				updateTransportRow.add("tag_over", diffDay);
			}
			floorRepairOrderMgrZyj.modRepairOrder(file_with_id, updateTransportRow);

			boolean isCompelte = (file_with_class == RepairTagKey.FINISH);
			String scheduleContext = "返修单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.Label,scheduleContext.toString(), isCompelte, request, "repair_tag_period");
		 
		}catch (Exception e) {
			 throw new SystemException(e, "handleRepairTag", log);
		}
	}
	
	@Override
	public void addRepairLog(HttpServletRequest request) throws Exception {
		try{
			long repair_order_id = StringUtil.getLong(request,"repair_order_id");
			String transport_content = StringUtil.getString(request,"repair_content");
			String eta = StringUtil.getString(request, "eta");
			int transport_type = StringUtil.getInt(request,"repair_type");
			int stage = StringUtil.getInt(request,"stage");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogs(repair_order_id, transport_content, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),transport_type,eta,stage );
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addRepairLog", log);
		}
		
	}
	
	@Override
	public void repairCertificateFollowUp(HttpServletRequest request)
			throws Exception
	{
		try
		{
			long repair_order_id = StringUtil.getLong(request, "repair_order_id");
			String context = "返修单:"+repair_order_id+StringUtil.getString(request, "context") ;
			String eta = StringUtil.getString(request, "eta");
			int certificate = StringUtil.getInt(request, "certificate");
			boolean isover = false; 
			if(certificate == RepairCertificateKey.FINISH){
				eta = null ;
				DBRow row = new DBRow();
				String transport_date = StringUtil.getString(request, "repair_date");
				TDate tDate = new TDate();
				TDate endDate = new TDate(transport_date);
				double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
				row.add("certificate_over", diffDay); // 单证减去开始时间
				row.add("certificate",RepairCertificateKey.FINISH);
				floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, row);
				isover = true; 
			}
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogs(repair_order_id, context, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), RepairLogTypeKey.Document, eta, certificate);
			scheduleMgrZr.addScheduleReplayExternal(repair_order_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.Document, context, isover, request, "repair_certificate_period");

		}catch (Exception e)
		{
			 throw new SystemException(e, "repairCertificateFollowUp", log);
		}
		
	}
	
	/**
	 * 删除返修单
	 * @param request
	 * @throws Exception
	 */
	public void delRepairOrder(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long repair_order_id = StringUtil.getLong(request,"repair_order_id");
			long purchase_id = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id).get("purchase_id", 0L);
			
			floorRepairOrderMgrZyj.delRepairDetailByRepairId(repair_order_id);
			//删除日志
			floorRepairOrderMgrZyj.deleteRepairLogsByRepairId(repair_order_id);
			//删除运费项目
			floorRepairOrderMgrZyj.deleteRepairFreightByRepairId(repair_order_id);
			//删除文件，(单证，清关，报关，运费，制签)需要根据关联ID，关联类型的数组，因为采购和返修单可能冲突，导致误删
			int[] types = {FileWithTypeKey.REPAIR_CERTIFICATE,FileWithTypeKey.REPAIR_CLEARANCE,FileWithTypeKey.REPAIR_DECLARATION,FileWithTypeKey.REPAIR_STOCKINSET,FileWithTypeKey.REPAIR_TAG};
			floorRepairOrderMgrZyj.deleteTransportFileByTransportIdAndTypes(repair_order_id, types);
			//删除返修单下商品的文件(7,16)(商品文件，商品标签文件)
			int[] productTypes = {FileWithTypeKey.REPAIR_PRODUCT_FILE, FileWithTypeKey.REPAIR_PRODUCT_TAG_FILE};
			floorRepairOrderMgrZyj.deleteTransportProductFileByTransportIdAndTypes(repair_order_id, productTypes);
			//删除任务
			this.deleteRepairOrderSchedule(repair_order_id,purchase_id, request);
			
			this.editRepairOrderIndex(repair_order_id, "del");
			floorRepairOrderMgrZyj.delRepairOrder(repair_order_id);
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delTransport",log);
		}
	}
	
	/**
	 * 删除任务
	 * @throws Exception
	 */
	private void deleteRepairOrderSchedule(long repair_order_id, long purchase_id, HttpServletRequest request) throws Exception
	{
		try 
		{
			//各流程任务
			int[] associateTypeArr = {ModuleKey.REPAIR_ORDER};
			DBRow[] transportSchedules = scheduleMgrZr.getScheduleByAssociateIdAndType(repair_order_id, associateTypeArr);
			for (int i = 0; i < transportSchedules.length; i++) {
				scheduleMgrZr.deleteScheduleByScheduleId(transportSchedules[i].get("schedule_id", 0L), request);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"deleteTransportSchedule",log);
		}
	}
	
	/**
	 * 检查库存，返回缺货信息
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkStorage(long repair_order_id)
	throws Exception
	{
		try 
		{
			DBRow[] transportDetails = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id);
			
			if(!(transportDetails.length>0))
			{
				throw new NoExistTransportDetailException();
			}
			
			DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			long ps_id = transport.get("send_psid",0l);
			DBRow productCount = new DBRow();
			
			ArrayList<DBRow> result = new ArrayList<DBRow>();
			
			boolean orderIsLacking = false;
			for (int j=0; j<transportDetails.length; j++)
			{
					long pid = StringUtil.getLong(transportDetails[j].getString("repair_pc_id"));
					DBRow product = floorProductMgr.getDetailProductByPcid(pid);//每个商品信息，为了获得商品
	
					float quantity = StringUtil.getFloat(transportDetails[j].getString("repair_total_count"));
	
					if (product.get("union_flag",0)==0)//0散件
					{
						int lacking = 0;
	
						DBRow detailPro = floorProductMgr.getDamageProductByStorageAndProductId(ps_id,pid);
	
						if (detailPro==null)
						{
							lacking = 1;
							orderIsLacking = true;
						}
						else
						{
							if (detailPro.get("damaged_count_tatal", 0f)<quantity+this.getCumProductQuantity(productCount, pid))
							{
								lacking = 1;
								orderIsLacking = true;
							}
						}
						this.addCumProductQuantity(productCount, pid, quantity);
						
						//对原来的购物车中的item增加一个缺货标记字段
						transportDetails[j].add("lacking", lacking);
					}
					else if (product.get("union_flag",0)==1)//1套装
					{
						int lacking = 0;
						boolean standardUnionLacking = false;
						boolean manualUnionLacking = false;
						float store_count = 0;
						
						DBRow detailPro = floorProductMgr.getDamageProductByStorageAndProductId(ps_id,pid);
							
						if (detailPro==null)
						{
							lacking = 1;
							standardUnionLacking = true;
						}
						else
						{
							
							store_count = detailPro.get("damaged_count_tatal", 0f);
							
							if (store_count<quantity)
							{
								lacking = 1;
								standardUnionLacking = true;
							}
						}
						//标准套装有货
						if (lacking==0)
						{
							transportDetails[j].add("lacking", lacking);
						}
						else//否则，再检查散件
						{
							lacking = 0;
							
							float different = quantity-store_count;
							
							DBRow productsInSet[] = floorProductMgr.getProductsInSetBySetPid(pid);
							for (int jj=0; jj<productsInSet.length; jj++)
							{
								//检查库存
								DBRow detailPro2 = floorProductMgr.getDamageProductByStorageAndProductId(ps_id,productsInSet[jj].get("pid", 0l));
								
								if (detailPro2 == null)	//	商品还没进库，当然缺货
								{
									lacking = 1;
									manualUnionLacking = true;
								}
								else
								{
	
									if (detailPro2.get("damaged_count_tatal", 0f)< productsInSet[jj].get("quantity", 0f)*(different)+this.getCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l)))
									{
										lacking = 1;
										manualUnionLacking = true;
									}
								}
								
								this.addCumProductQuantity(productCount, productsInSet[jj].get("pc_id", 0l), productsInSet[jj].get("quantity", 0f)*different);
							}
	
							
							if (standardUnionLacking&&manualUnionLacking)
							{
								orderIsLacking = true;
							}
							transportDetails[j].add("lacking", lacking);
						}
						
						
					}
					
					if(transportDetails[j].get("lacking",0)==1)//缺货加入缺货列表
					{
						result.add(transportDetails[j]);
					}
					
				}
			
				return result.toArray(new DBRow[0]);
		} 
		catch(NoExistTransportDetailException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"checkStorage",log);
		}
	}
	
	/**
	 * 检查库存，返回缺货信息
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
//	public DBRow[] checkStorage(long repair_order_id)
//	throws Exception
//	{
//		try 
//		{
//			DBRow[] transportDetails = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id);
//			
//			if(!(transportDetails.length>0))
//			{
//				throw new NoExistTransportDetailException();
//			}
//			
//			DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
//			long ps_id = transport.get("send_psid",0l);
//			
//			ArrayList<DBRow> result = new ArrayList<DBRow>();
//			
//			for (int j=0; j<transportDetails.length; j++)
//			{
//				
//				long pcid = transportDetails[j].get("transport_pc_id",0l);
//				DBRow storage = floorProductMgr.getDetailProductProductStorageByPcid(ps_id, pcid);
//				
//				float damaged_count			= storage.get("damaged_count",0f);
//				float damaged_package_count	= storage.get("damaged_package_count",0f);
//				float detail_product_count	= transportDetails[j].get("repair_total_count",0.0F);
//				if(detail_product_count > (damaged_count+damaged_package_count))
//				{
//					result.add(transportDetails[j]);
//				}
//			}
//				return result.toArray(new DBRow[0]);
//		} 
//		catch(NoExistTransportDetailException e)
//		{
//			throw e;
//		}
//		catch (Exception e) 
//		{
//			throw new SystemException(e,"checkStorage",log);
//		}
//	}
	
	/**
	 * 累计商品需要的数量
	 * 累加一个订单某个商品需要的累计数量
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @return
	 */
	private float getCumProductQuantity(DBRow productCount,long pid)
		throws Exception
	{
		if ( productCount.getString( String.valueOf(pid) ).equals("") )
		{
			return(0);
		}
		else
		{
			return( StringUtil.getFloat(productCount.getString(String.valueOf(pid))) );
		}
	}
	
	/**
	 * 累计订单某个商品需要的数量
	 * @param productCount
	 * @param pid
	 * @param quantity
	 * @throws Exception
	 */
	private void addCumProductQuantity(DBRow productCount,long pid,float quantity)
		throws Exception
	{
		if ( productCount.getString( String.valueOf(pid) ).equals("") )
		{
			productCount.add(String.valueOf(pid), quantity);
		}
		else
		{
			float currentQuantity = StringUtil.getFloat(productCount.getString(String.valueOf(pid)));
			currentQuantity += quantity;
			productCount.add(String.valueOf(pid), currentQuantity);
		}
	}
	
	/**
	 * 返修单装箱
	 * @param request
	 * @throws Exception
	 */
	public void packingRepair(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long repair_order_id = StringUtil.getLong(request,"repair_order_id");
			DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			DBRow[] transportDetails = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
			
			//复杂的验证是否缺货
			DBRow[] lacking = this.checkStorage(repair_order_id);
			if(lacking.length>0)
			{
				throw new TransportLackException();
			}
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			
			for (int i = 0; i < transportDetails.length; i++) 
			{
				float store_count = floorProductMgr.getDetailProductProductStorageByPcid(transport.get("send_psid",0l), transportDetails[i].get("repair_pc_id",0l)).get("store_count",0f);
				float difference = store_count - transportDetails[i].get("repair_total_count",0f);//返修量与实际库存的差额，大于0说明套装足够，小于0需要单独扣除散件库存，散件商品绝对差额>=0
				float sumPrice = 0;
				float sumPrice1 = 0;
				float sumPrice2 = 0;
				if(difference<0)
				{
					if(store_count!=0)//套装没库存
					{
						sumPrice1 = getStockOutPrice(transport.get("send_psid",0l),transportDetails[i].get("repair_pc_id",0l),0);
						DBRow transport_detail_storage= new DBRow();//实际扣库存的DBRow
						transport_detail_storage.add("pc_id",transportDetails[i].get("repair_pc_id",0l));
						transport_detail_storage.add("repair_total_count",store_count);
						
						list.add(transport_detail_storage);//实际扣的套装
						
						//返修扣掉的套装数
						DBRow para = new DBRow();
						para.add("repair_union_count",store_count);
						floorRepairOrderMgrZyj.modRepairDetail(transportDetails[i].get("repair_detail_id",0l), para);
					}
					//套装不够的部分扣散件
					DBRow[] union_products = floorProductMgr.getProductUnionsBySetPid(transportDetails[i].get("repair_pc_id",0l));
					for (int j = 0; j < union_products.length; j++) 
					{
						sumPrice2 = getStockOutPrice(transport.get("send_psid",0l),transportDetails[i].get("repair_pc_id",0l),1);
						DBRow transport_detail_storage_union = new DBRow();
						transport_detail_storage_union.add("pc_id",union_products[j].get("pid",0l));
						transport_detail_storage_union.add("repair_total_count",(union_products[j].get("quantity",0f)*difference*-1));//这里差额绝对为负数
						
						list.add(transport_detail_storage_union);//套装不够扣除的散件
					}
					//返修的套装拼装数
					DBRow para = new DBRow();
					sumPrice = (sumPrice1+sumPrice2)/transportDetails[i].get("repair_total_count",0f);
					para.add("repair_normal_count",difference*-1);
					para.add("repair_send_price", sumPrice);//记录出库金额
					floorRepairOrderMgrZyj.modRepairDetail(transportDetails[i].get("repair_detail_id",0l), para);
				}
				else
				{
					DBRow transport_detail = transportDetails[i];
					transport_detail.add("pc_id",transportDetails[i].get("repair_pc_id",0l));
					list.add(transport_detail);
					
					sumPrice = getStockOutPrice(transport.get("send_psid",0l),transportDetails[i].get("repair_pc_id",0l),0);
					DBRow product = floorProductMgr.getDetailProductByPcid(transportDetails[i].get("repair_pc_id",0l));
					
					DBRow para = new DBRow();
					if(product.get("union_flag",0)==0)//返修散件商品
					{
						para.add("repair_normal_count",transport_detail.get("repair_total_count",0f));
					}
					else//套装商品
					{
						para.add("repair_union_count",transport_detail.get("repair_total_count",0f));
					}
					para.add("repair_send_price", sumPrice);//记录出库金额
					//记录返修的类型数量
					floorRepairOrderMgrZyj.modRepairDetail(transport_detail.get("repair_detail_id",0l), para);
				}
			}
			
			//扣除库存
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			ProductStoreLogBean deInProductStoreLogBean = new ProductStoreLogBean();
			deInProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
			deInProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
			deInProductStoreLogBean.setOid(repair_order_id);
			deInProductStoreLogBean.setBill_type(ProductStoreBillKey.TRANSPORT_ORDER);
			deInProductStoreLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_TRANSPORT);
			
			for (int q = 0; q < list.size(); q++) 
			{
				floorProductMgr.deIncProductStoreCountByPcid(deInProductStoreLogBean,transport.get("send_psid",0l),list.get(q).get("pc_id",0l),list.get(q).get("repair_total_count",0f));
			}

			if(transport.get("repair_status",0)==RepairOrderKey.READY)//准备状态的才可转换成已起运
			{
				DBRow para = new DBRow();
				para.add("repair_status",RepairOrderKey.PACKING);
				para.add("packing_account",adminLoggerBean.getAdid());//决定装箱人（谁操作的确定装箱）
				floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, para);
			}
			
			this.insertLogs(repair_order_id, "装箱:装箱完成", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods,"",RepairOrderKey.PACKING);
		}
		catch(TransportLackException e)
		{
			throw e;
		}
		catch(NoExistTransportDetailException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"packingRepair",log);
		}
	}
	
	/**
	 * 返修单装箱
	 * @param repair_id
	 * @throws Exception
	 */
	public void packingDamagedRepair(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			
			long repair_id = StringUtil.getLong(request,"repair_order_id");
			
			DBRow damagedRepair = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_id);
				//floorDamagedRepairMgrZJ.getDetailDamagedRepairById(repair_id);
			
			long ps_id = damagedRepair.get("send_psid",0l);
			
			DBRow[] damagedRepair_details = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_id);
				//floorDamagedRepairMgrZJ.getDamagedRepairDetailsByRepairId(repair_id, null, null, null, null);
			//记录残损件出库日志
			AdminLoginBean adminLoggerBean = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
			
//			if(damagedRepair_details == null||damagedRepair_details.length==0)
//			{
//				throw new NoExistDamagedRepairOrderDetailsException();
//			}
			for (int i = 0; i < damagedRepair_details.length; i++) 
			{
				
				float repair_count = damagedRepair_details[i].get("repair_total_count",0f);
				long pcid = damagedRepair_details[i].get("repair_pc_id",0l);
				DBRow storage = floorProductMgr.getDetailProductProductStorageByPcid(ps_id, pcid);
				
				long pid = storage.get("pid",0l);
				float damaged_count = storage.get("damaged_count",0f);
				float damaged_package_count = storage.get("damaged_package_count",0f);
				
				damaged_count = damaged_count-repair_count;//先用残损值减去返修量
				
				DBRow para = new DBRow();
				
				if(damaged_count>0)
				{
					para.add("damaged_count",damaged_count);
				}
				else
				{
					if(damaged_package_count+damaged_count >= 0)
					{
						para.add("damaged_count",0f);
						para.add("damaged_package_count",(damaged_package_count+damaged_count));
					}
					else
					{
						throw new RepairNumberHandleErrorException("库中残损商品数不够");
					}
				}
				//修改残损库存
				floorProductMgr.modProductStorage(pid,para);
				
				ProductStoreLogBean deProductDamagedCountLogBean = new ProductStoreLogBean();
				deProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
				deProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
				deProductDamagedCountLogBean.setOid(repair_id);
				deProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
				deProductDamagedCountLogBean.setPs_id(ps_id);
				deProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_DAMAGED_SPLIT_PRODUCT);
				deProductDamagedCountLogBean.setQuantity(-repair_count);
				deProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				deProductDamagedCountLogBean.setPc_id(pcid);
				productMgr.addProductStoreLogs(deProductDamagedCountLogBean);
			}
			
			DBRow repair_para = new DBRow();
			repair_para.add("repair_status",RepairOrderKey.PACKING);
			repair_para.add("packing_account",adminLoggerBean.getAdid());
			floorRepairOrderMgrZyj.modRepairOrder(repair_id, repair_para);
			this.insertLogs(repair_id, "装箱:装箱完成", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods,"",RepairOrderKey.PACKING);
			String is_need_notify_executer = StringUtil.getString(request, "is_need_notify_executer");
			scheduleMgrZr.addScheduleReplayExternal(repair_id , ModuleKey.REPAIR_ORDER ,
					4 , "返修单:"+repair_id+"装箱:装箱完成" , true, adminLoggerBean, "repair_intransit_period", is_need_notify_executer);
		} 
		catch (RepairNumberHandleErrorException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"trackDeliveryAndRepairOrderCountFirstMenu",log);
		}
	}
	
	
	private float getStockOutPrice(long cid, long pc_id,int union_flag) 
	throws Exception
	{
		float sumPrice = 0;
		if(union_flag==1) {
			DBRow[] set_product = floorProductMgr.getProductUnionsBySetPid(pc_id);
			for(int i=0;i<set_product.length;i++) {
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				DBRow product_storage = floorProductMgr.getDetailProductStorageByRscIdPcid(cid, product.get("pc_id", 0l));
				sumPrice += product_storage.get("storage_unit_price", 0f) + product_storage.get("storage_unit_freight", 0f);
			}
		}else {
			DBRow product_storage = floorProductMgr.getDetailProductStorageByRscIdPcid(cid, pc_id);
			sumPrice = product_storage.get("storage_unit_price", 0f) + product_storage.get("storage_unit_freight", 0f); 
		}
		return sumPrice;
	}
	
	/**
	 * 返修交货跟进一级跟进
	 * @return
	 * @throws Exception
	 */
	public DBRow trackDeliveryAndRepairOrderCountFirstMenu()
		throws Exception
	{	
		try 
		{
			int product_file_day = systemConfig.getIntConfigValue("repair_product_file_period");			//返修实物图片周期
			int quality_inspection_day = systemConfig.getIntConfigValue("repair_quality_inspection");	//返修质检周期
			int tag_day = systemConfig.getIntConfigValue("repair_tag_period");							//返修制签周期
			
			
			int need_track_ready_transport_day = systemConfig.getIntConfigValue("repair_ready_period");//返修单备货跟进周期（天）
			int need_track_packing_transport_day = systemConfig.getIntConfigValue("repair_packing_period");//返修单装箱跟进周期（天）
			
			int need_track_intransit_day = systemConfig.getIntConfigValue("repair_intransit_period");//交货返修单运输中跟进周期（天）
			int need_track_alreadyRecive_transport_hour = systemConfig.getIntConfigValue("repair_alreadyreceive_period");//交货返修单已收货跟进周期（时）
			
			int certificate_day = systemConfig.getIntConfigValue("repair_certificate_period");	//返修单的单证流程周期
			int clearance_day = systemConfig.getIntConfigValue("repair_clearance_period");		//返修单的进口清关流程周期
			int declaration_day = systemConfig.getIntConfigValue("repair_declaration_period");	//返修单的出口报送流程周期


			int need_track_ready_transprot_count = 0;
			int need_track_packing_transport_count = 0;
			int need_track_tag_transport_count = 0;
			int need_track_product_file_transport_count = 0;
			int need_track_quality_inspection_transport_count = 0;
			
			int need_track_intransit_count = 0;
			int need_track_alreadyRecive_transport_count = 0;
			
			DBRow[] track_delivery = this.trackDeliveryCountGroupByProductLine();
			
			int track_delivery_count = 0;
			for (int i = 0; i < track_delivery.length; i++)
			{
				track_delivery_count += track_delivery[i].get("track_count",0);
			}
			
			int track_send_count = 0;
			long ps_id = 0;
			DBRow[] storageCatalogs = floorCatalogMgr.getAllProductStoreCatalog();
			
			for (int i = 0; i < storageCatalogs.length; i++)
			{
				ps_id = storageCatalogs[i].get("id",0l);
				
				need_track_ready_transprot_count += floorRepairOrderMgrZyj.getNeedTrackReadyRepairCountByPsid(ps_id, need_track_ready_transport_day);
				need_track_packing_transport_count += floorRepairOrderMgrZyj.getNeedTrackPackingRepairCountByPsid(ps_id,need_track_packing_transport_day);
				need_track_tag_transport_count += floorRepairOrderMgrZyj.getNeedTrackTagCountByPS(ps_id,tag_day);
				need_track_product_file_transport_count += floorRepairOrderMgrZyj.getNeedTrackProductFileCountByPS(ps_id,product_file_day);
				need_track_quality_inspection_transport_count += floorRepairOrderMgrZyj.getNeedTrackQualityInspectionCountByPS(ps_id, quality_inspection_day);
				
				need_track_intransit_count += floorRepairOrderMgrZyj.getNeedTrackIntransitRepairCountByPsid(ps_id, need_track_intransit_day);
				need_track_alreadyRecive_transport_count += floorRepairOrderMgrZyj.getNeedTrackAlreadyReciveRepairCount(ps_id, need_track_alreadyRecive_transport_hour);
				
			}
			track_send_count = need_track_ready_transprot_count+need_track_packing_transport_count+need_track_tag_transport_count+need_track_product_file_transport_count+need_track_quality_inspection_transport_count;
			int track_recive_count = need_track_intransit_count+need_track_alreadyRecive_transport_count;
			
			//海运
			int track_certificate = floorRepairOrderMgrZyj.getNeedTrackCertificateCount(certificate_day);
			int track_clearance_count = floorRepairOrderMgrZyj.getNeedTrackClearanceCount(clearance_day);
			int track_declaration_count = floorRepairOrderMgrZyj.getNeedTrackDeclarationCount(declaration_day);
			
			int ocean_shipping_count = track_certificate+track_clearance_count+track_declaration_count;
			
			DBRow result = new DBRow();
			result.add("track_delivery_count",track_delivery_count);
			result.add("track_send_count",track_send_count);
			result.add("track_recive_count",track_recive_count);
			result.add("ocean_shipping_count",ocean_shipping_count);
			
			return result;
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"trackDeliveryAndRepairOrderCountFirstMenu",log);
		}
	}
	
	/**
	 * 备货中的交货单按产品线分组
	 * @return
	 * @throws Exception
	 */
	public DBRow[] trackDeliveryCountGroupByProductLine()
		throws Exception
	{
		try
		{
			int need_track_ready_delivery_day = systemConfig.getIntConfigValue("repair_ready_period");	//交货单备货跟进周期（天）
			
			int product_file_day = systemConfig.getIntConfigValue("repair_product_file_period");			//交货实物图片周期
			int quality_inspection_day = systemConfig.getIntConfigValue("repair_quality_inspection");	//交货质检周期
			int tag_day = systemConfig.getIntConfigValue("repair_tag_period");							//交货制签周期
			
			DBRow[] ready_delivery = floorRepairOrderMgrZyj.getNeedTrackReadDeliveryCountGroupByProductLine(need_track_ready_delivery_day);
			DBRow[] track_product_file = floorRepairOrderMgrZyj.getNeedTrackProductFileCountGroupByProductLine(product_file_day);
			DBRow[] track_quality_inspection = floorRepairOrderMgrZyj.getNeedTrackQualityInspectionCountGroupByProductLine(quality_inspection_day);
			DBRow[]	track_tag = floorRepairOrderMgrZyj.getNeedTrackTagCountGroupByProductLine(tag_day);
			
			Map<String,DBRow> map = new HashMap<String,DBRow>();
			//备货需跟进的交货单
			for (int i = 0; i < ready_delivery.length; i++) 
			{
				String product_line_name = ready_delivery[i].getString("product_line_name");
				long product_line_id = ready_delivery[i].get("product_line_id",0l);
				int readyneedtrackcountforline = ready_delivery[i].get("readyneedtrackcountforline",0);

				int track_count = 0;
				if(map.containsKey(String.valueOf(product_line_id)))
				{
					track_count = map.get(String.valueOf(product_line_id)).get("track_count",0);
				}
				track_count += readyneedtrackcountforline;
				
				DBRow result = new DBRow();
				result.add("product_line_name",product_line_name);
				result.add("product_line_id",product_line_id);
				result.add("track_count",track_count);
				
				map.put(String.valueOf(product_line_id),result);
			}
			//实物图片需跟进的交货单
			for (int i = 0; i < track_product_file.length; i++) 
			{
				String product_line_name = track_product_file[i].getString("product_line_name");
				long product_line_id = track_product_file[i].get("product_line_id",0l);
				int  need_track_product_file_count = track_product_file[i].get("need_track_product_file_count",0);

				int track_count = 0;
				if(map.containsKey(String.valueOf(product_line_id)))
				{
					track_count = map.get(String.valueOf(product_line_id)).get("track_count",0);
				}
				track_count += need_track_product_file_count;
				
				DBRow result = new DBRow();
				result.add("product_line_name",product_line_name);
				result.add("product_line_id",product_line_id);
				result.add("track_count",track_count);
				
				map.put(String.valueOf(product_line_id),result);
			}
			//质检需跟进的交货单
			for (int i = 0; i < track_quality_inspection.length; i++) 
			{
				String product_line_name = track_quality_inspection[i].getString("product_line_name");
				long product_line_id = track_quality_inspection[i].get("product_line_id",0l);
				int  need_track_quality_inspection_count = track_quality_inspection[i].get("need_track_quality_inspection_count",0);

				int track_count = 0;
				if(map.containsKey(String.valueOf(product_line_id)))
				{
					track_count = map.get(String.valueOf(product_line_id)).get("track_count",0);
				}
				track_count += need_track_quality_inspection_count;
				
				DBRow result = new DBRow();
				result.add("product_line_name",product_line_name);
				result.add("product_line_id",product_line_id);
				result.add("track_count",track_count);
				
				map.put(String.valueOf(product_line_id),result);
			}
			//制签需跟进的交货单
			for (int i = 0; i < track_tag.length; i++) 
			{
				String product_line_name = track_tag[i].getString("product_line_name");
				long product_line_id = track_tag[i].get("product_line_id",0l);
				int  need_track_tag_count = track_tag[i].get("need_track_tag_count",0);

				int track_count = 0;
				if(map.containsKey(String.valueOf(product_line_id)))
				{
					track_count = map.get(String.valueOf(product_line_id)).get("track_count",0);
				}
				track_count += need_track_tag_count;
				
				DBRow result = new DBRow();
				result.add("product_line_name",product_line_name);
				result.add("product_line_id",product_line_id);
				result.add("track_count",track_count);
				
				map.put(String.valueOf(product_line_id),result);
			}
			
			Set<String> keys = map.keySet();   
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for(String key:keys)
			{
				list.add(map.get(key));
			}
			
			return list.toArray(new DBRow[0]); 
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"trackDeliverySecondMenu",log);
		}
	}
	
	/**
	 * 需跟进发货仓库分组
	 */
	public DBRow[] trackSendRepairCountGroupByPs()
		throws Exception
	{
		
		int need_track_ready_transport_day = systemConfig.getIntConfigValue("repair_ready_period");//返修单备货跟进周期（天）
		int need_track_packing_transport_day = systemConfig.getIntConfigValue("repair_packing_period");//返修单装箱跟进周期（天）
		
		int product_file_day = systemConfig.getIntConfigValue("repair_product_file_period");			//返修实物图片周期
		int quality_inspection_day = systemConfig.getIntConfigValue("repair_quality_inspection");	//返修质检周期
		int tag_day = systemConfig.getIntConfigValue("repair_tag_period");							//返修制签周期
		
		long ps_id = 0;
		DBRow[] storageCatalogs = floorCatalogMgr.getAllProductStoreCatalog();
		
		int need_track_ready_transprot_count;
		int need_track_packing_transport_count;
		int need_track_product_file_transport_count;
		int need_track_quality_inspection_transport_count;
		int need_track_tag_transport_count;
		
		int need_track_send_count;
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < storageCatalogs.length; i++)
		{
			ps_id = storageCatalogs[i].get("id",0l);
			
			need_track_ready_transprot_count = floorRepairOrderMgrZyj.getNeedTrackReadyRepairCountByPsid(ps_id, need_track_ready_transport_day);
			need_track_packing_transport_count = floorRepairOrderMgrZyj.getNeedTrackPackingRepairCountByPsid(ps_id,need_track_packing_transport_day);
			need_track_tag_transport_count = floorRepairOrderMgrZyj.getNeedTrackTagCountByPS(ps_id,tag_day);
			need_track_product_file_transport_count = floorRepairOrderMgrZyj.getNeedTrackProductFileCountByPS(ps_id, product_file_day);
			need_track_quality_inspection_transport_count = floorRepairOrderMgrZyj.getNeedTrackQualityInspectionCountByPS(ps_id, quality_inspection_day);
			
			need_track_send_count = need_track_ready_transprot_count+need_track_packing_transport_count+need_track_product_file_transport_count+need_track_quality_inspection_transport_count+need_track_tag_transport_count;		
			if(need_track_send_count>0)
			{
				DBRow storeCount = new DBRow();
				storeCount.add("title",storageCatalogs[i].getString("title"));
				storeCount.add("ps_id",ps_id);
				storeCount.add("need_track_send_count",need_track_send_count);
				
				list.add(storeCount);
			}
		}
		
		return list.toArray(new DBRow[0]);
	}
	/**
	 * 需跟进收货仓库分组
	 */
	public DBRow[] trackReciveRepairCountGroupByPs()
		throws Exception
	{
		int need_track_intransit_day = systemConfig.getIntConfigValue("repair_intransit_period");//交货返修单运输中跟进周期（天）
		int need_track_alreadyRecive_transport_hour = systemConfig.getIntConfigValue("repair_alreadyreceive_period");//交货返修单已收货跟进周期（时）
		
		int need_track_intransit_count;
		int need_track_alreadyRecive_transport_count;
		int need_track_recive_count;
		long ps_id = 0;
		DBRow[] storageCatalogs = floorCatalogMgr.getAllProductStoreCatalog();
		
		ArrayList<DBRow> list = new ArrayList<DBRow>();
		for (int i = 0; i < storageCatalogs.length; i++)
		{
			ps_id = storageCatalogs[i].get("id",0l);
			
			need_track_intransit_count = floorRepairOrderMgrZyj.getNeedTrackIntransitRepairCountByPsid(ps_id, need_track_intransit_day);
			need_track_alreadyRecive_transport_count = floorRepairOrderMgrZyj.getNeedTrackAlreadyReciveRepairCount(ps_id, need_track_alreadyRecive_transport_hour);
			
			need_track_recive_count = need_track_intransit_count+need_track_alreadyRecive_transport_count;
			
			if(need_track_recive_count>0)
			{
				DBRow storeCount = new DBRow();
				storeCount.add("title",storageCatalogs[i].getString("title"));
				storeCount.add("ps_id",ps_id);
				storeCount.add("need_track_recive_count",need_track_recive_count);
				
				list.add(storeCount);
			}
		}
		
		return list.toArray(new DBRow[0]);
	}
	
	/**
	 * 海运需跟进
	 * @return
	 * @throws Exception
	 */
	public DBRow[] trackOceanShippingCount()
		throws Exception
	{
		int certificate_day = systemConfig.getIntConfigValue("repair_certificate_period");	//返修单的单证流程周期
		int clearance_day = systemConfig.getIntConfigValue("repair_clearance_period");		//返修单的进口清关流程周期
		int declaration_day = systemConfig.getIntConfigValue("repair_declaration_period");	//返修单的出口报送流程周期
		
		int track_certificate_count = floorRepairOrderMgrZyj.getNeedTrackCertificateCount(certificate_day);
		int track_clearance_count = floorRepairOrderMgrZyj.getNeedTrackClearanceCount(clearance_day);
		int track_declaration_count = floorRepairOrderMgrZyj.getNeedTrackDeclarationCount(declaration_day);
		
		DBRow[] result = new DBRow[3];
		
		result[0] = new DBRow();
		result[0].add("track_title","单证");
		result[0].add("track_count",track_certificate_count);
		result[0].add("cmd","track_certificate");
		
		result[1] = new DBRow();
		result[1].add("track_title","清关");
		result[1].add("track_count",track_clearance_count);
		result[1].add("cmd","track_clearance");
		
		result[2] = new DBRow();
		result[2].add("track_title","报关");
		result[2].add("track_count",track_declaration_count);
		result[2].add("cmd","track_declaration");
		
		return result;
	}
	
	/**
	 * 根据仓库ID获得发货需跟进数
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow trackSendRepairPsid(long ps_id)
		throws Exception
	{
		int need_track_ready_transport_day = systemConfig.getIntConfigValue("repair_ready_period");//返修单备货跟进周期（天）
		int need_track_packing_transport_day = systemConfig.getIntConfigValue("repair_packing_period");//返修单装箱跟进周期（天）
		int product_file_day = systemConfig.getIntConfigValue("repair_product_file_period");			//返修实物图片周期
		int quality_inspection_day = systemConfig.getIntConfigValue("repair_quality_inspection");	//返修质检周期
		int tag_day = systemConfig.getIntConfigValue("repair_tag_period");							//返修制签周期
		
		
		int need_track_ready_transprot_count = floorRepairOrderMgrZyj.getNeedTrackReadyRepairCountByPsid(ps_id, need_track_ready_transport_day);
		int need_track_packing_transport_count = floorRepairOrderMgrZyj.getNeedTrackPackingRepairCountByPsid(ps_id,need_track_packing_transport_day);
		int need_track_tag_transport_count = floorRepairOrderMgrZyj.getNeedTrackTagCountByPS(ps_id,tag_day);
		int need_track_product_file_transport_count = floorRepairOrderMgrZyj.getNeedTrackProductFileCountByPS(ps_id, product_file_day);
		int need_track_quality_inspection_transport_count = floorRepairOrderMgrZyj.getNeedTrackQualityInspectionCountByPS(ps_id, quality_inspection_day);
		
		DBRow result = new DBRow();
		result.add("need_track_ready_transprot_count",need_track_ready_transprot_count);
		result.add("need_track_packing_transport_count",need_track_packing_transport_count);
		result.add("need_track_tag_transport_count",need_track_tag_transport_count);
		result.add("need_track_product_file_transport_count",need_track_product_file_transport_count);
		result.add("need_track_quality_inspection_transport_count",need_track_quality_inspection_transport_count);
		result.add("ps_id",ps_id);
		
		return result;
	}
	
	public DBRow trackReciveRepairPsid(long ps_id)
	throws Exception
	{
		int need_track_intransit_day = systemConfig.getIntConfigValue("repair_intransit_period");//交货返修单运输中跟进周期（天）
		int need_track_alreadyRecive_transport_hour = systemConfig.getIntConfigValue("repair_alreadyreceive_period");//交货返修单已收货跟进周期（时）
		
		int need_track_intransit_count = floorRepairOrderMgrZyj.getNeedTrackIntransitRepairCountByPsid(ps_id, need_track_intransit_day);
		int need_track_alreadyRecive_transport_count = floorRepairOrderMgrZyj.getNeedTrackAlreadyReciveRepairCount(ps_id, need_track_alreadyRecive_transport_hour);
		
		DBRow result = new DBRow();
		result.add("need_track_intransit_count",need_track_intransit_count);
		result.add("need_track_alreadyrecive_transport_count",need_track_alreadyRecive_transport_count);
		result.add("ps_id",ps_id);
		return result;
	}
	
	public DBRow[] getRepairLogsByType(long repair_order_id, int transport_type) throws Exception {
		try {
			return floorRepairOrderMgrZyj.getRepairLogsByType(repair_order_id, transport_type);
		} catch (Exception e) {
			throw new SystemException(e,"getTransportLogsByType",log);
		}
	}
	@Override
	public DBRow[] getProductFileByFileTypeAndWithId(long file_with_id,
			int file_with_type, int product_file_type, PageCtrl pc)
			throws Exception {
		 try{
			 return floorRepairOrderMgrZyj.getProductFileByWithIdAndWithTypeFileType(file_with_id, file_with_type, product_file_type, pc);
		 }catch (Exception e) {
			 throw new SystemException(e, "getFileByFilwWidthIdAndFileClass", log);
		}
	}
	
	@Override
	public void updateRepairCertificateAndLogs(HttpServletRequest request)
			throws Exception {
		 try{
		 	long transportId = StringUtil.getLong(request, "repair_order_id");
			DBRow row = new DBRow();
			String transport_date = StringUtil.getString(request, "repair_date");
			TDate tDate = new TDate();
			TDate endDate = new TDate(transport_date);
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			row.add("certificate_over", diffDay); // 单证减去开始时间
			row.add("certificate",RepairCertificateKey.FINISH);
			floorRepairOrderMgrZyj.modRepairOrder(transportId, row);
			
			//记录日志
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
	  
			this.insertLogs(transportId, "[单证上传完成]", adminLoggerBean.getAdid(),  adminLoggerBean.getEmploye_name(), RepairLogTypeKey.Document, null, RepairCertificateKey.FINISH);
			scheduleMgrZr.addScheduleReplayExternal(transportId, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.Document, "返修单:"+transportId+"[单证上传完成]", true, request, "repair_certificate_period");

		 }catch (Exception e) {
			 throw new SystemException(e, "updateRepairCertificateAndLogs", log);
		}
		
	}
	
	/**
	 * 上传上来的文件命名:T_certificate_(文件名称)_index
	 * 多个文件上传
	 * 文件移动到另外的位置
	 * 
	 */
	@Override
	public void addRepairCertificateAddFile(HttpServletRequest request) throws Exception {
		try{
	 
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long file_with_id = StringUtil.getLong(request,"file_with_id",0l);
			int file_with_type = StringUtil.getInt(request,"file_with_type",0);
			int file_with_class =  StringUtil.getInt(request,"file_with_class",0);
			String sn = StringUtil.getString(request,"sn");
			String path = StringUtil.getString(request,"path");
 			String file_names = StringUtil.getString(request, "file_names");
 			String[] fileNameArray = null ;
 			String baseUrl = Environment.getHome()+"upl_imags_tmp/";
 			StringBuffer logContext = new StringBuffer();
 			if(file_names.trim().length() > 0 ){
 				fileNameArray = file_names.trim().split(",");
 			}
 			if(fileNameArray != null && fileNameArray.length > 0){
 				for(String tempFileName : fileNameArray){
 					//真是的文件名称
 					StringBuffer realyFileName = new StringBuffer(sn);
 					String suffix = this.getSuffix(tempFileName);
 					realyFileName.append("_").append(file_with_id).append("_").append(tempFileName.replace("."+suffix, ""));
 					int indexFile = floorRepairOrderMgrZyj.getIndexOfFilebyFileName(realyFileName.toString());
 					if(indexFile != 0){
 						realyFileName.append("_").append(indexFile);
 					}
 					
 					realyFileName.append(".").append(suffix);
 					logContext.append(",").append(realyFileName.toString());
 					// 插入file 表中
 					this.addTransportFileLogs(realyFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
 					// 移动文件
 					FileUtil.moveFile(baseUrl+tempFileName,  Environment.getHome()+"upload/"+path+"/"+realyFileName.toString());
 				}
 		 
 			}
 			//添加日志记录一条
 			String context = "" ;
 			if(logContext.length() > 1 ){
 				context = logContext.substring(1);
 			}
			this.insertLogs(file_with_id, getRepairCertificateFromConfigJsp(file_with_class)+"上传文件"+context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Document, null, file_with_class);
 
		}catch (Exception e) {
			throw new SystemException(e, "addRepairCertificateAddFile", log);
		}
	}
	public String getRepairCertificateFromConfigJsp(int fileWithClass) throws Exception {
		try{
			SystemConfigIFace  systemConfig = (SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig");
			String value = systemConfig.getStringConfigValue("repair_certificate");
			String[] arraySelected = value.split("\n");
			//将arraySelected组成一个List
			ArrayList<String> selectedList= new ArrayList<String>();
			for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
					String tempStr = arraySelected[index];
					if(tempStr.indexOf("=") != -1){
						String[] tempArray = tempStr.split("=");
						String tempHtml = tempArray[1];
						selectedList.add(tempHtml);
					}
			}
			return selectedList.get(fileWithClass-1);
		}catch (Exception e) {
			throw new SystemException(e, "getRepairCertificateFromConfigJsp", log);
		}
	}
	
	public DBRow getSumRepairFreightCost(String repair_order_id) throws Exception {
		try 
		{
			return floorRepairOrderMgrZyj.getSumRepairFreightCost(repair_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "getSumRepairFreightCost", log);
		}
	}
	
	public long updateRepairBasic(HttpServletRequest request) 
	throws Exception
	{
		try 
		{
			long repair_order_id = StringUtil.getLong(request, "repair_order_id");
			DBRow oldTransport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			
			int isOutter = StringUtil.getInt(request,"isOutter");
			long send_psid = StringUtil.getLong(request,"send_psid",oldTransport.get("send_psid",0l));
			long receive_psid = StringUtil.getLong(request,"receive_psid");
			
			DBRow sendProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
			DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(receive_psid);
	
			long deliver_ccid = StringUtil.getLong(request,"deliver_ccid");
			long deliver_pro_id = StringUtil.getLong(request,"deliver_pro_id");			
			String deliver_city = StringUtil.getString(request,"deliver_city");
			String deliver_house_number = StringUtil.getString(request,"deliver_house_number");
			String deliver_street = StringUtil.getString(request,"deliver_street");
			String deliver_zip_code = StringUtil.getString(request,"deliver_zip_code");
			String deliver_name = StringUtil.getString(request,"deliver_name");
			String deliver_linkman_phone = StringUtil.getString(request,"deliver_linkman_phone");
			long send_ccid = StringUtil.getLong(request,"send_ccid");
			long send_pro_id = StringUtil.getLong(request,"send_pro_id");
			String send_city = StringUtil.getString(request,"send_city");
			String send_house_number = StringUtil.getString(request,"send_house_number");
			String send_street = StringUtil.getString(request,"send_street");
			String send_zip_code = StringUtil.getString(request,"send_zip_code");
			String send_name = StringUtil.getString(request,"send_name");
			String send_linkman_phone = StringUtil.getString(request,"send_linkman_phone");
			String transport_receive_date = StringUtil.getString(request, "repair_receive_date");
			String remark = StringUtil.getString(request, "remark");
			
			DBRow transport = new DBRow();
			transport.add("receive_psid",receive_psid);
			transport.add("deliver_ccid",deliver_ccid);
			transport.add("deliver_pro_id",deliver_pro_id);
			transport.add("deliver_city",deliver_city);
			transport.add("deliver_house_number",deliver_house_number);
			transport.add("deliver_street",deliver_street);
			transport.add("deliver_zip_code",deliver_zip_code);
			
			if(oldTransport.get("repair_status",0)==RepairOrderKey.READY)
			{
				
				transport.add("send_psid",send_psid);
				transport.add("send_city",send_city);
				transport.add("send_house_number",send_house_number);
				transport.add("send_street",send_street);
				transport.add("send_ccid",send_ccid);
				transport.add("send_pro_id",send_pro_id);
				transport.add("send_zip_code",send_zip_code);
				transport.add("send_name",send_name);
				transport.add("send_linkman_phone",send_linkman_phone);
				transport.add("from_ps_type",sendProductStorage.get("storage_type",0));
			}
			
			if(isOutter!=2)
			{
				transport.add("create_account_id",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());//返修单的创建者
				transport.add("create_account",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name());//返修单的创建者
			}
			
			transport.add("repair_address","");
			transport.add("repair_linkman",deliver_name);
			transport.add("repair_linkman_phone",deliver_linkman_phone);
			if(!"".equals(transport_receive_date))
			{
				transport.add("repair_receive_date", transport_receive_date);
			}
			transport.add("remark", remark);
			transport.add("repair_date",DateUtil.NowStr());
			transport.add("updatedate",DateUtil.NowStr());
			transport.add("updateby",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());//返修单的创建者
			transport.add("updatename",(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name());//返修单的创建者
			transport.add("target_ps_type",receiveProductStorage.get("storage_type",0));
			
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, transport);
			this.insertLogs(repair_order_id, "修改返修单:单号"+repair_order_id, (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid(), (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name(), RepairLogTypeKey.Update,"",oldTransport.get("repair_status",0));//跟进记录
			//修改返修单索引
			this.editRepairOrderIndex(repair_order_id, "update");
			
			return repair_order_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 更新返修单各个流程信息
	 */
	public void updateRepairProdures(HttpServletRequest request) throws Exception
	{
		try 
		{
			SystemConfigIFace systemConfig	=((SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig"));
			AdminMgr adminMgr 				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long logingUserId				 = adminLoggerBean.getAdid();
			String loginUserName			= adminLoggerBean.getEmploye_name();
			
			Long repair_order_id				= StringUtil.getLong(request, "repair_order_id");
			DBRow transportRow				= floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			int trans_transport_status		= null != transportRow ? transportRow.get("repair_status", 0):0;
			int trans_declaration			= null != transportRow ? transportRow.get("declaration", 0):0;
			int trans_clearance				= null != transportRow ? transportRow.get("clearance", 0):0;
			int trans_product_file			= null != transportRow ? transportRow.get("product_file", 0):0;
			int trans_tag					= null != transportRow ? transportRow.get("tag", 0):0;
			int trans_stock_in_set			= null != transportRow ? transportRow.get("stock_in_set", 0):0;
			int trans_certificate			= null != transportRow ? transportRow.get("certificate", 0):0;
			int trans_quality_inspection	= null != transportRow ? transportRow.get("quality_inspection", 0):0;
			String trans_transport_date		= null != transportRow ? transportRow.getString("repair_date"):"";
			
			//主流程
			String adminUserIdsTransport	= StringUtil.getString(request, "adminUserIdsTransport");
			String adminUserNamesTransport	= StringUtil.getString(request, "adminUserNamesTransport");
			int needMailTransport				= StringUtil.getInt(request, "needMailTransport");
			int needMessageTransport			= StringUtil.getInt(request, "needMessageTransport");
			int needPageTransport				= StringUtil.getInt(request, "needPageTransport");
			
			//出口报关	
			int declaration					= StringUtil.getInt(request, "declaration");
			String adminUserIdsDeclaration	= StringUtil.getString(request, "adminUserIdsDeclaration");
			String adminUserNamesDeclaration= StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration			= StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration		= StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration			= StringUtil.getInt(request, "needPageDeclaration");
			
			//进口清关
			int clearance					= StringUtil.getInt(request, "clearance");
			String adminUserIdsClearance	= StringUtil.getString(request, "adminUserIdsClearance");
			String adminUserNamesClearance	= StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance			= StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance		= StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance			= StringUtil.getInt(request, "needPageClearance");
			
			//商品图片
			int product_file				= StringUtil.getInt(request, "product_file");
			String adminUserIdsProductFile	= StringUtil.getString(request, "adminUserIdsProductFile");
			String adminUserNamesProductFile= StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile			= StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile		= StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile			= StringUtil.getInt(request, "needPageProductFile");
			
			//制签	
			int tag							= StringUtil.getInt(request, "tag");
			String adminUserIdsTag			= StringUtil.getString(request, "adminUserIdsTag");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag					= StringUtil.getInt(request, "needMailTag");
			int needMessageTag				= StringUtil.getInt(request, "needMessageTag");
			int needPageTag					= StringUtil.getInt(request, "needPageTag");
			
			//运费
			int stock_in_set				= StringUtil.getInt(request, "stock_in_set");
			String adminUserIdsStockInSet	= StringUtil.getString(request, "adminUserIdsStockInSet");
			String adminUserNamesStockInSet	= StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet			= StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet		= StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet			= StringUtil.getInt(request, "needPageStockInSet");
			
			//单证
			int certificate					= StringUtil.getInt(request, "certificate");
			String adminUserIdsCertificate	= StringUtil.getString(request, "adminUserIdsCertificate");
			String adminUserNamesCertificate= StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate			= StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate		= StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate			= StringUtil.getInt(request, "needPageCertificate");
			
			//质检
			int quality_inspection 			= StringUtil.getInt(request, "quality_inspection");
			String adminUserIdsQualityInspection	= StringUtil.getString(request, "adminUserIdsQualityInspection");
			String adminUserNamesQualityInspection	= StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection	= StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection= StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection	= StringUtil.getInt(request, "needPageQualityInspection");
			
			String transport_date=DateUtil.NowStr();
			TDate tDate = new TDate();
			tDate.addDay(+1);
			DBRow transRow = new DBRow();
			//如果数据库中是需要，不需要，或者为0，才更新主单据
			if(RepairDeclarationKey.NODELARATION == trans_declaration || RepairDeclarationKey.DELARATION == trans_declaration || 0 == trans_declaration){
				transRow.add("declaration", declaration);
			}
			if(RepairClearanceKey.NOCLEARANCE == trans_clearance || RepairClearanceKey.CLEARANCE == trans_clearance || 0 == trans_clearance){
				transRow.add("clearance", clearance);
			}
			if(RepairProductFileKey.NOPRODUCTFILE == trans_product_file || RepairProductFileKey.PRODUCTFILE == trans_product_file || 0 == trans_product_file){
				transRow.add("product_file", product_file);
			}
			if(RepairTagKey.NOTAG == trans_tag || RepairTagKey.TAG == trans_tag || 0 == trans_tag){
				transRow.add("tag", tag);
			}
			if(RepairStockInSetKey.SHIPPINGFEE_NOTSET == trans_stock_in_set || RepairStockInSetKey.SHIPPINGFEE_SET == trans_stock_in_set || 0 == trans_stock_in_set){
				transRow.add("stock_in_set", stock_in_set);
			}
			if(RepairCertificateKey.NOCERTIFICATE == trans_certificate || RepairCertificateKey.CERTIFICATE == trans_certificate || 0 == trans_certificate){
				transRow.add("certificate", certificate);
			}
			if(RepairQualityInspectionKey.NO_NEED_QUALITY == trans_quality_inspection || RepairQualityInspectionKey.NEED_QUALITY == trans_quality_inspection || 0 == trans_quality_inspection){
				transRow.add("quality_inspection", quality_inspection);
			}
			transRow.add("updatedate",transport_date);
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, transRow);
			
			//发送的内容
			String sendContent = this.handleSendContent(repair_order_id, declaration, adminUserNamesDeclaration,
					clearance, adminUserNamesClearance, product_file, adminUserNamesProductFile,
					tag, adminUserNamesTag, stock_in_set, adminUserNamesStockInSet, 
					certificate, adminUserNamesCertificate, quality_inspection, adminUserNamesQualityInspection);
			//主流程
			this.updateRepairTransport(logingUserId, adminUserIdsTransport, "", repair_order_id,
					ModuleKey.REPAIR_ORDER, needPageTransport, needMailTransport, needMessageTransport, request, true,
					4, trans_transport_status, loginUserName, sendContent);
			//报关
			this.updateRepairDelaration(logingUserId, adminUserIdsDeclaration, "", repair_order_id,
					ModuleKey.REPAIR_ORDER, needPageDeclaration, needMailDeclaration, needMessageDeclaration, request, false, 
					7, declaration, trans_declaration, loginUserName, sendContent);
			//清关
			this.updateRepairClearance(logingUserId, adminUserIdsClearance, "", repair_order_id,
					ModuleKey.REPAIR_ORDER, needPageClearance, needMailClearance, needMessageClearance, request, false,
					6, clearance, trans_clearance, loginUserName, sendContent);
			//实物图片
			this.updateRepairProductFile(logingUserId, adminUserIdsProductFile, "", repair_order_id,
					ModuleKey.REPAIR_ORDER, needPageProductFile, needMailProductFile, needMessageProductFile, request, true,
					10, product_file, trans_product_file, loginUserName, sendContent, trans_transport_date);
			//制签
			this.updateRepairTag(logingUserId, adminUserIdsTag, "", repair_order_id,
					ModuleKey.REPAIR_ORDER, needPageTag, needMailTag, needMessageTag, request, true,
					8, tag, trans_tag, loginUserName, sendContent, transport_date);
			//运费
			this.updateRepairStockInSet(logingUserId, adminUserIdsStockInSet, "", repair_order_id,
					ModuleKey.REPAIR_ORDER, needPageStockInSet, needMailStockInSet, needMessageStockInSet, request, false, 
					5, stock_in_set, trans_stock_in_set, loginUserName, sendContent, transport_date);
			//单证
			this.updateRepairCertificate(logingUserId, adminUserIdsCertificate, "", repair_order_id,
					ModuleKey.REPAIR_ORDER, needPageCertificate, needMailCertificate, needMessageCertificate, request, true, 
					9, certificate, trans_certificate, loginUserName, sendContent, transport_date);
			//质检报告
			this.updateRepairQuality(logingUserId, adminUserIdsQualityInspection, "", repair_order_id, 
					ModuleKey.REPAIR_ORDER, needPageQualityInspection, needMailQualityInspection, needMessageQualityInspection, request, true,
					11, quality_inspection, trans_quality_inspection, loginUserName, sendContent, transport_date);
			
			
		} catch (Exception e) {
			throw new SystemException(e,"updateTransportProdures",log);
		}
	}
	
	public String handleSendContent(long repair_order_id, int declaration, String declarationPerson, 
			int clearance, String clearancePerson,int productFile, String productPerson,
			int tag, String tagPerson,int stockInSet, String stockPerson,
			int certificate, String certificatePerson,int quality, String qualityPerson) throws Exception{
		
		try
		{
			String rnStr				= "";
			DBRow transportRow			= floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			long purchase_id			= transportRow.get("purchase_id",0L);
			//获取提货仓库的名字
			long pro_cata_id_send		= transportRow.get("send_psid", 0L);
		
			String cata_name_send		= "";
			DBRow storageCatalogSend	= floorStorageCatalogMgrZyj.getStorageCatalogById(pro_cata_id_send);
			if(null != storageCatalogSend)
			{
				cata_name_send = storageCatalogSend.getString("title");
				rnStr += "提货仓库:"+cata_name_send+",";
			}
			//获取收货仓库的名字
			long pro_cata_id_deliver	= transportRow.get("receive_psid",0L);
			DBRow storageCatalogDeliver	= floorStorageCatalogMgrZyj.getStorageCatalogById(pro_cata_id_deliver);
			String cata_name_deliver 	= "";
			if(null != storageCatalogDeliver){
				cata_name_deliver = storageCatalogDeliver.getString("title");
				rnStr += "收货仓库:"+cata_name_deliver+",";
			}
			//获取eta
			String eta = transportRow.getString("repair_receive_date");
			if(!"".equals(eta)){
				TDate tdateEta = new TDate(eta);
				String tdateEtaStr = tdateEta.formatDate("yyyy-MM-dd");
				rnStr += "预计到货时间:"+tdateEtaStr+",";
			}
			
			//返修货物的总体积，如果 有值，添加，此处待完善
			rnStr += "总体积:" +this.getRepairOrderVolume(repair_order_id);
			return rnStr;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"handleSendContent",log);
		}
	}
	/*
	 * 更新主流程
	 */
	private void updateRepairTransport(long logingUserId, String executePersonIds,
			String joinPersonIds, long repair_order_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int pur_activity, String loginUserName, String sendContent) throws Exception
	{
		try
		{
			RepairOrderKey repairOrderKey = new RepairOrderKey();
			this.updateTransportScheduleOneProdure(DateUtil.NowStr(), "", logingUserId,
					executePersonIds, joinPersonIds, 
					repair_order_id, moduleId, "返修单:"+repair_order_id+"[货物状态]", 
					sendContent,
					page, mail, message, request, isNow, processType,
					pur_activity, loginUserName, "["+repairOrderKey.getRepairOrderStatusById(pur_activity)+"]"
					);
		} catch (Exception e) {
			throw new SystemException(e,"updateRepairTransport",log);
		}
	}
	
	/*
	 * 更新报关流程
	 */
	private void updateRepairDelaration(long logingUserId, String executePersonIds,
			String joinPersonIds, long repair_order_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent) throws Exception
	{
		try {
			RepairDeclarationKey repairDeclarationKey = new RepairDeclarationKey();
			//报关，3.报关中，4.完成
			//原数据未填写是否需要报送且现在需要报送或原来不需要，现在需要，添加
			if((0 == pur_activity && RepairDeclarationKey.DELARATION == activity) || (RepairDeclarationKey.NODELARATION == pur_activity && RepairDeclarationKey.DELARATION == activity)){
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[报关流程]", 
						sendContent,
						page, mail, message, request, false, RepairLogTypeKey.ExportCustoms,
						RepairDeclarationKey.DELARATION, loginUserName, "[需要报关]");
				
			//如果原数据需要，现在不需要，删除
			}else if(RepairDeclarationKey.DELARATION == pur_activity && RepairDeclarationKey.NODELARATION == activity){
				//删除
				this.deleteTransportScheduleById(repair_order_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.ExportCustoms
						, RepairDeclarationKey.NODELARATION, loginUserName, "[不需要报关]" , logingUserId, request);
				
			//原来需要，现在需要，更新；更新的前提是看，是否添加过任务
			}else if((RepairDeclarationKey.DELARATION == pur_activity && RepairDeclarationKey.DELARATION == activity)){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[报关流程]", 
						sendContent,
						page, mail, message, request, false, RepairLogTypeKey.ExportCustoms,
						RepairDeclarationKey.DELARATION, loginUserName, "[需要报关]"
						);
			//报送处于中，更新任务
			}else if(RepairDeclarationKey.DELARATING == pur_activity){
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[报关流程]", 
						sendContent,
						page, mail, message, request, false, RepairLogTypeKey.ExportCustoms,
						RepairDeclarationKey.DELARATING, loginUserName, "["+repairDeclarationKey.getStatusById(pur_activity)+"]"
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updateRepairDelaration",log);
		}
	}
	/*
	 * 更新清关流程
	 */
	private void updateRepairClearance(long logingUserId, String executePersonIds,
			String joinPersonIds, long repair_order_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent) throws Exception
	{
		try 
		{
			//清关	3.清关中，4.清关完成，5.查货中
			//原数据未填写是否需要清关且现在需要清关或原来不需要，现在需要，添加
			if((0 == pur_activity && RepairClearanceKey.CLEARANCE == activity) || (RepairClearanceKey.NOCLEARANCE == pur_activity && RepairClearanceKey.CLEARANCE == activity)){
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[清关流程]",
						sendContent,
						page, mail, message, request, false, RepairLogTypeKey.ImportCustoms,
						RepairClearanceKey.CLEARANCE, loginUserName, "[需要清关]");
			//原来需要，现在不需要，删除
			}else if(RepairClearanceKey.CLEARANCE == pur_activity && RepairClearanceKey.NOCLEARANCE == activity){
				//删除
				this.deleteTransportScheduleById(repair_order_id, ModuleKey.REPAIR_ORDER, 6
						, RepairClearanceKey.NOCLEARANCE , loginUserName, "[不需要清关]", logingUserId, request);
				
			//原来需要，现在也需要
			}else if((RepairClearanceKey.CLEARANCE == pur_activity && RepairClearanceKey.CLEARANCE == activity)){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[清关流程]",
						sendContent,
						page, mail, message, request, false, RepairLogTypeKey.ImportCustoms
						,RepairClearanceKey.CLEARANCE, loginUserName, "[需要清关]"
						);
				
			//原数据正在清关中			原数据处理查货中	
			}else if(RepairClearanceKey.CLEARANCEING == pur_activity){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[清关流程]",
						sendContent,
						page, mail, message, request, false, RepairLogTypeKey.ImportCustoms
						,RepairClearanceKey.CLEARANCE, loginUserName, "["+new RepairClearanceKey().getStatusById(pur_activity)+"]"
						);
			}	
			else if(RepairClearanceKey.CHECKCARGO == pur_activity)
			{
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[清关流程]",
						sendContent,
						page, mail, message, request, false, RepairLogTypeKey.ImportCustoms
						,RepairClearanceKey.CHECKCARGO, loginUserName, "["+new RepairClearanceKey().getStatusById(pur_activity)+"]"
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updateRepairClearance",log);
		}
	}
	/*
	 * 更新实物图片流程
	 */
	private void updateRepairProductFile(long logingUserId, String executePersonIds,
			String joinPersonIds, long repair_order_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
	{
		try 
		{
			TDate tdateProductFile = new TDate(transport_date);
			tdateProductFile.addDay(Integer.parseInt(systemConfig.getStringConfigValue("repair_product_file_period")));
			String endTimeProductFile = tdateProductFile.formatDate("yyyy-MM-dd HH:mm:ss");
			//商品图片流程	1.无需商品文件，2.商品文件上传中，3.商品文件上传完成
			if((0 == pur_activity && RepairProductFileKey.PRODUCTFILE == activity) || (RepairProductFileKey.NOPRODUCTFILE == pur_activity && RepairProductFileKey.PRODUCTFILE == activity)){
				//添加
				this.addTransportScheduleOneProdure(transport_date, endTimeProductFile, logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[商品图片流程]",
						sendContent,
						page, mail, message, request, true, RepairLogTypeKey.ProductShot,
						RepairProductFileKey.PRODUCTFILE, loginUserName, "[商品文件上传中]");
			}else if(RepairProductFileKey.PRODUCTFILE == pur_activity && RepairProductFileKey.NOPRODUCTFILE == activity){
				//删除
				this.deleteTransportScheduleById(repair_order_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.ProductShot
						,RepairProductFileKey.NOPRODUCTFILE, loginUserName, "[无需商品文件]", logingUserId, request);
			}else if((RepairProductFileKey.PRODUCTFILE == pur_activity && RepairProductFileKey.PRODUCTFILE == activity)){
				//更新
				this.updateTransportScheduleOneProdure(transport_date, endTimeProductFile, logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[商品图片流程]",
						sendContent,
						page, mail, message, request, true, RepairLogTypeKey.ProductShot
						,RepairProductFileKey.PRODUCTFILE, loginUserName, "[商品文件上传中]"
						);
			}	
		} catch (Exception e) {
			throw new SystemException(e,"updateRepairProductFile",log);
		}
	}
	
	/*
	 * 更新制签流程
	 */
	private void updateRepairTag(long logingUserId, String executePersonIds,
			String joinPersonIds, long repair_order_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
		{
			try 
			{
				TDate tdateTag = new TDate(transport_date);
				tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("repair_tag_period")));
				String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
				//制签流程	1.无需制签，2.需要制签，3.制签完成
				if((0 == pur_activity && RepairTagKey.TAG == activity) || (RepairTagKey.NOTAG == pur_activity && RepairTagKey.TAG == activity)){
					//添加
					this.addTransportScheduleOneProdure(transport_date, endTimeTag, logingUserId,
							executePersonIds, joinPersonIds, 
							repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[制签流程]",
							sendContent,
							page, mail, message, request, true, RepairLogTypeKey.Label,
							RepairTagKey.TAG, loginUserName, "[制签中]");
				}else if(RepairTagKey.TAG == pur_activity && RepairTagKey.NOTAG == activity){
					//删除
					this.deleteTransportScheduleById(repair_order_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.Label
							, RepairTagKey.NOTAG, loginUserName, "[无需制签]", logingUserId, request);
				}else if((RepairTagKey.TAG == pur_activity && RepairTagKey.TAG == activity)){
					//更新
					this.updateTransportScheduleOneProdure(transport_date, endTimeTag, logingUserId,
							executePersonIds, joinPersonIds, 
							repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[制签流程]",
							sendContent,
							page, mail, message, request, true, RepairLogTypeKey.Label
							,RepairTagKey.TAG, loginUserName, "[制签中]"
							);
				}
			} 
			catch (Exception e) {
				throw new SystemException(e,"updateRepairClearance",log);
			}
		}
	/*
	 * 更新运费流程
	 */
	private void updateRepairStockInSet(long logingUserId, String executePersonIds,
			String joinPersonIds, long repair_order_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
	{
		try
		{
			//运费流程，阶段：3.运费已经设置，4.运费已申请，5.运费转账完
			if((0 == pur_activity && RepairStockInSetKey.SHIPPINGFEE_SET == activity) || (RepairStockInSetKey.SHIPPINGFEE_NOTSET == pur_activity && RepairStockInSetKey.SHIPPINGFEE_SET == activity)){
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[运费流程]",
						sendContent,
						page, mail, message, request, false, RepairLogTypeKey.Freight,
						RepairStockInSetKey.SHIPPINGFEE_SET, loginUserName, "[需要运费]");
			}else if((RepairStockInSetKey.SHIPPINGFEE_SET == pur_activity) && RepairStockInSetKey.SHIPPINGFEE_NOTSET == activity){
				//删除
				this.deleteTransportScheduleById(repair_order_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.Freight
						, RepairStockInSetKey.SHIPPINGFEE_NOTSET, loginUserName, "[无需运费]", logingUserId, request);
				
			//原数据为需要，现在为需要，更新
			}else if((RepairStockInSetKey.SHIPPINGFEE_SET == pur_activity && RepairStockInSetKey.SHIPPINGFEE_SET == activity)){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[运费流程]",
						sendContent,
						page, mail, message, request, false, RepairLogTypeKey.Freight
						,RepairStockInSetKey.SHIPPINGFEE_SET, loginUserName, "[需要运费]"
						);
				
			}
		} catch (Exception e) {
			throw new SystemException(e,"updateRepairStockInSet",log);
		}
	}
	/*
	 * 更新单证流程
	 */
	private void updateRepairCertificate(long logingUserId, String executePersonIds,
			String joinPersonIds, long repair_order_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
	{
		try 
		{
			TDate tdateCertificate = new TDate(transport_date);
			tdateCertificate.addDay(Integer.parseInt(systemConfig.getStringConfigValue("repair_certificate_period")));
			String endTimeCertificate = tdateCertificate.formatDate("yyyy-MM-dd HH:mm:ss");
			//单证流程	1.无需单证，2.单证采集中，3，单证采集完成
			if((0 == pur_activity && RepairCertificateKey.CERTIFICATE == activity) || (RepairCertificateKey.NOCERTIFICATE == pur_activity && RepairCertificateKey.CERTIFICATE == activity)){
				//添加
				this.addTransportScheduleOneProdure(transport_date, endTimeCertificate, logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[单证流程]",
						sendContent,
						page, mail, message, request, true, RepairLogTypeKey.Document,
						RepairCertificateKey.CERTIFICATE, loginUserName, "[单证采集中]");
			}else if(RepairCertificateKey.CERTIFICATE == pur_activity && RepairCertificateKey.NOCERTIFICATE == activity){
				//删除
				this.deleteTransportScheduleById(repair_order_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.Document
						, RepairCertificateKey.NOCERTIFICATE, loginUserName, "[无需单证]", logingUserId, request);
			}else if((RepairCertificateKey.CERTIFICATE == pur_activity && RepairCertificateKey.CERTIFICATE == activity)){
				//更新
				this.updateTransportScheduleOneProdure(transport_date, endTimeCertificate, logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[单证流程]",
						sendContent,
						page, mail, message, request, true, RepairLogTypeKey.Document
						, RepairCertificateKey.CERTIFICATE, loginUserName, "[单证采集中]"
						);
			}	
		} catch (Exception e) {
			throw new SystemException(e,"updateRepairCertificate",log);
		}
	}
	/*
	 * 更新质检报告流程
	 */
	private void updateRepairQuality(long logingUserId, String executePersonIds,
			String joinPersonIds, long repair_order_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
	{

		try 
		{
			TDate tdateQuality = new TDate(transport_date);
			tdateQuality.addDay(Integer.parseInt(systemConfig.getStringConfigValue("repair_quality_inspection")));
			String endTimeQuality = tdateQuality.formatDate("yyyy-MM-dd HH:mm:ss");
			//质检流程	3.质检中，4.质检完成
			if((0 == pur_activity && RepairQualityInspectionKey.NEED_QUALITY == activity) || (RepairQualityInspectionKey.NO_NEED_QUALITY == pur_activity && RepairQualityInspectionKey.NEED_QUALITY == activity)){
				//添加
				this.addTransportScheduleOneProdure(transport_date, endTimeQuality, logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[质检流程]",
						sendContent,
						page, mail, message, request, true, RepairLogTypeKey.QualityControl,
						RepairQualityInspectionKey.NEED_QUALITY, loginUserName, "[需要质检]");
			}else if(RepairQualityInspectionKey.NEED_QUALITY == pur_activity && RepairQualityInspectionKey.NO_NEED_QUALITY == activity){
				//删除
				this.deleteTransportScheduleById(repair_order_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.QualityControl
						, RepairQualityInspectionKey.NO_NEED_QUALITY, loginUserName, "[无需质检]", logingUserId, request);
				
			//原数据需要且现需要		原数据处于质检中
			}else if((RepairQualityInspectionKey.NEED_QUALITY == pur_activity && RepairQualityInspectionKey.NEED_QUALITY == activity)){
				//更新
				this.updateTransportScheduleOneProdure(transport_date, endTimeQuality, logingUserId,
						executePersonIds, joinPersonIds, 
						repair_order_id, ModuleKey.REPAIR_ORDER, "返修单:"+repair_order_id+"[质检流程]",
						sendContent,
						page, mail, message, request, true, RepairLogTypeKey.QualityControl
						,RepairQualityInspectionKey.NEED_QUALITY, loginUserName, "[需要质检]"
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updateRepairQuality",log);
		}
	}
	
	/**
	 * 根据返修单Id,业务类型，流程类型，删除任务
	 */
	private void deleteTransportScheduleById(long repair_order_id, int moduleId, int processType, int activity, String employeeName, String logContent, long adid, HttpServletRequest request) throws Exception{
		try 
		{
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(repair_order_id, moduleId, processType);
			if(null != schedule){
				long scheduleId	= schedule.get("schedule_id", 0L);
				scheduleMgrZr.deleteScheduleByScheduleId(scheduleId, request);
				this.insertLogs(repair_order_id, logContent, adid, employeeName, processType, "", activity);
			}
			
		}
		catch (Exception e)
		{
			throw new SystemException(e,"deleteTransportScheduleById",log);
		}
	}
	
	/**
	 * 更新任务
	 */
	private void updateTransportScheduleOneProdure(String beginTime, String endTime, long adid, 
			String executePersonIds, String joinPersonIds,
			long repair_order_id, int moduleId, String title, String content,
			int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, String employeeName, String logContent
				) throws Exception{
		
		try
		{
			boolean needPage	= false;
			boolean needMail	= false;
			boolean needMessage	= false;
			if(2 == page){
				needPage = true;
			}
			if(2 == mail){
				needMail = true;
			}
			if(2 == message){
				needMessage = true;
			}
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(repair_order_id, moduleId, processType);
			DBRow[] executePersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(repair_order_id, moduleId, processType);
			if(!"".equals(executePersonIds)){
				String[] adminUserIds	= executePersonIds.split(",");
				if(executePersons.length != adminUserIds.length){
					if(null != schedule){
						scheduleMgrZr.updateScheduleExternal(repair_order_id, moduleId, processType, 
								content, executePersonIds, request, needMail, needMessage, needPage);
						//添加日志
						this.insertLogs(repair_order_id, logContent, adid, employeeName, processType, "", activity);
					}else{
						this.addTransportScheduleOneProdure(beginTime, endTime, adid, executePersonIds,
								joinPersonIds, repair_order_id, moduleId, title, 
								content, page, mail, message, request, isNow, processType,
								activity, employeeName, logContent);
					}
				}else{
					HashSet<String> executePersonSet = new HashSet<String>();
					for (int i = 0; i < executePersons.length; i++) {
						executePersonSet.add(executePersons[i].getString("schedule_execute_id"));
					}
					int executePersonLen = executePersonSet.size();
					for (int i = 0; i < adminUserIds.length; i++) {
						executePersonSet.add(adminUserIds[i]);
					}
					int executePersonArrLen = executePersonSet.size();
					if(executePersonLen != executePersonArrLen){
						if(null != schedule){
							scheduleMgrZr.updateScheduleExternal(repair_order_id, moduleId, processType, 
									content, executePersonIds, request, needMail, needMessage, needPage);
							//添加日志
							this.insertLogs(repair_order_id, logContent, adid, employeeName, processType, "", activity);
						}else{
							this.addTransportScheduleOneProdure(beginTime, endTime, adid, executePersonIds,
									joinPersonIds, repair_order_id, moduleId, title, 
									content, page, mail, message, request, isNow, processType,
									activity, employeeName, logContent);
						}
					}
				}
			}
		}catch (Exception e)
		{
			throw new SystemException(e,"addTransportScheduleOneProdure",log);
		}
	}
	
	/**
	 * 下载返修单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String downloadRepairOrder(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long repair_order_id = StringUtil.getLong(request,"repair_order_id");
			DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			
			DBRow[] transportDetails = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
			if(transport == null)
			{
				throw new NoExistTransportOrderException();
			}
			if(transportDetails==null)
			{
				throw new NoExistTransportDetailException();
			}
			
			
			POIFSFileSystem fs = null;//获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/repair/repair_down.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);//根据模板创建工作文件
			HSSFSheet sheet = wb.getSheetAt(0);//获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);
			HSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT); //创建一个居中格式
			style.setLocked(false);//设置不锁定
			style.setWrapText(true); 
			
			for (int i = 0; i < transportDetails.length; i++) 
			{
				DBRow transportDetail = transportDetails[i];
				long pc_id = transportDetail.get("repair_pc_id",0l);
				DBRow product = productMgr.getDetailProductByPcid(pc_id);
				
				row = sheet.createRow((int) i + 1);
				
				row.createCell(0).setCellValue(product.getString("p_name"));
				row.getCell(0).setCellStyle(style);
				
				row.createCell(1).setCellValue(transportDetail.getString("repair_delivery_count"));
				row.getCell(1).setCellStyle(style);
				
				row.createCell(2).setCellValue(transportDetail.getString("repair_backup_count"));
				row.getCell(2).setCellStyle(style);
				
				row.createCell(3).setCellValue(transportDetail.getString("repair_box"));
				row.getCell(3).setCellStyle(style);
			}
			String path = "upl_excel_tmp/R"+repair_order_id+".xls";
			FileOutputStream fout = new FileOutputStream(Environment.getHome()+ path);
			wb.write(fout);
			fout.close();
			return (path);
		} 
		catch(NoExistTransportOrderException e)
		{
			throw e;
		}
		catch(NoExistTransportDetailException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public void updateRepairFreight(HttpServletRequest request) throws Exception {
		try 
		{
			DBRow row =Utils.getDBRowByName("repair_order", request);
			long repair_order_id = row.get("repair_order_id",0l);
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, row);
			
			this.editRepairOrderIndex(repair_order_id,"update");
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogs(row.get("repair_order_id",0L), "返修单设置运单:单号"+row.getString("repair_order_id"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3,"",row.get("repair_status", 0));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateRepairFreight",log);
		}
	}
	
	/**
	 * 上传发票文件
	 * @param request
	 * @throws Exception
	 * @throws FileTypeException
	 * @throws FileException
	 */
	public void uploadRepairInvoice(HttpServletRequest request)
		throws Exception,FileTypeException,FileException
	{
		try 
		{
			String permitFile = "xls,doc,xlsx,docx";
			
			TUpload upload = new TUpload();
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long repair_order_id = Long.parseLong(upload.getRequestRow().getString("repair_order_id"));
			
				
			if (flag==2)
			{
				throw new FileTypeException();//文件上传格式不对
			}
			else if (flag==1)
			{
				throw new FileException();
			}
			else
			{		  
				String[] temp = upload.getFileName().split("\\."); 
				String fileType = temp[temp.length-1];
				
				String fileName = "R"+repair_order_id+"."+fileType;
				String temp_url = Environment.getHome()+"upl_excel_tmp/"+upload.getFileName();
				String invoice_path = "invoice_file/"+fileName;
				String url = Environment.getHome()+invoice_path;
				FileUtil.moveFile(temp_url,url);
				
				DBRow para  = new DBRow();
				para.add("invoice_path",invoice_path);
				
				floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, para);
				
			}
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch(FileException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"uploadRepairInvoice",log);
		}
	}
	
	public DBRow[] getRepairFreightCostByRepairId(String repair_order_id) throws Exception{
		try
		{
			return floorRepairOrderMgrZyj.getRepairFreightCostByRepairId(repair_order_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getRepairFreightCostByRepairId",log);
		}
	}
	
	public DBRow repairSetFreight(HttpServletRequest request) throws Exception {
		String repair_order_id = StringUtil.getString(request,"repair_order_id");
		String[] tfc_ids = request.getParameterValues("tfc_id");
		String[] tfc_project_names = request.getParameterValues("tfc_project_name");
		String[] tfc_ways = request.getParameterValues("tfc_way");
		String[] tfc_units = request.getParameterValues("tfc_unit");
		String[] tfc_unit_prices = request.getParameterValues("tfc_unit_price");
		String[] tfc_unit_counts = request.getParameterValues("tfc_unit_count");
		String[] tfc_currency = request.getParameterValues("tfc_currency");
		String[] tfc_exchange_rate = request.getParameterValues("tfc_exchange_rate");
//		int stock_in_set = StrUtil.getInt(request,"stock_in_set");
		int changed = StringUtil.getInt(request,"changed",0);
		
		double modFreightTotal = 0.0;
		//修改运费时，如果修改最终的值比申请的值小，不能修改
		for(int i=0;tfc_ids!=null && i<tfc_ids.length;i++) {
			if(!"".equals(tfc_unit_prices[i]) && !"".equals(tfc_exchange_rate[i]) && !"".equals(tfc_unit_counts[i]))
			{
				modFreightTotal += Double.parseDouble(tfc_unit_prices[i])*Double.parseDouble(tfc_exchange_rate[i])*Double.parseDouble(tfc_unit_counts[i]);
			}
		}
		double applyFreight = applyFundsMgrZyj.getTransportFreightTotalApply(Long.parseLong(repair_order_id), FinanceApplyTypeKey.REPAIR_ORDER);
		DBRow result = new DBRow();
		SystemConfigIFace systemConfig	=((SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig"));
		if((modFreightTotal >= applyFreight) || (modFreightTotal<applyFreight && (Math.abs(applyFreight-modFreightTotal)<=Double.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))*modFreightTotal*0.01))){
			if(changed == 2) 
			{
				floorRepairOrderMgrZyj.deleteRepairOrderFreightCostByRepairId(String.valueOf(repair_order_id));
			}
			
			for(int i=0;tfc_ids!=null && i<tfc_ids.length;i++) {
				DBRow row = new DBRow();
				row.add("tfc_id", tfc_ids[i]);
				row.add("repair_order_id", repair_order_id);
				row.add("tfc_project_name", tfc_project_names[i]);
				row.add("tfc_way", tfc_ways[i]);
				row.add("tfc_unit", tfc_units[i]);
				row.add("tfc_unit_price", tfc_unit_prices[i]);
				row.add("tfc_unit_count", tfc_unit_counts[i]);
				row.add("tfc_currency", tfc_currency[i]);
				row.add("tfc_exchange_rate", tfc_exchange_rate[i]);
				
				if(changed == 2 || row.getString("tfc_id").equals("") || row.getString("tfc_id").equals("0"))
				{
					row.remove("tfc_id");
					floorRepairOrderMgrZyj.insertRepairOrderFreightCost(row);
				}
				else 
				{
					floorRepairOrderMgrZyj.updateRepairOrderFreightCost(row, Long.parseLong(tfc_ids[i]));
				}		
			}
			result.add("flag", "success");
		}
		else
		{
			result.add("flag", "fail");
			result.add("apply_freight", String.valueOf(applyFreight));
		}
		
		AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
//		batchMgrLL.batchTransportStockInChangeFreight(Long.parseLong(repair_order_id), adminLoggerBean);
		this.insertLogs(Long.parseLong(repair_order_id), "返修单设置运费:单号"+repair_order_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3,"",0);
		return result;
	}
	
	/**
	 * 上传文件保存返修单明细
	 * @param request
	 * @throws Exception
	 */
	public void saveRepairDetail(HttpServletRequest request)
		throws Exception 
	{
		try 
		{
			long repair_order_id = Long.parseLong(StringUtil.getString(request,"repair_order_id"));
			
			DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			
			long purchase_id = transport.get("purchase_id",0l);
			this.addPurchaseRepairDetail(purchase_id, repair_order_id, request);
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogs(repair_order_id, "返修单导入:",adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 3,"", transport.get("repair_state", 0));
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"saveRepairDetail",log);
		}
	}
	
	/**
	 * 返修单出库
	 * @param request
	 * @throws Exception
	 */
	public void repairStockTempOut(HttpServletRequest request) 
	throws Exception
	{
		try 
		{
			String repair_order_id = StringUtil.getString(request,"repair_order_id");
			String[] product_names = request.getParameterValues("product_name");
			String[] stockIn_counts = request.getParameterValues("stockOut_count");
			floorRepairOrderMgrZyj.insertRepairOutbound(repair_order_id, product_names, stockIn_counts);//出库添加临时表
			
			DBRow transport_row = floorRepairOrderMgrZyj.getDetailRepairOrderById(Long.parseLong(repair_order_id));
			int transport_status = transport_row.get("repair_status", 0);
			/**
			 * 普通返修单单，非装箱状态不许出库（扣过库存）
			 * 交货返修单,非准备状态不许出库（不扣库存）
			 */
			if(transport_row.get("purchase_id",0l)==0&&transport_status != RepairOrderKey.PACKING)
			{
				throw new TransportStockOutHandleErrorException("返修单出库状态不正确!");
			}
			else if(transport_row.get("purchase_id",0l)!=0&&transport_status != RepairOrderKey.READY)
			{
				throw new TransportStockOutHandleErrorException("返修单出库状态不正确!");
			}
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			this.deliveryRepair(Long.parseLong(repair_order_id),adminLoggerBean);//返修单已起运
			
			this.addRepairOutboundApproveSub(Long.parseLong(repair_order_id), adminLoggerBean);//创建返修出库审核
			
			this.delRepairOutboundByRepairId(Long.parseLong(repair_order_id));//清除返修发货数据
			this.insertLogs(Long.parseLong(repair_order_id), "单据:"+repair_order_id+"起运", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 4, "", transport_row.get("repair_status", 0));
			scheduleMgrZr.addScheduleReplayExternal(Long.parseLong(repair_order_id) , ModuleKey.REPAIR_ORDER ,
					4 , "返修单:"+repair_order_id+"起运" , false, request, "repair_intransit_period");
		}
		catch (RepairNumberHandleErrorException e)
		{
			throw e;
		}
		catch(TransportStockOutHandleErrorException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"transportStockTempOut",log);
		}
	}
	/**
	 * 返修单起运
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void deliveryRepair(long repair_order_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		try 
		{
			long send_psid = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id).get("send_psid",0L);
			DBRow[] transportbounds = floorRepairOrderMgrZyj.getTransportOutboundByTransportId(repair_order_id);
			
			DBRow[] repairDetails = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id);
			//如果出库时删除了商品，删除详细并处理库存（有疑问？詹洁）
			for (int m = 0; m < repairDetails.length; m++) {
				long repair_detail_id		= repairDetails[m].get("repair_detail_id", 0L);
				long d_pc_id				= repairDetails[m].get("repair_pc_id", 0L);
				float d_repair_total_count	= repairDetails[m].get("repair_total_count", 0F);
				DBRow[] repairBound			= floorRepairOrderMgrZyj.getRepairOutboundByRepairAndPcId(repair_order_id, d_pc_id);
				if(0 == repairBound.length)
				{
					floorRepairOrderMgrZyj.deleteRepairOrderDetailByDetailId(repair_detail_id);
					DBRow storageRow = floorProductMgr.getDetailProductProductStorageByPcid(send_psid, d_pc_id);
					DBRow storageRowAdd = new DBRow();
					storageRowAdd.add("damaged_count", storageRow.get("damaged_count", 0F)+d_repair_total_count);
					floorProductMgr.modProductStorage(storageRow.get("pid", 0L),storageRowAdd);
					
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(repair_order_id);
					inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
					inProductDamagedCountLogBean.setPs_id(send_psid);
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_PRODUCT_OUTSTORAGE);
					inProductDamagedCountLogBean.setQuantity(d_repair_total_count);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(d_pc_id);
					productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
				}
			}
			
			//记录返修单实际发货量
			for(int i = 0;i<transportbounds.length;i++)
			{
				long pc_id = transportbounds[i].get("to_pc_id",0l);
				float repair_send_count = transportbounds[i].get("to_count",0f);
				DBRow repairDetail = floorRepairOrderMgrZyj.getRepairDetailDeliveryByRepairAndPc(repair_order_id, pc_id);
				//库存信息
				DBRow storage = floorProductMgr.getDetailProductProductStorageByPcid(send_psid, pc_id);
				long pid = storage.get("pid",0l);
				float damaged_count = storage.get("damaged_count",0f);
				float damaged_package_count = storage.get("damaged_package_count",0f);
				
				if(null == repairDetail)//没有记录就插入新记录 
				{
					DBRow transportDetail = new DBRow();
					transportDetail.add("repair_p_name",transportbounds[i].getString("to_p_name"));
					transportDetail.add("repair_total_count",0);
					transportDetail.add("repair_send_count",repair_send_count);
					transportDetail.add("repair_order_id",repair_order_id);
					transportDetail.add("repair_box","");
					transportDetail.add("repair_pc_id",transportbounds[i].getString("to_pc_id"));
					transportDetail.add("repair_p_code",transportbounds[i].getString("to_p_code"));
					floorRepairOrderMgrZyj.addRepairDetail(transportDetail);
					//扣库存,够
					DBRow StroageUpd = new DBRow();
					if((damaged_count+damaged_package_count) >= repair_send_count)
					{
						if(damaged_count >= repair_send_count)
						{
							StroageUpd.add("damaged_count", damaged_count-repair_send_count);
						}
						else
						{
							StroageUpd.add("damaged_count", 0);
							StroageUpd.add("damaged_package_count", damaged_package_count-(repair_send_count-damaged_count));
						}
						
						ProductStoreLogBean outProductDamagedCountLogBean = new ProductStoreLogBean();
						outProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
						outProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
						outProductDamagedCountLogBean.setOid(repair_order_id);
						outProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
						outProductDamagedCountLogBean.setPs_id(send_psid);
						outProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.OUT_STROE_DAMAGED_SPLIT_PRODUCT_OUTSTRAGE);
						outProductDamagedCountLogBean.setQuantity(-repair_send_count);
						outProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
						outProductDamagedCountLogBean.setPc_id(pc_id);
						productMgr.addProductStoreLogs(outProductDamagedCountLogBean);
					}
					else
					{
						throw new RepairNumberHandleErrorException("库中残损商品数不够");
					}
					floorProductMgr.modProductStorage(pid,StroageUpd);
					
				}
				else
				{
					DBRow repairDetailMod = new DBRow();
					float repair_total_count = repairDetail.get("repair_total_count", 0F);
					//如果计划装箱与出库数不一致，修改库存和计划装箱数
					if(repair_send_count != repair_total_count)
					{
						repairDetailMod.add("repair_total_count", repair_send_count);
						//如果计划比出库多，库存加差数
						DBRow storageMod = new DBRow();
						if(repair_total_count > repair_send_count)
						{
							storageMod.add("damaged_count", damaged_count+(repair_total_count-repair_send_count));
							
							ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
							inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
							inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
							inProductDamagedCountLogBean.setOid(repair_order_id);
							inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
							inProductDamagedCountLogBean.setPs_id(send_psid);
							inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_PRODUCT_OUTSTORAGE);
							inProductDamagedCountLogBean.setQuantity(repair_total_count-repair_send_count);
							inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
							inProductDamagedCountLogBean.setPc_id(pc_id);
							productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
							
						}
						else if(repair_total_count < repair_send_count)
						{
							//需要验证库存是否够，如果够
							if((damaged_count+damaged_package_count) >= (repair_send_count - repair_total_count))
							{
								if(damaged_count >= (repair_send_count - repair_total_count))
								{
									storageMod.add("damaged_count", damaged_count-(repair_send_count-repair_total_count));
								}
								else
								{
									storageMod.add("damaged_count", 0);
									storageMod.add("damaged_package_count", damaged_package_count-(repair_send_count-repair_total_count-damaged_count));
								}
								
								ProductStoreLogBean outProductDamagedCountLogBean = new ProductStoreLogBean();
								outProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
								outProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
								outProductDamagedCountLogBean.setOid(repair_order_id);
								outProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
								outProductDamagedCountLogBean.setPs_id(send_psid);
								outProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.OUT_STROE_DAMAGED_SPLIT_PRODUCT_OUTSTRAGE);
								outProductDamagedCountLogBean.setQuantity(-(repair_send_count-repair_total_count));
								outProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
								outProductDamagedCountLogBean.setPc_id(pc_id);
								productMgr.addProductStoreLogs(outProductDamagedCountLogBean);
								
							}
							else
							{
								throw new RepairNumberHandleErrorException("库中残损商品数不够");
							}
						}
						floorProductMgr.modProductStorage(pid,storageMod);
					}
					long repair_detail_id = repairDetail.get("repair_detail_id", 0L);
					repairDetailMod.add("repair_send_count", repair_send_count);
					floorRepairOrderMgrZyj.modRepairDetail(repair_detail_id, repairDetailMod);
				}
				
				
				
			}
			
//			batchMgrLL.batchTransportStockOut(repair_order_id, adminLoggerBean);
			//修改返修单状态
			DBRow para = new DBRow();
			para.add("repair_status",RepairOrderKey.INTRANSIT);
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, para);
			
			this.insertLogs(repair_order_id, "启运:启运完成", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods,"", RepairOrderKey.INTRANSIT);
			((RepairOrderMgrZyjIFace)AopContext.currentProxy()).deliveryRepairForJbpm(repair_order_id, send_psid);
			//移到返修发货审核创建改
		} 
		catch (RepairNumberHandleErrorException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"deliveryRepair",log);
		}
	}
	
	/**
	 * 工作流拦截返修已装箱发货
	 * @param repair_order_id
	 * @throws Exception
	 */
	public String deliveryRepairForJbpm(long repair_order_id,long receive_id) 
		throws Exception 
	{
		return (repair_order_id+","+receive_id);
	}
	
	/**
	 * 返修发货审核提交
	 * @param repair_order_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void addRepairOutboundApproveSub(long repair_order_id,AdminLoginBean adminLoggerBean) 
		throws Exception 
	{
		try 
		{
			DBRow[] differents = floorRepairOrderMgrZyj.compareRepairPacking(repair_order_id);
			
			DBRow para = new DBRow();
			
			if(differents.length>0)//无差异就不创建审核了
			{
				String commit_account = adminLoggerBean.getEmploye_name();
				String commit_date = DateUtil.NowStr();
				int different_count = differents.length;
				
				DBRow dbrow = new DBRow();
				dbrow.add("repair_order_id",repair_order_id);
				dbrow.add("commit_account",commit_account);
				dbrow.add("commit_date",commit_date);
				dbrow.add("different_count",different_count);
				
				long tsa_id = floorRepairOrderMgrZyj.addSendApprove(dbrow);
				
				for (int i = 0; i < differents.length; i++) 
				{
					String product_name = differents[i].getString("p_name");
					String product_code = differents[i].getString("p_code");
					long product_id = differents[i].get("pc_id",0l);
					float repair_total_count = differents[i].get("repair_total_count",0f);
					float transport_send_count = differents[i].get("real_send_count",0f);
					
					DBRow difference = new DBRow();
					difference.add("product_name",product_name);
					difference.add("product_code",product_code);
					difference.add("product_id",product_id);
					difference.add("repair_total_count",repair_total_count);
					difference.add("repair_send_count",transport_send_count);
					difference.add("rsa_id",tsa_id);
					
					floorRepairOrderMgrZyj.addSendApproveDetail(difference);
					
					//提供给工作流拦截
					((RepairOrderMgrZyjIFace)AopContext.currentProxy()).approveRepairOutboundForJbpm(tsa_id);
				}
				
				para.add("repair_status",RepairOrderKey.INTRANSIT);//正常运输
//				para.add("transport_status",RepairOrderKey.ERRORINTRANSIT);//状态差异运输
				
			}
			else
			{
				para.add("repair_status",RepairOrderKey.INTRANSIT);//正常运输
			}
			
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, para);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"addRepairOutboundApproveSub",log);
		}
	}
	/**
	 * 返修发货审核
	 * @param tsa_id
	 * @return
	 * @throws Exception
	 */
	public long approveRepairOutboundForJbpm(long tsa_id) 
		throws Exception 
	{
		return (tsa_id);
	}
	
	/**
	 * 删除返修出货
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void delRepairOutboundByRepairId(long repair_order_id)
		throws Exception 
	{
		try 
		{
			floorRepairOrderMgrZyj.delRepairOutBoundByRepairId(repair_order_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delRepairOutboundByRepairId",log);
		}
	}
	
	public void repairStockTempIn(HttpServletRequest request) throws Exception {
		try {
			String repair_order_id = StringUtil.getString(request,"repair_order_id");
			String[] product_names = request.getParameterValues("product_name");
			String[] stockIn_counts = request.getParameterValues("stockIn_count");
			floorRepairOrderMgrZyj.insertRepairWarehouses(repair_order_id, product_names, stockIn_counts);//添加临时表
			
			DBRow transport_row = floorRepairOrderMgrZyj.getDetailRepairOrderById(Long.parseLong(repair_order_id));
			int transport_status = transport_row.get("repair_status", 0);
			if(!(transport_status == RepairOrderKey.INTRANSIT || transport_status == RepairOrderKey.AlREADYRECEIVE))
			{
				throw new TransportStockInHandleErrorException("返修单入库状态不正确!");
			}
			
			DBRow[] transportDetailsWarehouse = floorRepairOrderMgrZyj.getRepairWarehousesByRepairId(repair_order_id);
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.repairWarehousingSub(Long.parseLong(repair_order_id), transportDetailsWarehouse, adminLoggerBean, "");//入库检查
		}
		catch (TransportStockInHandleErrorException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"transportStockTempIn",log);
		}
	}
	
	/**
	 * 返修入库提交
	 * @param repair_order_id
	 * @param transportDetailsWarehouse
	 * @param adminLoggerBean
	 * @param machine_id
	 * @throws Exception
	 */
	public void repairWarehousingSub(long repair_order_id,DBRow[] transportDetailsWarehouse,AdminLoginBean adminLoggerBean,String machine_id)
		throws Exception
	{		
		try {
			DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			DBRow transportPara = new DBRow();
			
			for (int i = 0; i < transportDetailsWarehouse.length; i++) 
			{
				float count = Float.valueOf(transportDetailsWarehouse[i].getString("tw_count"));//获得交货数量
				if(count > 0f)
				{
					DBRow transportDetail = floorRepairOrderMgrZyj.getRepairDetailByTPId(repair_order_id,transportDetailsWarehouse[i].get("tw_product_id",0l));//获得返修详细
					
					if(transportDetail !=null)
					{
						float reap_count = transportDetail.get("repair_reap_count",0f);
						reap_count = reap_count + count;//到货数量累加
						
						transportDetail.add("repair_reap_count",reap_count);
						floorRepairOrderMgrZyj.modRepairDetail(transportDetail.get("repair_detail_id",0l), transportDetail);//修改到货量
					}
					else
					{
						DBRow noInTransportOrderproduct = floorProductMgr.getDetailProductByPcid(transportDetailsWarehouse[i].get("tw_product_id",0l));
						if(noInTransportOrderproduct == null)
						{
							throw new ProductNotExistException();
						}
						
						transportDetail = new DBRow();
						
						transportDetail.add("repair_pc_id",noInTransportOrderproduct.get("pc_id",0l));
						transportDetail.add("repair_p_name",noInTransportOrderproduct.getString("p_name"));
						transportDetail.add("repair_p_code",noInTransportOrderproduct.getString("p_code"));
						transportDetail.add("repair_total_count",0);//原本预计返修量
						transportDetail.add("repair_send_count",0);//实际返修量
						transportDetail.add("repair_reap_count",count);
						transportDetail.add("repair_order_id",repair_order_id);
						
						floorRepairOrderMgrZyj.addRepairDetail(transportDetail);
					}
					
					//商品入库
				}
				
			}
			//检查返修单是否已完成
			DBRow[] checkApprove = floorRepairOrderMgrZyj.checkRepairDetailApprove(repair_order_id);
			if(checkApprove!=null&&checkApprove.length>0)
			{
				this.addRepairApproveSub(repair_order_id, adminLoggerBean);
				
				transportPara.add("repair_status",RepairOrderKey.APPROVEING);
			}
			else
			{
				TDate tDate = new TDate();
				TDate endDate = new TDate(transport.getString("repair_date")+" 00:00:00");
				double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
				transportPara.add("all_over", diffDay);
				transportPara.add("repair_status",RepairOrderKey.FINISH);
				//返修单确认入库
				this.stockIn(repair_order_id, adminLoggerBean,ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_DAMDGED_NORMAL);
			}
			transportPara.add("warehousinger_id", adminLoggerBean.getAdid());
			transportPara.add("warehousing_date", DateUtil.NowStr());
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, transportPara);//修改返修状态	
			RepairOrderKey repairOrderKey = new RepairOrderKey();
			this.insertLogs(repair_order_id, "返修单入库:"+repairOrderKey.getRepairOrderStatusById(transportPara.get("repair_status",0)), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods,"",transportPara.get("repair_status",0));
			scheduleMgrZr.addScheduleReplayExternal(repair_order_id , ModuleKey.REPAIR_ORDER ,
					4 , "返修单:"+repair_order_id+"入库" , true, adminLoggerBean, "repair_intransit_period","true");
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"repairWarehousingSub",log);
		}
	}
	
	/**
	 * 返修单审核提交
	 * @param repair_order_id
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void addRepairApproveSub(long repair_order_id,AdminLoginBean adminLoggerBean) 
		throws Exception 
	{
		try 
		{
			DBRow[] differences = floorRepairOrderMgrZyj.getDifferentRepairDetailByRepairId(repair_order_id);
			
			if (differences.length>0) 
			{
				String commit_account = adminLoggerBean.getEmploye_name();
				String commit_date = DateUtil.NowStr();
				int different_count = differences.length;
				DBRow dbrow = new DBRow();
				dbrow.add("repair_order_id", repair_order_id);
				dbrow.add("commit_account", commit_account);
				dbrow.add("commit_date", commit_date);
				dbrow.add("different_count", different_count);
				long ta_id = floorRepairOrderMgrZyj.addRepairApprove(dbrow);
				for (int i = 0; i < differences.length; i++)
				{
					String product_name = differences[i].getString("p_name");
					String product_code = differences[i].getString("p_code");
					long product_id = differences[i].get("repair_pc_id", 0l);
					float repair_total_count = differences[i].get("repair_total_count", 0f);
					float transport_reap_count = differences[i].get("repair_reap_count", 0f);

					DBRow difference = new DBRow();
					difference.add("product_name", product_name);
					difference.add("product_code", product_code);
					difference.add("product_id", product_id);
					difference.add("repair_total_count", repair_total_count);
					difference.add("repair_reap_count", transport_reap_count);
					difference.add("ta_id", ta_id);

					floorRepairOrderMgrZyj.addRepairApproveDetail(difference);
				}
				
				//提供工作流审核
				((RepairOrderMgrZyjIFace)AopContext.currentProxy()).approveRepairOrderForJbpm(ta_id);
				
				DBRow para = new DBRow();
				para.add("repair_status", RepairOrderKey.APPROVEING);
				floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, para);//修改返修单为待审核状态
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addRepairApproveSub",log);
		}
	}
	/**
	 * 拦截转运单需要审核
	 */
	public long approveRepairOrderForJbpm(long ta_id) 
		throws Exception 
	{
		return (ta_id);
	}
	public void stockIn(long repair_order_id,AdminLoginBean adminLoggerBean, int product_storep_oeration) throws Exception {
		try 
		{
			DBRow[] transportDetailsWarehouse = this.getRepairWarehouseByRepairId(repair_order_id);
			DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			long receive_psid = transport.get("receive_psid", 0L);
			for (int i = 0; i < transportDetailsWarehouse.length; i++) 
			{
				float count = Float.valueOf(transportDetailsWarehouse[i].getString("tw_count"));//获得交货数量
				long product_id = transportDetailsWarehouse[i].get("tw_product_id", 0L);
				//残损入库
				floorProductMgr.damageProductCountInStore(receive_psid, product_id, count);
				//加日志
				ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
				inProductStoreLogBean.setOid(repair_order_id);
				inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_PRODUCT);
				inProductStoreLogBean.setQuantity(count);
				inProductStoreLogBean.setPc_id(product_id);
				inProductStoreLogBean.setPs_id(receive_psid);
				inProductStoreLogBean.setAccount(adminLoggerBean.getEmploye_name());
				inProductStoreLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
				inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
				inProductStoreLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
				productMgr.addProductStoreLogs(inProductStoreLogBean);
			}
			this.delRepairWareHouseByRepairId(repair_order_id);//清除虚拟交货数据
			String returnStr,returnStr2;
			returnStr = floorRepairOrderMgrZyj.computePriceForRepair(repair_order_id);
		    if(!"".equals(returnStr))
		    {
		    	throw new Exception(returnStr);
		    }
		    returnStr2 = floorRepairOrderMgrZyj.computeFreightForRepair(repair_order_id);
		    if(!"".equals(returnStr2))
		    {
		    	throw new Exception(returnStr2);
		    }
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"stockIn",log);
		}
	}
	/**
	 * 获得返修交货
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairWarehouseByRepairId(long repair_order_id)
		throws Exception 
	{
		try
		{
			return floorRepairOrderMgrZyj.getRepairWarehouse(repair_order_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getRepairWarehouseByRepairId",log);
		}
	}
	
	/**
	 * 删除返修伪入库
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void delRepairWareHouseByRepairId(long repair_order_id)
		throws Exception 
	{
		try 
		{
			floorRepairOrderMgrZyj.delRepairWareHouseByRepairId(repair_order_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"delRepairWareHouseByRepairId",log);
		}
	}
	/**
	 * 更新重量与体积
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void updateRepairDetailVW(long repair_order_id)
	throws Exception
	{
		DBRow[] transportDetails;
		
		if(repair_order_id==0)
		{
			transportDetails = floorRepairOrderMgrZyj.getAllRepairOrderDetail();
		}
		else
		{
			transportDetails = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
		}
		
		DBRow product = null;
		float product_volume = 0;
		float product_weight = 0;
		for (int i = 0; i < transportDetails.length; i++) 
		{
			
			if(transportDetails[i].get("volume",0f)==0f)
			{
				product = floorProductMgr.getDetailProductByPcid(transportDetails[i].get("repair_pc_id",0l));
				if(product!=null)
				{
					product_volume = product.get("volume",0f);
					product_weight = product.get("weight", 0f);
				}
				
				DBRow para = new DBRow();
				para.add("repair_volume",product_volume);
				para.add("repair_weight",product_weight);
				
				floorRepairOrderMgrZyj.modRepairDetail(transportDetails[i].get("repair_detail_id",0l), para);
			}
		}
	}
	
	/**
	 * 修改返修单明细 
	 * @param request
	 * @throws Exception
	 */
	public void modRepairDetail(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long transport_detail_id = StringUtil.getLong(request,"id");//grid默认参数是ID
			
			DBRow transportDetail = floorRepairOrderMgrZyj.getRepairDetailById(transport_detail_id);
			long repair_order_id = transportDetail.get("repair_order_id",0l);
			DBRow product = floorProductMgr.getDetailProductByPcid(transportDetail.get("repair_pc_id",0l));
			
			Map parameter = request.getParameterMap();
			float transport_delivery_count = transportDetail.get("repair_delivery_count",0f);
			float transport_backup_count = transportDetail.get("repair_backup_count",0f);
			
			String transport_box;
			String transport_p_name;
			DBRow para = new DBRow();
			
			if(parameter.containsKey("repair_delivery_count"))
			{
				transport_delivery_count = StringUtil.getFloat(request,"repair_delivery_count");
				para.add("repair_delivery_count",transport_delivery_count);
			}
			
			if(parameter.containsKey("repair_backup_count"))
			{
				transport_backup_count = StringUtil.getFloat(request,"repair_backup_count");
				para.add("repair_backup_count",transport_backup_count);
			}
			
			if(parameter.containsKey("repair_box"))
			{
				transport_box = StringUtil.getString(request,"repair_box");
				para.add("repair_box",transport_box);
			}
			if(parameter.containsKey("p_name"))
			{
				transport_p_name = StringUtil.getString(request,"p_name");
				
				product = floorProductMgr.getDetailProductByPname(transport_p_name);
				if(product == null)
				{
					throw new ProductNotExistException();
				}
				
				long product_id = product.get("pc_id",0l);
				
				
				
				DBRow productTransport = floorRepairOrderMgrZyj.getRepairDetailByTPId(repair_order_id, product_id); 
				if(productTransport !=null)
				{
					throw new RepeatProductException();
				}
				para.add("repair_pc_id",product.get("pc_id",0l));
				para.add("repair_p_code",product.getString("p_code"));
				para.add("repair_p_name",transport_p_name);
				
				para.add("repair_volume",product.get("volume",0f));
				para.add("repair_weight",product.get("weight",0f));
				
				DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
				long purchase_id = transport.get("purchase_id",0l);
				
				double send_price = product.get("unit_price",0d);
				if(purchase_id!=0)
				{
					DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id,product_id);
					if(purchaseDetail!=null)
					{
						send_price = purchaseDetail.get("price",0d);
					}
				}
				para.add("repair_send_price",send_price);
			}
			
			//交货数量、备件数量修改后从重新计算总数量
			if(parameter.containsKey("repair_delivery_count")||parameter.containsKey("repair_backup_count"))
			{
				float repair_total_count = transport_backup_count+transport_delivery_count;
				para.add("repair_total_count",repair_total_count);
			}
			//计划装箱数
			if(parameter.containsKey("repair_total_count"))
			{
				transport_delivery_count = StringUtil.getFloat(request,"repair_total_count");
				para.add("repair_total_count",transport_delivery_count);
			}
			
			
			floorRepairOrderMgrZyj.modRepairDetail(transport_detail_id, para);
		} 
		catch (ProductNotExistException e) 
		{
			throw e;
		}
		catch(RepeatProductException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modTransportDetail",log);
		}
	}
	
	/**
	 * 增加返修单详细（单个）jqgrid的form增加
	 * @param request
	 * @throws Exception
	 */
	public void addRepairDetail(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			String transport_p_name = StringUtil.getString(request,"p_name");
			
			long repair_order_id = StringUtil.getLong(request,"repair_order_id");
			String transport_box = StringUtil.getString(request,"repair_box");
			float transport_delivery_count = StringUtil.getFloat(request,"repair_delivery_count");
			float transport_backup_count = StringUtil.getFloat(request,"repair_backup_count");
			float repair_total_count = transport_delivery_count+transport_backup_count;
			
			DBRow product = floorProductMgr.getDetailProductByPname(transport_p_name);
			if(product==null)
			{
				throw new ProductNotExistException();
			}
			
			long transport_pc_id = product.get("pc_id",0l);
			
			DBRow transportDetailIsExit = floorRepairOrderMgrZyj.getRepairDetailByTPId(repair_order_id, transport_pc_id);
			
			if(transportDetailIsExit!=null)
			{
				throw new TransportOrderDetailRepeatException();
			}
			
			float transport_volume = product.get("volume",0f);
			float transport_weight = product.get("weight",0f);
			String transport_p_code = product.getString("p_code");
			
			DBRow transportDetail = new DBRow();
			transportDetail.add("repair_p_name",transport_p_name);
			transportDetail.add("repair_total_count",repair_total_count);
			transportDetail.add("repair_order_id",repair_order_id);
			transportDetail.add("repair_box",transport_box);
			transportDetail.add("repair_pc_id",transport_pc_id);
			transportDetail.add("repair_p_code",transport_p_code);
			transportDetail.add("repair_volume",transport_volume);
			transportDetail.add("repair_weight",transport_weight);
			
			transportDetail.add("repair_backup_count",transport_backup_count);
			transportDetail.add("repair_delivery_count",transport_delivery_count);
			
			DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			long purchase_id = transport.get("purchase_id",0l);
			double send_price = product.get("unit_price",0d);
			if(purchase_id!=0)
			{
				DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id,transport_pc_id);
				if(purchaseDetail!=null)
				{
					send_price = purchaseDetail.get("price",0d);
				}
			}
			transportDetail.add("repair_send_price",send_price);
			
			floorRepairOrderMgrZyj.addRepairDetail(transportDetail);
		}
		catch(ProductNotExistException e)
		{
			throw e;
		}
		catch (TransportOrderDetailRepeatException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addRepairDetail",log);
		}
	}
	
	/**
	 * 删除返修单明细
	 * @param request
	 * @throws Exception
	 */
	public void delRepairDetail(HttpServletRequest request) 
		throws Exception 
	{
		try 
		{
			long transport_detail_id = StringUtil.getLong(request,"id");//id因为jqgrid因素
			
			floorRepairOrderMgrZyj.delRepairDetail(transport_detail_id);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"delRepairDetail",log);
		}
	}
	
	/**
	 * 多个商品。多个文件
	 */
	public void handleRepairProductPictureUp(HttpServletRequest request)
			throws Exception {
		try{
			//如果是多个商品的 那么注意多个商品都关联上.就是循环的添加
			//有多个文件的时候循环的插入到product_file表.
			//文件的命名为T_product_上传文件的名称.jpg
			//如果是上传文件名有重复的那么就是要加上_index.jpg;
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			String pc_id = StringUtil.getString(request,"pc_id");	 //商品的Id
			String[] arrayPcId = pc_id.split(",");
			long file_with_id = StringUtil.getLong(request, "repair_order_id");	//关联的业务Id
			int file_with_type = StringUtil.getInt(request,"file_with_type");
			int file_with_class = StringUtil.getInt(request,"file_with_class"); // 这个目前写成商品的那种标签 比如 是商品本身，称重ID等等。
			String file_with_className = StringUtil.getString(request,"file_with_class_name"); //是商品本身，称重等等
			String sn = StringUtil.getString(request,"sn");
			String path = StringUtil.getString(request,"path");
		    String fileNames = StringUtil.getString(request, "file_names");
			String[] fileNameArray = null ;
			if(fileNames.trim().length() > 0 ){
				fileNameArray = fileNames.trim().split(",");
			}
			
			if(fileNameArray != null && fileNameArray.length > 0){
				StringBuffer logFileNameContent = new StringBuffer();
				String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
				for(String tempFileName : fileNameArray){
					String tempSuffix = getSuffix(tempFileName);
					String tempfileUpName = tempFileName.replace("."+tempSuffix, "");
					StringBuffer realyFileName = new StringBuffer(sn);
					realyFileName.append("_").append(file_with_id).append("_").append(tempfileUpName);
					
					int indexFile = floorRepairOrderMgrZyj.getIndexOfProductFileByFileName(realyFileName.toString());
					if(indexFile != 0){realyFileName.append("_").append(indexFile);}
					realyFileName.append(".").append(tempSuffix);
					logFileNameContent.append(",").append(realyFileName.toString());
					
					String  tempUrl =  baseTempUrl+tempFileName;
					//移动文件到指定的
					StringBuffer url = new StringBuffer();
					url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realyFileName.toString());
				
					FileUtil.moveFile(tempUrl,url.toString());
					for(int index = 0 , count = arrayPcId.length ; index < count ; index++){
						DBRow file = new DBRow();
						file.add("file_name",realyFileName.toString());
						file.add("file_with_id",file_with_id);
						file.add("file_with_type",file_with_type);
						file.add("product_file_type",file_with_class);
						file.add("upload_adid",adminLoggerBean.getAdid());
						file.add("upload_time",DateUtil.NowStr());
						file.add("pc_id", arrayPcId[index]);
						floorRepairOrderMgrZyj.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
					}
				}
			}
		}catch (Exception e) {
			throw new SystemException(e, "handleRepairProductPictureUp", log);
		}
	}
	public String saveProductTagFile(HttpServletRequest request) throws Exception{
		try {
			// 这里上传的文件是保存在product_file这张表中pc_id
			// 保存的文件名为T_product_121212_商品分类_index(商品分类)
			//1=AMZON标签
			//2=UPC标签
			// 记录日志 日志中
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
		 
	   
				 
				String  pc_id = StringUtil.getString(request, "pc_id"); //商品的Ids
				String[] arrayPcIds = pc_id.split(",");
				long file_with_id = StringUtil.getLong(request,"repair_order_id");	//关联的业务Id
				int file_with_type = StringUtil.getInt(request,"file_with_type");
				int file_with_class = StringUtil.getInt(request,"file_with_class"); // 这个目前写成商品的那种标签
				String file_with_className = StringUtil.getString(request,"file_with_class_name");  
				String sn = StringUtil.getString(request,"sn");
				String path = StringUtil.getString(request,"path");
			 
				//得到上传的临时文件
				String fileNames = StringUtil.getString(request, "file_names");
				String[] fileNameArray = null ;
				if(fileNames.trim().length() > 0 ){
					fileNameArray = fileNames.trim().split(",");
				}
				if(fileNameArray != null && fileNameArray.length > 0){
					StringBuffer logFileNameContent = new StringBuffer();
					String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
					for(String tempFileName : fileNameArray){
						String tempSuffix = getSuffix(tempFileName);
						String tempfileUpName = tempFileName.replace("."+tempSuffix, "");
						StringBuffer realyFileName = new StringBuffer(sn);
						realyFileName.append("_").append(file_with_id).append("_").append(tempfileUpName);
						int indexFile = floorRepairOrderMgrZyj.getIndexOfProductFileByFileName(realyFileName.toString());
						if(indexFile != 0){realyFileName.append("_").append(indexFile);}
						realyFileName.append(".").append(tempSuffix);
						logFileNameContent.append(",").append(realyFileName.toString());
						String  tempUrl =  baseTempUrl+tempFileName;
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realyFileName.toString());
					
						FileUtil.moveFile(tempUrl,url.toString());
						for(int index = 0 , count = arrayPcIds.length ; index < count ; index++){
							DBRow file = new DBRow();
							file.add("file_name",realyFileName.toString());
							file.add("file_with_id",file_with_id);
							file.add("file_with_type",file_with_type);
							file.add("product_file_type",file_with_class);
							file.add("upload_adid",adminLoggerBean.getAdid());
							file.add("upload_time",DateUtil.NowStr());
							file.add("pc_id", arrayPcIds[index]);
							floorRepairOrderMgrZyj.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
						}
					}
			}
				return "" ;
		} catch (Exception e) {
			throw new SystemException(e,"saveProductTagFile",log);
		}
	}
	
	/**
	 * 返修单到货派送
	 */
	public void updateRepairGoodsArriveDelivery(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean	= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid				= adminLoggerBean.getAdid();
			String employeeName		= adminLoggerBean.getEmploye_name();
			long repair_order_id		= StringUtil.getLong(request, "repair_order_id");
			String deliveryed_date	= StringUtil.getString(request, "deliveryed_date");
			long deliveryer_id		= StringUtil.getLong(request, "deliveryer_id");
			String deliveryer_name	= StringUtil.getString(request, "deliveryer_name");
			
			DBRow transportRow		= floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			int transport_status	= transportRow.get("repair_status", 0);
			if(transport_status != RepairOrderKey.INTRANSIT)
			{
				throw new TransportStockInHandleErrorException("返修单入库状态不正确!");
			}
			DBRow row = new DBRow();
			row.add("deliveryed_date", deliveryed_date);
			row.add("deliveryer_id", deliveryer_id);
			row.add("repair_status", RepairOrderKey.AlREADYRECEIVE);
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, row);
			
			String content = employeeName+"确认了单据:R"+repair_order_id+"到货派送,仓库收货人:"+deliveryer_name+"送至时间:"+deliveryed_date;
			//日志
			this.insertLogs(repair_order_id, content, adid, employeeName, 4, "", RepairOrderKey.AlREADYRECEIVE);
			//跟进
			scheduleMgrZr.addScheduleReplayExternal(repair_order_id , ModuleKey.REPAIR_ORDER ,
					4 , content , false, request, "repair_intransit_period");
			
			//给仓库人员创建任务，如果有任务了，就不创建了
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(repair_order_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.STORAGEPERSON);
			if(null == schedule)
			{
				this.addTransportScheduleOneProdure(DateUtil.NowStr(), "", adid,
						String.valueOf(deliveryer_id), "", 
						repair_order_id, ModuleKey.REPAIR_ORDER, "转运单:"+repair_order_id+"[主流程]",
						content,2, 2, 2, request, true, RepairLogTypeKey.STORAGEPERSON,
						transport_status, employeeName,content);
			}
		}
		catch (TransportStockInHandleErrorException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e, "updateRepairGoodsArriveDelivery", log);
		}
	}
	
	/**
	 * 处理返修翻新
	 * @param request
	 * @throws Exception
	 */
	public void updateHandleRepairProduct(HttpServletRequest request)throws Exception
	{
		try 
		{
			long repair_order_id			= StringUtil.getLong(request, "repair_order_id");
			DBRow transportRow				= floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			long receive_psid				= transportRow.get("receive_psid", 0L);
			String[] transport_detail_ids	= request.getParameterValues("repair_detail_id");
			String[] transport_pc_ids		= request.getParameterValues("repair_pc_id");
			String[] repair_counts			= request.getParameterValues("repair_count");
			String[] destroy_counts			= request.getParameterValues("destroy_count");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			double repair_counts_total		= 0;
			double destroy_counts_total		= 0;
			
			//处理翻新
			if(transport_detail_ids.length > 0)
			{
				for (int i = 0; i < repair_counts.length; i++) {
					//将翻新数和销毁数存入返修单详细中
					DBRow transportDetail	= floorRepairOrderMgrZyj.getRepairDetailById(Long.parseLong(transport_detail_ids[i]));
					double repair_count_be	= transportDetail.get("repair_count", 0.0);
					double destory_count_be	= transportDetail.get("destroy_count", 0.0);
					DBRow transportDetailRow= new DBRow();
					double repair_count		= !"".equals(repair_counts[i])?Double.parseDouble(repair_counts[i]):0;
					double destroy_count	= !"".equals(destroy_counts[i])?Double.parseDouble(destroy_counts[i]):0;
					transportDetailRow.add("repair_count", repair_count);
					transportDetailRow.add("destroy_count", destroy_count);
					floorRepairOrderMgrZyj.modRepairDetail(Long.parseLong(transport_detail_ids[i]), transportDetailRow);
					repair_counts_total		+= repair_count;
					destroy_counts_total	+= destroy_count;
					//需要验证库存中的产品是否够
					double damaged_count_tatal = floorProductMgr.getProductDamageByStorageAndProductId(receive_psid, Long.parseLong(transport_pc_ids[i]));
					if(damaged_count_tatal < (Math.abs(repair_count-repair_count_be)+Math.abs(destroy_count-destory_count_be)))
					{
						throw new RepairNumberHandleErrorException("库中残损商品数不够");
					}
					
					
					//处理库存，残损变正常减少
					if(repair_count_be != repair_count)
					{
						floorProductMgr.damageProductCountChangeNormal(receive_psid, Long.parseLong(transport_pc_ids[i]), Math.abs(repair_count_be-repair_count));
						//加日志,残损变正常减少
						ProductStoreLogBean damageDecLogBean = new ProductStoreLogBean();
						damageDecLogBean.setOid(repair_order_id);
						damageDecLogBean.setOperation(ProductStoreOperationKey.IN_STORE_NORMAL_DAMAGED_SPLIT_DEC_PRODUCT);
						damageDecLogBean.setQuantity(-(repair_count-repair_count_be));
						damageDecLogBean.setPc_id(Long.parseLong(transport_pc_ids[i]));
						damageDecLogBean.setPs_id(receive_psid);
						damageDecLogBean.setAccount(adminLoggerBean.getEmploye_name());
						damageDecLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
						damageDecLogBean.setAdid(adminLoggerBean.getAdid());
						damageDecLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
						productMgr.addProductStoreLogs(damageDecLogBean);
						
						//残损变正常入库
						floorProductMgr.productRepairCountInStore(receive_psid, Long.parseLong(transport_pc_ids[i]),Math.abs(repair_count_be-repair_count));
						//加日志，正常增加
						ProductStoreLogBean normalAddLogBean = new ProductStoreLogBean();
						normalAddLogBean.setOid(repair_order_id);
						normalAddLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_NORMAL_SPLIT_ADD_PRODUCT);
						normalAddLogBean.setQuantity(repair_count-repair_count_be);
						normalAddLogBean.setPc_id(Long.parseLong(transport_pc_ids[i]));
						normalAddLogBean.setPs_id(receive_psid);
						normalAddLogBean.setAccount(adminLoggerBean.getEmploye_name());
						normalAddLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
						normalAddLogBean.setAdid(adminLoggerBean.getAdid());
						normalAddLogBean.setQuantity_type(ProductStoreCountTypeKey.STORECOUNT);
						productMgr.addProductStoreLogs(normalAddLogBean);
					}
					
					
					//处理库存，残损被销毁减少
					if(destroy_count != destory_count_be)
					{
						floorProductMgr.damageProductCountChangeNormal(receive_psid, Long.parseLong(transport_pc_ids[i]), Math.abs(destroy_count-destory_count_be));
						//加日志,残损变销毁，减少
						ProductStoreLogBean damageDesLogBean = new ProductStoreLogBean();
						damageDesLogBean.setOid(repair_order_id);
						damageDesLogBean.setOperation(ProductStoreOperationKey.OUT_STORE_DAMAGED_SPLIT_DESTROY);
						damageDesLogBean.setQuantity(-(destroy_count-destory_count_be));
						damageDesLogBean.setPc_id(Long.parseLong(transport_pc_ids[i]));
						damageDesLogBean.setPs_id(receive_psid);
						damageDesLogBean.setAccount(adminLoggerBean.getEmploye_name());
						damageDesLogBean.setBill_type(ProductStoreBillKey.DAMAGED_REPAIR);
						damageDesLogBean.setAdid(adminLoggerBean.getAdid());
						damageDesLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
						productMgr.addProductStoreLogs(damageDesLogBean);
					}
					
					
				}
			}
			//通过返修数更改返修单主单据状态
			double repair_order_details_total = floorRepairOrderMgrZyj.getRepairDetailProductTotalByRepairId(repair_order_id);
			boolean isFinish = false;
			int isFinishStatus = RepairOrderKey.PART_REPAIR;
			DBRow transportUpRow = new DBRow();
			if(repair_counts_total > 0 && (repair_counts_total+destroy_counts_total) < repair_order_details_total)
			{
				transportUpRow.add("repair_status", RepairOrderKey.PART_REPAIR);
			}
			else if((repair_counts_total+destroy_counts_total) == repair_order_details_total)
			{
				transportUpRow.add("repair_status", RepairOrderKey.FINISH_REPAIR);
				isFinish = true;
				isFinishStatus = RepairOrderKey.FINISH_REPAIR;
			}
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, transportUpRow);
			
			scheduleMgrZr.addScheduleReplayExternal(repair_order_id, ModuleKey.REPAIR_ORDER, 4, 
					adminLoggerBean.getEmploye_name()+"确认了返修单"+repair_order_id+new RepairOrderKey().getRepairOrderStatusById(isFinishStatus), isFinish, request, "repair_intransit_period");
		} 
		catch (RepairNumberHandleErrorException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e, "updateHandleRepairProduct", log);
		}
	}
	/**
	 * 根据返修单ID查询，转运单是否存在
	 */
	public boolean isRepairOrderExist(long repairOrderId) throws Exception
	{
		try
		{
			return floorRepairOrderMgrZyj.isRepairOrderExist(repairOrderId);
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "isRepairOrderExist(long repairOrderId)", log);
		}
	}
	
	/**
	 * 过滤返修单审核
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param approve_status
	 * @param sorttype
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterRepairApprove(long send_psid, long receive_psid,PageCtrl pc, int approve_status, String sorttype) throws Exception
	{
		try 
		{
			return floorRepairOrderMgrZyj.fillterTransportApprove(send_psid, receive_psid, pc, approve_status, sorttype);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "fillterTransportApprove(long send_psid, long receive_psid,PageCtrl pc, int approve_status, String sorttype)", log);
		}
	
	}
	
	/**
	 * 根据转运单审核ID获得转运审核明细
	 * @param ta_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairOrderApproverDetailsByTaid(long ta_id, PageCtrl pc) throws Exception 
	{
		try
		{
			return floorRepairOrderMgrZyj.getApproveDetailByTaid(ta_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 审核转运单
	 * @param ta_id
	 * @param tad_ids
	 * @param notes
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void approveRepairOrderDifferent(long ta_id, long[] tad_ids,String[] notes, AdminLoginBean adminLoggerBean) throws Exception 
	{
		for (int i = 0; i < tad_ids.length; i++) 
		{
			DBRow modpara = new DBRow();
			modpara.add("note",notes[i]);
			modpara.add("approve_account",adminLoggerBean.getEmploye_name());
			modpara.add("approve_status",ApproveRepairKey.APPROVE);
			
			floorRepairOrderMgrZyj.modRepairOrderApproveDetail(tad_ids[i], modpara);
		}
		
		DBRow[] noApprove = floorRepairOrderMgrZyj.getApproveRepairDetailWihtStatus(ta_id,ApproveRepairKey.WAITAPPROVE);//查找未审核的不同
		DBRow[] hasApprove = floorRepairOrderMgrZyj.getApproveRepairDetailWihtStatus(ta_id,ApproveRepairKey.APPROVE);//查找已审核的不同
		
		
		DBRow modApproveTransport = new DBRow();
		long repair_order_id = floorRepairOrderMgrZyj.getDetailApproveRepairById(ta_id).get("repair_order_id",0l);
		DBRow repairOrder = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);//获得返修单
		if(noApprove.length == 0)//差异都已审核过，变更交货单审核状态
		{
			modApproveTransport.add("approve_status",ApproveRepairKey.APPROVE);
			modApproveTransport.add("approve_account",adminLoggerBean.getEmploye_name());
			modApproveTransport.add("approve_date",DateUtil.NowStr());
			
			modApproveTransport.add("difference_approve_count",hasApprove.length);
			floorRepairOrderMgrZyj.modRepairApprove(ta_id, modApproveTransport);
			
			//转运单更改状态
			
			
			DBRow updatepara = new DBRow();
			TDate tDate = new TDate();
			TDate endDate = new TDate(repairOrder.getString("repair_date")+" 00:00:00");
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			updatepara.add("all_over", diffDay);//获取结束天数
			updatepara.add("repair_status",RepairOrderKey.FINISH);		
			
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, updatepara);
			stockIn(repair_order_id, adminLoggerBean,ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_PRODUCT);
		}
		else
		{
			modApproveTransport.add("difference_approve_count",hasApprove.length);
			floorRepairOrderMgrZyj.modRepairApprove(ta_id, modApproveTransport);
		}
		DBRow repairOrderChange = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);//获得返修单
		this.insertLogs(repair_order_id, "返修单审核:"+repair_order_id+","+(noApprove.length == 0?"完全审核":"部分审核"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),1,"", repairOrderChange.get("repair_status", 0));
		this.insertLogs(repair_order_id, "返修单审核:"+repair_order_id+","+(noApprove.length == 0?"完全审核":"部分审核"), adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods,"",repairOrderChange.get("repair_status",0));
		scheduleMgrZr.addScheduleReplayExternal(repair_order_id , ModuleKey.REPAIR_ORDER ,
				4 , "返修单:"+repair_order_id+","+(noApprove.length == 0?"完全审核":"部分审核") ,
				repairOrder.get("repair_status",0)==RepairOrderKey.FINISH?true:false, adminLoggerBean, "repair_intransit_period","true");
	}
	
	/**
	 * 取消审核
	 */
	public void repairAproveCancel(String transport_approve_id,AdminLoginBean adminLoggerBean) throws Exception {
		try {
			DBRow approveRow = floorRepairOrderMgrZyj.getRepairOrderApproveById(Long.parseLong(transport_approve_id));
			long repair_order_id = approveRow.get("repair_order_id",0L);
			DBRow repairOrderRow = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			
			floorRepairOrderMgrZyj.deleteRepairWarehouseByRepairId(repair_order_id);//删除库存临时表
			
			floorRepairOrderMgrZyj.deleteRepairApproveDetailByCond(Long.parseLong(transport_approve_id));//删除审核明细
			floorRepairOrderMgrZyj.deleteRepairApproveById(Long.parseLong(transport_approve_id));//删除返修单审核主表
			
			DBRow transportDetailRow = new DBRow();
			transportDetailRow.add("repair_reap_count", 0);
			floorRepairOrderMgrZyj.updateRepairOrderDetailByRepairId(repair_order_id, transportDetailRow);//更新返修单实际入库数量
			//更新返修单状态
			DBRow[] transportDetailRows = floorRepairOrderMgrZyj.getRepairOrderDetailByRepairId(repair_order_id);
			for(int i=0;i<transportDetailRows.length; i++) {
				if(transportDetailRows[i].get("repair_send_count", 0d)==0)//多到商品删除
				{
					floorRepairOrderMgrZyj.delRepairDetailByRepairDetailId(transportDetailRows[i].get("repair_detail_id",0L));
				}
			}
//			status = status!=RepairOrderKey.ERRORINTRANSIT?RepairOrderKey.INTRANSIT:status;
			DBRow repairRow = new DBRow();
			if(RepairOrderKey.APPROVEING == repairOrderRow.get("repair_status", 0))
			{
				repairRow.add("repair_status", RepairOrderKey.INTRANSIT);
			}
			floorRepairOrderMgrZyj.modRepairOrder(repair_order_id, repairRow);
			
			DBRow repairOrderRowChange = floorRepairOrderMgrZyj.getDetailRepairOrderById(repair_order_id);
			
			this.insertLogs(repair_order_id, "取消入库审核:单号"+repair_order_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 1,"",repairOrderRowChange.get("repair_status", 0));
		
			this.insertLogs(repair_order_id, "取消入库审核:单号"+repair_order_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),RepairLogTypeKey.Goods,"",repairOrderRowChange.get("repair_status",0));
			scheduleMgrZr.addScheduleReplayExternal(repair_order_id , ModuleKey.REPAIR_ORDER ,
					4 , "返修单:"+repair_order_id+"取消入库审核" ,
					repairOrderRowChange.get("repair_status",0)==RepairOrderKey.FINISH?true:false, adminLoggerBean, "repair_intransit_period","true");
		} catch (Exception e) {
			throw new SystemException(e,"repairAproveCancel",log);
		}
	}
	
	/**
	 * 通过返修单ID及商品名模糊查询返修单详细
	 * @param repair_order_id
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairDetailByRepairProductName(long repair_order_id, String name)throws Exception
	{
		try
		{
			return floorRepairOrderMgrZyj.getRepairDetailByRepairProductName(repair_order_id, name);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getRepairDetailByRepairProductName",log);
		}
	}
	
	/**
	 * 根据返修单ID及商品名严格查询返修单详细
	 * @param repair_order_id
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public DBRow getRepairDetailsByRepairProductName(HttpServletRequest request)throws Exception
	{
		try
		{
			long repair_order_id	= StringUtil.getLong(request, "repair_order_id");
			String name				= StringUtil.getString(request, "product_name");
			DBRow product			= floorProductMgr.getDetailProductByPname(name);
			DBRow result			= new DBRow();
			if(null == product)
			{
				result.add("flag", "product_not_exist");
			}
			else
			{
				DBRow[] details = floorRepairOrderMgrZyj.getRepairDetailsByRepairProductName(repair_order_id, name);
				if(details.length == 0)
				{
					result.add("flag", "not_exist");
				}
				else
				{
					result.add("flag", "exist");
				}
			}
			return result;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getRepairDetailByRepairProductName",log);
		}
	}
	
	public void setFloorRepairOrderMgrZyj(
			FloorRepairOrderMgrZyj floorRepairOrderMgrZyj) {
		this.floorRepairOrderMgrZyj = floorRepairOrderMgrZyj;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setProductMgr(ProductMgrIFace productMgr) {
		this.productMgr = productMgr;
	}

	public void setFloorPurchaseMgr(FloorPurchaseMgr floorPurchaseMgr) {
		this.floorPurchaseMgr = floorPurchaseMgr;
	}

	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}


	public void setFloorStorageCatalogMgrZyj(
			FloorStorageCatalogMgrZyj floorStorageCatalogMgrZyj) {
		this.floorStorageCatalogMgrZyj = floorStorageCatalogMgrZyj;
	}

	public void setApplyFundsMgrZyj(ApplyFundsMgrZyjIFace applyFundsMgrZyj) {
		this.applyFundsMgrZyj = applyFundsMgrZyj;
	}

	public void setProductStoreEditLogMgrZJ(
			ProductStoreEditLogMgrIFaceZJ productStoreEditLogMgrZJ) {
		this.productStoreEditLogMgrZJ = productStoreEditLogMgrZJ;
	}


}
