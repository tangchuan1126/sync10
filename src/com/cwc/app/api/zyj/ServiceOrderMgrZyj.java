package com.cwc.app.api.zyj;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.api.CartReturn;
import com.cwc.app.api.zj.CartReturnProductReason;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zr.FloorBillMgrZr;
import com.cwc.app.floor.api.zr.FloorReturnProductItemsFix;
import com.cwc.app.floor.api.zr.FloorReturnProductMgrZr;
import com.cwc.app.floor.api.zyj.FloorReturnProductOrderMgrZyj;
import com.cwc.app.floor.api.zyj.FloorServiceOrderMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zyj.ServiceOrderMgrZyjIFace;
import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.key.ReturnProductSourceKey;
import com.cwc.app.key.ReturnProductStatusKey;
import com.cwc.app.key.ReturnTypeKey;
import com.cwc.app.key.ServiceOrderStatusKey;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.lucene.zyj.ServiceOrderIndexMgrZyj;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class ServiceOrderMgrZyj implements ServiceOrderMgrZyjIFace{
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorServiceOrderMgrZyj floorServiceOrderMgrZyj;
	private FloorProductMgr floorProductMgr;
	private CartReturnProductReason cartReturnProductReason;
	private CartReturn cartReturn;
	private FloorOrderMgr floorOrderMgr;
	private SystemConfigIFace systemConfig;
	private FloorReturnProductMgrZr floorReturnProductMgrZr;
	private FloorReturnProductItemsFix floorReturnProductItemsFix;
	private FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj;
	private FloorBillMgrZr floorBillMgrZr;

	/**
	 * 处理购物车中的商品
	 * @param request
	 * @throws Exception
	 */
	public void putToServiceProductReasonCartForSelect(HttpServletRequest request) throws Exception
	{
		try
		{
			//套装：OI/WI单据明细ID_商品ID_套装数量_订单明细ID
			//散件：OI/WI单据明细ID_商品ID_套装数量_订单明细ID_散件商品ID_散件商品数量
			String value		= StringUtil.getString(request,"value");//商品ID
			String[] valueArr	= value.split("_");
			String typeId		= valueArr[0];//单据类型及ID
			String setPcId		= valueArr[1];//套装或普通商品ID
			float set_pc_count	= Float.parseFloat(valueArr[2]);//套装商品数量
			String orderItemType= typeId.substring(0, 2);
			long orderItemId	= Long.parseLong(typeId.substring(typeId.indexOf("I")+1));//自身单据明细ID
			long porderItemId	= Long.parseLong(valueArr[3]);//订单明细ID
			
			String pc_id		= "";//商品ID
			String set_or_part_pc_id = "";//套装或散件ID
			float pc_count		= 0F;//商品数量
			HttpSession session = StringUtil.getSession(request);
			
			//如果是XXX_XXX_XXX_XXX_XXX形式，那说明选择的是套装中的散件，要从购物车中清除套装商品再加入散件商品
			if(6 == value.split("_").length)
			{
				pc_id = valueArr[4];
				set_or_part_pc_id = setPcId;
				pc_count = Float.parseFloat(valueArr[5])*set_pc_count;
				this.removeServiceProductReason(session, orderItemType, orderItemId, setPcId, "0");//删除套装
			}
			//如果是套装或普通商品，移除其包含的所有散件，加入此商品
			else
			{
				pc_id = setPcId;
				set_or_part_pc_id = "0";
				pc_count = set_pc_count;
				DBRow[] productUnions = floorProductMgr.getProductUnionsBySetPid(Long.parseLong(setPcId));
				
				for (int i = 0; i < productUnions.length; i++) 
				{
					this.removeServiceProductReason(session, orderItemType, orderItemId, productUnions[i].getString("pid"), setPcId);//删除散件
				}
			}
			DBRow product = floorProductMgr.getDetailProductByPcid(Long.parseLong(pc_id));
			//放进去时，商品ID
			put2CartServiceProductReason(session, product.getString("p_name"),pc_id, 1,orderItemType,orderItemId,set_or_part_pc_id, "", 0,pc_count, set_pc_count, porderItemId);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"putToServiceProductReasonCartForSelect",log);
		}
	}
	
	/**
	 * 从购物车去除商品质保原因
	 * 删除时，需要检验单据类型，单据ID，套装ID，散件ID
	 * 所删除的商品一致属于此各形式OI（WI）订单（运单）明细ID_套装商品ID_散件商品ID
	 * @param session
	 * @param pc_id
	 * @throws Exception
	 */
	private void removeServiceProductReason(HttpSession session,String order_item_type, long order_item_id, String pc_id, String set_or_part_pc_id) throws Exception
	{
		try
		{
			if ( session.getAttribute(Config.cartReturnProductReason) != null )
			{
				DBRow rowOld;
				ArrayList al = (ArrayList)session.getAttribute(Config.cartReturnProductReason);
				
				for ( int i=0;i<al.size(); i++ )
				{
					rowOld = (DBRow)al.get(i);
					if (rowOld.getString("order_item_type").equals(order_item_type)
							&& rowOld.get("order_item_id", 0L) == order_item_id
							&& rowOld.getString("cart_pid").equals(pc_id)
							&& rowOld.getString("set_or_part_pc_id").equals(set_or_part_pc_id))
					{
						al.remove(i);
					}
				}
				session.setAttribute(Config.cartReturnProductReason,al);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"removeServiceProductReason",log);
		}
		
	}
	
	/**
	 * 将商品加到购物车中
	 * @param session
	 * @param p_name
	 * @param pc_id
	 * @param quantity
	 * @param reason
	 * @param warranty_type
	 * @throws Exception
	 */
	private void put2CartServiceProductReason(HttpSession session,String p_name,String pc_id,float quantity
			, String order_item_type, long order_item_id, String set_or_part_pc_id
			,String reason,int warranty_type,float count, float set_pc_count, long porder_item_id)
	throws Exception
	{
		try 
		{
			ArrayList<DBRow> al = null;
			if ( !"0".equals(pc_id) && quantity > 0)
			{
				
				//购物车每条记录属性
				DBRow row = new DBRow();
				row.add("order_item_type", order_item_type);//单据类型
				row.add("order_item_id", order_item_id);//单据ID
				row.add("cart_pid",pc_id);//商品ID
				row.add("cart_quantity",quantity);//商品数量
				row.add("p_name",p_name);//商品名
				row.add("set_or_part_pc_id", set_or_part_pc_id);//与此关联的套装（散件）商品ID，可为0
				row.add("return_reason",reason);//原因
				row.add("warranty_type",warranty_type);
				row.add("cart_total_count", count);
				row.add("set_pc_count", set_pc_count);//套装商品数量
				row.add("porder_item_id", porder_item_id);//订单明细ID
				if (session.getAttribute(Config.cartReturnProductReason) == null )
				{
					al = new ArrayList<DBRow>();
					al.add(row);			
				}
				else
				{
					//nOrR 1：添加，2：覆盖；3：不添加
					int nOrR = 1;
					int listIndex = 0;
					al = (ArrayList)session.getAttribute(Config.cartReturnProductReason);
					for (int i = 0; i < al.size(); i++) 
					{
						//如果order_item_id都不为0，什么都比，如果有一方为0，只比较商品ID，一致
						//如果添加的有明细而已有的没有明细用新添加的替代
						if(0 != al.get(i).get("order_item_id", 0L) && 0 != order_item_id)
						{
							//验证是否重商品，单据类型、单据ID、商品ID、关联ID
							if(al.get(i).getString("order_item_type").equals(order_item_type)
									&& al.get(i).get("order_item_id", 0L) == order_item_id
									&& al.get(i).getString("cart_pid").equals(pc_id)
									&& al.get(i).getString("set_or_part_pc_id").equals(set_or_part_pc_id))
							{
								listIndex = i;
								nOrR = 3;
								break;
							}
						}
						else
						{
							if(al.get(i).getString("cart_pid").equals(pc_id))
							{
								if(0 != order_item_id)
								{
									listIndex = i;
									nOrR = 2;
									break;
								}
								else
								{
									nOrR = 3;
								}
							}
						}
					}
					if(1 == nOrR)
					{
						al.add(row);
					}
					else if(2 == nOrR)
					{
						al.set(listIndex,row);
					}
					
				}
				session.setAttribute(Config.cartReturnProductReason,al);			
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"put2CartServiceProductReason",log);
		}
	}
	
	/**
	 * 将商品从购物车中删除
	 * @param request
	 * @throws Exception
	 */
	public void removeServiceProductReason(HttpServletRequest request) throws Exception
	{
		try
		{
			//套装：OI/WI单据明细ID_商品ID_套装数量_订单明细ID
			//散件：OI/WI单据明细ID_商品ID_套装数量_订单明细ID_散件商品ID_散件商品数量
			String value		= StringUtil.getString(request,"value");//商品ID
			String[] valueArr	= value.split("_");
			String typeId		= valueArr[0];//单据类型及ID
			String setPcId		= valueArr[1];//套装或普通商品ID
			String orderItemType= "00".equals(typeId)?"0":typeId.substring(0, 2);
			long orderItemId	= Long.parseLong(typeId.substring(typeId.indexOf("I")+1));
			HttpSession session = StringUtil.getSession(request);
			
			//如果是XXX_XXX_XXX_XXX_XXX形式，那说明选择的是套装中的散件
			if(6 == value.split("_").length)
			{
				this.removeServiceProductReason(session, orderItemType, orderItemId, valueArr[4], setPcId);//删除散件
			}
			//如果是套装或普通商品，移除其包含的所有散件，加入此商品
			else
			{
				this.removeServiceProductReason(session, orderItemType, orderItemId, setPcId, "0");//删除套装
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"removeServiceProductReason(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 添加商品进服务单session
	 */
	public void putToServiceProductReasonCartForAdd(HttpServletRequest request) throws Exception
	{
		try 
		{
			HttpSession session = StringUtil.getSession(request);
			//通过商品名获取商品ID
			String p_name = StringUtil.getString(request,"p_name");
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			if(product==null)
			{
				throw new Exception();
			}
			long pc_id = product.get("pc_id",0l);
			float quantity = StringUtil.getFloat(request,"quantity");
			String reason = StringUtil.getString(request,"reason");
			int warranty_type = StringUtil.getInt(request,"warranty_type");
			this.removeServiceProductReason(session, "0", 0, String.valueOf(pc_id), "0");//删除重复商品
			
			this.put2CartServiceProductReason(session, p_name, String.valueOf(pc_id), quantity, "0", 0, "0", reason, warranty_type, quantity, 0, 0);//将商品质保放入质保明细购物车
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"putToServiceProductReasonCartForAdd(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 将订单或运单上的商品入服务单
	 * @param request
	 * @throws Exception
	 */
	public long applicationOrderOrWaybillPcServing(HttpServletRequest request) throws Exception
	{
		try 
		{
			long oid	= StringUtil.getLong(request,"oid");
			long wid	= StringUtil.getLong(request, "wid");
			cartReturnProductReason.flush(StringUtil.getSession(request));
			DBRow[] productReasons = cartReturnProductReason.getProductReason();
			if(productReasons == null || productReasons.length == 0)
			{
				throw new Exception("cartReturnProductReason is null");
			}
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid				= adminLoggerBean.getAdid();
			//添加服务单
			DBRow serviceRow = new DBRow();
			serviceRow.add("oid", oid);
			serviceRow.add("create_user", adid);
			serviceRow.add("create_date", DateUtil.NowStr());
			serviceRow.add("wid", wid);
			serviceRow.add("status", ServiceOrderStatusKey.WAITING);//等待处理
			long sid = floorServiceOrderMgrZyj.addServiceOrder(serviceRow);
			//添加服务单明细
			for (int i = 0; i < productReasons.length; i++)
			{
				String order_item_type	= productReasons[i].getString("order_item_type");//单据类型
				long order_item_id		= productReasons[i].get("order_item_id", 0L);//相关订单或运单明细ID
				long porder_item_id		= productReasons[i].get("porder_item_id", 0L);//订单明细ID
				long cart_pid			= productReasons[i].get("cart_pid", 0L);//商品ID
				float cart_quantity		= productReasons[i].get("cart_quantity", 0F);//商品数量
				String p_name			= productReasons[i].getString("p_name");//商品名
				long set_or_part_pc_id	= productReasons[i].get("set_or_part_pc_id", 0L);//散件的套装ID
				String return_reason	= productReasons[i].getString("return_reason");//服务原因
				int warranty_type		= productReasons[i].get("warranty_type", 0);//申请质保类型
				float cart_total_count	= productReasons[i].get("cart_total_count", 0F);
				float set_pc_count		= productReasons[i].get("set_pc_count", 0F);//散件的套装数量
				int product_type		= 0;
				DBRow product			= floorProductMgr.getDetailProductByPcid(cart_pid);
				if(0 == product.get("union_flag", 0))
				{
					product_type = CartReturn.NORMAL;
				}
				else
				{
					product_type = CartReturn.UNION_STANDARD;
				}
				
				DBRow  sdRow			= new DBRow();
				sdRow.add("oid", oid);
				sdRow.add("sid", sid);
				
				//运单上的商品记录了订单明细的ID
				if("WI".equals(order_item_type))
				{
					sdRow.add("oi_id", porder_item_id);
					sdRow.add("wi_id", order_item_id);
				}
				else if("OI".equals(order_item_type))
				{
					sdRow.add("oi_id", order_item_id);
					sdRow.add("wi_id", 0);
				}
				else
				{
					sdRow.add("oi_id", 0);
					sdRow.add("wi_id", 0);
				}
				sdRow.add("wid", wid);
				sdRow.add("pid", cart_pid);
				sdRow.add("p_name", p_name);
				sdRow.add("product_type", product_type);
				sdRow.add("quantity", cart_quantity);
				sdRow.add("unit_id", set_or_part_pc_id);
				sdRow.add("unit_name", null!=product?product.getString("unit_name"):"");
				sdRow.add("unit_count", set_pc_count);
				sdRow.add("service_reason", return_reason);
				sdRow.add("creator", adid);
				sdRow.add("create_time", DateUtil.NowStr());
				sdRow.add("warranty_type", warranty_type);
				floorServiceOrderMgrZyj.addServiceOrderDetails(sdRow);
				
			}
			
			cartReturn.clearCart(StringUtil.getSession(request));
			
			DBRow porder = floorOrderMgr.getDetailPOrderByOid(oid);
			//加跟进
			DBRow note = new DBRow();
			note.add("oid", oid);
			note.add("note", "申请服务 服务单号:"+sid);
			note.add("account", adminLoggerBean.getAccount());
			note.add("adid", adminLoggerBean.getAdid());
			note.add("trace_type", TracingOrderKey.CREATE_WARRANTY);
			note.add("post_date", DateUtil.NowStr());
			note.add("rel_id", sid);
			note.add("trace_child_type",porder.get("internal_tracking_status",0));
			floorOrderMgr.addPOrderNote(note);
			
			//处理索引
			ServiceOrderIndexMgrZyj.getInstance().addIndex(sid);
			return sid;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"applicationOrderOrWaybillPcServing(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 通过服务单ID获取服务信息
	 */
	public DBRow getServiceOrderDetailBySid(long sid) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceOrderDetailBySid(sid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getServiceOrderBySid(long sid)",log);
		}
	}
	
	/**
	 * 通过服务单ID获取服务单明细列表
	 */
	public DBRow[] getServiceOrderItemsByServiceId(long sid) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceOrderItemsByServiceId(sid);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getServiceOrderItemsByServiceId(long sid)",log);
		}
	}
	
	/**
	 * 保存服务单的服务原因及处理结果
	 */
	public long saveServiceOrderReasonSolution(long sid, String reason, String solution, String description, long adid) throws Exception
	{
		try 
		{
			DBRow row			= new DBRow();
			row.add("sid", sid);
			row.add("reason", reason);
			row.add("solution", solution);
			row.add("description", description);
			row.add("creator", adid);
			row.add("create_time", DateUtil.NowStr());
			return floorServiceOrderMgrZyj.saveServiceOrderReasonSolution(row);
			
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"saveServiceOrderReasonSolution(request)",log);
		}
	}
	
	/**
	 * 保存服务单的服务原因及处理结果
	 * @param request
	 * @throws Exception
	 */
	public void saveServiceOrderReasonSolutions(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean	=((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long sid				= StringUtil.getLong(request, "sid");
			String[] reasons		= request.getParameterValues("reason");
			String[] solutions		= request.getParameterValues("solution");
			String[] descriptions	= request.getParameterValues("description");
			if(null != reasons)
			{
				for (int i = 0; i < reasons.length; i++) 
				{
					if(!"".equals(reasons[i]) && !"".equals(reasons[i].trim()))
					{
						this.saveServiceOrderReasonSolution(sid, reasons[i], solutions[i], descriptions[i],adminLoggerBean.getAdid());
					}
				}
			}
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"saveServiceOrderReasonSolutions(request)",log);
		}
		
	}
	
	/**
	 * 通过订单ID获取服务列表
	 */
	public DBRow[] getServiceOrdersByPOrderId(long oid) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceOrdersByPOrderId(oid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getServiceOrdersByPOrderId(oid)",log);
		}
	}
	
	/**
	 * 通过订单ID获取服务总数
	 */
	public int getServiceOrderCountByPOrderId(long oid) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceOrderCountByPOrderId(oid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getServiceOrderCountByPOrderId(oid)",log);
		}
	}
	
	/**
	 * 通过运单ID得到服务单列表
	 */
	public DBRow[] getServiceOrdersByWaybillId(long wid) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceOrdersByWaybillId(wid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getServiceOrdersByWaybillId(wid)",log);
		}
	}
	
	/**
	 * 通过运单ID得到服务单数量
	 * @param wid
	 * @return
	 * @throws Exception
	 */
	public int getServiceOrderCountByWaybillId(long wid) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceOrderCountByWaybillId(wid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getServiceOrderCountByWaybillId(wid)",log);
		}
	}
	
	/**
	 * 通过服务单id获取服务处理列表
	 */
	public DBRow[] getServiceOrderReasonSolutionsBySid(long sid) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceOrderReasonSolutionsBySid(sid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getServiceOrderReasonSolutionsBySid(sid)",log);
		}
	}
	
	/**
	 * 通过服务单id获取服务处理列表
	 */
	public DBRow[] getServiceOrderReasonSolutionsBySid(long sid, int num) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceOrderReasonSolutionsBySid(sid,num);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getServiceOrderReasonSolutionsBySid(long sid, int num)",log);
		}
	}
	
	/**
	 * 通过服务单id获取服务处理数量
	 */
	public int getServiceOrderReasonSolutionCountBySid(long sid) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceOrderReasonSolutionCountBySid(sid);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getServiceOrderReasonSolutionCountBySid(sid)",log);
		}
	}
	
	@Override
	public void saveServiceIsNeedReturn(HttpServletRequest request) throws Exception
	{
		try 
		{
			long sid			= StringUtil.getLong(request, "sid");
			int is_need_return	= StringUtil.getInt(request, "is_need_return");
			DBRow row			= new DBRow();
			row.add("sid", sid);
			row.add("is_need_return", is_need_return);
			floorServiceOrderMgrZyj.updateServiceOrder(row, sid);
		}
		catch (Exception e)
		{
			throw new SystemException(e, "saveServiceIsNeedReturn", log);
		}
		
	}
	
	/**
	 * 通过服务单ID获取账单列表
	 */
	@Override
	public DBRow[] getServiceBillOrdersBySid(long sid) throws Exception
	{
		try 
		{
			return floorServiceOrderMgrZyj.getServiceBillOrdersBySid(sid);
		}
		catch (Exception e)
		{
			throw new SystemException(e, "saveServiceIsNeedReturn", log);
		}
	}
	
	/**
	 * 将服务明细放在购物车里
	 * @param request
	 * @throws Exception
	 */
	public void loadServiceItemToCart(HttpServletRequest request) throws Exception
	{
		try 
		{
			HttpSession session		= StringUtil.getSession(request);
			long sid				= StringUtil.getLong(request, "sid");
			DBRow[] serviceItems	= floorServiceOrderMgrZyj.getServiceOrderItemsByServiceId(sid);
			for (int i = 0; i < serviceItems.length; i++) 
			{
				long order_item_id	= 0;
				String orderItemType= "";
				if(0 != serviceItems[i].get("wi_id",0L))
				{
					orderItemType = "WI";
					order_item_id = serviceItems[i].get("wi_id",0L);
				}
				else if(0 != serviceItems[i].get("oi_id",0L))
				{
					orderItemType = "OI";
					order_item_id = serviceItems[i].get("oi_id",0L);
				}
				else if(0 == serviceItems[i].get("oi_id",0L) && 0 == serviceItems[i].get("wi_id",0L))
				{
					orderItemType = "0";
					order_item_id = 0;
				}
				
				put2CartServiceProductReason(session, serviceItems[i].getString("p_name"),String.valueOf(serviceItems[i].get("pid",0L)),
						serviceItems[i].get("quantity", 0F),orderItemType,order_item_id,String.valueOf(serviceItems[i].get("unit_id",0L)), serviceItems[i].getString("service_reason"),
						serviceItems[i].get("warranty_type", 0),serviceItems[i].get("quantity", 0F), serviceItems[i].get("unit_count", 0F), serviceItems[i].get("oi_id",0L));
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e, "loadServiceItemToCart", log);
		}
	}
	
	/**
	 * 修改服务session
	 * @param request
	 * @throws Exception
	 */
	@Override
	public void modServiceQuantitySession(HttpServletRequest request) throws Exception
	{
		try
		{
			String[] item_pc_unit_infos	= request.getParameterValues("item_pc_unit_info");
			String[] return_reasons		= request.getParameterValues("return_reason");
			String[] quantitys			= request.getParameterValues("quantity");		
			String[] waranty_types		= request.getParameterValues("waranty_type");
			
			HttpSession session = StringUtil.getSession(request);
			
			if ( session.getAttribute(Config.cartReturnProductReason) != null )
			{
				if(item_pc_unit_infos!=null&&item_pc_unit_infos.length>0)
				{
					ArrayList al = (ArrayList)session.getAttribute(Config.cartReturnProductReason);
					
					for (int i = 0; i < item_pc_unit_infos.length; i++) 
					{
						String[] item_pc_unit_info	= item_pc_unit_infos[i].split("_");
						String order_item_type_p	= item_pc_unit_info[0];
						long order_item_id_p		= Long.parseLong(item_pc_unit_info[1]);
						long pc_id_p				= Long.parseLong(item_pc_unit_info[2]);
						long set_or_part_pc_id_p	= Long.parseLong(item_pc_unit_info[3]);
						float quantity_p			= Float.parseFloat(quantitys[i]);
						int waranty_type_p			= Integer.parseInt(waranty_types[i]);
						String return_reason_p		= return_reasons[i];
						
						for (int j = 0; j < al.size(); j++)
						{
							DBRow productReason = (DBRow)al.get(j);
							
							String order_item_type_s	= productReason.getString("order_item_type");
							long order_item_id_s		= productReason.get("order_item_id", 0L);
							long pc_id_s				= productReason.get("cart_pid", 0L);
							long set_or_part_pc_id_s	= productReason.get("set_or_part_pc_id", 0L);
							
							if(order_item_type_p.equals(order_item_type_s) && order_item_id_p == order_item_id_s
									&& pc_id_p == pc_id_s && set_or_part_pc_id_p == set_or_part_pc_id_s)
							{
								productReason.add("cart_quantity",quantity_p);
								productReason.add("return_reason",return_reason_p);
								productReason.add("warranty_type",waranty_type_p);
								al.set(j,productReason);
								break;
							}
						}
					}
					
					session.setAttribute(Config.cartReturnProductReason,al);
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "modServiceQuantitySession", log);
		}
	}
	
	/**
	 * 修改服务单
	 */
	@Override
	public void modServiceItems(HttpServletRequest request) throws Exception
	{
		try
		{
			long sid	= StringUtil.getLong(request, "sid");
			long oid	= StringUtil.getLong(request, "oid");
			long wid	= StringUtil.getLong(request, "wid");
			//将修改的服务单上原有的明细删除
			floorServiceOrderMgrZyj.deleteServiceItemsBySid(sid);
			cartReturnProductReason.flush(StringUtil.getSession(request));
			DBRow[] productReasons	= cartReturnProductReason.getProductReason();
			if(null == productReasons || 0 == productReasons.length)
			{
				throw new Exception("cartReturnProductReason is null");
			}
			AdminLoginBean adminLoggerBean	=((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid						= adminLoggerBean.getAdid();
			for 
			(int i = 0; i < productReasons.length; i++)
			{
				String order_item_type	= productReasons[i].getString("order_item_type");//单据类型
				long order_item_id		= productReasons[i].get("order_item_id", 0L);//相关订单或运单明细ID
				long porder_item_id		= productReasons[i].get("porder_item_id", 0L);//订单明细ID
				long cart_pid			= productReasons[i].get("cart_pid", 0L);//商品ID
				float cart_quantity		= productReasons[i].get("cart_quantity", 0F);//商品数量
				String p_name			= productReasons[i].getString("p_name");//商品名
				long set_or_part_pc_id	= productReasons[i].get("set_or_part_pc_id", 0L);//散件的套装ID
				String return_reason	= productReasons[i].getString("return_reason");//服务原因
				int warranty_type		= productReasons[i].get("warranty_type", 0);
				float cart_total_count	= productReasons[i].get("cart_total_count", 0F);
				float set_pc_count		= productReasons[i].get("set_pc_count", 0F);//散件的套装数量
				int product_type		= 0;
				DBRow product			= floorProductMgr.getDetailProductByPcid(cart_pid);
				if(0 == product.get("union_flag", 0))
				{
					product_type = CartReturn.NORMAL;
				}
				else
				{
					product_type = CartReturn.UNION_STANDARD;
				}
				
				DBRow  sdRow			= new DBRow();
				sdRow.add("oid", oid);
				sdRow.add("sid", sid);
				//运单上的商品记录了订单明细的ID
				if("WI".equals(order_item_type))
				{
					sdRow.add("oi_id", porder_item_id);
					sdRow.add("wi_id", order_item_id);
				}
				else if("OI".equals(order_item_type))
				{
					sdRow.add("oi_id", order_item_id);
					sdRow.add("wi_id", 0);
				}
				else
				{
					sdRow.add("oi_id", 0);
					sdRow.add("wi_id", 0);
				}
				sdRow.add("wid", wid);
				sdRow.add("pid", cart_pid);
				sdRow.add("p_name", p_name);
				sdRow.add("product_type", product_type);
				sdRow.add("quantity", cart_quantity);
				sdRow.add("unit_id", set_or_part_pc_id);
				sdRow.add("unit_name", null!=product?product.getString("unit_name"):"");
				sdRow.add("unit_count", set_pc_count);
				sdRow.add("service_reason", return_reason);
				sdRow.add("creator", adid);
				sdRow.add("create_time", DateUtil.NowStr());
				sdRow.add("warranty_type", warranty_type);
				floorServiceOrderMgrZyj.addServiceOrderDetails(sdRow);
			}
			//加日志
			
			cartReturn.clearCart(StringUtil.getSession(request));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "modServiceItems(HttpServletRequest request)", log);
		}
	}
	
	/**
	 * 分页查询服务列表
	 */
	public DBRow[] getServiceOrderByPage(PageCtrl pc) throws Exception
	{
		try
		{
			return floorServiceOrderMgrZyj.getServiceOrderByPage(pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "getServiceOrderByPage(PageCtrl pc)", log);
		}
	}
	
	/**
	 * 根据条件过滤服务列表
	 * @param pc
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceOrderByPage(PageCtrl pc, int status, int is_need_return, String st, String end, long create_adid) throws Exception
	{
		try
		{
			return floorServiceOrderMgrZyj.getServiceOrderByPage(pc, status,is_need_return,st, end, create_adid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "getServiceOrderByPage(PageCtrl pc)", log);
		}
	}
	
	/**
	 * 根据返修单号搜索返修单
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchServiceOrderByNumber(String search_key,int search_mode,PageCtrl pc) throws Exception 
	{
		try 
		{
			if(!search_key.equals("")&&!search_key.equals("\""))
			{
				search_key = search_key.replaceAll("\"","");
				search_key = search_key.replaceAll("'","");
				search_key = search_key.replaceAll("\\*","");
//				search_key +="*";这个索引是IK分词器，不要最后+*
			}
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			return ServiceOrderIndexMgrZyj.getInstance().getSearchResults(search_key,search_mode,page_count,pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"searchServiceOrderByNumber",log);
		}
	}
	
	/**
	 * 通过服务处理ID获取处理明细
	 */
	@Override
	public DBRow getServiceReasonSolutionBySrsid(long srs_id) throws Exception
	{
		try
		{
			return floorServiceOrderMgrZyj.getServiceReasonSolutionBySrsid(srs_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getServiceReasonSolutionBySrsid(long srs_id)",log);
		}
	}
	
	/**
	 * 更新服务处理
	 */
	@Override
	public void updateServiceReasonSolutionBySrsid(HttpServletRequest request) throws Exception
	{
		try
		{
			long srs_id			= StringUtil.getLong(request, "srs_id");
			String reason		= StringUtil.getString(request, "reason");
			String solution		= StringUtil.getString(request, "solution");
			String description	= StringUtil.getString(request, "description");
			
			DBRow row			= new DBRow();
			row.add("reason", reason);
			row.add("solution", solution);
			row.add("description", description);
			
			floorServiceOrderMgrZyj.updateServiceReasonSolutionBySrsid(row, srs_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateServiceReasonSolutionBySrsid(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 更新服务单
	 */
	public void updateServiceOrderBySid(DBRow row, long sid) throws Exception
	{
		try
		{
			floorServiceOrderMgrZyj.updateServiceOrder(row, sid);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateServiceOrderBySid(DBRow row, long sid)",log);
		}
	}
	
	/**
	 * 处理退货单数据
	 * 创建服务单及服务单明细
	 * 更改退货单及退货明细
	 * 更改账单记录的ID
	 * 更改porder_note的相关数据，如果是申请服务则将关联ID由退货单ID改成服务ID
	 * @throws Exception
	 */
	public void handleReturnProductData()throws Exception
	{
		try 
		{
			long start = System.currentTimeMillis();
			//遍历所有的退货单，创建服务单
			DBRow[] returns = floorServiceOrderMgrZyj.getAllReturnOrder();
			for (int i = 0; i < returns.length; i++)
			{
				long rp_id			= returns[i].get("rp_id", 0L);
				long oid			= returns[i].get("oid", 0L);
				long create_user	= returns[i].get("create_user", 0L);
				String create_date	= returns[i].getString("create_date");
				long handle_user	= returns[i].get("handle_user", 0L);
				String handle_date	= returns[i].getString("handle_date");
				int status			= returns[i].get("status",0);
				String note			= returns[i].getString("note");
				long warranty_oid	= returns[i].get("warranty_oid", 0L);
				int product_status	= returns[i].get("product_status", 0);
				
				//创建服务单
				DBRow serviceRow	= new DBRow();
				serviceRow.add("oid", oid);
				serviceRow.add("create_user", create_user);
				serviceRow.add("create_date", create_date);
				if(!"".equals(handle_user))
				{
					serviceRow.add("handle_user", handle_user);
				}
				if(!"".equals(handle_date))
				{
					serviceRow.add("handle_date", handle_date);
				}
				//如果退货单的状态为等待付款或者已有订单或者登记退货，则认为服务完成
//				if(status == ReturnProductKey.WAITINGPAY || status == ReturnProductKey.CREATEORDER || status == ReturnProductKey.FINISH || ReturnProductStatusKey.NoReturn==product_status)
//				{
//					serviceRow.add("status", ServiceOrderStatusKey.FINISH);
//				}
//				else
//				{
					serviceRow.add("status", status);
//				}
				serviceRow.add("note",note);
				if(ReturnProductStatusKey.NoReturn==product_status)
				{
					serviceRow.add("is_need_return",ReturnTypeKey.NOT_NEED);
				}
				else
				{
					serviceRow.add("is_need_return",ReturnTypeKey.NEED);
				}
				serviceRow.add("warranty_oid", warranty_oid);
				long sid = floorServiceOrderMgrZyj.addServiceOrder(serviceRow);
				
				//更新退货
				DBRow returnRow		= new DBRow();
				returnRow.add("sid", sid);
				returnRow.add("create_type", 2); 
				DBRow porder				= floorOrderMgr.getDetailPOrderByOid(oid); //获取订单
				if(null != porder)
				{
					//获取地址信息
					String first_name			= porder.getString("first_name");
					String last_name			= porder.getString("last_name");
					long ccid					= porder.get("ccid",0L);				//国家ID
					String address_country		= porder.getString("address_country");	//国家名
					String address_country_code = porder.getString("address_country_code");//国家code
					long pro_id					= porder.get("pro_id",0L);			//省份ID
					String address_state		= porder.getString("address_state");	//省份字符串   
					String address_city			= porder.getString("address_city");	//城市
					String address_street		= porder.getString("address_street");	//街道
					String address_zip			= porder.getString("address_zip");		//邮编
					String tel					= porder.getString("tel");				//电话
					
					returnRow.add("first_name", first_name);
					returnRow.add("last_name", last_name);
					returnRow.add("address_country", address_country);
					returnRow.add("address_city", address_city);
					returnRow.add("address_state", address_state);
					returnRow.add("address_zip", address_zip);
					returnRow.add("address_country_code", address_country_code);
					returnRow.add("address_street", address_street);
					returnRow.add("tel", tel);
					returnRow.add("ccid", ccid);
					returnRow.add("pro_id", pro_id);
				}
				returnRow.add("return_status", status);//记录原状态
				returnRow.add("return_product_source", ReturnProductSourceKey.CUSTOMER_POST_BACK);//客户来源：客户退回
				//如果退货单的状态为等待付款或者已有订单或者登记退货，则退货完成
//				if(status == ReturnProductKey.WAITINGPAY || status == ReturnProductKey.CREATEORDER || status == ReturnProductKey.FINISH)
//				{
//					returnRow.add("status", ReturnProductKey.FINISHALL);
//				}
				if(ReturnProductStatusKey.NoReturn==product_status)
				{
					returnRow.add("is_need_return",ReturnTypeKey.NOT_NEED);
				}
				else
				{
					returnRow.add("is_need_return",ReturnTypeKey.NEED);
				}
				floorReturnProductMgrZr.updateReturnProduct(returnRow, rp_id);
				
				DBRow[] returnItems	= floorReturnProductItemsFix.getReturnProductItemByRpId(rp_id);
				for (int j = 0; j < returnItems.length; j++)
				{
					long rpi_id			= returnItems[j].get("rpi_id", 0L);
					String p_name		= returnItems[j].getString("p_name");
					long pid			= returnItems[j].get("pid", 0L);
					float quantity		= returnItems[j].get("quantity", 0F);
					String unit_name	= returnItems[j].getString("unit_name");
					int product_type	= returnItems[j].get("product_type", 0);
					
					int warranty_type	= 0;
					String return_reason= "";
					DBRow[] returnItemReasons = floorReturnProductOrderMgrZyj.getReturnReasonByRpidAndPid(rp_id, pid);
					if(returnItemReasons.length > 0)
					{
						warranty_type = returnItemReasons[0].get("warranty_type", 0);
						return_reason = returnItemReasons[0].getString("return_reason");
					}
					DBRow serviceItem = new DBRow();
					serviceItem.add("p_name", p_name);
					serviceItem.add("pid", pid);
					serviceItem.add("quantity", quantity);
					serviceItem.add("unit_name", unit_name);
					serviceItem.add("product_type", product_type);
					serviceItem.add("warranty_type", warranty_type);
					serviceItem.add("service_reason", return_reason);
					serviceItem.add("oid", oid);
					serviceItem.add("sid", sid);
					serviceItem.add("creator", create_user);
					serviceItem.add("create_time", create_date);
					long si_id = floorServiceOrderMgrZyj.addServiceOrderDetails(serviceItem);
					//更新退货明细
					DBRow returnItem = new DBRow();
					returnItem.add("siid", si_id);
					returnItem.add("return_reason", return_reason);
					floorReturnProductItemsFix.updateReturnProductItem(returnItem, rpi_id);
				}
				
				//账单
				DBRow billRow = new DBRow();
				billRow.add("sid", sid);
				floorBillMgrZr.updateBill(returns[i].get("bill_id", 0L), billRow);
				
				//note
				int trace_type		= TracingOrderKey.CREATE_WARRANTY;
				DBRow[] orderNotes	= floorServiceOrderMgrZyj.getPorderNoteByOidTraceTypeRelId(oid, trace_type, rp_id);
				for (int j = 0; j < orderNotes.length; j++)
				{
					DBRow orderNote = new DBRow();
					orderNote.add("rel_id", sid);
					orderNote.add("rel_id_old", orderNote.get("rel_id", 0L));
					floorOrderMgr.modPOrderNote(orderNotes[j].get("on_id", 0L), orderNote);
				}
				////system.out.println("I:"+i+",rp_id:"+returns[i].get("rp_id", 0L));
			}
			long end = System.currentTimeMillis();
			////system.out.println("-------------------------------------用时:"+(end-start)+"----------------------------------------------");
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"handleReturnProductData()",log);
		}
	}
	
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
	
	public void setFloorServiceOrderMgrZyj(
			FloorServiceOrderMgrZyj floorServiceOrderMgrZyj) {
		this.floorServiceOrderMgrZyj = floorServiceOrderMgrZyj;
	}

	public void setCartReturnProductReason(
			CartReturnProductReason cartReturnProductReason) {
		this.cartReturnProductReason = cartReturnProductReason;
	}

	public void setCartReturn(CartReturn cartReturn) {
		this.cartReturn = cartReturn;
	}

	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setFloorReturnProductMgrZr(
			FloorReturnProductMgrZr floorReturnProductMgrZr) {
		this.floorReturnProductMgrZr = floorReturnProductMgrZr;
	}

	public void setFloorReturnProductItemsFix(
			FloorReturnProductItemsFix floorReturnProductItemsFix) {
		this.floorReturnProductItemsFix = floorReturnProductItemsFix;
	}

	public void setFloorReturnProductOrderMgrZyj(
			FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj) {
		this.floorReturnProductOrderMgrZyj = floorReturnProductOrderMgrZyj;
	}

	public void setFloorBillMgrZr(FloorBillMgrZr floorBillMgrZr) {
		this.floorBillMgrZr = floorBillMgrZr;
	}
	
	
}
