package com.cwc.app.api.zyj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.b2b.B2BOrderDetailRepeatException;
import com.cwc.app.exception.b2b.B2BOrderDetailSamePcDiffLotNumberException;
import com.cwc.app.exception.b2b.B2BOrderNoExistException;
import com.cwc.app.exception.b2b.B2BOrderNoExistItemException;
import com.cwc.app.exception.b2b.B2BOrderSerialNumberRepeatException;
import com.cwc.app.exception.deliveryOrder.RepeatProductException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.exception.transport.CanNotFillSerialNumberException;
import com.cwc.app.exception.transport.HadSerialNumberException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorCustomerIdMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorSyncLoadRelationMgr;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.app.floor.api.zyj.FloorPurchaseMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zj.LPTypeMgrIFaceZJ;
import com.cwc.app.iface.zj.SQLServerMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.TransportMgrIfaceZr;
import com.cwc.app.iface.zyj.ApplyFundsMgrZyjIFace;
import com.cwc.app.iface.zyj.B2BOrderMgrIFaceZyj;
import com.cwc.app.iface.zyj.ContainerMgrIFaceZyj;
import com.cwc.app.iface.zyj.TransportMgrZyjIFace;
import com.cwc.app.key.B2BOrderCertificateKey;
import com.cwc.app.key.B2BOrderClearanceKey;
import com.cwc.app.key.B2BOrderDeclarationKey;
import com.cwc.app.key.B2BOrderKey;
import com.cwc.app.key.B2BOrderLogTypeKey;
import com.cwc.app.key.B2BOrderProductFileKey;
import com.cwc.app.key.B2BOrderQualityInspectionKey;
import com.cwc.app.key.B2BOrderStockInSetKey;
import com.cwc.app.key.B2BOrderTagKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.lucene.zyj.B2BOrderIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.json.JsonObject;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;

@SuppressWarnings("all")
public class B2BOrderMgrZyj implements B2BOrderMgrIFaceZyj {
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorB2BOrderMgrZyj floorB2BOrderMgrZyj;
	private SystemConfigIFace systemConfig;
	private FloorProductMgr floorProductMgr;
	private FloorCatalogMgr floorCatalogMgr;
	private LPTypeMgrIFaceZJ lpTypeMgrZJ;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private ApplyFundsMgrZyjIFace applyFundsMgrZyj;
	private FloorTransportMgrZr floorTransportMgrZr;
	private TransportMgrIfaceZr transportMgrZr;
	private FloorPurchaseMgrZyj floorPurchaseMgrZyj;
	private TransportMgrZyjIFace transportMgrZyj;
	
	private ContainerMgrIFaceZyj containerMgrZyj;
	private SQLServerMgrIFaceZJ sqlServerMgrZJ;
	private FloorCustomerIdMgr floorCustomerIdMgr;
	private FloorSyncLoadRelationMgr floorLoadMgr;
	
	public void setFloorLoadMgr(FloorSyncLoadRelationMgr floorLoadMgr) {
		this.floorLoadMgr = floorLoadMgr;
	}
	
	public void setFloorCustomerIdMgr(FloorCustomerIdMgr floorCustomerIdMgr) {
		this.floorCustomerIdMgr = floorCustomerIdMgr;
	}
	
	/**
	 * 过滤订单（支持根据转运仓库、目的仓库、订单状态过滤）
	 * 
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow[] fillterB2BOrder(long send_psid, long receive_psid, PageCtrl pc, int status, int declaration,
			int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id)
			throws Exception {
		try {
			return (floorB2BOrderMgrZyj.fillterB2BOrder(send_psid, receive_psid, pc, status, declaration, clearance,
					invoice, drawback, day, stock_in_set, create_account_id));
		} catch (Exception e) {
			throw new SystemException(e, "fillterB2BOrder", log);
		}
	}
	
	/**
	 * 根据订单号搜索订单
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchB2BOrderByNumber(String search_key, int search_mode, PageCtrl pc) throws Exception {
		try {
			if (!search_key.equals("") && !search_key.equals("\"")) {
				search_key = search_key.replaceAll("\"", "");
				search_key = search_key.replaceAll("'", "");
				search_key = search_key.replace("\\*", "");
				// search_key +="*";这个索引是IK分词器，不要最后+*
			}
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			return B2BOrderIndexMgr.getInstance().getSearchResults(search_key, search_mode, page_count, pc);
		} catch (Exception e) {
			throw new SystemException(e, "searchB2BOrderByNumber", log);
		}
	}
	
	/**
	 * 搜索订单
	 */
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc)
			throws Exception {
		try {
			return floorB2BOrderMgrZyj.getAnalysis(st, en, analysisType, analysisStatus, day, pc);
		} catch (Exception e) {
			throw new SystemException(e, "getAnalysis", log);
		}
	}
	
	/**
	 * 跟进备货
	 */
	public DBRow[] getNeedTrackReadyB2BOrder(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int need_track_ready_delivery_day = systemConfig.getIntConfigValue("b2b_order_ready_period");// 订单备货跟进周期（天）
			return floorB2BOrderMgrZyj.getNeedTrackReadyB2BOrder(product_line_id, need_track_ready_delivery_day, pc);
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackReadyB2BOrder(long product_line_id,int track_day,PageCtrl pc)",
					log);
		}
	}
	
	/**
	 * 需跟进内部标签的订单
	 * 
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackTagB2BOrder(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int tag_day = systemConfig.getIntConfigValue("b2b_order_tag_period"); // 订单内部标签周期
			return (floorB2BOrderMgrZyj.getNeedTrackTagB2BOrder(product_line_id, tag_day, pc));
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackTagB2BOrder", log);
		}
	}
	
	/**
	 * 需跟进第三方标签的订单
	 * 
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackThirdTagB2BOrder(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int tag_day = systemConfig.getIntConfigValue("b2b_order_third_tag_period"); // 第三方标签周期
			return (floorB2BOrderMgrZyj.getNeedTrackThirdTagB2BOrder(product_line_id, tag_day, pc));
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackTagB2BOrder", log);
		}
	}
	
	/**
	 * 获得需跟进质检的订单
	 * 
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackQualityInspectionB2BOrder(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int quality_inspection_day = systemConfig.getIntConfigValue("b2b_order_quality_inspection"); // 订质检周期
			
			return (floorB2BOrderMgrZyj.getNeedTrackQualityInspectionB2BOrder(product_line_id, quality_inspection_day,
					pc));
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackQualityInspectionB2BOrder", log);
		}
	}
	
	/**
	 * 获得需跟进实物图片的订单
	 * 
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackProductFileB2BOrder(long product_line_id, PageCtrl pc) throws Exception {
		try {
			int product_file_day = systemConfig.getIntConfigValue("b2b_order_product_file_period"); // 订实物图片周期
			
			return (floorB2BOrderMgrZyj.getNeedTrackProductFileB2BOrder(product_line_id, product_file_day, pc));
		} catch (Exception e) {
			throw new SystemException(e, "getProductFileB2BOrder", log);
		}
	}
	
	/**
	 * 根据仓库还有类型过滤需跟进的订单发货
	 * 
	 * @param ps_id
	 * @param cmd
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackSendB2BOrderByPsid(long ps_id, String cmd, PageCtrl pc) throws Exception {
		try {
			DBRow[] results = new DBRow[0];
			if (cmd.equals("ready_send_ps")) {
				int b2b_order_ready_period = systemConfig.getIntConfigValue("b2b_order_ready_period");// 订单备货跟进周期（天）
				results = floorB2BOrderMgrZyj.getNeedTrackSendB2BOrderByPsid(ps_id, b2b_order_ready_period,
						B2BOrderKey.READY, pc);
			} else if (cmd.equals("packing_send_ps")) {
				int b2b_order_packing_period = systemConfig.getIntConfigValue("b2b_order_packing_period");// 订单装箱跟进周期（天）
				results = floorB2BOrderMgrZyj.getNeedTrackSendB2BOrderByPsid(ps_id, b2b_order_packing_period,
						B2BOrderKey.PACKING, pc);
			} else if (cmd.equals("tag_send_ps")) {
				int tag_day = systemConfig.getIntConfigValue("b2b_order_tag_period"); // 订单制签周期
				results = floorB2BOrderMgrZyj.getNeedTrackTagB2BOrderByPs(ps_id, tag_day, pc);
			} else if (cmd.equals("product_file_send_ps")) {
				int product_file_day = systemConfig.getIntConfigValue("b2b_order_product_file_period"); // 订单实物图片周期
				results = floorB2BOrderMgrZyj.getNeedTrackProductFileB2BOrder(ps_id, product_file_day, pc);
			} else if (cmd.equals("quality_inspection_send_ps")) {
				int quality_inspection_day = systemConfig.getIntConfigValue("b2b_order_quality_inspection"); // 订单质检周期
				results = floorB2BOrderMgrZyj.getNeedTrackQualityInspectionB2BOrder(ps_id, quality_inspection_day, pc);
			}
			return results;
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackSendB2BOrderByPsid", log);
		}
	}
	
	/**
	 * 根据仓库还有类型过滤需跟进的仓库收货
	 * 
	 * @param ps_id
	 * @param cmd
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReceiveB2BOrderByPsid(long ps_id, String cmd, PageCtrl pc) throws Exception {
		try {
			DBRow[] results = new DBRow[0];
			if (cmd.equals("intransit_receive_ps")) {
				int need_track_intransit_day = systemConfig.getIntConfigValue("b2b_order_intransit_period");// 订单运输中跟进周期（天）
				
				results = floorB2BOrderMgrZyj.getNeedTrackReceiveB2BOrderByPsid(ps_id, need_track_intransit_day,
						B2BOrderKey.INTRANSIT, pc);
			} else if (cmd.equals("alreadyRecive_receive_ps")) {
				int need_track_alreadyRecive_transport_hour = systemConfig
						.getIntConfigValue("b2b_order_alreadyreceive_period");// 订单已收货跟进周期（时）
				
				results = floorB2BOrderMgrZyj.getNeedTrackReceiveB2BOrderByPsid(ps_id,
						need_track_alreadyRecive_transport_hour, B2BOrderKey.AlREADYARRIAL, pc);
			}
			
			return results;
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackReceiveB2BOrderByPsid", log);
		}
	}
	
	/**
	 * 需跟进海运的订单
	 * 
	 * @param cmd
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] trackOceanShippingB2BOrder(String cmd, PageCtrl pc) throws Exception {
		try {
			DBRow[] result = new DBRow[0];
			
			if (cmd.equals("track_certificate")) {
				int certificate_day = systemConfig.getIntConfigValue("b2b_order_certificate_period"); // 订单的单证流程周期
				result = floorB2BOrderMgrZyj.getNeedTrackCertificateB2BOrder(certificate_day, pc);
			} else if (cmd.equals("track_clearance")) {
				int clearance_day = systemConfig.getIntConfigValue("b2b_order_clearance_period"); // 订单的进口清关流程周期
				result = floorB2BOrderMgrZyj.getNeedTrackClearanceB2BOrder(clearance_day, pc);
			} else if (cmd.equals("track_declaration")) {
				int declaration_day = systemConfig.getIntConfigValue("b2b_order_declaration_period"); // 订单的出口报送流程周期
				result = floorB2BOrderMgrZyj.getNeedTrackDeclarationB2BOrder(declaration_day, pc);
			}
			return result;
		} catch (Exception e) {
			throw new SystemException(e, "getNeedTrackReceiveB2BOrderByPsid", log);
		}
	}
	
	/**
	 * 获得全部订单
	 * 
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllB2BOrder(PageCtrl pc) throws Exception {
		
		try {
			return floorB2BOrderMgrZyj.getAllB2BOrder(pc);
		} catch (Exception e) {
			throw new SystemException(e, "getAllB2BOrder", log);
		}
	}
	
	/**
	 * 根据当前登录者的Id，获取其上次创建的订单的ID
	 * 
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public String getB2BOrderLastTimeCreateByAdid(HttpServletRequest request) throws Exception {
		try {
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			DBRow row = floorB2BOrderMgrZyj.getB2BOrderLastTimeCreateByAdid(adminLoginBean.getAdid());
			if (null == row) {
				return "";
			} else {
				return row.getString("b2b_oid");
			}
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderLastTimeCreateByAdid", log);
		}
	}
	
	/**
	 * 导入订单明细(返回系统文件名)
	 */
	public String importB2BOrderDetail(String fileName) throws Exception {
		try {
			DBRow[] fileDetails = this.excelshow(fileName);
			
			String[] type = fileName.split("\\.");
			String fileType = type[type.length - 1];
			
			String baseUrl = Environment.getHome() + "upl_imags_tmp/";
			String systemTimeFile = System.currentTimeMillis() + "." + fileType;
			FileUtil.moveFile(baseUrl + fileName, baseUrl + systemTimeFile);
			for (int i = 0; i < fileDetails.length; i++) {
				DBRow product = floorProductMgr.getDetailProductByPname(fileDetails[i].getString("p_name"));
				if (product == null) {
					product = new DBRow();
				}
				
				float deliver_count = Float.parseFloat(fileDetails[i].getString("prefill_deliver_count"));
				float backup_count = Float.parseFloat(fileDetails[i].getString("prefill_backup_count"));
				float count = deliver_count + backup_count;
				String box = fileDetails[i].getString("prefill_box");
				String serial_number = fileDetails[i].getString("serial_number");
				String lot_number = fileDetails[i].getString("lot_number");
				
				DBRow importB2BOrderDetail = new DBRow();
				importB2BOrderDetail.add("b2b_pc_id", product.get("pc_id", 0l));
				importB2BOrderDetail.add("b2b_delivery_count", deliver_count);
				importB2BOrderDetail.add("b2b_backup_count", backup_count);
				importB2BOrderDetail.add("b2b_count", count);
				importB2BOrderDetail.add("file_name", systemTimeFile);
				importB2BOrderDetail.add("import_p_name", fileDetails[i].getString("p_name"));
				importB2BOrderDetail.add("lot_number", lot_number);
				importB2BOrderDetail.add("line_number", i + 2);
				
				if (!box.equals("")) {
					importB2BOrderDetail.add("b2b_detail_box", box);
				}
				
				if (!serial_number.equals("")) {
					importB2BOrderDetail.add("b2b_detail_serial_number", serial_number);
				}
				
				floorB2BOrderMgrZyj.addB2BOrderImportDetail(importB2BOrderDetail);
			}
			
			return systemTimeFile;
		} catch (Exception e) {
			throw new SystemException(e, "importB2BOrderDetail", log);
		}
	}
	
	/**
	 * 将excel文件转换成DBRow[]
	 * 
	 * @param filename
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] excelshow(String filename) throws Exception {
		try {
			HashMap<String, String> filedName = new HashMap<String, String>();
			filedName.put("0", "p_name");
			filedName.put("1", "prefill_deliver_count");
			filedName.put("2", "prefill_backup_count");
			filedName.put("3", "lot_number");
			
			HashMap<String, String> pcidMap = new HashMap<String, String>();
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			String path = Environment.getHome() + "upl_imags_tmp/" + filename;
			
			InputStream is = new FileInputStream(path);
			Workbook rwb = Workbook.getWorkbook(is);
			
			Sheet rs = rwb.getSheet(0);
			int rsColumns = rs.getColumns(); // excel表字段数
			int rsRows = rs.getRows(); // excel表记录行数
			
			for (int i = 1; i < rsRows; i++) {
				DBRow pro = new DBRow();
				boolean result = false;// 判断是否纯空行数据
				for (int j = 0; j < rsColumns; j++) {
					if (j == 0) {
						DBRow product = floorProductMgr.getDetailProductByPname(rs.getCell(j, i).getContents().trim());
						if (product != null)// 商品查不到，跳过循环，不添加入数组
						{
							pcidMap.put(String.valueOf(product.get("pc_id", 0l)),
									String.valueOf(product.get("pc_id", 0l)));
							pro.add("pc_id", product.get("pc_id", 0l));
						}
					}
					
					pro.add(filedName.get(String.valueOf(j)), rs.getCell(j, i).getContents().trim());
					ArrayList proName = pro.getFieldNames();// DBRow 字段名
					for (int p = 0; p < proName.size(); p++) {
						if (!pro.getString(proName.get(p).toString()).trim().equals(""))// 去掉空格
						{
							result = true;
						}
					}
				}
				if (result)// 不是纯空行就加入到DBRow
				{
					al.add(pro);
				}
			}
			return (al.toArray(new DBRow[0]));
		} catch (Exception e) {
			throw new SystemException(e, "excelshow", log);
		}
	}
	
	/**
	 * 根据系统文件名返回错误
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] errorImportB2BOrderDetail(String fileName) throws Exception {
		try {
			DBRow[] errors = floorB2BOrderMgrZyj.errorImportB2BOrderDetail(fileName);
			
			ArrayList<DBRow> errorlist = new ArrayList<DBRow>();// 整理好的错误信息集合
			HashMap<String, String> map = new HashMap<String, String>();
			DBRow error = new DBRow();
			for (int i = 0; i < errors.length; i++) {
				if (map.containsKey(errors[i].getString("number")))// 如果同一行有多个错误信息，错误信息用，分割组合
				{
					String errorMessage = error.getString("errorMessage");
					errorMessage += "," + errors[i].getString("error_type");
					error.add("errorMessage", errorMessage);
				} else {
					map.put(errors[i].getString("line_number"), errors[i].getString("b2b_pc_id"));
					if (i > 0) {
						errorlist.add(error);
					}
					error = new DBRow();
					error.add("p_name", errors[i].getString("p_name"));
					error.add("b2b_delivery_count", errors[i].getString("b2b_delivery_count"));
					error.add("b2b_backup_count", errors[i].getString("b2b_backup_count"));
					error.add("b2b_detail_serial_number", errors[i].getString("b2b_detail_serial_number"));
					error.add("b2b_detail_box", errors[i].getString("b2b_detail_box"));
					error.add("lot_number", errors[i].getString("lot_number"));
					error.add("errorProductRow", errors[i].getString("number"));
					error.add("errorMessage", errors[i].getString("error_type"));
				}
				// 最后一条记录加入错误集合中
				if (i == errors.length - 1) {
					errorlist.add(error);
				}
			}
			return errorlist.toArray(new DBRow[0]);
		} catch (Exception e) {
			throw new SystemException(e, "errorImportB2BOrderDetail", log);
		}
	}
	
	/**
	 * 根据系统文件名返回全部信息
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] allImportB2BOrderDetail(String fileName) throws Exception {
		try {
			return floorB2BOrderMgrZyj.allImportB2BOrderDetail(fileName);
		} catch (Exception e) {
			throw new SystemException(e, "allImportB2BOrderDetail", log);
		}
	}
	
	/**
	 * 增加订单
	 * 
	 * @param request
	 * @throws Exception
	 */
	public long addB2BOrder(HttpServletRequest request) throws Exception {
		try {
			// 收货信息
			long receive_psid = StringUtil.getLong(request, "receive_psid");
			DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(receive_psid);
			long deliver_ccid = StringUtil.getLong(request, "deliver_ccid");
			long deliver_pro_id = StringUtil.getLong(request, "deliver_pro_id");
			String deliver_city = StringUtil.getString(request, "deliver_city");
			String deliver_house_number = StringUtil.getString(request, "deliver_house_number");
			String deliver_street = StringUtil.getString(request, "deliver_street");
			String deliver_zip_code = StringUtil.getString(request, "deliver_zip_code");
			String deliver_name = StringUtil.getString(request, "deliver_name");
			String deliver_linkman_phone = StringUtil.getString(request, "deliver_linkman_phone");
			String b2b_order_receive_date = StringUtil.getString(request, "b2b_order_receive_date");
			String address_state_deliver = StringUtil.getString(request, "address_state_deliver");
			
			// 提货地址
			long send_psid = StringUtil.getLong(request, "send_psid");
			long send_ccid = 0L;
			long send_pro_id = 0L;
			String send_city = null;
			String send_house_number = null;
			String send_street = null;
			String send_zip_code = null;
			String send_name = null;
			String send_linkman_phone = null;
			String b2b_order_out_date = null;
			String address_state_send = null;
			DBRow sendProductStorage = new DBRow();
			if (0 != send_psid) {
				sendProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
				send_ccid = StringUtil.getLong(request, "send_ccid");
				send_pro_id = StringUtil.getLong(request, "send_pro_id");
				send_city = StringUtil.getString(request, "send_city");
				send_house_number = StringUtil.getString(request, "send_house_number");
				send_street = StringUtil.getString(request, "send_street");
				send_zip_code = StringUtil.getString(request, "send_zip_code");
				send_name = StringUtil.getString(request, "send_name");
				send_linkman_phone = StringUtil.getString(request, "send_linkman_phone");
				b2b_order_out_date = StringUtil.getString(request, "b2b_order_out_date");
				address_state_send = StringUtil.getString(request, "address_state_send");
			}
			
			String remark = StringUtil.getString(request, "remark");
			long title_id = StringUtil.getLong(request, "title_id");
			// 运输设置
			long fr_id = StringUtil.getLong(request, "fr_id");
			String b2b_order_waybill_name = StringUtil.getString(request, "b2b_order_waybill_name");
			String b2b_order_waybill_number = StringUtil.getString(request, "b2b_order_waybill_number");
			int b2b_orderby = StringUtil.getInt(request, "b2b_orderby");
			String carriers = StringUtil.getString(request, "carriers");
			String b2b_order_send_place = StringUtil.getString(request, "b2b_order_send_place");
			String b2b_order_receive_place = StringUtil.getString(request, "b2b_order_receive_place");
			
			// 主流程
			String adminUserIdsB2BOrder = StringUtil.getString(request, "adminUserIdsB2BOrder");
			if (adminUserIdsB2BOrder.equals("")) {
				adminUserIdsB2BOrder = "0";
			}
			String adminUserNamesB2BOrder = StringUtil.getString(request, "adminUserNamesB2BOrder");
			int needMailB2BOrder = StringUtil.getInt(request, "needMailB2BOrder");
			int needMessageB2BOrder = StringUtil.getInt(request, "needMessageB2BOrder");
			int needPageB2BOrder = StringUtil.getInt(request, "needPageB2BOrder");
			
			// 出口报关
			int declaration = StringUtil.getInt(request, "declaration");
			String adminUserIdsDeclaration = StringUtil.getString(request, "adminUserIdsDeclaration");
			if (adminUserIdsDeclaration.equals("")) {
				adminUserIdsDeclaration = "0";
			}
			String adminUserNamesDeclaration = StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration = StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration = StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration = StringUtil.getInt(request, "needPageDeclaration");
			
			// 进口清关
			int clearance = StringUtil.getInt(request, "clearance");
			String adminUserIdsClearance = StringUtil.getString(request, "adminUserIdsClearance");
			if (adminUserIdsClearance.equals("")) {
				adminUserIdsClearance = "0";
			}
			String adminUserNamesClearance = StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance = StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance = StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance = StringUtil.getInt(request, "needPageClearance");
			
			// 商品图片
			String adminUserIdsProductFile = StringUtil.getString(request, "adminUserIdsProductFile");
			if (adminUserIdsProductFile.equals("")) {
				adminUserIdsProductFile = "0";
			}
			String adminUserNamesProductFile = StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile = StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile = StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile = StringUtil.getInt(request, "needPageProductFile");
			
			// 内部标签
			int tag = StringUtil.getInt(request, "tag");
			String adminUserIdsTag = StringUtil.getString(request, "adminUserIdsTag");
			if (adminUserIdsTag.equals("")) {
				adminUserIdsTag = "0";
			}
			String adminUserNamesTag = StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag = StringUtil.getInt(request, "needMailTag");
			int needMessageTag = StringUtil.getInt(request, "needMessageTag");
			int needPageTag = StringUtil.getInt(request, "needPageTag");
			
			// 第三方标签
			int tag_third = StringUtil.getInt(request, "tag_third");
			String adminUserIdsTagThird = StringUtil.getString(request, "adminUserIdsTagThird");
			if (adminUserIdsTagThird.equals("")) {
				adminUserIdsTagThird = "0";
			}
			String adminUserNamesTagThird = StringUtil.getString(request, "adminUserNamesTagThird");
			int needMailTagThird = StringUtil.getInt(request, "needMailTagThird");
			int needMessageTagThird = StringUtil.getInt(request, "needMessageTagThird");
			int needPageTagThird = StringUtil.getInt(request, "needPageTagThird");
			
			// 运费
			
			int stock_in_set = StringUtil.getInt(request, "stock_in_set");
			String adminUserIdsStockInSet = StringUtil.getString(request, "adminUserIdsStockInSet");
			if (adminUserIdsStockInSet.equals("")) {
				adminUserIdsStockInSet = "0";
			}
			String adminUserNamesStockInSet = StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet = StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet = StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet = StringUtil.getInt(request, "needPageStockInSet");
			
			// 单证
			int certificate = StringUtil.getInt(request, "certificate");
			String adminUserIdsCertificate = StringUtil.getString(request, "adminUserIdsCertificate");
			if (adminUserIdsCertificate.equals("")) {
				adminUserIdsCertificate = "0";
			}
			String adminUserNamesCertificate = StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate = StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate = StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate = StringUtil.getInt(request, "needPageCertificate");
			
			// 质检
			int quality_inspection = StringUtil.getInt(request, "quality_inspection");
			String adminUserIdsQualityInspection = StringUtil.getString(request, "adminUserIdsQualityInspection");
			if (adminUserIdsQualityInspection.equals("")) {
				adminUserIdsQualityInspection = "0";
			}
			String adminUserNamesQualityInspection = StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection = StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection = StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection = StringUtil.getInt(request, "needPageQualityInspection");
			
			DBRow b2BOrder = new DBRow();
			// 收货信息
			b2BOrder.add("receive_psid", receive_psid);
			b2BOrder.add("deliver_ccid", deliver_ccid);
			b2BOrder.add("deliver_pro_id", deliver_pro_id);
			b2BOrder.add("deliver_city", deliver_city);
			b2BOrder.add("deliver_house_number", deliver_house_number);
			b2BOrder.add("deliver_street", deliver_street);
			b2BOrder.add("deliver_zip_code", deliver_zip_code);
			b2BOrder.add("b2b_order_address", "");
			b2BOrder.add("b2b_order_linkman", deliver_name);
			b2BOrder.add("b2b_order_linkman_phone", deliver_linkman_phone);
			if (1 == receiveProductStorage.get("storage_type", 0) && !"".equals(b2b_order_receive_date)) {
				b2BOrder.add("b2b_order_receive_date", b2b_order_receive_date);
			} else {
				b2BOrder.add("b2b_order_receive_date", null);
			}
			b2BOrder.add("address_state_deliver", address_state_deliver);
			
			// 提货信息
			if (0 != send_psid) {
				b2BOrder.add("send_psid", send_psid);
				b2BOrder.add("send_city", send_city);
				b2BOrder.add("send_house_number", send_house_number);
				b2BOrder.add("send_street", send_street);
				b2BOrder.add("send_ccid", send_ccid);
				b2BOrder.add("send_pro_id", send_pro_id);
				b2BOrder.add("send_zip_code", send_zip_code);
				b2BOrder.add("send_name", send_name);
				b2BOrder.add("send_linkman_phone", send_linkman_phone);
				if (1 == sendProductStorage.get("storage_type", 0) && !"".equals(b2b_order_out_date)) {
					b2BOrder.add("b2b_order_out_date", b2b_order_out_date);
				} else {
					b2BOrder.add("b2b_order_out_date", null);
				}
				b2BOrder.add("address_state_send", address_state_send);
				b2BOrder.add("from_ps_type", sendProductStorage.get("storage_type", 0));
			} else {
				b2BOrder.add("send_psid", 0);
			}
			
			// 其他信息(ETA,备注)
			b2BOrder.add("remark", remark);
			b2BOrder.add("title_id", title_id);
			
			long logingUserId = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			String loginUserName = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name();
			
			// 主流程信息
			b2BOrder.add("b2b_order_status", B2BOrderKey.READY);// 创建的订单都是准备中
			b2BOrder.add("b2b_order_date", DateUtil.NowStr());
			b2BOrder.add("create_account_id", logingUserId);// 订单的创建者
			b2BOrder.add("create_account", loginUserName);// 订单的创建者
			b2BOrder.add("updatedate", DateUtil.NowStr());
			b2BOrder.add("updateby", logingUserId);// 订单的创建者
			b2BOrder.add("updatename", loginUserName);// 订单的创建者
			
			// 提货与收货仓库类型
			b2BOrder.add("target_ps_type", receiveProductStorage.get("storage_type", 0));
			
			// 运输设置
			b2BOrder.add("fr_id", fr_id);
			b2BOrder.add("b2b_order_waybill_name", b2b_order_waybill_name);
			b2BOrder.add("b2b_order_waybill_number", b2b_order_waybill_number);
			b2BOrder.add("b2b_orderby", b2b_orderby);
			b2BOrder.add("carriers", carriers);
			b2BOrder.add("b2b_order_send_place", b2b_order_send_place);
			b2BOrder.add("b2b_order_receive_place", b2b_order_receive_place);
			
			// 流程信息
			b2BOrder.add("declaration", declaration);
			b2BOrder.add("clearance", clearance);
			b2BOrder.add("product_file", B2BOrderProductFileKey.PRODUCTFILE);
			b2BOrder.add("tag", tag);
			b2BOrder.add("tag_third", tag_third);
			b2BOrder.add("stock_in_set", stock_in_set);
			b2BOrder.add("certificate", certificate);
			b2BOrder.add("quality_inspection", quality_inspection);
			
			long b2b_oid = floorB2BOrderMgrZyj.addB2BOrder(b2BOrder);
			
			// 添加订单详细
			this.addB2BOrderItem(b2b_oid, request);
			this.insertLogsAndUpdateOrder(b2b_oid, "创建订单:单号" + b2b_oid,
					(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid(), (new AdminMgr())
							.getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name(),
					B2BOrderLogTypeKey.Goods, b2b_order_receive_date, B2BOrderKey.READY);
			this.b2bOrderSetFreight(b2b_oid, B2BOrderKey.READY, request);
			
			// 添加订单索引
			this.editB2BOrderIndex(b2b_oid, "add");
			
			// 订单发送邮件的内容不同，需要区分是供应商还是仓库，组织内容时需要传采购单的Id，订单传0
			addSculde(b2b_oid, sendProductStorage.getString("title"), receiveProductStorage.getString("title"),
					b2b_order_receive_date, logingUserId, loginUserName, request, adminUserIdsB2BOrder,
					adminUserNamesB2BOrder, needMailB2BOrder, needMessageB2BOrder, needPageB2BOrder, declaration,
					adminUserNamesDeclaration, adminUserIdsDeclaration, needPageDeclaration, needMailDeclaration,
					needMessageDeclaration, clearance, adminUserNamesClearance, adminUserIdsClearance,
					needPageClearance, needMailClearance, needMessageClearance, B2BOrderProductFileKey.PRODUCTFILE,
					adminUserNamesProductFile, adminUserIdsProductFile, needPageProductFile, needMailProductFile,
					needMessageProductFile, tag, adminUserNamesTag, adminUserIdsTag, needPageTag, needMailTag,
					needMessageTag, stock_in_set, adminUserNamesStockInSet, adminUserIdsStockInSet, needPageStockInSet,
					needMailStockInSet, needMessageStockInSet, certificate, adminUserNamesCertificate,
					adminUserIdsCertificate, needPageCertificate, needMailCertificate, needMessageCertificate,
					quality_inspection, adminUserNamesQualityInspection, adminUserIdsQualityInspection,
					needPageQualityInspection, needMailQualityInspection, needMessageQualityInspection, tag_third,
					adminUserIdsTagThird, adminUserNamesTagThird, needMailTagThird, needMessageTagThird,
					needPageTagThird);
			
			return b2b_oid;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 保存导入订单商品
	 * 
	 * @param purchase_id
	 * @param b2b_oid
	 * @param request
	 * @throws Exception
	 */
	private void addB2BOrderItem(long b2b_oid, HttpServletRequest request) throws Exception {
		try {
			DBRow b2bOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			long receive_psid = b2bOrder.get("receive_psid", 0L);
			long title_id = b2bOrder.get("title_id", 0L);
			
			floorB2BOrderMgrZyj.delB2BOrderItemByB2BOrderId(b2b_oid);// 批量删除订单明细
			
			String import_name = StringUtil.getString(request, "import_name");
			DBRow[] importB2BOrderItems = floorB2BOrderMgrZyj.getImportB2BOrderItem(import_name);
			
			for (int i = 0; i < importB2BOrderItems.length; i++) {
				long pc_id = importB2BOrderItems[i].get("b2b_pc_id", 0l);
				
				int deliver_count = (int) importB2BOrderItems[i].get("b2b_delivery_count", 0f);
				int backup_count = (int) importB2BOrderItems[i].get("b2b_backup_count", 0f);
				String box = importB2BOrderItems[i].getString("b2b_detail_box");
				String lot_number = importB2BOrderItems[i].getString("lot_number");
				
				if ("".equals(lot_number)) {
					lot_number = String.valueOf(b2b_oid);
				}
				int count = deliver_count + backup_count;
				String serial_number = importB2BOrderItems[i].getString("b2b_detail_serial_number");
				
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				
				double send_price = product.get("unit_price", 0d);
				
				float union_count = 0;
				float normal_count = 0;
				
				if (product.get("union_flag", 0) == 1) {
					union_count = count;
				} else {
					normal_count = count;
				}
				
				DBRow b2bOrderItem = new DBRow();
				b2bOrderItem.add("b2b_pc_id", pc_id);
				b2bOrderItem.add("b2b_delivery_count", deliver_count);
				b2bOrderItem.add("b2b_backup_count", backup_count);
				b2bOrderItem.add("b2b_wait_count", count);
				b2bOrderItem.add("b2b_count", count);
				b2bOrderItem.add("b2b_oid", b2b_oid);
				b2bOrderItem.add("b2b_p_name", product.getString("p_name"));
				b2bOrderItem.add("b2b_p_code", product.getString("p_code"));
				b2bOrderItem.add("b2b_volume", product.getString("volume"));
				b2bOrderItem.add("b2b_weight", product.getString("weight"));
				// b2bOrderItem.add("b2b_box",box);
				b2bOrderItem.add("lot_number", lot_number);
				
				b2bOrderItem.add("b2b_send_price", send_price);
				b2bOrderItem.add("b2b_union_count", union_count);
				b2bOrderItem.add("b2b_normal_count", normal_count);
				if (!serial_number.equals("")) {
					// b2bOrderItem.add("b2b_product_serial_number",serial_number);
				}
				
				long clp_type_id = 0;
				long blp_type_id = 0;
				DBRow[] clps = lpTypeMgrZJ.getCLPTypeForSkuToShip(pc_id, title_id, receive_psid);
				if (clps.length > 0) {
					clp_type_id = clps[0].get("sku_lp_type_id", 0l);
					blp_type_id = clps[0].get("sku_lp_box_type", 0l);
					
				} else {
					DBRow[] blps = lpTypeMgrZJ.getBLPTypeForSkuToShipNotNull(pc_id, receive_psid);
					if (blps.length > 0) {
						blp_type_id = blps[0].get("box_type_id", 0l);
					}
					
				}
				
				b2bOrderItem.add("clp_type_id", clp_type_id);
				b2bOrderItem.add("blp_type_id", blp_type_id);
				
				floorB2BOrderMgrZyj.addB2BOrderItem(b2bOrderItem);
			}
		} catch (Exception e) {
			throw new SystemException(e, "addB2BOrderItem(long b2b_oid,HttpServletRequest request)", log);
		}
	}
	
	/**
	 * 操作订单索引
	 * 
	 * @param b2b_oid 订单号
	 * @param type 操作类型(add,update)
	 * @throws Exception
	 */
	public void editB2BOrderIndex(long b2b_oid, String type) throws Exception {
		DBRow oldB2BOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
		
		String sendStorage = "";
		DBRow sendProductStorageCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(oldB2BOrder.get(
				"send_psid", 0l));
		if (null != sendProductStorageCatalog) {
			sendStorage = sendProductStorageCatalog.getString("title");
		}
		
		DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(oldB2BOrder.get(
				"receive_psid", 0l));
		
		if (type.equals("add")) {
			B2BOrderIndexMgr.getInstance().addIndex(b2b_oid, sendStorage, receiveProductStorage.getString("title"),
					oldB2BOrder.getString("b2b_order_waybill_number"), oldB2BOrder.getString("b2b_order_waybill_name"),
					oldB2BOrder.getString("carriers"));
		} else if (type.equals("update")) {
			B2BOrderIndexMgr.getInstance().updateIndex(b2b_oid, sendStorage,
					receiveProductStorage != null ? receiveProductStorage.getString("title") : "",
					oldB2BOrder.getString("b2b_order_waybill_number"), oldB2BOrder.getString("b2b_order_waybill_name"),
					oldB2BOrder.getString("carriers"));
		} else if (type.equals("del")) {
			B2BOrderIndexMgr.getInstance().deleteIndex(b2b_oid);
		}
	}
	
	/**
	 * 订单添加任务
	 * 
	 * @throws Exception
	 */
	private void addSculde(long b2b_oid, String cata_name_send, String cata_name_deliver, String eta,
			long logingUserId, String loginUserName, HttpServletRequest request, String adminUserIdsTransport,
			String adminUserNamesTransport, int needMailTransport, int needMessageTransport, int needPageTransport,
			int declaration, String adminUserNamesDeclaration, String adminUserIdsDeclaration, int needPageDeclaration,
			int needMailDeclaration, int needMessageDeclaration, int clearance, String adminUserNamesClearance,
			String adminUserIdsClearance, int needPageClearance, int needMailClearance, int needMessageClearance,
			int product_file, String adminUserNamesProductFile, String adminUserIdsProductFile,
			int needPageProductFile, int needMailProductFile, int needMessageProductFile, int tag,
			String adminUserNamesTag, String adminUserIdsTag, int needPageTag, int needMailTag, int needMessageTag,
			int stock_in_set, String adminUserNamesStockInSet, String adminUserIdsStockInSet, int needPageStockInSet,
			int needMailStockInSet, int needMessageStockInSet, int certificate, String adminUserNamesCertificate,
			String adminUserIdsCertificate, int needPageCertificate, int needMailCertificate,
			int needMessageCertificate, int quality_inspection, String adminUserNamesQualityInspection,
			String adminUserIdsQualityInspection, int needPageQualityInspection, int needMailQualityInspection,
			int needMessageQualityInspection, int tag_third, String adminUserIdsTagThird,
			String adminUserNamesTagThird, int needMailTagThird, int needMessageTagThird, int needPageTagThird)
			throws Exception {
		String sendContent = this.handleSendContent(b2b_oid, declaration, adminUserNamesDeclaration, clearance,
				adminUserNamesClearance, product_file, adminUserNamesProductFile, tag, adminUserNamesTag, stock_in_set,
				adminUserNamesStockInSet, certificate, adminUserNamesCertificate, quality_inspection,
				adminUserNamesQualityInspection, cata_name_send, cata_name_deliver, eta);
		
		// 订单主流程任务
		this.addB2BOrderScheduleOneProdure(DateUtil.NowStr(), "", logingUserId, adminUserIdsTransport, "", b2b_oid,
				ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[主流程]", sendContent, needPageTransport, needMailTransport,
				needMessageTransport, request, true, 4, B2BOrderKey.READY, loginUserName, "创建订单" + b2b_oid + "[备货中]");
		// 报关
		if (B2BOrderDeclarationKey.DELARATION == declaration) {
			this.addB2BOrderScheduleOneProdure("", "", logingUserId, adminUserIdsDeclaration, "", b2b_oid,
					ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[报关流程]", sendContent, needPageDeclaration,
					needMailDeclaration, needMessageDeclaration, request, false, 7, B2BOrderDeclarationKey.DELARATION,
					loginUserName, "创建订单" + b2b_oid + "[需要报关]");
		}
		
		// 清关
		if (B2BOrderClearanceKey.CLEARANCE == clearance) {
			this.addB2BOrderScheduleOneProdure("", "", logingUserId, adminUserIdsClearance, "", b2b_oid,
					ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[清关流程]", sendContent, needPageClearance, needMailClearance,
					needMessageClearance, request, false, 6, B2BOrderClearanceKey.CLEARANCE, loginUserName, "创建订单"
							+ b2b_oid + "[需要清关]");
		}
		// 商品图片
		if (B2BOrderProductFileKey.PRODUCTFILE == product_file) {
			TDate tdateProductFile = new TDate();
			tdateProductFile
					.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_product_file_period")));
			String endTimeProductFile = tdateProductFile.formatDate("yyyy-MM-dd HH:mm:ss");
			
			this.addB2BOrderScheduleOneProdure(DateUtil.NowStr(), endTimeProductFile, logingUserId,
					adminUserIdsProductFile, "", b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[商品图片流程]",
					sendContent, needPageProductFile, needMailProductFile, needMessageProductFile, request, true, 10,
					B2BOrderProductFileKey.PRODUCTFILE, loginUserName, "创建订单" + b2b_oid + "[商品文件上传中]");
		}
		// 内部标签
		if (B2BOrderTagKey.TAG == tag) {
			TDate tdateTag = new TDate();
			tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_tag_period")));
			String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
			// 添加
			this.addB2BOrderScheduleOneProdure(DateUtil.NowStr(), endTimeTag, logingUserId, adminUserIdsTag, "",
					b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[内部标签流程]", sendContent, needPageTag, needMailTag,
					needMessageTag, request, true, 8, B2BOrderTagKey.TAG, loginUserName, "创建订单" + b2b_oid + "[制签中]");
		}
		
		// 第三方标签
		if (B2BOrderTagKey.TAG == tag_third) {
			TDate tdateTagThird = new TDate();
			tdateTagThird.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_third_tag_period")));
			String endTimeTagThird = tdateTagThird.formatDate("yyyy-MM-dd HH:mm:ss");
			// 添加
			this.addB2BOrderScheduleOneProdure(DateUtil.NowStr(), endTimeTagThird, logingUserId, adminUserIdsTagThird,
					"", b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[第三方标签流程]", sendContent, needPageTagThird,
					needMailTagThird, needMessageTagThird, request, true, B2BOrderLogTypeKey.THIRD_TAG,
					B2BOrderTagKey.TAG, loginUserName, "创建订单" + b2b_oid + "[制签中]");
		}
		
		// 运费
		if (B2BOrderStockInSetKey.SHIPPINGFEE_SET == stock_in_set) {
			// 添加
			this.addB2BOrderScheduleOneProdure("", "", logingUserId, adminUserIdsStockInSet, "", b2b_oid,
					ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[运费流程]", sendContent, needPageStockInSet,
					needMailStockInSet, needMessageStockInSet, request, false, 5,
					B2BOrderStockInSetKey.SHIPPINGFEE_SET, loginUserName, "创建订单" + b2b_oid + "[需要运费]");
		}
		
		// 单证
		if (B2BOrderCertificateKey.CERTIFICATE == certificate) {
			TDate tdateCertificate = new TDate();
			tdateCertificate
					.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_certificate_period")));
			String endTimeCertificate = tdateCertificate.formatDate("yyyy-MM-dd HH:mm:ss");
			this.addB2BOrderScheduleOneProdure(DateUtil.NowStr(), endTimeCertificate, logingUserId,
					adminUserIdsCertificate, "", b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[单证流程]", sendContent,
					needPageCertificate, needMailCertificate, needMessageCertificate, request, true, 9,
					B2BOrderCertificateKey.CERTIFICATE, loginUserName, "创建订单" + b2b_oid + "[单证采集中]");
		}
		
		// 质检
		if (B2BOrderQualityInspectionKey.NEED_QUALITY == quality_inspection) {
			TDate tdateQuality = new TDate();
			tdateQuality.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_quality_inspection")));
			String endTimeQuality = tdateQuality.formatDate("yyyy-MM-dd HH:mm:ss");
			this.addB2BOrderScheduleOneProdure(DateUtil.NowStr(), endTimeQuality, logingUserId,
					adminUserIdsQualityInspection, "", b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[质检流程]",
					sendContent, needPageQualityInspection, needMailQualityInspection, needMessageQualityInspection,
					request, true, 11, B2BOrderQualityInspectionKey.NEED_QUALITY, loginUserName, "创建订单" + b2b_oid
							+ "[需要质检]");
		}
	}
	
	/**
	 * 构成发送短信和邮件的内容
	 * 
	 * @param b2b_oid
	 * @param declaration
	 * @param declarationPerson
	 * @param clearance
	 * @param clearancePerson
	 * @param productFile
	 * @param productPerson
	 * @param tag
	 * @param tagPerson
	 * @param stockInSet
	 * @param stockPerson
	 * @param certificate
	 * @param certificatePerson
	 * @param quality
	 * @param qualityPerson
	 * @param cata_name_send 提货仓库名称（订单为仓库名）
	 * @param cata_name_deliver 收货仓库名称
	 * @param eta
	 * @return
	 * @throws Exception
	 */
	public String handleSendContent(long b2b_oid, int declaration, String declarationPerson, int clearance,
			String clearancePerson, int productFile, String productPerson, int tag, String tagPerson, int stockInSet,
			String stockPerson, int certificate, String certificatePerson, int quality, String qualityPerson,
			String cata_name_send, String cata_name_deliver, String eta) throws Exception {
		try {
			String rnStr = "";
			
			// 获取提货仓库的名字
			if (!StringUtil.isBlank(cata_name_send)) {
				rnStr += "提货仓库:" + cata_name_send + ",";
			}
			// 获取收货仓库的名字
			rnStr += "收货仓库:" + cata_name_deliver + ",";
			// 获取eta
			if (!"".equals(eta)) {
				rnStr += "预计到货时间:" + eta + ",";
			}
			// 订单货物的总体积，如果 有值，添加，此处待完善
			rnStr += "总体积:" + this.getB2BOrderVolume(b2b_oid);
			
			return rnStr;
		} catch (Exception e) {
			throw new SystemException(e, "handleSendContent", log);
		}
	}
	
	/**
	 * 获得订单的体积
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public float getB2BOrderVolume(long b2b_oid) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getB2BOrderVolume(b2b_oid);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderVolume", log);
		}
	}
	
	/**
	 * 添加日志并更新主单据update人及时间
	 * 
	 * @param b2b_oid
	 * @param b2b_content
	 * @param adid
	 * @param employe_name
	 * @param b2b_type
	 * @param eta
	 * @param activity
	 * @throws Exception
	 */
	private void insertLogsAndUpdateOrder(long b2b_oid, String b2b_content, long adid, String employe_name,
			int b2b_type, String eta, int activity) throws Exception {
		try {
			
			TDate tDate = new TDate();
			String currentTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
			DBRow row = new DBRow();
			row.add("b2b_oid", b2b_oid);
			row.add("b2b_content", b2b_content);
			row.add("b2ber_id", adid);
			row.add("b2ber", employe_name);
			row.add("b2b_type", b2b_type);
			row.add("activity_id", activity);
			if (eta != null && eta.trim().length() > 0) {
				row.add("time_complete", eta);
			}
			row.add("b2b_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			floorB2BOrderMgrZyj.addB2BOrderLog(row);
			
			// 同时更新 主信息上的 updatedate,updateby(id),updatename。
			DBRow b2BOrderRow = new DBRow();
			b2BOrderRow.add("updatedate", currentTime);
			b2BOrderRow.add("updateby", adid);
			b2BOrderRow.add("updatename", employe_name);
			floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, b2BOrderRow);
			
		} catch (Exception e) {
			throw new SystemException(e, "insertLogs", log);
		}
	}
	
	/**
	 * 订单设置运单信息
	 * 
	 * @param b2b_oid
	 * @param b2b_order_status
	 * @param request
	 * @throws Exception
	 */
	public void b2bOrderSetFreight(long b2b_oid, int b2b_order_status, HttpServletRequest request) throws Exception {
		String[] tfc_ids = request.getParameterValues("tfc_id");
		String[] tfc_project_names = request.getParameterValues("tfc_project_name");
		String[] tfc_ways = request.getParameterValues("tfc_way");
		String[] tfc_units = request.getParameterValues("tfc_unit");
		String[] tfc_unit_prices = request.getParameterValues("tfc_unit_price");
		String[] tfc_unit_counts = request.getParameterValues("tfc_unit_count");
		String[] tfc_currency = request.getParameterValues("tfc_currency");
		String[] tfc_exchange_rate = request.getParameterValues("tfc_exchange_rate");
		int stock_in_set = StringUtil.getInt(request, "stock_in_set");
		int changed = StringUtil.getInt(request, "changed", 0);
		
		if (changed == 2) {
			floorB2BOrderMgrZyj.deleteB2BOrderFreghtCostByB2BOrderId(b2b_oid);
		}
		
		for (int i = 0; tfc_ids != null && i < tfc_ids.length; i++) {
			DBRow row = new DBRow();
			row.add("tfc_id", tfc_ids[i]);
			row.add("b2b_oid", b2b_oid);
			row.add("tfc_project_name", tfc_project_names[i]);
			row.add("tfc_way", tfc_ways[i]);
			row.add("tfc_unit", tfc_units[i]);
			row.add("tfc_unit_price", tfc_unit_prices[i]);
			row.add("tfc_unit_count", tfc_unit_counts[i]);
			row.add("tfc_currency", tfc_currency[i]);
			row.add("tfc_exchange_rate", tfc_exchange_rate[i]);
			
			if (changed == 2 || row.getString("tfc_id").equals("") || row.getString("tfc_id").equals("0")) {
				row.remove("tfc_id");
				floorB2BOrderMgrZyj.addB2BOrderFreghtCost(row);
			} else {
				floorB2BOrderMgrZyj.addB2BOrderFreghtCost(row);
			}
		}
		
		AdminLoginBean adminLoginBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
		
		this.insertLogsAndUpdateOrder(b2b_oid, "订单设置运费:单号" + b2b_oid, adminLoginBean.getAdid(),
				adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.Freight, "", b2b_order_status);
		
	}
	
	/**
	 * 获得订单的总重量
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public float getB2BOrderWeight(long b2b_oid) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getB2BOrderWeight(b2b_oid);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderWeight", log);
		}
	}
	
	/**
	 * 获得所有订单出库金额
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public double getB2BOrderSendPrice(long b2b_oid) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getB2BOrderSendPrice(b2b_oid);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderSendPrice", log);
		}
	}
	
	/**
	 * 通过ID获取订单详细信息
	 */
	public DBRow getDetailB2BOrderById(long b2b_oid) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
		} catch (Exception e) {
			throw new SystemException(e, "getDetailB2BOrderById", log);
		}
	}
	
	/**
	 * 通过订单ID，结果集的数量查询日志
	 */
	public DBRow[] getB2BOrderLogs(long b2b_oid, int number) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getB2BOrderLogs(b2b_oid, number);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderLogs", log);
		}
	}
	
	/**
	 * 通过订单ID获取订单明细
	 */
	public DBRow[] getB2BOrderItemByB2BOrderId(long b2b_oid, PageCtrl pc, String sidx, String sord,
			FilterBean fillterBean) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getB2BOrderItemByB2BOrderId(b2b_oid, pc, sidx, sord, fillterBean);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderItemByB2BOrderId", log);
		}
	}
	
	/**
	 * 通过订单ID获得可能需要运费总额
	 */
	public DBRow getSumB2BOrderFreightCost(String b2b_oid) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getSumB2BOrderFreightCost(Long.parseLong(b2b_oid));
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderItemByB2BOrderId", log);
		}
	}
	
	/**
	 * 通过订单ID获取所有运费项目
	 */
	public DBRow[] findB2BOrderFreightCostByB2BOrderId(long b2b_oid) throws Exception {
		try {
			return floorB2BOrderMgrZyj.findB2BOrderFreightCostByB2BOrderId(b2b_oid);
		} catch (Exception e) {
			throw new SystemException(e, "findB2BOrderFreightCostByB2BOrderId", log);
		}
	}
	
	/**
	 * 根据订单ID，日志类型数组，获取日志
	 */
	public DBRow[] getB2BOrderLogsByB2BOrderIdAndType(long b2b_oid, int[] types) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getB2BOrderLogsByB2BOrderIdAndType(b2b_oid, types);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderLogsByB2BOrderIdAndType", log);
		}
	}
	
	/**
	 * 更新订单
	 */
	public long updateB2BOrderBasic(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			
			DBRow oldB2BOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			
			int isOutter = StringUtil.getInt(request, "isOutter");
			
			long receive_psid = StringUtil.getLong(request, "re_psid");
			
			DBRow receiveProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(receive_psid);
			
			long deliver_ccid = StringUtil.getLong(request, "deliver_ccid");
			long deliver_pro_id = StringUtil.getLong(request, "deliver_pro_id");
			String deliver_city = StringUtil.getString(request, "deliver_city");
			String deliver_house_number = StringUtil.getString(request, "deliver_house_number");
			String deliver_street = StringUtil.getString(request, "deliver_street");
			String deliver_zip_code = StringUtil.getString(request, "deliver_zip_code");
			String deliver_name = StringUtil.getString(request, "deliver_name");
			String deliver_linkman_phone = StringUtil.getString(request, "deliver_linkman_phone");
			String b2b_order_receive_date = StringUtil.getString(request, "b2b_order_receive_date");
			String address_state_deliver = StringUtil.getString(request, "address_state_deliver");
			
			long send_psid = StringUtil.getLong(request, "send_psid");
			DBRow sendProductStorage = new DBRow();
			long send_ccid = 0L;
			long send_pro_id = 0L;
			String send_city = "";
			String send_house_number = "";
			String send_street = "";
			String send_zip_code = "";
			String send_name = "";
			String send_linkman_phone = "";
			String b2b_order_out_date = "";
			String address_state_send = "";
			if (0 != send_psid) {
				sendProductStorage = floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
				send_ccid = StringUtil.getLong(request, "send_ccid");
				send_pro_id = StringUtil.getLong(request, "send_pro_id");
				send_city = StringUtil.getString(request, "send_city");
				send_house_number = StringUtil.getString(request, "send_house_number");
				send_street = StringUtil.getString(request, "send_street");
				send_zip_code = StringUtil.getString(request, "send_zip_code");
				send_name = StringUtil.getString(request, "send_name");
				send_linkman_phone = StringUtil.getString(request, "send_linkman_phone");
				b2b_order_out_date = StringUtil.getString(request, "b2b_order_out_date");
				address_state_send = StringUtil.getString(request, "address_state_send");
			}
			
			String remark = StringUtil.getString(request, "remark");
			long title_id = StringUtil.getLong(request, "title_id");
			
			DBRow b2BOrderRow = new DBRow();
			b2BOrderRow.add("receive_psid", receive_psid);
			b2BOrderRow.add("deliver_ccid", deliver_ccid);
			b2BOrderRow.add("deliver_pro_id", deliver_pro_id);
			b2BOrderRow.add("deliver_city", deliver_city);
			b2BOrderRow.add("deliver_house_number", deliver_house_number);
			b2BOrderRow.add("deliver_street", deliver_street);
			b2BOrderRow.add("deliver_zip_code", deliver_zip_code);
			b2BOrderRow.add("address_state_deliver", address_state_deliver);
			if (receiveProductStorage != null && 1 == receiveProductStorage.get("storage_type", 0)
					&& !"".equals(b2b_order_receive_date)) {
				b2BOrderRow.add("b2b_order_receive_date", b2b_order_receive_date);
			} else {
				b2BOrderRow.add("b2b_order_receive_date", null);
			}
			
			if (oldB2BOrder.get("b2b_order_status", 0) == B2BOrderKey.READY) {
				
				b2BOrderRow.add("send_psid", send_psid);
				b2BOrderRow.add("send_city", send_city);
				b2BOrderRow.add("send_house_number", send_house_number);
				b2BOrderRow.add("send_street", send_street);
				b2BOrderRow.add("send_ccid", send_ccid);
				b2BOrderRow.add("send_pro_id", send_pro_id);
				b2BOrderRow.add("send_zip_code", send_zip_code);
				b2BOrderRow.add("send_name", send_name);
				b2BOrderRow.add("send_linkman_phone", send_linkman_phone);
				b2BOrderRow.add("address_state_send", address_state_send);
				b2BOrderRow.add("from_ps_type", sendProductStorage.get("storage_type", 0));
				if (1 == sendProductStorage.get("storage_type", 0) && !"".equals(b2b_order_out_date)) {
					b2BOrderRow.add("b2b_order_out_date", b2b_order_out_date);
				} else {
					b2BOrderRow.add("b2b_order_out_date", null);
				}
			}
			
			if (isOutter != 2) {
				b2BOrderRow.add("create_account_id", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request))
						.getAdid());// 订单的创建者
				b2BOrderRow.add("create_account", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request))
						.getEmploye_name());// 订单的创建者
			}
			
			b2BOrderRow.add("b2b_order_address", "");
			b2BOrderRow.add("b2b_order_linkman", deliver_name);
			b2BOrderRow.add("b2b_order_linkman_phone", deliver_linkman_phone);
			b2BOrderRow.add("remark", remark);
			b2BOrderRow.add("title_id", title_id);
			
			b2BOrderRow.add("b2b_order_date", DateUtil.NowStr());
			
			b2BOrderRow.add("updatedate", DateUtil.NowStr());
			b2BOrderRow.add("updateby", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid());// 订单的创建者
			b2BOrderRow.add("updatename", (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request))
					.getEmploye_name());// 订单的创建者
			if (receiveProductStorage != null) {
				b2BOrderRow.add("target_ps_type", receiveProductStorage.get("storage_type", 0));
			}
			
			floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, b2BOrderRow);
			this.insertLogsAndUpdateOrder(b2b_oid, "修改订单:单号" + b2b_oid,
					(new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid(), (new AdminMgr())
							.getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name(),
					B2BOrderLogTypeKey.Update, "", oldB2BOrder.get("b2b_order_status", 0));// 类型1跟进记录
			
			// 修改订单索引
			this.editB2BOrderIndex(b2b_oid, "update");
			
			return b2b_oid;
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderBasic", log);
		}
	}
	
	/**
	 * 上传文件保存订单明细
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void saveB2BOrderItem(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = Long.parseLong(StringUtil.getString(request, "b2b_oid"));
			
			DBRow b2BOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			
			this.addB2BOrderItem(b2b_oid, request);
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogsAndUpdateOrder(b2b_oid, "订单导入:", adminLoginBean.getAdid(), adminLoginBean.getEmploye_name(),
					B2BOrderLogTypeKey.Update, "", b2BOrder.get("b2b_order_status", 0));
		} catch (ProductNotExistException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "saveB2BOrderItem", log);
		}
	}
	
	/**
	 * 修改订单明细
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void modB2BOrderItem(HttpServletRequest request) throws Exception {
		try {
			long b2b_order_item_id = StringUtil.getLong(request, "id");// grid默认参数是ID
			
			DBRow b2BOrderItem = floorB2BOrderMgrZyj.findB2BOrderItemByItemId(b2b_order_item_id);
			long b2b_oid = b2BOrderItem.get("b2b_oid", 0l);
			DBRow product = floorProductMgr.getDetailProductByPcid(b2BOrderItem.get("b2b_pc_id", 0l));
			
			Map parameter = request.getParameterMap();
			float b2b_delivery_count = b2BOrderItem.get("b2b_delivery_count", 0f);
			float b2b_backup_count = b2BOrderItem.get("b2b_backup_count", 0f);
			
			String b2b_box;
			String lot_number;
			String b2b_order_p_name;
			DBRow para = new DBRow();
			
			if (parameter.containsKey("b2b_delivery_count")) {
				b2b_delivery_count = StringUtil.getFloat(request, "b2b_delivery_count");
				para.add("b2b_delivery_count", b2b_delivery_count);
			}
			
			if (parameter.containsKey("b2b_backup_count")) {
				b2b_backup_count = StringUtil.getFloat(request, "b2b_backup_count");
				para.add("b2b_backup_count", b2b_backup_count);
			}
			
			if (parameter.containsKey("b2b_box")) {
				b2b_box = StringUtil.getString(request, "b2b_box");
				para.add("b2b_box", b2b_box);
			}
			if (parameter.containsKey("lot_number")) {
				lot_number = StringUtil.getString(request, "lot_number");
				DBRow[] b2BOrderItemsByOidPcId = floorB2BOrderMgrZyj.getB2BOrderItemByB2BOidPcId(b2b_oid,
						product.get("pc_id", 0l));
				if (b2BOrderItemsByOidPcId.length > 0) {
					for (int i = 0; i < b2BOrderItemsByOidPcId.length; i++) {
						if (b2BOrderItemsByOidPcId[i].getString("blp_type_id").equals(
								b2BOrderItem.getString("blp_type_id"))
								&& !b2BOrderItemsByOidPcId[i].getString("lot_number").equals(lot_number)
								&& b2b_order_item_id != b2BOrderItemsByOidPcId[i].get("b2b_detail_id", 0L)) {
							throw new B2BOrderDetailSamePcDiffLotNumberException();
						}
					}
				}
				para.add("lot_number", lot_number);
			}
			if (parameter.containsKey("p_name")) {
				b2b_order_p_name = StringUtil.getString(request, "p_name");
				
				product = floorProductMgr.getDetailProductByPname(b2b_order_p_name);
				if (product == null) {
					throw new ProductNotExistException();
				}
				DBRow[] b2BOrderItemsByOidPcId = floorB2BOrderMgrZyj.getB2BOrderItemByB2BOidPcId(b2b_oid,
						product.get("pc_id", 0l));
				if (b2BOrderItemsByOidPcId.length > 0) {
					for (int i = 0; i < b2BOrderItemsByOidPcId.length; i++) {
						if (b2BOrderItemsByOidPcId[i].getString("blp_type_id").equals(
								b2BOrderItem.getString("blp_type_id"))
								&& !b2BOrderItemsByOidPcId[i].getString("lot_number").equals(
										b2BOrderItem.getString("lot_number"))
								&& b2b_order_item_id != b2BOrderItemsByOidPcId[i].get("b2b_detail_id", 0L)) {
							throw new B2BOrderDetailSamePcDiffLotNumberException();
						}
					}
				}
				
				para.add("b2b_pc_id", product.get("pc_id", 0l));
				para.add("b2b_p_code", product.getString("p_code"));
				para.add("b2b_p_name", b2b_order_p_name);
				
				para.add("b2b_volume", product.get("volume", 0f));
				para.add("b2b_weight", product.get("weight", 0f));
				
				double send_price = product.get("unit_price", 0d);
				para.add("b2b_send_price", send_price);
			}
			
			if (parameter.containsKey("b2b_product_serial_number")) {
				if (b2BOrderItem.get("b2b_count", 0f) != 1) {
					throw new CanNotFillSerialNumberException();
				}
				String b2b_product_serial_number = StringUtil.getString(request, "b2b_product_serial_number");
				if (b2b_product_serial_number.equals("")) {
					b2b_product_serial_number = null;
				}
				
				DBRow b2BItemSN = floorB2BOrderMgrZyj.getB2BOrderItemBySN(b2b_oid, b2b_product_serial_number);
				if (b2BItemSN != null) {
					throw new B2BOrderSerialNumberRepeatException();
				}
				
				para.add("b2b_product_serial_number", b2b_product_serial_number);
			}
			
			// 订货数量、备件数量修改后从重新计算总数量
			if (parameter.containsKey("b2b_delivery_count") || parameter.containsKey("b2b_backup_count")) {
				float b2b_count = b2b_backup_count + b2b_delivery_count;
				para.add("b2b_count", b2b_count);
				para.add("b2b_wait_count", b2b_count);
				
				if (b2b_count != 1 && !b2BOrderItem.getString("b2b_product_serial_number").equals("")) {
					throw new HadSerialNumberException();
				}
			}
			
			if (parameter.containsKey("clp_type")) {
				long clp_type_id = StringUtil.getLong(request, "clp_type");
				
				DBRow clpType = lpTypeMgrZJ.getDetailCLPType(clp_type_id);
				
				if (clpType == null) {
					clpType = new DBRow();
				}
				
				DBRow[] b2BOrderItemsByOidPcId = floorB2BOrderMgrZyj.getB2BOrderItemByB2BOidPcId(b2b_oid,
						product.get("pc_id", 0l));
				if (b2BOrderItemsByOidPcId.length > 0) {
					for (int i = 0; i < b2BOrderItemsByOidPcId.length; i++) {
						if (b2BOrderItemsByOidPcId[i].getString("clp_type_id").equals(
								StringUtil.getString(request, "clp_type"))
								&& !b2BOrderItemsByOidPcId[i].getString("lot_number").equals(
										b2BOrderItem.getString("lot_number"))
								&& b2b_order_item_id != b2BOrderItemsByOidPcId[i].get("b2b_detail_id", 0L)) {
							throw new B2BOrderDetailSamePcDiffLotNumberException();
						}
					}
				}
				
				para.add("clp_type_id", clp_type_id);
				para.add("blp_type_id", clpType.get("sku_lp_box_type", 0));
			}
			
			if (parameter.containsKey("blp_type")) {
				long clp_type_id = b2BOrderItem.get("clp_type_id", 0l);
				
				if (clp_type_id == 0l) {
					long blp_type_id = StringUtil.getLong(request, "blp_type");
					DBRow[] b2BOrderItemsByOidPcId = floorB2BOrderMgrZyj.getB2BOrderItemByB2BOidPcId(b2b_oid,
							product.get("pc_id", 0l));
					if (b2BOrderItemsByOidPcId.length > 0) {
						for (int i = 0; i < b2BOrderItemsByOidPcId.length; i++) {
							if (b2BOrderItemsByOidPcId[i].getString("blp_type_id").equals(
									StringUtil.getString(request, "blp_type"))
									&& !b2BOrderItemsByOidPcId[i].getString("lot_number").equals(
											b2BOrderItem.getString("lot_number"))
									&& b2b_order_item_id != b2BOrderItemsByOidPcId[i].get("b2b_detail_id", 0L)) {
								throw new B2BOrderDetailSamePcDiffLotNumberException();
							}
						}
					}
					para.add("blp_type_id", blp_type_id);
				}
				
			}
			
			floorB2BOrderMgrZyj.modB2BOrderItem(b2b_order_item_id, para);
		} catch (CanNotFillSerialNumberException e) {
			throw e;
		} catch (B2BOrderSerialNumberRepeatException e) {
			throw e;
		} catch (HadSerialNumberException e) {
			throw e;
		} catch (ProductNotExistException e) {
			throw e;
		} catch (RepeatProductException e) {
			throw e;
		} catch (B2BOrderDetailSamePcDiffLotNumberException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 增加订单详细（单个）jqgrid的form增加 只能修改BLP，通过BLP获取CLP信息
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void addB2BOrderItem(HttpServletRequest request) throws Exception {
		try {
			String b2b_order_p_name = StringUtil.getString(request, "p_name");
			
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			DBRow b2bOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			long receive_psid = b2bOrder.get("receive_psid", 0l);
			long title_id = b2bOrder.get("title_id", 0l);
			
			// String b2b_box = StringUtil.getString(request,"b2b_box");
			String lot_number = StringUtil.getString(request, "lot_number");
			float b2b_delivery_count = StringUtil.getFloat(request, "b2b_delivery_count");
			float b2b_backup_count = StringUtil.getFloat(request, "b2b_backup_count");
			float b2b_count = b2b_delivery_count + b2b_backup_count;
			// String b2b_product_serial_number = StringUtil.getString(request,"b2b_product_serial_number",null);
			
			// if(b2b_count > 1 && !"".equals(b2b_product_serial_number))
			// {
			// throw new CanNotFillSerialNumberException();
			// }
			
			String clpTypeId = StringUtil.getString(request, "clp_type_id");
			String blpTypeId = StringUtil.getString(request, "blp_type");
			
			DBRow product = floorProductMgr.getDetailProductByPname(b2b_order_p_name);
			if (product == null) {
				throw new ProductNotExistException();
			}
			
			long b2b_pc_id = product.get("pc_id", 0l);
			
			DBRow[] b2BOrderItemsByOidPcId = floorB2BOrderMgrZyj.getB2BOrderItemByB2BOidPcId(b2b_oid, b2b_pc_id);
			if (b2BOrderItemsByOidPcId.length > 0) {
				for (int i = 0; i < b2BOrderItemsByOidPcId.length; i++) {
					if (b2BOrderItemsByOidPcId[i].getString("blp_type_id").equals(blpTypeId)
							&& !b2BOrderItemsByOidPcId[i].getString("lot_number").equals(lot_number)) {
						throw new B2BOrderDetailSamePcDiffLotNumberException();
					}
				}
			}
			
			// DBRow b2BOrderItemIsExit =
			// floorB2BOrderMgrZyj.getB2BOrderItemByB2BOidPcIdSerial(b2b_oid,b2b_pc_id,b2b_product_serial_number);
			//
			// if(b2BOrderItemIsExit!=null)
			// {
			// throw new B2BOrderDetailRepeatException();
			// }
			
			float b2b_volume = product.get("volume", 0f);
			float b2b_weight = product.get("weight", 0f);
			String b2b_p_code = product.getString("p_code");
			
			DBRow b2BOrderItem = new DBRow();
			b2BOrderItem.add("b2b_p_name", b2b_order_p_name);
			b2BOrderItem.add("b2b_count", b2b_count);
			b2BOrderItem.add("b2b_wait_count", b2b_count);
			b2BOrderItem.add("b2b_oid", b2b_oid);
			// b2BOrderItem.add("b2b_box",b2b_box);
			b2BOrderItem.add("lot_number", lot_number);
			b2BOrderItem.add("b2b_pc_id", b2b_pc_id);
			b2BOrderItem.add("b2b_p_code", b2b_p_code);
			b2BOrderItem.add("b2b_volume", b2b_volume);
			b2BOrderItem.add("b2b_weight", b2b_weight);
			// b2BOrderItem.add("b2b_product_serial_number",b2b_product_serial_number);
			
			b2BOrderItem.add("b2b_backup_count", b2b_backup_count);
			b2BOrderItem.add("b2b_delivery_count", b2b_delivery_count);
			
			double send_price = product.get("unit_price", 0d);
			b2BOrderItem.add("b2b_send_price", send_price);
			
			long clp_type_id = 0l;
			long blp_type_id = 0l;
			
			DBRow[] clps = lpTypeMgrZJ.getCLPTypeForSkuToShip(b2b_pc_id, title_id, receive_psid);
			if (clps.length > 0) {
				clp_type_id = clps[0].get("sku_lp_type_id", 0l);
				blp_type_id = clps[0].get("sku_lp_box_type", 0l);
				
			} else {
				DBRow[] blps = lpTypeMgrZJ.getBLPTypeForSkuToShipNotNull(b2b_pc_id, receive_psid);
				if (blps.length > 0) {
					blp_type_id = blps[0].get("box_type_id", 0l);
				}
				
			}
			
			b2BOrderItem.add("clp_type_id", clp_type_id);
			b2BOrderItem.add("blp_type_id", blp_type_id);
			
			floorB2BOrderMgrZyj.addB2BOrderItem(b2BOrderItem);
		} catch (CanNotFillSerialNumberException e) {
			throw e;
		} catch (HadSerialNumberException e) {
			throw e;
		} catch (ProductNotExistException e) {
			throw e;
		} catch (B2BOrderDetailRepeatException e) {
			throw e;
		} catch (B2BOrderDetailSamePcDiffLotNumberException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "addB2BOrderItem(HttpServletRequest request)", log);
		}
	}
	
	/**
	 * 删除订单明细
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void delB2BOrderItem(HttpServletRequest request) throws Exception {
		try {
			long b2b_detail_id = StringUtil.getLong(request, "id");// id因为jqgrid因素
			
			floorB2BOrderMgrZyj.delB2BOrderItem(b2b_detail_id);
		} catch (Exception e) {
			throw new SystemException(e, "delB2BOrderItem(HttpServletRequest request)", log);
		}
	}
	
	/**
	 * 更新订单各个流程信息
	 */
	public void updateB2BOrderProdures(HttpServletRequest request) throws Exception {
		try {
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long logingUserId = adminLoginBean.getAdid();
			String loginUserName = adminLoginBean.getEmploye_name();
			
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			int b2b_b2b_order_status = null != b2BOrderRow ? b2BOrderRow.get("b2b_order_status", 0) : 0;
			int b2b_declaration = null != b2BOrderRow ? b2BOrderRow.get("declaration", 0) : 0;
			int b2b_clearance = null != b2BOrderRow ? b2BOrderRow.get("clearance", 0) : 0;
			int b2b_product_file = null != b2BOrderRow ? b2BOrderRow.get("product_file", 0) : 0;
			int b2b_tag = null != b2BOrderRow ? b2BOrderRow.get("tag", 0) : 0;
			int b2b_tag_third = null != b2BOrderRow ? b2BOrderRow.get("tag_third", 0) : 0;
			int b2b_stock_in_set = null != b2BOrderRow ? b2BOrderRow.get("stock_in_set", 0) : 0;
			int b2b_certificate = null != b2BOrderRow ? b2BOrderRow.get("certificate", 0) : 0;
			int b2b_quality_inspection = null != b2BOrderRow ? b2BOrderRow.get("quality_inspection", 0) : 0;
			String b2b_b2b_order_date = null != b2BOrderRow ? b2BOrderRow.getString("b2b_order_date") : "";
			
			// 主流程
			String adminUserIdsTransport = StringUtil.getString(request, "adminUserIdsTransport");
			String adminUserNamesTransport = StringUtil.getString(request, "adminUserNamesTransport");
			int needMailTransport = StringUtil.getInt(request, "needMailTransport");
			int needMessageTransport = StringUtil.getInt(request, "needMessageTransport");
			int needPageTransport = StringUtil.getInt(request, "needPageTransport");
			
			// 出口报关
			int declaration = StringUtil.getInt(request, "declaration");
			String adminUserIdsDeclaration = StringUtil.getString(request, "adminUserIdsDeclaration");
			String adminUserNamesDeclaration = StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration = StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration = StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration = StringUtil.getInt(request, "needPageDeclaration");
			
			// 进口清关
			int clearance = StringUtil.getInt(request, "clearance");
			String adminUserIdsClearance = StringUtil.getString(request, "adminUserIdsClearance");
			String adminUserNamesClearance = StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance = StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance = StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance = StringUtil.getInt(request, "needPageClearance");
			
			// 商品图片
			int product_file = StringUtil.getInt(request, "product_file");
			String adminUserIdsProductFile = StringUtil.getString(request, "adminUserIdsProductFile");
			String adminUserNamesProductFile = StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile = StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile = StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile = StringUtil.getInt(request, "needPageProductFile");
			
			// 内部标签
			int tag = StringUtil.getInt(request, "tag");
			String adminUserIdsTag = StringUtil.getString(request, "adminUserIdsTag");
			String adminUserNamesTag = StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag = StringUtil.getInt(request, "needMailTag");
			int needMessageTag = StringUtil.getInt(request, "needMessageTag");
			int needPageTag = StringUtil.getInt(request, "needPageTag");
			
			// 第三方标签
			int tag_third = StringUtil.getInt(request, "tag_third");
			String adminUserIdsTagThird = StringUtil.getString(request, "adminUserIdsTagThird");
			String adminUserNamesTagThird = StringUtil.getString(request, "adminUserNamesTagThird");
			int needMailTagThird = StringUtil.getInt(request, "needMailTagThird");
			int needMessageTagThird = StringUtil.getInt(request, "needMessageTagThird");
			int needPageTagThird = StringUtil.getInt(request, "needPageTagThird");
			
			// 运费
			int stock_in_set = StringUtil.getInt(request, "stock_in_set");
			String adminUserIdsStockInSet = StringUtil.getString(request, "adminUserIdsStockInSet");
			String adminUserNamesStockInSet = StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet = StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet = StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet = StringUtil.getInt(request, "needPageStockInSet");
			
			// 单证
			int certificate = StringUtil.getInt(request, "certificate");
			String adminUserIdsCertificate = StringUtil.getString(request, "adminUserIdsCertificate");
			String adminUserNamesCertificate = StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate = StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate = StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate = StringUtil.getInt(request, "needPageCertificate");
			
			// 质检
			int quality_inspection = StringUtil.getInt(request, "quality_inspection");
			String adminUserIdsQualityInspection = StringUtil.getString(request, "adminUserIdsQualityInspection");
			String adminUserNamesQualityInspection = StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection = StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection = StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection = StringUtil.getInt(request, "needPageQualityInspection");
			
			String nowDate = DateUtil.NowStr();
			TDate tDate = new TDate();
			tDate.addDay(+1);
			DBRow b2BOrderUpdateRow = new DBRow();
			// 如果数据库中是需要，不需要，或者为0，才更新主单据
			if (B2BOrderDeclarationKey.NODELARATION == b2b_declaration
					|| B2BOrderDeclarationKey.DELARATION == b2b_declaration || 0 == b2b_declaration) {
				b2BOrderUpdateRow.add("declaration", declaration);
			}
			if (B2BOrderClearanceKey.NOCLEARANCE == b2b_clearance || B2BOrderClearanceKey.CLEARANCE == b2b_clearance
					|| 0 == b2b_clearance) {
				b2BOrderUpdateRow.add("clearance", clearance);
			}
			if (B2BOrderProductFileKey.NOPRODUCTFILE == b2b_product_file
					|| B2BOrderProductFileKey.PRODUCTFILE == b2b_product_file || 0 == b2b_product_file) {
				b2BOrderUpdateRow.add("product_file", product_file);
			}
			if (B2BOrderTagKey.NOTAG == b2b_tag || B2BOrderTagKey.TAG == b2b_tag || 0 == b2b_tag) {
				b2BOrderUpdateRow.add("tag", tag);
			}
			if (B2BOrderTagKey.NOTAG == b2b_tag_third || B2BOrderTagKey.TAG == b2b_tag_third || 0 == b2b_tag_third) {
				b2BOrderUpdateRow.add("tag_third", tag_third);
			}
			if (B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET == b2b_stock_in_set
					|| B2BOrderStockInSetKey.SHIPPINGFEE_SET == b2b_stock_in_set || 0 == b2b_stock_in_set) {
				b2BOrderUpdateRow.add("stock_in_set", stock_in_set);
			}
			if (B2BOrderCertificateKey.NOCERTIFICATE == b2b_certificate
					|| B2BOrderCertificateKey.CERTIFICATE == b2b_certificate || 0 == b2b_certificate) {
				b2BOrderUpdateRow.add("certificate", certificate);
			}
			if (B2BOrderQualityInspectionKey.NO_NEED_QUALITY == b2b_quality_inspection
					|| B2BOrderQualityInspectionKey.NEED_QUALITY == b2b_quality_inspection
					|| 0 == b2b_quality_inspection) {
				b2BOrderUpdateRow.add("quality_inspection", quality_inspection);
			}
			b2BOrderUpdateRow.add("updatedate", nowDate);
			floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, b2BOrderUpdateRow);
			
			// 发送的内容
			String sendContent = this.handleSendContent(b2b_oid, adminUserNamesTransport, declaration,
					adminUserNamesDeclaration, clearance, adminUserNamesClearance, product_file,
					adminUserNamesProductFile, tag, adminUserNamesTag, stock_in_set, adminUserNamesStockInSet,
					certificate, adminUserNamesCertificate, quality_inspection, adminUserNamesQualityInspection);
			// 主流程
			this.updateB2BOrderTransport(logingUserId, adminUserIdsTransport, "", b2b_oid, ModuleKey.B2B_ORDER,
					needPageTransport, needMailTransport, needMessageTransport, request, true, 4, b2b_b2b_order_status,
					loginUserName, sendContent);
			// 报关
			this.updateB2BOrderDelaration(logingUserId, adminUserIdsDeclaration, "", b2b_oid, ModuleKey.B2B_ORDER,
					needPageDeclaration, needMailDeclaration, needMessageDeclaration, request, false, 7, declaration,
					b2b_declaration, loginUserName, sendContent);
			// 清关
			this.updateB2BOrderClearance(logingUserId, adminUserIdsClearance, "", b2b_oid, ModuleKey.B2B_ORDER,
					needPageClearance, needMailClearance, needMessageClearance, request, false, 6, clearance,
					b2b_clearance, loginUserName, sendContent);
			// 实物图片
			this.updateB2BOrderProductFile(logingUserId, adminUserIdsProductFile, "", b2b_oid, ModuleKey.B2B_ORDER,
					needPageProductFile, needMailProductFile, needMessageProductFile, request, true, 10, product_file,
					b2b_product_file, loginUserName, sendContent, b2b_b2b_order_date);
			// 内部标签
			this.updateB2BOrderTag(logingUserId, adminUserIdsTag, "", b2b_oid, ModuleKey.B2B_ORDER, needPageTag,
					needMailTag, needMessageTag, request, true, 8, tag, b2b_tag, loginUserName, sendContent, nowDate);
			// 第三方标签
			this.updateB2BOrderTagThird(logingUserId, adminUserIdsTagThird, "", b2b_oid, ModuleKey.B2B_ORDER,
					needPageTagThird, needMailTagThird, needMessageTagThird, request, true,
					B2BOrderLogTypeKey.THIRD_TAG, tag_third, b2b_tag_third, loginUserName, sendContent, nowDate);
			// 运费
			this.updateB2BOrderStockInSet(logingUserId, adminUserIdsStockInSet, "", b2b_oid, ModuleKey.B2B_ORDER,
					needPageStockInSet, needMailStockInSet, needMessageStockInSet, request, false, 5, stock_in_set,
					b2b_stock_in_set, loginUserName, sendContent, nowDate);
			// 单证
			this.updateB2BOrderCertificate(logingUserId, adminUserIdsCertificate, "", b2b_oid, ModuleKey.B2B_ORDER,
					needPageCertificate, needMailCertificate, needMessageCertificate, request, true, 9, certificate,
					b2b_certificate, loginUserName, sendContent, nowDate);
			// 质检报告
			this.updateB2BOrderQuality(logingUserId, adminUserIdsQualityInspection, "", b2b_oid, ModuleKey.B2B_ORDER,
					needPageQualityInspection, needMailQualityInspection, needMessageQualityInspection, request, true,
					11, quality_inspection, b2b_quality_inspection, loginUserName, sendContent, nowDate);
			
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderProdures", log);
		}
	}
	
	public String handleSendContent(long b2b_oid, String adminUserNamesTransport, int declaration,
			String declarationPerson, int clearance, String clearancePerson, int productFile, String productPerson,
			int tag, String tagPerson, int stockInSet, String stockPerson, int certificate, String certificatePerson,
			int quality, String qualityPerson) throws Exception {
		
		try {
			String rnStr = "";
			DBRow b2bOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			// 获取提货仓库的名字
			long pro_cata_id_send = b2bOrderRow.get("send_psid", 0L);
			String cata_name_send = "";
			DBRow storageCatalogSend = floorCatalogMgr.getDetailProductStorageCatalogById(pro_cata_id_send);
			if (null != storageCatalogSend) {
				cata_name_send = storageCatalogSend.getString("title");
				rnStr += "提货仓库:" + cata_name_send + ",";
			}
			// 获取收货仓库的名字
			long pro_cata_id_deliver = b2bOrderRow.get("receive_psid", 0L);
			DBRow storageCatalogDeliver = floorCatalogMgr.getDetailProductStorageCatalogById(pro_cata_id_deliver);
			String cata_name_deliver = "";
			if (null != storageCatalogDeliver) {
				cata_name_deliver = storageCatalogDeliver.getString("title");
				rnStr += "收货仓库:" + cata_name_deliver + ",";
			}
			// 获取eta
			String eta = b2bOrderRow.getString("b2b_order_receive_date");
			if (!"".equals(eta)) {
				TDate tdateEta = new TDate(eta);
				String tdateEtaStr = tdateEta.formatDate("yyyy-MM-dd");
				rnStr += "预计到货时间:" + tdateEtaStr + ",";
			}
			// 订单货物的总体积，如果 有值，添加，此处待完善
			rnStr += "总体积:" + floorB2BOrderMgrZyj.getB2BOrderVolume(b2b_oid);
			return rnStr;
		} catch (Exception e) {
			throw new SystemException(e, "handleSendContent", log);
		}
	}
	
	/*
	 * 更新主流程
	 */
	private void updateB2BOrderTransport(long logingUserId, String executePersonIds, String joinPersonIds,
			long b2b_oid, int moduleId, int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int pur_activity, String loginUserName, String sendContent) throws Exception {
		try {
			B2BOrderKey B2BOrderKey = new B2BOrderKey();
			this.updateB2BOrderScheduleOneProdure(DateUtil.NowStr(), "", logingUserId, executePersonIds, joinPersonIds,
					b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[货物状态]", sendContent, page, mail, message,
					request, false, 4, pur_activity, loginUserName, "[" + B2BOrderKey.getB2BOrderKeyById(pur_activity)
							+ "]");
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderTransport", log);
		}
	}
	
	/*
	 * 更新报关流程
	 */
	private void updateB2BOrderDelaration(long logingUserId, String executePersonIds, String joinPersonIds,
			long b2b_oid, int moduleId, int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, int pur_activity, String loginUserName, String sendContent) throws Exception {
		try {
			B2BOrderDeclarationKey b2BOrderDeclarationKey = new B2BOrderDeclarationKey();
			// 报关，3.报关中，4.完成
			// 原数据未填写是否需要报送且现在需要报送或原来不需要，现在需要，添加
			if ((0 == pur_activity && B2BOrderDeclarationKey.DELARATION == activity)
					|| (B2BOrderDeclarationKey.NODELARATION == pur_activity && B2BOrderDeclarationKey.DELARATION == activity)) {
				// 添加
				this.addB2BOrderScheduleOneProdure("", "", logingUserId, executePersonIds, joinPersonIds, b2b_oid,
						ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[报关流程]", sendContent, page, mail, message, request,
						false, 7, B2BOrderDeclarationKey.DELARATION, loginUserName, "修改订单:" + b2b_oid + "[需要报关]");
				
				// 如果原数据需要，现在不需要，删除
			} else if (B2BOrderDeclarationKey.DELARATION == pur_activity
					&& B2BOrderDeclarationKey.NODELARATION == activity) {
				// 删除
				this.deleteB2BOrderScheduleById(b2b_oid, ModuleKey.B2B_ORDER, 7, B2BOrderDeclarationKey.NODELARATION,
						loginUserName, "修改订单:" + b2b_oid + "[不需要报关]", logingUserId, request);
				
				// 原来需要，现在需要，更新；更新的前提是看，是否添加过任务
			} else if ((B2BOrderDeclarationKey.DELARATION == pur_activity && B2BOrderDeclarationKey.DELARATION == activity)) {
				// 更新
				this.updateB2BOrderScheduleOneProdure("", "", logingUserId, executePersonIds, joinPersonIds, b2b_oid,
						ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[报关流程]", sendContent, page, mail, message, request,
						false, 7, B2BOrderDeclarationKey.DELARATION, loginUserName, "修改订单:" + b2b_oid + "[需要报关]");
				// 报送处于中，更新任务
			} else if (B2BOrderDeclarationKey.DELARATING == pur_activity) {
				this.updateB2BOrderScheduleOneProdure("", "", logingUserId, executePersonIds, joinPersonIds, b2b_oid,
						ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[报关流程]", sendContent, page, mail, message, request,
						false, 7, B2BOrderDeclarationKey.DELARATING, loginUserName, "修改订单:" + b2b_oid + "["
								+ b2BOrderDeclarationKey.getB2BOrderDeclarationKeyById(pur_activity) + "]");
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderDelaration", log);
		}
	}
	
	/*
	 * 更新清关流程
	 */
	private void updateB2BOrderClearance(long logingUserId, String executePersonIds, String joinPersonIds,
			long b2b_oid, int moduleId, int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, int pur_activity, String loginUserName, String sendContent) throws Exception {
		try {
			// 清关 3.清关中，4.清关完成，5.查货中
			// 原数据未填写是否需要清关且现在需要清关或原来不需要，现在需要，添加
			if ((0 == pur_activity && B2BOrderClearanceKey.CLEARANCE == activity)
					|| (B2BOrderClearanceKey.NOCLEARANCE == pur_activity && B2BOrderClearanceKey.CLEARANCE == activity)) {
				// 添加
				this.addB2BOrderScheduleOneProdure("", "", logingUserId, executePersonIds, joinPersonIds, b2b_oid,
						ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[清关流程]", sendContent, page, mail, message, request,
						false, 6, B2BOrderClearanceKey.CLEARANCE, loginUserName, "修改订单:" + b2b_oid + "[需要清关]");
				// 原来需要，现在不需要，删除
			} else if (B2BOrderClearanceKey.CLEARANCE == pur_activity && B2BOrderClearanceKey.NOCLEARANCE == activity) {
				// 删除
				this.deleteB2BOrderScheduleById(b2b_oid, ModuleKey.B2B_ORDER, 6, B2BOrderClearanceKey.NOCLEARANCE,
						loginUserName, "修改订单:" + b2b_oid + "[不需要清关]", logingUserId, request);
				
				// 原来需要，现在也需要
			} else if ((B2BOrderClearanceKey.CLEARANCE == pur_activity && B2BOrderClearanceKey.CLEARANCE == activity)) {
				// 更新
				this.updateB2BOrderScheduleOneProdure("", "", logingUserId, executePersonIds, joinPersonIds, b2b_oid,
						ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[清关流程]", sendContent, page, mail, message, request,
						false, 6, B2BOrderClearanceKey.CLEARANCE, loginUserName, "修改订单:" + b2b_oid + "[需要清关]");
				
				// 原数据正在清关中 原数据处理查货中
			} else if (B2BOrderClearanceKey.CLEARANCEING == pur_activity) {
				// 更新
				this.updateB2BOrderScheduleOneProdure("", "", logingUserId, executePersonIds, joinPersonIds, b2b_oid,
						ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[清关流程]", sendContent, page, mail, message, request,
						false, 6, B2BOrderClearanceKey.CLEARANCE, loginUserName, "修改订单:" + b2b_oid + "["
								+ new B2BOrderClearanceKey().getB2BOrderClearanceKeyById(pur_activity) + "]");
			} else if (B2BOrderClearanceKey.CHECKCARGO == pur_activity) {
				// 更新
				this.updateB2BOrderScheduleOneProdure("", "", logingUserId, executePersonIds, joinPersonIds, b2b_oid,
						ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[清关流程]", sendContent, page, mail, message, request,
						false, 6, B2BOrderClearanceKey.CHECKCARGO, loginUserName, "修改订单:" + b2b_oid + "["
								+ new B2BOrderClearanceKey().getB2BOrderClearanceKeyById(pur_activity) + "]");
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderClearance", log);
		}
	}
	
	/*
	 * 更新实物图片流程
	 */
	private void updateB2BOrderProductFile(long logingUserId, String executePersonIds, String joinPersonIds,
			long b2b_oid, int moduleId, int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, int pur_activity, String loginUserName, String sendContent,
			String b2b_order_date) throws Exception {
		try {
			TDate tdateProductFile = new TDate(b2b_order_date);
			tdateProductFile
					.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_product_file_period")));
			String endTimeProductFile = tdateProductFile.formatDate("yyyy-MM-dd HH:mm:ss");
			// 商品图片流程 1.无需商品文件，2.商品文件上传中，3.商品文件上传完成
			if ((0 == pur_activity && B2BOrderProductFileKey.PRODUCTFILE == activity)
					|| (B2BOrderProductFileKey.NOPRODUCTFILE == pur_activity && B2BOrderProductFileKey.PRODUCTFILE == activity)) {
				// 添加
				this.addB2BOrderScheduleOneProdure(b2b_order_date, endTimeProductFile, logingUserId, executePersonIds,
						joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[商品图片流程]", sendContent, page,
						mail, message, request, true, 10, B2BOrderProductFileKey.PRODUCTFILE, loginUserName, "修改订单:"
								+ b2b_oid + "[商品文件上传中]");
			} else if (B2BOrderProductFileKey.PRODUCTFILE == pur_activity
					&& B2BOrderProductFileKey.NOPRODUCTFILE == activity) {
				// 删除
				this.deleteB2BOrderScheduleById(b2b_oid, ModuleKey.B2B_ORDER, 10, B2BOrderProductFileKey.NOPRODUCTFILE,
						loginUserName, "修改订单:" + b2b_oid + "[无需商品文件]", logingUserId, request);
			} else if ((B2BOrderProductFileKey.PRODUCTFILE == pur_activity && B2BOrderProductFileKey.PRODUCTFILE == activity)) {
				// 更新
				this.updateB2BOrderScheduleOneProdure(b2b_order_date, endTimeProductFile, logingUserId,
						executePersonIds, joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[商品图片流程]",
						sendContent, page, mail, message, request, true, 10, B2BOrderProductFileKey.PRODUCTFILE,
						loginUserName, "修改订单:" + b2b_oid + "[商品文件上传中]");
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderProductFile", log);
		}
	}
	
	/*
	 * 更新制签流程
	 */
	private void updateB2BOrderTag(long logingUserId, String executePersonIds, String joinPersonIds, long b2b_oid,
			int moduleId, int page, int mail, int message, HttpServletRequest request, boolean isNow, int processType,
			int activity, int pur_activity, String loginUserName, String sendContent, String b2b_order_date)
			throws Exception {
		try {
			TDate tdateTag = new TDate(b2b_order_date);
			tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_tag_period")));
			String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
			// 制签流程 1.无需制签，2.需要制签，3.制签完成
			if ((0 == pur_activity && B2BOrderTagKey.TAG == activity)
					|| (B2BOrderTagKey.NOTAG == pur_activity && B2BOrderTagKey.TAG == activity)) {
				// 添加
				this.addB2BOrderScheduleOneProdure(b2b_order_date, endTimeTag, logingUserId, executePersonIds,
						joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[内部标签流程]", sendContent, page,
						mail, message, request, true, 8, B2BOrderTagKey.TAG, loginUserName, "修改订单:" + b2b_oid + "[制签中]");
			} else if (B2BOrderTagKey.TAG == pur_activity && B2BOrderTagKey.NOTAG == activity) {
				// 删除
				this.deleteB2BOrderScheduleById(b2b_oid, ModuleKey.B2B_ORDER, 8, B2BOrderTagKey.NOTAG, loginUserName,
						"修改订单:" + b2b_oid + "[无需制签]", logingUserId, request);
			} else if ((B2BOrderTagKey.TAG == pur_activity && B2BOrderTagKey.TAG == activity)) {
				// 更新
				this.updateB2BOrderScheduleOneProdure(b2b_order_date, endTimeTag, logingUserId, executePersonIds,
						joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[内部标签流程]", sendContent, page,
						mail, message, request, true, 8, B2BOrderTagKey.TAG, loginUserName, "修改订单:" + b2b_oid + "[制签中]");
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderTag", log);
		}
	}
	
	/*
	 * 更新制签流程
	 */
	private void updateB2BOrderTagThird(long logingUserId, String executePersonIds, String joinPersonIds, long b2b_oid,
			int moduleId, int page, int mail, int message, HttpServletRequest request, boolean isNow, int processType,
			int activity, int pur_activity, String loginUserName, String sendContent, String b2b_order_date)
			throws Exception {
		try {
			TDate tdateTag = new TDate(b2b_order_date);
			tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_third_tag_period")));
			String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
			// 制签流程 1.无需制签，2.需要制签，3.制签完成
			if ((0 == pur_activity && B2BOrderTagKey.TAG == activity)
					|| (B2BOrderTagKey.NOTAG == pur_activity && B2BOrderTagKey.TAG == activity)) {
				// 添加
				this.addB2BOrderScheduleOneProdure(b2b_order_date, endTimeTag, logingUserId, executePersonIds,
						joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[第三方标签流程]", sendContent, page,
						mail, message, request, true, B2BOrderLogTypeKey.THIRD_TAG, B2BOrderTagKey.TAG, loginUserName,
						"修改订单:" + b2b_oid + "[制签中]");
			} else if (B2BOrderTagKey.TAG == pur_activity && B2BOrderTagKey.NOTAG == activity) {
				// 删除
				this.deleteB2BOrderScheduleById(b2b_oid, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.THIRD_TAG,
						B2BOrderTagKey.NOTAG, loginUserName, "修改订单:" + b2b_oid + "[无需制签]", logingUserId, request);
			} else if ((B2BOrderTagKey.TAG == pur_activity && B2BOrderTagKey.TAG == activity)) {
				// 更新
				this.updateB2BOrderScheduleOneProdure(b2b_order_date, endTimeTag, logingUserId, executePersonIds,
						joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[第三方标签流程]", sendContent, page,
						mail, message, request, true, B2BOrderLogTypeKey.THIRD_TAG, B2BOrderTagKey.TAG, loginUserName,
						"修改订单:" + b2b_oid + "[制签中]");
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderTagThird", log);
		}
	}
	
	/*
	 * 更新运费流程
	 */
	private void updateB2BOrderStockInSet(long logingUserId, String executePersonIds, String joinPersonIds,
			long b2b_oid, int moduleId, int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, int pur_activity, String loginUserName, String sendContent,
			String b2b_order_date) throws Exception {
		try {
			// 运费流程，阶段：3.运费已经设置，4.运费已申请，5.运费转账完
			if ((0 == pur_activity && B2BOrderStockInSetKey.SHIPPINGFEE_SET == activity)
					|| (B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET == pur_activity && B2BOrderStockInSetKey.SHIPPINGFEE_SET == activity)) {
				// 添加
				this.addB2BOrderScheduleOneProdure("", "", logingUserId, executePersonIds, joinPersonIds, b2b_oid,
						ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[运费流程]", sendContent, page, mail, message, request,
						false, 5, B2BOrderStockInSetKey.SHIPPINGFEE_SET, loginUserName, "修改订单:" + b2b_oid + "[需要运费]");
			} else if ((B2BOrderStockInSetKey.SHIPPINGFEE_SET == pur_activity)
					&& B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET == activity) {
				// 删除
				this.deleteB2BOrderScheduleById(b2b_oid, ModuleKey.B2B_ORDER, 5,
						B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET, loginUserName, "修改订单:" + b2b_oid + "[无需运费]",
						logingUserId, request);
				
				// 原数据为需要，现在为需要，更新
			} else if ((B2BOrderStockInSetKey.SHIPPINGFEE_SET == pur_activity && B2BOrderStockInSetKey.SHIPPINGFEE_SET == activity)) {
				// 更新
				this.updateB2BOrderScheduleOneProdure("", "", logingUserId, executePersonIds, joinPersonIds, b2b_oid,
						ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[运费流程]", sendContent, page, mail, message, request,
						false, 5, B2BOrderStockInSetKey.SHIPPINGFEE_SET, loginUserName, "修改订单:" + b2b_oid + "[需要运费]");
				
				// 原运费已设置，更新
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderStockInSet", log);
		}
	}
	
	/*
	 * 更新单证流程
	 */
	private void updateB2BOrderCertificate(long logingUserId, String executePersonIds, String joinPersonIds,
			long b2b_oid, int moduleId, int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, int pur_activity, String loginUserName, String sendContent,
			String b2b_order_date) throws Exception {
		try {
			TDate tdateCertificate = new TDate(b2b_order_date);
			tdateCertificate
					.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_certificate_period")));
			String endTimeCertificate = tdateCertificate.formatDate("yyyy-MM-dd HH:mm:ss");
			// 单证流程 1.无需单证，2.单证采集中，3，单证采集完成
			if ((0 == pur_activity && B2BOrderCertificateKey.CERTIFICATE == activity)
					|| (B2BOrderCertificateKey.NOCERTIFICATE == pur_activity && B2BOrderCertificateKey.CERTIFICATE == activity)) {
				// 添加
				this.addB2BOrderScheduleOneProdure(b2b_order_date, endTimeCertificate, logingUserId, executePersonIds,
						joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[单证流程]", sendContent, page,
						mail, message, request, true, 9, B2BOrderCertificateKey.CERTIFICATE, loginUserName, "修改订单:"
								+ b2b_oid + "[单证采集中]");
			} else if (B2BOrderCertificateKey.CERTIFICATE == pur_activity
					&& B2BOrderCertificateKey.NOCERTIFICATE == activity) {
				// 删除
				this.deleteB2BOrderScheduleById(b2b_oid, ModuleKey.B2B_ORDER, 9, B2BOrderCertificateKey.NOCERTIFICATE,
						loginUserName, "修改订单:" + b2b_oid + "[无需单证]", logingUserId, request);
			} else if ((B2BOrderCertificateKey.CERTIFICATE == pur_activity && B2BOrderCertificateKey.CERTIFICATE == activity)) {
				// 更新
				this.updateB2BOrderScheduleOneProdure(b2b_order_date, endTimeCertificate, logingUserId,
						executePersonIds, joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[单证流程]",
						sendContent, page, mail, message, request, true, 9, B2BOrderCertificateKey.CERTIFICATE,
						loginUserName, "修改订单:" + b2b_oid + "[单证采集中]");
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderCertificate", log);
		}
	}
	
	/*
	 * 更新质检报告流程
	 */
	private void updateB2BOrderQuality(long logingUserId, String executePersonIds, String joinPersonIds, long b2b_oid,
			int moduleId, int page, int mail, int message, HttpServletRequest request, boolean isNow, int processType,
			int activity, int pur_activity, String loginUserName, String sendContent, String b2b_order_date)
			throws Exception {
		
		try {
			TDate tdateQuality = new TDate(b2b_order_date);
			tdateQuality.addDay(Integer.parseInt(systemConfig.getStringConfigValue("b2b_order_quality_inspection")));
			String endTimeQuality = tdateQuality.formatDate("yyyy-MM-dd HH:mm:ss");
			// 质检流程 3.质检中，4.质检完成
			if ((0 == pur_activity && B2BOrderQualityInspectionKey.NEED_QUALITY == activity)
					|| (B2BOrderQualityInspectionKey.NO_NEED_QUALITY == pur_activity && B2BOrderQualityInspectionKey.NEED_QUALITY == activity)) {
				// 添加
				this.addB2BOrderScheduleOneProdure(b2b_order_date, endTimeQuality, logingUserId, executePersonIds,
						joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[质检流程]", sendContent, page,
						mail, message, request, true, 11, B2BOrderQualityInspectionKey.NEED_QUALITY, loginUserName,
						"修改订单:" + b2b_oid + "[需要质检]");
			} else if (B2BOrderQualityInspectionKey.NEED_QUALITY == pur_activity
					&& B2BOrderQualityInspectionKey.NO_NEED_QUALITY == activity) {
				// 删除
				this.deleteB2BOrderScheduleById(b2b_oid, ModuleKey.B2B_ORDER, 11,
						B2BOrderQualityInspectionKey.NO_NEED_QUALITY, loginUserName, "修改订单:" + b2b_oid + "[无需质检]",
						logingUserId, request);
				
				// 原数据需要且现需要 原数据处于质检中
			} else if ((B2BOrderQualityInspectionKey.NEED_QUALITY == pur_activity && B2BOrderQualityInspectionKey.NEED_QUALITY == activity)) {
				// 更新
				this.updateB2BOrderScheduleOneProdure(b2b_order_date, endTimeQuality, logingUserId, executePersonIds,
						joinPersonIds, b2b_oid, ModuleKey.B2B_ORDER, "订单:" + b2b_oid + "[质检流程]", sendContent, page,
						mail, message, request, true, 11, B2BOrderQualityInspectionKey.NEED_QUALITY, loginUserName,
						"修改订单:" + b2b_oid + "[需要质检]");
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderQuality", log);
		}
	}
	
	public void updateB2BOrderScheduleOneProdure(String beginTime, String endTime, long adid, String executePersonIds,
			String joinPersonIds, long b2b_oid, int moduleId, String title, String content, int page, int mail,
			int message, HttpServletRequest request, boolean isNow, int processType, int activity, String employeeName,
			String logContent) throws Exception {
		
		try {
			boolean needPage = false;
			boolean needMail = false;
			boolean needMessage = false;
			if (2 == page) {
				needPage = true;
			}
			if (2 == mail) {
				needMail = true;
			}
			if (2 == message) {
				needMessage = true;
			}
			DBRow schedule = scheduleMgrZr.getScheduleByAssociate(b2b_oid, moduleId, processType);
			DBRow[] executePersons = scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(b2b_oid, moduleId,
					processType);
			if (!"".equals(executePersonIds)) {
				String[] adminUserIds = executePersonIds.split(",");
				if (executePersons.length != adminUserIds.length) {
					if (null != schedule) {
						scheduleMgrZr.updateScheduleExternal(b2b_oid, moduleId, processType, content, executePersonIds,
								request, needMail, needMessage, needPage);
						// 添加日志
						this.insertLogsAndUpdateOrder(b2b_oid, logContent, adid, employeeName, processType, "",
								activity);
					} else {
						this.addB2BOrderScheduleOneProdure(beginTime, endTime, adid, executePersonIds, joinPersonIds,
								b2b_oid, moduleId, title, content, page, mail, message, request, isNow, processType,
								activity, employeeName, logContent);
					}
				} else {
					HashSet<String> executePersonSet = new HashSet<String>();
					for (int i = 0; i < executePersons.length; i++) {
						executePersonSet.add(executePersons[i].getString("schedule_execute_id"));
					}
					int executePersonLen = executePersonSet.size();
					for (int i = 0; i < adminUserIds.length; i++) {
						executePersonSet.add(adminUserIds[i]);
					}
					int executePersonArrLen = executePersonSet.size();
					if (executePersonLen != executePersonArrLen) {
						if (null != schedule) {
							scheduleMgrZr.updateScheduleExternal(b2b_oid, moduleId, processType, content,
									executePersonIds, request, needMail, needMessage, needPage);
							// 添加日志
							this.insertLogsAndUpdateOrder(b2b_oid, logContent, adid, employeeName, processType, "",
									activity);
						} else {
							this.addB2BOrderScheduleOneProdure(beginTime, endTime, adid, executePersonIds,
									joinPersonIds, b2b_oid, moduleId, title, content, page, mail, message, request,
									isNow, processType, activity, employeeName, logContent);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderScheduleOneProdure", log);
		}
	}
	
	/**
	 * 添加任务
	 */
	public void addB2BOrderScheduleOneProdure(String beginTime, String endTime, long adid, String executePersonIds,
			String joinPersonIds, long b2b_oid, int moduleId, String title, String content, int page, int mail,
			int message, HttpServletRequest request, boolean isNow, int processType, int activity, String employeeName,
			String logContent) throws Exception {
		
		try {
			boolean needPage = false;
			boolean needMail = false;
			boolean needMessage = false;
			if (2 == page) {
				needPage = true;
			}
			if (2 == mail) {
				needMail = true;
			}
			if (2 == message) {
				needMessage = true;
			}
			// 添加之前再确定一下，是否此流程的任务真的没有
			DBRow schedule = scheduleMgrZr.getScheduleByAssociate(b2b_oid, moduleId, processType);
			if (null == schedule) {
				scheduleMgrZr.addScheduleByExternalArrangeNow(beginTime, endTime, adid, executePersonIds,
						joinPersonIds, b2b_oid, moduleId, title, content, needPage, needMail, needMessage, request,
						isNow, processType);
				// 添加日志
				this.insertLogsAndUpdateOrder(b2b_oid, logContent, adid, employeeName, processType, "", activity);
			}
			
		} catch (Exception e) {
			throw new SystemException(e, "addB2BOrderScheduleOneProdure", log);
		}
		
	}
	
	/**
	 * 根据订单Id,业务类型，流程类型，删除任务
	 */
	public void deleteB2BOrderScheduleById(long b2b_oid, int moduleId, int processType, int activity,
			String employeeName, String logContent, long adid, HttpServletRequest request) throws Exception {
		try {
			DBRow schedule = scheduleMgrZr.getScheduleByAssociate(b2b_oid, moduleId, processType);
			if (null != schedule) {
				long scheduleId = schedule.get("schedule_id", 0L);
				scheduleMgrZr.deleteScheduleByScheduleId(scheduleId, request);
				this.insertLogsAndUpdateOrder(b2b_oid, logContent, adid, employeeName, processType, "", activity);
			}
			
		} catch (Exception e) {
			throw new SystemException(e, "deleteB2BOrderScheduleById", log);
		}
	}
	
	/**
	 * 下载订单
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String downloadB2BOrder(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			DBRow b2BOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			
			DBRow[] b2BOrderItems = floorB2BOrderMgrZyj.getB2BOrderItemByB2BOrderId(b2b_oid, null, null, null, null);
			if (b2BOrder == null) {
				throw new B2BOrderNoExistException();
			}
			if (b2BOrderItems == null) {
				throw new B2BOrderNoExistItemException();
			}
			
			POIFSFileSystem fs = null;// 获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()
					+ "/administrator/b2b_order/b2b_order_down.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);// 根据模板创建工作文件
			HSSFSheet sheet = wb.getSheetAt(0);// 获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);
			HSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 创建一个居中格式
			style.setLocked(false);// 设置不锁定
			style.setWrapText(true);
			
			for (int i = 0; i < b2BOrderItems.length; i++) {
				DBRow b2BOrderItem = b2BOrderItems[i];
				long pc_id = b2BOrderItem.get("b2b_pc_id", 0l);
				
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				
				row = sheet.createRow((int) i + 1);
				
				row.createCell(0).setCellValue(product.getString("p_name"));
				row.getCell(0).setCellStyle(style);
				
				row.createCell(1).setCellValue(b2BOrderItem.getString("b2b_delivery_count"));
				row.getCell(1).setCellStyle(style);
				
				row.createCell(2).setCellValue(b2BOrderItem.getString("b2b_backup_count"));
				row.getCell(2).setCellStyle(style);
				
				row.createCell(3).setCellValue(b2BOrderItem.getString("b2b_product_serial_number", ""));
				row.getCell(3).setCellStyle(style);
				
				row.createCell(4).setCellValue(b2BOrderItem.getString("b2b_box"));
				row.getCell(4).setCellStyle(style);
				
				row.createCell(5).setCellValue(b2BOrderItem.getString("lot_number"));
				row.getCell(5).setCellStyle(style);
			}
			String path = "upl_excel_tmp/B" + b2b_oid + "_" + DateUtil.FormatDatetime("yyyyMMddHHmmssSS", new Date())
					+ ".xls";
			FileOutputStream fout = new FileOutputStream(Environment.getHome() + path);
			wb.write(fout);
			fout.close();
			return (path);
		} catch (B2BOrderNoExistException e) {
			throw e;
		} catch (B2BOrderNoExistItemException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "downloadB2BOrder", log);
		}
	}
	
	/**
	 * 更新订单运单信息
	 */
	public void updateB2BOrderFreight(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			long fr_id = StringUtil.getLong(request, "fr_id");
			String b2b_order_waybill_name = StringUtil.getString(request, "b2b_order_waybill_name");
			String b2b_order_waybill_number = StringUtil.getString(request, "b2b_order_waybill_number");
			int b2b_orderby = StringUtil.getInt(request, "b2b_orderby");
			String carriers = StringUtil.getString(request, "carriers");
			String b2b_order_send_place = StringUtil.getString(request, "b2b_order_send_place");
			String b2b_order_receive_place = StringUtil.getString(request, "b2b_order_receive_place");
			
			DBRow updateRow = new DBRow();
			
			// 运输设置
			updateRow.add("fr_id", fr_id);
			updateRow.add("b2b_order_waybill_name", b2b_order_waybill_name);
			updateRow.add("b2b_order_waybill_number", b2b_order_waybill_number);
			updateRow.add("b2b_orderby", b2b_orderby);
			updateRow.add("carriers", carriers);
			updateRow.add("b2b_order_send_place", b2b_order_send_place);
			updateRow.add("b2b_order_receive_place", b2b_order_receive_place);
			
			floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, updateRow);
			
			this.editB2BOrderIndex(b2b_oid, "update");
			
			DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			AdminLoginBean adminLoginBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogsAndUpdateOrder(b2b_oid, "订单设置运单:单号" + b2b_oid, adminLoginBean.getAdid(),
					adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.Update, "",
					b2BOrderRow.get("b2b_order_status", 0));
			
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderFreight(HttpServletRequest request)", log);
		}
	}
	
	/**
	 * 上传发票文件
	 * 
	 * @param request
	 * @throws Exception
	 * @throws FileTypeException
	 * @throws FileException
	 */
	public void uploadB2BOrderInvoice(HttpServletRequest request) throws Exception, FileTypeException, FileException {
		try {
			String permitFile = "xls,doc,xlsx,docx";
			
			TUpload upload = new TUpload();
			
			upload.setFileName(DateUtil.FormatDatetime("yyyyMMddHHmmssSS", new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			
			int flag = upload.upload(request);
			
			long b2b_oid = Long.parseLong(upload.getRequestRow().getString("b2b_oid"));
			
			if (flag == 2) {
				throw new FileTypeException();// 文件上传格式不对
			} else if (flag == 1) {
				throw new FileException();
			} else {
				String[] temp = upload.getFileName().split("\\.");
				String fileType = temp[temp.length - 1];
				
				String fileName = "B" + b2b_oid + "." + fileType;
				String temp_url = Environment.getHome() + "upl_excel_tmp/" + upload.getFileName();
				String invoice_path = "invoice_file/" + fileName;
				String url = Environment.getHome() + invoice_path;
				FileUtil.moveFile(temp_url, url);
				
				DBRow para = new DBRow();
				para.add("invoice_path", invoice_path);
				
				floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, para);
			}
		} catch (FileTypeException e) {
			throw e;
		} catch (FileException e) {
			throw e;
		} catch (Exception e) {
			throw new SystemException(e, "uploadB2BOrderInvoice", log);
		}
	}
	
	/**
	 * 通过订单ID获取运费项目
	 */
	public DBRow[] getB2BOrderFreightCostByB2BOrderId(long b2b_oid, PageCtrl pc) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getB2BOrderFreightCostByB2BOrderId(b2b_oid, pc);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderFreightCostByB2BOrderId(long b2b_oid, PageCtrl pc)", log);
		}
	}
	
	/**
	 * 订单设置运单信息
	 */
	public DBRow b2BOrderSetFreight(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			String[] tfc_ids = request.getParameterValues("tfc_id");
			String[] tfc_project_names = request.getParameterValues("tfc_project_name");
			String[] tfc_ways = request.getParameterValues("tfc_way");
			String[] tfc_units = request.getParameterValues("tfc_unit");
			String[] tfc_unit_prices = request.getParameterValues("tfc_unit_price");
			String[] tfc_unit_counts = request.getParameterValues("tfc_unit_count");
			String[] tfc_currency = request.getParameterValues("tfc_currency");
			String[] tfc_exchange_rate = request.getParameterValues("tfc_exchange_rate");
			int changed = StringUtil.getInt(request, "changed", 0);
			
			double modFreightTotal = 0.0;
			// 修改运费时，如果修改最终的值比申请的值小，不能修改
			for (int i = 0; tfc_ids != null && i < tfc_ids.length; i++) {
				if (!"".equals(tfc_unit_prices[i]) && !"".equals(tfc_exchange_rate[i])
						&& !"".equals(tfc_unit_counts[i])) {
					modFreightTotal += Double.parseDouble(tfc_unit_prices[i])
							* Double.parseDouble(tfc_exchange_rate[i]) * Double.parseDouble(tfc_unit_counts[i]);
				}
			}
			int typeId = FinanceApplyTypeKey.B2B_ORDER;
			double applyFreight = applyFundsMgrZyj.getTransportFreightTotalApply(b2b_oid, typeId);
			DBRow result = new DBRow();
			SystemConfigIFace systemConfig = ((SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig"));
			if ((modFreightTotal >= applyFreight)
					|| (modFreightTotal < applyFreight && (Math.abs(applyFreight - modFreightTotal) <= Double
							.parseDouble(systemConfig.getStringConfigValue("exchange_rate_deviation"))
							* modFreightTotal * 0.01))) {
				if (changed == 2) {
					floorB2BOrderMgrZyj.deleteB2BOrderFreghtCostByB2BOrderId(b2b_oid);
				}
				
				for (int i = 0; tfc_ids != null && i < tfc_ids.length; i++) {
					DBRow row = new DBRow();
					row.add("tfc_id", tfc_ids[i]);
					row.add("b2b_oid", b2b_oid);
					row.add("tfc_project_name", tfc_project_names[i]);
					row.add("tfc_way", tfc_ways[i]);
					row.add("tfc_unit", tfc_units[i]);
					row.add("tfc_unit_price", tfc_unit_prices[i]);
					row.add("tfc_unit_count", tfc_unit_counts[i]);
					row.add("tfc_currency", tfc_currency[i]);
					row.add("tfc_exchange_rate", tfc_exchange_rate[i]);
					
					if (changed == 2 || row.getString("tfc_id").equals("") || row.getString("tfc_id").equals("0")) {
						row.remove("tfc_id");
						floorB2BOrderMgrZyj.addB2BOrderFreghtCost(row);
					} else {
						floorB2BOrderMgrZyj.updateB2BOrderFreghtCost(Long.parseLong(tfc_ids[i]), row);
					}
				}
				result.add("flag", "success");
			} else {
				result.add("flag", "fail");
				result.add("apply_freight", String.valueOf(applyFreight));
			}
			
			DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			AdminLoginBean adminLoginBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogsAndUpdateOrder(b2b_oid, "订单设置运费:单号" + b2b_oid, adminLoginBean.getAdid(),
					adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.Freight, "",
					b2BOrderRow.get("b2b_order_status", 0));
			return result;
		} catch (Exception e) {
			throw new SystemException(e, "b2BOrderSetFreight", log);
		}
	}
	
	/**
	 * 更新订单商品文件并刻录日志
	 */
	public void updateB2BOrderProductFileAndLogs(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			DBRow row = new DBRow();
			String b2b_order_date = StringUtil.getString(request, "b2b_order_date");
			TDate tDate = new TDate();
			TDate endDate = new TDate(b2b_order_date);
			
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			row.add("product_file_over", diffDay);
			row.add("product_file", B2BOrderProductFileKey.FINISH);
			floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, row);
			// 记录日志
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			
			this.insertLogsAndUpdateOrder(b2b_oid, "[商品文件]采集完成", adminLoginBean.getAdid(),
					adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.ProductShot, "", B2BOrderProductFileKey.FINISH);
			
			scheduleMgrZr.addScheduleReplayExternal(b2b_oid, ModuleKey.B2B_ORDER, 10, "订单" + b2b_oid + "[商品文件]采集完成",
					true, request, "b2b_order_product_file_period");
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderProductFileAndLogs", log);
		}
	}
	
	/**
	 * 多个商品。多个文件
	 */
	public void handleB2BOrderProductPictureUp(HttpServletRequest request) throws Exception {
		try {
			// 如果是多个商品的 那么注意多个商品都关联上.就是循环的添加
			// 有多个文件的时候循环的插入到product_file表.
			// 文件的命名为T_product_上传文件的名称.jpg
			// 如果是上传文件名有重复的那么就是要加上_index.jpg;
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			String pc_id = StringUtil.getString(request, "pc_id"); // 商品的Id
			String[] arrayPcId = pc_id.split(",");
			long file_with_id = StringUtil.getLong(request, "b2b_oid"); // 关联的业务Id
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class = StringUtil.getInt(request, "file_with_class"); // 这个目前写成商品的那种标签 比如 是商品本身，称重ID等等。
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			String fileNames = StringUtil.getString(request, "file_names");
			String[] fileNameArray = null;
			if (fileNames.trim().length() > 0) {
				fileNameArray = fileNames.trim().split(",");
			}
			
			if (fileNameArray != null && fileNameArray.length > 0) {
				StringBuffer logFileNameContent = new StringBuffer();
				String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
				for (String tempFileName : fileNameArray) {
					String tempSuffix = getSuffix(tempFileName);
					String tempfileUpName = tempFileName.replace("." + tempSuffix, "");
					StringBuffer realyFileName = new StringBuffer(sn);
					realyFileName.append("_").append(file_with_id).append("_").append(tempfileUpName);
					
					int indexFile = floorTransportMgrZr.getIndexOfProductFileByFileName(realyFileName.toString());
					if (indexFile != 0) {
						realyFileName.append("_").append(indexFile);
					}
					realyFileName.append(".").append(tempSuffix);
					logFileNameContent.append(",").append(realyFileName.toString());
					
					String tempUrl = baseTempUrl + tempFileName;
					// 移动文件到指定的
					StringBuffer url = new StringBuffer();
					url.append(Environment.getHome()).append("upload/").append(path).append("/")
							.append(realyFileName.toString());
					
					FileUtil.moveFile(tempUrl, url.toString());
					for (int index = 0, count = arrayPcId.length; index < count; index++) {
						DBRow file = new DBRow();
						file.add("file_name", realyFileName.toString());
						file.add("file_with_id", file_with_id);
						file.add("file_with_type", file_with_type);
						file.add("product_file_type", file_with_class);
						file.add("upload_adid", adminLoginBean.getAdid());
						file.add("upload_time", DateUtil.NowStr());
						file.add("pc_id", arrayPcId[index]);
						floorTransportMgrZr.addProductFile(file); // // 都是插入的同一张表 所以用这个方法;
					}
				}
			}
		} catch (Exception e) {
			throw new SystemException(e, "handleB2BOrderProductPictureUp", log);
		}
	}
	
	private String getSuffix(String filename) {
		String suffix = "";
		int pos = filename.lastIndexOf('.');
		if (pos > 0 && pos < filename.length() - 1) {
			suffix = filename.substring(pos + 1);
		}
		return suffix;
	}
	
	/**
	 * 上传订单运费跟进文件
	 */
	public void hanleB2BOrderStockInSet(HttpServletRequest request) throws Exception {
		try {
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			// 重新命名图片文件T_declaration_b2BOrderId_文件的名称_index.jpg
			// 移动图片位置
			// 多个文件上传的时候要循环的处理
			long file_with_id = StringUtil.getLong(request, "b2b_oid");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class = StringUtil.getInt(request, "stock_in_set");
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true : false)
					&& (file_with_class == B2BOrderStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH);
			StringBuffer fileNameSb = new StringBuffer("");
			if (isFileUp) {
				if (fileNames.trim().length() > 0) {
					String[] fileNameArray = fileNames.split(",");
					if (fileNameArray != null && fileNameArray.length > 0) {
						for (String tempFileName : fileNameArray) {
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request, "context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl = baseTempUrl + tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
									.append("_").append(tempFileName.replace("." + suffix, ""));
							int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
							if (indexFileName != 0) {
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/")
									.append(realFileName.toString());
							FileUtil.moveFile(tempUrl, url.toString());
							// file表上添加一条记录
							transportMgrZr.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type,
									file_with_class, adminLoginBean.getAdid(), DateUtil.NowStr());
						}
						
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if (!isFileUp) {
				String eta = StringUtil.getString(request, "eta");
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.Freight, eta, file_with_class);
			} else {
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.Freight, null, file_with_class);
			}
			DBRow updateB2BOrderRow = new DBRow();
			updateB2BOrderRow.add("stock_in_set", file_with_class);
			if (file_with_class == B2BOrderStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH) {
				// 如果是运费转账完成的话
				DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(file_with_id);
				
				double diffDay = this.getCostTimeOfB2BOrderProcess(file_with_id, 5,
						B2BOrderStockInSetKey.SHIPPINGFEE_SET, b2BOrderRow.getString("b2b_order_date"));
				
				updateB2BOrderRow.add("stock_in_set_over", diffDay);
			}
			floorB2BOrderMgrZyj.modB2BOrder(file_with_id, updateB2BOrderRow);
			
			boolean isCompelte = (file_with_class == B2BOrderStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH);
			String scheduleContext = "订单:" + file_with_id + StringUtil.getString(request, "context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.B2B_ORDER, 5, scheduleContext, isCompelte,
					request, "b2b_order_stock_in_set_period");
		} catch (Exception e) {
			throw new SystemException(e, "hanleB2BOrderStockInSet", log);
		}
	}
	
	/**
	 * 得到流程所花费的时间 查询日志中的记录的第一条日志作为开始的点。 如果没有那么就是用创建时间的作为开始的点
	 * 
	 * @param b2b_oid
	 * @return
	 */
	private double getCostTimeOfB2BOrderProcess(long b2b_oid, int b2b_order_type, int activity_id,
			String b2BOrderCreateTime) throws Exception {
		try {
			DBRow firstLogClearance = floorB2BOrderMgrZyj.getFirstB2BOrderIngLogTime(b2b_oid, b2b_order_type,
					activity_id);
			String firstTime = "";
			if (firstLogClearance != null) {
				firstTime = firstLogClearance.getString("b2b_order_date");
			}
			if (firstTime.trim().length() < 1) {
				firstTime = b2BOrderCreateTime;
			}
			TDate tDate = new TDate();
			TDate endDate = new TDate(firstTime);
			// 流程的完成时所需时间
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			;
			return diffDay;
		} catch (Exception e) {
			throw new SystemException(e, "getCostTimeOfB2BOrderProcess", log);
		}
	}
	
	/**
	 * 跟进订单报关
	 */
	public void handleB2BOrderDeclaration(HttpServletRequest request) throws Exception {
		try {
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			// 重新命名图片文件T_declaration_b2BOrderId_文件的名称_index.jpg
			// 移动图片位置
			// 多个文件上传的时候要循环的处理
			long file_with_id = StringUtil.getLong(request, "b2b_oid");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class = StringUtil.getInt(request, "declaration");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true : false)
					&& (file_with_class == B2BOrderDeclarationKey.FINISH);
			StringBuffer fileNameSb = new StringBuffer("");
			if (isFileUp) {
				if (fileNames.trim().length() > 0) {
					String[] fileNameArray = fileNames.split(",");
					if (fileNameArray != null && fileNameArray.length > 0) {
						for (String tempFileName : fileNameArray) {
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request, "context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl = baseTempUrl + tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
									.append("_").append(tempFileName.replace("." + suffix, ""));
							int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
							if (indexFileName != 0) {
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/")
									.append(realFileName.toString());
							FileUtil.moveFile(tempUrl, url.toString());
							// file表上添加一条记录
							transportMgrZr.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type,
									file_with_class, adminLoginBean.getAdid(), DateUtil.NowStr());
							
						}
						
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if (!isFileUp) {
				String eta = StringUtil.getString(request, "eta");
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.ExportCustoms, eta, file_with_class);
			} else {
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.ExportCustoms, null, file_with_class);
			}
			DBRow updateB2BOrderRow = new DBRow();
			updateB2BOrderRow.add("declaration", file_with_class);
			if (file_with_class == B2BOrderDeclarationKey.FINISH) {
				DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(file_with_id);
				double diffDay = this.getCostTimeOfB2BOrderProcess(file_with_id, 7, B2BOrderDeclarationKey.DELARATING,
						b2BOrderRow.getString("b2b_order_date"));
				updateB2BOrderRow.add("declaration_over", diffDay);
			}
			floorB2BOrderMgrZyj.modB2BOrder(file_with_id, updateB2BOrderRow);
			
			boolean isCompelte = (file_with_class == B2BOrderDeclarationKey.FINISH);
			String scheduleContext = "订单:" + file_with_id + StringUtil.getString(request, "context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.B2B_ORDER, 7, scheduleContext, isCompelte,
					request, "b2b_order_declaration_period");
			
		} catch (Exception e) {
			throw new SystemException(e, "handleB2BOrderDeclaration", log);
		}
	}
	
	/**
	 * 更新订单清关
	 */
	public void handleB2BOrderClearance(HttpServletRequest request) throws Exception {
		try {
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			// 如果是完成那么是要记录文件路径,记录日志.更新主表的记录。
			// 移动文件的位置。如果是有待上传的文件
			// 文件的命名规范为 T_clearance_阶段状态_232323_index.jpg;
			// 清关文件支持多个
			
			long file_with_id = StringUtil.getLong(request, "b2b_oid");
			int file_with_class = StringUtil.getInt(request, "clearance");
			
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true : false)
					&& (file_with_class == B2BOrderClearanceKey.FINISH);
			StringBuffer fileNameSb = new StringBuffer();
			if (fileNames.trim().length() > 0) {
				String[] fileNameArray = fileNames.split(",");
				if (fileNameArray != null && fileNameArray.length > 0) {
					for (String tempFileName : fileNameArray) {
						StringBuffer context = new StringBuffer();
						context.append(StringUtil.getString(request, "context"));
						// 获取真实的名字,把文件该名称
						// 清关文件支持多个,文件名称T_clearance_b2BOrderId_index
						
						// 1.移动文件
						String suffix = getSuffix(tempFileName);
						String tempUrl = baseTempUrl + tempFileName;
						
						StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id).append("_")
								.append(tempFileName.replace("." + suffix, ""));
						int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
						if (indexFileName != 0) {
							realFileName.append("_").append(indexFileName);
						}
						realFileName.append(".").append(suffix);
						fileNameSb.append(",").append(realFileName);
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append("upload/").append(path).append("/")
								.append(realFileName.toString());
						
						FileUtil.moveFile(tempUrl, url.toString());
						
						// file表上添加一条记录
						transportMgrZr.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type,
								file_with_class, adminLoginBean.getAdid(), DateUtil.NowStr());
						
					}
					
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if (!isFileUp) {
				// 如果不是上传文件。那么是要有eta的
				String eta = StringUtil.getString(request, "eta");
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.ImportCustoms, eta, file_with_class);
				
			} else {
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.ImportCustoms, null, file_with_class);
			}
			
			// 更新主b2BOrder上的信息表示当前清关已经完成
			DBRow updateB2BOrderRow = new DBRow();
			updateB2BOrderRow.add("clearance", file_with_class);
			if (file_with_class == B2BOrderClearanceKey.FINISH) {
				DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(file_with_id);
				updateB2BOrderRow.add("clearance_over", this.getCostTimeOfB2BOrderProcess(file_with_id, 6,
						B2BOrderClearanceKey.CLEARANCEING, b2BOrderRow.getString("b2b_order_date")));
			}
			floorB2BOrderMgrZyj.modB2BOrder(file_with_id, updateB2BOrderRow);
			
			// 任务的跟进
			boolean isCompelte = (file_with_class == B2BOrderClearanceKey.FINISH);
			String scheduleContext = "订单:" + file_with_id + StringUtil.getString(request, "context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.B2B_ORDER, 6, scheduleContext, isCompelte,
					request, "b2b_order_clearance_period");
			
		} catch (Exception e) {
			throw new SystemException(e, "handleB2BOrderClearance", log);
		}
		
	}
	
	/**
	 * 跟进订单内部标签
	 */
	public void handleB2BOrderTag(HttpServletRequest request) throws Exception {
		try {
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			// 重新命名图片文件T_tag_b2BOrderId_文件的名称_index.jpg
			// 移动图片位置
			// 多个文件上传的时候要循环的处理
			long file_with_id = StringUtil.getLong(request, "b2b_oid");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class = StringUtil.getInt(request, "tag");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			if (B2BOrderTagKey.FINISH_PURCHASE == file_with_class) {
				file_with_class = B2BOrderTagKey.FINISH;
			}
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String fileNamesSelect = StringUtil.getString(request, "file_names_select");
			if (!"".equals(fileNames) && !"".equals(fileNamesSelect)) {
				fileNames += "," + fileNamesSelect;
			}
			if (fileNames.startsWith(",")) {
				fileNames = fileNames.substring(1, fileNames.length() - 1);
			}
			String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true : false)
					&& (file_with_class == B2BOrderTagKey.FINISH);
			StringBuffer fileNameSb = new StringBuffer("");
			if (isFileUp) {
				if (fileNames.trim().length() > 0) {
					String[] fileNameArray = fileNames.split(",");
					if (fileNameArray != null && fileNameArray.length > 0) {
						for (String tempFileName : fileNameArray) {
							StringBuffer realFileName = new StringBuffer();
							if (tempFileName.endsWith("_select")) {
								realFileName.append(tempFileName.subSequence(0, tempFileName.length() - 7));
							} else {
								StringBuffer context = new StringBuffer();
								context.append(StringUtil.getString(request, "context"));
								String suffix = getSuffix(tempFileName);
								String tempUrl = baseTempUrl + tempFileName;
								
								realFileName = new StringBuffer(sn).append("_").append(file_with_id).append("_")
										.append(tempFileName.replace("." + suffix, ""));
								int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName
										.toString());
								if (indexFileName != 0) {
									realFileName.append("_").append(indexFileName);
								}
								realFileName.append(".").append(suffix);
								
								fileNameSb.append(",").append(realFileName);
								StringBuffer url = new StringBuffer();
								url.append(Environment.getHome()).append("upload/").append(path).append("/")
										.append(realFileName.toString());
								FileUtil.moveFile(tempUrl, url.toString());
							}
							// file表上添加一条记录
							transportMgrZr.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type,
									file_with_class, adminLoginBean.getAdid(), DateUtil.NowStr());
							
						}
						
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if (!isFileUp) {
				String eta = StringUtil.getString(request, "eta");
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.Label, eta, file_with_class);
			} else {
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.Label, null, file_with_class);
			}
			DBRow updateB2BOrderRow = new DBRow();
			updateB2BOrderRow.add("tag", file_with_class);
			if (file_with_class == B2BOrderTagKey.FINISH) {
				// 如果是运费转账完成的话
				DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(file_with_id);
				
				double diffDay = this.getDiffTimeByB2BOrderCreateTime(b2BOrderRow.getString("b2b_order_date"));
				
				// getCostTimeOfB2BOrderProcess
				
				updateB2BOrderRow.add("tag_over", diffDay);
			}
			floorB2BOrderMgrZyj.modB2BOrder(file_with_id, updateB2BOrderRow);
			
			boolean isCompelte = (file_with_class == B2BOrderTagKey.FINISH);
			String scheduleContext = "订单:" + file_with_id + StringUtil.getString(request, "context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.B2B_ORDER, 8, scheduleContext.toString(),
					isCompelte, request, "b2b_order_tag_period");
			
		} catch (Exception e) {
			throw new SystemException(e, "handleB2BOrderTag", log);
		}
	}
	
	private double getDiffTimeByB2BOrderCreateTime(String b2b_order_date) throws Exception {
		try {
			TDate tDate = new TDate();
			TDate endDate;
			
			endDate = new TDate(b2b_order_date);
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			return diffDay;
		} catch (Exception e) {
			throw new SystemException(e, "getDiffTimeByB2BOrderCreateTime", log);
		}
		
	}
	
	/**
	 * 商品标签是否全部上传完全
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String checkB2BOrderProductTagFileFinish(HttpServletRequest request) throws Exception {
		try {
			long file_with_id = StringUtil.getLong(request, "b2b_oid");
			StringBuffer prompt = new StringBuffer();
			
			int[] fileType = { FileWithTypeKey.B2B_ORDER_PRODUCT_TAG_FILE };
			String value = systemConfig.getStringConfigValue("b2b_order_tag_types");
			if (!"".equals(value)) {
				String[] arraySelected = value.split("\n");
				for (int i = 0; i < arraySelected.length; i++) {
					if (!"".equals(arraySelected[i])) {
						String[] typeIdName = arraySelected[i].split("=");
						if (2 == typeIdName.length) {
							int[] typeId = { Integer.parseInt(typeIdName[0]) };
							String typeName = typeIdName[1];
							DBRow[] tagFile = floorPurchaseMgrZyj.getAllProductFileByPcId(fileType, typeId,
									file_with_id);
							if (0 == tagFile.length) {
								prompt.append("请上传" + typeName + "的照片").append(",");
							}
						}
					}
				}
			}
			if (prompt.length() > 0) {
				return prompt.substring(0, prompt.length() - 1);
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "checkB2BOrderProductTagFileFinish", log);
		}
	}
	
	/**
	 * 跟进订单第三方标签
	 */
	public void handleB2BOrderThirdTag(HttpServletRequest request) throws Exception {
		try {
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			// 重新命名图片文件T_tag_b2BOrderId_文件的名称_index.jpg
			// 移动图片位置
			// 多个文件上传的时候要循环的处理
			long file_with_id = StringUtil.getLong(request, "b2b_oid");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class = StringUtil.getInt(request, "tag_third");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true : false)
					&& (file_with_class == B2BOrderTagKey.FINISH);
			StringBuffer fileNameSb = new StringBuffer("");
			if (isFileUp) {
				if (fileNames.trim().length() > 0) {
					String[] fileNameArray = fileNames.split(",");
					if (fileNameArray != null && fileNameArray.length > 0) {
						for (String tempFileName : fileNameArray) {
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request, "context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl = baseTempUrl + tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
									.append("_").append(tempFileName.replace("." + suffix, ""));
							int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
							if (indexFileName != 0) {
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/")
									.append(realFileName.toString());
							FileUtil.moveFile(tempUrl, url.toString());
							// file表上添加一条记录
							transportMgrZr.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type,
									file_with_class, adminLoginBean.getAdid(), DateUtil.NowStr());
						}
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if (!isFileUp) {
				String eta = StringUtil.getString(request, "eta");
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.THIRD_TAG, eta, file_with_class);
			} else {
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.THIRD_TAG, null, file_with_class);
			}
			DBRow updateB2BOrderRow = new DBRow();
			updateB2BOrderRow.add("tag_third", file_with_class);
			if (file_with_class == B2BOrderTagKey.FINISH) {
				// 如果是运费转账完成的话
				DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(file_with_id);
				
				double diffDay = this.getDiffTimeByB2BOrderCreateTime(b2BOrderRow.getString("b2b_order_date"));
				
				updateB2BOrderRow.add("tag_third_over", diffDay);
			}
			floorB2BOrderMgrZyj.modB2BOrder(file_with_id, updateB2BOrderRow);
			
			boolean isCompelte = (file_with_class == B2BOrderTagKey.FINISH);
			String scheduleContext = "订单:" + file_with_id + StringUtil.getString(request, "context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.THIRD_TAG,
					scheduleContext.toString(), isCompelte, request, "b2b_order_third_tag_period");
			
		} catch (Exception e) {
			throw new SystemException(e, "handleB2BOrderThirdTag", log);
		}
	}
	
	/**
	 * 上传商品标签文件
	 */
	public String saveB2BOrderProductTagFile(HttpServletRequest request) throws Exception {
		try {
			// 这里上传的文件是保存在product_file这张表中pc_id
			// 保存的文件名为B_product_121212_商品分类_index(商品分类)
			// 1=AMZON标签
			// 2=UPC标签
			// 记录日志 日志中
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			
			String pc_id = StringUtil.getString(request, "pc_id"); // 商品的Ids
			String[] arrayPcIds = pc_id.split(",");
			long file_with_id = StringUtil.getLong(request, "b2b_oid"); // 关联的业务Id
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class = StringUtil.getInt(request, "file_with_class"); // 这个目前写成商品的那种标签
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			// 得到上传的临时文件
			String fileNames = StringUtil.getString(request, "file_names");
			String[] fileNameArray = null;
			if (fileNames.trim().length() > 0) {
				fileNameArray = fileNames.trim().split(",");
			}
			if (fileNameArray != null && fileNameArray.length > 0) {
				StringBuffer logFileNameContent = new StringBuffer();
				String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
				for (String tempFileName : fileNameArray) {
					String tempSuffix = getSuffix(tempFileName);
					String tempfileUpName = tempFileName.replace("." + tempSuffix, "");
					StringBuffer realyFileName = new StringBuffer(sn);
					realyFileName.append("_").append(file_with_id).append("_").append(tempfileUpName);
					int indexFile = floorTransportMgrZr.getIndexOfProductFileByFileName(realyFileName.toString());
					if (indexFile != 0) {
						realyFileName.append("_").append(indexFile);
					}
					realyFileName.append(".").append(tempSuffix);
					logFileNameContent.append(",").append(realyFileName.toString());
					String tempUrl = baseTempUrl + tempFileName;
					StringBuffer url = new StringBuffer();
					url.append(Environment.getHome()).append("upload/").append(path).append("/")
							.append(realyFileName.toString());
					
					FileUtil.moveFile(tempUrl, url.toString());
					for (int index = 0, count = arrayPcIds.length; index < count; index++) {
						DBRow file = new DBRow();
						file.add("file_name", realyFileName.toString());
						file.add("file_with_id", file_with_id);
						file.add("file_with_type", file_with_type);
						file.add("product_file_type", file_with_class);
						file.add("upload_adid", adminLoginBean.getAdid());
						file.add("upload_time", DateUtil.NowStr());
						file.add("pc_id", arrayPcIds[index]);
						floorTransportMgrZr.addProductFile(file); // // 都是插入的同一张表 所以用这个方法;
					}
				}
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "saveB2BOrderProductTagFile", log);
		}
		
	}
	
	/**
	 * 跟进订单单证
	 */
	public void b2BOrderCertificateFollowUp(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			String context = "订单:" + b2b_oid + StringUtil.getString(request, "context");
			String eta = StringUtil.getString(request, "eta");
			int certificate = StringUtil.getInt(request, "certificate");
			boolean isover = false;
			if (certificate == B2BOrderCertificateKey.FINISH) {
				eta = null;
				DBRow row = new DBRow();
				String b2b_order_date = StringUtil.getString(request, "b2b_order_date");
				TDate tDate = new TDate();
				TDate endDate = new TDate(b2b_order_date);
				double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
				row.add("certificate_over", diffDay); // 单证减去开始时间
				row.add("certificate", B2BOrderCertificateKey.FINISH);
				floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, row);
				isover = true;
			}
			AdminLoginBean adminLoginBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogsAndUpdateOrder(b2b_oid, context, adminLoginBean.getAdid(), adminLoginBean.getEmploye_name(),
					B2BOrderLogTypeKey.Document, eta, certificate);
			scheduleMgrZr.addScheduleReplayExternal(b2b_oid, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Document, context,
					isover, request, "b2b_order_certificate_period");
			
		} catch (Exception e) {
			throw new SystemException(e, "b2BOrderCertificateFollowUp", log);
		}
	}
	
	/**
	 * 添加订单单证文件
	 */
	public void addB2BOrderCertificateAddFile(HttpServletRequest request) throws Exception {
		try {
			
			AdminLoginBean adminLoginBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			long file_with_id = StringUtil.getLong(request, "file_with_id", 0l);
			int file_with_type = StringUtil.getInt(request, "file_with_type", 0);
			int file_with_class = StringUtil.getInt(request, "file_with_class", 0);
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			String file_names = StringUtil.getString(request, "file_names");
			String[] fileNameArray = null;
			String baseUrl = Environment.getHome() + "upl_imags_tmp/";
			StringBuffer logContext = new StringBuffer();
			if (file_names.trim().length() > 0) {
				fileNameArray = file_names.trim().split(",");
			}
			if (fileNameArray != null && fileNameArray.length > 0) {
				for (String tempFileName : fileNameArray) {
					// 真是的文件名称
					StringBuffer realyFileName = new StringBuffer(sn);
					String suffix = this.getSuffix(tempFileName);
					realyFileName.append("_").append(file_with_id).append("_")
							.append(tempFileName.replace("." + suffix, ""));
					int indexFile = floorTransportMgrZr.getIndexOfFilebyFileName(realyFileName.toString());
					if (indexFile != 0) {
						realyFileName.append("_").append(indexFile);
					}
					
					realyFileName.append(".").append(suffix);
					logContext.append(",").append(realyFileName.toString());
					// 插入file 表中
					transportMgrZr.addTransportFile(realyFileName.toString(), file_with_id, file_with_type,
							file_with_class, adminLoginBean.getAdid(), DateUtil.NowStr());
					// 移动文件
					FileUtil.moveFile(baseUrl + tempFileName, Environment.getHome() + "upload/" + path + "/"
							+ realyFileName.toString());
				}
				
			}
			// 添加日志记录一条
			String context = "";
			if (logContext.length() > 1) {
				context = logContext.substring(1);
			}
			this.insertLogsAndUpdateOrder(file_with_id, getB2BOrderCertificateFromConfigJsp(file_with_class) + "上传文件"
					+ context, adminLoginBean.getAdid(), adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.Document,
					null, file_with_class);
			
		} catch (Exception e) {
			throw new SystemException(e, "addB2BOrderCertificateAddFile", log);
		}
		
	}
	
	/**
	 * 得到订单单证类型
	 * 
	 * @param fileWithClass
	 * @return
	 * @throws Exception
	 */
	public String getB2BOrderCertificateFromConfigJsp(int fileWithClass) throws Exception {
		try {
			SystemConfigIFace systemConfig = (SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig");
			String value = systemConfig.getStringConfigValue("b2b_order_certificate");
			String[] arraySelected = value.split("\n");
			// 将arraySelected组成一个List
			ArrayList<String> selectedList = new ArrayList<String>();
			for (int index = 0, count = arraySelected.length; index < count; index++) {
				String tempStr = arraySelected[index];
				if (tempStr.indexOf("=") != -1) {
					String[] tempArray = tempStr.split("=");
					String tempHtml = tempArray[1];
					selectedList.add(tempHtml);
				}
			}
			
			return selectedList.get(fileWithClass - 1);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderCertificateFromConfigJsp", log);
		}
	}
	
	/**
	 * 检验订单商品文件是否可以完成
	 */
	public String checkB2BOrderProductFileFinish(long b2b_oid) throws Exception {
		try {
			StringBuffer prompt = new StringBuffer();
			int[] types = { FileWithTypeKey.B2B_ORDER_PRODUCT_FILE };
			String value = systemConfig.getStringConfigValue("b2b_order_product_file");
			if (!"".equals(value)) {
				String[] arraySelected = value.split("\n");
				for (int i = 0; i < arraySelected.length; i++) {
					if (!"".equals(arraySelected[i])) {
						String[] typeIdName = arraySelected[i].split("=");
						if (2 == typeIdName.length) {
							int[] typeId = { Integer.parseInt(typeIdName[0]) };
							String typeName = typeIdName[1];
							DBRow[] productPackages = floorPurchaseMgrZyj.getAllProductFilesByProductModelAndTypes(
									b2b_oid, types, typeId);
							if (0 == productPackages.length) {
								prompt.append("请上传" + typeName + "的照片").append(",");
							}
						}
					}
				}
			}
			if (prompt.length() > 0) {
				return prompt.substring(0, prompt.length() - 1);
			}
			return "";
		} catch (Exception e) {
			throw new SystemException(e, "checkB2BOrderProductFileFinish", log);
		}
	}
	
	/**
	 * 跟进订单商品文件
	 */
	public void b2BOrderProductFileFollowUp(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			String context = "订单:" + b2b_oid + StringUtil.getString(request, "context");
			String eta = StringUtil.getString(request, "eta");
			int productFile = StringUtil.getInt(request, "product_file");
			boolean isover = false;
			if (productFile == B2BOrderProductFileKey.FINISH) {
				eta = null;
				DBRow row = new DBRow();
				String b2b_order_date = StringUtil.getString(request, "b2b_order_date");
				TDate tDate = new TDate();
				TDate endDate = new TDate(b2b_order_date);
				double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
				row.add("product_file_over", diffDay); // 商品图片减去开始时间
				row.add("product_file", B2BOrderProductFileKey.FINISH);
				floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, row);
				isover = true;
			}
			AdminLoginBean adminLoginBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogsAndUpdateOrder(b2b_oid, context, adminLoginBean.getAdid(), adminLoginBean.getEmploye_name(),
					B2BOrderLogTypeKey.ProductShot, eta, productFile);
			scheduleMgrZr.addScheduleReplayExternal(b2b_oid, ModuleKey.B2B_ORDER, 10, context, isover, request,
					"b2b_order_product_file_period");
			
		} catch (Exception e) {
			throw new SystemException(e, "b2BOrderProductFileFollowUp", log);
		}
	}
	
	/**
	 * 将订单实物图片流程阶段从完成改成需要
	 */
	public void updateB2BOrderPcModelFinishToNeed(HttpServletRequest request) throws Exception {
		try {
			// 修改订单实物图片状态
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			String context = StringUtil.getString(request, "context");
			String e_finish_time = StringUtil.getString(request, "eta");
			DBRow b2BOrderRow = new DBRow();
			b2BOrderRow.add("product_file", B2BOrderProductFileKey.PRODUCTFILE);
			floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, b2BOrderRow);
			// 加日志
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			long adid = adminLoginBean.getAdid();
			String employeeName = adminLoginBean.getEmploye_name();
			
			this.insertLogsAndUpdateOrder(b2b_oid, context + "(从完成到需要)", adid, employeeName,
					B2BOrderLogTypeKey.ProductShot, e_finish_time, B2BOrderProductFileKey.PRODUCTFILE);
			
			// 任务跟进
			DBRow schedule = scheduleMgrZr.getScheduleByAssociate(b2b_oid, ModuleKey.B2B_ORDER,
					B2BOrderLogTypeKey.ProductShot);
			if (null != schedule) {
				String contentPcFile = adminLoginBean.getEmploye_name() + "确认了订单:" + b2b_oid + "(从完成到需要)," + context;
				scheduleMgrZr.addScheduleReplayExternal(b2b_oid, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.ProductShot,
						contentPcFile, false, request, "b2b_order_product_file_period");
			}
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderPcModelFinishToNeed", log);
		}
	}
	
	/**
	 * 上传质检文件
	 */
	public void hanleB2BOrderQualityInspection(HttpServletRequest request) throws Exception {
		try {
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			
			// 重新命名图片文件T_quality_inspection文件的名称_index.jpg
			// 移动图片位置
			// 多个文件上传的时候要循环的处理
			long file_with_id = StringUtil.getLong(request, "b2b_oid");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class = StringUtil.getInt(request, "quality_inspection");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome() + "upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true : false);
			StringBuffer fileNameSb = new StringBuffer("");
			if (isFileUp) {
				if (fileNames.trim().length() > 0) {
					String[] fileNameArray = fileNames.split(",");
					if (fileNameArray != null && fileNameArray.length > 0) {
						for (String tempFileName : fileNameArray) {
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request, "context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl = baseTempUrl + tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
									.append("_").append(tempFileName.replace("." + suffix, ""));
							int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
							if (indexFileName != 0) {
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/")
									.append(realFileName.toString());
							FileUtil.moveFile(tempUrl, url.toString());
							// file表上添加一条记录
							transportMgrZr.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type,
									file_with_class, adminLoginBean.getAdid(), DateUtil.NowStr());
						}
						
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if (!isFileUp) {
				String eta = StringUtil.getString(request, "eta");
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.QualityControl, eta, file_with_class);
			} else {
				if ("".equals(context)) {
					context = "上传文件";
				}
				this.insertLogsAndUpdateOrder(file_with_id, context, adminLoginBean.getAdid(),
						adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.QualityControl, null, file_with_class);
			}
			
			DBRow updateB2BOrderRow = new DBRow();
			updateB2BOrderRow.add("quality_inspection", file_with_class);
			if (file_with_class == B2BOrderQualityInspectionKey.FINISH) {
				// 如果是报告完成的话那么就是整体完成
				DBRow b2BOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(file_with_id);
				// 质检是用开始时间来计算的
				double diffDay = this.getDiffTimeByB2BOrderCreateTime(b2BOrderRow.getString("b2b_order_date"));
				
				updateB2BOrderRow.add("quality_inspection_over", diffDay);
			}
			floorB2BOrderMgrZyj.modB2BOrder(file_with_id, updateB2BOrderRow);
			
			boolean isCompelte = (file_with_class == B2BOrderQualityInspectionKey.FINISH);
			String scheduleContext = "订单:" + file_with_id + StringUtil.getString(request, "context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.B2B_ORDER, 11, scheduleContext, isCompelte,
					request, "b2b_order_quality_inspection");
			
		} catch (Exception e) {
			throw new SystemException(e, "hanleB2BOrderQualityInspection", log);
		}
	}
	
	/**
	 * 跟进订单单证
	 */
	public void updateB2BOrderCertificateAndLogs(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			DBRow row = new DBRow();
			String b2b_order_date = StringUtil.getString(request, "b2b_order_date");
			TDate tDate = new TDate();
			TDate endDate = new TDate(b2b_order_date);
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			row.add("certificate_over", diffDay); // 单证减去开始时间
			row.add("certificate", B2BOrderCertificateKey.FINISH);
			floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, row);
			
			// 记录日志
			AdminLoginBean adminLoginBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr"))
					.getAdminLoginBean(StringUtil.getSession(request));
			
			this.insertLogsAndUpdateOrder(b2b_oid, "[单证上传完成]", adminLoginBean.getAdid(),
					adminLoginBean.getEmploye_name(), B2BOrderLogTypeKey.Document, null, B2BOrderCertificateKey.FINISH);
			scheduleMgrZr.addScheduleReplayExternal(b2b_oid, ModuleKey.B2B_ORDER, B2BOrderLogTypeKey.Document, "订单:"
					+ b2b_oid + "[单证上传完成]", true, request, "b2b_order_certificate_period");
			
		} catch (Exception e) {
			throw new SystemException(e, "updateB2BOrderCertificateAndLogs", log);
		}
		
	}
	
	/**
	 * 添加订单日志
	 */
	public void addB2BOrderLog(HttpServletRequest request) throws Exception {
		try {
			// 在表b2b_order_log 中添加字段 activity_id 这里的stage 就是这个字段的key值.
			// b2b_oid ， b2b_content(跟进内容),eta(具体内容的完成时间),b2b_type 跟进类型主Key,stage
			// stage表示的第二级跟进的key的value
			// b2b_type 操作日志类型:1表示进度跟进2:表示财务跟进;3修改日志;1货物状态;5运费流程;6进口清关;7出口报关8标签流程;9:单证流程
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			String b2b_content = StringUtil.getString(request, "b2b_content");
			String eta = StringUtil.getString(request, "eta");
			int b2b_type = StringUtil.getInt(request, "b2b_type");
			int stage = StringUtil.getInt(request, "stage");
			AdminLoginBean adminLoginBean = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request));
			this.insertLogsAndUpdateOrder(b2b_oid, b2b_content, adminLoginBean.getAdid(),
					adminLoginBean.getEmploye_name(), b2b_type, eta, stage);
		} catch (Exception e) {
			throw new SystemException(e, "addB2BOrderLog", log);
		}
	}
	
	/**
	 * 更新订单体积和重量
	 */
	public void updateB2BOrderItemVW(long b2b_oid) throws Exception {
		DBRow[] b2BOrderItems;
		
		if (b2b_oid == 0) {
			b2BOrderItems = floorB2BOrderMgrZyj.getB2BOrderItemByB2BOrderId(0L, null, null, null, null);
		} else {
			b2BOrderItems = floorB2BOrderMgrZyj.getB2BOrderItemByB2BOrderId(b2b_oid, null, null, null, null);
		}
		
		DBRow product = null;
		float product_volume = 0;
		float product_weight = 0;
		for (int i = 0; i < b2BOrderItems.length; i++) {
			
			if (b2BOrderItems[i].get("b2b_volume", 0f) == 0f) {
				product = floorProductMgr.getDetailProductByPcid(b2BOrderItems[i].get("b2b_pc_id", 0l));
				if (product != null) {
					product_volume = product.get("volume", 0f);
					product_weight = product.get("weight", 0f);
				}
				
				DBRow para = new DBRow();
				para.add("b2b_volume", product_volume);
				para.add("b2b_weight", product_weight);
				
				floorB2BOrderMgrZyj.modB2BOrderItem(b2BOrderItems[i].get("b2b_detail_id", 0l), para);
			}
		}
	}
	
	/**
	 * 删除订单
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void delB2BOrder(HttpServletRequest request) throws Exception {
		try {
			long b2b_oid = StringUtil.getLong(request, "b2b_oid");
			
			// 删除订单索引
			this.editB2BOrderIndex(b2b_oid, "del");
			
			floorB2BOrderMgrZyj.delB2BOrder(b2b_oid);
			floorB2BOrderMgrZyj.delB2BOrderItemByB2BOrderId(b2b_oid);
			// 删除日志
			floorB2BOrderMgrZyj.deleteB2BOrderLogsByB2BOrderId(b2b_oid);
			floorB2BOrderMgrZyj.deleteB2BOrderFreghtCostByB2BOrderId(b2b_oid);
			// 删除文件，(单证，清关，报关，运费，制签)需要根据关联ID，关联类型的数组，因为采购和订单可能冲突，导致误删
			int[] types = { FileWithTypeKey.B2B_ORDER_CERTIFICATE, FileWithTypeKey.B2B_ORDER_CLEARANCE,
					FileWithTypeKey.B2B_ORDER_DECLARATION, FileWithTypeKey.B2B_ORDER_STOCKINSET,
					FileWithTypeKey.B2B_ORDER_TAG };
			transportMgrZyj.deleteTransportFileByTransportIdAndTypes(b2b_oid, types);
			// 删除订单下商品的文件(7,16)(商品文件，商品标签文件)
			int[] productTypes = { FileWithTypeKey.B2B_ORDER_PRODUCT_FILE, FileWithTypeKey.B2B_ORDER_PRODUCT_TAG_FILE };
			transportMgrZyj.deleteTransportProductFileByTransportIdAndTypes(b2b_oid, productTypes);
			// 删除任务
			this.deleteB2BOrderSchedule(b2b_oid, request);
		} catch (Exception e) {
			throw new SystemException(e, "delB2BOrder", log);
		}
	}
	
	/**
	 * 删除任务
	 * 
	 * @throws Exception
	 */
	private void deleteB2BOrderSchedule(long b2b_oid, HttpServletRequest request) throws Exception {
		try {
			// 各流程任务
			int[] associateTypeArr = { ModuleKey.B2B_ORDER };
			DBRow[] b2BOrderSchedules = scheduleMgrZr.getScheduleByAssociateIdAndType(b2b_oid, associateTypeArr);
			for (int i = 0; i < b2BOrderSchedules.length; i++) {
				scheduleMgrZr.deleteScheduleByScheduleId(b2BOrderSchedules[i].get("schedule_id", 0L), request);
			}
		} catch (Exception e) {
			throw new SystemException(e, "deleteB2BOrderSchedule", log);
		}
	}
	
	/**
	 * 通过订单ID和商品名查询订单明细
	 */
	public DBRow[] getB2BOrderProductByName(long b2b_oid, String name) throws Exception {
		try {
			return floorB2BOrderMgrZyj.getB2BOrderProductByName(b2b_oid, name);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderProductByName", log);
		}
	}
	
	public DBRow[] fillterB2BOrderItem(long b2b_oid, String name,String title) throws Exception {
		try {
			return floorB2BOrderMgrZyj.fillterB2BOrderItem(b2b_oid, name,title);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderProductByName", log);
		}
		
	}
	
	/**
	 * 根据订号生成需批量打印条码文件
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public String exportProductBarcodeByB2BOrder(HttpServletRequest request) throws Exception {
		FileOutputStream fout = null;
		
		try {
			String id = StringUtil.getString(request, "id");
			
			String regEx = "[^0-9]";
			Pattern p = Pattern.compile(regEx);
			Matcher m = p.matcher(id);
			long b2b_oid = 0;
			if (!m.replaceAll("").equals("")) {
				b2b_oid = Long.parseLong(m.replaceAll(""));
			}
			
			POIFSFileSystem fs = null;// 获得模板
			fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()
					+ "/administrator/product/ExportBarcodeModel.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);// 根据模板创建工作文件
			HSSFSheet sheet = wb.getSheetAt(0);// 获得模板的第一个sheet
			HSSFRow row = sheet.getRow(0);
			
			HSSFCellStyle style = wb.createCellStyle();
			style.setLocked(false); // 创建一个非锁定样式
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			style.setWrapText(true);
			
			DBRow[] b2bOrderItems = floorB2BOrderMgrZyj.getB2BOrderItemByB2BOrderId(b2b_oid, null, null, null, null);
			
			if (b2bOrderItems != null && b2bOrderItems.length > 0) {
				for (int i = 0; i < b2bOrderItems.length; i++) {
					DBRow product = floorProductMgr.getDetailProductByPcid(b2bOrderItems[i].get("b2b_pc_id", 0l));
					
					row = sheet.createRow((int) i + 1);
					
					sheet.setDefaultColumnWidth(26);
					
					// 创建单元格，并设置值
					
					row.createCell(0).setCellValue(product.getString("p_name"));
					row.getCell(0).setCellStyle(style);
					
					row.createCell(1).setCellValue(product.getString("p_code"));
					row.getCell(1).setCellStyle(style);
					
					row.createCell(2).setCellValue(1);
					row.getCell(2).setCellStyle(style);
					
					row.createCell(3).setCellValue(1);
					row.getCell(3).setCellStyle(style);
				}
				
				String path = "upl_excel_tmp/Out_product_barcode_"
						+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS", new Date()) + ".xls";
				fout = new FileOutputStream(Environment.getHome() + path);
				wb.write(fout);
				fout.close();
				return (path);
			} else {
				return "0";
			}
		} catch (Exception e) {
			throw new SystemException(e, "exportProductBarcodeByB2BOrder", log);
		}
	}
	
	/**
	 * 根据转运明细ID获得转运明细
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getB2BOrderItemById(HttpServletRequest request) throws Exception {
		try {
			long b2b_detail_id = StringUtil.getLong(request, "b2b_detail_id");
			
			return floorB2BOrderMgrZyj.findB2BOrderItemByItemId(b2b_detail_id);
		} catch (Exception e) {
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 预算需保留样式
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 * @author zhanjie
	 */
	public DBRow[] showReceiveAllocate(long b2b_oid) throws Exception {
		try {
			DBRow[] transportDetails = floorB2BOrderMgrZyj.getWaitAllocateB2bOrderItem(b2b_oid);
			
			ArrayList<DBRow> receiveAllocateList = new ArrayList<DBRow>();
			for (int i = 0; i < transportDetails.length; i++) {
				long pc_id = transportDetails[i].get("b2b_pc_id", 0l);
				int count = (int) transportDetails[i].get("b2b_wait_count", 0f);
				long clp_type_id = transportDetails[i].get("clp_type_id", 0l);
				long blp_type_id = transportDetails[i].get("blp_type_id", 0l);
				long b2b_detail_id = transportDetails[i].get("b2b_detail_id", 0l);
				
				DBRow receiveAllocate = lpTypeMgrZJ.calculateProductLPCount(pc_id, count, clp_type_id, blp_type_id);
				receiveAllocate.add("b2b_detail_id", b2b_detail_id);
				
				receiveAllocateList.add(receiveAllocate);
			}
			
			return receiveAllocateList.toArray(new DBRow[0]);
		} catch (Exception e) {
			throw new SystemException(e, "getReceiveAllocateShow", log);
		}
	}
	
	@Override
	public DBRow[] getB2BOrderOrProductCountGroupCountry(HttpServletRequest request) throws Exception {
		try {
			long product_line = StringUtil.getLong(request, "product_line");
			long product_catalog = StringUtil.getLong(request, "product_catalog");
			String pc_name = StringUtil.getString(request, "pc_name");
			long title_id = StringUtil.getLong(request, "title_id");
			String start_time = StringUtil.getString(request, "start_time");
			String end_time = StringUtil.getString(request, "end_time");
			return floorB2BOrderMgrZyj.getB2BOrderOrProductCountGroupCountry(product_line, product_catalog, pc_name,
					title_id, start_time, end_time);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderOrProductCountGroupCountry", log);
		}
	}
	
	@Override
	public DBRow[] getB2BOrderOrProductCountGroupProvince(HttpServletRequest request) throws Exception {
		try {
			long product_line = StringUtil.getLong(request, "product_line");
			long product_catalog = StringUtil.getLong(request, "product_catalog");
			String pc_name = StringUtil.getString(request, "pc_name");
			long title_id = StringUtil.getLong(request, "title_id");
			long ccid = StringUtil.getLong(request, "ccid");
			String start_time = StringUtil.getString(request, "start_time");
			String end_time = StringUtil.getString(request, "end_time");
			return floorB2BOrderMgrZyj.getB2BOrderOrProductCountGroupProvince(product_line, product_catalog, pc_name,
					title_id, ccid, start_time, end_time);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderOrProductCountGroupProvince", log);
		}
	}
	
	/**
	 * product_count， deliver_ccid
	 */
	@Override
	public JsonObject getB2BOrderOrProductCountGroupCountryJson(HttpServletRequest request) throws Exception {
		try {
			DBRow row = new DBRow();
			row.add("b2b_product_count_country", this.getB2BOrderOrProductCountGroupCountry(request));
			return new JsonObject(row);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderOrProductCountGroupCountryJson", log);
		}
	}
	
	/**
	 * product_count， deliver_pro_id
	 */
	@Override
	public JsonObject getB2BOrderOrProductCountGroupProvinceJson(HttpServletRequest request) throws Exception {
		try {
			DBRow row = new DBRow();
			row.add("b2b_product_count_province", this.getB2BOrderOrProductCountGroupProvince(request));
			return new JsonObject(row);
		}
		
		catch (Exception e) {
			throw new SystemException(e, "getB2BOrderOrProductCountGroupProvinceJson", log);
		}
	}
	
	public DBRow[] getB2BOrderOrProductCountAnalyze(String start_time, String end_time, String catalog_id,
			String pro_line_id, String pc_name, long ca_id, long ccid, long pro_id, long cid, int type,
			String title_id, PageCtrl pc) throws Exception {
		try {
			// return floorB2BOrderMgrZyj.getB2BOrderOrProductCountAnalyze(product_line, product_catalog, pc_name,
			// title_id, ccid, start_time, end_time);
			return floorB2BOrderMgrZyj.getB2BOrderOrProductCountAnalyze(start_time, end_time, catalog_id, pro_line_id,
					pc_name, ca_id, ccid, pro_id, cid, type, title_id, pc);
		} catch (Exception e) {
			throw new SystemException(e, "getB2BOrderOrProductCountAnalyze", log);
		}
	}
	
	public String exportB2BOrderOrProductCountAnalyze(HttpServletRequest request) throws Exception {
		try {
			String start_time = StringUtil.getString(request, "start_time");
			String end_time = StringUtil.getString(request, "end_time");
			String catalog_id = StringUtil.getString(request, "filter_pcid");
			String pro_line_id = StringUtil.getString(request, "filter_productLine");
			String pc_name = StringUtil.getString(request, "search_key");
			long ca_id = StringUtil.getLong(request, "ca_id");
			long ccid = StringUtil.getLong(request, "ccid_hidden");
			long pro_id = StringUtil.getLong(request, "pro_id");
			long cid = StringUtil.getLong(request, "cid");
			int type = StringUtil.getInt(request, "type");
			String title_id = StringUtil.getString(request, "title_id");
			
			DBRow[] exportDatas = this.getB2BOrderOrProductCountAnalyze(start_time, end_time, catalog_id, pro_line_id,
					pc_name, ca_id, ccid, pro_id, cid, type, title_id, null);
			DBRow[] dates = floorB2BOrderMgrZyj.getDate(start_time, end_time);
			
			long[] storageAddressBook = new long[exportDatas.length];
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()
					+ "/administrator/b2b_order/ExportB2BOrderDemandAnalyze.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			// wb.setSheetName(0,"第一张表单");
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row = sheet.createRow(0);
			HSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style.setLocked(false);
			style.setWrapText(true);
			
			HSSFCellStyle stylelock = wb.createCellStyle();
			stylelock.setLocked(true); // 创建一个锁定样式
			stylelock.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			
			int n = 0;
			
			row.createCell(n).setCellValue("所属产品线");
			row.getCell(n).setCellStyle(style);
			n++;
			row.createCell(n).setCellValue("产品分类");
			row.getCell(n).setCellStyle(style);
			n++;
			row.createCell(n).setCellValue("商品名称");
			row.getCell(n).setCellStyle(style);
			n++;
			row.createCell(n).setCellValue("仓库");
			row.getCell(n).setCellStyle(style);
			n++;
			row.createCell(n).setCellValue("区域");
			row.getCell(n).setCellStyle(style);
			n++;
			if (ca_id > 0) {
				row.createCell(n).setCellValue("国家");
				row.getCell(n).setCellStyle(style);
				n++;
			}
			if (ccid > 0) {
				row.createCell(n).setCellValue("地区");
				row.getCell(n).setCellStyle(style);
				n++;
			}
			
			row.createCell(n).setCellValue("需求总数");
			row.getCell(n).setCellStyle(style);
			n++;
			for (int j = 0; j < dates.length; j++) {
				row.createCell(n + j).setCellValue(dates[j].getString("date"));
				row.getCell(n + j).setCellStyle(style);
			}
			
			if (exportDatas.length > 0) {
				for (int i = 0; i < exportDatas.length; i++) {
					row = sheet.createRow((int) i + 1);
					sheet.setDefaultColumnWidth(30);
					DBRow exportAdminTitle = exportDatas[i];
					// 设置excel列的值
					storageAddressBook[i] = 0L;
					int m = 0;
					row.createCell(m).setCellValue(exportAdminTitle.getString("pl_name"));
					row.getCell(m).setCellStyle(style);
					m++;
					row.createCell(m).setCellValue(exportAdminTitle.getString("title"));
					row.getCell(m).setCellStyle(style);
					m++;
					row.createCell(m).setCellValue(exportAdminTitle.getString("p_name"));
					row.getCell(m).setCellStyle(style);
					m++;
					row.createCell(m).setCellValue((0 == cid) ? "ALL" : exportAdminTitle.getString("storage_title"));
					row.getCell(m).setCellStyle(style);
					m++;
					row.createCell(m).setCellValue(
							(0 == ca_id && 1 == type) ? "ALL" : exportAdminTitle.getString("area_name"));
					row.getCell(m).setCellStyle(style);
					m++;
					if (ca_id > 0) {
						row.createCell(m).setCellValue(
								(ccid == 0 && type == 1) ? "ALL" : exportAdminTitle.getString("c_country"));
						row.getCell(m).setCellStyle(style);
						m++;
					}
					if (ccid > 0) {
						row.createCell(m).setCellValue(
								(pro_id == 0 && type == 1) ? "ALL" : exportAdminTitle.getString("pro_name"));
						row.getCell(m).setCellStyle(style);
						m++;
					}
					row.createCell(m).setCellValue(exportAdminTitle.getString("sum_quantity"));
					row.getCell(m).setCellStyle(style);
					m++;
					for (int j = 0; j < dates.length; j++) {
						row.createCell(m + j).setCellValue(exportAdminTitle.getString("date_" + j));
						row.getCell(m + j).setCellStyle(style);
					}
					
				}
			}
			// 写文件
			String path = "upl_excel_tmp/ExportB2BOrderDemandAnalyze_"
					+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS", new Date()) + ".xls";
			FileOutputStream os = new FileOutputStream(Environment.getHome() + path);
			wb.write(os);
			os.close();
			return (path);
		} catch (Exception e) {
			throw new SystemException(e, "exportB2BOrderOrProductCountAnalyze", log);
		}
	}
	
	public DBRow importB2BOrder(String fileName, int type, String[] orderOrWms) throws Exception {
		try {
			// HashMap<String,String> filedName = fieldMap();
			
			HashMap<String, String> pcidMap = new HashMap<String, String>();
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			String path = Environment.getHome() + "upl_imags_tmp/" + fileName;
			
			InputStream is = new FileInputStream(path);
			Workbook rwb = Workbook.getWorkbook(is);
			
			Sheet rs = rwb.getSheet(0);
			int rsRows = rs.getRows(); // excel表记录行数
			
			if (rsRows > 1) {
				String CompanyID = rs.getCell(0, 1).getContents().trim();
				String CustomerID = rs.getCell(1, 1).getContents().trim();
				String SupplierID = rs.getCell(2, 1).getContents().trim();
				String AccountID = rs.getCell(3, 1).getContents().trim();
				String ReferenceNo = rs.getCell(4, 1).getContents().trim();
				String PONo = rs.getCell(6, 1).getContents().trim();
				String ShipToAddress1 = rs.getCell(13, 1).getContents().trim();
				String ShipToCity = rs.getCell(14, 1).getContents().trim();
				String ShipToState = rs.getCell(15, 1).getContents().trim();
				String ShipToZipCode = rs.getCell(16, 1).getContents().trim();
				String CarrierID = rs.getCell(31, 1).getContents().trim();
				
				// 不确定取的是否正确
				DBRow customerItem = sqlServerMgrZJ.findShipToInfoByCustomerAndAccountAddress(CustomerID, AccountID,
						ShipToAddress1);
				String ShipToID = customerItem.getString("AddressID");
				long OrderNo = sqlServerMgrZJ.findMaxOrderNoByCompanyId(CompanyID) + 1;
				
				DBRow b2BOrder = new DBRow();
				b2BOrder.add("CompanyID", CompanyID);
				b2BOrder.add("OrderNo", OrderNo);// 获取之后再生成
				b2BOrder.add("Status", "Imported");
				b2BOrder.add("CustomerID", CustomerID);
				b2BOrder.add("AccountID", AccountID);
				b2BOrder.add("OrderedDate", DateUtil.DateStr());
				b2BOrder.add("RequestedDate", DateUtil.DateStr());
				b2BOrder.add("ReferenceNo", ReferenceNo);
				b2BOrder.add("PONo", PONo);
				b2BOrder.add("ShipToAddress1", ShipToAddress1);
				b2BOrder.add("ShipToCity", ShipToCity);
				b2BOrder.add("ShipToState", ShipToState);
				b2BOrder.add("ShipToZipCode", ShipToZipCode);
				b2BOrder.add("ShipToID", ShipToID);
				
				b2BOrder.add("StoreAddress1", ShipToAddress1);
				b2BOrder.add("StoreCity", ShipToCity);
				b2BOrder.add("StoreState", ShipToState);
				b2BOrder.add("ShipToZipCode", ShipToZipCode);
				b2BOrder.add("StoreID", ShipToID);
				
				b2BOrder.add("CarrierID", CarrierID);
				
				b2BOrder.add("ScheduledDate", DateUtil.DateStr());
				b2BOrder.add("DateCreated", DateUtil.DateStr());
				b2BOrder.add("DateUpdated", DateUtil.DateStr());
				
				sqlServerMgrZJ.addOrders(b2BOrder);
				
				for (int i = 1; i < rsRows; i++) {
					String ItemID = rs.getCell(7, i).getContents().trim();
					String BuyerItemID = rs.getCell(3, i).getContents().trim();
					String OrderedQty = rs.getCell(17, i).getContents().trim();
					
					DBRow[] itemRow = sqlServerMgrZJ.findItemInfoByItemId(BuyerItemID, CompanyID, CustomerID);
					
					DBRow b2BOrderItem = new DBRow();
					b2BOrderItem.add("CompanyID", CompanyID);
					b2BOrderItem.add("OrderNo", OrderNo);
					b2BOrderItem.add("LineNo", i);
					b2BOrderItem.add("ItemID", ItemID);// CustomerItems
					b2BOrderItem.add("WarehouseID", "");
					b2BOrderItem.add("ZoneID", "");
					b2BOrderItem.add("BuyerItemID", BuyerItemID);
					b2BOrderItem.add("SupplierID", SupplierID);
					b2BOrderItem.add("LotNo", "");
					b2BOrderItem.add("PoundPerPackage", itemRow[0].get("PoundPerPackage", 0d));// CustomerItems
					b2BOrderItem.add("PONo", PONo);
					b2BOrderItem.add("ReferenceNo", "");
					b2BOrderItem.add("ReferenceLineNo", "");
					b2BOrderItem.add("OrderedQty", OrderedQty);
					b2BOrderItem.add("Pallets", "");
					b2BOrderItem.add("UnitPrice", itemRow[0].get("UnitPrice", 0d));// CustomerItems
					b2BOrderItem.add("PrePackQty", "");
					b2BOrderItem.add("PrePackDescription", "");
					b2BOrderItem.add("CommodityDescription", itemRow[0].getString("CommodityDescription"));
					b2BOrderItem.add("NMFC", "");
					b2BOrderItem.add("FreightClass", "");
					b2BOrderItem.add("PalletWeight", "");
					b2BOrderItem.add("Length", itemRow[0].get("Length", 0d));// CustomerItems
					b2BOrderItem.add("Width", itemRow[0].get("Width", 0d));// CustomerItems
					b2BOrderItem.add("Height", itemRow[0].get("Height", 0d));// CustomerItems
					b2BOrderItem.add("Note", "");
					
					sqlServerMgrZJ.addOrderLines(b2BOrderItem);
				}
				
			}
			return new DBRow();
		} catch (Exception e) {
			throw new SystemException(e, "addB2BOrderByJN", log);
		}
	}
	
	public DBRow importB2BOrderItemAndDisplay(String fileName, int type, String[] orderOrWms) throws Exception {
		try {
			DBRow reRow = new DBRow();
			String path = Environment.getHome() + "upl_imags_tmp/" + fileName;
			
			InputStream is = new FileInputStream(path);
			Workbook rwb = Workbook.getWorkbook(is);
			
			Sheet rs = rwb.getSheet(0);
			int rsRows = rs.getRows(); // excel表记录行数
			
			if (rsRows > 1) {
				String CompanyID = rs.getCell(0, 1).getContents().trim();
				String CustomerID = rs.getCell(1, 1).getContents().trim();
				String AccountID = rs.getCell(3, 1).getContents().trim();
				String ReferenceNo = rs.getCell(4, 1).getContents().trim();
				String PONo = rs.getCell(6, 1).getContents().trim();
				String ShipToAddress1 = rs.getCell(13, 1).getContents().trim();
				String ShipToCity = rs.getCell(14, 1).getContents().trim();
				String ShipToState = rs.getCell(15, 1).getContents().trim();
				String ShipToZipCode = rs.getCell(16, 1).getContents().trim();
				String CarrierID = rs.getCell(31, 1).getContents().trim();
				
				DBRow b2BOrder = new DBRow();
				b2BOrder.add("CompanyID", CompanyID);
				b2BOrder.add("CustomerID", CustomerID);
				b2BOrder.add("AccountID", AccountID);
				b2BOrder.add("OrderedDate", DateUtil.DateStr());
				b2BOrder.add("RequestedDate", DateUtil.DateStr());
				b2BOrder.add("ReferenceNo", ReferenceNo);
				b2BOrder.add("PONo", PONo);
				b2BOrder.add("ShipToAddress1", ShipToAddress1);
				b2BOrder.add("ShipToCity", ShipToCity);
				b2BOrder.add("ShipToState", ShipToState);
				b2BOrder.add("ShipToZipCode", ShipToZipCode);
				b2BOrder.add("StoreAddress1", ShipToAddress1);
				b2BOrder.add("StoreCity", ShipToCity);
				b2BOrder.add("StoreState", ShipToState);
				b2BOrder.add("ShipToZipCode", ShipToZipCode);
				b2BOrder.add("CarrierID", CarrierID);
				reRow.add("order_row", b2BOrder);
				
				DBRow[] list = new DBRow[rsRows - 1];
				for (int i = 1; i < rsRows; i++) {
					String ItemID = rs.getCell(7, i).getContents().trim();
					String BuyerItemID = rs.getCell(3, i).getContents().trim();
					String SupplierID = rs.getCell(2, 1).getContents().trim();
					String OrderedQty = rs.getCell(17, i).getContents().trim();
					
					DBRow b2BOrderItem = new DBRow();
					b2BOrderItem.add("ItemID", ItemID);// CustomerItems
					b2BOrderItem.add("BuyerItemID", BuyerItemID);
					b2BOrderItem.add("SupplierID", SupplierID);
					b2BOrderItem.add("OrderedQty", OrderedQty);
					list[i] = b2BOrderItem;
				}
				reRow.add("order_items", list);
			}
			return reRow;
		} catch (Exception e) {
			throw new SystemException(e, "importB2BOrderItemAndDisplay", log);
		}
	}
	
	private HashMap<String, String> fieldMap() {
		HashMap<String, String> filedName = new HashMap<String, String>();
		filedName.put("0", "CompanyID");
		filedName.put("1", "OrderNo");
		filedName.put("2", "Status");
		filedName.put("3", "CustomerID");
		filedName.put("4", "AccountID");
		filedName.put("5", "OrderedDate");
		filedName.put("6", "RequestedDate");
		filedName.put("7", "ReferenceNo");
		filedName.put("8", "PONo");
		filedName.put("9", "StoreID");
		filedName.put("10", "StoreName");
		filedName.put("11", "StoreAddress1");
		filedName.put("12", "StoreAddress2");
		filedName.put("13", "StoreCity");
		filedName.put("14", "StoreState");
		filedName.put("15", "StoreZipCode");
		filedName.put("16", "StoreCountry");
		filedName.put("17", "StoreContact");
		filedName.put("18", "StorePhone");
		filedName.put("19", "StoreExtension");
		filedName.put("20", "StoreFax");
		filedName.put("21", "StoreStoreNo");
		filedName.put("22", "StoreBatchCode");
		filedName.put("23", "StoreHome");
		filedName.put("24", "ShipToID");
		filedName.put("25", "ShipToName");
		filedName.put("26", "StoreAddress1");
		filedName.put("27", "StoreAddress2");
		filedName.put("28", "StoreCity");
		filedName.put("29", "StoreState");
		filedName.put("30", "StoreZipCode");
		filedName.put("31", "StoreCountry");
		filedName.put("32", "StoreContact");
		filedName.put("33", "StorePhone");
		filedName.put("34", "StoreExtension");
		filedName.put("35", "StoreFax");
		filedName.put("36", "StoreStoreNo");
		filedName.put("37", "StoreBatchCode");
		filedName.put("38", "StoreHome");
		filedName.put("39", "BillToID");
		filedName.put("40", "BillToName");
		filedName.put("41", "BillToAddress1");
		filedName.put("42", "BillToAddress2");
		filedName.put("43", "BillToCity");
		filedName.put("44", "BillToState");
		filedName.put("45", "BillToZipCode");
		filedName.put("46", "BillToCountry");
		filedName.put("47", "BillToContact");
		filedName.put("48", "BillToPhone");
		filedName.put("49", "BillToExtension");
		filedName.put("50", "BillToFax");
		filedName.put("51", "BillToStoreNo");
		filedName.put("52", "BillToBatchCode");
		filedName.put("53", "BillToHome");
		filedName.put("54", "FreightTerm");
		filedName.put("55", "CarrierID");
		filedName.put("56", "WarehouseID");
		filedName.put("57", "PickingType");
		filedName.put("58", "ScheduledDate");
		filedName.put("59", "StagingAreaID");
		filedName.put("60", "ProNo");
		filedName.put("61", "LoadNo");
		filedName.put("62", "OrderType");
		filedName.put("63", "RetailerOrderType");
		filedName.put("64", "DeptNo");
		filedName.put("65", "VehicleNo");
		filedName.put("66", "Seals");
		filedName.put("67", "MasterReferenceNo");
		filedName.put("68", "DivisionDescription");
		filedName.put("69", "ShippingAccountNo");
		filedName.put("70", "ShippingZipCode");
		filedName.put("71", "DockID");
		filedName.put("72", "PalletTypeID");
		filedName.put("73", "LinkSequenceNo");
		filedName.put("74", "RFControlNo");
		filedName.put("75", "SWControlNo");
		filedName.put("76", "SHControlNo");
		filedName.put("77", "INControlNo");
		filedName.put("78", "Note");
		filedName.put("79", "BOLNote");
		filedName.put("80", "LabelNote");
		filedName.put("81", "CartonLabelPrinted");
		filedName.put("82", "PalletLabelPrinted");
		filedName.put("83", "PickTicketPrinted");
		filedName.put("84", "UCCLabelPrinted");
		filedName.put("85", "ShippingLabelPrinted");
		filedName.put("86", "ReturnLabelPrinted");
		filedName.put("87", "TallySheetPrinted");
		filedName.put("88", "PackingListPrinted");
		filedName.put("89", "PackingSerialNosPrinted");
		filedName.put("90", "BillOfLadingPrinted");
		filedName.put("91", "EDIOrder");
		filedName.put("92", "Send753");
		filedName.put("93", "Send945");
		filedName.put("94", "Send856");
		filedName.put("95", "Send810");
		filedName.put("96", "Send214");
		filedName.put("97", "Send857");
		filedName.put("98", "CustomerInvoiceNo");
		filedName.put("99", "DateCreated");
		filedName.put("100", "UserCreated");
		filedName.put("101", "DateUpdated");
		filedName.put("102", "UserUpdated");
		
		return filedName;
	}
	
	public DBRow addB2BOrderByJN(String fileName, int type, String[] orderOrWms) throws Exception {
		try {
			
			return new DBRow();
		} catch (Exception e) {
			throw new SystemException(e, "addB2BOrderByJN", log);
		}
	}
	
	public void setFloorB2BOrderMgrZyj(FloorB2BOrderMgrZyj floorB2BOrderMgrZyj) {
		this.floorB2BOrderMgrZyj = floorB2BOrderMgrZyj;
	}
	
	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}
	
	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
	
	public void setLpTypeMgrZJ(LPTypeMgrIFaceZJ lpTypeMgrZJ) {
		this.lpTypeMgrZJ = lpTypeMgrZJ;
	}
	
	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}
	
	public void setApplyFundsMgrZyj(ApplyFundsMgrZyjIFace applyFundsMgrZyj) {
		this.applyFundsMgrZyj = applyFundsMgrZyj;
	}
	
	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}
	
	public void setTransportMgrZr(TransportMgrIfaceZr transportMgrZr) {
		this.transportMgrZr = transportMgrZr;
	}
	
	public void setFloorPurchaseMgrZyj(FloorPurchaseMgrZyj floorPurchaseMgrZyj) {
		this.floorPurchaseMgrZyj = floorPurchaseMgrZyj;
	}
	
	public void setTransportMgrZyj(TransportMgrZyjIFace transportMgrZyj) {
		this.transportMgrZyj = transportMgrZyj;
	}
	
	public void setContainerMgrZyj(ContainerMgrIFaceZyj containerMgrZyj) {
		this.containerMgrZyj = containerMgrZyj;
	}
	
	public void setSqlServerMgrZJ(SQLServerMgrIFaceZJ sqlServerMgrZJ) {
		this.sqlServerMgrZJ = sqlServerMgrZJ;
	}
	
	/**
	 * 
	 * 根据订单id获取运单信息
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception <b>Date:</b>2014-8-15下午3:26:29<br>
	 * @author: cuicong
	 */
	public List<List<DBRow>> getTransportByOderId(long b2b_oid) throws Exception {
		DBRow[] dbRows = this.floorB2BOrderMgrZyj.getTransportByOderId(b2b_oid);
		List<List<DBRow>> returnList = new ArrayList<List<DBRow>>();
		List<DBRow> putList = new ArrayList<DBRow>();
		for (int i = 0; i < dbRows.length; i++) {
			putList.add(dbRows[i]);
			if ((i + 1) % 4 == 0) {
				returnList.add(putList);
				putList = new ArrayList<DBRow>();
				
			}
		}
		
		if (putList.size() != 0) {
			int length = 4 - putList.size();
			for (int i = 0; i < length; i++) {
				putList.add(null);
			}
			
			returnList.add(putList);
		}
		return returnList;
		
	}
	
	/**
	 * 
	 * 添加B2b
	 * 
	 * @param dbRow
	 * @throws Exception <b>Date:</b>2014-12-23下午8:58:53<br>
	 * @author: cuicong
	 */
	@Override
	public long addB2BOrder(DBRow dbRow) throws Exception {
		return this.floorB2BOrderMgrZyj.addB2BOrder(dbRow);
		
	}
	
	/**
	 * 
	 * 添加B2b明细
	 * 
	 * @param dbRow
	 * @throws Exception <b>Date:</b>2014-12-23下午8:58:10<br>
	 * @author: cuicong
	 */
	@Override
	public long addB2BOrderItem(DBRow dbRow) throws Exception {
		
		return this.floorB2BOrderMgrZyj.addB2BOrderItem(dbRow);
		
	}
	
	@Override
	public void deleteB2BOrderItem(long b2b_detail_id) throws Exception {
		this.floorB2BOrderMgrZyj.delB2BOrderItem(b2b_detail_id);
	}
	
	@Override
	public void deleteB2BOrder(long b2b_oid) throws Exception {
		this.floorB2BOrderMgrZyj.delB2BOrderItemByB2BOrderId(b2b_oid);
		this.floorB2BOrderMgrZyj.delB2BOrder(b2b_oid);
		
	}
	
	@Override
	public void updateB2BOrderItem(long b2b_detail_id, DBRow dbRow) throws Exception {
		this.floorB2BOrderMgrZyj.modB2BOrderItem(b2b_detail_id, dbRow);
	}
	
	@Override
	public void updateB2BOrder(Long b2b_oid, DBRow dbRow) throws Exception {
		
		
		this.floorB2BOrderMgrZyj.modB2BOrder(b2b_oid, dbRow);
		
	}
	
	@Override
	public DBRow getB2BOrder(long b2b_oid) throws Exception {
		return this.floorB2BOrderMgrZyj.getB2BOrder(b2b_oid);
		
	}
	
	@Override
	public DBRow[] getB2BOrderItems(long b2b_oid) throws Exception {
		return this.floorB2BOrderMgrZyj.getB2BOrderItems(b2b_oid);
	}
	
	@Override
	public DBRow getB2BOrderItem(long b2b_oid_detal) throws Exception {
		return this.floorB2BOrderMgrZyj.getB2BOrderItem(b2b_oid_detal);
	}
	
	@Override
	public DBRow[] fillterB2BOrder(DBRow dbRow, PageCtrl ctrl) throws Exception {
		return this.floorB2BOrderMgrZyj.fillterB2BOrder(dbRow, ctrl);
	}
	
	@Override
	public DBRow[] fillterCustomerId(DBRow dbRow, PageCtrl ctrl) throws Exception {
		return this.floorCustomerIdMgr.fillterCustomerId(dbRow, ctrl);
	}
	
	@Override
	public long addB2BOrderLog(DBRow dbRow) throws Exception {
		return this.floorB2BOrderMgrZyj.addB2BOrderLog(dbRow);
	}

	@Override
	public String[] getLoadAndApp(long oid) throws Exception {
		return floorLoadMgr.getLoadIdAndAid(oid);
	}

	@Override
	public DBRow[] fillterCustomerBrand(DBRow dbRow, PageCtrl ctrl) 	throws Exception {
		return floorCustomerIdMgr.fillterCustomerBrand(dbRow, ctrl);
	}

	public DBRow[] getB2BOrders(List<Long> ids) throws Exception {
		return floorB2BOrderMgrZyj.getB2BOrders(ids);
	}
}
