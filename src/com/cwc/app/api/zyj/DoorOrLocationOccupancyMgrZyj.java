package com.cwc.app.api.zyj;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zyj.FloorDoorOrLocationOccupancyMgrZyj;
import com.cwc.app.floor.api.zyj.FloorTransportMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zyj.DoorOrLocationOccupancyMgrZyjIFace;
import com.cwc.app.iface.zyj.TransportMgrZyjIFace;
import com.cwc.app.key.LoadUnloadOccupancyStatusKey;
import com.cwc.app.key.LoadUnloadOccupancyTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.TransportLogTypeKey;
import com.cwc.app.key.LoadUnloadRelationTypeKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportPutKey;
import com.cwc.app.key.TransportRegistrationTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class DoorOrLocationOccupancyMgrZyj implements DoorOrLocationOccupancyMgrZyjIFace{

	private FloorDoorOrLocationOccupancyMgrZyj floorDoorOrLocationOccupancyMgrZyj;
	private FloorTransportMgrZyj floorTransportMgrZyj;
	private TransportMgrZyjIFace transportMgrZyj;

	
	
	/**
	 * 添加门或位置的使用信息可以重复占用
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow addDoorOrLocation(HttpServletRequest request) throws Exception
	{
		try
		{
			DBRow result = new DBRow();
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid					= adminLoggerBean.getAdid();
			int rel_type				= StringUtil.getInt(request, "rel_type");
			long rel_id					= StringUtil.getLong(request, "rel_id");
			int rel_occupancy_use		= StringUtil.getInt(request, "rel_occupancy_use");
			long ps_id					= StringUtil.getLong(request, "ps_id");
			String[] occupancy_ids		= request.getParameterValues("occupancy_ids");
			String[] occupancy_types	= request.getParameterValues("occupancy_types");
			String[] book_start_times	= request.getParameterValues("book_start_times");
			if(null != occupancy_ids && occupancy_ids.length > 0){
				//如果可以被占用保存
				for (int i = 0; i < occupancy_ids.length; i++)
				{
						int occupancy_type	= !"".equals(occupancy_types[i])?Integer.parseInt(occupancy_types[i]):0;
						long occupancy_id	= !"".equals(occupancy_ids[i])?Integer.parseInt(occupancy_ids[i]):0;
						this.addDoorOrLocationOccupancy(occupancy_type, occupancy_id, book_start_times[i],"", rel_type,rel_id,rel_occupancy_use,ps_id, adid);
				}
				this.updateTransportRegistratioinAndStatus(rel_occupancy_use, rel_id, adid, adminLoggerBean.getEmploye_name(), request);	
			}
			result.add("flag", true);
			return result;
		}
		catch (Exception e) {
			throw new Exception("addDoorOrLocationOccupancy(HttpServletRequest request):"+e);
		}
	}
	
	/**
	 * 添加门或位置的使用信息
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow addDoorOrLocationOccupancys(HttpServletRequest request) throws Exception
	{
		try
		{
			DBRow result = new DBRow();
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid					= adminLoggerBean.getAdid();
			int rel_type				= StringUtil.getInt(request, "rel_type");
			long rel_id					= StringUtil.getLong(request, "rel_id");
			int rel_occupancy_use		= StringUtil.getInt(request, "rel_occupancy_use");
			long ps_id					= StringUtil.getLong(request, "ps_id");
			String[] occupancy_ids		= request.getParameterValues("occupancy_ids");
			String[] occupancy_types	= request.getParameterValues("occupancy_types");
			String[] book_start_times	= request.getParameterValues("book_start_times");
//			String[] book_end_times		= request.getParameterValues("book_end_times");
			boolean isCanSave = false;
			
			if(null != occupancy_ids && occupancy_ids.length > 0)
			{
				//如果需要判断所提交的时间是否交叉，如果不能同时在两个门或位置缺货
				//isCanSave = this.checkBookOccupancyTimeIsInterleaved(book_start_times, book_end_times);
				//if(isCanSave)
				//{
					for (int i = 0; i < occupancy_ids.length; i++)
					{
						int occupancy_type	= !"".equals(occupancy_types[i])?Integer.parseInt(occupancy_types[i]):0;
						
						if(LoadUnloadOccupancyTypeKey.DOOR == occupancy_type)
						{
							isCanSave = getBookDoorInfoCanChooseByDoorId(book_start_times[i], ps_id, Long.parseLong(occupancy_ids[i]));
							
						}
						else if(LoadUnloadOccupancyTypeKey.LOCATION == occupancy_type)
						{
							
							isCanSave = getBookLocationInfoCanChooseByLocId(book_start_times[i], ps_id, Long.parseLong(occupancy_ids[i]));
						}
						if(!isCanSave)
						{
							break;
						}
					}
					//如果可以被占用保存
					if(isCanSave)
					{
						String adminUserIds = StringUtil.getString(request, "adminUserIds");
						for (int i = 0; i < occupancy_ids.length; i++)
						{
								int occupancy_type	= !"".equals(occupancy_types[i])?Integer.parseInt(occupancy_types[i]):0;
								long occupancy_id	= !"".equals(occupancy_ids[i])?Integer.parseInt(occupancy_ids[i]):0;
								this.addDoorOrLocationOccupancy(occupancy_type, occupancy_id, book_start_times[i],"", rel_type,rel_id,rel_occupancy_use,ps_id, adid);
						}
						this.updateTransportRegistratioinAndStatus(rel_occupancy_use, rel_id, adid, adminLoggerBean.getEmploye_name(), request);
					}
				//}
			}
			result.add("flag", isCanSave);
			return result;
		}
		catch (Exception e) {
			throw new Exception("addDoorOrLocationOccupancy(HttpServletRequest request):"+e);
		}
	}
	
	/**
	 * 检查所填写的时间是否交叉
	 * @param book_start_times
	 * @param book_end_times
	 * @return
	 * @throws Exception
	 */
	public boolean checkBookOccupancyTimeIsInterleaved(String[] book_start_times, String[] book_end_times) throws Exception
	{
		try
		{
			boolean isTimeInterleaved = true;
			TreeSet<String> set = new TreeSet<String>();
			for (int i = 0; i < book_start_times.length; i++) 
			{
				set.add(book_start_times[i]+"_"+i);
				set.add(book_end_times[i]+"_"+i);
			}
			Iterator<String> it = set.iterator();
			for (Iterator iterator = set.iterator(); iterator.hasNext();) {
				String time1 = (String) iterator.next();
				String time2 = (String) iterator.next();
				if(!time1.split("_")[1].equals(time2.split("_")[1]))
				{
					isTimeInterleaved = false;
					break;
				}
			}
			return isTimeInterleaved;
		}
		catch (Exception e) 
		{
			throw new Exception("checkBookOccupancyTimeIsInterleaved(String[] book_start_times, String[] book_end_times):"+e);
		}
	}
	
	
	/**
	 * 更新转运单签到人及状态
	 * @throws Exception
	 */
	public void updateTransportRegistratioinAndStatus(int rel_occupancy_use, long rel_id, long adid, String employ_name
		, int mail, int message, int page, String adminUserIds, AdminLoginBean adminLoggerBean) throws Exception
	{
		try
		{
			int processType = 0;
			DBRow transportRow = new DBRow();
			//将签到人的信息更新到主单据上
			if(TransportRegistrationTypeKey.SEND == rel_occupancy_use)
			{
				transportRow.add("send_registration", adid);
				transportRow.add("send_registration_time", DateUtil.NowStr());
				processType = TransportLogTypeKey.UNLOAD_REGISTER;
			}
			else if(TransportRegistrationTypeKey.DELEIVER == rel_occupancy_use)
			{
				transportRow.add("deleiver_registration", adid);
				transportRow.add("deleiver_registration_time", DateUtil.NowStr());
				transportRow.add("transport_status", TransportOrderKey.AlREADYARRIAL);
				transportRow.add("put_status",TransportPutKey.Puting);
				processType = TransportLogTypeKey.LOAD_REGISTER;
			}
			
			//加或更新任务
			transportMgrZyj.updateTransportScheduleOneProdure(DateUtil.NowStr(), "", adid, adminUserIds, "", rel_id,
					ModuleKey.TRANSPORT_ORDER, "转运单"+rel_id+"["+new TransportLogTypeKey().getTransportLogTypeById(processType)+"]", "", page, mail, message, adminLoggerBean, true,
					processType, 0, employ_name, employ_name+":"+new TransportLogTypeKey().getTransportLogTypeById(processType));
			
			floorTransportMgrZyj.updateTransport(rel_id, transportRow);
		}
		catch (Exception e) 
		{
			throw new Exception("updateTransportRegistratioinAndStatus(int rel_occupancy_use, long rel_id, long adid):"+e);
		}
	}
	
	/**
	 * 更新转运单签到人及状态
	 * @throws Exception
	 */
	public void updateTransportRegistratioinAndStatus(int rel_occupancy_use, long rel_id, long adid, String employ_name, HttpServletRequest request) throws Exception
	{
		try
		{
			String adminUserIds = StringUtil.getString(request, "adminUserIds");
			int mail = StringUtil.getInt(request, "isNeedMail");
			int message = StringUtil.getInt(request, "isNeedMessage");
			int page = StringUtil.getInt(request, "isNeedPage");
			int processType = 0;
			DBRow transportRow = new DBRow();
			//将签到人的信息更新到主单据上
			if(TransportRegistrationTypeKey.SEND == rel_occupancy_use)
			{
				transportRow.add("send_registration", adid);
				transportRow.add("send_registration_time", DateUtil.NowStr());
				processType = TransportLogTypeKey.UNLOAD_REGISTER;
			}
			else if(TransportRegistrationTypeKey.DELEIVER == rel_occupancy_use)
			{
				transportRow.add("deleiver_registration", adid);
				transportRow.add("deleiver_registration_time", DateUtil.NowStr());
				transportRow.add("transport_status", TransportOrderKey.AlREADYARRIAL);
				transportRow.add("put_status",TransportPutKey.Puting);
				processType = TransportLogTypeKey.LOAD_REGISTER;
			}
			
			//加或更新任务
			transportMgrZyj.updateTransportScheduleOneProdure(DateUtil.NowStr(), "", adid, adminUserIds, "", rel_id,
					ModuleKey.TRANSPORT_ORDER, "转运单"+rel_id+"["+new TransportLogTypeKey().getTransportLogTypeById(processType)+"]", "", page, mail, message, request, true,
					processType, 0, employ_name, employ_name+":"+new TransportLogTypeKey().getTransportLogTypeById(processType));
			
			floorTransportMgrZyj.updateTransport(rel_id, transportRow);
		}
		catch (Exception e) 
		{
			throw new Exception("updateTransportRegistratioinAndStatus(int rel_occupancy_use, long rel_id, long adid):"+e);
		}
	}
	
	
	/**
	 * 添加门或位置的使用信息
	 * @param occupancy_type
	 * @param occupancy_id
	 * @param book_start_time……
	 * @return
	 * @throws Exception
	 */
	public long addDoorOrLocationOccupancy(int occupancy_type, long occupancy_id, String book_start_time, String book_end_time, int rel_type, long rel_id, int rel_occupancy_use, long ps_id, long adid) throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("occupancy_type", occupancy_type);
			row.add("rl_id", occupancy_id);
			row.add("rel_type", rel_type);
			row.add("rel_id", rel_id);
			row.add("rel_occupancy_use", rel_occupancy_use);
			if(null != book_start_time && !"".equals(book_start_time))
			{
				row.add("book_start_time", book_start_time);
			}
			if(null != book_end_time && !"".equals(book_end_time))
			{
				row.add("book_end_time", book_end_time);
			}
			row.add("ps_id", ps_id);
			row.add("creator", adid);
			row.add("create_time", DateUtil.NowStr());
			row.add("occupancy_status", LoadUnloadOccupancyStatusKey.BOOKING);
			return floorDoorOrLocationOccupancyMgrZyj.addDoorOrLocationOccupancy(row);
		}
		catch (Exception e) {
			throw new Exception("addDoorOrLocationOccupancy(int occupancy_type, long occupancy_id, String book_start_time, String book_end_time,long ps_id, long adid):"+e);
		}
	}
	
	/**
	 * 更新门或位置的占用信息
	 * @param id
	 * @param row
	 * @throws Exception
	 */
	public void updateDoorOrLocationOccupancyStatus(HttpServletRequest request) throws Exception
	{
		try
		{
			long id					= StringUtil.getLong(request, "id");
			int occupancy_status	= StringUtil.getInt(request, "occupancy_status");
			String book_end_time	= StringUtil.getString(request, "book_end_time_"+id);
			DBRow row = new DBRow();
			row.add("id", id);
			row.add("occupancy_status", occupancy_status);
			if(LoadUnloadOccupancyStatusKey.RELEASE == occupancy_status)
			{
				row.add("book_end_time", book_end_time);
			}
			
			floorDoorOrLocationOccupancyMgrZyj.updateDoorOrLocationOccupancy(id, row);
		}
		catch (Exception e) {
			throw new Exception("updateDoorOrLocationOccupancyStatus(HttpServletRequest request):"+e);
		}
	}
	
	/**
	 * 根据门或位置ID获取其使用信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorOrLocationOccupancys(HttpServletRequest request) throws Exception
	{
		try
		{
			int occupancy_type		= StringUtil.getInt(request, "occupancy_type");
			long rl_id				= StringUtil.getLong(request, "rl_id");
			int rel_type			= StringUtil.getInt(request, "rel_type");
			long rel_id				= StringUtil.getLong(request, "rel_id");
			int occupancy_status	= StringUtil.getInt(request, "occupancy_status");
			String book_start_time	= StringUtil.getString(request, "book_start_time");
			String book_end_time	= StringUtil.getString(request, "book_end_time");
			String actual_start_time= StringUtil.getString(request, "actual_start_time");
			String actual_end_time	= StringUtil.getString(request, "actual_end_time");
			int is_after_now		= StringUtil.getInt(request, "is_after_now");
			int ps_id				= StringUtil.getInt(request, "ps_id");
			int rel_occupancy_use	= StringUtil.getInt(request, "rel_occupancy_use");
			
			return this.getDoorOrLocationOccupancys(occupancy_type, rl_id, rel_type, rel_id, occupancy_status, book_start_time, book_end_time, actual_start_time, actual_end_time,ps_id,is_after_now,rel_occupancy_use);
			
		}
		catch (Exception e) {
			throw new Exception("getDoorOrLocationOccupancys(HttpServletRequest request):"+e);
		}
	}
	
	/**
	 * 根据门或位置ID获取其使用信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorOrLocationOccupancys(int occupancy_type, long rl_id, int rel_type, long rel_id, int occupancy_status,
			String book_start_time, String book_end_time, String actual_start_time, String actual_end_time, long ps_id, int is_after_now, int rel_occupancy_use) throws Exception
	{
		try
		{
			return floorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(occupancy_type, rl_id, rel_type, rel_id, occupancy_status, book_start_time, book_end_time, actual_start_time, actual_end_time,ps_id, is_after_now, rel_occupancy_use);
			
		}
		catch (Exception e) {
			throw new Exception("getDoorOrLocationOccupancys(int occupancy_type, int rel_type, long rel_id……):"+e);
		}
	}
	
	/**
	 * 根据门或位置ID获取其使用信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorOrLocationOccupancysByTime(HttpServletRequest request) throws Exception
	{
		try
		{
			int occupancy_type		= StringUtil.getInt(request, "occupancy_type");
			long rl_id				= StringUtil.getLong(request, "rl_id");
			int occupancy_status	= StringUtil.getInt(request, "occupancy_status");
			String book_start_time	= StringUtil.getString(request, "book_start_time");
			String book_end_time	= StringUtil.getString(request, "book_end_time");
			return this.getDoorOrLocationOccupancysByTime(occupancy_type, rl_id, occupancy_status, book_start_time, book_end_time);
		}
		catch (Exception e)
		{
			throw new Exception("getDoorOrLocationOccupancys(HttpServletRequest request):"+e);
		}
	}
	
	/**
	 * 根据门或位置ID获取其使用信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorOrLocationOccupancysByTime(int occupancy_type, long rl_id, int occupancy_status, String book_start_time, String book_end_time) throws Exception
	{
		try
		{
			return floorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancysByTime(occupancy_type, rl_id, occupancy_status, book_start_time, book_end_time);
		}
		catch (Exception e)
		{
			throw new Exception("getDoorOrLocationOccupancys(int occupancy_type, long rl_id……):"+e);
		}
	}
	
	/**
	 * 得到可利用的门或位置
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBookDoorInfoCanChoose(String start_time, String end_time, long ps_id, PageCtrl pageCtrl) throws Exception
	{
		try
		{
			
			//可用门或位置除了与已有的时间不冲突，还包括未占用过的
			long door_or_loc_id_mand = 0L;
			Map<DBRow, List> door_or_loc_map = new HashMap<DBRow, List>();
			DBRow[] rows = floorDoorOrLocationOccupancyMgrZyj.getBookDoorInfoEndTimeIsNotNull(ps_id);
			List<String> list		= null;
			for (int i = 0; i < rows.length; i++) 
			{
				
				long door_or_loc_id		= rows[i].get("rl_id", 0L);
				String book_start_time	= rows[i].getString("book_start_time");
				String book_end_time	= rows[i].getString("book_end_time");
				//如果相同，重新创建List，并往list中添加数据
				if(door_or_loc_id_mand != door_or_loc_id)
				{
					list = new ArrayList<String>();
					list.add(book_start_time);
					list.add(book_end_time);
					DBRow row = new DBRow();
					row.add("sd_id", door_or_loc_id);
					row.add("doorId", rows[i].getString("doorId"));
					
					door_or_loc_map.put(row, list);
					door_or_loc_id_mand = door_or_loc_id;
				}
				//如果相同直接放数据
				else
				{
					list.add(book_start_time);
					list.add(book_end_time);
				}
			}
			//遍历map找出时间允许的门或位置
			List<DBRow> rowList = new ArrayList<DBRow>();
			Set<Map.Entry<DBRow, List>> set = (Set<Entry<DBRow, List>>) door_or_loc_map.entrySet();
			for (Iterator iterator = set.iterator(); iterator.hasNext();) 
			{
				boolean isCanChoose = false;
				Entry<DBRow, List> entry = (Entry<DBRow, List>) iterator.next();
				DBRow key			 = entry.getKey();
				List<String> allTime = entry.getValue();
				//遍历list找出是否存在时间允许的时间段
				isCanChoose = this.judgeStartAndEndTimeIsCanChoose(start_time, end_time, allTime);
				if(isCanChoose)
				{
					rowList.add(key);
				}
			}
			
			DBRow[] isNotOccupancyDoors = floorDoorOrLocationOccupancyMgrZyj.getNotOccupancyDoors(ps_id);
			DBRow[] handleRows = new DBRow[rowList.size()];
			rowList.toArray(handleRows);
			DBRow[] reRows	= new DBRow[isNotOccupancyDoors.length + handleRows.length];
			System.arraycopy(isNotOccupancyDoors, 0, reRows, 0, isNotOccupancyDoors.length);
			System.arraycopy(handleRows, 0, reRows, isNotOccupancyDoors.length, handleRows.length);
			
			if(null == pageCtrl)
			{
				return reRows;
			}
			else
			{
				return this.makeResutltSetsPageCtrl(reRows, pageCtrl);
			}
		}
		catch (Exception e)
		{
			throw new Exception("getBookDoorInfoCanChoose(String start_time, String end_time, long ps_id, PageCtrl pageCtrl):"+e);
		}
	}
	
	
	/**
	 * 得到可利用的门或位置
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBookLocationInfoCanChoose(String start_time, String end_time, long ps_id, PageCtrl pageCtrl) throws Exception
	{
		try
		{
			
			//可用门或位置除了与已有的时间不冲突，还包括未占用过的
			long door_or_loc_id_mand = 0L;
			Map<DBRow, List> door_or_loc_map = new HashMap<DBRow, List>();
			List<String> list		= null;
			DBRow[] rows = floorDoorOrLocationOccupancyMgrZyj.getBookLocationInfoEndTimeIsNotNull(ps_id);
			for (int i = 0; i < rows.length; i++) 
			{
				long door_or_loc_id		= rows[i].get("rl_id", 0L);
				String book_start_time	= rows[i].getString("book_start_time");
				String book_end_time	= rows[i].getString("book_end_time");
				//如果相同，重新创建List，并往list中添加数据
				if(door_or_loc_id_mand != door_or_loc_id)
				{
					list = new ArrayList<String>();
					list.add(book_start_time);
					list.add(book_end_time);
					DBRow row = new DBRow();
					row.add("id", door_or_loc_id);
					row.add("location_name", rows[i].getString("location_name"));
					door_or_loc_map.put(row, list);
					door_or_loc_id_mand = door_or_loc_id;
				}
				//如果相同直接放数据
				else
				{
					list.add(book_start_time);
					list.add(book_end_time);
				}
			}
			//遍历map找出时间允许的门或位置
			List<DBRow> rowList = new ArrayList<DBRow>();
			Set<Map.Entry<DBRow, List>> set = (Set<Entry<DBRow, List>>) door_or_loc_map.entrySet();
			for (Iterator iterator = set.iterator(); iterator.hasNext();) 
			{
				boolean isCanChoose = false;
				Entry<DBRow, List> entry = (Entry<DBRow, List>) iterator.next();
				DBRow key			 = entry.getKey();
				List<String> allTime = entry.getValue();
				//遍历list找出是否存在时间允许的时间段
				isCanChoose = this.judgeStartAndEndTimeIsCanChoose(start_time, end_time, allTime);
				if(isCanChoose)
				{
					rowList.add(key);
				}
			}
			DBRow[] isNotOccupancyLocs = floorDoorOrLocationOccupancyMgrZyj.getNotOccupancyLocations(ps_id);
			DBRow[] handleRows = new DBRow[rowList.size()];
			rowList.toArray(handleRows);
			DBRow[] reRows	= new DBRow[isNotOccupancyLocs.length + handleRows.length];
			System.arraycopy(handleRows, 0, reRows, 0, handleRows.length);
			System.arraycopy(isNotOccupancyLocs, 0, reRows, handleRows.length, isNotOccupancyLocs.length);
			
			if(null == pageCtrl)
			{
				return reRows;
			}
			else
			{
				return this.makeResutltSetsPageCtrl(reRows, pageCtrl);
			}
			
		}
		catch (Exception e)
		{
			throw new Exception("getBookLocationInfoCanChoose(String start_time, String end_time, long ps_id, PageCtrl pageCtrl):"+e);
		}
	}
	
	/**
	 * 将结果集分页
	 * @return
	 * @throws Exception
	 */
	public DBRow[] makeResutltSetsPageCtrl(DBRow[] rows, PageCtrl pageCtrl) throws Exception
	{
		try
		{
			int allCount = rows.length;					//记录总数
	        int start = 0;								//开始偏移
	        int end = 0;								//结束偏移
	        int ind = 0;								//游标偏移
	        int pageSize = 0;							//页面记录数
	        int pageNo = 0;								//当前页
	          
	        if(pageCtrl != null)
	        {
	        	pageSize = pageCtrl.getPageSize();		//得到外部设置的每页显示记录数
	        	pageNo = pageCtrl.getPageNo();			//得到外部设置当前第几页
	        	  
	            if(pageSize <= 0 || pageNo < 1) throw new Exception("PageCtrl data is error");
	             
	            start = ( pageNo - 1) * pageSize;			//计算起始偏移量
	            end = pageNo * pageSize;					//计算终点偏移量
	        }
			
	        ;
	        DBRow results[] = null;
	        ArrayList list = new ArrayList();

	        for(int i = 0; i < rows.length; i ++)
	        {
	            if( (++ind > start && ind <= end ) || (start == 0 && end == 0) )		//跳过偏移量外的结果集
	            {
	                 list.add(rows[i]);	//把一行记录加入arraylist，在最后转换为DBRow[]
	            }
	        }
	        
	        if(list.size() > 0)
	        {
	        	results = (DBRow[])list.toArray(new DBRow[0]);
	        }
	        else
	        {
	        	results = new DBRow[0];
	        }
	          
	        //计算分页结果
	        if(pageCtrl != null)
	        {
	        	//计算总页数
	        	int pageCount = ((allCount + pageSize) - 1) / pageSize;

	        	if( pageCount < pageNo)
	        	{
	            	pageNo = pageCount;
	         	}
	            pageCtrl.setPageNo(pageNo);				//当前页
	            pageCtrl.setPageSize(pageSize);			//每页记录数
	            pageCtrl.setPageCount(pageCount);			//总页数
	            pageCtrl.setAllCount(allCount);			//总记录数
	            pageCtrl.setRowCount(results.length);		//当前页记录数
	        }
			
			return results;
		}
		catch (Exception e)
		{
			throw new Exception("makeResutltSetsPageCtrl(DBRow[] rows, PageCtrl pageCtrl):"+e);
		}
	}
	
	/**
	 * 想要占用的门是否被占用
	 * @return
	 * @throws Exception
	 */
	public boolean getBookDoorInfoCanChooseByDoorId(String start_time, long ps_id, long door_id) throws Exception
	{
		try
		{
			//可用门或位置除了与已有的时间不冲突，还包括未占用过的
			boolean isCanChoose = false;
			DBRow[] rowsOcc = floorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.DOOR, door_id, 0, 0,LoadUnloadOccupancyStatusKey.BOOKING,"", "", "", "", ps_id, 0, 0);
			if(0 != rowsOcc.length)
			{
				isCanChoose = false;
			}
			else
			{
				DBRow[] rows = floorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.DOOR, door_id, 0, 0,LoadUnloadOccupancyStatusKey.RELEASE,"", "", "", "", ps_id, 0, 0);
				if(0 == rows.length)
				{
					isCanChoose = true;
				}
				else
				{
					//将此门的所有占用时间段放在一个list里面
					List<String> list = new ArrayList<String>();
					for (int i = 0; i < rows.length; i++) 
					{
						String book_start_time	= rows[i].getString("book_start_time");
						String book_end_time	= rows[i].getString("book_end_time");
						if("".equals(book_start_time) || "".equals(rows[i].getString("book_end_time")))
						{
							list = new ArrayList<String>();
							break;
						}
						else
						{
							list.add(book_start_time);
							list.add(book_end_time);
						}
					}
					//遍历list找出是否存在时间允许的时间段
					if(list.size() > 0)
					{
						isCanChoose = this.judgeStartAndEndTimeIsCanChoose(start_time, list);
					}
				}
			}
			
			return isCanChoose;
		}
		catch (Exception e)
		{
			throw new Exception("getBookDoorInfoCanChooseByDoorId(String start_time, long ps_id, long door_id):"+e);
		}
	}
	
	/**
	 * 遍历list找出是否存在时间允许的时间段
	 * @param start_time
	 * @param end_time
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public boolean judgeStartAndEndTimeIsCanChoose(String start_time, String end_time, List<String> list) throws Exception
	{
		try
		{
			boolean isCanChoose = false;
			for (int i = 0; i < list.size(); i++)
			{
				//遍历所有时间进行处理，与传进来的时间进行比较
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String time = list.get(i);
				Date d1 = df.parse(start_time);
				Date d2 = df.parse(end_time);
				Date d3 = df.parse(time);
				//如果想要预定的时间段早于此门所有占用时间段，此想要预定的时间段可被占用
				if(i == 0)
				{
					if(d2.before(d3))
					{
						isCanChoose = true;
						break;
					}
				}
				//想要预定的时间段是否在已经占用的时间段是前一个结束时间与后一个开始时间之间
				else if(i == (list.size()-1))
				{
					if(d1.after(d3))
					{
						isCanChoose = true;
						break;
					}
				}
				//如果想要预定的时间段晚于此门所有占用时间段，此想要预定的时间段可被占用
				else
				{
					Date d4 = df.parse(list.get(i+1));
					if(d1.after(d3) && d2.before(d4))
					{
						isCanChoose = true;
						break;
					}
					i++;
				}
				
			}
			return isCanChoose;
		}
		catch (Exception e)
		{
			throw new Exception("judgeStartAndEndTimeIsCanChoose(String start_time, String end_time, List<String> list):"+e);
		}
	}
	
	/**
	 * 遍历list找出是否存在时间允许的时间段
	 * @param start_time
	 * @param end_time
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public boolean judgeStartAndEndTimeIsCanChoose(String start_time,List<String> list) throws Exception
	{
		try
		{
			boolean isCanChoose = false;
//			for (int i = 0; i < list.size(); i++)
//			{
				//遍历所有时间进行处理，与传进来的时间进行比较
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String time = list.get(list.size()-1);
				Date d1 = df.parse(start_time);
				Date d3 = df.parse(time);
				//想要预定的时间段是否在已经占用的时间段是前一个结束时间与后一个开始时间之间
//				if(i == (list.size()-1))
//				{
					if(d1.after(d3))
					{
						isCanChoose = true;
//						break;
					}
//				}
//			}
			return isCanChoose;
		}
		catch (Exception e)
		{
			throw new Exception("judgeStartAndEndTimeIsCanChoose(String start_time, List<String> list):"+e);
		}
	}
	
	
	/**
	 * 想要占用的位置是否被占用
	 * @return
	 * @throws Exception
	 */
	public boolean getBookLocationInfoCanChooseByLocId(String start_time, long ps_id, long loc_id) throws Exception
	{
		try
		{
		
			//可用门或位置除了与已有的时间不冲突，还包括未占用过的
			boolean isCanChoose = false;
			DBRow[] rowsOcc = floorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.LOCATION, loc_id, 0, 0, LoadUnloadOccupancyStatusKey.BOOKING,"", "", "", "", ps_id, 0, 0);
			if(0 != rowsOcc.length)
			{
				isCanChoose = false;
			}
			else
			{
				DBRow[] rows = floorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.LOCATION, loc_id, 0, 0, LoadUnloadOccupancyStatusKey.BOOKING,"", "", "", "", ps_id, 0, 0);
				if(0 == rows.length)
				{
					isCanChoose = true;
				}
				else
				{
					List<String> list		= new ArrayList<String>();
					for (int i = 0; i < rows.length; i++) 
					{
						String book_start_time	= rows[i].getString("book_start_time");
						String book_end_time	= rows[i].getString("book_end_time");
						if("".equals(book_start_time) || "".equals(rows[i].getString("book_end_time")))
						{
							list = new ArrayList<String>();
							break;
						}
						else
						{
							list.add(book_start_time);
							list.add(book_end_time);
						}
					}
					//遍历list找出时间允许的门或位置
					if(list.size() > 0)
					{
						isCanChoose = this.judgeStartAndEndTimeIsCanChoose(start_time, list);
					}
				}
				
			}
			return isCanChoose;
		}
		catch (Exception e)
		{
			throw new Exception("getBookLocationInfoCanChooseByLocId(String start_time, String end_time, long ps_id, long loc_id):"+e);
		}
	}
	
	
	public boolean getBookDoorInfoProbableChooseCanOrNot(String start_time, String end_time, long ps_id, long doorId) throws Exception
	{
		try
		{
			//可用门或位置除了与已有的时间不冲突，还包括未占用过的
			boolean isCanChoose = false;
			DBRow[] rows = floorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.DOOR, doorId, 0, 0, LoadUnloadOccupancyStatusKey.BOOKING,"", "", "", "", ps_id, 0, 0);
			if(0 == rows.length)
			{
				isCanChoose = true;
			}
			else
			{
				//将此门的所有占用时间段放在一个list里面
				List<String> list = new ArrayList<String>();
				for (int i = 0; i < rows.length; i++) 
				{
					String book_start_time	= rows[i].getString("book_start_time");
					String book_end_time	= rows[i].getString("book_end_time");
					list.add(book_start_time);
					list.add(book_end_time);
				}
				//遍历list找出是否存在时间允许的时间段
				isCanChoose = this.judgeStartAndEndTimeIsProbableChoose(start_time, end_time, list);
			}
			
			return isCanChoose;
		}
		catch (Exception e)
		{
			throw new Exception("getBookDoorInfoProbableChooseCanOrNot(String start_time, String end_time, long ps_id, long doorId):"+e);
		}
	}
	
	public boolean getBookLocInfoProbableChooseCanOrNot(String start_time, String end_time, long ps_id, long loc_id) throws Exception
	{
		try
		{
			//可用门或位置除了与已有的时间不冲突，还包括未占用过的
			boolean isCanChoose = false;
			DBRow[] rows = floorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(LoadUnloadOccupancyTypeKey.LOCATION, loc_id, 0, 0, LoadUnloadOccupancyStatusKey.BOOKING,"", "", "", "", ps_id, 0, 0);
			if(0 == rows.length)
			{
				isCanChoose = true;
			}
			else
			{
				//将此门的所有占用时间段放在一个list里面
				List<String> list = new ArrayList<String>();
				for (int i = 0; i < rows.length; i++) 
				{
					String book_start_time	= rows[i].getString("book_start_time");
					String book_end_time	= rows[i].getString("book_end_time");
					list.add(book_start_time);
					list.add(book_end_time);
				}
				//遍历list找出是否存在时间允许的时间段
				isCanChoose = this.judgeStartAndEndTimeIsProbableChoose(start_time, end_time, list);
			}
			
			return isCanChoose;
		}
		catch (Exception e)
		{
			throw new Exception("getBookLocInfoProbableChooseCanOrNot(String start_time, String end_time, long ps_id, long loc_id):"+e);
		}
	}
	
	/**
	 * 遍历list找出是否可能存在时间允许的时间段
	 * @param start_time
	 * @param end_time
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public boolean judgeStartAndEndTimeIsProbableChoose(String start_time, String end_time, List<String> list) throws Exception
	{
		try
		{
			SystemConfig systemConfig =(SystemConfig) MvcUtil.getBeanFromContainer("systemConfig");
			long can_cross_time =  Long.parseLong(systemConfig.getStringConfigValue("can_cross_time"));
			boolean isCanChoose = false;
			//遍历list找出是否存在时间允许的时间段
			for (int i = 0; i < list.size(); i++)
			{
				//遍历所有时间进行处理，与传进来的时间进行比较
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String time = list.get(i);
				long d1 = df.parse(start_time).getTime();
				long d2 = df.parse(end_time).getTime();
				long d3 = df.parse(time).getTime();
				//如果想要预定的时间段早于此门所有占用时间段，此想要预定的时间段可被占用
				if(i == 0)
				{
					if(d2 <= d3)
					{
						isCanChoose = true;
						break;
					}
					else
					{
						//在一定范围内也可以,5分钟
						long difference = Math.abs(d2 - d3);//毫秒值
						if(difference <= can_cross_time)
						{
							isCanChoose = true;
							break;
						}
					}
				}
				//想要预定的时间段是否在已经占用的时间段是前一个结束时间与后一个开始时间之间
				else if(i == (list.size()-1))
				{
					if(d1 >= d3)
					{
						isCanChoose = true;
						break;
					}
					else
					{
						//在一定范围内也可以,5分钟
						long difference = Math.abs(d3 - d1);//毫秒值
						if(difference <= can_cross_time)
						{
							isCanChoose = true;
							break;
						}
					}
				}
				//如果想要预定的时间段晚于此门所有占用时间段，此想要预定的时间段可被占用
				else
				{
					long d4 = df.parse(list.get(i+1)).getTime();
					if(d1 >= d3 && d2 <= d4)
					{
						isCanChoose = true;
						break;
					}
					else if(d1 >= d3 && d2 >= d4)
					{
						//在一定范围内也可以,5分钟
						long difference = d2 - d4;
						if(difference <= can_cross_time)
						{
							isCanChoose = true;
							break;
						}
					}
					else if(d1 <= d3 && d2 <= d4)
					{
						//在一定范围内也可以,5分钟
						long difference = d3 - d1;//毫秒值
						if(difference <= can_cross_time)
						{
							isCanChoose = true;
							break;
						}
					}
					else if(d1 <= d3 && d2 >= d4)
					{
						//在一定范围内也可以,5分钟
						long difference1 = Math.abs(d3 - d1);//毫秒值
						long difference2 = Math.abs(d2 - d4);
						if(difference1 <= can_cross_time && difference2 <= can_cross_time)
						{
							isCanChoose = true;
							break;
						}
					}
					i++;
				}
			}
			return isCanChoose;
		}
		catch (Exception e)
		{
			throw new Exception("judgeStartAndEndTimeIsCanChoose(String start_time, String end_time, List<String> list):"+e);
		}
	}
	
	/**
	 * 得到可能利用的门或位置
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBookDoorInfoProbableChoose(String start_time, String end_time, long ps_id, PageCtrl pc) throws Exception
	{
		try
		{
			//可用门或位置除了与已有的时间不冲突，还包括未占用过的
			long door_or_loc_id_mand = 0L;
			Map<DBRow, List> door_or_loc_map = new HashMap<DBRow, List>();
			DBRow[] rows = floorDoorOrLocationOccupancyMgrZyj.getBookDoorInfoEndTimeIsNotNull(ps_id);
			List<String> list		= null;
			for (int i = 0; i < rows.length; i++) 
			{
				long door_or_loc_id		= rows[i].get("rl_id", 0L);
				String book_start_time	= rows[i].getString("book_start_time");
				String book_end_time	= rows[i].getString("book_end_time");
				//如果相同，重新创建List，并往list中添加数据
				if(door_or_loc_id_mand != door_or_loc_id)
				{
					list = new ArrayList<String>();
					list.add(book_start_time);
					list.add(book_end_time);
					DBRow row = new DBRow();
					row.add("sd_id", door_or_loc_id);
					row.add("doorId", rows[i].getString("doorId"));
					
					door_or_loc_map.put(row, list);
					door_or_loc_id_mand = door_or_loc_id;
				}
				//如果相同直接放数据
				else
				{
					list.add(book_start_time);
					list.add(book_end_time);
				}
			}
			//遍历map找出时间允许的门或位置
			List<DBRow> rowList = new ArrayList<DBRow>();
			Set<Map.Entry<DBRow, List>> set = (Set<Entry<DBRow, List>>) door_or_loc_map.entrySet();
			for (Iterator iterator = set.iterator(); iterator.hasNext();) 
			{
				boolean isCanChoose = false;
				Entry<DBRow, List> entry = (Entry<DBRow, List>) iterator.next();
				DBRow key			 = entry.getKey();
				List<String> allTime = entry.getValue();
				//遍历list找出是否存在时间允许的时间段
				isCanChoose = this.judgeStartAndEndTimeIsProbableChoose(start_time, end_time, allTime);
				if(isCanChoose)
				{
					rowList.add(key);
				}
			}
			DBRow[] isNotOccupancyDoors = floorDoorOrLocationOccupancyMgrZyj.getNotOccupancyDoors(ps_id);
			DBRow[] handleRows = new DBRow[rowList.size()];
			rowList.toArray(handleRows);
			DBRow[] reRows	= new DBRow[isNotOccupancyDoors.length + handleRows.length];
			System.arraycopy(handleRows, 0, reRows, 0, handleRows.length);
			System.arraycopy(isNotOccupancyDoors, 0, reRows, handleRows.length, isNotOccupancyDoors.length);
			
			return this.makeResutltSetsPageCtrl(reRows, pc);
		}
		catch (Exception e)
		{
			throw new Exception("getBookDoorInfoProbableChoose(String start_time, String end_time, long ps_id, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 得到可能利用的门或位置
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBookLocationInfoProbableChoose(String start_time, String end_time, long ps_id, PageCtrl pc) throws Exception
	{
		try
		{
			//可用门或位置除了与已有的时间不冲突，还包括未占用过的
			long door_or_loc_id_mand = 0L;
			Map<DBRow, List> door_or_loc_map = new HashMap<DBRow, List>();
			List<String> list		= null;
			DBRow[] rows = floorDoorOrLocationOccupancyMgrZyj.getBookLocationInfoEndTimeIsNotNull(ps_id);
			for (int i = 0; i < rows.length; i++) 
			{
				long door_or_loc_id		= rows[i].get("rl_id", 0L);
				String book_start_time	= rows[i].getString("book_start_time");
				String book_end_time	= rows[i].getString("book_end_time");
				//如果相同，重新创建List，并往list中添加数据
				if(door_or_loc_id_mand != door_or_loc_id)
				{
					list = new ArrayList<String>();
					list.add(book_start_time);
					list.add(book_end_time);
					DBRow row = new DBRow();
					row.add("id", door_or_loc_id);
					row.add("location_name", rows[i].getString("location_name"));
					door_or_loc_map.put(row, list);
					door_or_loc_id_mand = door_or_loc_id;
				}
				//如果相同直接放数据
				else
				{
					list.add(book_start_time);
					list.add(book_end_time);
				}
			}
			//遍历map找出时间允许的门或位置
			List<DBRow> rowList = new ArrayList<DBRow>();
			Set<Map.Entry<DBRow, List>> set = (Set<Entry<DBRow, List>>) door_or_loc_map.entrySet();
			for (Iterator iterator = set.iterator(); iterator.hasNext();) 
			{
				boolean isCanChoose = false;
				Entry<DBRow, List> entry = (Entry<DBRow, List>) iterator.next();
				DBRow key			 = entry.getKey();
				List<String> allTime = entry.getValue();
				//遍历list找出是否存在时间允许的时间段
				isCanChoose = this.judgeStartAndEndTimeIsProbableChoose(start_time, end_time, allTime);
				if(isCanChoose)
				{
					rowList.add(key);
				}
			}
			DBRow[] isNotOccupancyLocs = floorDoorOrLocationOccupancyMgrZyj.getNotOccupancyLocations(ps_id);
			DBRow[] handleRows = new DBRow[rowList.size()];
			rowList.toArray(handleRows);
			DBRow[] reRows	= new DBRow[isNotOccupancyLocs.length + handleRows.length];
			System.arraycopy(handleRows, 0, reRows, 0, handleRows.length);
			System.arraycopy(isNotOccupancyLocs, 0, reRows, handleRows.length, isNotOccupancyLocs.length);
			
			return this.makeResutltSetsPageCtrl(reRows, pc);
		}
		catch (Exception e)
		{
			throw new Exception("getBookLocationInfoProbableChoose(String start_time, String end_time, long ps_id, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 查询门
	 * @param doorName 门牌号
	 * @param ps_id 仓库ID
	 * @param occupancyType 占用类型
	 * @param rel_type 关联单据类型
	 * @param rel_id 关联单据号
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageDoorsByNamePsRel(String doorName, long ps_id, int occupancyType, int rel_type, long rel_id, PageCtrl pc) throws Exception
	{
		try
		{
			return floorDoorOrLocationOccupancyMgrZyj.getStorageDoorsByNamePsRel(doorName, ps_id, occupancyType, rel_type, rel_id, pc);
		}
		catch (Exception e)
		{
			throw new Exception("getStorageDoorsByNamePsRel(String doorName, long ps_id, int occupancyType, int rel_type, long rel_id):"+e);
		}
	}
	
	/**
	 * 查询位置
	 * @param locName 位置编号
	 * @param ps_id 仓库ID
	 * @param occupancyType 占用类型
	 * @param rel_type 关联单据类型
	 * @param rel_id 关联单据号
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationsByNamePsRel(String locName, long ps_id, int occupancyType, int rel_type, long rel_id, PageCtrl pc) throws Exception
	{
		try
		{
			return floorDoorOrLocationOccupancyMgrZyj.getStorageLocationsByNamePsRel(locName, ps_id, occupancyType, rel_type, rel_id, pc);
		}
		catch (Exception e)
		{
			throw new Exception("getStorageLocationsByNamePsRel(String locName, long ps_id, int occupancyType, int rel_type, long rel_id):"+e);
		}
	}
	
	/**
	 * 查出转运单的订定门或位置信息，更新其结束时间及状态
	 * @param transport_id
	 * @throws Exception
	 */
	public void updateDoorOrLocationStatusAndTime(long transport_id, int status) throws Exception
	{
		try
		{
			DBRow[] locationOccupancysUnloads = new DBRow[0];
			if(TransportOrderKey.INTRANSIT == status || TransportOrderKey.READY == status)//起运
			{
				locationOccupancysUnloads = this.getDoorOrLocationOccupancys(0,0,ProductStoreBillKey.TRANSPORT_ORDER, transport_id , 
						LoadUnloadOccupancyStatusKey.BOOKING, null, null, null, null,0,2,TransportRegistrationTypeKey.SEND);
			}
			else if(TransportOrderKey.FINISH == status || TransportOrderKey.APPROVEING == status || TransportOrderKey.AlREADYRECEIVE == status)//入库、核审中、到货派送
			{
				locationOccupancysUnloads = this.getDoorOrLocationOccupancys(0,0,ProductStoreBillKey.TRANSPORT_ORDER, transport_id , 
						LoadUnloadOccupancyStatusKey.BOOKING, null, null, null, null,0,2,TransportRegistrationTypeKey.DELEIVER);
				
			}
			if(locationOccupancysUnloads.length > 0)
			{
				for (int i = 0; i < locationOccupancysUnloads.length; i++)
				{
					DBRow occupancyInfo = new DBRow();
					if("".equals(locationOccupancysUnloads[i].getString("book_end_time")))
					{
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						occupancyInfo.add("book_end_time", sdf.format(DateUtil.Now()));
					}
					if(0 == locationOccupancysUnloads[i].get("occupancy_status", 0) || LoadUnloadOccupancyStatusKey.BOOKING == locationOccupancysUnloads[i].get("occupancy_status", 0))
					{
						occupancyInfo.add("occupancy_status", LoadUnloadOccupancyStatusKey.RELEASE);
					}
					floorDoorOrLocationOccupancyMgrZyj.updateDoorOrLocationOccupancy(locationOccupancysUnloads[i].get("id", 0L), occupancyInfo);
				}
			}
		}
		catch (Exception e)
		{
			throw new Exception("updateDoorOrLocationStatusAndTime(long transport_id):"+e);
		}
	}
	
	
	/**
	 * 添加门或位置的使用信息
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow addDoorOrLocationOccupancysFromXml(String xml, AdminLoginBean adminLoggerBean) throws Exception
	{
		try
		{
			
			////system.out.println("xml:"+xml);
			
			DBRow result = new DBRow();
			long adid					= adminLoggerBean.getAdid();
			
			int rel_type				= Integer.parseInt(StringUtil.getSampleNode(xml, "rel_type"));
			long rel_id					= Long.parseLong(StringUtil.getSampleNode(xml, "rel_id"));
			int rel_occupancy_use		= Integer.parseInt(StringUtil.getSampleNode(xml, "rel_occupancy_use"));
			long ps_id					= adminLoggerBean.getPs_id();
				//Long.parseLong(StrUtil.getSampleNode(xml, "ps_id"));
			String adminUserIds			= StringUtil.getSampleNode(xml, "adminUserIds");
			String mail					= StringUtil.getSampleNode(xml, "isNeedMail");
			String message				= StringUtil.getSampleNode(xml, "isNeedMessage");
			String page					= StringUtil.getSampleNode(xml,"isNeedPage");
			String doorOrLocationOccupancy = StringUtil.getSampleNode(xml, "doorOrLocationOccupancy");
			
			int mailInt					= parseStringToInt(mail);
			int msgInt					= parseStringToInt(message);
			int pageInt					= parseStringToInt(page);
			
			String[] occupancy_ids = new String[0];
			String[] occupancy_types = new String[0];
			String[] book_start_times = new String[0];
			String[] occupancys = doorOrLocationOccupancy.split("\\|");
			if(null != occupancys)
			{
				occupancy_ids = new String[occupancys.length];
				occupancy_types = new String[occupancys.length]; 
				book_start_times = new String[occupancys.length];
				for (int i = 0; i < occupancys.length; i++)
				{
					String occupancyIdTypeTimes = occupancys[i];
					if(null != occupancyIdTypeTimes && !"".equals(occupancyIdTypeTimes))
					{
						String[] idTypeTimes = occupancyIdTypeTimes.split(",");
						if(null != idTypeTimes)
						{
							occupancy_ids[i] = idTypeTimes[0];
							occupancy_types[i] = idTypeTimes[1];
							book_start_times[i] = idTypeTimes[2];
						}
					}
				}
			}
			
			boolean isCanSave = false;
			
			if(null != occupancy_ids && occupancy_ids.length > 0)
			{
				//如果需要判断所提交的时间是否交叉，如果不能同时在两个门或位置缺货
				//isCanSave = this.checkBookOccupancyTimeIsInterleaved(book_start_times, book_end_times);
				//if(isCanSave)
				//{
					for (int i = 0; i < occupancy_ids.length; i++)
					{
						int occupancy_type	= !"".equals(occupancy_types[i])?Integer.parseInt(occupancy_types[i]):0;
						
						if(LoadUnloadOccupancyTypeKey.DOOR == occupancy_type)
						{
							isCanSave = getBookDoorInfoCanChooseByDoorId(book_start_times[i], ps_id, Long.parseLong(occupancy_ids[i]));
							
						}
						else if(LoadUnloadOccupancyTypeKey.LOCATION == occupancy_type)
						{
							isCanSave = getBookLocationInfoCanChooseByLocId(book_start_times[i], ps_id, Long.parseLong(occupancy_ids[i]));
						}
						if(!isCanSave)
						{
							break;
						}
					}
					//如果可以被占用保存
					if(isCanSave)
					{
						for (int i = 0; i < occupancy_ids.length; i++)
						{
								int occupancy_type	= !"".equals(occupancy_types[i])?Integer.parseInt(occupancy_types[i]):0;
								long occupancy_id	= !"".equals(occupancy_ids[i])?Integer.parseInt(occupancy_ids[i]):0;
								this.addDoorOrLocationOccupancy(occupancy_type, occupancy_id, book_start_times[i],"", rel_type,rel_id,rel_occupancy_use,ps_id, adid);
						}
						this.updateTransportRegistratioinAndStatus(rel_occupancy_use, rel_id, adid, adminLoggerBean.getEmploye_name(), mailInt, msgInt, pageInt,adminUserIds, adminLoggerBean);
					}
				//}
			}
			result.add("flag", isCanSave+"");
			return result;
		}
		catch (Exception e) {
			throw new Exception("addDoorOrLocationOccupancy(HttpServletRequest request):"+e);
		}
	}
	
	
	private int parseStringToInt(String str)
	{
		int reInt = 0;
		if(null != str && !"".equals(str))
		{
			reInt = Integer.parseInt(str);
		}
		else
		{
			reInt = 0;
		}
		return reInt;	
	}
	
	
	public void setFloorDoorOrLocationOccupancyMgrZyj(
			FloorDoorOrLocationOccupancyMgrZyj floorDoorOrLocationOccupancyMgrZyj) {
		this.floorDoorOrLocationOccupancyMgrZyj = floorDoorOrLocationOccupancyMgrZyj;
	}

	public void setFloorTransportMgrZyj(FloorTransportMgrZyj floorTransportMgrZyj) {
		this.floorTransportMgrZyj = floorTransportMgrZyj;
	}

	public void setTransportMgrZyj(TransportMgrZyjIFace transportMgrZyj) {
		this.transportMgrZyj = transportMgrZyj;
	}

}
