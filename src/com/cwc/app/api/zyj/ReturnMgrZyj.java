package com.cwc.app.api.zyj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.floor.api.zyj.FloorReturnMgrZyj;
import com.cwc.app.iface.zyj.ReturnMgrIFaceZyj;
import com.cwc.app.key.ReturnServiceAccessoriesKey;
import com.cwc.app.key.ReturnServicePackagingKey;
import com.cwc.app.key.ReturnServicePalletHandleStatusKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ReturnMgrZyj implements ReturnMgrIFaceZyj{

	static Logger log = Logger.getLogger("ACTION");
	private FloorReturnMgrZyj floorReturnMgrZyj;
	
	
	public DBRow[] findReturnPalletRows(long return_receive_id, PageCtrl pc) throws Exception
	{
		try
		{
			return floorReturnMgrZyj.findReturnPalletRows(return_receive_id, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findReturnPalletRows(PageCtrl pc)",log);
		}
	}
	
	/**
	 * 根据传入的条件查询列表
	 */
	public DBRow[] getReturnPalletsByPara(long return_receive_id,String palletNo,String ra_rv,
			String cust_ref_type,String cust_ref_no,PageCtrl pc) throws Exception
	{
		try
		{
			return floorReturnMgrZyj.getReturnPalletsByPara(return_receive_id,palletNo,ra_rv,cust_ref_type,cust_ref_no, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"ReturnMgrZyj",log);
		}
	}
	
	public void addOrUpdateReturnPalletRow(HttpServletRequest request) throws Exception
	{
		try
		{
			long pallet_view_id		= StringUtil.getLong(request, "pallet_view_id");
			String pallet_no		= StringUtil.getString(request, "pallet_no");
			String ra_rv			= StringUtil.getString(request, "ra_rv");
			int cust_ref_type		= StringUtil.getInt(request, "cust_ref_type");
			String cust_ref_no		= StringUtil.getString(request, "cust_ref_no");
			long return_receive_id	= StringUtil.getLong(request, "return_receive_id");
			int status				= StringUtil.getInt(request, "status");
			long creator			= StringUtil.getLong(request, "creator");
			String create_date		= StringUtil.getString(request, "create_date");
			String pallet_process_date	= StringUtil.getString(request, "pallet_process_date");
			
			long logingUserId = new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			
			DBRow row	= new DBRow();
			row.add("pallet_no", pallet_no);
			row.add("ra_rv", ra_rv);
			row.add("cust_ref_type", cust_ref_type);
			row.add("cust_ref_no", cust_ref_no);
			row.add("return_receive_id", return_receive_id);
			row.add("status", status);
			row.add("creator", 0==creator?logingUserId:creator);
			row.add("create_date", "".equals(create_date)?DateUtil.NowStr():create_date);
			row.add("pallet_process_date", pallet_process_date);
			
			if(0 == pallet_view_id)
			{
				floorReturnMgrZyj.addReturnPalletRow(row);
			}
			else
			{
				floorReturnMgrZyj.updateReturnPalletRow(row, pallet_view_id);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addOrUpdateReturnPalletRow(HttpServletRequest request)",log);
		}
	}
	
	
	public void addReturnPalletRowByReceive(String pallet_no, long return_receive_id, long loginUserId) throws Exception
	{
		try
		{
			DBRow row	= new DBRow();
			row.add("pallet_no", pallet_no);
			row.add("return_receive_id", return_receive_id);
			row.add("status", ReturnServicePalletHandleStatusKey.NON_PROCESS);
			row.add("creator", loginUserId);
			row.add("create_date", DateUtil.NowStr());
			
			floorReturnMgrZyj.addReturnPalletRow(row);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addOrUpdateReturnPalletRow(HttpServletRequest request)",log);
		}
	}
	
	
	
	
	public DBRow findReturnPalletRowById(long pallet_view_id) throws Exception
	{
		try
		{
			return floorReturnMgrZyj.findReturnPalletRowById(pallet_view_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findReturnPalletRowById(long pallet_view_id)",log);
		}
	}
	
	public void deleteReturnPalletRow(HttpServletRequest request) throws Exception
	{
		try
		{
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addOrUpdateReturnPalletRow(HttpServletRequest request)",log);
		}
	}
	
	
	public DBRow[] findReturnPalletItemRows(long pallet_view_id, PageCtrl pc) throws Exception
	{
		try
		{
			return floorReturnMgrZyj.findReturnPalletItemRows(pallet_view_id, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findReturnPalletItemRows(long pallet_view_id, PageCtrl pc)",log);
		}
	}


	


	@Override
	public void addOrUpdateReturnPalletItemRow(HttpServletRequest request) throws Exception 
	{
		try
		{
			long pallet_view_id			= StringUtil.getLong(request, "pallet_view_id");
			long item_id 				= StringUtil.getLong(request, "item_id");
			String[] packagings			= request.getParameterValues("packaging");
			String[] accessoriess		= request.getParameterValues("accessories");
			String serial_number		= StringUtil.getString(request, "serial_number");
			String box_serial_number	= StringUtil.getString(request, "box_serial_number");
			String model				= StringUtil.getString(request, "model");
			String sku					= StringUtil.getString(request, "sku");
			int status 					= StringUtil.getInt(request, "status");
			String ra_rv				= StringUtil.getString(request, "ra_rv");
			long creator				= StringUtil.getLong(request, "creator");
			String create_date			= StringUtil.getString(request, "create_date");
			long logingUserId 			= new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			
			String packaging				= stringArrToStr(packagings);
			String accessories				= stringArrToStr(accessoriess);
			
			DBRow row	= new DBRow();
			row.add("pallet_view_id", pallet_view_id);
			row.add("packaging", packaging);
			row.add("accessories", accessories);
			row.add("serial_number", serial_number);
			row.add("box_serial_number", box_serial_number);
			row.add("model", model);
			row.add("sku", sku);
			row.add("status", status);
			row.add("ra_rv", ra_rv);
			row.add("creator", 0==creator?logingUserId:creator);
			row.add("create_date", "".equals(create_date)?DateUtil.NowStr():create_date);
			
			if(0 == item_id)
			{
				floorReturnMgrZyj.addReturnPalletItemRow(row);
				
				
				int pallet_status = StringUtil.getInt(request, "pallet_status");//pallet中的status的当前状态
				int palletStatus = ReturnServicePalletHandleStatusKey.NON_PROCESS ;//pallet中的status的状态为“未在处理”
				//若pallet的status状态为“未处理”时，修改item时修改为“正在处理”。若已经修改，不进行该操作。
				if(palletStatus==pallet_status)
				{
					palletStatus = ReturnServicePalletHandleStatusKey.PROCESSING;//修改pallet中的status的状态为“正在处理”
					floorReturnMgrZyj.updateReturnPalletStatus(palletStatus, pallet_view_id);
				}
				
			}
			else
			{
				floorReturnMgrZyj.updateReturnPalletItemRow(row, item_id);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addReturnPalletItemRow(HttpServletRequest request)",log);
		}
	}
	
	private String stringArrToStr(String[] strArr)
	{
		String str = "";
		if(null != strArr && strArr.length > 0)
		{
			str += ",";
			for (int i = 0; i < strArr.length; i++) 
			{
				str += strArr[i] + ",";
			}
		}
		return str;
	}

	public String changeServicePackagingKeyStrToNameStr(String packaging) throws Exception 
	{
		ReturnServicePackagingKey returnServicePackagingKey = new ReturnServicePackagingKey();
		try
		{
			String nameStr = "";
			if(!StringUtil.isBlank(packaging))
			{
				String[] packageArr = packaging.split(",");
				for (int i = 0; i < packageArr.length; i++) 
				{
					if(!StringUtil.isBlank(packageArr[i]))
					{
						nameStr += ","+returnServicePackagingKey.getReturnServicePackagingKeyById(packageArr[i]);
					}
				}
				if(nameStr.length() > 0)
				{
					nameStr = nameStr.substring(1);
				}
			}
			return nameStr;	
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"changeServicePackagingKeyStrToNameStr(String packaging)",log);
		}
	}
	
	
	public String changeServiceAccessoriesKeyStrToNameStr(String accessories) throws Exception 
	{
		ReturnServiceAccessoriesKey returnServiceAccessoriesKey = new ReturnServiceAccessoriesKey();
		try
		{
			String nameStr = "";
			if(!StringUtil.isBlank(accessories))
			{
				String[] accessoriesArr = accessories.split(",");
				for (int i = 0; i < accessoriesArr.length; i++) 
				{
					if(!StringUtil.isBlank(accessoriesArr[i]))
					{
						nameStr += ","+returnServiceAccessoriesKey.getReturnServiceAccessoriesKeyById(accessoriesArr[i]);
					}
				}
				if(nameStr.length() > 0)
				{
					nameStr = nameStr.substring(1);
				}
			}
			return nameStr;	
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"changeServiceAccessoriesKeyStrToNameStr(String accessories)",log);
		}
	}
	
	
	public DBRow findReturnPalletItemRowById(long item_id) throws Exception
	{
		try
		{
			return floorReturnMgrZyj.findReturnPalletItemRowById(item_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findReturnPalletItemRowById(long item_id)",log);
		}
	}
	
	@Override
	public void deleteReturnPalletItemRow(long item_id) throws Exception {
		try
		{
			floorReturnMgrZyj.deleteReturnPalletItemRow(item_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"deleteReturnPalletItemRow(long item_id)",log);
		}
		
	}
	
	public int countPalletItemCountByPalletId(long pallet_id) throws Exception
	{
		try
		{
			return floorReturnMgrZyj.countPalletItemCountByPalletId(pallet_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"countPalletItemCountByPalletId(long pallet_id)",log);
		}
	}
	
	
	
	public int countPalletCountByReceiveId(long receive_id) throws Exception
	{
		try
		{
			return floorReturnMgrZyj.countPalletCountByReceiveId(receive_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"countPalletCountByReceiveId(long receive_id)",log);
		}
	}
	
	
	
	public void setFloorReturnMgrZyj(FloorReturnMgrZyj floorReturnMgrZyj) {
		this.floorReturnMgrZyj = floorReturnMgrZyj;
	}


	
}
