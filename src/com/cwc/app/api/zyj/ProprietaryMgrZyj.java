package com.cwc.app.api.zyj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.util.Assert;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.gzy.FloorAdminMgrGZY;
import com.cwc.app.floor.api.zr.FloorClpTypeMgrZr;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.ProductCustomer;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.DataOrCountKey;
import com.cwc.app.key.GlobalKey;
import com.cwc.app.key.ImportTitleRelatedExcelErrorKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.lucene.zyj.TitleIndexMgr;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ProprietaryMgrZyj implements ProprietaryMgrIFaceZyj {
	
	static Logger log = Logger.getLogger("ACTION");
	//表示用户身份是Admin
	static final String ADMIN = "Admin";
	//表示用户身份是Customer
	static final String CUSTOMER = "Customer";
	//表示用户身份是Title
	static final String TITLE = "Title";
	
	private FloorProprietaryMgrZyj floorProprietaryMgrZyj;
	
	private FloorAdminMgrGZY floorAdminMgrGZY;
	
	private FloorProductMgr floorProductMgr;
	
	private SystemConfigIFace systemConfig;
	
	private FloorCatalogMgr floorCatalogMgr;
	
	private	FloorProductStoreMgr productStoreMgr;
	
	private FloorClpTypeMgrZr floorClpTypeMgrZr;
	
	private ProprietaryMgrIFaceZyj proprietaryMgrZyj;
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}	
	
	public void setFloorProprietaryMgrZyj( FloorProprietaryMgrZyj floorProprietaryMgrZyj) {
		this.floorProprietaryMgrZyj = floorProprietaryMgrZyj;
	}

	public void setFloorAdminMgrGZY(FloorAdminMgrGZY floorAdminMgrGZY) {
		this.floorAdminMgrGZY = floorAdminMgrGZY;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
	
	public void setFloorCatalogMgr( FloorCatalogMgr floorCatalogMgr ) {
		this.floorCatalogMgr = floorCatalogMgr;
	}

	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

	public void setFloorClpTypeMgrZr(FloorClpTypeMgrZr floorClpTypeMgrZr) {
		this.floorClpTypeMgrZr = floorClpTypeMgrZr;
	}

	public void setProprietaryMgrZyj(ProprietaryMgrIFaceZyj proprietaryMgrZyj) {
		this.proprietaryMgrZyj = proprietaryMgrZyj;
	}

	/**
	 * 添加用户与title之间的关系
	 * @param user_adid
	 * @param title_id
	 * @param title_priority
	 * @return
	 * @throws Exception
	 */
	public long addProprietary(long user_adid, long title_id, int title_priority) throws Exception
	{
		try
		{
			DBRow propAdidRow = new DBRow();
			propAdidRow.add("title_admin_adid", user_adid);
			propAdidRow.add("title_admin_title_id", title_id);
			propAdidRow.add("title_admin_sort", title_priority);
			long title_adid_id = floorProprietaryMgrZyj.addProprietaryAdmin(propAdidRow);
			this.editTitleIndex(title_id, "update");
			return title_adid_id;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietary(long user_adid, long title_id, int title_priority)",log);
		}
	}
	
	/**
	 * 添加title,1:title已经存在；2.此优先级的title已经存在
	 * @author Administrator
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProprietary(HttpServletRequest request) throws Exception{
		
		try{
			String title_name = StringUtil.getString(request,"title_name");
			long user_adid = StringUtil.getLong(request,"user_adid");
			
			//AdminMgr adminMgr = new AdminMgr();
			//AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StrUtil.getSession(request));
			
			return this.addProprietaryOrProprietaryAdmin(title_name, user_adid);
			
		}catch(Exception e){
			throw new SystemException(e,"addProprietary(HttpServletRequest request)",log);
		}
	}
	
	public long addProprietaryOrProprietaryAdmin(String title_name,long user_adid) throws Exception{
		try{
			
			long flag = 0;
			
			//验证此title_name是否已经添加
			if(null != title_name && !"".equals(title_name)){
				
				int propCount = floorProprietaryMgrZyj.findProprietaryCountByTitleName(title_name);
				
				if(propCount > 0){
					flag = -1;
					return flag;
				}
			}
			
			DBRow row = new DBRow();
			row.add("title_name", title_name);
			row.add("creator",user_adid);
			row.add("create_time",DateUtil.NowStr());
			//添加TITLE
			flag = floorProprietaryMgrZyj.addProprietary(row);
			
			//添加用户与TITLE关系
			if(0 != user_adid)
			{
				floorProprietaryMgrZyj.insertTitleAdmin(user_adid,String.valueOf(flag));
			}
			
			//创建TITLE提示
			this.editTitleIndex(flag, "add");
			
			return flag;
		}
		catch (Exception e){
			throw new SystemException(e,"addProprietaryOrProprietaryAdmin(String title_name, long user_adid, long title_id, int title_priority, AdminLoginBean adminLoggerBean) ",log);
		}
	}
	
	/**
	 * -1:title已经存在；-2.此titleID已经被选择；-3.此优先级的title已经存在
	 * @param title_name
	 * @param user_adid
	 * @param title_priority
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public long addProprietaryOrProprietaryAdmin(String title_name, long user_adid, long title_id, int title_priority, AdminLoginBean adminLoggerBean) throws Exception{
		try{
			long flag = 0;
			//验证此title_name是否已经添加
			if(null != title_name && !"".equals(title_name))
			{
				int propCount		= floorProprietaryMgrZyj.findProprietaryCountByTitleName(title_name);
				if(propCount > 0)
				{
					flag = -1;
					return flag;
				}
			}
			//验证此优先级的title是否已经添加
			if(user_adid > 0)
			{
				if(title_id > 0)
				{
					int propAdidCount = floorProprietaryMgrZyj.findProprietaryAdminByAdidAndPriority(user_adid, title_id, 0);
					if(propAdidCount > 0)
					{
						flag = -2;
						return flag;
					}
				}
				int propPrioAdidCount = floorProprietaryMgrZyj.findProprietaryAdminByAdidAndPriority(user_adid, 0, title_priority);
				if(propPrioAdidCount > 0)
				{
					flag = -3;
					return flag;
				}
			}
			
			DBRow row = new DBRow();
			row.add("title_name", title_name);
			if(0 == title_id)
			{
				flag = floorProprietaryMgrZyj.addProprietary(row);
				this.editTitleIndex(flag, "add");
			}
			else
			{
				flag = title_id;
			}
			if(user_adid > 0)
			{
				DBRow propAdidRow = new DBRow();
				propAdidRow.add("title_admin_adid", user_adid);
				propAdidRow.add("title_admin_title_id", flag);
				propAdidRow.add("title_admin_sort", title_priority);
				floorProprietaryMgrZyj.addProprietaryAdmin(propAdidRow);
				this.editTitleIndex(title_id, "update");
			}
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietaryOrProprietaryAdmin(String title_name, long user_adid, long title_id, int title_priority, AdminLoginBean adminLoggerBean) ",log);
		}
	}
	
	/**
	 * 系统管理>帐号管理>title管理>管理title
	 */
	public long addProprietaryAdmins(HttpServletRequest request) throws Exception{
		
		try{
			long flag = 0;
			
			
			long adid = StringUtil.getLong(request,"adid");
			String titleFlag = StringUtil.getString(request,"flag");
			String[] titleId = StringUtil.getString(request,"title_id").split(",");
			
			flag = managerAdminTitle(adid,titleFlag,titleId);
			
			return flag;
			
		}catch (Exception e){
			throw new SystemException(e,"addProprietaryAdmins(HttpServletRequest request)",log);
		}
	}
	
	private long managerAdminTitle(long adid,String titleFlag,String[] titleId) throws Exception{
		try{
			long flag = 0;
			
			if(titleFlag.equals("add")){
				//删除TITLE
				for(int i=0;i<titleId.length;i++){
					floorProprietaryMgrZyj.deleteTitleAdminByAdidAndTitleId(adid,titleId[i]);
				}
			}else{
				//添加TITLE
				for(int i=0;i<titleId.length;i++){
					
					floorProprietaryMgrZyj.insertTitleAdmin(adid,titleId[i]);
				}
			}
			return flag;
		}
		catch (Exception e){
			throw new SystemException(e,"managerAdminTitle(long adid,String titleFlag,String[] titleId)",log);
		}
	}
	
	/*輔助方法	暫時放在這
	 * 
	 * 説明：在數組中查找字符串
	 * 返回：真假
	 * */
	public boolean compareArray(String[] one,String two){
		
		boolean result = false;
		
		for(int i=0;i<one.length;i++){
			
			if(one[i].equals(two)){
				result = true;
				break;
			}
		}
		return result;
	}
	
	private long addProprietaryAdmins(long adid, String[] title_ids) throws Exception{
		try{
			long flag = 0;
			
			//查询已经保存的关联
			DBRow[] hadSavedRow = floorProprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(adid, null, null);
			
			String[] title_ids_once = null;
			if(hadSavedRow.length > 0){
				title_ids_once = new String[hadSavedRow.length];
				for (int i = 0; i < hadSavedRow.length; i++){
					title_ids_once[i]=hadSavedRow[i].getString("title_id");
				}
			}
			
			if(title_ids_once!=null&&title_ids!=null){
				
				String del_titles="";
				//获得要删除数据
				for(int i=0;i<title_ids_once.length;i++){
					
					if(!this.compareArray(title_ids,title_ids_once[i])){
						
						if(del_titles.equals("")){
							del_titles = title_ids_once[i];
						}else{
							del_titles = del_titles +"," + title_ids_once[i];
						}
					}
				}
				if(!del_titles.equals("")){
					String[] del_title = del_titles.split(",");
					//数据库删除
					for(int i=0;i<del_title.length;i++){
						
						floorProprietaryMgrZyj.deleteTitleAdminByAdidAndTitleId(adid,del_title[i]);
					}
				}
			
				String add_titles="";
				//获得要添加数据
				for(int i=0;i<title_ids.length;i++){
					
					if(!this.compareArray(title_ids_once,title_ids[i])){
						if(add_titles.equals("")){
							add_titles = title_ids[i];
						}else{
							add_titles = add_titles + "," + title_ids[i];
						}
					}
				}
				if(!add_titles.equals("")){
					String[] add_title = add_titles.split(",");
					//数据库添加
					for(int i=0;i<add_title.length;i++){
						
						floorProprietaryMgrZyj.insertTitleAdmin(adid,add_title[i]);
					}
				}
			}else if(title_ids_once!=null){
				
				//数据库删除
				for(int i=0;i<title_ids_once.length;i++){
					
					floorProprietaryMgrZyj.deleteTitleAdminByAdidAndTitleId(adid,title_ids_once[i]);
				}
				
			}else{
				//数据库添加
				for(int i=0;i<title_ids.length;i++){
					
					floorProprietaryMgrZyj.insertTitleAdmin(adid,title_ids[i]);
				}
			}
			return flag;
		}
		catch (Exception e){
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 
	 * @param user_adid：用ID
	 * @param proprietary_admins ：即将保存的title，titleId_titleSort
	 * @param isAdd ：验证成功是否添加关系
	 * @param isUp	:验证成功是否更新关系
	 * @param isDel	：验证成功是否删除关系
	 * @return :0.验证成功；-1.title_name重名；-2.未修改titleName
	 * @throws Exception
	 */
	private long addProprietaryAdmins(long user_adid, String[] proprietary_admins, List<DBRow> admin_title_sorts, boolean isAdd, boolean isUp, boolean isDel) throws Exception{
		try{
			long flag = 0;
			//已经保存了的关系
			DBRow[] hadSavedRow = floorProprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(user_adid, null, null);
			Map<String, String> hadSavedIds = new HashMap<String, String>();
			
			if(hadSavedRow.length > 0){
				for (int i = 0; i < hadSavedRow.length; i++){
					
					hadSavedIds.put(hadSavedRow[i].getString("title_id"), hadSavedRow[i].getString("title_admin_sort"));
				}
				
			}
			
			//即将保存的关系
			Map<String, String> willSaveIds = new HashMap<String, String>();
			
			if(null != proprietary_admins && proprietary_admins.length > 0){
				for (int i = 0; i < proprietary_admins.length; i++){
					
					String proprietary_admin = proprietary_admins[i];
					
					if(!"".equals(proprietary_admin)){
						
						String[] idAndSort = proprietary_admin.split("_");
						willSaveIds.put(idAndSort[0], idAndSort[1]);
					}
				}
			}
			
			if(null != admin_title_sorts && admin_title_sorts.size() > 0){
				
				for (int i = 0; i < admin_title_sorts.size(); i++){
					willSaveIds.put(admin_title_sorts.get(i).getString("titleId"), admin_title_sorts.get(i).getString("adminTitleSort"));
				}
			}
			//验证
			flag = this.handAddOrRemoveProprietaryAdmin(hadSavedIds, willSaveIds, user_adid,isAdd, isUp, isDel);
			
			return flag;
		}catch (Exception e){
			throw new SystemException(e,"addProprietaryAdmins(long user_adid, String[] proprietary_admins, List<DBRow> admin_title_sorts, boolean isAdd, boolean isUp, boolean isDel)",log);
		}
	}
	/**
	 * 验证titleSort并操作
	 * @param hadSavedIds :已经保存的，<title_id， title_sort>
	 * @param willSaveIds :将要保存的，<title_id， title_sort>
	 * @param user_adid	：用户ID
	 * @param isAdd ：验证之后，是否添加
	 * @param isUp ：验证之后，是否更新
	 * @param isDel ：验证之后，是否删除
	 * @return :0.验证成功；-1.title_name重名；-2.未修改titleName
	 * @throws Exception
	 */
	private long handAddOrRemoveProprietaryAdmin(Map<String, String> hadSavedIds, Map<String, String> willSaveIds, long user_adid, boolean isAdd, boolean isUp, boolean isDel) throws Exception{
		
		try{
			
			long flag = 0L;
			Map<String, String> addAndSames	= new HashMap<String, String>();
			//添加的关系
			Map<String, String> adds = new HashMap<String, String>();
			//删除的关系
			List<String> removes = new ArrayList<String>();
			Map<String, String> sames = new HashMap<String, String>();
			
			//得到相同的和将要被删除的
			Set<Map.Entry<String, String>> hadSavedIdsSet = hadSavedIds.entrySet();//已经保存
			
			for (Iterator iterator = hadSavedIdsSet.iterator(); iterator.hasNext();) {
				
				Entry<String, String> entry = (Entry<String, String>) iterator.next();
				String hadSavedId	= entry.getKey();
				String hadSavedSort	= entry.getValue();
				
				//将要保存的ids中不包含已经保存的，删除
				if(!willSaveIds.containsKey(hadSavedId)){
					removes.add(hadSavedId);
					
				}else{
					sames.put(hadSavedId, hadSavedSort);
					addAndSames.put(hadSavedId, hadSavedSort);
				}
			}
			//得到将要添加的
			Set<Map.Entry<String, String>> willSaveSet = willSaveIds.entrySet();
			
			for (Iterator iterator = willSaveSet.iterator(); iterator.hasNext();){
				
				Entry<String, String> entry = (Entry<String, String>) iterator.next();
				String titleId = entry.getKey();
				String titleSort = entry.getValue();
				//已经保存的不包含将要保存的，添加
				if(!sames.containsKey(titleId))
				{
					adds.put(titleId, titleSort);
					addAndSames.put(titleId, titleSort);
				}
				//已经保存的包含将要保存的，更新
				else
				{
					//先移除后添加
					addAndSames.remove(titleId);
					addAndSames.put(titleId, titleSort);
					sames.remove(titleId);
					sames.put(titleId, titleSort);
				}
			}
			//验证sort是否相同，都不相同才能删除或添加
			Set<Map.Entry<String, String>> addAndSamesSet = addAndSames.entrySet();
			Set<String> addAndSamesSorts = new HashSet<String>();
			for (Iterator iterator = addAndSamesSet.iterator(); iterator.hasNext();) 
			{
				Entry<String, String> entry = (Entry<String, String>) iterator.next();
				String addAndSamesId	= entry.getKey();
				String addAndSamesSort	= entry.getValue();
				if(!addAndSamesSorts.add(addAndSamesSort))
				{
					flag = -3;
				}
			}
			if(0 == flag)
			{
				//删除
				if(isDel)
				{
					for (int i = 0; i < removes.size(); i++) 
					{
//						//system.out.println("remove"+i+":"+removes.get(i));
						floorProprietaryMgrZyj.deleteProprietaryAdminById(0, user_adid, Long.parseLong(removes.get(i)));
						this.editTitleIndex(Long.parseLong(removes.get(i)), "update");
					}
				}
				
				//添加
				if(isAdd)
				{
					Set<Map.Entry<String, String>> addSaveSet = adds.entrySet();
					for (Iterator iterator = addSaveSet.iterator(); iterator.hasNext();) 
					{
						Entry<String, String> entry = (Entry<String, String>) iterator.next();
						String titleId = entry.getKey();
						String titleSort = entry.getValue();
//						//system.out.println("add:"+user_adid+","+titleId+","+titleSort);
						flag = addProprietary(user_adid, Long.parseLong(titleId), Integer.parseInt(titleSort));
					}
				}
				
				//更新，sames
				if(isUp)
				{
					Set<Map.Entry<String, String>> samesSet = sames.entrySet();
					for (Iterator iterator = samesSet.iterator(); iterator.hasNext();) 
					{
						Entry<String, String> entry = (Entry<String, String>) iterator.next();
						String sameTitleId		= entry.getKey();
						String sameTitleSort	= entry.getValue();
						DBRow update			= new DBRow();
						update.add("title_admin_sort", sameTitleSort);
						update.add("title_admin_adid", user_adid);
//						//system.out.println("upate:"+user_adid+","+sameTitleId+","+sameTitleSort);
						floorProprietaryMgrZyj.updateProprietaryAdminByTitleId(Long.parseLong(sameTitleId), user_adid, update);
					}
				}
				
			}
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"handAddOrRemoveProprietaryAdmin(Map<String, String> hadSavedIds, Map<String, String> willSaveIds, long user_adid, boolean isAdd, boolean isUp, boolean isDel)",log);
		}
	}
	
	/**
	 * 根据用户ID查询title
	 * @author Administrator
	 * @return 0.保存成功；-1.title_name重名；-2.未修改titleName
	 * @throws Exception
	 */
	@Override
	public long updateProprietary(HttpServletRequest request) throws Exception
	{
		try
		{
			long flag = 0;
			long title_id		= StringUtil.getLong(request, "title_id");
			String title_name	= StringUtil.getString(request, "title_name");
			DBRow titleRow		= floorProprietaryMgrZyj.findProprietaryByTitleName(title_name);
			DBRow row = new DBRow();
			if(null != titleRow)
			{
				if(title_id == titleRow.get("title_id", 0L))
				{
					flag = -2;
				}
				else
				{
					flag = -1;
				}
			}
			else
			{
				row.add("title_name", title_name);
				floorProprietaryMgrZyj.updateProprietary(title_id, row);
				this.editTitleIndex(title_id, "update");
			}
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateProprietary(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 通过title_id获取title
	 */
	public DBRow findProprietaryByTitleId(long title_id) throws Exception
	{
		try
		{
			return floorProprietaryMgrZyj.findProprietaryByTitleId(title_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryByTitleId(long title_id)",log);
		}
	}

	/**
	 * 根据用户查询title
	 * @author Administrator
	 * @param request
	 * @throws Exception
	 */
	@Override
	public DBRow[] findProprietaryAllOrByAdidTitleId(HttpServletRequest request) throws Exception
	{
		try
		{
			long adid		= StringUtil.getLong(request, "user_adid");
			String title_id	= StringUtil.getString(request, "title_id");
			return floorProprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(adid, this.parseStrToLongArr(title_id), null);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryAllOrByAdidTitleId(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 根据用户查询title
	 * @author Administrator
	 * @param request
	 * @throws Exception
	 */
	public DBRow[] findProprietaryAllRelatesOrByAdidTitleId(boolean adidNullToLogin, long adid, String title_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			DBRow[] rows = this.findProprietaryAllOrByAdidTitleId(adidNullToLogin, adid, title_id, pc, request);
			for (int i = 0; i < rows.length; i++)
			{
				DBRow[] adminRows = this.findAdminsByTitleId(adidNullToLogin,adid, rows[i].get("title_id", 0), 0 , 0, null, request);
				rows[i].add("admins", adminRows);
				DBRow[] pcLineRows = this.findProductLinesByTitleId(adidNullToLogin, adid, "", rows[i].getString("title_id"), 0, 0, null, request);
				rows[i].add("pcLines", pcLineRows);
				DBRow[] pcCataRows = this.findProductCatagorysByTitleId(adidNullToLogin, adid, "", "", "", rows[i].getString("title_id"), 0, 0, null, request);
				rows[i].add("pcCatagorys", pcCataRows);
				DBRow[] productRows = this.findProductsByTitleId(adidNullToLogin, adid, 0L, null, rows[i].get("title_id", 0), 0, 0, null, request);
				rows[i].add("products", productRows);
//				DBRow[] row = (DBRow[]) rows[i].get("admins", new DBRow[0]);
//				//system.out.println(rows[i].get("title_id", 0L)+","+row.length);;
			}
			return rows;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryAllRelatesOrByAdidTitleId(boolean adidNullToLogin, long adid, long title_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	public DBRow[] findProprietaryAllOrByAdidTitleId(boolean adidNullToLogin, long adid, String title_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(adid, this.parseStrToLongArr(title_id), pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryAllOrByAdidTitleId(boolean adidNullToLogin, long adid, long title_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}

	public DBRow[] findProprietaryAllByAdid(long adid, PageCtrl pc) throws Exception
	{
		try
		{
			return floorProprietaryMgrZyj.findProprietaryAllByAdid(adid, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryAllByAdid(long adid, PageCtrl pc)",log);
		}
	}
	
	
	/**
	 * 删除title
	 */
	public int deleteProprietary(HttpServletRequest request) throws Exception
	{
		try
		{
			long title_id = StringUtil.getLong(request, "title_id");
			return this.deleteProprietary(title_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"deleteProprietary(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 删除title,1:删除成功；2.存在关系不能删除
	 * @param title_id
	 * @throws Exception
	 */
	public int deleteProprietary(long title_id) throws Exception
	{
		try
		{
			int flag = 0;
			int countAdmin = floorProprietaryMgrZyj.findProprietaryAdminByAdidAndPriority(0L, title_id, 0);
			int countPcLine = floorProprietaryMgrZyj.findProprietaryPcLineCountByTitleIdPcId(title_id, 0L);
			int countPcCata = floorProprietaryMgrZyj.findProprietaryCatagoryCountByTitleIdPcId(title_id, 0L);
			int countProduct = floorProprietaryMgrZyj.findProprietaryProductCountByTitleIdPcId(title_id, 0L);
			
			if(0 != countAdmin){
				
				flag = 2;
				
			}else if(0 != countPcLine){
				
				flag = 3;
				
			}else if(0 != countPcCata){
				
				flag = 4;
				
			}else if(0 != countProduct){
				
				flag = 5;
				
			}else{
				
				flag = 1;
				floorProprietaryMgrZyj.deleteProprietary(title_id);
				this.editTitleIndex(title_id, "del");
			}
			
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"deleteProprietary(long title_id)",log);
		}
	}
	
	/**
	 * 删除用户与title关系
	 * @param request
	 * @throws Exception
	 */
	public void deleteProprietaryAdmin(HttpServletRequest request) throws Exception
	{
		try
		{
			long title_admin_id	= StringUtil.getLong(request, "title_admin_id");
			long adid			= StringUtil.getLong(request, "admin_user");
			long title_admin_title_id = StringUtil.getLong(request, "title_admin_title_id");
			this.deleteProprietaryAdmin(title_admin_id, adid, title_admin_title_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"deleteProprietaryAdmin(HttpServletRequest request)",log);
		}
	}
	
	
	/**
	 * 通过ID或者用户及titleId删除关系
	 * @param title_admin_id
	 * @param adid
	 * @param title_admin_title_id
	 * @throws Exception
	 */
	public void deleteProprietaryAdmin(long title_admin_id, long adid, long title_admin_title_id) throws Exception
	{
		try
		{
			floorProprietaryMgrZyj.deleteProprietaryAdminById(title_admin_id, adid, title_admin_title_id);
			this.editTitleIndex(title_admin_title_id, "update");
		}
		catch (Exception e)
		{
			throw new SystemException(e,"deleteProprietaryAdmin(long title_admin_id, long adid, long title_admin_title_id)",log);
		}
	}
	
	public DBRow[] findProprietaryByAdidAndTitleId(long adid, String title_id, PageCtrl pc, HttpServletRequest request) throws Exception {
		try
		{
			adid = this.findAdminIdByCondition(adid, request);
			return floorProprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(adid, this.parseStrToLongArr(title_id), pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryByAdidAndTitleId(long adid, long title_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}

	
	/**
	 * 根据用户及titleId查询title
	 * @author Administrator
	 * @param request
	 * @throws Exception
	 */
	public DBRow[] findProprietaryByAdidProduct(long adid, long title_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adid, request);
			return floorProprietaryMgrZyj.findProprietaryAllByAdminProduct(adid, pc_id, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryByAdidProduct(long adid, long title_id, long pc_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 根据用户及产品线Id查询title
	 * @author Administrator
	 * @throws Exception
	 */
	public DBRow[] findProprietaryAllByAdidAndProductLineId(long adid, long product_line_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adid, request);
			return floorProprietaryMgrZyj.findProprietaryAllByAdminProductLine(adid, product_line_id, pc_id, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryAllByAdidAndProductLineId(long adid, long product_line_id, long pc_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 根据用户及产品线Id查询title
	 * @author Administrator
	 * @throws Exception
	 */
	public DBRow[] findProprietaryByAdidAndProductLineId(long adid, long product_line_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adid, request);
			return floorProprietaryMgrZyj.findProprietaryByAdminProductLine(adid, product_line_id, pc_id, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryByAdidAndProductLineId(long adid, long product_line_id, long pc_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 根据用户及分类Id查询title
	 * @author Administrator
	 * @throws Exception
	 */
	public DBRow[] findProprietaryAllByAdidAndProductCatagoryId(long adid, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adid, request);
			return floorProprietaryMgrZyj.findProprietaryAllByAdminProductCatagory(adid, product_catagory_id, pc_id, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryByAdidAndProductCatagoryId(long adid, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 根据用户及分类Id查询title
	 * @author Administrator
	 * @throws Exception
	 */
	public DBRow[] findProprietaryByAdidAndProductCatagoryId(long adid, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adid, request);
			return floorProprietaryMgrZyj.findProprietaryByAdminProductCatagory(adid, product_catagory_id, pc_id, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryByAdidAndProductCatagoryId(long adid, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	public DBRow[] getCustomerAndTitleByCategory(long adid, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adid, request);
			return floorProprietaryMgrZyj.getCustomerAndTitleByCategory(adid, product_catagory_id, pc_id, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryByAdidAndProductCatagoryId(long adid, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	
	/**
	 * 根据用户及分类Id查询title
	 * @author Administrator
	 * @throws Exception
	 */
	public DBRow[] findProprietaryByPcSelf(long adid, long pc_line_id, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adid, request);
//			//system.out.println("byProductCatagoryAdid:"+adid);
			return floorProprietaryMgrZyj.findProprietaryByPcSelf(adid, pc_line_id, product_catagory_id, pc_id, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryByPcSelf(long adid, long pc_line_id, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	
	/**
	 * 选择多个商品与title关系并保存
	 */
	@Override
	public long addProprietaryProductSeleteds(HttpServletRequest request) throws Exception
	{
		try
		{
			long user_adid	= StringUtil.getLong(request, "user_adid");
			long pc_id		= StringUtil.getLong(request, "pc_id");
			long pc_line_id	= StringUtil.getLong(request, "pc_line_id");
			long pc_catagory_id = StringUtil.getLong(request, "pc_catagory_id");
			int subFormType	= StringUtil.getInt(request, "subFormType");//根据提交的类型判断删除和添加的数据
			
			user_adid = this.findAdminIdByCondition(user_adid, request);
			
			long flag = this.addProprietaryProductSeleteds(user_adid, pc_id, pc_line_id, pc_catagory_id, subFormType, null, true, true, request);
			
//			//system.out.println("flag:"+flag);
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietaryProductSeleteds(HttpServletRequest request)",log);
		}
	}
	
	public long addProprietaryProductSeleteds(long user_adid, long pc_id, long pc_line_id, long pc_catagory_id, int subFormType,
			List<DBRow> willSaveRows, boolean isAdd, boolean isDel, HttpServletRequest request) throws Exception
	{
		try
		{
			long flag = 0;
			//已经保存了的关系
			DBRow[] hadSavedRow = this.getProprietaryProductsByType(subFormType, user_adid, pc_line_id, pc_catagory_id, pc_id, null, request);
				//floorProprietaryMgrZyj.findProprietaryByPcSelf(user_adid, pc_line_id, pc_catagory_id, pc_id, null);
			Set<String> hadSavedIds = new HashSet<String>();
			if(hadSavedRow.length > 0)
			{
				for (int i = 0; i < hadSavedRow.length; i++)
				{
					hadSavedIds.add(hadSavedRow[i].getString("title_id"));
//					//system.out.println("checkHad:"+pc_id+","+hadSavedRow[i].getString("title_id"));
				}
			}
			Set<String> willSaveIds = new HashSet<String>();
			//即将保存的关系
			if(null == willSaveRows)
			{
				
				String[] proprietary_product = this.getProprietaryProductByType(subFormType, request);
//				//system.out.println("subFormType:"+subFormType);
				
				if(null != proprietary_product && proprietary_product.length > 0)
				{
					for (int i = 0; i < proprietary_product.length; i++) 
					{
							willSaveIds.add(proprietary_product[i]);
//							//system.out.println("will:"+proprietary_product[i]);
					}
				}
			}
			else
			{
				for (int i = 0; i < willSaveRows.size(); i++) 
				{
					willSaveIds.add(willSaveRows.get(i).getString("titleId"));
//					//system.out.println("checkWill:"+pc_id+","+willSaveRows.get(i).getString("titleId"));
				}
			}
			
			flag = this.handAddOrRemoveProprietaryProduct(hadSavedIds, willSaveIds, pc_id, isAdd, isDel);
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietaryProductSeleteds(long user_adid, long pc_id, long pc_line_id, long pc_catagory_id, int subFormType,"+
			"List<DBRow> willSaveRows, boolean isAdd, boolean isDel, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 根据提交类型，从页面获取不同的商品title信息
	 * @param submitType
	 * @param request
	 * @return
	 */
	private String[] getProprietaryProductByType(int submitType, HttpServletRequest request)
	{
		String[] submitProprietaryProduct = new String[0];
		switch (submitType) {
		case 1:
			submitProprietaryProduct = request.getParameterValues("proprietary_product_pcLine");
			break;
		case 2:
			submitProprietaryProduct = request.getParameterValues("proprietary_product_cata");
			break;
		case 3:
			submitProprietaryProduct = request.getParameterValues("proprietary_admin");
			break;
		default:
			break;
		}
		return submitProprietaryProduct;
	}
	
	/**
	 * 根据提交类型，从数据库中获取不同的商品title信息
	 * @param subFormType
	 * @param user_adid
	 * @param pc_line_id
	 * @param pc_catagory_id
	 * @param pc_id
	 * @param pc
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private DBRow[] getProprietaryProductsByType(int subFormType, long user_adid, long pc_line_id, long pc_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		DBRow[] submitProprietaryProduct = new DBRow[0];
		try
		{
			switch (subFormType) {
			case 1:
				submitProprietaryProduct = floorProprietaryMgrZyj.findProprietaryByAdminProductLine(user_adid, pc_line_id, pc_id, pc);
				break;
			case 2:
				submitProprietaryProduct = floorProprietaryMgrZyj.findProprietaryByAdminProductCatagory(user_adid, pc_catagory_id, pc_id, pc);
				break;
			case 3:
				submitProprietaryProduct = floorProprietaryMgrZyj.findProprietaryByAdminProduct(user_adid, pc_id, 0, 0, pc, DataOrCountKey.DATA);
				break;
			default:
				break;
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProprietaryProductsByType(int submitType, long adid, long product_line_id, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request)",log);
		}
		return submitProprietaryProduct;
	}
	
	/**
	 * 比较已经提交的用户title和将要保存的用户title
	 * @param hadSavedIdsSet
	 * @param willSaveSet
	 * @param pc_id
	 * @param isAdd
	 * @param isDel
	 * @return
	 * @throws Exception
	 */
	private long handAddOrRemoveProprietaryProduct(Set<String> hadSavedIdsSet, Set<String> willSaveSet, long pc_id, boolean isAdd, boolean isDel) throws Exception
	{
		try
		{
			long flag = 0L;
			Set<String> adds		= new HashSet<String>();//添加的关系
			List<String> removes	= new ArrayList<String>();//删除的关系
			Set<String> sames		= new HashSet<String>();
			
			//得到相同的和将要被删除的
			for (Iterator iterator = hadSavedIdsSet.iterator(); iterator.hasNext();) 
			{
				String hadSavedId	= (String)iterator.next();
				//将要保存的ids中不包含已经保存的，删除
				if(!willSaveSet.contains(hadSavedId))
				{
					removes.add(hadSavedId);
				}
				else
				{
					sames.add(hadSavedId);
				}
			}
			//得到将要添加的
			for (Iterator iterator = willSaveSet.iterator(); iterator.hasNext();) 
			{
				String titleId = (String)iterator.next();
				//已经保存的不包含将要保存的，添加
				if(!sames.contains(titleId))
				{
					adds.add(titleId);
				}
			}
			if(0 == flag)
			{
				
				//删除
				if(isDel)
				{
					for (int i = 0; i < removes.size(); i++) 
					{
//						//system.out.println("removes:"+removes.get(i));
						floorProprietaryMgrZyj.deleteProprietaryProductById(0, pc_id, Long.parseLong(removes.get(i)));
						this.editTitleIndex(Long.parseLong(removes.get(i)), "update");
					}
				}
				//添加
				if(isAdd)
				{
					for (Iterator iterator = adds.iterator(); iterator.hasNext();) 
					{
						String titleId = (String)iterator.next();;
						DBRow propPorductRow = new DBRow();
						propPorductRow.add("tp_pc_id", pc_id);
						propPorductRow.add("tp_title_id", titleId);
//						//system.out.println("add:"+pc_id+","+titleId);
						floorProprietaryMgrZyj.addProprietaryProduct(propPorductRow);
						this.editTitleIndex(Long.parseLong(titleId), "update");
					}
				}
				
			}
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"handAddOrRemoveProprietaryProduct(Set<String> hadSavedIdsSet, Set<String> willSaveSet, long pc_id, boolean isAdd, boolean isDel)",log);
		}
	}
	
	/**
	 * 删除商品与title关系
	 * @param request
	 * @throws Exception
	 */
	public void deleteProprietaryProduct(HttpServletRequest request) throws Exception
	{
		try
		{
			long tp_id			= StringUtil.getLong(request, "tp_id");
			long tp_pc_id		= StringUtil.getLong(request, "tp_pc_id");
			long tp_title_id	= StringUtil.getLong(request, "tp_title_id");
			this.deleteProprietaryProductById(tp_id, tp_pc_id, tp_title_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"deleteProprietaryProduct(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 通过商品删除商品与title的关联
	 * @param tp_id
	 * @param tp_pc_id
	 * @param tp_title_id
	 * @throws Exception
	 */
	public void deleteProprietaryProductById(long tp_id, long tp_pc_id, long tp_title_id) throws Exception
	{
		try
		{
			floorProprietaryMgrZyj.deleteProprietaryProductById(tp_id, tp_pc_id, tp_title_id);
			this.editTitleIndex(tp_title_id, "update");
		}
		catch (Exception e)
		{
			throw new SystemException(e,"deleteProprietaryProductById(long tp_id, long tp_pc_id, long tp_title_id)",log);
		}
	}
	/**
	 *  通过商品ID，titleid查出产品线中是否存在title
	 */
	public DBRow[] findProductTitleParentNode(long pc_id,String titleId) throws Exception 
	{
		try
		{
			return null;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"findProductTitleParentNode(long pc_id,String titleId)",log);
		}
	}
	
	/**
	 *  商品已有的Title关系
	 */
	public DBRow[] getProductHasTitleList(long pc_id) throws Exception{
		try{
			return floorProprietaryMgrZyj.getProudctHasTitleList(pc_id);
			//return floorUserTitleMgr.getAdminHasTitleList(pcid);
			
		}catch (Exception e){
			throw new SystemException(e,"",log);
		}
	}
	/**
	 *  商品没有的Title关系
	 */
	public DBRow[] getProudctUnhasTitleList(long pc_id) throws Exception{
		try{
			return floorProprietaryMgrZyj.getProudctUnhasTitleList(pc_id);
			
		}catch (Exception e){
			throw new SystemException(e,"",log);
		}
	}
	
	/**********************************************************************************************/
	
	/**
	 * 基础数据>>商品与title的关系
	 * 
	 * @author subin
	 */
	public DBRow addProudctTitle(HttpServletRequest request) throws Exception{
		
		try{
			
			long pc_id = StringUtil.getLong(request,"pc_id");
			String titleFlag = StringUtil.getString(request,"flag");
			String[] titleId = StringUtil.getString(request,"title_id").split(",");
			
			//更新了商品与title的关系
			DBRow flag = managerProudctTitle(pc_id,titleFlag,titleId);
			
			
			//修改商品title后影响分类与title的关系和产品线与title的关系
			//修改商品title后影响分类与 品牌商/title 的关系和产品线与 品牌商/title的关系
			this.updateCustomerTitleAndCategoryLineRelation(pc_id);
			
			
			DBRow[] containerProduct=this.floorClpTypeMgrZr.findProductNode(pc_id);
			DBRow containerProductLine=this.floorClpTypeMgrZr.findProductLineNode(pc_id);
			DBRow[] hasTitle = this.getProductHasTitleList(pc_id);
			String title_id = "";
			
			for(int j = 0; j<hasTitle.length; j++){
				
				title_id += ","+hasTitle[j].get("title_id",0l);
			}
			
			for(int i = 0; i < containerProduct.length; i++){
				Map<String,Object> productNode = new HashMap<String, Object>();

				productNode.put("pc_id",pc_id);
				productNode.put("title_id",title_id.equals("")?title_id:title_id.substring(1));				
				productNode.put("product_line",containerProductLine.get("product_line_id",0l));
				productNode.put("catalog_id",containerProduct[i].get("catalog_id",0l));
				productNode.put("p_name",containerProduct[i].getString("p_name"));
				productNode.put("union_flag",containerProduct[i].get("union_flag",0));
				
				
				Map<String,Object> searchFor = new HashMap<String, Object>();
				searchFor.put("pc_id", pc_id);
				
				//productStoreMgr.addNode(1,NodeType.Product,productNode);//添加商品节点
				productStoreMgr.updateNode(1,NodeType.Product,searchFor,productNode);//添加商品节点
				//productStoreMgr.clearGraphDB(1, false);
			}
			
			return flag;
			
		}catch (Exception e){
			
			throw new SystemException(e,"addProudctTitle(HttpServletRequest request)",log);
		}
	}
	
	/**********************************************************************************************/
	
	public DBRow[] compareCusomerTitle(DBRow[] row1, DBRow[] row2) throws Exception{
		
		List<DBRow> result = new ArrayList<DBRow>();
		
		for(DBRow one : row1){
			
			boolean find = false;
			
			for(DBRow two : row2){
				
				if(one.get("customer",0) == two.get("customer",-1) && one.get("title",0) == two.get("title",-1)){
					
					find = true;
				}
			}
			
			if(!find){
				
				DBRow delRow = new DBRow();
				delRow.put("customer", one.get("customer",0));
				delRow.put("title", one.get("title",0));
				delRow.put("flag", "delete");
				
				result.add(delRow);
			}
		}
		
		for(DBRow one : row2){
			
			boolean find = false;
			
			for(DBRow two : row1){
				
				if(one.get("customer",0) == two.get("customer",-1) && one.get("title",0) == two.get("title",-1)){
					
					find = true;
				}
			}
			
			if(!find){
				
				DBRow delRow = new DBRow();
				delRow.put("customer", one.get("customer",0));
				delRow.put("title", one.get("title",0));
				delRow.put("flag", "insert");
				
				result.add(delRow);
			}
		}
		
		return result.toArray(new DBRow[result.toArray().length]);
		
		//String[] y = x.toArray(new String[0]);
	}

	/**
	 * 修改了商品的 品牌商/title 后 
	 * 
	 * 1.原 品牌商/title 与分类父分类的关系[是否需要删除]
	 * 2.现 品牌商/title 与分类父分类的关系[是否需要添加]
	 * 3.原品牌商/title 与产品线的关系[是否需要删除]
	 * 4.现 品牌商/title 与产品线的关系[是否需要添加]
	 * 
	 * @param Long[商品ID]
	 * @author subin
	*/
	public void updateCustomerTitleAndCategoryLineRelation(long pcId) throws Exception{
		
		DBRow product = floorProprietaryMgrZyj.getProductDetailByPcId(pcId);
		
		//1.2.品牌商/title 与分类父分类的关系[是否需要删除/是否需要添加]
		this.customerTitleAndCategory(product.get("catalog_id", 0L));
		
		DBRow category = floorCatalogMgr.getProductCatalogById(product.get("catalog_id", 0L));
		
		if(category != null){
			
			//3.4.原title与产品线的关系[是否需要删除]
			this.customerTitleAndLine(category.get("product_line_id",0L));
		}
	}
	
	//1.2.品牌商/title 与分类父分类的关系[是否需要删除/是否需要添加]
	public void customerTitleAndCategory(long categoryId) throws Exception{
		
		//如果存在分类
		if(categoryId != 0){
			
			//查询分类与customer/title的关系
			DBRow[] title1 = floorCatalogMgr.getCustomerTitleByCategory(categoryId);
			
			//查询分类下分类和商品与customer/title的关系
			DBRow[] title2 = floorCatalogMgr.getCustomerTitleLowerByCategory(categoryId);
			
			//比较分类与分类下商品和分类的customer/title
			DBRow[] comRes = this.compareCusomerTitle(title1, title2);
			
			for(DBRow one : comRes){
				
				if(one.get("flag", "").equals("insert")){
					
					DBRow param = new DBRow();
					param.put("tpc_title_id", one.get("title",0));
					param.put("tpc_product_catalog_id", categoryId);
					param.put("tpc_customer_id", one.get("customer",0));
					
					floorCatalogMgr.insertTitleProductCatalog(param);
					
				} else {
					
					floorCatalogMgr.deleteTitleProductCatalog(categoryId,one.get("title",0),one.get("customer",0));
				}
			}
			
			//判断是否原父分类依然有父分类
			DBRow parentCategory = floorCatalogMgr.getParentCategory(categoryId);
			
			if(parentCategory != null){
				
				this.customerTitleAndCategory(parentCategory.get("id",0L));
			}
		}
	}
	
	//3.4.品牌商/title 与产品线的关系[是否需要删除][是否需要添加]
	public void customerTitleAndLine(long lineId) throws Exception{
		
		//如果存在产品线
		if(lineId != 0){
			
			//查询产品线与customer/title的关系
			DBRow[] title1 = floorCatalogMgr.getCustomerTitleByLine(lineId);
			//查询产品线下分类与customer/title的关系
			DBRow[] title2 = floorCatalogMgr.getCustomerTitleLowerByLine(lineId);
			
			//比较分类与分类下商品和分类的customer/title
			DBRow[] comRes = this.compareCusomerTitle(title1, title2);
			
			for(DBRow one : comRes){
				
				if(one.get("flag", "").equals("insert")){
					
					DBRow param = new DBRow();
					param.put("tpl_title_id", one.get("title",0));
					param.put("tpl_product_line_id", lineId);
					param.put("tpl_customer_id", one.get("customer",0));
					
					floorCatalogMgr.insertTitleProductLine(param);
					
				}else{
					
					floorCatalogMgr.deleteTitleProductLine(lineId,one.get("title",0L));
				}
			}
		}
	}
	
	/**********************************************************************************************/
	
	/**
	 * 修改商品title后影响分类与title的关系和产品线与title的关系
	 * 
	 * 1.原title与分类以及父分类的关系[是否需要删除]
	 * 2.现title与分类以及父分类的关系[是否需要添加]
	 * 3.原title与产品线的关系[是否需要删除]
	 * 4.现title与产品线的关系[是否需要添加]
	 * 
	 * @param Long[商品ID]
	 * @author subin
	*/
	@SuppressWarnings("unused")
	private void updateCategoryAndLineForTitleRelation(long pcId) throws Exception{
		
		DBRow product = floorProprietaryMgrZyj.getProductDetailByPcId(pcId);
		
		//1.原title与分类以及父分类的关系[是否需要删除]
		this.beforeParentCategoryAndTitleRelation(product.get("catalog_id", 0L));
		
		//2.现title与分类以及父分类的关系[是否需要添加]
		DBRow[] titles = floorProprietaryMgrZyj.getProudctHasTitleList(pcId);
		
		this.nowParentCategoryAndTitleRelation(product.get("catalog_id", 0L),titles);
		
		DBRow category = floorCatalogMgr.getProductCatalogById(product.get("catalog_id", 0L));
		
		if(category != null){
			
			//3.原title与产品线的关系[是否需要删除]
			this.beforeLineAndTitleRelation(category.get("product_line_id",0L));
			
			//4.现title与产品线的关系[是否需要添加]
			this.nowLineAndTitleRelation(category.get("product_line_id",0L), titles);
		}
	}
	
	//4.现title与产品线的关系[是否需要添加]
	private void nowLineAndTitleRelation(long lineId,DBRow[] titles) throws Exception{
		
		if(lineId != 0){
			
			for(DBRow one:titles){
				
				DBRow titlePro = floorCatalogMgr.getTitleProductLine(lineId,one.get("title_id",0L));
				
				if( titlePro == null ){
					
					DBRow row = new DBRow();
					row.put("tpl_title_id", one.get("title_id",0L));
					row.put("tpl_product_line_id", lineId);
					//添加
					floorCatalogMgr.insertTitleProductLine(row);
				}
			}
		}
	}

	//3.原title与产品线的关系[是否需要删除]
	private void beforeLineAndTitleRelation(long lineId) throws Exception{
		
		if(lineId != 0){
			
			//查询产品线下产品分类与title的关系
			DBRow[] title1 = floorCatalogMgr.getTitleForLineCategoryRelation(lineId);
			//查询产品线与title的关系
			DBRow[] title2 = floorCatalogMgr.getTitleForLineByLineId(lineId);
			//对比title
			for(DBRow one:title1){
				
				for(DBRow two:title2){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						two.put("exist",1);
						break;
					}
				}
			}
			//删除不存在的title
			for(DBRow one:title2){
				
				if(one.get("exist",0)==0){
					
					floorCatalogMgr.deleteTitleProductLine(lineId,one.get("title",0L));
				}
			}
		}
	}
	
	//2.现title与分类以及父分类的关系[是否需要添加]
	private void nowParentCategoryAndTitleRelation(long categoryId,DBRow[] titles) throws Exception{
		
		//如果存在分类
		if(categoryId != 0 && titles.length > 0){
			
			for(DBRow one:titles){
				
				DBRow titlePro = floorCatalogMgr.getTitleProductCatalog(categoryId,one.get("title_id",0L));
				
				if( titlePro == null ){
					
					DBRow row = new DBRow();
					row.put("tpc_title_id", one.get("title_id",0L));
					row.put("tpc_product_catalog_id",categoryId);
					//添加
					floorCatalogMgr.insertTitleProductCatalog(row);
				}
			}
			
			//判断是否原父分类依然有父分类
			DBRow parentCategory = floorCatalogMgr.getParentCategory(categoryId);
			
			if(parentCategory != null){
				
				this.nowParentCategoryAndTitleRelation(parentCategory.get("id",0L),titles);
			}
		}
	}
	
	//1.原title与分类以及父分类的关系[是否需要删除]
	private void beforeParentCategoryAndTitleRelation(long categoryId)throws Exception{
		
		//如果存在分类
		if(categoryId != 0){
			
			//查询此分类关联的title
			DBRow[] title1 = floorCatalogMgr.getTitleForCategory(categoryId);
			
			//查询原父分类下的分类关联的title//查询原父分类下商品关联的title
			DBRow[] title4 = floorCatalogMgr.getTitleForCategoryProductAndSubCatagory(categoryId);
			
			//对比title
			for(DBRow one:title4){
				
				for(DBRow two:title1){
					
					if(one.get("title",-1L) == two.get("title",-2L)){
						
						two.put("exist",1);
						break;
					}
				}
			}
			
			//删除不存在的title
			for(DBRow one:title1){
				
				if(one.get("exist",0)==0){
					
					floorCatalogMgr.deleteTitleProductCatalog(categoryId,one.get("title",-1L));
				}
			}
			
			//判断是否原父分类依然有父分类
			DBRow parentCategory = floorCatalogMgr.getParentCategory(categoryId);
			
			if(parentCategory != null){
				
				this.beforeParentCategoryAndTitleRelation(parentCategory.get("id",0L));
			}
		}
	}
	
	/**********************************************************************************************/
	
	private DBRow managerProudctTitle(long pc_id,String titleFlag,String[] titleId) throws Exception{
		
		try{
			
			int flag = 0;
			DBRow result = new DBRow();
			List<DBRow> rowList = new ArrayList<DBRow>();
			
			if(titleFlag.equals("add")){
		
				//删除TITLE
				for(int i=0;i<titleId.length;i++){
				
					floorProprietaryMgrZyj.deleteTitleProductByPcidAndTitleId(pc_id,titleId[i]);
				}
				
				result.add("flag", flag);
				result.add("result_infos", rowList.toArray(new DBRow[0]));
			
			}else{
				
				//添加TITLE
				for(int i=0;i<titleId.length;i++){
					addProprietaryProduct(pc_id, Long.parseLong(titleId[i]));
				}
			}
			
			return result;
		}
		catch (Exception e){
			throw new SystemException(e,"",log);
		}
	}
	
	/**
	 * 添加title及商品关系
	 */
	public long addProprietaryProduct(HttpServletRequest request) throws Exception
	{
		try
		{
			long adid			= this.findAdminIdByCondition(0, request);
			long flag			= 0;
			String title_name	= StringUtil.getString(request, "title_name");
			long pc_id			= StringUtil.getLong(request, "pc_id");
			int title_priority	= StringUtil.getInt(request, "title_priority");
			long title_id		= 0;
			
			//验证次titleName是否已经添加
			flag = this.addProprietaryCheck(title_name);
			if(flag  >= 0)
			{
				//验证此优先级的title是否已经添加
				flag = this.addProprietaryAdminCheck(adid, flag, title_priority);
			}
			if(flag >= 0)
			{
				DBRow row = new DBRow();
				row.add("title_name", title_name);
				title_id = floorProprietaryMgrZyj.addProprietary(row);
				this.editTitleIndex(title_id, "add");
				
				DBRow propAdidRow = new DBRow();
				propAdidRow.add("title_admin_adid", adid);
				propAdidRow.add("title_admin_title_id", title_id);
				propAdidRow.add("title_admin_sort", title_priority);
				floorProprietaryMgrZyj.addProprietaryAdmin(propAdidRow);
				
				//保存title与商品的关系
				if(pc_id > 0)
				{
					DBRow propPorductRow = new DBRow();
					propPorductRow.add("tp_pc_id", pc_id);
					propPorductRow.add("tp_title_id", title_id);
					floorProprietaryMgrZyj.addProprietaryProduct(propPorductRow);
				}
				this.editTitleIndex(title_id, "update");
			}
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietaryProduct(HttpServletRequest request)",log);
		}
	}
	
	
	/**
	 * -1:title已经存在
	 * @param title_name
	 * @throws Exception
	 */
	public long addProprietaryCheck(String title_name) throws Exception
	{
		try
		{
			long flag = 0;
			//验证此title_name是否已经添加
			if(null != title_name && !"".equals(title_name))
			{
				int propCount		= floorProprietaryMgrZyj.findProprietaryCountByTitleName(title_name);
				if(propCount > 0)
				{
					flag = -1;
					return flag;
				}
			}
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietaryCheck(String title_name)",log);
		}
	}
	
	public long addProprietaryAdminCheck(long user_adid, long title_id, int title_priority) throws Exception
	{
		try
		{
			long flag = 0;
			//验证此优先级的title是否已经添加
			if(user_adid > 0)
			{
				if(title_id > 0)
				{
					int propAdidCount = floorProprietaryMgrZyj.findProprietaryAdminByAdidAndPriority(user_adid, title_id, 0);
					if(propAdidCount > 0)
					{
						flag = -2;
					}
				}
				if(flag >= 0)
				{
					int propPrioAdidCount = floorProprietaryMgrZyj.findProprietaryAdminByAdidAndPriority(user_adid, 0, title_priority);
					if(propPrioAdidCount > 0)
					{
						flag = -3;
					}
				}
			}
			return flag;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietaryAdminCheck(long user_adid, long title_id, int title_priority)",log);
		}
	}
	
	/**
	 * 通过titleID获得用户
	 */
	public DBRow[] findAdminsByTitleId(boolean adidNullToLogin, long adid, long title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findAdminsByTitleId(adid, title_id, begin, length, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findAdminsByTitleId(boolean adidNullToLogin, long adid, long title_id, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 通过titleID查询与title关联的产品线
	 * @param adidNullToLogin=true 如果adid=0，是否将当前登录者的ID赋给adid
	 * @param pc_line_id
	 * @param title_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductLinesByTitleId(boolean adidNullToLogin, long adid, String pc_line_ids, String title_ids, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProductLinesByTitleId(adid, this.parseStrToLongArr(pc_line_ids), this.parseStrToLongArr(title_ids), begin, length, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProductLinesByTitleId(boolean adidNullToLogin, long adid, String pc_line_ids, String title_ids, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 通过titleID查询与title关联的产品分类
	 */
	public DBRow[] findProductCatagorysByTitleId(boolean adidNullToLogin, long adid, String pc_line_id, String category_parent_id, String pc_cata_id, String title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProductCatagorysByTitleId(adid, this.parseStrToLongArr(pc_line_id), this.parseStrToLongArr(category_parent_id), this.parseStrToLongArr(pc_cata_id), this.parseStrToLongArr(title_id), begin, length, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProductCatagorysByTitleId(boolean adidNullToLogin, long adid, long pc_line_id, long category_parent_id, long pc_cata_id, long title_id, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 通过titleID查询与title关联的产品分类
	 */
	public DBRow[] findProductCatagoryParentsByTitleId(boolean adidNullToLogin, long adid, String pc_line_id, String category_parent_id, String pc_cata_id, String title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProductCatagoryParentsByTitleId(adid, this.parseStrToLongArr(pc_line_id), this.parseStrToLongArrOriginal(category_parent_id), this.parseStrToLongArr(pc_cata_id), this.parseStrToLongArr(title_id), begin, length, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProductCatagoryParentsByTitleId(boolean adidNullToLogin, long adid, long pc_line_id, long category_parent_id, long pc_cata_id, long title_id, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 通过titleID查询与title关联的产品
	 * @param pc_id
	 * @param title_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductsByTitleId(boolean adidNullToLogin, long adid, long pc_id, String pc_name, long title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProductsByTitleId(adid, pc_id, pc_name, title_id, begin, length, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProductsByTitleId(boolean adidNullToLogin, long adid, long pc_id, String pc_name, long title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) ",log);
		}
	}
	
	/**
	 * 根据用户id,产品线ID,分类ID,商品名查询title
	 */
	public DBRow[] findProprietaryAllRelatesByAdminProductNameLineCatagory(boolean adidNullToLogin, long adid, String product_line_ids, String category_parent_ids, String product_catagory_ids,
			String pc_name, int begin, int length, PageCtrl pc, HttpServletRequest request)throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			DBRow[] rows = this.findProprietaryAllByAdminProductNameLineCatagory(adidNullToLogin, adid, product_line_ids, category_parent_ids, pc_name, pc, request);
			for (int i = 0; i < rows.length; i++)
			{
				DBRow[] adminRows = this.findAdminsByTitleId(adidNullToLogin, adid, rows[i].get("title_id", 0), begin, length, null, request);
				rows[i].add("admins", adminRows);
				DBRow[] pcLineRows = this.findProductLinesByTitleId(adidNullToLogin, adid, product_line_ids, rows[i].getString("title_id"), begin, length, null, request);
				rows[i].add("pcLines", pcLineRows);
				DBRow[] pcCataRows = this.findProductCatagorysByTitleId(adidNullToLogin, adid, product_line_ids,category_parent_ids, "", rows[i].getString("title_id"), begin, length, null, request);
				rows[i].add("pcCatagorys", pcCataRows);
				DBRow[] productRows = this.findProductsByTitleId(adidNullToLogin, adid, 0L,null, rows[i].get("title_id", 0), begin, length, null, request);
				rows[i].add("products", productRows);
			}
			return rows;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryAllRelatesByAdminProductNameLineCatagory(boolean adidNullToLogin, long adid, long product_line_id, long category_parent_id, long product_catagory_id,"
			+"String pc_name, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	public DBRow[] findProprietaryAllByAdminProductNameLineCatagory(boolean adidNullToLogin, long adid, String product_line_ids, String product_catagory_ids, String pc_name, PageCtrl pc, HttpServletRequest request)throws Exception
	{
		try
		{
			//如果title列表页面只需要列出当前用户的title，下行无需注释
			//adid = this.findAdidByLogin(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProprietaryAllByAdminProductNameLineCatagory(adid, this.parseStrToLongArr(product_line_ids), this.parseStrToLongArr(product_catagory_ids), pc_name, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryAllByAdminProductNameLineCatagory(boolean adidNullToLogin, long adid, long product_line_id, long product_catagory_id, String pc_name, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 过滤商品
	 */
	public DBRow[] filterProductByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status, String title_id, long adid, int equal_or_not, long product_id, int active, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.filterProductByPcLineCategoryTitleId(this.parseStrToLongArr(catalog_id), this.parseStrToLongArr(product_line_id), union_flag, product_file_type, product_upload_status, this.parseStrToLongArr(title_id), adid, equal_or_not, product_id,active, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"filterProductByPcLineCategoryTitleId(boolean adidNullToLogin, long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 过滤商品添加active维度
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015-4-22
	 */
	public DBRow[] filterProductByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status, String title_id, long adid, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request, long active) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(request.getSession(true));
			
			return floorProprietaryMgrZyj.filterProductByPcLineCategoryTitleId(this.parseStrToLongArr(catalog_id), this.parseStrToLongArr(product_line_id), union_flag, product_file_type, product_upload_status, this.parseStrToLongArr(title_id), adid, equal_or_not, product_id, pc, active, adminLoggerBean);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"filterProductByPcLineCategoryTitleId(boolean adidNullToLogin, long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, PageCtrl pc, HttpServletRequest request, long active)",log);
		}
	}
	
	/**
	 * 过滤商品（不包含套装）
	 */
	public DBRow[] filterChoiceProductByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status, String title_id, long adid, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
//			return floorProprietaryMgrZyj.filterChoiceProductByPcLineCategoryTitleId(this.parseStrToLongArr(catalog_id), this.parseStrToLongArr(product_line_id), union_flag, product_file_type, product_upload_status, this.parseStrToLongArr(title_id), adid, pc);
			return floorProprietaryMgrZyj.filterProductByPcLineCategoryTitleId(this.parseStrToLongArr(catalog_id), this.parseStrToLongArr(product_line_id), union_flag, product_file_type, product_upload_status, this.parseStrToLongArr(title_id), adid,equal_or_not, product_id, 0, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"filterChoiceProductByPcLineCategoryTitleId(boolean adidNullToLogin, long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	public DBRow[] findDetailProductLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int union_flag, int equal_or_not, long product_id, int active,PageCtrl pc, HttpServletRequest request)throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findDetailProductLikeSearch(key, this.parseStrToLongArr(title_id), adid,union_flag, equal_or_not, product_id,active, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findDetailProductLikeSearch(boolean adidNullToLogin, String key,long title_id, long adid,PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 搜索商品明细添加active维度
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015-4-22
	 */
	public DBRow[] findDetailProductLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int union_flag, int equal_or_not, long product_id,PageCtrl pc, HttpServletRequest request, long active)throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(request.getSession(true));
			
			return floorProprietaryMgrZyj.findDetailProductLikeSearch(key, this.parseStrToLongArr(title_id), adid,union_flag, equal_or_not, product_id, pc, active, adminLoggerBean);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findDetailProductLikeSearch(boolean adidNullToLogin, String key,long title_id, long adid,PageCtrl pc, HttpServletRequest request, long active)",log);
		}
	}
	
	/*
	 * 
	 * 商品综合收索（不包含套装）
	 */
	public DBRow[] findChoiceDetailProductLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int union_flag, int equal_or_not, long product_id,PageCtrl pc,int active, HttpServletRequest request)throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			//return floorProprietaryMgrZyj.findChoiceDetailProductLikeSearch(key, this.parseStrToLongArr(title_id), adid, pc);
			return floorProprietaryMgrZyj.findDetailProductLikeSearch(key, this.parseStrToLongArr(title_id), adid, union_flag, equal_or_not, product_id, active, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findChoiceDetailProductLikeSearch(boolean adidNullToLogin, String key,long title_id, long adid,PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	
	public DBRow[] findProductInfosProductLineProductCodeByLineId(boolean adidNullToLogin, String pc_line_id, String title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProductInfosProductLineProductCodeByLineId(this.parseStrToLongArr(pc_line_id), this.parseStrToLongArr(title_id), adid, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProductInfosProductLineProductCodeByLineId(boolean adidNullToLogin, long pc_line_id, long title_id, long adid, PageCtrl pc, HttpServletRequest request) ",log);
		}
	}
	
	public DBRow[] findAllProductsByTitleAdid(boolean adidNullToLogin, String title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findAllProductsByTitleAdid(this.parseStrToLongArr(title_id), adid, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findAllProductsByTitleAdid(boolean adidNullToLogin, long title_id, long adid, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 查找商品时添加active维度
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015-4-22
	 */
	public DBRow[] findAllProductsByTitleAdid(boolean adidNullToLogin, String title_id, long adid, PageCtrl pc, HttpServletRequest request, long active) throws Exception {
		
		try {
			AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(request.getSession(true));
			
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			
			return floorProprietaryMgrZyj.findAllProductsByTitleAdid(this.parseStrToLongArr(title_id), adid, pc, active, adminLoggerBean);
			
		} catch (Exception e) {
			throw new SystemException(e,"findAllProductsByTitleAdid(boolean adidNullToLogin, long title_id, long adid, PageCtrl pc, HttpServletRequest request, long active)",log);
		}
	}
	
	/*
	 * 过滤套装商品
	 */
	public DBRow[] findCohiceProductsByTitleAdid(boolean adidNullToLogin, String title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
//			return floorProprietaryMgrZyj.findChoiceProductsByTitleAdid(this.parseStrToLongArr(title_id), adid, pc);
			return floorProprietaryMgrZyj.findAllProductsByTitleAdid(this.parseStrToLongArr(title_id), adid, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findAllProductsByTitleAdid(boolean adidNullToLogin, long title_id, long adid, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	
	
	/**
	 * 导入商品title
	 */
	public HashMap<String, DBRow[]> uploadAdminTitleExcelShow(String filename, String type)throws Exception {
		
		try{
			String path = "";
			InputStream is;
			Workbook wrb;
			//返回值
			HashMap<String,DBRow[]> resultMap = new HashMap<String, DBRow[]>();
			if ("show".equals(type) && null != filename && !"".equals(filename)){
				
				path = Environment.getHome() + "upl_imags_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				
				DBRow[] adminTitleRows = adminTitlesExcelToRow(wrb);
				if(adminTitleRows.length > 0){
					//检查adminTitles
					DBRow[] errorAdminTitles = checkAdminTitles(adminTitleRows);
					resultMap.put("allExcelAdminTitles", adminTitleRows);
					resultMap.put("errorAdminTitles",errorAdminTitles);
				}
				
			}else if("data".equals(type) && null != filename && !"".equals(filename)){
				
				path = Environment.getHome() + "upl_imags_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				DBRow[] adminTitleRows = adminTitlesExcelToRow(wrb);
				DBRow[] errorAdminTitles = new DBRow[0];
				
				if(adminTitleRows.length > 0){
					//检查adminTitles
					errorAdminTitles = checkAdminTitles(adminTitleRows);
					resultMap.put("allExcelAdminTitles", adminTitleRows);
					resultMap.put("errorAdminTitles",errorAdminTitles);
				}
			}
			return resultMap;
		}catch (Exception e){
			throw new SystemException(e,"uploadAdminTitleExcelShow(String filename, String type)",log);
		}
	}
	
	/**
	 * 转化成DBRow类型
	 * @param wrb
	 * @return
	 * @throws Exception
	 */
	private DBRow[] adminTitlesExcelToRow(Workbook wrb) throws Exception{
		
		try {
			ArrayList<DBRow> adminUserTitleRows = new ArrayList<DBRow>();
			Sheet rs = wrb.getSheet(0);
			//excel表记录行数
			int rsRows = rs.getRows();    
			for(int i=1;i<rsRows;i++){
				
				String adminUserName = rs.getCell(0, i).getContents().trim();
				String adminUserTitle = rs.getCell(1, i).getContents().trim();
				//String adminTitleSort = rs.getCell(2, i).getContents().trim();
				//if(!"".equals(adminUserName) || !"".equals(adminUserTitle) || !"".equals(adminTitleSort))
				if(!"".equals(adminUserName) || !"".equals(adminUserTitle)){
					DBRow adminUserTitleRow = new DBRow();
					adminUserTitleRow.add("adminUserName", adminUserName);
					adminUserTitleRow.add("adminUserTitle", adminUserTitle);
					//adminUserTitleRow.add("adminTitleSort", adminTitleSort);
					adminUserTitleRow.add("adminTitleRowNumber", i);
					adminUserTitleRows.add(adminUserTitleRow);
				}
			}
			return (adminUserTitleRows.toArray(new DBRow[0]));
			  
		} catch (Exception e) {
			throw new SystemException(e,"adminTitlesExcelToRow(Workbook wrb) ",log);
		}
	}
	
	
	/**
	 * 检查地址信息
	 * @param adminTitles
	 * @return
	 * @throws Exception
	 */
	private DBRow[] checkAdminTitles(DBRow[] adminTitles) throws Exception {
		try{
			ImportTitleRelatedExcelErrorKey impTitRelErr = new ImportTitleRelatedExcelErrorKey();
			ArrayList<DBRow> errorAdminTitles = new ArrayList<DBRow>();
			//用户ID,《titleId, titleSort》
			HashMap<String, List<DBRow>> adminTitleMap = new HashMap<String, List<DBRow>>();
			HashMap<String, String> adminTitleNotRepeat = new HashMap<String, String>();
			List<DBRow> needCheckSorts = new ArrayList<DBRow>();
			
			for (int i = 0; i < adminTitles.length; i++){
				
				DBRow adminTitleAndError = adminTitles[i];
				//判断用户与TITLE是否存在
				adminTitleAndError = this.checkAdminTitleNullOrExist(adminTitleAndError);
				
				String adminUserId = adminTitleAndError.getString("adminUserId");
				String titleId = adminTitleAndError.getString("titleId");
				//String titleSort = adminTitleAndError.getString("adminTitleSort");
				int adminTitleRowNumber	 = adminTitleAndError.get("adminTitleRowNumber", 0);
				
				//提交的数据是否为空或者存在的检查
				if (!"".equals(adminTitleAndError.getString("errorMessage"))){
					errorAdminTitles.add(adminTitleAndError);
				}else{
					//提交的数据是否重复
					if(adminTitleNotRepeat.containsKey(adminUserId+"_"+titleId)){
						
						String errorMsg = adminTitleAndError.getString("errorMessage");
						if(!"".equals(errorMsg)){
							errorMsg += ";";
						}
						errorMsg += impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.ADMIN_TITLE_REPEAT)
									+ ",与excel表中的第"+adminTitleNotRepeat.get(adminUserId+"_"+titleId).split("_")[1]+"行重复;";
						adminTitleAndError.add("errorMessage", errorMsg);
					}else{
						adminTitleNotRepeat.put(adminUserId+"_"+titleId, ""+"_"+adminTitleRowNumber);
					}
					if (!"".equals(adminTitleAndError.getString("errorMessage"))){
						errorAdminTitles.add(adminTitleAndError);
					}
				}
				if("".equals(adminTitleAndError.getString("errorMessage"))){
					needCheckSorts.add(adminTitleAndError);
				}
			}
			/*//上传数据本身没有问题，检查title的sort是否重复
			if(needCheckSorts.size() > 0){
				for (int i = 0; i < needCheckSorts.size(); i++){
					DBRow adminTitleRow = needCheckSorts.get(i);
					long adminUserId		 = adminTitleRow.get("adminUserId", 0L);
					
					List<DBRow> titleAndSortList = new ArrayList<DBRow>();
					if(adminTitleMap.containsKey(adminUserId+""))
					{
						titleAndSortList = adminTitleMap.get(String.valueOf(adminUserId)); 
					}
					titleAndSortList.add(adminTitleRow);
					adminTitleMap.put(String.valueOf(adminUserId), titleAndSortList);
				}
				Set<Entry<String, List<DBRow>>> adminTitleSet = adminTitleMap.entrySet();
				for (Iterator iterator = adminTitleSet.iterator(); iterator.hasNext();) 
				{
					long flag = 0;
					Entry<String, List<DBRow>> entry = (Entry<String, List<DBRow>>) iterator.next();
					long adid = Long.parseLong(entry.getKey());
					List<DBRow> titleAndSort = entry.getValue();
					flag = this.addProprietaryAdmins(adid, null, titleAndSort, false, false, false);
					if(flag < 0)
					{
						for (int i = 0; i < titleAndSort.size(); i++) 
						{
							titleAndSort.get(i).add("errorMessage", impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.TITLE_SORT_REPEAT));
							errorAdminTitles.add(titleAndSort.get(i));
						}
					}
				}
			}*/
			/*//system.out.println("errorRows");
			for (int i = 0; i < errorAdminTitles.size(); i++) 
			{
				DBRow row = errorAdminTitles.get(i);
				//system.out.println(row.getString("adminUserName")+","+row.getString("adminUserTitle")+","+row.getString("adminTitleSort")+","+row.getString("adminTitleRowNumber"));
			}*/
			return (errorAdminTitles.toArray(new DBRow[0]));
		}catch (Exception e){
			throw new SystemException(e,"checkAdminTitles(DBRow[] adminTitles)",log);
		}
	}
	
	/**
	 * 验证用户名，title名是否为空，是否存在
	 * @param adminTitle
	 * @return
	 * @throws Exception
	 */
	private DBRow checkAdminTitleNullOrExist(DBRow adminTitle) throws Exception{
		
		try{
			ImportTitleRelatedExcelErrorKey impTitRelErr = new ImportTitleRelatedExcelErrorKey();
			StringBuffer errorMessage = new StringBuffer("");//错误信息
			String adminUserName = adminTitle.getString("adminUserName");//账号
			String adminUserTitle = adminTitle.getString("adminUserTitle");//title名
			//String adminTitleSort	= adminTitle.getString("adminTitleSort");//titleSort
			long adminUserId = 0L;//用户ID
			long titleId = 0L;//titleId
			//检查admin是否为空
			if("".equals(adminUserName)){
				
				errorMessage.append(impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.ADMIN_IS_BLANK)+";");

			//检查admin是否存在
			}else{
				DBRow[] adminUsers = floorAdminMgrGZY.getActionsByPage(null, adminUserName);
				if(0 == adminUsers.length){
					errorMessage.append(adminUserName+":"+impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.ADMIN_NOT_EXIST)+";");
				}else{
					adminUserId = adminUsers[0].get("adid", 0L);
				}
			}
			
			//检查title是否为空
			if("".equals(adminUserTitle)){
				
				errorMessage.append(impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.TITLE_IS_BLANK)+";");
				
			}else{
				//检查title是否存在
				if(0 == floorProprietaryMgrZyj.findProprietaryCountByTitleName(adminUserTitle)){
					
					errorMessage.append(adminUserTitle+":"+impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.TITLE_NOT_EXIST)+";");
				}else{
					
					DBRow titleInfo = floorProprietaryMgrZyj.findProprietaryByTitleName(adminUserTitle);
					titleId = titleInfo.get("title_id", 0L);
				}
			}
			
			String error = errorMessage.toString();
			if(!"".equals(error.trim())){
				error = error.trim().substring(0, error.length()-1);
			}
			adminTitle.add("errorMessage", error);
			adminTitle.add("adminUserId", adminUserId);
			adminTitle.add("titleId", titleId);
			
			return adminTitle;
		}catch (Exception e){
			throw new SystemException(e,"checkAdminTitleNullOrExist(DBRow adminTitle)",log);
		}
	}
	
	/**
	 * 导入用户与title多对多的用户的title关系
	 */
	public DBRow addProprietaryAdminsByAdmins(HttpServletRequest request) throws Exception{
		
		try{
			String flag = "true";
			
			String[] importAllAdminTitles = request.getParameterValues("importAllAdminTitles");
			ArrayList<DBRow> adminUserTitleRows = new ArrayList<DBRow>();
			if(importAllAdminTitles.length > 0){
				for (int i = 0; i < importAllAdminTitles.length; i++){
					
					String[] importAdminOneTitleArr = importAllAdminTitles[i].split("_");
					String adminUserName = importAdminOneTitleArr[0];//账号
					String adminUserTitle = importAdminOneTitleArr[1];//title名
					String adminTitleRowNumber = importAdminOneTitleArr[2];
					
					DBRow adminUserTitleRow = new DBRow();
					adminUserTitleRow.add("adminUserName", adminUserName);
					adminUserTitleRow.add("adminUserTitle", adminUserTitle);
					adminUserTitleRow.add("adminTitleRowNumber", adminTitleRowNumber);
					adminUserTitleRows.add(adminUserTitleRow);
				}
			}
			
			DBRow[] adminTitleRows = adminUserTitleRows.toArray(new DBRow[0]);
			//重新验证
			DBRow[] errorAdminTitles = checkAdminTitles(adminTitleRows);
			
			//存在错误信息，提示
			if(errorAdminTitles.length > 0){
				flag = "false";
				
			//不存在错误信息，保存到数据库
			}else{
				
				for (int i = 0; i < adminTitleRows.length; i++){
					
					DBRow adminTitle = adminTitleRows[i];
					//账号
					String adminUserName = adminTitle.getString("adminUserName");
					//title Name
					String adminUserTitle = adminTitle.getString("adminUserTitle");
					//Account ID
					long adminUserId = 0L;
					//Title ID
					long title_id = 0L;
					
					DBRow[] adminUsers = floorAdminMgrGZY.getActionsByPage(null, adminUserName);
					
					if(adminUsers.length > 0){
						adminUserId = adminUsers[0].get("adid", 0L);
					}
					
					DBRow titleInfo = floorProprietaryMgrZyj.findProprietaryByTitleName(adminUserTitle);
					if(null != titleInfo){
						title_id = titleInfo.get("title_id", 0L);
					}
					this.importProprietaryAdmins(adminUserId, title_id);
				}
			}
			
			DBRow result = new DBRow();
			result.add("flag", flag);
			return result;
			
		}catch (Exception e){
			throw new SystemException(e,"addProprietaryAdminsByAdmins(HttpServletRequest request)",log);
		}
	}
	
	private void importProprietaryAdmins(long adid, long title_id) throws Exception{
		try{
			//查询[title_admin]中是否存在
			if(!floorProprietaryMgrZyj.findAdminAndTitleExists(adid,title_id)){
				//插入数据库
				floorProprietaryMgrZyj.insertTitleAdmin(adid,String.valueOf(title_id));
			}
		}catch (Exception e){
			throw new SystemException(e,"",log);
		}
	}
	/**
	 * 查询admin与title的关系
	 * @param proPsId 仓库
	 * @param proJsId 角色
	 * @param proAdgid 部门
	 * @param filter_lid 办公地点
	 * @param lock_state 状态
	 * @param pc 分页
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAdminsTitleByAdminNameAdgidStorageStatus(String cmd, long proPsId, String proJsId, long proAdgid, long filter_lid, int lock_state, String account, PageCtrl pc) throws Exception
	{
		try
		{
			return floorProprietaryMgrZyj.findAdminsTitleByAdminNameAdgidStorageStatus(cmd, proPsId, proJsId, proAdgid, filter_lid, lock_state, account, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findAdminsTitleByAdminNameAdgidStorageStatus(String cmd, long proPsId, String proJsId, long proAdgid, long filter_lid, int lock_state, String account, PageCtrl pc)",log);
		}
	}
	
	public String exportAdminTitlesByAdminNameAdgidStorageStatus(HttpServletRequest request) throws Exception
	{
		try
		{
			String cmd = StringUtil.getString(request, "cmd");
			long proPsId = StringUtil.getLong(request, "proPsId"); //仓库
			String proJsId = StringUtil.getString(request,"proJsId");//角色
			long proAdgid = StringUtil.getLong(request, "proAdgid"); //部门
			long filter_lid = StringUtil.getLong(request,"filter_lid");//办公地点
			int	lock_state	= StringUtil.getInt(request,"lock_state");
			String account = StringUtil.getString(request, "account");
			
	//		//system.out.println("----"+","+proPsId+","+proJsId+","+proAdgid+","+filter_lid+","+lock_state+","+account+"----");
			
			DBRow[] exportAdminTitles = this.findAdminsTitleByAdminNameAdgidStorageStatus(cmd, proPsId, proJsId, proAdgid, filter_lid, lock_state, account, null);
			
			long[] storageAddressBook = new long[exportAdminTitles.length];
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/admin/ExportAdminTitles.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			//wb.setSheetName(0,"第一张表单");
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row = sheet.getRow(0);
			HSSFCellStyle style = wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			style.setLocked(false);
			style.setWrapText(true);
			
			HSSFCellStyle stylelock = wb.createCellStyle();
			stylelock.setLocked(true); //创建一个锁定样式
			stylelock.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			
			if(exportAdminTitles.length > 0)
			{
				for(int i = 0 ; i< exportAdminTitles.length ; i++){
					row = sheet.createRow((int)i+1);
					sheet.setDefaultColumnWidth(30);
					DBRow exportAdminTitle = exportAdminTitles[i];
					//设置excel列的值
					storageAddressBook[i] = exportAdminTitle.get("adid",0L);
					row.createCell(0).setCellValue(exportAdminTitle.getString("account"));
					row.getCell(0).setCellStyle(style);
					
					row.createCell(1).setCellValue(exportAdminTitle.getString("title_name"));
					row.getCell(1).setCellStyle(style);
					
					row.createCell(2).setCellValue(exportAdminTitle.getString("title_admin_sort"));
					row.getCell(2).setCellStyle(style);
				}
			}
			//写文件  
			String path = "upl_excel_tmp/ExportAdminTitles_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xls";
			FileOutputStream os= new FileOutputStream(Environment.getHome()+path);
			wb.write(os);  
			os.close();  
			return (path);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"exportAdminTitlesByAdminNameAdgidStorageStatus(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 导入商品title
	 */
	public HashMap<String, DBRow[]> uploadProductTitleExcelShow(String filename, String type, HttpServletRequest request)throws Exception {
		
		try 
		{
			AdminMgr adminMgr 				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));//获取系统当前登录者id
			String path = "";
			InputStream is;
			Workbook wrb;
			HashMap<String,DBRow[]> resultMap = new HashMap<String, DBRow[]>();//返回值
			if ("show".equals(type) && null != filename && !"".equals(filename)) 
			{
				path = Environment.getHome() + "upl_imags_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				////system.out.println("filepath:"+path);
				DBRow[] productTitleRows = productTitlesExcelToRow(wrb);
				if(productTitleRows.length > 0)
				{
					DBRow[] errorProductTitles = checkProductTitles(productTitleRows, adminLoggerBean, request);//检查adminTitles
					resultMap.put("allExcelProductTitles", productTitleRows);
					resultMap.put("errorProductTitles",errorProductTitles);
				}
			}
			else if("data".equals(type) && null != filename && !"".equals(filename))
			{
				path = Environment.getHome() + "upl_imags_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				DBRow[] productTitleRows = productTitlesExcelToRow(wrb);
				DBRow[] errorProductTitles = new DBRow[0];
				if(productTitleRows.length > 0)
				{
					errorProductTitles = checkProductTitles(productTitleRows, adminLoggerBean, request);//检查adminTitles
					resultMap.put("allExcelProductTitles", productTitleRows);
					resultMap.put("errorProductTitles",errorProductTitles);
				}
			}
			return resultMap;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"uploadProductTitleExcelShow(String filename, String type, HttpServletRequest request)",log);
		}
	}
	
	
	/**
	 * 转化成DBRow类型
	 * @param wrb
	 * @return
	 * @throws Exception
	 */
	private DBRow[] productTitlesExcelToRow(Workbook wrb) throws Exception
	{
		try 
		{
			ArrayList<DBRow> productTitleRows = new ArrayList<DBRow>();
			Sheet rs = wrb.getSheet(0);
			int rsRows = rs.getRows();    //excel表记录行数
//			//system.out.println("excelRows");
			for(int i=1;i<rsRows;i++)
			{
				String productName = rs.getCell(0, i).getContents().trim();
				String productTitle = rs.getCell(1, i).getContents().trim();
				if(!"".equals(productName) || !"".equals(productTitle))
				{
					DBRow adminUserTitleRow = new DBRow();
					adminUserTitleRow.add("productName", productName);
					adminUserTitleRow.add("productTitle", productTitle);
					adminUserTitleRow.add("productTitleRowNumber", i);
					productTitleRows.add(adminUserTitleRow);
//					//system.out.println(productName+","+productTitle+","+i);
				}
			}
			return (productTitleRows.toArray(new DBRow[0]));
			  
		} catch (Exception e) {
			throw new SystemException(e,"productTitlesExcelToRow(Workbook wrb)",log);
		}
	}
	
	/**
	 * 检查商品title信息
	 * @param productTitles
	 * @return
	 * @throws Exception
	 */
	private DBRow[] checkProductTitles(DBRow[] productTitles, AdminLoginBean adminLoggerBean, HttpServletRequest request) throws Exception 
	{
		try 
		{
			ImportTitleRelatedExcelErrorKey impTitRelErr = new ImportTitleRelatedExcelErrorKey();
			ArrayList<DBRow> errorProductTitles = new ArrayList<DBRow>();
			HashMap<String, List<DBRow>> productTitleMap = new HashMap<String, List<DBRow>>();//用户ID,《titleId, titleSort》
			HashMap<String, String> productTitleNotRepeat = new HashMap<String, String>();
			List<DBRow> needCheckProductTitles = new ArrayList<DBRow>();
			
			for (int i = 0; i < productTitles.length; i++) 
			{
				DBRow productTitleAndError = productTitles[i];
				
				productTitleAndError	 = this.checkProductTitleNullOrExist(productTitleAndError, adminLoggerBean); 
				String productId		 = productTitleAndError.getString("productId");
				String titleId			 = productTitleAndError.getString("titleId");
				int productTitleRowNumber = productTitleAndError.get("productTitleRowNumber", 0);
				
				//提交的数据是否为空或者存在的检查
				if (!"".equals(productTitleAndError.getString("errorMessage")))
				{
					errorProductTitles.add(productTitleAndError);
				}
				else
				{
					//提交的数据是否重复
					if(productTitleNotRepeat.containsKey(productId+"_"+titleId))
					{
						String errorMsg = productTitleAndError.getString("errorMessage");
						if(!"".equals(errorMsg))
						{
							errorMsg += ";";
						}
						errorMsg += impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.PRODUCT_TITLE_REPEAT)
									+ ",与excel表中的第"+productTitleNotRepeat.get(productId+"_"+titleId)+"行重复;";
						productTitleAndError.add("errorMessage", errorMsg);
					}
					else
					{
						productTitleNotRepeat.put(productId+"_"+titleId, ""+productTitleRowNumber);
					}
					if (!"".equals(productTitleAndError.getString("errorMessage")))
					{
						errorProductTitles.add(productTitleAndError);
					}
				}
				if("".equals(productTitleAndError.getString("errorMessage")))
				{
					needCheckProductTitles.add(productTitleAndError);
				}
			}
//			//system.out.println("errorRows");
//			for (int i = 0; i < errorProductTitles.size(); i++) 
//			{
//				DBRow row = errorProductTitles.get(i);
//			}
			return (errorProductTitles.toArray(new DBRow[0]));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"checkProductTitles(DBRow[] productTitles, HttpServletRequest request) ",log);
		}
	}
	
	/**
	 * 验证用户名，title名是否为空，是否存在
	 * @param productTitleRow
	 * @return
	 * @throws Exception
	 */
	private DBRow checkProductTitleNullOrExist(DBRow productTitleRow, AdminLoginBean adminLoggerBean) throws Exception
	{
		try
		{
			ImportTitleRelatedExcelErrorKey impTitRelErr = new ImportTitleRelatedExcelErrorKey();
			StringBuffer errorMessage	= new StringBuffer("");//错误信息
			String productName	= productTitleRow.getString("productName");//商品名
			String productTitle	= productTitleRow.getString("productTitle");//title名
			long productId		= 0L;//用户ID
			long titleId			= 0L;//titleId
			
			//检查admin是否为空
			if("".equals(productName))
			{
				errorMessage.append(impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.PRODUCT_IS_BLANK)+";");
			}
			//检查admin是否存在
			else
			{
				//商品条码可不惟一 zyj
				DBRow product = floorProductMgr.getDetailProductByPname(productName);
				if(null == product)
				{
					errorMessage.append(productName+":"+impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.PRODUCT_NOT_EXIST)+";");
				}
				else
				{
					productId = product.get("pc_id", 0L);
				}
			}
			//检查title是否为空
			if("".equals(productTitle))
			{
				errorMessage.append(impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.TITLE_IS_BLANK)+";");
			}
			else
			{
				//检查title是否存在
				if(0 == floorProprietaryMgrZyj.findProprietaryCountByTitleName(productTitle))
				{
					errorMessage.append(productTitle+":"+impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.TITLE_NOT_EXIST)+";");
				}
				else
				{
					//检查此title是否是此用户的，只有登录者是客户需要验证
					if(DBRowUtils.existDeptAndPostInLoginBean(GlobalKey.CLIENT_ADGID, adminLoggerBean))
					{
						DBRow titleAdmin = floorProprietaryMgrZyj.findTitleAdminByAdidTitleName(adminLoggerBean.getAdid(), productTitle);
						if(null == titleAdmin)
						{
							errorMessage.append(productTitle+":"+impTitRelErr.getImportTitleRelatedExcelErrorKeyById(ImportTitleRelatedExcelErrorKey.ADMIN_TITLE_NOT_EXIST)+";");
						}
						else
						{
							DBRow titleInfo = floorProprietaryMgrZyj.findProprietaryByTitleName(productTitle);
							titleId = titleInfo.get("title_id", 0L);
						}
					}
					else
					{
						DBRow titleInfo = floorProprietaryMgrZyj.findProprietaryByTitleName(productTitle);
						titleId = titleInfo.get("title_id", 0L);
					}
				}
			}
			
			String error = errorMessage.toString();
			if(!"".equals(error.trim()))
			{
				error = error.trim().substring(0, error.length()-1);
			}
			productTitleRow.add("errorMessage", error);
			productTitleRow.add("productId", productId);
			productTitleRow.add("titleId", titleId);
			
			return productTitleRow;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"checkProductTitleNullOrExist(DBRow productTitleRow) ",log);
		}
	}
	
	
	/**
	 * 导入用户与title多对多的用户的title关系
	 */
	public DBRow addProprietaryProductsByProducts(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminMgr adminMgr 				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));//获取系统当前登录者
			
			String flag = "true";
			//将要提交的数据重新检查一刻，避免在显示上传后的提示页面到点击提交的这段时间内，有些正确数据变成错误数据
			String[] importAllProductTitles = request.getParameterValues("importAllProductTitles");
//			return new DBRow();
//			/*
			ArrayList<DBRow> productTitleList = new ArrayList<DBRow>();
			if(importAllProductTitles.length > 0)
			{
				for (int i = 0; i < importAllProductTitles.length; i++) 
				{
					String[] importProductOneTitleArr = importAllProductTitles[i].split("_");
					String productName	= importProductOneTitleArr[0];//账号
					String productTitle	= importProductOneTitleArr[1];//title名
					String adminTitleRowNumber	= importProductOneTitleArr[2];//titleSort
					
					DBRow productTitleRow = new DBRow();
					productTitleRow.add("productName", productName);
					productTitleRow.add("productTitle", productTitle);
					productTitleRow.add("adminTitleRowNumber", adminTitleRowNumber);
					productTitleList.add(productTitleRow);
				}
			}
			DBRow[] productTitleRows = productTitleList.toArray(new DBRow[0]);
			//检查adminTitles
			DBRow[] errorProductTitles = checkProductTitles(productTitleRows, adminLoggerBean, request);
			//存在错误信息，提示
			if(errorProductTitles.length > 0)
			{
				flag = "false";
			}
			//不存在错误信息，保存到数据库
			else
			{
				HashMap<String, List<DBRow>> productTitleMap = new HashMap<String, List<DBRow>>();
				for (int i = 0; i < productTitleRows.length; i++) 
				{
					DBRow productTitleRow	= productTitleRows[i];
					String productName		= productTitleRow.getString("productName");//账号
					String productTitle		= productTitleRow.getString("productTitle");//title名
					long productId			= 0L;
					long title_id			= 0L;
					//商品条码可不惟一 zyj
					DBRow product = floorProductMgr.getDetailProductByPname(productName);
					if(null != product)
					{
						productId = product.get("pc_id", 0L);
					}
					DBRow titleInfo = floorProprietaryMgrZyj.findProprietaryByTitleName(productTitle);
					if(null != titleInfo)
					{
						title_id = titleInfo.get("title_id", 0L);
					}
					productTitleRow.add("titleId", title_id);
					List<DBRow> titleAndSortList = new ArrayList<DBRow>();
					if(productTitleMap.containsKey(productId+""))
					{
						titleAndSortList = productTitleMap.get(String.valueOf(productId)); 
					}
					titleAndSortList.add(productTitleRow);
					productTitleMap.put(String.valueOf(productId), titleAndSortList);
				}
				Set<Entry<String, List<DBRow>>> productTitleSet = productTitleMap.entrySet();
				for (Iterator iterator = productTitleSet.iterator(); iterator.hasNext();) 
				{
					Entry<String, List<DBRow>> entry = (Entry<String, List<DBRow>>) iterator.next();
					long productId = Long.parseLong(entry.getKey());
					long adid = this.findAdminIdByCondition(true, 0L, request);
					List<DBRow> titleAndSort = entry.getValue();
					long fl = this.addProprietaryProductSeleteds(adid, productId, 0L, 0L, 3, titleAndSort, true, false, request);
				}
			}
			DBRow result = new DBRow();
			result.add("flag", flag);
			return result;
//			*/
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addProprietaryProductsByProducts(HttpServletRequest request)",log);
		}
	}
	
	

	@Override
	public DBRow[] filterExportProductTitlesByPcLineCategoryTitleId( boolean adidNullToLogin, long catalog_id, long product_line_id,int union_flag, int product_file_type,
		int product_upload_status, long title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception 
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.filterExportProductTitlesByPcLineCategoryTitleId(catalog_id, product_line_id, union_flag, product_file_type, product_upload_status, title_id, adid, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"filterExportProductTitlesByPcLineCategoryTitleId( boolean adidNullToLogin, long catalog_id, long product_line_id,int union_flag, int product_file_type,"
					+"int product_upload_status, long title_id, long adid, PageCtrl pc, HttpServletRequest request)",log);
		}
	}

	@Override
	public DBRow[] findExportAllProductsByTitleAdid(boolean adidNullToLogin, long title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception {
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findExportAllProductsByTitleAdid(title_id, adid, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findExportAllProductsByTitleAdid(boolean adidNullToLogin, long title_id, long adid, PageCtrl pc, HttpServletRequest request)",log);
		}
	}

	@Override
	public DBRow[] findExportDetailProductLikeSearch(boolean adidNullToLogin, String key, long title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception {
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findExportDetailProductLikeSearch(key, title_id, adid, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findExportDetailProductLikeSearch(boolean adidNullToLogin, String key, long title_id, long adid, PageCtrl pc, HttpServletRequest request)",log);
		}
	}

	@Override
	public DBRow[] findExportProductInfosProductLineProductCodeByLineId( boolean adidNullToLogin, long pc_line_id, long title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception {
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findExportProductInfosProductLineProductCodeByLineId(pc_line_id, title_id, adid, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findExportProductInfosProductLineProductCodeByLineId( boolean adidNullToLogin, long pc_line_id, long title_id, long adid, PageCtrl pc, HttpServletRequest request)",log);
		}
	}

	/**
	 * 通过商品ID，获取title
	 */
	public DBRow[] findProprietaryByAdminProduct(boolean adidNullToLogin,long adid, long pc_id, int begin, int length, PageCtrl pc, int dataOrCount, HttpServletRequest request)throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProprietaryByAdminProduct(adid, pc_id, begin, length, pc, dataOrCount);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findProprietaryByAdminProduct(boolean adidNullToLogin,long adid, long pc_id, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	public DBRow[] filterProductAndRelateInfoByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status, String title_id, long adid, int begin, int length, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try{
			return this.filterProductAndRelateInfoByPcLineCategoryTitleId(adidNullToLogin, catalog_id, product_line_id, union_flag, product_file_type, product_upload_status, title_id, adid, begin, length, equal_or_not, product_id, pc, request, -1L);
		}catch(Exception e){
			throw new SystemException(e,"filterProductAndRelateInfoByPcLineCategoryTitleId(boolean adidNullToLogin, long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 商品信息管理
	 */
	public DBRow[] filterProductAndRelateInfoByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status, String title_id, long adid, int begin, int length, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request, long active) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			DBRow[] rows = floorProprietaryMgrZyj.filterProductByPcLineCategoryTitleId(this.parseStrToLongArr(catalog_id), this.parseStrToLongArr(product_line_id), union_flag, product_file_type, product_upload_status, this.parseStrToLongArr(title_id), adid,equal_or_not, product_id, pc, active);
			for (int i = 0; i < rows.length; i++)
			{
				DBRow product = rows[i];
				long pc_id = product.get("pc_id", 0L);
				product.add("titles", this.getByProduct((int)pc_id));
				DBRow[] titleAlls = floorProprietaryMgrZyj.findProprietaryByAdminProduct(adid, pc_id, -1, -1, null, DataOrCountKey.COUNT);
				product.add("title_all_count", null!=titleAlls[0]?titleAlls[0].get("cn", 0):0);
			}
			return rows;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"filterProductAndRelateInfoByPcLineCategoryTitleId(boolean adidNullToLogin, long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request, long active)",log);
		}
	}
	
	/**
	 * 根据产品分类和生产商，查询与某个品牌商相关的产品
	 */
	public DBRow[] filterProductAndRelateInfoByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status, String title_id, long adid, int begin, int length, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request,long active,int customerId) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			DBRow[] rows = this.filterProductByPcLineCategoryTitleId(this.parseStrToLongArr(catalog_id), this.parseStrToLongArr(product_line_id), union_flag, product_file_type, product_upload_status, this.parseStrToLongArr(title_id), adid,equal_or_not, product_id, pc,active,customerId);
			for (int i = 0; i < rows.length; i++)
			{
				DBRow product = rows[i];
				long pc_id = product.get("pc_id", 0L);
				product.add("titles", this.getByProductAndCustomer((int)pc_id, customerId));
				
				DBRow[] titleAlls = floorProprietaryMgrZyj.findProprietaryByAdminProduct(adid, pc_id, -1, -1, null, DataOrCountKey.COUNT);
				product.add("title_all_count", null!=titleAlls[0]?titleAlls[0].get("cn", 0):0);
			}
			return rows;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"filterProductAndRelateInfoByPcLineCategoryTitleId(boolean adidNullToLogin, long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}	
	
	private DBRow[] filterProductByPcLineCategoryTitleId(Long[] catalog_id, Long[] product_line_id, int union_flag, int product_file_type, int product_upload_status, Long[] title_id, long adid, int equal_or_not, long product_id, PageCtrl pc,long active,int customerId)
    throws Exception{
		  try
		     {
		       StringBuffer sb = new StringBuffer();
		 
		       if (0 != product_file_type)
		       {
		         sb.append(" select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc");
		         sb.append(" from (select s.* from ");
		         sb.append(" \t\t( select p.pc_id , p.alive, p.catalog_id, p.heigth, p.length,");
		         sb.append(" \t\t\t\t\t\t p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,");
		         sb.append(" \t\t\t\t\t\t p.unit_price, p.volume, p.weight, p.width,p.sn_size");
		         sb.append(" \t\t\t\t\t\t,p.length_uom,p.weight_uom,p.price_uom");
		         sb.append(" \t\t\tfrom product p ");
		         sb.append(" \t\t\tLEFT JOIN (select pc_id , count(pc_id) as count_product from product_file ");
		         sb.append(" \t\t\t\t\t\t\twhere file_with_type = ").append(31);
		         sb.append("\t\t\t\t\t\t\t\tand product_file_type = ").append(product_file_type);
		         sb.append(" \t\t\t\t\t\t\tGROUP BY pc_id, product_file_type)m");
		         sb.append(" \t\t\tON m.pc_id = p.pc_id ");
		         sb.append(" \t\t\tWHERE p.orignal_pc_id = 0");
		         if(active > -1){
		        	 sb.append(" AND p.alive=" + active);
		         }
		         
		         if (union_flag > -1)
		         {
		           sb.append(" \t\tAND p.union_flag = ").append(union_flag);
		         }
		 
		         if (1 == product_upload_status)
		         {
		           sb.append("\t\t\t\tAND m.count_product IS NULL");
		         }
		         else if (2 == product_upload_status)
		         {
		           sb.append("\t\t\t\tAND m.count_product IS NOT NULL");
		         }
		         if (equal_or_not == YesOrNotKey.NO)
		         {
		           sb.append("\t\t\t\tAND p.pc_id !=" + product_id);
		         }
		 
		         sb.append("\t\t\t\t)s");
		 
		         sb.append(" \tLEFT JOIN product_catalog c ON s.catalog_id = c.id");
		         sb.append("\t\tLEFT JOIN product_line_define l ON l.id = c.product_line_id ");
		 
		         sb.append(" LEFT JOIN title_product tp ON s.pc_id = tp.tp_pc_id");
		         if (adid > 0L)
		         {
		           //sb.append("\t\tJOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
		         }
		 
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           sb.append("     LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
		         }
		         sb.append(" \tWHERE 1=1 and tp.tp_customer_id=" + customerId);
		         if ((null != product_line_id) && (product_line_id.length > 0))
		         {
		           sb.append(" AND ( l.id = " + product_line_id[0]);
		           for (int i = 1; i < product_line_id.length; ++i)
		           {
		             sb.append(" OR l.id = " + product_line_id[i]);
		           }
		           sb.append(" )");
		         }
		         if ((null != title_id) && (title_id.length > 0))
		         {
		           sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
		           for (int i = 1; i < title_id.length; ++i)
		           {
		             sb.append(" OR tp.tp_title_id = " + title_id[i]);
		           }
		           sb.append(" )");
		         }
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           if (-1L != catalog_id[0].longValue())
		           {
		             sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
		           }
		           else
		           {
		             sb.append(" AND ( pcl.search_rootid = 0");
		           }
		           for (int i = 1; i < catalog_id.length; ++i)
		           {
		             sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
		           }
		           sb.append(" )");
		         }
		         sb.append("  GROUP BY s.pc_id");
		         sb.append(" \t) z");
		         sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
		         sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
		         sb.append("\tLEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
		         sb.append(" order by z.pc_id desc");
		       }
		       else if ((0 == product_file_type) && (1 == product_upload_status))
		       {
		         sb.append(" select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
		         sb.append("  (select s.* from ");
		         sb.append("  \t(select w.pc_id , w.alive, w.catalog_id, w.heigth, w.length,");
		         sb.append("  \t\t\t\tw.orignal_pc_id, w.p_name, w.union_flag, w.unit_name,");
		         sb.append("  \t\t\t\tw.unit_price, w.volume, w.weight, w.width,w.sn_size");
		         sb.append(" \t\t\t\t,w.length_uom,w.weight_uom,w.price_uom");
		         sb.append("  \t from product w left JOIN");
		         sb.append("  \t\t(select p.pc_id , n.count_product from product p LEFT JOIN");
		         sb.append("  \t\t\t(select count(*) as count_product,pc_id  ");
		         sb.append("  \t\t\t\t\tfrom (select * from product_file where file_with_type = ").append(31);
		         sb.append(" GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
		         sb.append("  \t\t\ton n.pc_id = p.pc_id ");
		         sb.append("  \t\t\twhere p.orignal_pc_id = 0");
		         sb.append("  \t\t\tand n.count_product = 5 ");
		         sb.append("  \t\t\tand p.orignal_pc_id = 0)q");
		         sb.append("  \tON w.pc_id = q.pc_id ");
		         sb.append("  \twhere w.orignal_pc_id = 0");
		         if(active > -1){
		        	 sb.append(" AND w.active=" + active);
		         }
		         if (union_flag > -1)
		         {
		           sb.append("\t\tAND w.union_flag = ").append(union_flag);
		         }
		         if (equal_or_not == YesOrNotKey.NO)
		         {
		           sb.append("\t\t\t\tAND w.pc_id !=" + product_id);
		         }
		         sb.append("  \tAND (q.count_product is null or q.count_product < 5) )s");
		         sb.append(" \tLEFT JOIN product_catalog c ON s.catalog_id = c.id");
		         sb.append("\t\tLEFT JOIN product_line_define l ON l.id = c.product_line_id ");
		 
		         sb.append(" LEFT\tJOIN title_product tp ON s.pc_id = tp.tp_pc_id");
		         if (adid > 0L)
		         {
		           //sb.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
		         }
		 
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           sb.append("     LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
		         }
		         sb.append(" \tWHERE 1=1 and tp.tp_customer_id=" + customerId);
		         if ((null != product_line_id) && (product_line_id.length > 0))
		         {
		           sb.append(" AND ( l.id = " + product_line_id[0]);
		           for (int i = 1; i < product_line_id.length; ++i)
		           {
		             sb.append(" OR l.id = " + product_line_id[i]);
		           }
		           sb.append(" )");
		         }
		         if ((null != title_id) && (title_id.length > 0))
		         {
		           sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
		           for (int i = 1; i < title_id.length; ++i)
		           {
		             sb.append(" OR tp.tp_title_id = " + title_id[i]);
		           }
		           sb.append(" )");
		         }
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           if (-1L != catalog_id[0].longValue())
		           {
		             sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
		           }
		           else
		           {
		             sb.append(" AND ( pcl.search_rootid = 0");
		           }
		           for (int i = 1; i < catalog_id.length; ++i)
		           {
		             sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
		           }
		           sb.append(" )");
		         }
		         sb.append("  GROUP BY s.pc_id ORDER BY s.pc_id");
		         sb.append("\t ) z");
		         sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
		         sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
		         sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
		         sb.append(" order by z.pc_id desc");
		       }
		       else if ((0 == product_file_type) && (2 == product_upload_status))
		       {
		         sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc ");
		         sb.append("   from(select q.* from ");
		         sb.append(" \t\t(select p.pc_id ,p.alive, p.catalog_id, p.heigth, p.length,");
		         sb.append(" \t\t\t\tp.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,");
		         sb.append(" \t\t\t\tp.unit_price, p.volume, p.weight, p.width,p.sn_size");
		         sb.append(" \t\t\t\t,p.length_uom,p.weight_uom,p.price_uom");
		         sb.append(" \t\t   from product p LEFT JOIN");
		         sb.append(" \t\t\t(select count(*) as count_product,pc_id  ");
		         sb.append(" \t\t\t\t\tfrom (select * from product_file where file_with_type = ").append(31);
		         sb.append("\t\t\t\t\t\tGROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
		         sb.append(" \t\t\ton n.pc_id = p.pc_id ");
		         sb.append(" \t\t\twhere p.orignal_pc_id = 0");
		         sb.append(" \t\t\tand n.count_product = 5 ");
		         if(active > -1){
		        	 sb.append(" AND p.alive=" + active);
		         }
		         if (union_flag > -1)
		         {
		           sb.append(" \t\tAND p.union_flag = ").append(union_flag);
		         }
		         if (equal_or_not == YesOrNotKey.NO)
		         {
		           sb.append("\t\t\t\tAND p.pc_id !=" + product_id);
		         }
		         sb.append(" \t\t\tand p.orignal_pc_id = 0)q");
		         sb.append(" \tLEFT JOIN product_catalog c ON q.catalog_id = c.id");
		         sb.append("\t\tLEFT JOIN product_line_define l ON l.id = c.product_line_id ");
		 
		         sb.append(" LEFT JOIN title_product tp ON q.pc_id = tp.tp_pc_id");
		         if (adid > 0L)
		         {
		           //sb.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
		         }
		 
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           sb.append("     LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
		         }
		         sb.append(" \tWHERE 1=1 and tp.tp_customer_id=" + customerId);
		         if ((null != product_line_id) && (product_line_id.length > 0))
		         {
		           sb.append(" AND ( l.id = " + product_line_id[0]);
		           for (int i = 1; i < product_line_id.length; ++i)
		           {
		             sb.append(" OR l.id = " + product_line_id[i]);
		           }
		           sb.append(" )");
		         }
		         if ((null != title_id) && (title_id.length > 0))
		         {
		           sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
		           for (int i = 1; i < title_id.length; ++i)
		           {
		             sb.append(" OR tp.tp_title_id = " + title_id[i]);
		           }
		           sb.append(" )");
		         }
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           if (-1L != catalog_id[0].longValue())
		           {
		             sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
		           }
		           else
		           {
		             sb.append(" AND ( pcl.search_rootid = 0");
		           }
		           for (int i = 1; i < catalog_id.length; ++i)
		           {
		             sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
		           }
		           sb.append(" )");
		         }
		         sb.append("  GROUP BY q.pc_id ORDER BY q.pc_id");
		         sb.append("\t\t ) z");
		         sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
		         sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
		         sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
		         sb.append(" order by z.pc_id  desc");
		       }
		       else
		       {
		         sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
		         sb.append("\t(select p.pc_id , p.alive, p.catalog_id, p.heigth, p.length,");
		         sb.append(" p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,p.unit_price, p.volume, p.weight, p.width,p.sn_size ,p.length_uom,p.weight_uom,p.price_uom,c.title");
		         sb.append(" FROM product p LEFT JOIN product_catalog c ON p.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
		 
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           sb.append(" LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
		         }
		 
		         sb.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
		         if (adid > 0L)
		         {
		           //sb.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
		         }
		 
		         sb.append("\tWHERE 1=1 and tp.tp_customer_id=" + customerId);
		         sb.append("\tAND p.orignal_pc_id = 0 ");
		         if(active > -1){
		        	 sb.append(" AND p.alive=" + active);
		         }
		         if ((null != product_line_id) && (product_line_id.length > 0))
		         {
		           sb.append(" AND ( l.id = " + product_line_id[0]);
		           for (int i = 1; i < product_line_id.length; ++i)
		           {
		             sb.append(" OR l.id = " + product_line_id[i]);
		           }
		           sb.append(" )");
		         }
		         if ((null != title_id) && (title_id.length > 0))
		         {
		           sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
		           for (int i = 1; i < title_id.length; ++i)
		           {
		             sb.append(" OR tp.tp_title_id = " + title_id[i]);
		           }
		           sb.append(" )");
		         }
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           if (-1L != catalog_id[0].longValue())
		           {
		             sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
		           }
		           else
		           {
		             sb.append(" AND ( pcl.search_rootid = 0");
		           }
		           for (int i = 1; i < catalog_id.length; ++i)
		           {
		             sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
		           }
		           sb.append(" )");
		         }
		         if (union_flag > -1)
		         {
		           sb.append("  AND p.union_flag = ").append(union_flag);
		         }
		         if (equal_or_not == YesOrNotKey.NO)
		         {
		           sb.append("\t\t\t\tAND p.pc_id !=" + product_id);
		         }
		         sb.append("  GROUP BY p.pc_id ORDER BY p.pc_id");
		         sb.append("\t ) z");
		         sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
		         sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
		         sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
		         sb.append(" order by z.pc_id desc");
		       }
		 
		       return this.dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		     }
		     catch (Exception e)
		     {
		       throw new Exception("FloorProprietaryMgrZyj.filterProductByPcLineCategoryTitleId(long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, PageCtrl pc):" + e);
		     }
	}
	
	public DBRow[] findDetailProductRelateInfoLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int begin, int length,int union_flag, int equal_or_not, long product_id,PageCtrl pc, HttpServletRequest request)throws Exception
	{
		try
		{
			return findDetailProductRelateInfoLikeSearch(adidNullToLogin, key, title_id, adid, begin, length, union_flag, equal_or_not, product_id, pc, request, -1L);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findDetailProductRelateInfoLikeSearch(boolean adidNullToLogin, String key,long title_id, long adid, int begin, int length,PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 商品信息管理
	 */
	public DBRow[] findDetailProductRelateInfoLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int begin, int length,int union_flag, int equal_or_not, long product_id,PageCtrl pc, HttpServletRequest request, long active)throws Exception
	{
		try
		{
			
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			DBRow[] rows =  floorProprietaryMgrZyj.findDetailProductLikeSearch(key, this.parseStrToLongArr(title_id), adid,union_flag, equal_or_not, product_id, pc, active);
			for (int i = 0; i < rows.length; i++)
			{
				DBRow product = rows[i];
				long pc_id = product.get("pc_id", 0L);
				product.add("titles", this.getByProduct((int)pc_id));
				
				DBRow[] titleAlls = floorProprietaryMgrZyj.findProprietaryByAdminProduct(adid, pc_id, -1, -1, null, DataOrCountKey.COUNT);
				product.add("title_all_count", null!=titleAlls[0]?titleAlls[0].get("cn", 0):0);
			}
			return rows;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findDetailProductRelateInfoLikeSearch(boolean adidNullToLogin, String key,long title_id, long adid, int begin, int length,PageCtrl pc, HttpServletRequest request, long active)",log);
		}
	}
	
	/**
	 * 根据指定条件，搜索与特定品牌商相关的产品
	 * @param adidNullToLogin
	 * @param key
	 * @param title_id
	 * @param adid
	 * @param begin
	 * @param length
	 * @param union_flag
	 * @param equal_or_not
	 * @param product_id
	 * @param pc
	 * @param request
	 * @param customerId  品牌商ID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findDetailProductRelateInfoLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int begin, int length, int union_flag, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request,long active,int customerId)throws Exception
	{
		try
		{
			
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			DBRow[] rows =  this.findDetailProductByCustomerAndCondition(key, this.parseStrToLongArr(title_id), adid, union_flag, equal_or_not, product_id, pc,active, customerId);
			for (int i = 0; i < rows.length; i++)
			{
				DBRow product = rows[i];
				long pc_id = product.get("pc_id", 0L);
				product.add("titles", this.getByProductAndCustomer((int)pc_id, customerId));
				
				DBRow[] titleAlls = floorProprietaryMgrZyj.findProprietaryByAdminProduct(adid, pc_id, -1, -1, null, DataOrCountKey.COUNT);
				product.add("title_all_count", null!=titleAlls[0]?titleAlls[0].get("cn", 0):0);
			}
			return rows;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findDetailProductRelateInfoLikeSearch(boolean adidNullToLogin, String key,long title_id, long adid, int begin, int length,PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	protected DBRow[]  findDetailProductByCustomerAndCondition(String key, Long[] title_id, long adid, int union_flag, int equal_or_not, long product_id, PageCtrl pc,long active,int customerId) throws Exception{
	     try
	     {
	       StringBuffer sql = new StringBuffer();
	       sql.append(" select * from (");
	       sql.append("select p.pc_id, p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,");
	       sql.append("pcode_main.p_code as p_code,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width,p.sn_size ,pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc");
	       sql.append(" ,p.length_uom,p.weight_uom,p.price_uom");
	       sql.append(" from product as p ");
	       sql.append(" JOIN product_code AS pcode_main ON p.pc_id = pcode_main.pc_id AND pcode_main.code_type=").append(CodeTypeKey.Main);
	       sql.append(" LEFT JOIN product_code AS pcode_amazon ON p.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = ").append(CodeTypeKey.Amazon);
	       sql.append(" LEFT JOIN product_code AS pcode_upc ON p.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = ").append(CodeTypeKey.UPC);
	       sql.append(" JOIN title_product tp ON p.pc_id = tp.tp_pc_id ");
	       if ((null != title_id) && (title_id.length > 0))
	       {
	         
	         sql.append(" AND (tp.tp_title_id = " + title_id[0]);
	         for (int i = 1; i < title_id.length; ++i)
	         {
	           sql.append(" OR tp.tp_title_id = " + title_id[i]);
	         }
	         sql.append(" )");
	       }
	 
	       if (adid > 0L)
	       {
	         //sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
	       }
	       sql.append(" where p_name like '%" + key + "%' and   tp.tp_customer_id=" + customerId);
	       if(active > -1){
	    	   sql.append(" AND p.alive = ").append(active);
	       }
	       if (union_flag > -1)
	       {
	         sql.append(" AND p.union_flag = ").append(union_flag);
	       }
	       if (equal_or_not == YesOrNotKey.NO)
	       {
	         sql.append(" AND p.pc_id !=" + product_id);
	       }
	 
	       sql.append(" union ");
	       sql.append(" select p.pc_id, p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,");
	       sql.append(" pcode_main.p_code as p_code,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width,p.sn_size ,pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc");
	       sql.append(" ,p.length_uom,p.weight_uom,p.price_uom");
	       sql.append(" from product as p ");
	       sql.append(" JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
	       if ((null != title_id) && (title_id.length > 0))
	       {
	         
	         sql.append(" AND (tp.tp_title_id = " + title_id[0]);
	         for (int i = 1; i < title_id.length; ++i)
	         {
	           sql.append(" OR tp.tp_title_id = " + title_id[i]);
	         }
	         sql.append(" )");
	       }
	 
	       if (adid > 0L)
	       {
	         //sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
	       }
	       sql.append(" JOIN product_code AS pcode_main ON p.pc_id = pcode_main.pc_id AND pcode_main.code_type=").append(CodeTypeKey.Main);
	       sql.append(" LEFT JOIN product_code AS pcode_amazon ON p.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = ").append(CodeTypeKey.Amazon);
	       sql.append(" LEFT JOIN product_code AS pcode_upc ON p.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = ").append(CodeTypeKey.UPC);
	       sql.append(" where pcode_main.p_code like '%" + key + "%' and   tp.tp_customer_id=" + customerId);
	       if (union_flag > -1)
	       {
	         sql.append(" AND p.union_flag = ").append(union_flag);
	       }
	       if (equal_or_not == YesOrNotKey.NO)
	       {
	         sql.append(" AND p.pc_id !=" + product_id);
	       }
	       sql.append(" ) a ");
	       sql.append(" order by a.pc_id desc");
	 
	       return this.dbUtilAutoTran.selectMutliple(sql.toString(), pc);
	     }
	     catch (Exception e)
	     {
	       throw new Exception("FloorProprietaryMgrZyj.findDetailProductLikeSearch(String key,long title_id, long adid,PageCtrl pc) error:" + e);
	     }
		
	}
	
	public DBRow[] findAllProductsRelateInfoByTitleAdid(boolean adidNullToLogin, String title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			return findAllProductsRelateInfoByTitleAdid(adidNullToLogin, title_id, adid, begin, length, pc, request, -1L);
		}catch (Exception e)
		{
			throw new SystemException(e,"findAllProductsRelateInfoByTitleAdid(boolean adidNullToLogin, long title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 商品信息管理
	 */
	public DBRow[] findAllProductsRelateInfoByTitleAdid(boolean adidNullToLogin, String title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request, long active) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			DBRow[] rows = floorProprietaryMgrZyj.findAllProductsByTitleAdid(this.parseStrToLongArr(title_id), adid, pc, active);
			
			for (int i = 0; i < rows.length; i++)
			{
				DBRow product = rows[i];
				long pc_id = product.get("pc_id", 0L);
				product.add("titles", this.getByProduct((int)pc_id));
				
				DBRow[] titleAlls = floorProprietaryMgrZyj.findProprietaryByAdminProduct(adid, pc_id, -1, -1, null, DataOrCountKey.COUNT);
				product.add("title_all_count", null!=titleAlls[0]?titleAlls[0].get("cn", 0):0);
			}
			
			return rows;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findAllProductsRelateInfoByTitleAdid(boolean adidNullToLogin, String title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request, long active)",log);
		}
		
	}
	
	public DBRow[] findAllProductsRelateInfoByTitle(boolean adidNullToLogin,  long adid, int begin, int length, PageCtrl pc, HttpServletRequest request,int titleId) throws Exception
	{
		try{
			return findAllProductsRelateInfoByTitle(adidNullToLogin, adid, begin, length, pc, request, titleId, -1L);
		}catch(Exception e){
			throw new SystemException(e,"findAllProductsRelateInfoByTitle(boolean adidNullToLogin,  long adid, int begin, int length, PageCtrl pc, HttpServletRequest request,int titleId)",log);
		}
	}
	
	/**
	 * 商品信息管理
	 */
	public DBRow[] findAllProductsRelateInfoByTitle(boolean adidNullToLogin,  long adid, int begin, int length, PageCtrl pc, HttpServletRequest request,int titleId, long active) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			DBRow[] rows = floorProprietaryMgrZyj.findAllProductsByTitleAdid(this.parseStrToLongArr(titleId + ""), adid, pc, active);
			
			for (int i = 0; i < rows.length; i++)
			{
				DBRow product = rows[i];
				long pc_id = product.get("pc_id", 0L);
				product.add("titles", this.getByProductAndTitle((int)pc_id, titleId));
				
				
				DBRow[] titleAlls = floorProprietaryMgrZyj.findProprietaryByAdminProduct(adid, pc_id, -1, -1, null, DataOrCountKey.COUNT);
				product.add("title_all_count", null!=titleAlls[0]?titleAlls[0].get("cn", 0):0);
			}
			
			return rows;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findAllProductsRelateInfoByTitle(boolean adidNullToLogin,  long adid, int begin, int length, PageCtrl pc, HttpServletRequest request,int titleId, long active)",log);
		}
		
	}	
	
	public DBRow[] findAllProductsRelateInfoByCustomer(boolean adidNullToLogin,String title_Ids , long adid, int begin, int length, PageCtrl pc, HttpServletRequest request,long active,int customerId) throws Exception{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			DBRow[] rows = this.findAllProductsByTitleAndCustomer(this.parseStrToLongArr(title_Ids + ""),adid,active,customerId,pc);
			
			for (int i = 0; i < rows.length; i++)
			{
				DBRow product = rows[i];
				long pc_id = product.get("pc_id", 0L);
				product.add("titles", this.getByProductAndCustomer((int)pc_id, customerId));
				
				
				DBRow[] titleAlls = floorProprietaryMgrZyj.findProprietaryByAdminProduct(adid, pc_id, -1, -1, null, DataOrCountKey.COUNT);
				product.add("title_all_count", null!=titleAlls[0]?titleAlls[0].get("cn", 0):0);
			}
			
			return rows;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findAllProductsRelateInfoByTitleAdid(boolean adidNullToLogin, long title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	
	protected DBRow[] findAllProductsByTitleAndCustomer(Long[] title_id, long adid,long active,int customerId,PageCtrl pc) throws Exception{
		  try
		    {
			      StringBuffer sql = new StringBuffer();
			      sql.append("select p.pc_id, p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,");
			      sql.append(" pcode_main.p_code as p_code,pcode_amazon.p_code as p_code2,pcode_upc.p_code as upc,");
			      sql.append(" p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width,p.sn_size ");
			      sql.append(" ,length_uom,weight_uom,price_uom,p.freight_class,p.nmfc_code,p.sku ,p.lot_no ");
			      sql.append(" from product as p ");
			      sql.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
		
			     /*if (adid > 0L)
			     {
			    	 sql.append("  JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
		         }*/
			     sql.append(" join product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type =" + CodeTypeKey.Main);
			     sql.append(" left join product_code as pcode_amazon on p.pc_id = pcode_amazon.pc_id and pcode_amazon.code_type =" + CodeTypeKey.Amazon);
			     sql.append(" left join product_code as pcode_upc on p.pc_id = pcode_upc.pc_id and pcode_upc.code_type =" + CodeTypeKey.UPC);
			     sql.append(" where orignal_pc_id = 0 and tp.tp_customer_id="  + customerId);
			     if(active > -1){
			    	 sql.append(" AND p.alive=").append(active);
			     }
			     
			     if ((null != title_id) && (title_id.length > 0))
			     {
			    	 sql.append(" AND (tp.tp_title_id = " + title_id[0]);
			         for (int i = 1; i < title_id.length; ++i)
			         {
			        	 sql.append(" OR tp.tp_title_id = " + title_id[i]);
			         }
			         sql.append(" )");
			     }
			     sql.append(" GROUP BY p.pc_id");
			     sql.append(" order by p.pc_id desc ,catalog_id desc,p_name asc");
		          if (pc != null)
		          {
		        	  return this.dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		          }else{
		        	  return this.dbUtilAutoTran.selectMutliple(sql.toString());
		          }
		     }
		 catch (Exception e)
	     {
		     throw new Exception("FloorProprietaryMgrZyj.findAllProductsByTitleAdid(long title_id, long adid, PageCtrl pc) error:" + e);
		 }		
	}
	
	/**
	 * 分页查询与特定产品相关的ProductCustomer
	 * @param productId
	 * @return
	 */
	public List<ProductCustomer>  getByProduct(int productId){
		
		//SELECT a.tp_title_id,a.tp_customer_id,b.customer_name,c.title_name  from title_product a inner JOIN customer_id b on  a.tp_customer_id=b.customer_key inner join title c on a.tp_title_id=c.title_id where a.tp_pc_id=1001747;
		List<ProductCustomer> list = new ArrayList<ProductCustomer>();
		Map<Integer,ProductCustomer> map = new HashMap<Integer,ProductCustomer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT a.tp_customer_id customer_id,b.customer_id customer_name,a.tp_title_id title_id,c.title_name  from title_product a inner JOIN customer_id b on  a.tp_customer_id=b.customer_key inner join title c on a.tp_title_id=c.title_id where a.tp_pc_id=? order by a.tp_id desc",param_row);
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					Integer titleId = rows[i].get("title_id", 0);
					ProductCustomer productCustomer = map.get(titleId);
					
					if( productCustomer == null){
						productCustomer = new ProductCustomer();
						productCustomer.setProductId(productId);
						productCustomer.getTitle().setId(rows[i].get("title_id", 0));
						productCustomer.getTitle().setName(rows[i].get("title_name", ""));
						list.add(productCustomer);
						map.put(titleId, productCustomer);
					}
					this.addCustomer(rows[i], productCustomer);
					
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}	

	/**
	 * 查询与特定产品,生产商相关的ProductCustomer
	 * @param productId   产品ID
	 * @param titleId     生产商ID
	 * @return
	 */
	private List<ProductCustomer>  getByProductAndTitle(int productId,int titleId){
		
		//SELECT a.tp_title_id,a.tp_customer_id,b.customer_name,c.title_name  from title_product a inner JOIN customer_id b on  a.tp_customer_id=b.customer_key inner join title c on a.tp_title_id=c.title_id where a.tp_pc_id=1001747;
		List<ProductCustomer> list = new ArrayList<ProductCustomer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			param_row.add("tp_title_id", titleId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT a.tp_customer_id customer_id,b.customer_id customer_name,a.tp_title_id title_id,c.title_name  from title_product a inner JOIN customer_id b on  a.tp_customer_id=b.customer_key inner join title c on a.tp_title_id=c.title_id where a.tp_pc_id=?  and a.tp_title_id=?",param_row);
			if(rows != null && rows.length > 0){
				ProductCustomer productCustomer = new ProductCustomer();
				for(int i = 0; i < rows.length; i++){
					if(i == 0){
						productCustomer.setProductId(productId);
						productCustomer.getTitle().setId(rows[i].get("title_id", 0));
						productCustomer.getTitle().setName(rows[i].get("title_name", ""));
					}
					this.addCustomer(rows[i], productCustomer);
				}
				list.add(productCustomer);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}		
	
	/**
	 * 查询与特定产品,品牌商相关的ProductCustomer
	 * @param productId   产品ID
	 * @param titleId     品牌商ID
	 * @return
	 */
	private List<ProductCustomer>  getByProductAndCustomer(int productId,int customerId){
		
		List<ProductCustomer> list = new ArrayList<ProductCustomer>();
		try {
			DBRow customer_row = this.dbUtilAutoTran.selectSingle("select a.customer_key id,a.customer_id name from customer_id a where a.customer_key=" + customerId);
			
			if(customer_row != null){
				String customerName = customer_row.get("name", "");
				Customer customer = new Customer(customerId,customerName);
				DBRow param_row = new DBRow();
				param_row.add("tp_pc_id", productId);
				param_row.add("tp_customer_id", customerId);
				DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT a.tp_title_id title_id,c.title_name  from title_product a inner join title c on a.tp_title_id=c.title_id where a.tp_pc_id=? and a.tp_customer_id=? order by a.tp_title_id desc",param_row);
				if(rows != null && rows.length > 0){
					for(int i = 0; i < rows.length; i++){
						Integer titleId = rows[i].get("title_id", 0);
						ProductCustomer productCustomer = new ProductCustomer();
						
							productCustomer = new ProductCustomer();
							productCustomer.setProductId(productId);
							productCustomer.getTitle().setId(rows[i].get("title_id", 0));
							productCustomer.getTitle().setName(rows[i].get("title_name", ""));
							
							productCustomer.getCustomers().add(customer);
							list.add(productCustomer);
					}
				}				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}	
	
	/**
	 * 将DBRow 对象转变为对应的 CustomerId对象
	 * @param row
	 * @return
	 */
	private Customer convertToCustomer(DBRow row){
		Assert.notNull(row);
		Customer customer = new Customer();
		customer.setId(row.get("customer_id", 1));
		customer.setName( row.get("customer_name", "") );
		return customer;
	}
	
	
	/**
	 * 往指定的ProductCustomer 中 添加生产商
	 * @param row
	 * @param productCustomer
	 */
	private void addCustomer(DBRow row,ProductCustomer productCustomer){
		Assert.notNull(row);
		Assert.notNull(productCustomer);
		productCustomer.getCustomers().add(this.convertToCustomer(row));
	}
	
	/**
	 * 通过用户ID,查询此用户是否是客户
	 * zyj
	 */
	public DBRow findAdminByGroupClientAdid(long adminId) throws Exception 
	{
		try
		{
			return floorProprietaryMgrZyj.findAdminByGroupClientAdid(adminId);
		}
		catch(Exception e)
		{
			throw new SystemException(e,"findAdminByGroupClientAdid(long adminId)",log);
		}
	}
	
	private long findAdminIdByCondition(boolean adidNullToLogin, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			if(null != request)
			{
				AdminMgr adminMgr 				= new AdminMgr();
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));//获取系统当前登录者
				if(DBRowUtils.existDeptAndPostInLoginBean(GlobalKey.CLIENT_ADGID, adminLoggerBean) && adidNullToLogin && 0 == adid)
				{
					adid = adminLoggerBean.getAdid();
				}
			}
			return adid;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"findAdminIdByCondition(boolean adidNullToLogin, long adid, HttpServletRequest request)",log);
		}	
	}
	
	private long findAdminIdByCondition(long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			AdminMgr adminMgr 				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));//获取系统当前登录者
			if(DBRowUtils.existDeptAndPostInLoginBean(GlobalKey.CLIENT_ADGID, adminLoggerBean) && adid <= 0)
			{
				adid = adminLoggerBean.getAdid();
			}
			return adid;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"findAdminIdByCondition(boolean adidNullToLogin, long adid, HttpServletRequest request)",log);
		}	
	}
	
	private long findAdminIdByCondition(long adid, AdminLoginBean adminLoggerBean) throws Exception
	{
		try
		{
			if(DBRowUtils.existDeptAndPostInLoginBean(GlobalKey.CLIENT_ADGID, adminLoggerBean) && adid <= 0)
			{
				adid = adminLoggerBean.getAdid();
			}
			return adid;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"findAdminIdByCondition(boolean adidNullToLogin, long adid, HttpServletRequest request)",log);
		}	
	}
	
	/**
	 * 给商品添加当前登录者优先级最大的商品和title关系
	 * @param pc_id
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addProprietaryProductByMaxPriority(long pc_id, AdminLoginBean adminLoggerBean) throws Exception
	{
		try
		{
			long adid = findAdminIdByCondition(0L, adminLoggerBean);
			long proPcId = 0L;
			if(adid > 0)
			{
				long titleId = floorProprietaryMgrZyj.findAdminMaxPriorityByAdid(adid);
				if(titleId > 0)
				{
					DBRow row = new DBRow();
					row.add("tp_pc_id", pc_id);
					row.add("tp_title_id", titleId);
					floorProprietaryMgrZyj.addProprietaryProduct(row);
					this.editTitleIndex(titleId, "update");
				}
			}
			return proPcId;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"findAdminIdByCondition(boolean adidNullToLogin, long adid, HttpServletRequest request)",log);
		}	
	}
	
	/**
	 * 通过titleID查询与title关联的产品线的ID和name
	 * @param adidNullToLogin=true 如果adid=0，是否将当前登录者的ID赋给adid
	 * @param pc_line_id
	 * @param title_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductLinesIdAndNameByTitleId(boolean adidNullToLogin, long adid, String pc_line_ids, String title_ids, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProductLinesIdAndNameByTitleId(adid, this.parseStrToLongArr(pc_line_ids), this.parseStrToLongArr(title_ids), begin, length, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProductLinesIdAndNameByTitleId(boolean adidNullToLogin, long adid, String pc_line_ids, String title_ids, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 通过titleID查询与title关联的产品分类
	 */
	public DBRow[] findProductCatagoryParentsIdAndNameByTitleId(boolean adidNullToLogin, long adid, String pc_line_id, String category_parent_id, String pc_cata_id, String title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(adid, this.parseStrToLongArr(pc_line_id), this.parseStrToLongArr(category_parent_id), this.parseStrToLongArr(pc_cata_id), this.parseStrToLongArr(title_id), begin, length, pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProductCatagoryParentsIdAndNameByTitleId(boolean adidNullToLogin, long adid, long pc_line_id, long category_parent_id, long pc_cata_id, long title_id, int begin, int length, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	private Long[] parseStrToLongArr(String str) throws NumberFormatException, Exception
	{
		List<Long> longList = new ArrayList<Long>();
		if(!StringUtil.isBlank(str))
		{
			String[] arr = str.split(",");
			for (int i = 0; i < arr.length; i++) 
			{
				if(!StringUtil.isBlankAndCanParseLong(arr[i]))
				{
					if(!"-1".equals(arr[i]))
					{
						longList.add(Long.valueOf(arr[i]));
					}
				}
			}
		}
		return longList.toArray(new Long[0]);
	}
	
	private Long[] parseStrToLongArrOriginal(String str) throws NumberFormatException, Exception
	{
		List<Long> longList = new ArrayList<Long>();
		if(!StringUtil.isBlank(str))
		{
			String[] arr = str.split(",");
			for (int i = 0; i < arr.length; i++) 
			{
				if(StringUtil.isBlankAndCanParseLongOriginal(arr[i]))
				{
					longList.add(Long.valueOf(arr[i]));
				}
			}
		}
		return longList.toArray(new Long[0]);
	}
	
	/**
	 * 根据订单号搜索订单
	 * @param key                         
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchTitleByNumber(String search_key,int search_mode,PageCtrl pc, int begin, int length, HttpServletRequest request) throws Exception 
	{
		try 
		{			
			if(!search_key.equals("")&&!search_key.equals("\""))
			{
				search_key = search_key.replaceAll("\"","");
				search_key = search_key.replaceAll("'","");
				search_key = search_key.replaceAll("\\*","");
			}
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			DBRow[] rows = TitleIndexMgr.getInstance().getSearchResults(search_key.toLowerCase(),search_mode,page_count,pc);
			for (int i = 0; i < rows.length; i++)
			{
				DBRow[] adminRows = this.findAdminsByTitleId(true, 0, rows[i].get("title_id", 0), begin, length, null, request);
				rows[i].add("admins", adminRows);
				DBRow[] pcLineRows = this.findProductLinesByTitleId(true, 0, "", rows[i].getString("title_id"), begin, length, null, request);
				rows[i].add("pcLines", pcLineRows);
				DBRow[] pcCataRows = this.findProductCatagorysByTitleId(true, 0, "","", "", rows[i].getString("title_id"), begin, length, null, request);
				rows[i].add("pcCatagorys", pcCataRows);
				DBRow[] productRows = this.findProductsByTitleId(true, 0, 0L,null, rows[i].get("title_id", 0), begin, length, null, request);
				rows[i].add("products", productRows);
			}
			
			return rows;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"searchTitleByNumber",log);
		}
	}
	
	public DBRow[] getTitleByEso(String search_key,int count) throws Exception {
		
		try {
			
			if(!search_key.equals("")&&!search_key.equals("\"")) {
				
				search_key = search_key.replaceAll("\"","");
				search_key = search_key.replaceAll("'","");
				search_key = search_key.replaceAll("\\*","");
			}
			
			DBRow[] rows = TitleIndexMgr.getInstance().getTitleByEso(search_key,count);
			
			return rows;
			
		} catch (Exception e) {
			throw new SystemException(e,"searchTitleByNumber",log);
		}
	}
	
	/**
	 * 操作转运单索引
	 * @param title_id 转运单号
	 * @param type		   操作类型(add,update)
	 * @throws Exception
	 */
	public void editTitleIndex(long title_id,String type) throws Exception
	{
		
		if(type.equals("add"))
		{
			TitleIndexMgr.getInstance().addIndex(title_id);
		}
		else if(type.equals("update"))
		{
			TitleIndexMgr.getInstance().updateIndex(title_id);
		}
		else if(type.equals("del"))
		{
			TitleIndexMgr.getInstance().deleteIndex(title_id);
		}
	}
	
	/**
	 * 通过用户ID获取title
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPropretarysByAdid(long adid) throws Exception
	{
		try
		{
			DBRow adminRow = floorAdminMgrGZY.getDetailAdmin(adid);
			if(GlobalKey.CLIENT_ADGID != adminRow.get("adgid", 0L))
			{
				adid = 0L;
			}
			return floorProprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(adid, null, null);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findPropretarysByAdid(long adid)",log);
		}
	}
	@Override
	public long addProprietaryTitleProductByDBRow(DBRow insertRow) throws Exception
	{
		try{
			return floorProprietaryMgrZyj.addProprietaryProduct(insertRow);
		}catch (Exception e) {
			throw new SystemException(e,"addProprietaryTitleProductByDBRow",log);
		}
	}
	
	/**
	 * 根据title_id获得title
	 * @author 詹洁
	 * @param title_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailTitleByTitleId(long title_id)
		throws Exception
	{
		try 
		{
			return floorProprietaryMgrZyj.getDetailTitleByTitleId(title_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getDetailTitleByTitleId",log);
		}
	}
	
	/**
	 * zhangrui添加
	 */
	@Override
	public DBRow findProprietaryByTitleName(String titleName) throws Exception {
		try
		{
			return floorProprietaryMgrZyj.findProprietaryByTitleName(titleName);
  		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findProprietaryByTitleName(String titleName)",log);
		}
 	}
	
	/**
	 * 通过titleID查询与title关联的产品分类
	 */
	public DBRow[] findParentProductCatagorysByTitleId(boolean adidNullToLogin, long adid, String pc_line_id, String title_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorProprietaryMgrZyj.findParentProductCatagorysByTitleId(adid, this.parseStrToLongArr(pc_line_id),this.parseStrToLongArr(title_id), pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findParentProductCatagorysByTitleId(boolean adidNullToLogin, long adid, String pc_line_id, String title_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	public int findProprietaryProductByPcidTitleID(long pc_id, long title_id)throws Exception
	{
		try
		{
			return floorProprietaryMgrZyj.findProprietaryProductByPcidTitleID(pc_id, title_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietaryProduct",log);
		}
	}
	
	
	public long addProprietaryProduct(long pc_id, long title_id) throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("tp_pc_id", pc_id);
			row.add("tp_title_id", title_id);
			int cn = floorProprietaryMgrZyj.findProprietaryProductByPcidTitleID(pc_id, title_id);
			if(cn == 0)
			{
				long id = floorProprietaryMgrZyj.addProprietaryProduct(row);
				this.editTitleIndex(title_id, "update");
				return id;
			}
			return 0;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietaryProduct",log);
		}
	}
	
	public DBRow[] getProductSnByPcId(long pc_id) throws Exception
	{
		try
		{
			return floorProprietaryMgrZyj.getProductSnByPcId(pc_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductSnByPcId(long pc_id)",log);
		}
	}
	
	public DBRow findProprietaryByCategoryId(long categoryId) throws Exception
	{
		try
		{
			DBRow row = floorProprietaryMgrZyj.findProprietaryByCategoryId(categoryId);
			return row;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addProprietaryProduct",log);
		}
	}
	
	
	/**
	 * 查询product信息
	 * @param pc_id
	 * @return
	 * @throws Exception
	 * @author Yuanxinyu
	 */
	public DBRow getProductDetailByPcId(long pc_id) throws Exception
	{
		try
		{
			DBRow row = floorProprietaryMgrZyj.getProductDetailByPcId(pc_id);
			return row;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductDetailByPcId",log);
		}
	}
	
	/**
	 * 查询product_catalog信息
	 * @param id
	 * @return
	 * @throws Exception
	 * @author Yuanxinyu
	 */
	public DBRow getProductCatalogById(long id) throws Exception
	{
		try
		{
			DBRow row = floorProprietaryMgrZyj.getProductCatalogById(id);
			return row;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductCatalogById",log);
		}
	}
	
	
	/**
	 * 查询product_catalog信息
	 * @param id
	 * @return
	 * @throws Exception
	 * @author Yuanxinyu
	 */
	public DBRow[] getProductTitleByPcId(long pc_id) throws Exception
	{
		try
		{
			return floorProprietaryMgrZyj.getProductTitleByPcId(pc_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductTitleByPcId",log);
		}
	}
	
	
	/**
	 * 根据商品和用户查询title
	 */
	public DBRow[] findTitleByAdidAndProduct(long adid, long pc_id, HttpServletRequest request) throws Exception {
		try
		{
			adid = this.findAdminIdByCondition(adid, request);
			return floorProprietaryMgrZyj.findTitlesByAdidAndProduct(adid, pc_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"findProprietaryByAdidProduct(long adid, long title_id, long pc_id, PageCtrl pc, HttpServletRequest request)",log);
		}
	}
	
	/**
	 * @param lineId
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductLinesUnExistLineId(long lineId, HttpServletRequest request) throws Exception {
		try{
			return floorProprietaryMgrZyj.findProductLinesUnExistLineId(lineId);
		}catch (Exception e){
			throw new SystemException(e,"findProductLinesUnExistLineId",log);
		}
	}
	
	/**
	 * 通过商品与title的关系重新计算产品分类、产品线与title的关系
	 * @param pc_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月22日 下午6:52:00
	 */
	 public void calculateCatelogAndLineTitle(long pc_id) throws Exception
	 {
		 try{
				floorProprietaryMgrZyj.calculateCatelogAndLineTitle(pc_id);
			}catch (Exception e){
				throw new SystemException(e,"calculateCatelogAndLineTitle",log);
			}
	 }
	 
	 	/**
	     * 查询商品归属的title
	     * @param pc_id
	     * @param adid
	     * @return
	     * @throws Exception
	     * @author:zyj
	     * @date: 2015年4月7日 下午4:32:18
	     */
	    public DBRow[] getProudctHasTitleListByPcAdmin(boolean adidNullToLogin, long pc_id, long adid, HttpServletRequest request) throws Exception
	    {
	    	try{
	  
	    		adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
				return floorProprietaryMgrZyj.getProudctHasTitleListByPcAdmin(pc_id, adid);
			}catch (Exception e){
				throw new SystemException(e,"getProudctHasTitleListByPcAdmin",log);
			}
	    }
	 
	    /**
	     * 商品不归属的title
	     * @param pc_id
	     * @param adid
	     * @return
	     * @throws Exception
	     * @author:zyj
	     * @date: 2015年4月7日 下午4:32:42
	     */
	    public DBRow[] getProudctUnHasTitleListByPcAdmin(boolean adidNullToLogin, long pc_id, long adid, HttpServletRequest request) throws Exception
	    {
	    	try{
	    		adid = this.findAdminIdByCondition(adidNullToLogin, adid, request);
				return floorProprietaryMgrZyj.getProudctUnHasTitleListByPcAdmin(pc_id, adid);
			}catch (Exception e){
				throw new SystemException(e,"getProudctUnHasTitleListByPcAdmin",log);
			}
	    }
	    
	public DBRow[] findProductByCategory(long id) throws Exception {
		
		try{
			
			return floorCatalogMgr.getProductByCategory(id);
			
		}catch (Exception e){
			throw new SystemException(e,"getProductByCategory",log);
		}
	}
	public DBRow findDetailProductByPcId(long pc_id) throws Exception {
		
		try{
			
			return floorProductMgr.getDetailProductByPcid(pc_id);
			
		}catch (Exception e){
			throw new SystemException(e,"getProductByCategory",log);
		}
	}
}
