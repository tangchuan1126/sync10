package com.cwc.app.api.zyj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zyj.FloorAndroidMgrZyj;
import com.cwc.app.iface.zj.ProductStoreMgrIFaceZJ;
import com.cwc.app.iface.zr.AndroidMgrIfaceZr;
import com.cwc.app.iface.zyj.AndroidMgrIFaceZyj;
import com.cwc.app.key.BCSKey;
import com.cwc.db.DBRow;

public class AndroidMgrZyj implements AndroidMgrIFaceZyj{

	static Logger log = Logger.getLogger("ACTION");
	private AndroidMgrIfaceZr androidMgrZr;
	private FloorAndroidMgrZyj floorAndroidMgrZyj;
	private FloorProductStoreMgr productStoreMgr;
	private ProductStoreMgrIFaceZJ productStoreMgrZJ;
	private FloorProductMgr floorProductMgr;
	
	public DBRow submitContainer(Map<String,Object> data)
		throws Exception 
	{
		long[] allContainerIds = (long[]) data.get("allContainerIds");
		for (int i = 0; i < allContainerIds.length; i++) 
		{
			Map<String,Object> containerNode = new HashMap<String, Object>();
			containerNode.put("con_id",allContainerIds[i]);
			//对临时库内原容器与商品的关系解除
			productStoreMgr.removeRelations(0l,NodeType.Container,NodeType.Product,containerNode,new HashMap<String,Object>(),new HashMap<String,Object>());
			//对临时库内容器与容器关系解除
			productStoreMgr.removeRelations(0l,NodeType.Container,NodeType.Container,containerNode,new HashMap<String,Object>(),new HashMap<String,Object>());
		}
		
		DBRow[] containerProduct =  (DBRow[])data.get("products");
		for (int i = 0; i < containerProduct.length; i++) 
		{
			Map<String,Object> containerNode = new HashMap<String, Object>();
			containerNode.put("con_id",containerProduct[i].get("cp_lp_id",0l));
			
			long pc_id = containerProduct[i].get("cp_pc_id",0l);
			DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
			HashMap mangoDBProduct = productStoreMgrZJ.productMongoDB(pc_id);
			long[] catalogs = (long[]) mangoDBProduct.get("catalogs");
			long product_line_id = Long.parseLong(mangoDBProduct.get("product_line_id").toString());
			
			Map<String,Object> productNode = new HashMap<String, Object>();
			productNode.put("pc_id",pc_id);
			productNode.put("title_id",containerProduct[i].get("title_id",0l));
			productNode.put("product_line",product_line_id);
			productNode.put("catalogs",catalogs);
			productNode.put("p_name",product.getString("p_name"));
			productNode.put("union_flag",product.get("union_flag",0));
			productStoreMgr.addNode(0,NodeType.Product,productNode);//添加商品节点
			
			Map<String,Object> relProps = new HashMap<String, Object>();
			relProps.put("quantity",containerProduct[i].get("cp_quantity",0));
			
			productStoreMgr.addRelation(0l,NodeType.Container,NodeType.Product,containerNode,productNode, relProps);
		}
		
		DBRow[] containers = (DBRow[]) data.get("containers");
		for (int i = 0; i < containers.length; i++) 
		{
			Map<String,Object> fromContainerNode = new HashMap<String, Object>();
			fromContainerNode.put("con_id",containers[i].get("parent_con_id",0l));
			
			Map<String,Object> toContainerNode = new HashMap<String, Object>();
			toContainerNode.put("con_id",containers[i].get("con_id",0l));
			
			productStoreMgr.addRelation(0,NodeType.Container,NodeType.Container,fromContainerNode,toContainerNode,new HashMap<String,Object>());
		}
		
		DBRow result = new DBRow();
		result.add("ret", BCSKey.SUCCESS);
		return result ;
	}

	public void setAndroidMgrZr(AndroidMgrIfaceZr androidMgrZr) {
		this.androidMgrZr = androidMgrZr;
	}

	public void setFloorAndroidMgrZyj(FloorAndroidMgrZyj floorAndroidMgrZyj) {
		this.floorAndroidMgrZyj = floorAndroidMgrZyj;
	}
	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

	public void setProductStoreMgrZJ(ProductStoreMgrIFaceZJ productStoreMgrZJ) {
		this.productStoreMgrZJ = productStoreMgrZJ;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

}
