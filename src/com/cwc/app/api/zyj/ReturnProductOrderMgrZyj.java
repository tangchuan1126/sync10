package com.cwc.app.api.zyj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.Cart;
import com.cwc.app.api.CartReturn;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.emailtemplate.velocity.returnproduct.ReturnReceiveEmailPageString;
import com.cwc.app.emailtemplate.velocity.returnproduct.ReturnVoiceEmailPageString;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorReturnProductMgrZJ;
import com.cwc.app.floor.api.zr.FloorCheckReturnProductItemMgrZr;
import com.cwc.app.floor.api.zr.FloorFileMgrZr;
import com.cwc.app.floor.api.zr.FloorOrderMgrZr;
import com.cwc.app.floor.api.zr.FloorReturnProductItemsFix;
import com.cwc.app.floor.api.zr.FloorReturnProductMgrZr;
import com.cwc.app.floor.api.zr.FloorReturnProductSubItemsMgrZr;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zyj.FloorReturnProductOrderMgrZyj;
import com.cwc.app.floor.api.zyj.FloorServiceOrderMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.OrderMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace;
import com.cwc.app.key.AfterServiceKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.key.ReturnProductCheckItemTypeKey;
import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.key.ReturnProductLogTypeKey;
import com.cwc.app.key.ReturnProductPictureRecognitionkey;
import com.cwc.app.key.ReturnProductSourceKey;
import com.cwc.app.key.ReturnTypeKey;
import com.cwc.app.key.ServiceOrderStatusKey;
import com.cwc.app.key.StorageReturnProductTypeKey;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.key.TransactionType;
import com.cwc.app.lucene.zyj.ReturnProductOrderIndexMgrZyj;
import com.cwc.app.lucene.zyj.ServiceOrderIndexMgrZyj;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class ReturnProductOrderMgrZyj implements ReturnProductOrderMgrZyjIFace{

	static Logger log = Logger.getLogger("ACTION");
	private FloorProductMgr floorProductMgr;
	private FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj;
	private CartReturn cartReturn;
	private FloorOrderMgr floorOrderMgr;
	private FloorReturnProductMgrZr floorReturnProductMgrZr;
	private FloorReturnProductItemsFix floorReturnProductItemsFix;
	private FloorCheckReturnProductItemMgrZr floorCheckReturnProductItemMgrZr;
	private FloorReturnProductMgrZJ floorReturnProductMgrZJ;
	private FloorServiceOrderMgrZyj floorServiceOrderMgrZyj;
	private FloorTransportMgrZr floorTransportMgrZr;
	private FloorOrderMgrZr floorOrderMgrZr;
	private OrderMgrIFace orderMgr;
	private FloorFileMgrZr floorFileMgrZr;
	private SystemConfigIFace systemConfig;
	private ProductMgr productMgr;
	private FloorReturnProductSubItemsMgrZr floorReturnProductSubItemsMgrZr;
	private FloorCatalogMgr floorCatalogMgr;
	

	/**`
	 * 将即将退货的商品放在session中
	 * @param request
	 * @throws Exception
	 */
	public void putProductToReturnCartSession(HttpServletRequest request) throws Exception
	{
		try
		{
			//套装：服务单明细ID_商品ID_套装数量
			//散件：服务单明细ID_商品ID_套装数量_散件商品ID
			String value		= StringUtil.getString(request,"value");//商品ID
			String[] valueArr	= value.split("_");
			String orderId		= valueArr[0];//单据类型及ID
			String setPcId		= valueArr[1];//套装或普通商品ID
			float set_pc_count	= Float.parseFloat(valueArr[2]);//套装商品数量
			long orderItemId	= Long.parseLong(orderId);//自身单据明细ID
			String serviceReason= StringUtil.getString(request, "serviceReason");
			
			String pc_id		= "";//商品ID
			String set_or_part_pc_id = "";//套装或散件ID
			float pc_count		= 0F;//商品数量
			int product_type	= 0;//商品类型
			HttpSession session = StringUtil.getSession(request);
			
			//如果是XXX_XXX_XXX_XXX_XXX形式，那说明选择的是套装中的散件，要从购物车中清除套装商品再加入散件商品
			if(4 == value.split("_").length)
			{
				pc_id = valueArr[3];
				set_or_part_pc_id = setPcId;
				pc_count = 1;
				this.removeReturnProductSession(session, orderItemId, setPcId, "0");//删除套装
			}
			//如果是套装或普通商品，移除其包含的所有散件，加入此商品
			else
			{
				pc_id = setPcId;
				set_or_part_pc_id = "0";
				pc_count = set_pc_count;
				DBRow[] productUnions = floorProductMgr.getProductUnionsBySetPid(Long.parseLong(setPcId));
				
				for (int i = 0; i < productUnions.length; i++) 
				{
					this.removeReturnProductSession(session, orderItemId, productUnions[i].getString("pid"), setPcId);//删除散件
				}
			}
			DBRow product = floorProductMgr.getDetailProductByPcid(Long.parseLong(pc_id));
			if (product.get("union_flag", 0)==0)
			{
				product_type = CartReturn.NORMAL;
			}
			else
			{
				product_type = CartReturn.UNION_STANDARD;
			}
			//放进去时，商品ID
			putReturnProdutToCartSession(session, product.getString("p_name"),pc_id, 1,orderItemId,set_or_part_pc_id,product_type, "", 0,pc_count, set_pc_count,-1,"",serviceReason);
			
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"putProductToReturnCart(long sid)",log);
		}
	}
	
	/**
	 * 从购物车去除商品质保原因
	 * 删除时，需要检验单据类型，单据ID，套装ID，散件ID
	 * 所删除的商品一致属于此各形式OI（WI）订单（运单）明细ID_套装商品ID_散件商品ID
	 * @param session
	 * @param pc_id
	 * @throws Exception
	 */
	private void removeReturnProductSession(HttpSession session, long order_item_id, String pc_id, String set_or_part_pc_id) throws Exception
	{
		try
		{
			if ( session.getAttribute(Config.cartReturnSession) != null )
			{
				DBRow rowOld;
				ArrayList al = (ArrayList)session.getAttribute(Config.cartReturnSession);
				
				for ( int i=0;i<al.size(); i++ )
				{
					rowOld = (DBRow)al.get(i);
					if (rowOld.get("order_item_id", 0L) == order_item_id
							&& rowOld.getString("cart_pid").equals(pc_id))
					{
						al.remove(i);
					}
				}
				
				session.setAttribute(Config.cartReturnSession,al);
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"removeReturnProductSession",log);
		}
	}
	
	/**
	 * 将商品加到购物车中
	 * @param session
	 * @param p_name
	 * @param pc_id
	 * @param quantity
	 * @param reason
	 * @param warranty_type
	 * @throws Exception
	 */
	private void putReturnProdutToCartSession(HttpSession session,String p_name,String pc_id,float quantity
			, long order_item_id, String set_or_part_pc_id, int product_type
			,String reason,int warranty_type,float count, float set_pc_count
			,int handle_result_request,String check_product_context, String return_reason)
	throws Exception
	{
		try 
		{
			//套装：服务单明细ID_商品ID_套装数量
			//散件：服务单明细ID_商品ID_套装数量_散件商品ID
			
			ArrayList<DBRow> al = null;
			
			if ( !"0".equals(pc_id) && quantity > 0)
			{
				//购物车每条记录属性
				DBRow row = new DBRow();
				row.add("order_item_id", order_item_id);//单据ID
				row.add("cart_pid",pc_id);//商品ID
				row.add("cart_quantity",quantity);//商品数量
				row.add("p_name",p_name);//商品名
				row.add("set_or_part_pc_id", set_or_part_pc_id);//与此关联的套装（散件）商品ID，可为0
				row.add("cart_total_count", count);
				row.add("set_pc_count", set_pc_count);//套装商品数量
				row.add("cart_product_type",product_type);//商品类型
				row.add("handle_result_request", handle_result_request);
				row.add("check_product_context", check_product_context);
				row.add("return_reason", return_reason);
				
				if (session.getAttribute(Config.cartReturnSession) == null )
				{
					al = new ArrayList<DBRow>();
					al.add(row);			
				}
				else
				{
					//nOrR 1：添加，2：覆盖；3：不操作
					int nOrR = 1;
					int listIndex = 0;
					al = (ArrayList)session.getAttribute(Config.cartReturnSession);
					for (int i = 0; i < al.size(); i++) 
					{
						
						//如果order_item_id都不为0，什么都比，如果有一方为0，只比较商品ID，一致
						//如果添加的有明细而已有的没有明细用新添加的替代
						if(0 != al.get(i).get("order_item_id", 0L) && 0 != order_item_id)
						{
							//验证是否重商品，单据类型、单据ID、商品ID、关联ID
							if(al.get(i).get("order_item_id", 0L) == order_item_id
									&& al.get(i).getString("cart_pid").equals(pc_id))
							{
								listIndex = i;
								nOrR = 3;
								break;
							}
						}
						else
						{
							if(al.get(i).getString("cart_pid").equals(pc_id))
							{
								if(0 != order_item_id)
								{
									listIndex = i;
									nOrR = 2;
									break;
								}
								else
								{
									nOrR = 3;
								}
							}
						}
					}
					if(1 == nOrR)
					{
						al.add(row);
					}
					else if(2 == nOrR)
					{
						al.set(listIndex,row);
					}
					
				}
				session.setAttribute(Config.cartReturnSession,al);			
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"putReturnProdutToCart",log);
		}
	}
	
	
	/**
	 * 将商品从购物车中删除
	 * @param request
	 * @throws Exception
	 */
	public void removeReturnProductSession(HttpServletRequest request) throws Exception
	{
		try
		{
			//套装：SI单据明细ID_商品ID_套装数量
			//散件：SI单据明细ID_商品ID_套装数量_散件商品ID
			String value		= StringUtil.getString(request,"value");//商品ID
			String[] valueArr	= value.split("_");
			String typeId		= valueArr[0];//单据类型及ID
			String setPcId		= valueArr[1];//套装或普通商品ID
			long orderItemId	= Long.parseLong(typeId);
			HttpSession session = StringUtil.getSession(request);
			
			//如果是XXX_XXX_XXX_XXX_XXX形式，那说明选择的是套装中的散件
			if(4 == value.split("_").length)
			{
				this.removeReturnProductSession(session, orderItemId, valueArr[3], setPcId);//删除散件
			}
			//如果是套装或普通商品，移除其包含的所有散件，加入此商品
			else
			{
				this.removeReturnProductSession(session, orderItemId, setPcId, "0");//删除套装
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"removeReturnProduct(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 批量修改商品数量
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	public void modReturnProductQuantitySession(HttpServletRequest request)throws  Exception
	{
		try
		{
			HttpSession session				= StringUtil.getSession(request);
			String[] pids					= request.getParameterValues("pids");
			String[] quantitys				= request.getParameterValues("quantitys");
			String[] handle_result_requests = request.getParameterValues("handle_result_request");
			String[] order_item_ids			= request.getParameterValues("order_item_id");
			String[] set_or_part_pc_ids		= request.getParameterValues("set_or_part_pc_id");
			String[] return_reasons			= request.getParameterValues("return_reason");
			String[] check_product_contexts	= request.getParameterValues("check_product_context");
			
			if ( session.getAttribute(Config.cartReturnSession) != null )
			{
				ArrayList al = (ArrayList)session.getAttribute(Config.cartReturnSession);
				for (int i=0; i<pids.length; i++)
				{
					float quantity				= StringUtil.getFloat(quantitys[i]);
					int handle_result_request	= Integer.parseInt(handle_result_requests[i]);
					String check_product_context= check_product_contexts[i];
					for (int j=0; j<al.size(); j++)
					{
						DBRow product = (DBRow)al.get(j);
						//找到对应的商品记录进行数量修改
						if(product.getString("order_item_id").equals(order_item_ids[i])
								&& product.getString("cart_pid").equals(pids[i]))
						{
							if ( quantity<=0 )
							{
								quantity = 1;
							}
							product.add("cart_quantity",quantity);
							product.add("handle_result_request",handle_result_request);
							product.add("check_product_context",check_product_context);
							product.add("return_reason", return_reasons[i]);
						}
						al.set(j,product);
					}
				}

				session.setAttribute(Config.cartReturnSession,al);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modReturnProductQuantitySession(HttpServletRequest request)",log);
		}
		
	}
	
	
	/**
	 * 添加商品进退货单
	 */
	public void putToReturnProductCartForAdd(HttpServletRequest request) throws Exception
	{
		try 
		{
			HttpSession session = StringUtil.getSession(request);
			String value		= StringUtil.getString(request, "value");
			//通过商品名获取商品ID
			String p_name		= value.split("_")[0];
			float count			= Float.parseFloat(value.split("_")[1]);
			DBRow product = floorProductMgr.getDetailProductByPname(p_name);
			if(product==null)
			{
				throw new ProductNotExistException();
			}
			String pc_id = product.getString("pc_id");
			int product_type;
			if (product.get("union_flag", 0)==0)
			{
				product_type = CartReturn.NORMAL;
			}
			else
			{
				product_type = CartReturn.UNION_STANDARD;
			}
			//删除重复商品
			this.removeReturnProductSession(session, 0, String.valueOf(pc_id), "0");
			
			//将商品质保放入质保明细购物车
			this.putReturnProdutToCartSession(session, p_name, pc_id, count, 0L, "0", product_type, "", 0, count, 0F, -1, "","");
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"putToReturnProductCartForAdd(HttpServletRequest request)",log);
		}
	}
	
	/**
	 * 通过服务单ID获取退货单
	 */
	@Override
	public DBRow[] getReturnOrderBySid(long sid) throws Exception 
	{
		try 
		{
			return floorReturnProductOrderMgrZyj.getReturnOrderBySid(sid);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"getReturnOrderBySid(sid)",log);
		}
	}
	
	/**
	 * 保存退货单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addReturnOrder(HttpServletRequest request) throws Exception
	{
		try 
		{
			//虚拟退货与需要退货的区别，文件及状态
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid		= adminLoggerBean.getAdid();
			String note		= StringUtil.getString(request, "note");
			long sid		= StringUtil.getLong(request,"sid");
			long oid		= StringUtil.getLong(request,"oid");
			long ps_id		= StringUtil.getLong(request, "ps_id");
			int return_product_flag	= StringUtil.getInt(request, "return_product_flag");
			DBRow porder	= floorOrderMgr.getDetailPOrderByOid(oid);
			//修改服务单
			DBRow serviceRow = new DBRow();
			serviceRow.add("is_need_return", return_product_flag);
			DBRow service	= floorServiceOrderMgrZyj.getServiceOrderDetailBySid(sid);
			int status		= service.get("status", 8);
			if(ReturnTypeKey.NEED == return_product_flag)
			{
				//状态为等待处理、虚拟退货、无需退货时才可以编辑状态为等待退货
				if(ServiceOrderStatusKey.WAITING == status || ServiceOrderStatusKey.VIRTUAL_RETURN == status
						 || ServiceOrderStatusKey.NOT_NEED_RETURN == status)
				{
					serviceRow.add("status", ServiceOrderStatusKey.WAITING_RETURN);
				}
			}
			else
			{
				serviceRow.add("status", ServiceOrderStatusKey.VIRTUAL_RETURN);
			}
			
			floorServiceOrderMgrZyj.updateServiceOrder(serviceRow, sid);
			//获取地址信息
			/*	
		 	String first_name		= StrUtil.getString(request, "first_name");
			String last_name		= StrUtil.getString(request, "last_name");
			long ccid				= StrUtil.getLong(request, "ccid");				//国家ID
			String address_country	= StrUtil.getString(request, "address_country");	//国家名
			String address_country_code = StrUtil.getString(request, "address_country_code");//国家code
			long pro_id				= StrUtil.getLong(request, "pro_id");			//省份ID
			String address_state	= StrUtil.getString(request, "address_state");	//省份字符串   
			String address_city		= StrUtil.getString(request, "address_city");	//城市
			String address_street	= StrUtil.getString(request, "address_street");	//街道
			String address_zip		= StrUtil.getString(request, "address_zip");		//邮编
			String tel				= StrUtil.getString(request, "tel");				//电话
			*/
			String first_name			= porder.getString("first_name");
			String last_name			= porder.getString("last_name");
			long ccid					= porder.get("ccid",0L);				//国家ID
			String address_country		= porder.getString("address_country");	//国家名
			String address_country_code = porder.getString("address_country_code");//国家code
			long pro_id					= porder.get("pro_id",0L);			//省份ID
			String address_state		= porder.getString("address_state");	//省份字符串   
			String address_city			= porder.getString("address_city");	//城市
			String address_street		= porder.getString("address_street");	//街道
			String address_zip			= porder.getString("address_zip");		//邮编
			String tel					= porder.getString("tel");				//电话
			int rp_status				= 0;
			
			int is_send_mail		= StringUtil.getInt(request, "is_send_mail");		//是否需要发送邮件
			
			//创建退货单
			DBRow returnRow = new DBRow();
			returnRow.add("oid", oid);
			returnRow.add("sid", sid);
			if(ReturnTypeKey.NEED == return_product_flag)
			{
				rp_status = ReturnProductKey.WAITINGPRODUCT;
			}
			else
			{
				rp_status = ReturnProductKey.WAITING;
				
			}
			returnRow.add("create_user", adid);
			returnRow.add("create_date", DateUtil.NowStr());
			returnRow.add("note", note);
			returnRow.add("return_product_source", ReturnProductSourceKey.CUSTOMER_POST_BACK);//客户来源：客户退回
			returnRow.add("picture_recognition", ReturnProductPictureRecognitionkey.RecognitionNoneFinish);//图片识别未完成
			returnRow.add("return_product_type", StorageReturnProductTypeKey.HAS_PACK);//商品包装：有
			returnRow.add("gather_picture_status", 0);//商品采集未完成
			returnRow.add("ps_id", ps_id);
			returnRow.add("is_need_return", return_product_flag);
			returnRow.add("create_type", 2);//客服创建
			returnRow.add("status", rp_status);
			
			returnRow.add("first_name", first_name);
			returnRow.add("last_name", last_name);
			returnRow.add("address_country", address_country);
			returnRow.add("address_city", address_city);
			returnRow.add("address_state", address_state);
			returnRow.add("address_zip", address_zip);
			returnRow.add("address_country_code", address_country_code);
			returnRow.add("tel", tel);
			returnRow.add("ccid", ccid);
			returnRow.add("pro_id", pro_id);
			returnRow.add("address_street", address_street);
			
			returnRow.add("is_send_mail", is_send_mail);
			long rp_id		= floorReturnProductMgrZr.addReturnProduct(returnRow);
			
			ReturnProductOrderIndexMgrZyj.getInstance().addIndex(rp_id);
			
			//创建退货明细
			this.addReturnProducts(request,false,rp_id,adid,oid);
			cartReturn.clearCart(StringUtil.getSession(request));
			
			
			DBRow productStoreCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(ps_id);
			//增加退货单日志
			this.addReturnProductLog(rp_id, 0,0,0,rp_status, "创建退货单:["+rp_id+"],"+new ReturnTypeKey().getReturnTypeKeyValue(return_product_flag)+",退货仓库:"+(null!=productStoreCatalog?productStoreCatalog.getString("title"):""), ReturnProductLogTypeKey.CREATE, adid, DateUtil.NowStr());
			
			//修改订单据状态
			DBRow order = new DBRow();
			order.add("after_service_status", AfterServiceKey.NORMAL_WARRANTYING);
			floorOrderMgr.modPOrder(oid, order);
			
			//创建交易
			DBRow trade = new DBRow();
			trade.add("client_id",porder.getString("client_id"));
			trade.add("relation_type",0);
			trade.add("relation_id",oid);
			trade.add("parent_txn_id ",porder.getString("txn_id"));
			trade.add("is_create_order",0);
			trade.add("transaction_type",TransactionType.Warranty);
			trade.add("delivery_address_name",porder.getString("address_name"));
			trade.add("delivery_address_street",porder.getString("address_street"));
			trade.add("delivery_address_city",porder.getString("address_city"));
			trade.add("delivery_address_state",porder.getString("address_state"));
			trade.add("delivery_address_zip",porder.getString("address_zip"));
			trade.add("delivery_address_country_code",porder.getString("address_country_code"));
			trade.add("delivery_address_country",porder.getString("address_country"));
			trade.add("delivery_pro_id",porder.get("pro_id",0l));
			trade.add("delivery_ccid_id",porder.get("ccid",0l));
			trade.add("post_date", DateUtil.NowStr());
//			trade.add("rp_id", rp_id);
			
			floorOrderMgrZr.insertTrade(trade);
			
			orderMgr.validateTraceService(adminLoggerBean.getAdgid(), oid, TracingOrderKey.NORMAL_WARRANTYING);
			
			//加跟进
			DBRow noteRow = new DBRow();
			noteRow.add("oid", oid);
			noteRow.add("note", "创建退货 退货单号:"+rp_id);
			noteRow.add("account", adminLoggerBean.getAccount());
			noteRow.add("adid", adminLoggerBean.getAdid());
			noteRow.add("trace_type", TracingOrderKey.NORMAL_WARRANTYING);
			noteRow.add("post_date", DateUtil.NowStr());
			noteRow.add("rel_id", rp_id);
			noteRow.add("trace_child_type",porder.get("internal_tracking_status",0));
			floorOrderMgr.addPOrderNote(noteRow);
			
			ServiceOrderIndexMgrZyj.getInstance().updateIndex(sid);
			
			if(1 == is_send_mail && ReturnTypeKey.NEED == return_product_flag)
			{
				//发送邮件通知顾客
				ReturnVoiceEmailPageString returnVoiceEmailPageString = new ReturnVoiceEmailPageString(rp_id,oid);
				User user = new User(returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getPassWord(),returnVoiceEmailPageString.getHost(),returnVoiceEmailPageString.getPort(),returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getAuthor(),returnVoiceEmailPageString.getRePly());
				MailAddress mailAddress = new MailAddress(porder.getString("client_id"));
				MailBody mailBody = new MailBody(returnVoiceEmailPageString,true,null);
				
				Mail mail = new Mail(user);
				mail.setAddress(mailAddress);
				mail.setMailBody(mailBody);
				mail.send();
			}
			if(ReturnTypeKey.VIRTUAL == return_product_flag)
			{
				this.uploadServiceVirtualFiles(rp_id, adid, request);
			}
			return (rp_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addReturnOrder(sid)",log);
		}
	}
	
	/**
	 * 保存退货明细
	 * @param request
	 * @param flag
	 * @param rp_id
	 * @param adid
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public long addReturnProducts(HttpServletRequest request,boolean flag,long rp_id, long adid,long oid) throws Exception
	{
		try
		{
			/**
			 * 插入散件的时候，先插入到数据池，如果有重复商品，则累加数量
			 * pid做主键
			 */
			HashMap<String, DBRow> tmpDataPool = new HashMap<String, DBRow>();
			
			//先清空原来的退货商品记录
//			floorReturnProductMgrZJ.delReturnProductItemsByRpId(rp_id);
//			floorReturnProductMgrZJ.delReturnProductSubItemsByRpId(rp_id);
			
			//插入退货主商品
			cartReturn.flush(StringUtil.getSession(request));
			DBRow returnProducts[] = cartReturn.getDetailProducts();
			boolean isUpdateReturnOrderNeedCheck = false;
			for (int i=0; i<returnProducts.length; i++)
			{
				DBRow product			= floorProductMgr.getDetailProductByPcid(returnProducts[i].get("cart_pid",0L));
				DBRow returnProduct = new DBRow();
				returnProduct.add("p_name", returnProducts[i].getString("p_name"));
				returnProduct.add("pid", returnProducts[i].getString("cart_pid"));
				returnProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
				returnProduct.add("unit_name", null!=product?product.getString("unit_name"):"");
				returnProduct.add("product_type", returnProducts[i].getString("cart_product_type"));
				returnProduct.add("rp_id", rp_id);
				returnProduct.add("is_merge_item", 0);
				returnProduct.add("is_receive_item", 0);
				returnProduct.add("handle_result_request", returnProducts[i].getString("handle_result_request"));
				long order_item_id = returnProducts[i].get("order_item_id", 0L);
				returnProduct.add("siid", order_item_id);
				returnProduct.add("return_reason", returnProducts[i].getString("return_reason"));
				if(null != returnProducts[i].getString("check_product_context") && !"".equals(returnProducts[i].getString("check_product_context")))
				{
					returnProduct.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
				}
				long rpi_id = floorReturnProductItemsFix.addReturnProductItem(returnProduct);
				
				//如果check_product_context不为空，需要创建退货检查明细
				if(null != returnProducts[i].getString("check_product_context") && !"".equals(returnProducts[i].getString("check_product_context")))
				{
					DBRow returnCheck = new DBRow();
					returnCheck.add("rp_id", rp_id);
					returnCheck.add("rpi_id", rpi_id);
					returnCheck.add("create_adid", adid);
					returnCheck.add("create_date", DateUtil.NowStr());
					returnCheck.add("check_product_context", returnProducts[i].getString("check_product_context"));
					returnCheck.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
					
					floorCheckReturnProductItemMgrZr.addCheckReturnProductItem(returnCheck);
					//增加退货单日志
					DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
					floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,rpi_id,0,0, returnRow.get("status", 0), "退货单["+rp_id+"]添加商品测试", ReturnProductLogTypeKey.CHECK, adid, DateUtil.NowStr());
					
					if(!isUpdateReturnOrderNeedCheck)
					{
						isUpdateReturnOrderNeedCheck = true;
						//如果有一个商品需要测试，则退货单需要测试
						DBRow returnProductInfo = new DBRow();
						returnProductInfo.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
						floorReturnProductMgrZr.updateReturnProduct(returnProductInfo, rp_id);
					}
				}
				
				//分解套装成散件returnProducts[i].getString("cart_product_type").equals(String.valueOf(ProductTypeKey.UNION_STANDARD))
				if (1 == product.get("union_flag", 0))
				{
					DBRow returnSubProducts[] = floorProductMgr.getProductsInSetBySetPid(returnProducts[i].get("pc_id", 0l));
					for (int j=0; j<returnSubProducts.length; j++)
					{
						if (tmpDataPool.containsKey(returnSubProducts[j].getString("pc_id")+"_"+order_item_id))
						{
							DBRow returnSubProduct =  tmpDataPool.get(returnSubProducts[j].getString("pc_id")+"_"+order_item_id);
							returnSubProduct.add("quantity", StringUtil.getFloat(returnSubProduct.getString("quantity"))+returnSubProducts[j].get("quantity", 0f)*StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
							tmpDataPool.put(returnSubProducts[j].getString("pc_id")+"_"+order_item_id, returnSubProduct);
						}
						else
						{
							DBRow returnSubProduct = new DBRow();
							returnSubProduct.add("p_name", returnSubProducts[j].getString("p_name"));
							returnSubProduct.add("pid", returnSubProducts[j].getString("pc_id"));
							returnSubProduct.add("quantity", returnSubProducts[j].get("quantity", 0f)*StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
							returnSubProduct.add("unit_name", returnSubProducts[j].getString("unit_name"));
							returnSubProduct.add("rpi_id", rpi_id);
							
							tmpDataPool.put(returnSubProducts[j].getString("pc_id")+"_"+order_item_id, returnSubProduct);
						}
					}
				}
				else
				{
					//普通商品直接插入
					if (tmpDataPool.containsKey(returnProducts[i].getString("cart_pid")+"_"+order_item_id))
					{
						DBRow returnSubProduct =  tmpDataPool.get(returnProducts[i].getString("cart_pid")+"_"+order_item_id);
						returnSubProduct.add("quantity", StringUtil.getFloat(returnSubProduct.getString("quantity"))+StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
						tmpDataPool.put(returnProducts[i].getString("cart_pid")+"_"+order_item_id, returnSubProduct);
					}
					else
					{
						DBRow returnSubProduct = new DBRow();
						returnSubProduct.add("p_name", returnProducts[i].getString("p_name"));
						returnSubProduct.add("pid", returnProducts[i].getString("cart_pid"));
						returnSubProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
						returnSubProduct.add("unit_name", returnProducts[i].getString("unit_name"));
						returnSubProduct.add("rpi_id", rpi_id);
						
						tmpDataPool.put(returnProducts[i].getString("cart_pid")+"_"+order_item_id, returnSubProduct);
					}
				}
				
			}
	
			//最后插入散件
			Set<String> keys = tmpDataPool.keySet();   
			for(String key:keys)
			{
				floorReturnProductMgrZJ.addReturnProductSubItems(tmpDataPool.get(key));
			}
	
			//清空退货购物车
			cartReturn.clearCart(StringUtil.getSession(request));
			
			//发送邮件通知顾客,当正常质保的时候则不直接发送邮件给客户
			if(flag)
			{
				DBRow detailOrder = floorOrderMgr.getDetailPOrderByOid(oid);
				
				ReturnVoiceEmailPageString returnVoiceEmailPageString = new ReturnVoiceEmailPageString(rp_id,oid);
				User user = new User(returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getPassWord(),returnVoiceEmailPageString.getHost(),returnVoiceEmailPageString.getPort(),returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getAuthor(),returnVoiceEmailPageString.getRePly());
	            MailAddress mailAddress = new MailAddress(detailOrder.getString("client_id"));
	            MailBody mailBody = new MailBody(returnVoiceEmailPageString,true,null);
	
	            Mail mail = new Mail(user);
	            mail.setAddress(mailAddress);
	            mail.setMailBody(mailBody);
	            mail.send();
			}
			return(rp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addReturnProducts",log);
		}
	}
	
	/**
	 * 保存退货明细
	 * @param request
	 * @param flag
	 * @param rp_id
	 * @param adid
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public void updateReturnProducts(HttpServletRequest request,boolean flag,long rp_id, long adid,long oid) throws Exception
	{
		try
		{
			DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			//将所有的商品的siid与pid组成siid_pid放在set中
			cartReturn.flush(StringUtil.getSession(request));
			DBRow returnProducts[] = cartReturn.getDetailProducts();
			Set<String> siidAndPidSet = new HashSet<String>();
			for (int i=0; i<returnProducts.length; i++)
			{
				long pid	= returnProducts[i].get("cart_pid", 0L);
				long siid	= returnProducts[i].get("order_item_id", 0L);
				siidAndPidSet.add(siid+"_"+pid);
			}
			//查询出所有的returnItems，数据siid_pid与rpi_id，放在set中
			DBRow[] returnSiidAndPids = floorReturnProductOrderMgrZyj.getReturnItemsSiidAndPidByRpId(rp_id);
			for (int i = 0; i < returnSiidAndPids.length; i++) 
			{
				String siid_pid = returnSiidAndPids[i].get("siid", 0L)+"_"+returnSiidAndPids[i].get("pid", 0L);
				//如果能够放进去，说明，更新的数据已无此记录，该记录将被删除
				if(siidAndPidSet.add(siid_pid))
				{
					//删除子明细及明细
					floorReturnProductOrderMgrZyj.deleteReturnSubItemsByRpiId(returnSiidAndPids[i].get("rpi_id", 0L));
					floorReturnProductOrderMgrZyj.deleteReturnItemsByRpiId(returnSiidAndPids[i].get("rpi_id", 0L));
					floorReturnProductOrderMgrZyj.deleteReturnProductCheckByRpiId(returnSiidAndPids[i].get("rpi_id", 0L));
				}
			}
			//遍历session中的数据，在数据库中能够查到，更新明细及子明细；不能查到添加明细及子明细
			for (int i = 0; i < returnProducts.length; i++) 
			{
				long pid	= returnProducts[i].get("cart_pid", 0L);
				long siid	= returnProducts[i].get("order_item_id", 0L);
				DBRow[] reutrnItemsBySiidPid = floorReturnProductOrderMgrZyj.getReturnItemsBySiidPidRpId(rp_id, pid, siid);
				//能够查到更新明细及子明细、更新或者创建测试,reutrnItemsBySiidPid没有问题的话，只会有一条
				if(reutrnItemsBySiidPid.length > 0)
				{
					DBRow product			= floorProductMgr.getDetailProductByPcid(returnProducts[i].get("cart_pid",0L));
					DBRow returnProduct = new DBRow();
					returnProduct.add("p_name", returnProducts[i].getString("p_name"));
//					returnProduct.add("pid", returnProducts[i].getString("cart_pid"));
					returnProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
					returnProduct.add("unit_name", null!=product?product.getString("unit_name"):"");
					returnProduct.add("product_type", returnProducts[i].getString("cart_product_type"));
//					returnProduct.add("rp_id", rp_id);
//					returnProduct.add("is_merge_item", 0);
//					returnProduct.add("is_receive_item", 0);
					returnProduct.add("handle_result_request", returnProducts[i].getString("handle_result_request"));
//					long order_item_id = returnProducts[i].get("order_item_id", 0L);
//					returnProduct.add("siid", order_item_id);
					returnProduct.add("return_reason", returnProducts[i].getString("return_reason"));
					if(null != returnProducts[i].getString("check_product_context") && !"".equals(returnProducts[i].getString("check_product_context")))
					{
						returnProduct.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
					}
					else
					{
						returnProduct.add("return_product_check", ReturnProductCheckItemTypeKey.OUTCHECK);
					}
					floorReturnProductItemsFix.updateReturnProductItem(returnProduct, reutrnItemsBySiidPid[0].get("rpi_id",0L));
					
					//通过rpi_id查询检查
					DBRow[] returnProductCheck = floorReturnProductOrderMgrZyj.getReturnProductCheckByRpiId(reutrnItemsBySiidPid[0].get("rpi_id",0L));
					
					//如果check_product_context不为空，需要创建退货检查明细
					if(null != returnProducts[i].getString("check_product_context") && !"".equals(returnProducts[i].getString("check_product_context")))
					{
						DBRow returnCheck = new DBRow();
						//之前无需检查，现在需要检查，创建
						if(0 == returnProductCheck.length)
						{
							returnCheck.add("rp_id", rp_id);
							returnCheck.add("rpi_id", reutrnItemsBySiidPid[0].get("rpi_id",0L));
							returnCheck.add("create_adid", adid);
							returnCheck.add("create_date", DateUtil.NowStr());
							returnCheck.add("check_product_context", returnProducts[i].getString("check_product_context"));
							returnCheck.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
							floorCheckReturnProductItemMgrZr.addCheckReturnProductItem(returnCheck);
							//增加退货单日志
							floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,reutrnItemsBySiidPid[0].get("rpi_id",0L),0,0, returnRow.get("status", 0), "退货单["+rp_id+"]添加商品测试", ReturnProductLogTypeKey.CHECK, adid, DateUtil.NowStr());
						}
						//之前需要，现在需要，更新检查内容及标识
						else
						{
							returnCheck.add("check_product_context", returnProducts[i].getString("check_product_context"));
							returnCheck.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
							floorReturnProductOrderMgrZyj.updateReturnProductCheckByCheckId(returnCheck, returnProductCheck[0].get("check_return_product_id", 0L));
							//增加退货单日志
							floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,reutrnItemsBySiidPid[0].get("rpi_id",0L),0,0, returnRow.get("status", 0), "退货单["+rp_id+"]更新商品测试", ReturnProductLogTypeKey.CHECK, adid, DateUtil.NowStr());
						}
					}
					//之前需要，现在不需要，更新为不需要
					else
					{
						if(returnProductCheck.length > 0)
						{
							floorReturnProductOrderMgrZyj.deleteReturnProductCheckByCheckId(returnProductCheck[0].get("check_return_product_id", 0L));
							//增加退货单日志
							floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,reutrnItemsBySiidPid[0].get("rpi_id",0L),0,0, returnRow.get("status", 0), "退货单["+rp_id+"]取消商品测试", ReturnProductLogTypeKey.CHECK, adid, DateUtil.NowStr());
						}
					}
					//根据退货单明细查询子明细
					DBRow[] returnSubItems = floorReturnProductOrderMgrZyj.getReturnSubItemsByRpiId(reutrnItemsBySiidPid[0].get("rpi_id",0L)); 
//					for (int j = 0; j < returnSubItems.length; j++) 
//					{
						//更新子明细，如果没有创建，如果有更新
						if (1 == product.get("union_flag", 0))
						{
							DBRow unionProducts[] = floorProductMgr.getProductsInSetBySetPid(returnProducts[i].get("pc_id", 0l));
							//套装下有子商品，添加子商品
							if(unionProducts.length > 0)
							{
								for (int m = 0; m < unionProducts.length; m ++)
								{
									DBRow returnSubProduct = new DBRow();
									DBRow[] returnSubItem = floorReturnProductOrderMgrZyj.getReturnSubItemsByRpiIdAndPid(reutrnItemsBySiidPid[0].get("rpi_id",0L), unionProducts[m].get("pid", 0L));
									if(returnSubItem.length > 0)
									{
										returnSubProduct.add("p_name", unionProducts[m].getString("p_name"));
										returnSubProduct.add("quantity", unionProducts[m].get("quantity", 0f)*StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
										returnSubProduct.add("unit_name", unionProducts[m].getString("unit_name"));
										floorReturnProductOrderMgrZyj.updateReturnSubItemByRpsiId(returnSubProduct, returnSubItems[m].get("rpsi_id", 0L));
									}
									else
									{
										returnSubProduct.add("p_name", unionProducts[m].getString("p_name"));
										returnSubProduct.add("pid", unionProducts[m].getString("pc_id"));
										returnSubProduct.add("quantity", unionProducts[m].get("quantity", 0f)*StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
										returnSubProduct.add("unit_name", unionProducts[m].getString("unit_name"));
										returnSubProduct.add("rpi_id", reutrnItemsBySiidPid[0].get("rpi_id",0L));
										floorReturnProductMgrZJ.addReturnProductSubItems(returnSubProduct);
									}
								}
							}
							//套装下无子商品，添加本商品
							else
							{
								DBRow returnSubProduct = new DBRow();
								returnSubProduct.add("p_name", returnProducts[i].getString("p_name"));
								returnSubProduct.add("pid", returnProducts[i].getString("cart_pid"));
								returnSubProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
								returnSubProduct.add("unit_name", returnProducts[i].getString("unit_name"));
								returnSubProduct.add("rpi_id", reutrnItemsBySiidPid[0].get("rpi_id",0L));
								floorReturnProductMgrZJ.addReturnProductSubItems(returnSubProduct);
							}
						}
						else
						{
							DBRow returnSubProduct = new DBRow();
							DBRow[] returnSubItem = floorReturnProductOrderMgrZyj.getReturnSubItemsByRpiIdAndPid(reutrnItemsBySiidPid[0].get("rpi_id",0L), returnProducts[i].get("cart_pid",0L));
							if(returnSubItem.length > 0)
							{
								returnSubProduct.add("p_name", returnProducts[i].getString("p_name"));
//								returnSubProduct.add("pid", returnProducts[i].getString("cart_pid"));
								returnSubProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
								returnSubProduct.add("unit_name", returnProducts[i].getString("unit_name"));
//								returnSubProduct.add("rpi_id", reutrnItemsBySiidPid[0].get("rpi_id",0L));
								floorReturnProductOrderMgrZyj.updateReturnSubItemByRpsiId(returnSubProduct, returnSubItems[0].get("rpsi_id", 0L));
							}
							else
							{
								returnSubProduct.add("p_name", returnProducts[i].getString("p_name"));
								returnSubProduct.add("pid", returnProducts[i].getString("cart_pid"));
								returnSubProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
								returnSubProduct.add("unit_name", returnProducts[i].getString("unit_name"));
								returnSubProduct.add("rpi_id", reutrnItemsBySiidPid[0].get("rpi_id",0L));
								floorReturnProductMgrZJ.addReturnProductSubItems(returnSubProduct);
							}
						}
//					}
					
				}
				//不能查到添加明细及子明细，如果需要测试创建测试
				else
				{
					DBRow product		= floorProductMgr.getDetailProductByPcid(returnProducts[i].get("cart_pid",0L));
					DBRow returnProduct = new DBRow();
					returnProduct.add("p_name", returnProducts[i].getString("p_name"));
					returnProduct.add("pid", returnProducts[i].getString("cart_pid"));
					returnProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
					returnProduct.add("unit_name", null!=product?product.getString("unit_name"):"");
					returnProduct.add("product_type", returnProducts[i].getString("cart_product_type"));
					returnProduct.add("rp_id", rp_id);
					returnProduct.add("is_merge_item", 0);
					returnProduct.add("is_receive_item", 0);
					returnProduct.add("handle_result_request", returnProducts[i].getString("handle_result_request"));
					long order_item_id = returnProducts[i].get("order_item_id", 0L);
					returnProduct.add("siid", order_item_id);
					returnProduct.add("return_reason", returnProducts[i].getString("return_reason"));
					long rpi_id = floorReturnProductItemsFix.addReturnProductItem(returnProduct);
					
					//如果check_product_context不为空，需要创建退货检查明细
					if(null != returnProducts[i].getString("check_product_context") && !"".equals(returnProducts[i].getString("check_product_context")))
					{
						DBRow returnCheck = new DBRow();
						returnCheck.add("rp_id", rp_id);
						returnCheck.add("rpi_id", rpi_id);
						returnCheck.add("create_adid", adid);
						returnCheck.add("create_date", DateUtil.NowStr());
						returnCheck.add("check_product_context", returnProducts[i].getString("check_product_context"));
						returnCheck.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
						floorCheckReturnProductItemMgrZr.addCheckReturnProductItem(returnCheck);
						//增加退货单日志
						floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id,0,0,0, returnRow.get("status", 0), "退货单["+rp_id+"]添加商品测试", ReturnProductLogTypeKey.CHECK, adid, DateUtil.NowStr());
					}
					//分解套装成散件returnProducts[i].getString("cart_product_type").equals(String.valueOf(ProductTypeKey.UNION_STANDARD))
					if (1 == product.get("union_flag", 0))
					{
						DBRow unionProducts[] = floorProductMgr.getProductsInSetBySetPid(returnProducts[i].get("pc_id", 0l));
						//能够查询到子商品添加子商品
						if(unionProducts.length > 0)
						{
							for (int j=0; j<unionProducts.length; j++)
							{
									DBRow returnSubProduct = new DBRow();
									returnSubProduct.add("p_name", unionProducts[j].getString("p_name"));
									returnSubProduct.add("pid", unionProducts[j].getString("pc_id"));
									returnSubProduct.add("quantity", unionProducts[j].get("quantity", 0f)*StringUtil.getFloat(returnProducts[i].getString("cart_quantity")));
									returnSubProduct.add("unit_name", unionProducts[j].getString("unit_name"));
									returnSubProduct.add("rpi_id", rpi_id);
									floorReturnProductMgrZJ.addReturnProductSubItems(returnSubProduct);
							}
						}
						//不能查询到子商品添加本商品
						else
						{
							DBRow returnSubProduct = new DBRow();
							returnSubProduct.add("p_name", returnProducts[i].getString("p_name"));
							returnSubProduct.add("pid", returnProducts[i].getString("cart_pid"));
							returnSubProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
							returnSubProduct.add("unit_name", returnProducts[i].getString("unit_name"));
							returnSubProduct.add("rpi_id", rpi_id);
							floorReturnProductMgrZJ.addReturnProductSubItems(returnSubProduct);
						}
					}
					else
					{
							DBRow returnSubProduct = new DBRow();
							returnSubProduct.add("p_name", returnProducts[i].getString("p_name"));
							returnSubProduct.add("pid", returnProducts[i].getString("cart_pid"));
							returnSubProduct.add("quantity", returnProducts[i].getString("cart_quantity"));
							returnSubProduct.add("unit_name", returnProducts[i].getString("unit_name"));
							returnSubProduct.add("rpi_id", rpi_id);
							floorReturnProductMgrZJ.addReturnProductSubItems(returnSubProduct);
					}
				}
			}
			//如果有一个商品需要测试，则退货单需要测试
			if(floorReturnProductOrderMgrZyj.getReturnProductCheckByRpId(rp_id).length > 0)
			{
				DBRow returnProductInfo = new DBRow();
				returnProductInfo.add("return_product_check", ReturnProductCheckItemTypeKey.CHECK);
				floorReturnProductMgrZr.updateReturnProduct(returnProductInfo, rp_id);
			}
			
			//清空退货购物车
			cartReturn.clearCart(StringUtil.getSession(request));
			
			//发送邮件通知顾客,当正常质保的时候则不直接发送邮件给客户
			if(flag)
			{
				DBRow detailOrder = floorOrderMgr.getDetailPOrderByOid(oid);
				
				ReturnVoiceEmailPageString returnVoiceEmailPageString = new ReturnVoiceEmailPageString(rp_id,oid);
				User user = new User(returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getPassWord(),returnVoiceEmailPageString.getHost(),returnVoiceEmailPageString.getPort(),returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getAuthor(),returnVoiceEmailPageString.getRePly());
	            MailAddress mailAddress = new MailAddress(detailOrder.getString("client_id"));
	            MailBody mailBody = new MailBody(returnVoiceEmailPageString,true,null);
	
	            Mail mail = new Mail(user);
	            mail.setAddress(mailAddress);
	            mail.setMailBody(mailBody);
	            mail.send();
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"updateReturnProducts(HttpServletRequest request,boolean flag,long rp_id, long adid,long oid)",log);
		}
	}

	/**
	 * 将退货单明细上的商品加载到session中
	 * @param request
	 * @throws Exception
	 */
	public void loadReturnProductToSesssion(HttpServletRequest request) throws Exception
	{
		try
		{
			HttpSession session		= StringUtil.getSession(request);
			long rp_id				= StringUtil.getLong(request, "rp_id");
			DBRow[] returnItems		= floorReturnProductItemsFix.getReturnProductItemByRpId(rp_id);
			for (int i = 0; i < returnItems.length; i++) 
			{
				DBRow returnItem			= new DBRow();
				long rpi_id					= returnItems[i].get("rpi_id", 0L);
				String pc_id				= returnItems[i].getString("pid");
				String p_name				= returnItems[i].getString("p_name");
				float quantity				= returnItems[i].get("quantity", 0F);
				int handle_result_request	= returnItems[i].get("handle_result_request", 0);
				long siid					= returnItems[i].get("siid", 0L);
				String return_reason		= returnItems[i].getString("return_reason");
				DBRow[] returnCheckItems	= floorReturnProductItemsFix.getCheckReturnProductItem(rpi_id);
				String check_product_context = "";
				if(returnCheckItems.length > 0)
				{
					check_product_context = returnCheckItems[0].getString("check_product_context");
					returnItem.add("check_product_context", check_product_context);
				}
				
				this.putReturnProdutToCartSession(session, p_name, pc_id, quantity, siid, "0", 0 , "", 0, quantity, 0f,
						handle_result_request,check_product_context, return_reason);
				
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addReturnProducts",log);
		}
	}
	
	/**
	 * 修改退货单
	 */
	public void modReturnProduct(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid		= adminLoggerBean.getAdid();
			long rp_id		= StringUtil.getLong(request, "rp_id");
			String note		= StringUtil.getString(request, "note");
			long sid		= StringUtil.getLong(request,"sid");
			long oid		= StringUtil.getLong(request,"oid");
			long ps_id		= StringUtil.getLong(request, "ps_id");
			int is_send_mail= StringUtil.getInt(request, "is_send_mail");
			int return_product_flag	= StringUtil.getInt(request, "return_product_flag");
			
			//修改服务单状态
			DBRow serviceRow = new DBRow();
			serviceRow.add("is_need_return", return_product_flag);
			
			DBRow service	= floorServiceOrderMgrZyj.getServiceOrderDetailBySid(sid);
			int status		= service.get("status", 8);
			if(ReturnTypeKey.NEED == return_product_flag)
			{
				//状态为等待处理、虚拟退货、无需退货时才可以编辑状态为等待退货
				if(ServiceOrderStatusKey.WAITING == status || ServiceOrderStatusKey.VIRTUAL_RETURN == status
						 || ServiceOrderStatusKey.NOT_NEED_RETURN == status)
				{
					serviceRow.add("status", ServiceOrderStatusKey.WAITING_RETURN);
				}
			}
			else
			{
				serviceRow.add("status", ServiceOrderStatusKey.VIRTUAL_RETURN);
			}
			DBRow porder	= floorOrderMgr.getDetailPOrderByOid(oid);
			floorServiceOrderMgrZyj.updateServiceOrder(serviceRow, sid);
			//修改退货单
			//获取地址信息
			/*
			String first_name		= StrUtil.getString(request, "first_name");
			String last_name		= StrUtil.getString(request, "last_name");
			long ccid				= StrUtil.getLong(request, "ccid");				//国家ID
			String address_country	= StrUtil.getString(request, "address_country");	//国家名
			String address_country_code = StrUtil.getString(request, "address_country_code");//国家code
			long pro_id				= StrUtil.getLong(request, "pro_id");			//省份ID
			String address_state	= StrUtil.getString(request, "address_state");	//省份字符串   
			String address_city		= StrUtil.getString(request, "address_city");	//城市
			String address_street	= StrUtil.getString(request, "address_street");	//街道
			String address_zip		= StrUtil.getString(request, "address_zip");		//邮编
			String tel				= StrUtil.getString(request, "tel");				//电话
			*/
			String first_name			= porder.getString("first_name");
			String last_name			= porder.getString("last_name");
			long ccid					= porder.get("ccid",0L);				//国家ID
			String address_country		= porder.getString("address_country");	//国家名
			String address_country_code = porder.getString("address_country_code");//国家code
			long pro_id					= porder.get("pro_id",0L);			//省份ID
			String address_state		= porder.getString("address_state");	//省份字符串   
			String address_city			= porder.getString("address_city");	//城市
			String address_street		= porder.getString("address_street");	//街道
			String address_zip			= porder.getString("address_zip");		//邮编
			String tel					= porder.getString("tel");				//电话
			
			DBRow returnRowOriginal	= floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);			
			int is_send_mail_original = returnRowOriginal.get("is_send_mail", 0);
			
			DBRow returnRow = new DBRow();
			returnRow.add("note", note);
			returnRow.add("ps_id", ps_id); 
			returnRow.add("is_need_return", return_product_flag);
			
			returnRow.add("first_name", first_name);
			returnRow.add("last_name", last_name);
			returnRow.add("address_country", address_country);
			returnRow.add("address_city", address_city);
			returnRow.add("address_state", address_state);
			returnRow.add("address_zip", address_zip);
			returnRow.add("address_country_code", address_country_code);
			returnRow.add("tel", tel);
			returnRow.add("ccid", ccid);
			returnRow.add("pro_id", pro_id);
			returnRow.add("address_street", address_street);
			
			returnRow.add("create_type", 2);//客服创建
			returnRow.add("return_product_source", ReturnProductSourceKey.CUSTOMER_POST_BACK);//客户来源：客户退回
			returnRow.add("picture_recognition", ReturnProductPictureRecognitionkey.RecognitionNoneFinish);//图片识别未完成
			returnRow.add("return_product_type", StorageReturnProductTypeKey.HAS_PACK);//商品包装：有
			returnRow.add("gather_picture_status", 0);//商品采集未完成
			int rp_status = 0;
			if(ReturnTypeKey.NEED == return_product_flag)
			{
				rp_status = ReturnProductKey.WAITINGPRODUCT;//等待退货
			}
			else
			{
				rp_status = ReturnProductKey.WAITING;//等待处理
			}
			returnRow.add("status", rp_status);
			
			//只有当原先未发送过邮件且此次修改需要发送邮件时才会向客户发送
			if(1 != is_send_mail_original && 1 == is_send_mail)
			{
				returnRow.add("is_send_mail", is_send_mail);
			}
			floorReturnProductMgrZr.updateReturnProduct(returnRow, rp_id);
			
			DBRow productStoreCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(ps_id);
			//增加退货单日志
			this.addReturnProductLog(rp_id,0,0,0, rp_status, "修改退货单:["+rp_id+"],"+new ReturnTypeKey().getReturnTypeKeyValue(return_product_flag)+",退货仓库:"+(null!=productStoreCatalog?productStoreCatalog.getString("title"):""), ReturnProductLogTypeKey.UPDATE, adid, DateUtil.NowStr());
			
			//this.deleteReturnItemsAndSubItemsByRpid(rp_id);
			//修改退货明细及子明细
			this.updateReturnProducts(request,false,rp_id,adid,oid);
			//虚拟退货上传文件
			if(ReturnTypeKey.VIRTUAL == return_product_flag)
			{
				this.uploadServiceVirtualFiles(rp_id, adid, request);
			}
			else
			{
				//删除虚拟退货的文件
				floorFileMgrZr.deleteFileByFileWithTypeAndFileWithId(rp_id, FileWithTypeKey.ProductReturn);
			}
			
			//只有当原先未发送过邮件且此次修改需要发送邮件时才会向客户发送
			if(1 != is_send_mail_original && 1 == is_send_mail && ReturnTypeKey.NEED == return_product_flag)
			{
				//发送邮件通知顾客
				ReturnVoiceEmailPageString returnVoiceEmailPageString = new ReturnVoiceEmailPageString(rp_id,oid);
				User user = new User(returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getPassWord(),returnVoiceEmailPageString.getHost(),returnVoiceEmailPageString.getPort(),returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getAuthor(),returnVoiceEmailPageString.getRePly());
				MailAddress mailAddress = new MailAddress(porder.getString("client_id"));
				MailBody mailBody = new MailBody(returnVoiceEmailPageString,true,null);
				
				Mail mail = new Mail(user);
				mail.setAddress(mailAddress);
				mail.setMailBody(mailBody);
				mail.send();
			}
			
			cartReturn.clearCart(StringUtil.getSession(request));
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modReturnProduct",log);
		}
	}
	
	/**
	 * 创建退货单，虚拟退货或无需退货。虚拟或无需退货的创建及修改皆调用此方法
	 * @param request
	 * @throws Exception
	 */
	public void addReturnOrderByVirtualAndNot(HttpServletRequest request) throws Exception
	{
		try
		{
			int is_need_return					= StringUtil.getInt(request, "is_need_return");
			long rp_id							= StringUtil.getLong(request, "rp_id");
			long sid							= StringUtil.getLong(request, "sid");
			String note							= StringUtil.getString(request, "note");
			long oid							= StringUtil.getLong(request, "oid");
			AdminLoginBean adminLoggerBean		= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid							= adminLoggerBean.getAdid();
			DBRow porder						= floorOrderMgr.getDetailPOrderByOid(oid); //获取订单
			
			//更新服务单，记录是否需要退货
			DBRow serviceRow						= new DBRow();
			serviceRow.add("is_need_return",is_need_return);
			DBRow service	= floorServiceOrderMgrZyj.getServiceOrderDetailBySid(sid);
			int status		= service.get("status", 1);
			//状态为等待处理、需要退货、虚拟退货、无需退货时才可以编辑状态为等待退货
			if(ServiceOrderStatusKey.WAITING == status || ServiceOrderStatusKey.VIRTUAL_RETURN == status
					 || ServiceOrderStatusKey.WAITING_RETURN == status || ServiceOrderStatusKey.NOT_NEED_RETURN == status)
			{
				serviceRow.add("status", ServiceOrderStatusKey.NOT_NEED_RETURN);
			}
			floorServiceOrderMgrZyj.updateServiceOrder(serviceRow, sid);
			
			//获取地址信息
			String first_name			= porder.getString("first_name");
			String last_name			= porder.getString("last_name");
			long ccid					= porder.get("ccid",0L);				//国家ID
			String address_country		= porder.getString("address_country");	//国家名
			String address_country_code = porder.getString("address_country_code");//国家code
			long pro_id					= porder.get("pro_id",0L);			//省份ID
			String address_state		= porder.getString("address_state");	//省份字符串   
			String address_city			= porder.getString("address_city");	//城市
			String address_street		= porder.getString("address_street");	//街道
			String address_zip			= porder.getString("address_zip");		//邮编
			String tel					= porder.getString("tel");				//电话
			
			if(0 != rp_id)
			{
				DBRow returnRow = new DBRow();
				returnRow.add("note", note);
				returnRow.add("status", ReturnProductKey.WAITING);
				returnRow.add("is_need_return", is_need_return);
				floorReturnProductMgrZr.updateReturnProduct(returnRow, rp_id);
				//增加退货单日志
				this.addReturnProductLog(rp_id,0,0,0, ReturnProductKey.WAITING, "修改退货单:["+rp_id+"],"+new ReturnTypeKey().getReturnTypeKeyValue(is_need_return), ReturnProductLogTypeKey.UPDATE, adid, DateUtil.NowStr());
				//如果是无需，删除
				this.deleteReturnItemsAndSubItemsByRpid(rp_id);
				//删除虚拟退货的文件
				floorFileMgrZr.deleteFileByFileWithTypeAndFileWithId(rp_id, FileWithTypeKey.ProductReturn);
			}
			else
			{
				rp_id	= this.addProductReturnOrder(oid, sid, adid, is_need_return, note, 0L,ReturnProductCheckItemTypeKey.OUTCHECK,
						/*ReturnProductItemMatchKey.NOT_RELATION*/ ReturnProductKey.WAITING, ReturnProductSourceKey.CUSTOMER_POST_BACK,
						ReturnProductPictureRecognitionkey.RecognitionNoneFinish,first_name,last_name,ccid,address_country,
						address_country_code,pro_id,address_state,address_city,address_street,address_zip,tel);
				
				//增加退货单日志
				this.addReturnProductLog(rp_id,0,0,0, ReturnProductKey.WAITING, "创建退货单:["+rp_id+"],"+new ReturnTypeKey().getReturnTypeKeyValue(is_need_return), ReturnProductLogTypeKey.CREATE, adid, DateUtil.NowStr());
				
				ReturnProductOrderIndexMgrZyj.getInstance().addIndex(rp_id);
				
				//创建交易
				DBRow trade = new DBRow();
				trade.add("client_id",porder.getString("client_id"));
				trade.add("relation_type",0);
				trade.add("relation_id",oid);
				trade.add("parent_txn_id ",porder.getString("txn_id"));
				trade.add("is_create_order",0);
				trade.add("transaction_type",TransactionType.Warranty);
				trade.add("delivery_address_name",porder.getString("address_name"));
				trade.add("delivery_address_street",porder.getString("address_street"));
				trade.add("delivery_address_city",porder.getString("address_city"));
				trade.add("delivery_address_state",porder.getString("address_state"));
				trade.add("delivery_address_zip",porder.getString("address_zip"));
				trade.add("delivery_address_country_code",porder.getString("address_country_code"));
				trade.add("delivery_address_country",porder.getString("address_country"));
				trade.add("delivery_pro_id",porder.get("pro_id",0l));
				trade.add("delivery_ccid_id",porder.get("ccid",0l));
				trade.add("post_date", DateUtil.NowStr());
//					trade.add("rp_id", rp_id);
				
				floorOrderMgrZr.insertTrade(trade);
				
				//加跟进
				DBRow noteRow = new DBRow();
				noteRow.add("oid", oid);
				noteRow.add("note", "无需退货");
				noteRow.add("account", adminLoggerBean.getAccount());
				noteRow.add("adid", adminLoggerBean.getAdid());
				noteRow.add("trace_type", TracingOrderKey.NOT_NEED_RETURN);
				noteRow.add("post_date", DateUtil.NowStr());
				noteRow.add("rel_id", rp_id);
				noteRow.add("trace_child_type",porder.get("internal_tracking_status",0));
				floorOrderMgr.addPOrderNote(noteRow);
				
				ServiceOrderIndexMgrZyj.getInstance().updateIndex(sid);
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addReturnOrderByVirtualAndNot",log);
		}
	}
	
	/**
	 * 通过退货单ID删除退货单明细及子明细
	 * @param rp_id
	 * @throws Exception
	 */
	public void deleteReturnItemsAndSubItemsByRpid(long rp_id) throws Exception
	{
		try 
		{
			//退货子明细删除
			DBRow[] returnItems = floorReturnProductItemsFix.getReturnProductItemByRpId(rp_id);
			for (int i = 0; i < returnItems.length; i++) 
			{
				long rpi_id = returnItems[i].get("rpi_id", 0L);
				floorReturnProductOrderMgrZyj.deleteReturnSubItemsByRpiId(rpi_id);
			}
			//将退货明细文件，退货检查
			floorReturnProductOrderMgrZyj.deleteReturnItemsFixByRpId(rp_id);
			floorReturnProductOrderMgrZyj.deleteReturnItemsCheckByRpId(rp_id);
			
			//删除退货单明细
			floorReturnProductOrderMgrZyj.deleteReturnItemsByRpId(rp_id);
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addReturnOrderByVirtualAndNot",log);
		}
	}
	
	/**
	 * 上传服务单虚拟退货的文件
	 */
	public void uploadServiceVirtualFiles(long file_with_id, long adid, HttpServletRequest request) throws Exception
	{
		try 
		{
			int file_with_type				= StringUtil.getInt(request, "file_with_type");
			String sn						= StringUtil.getString(request, "sn");
			String system_file_path				= StringUtil.getString(request, "system_file_path");
			String fileNames				= StringUtil.getString(request, "file_names_virtual");
			String baseTempUrl				= Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp				= fileNames.trim().length()>0?true:false;
			StringBuffer fileNameSb			= new StringBuffer("");
			if(isFileUp)
			{
				String[] fileNameArray		= fileNames.split(",");
				if(fileNameArray.length > 0)
				{
					for (String tempFileName : fileNameArray) {
						String suffix	= getSuffix(tempFileName);
						String tempUrl	= baseTempUrl + tempFileName;
						StringBuffer realFileName	= new StringBuffer(sn)
									.append("_").append(file_with_id)
									.append("_").append(tempFileName.replace("."+suffix, ""));
						int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
						if(0 != indexFileName)
						{
							realFileName.append("_").append(indexFileName);
						}
						realFileName.append(".").append(suffix);
						fileNameSb.append(",").append(realFileName);
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append(system_file_path)
							.append("/").append(realFileName.toString());
						
						FileUtil.moveFile(tempUrl, url.toString());
						this.addReturnProductFile(realFileName.toString(), file_with_id, file_with_type, 0, adid, DateUtil.NowStr());
					}
				}
			}
			
		} catch (Exception e) {
			throw new SystemException(e, "uploadServiceVirtualFiles", log);
		}
	}
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
	}
	
	/**
	 * 文件不能为重复的。就是说文件如果有了，那么就会覆盖掉原来数据库中的数据
	 * @param fileName
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param upload_adid
	 * @param upload_time
	 * @throws Exception
	 */
	// 在File表中添加一个字段
	private void addReturnProductFile(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception{
		try
		{
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",upload_adid);
			file.add("upload_time",DateUtil.NowStr());
			floorTransportMgrZr.addFileNotExits(file); //// 都是插入的同一张表 所以用这个方法;
		}
		catch (Exception e) 
		{
			 throw new SystemException(e, "addTransportLogs", log);
		}
	}
	
	/**
	 * 创建退货单
	 * @return
	 * @throws Exception
	 */
	public long addProductReturnOrder(long oid, long sid, long adid, int is_need_return, String note, long ps_id, int return_product_check,
			int status, int return_product_source, int picture_recognition,String first_name,String last_name,long ccid,
			String address_country,String address_country_code,long pro_id,String address_state,String address_city,
			String address_street,String address_zip,String tel) throws Exception
	{
		try
		{
			//创建退货单
			DBRow returnRow = new DBRow();
			returnRow.add("oid", oid);
			returnRow.add("sid", sid);
			returnRow.add("status", status);
			returnRow.add("create_user", adid);
			returnRow.add("create_date", DateUtil.NowStr());
			returnRow.add("note", note);
			returnRow.add("return_product_source", return_product_source);
			returnRow.add("picture_recognition", picture_recognition);
			returnRow.add("ps_id", ps_id); 
			returnRow.add("is_need_return", is_need_return);
			returnRow.add("return_product_check", return_product_check);
			returnRow.add("create_type", 2);
			returnRow.add("return_product_type", StorageReturnProductTypeKey.HAS_PACK);
			
			returnRow.add("first_name", first_name);
			returnRow.add("last_name", last_name);
			returnRow.add("address_country", address_country);
			returnRow.add("address_city", address_city);
			returnRow.add("address_state", address_state);
			returnRow.add("address_zip", address_zip);
			returnRow.add("address_country_code", address_country_code);
			returnRow.add("tel", tel);
			returnRow.add("ccid", ccid);
			returnRow.add("pro_id", pro_id);
			returnRow.add("address_street", address_street);
			returnRow.add("gather_picture_status", 0);//商品采集未完成
			long rp_id = floorReturnProductMgrZr.addReturnProduct(returnRow);
			return rp_id;
		}
		catch (Exception e) 
		{
			 throw new SystemException(e, "addTransportLogs", log);
		}
	}
	
	/**
	 * 将服务单明细-->退货单明细
	 * @param rp_id
	 * @param sid
	 * @throws Exception
	 */
	public void saveReturnItemsFromServiceItems(long sid, long rp_id) throws Exception
	{
		try
		{
			DBRow[] serviceItems = floorServiceOrderMgrZyj.getServiceOrderItemsByServiceId(sid);
			//将套装商品拆分，存在subItems中
			HashMap<String, DBRow> tmpDataPool = new HashMap<String, DBRow>();
			for (int i = 0; i < serviceItems.length; i++)
			{
				long siid				= serviceItems[i].get("soi_id", 0L);
				long pid				= serviceItems[i].get("pid", 0L);
				String p_name			= serviceItems[i].getString("p_name");
				float quantity			= serviceItems[i].get("quantity", 0F);
				int product_type		= serviceItems[i].get("product_type", 0);
				
				DBRow returnItem		= new DBRow();
				returnItem.add("siid", siid);
				returnItem.add("pid", pid);
				returnItem.add("p_name", p_name);
				returnItem.add("quantity", quantity);
				returnItem.add("product_type",product_type);
				returnItem.add("return_product_check", ReturnProductCheckItemTypeKey.OUTCHECK);
				returnItem.add("return_reason", serviceItems[i].getString("service_reason"));
				DBRow product	= floorProductMgr.getDetailProductByPcid(pid);
				String unit_name_str = null!=product?product.getString("unit_name"):"";
				returnItem.add("unit_name", unit_name_str);
				returnItem.add("rp_id", rp_id);
				long rpi_id = floorReturnProductItemsFix.addReturnProductItem(returnItem);
				
				//分解套装成散件
				if (String.valueOf(product_type).equals(String.valueOf(ProductTypeKey.UNION_STANDARD)))
				{
					DBRow returnSubProducts[] = floorProductMgr.getProductsInSetBySetPid(pid);
					for (int j=0; j<returnSubProducts.length; j++)
					{
						if (tmpDataPool.containsKey(returnSubProducts[j].getString("pc_id")))
						{
							DBRow returnSubProduct =  tmpDataPool.get(returnSubProducts[j].getString("pc_id"));
							returnSubProduct.add("quantity", StringUtil.getFloat(returnSubProduct.getString("quantity"))+returnSubProducts[j].get("quantity", 0f)*quantity);
							tmpDataPool.put(returnSubProducts[j].getString("pc_id"), returnSubProduct);
						}
						else
						{
							DBRow returnSubProduct = new DBRow();
							returnSubProduct.add("p_name", returnSubProducts[j].getString("p_name"));
							returnSubProduct.add("pid", returnSubProducts[j].getString("pc_id"));
							returnSubProduct.add("quantity", returnSubProducts[j].get("quantity", 0f)*quantity);
							returnSubProduct.add("unit_name", returnSubProducts[j].getString("unit_name"));
							returnSubProduct.add("rpi_id", rpi_id);
							
							tmpDataPool.put(returnSubProducts[j].getString("pc_id"), returnSubProduct);
						}
					}
				}
				else
				{
					//普通商品直接插入
					if (tmpDataPool.containsKey(pid))
					{
						DBRow returnSubProduct =  tmpDataPool.get(pid);
						returnSubProduct.add("quantity", StringUtil.getFloat(returnSubProduct.getString("quantity"))+quantity);
						tmpDataPool.put(String.valueOf(pid), returnSubProduct);
					}
					else
					{
						DBRow returnSubProduct = new DBRow();
						returnSubProduct.add("p_name", p_name);
						returnSubProduct.add("pid", pid);
						returnSubProduct.add("quantity", quantity);
						returnSubProduct.add("unit_name", unit_name_str);
						returnSubProduct.add("rpi_id", rpi_id);
						
						tmpDataPool.put(String.valueOf(pid), returnSubProduct);
					}
				}
				
			}
	
			//最后插入散件
			Set<String> keys = tmpDataPool.keySet();   
			for(String key:keys)
			{
				floorReturnProductMgrZJ.addReturnProductSubItems(tmpDataPool.get(key));
			}
				
		}
		catch (Exception e) 
		{
			 throw new SystemException(e, "saveReturnItemsFromServiceItems", log);
		}
	}
	
	/**
	 * 通过退货单ID获取退货单子明细
	 * @param rp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnSubItemsByRpid(long rp_id) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.getReturnSubItemsByRpid(rp_id);
		}
		catch (Exception e) 
		{
			 throw new SystemException(e, "getReturnSubItemsByRpid(long rp_id)", log);
		}
	}
	
	/**
	 * 根据返修单号搜索返修单
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchReturnOrderByNumber(String search_key,int search_mode,PageCtrl pc) throws Exception 
	{
		try 
		{
			if(!search_key.equals("")&&!search_key.equals("\""))
			{
				search_key = search_key.replaceAll("\"","");
				search_key = search_key.replaceAll("'","");
				search_key = search_key.replaceAll("\\*","");
//				search_key +="*";这个索引是IK分词器，不要最后+*
			}
			int page_count = systemConfig.getIntConfigValue("page_count");
			
			return ReturnProductOrderIndexMgrZyj.getInstance().getSearchResults(search_key,search_mode,page_count,pc);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"searchReturnOrderByNumber",log);
		}
	}
	
	/**
	 * 如果客户未联系客服直接将货退回，需要客服创建服务并将服务与退货关联
	 * @param request
	 * @throws Exception
	 */
	public DBRow serviceRelevanceReturnProductOrder(HttpServletRequest request) throws Exception
	{
		try
		{
			long rp_id	= StringUtil.getLong(request, "rp_id");
			DBRow returnOldRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			DBRow result = new DBRow();
			if(null == returnOldRow)
			{
				result.add("flag", "此退货单不存在");
			}
			else
			{
				long rp_sid	= returnOldRow.get("sid", 0L);
				
				if(0 != rp_sid)
				{
					result.add("flag", "此退货单已关联服务单");
				}
				//可以关联
				else
				{
					long sid	= StringUtil.getLong(request, "sid");
					int rstatus  = returnOldRow.get("status", 0);
					//更新退货单，记录sid
					DBRow returnRow	= new DBRow();
					returnRow.add("sid", sid);
					floorReturnProductMgrZr.updateReturnProduct(returnRow, rp_id);
					//更新服务单，更改状态
					DBRow serviceRow = new DBRow();
					DBRow serviceOldRow = floorServiceOrderMgrZyj.getServiceOrderDetailBySid(sid);
					int status = serviceOldRow.get("status", 0);
					//在服务单关联退货单时，以下状态的服务单可以改状态
					if(ServiceOrderStatusKey.WAITING_PAY != status && ServiceOrderStatusKey.CREATEORDER != status
							&& ServiceOrderStatusKey.FINISH_PAY != status && ServiceOrderStatusKey.FINISH != status
							&& ServiceOrderStatusKey.CANCEL != status)
					{
						if(ReturnProductKey.WAITING == rstatus)
						{
							serviceRow.add("status", ServiceOrderStatusKey.WAITING);
						}
						else if(ReturnProductKey.LITTLE_PRODUCT == rstatus)
						{
							serviceRow.add("status", ServiceOrderStatusKey.LITTLE_PRODUCT);
						}
						else if(ReturnProductKey.FINISH == rstatus)
						{
							serviceRow.add("status", ServiceOrderStatusKey.FINISH_HANDLE);
						}
						else if(ReturnProductKey.FINISHALL == rstatus)
						{
							serviceRow.add("status", ServiceOrderStatusKey.FINISH_RETURN);
						}
					}
					if(ReturnTypeKey.NEED != serviceOldRow.get("is_need_return", 0))
					{
						serviceRow.add("is_need_return", ReturnTypeKey.NEED);
					}
					
					floorServiceOrderMgrZyj.updateServiceOrder(serviceRow, sid);
					result.add("flag", "success");
					AdminLoginBean adminLoggerBean		= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
					this.addReturnProductLog(rp_id,0,0,0, rstatus, "退货单["+rp_id+"]关联服务单["+sid+"]", ReturnProductLogTypeKey.RELATION_ORDER, adminLoggerBean.getAdid(), DateUtil.NowStr());
				}
			}
			
			return result;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"serviceRelevanceReturnProductOrder",log);
		}
	}
	
	/**
	 * 统计需要处理的退货单数
	 * @return
	 * @throws Exception
	 */
	public DBRow statReturnProductOrderNeedHandleCount() throws Exception
	{
		try
		{
			int need_gather_pic_count		= floorReturnProductOrderMgrZyj.statNeedGatherPicReturnOrderCount();
			int need_recog_count			= floorReturnProductOrderMgrZyj.statNeedRecognitionReturnOrderCount();
			int need_register_count			= floorReturnProductOrderMgrZyj.statNeedRegisterReturnOrderCount();
			int need_register_finish		= floorReturnProductOrderMgrZyj.statRegisterFinishReturnOrderCount();
			int need_relation_other_order	= floorReturnProductOrderMgrZyj.statReturnOrderNotRelationOtherOrderCount();
			int servicer_need_handle_count	= need_recog_count + need_register_finish + need_relation_other_order;//需要识别，登记完成，需关联其他单据
			int storager_need_handle_count	= need_gather_pic_count + need_register_count;//需要采集，需要登记
			int servicer_and_stroager		= servicer_need_handle_count + storager_need_handle_count;//总数
			DBRow row = new DBRow();
			row.add("servicer_and_stroager", servicer_and_stroager);
			row.add("servicer_need_handle_count", servicer_need_handle_count);
			row.add("storager_need_handle_count", storager_need_handle_count);
			row.add("need_gather_pic_count", need_gather_pic_count);
			row.add("need_recog_count", need_recog_count);
			row.add("need_register_count", need_register_count);
			row.add("need_register_finish", need_register_finish);
			row.add("need_relation_other_order", need_relation_other_order);
			return row;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"returnNeedHandleCount",log);
		}
	}
	
	/**
	 * 统计退货，需要采集图片
	 * 采集图片未完成，需要退货，刚创建（仓库）或等待退货（客服）
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statNeedGatherPicReturnOrders(PageCtrl pc) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.statNeedGatherPicReturnOrders(pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"statNeedGatherPicReturnOrders()",log);
		}
	}
	
	/**
	 * 统计退货，需要识别商品
	 * 未识别，图片采集完成，需要退货，刚创建（仓库）或等待退货（客服）或部分退货
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statNeedRecognitionReturnOrders(PageCtrl pc) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.statNeedRecognitionReturnOrders(pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"statNeedRecognitionReturnOrders()",log);
		}
	}
	
	/**
	 * 统计退货，需要登记商品
	 * 图片采集完成，识别完成，需要退货，刚创建（仓库）或等待退货（客服）或部分退货
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statNeedRegisterReturnOrders(PageCtrl pc) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.statNeedRegisterReturnOrders(pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"statNeedRegisterReturnOrders()",log);
		}
	}
	
	/**
	 * 统计退货，登记完成
	 * 图片采集完成，识别完成，需要退货，登记完成
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statRegisterFinishReturnOrders(PageCtrl pc) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.statRegisterFinishReturnOrders(pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"statRegisterFinishReturnOrders()",log);
		}
	}
	
	/**
	 * 需要关联其他单据
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statReturnOrderNotRelationOtherOrders(PageCtrl pc) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.statReturnOrderNotRelationOtherOrders(pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"statReturnOrderNotRelationOtherOrders(PageCtrl pc)",log);
		}
	}
	
	/**
	 * 更改退货单明细数量
	 */
	public void modReturnItemCount(HttpServletRequest request) throws Exception
	{
		try
		{
			long rp_id		= StringUtil.getLong(request, "rp_id");
			DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			long rpi_id		= StringUtil.getLong(request, "rpi_id");
			long pid		= StringUtil.getLong(request, "pid");
			float quantity	= StringUtil.getFloat(request, "quantity");//修改数量
			//改变明细数量
			DBRow row = new DBRow();
			row.add("quantity", quantity);
			floorReturnProductOrderMgrZyj.modReturnItemCount(row, rpi_id);
			
			//处理子明细数量
			DBRow product			= floorProductMgr.getDetailProductByPcid(pid);
			
			//套装分解套装成散件
			if (1 == product.get("union_flag", 0))
			{
				DBRow subProducts[] = floorProductMgr.getProductsInSetBySetPid(pid);//套装下的散件
				//如果能够查询出散件，更新散件数量
				if(subProducts.length > 0)
				{
					for (int j = 0; j < subProducts.length; j ++)
					{
						//通过退货明细ID，商品ID，修改子明细的数量
						DBRow[] returnSubItems = floorReturnProductOrderMgrZyj.getReturnSubItemsByRpiIdAndPid(rpi_id, subProducts[j].get("pc_id", 0L));
						DBRow returnSubItem = new DBRow();//要更新的子明细
						if(returnSubItems.length > 0)
						{
							returnSubItem.add("quantity", quantity*subProducts[j].get("quantity", 0F));
							floorReturnProductOrderMgrZyj.updateReturnProductSubItem(returnSubItem, returnSubItems[0].get("rpsi_id",0L));
						}
					}
				}
				//如果不能查询出散件，更新本商品数量
				else
				{
					//通过退货明细ID，商品ID，修改子明细的数量
					DBRow[] returnSubItems = floorReturnProductOrderMgrZyj.getReturnSubItemsByRpiIdAndPid(rpi_id, pid);
					if(returnSubItems.length > 0)
					{
						DBRow returnSubItem = new DBRow();
						returnSubItem.add("quantity", quantity);
						floorReturnProductOrderMgrZyj.updateReturnProductSubItem(returnSubItem, returnSubItems[0].get("rpsi_id", 0L));
					}
				}
			}
			//散件
			else
			{
				//通过退货明细ID，商品ID，修改子明细的数量
				DBRow[] returnSubItems = floorReturnProductOrderMgrZyj.getReturnSubItemsByRpiIdAndPid(rpi_id, pid);
				if(returnSubItems.length > 0)
				{
					DBRow returnSubItem = new DBRow();
					returnSubItem.add("quantity", quantity);
					floorReturnProductOrderMgrZyj.updateReturnProductSubItem(returnSubItem, returnSubItems[0].get("rpsi_id", 0L));
				}
			}
			AdminLoginBean adminLoggerBean		= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			this.addReturnProductLog(rp_id, rpi_id, 0,0,returnRow.get("status", 0), "修改退货单["+rp_id+"]商品"+product.getString("p_name")+",数量:"+quantity, ReturnProductLogTypeKey.CHG_COUNT, adminLoggerBean.getAdid(), DateUtil.NowStr());
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modReturnItemCount(request)",log);
		}
	}
	
	/**
	 * 通过退货单子明细ID获取子明细
	 * @param rpsi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getReturnSubItemsByRpsiId(long rpsi_id) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.getReturnSubItemsByRpsiId(rpsi_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getReturnSubItemsByRpsi_id(rpsi_id)",log);
		}
	}
	
	/**
	 * 更改退货单子明细数量
	 */
	public void modReturnSubItemCount(HttpServletRequest request) throws Exception
	{
		try
		{
			long rpsi_id	= StringUtil.getLong(request, "rpsi_id");
			DBRow rpsiRow	= floorReturnProductOrderMgrZyj.getReturnSubItemByRpsiId(rpsi_id);
			long rpi_id		= rpsiRow.get("rpi_id", 0L);
			DBRow rpiRow	= floorReturnProductOrderMgrZyj.getReturnItemByRpiId(rpi_id);
			long rp_id		= rpiRow.get("rp_id", 0L);
			DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			
			float quantity	= StringUtil.getFloat(request, "quantity");
			DBRow returnSubItem = new DBRow();
			returnSubItem.add("quantity", quantity);
			floorReturnProductOrderMgrZyj.updateReturnProductSubItem(returnSubItem, rpsi_id);
			AdminLoginBean adminLoggerBean		= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			this.addReturnProductLog(rp_id,rpi_id,rpsi_id,0, returnRow.get("status", 0), "修改退货单["+rp_id+"]子明细商品"+rpsiRow.getString("p_name")+"数量:"+quantity, ReturnProductLogTypeKey.CHG_COUNT, adminLoggerBean.getAdid(), DateUtil.NowStr());
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modReturnSubItemCount(request)",log);
		}
	}
	
	/**
	 * 得到收到货且处理完成的退货明细
	 */
	public DBRow[] getReturnProductItemByReceiveHandleInfo(int is_receive_item, long rp_id) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.getReturnProductItemByReceiveHandleInfo(is_receive_item, rp_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getReturnProductItemByReceiveHandleInfo(request)",log);
		}
	}
	
	/**
	 * 登记退货
	 */
	public void handleReturnProductAllItems(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminMgr am = new AdminMgr();
			AdminLoginBean adminLoggerBean = am.getAdminLoginBean( request.getSession(true) );
			
			//将退货商品中完好、功能、外观残损的数量记录在退货子明细中
			long rp_id = StringUtil.getLong(request, "rp_id");
			String[] rpsi_ids =	request.getParameterValues("rpsi_id");
			if(rpsi_ids != null && rpsi_ids.length > 0 ){
				for(String rpsi_id : rpsi_ids){
					// 更新退货sub上面的信息
					double quality_nor = StringUtil.getDouble(request, rpsi_id+"_nor");
					double quality_damage = StringUtil.getDouble(request,rpsi_id+ "_damage");
					double quality_package_damage = StringUtil.getDouble(request, rpsi_id+"_package_damage");
					////system.out.println("quality_nor :" + quality_nor + "\n" +" quality_damage : "+ quality_damage + "\n" + "quality_package_damage : " + quality_package_damage );
					DBRow updateSubItemRow = new DBRow();
					updateSubItemRow.add("quality_nor", quality_nor);
					updateSubItemRow.add("quality_damage", quality_damage);
					updateSubItemRow.add("quality_package_damage", quality_package_damage);
					floorReturnProductSubItemsMgrZr.updateSubReturnProductItem(updateSubItemRow,Long.parseLong(rpsi_id) );
				}
			}
			//将收到货的退货明细状态改成已登记退货
			DBRow updateItemReceived = new DBRow();
			updateItemReceived.add("is_return_handle",1);
			floorReturnProductOrderMgrZyj.updateReceivedReturnProductItem(updateItemReceived, rp_id);
			
			//更新退货单主状态为完成
			String return_difference_note = StringUtil.getString(request, "return_difference_note");
			DBRow upDateRow = new DBRow();
			upDateRow.add("status", ReturnProductKey.FINISHALL);//StorageReturnProductStatusKey.FINISH
			upDateRow.add("return_difference_note", return_difference_note);
			upDateRow.add("handle_user", adminLoggerBean.getAdid());
			upDateRow.add("handle_date", DateUtil.NowStr());
			floorReturnProductMgrZr.updateReturnProduct(upDateRow, rp_id);
			
			//更新服务单状态
			DBRow detailReturnInfo	= floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			long oid = detailReturnInfo.get("oid", 0L);
		  	if(null != detailReturnInfo && 0 != detailReturnInfo.get("sid", 0L))
		  	{
		  		DBRow serviceRow	= new DBRow();
		  		serviceRow.add("status", ServiceOrderStatusKey.FINISH_RETURN);//退货完成
		  		floorServiceOrderMgrZyj.updateServiceOrder(serviceRow, detailReturnInfo.get("sid", 0L));
		  		if(0 == oid)
		  		{
		  			oid = (null != floorServiceOrderMgrZyj.getServiceOrderDetailBySid(detailReturnInfo.get("sid", 0L))?floorServiceOrderMgrZyj.getServiceOrderDetailBySid(detailReturnInfo.get("sid", 0L)).get("oid", 0L):0L);
		  		}
		  	}
			
			//修改库存数量
		  	//通过退货单ID获取退货子明细
		  	DBRow[] returnItemSubs	= floorReturnProductOrderMgrZyj.getReturnSubItemsByRpidAndReceived(rp_id);
		  	for (int i = 0; i < returnItemSubs.length; i++)
		  	{
				long pid						= returnItemSubs[i].get("pid", 0L);
				float quality_nor				= returnItemSubs[i].get("quality_nor", 0F);
				float quality_damage			= returnItemSubs[i].get("quality_damage", 0F);
				float quality_package_damage	= returnItemSubs[i].get("quality_package_damage", 0F);
				DBRow detailProduct = floorProductMgr.getDetailProductByPcid(pid);
				//完好商品进库
				if (quality_nor>0)
				{
					ProductStoreLogBean inProductStoreLogBean = new ProductStoreLogBean();
					inProductStoreLogBean.setAccount(adminLoggerBean.getAccount());
					inProductStoreLogBean.setAdid(adminLoggerBean.getAdid());
					inProductStoreLogBean.setOid(rp_id);
					inProductStoreLogBean.setBill_type(ProductStoreBillKey.RETURN_ORDER);
					inProductStoreLogBean.setOperation(ProductStoreOperationKey.IN_STORE_RETURN);
					
					//增加正常商品数量并记录入库日志
					floorProductMgr.incProductStoreCountByPcid(inProductStoreLogBean, detailReturnInfo.get("ps_id", 0l), pid, quality_nor,0);
					
					//ProductMgr productMgr = (ProductMgr)MvcUtil.getBeanFromContainer("productMgr");				
					//TDate td = new TDate();
					//long datemark = td.getDateTime();
					
					//productMgr.addInProductLog(detailProduct.getString("p_code"), detailReturnInfo.get("ps_id", 0l), quality_nor, adminLoggerBean.getAdid(), datemark, 1, InOutStoreKey.IN_STORE_RETURN,detailReturnInfo.get("rp_id", 0l));
				}
				//功能残损进库
				if (quality_damage>0)
				{
					//增加残损件数目adminLoggerBean.getPs_id()
					floorProductMgr.incProductDamagedCountByPcid(detailReturnInfo.get("ps_id", 0l),pid,quality_damage);
					
					//记录残损件入库日志
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(rp_id);
					inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.RETURN_ORDER);
					inProductDamagedCountLogBean.setPs_id(detailReturnInfo.get("ps_id", 0l));
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN);
					inProductDamagedCountLogBean.setQuantity(quality_damage);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(detailProduct.get("pc_id",0l));
					productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
				}
				//包装残损进库
				if (quality_package_damage>0)
				{
					//增加残损件数目
					floorProductMgr.incProductPackageDamagedCountByPcid(detailReturnInfo.get("ps_id", 0l),pid,quality_package_damage);
					
					ProductStoreLogBean inProductDamagedCountLogBean = new ProductStoreLogBean();
					inProductDamagedCountLogBean.setAccount(adminLoggerBean.getEmploye_name());
					inProductDamagedCountLogBean.setAdid(adminLoggerBean.getAdid());
					inProductDamagedCountLogBean.setOid(rp_id);
					inProductDamagedCountLogBean.setBill_type(ProductStoreBillKey.RETURN_ORDER);
					inProductDamagedCountLogBean.setPs_id(detailReturnInfo.get("ps_id", 0l));
					inProductDamagedCountLogBean.setOperation(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN);
					inProductDamagedCountLogBean.setQuantity(quality_package_damage);
					inProductDamagedCountLogBean.setQuantity_type(ProductStoreCountTypeKey.DAMAGED);
					inProductDamagedCountLogBean.setPc_id(detailProduct.get("pc_id",0l));
					productMgr.addProductStoreLogs(inProductDamagedCountLogBean);
				}
				
			}
		  	//加日志
		  	this.addReturnProductLog(rp_id,0,0,0, ReturnProductKey.FINISHALL, "退货单["+rp_id+"]登记退货", ReturnProductLogTypeKey.UPDATE, adminLoggerBean.getAdid(), DateUtil.NowStr());
		  	//对相关订单增加备注
			FloorCatalogMgr fcm = (FloorCatalogMgr)MvcUtil.getBeanFromContainer("floorCatalogMgr");
			String orderNote = fcm.getDetailProductStorageCatalogById(adminLoggerBean.getPs_id()).getString("title")+" warehouse 已经收到退件";
			DBRow detailOrder = floorOrderMgr.getDetailPOrderByOid(oid);
			
			if(null != detailOrder)
			{
				DBRow noteRow = new DBRow();
				noteRow.add("oid", detailOrder.get("oid", 0L));
				noteRow.add("note", orderNote);
				noteRow.add("account", adminLoggerBean.getAccount());
				noteRow.add("adid", adminLoggerBean.getAdid());
				noteRow.add("trace_type", TracingOrderKey.RETURN_PRODUCT);
				noteRow.add("post_date", DateUtil.NowStr());
				noteRow.add("rel_id", rp_id);
				noteRow.add("trace_child_type",detailOrder.get("internal_tracking_status",0));
				floorOrderMgr.addPOrderNote(noteRow);
			}
			
			//发送确认邮件通知顾客
			if(0 != oid)
			{
				ReturnReceiveEmailPageString returnReceiveEmailPageString = new ReturnReceiveEmailPageString(rp_id,oid);
				User user = new User(returnReceiveEmailPageString.getUserName(),returnReceiveEmailPageString.getPassWord(),returnReceiveEmailPageString.getHost(),returnReceiveEmailPageString.getPort(),returnReceiveEmailPageString.getUserName(),returnReceiveEmailPageString.getAuthor(),returnReceiveEmailPageString.getRePly());
	            MailAddress mailAddress = new MailAddress(detailOrder.getString("client_id"));
	            MailBody mailBody = new MailBody(returnReceiveEmailPageString,true,null);
	            
	            Mail mail = new Mail(user);
	            mail.setAddress(mailAddress);
	            mail.setMailBody(mailBody);
	            mail.send();
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"handleReturnProductAllItems(request)",log);
		}
	}
	
	
	/**
	 * 更新退货的收货仓库
	 */
	public void modProductOrderPsId(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long rp_id	= StringUtil.getLong(request, "rp_id");
			DBRow detailReturnInfo	= floorReturnProductMgrZr.getRetrunProductByRpId(rp_id);
			long ps_id	= StringUtil.getLong(request, "ps_id");
			long original_ps_id	= StringUtil.getLong(request, "original_ps_id");
			DBRow returnRow = new DBRow();
			returnRow.add("ps_id", ps_id);
			returnRow.add("original_ps_id", original_ps_id);
			returnRow.add("mod_ps_person", adminLoggerBean.getAdid());
			returnRow.add("mod_ps_time", DateUtil.NowStr());
			floorReturnProductMgrZr.updateReturnProduct(returnRow, rp_id);
			DBRow catalogOriginal = floorCatalogMgr.getDetailProductStorageCatalogById(original_ps_id);
			DBRow productStoreCatalog = floorCatalogMgr.getDetailProductStorageCatalogById(ps_id);
			String content = "退货单["+rp_id+"]更新收货仓库为:"
							+(null!=productStoreCatalog?productStoreCatalog.getString("title"):"")
							+",原仓库为:"+(null!=catalogOriginal?catalogOriginal.getString("title"):"");
			//增加退货单日志
			this.addReturnProductLog(rp_id,0,0,0, detailReturnInfo.get("status", 0), content, ReturnProductLogTypeKey.CHG_STORAGE, adminLoggerBean.getAdid(), DateUtil.NowStr());
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"modProductOrderPsId(request)",log);
		}
	}
	
	/**
	 * 添加退货日志
	 * @return
	 * @throws Exception
	 */
	public long addReturnProductLog(long rp_id, long rpi_id, long rpsi_id, long fix_rpi_id,
			int rp_status, String content, int follow_type, long operator, String operate_time) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.addReturnProductLog(rp_id, rpi_id, rpsi_id, fix_rpi_id, rp_status, content, follow_type, operator, operate_time);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"addReturnProductLog(long rp_id, int rp_status, String content, int follow_type, long operator, String operate_time)",log);
		}
	}
	
	
	/**
	 * 根据退货单ID，跟进类型，状态查询日志
	 * @param rp_id
	 * @param follow_type
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnProductLogsByIdTypeStatus(long rp_id, int follow_type, int status) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.getReturnProductLogsByIdTypeStatus(rp_id, follow_type, status);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getReturnProductLogsByIdTypeStatus(long rp_id, int follow_type, int status)",log);
		}
	}
	
	/**
	 * 根据退货单ID，跟进类型，状态查询日志数量
	 * @param rp_id
	 * @param follow_type
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public int getReturnProductLogsCountByIdTypeStatus(long rp_id, int follow_type, int status) throws Exception
	{
		try
		{
			return floorReturnProductOrderMgrZyj.getReturnProductLogsCountByIdTypeStatus(rp_id, follow_type, status);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getReturnProductLogsCountByIdTypeStatus(long rp_id, int follow_type, int status)",log);
		}
	}
	
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorReturnProductOrderMgrZyj(
			FloorReturnProductOrderMgrZyj floorReturnProductOrderMgrZyj) {
		this.floorReturnProductOrderMgrZyj = floorReturnProductOrderMgrZyj;
	}

	public void setCartReturn(CartReturn cartReturn) {
		this.cartReturn = cartReturn;
	}

	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}

	public void setFloorReturnProductMgrZr(
			FloorReturnProductMgrZr floorReturnProductMgrZr) {
		this.floorReturnProductMgrZr = floorReturnProductMgrZr;
	}

	public void setFloorReturnProductItemsFix(
			FloorReturnProductItemsFix floorReturnProductItemsFix) {
		this.floorReturnProductItemsFix = floorReturnProductItemsFix;
	}

	public void setFloorCheckReturnProductItemMgrZr(
			FloorCheckReturnProductItemMgrZr floorCheckReturnProductItemMgrZr) {
		this.floorCheckReturnProductItemMgrZr = floorCheckReturnProductItemMgrZr;
	}

	public void setFloorReturnProductMgrZJ(
			FloorReturnProductMgrZJ floorReturnProductMgrZJ) {
		this.floorReturnProductMgrZJ = floorReturnProductMgrZJ;
	}

	public void setFloorServiceOrderMgrZyj(
			FloorServiceOrderMgrZyj floorServiceOrderMgrZyj) {
		this.floorServiceOrderMgrZyj = floorServiceOrderMgrZyj;
	}

	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}
	
	public void setFloorOrderMgrZr(FloorOrderMgrZr floorOrderMgrZr) {
		this.floorOrderMgrZr = floorOrderMgrZr;
	}

	public void setOrderMgr(OrderMgrIFace orderMgr) {
		this.orderMgr = orderMgr;
	}

	public void setFloorFileMgrZr(FloorFileMgrZr floorFileMgrZr) {
		this.floorFileMgrZr = floorFileMgrZr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

	public static void setLog(Logger log) {
		ReturnProductOrderMgrZyj.log = log;
	}

	public void setProductMgr(ProductMgr productMgr) {
		this.productMgr = productMgr;
	}

	public void setFloorReturnProductSubItemsMgrZr(
			FloorReturnProductSubItemsMgrZr floorReturnProductSubItemsMgrZr) {
		this.floorReturnProductSubItemsMgrZr = floorReturnProductSubItemsMgrZr;
	}

	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}
	
}
