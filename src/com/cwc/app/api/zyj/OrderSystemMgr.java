package com.cwc.app.api.zyj;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.checkin.CheckinTaskNotFoundException;
import com.cwc.app.floor.api.zyj.FloorCheckInMgrZyj;
import com.cwc.app.floor.api.zyj.FloorOrderSystemMgr;
import com.cwc.app.iface.zyj.OrderSystemMgrIFace;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.DocLevelKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OrderSystemTypeKey;
import com.cwc.app.key.SyncOrderStatusKey;
import com.cwc.app.key.WmsDeliveryStatusIntStrKey;
import com.cwc.app.key.WmsPickUpStatusIntStrKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;

public class OrderSystemMgr implements OrderSystemMgrIFace{

	static Logger log = Logger.getLogger("ACTION");
	private FloorOrderSystemMgr floorOrderSystemMgr;
	private FloorCheckInMgrZyj floorCheckInMgrZyj;
	
	
	/**
	 * 处理单据表
	 * @param entry_detail entry明细ID
	 * @param ps_id 仓库ID
	 * @param order_status 查询的单据的状态
	 * @param system 单据所属系统
	 * @param adminLoginBean 账号信息
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月17日 下午4:31:16
	 */
	public DBRow addOrUpdateOrderSystems(long entry_detail, long ps_id, String order_status, int system,AdminLoginBean adminLoginBean) throws Exception
	{
		DBRow result = null;
		//system>0 说明是要更新task的业务单据
		if(system > 0)
		{
			result = new DBRow();
			DBRow entryDetail = floorCheckInMgrZyj.findTaskEquipmentResourceByDetailId(entry_detail);
			if(null == entryDetail) throw new CheckinTaskNotFoundException();
			//task当前保存的os
			long os_id = entryDetail.get("lr_id", 0L);
			long entry_id = entryDetail.get("dlo_id", 0L);
			long id = 0L;
			boolean existOrNot = false;
			long os_id1 = 0L;
			int status_had = 0;
//			if(OrderSystemTypeKey.WMS == system)
//			{
				DBRow[] wmsRows = findOrderSystemByNumberTypeInfos(system, entryDetail.getString("number"), entryDetail.get("number_type", 0), entryDetail.getString("company_id"), entryDetail.getString("account_id"), ps_id,entryDetail.get("receipt_no", 0L));
				
				if(wmsRows.length > 0)
				{
					existOrNot = true;
					os_id1 = wmsRows[0].get("id", 0L);
					status_had = wmsRows[0].get("number_status", 0);
				}
//			}
			
			DBRow row = new DBRow();
			int order_status_int = CheckInChildDocumentsStatusTypeKey.UNPROCESS;
			if(status_had != order_status_int && status_had != 0)
			{
				order_status_int = status_had;
			}
					//returnNumberStatusBySystemTypeStatus(order_status, system, entryDetail.get("number_type", 0),status_had);
			int order_type  = returnOrderTypeByDocType(entryDetail.get("number_type", 0));//DN或RN
			int doc_level = returnDocLevelByDocType(entryDetail.get("number_type", 0));
			
			row.add("number_status", order_status_int);
			row.add("system_number_status", order_status);
			row.add("customer_id", entryDetail.getString("customer_id"));
			row.add("account_id", entryDetail.getString("account_id"));
			row.add("title", entryDetail.getString("supplier_id"));
			row.add("appointment_date", !StrUtil.isBlank(entryDetail.getString("appointment_date"))?entryDetail.getString("appointment_date"):null);
//			row.add("entry_id", entry_id);
			row.add("receipt_no", entryDetail.get("receipt_no", 0L));
			
			if(!existOrNot)
			{
				row.add("system_type", system);
				row.add("number", entryDetail.getString("number"));
				row.add("doc_name", entryDetail.get("number_type", 0));
				row.add("company_id", entryDetail.getString("company_id"));
				row.add("ps_id", ps_id);
				row.add("order_type", order_type);
				row.add("doc_level", doc_level);
				
				row.add("creator", adminLoginBean.getAdid());
				row.add("create_time", DateUtil.NowStr());
				id = floorOrderSystemMgr.addOrderSystem(row);
			}
			else
			{
				floorOrderSystemMgr.updateOrderSystem(os_id1, row);
				id = os_id1;
			}
			//task关联的业务单据变了，如果只有一条与此业务单据关联且是当前单据，删除业务单据
			if(os_id > 0 && os_id1 != os_id)
			{
				Set<String> taskSet = new HashSet<String>();
				DBRow[] tasks = floorCheckInMgrZyj.findTaskByOsid(os_id);
				for (int i = 0; i < tasks.length; i++) 
				{
					taskSet.add(tasks[i].getString("dlo_detail_id"));
				}
				if(taskSet.size() == 0 || (taskSet.size() == 1 && taskSet.contains(String.valueOf(entry_detail))))
				{
					floorOrderSystemMgr.deleteOrderSystemByOsid(os_id);
				}
			}
			result.add("lr_id", id);
		}
		return result;
	}
	
	/**
	 * 通过number信息查询是否存在
	 * @param system
	 * @param number
	 * @param number_type
	 * @param companyId
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月18日 下午2:21:18
	 */
	public DBRow[] findOrderSystemByNumberTypeInfos(int system, String number, int doc_name, String companyId,String account_id, long ps_id, long receipt_no) throws Exception
	{
		return floorOrderSystemMgr.findOrderSystemByNumberTypeInfos(system, number, doc_name, companyId,account_id, ps_id, receipt_no);
	}
	
	/**
	 * 通过Number信息删除单据
	 * @param system
	 * @param number
	 * @param number_type
	 * @param companyId
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月18日 下午2:21:56
	 */
	public DBRow deleteOrderSystemByNumberTypeInfos(int system, String number, int number_type, String companyId, long ps_id) throws Exception
	{
		DBRow result = new DBRow();
		
		
		
		
		return result;
	}
	
	/**
	 * 通过taskId判断是否删除业务单据表
	 * @param entry_detail
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月18日 下午4:25:03
	 */
	public DBRow deleteOrNotOrderSystemByDetailInfos(long entry_detail) throws Exception
	{
		DBRow result = new DBRow();
		DBRow entryDetail = floorCheckInMgrZyj.findTaskEquipmentResourceByDetailId(entry_detail);
		if(null == entryDetail) throw new CheckinTaskNotFoundException();
		Set<String> taskSet = new HashSet<String>();
		DBRow[] tasks = floorCheckInMgrZyj.findTaskByOsid(entryDetail.get("lr_id", 0L));
		for (int i = 0; i < tasks.length; i++) 
		{
			taskSet.add(tasks[i].getString("dlo_detail_id"));
		}
		if(taskSet.size() == 0 || (taskSet.size() == 1 && taskSet.contains(String.valueOf(entry_detail))))
		{
			floorOrderSystemMgr.deleteOrderSystemByOsid(entryDetail.get("lr_id", 0L));
		}
		return result;
	}
	
	public int returnOrderTypeByDocType(int docType)
	{
		int order_type = 0;
		if(docType==ModuleKey.CHECK_IN_LOAD || docType==ModuleKey.CHECK_IN_PONO 
				|| docType==ModuleKey.CHECK_IN_ORDER || docType == ModuleKey.CHECK_IN_PICKUP_ORTHERS || docType == ModuleKey.SMALL_PARCEL)
		{
			order_type = CheckInMainDocumentsRelTypeKey.PICK_UP;
		}
		if(docType==ModuleKey.CHECK_IN_BOL || docType==ModuleKey.CHECK_IN_CTN 
				|| docType == ModuleKey.CHECK_IN_DELIVERY_ORTHERS)
		{
			order_type = CheckInMainDocumentsRelTypeKey.DELIVERY;
		}
		return order_type;
	}
	
	private int returnNumberStatusBySystemTypeStatus(String order_status, int system, int docType, int status_before)
	{
		int order_status_int = 0;
		if(OrderSystemTypeKey.WMS == system)
		{
			if(docType==ModuleKey.CHECK_IN_LOAD || docType==ModuleKey.CHECK_IN_PONO 
					|| docType==ModuleKey.CHECK_IN_ORDER)
			{
				order_status_int = new WmsPickUpStatusIntStrKey().getWmsStatusKeyByValue(order_status);
			}
			if(docType==ModuleKey.CHECK_IN_BOL || docType==ModuleKey.CHECK_IN_CTN)
			{
				order_status_int = new WmsDeliveryStatusIntStrKey().getWmsStatusKeyByValue(order_status);
			}
		}
		else if(OrderSystemTypeKey.SYNC == system)
		{
			order_status_int = SyncOrderStatusKey.OPEN;
		}
		return order_status_int;
	}
	
	private int returnDocLevelByDocType(int docType)
	{
		int docLevel = 0;
		if(docType==ModuleKey.CHECK_IN_LOAD)
		{
			docLevel = DocLevelKey.LEVEL3;
		}
		if(docType==ModuleKey.CHECK_IN_PONO)
		{
			docLevel = DocLevelKey.LEVEL2;
		}
		if(docType==ModuleKey.CHECK_IN_ORDER)
		{
			docLevel = DocLevelKey.LEVEL1;
		}
		
		if(docType==ModuleKey.CHECK_IN_BOL)
		{
			docLevel = DocLevelKey.LEVEL2;
		}
		if(docType==ModuleKey.CHECK_IN_CTN)
		{
			docLevel = DocLevelKey.LEVEL2;
		}
		return docLevel;
	}
	
	
	public DBRow findOrderSystemById(long id ) throws Exception
	{
		return floorOrderSystemMgr.findOrderSystemByNumberTypeInfos(id);
	}
	
	
	public void updateOrderSystemById(long id, DBRow row) throws Exception
	{
		floorOrderSystemMgr.updateOrderSystem(id, row);
	}

	public void setFloorOrderSystemMgr(FloorOrderSystemMgr floorOrderSystemMgr) {
		this.floorOrderSystemMgr = floorOrderSystemMgr;
	}


	public void setFloorCheckInMgrZyj(FloorCheckInMgrZyj floorCheckInMgrZyj) {
		this.floorCheckInMgrZyj = floorCheckInMgrZyj;
	}

}
