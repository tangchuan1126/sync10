package com.cwc.app.api.zyj;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zr.FloorPurchaseMgrZr;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zyj.FloorProductMgrZyj;
import com.cwc.app.floor.api.zyj.FloorPurchaseMgrZyj;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyMgrZZZ;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zyj.PurchaseMgrZyjIFace;
import com.cwc.app.key.DrawbackKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.InvoiceKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.PurchaseKey;
import com.cwc.app.key.PurchaseLogTypeKey;
import com.cwc.app.key.PurchaseProductModelKey;
import com.cwc.app.key.QualityInspectionKey;
import com.cwc.app.key.TransportProductFileKey;
import com.cwc.app.key.TransportTagKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class PurchaseMgrZyj implements PurchaseMgrZyjIFace{

	static Logger log = Logger.getLogger("ACTION");
	private FloorPurchaseMgrZr floorPurchaseMgrZr;
	private FloorPurchaseMgr floorPurchaseMgr;
	private FloorTransportMgrZr floorTransportMgrZr;
	private FloorPurchaseMgrZyj floorPurchaseMgrZyj;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private SystemConfig systemConfig;
	private FloorProductMgrZyj floorProductMgrZyj;
	private FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ;
	
	
	@Override
	public void handlePurchaseInvoiceCertificationState(
			HttpServletRequest request) throws Exception {
		
		try {
			//如果是完成那么是要记录文件路径,记录日志,更新主表的记录。
			// 文件的命名规范为 P_invoice_阶段状态_采购单Id;
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
				
				
			long file_with_id = StringUtil.getLong(request, "purchase_id");
			int file_with_type = StringUtil.getInt(request,"file_with_type");
			int file_with_class = StringUtil.getInt(request,"invoice");  
			String sn = StringUtil.getString(request,"sn");
			String path = StringUtil.getString(request,"path");
			int follow_up_type = StringUtil.getInt(request,"follow_up_type");
			String context = StringUtil.getString(request,"context");
			String eta = StringUtil.getString(request,"eta");
				
			String baseUrl = Environment.getHome()+"upl_imags_tmp/";
			StringBuffer fileNameSb = new StringBuffer();
			String fileNames = StringUtil.getString(request, "file_names");
			String[] fileNameArray = null ;
			boolean isFileUp = false ;
			if(fileNames.trim().length() > 0 ){
				fileNameArray = fileNames.trim().split(",");
				isFileUp = true ;
				}
			if(isFileUp){
				for(String tempFileName : fileNameArray){
				 
					String  suffix = this.getSuffix(tempFileName);
					StringBuffer realyFileName = new StringBuffer(sn);
					realyFileName.append("_").append(file_with_id).append("_").append(tempFileName.replace("."+suffix, ""));
					int indexFile = floorTransportMgrZr.getIndexOfFilebyFileName(realyFileName.toString());
					if(indexFile != 0){
						realyFileName.append("_").append(indexFile);
				}
					realyFileName.append(".").append(suffix);
					// 插入表中
					fileNameSb.append(",").append(realyFileName.toString());
						DBRow file = new DBRow();
					file.add("file_name",realyFileName.toString());
					file.add("file_with_id",file_with_id);
					file.add("file_with_type",file_with_type);
					file.add("file_with_class",file_with_class);
					file.add("upload_adid",adminLoggerBean.getAdid());
					file.add("upload_time",DateUtil.NowStr());
					floorTransportMgrZr.addTransportCertificateFile(file);
					StringBuffer url = new StringBuffer();
					url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realyFileName.toString());
					FileUtil.moveFile(baseUrl+tempFileName,url.toString());
				}	
			}
					
					/**
					 * 日志需要保存的信息
					 */
					String invoiceOrDrawback = "";
					String activityStr = "";
					if(4 == follow_up_type)
					{
						invoiceOrDrawback = "发票流程";
						InvoiceKey invoiceKey = new InvoiceKey();
						if(0 == file_with_class)
						{
								file_with_class = InvoiceKey.FINISH;
						}
						activityStr = invoiceKey.getInvoiceById(file_with_class);
					}
					else if(5 == follow_up_type)
					{
						invoiceOrDrawback = "退税流程";
						DrawbackKey drawback = new DrawbackKey();
						if(0 == file_with_class)
						{
								file_with_class = DrawbackKey.FINISH;
						}
						activityStr = drawback.getDrawbackById(file_with_class);
					}
					else if(8 == follow_up_type)
					{
						invoiceOrDrawback = "内部标签流程";
						TransportTagKey tagKey = new TransportTagKey();
						if(file_with_class == TransportTagKey.TAG)
						{
								activityStr = "制签中";
						}
						else
						{
							if(0 == file_with_class)
							{
									file_with_class = TransportTagKey.FINISH;
							}
							activityStr = tagKey.getTransportTagById(file_with_class);
						}
					}
					else if(33 == follow_up_type)
					{
						invoiceOrDrawback = "第三方标签流程";
						TransportTagKey tagKey = new TransportTagKey();
						if(file_with_class == TransportTagKey.TAG)
						{
								activityStr = "制签中";
						}
						else
						{
							if(0 == file_with_class)
							{
									file_with_class = TransportTagKey.FINISH;
							}
							activityStr = tagKey.getTransportTagById(file_with_class);
						}
					}
					DBRow dbrow=new DBRow();
					dbrow.add("followup_type", follow_up_type);//表示是发票或者退税日志
					if((4 == follow_up_type && InvoiceKey.FINISH != file_with_class) 
							|| (5 == follow_up_type && DrawbackKey.FINISH != file_with_class)
							|| (8 == follow_up_type && TransportTagKey.FINISH != file_with_class)
							|| (33 == follow_up_type && TransportTagKey.FINISH != file_with_class)){
						dbrow.add("followup_expect_date", eta);
					}
					dbrow.add("followup_type_sub", file_with_class);
					dbrow.add("purchase_id", file_with_id);
//					if(isFileUp)
//					{
//						dbrow.add("followup_content","上传凭证文件:"+(fileNameSb.length() > 1 ? fileNameSb.substring(1) : "") +" "+context);
//					}else
//					{
						dbrow.add("followup_content",context);
//					}	
					dbrow.add("follower_id",adminLoggerBean.getAdid());
					dbrow.add("follower",adminLoggerBean.getEmploye_name());
					dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
					floorPurchaseMgr.addPurchasefollowuplogs(dbrow);
				
					DBRow scheduleInvoice	= scheduleMgrZr.getScheduleByAssociate(file_with_id, ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
					DBRow scheduleDrawback	= scheduleMgrZr.getScheduleByAssociate(file_with_id, ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
					//更新主单据
					DBRow purchaseRow = new DBRow();
					DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(file_with_id);
					if(4 == follow_up_type)
					{
						if(file_with_class == InvoiceKey.FINISH)
						{
								purchaseRow.add("invoice", file_with_class);
								//查询是否有进入阶段中的日志，如果有，就用此日期而不用采购单创建的时间
								DBRow purchaseLog = floorPurchaseMgrZyj.getFollowUpLogsByPurchaseIdAndTypeActivity(file_with_id, follow_up_type, InvoiceKey.INVOICING);
								TDate endDate = new TDate();
								if(null == purchaseLog){
									endDate = new TDate(purchase.getString("purchase_date")+"00:00:00");
								}else{
									endDate = new TDate(purchaseLog.getString("followup_date")+"00:00:00");
								}
								double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
								purchaseRow.add("invoice_over_date", diffDay);
								//更新任务，开票结束，退税进入中，修改开票任务的状态，修改退税任务的状态，两个任务日志
	//							if(DrawbackKey.DRAWBACK == purchase.get("drawback", 0)){
								if(null != scheduleDrawback && DrawbackKey.DRAWBACK == purchase.get("drawback", 0)){
									purchaseRow.add("drawback", DrawbackKey.DRAWBACKING);
									String contentDrawback	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+file_with_id+",[退税中]";
									scheduleMgrZr.addScheduleReplayExternal(file_with_id , ModuleKey.PURCHASE_ORDER ,
											 ProcessKey.DRAWBACK , contentDrawback , false, request, "purchase_drawback_period");
									DBRow dbrowLog	= new DBRow();
									dbrowLog.add("followup_type", ProcessKey.DRAWBACK);
									dbrowLog.add("followup_type_sub", DrawbackKey.DRAWBACKING);
									dbrowLog.add("purchase_id", file_with_id);
									dbrowLog.add("followup_content","[退税中]");
									dbrowLog.add("follower_id",adminLoggerBean.getAdid());
									dbrowLog.add("follower",adminLoggerBean.getEmploye_name());
									dbrowLog.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
									floorPurchaseMgr.addPurchasefollowuplogs(dbrowLog);
								}
								//修改任务信息
								if(null != scheduleInvoice){
	//							if(InvoiceKey.INVOICE == purchase.get("invoice", 0) || InvoiceKey.INVOICING == purchase.get("invoice", 0)){
	 							String contentInvoice	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+file_with_id+","+context;
									scheduleMgrZr.addScheduleReplayExternal(file_with_id , ModuleKey.PURCHASE_ORDER ,
											ProcessKey.INVOICE , contentInvoice , true, request, "purchase_invoice_period");
								}
							}else{
								TDate tDated = new TDate();
								tDated.addDay(+2);
								purchaseRow.add("followup_date", tDated.formatDate("yyyy-MM-dd HH:mm:ss"));
								if(null != scheduleInvoice){
	//							if(InvoiceKey.INVOICE == purchase.get("invoice", 0) || InvoiceKey.INVOICING == purchase.get("invoice", 0)){
									String contentInvoicing = adminLoggerBean.getEmploye_name()+"确认了采购单:"+file_with_id+","+context;
									scheduleMgrZr.addScheduleReplayExternal(file_with_id , ModuleKey.PURCHASE_ORDER ,
											 ProcessKey.INVOICE , contentInvoicing , false, request, "purchase_invoice_period");
								}
							}
							floorPurchaseMgr.updatePurchaseBasic(purchaseRow, file_with_id);
						}else if(5 == follow_up_type){
							if(file_with_class == DrawbackKey.FINISH){
								purchaseRow.add("drawback", file_with_class);
	//							DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(file_with_id);
								//查询是否有进入阶段中的日志，如果有，就用此日期而不用采购单创建的时间
								DBRow purchaseLog = floorPurchaseMgrZyj.getFollowUpLogsByPurchaseIdAndTypeActivity(file_with_id, follow_up_type, DrawbackKey.DRAWBACKING);
								TDate endDate = new TDate();
								if(null == purchaseLog){
									endDate = new TDate(purchase.getString("purchase_date")+"00:00:00");
								}else{
									endDate = new TDate(purchaseLog.getString("followup_date")+"00:00:00");
								}
								double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
								purchaseRow.add("drawback_over_date", diffDay);
								//更新任务
								if(null != scheduleDrawback){
	//							if(DrawbackKey.DRAWBACK == purchase.get("drawback", 0) || DrawbackKey.DRAWBACKING == purchase.get("drawback", 0)){
	 							String contentDrawback	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+file_with_id+","+context;
									scheduleMgrZr.addScheduleReplayExternal(file_with_id , ModuleKey.PURCHASE_ORDER ,
											 ProcessKey.DRAWBACK , contentDrawback , true, request, "purchase_drawback_period");
								}
							}else{
								TDate tDated = new TDate();
								tDated.addDay(+2);
								purchaseRow.add("followup_date", tDated.formatDate("yyyy-MM-dd HH:mm:ss"));
								if(null != scheduleDrawback){
	//							if(DrawbackKey.DRAWBACK == purchase.get("drawback", 0) || DrawbackKey.DRAWBACKING == purchase.get("drawback", 0)){
									String contentDrawbacking	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+file_with_id+","+context;
									scheduleMgrZr.addScheduleReplayExternal(file_with_id , ModuleKey.PURCHASE_ORDER ,
											 ProcessKey.DRAWBACK , contentDrawbacking , false, request, "purchase_drawback_period");
								}
							}
							floorPurchaseMgr.updatePurchaseBasic(purchaseRow, file_with_id);
						}
						else if(8 == follow_up_type){
							DBRow scheduleTag	= scheduleMgrZr.getScheduleByAssociate(file_with_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
							if(file_with_class == TransportTagKey.FINISH){
								purchaseRow.add("need_tag", file_with_class);
	//							DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(file_with_id);
								TDate endDate = new TDate(purchase.getString("purchase_date")+"00:00:00");
								double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
								purchaseRow.add("need_tag_over", diffDay);
								//更新任务
								if(null != scheduleTag){
	//							if(TransportTagKey.TAG == purchase.get("need_tag", 0)){
								String contentTag	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+file_with_id+","+context;
									scheduleMgrZr.addScheduleReplayExternal(file_with_id , ModuleKey.PURCHASE_ORDER ,
											 ProcessKey.PURCHASETAG , contentTag , true, request, "purchase_tag_period");
								}
							}else{
								TDate tDated = new TDate();
								tDated.addDay(+2);
								purchaseRow.add("followup_date", tDated.formatDate("yyyy-MM-dd HH:mm:ss"));
								if(null != scheduleTag){
	//							if(TransportTagKey.TAG == purchase.get("need_tag", 0)){
									String contentTaging	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+file_with_id+","+context;
									scheduleMgrZr.addScheduleReplayExternal(file_with_id , ModuleKey.PURCHASE_ORDER ,
											 ProcessKey.PURCHASETAG , contentTaging , false, request, "purchase_tag_period");
								}
							}
							floorPurchaseMgr.updatePurchaseBasic(purchaseRow, file_with_id);
						}
						else if(33 == follow_up_type){
							DBRow scheduleTag	= scheduleMgrZr.getScheduleByAssociate(file_with_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_THIRD_TAG);
							if(file_with_class == TransportTagKey.FINISH){
								purchaseRow.add("need_third_tag", file_with_class);
	//							DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(file_with_id);
								TDate endDate = new TDate(purchase.getString("purchase_date")+"00:00:00");
								double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
								purchaseRow.add("need_third_tag_over", diffDay);
								//更新任务
								if(null != scheduleTag){
	//							if(TransportTagKey.TAG == purchase.get("need_tag", 0)){
								String contentTag	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+file_with_id+","+context;
									scheduleMgrZr.addScheduleReplayExternal(file_with_id , ModuleKey.PURCHASE_ORDER ,
											 ProcessKey.PURCHASE_THIRD_TAG , contentTag , true, request, "purchase_tag_period");
								}
							}else{
								TDate tDated = new TDate();
								tDated.addDay(+2);
								purchaseRow.add("followup_date", tDated.formatDate("yyyy-MM-dd HH:mm:ss"));
								if(null != scheduleTag){
	//							if(TransportTagKey.TAG == purchase.get("need_tag", 0)){
									String contentTaging	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+file_with_id+","+context;
									scheduleMgrZr.addScheduleReplayExternal(file_with_id , ModuleKey.PURCHASE_ORDER ,
											 ProcessKey.PURCHASE_THIRD_TAG , contentTaging , false, request, "purchase_tag_period");
								}
							}
							floorPurchaseMgr.updatePurchaseBasic(purchaseRow, file_with_id);
						}
				
		} catch (Exception e) {
			throw new SystemException(e,"handlePurchaseInvoiceCertificationState",log);
		}
		
	}
	
	private String handlePurchaseLogContent(HttpServletRequest request, long purchaseId, int process, int activity, String time)
	{
		AdminLoginBean adminLoggerBean = ((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
		String personName				= adminLoggerBean.getEmploye_name();
		ProcessKey processKey			= new ProcessKey();
		String processName				= processKey.getStatusById(process);
			
		return "";
	}
	
	@Override
	public String handlePurchaseProductTagTypesFile(HttpServletRequest request) throws Exception{
		
		try{
			// 这里上传的文件是保存在product_file这张表中pc_id
			// 保存的文件名为T_product_121212_商品分类_index(商品分类)
			//1=AMZON标签
			//2=UPC标签
			// 记录日志 日志中
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
				 
			String  pc_id = StringUtil.getString(request, "pc_id"); //商品的Ids
			String[] arrayPcIds = pc_id.split(",");
			long file_with_id = StringUtil.getLong(request,"purchase_id");	//关联的业务Id
			int file_with_type = StringUtil.getInt(request,"file_with_type");
			int file_with_class = StringUtil.getInt(request,"file_with_class"); // 这个目前写成商品的那种标签
			String file_with_className = StringUtil.getString(request,"file_with_class_name");  
			String sn = StringUtil.getString(request,"sn");
			String path = StringUtil.getString(request,"path");
			 
			//得到上传的临时文件
			String fileNames = StringUtil.getString(request, "file_names");
			String[] fileNameArray = null ;
			if(fileNames.trim().length() > 0 ){
				fileNameArray = fileNames.trim().split(",");
				}
			if(fileNameArray != null && fileNameArray.length > 0){
				StringBuffer logFileNameContent = new StringBuffer();
				String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
				for(String tempFileName : fileNameArray){
					String tempSuffix = getSuffix(tempFileName);
					String tempfileUpName = tempFileName.replace("."+tempSuffix, "");
					StringBuffer realyFileName = new StringBuffer(sn);
					realyFileName.append("_").append(file_with_id).append("_").append(tempfileUpName);
					int indexFile = floorTransportMgrZr.getIndexOfProductFileByFileName(realyFileName.toString());
					if(indexFile != 0){realyFileName.append("_").append(indexFile);}
					realyFileName.append(".").append(tempSuffix);
					logFileNameContent.append(",").append(realyFileName.toString());
					
					String  tempUrl =  baseTempUrl+tempFileName;
					StringBuffer url = new StringBuffer();
					url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realyFileName.toString());
					FileUtil.moveFile(tempUrl,url.toString());
					for(int index = 0 , count = arrayPcIds.length ; index < count ; index++){
					DBRow file = new DBRow();
					file.add("file_name",realyFileName.toString());
					file.add("file_with_id",file_with_id);
					file.add("file_with_type",file_with_type);
					file.add("product_file_type",file_with_class);
					file.add("upload_adid",adminLoggerBean.getAdid());
					file.add("upload_time",DateUtil.NowStr());
					file.add("pc_id", arrayPcIds[index]);
					floorTransportMgrZr.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
					}
				}
			}
			return null ;
		}catch (Exception e) {
			throw new SystemException(e, "handleTransportProductPictureUp", log);
		}
	}
	
	
	@Override
	public String handlePurhcaseInvoiceCertificationAjax(HttpServletRequest request) throws Exception{
		
		try {
			String backUrl		= StringUtil.getString(request, "backurl");
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate			= new TDate();
			int follow_up_type	= StringUtil.getInt(request, "follow_up_type");
			int invoice			= StringUtil.getInt(request, "invoice");
			long purchase_id	= StringUtil.getLong(request, "purchase_id");
			String eta			= StringUtil.getString(request, "eta");
			String context		= StringUtil.getString(request, "context");
			/**
			 * 日志需要保存的信息
			 */
			String invoiceOrDrawback = "";
			String activityStr = "";
			if(4 == follow_up_type){
				invoiceOrDrawback = "发票流程 ";
				InvoiceKey invoiceKey = new InvoiceKey();
				activityStr = invoiceKey.getInvoiceById(String.valueOf(invoice));
			}else if(5 == follow_up_type){
				invoiceOrDrawback = "退税流程 ";
				DrawbackKey drawback = new DrawbackKey();
				activityStr = drawback.getDrawbackById(String.valueOf(invoice));
			}else if(8 == follow_up_type){
				invoiceOrDrawback = "制签流程";
				TransportTagKey tagKey = new TransportTagKey();
				if(invoice == TransportTagKey.TAG){
					activityStr = "制签中";
				}else{
					activityStr = tagKey.getTransportTagById(String.valueOf(invoice));
				}
			}
			DBRow dbrow=new DBRow();
			dbrow.add("followup_type", follow_up_type);//表示是发票日志
			dbrow.add("followup_type_sub", invoice);
			dbrow.add("purchase_id", purchase_id);
			dbrow.add("followup_content",context);
			dbrow.add("follower_id",adminLoggerBean.getAdid());
			dbrow.add("follower",adminLoggerBean.getEmploye_name());
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			dbrow.add("followup_expect_date", eta);
			floorPurchaseMgr.addPurchasefollowuplogs(dbrow);
			
			//更新主单据
			DBRow purchaseRow = new DBRow();
			DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
			DBRow scheduleInvoice	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.INVOICE);
			DBRow scheduleDrawback	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.DRAWBACK);
			if(4 == follow_up_type){
				if(invoice == InvoiceKey.FINISH){
					purchaseRow.add("invoice", invoice);
					//查询是否有进入阶段中的日志，如果有，就用此日期而不用采购单创建的时间
					DBRow purchaseLog = floorPurchaseMgrZyj.getFollowUpLogsByPurchaseIdAndTypeActivity(purchase_id, follow_up_type, InvoiceKey.INVOICING);
					TDate endDate = new TDate();
					if(null == purchaseLog){
						endDate = new TDate(purchase.getString("purchase_date"));
					}else{
						endDate = new TDate(purchaseLog.getString("followup_date"));
					}
					double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
					purchaseRow.add("invoice_over_date", diffDay);
					//更新任务，开票结束，退税进入中，修改开票任务的状态，修改退税任务的状态，两个任务日志
					if(null != scheduleDrawback && DrawbackKey.DRAWBACK == purchase.get("drawback", 0)){
						purchaseRow.add("drawback", DrawbackKey.DRAWBACKING);
						String contentDrawback	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+",[退税中]";
						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
								 ProcessKey.DRAWBACK , contentDrawback , false, request, "purchase_drawback_period");
						DBRow dbrowLog	= new DBRow();
						dbrowLog.add("followup_type", ProcessKey.DRAWBACK);
						dbrowLog.add("followup_type_sub", DrawbackKey.DRAWBACKING);
						dbrowLog.add("purchase_id", purchase_id);
						dbrowLog.add("followup_content","[退税中]");
						dbrowLog.add("follower_id",adminLoggerBean.getAdid());
						dbrowLog.add("follower",adminLoggerBean.getEmploye_name());
						dbrowLog.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
						floorPurchaseMgr.addPurchasefollowuplogs(dbrowLog);
					}
//					else{
//						String contentDrawback2	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+"[退税流程]["+new DrawbackKey().getStatusById(purchaseRow.get("drawback", 0))+"] "+context;
//						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
//								 ProcessKey.DRAWBACK , contentDrawback2 , false, request, "purchase_drawback_period");
//					}
					//修改任务信息
					if(null != scheduleInvoice){
//					if(InvoiceKey.INVOICE == purchase.get("invoice", 0) || InvoiceKey.INVOICING == purchase.get("invoice", 0)){
						String contentInvoice	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+","+context;
						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
								ProcessKey.INVOICE , contentInvoice , true, request, "purchase_invoice_period");
					}
				}else{
					TDate tDated = new TDate();
					tDated.addDay(+2);
					purchaseRow.add("followup_date", tDated.formatDate("yyyy-MM-dd HH:mm:ss"));
					if(null != scheduleInvoice){
//					if(InvoiceKey.INVOICE == purchase.get("invoice", 0) || InvoiceKey.INVOICING == purchase.get("invoice", 0)){
						String contentInvoicing = adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+", "+context;
						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
								 ProcessKey.INVOICE , contentInvoicing , false, request, "purchase_invoice_period");
					}
				}
				floorPurchaseMgr.updatePurchaseBasic(purchaseRow, purchase_id);
			}else if(5 == follow_up_type){
				if(invoice == DrawbackKey.FINISH){
					purchaseRow.add("drawback", invoice);
//					DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
					//查询是否有进入阶段中的日志，如果有，就用此日期而不用采购单创建的时间
					DBRow purchaseLog = floorPurchaseMgrZyj.getFollowUpLogsByPurchaseIdAndTypeActivity(purchase_id, follow_up_type, DrawbackKey.DRAWBACKING);
					TDate endDate = new TDate();
					if(null == purchaseLog){
						endDate = new TDate(purchase.getString("purchase_date"));
					}else{
						endDate = new TDate(purchaseLog.getString("followup_date"));
					}
					double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
					purchaseRow.add("drawback_over_date", diffDay);
					if(null != scheduleDrawback){
//					if(DrawbackKey.DRAWBACK == purchase.get("drawback", 0) || DrawbackKey.DRAWBACKING == purchase.get("drawback", 0)){
						String contentDrawback	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+","+context;
						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
								 ProcessKey.DRAWBACK , contentDrawback , true, request, "purchase_drawback_period");
					}
				}else{
					TDate tDated = new TDate();
					tDated.addDay(+2);
					purchaseRow.add("followup_date", tDated.formatDate("yyyy-MM-dd HH:mm:ss"));
					if(null != scheduleDrawback){
//					if(DrawbackKey.DRAWBACK == purchase.get("drawback", 0) || DrawbackKey.DRAWBACKING == purchase.get("drawback", 0)){
						String contentDrawbacking	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+","+context;
						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
								 ProcessKey.DRAWBACK , contentDrawbacking , false, request, "purchase_drawback_period");
					}
				}
				floorPurchaseMgr.updatePurchaseBasic(purchaseRow, purchase_id);
			}else if(8 == follow_up_type){
				DBRow scheduleTag	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASETAG);
				if(invoice == TransportTagKey.FINISH){
					purchaseRow.add("need_tag", invoice);
//					DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
					TDate endDate = new TDate(purchase.getString("purchase_date")+"00:00:00");
					double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
					purchaseRow.add("need_tag_over", diffDay);
					if(null != scheduleTag){
//					if(TransportTagKey.TAG == purchase.get("need_tag", 0)){
						String contentTag	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+","+context;
						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
								 ProcessKey.PURCHASETAG , contentTag , true, request, "purchase_tag_period");
					}
				}else{
					TDate tDated = new TDate();
					tDated.addDay(+2);
					purchaseRow.add("followup_date", tDated.formatDate("yyyy-MM-dd HH:mm:ss"));
					if(null != scheduleTag){
//					if(TransportTagKey.TAG == purchase.get("need_tag", 0)){
						String contentTaging	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+","+context;
						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
								 ProcessKey.PURCHASETAG , contentTaging , false, request, "purchase_tag_period");
					}
				}
				floorPurchaseMgr.updatePurchaseBasic(purchaseRow, purchase_id);
			}
			
			return backUrl;
		} catch (Exception e) {
			throw new SystemException(e, "handlePurhcaseInvoiceCertificationAjax", log);
		}
		
	}

	/**
	 * 通过采购单ID，文件类型，查询商品文件
	 */
	@Override
	public DBRow[] getAllProductTagFilesByPcId(long purhcaseId, int[] types) throws Exception{
		try {
			return floorPurchaseMgrZyj.getAllProductTagFilesByPcId(purhcaseId, types);
		} catch (Exception e) {
			throw new SystemException(e, "getAllProductTagFilesByPcId", log);
		}
	}
	/**
	 * 通过采购单ID,文件类型数组查询文件信息的列表
	 */
	public DBRow[] getPurhcaseFileInfosByPurchaseIdTypes(long purhcaseId, int[] types) throws Exception{
		try {
			return floorPurchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(purhcaseId, types);
		} catch (Exception e) {
			throw new SystemException(e, "getPurhcaseFileInfosByPurchaseIdTypes", log);
		}
	}
	
	/**
	 * 通过采购单ID,日志类型，阶段类型，得到列表中的第一条日志
	 */
	@Override
	public DBRow getFollowUpLogsByPurchaseIdAndTypeActivity(long purchaseId, int type, int typeSub) throws Exception{
		try {
			return floorPurchaseMgrZyj.getFollowUpLogsByPurchaseIdAndTypeActivity(purchaseId, type, typeSub);
		} catch (Exception e) {
			throw new SystemException(e, "getFollowUpLogsByPurchaseIdAndTypeActivity", log);
		}
		
	}
	
	/**
	 * 根据采购单跟进获得差距时间
	 * @param purchase_id
	 * @param follow_up_type
	 * @param followup_type_sub
	 * @param diffTime_type
	 * @return
	 * @throws Exception
	 */
	public double getPurchaseDiffTime(long purchase_id,int follow_up_type,int followup_type_sub,String diffTime_type)
		throws Exception
	{
		//查询是否有进入阶段中的日志，如果有，就用此日期而不用采购单创建的时间
		DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
		//InvoiceKey.INVOICING
		DBRow purchaseLog = floorPurchaseMgrZyj.getFollowUpLogsByPurchaseIdAndTypeActivity(purchase_id, follow_up_type,followup_type_sub);
		
		TDate tDate = new TDate();
		TDate endDate = new TDate();
		double diffDay = 0;
		if(null == purchaseLog)
		{
			endDate = new TDate(purchase.getString("purchase_date"));
		}
		else
		{
			endDate = new TDate(purchaseLog.getString("followup_date"));
			
			diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"),diffTime_type);
		}
		
		diffDay = MoneyUtil.round(diffDay,2);
		return diffDay;
	}
	
	@Override
	public String handlePurchaseQualityInspectionAjax(HttpServletRequest request) throws Exception{
		try {
			
			String backUrl		= StringUtil.getString(request, "backurl");
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate			= new TDate();
			int quality_inspection	= StringUtil.getInt(request, "quality_inspection");
			long purchase_id	= StringUtil.getLong(request, "purchase_id");
			String eta			= StringUtil.getString(request, "eta");
			String context		= StringUtil.getString(request, "context");
			
			QualityInspectionKey qualityInspectionKey = new QualityInspectionKey();
			DBRow dbrow=new DBRow();
			dbrow.add("followup_type", ProcessKey.QUALITY_INSPECTION);//表示是质检日志
			//如果获取到的是0，说明select不可用，只有完成时不可用，所以现在质检处于完成状态
			if(0 == quality_inspection){
				quality_inspection = QualityInspectionKey.FINISH;
			}
			dbrow.add("followup_type_sub", quality_inspection);
			dbrow.add("purchase_id", purchase_id);
			dbrow.add("followup_content",context);
			dbrow.add("follower_id",adminLoggerBean.getAdid());
			dbrow.add("follower",adminLoggerBean.getEmploye_name());
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			if(quality_inspection != QualityInspectionKey.FINISH){
				dbrow.add("followup_expect_date", eta);
			}
			floorPurchaseMgr.addPurchasefollowuplogs(dbrow);
			
			//更新主单据
			DBRow purchaseRow = new DBRow();
			DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.QUALITY_INSPECTION);
			if(quality_inspection == QualityInspectionKey.FINISH){
				purchaseRow.add("quality_inspection", quality_inspection);
//				DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
				//查询是否有进入阶段中的日志，如果有，就用此日期而不用采购单创建的时间
//				DBRow purchaseLog = floorPurchaseMgrZyj.getFollowUpLogsByPurchaseIdAndTypeActivity(purchase_id, ProcessKey.QUALITY_INSPECTION, QualityInspectionKey.QUALITYING);
				TDate endDate = new TDate();
//				if(null == purchaseLog){
				endDate = new TDate(purchase.getString("purchase_date")+"00:00:00");
//				}else{
//					endDate = new TDate(purchaseLog.getString("followup_date")+"00:00:00");
//				}
				double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
				purchaseRow.add("quality_inspection_over", diffDay);
				
				if(null != schedule){
						String contentQuality	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+","+context;
						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
								ProcessKey.QUALITY_INSPECTION , contentQuality , true, request, "purchase_quality_period");
				}
			}else{
				TDate tDated = new TDate();
				tDated.addDay(+2);
				purchaseRow.add("followup_date", tDated.formatDate("yyyy-MM-dd HH:mm:ss"));
				
				if(null != schedule){
					String contentDrawbacking	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+","+context;
					scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
							 ProcessKey.QUALITY_INSPECTION , contentDrawbacking , false, request, "purchase_quality_period");
				}
			}
			floorPurchaseMgr.updatePurchaseBasic(purchaseRow, purchase_id);
			
			return backUrl;
		} catch (Exception e) {
			throw new SystemException(e, "handlePurchaseQualityInspectionAjax", log);
		}
	}
	
	@Override
	public String handlePurchaseProductModelAjax(HttpServletRequest request) throws Exception{
		try{
			String backUrl		= StringUtil.getString(request, "backurl");
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate			= new TDate();
			int product_model	= StringUtil.getInt(request, "product_model");
			long purchase_id	= StringUtil.getLong(request, "purchase_id");
			String eta			= StringUtil.getString(request, "eta");
			String context		= StringUtil.getString(request, "context");
			
			PurchaseProductModelKey purchaseProductModelKey = new PurchaseProductModelKey();
			DBRow dbrow=new DBRow();
			dbrow.add("followup_type", ProcessKey.PURCHASE_RODUCTMODEL);//表示是商品范例
			//如果获取到的是0，说明select不可用，只有完成时不可用，所以现在质检处于完成状态
			if(0 == product_model){
				product_model = PurchaseProductModelKey.FINISH;
			}
			dbrow.add("followup_type_sub", product_model);
			dbrow.add("purchase_id", purchase_id);
			dbrow.add("followup_content",context);
			dbrow.add("follower_id",adminLoggerBean.getAdid());
			dbrow.add("follower",adminLoggerBean.getEmploye_name());
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			if(product_model != PurchaseProductModelKey.FINISH){
				dbrow.add("followup_expect_date", eta);
			}
			floorPurchaseMgr.addPurchasefollowuplogs(dbrow);
			
			//更新主单据
			DBRow purchaseRow = new DBRow();
			DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
			if(product_model == PurchaseProductModelKey.FINISH){
				purchaseRow.add("product_model", product_model);
	//			DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
				//查询是否有进入阶段中的日志，如果有，就用此日期而不用采购单创建的时间
				DBRow purchaseLog = floorPurchaseMgrZyj.getFollowUpLogsByPurchaseIdAndTypeActivity(purchase_id, ProcessKey.PURCHASE_RODUCTMODEL, PurchaseProductModelKey.NEED_PRODUCT_MODEL);
				TDate endDate = new TDate();
				if(null == purchaseLog){
					endDate = new TDate(purchase.getString("purchase_date")+"00:00:00");
				}else{
					endDate = new TDate(purchaseLog.getString("followup_date")+"00:00:00");
				}
				double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
				purchaseRow.add("product_model_over", diffDay);
				
				if(null != schedule){
						String contentQuality	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+","+context;
						scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
								ProcessKey.PURCHASE_RODUCTMODEL , contentQuality , true, request, "purchase_product_model");
				}
			}else{
				TDate tDated = new TDate();
				tDated.addDay(+2);
				purchaseRow.add("followup_date", tDated.formatDate("yyyy-MM-dd HH:mm:ss"));
				
				if(null != schedule){
					String contentDrawbacking	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+","+context;
					scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
							 ProcessKey.PURCHASE_RODUCTMODEL , contentDrawbacking , false, request, "purchase_product_model");
				}
			}
			floorPurchaseMgr.updatePurchaseBasic(purchaseRow, purchase_id);
			
			return backUrl;
		} catch (Exception e) {
			throw new SystemException(e, "handlePurchaseQualityInspectionAjax", log);
		}
	}
	
	/**
	 * 将无法提供照片的图片关联给商品
	 * @param arrayPcId
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param fileNames
	 * @param adid
	 * @throws Exception
	 */
	public void handlePurchaseProductPictureUpToUnableToProvidePhoto(String[] arrayPcId,long file_with_id,int file_with_type,int file_with_class,String fileNames, long adid,int is_unable_to_provide)throws Exception{
		try 
		{
			for(int index = 0 , count = arrayPcId.length ; index < count ; index++){
				//先查询是否存在无法提供照片，再确定是否添加
				int[] file_with_types = {file_with_type};
				int[] file_types = {file_with_class};
				DBRow[] isUnableToProvides = floorProductMgrZyj.getProductFileByPcIdFileTypesAndIsUnableProvide(Long.parseLong(arrayPcId[index]),file_with_id, file_with_types, file_types, is_unable_to_provide);
				if(0 == isUnableToProvides.length)
				{
					DBRow file = new DBRow();
					file.add("file_name",fileNames);
					file.add("file_with_id",file_with_id);
					file.add("file_with_type",file_with_type);
					file.add("product_file_type",file_with_class);
					file.add("upload_adid",adid);
					file.add("upload_time",DateUtil.NowStr());
					file.add("pc_id", arrayPcId[index]);
					file.add("is_unable_to_provide", is_unable_to_provide);
					floorTransportMgrZr.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "handlePurchaseProductPictureUpToUnableToProvidePhoto", log);
		}
	}
	
	@Override
	public void handlePurchaseProductPictureUp(HttpServletRequest request)throws Exception{
		try {
			// 这里上传的文件是保存在product_file这张表中pc_id
			// 保存的文件名为T_product_121212_商品分类_index(商品分类)
			//1=商品包装
			//2=商品本身
			//3=商品底贴
			//4=商品称重
			//5=商品标签
			// 记录日志 日志中的分类算在 单证下面
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			String pc_id = StringUtil.getString(request,"pc_id");	 //商品的Id
			String[] arrayPcId = pc_id.split(",");
			long file_with_id = StringUtil.getLong(request, "purchase_id");	//关联的业务Id
			int file_with_type = StringUtil.getInt(request,"file_with_type");
			int file_with_class = StringUtil.getInt(request,"file_with_class"); // 这个目前写成商品的那种标签 比如 是商品本身，称重ID等等。
			String file_with_className = StringUtil.getString(request,"file_with_class_name"); //是商品本身，称重等等
			String sn = StringUtil.getString(request,"sn");
			String path = StringUtil.getString(request,"path");
			String fileNames = StringUtil.getString(request, "file_names");
			int is_unable_to_provide = StringUtil.getInt(request, "is_unable_to_provide");
			if(1 == is_unable_to_provide)
			{
				this.handlePurchaseProductPictureUpToUnableToProvidePhoto(arrayPcId,file_with_id,file_with_type,file_with_class,fileNames,adminLoggerBean.getAdid(),is_unable_to_provide);
			}
			else
			{
				TDate tDate = new TDate();
				String[] fileNameArray = null ;
				if(fileNames.trim().length() > 0 ){
					fileNameArray = fileNames.trim().split(",");
				}
				
				if(fileNameArray != null && fileNameArray.length > 0){
					StringBuffer logFileNameContent = new StringBuffer();
					String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
					for(String tempFileName : fileNameArray){
						String tempSuffix = getSuffix(tempFileName);
						String tempfileUpName = tempFileName.replace("."+tempSuffix, "");
						StringBuffer realyFileName = new StringBuffer(sn);
						realyFileName.append("_").append(file_with_id).append("_").append(tempfileUpName);
						int indexFile = floorTransportMgrZr.getIndexOfProductFileByFileName(realyFileName.toString());
						if(indexFile != 0){realyFileName.append("_").append(indexFile);}
						realyFileName.append(".").append(tempSuffix);
						logFileNameContent.append(",").append(realyFileName.toString());
						
						String  tempUrl =  baseTempUrl+tempFileName;
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realyFileName.toString());
						FileUtil.moveFile(tempUrl,url.toString());
						for(int index = 0 , count = arrayPcId.length ; index < count ; index++){
							DBRow file = new DBRow();
							file.add("file_name",realyFileName.toString());
							file.add("file_with_id",file_with_id);
							file.add("file_with_type",file_with_type);
							file.add("product_file_type",file_with_class);
							file.add("upload_adid",adminLoggerBean.getAdid());
							file.add("upload_time",DateUtil.NowStr());
							file.add("pc_id", arrayPcId[index]);
							floorTransportMgrZr.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
							// 添加一条日志的记录
						}
					}
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "handlePurchaseProductPictureUp", log);
		}
	}
	/**
	 * 上传采购单的质检要求
	 * @param request
	 * @return
	 */
	@Override
	public void uploadPurchaseQualityInspection(HttpServletRequest request, long purchase_id, int quality_inspection) throws Exception
	{
		try {
			AdminLoginBean adminLoggerBean	= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate						= new TDate();
			long file_with_id				= purchase_id;
			int file_with_type				= StringUtil.getInt(request, "file_with_type");
			int file_with_class 			= quality_inspection;
			String sn						= StringUtil.getString(request, "sn");
			String path						= StringUtil.getString(request, "path");
			String fileNames				= StringUtil.getString(request, "file_names");
			String baseTempUrl				= Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp				= fileNames.trim().length()>0?true:false;
			StringBuffer fileNameSb			= new StringBuffer("");
			if(isFileUp)
			{
				String[] fileNameArray		= fileNames.split(",");
				if(fileNameArray.length > 0)
				{
					for (String tempFileName : fileNameArray) {
						String suffix	= getSuffix(tempFileName);
						String tempUrl	= baseTempUrl + tempFileName;
						StringBuffer realFileName	= new StringBuffer(sn)
									.append("_").append(file_with_id)
									.append("_").append(tempFileName.replace("."+suffix, ""));
						int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
						if(0 != indexFileName)
						{
							realFileName.append("_").append(indexFileName);
						}
						realFileName.append(".").append(suffix);
						fileNameSb.append(",").append(realFileName);
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append("upload/").append(path)
							.append("/").append(realFileName.toString());
						FileUtil.moveFile(tempUrl, url.toString());
						this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
					}
				}
			}
		} catch (Exception e) {
			throw new SystemException(e, "updatePurchaseQualityInspection", log);
		}
	}
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
	}
	/**
	 * 文件不能为重复的。就是说文件如果有了，那么就会覆盖掉原来数据库中的数据
	 * @param fileName
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param upload_adid
	 * @param upload_time
	 * @throws Exception
	 */
	// 在File表中添加一个字段
	private void addTransportFileLogs(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception{
		try{
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",upload_adid);
			file.add("upload_time",DateUtil.NowStr());
			floorTransportMgrZr.addFileNotExits(file); //// 都是插入的同一张表 所以用这个方法;
		}catch (Exception e) {
			 throw new SystemException(e, "addTransportLogs", log);
		}
	}
	
	/**
	 * 得到需要跟进的采购单数
	 * @return
	 * @throws Exception
	 */
	public DBRow getPurchaseNeedFollowUpCount(long product_line_id)throws Exception
	{
		try 
		{
			//发票的周期及采购单数
			int purchase_invoice_period			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_invoice_period"));
			int[] invoice_activitys				= {InvoiceKey.INVOICING};
			int purchase_need_invoice			= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountByProcessKey(purchase_invoice_period, PurchaseLogTypeKey.INVOICE, invoice_activitys, product_line_id);
			//退税的周期及采购单数
			int purchase_drawback_period		= Integer.parseInt(systemConfig.getStringConfigValue("purchase_drawback_period"));
			int[] drawback_activitys			= {DrawbackKey.DRAWBACKING};
			int purchase_need_drawback			= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountByProcessKey(purchase_drawback_period, PurchaseLogTypeKey.DRAWBACK, drawback_activitys, product_line_id);
			//内部标签的周期及采购单数
			int purchase_tag_period				= Integer.parseInt(systemConfig.getStringConfigValue("purchase_tag_period"));
			int[] need_tag_activitys			= {TransportTagKey.TAG};
			int purchase_need_tag				= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountByProcessKey(purchase_tag_period, PurchaseLogTypeKey.NEED_TAG, need_tag_activitys, product_line_id);
			//第三方标签的周期及采购单数
			int purchase_tag_period_third		= Integer.parseInt(systemConfig.getStringConfigValue("purchase_third_tag_period"));
			int[] need_tag_activitys_third		= {TransportTagKey.TAG};
			int purchase_need_tag_third			= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountByProcessKey(purchase_tag_period_third, PurchaseLogTypeKey.THIRD_TAG, need_tag_activitys_third, product_line_id);
			//质检要求周期及采购单数
			int purchase_quality_period			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_quality_period"));
			int[] quality_activitys				= {QualityInspectionKey.NEED_QUALITY};
			int purchase_need_quality			= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountByProcessKey(purchase_quality_period, PurchaseLogTypeKey.QUALITY, quality_activitys, product_line_id);
			//商品范例周期及采购单数
			int purchase_product_model			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_product_model"));
			int[] product_activitys				= {PurchaseProductModelKey.NEED_PRODUCT_MODEL};
			int purchase_need_product_model		= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountByProcessKey(purchase_product_model, PurchaseLogTypeKey.PRODUCT_MODEL, product_activitys, product_line_id);
			//需要确认价格
			int purchase_price_period			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_price_period"));
			int[] need_affirm_price				= {PurchaseKey.OPEN};
			int purchase_need_affirm_price		= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountByProcessKey(purchase_price_period, PurchaseLogTypeKey.PRICE, need_affirm_price, product_line_id);
			//价格已确认
			int purchase_had_affirm_price		= Integer.parseInt(systemConfig.getStringConfigValue("purchase_had_affirm_price"));
			int[] had_affirm_price				= {PurchaseKey.AFFIRMTRANSFER};
			int purchase_affirm_price			= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountByProcessKey(purchase_had_affirm_price, PurchaseLogTypeKey.PRICE, had_affirm_price, product_line_id);
			//到货完成审核中
			int purchase_arrive_examine			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_arrive_examine"));
			int[] had_arrive_examine			= {PurchaseKey.APPROVEING};
			int purchase_arrival_examine		= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountByProcessKey(purchase_arrive_examine, PurchaseLogTypeKey.PRICE, had_arrive_examine, product_line_id);
			
			//需要跟进的采购单数
			DBRow purchaseNeedFollowUpCount		= new DBRow();
			int purchase_need_follow_up_count	= purchase_need_invoice + purchase_need_drawback + purchase_need_tag + purchase_need_tag_third
												+ purchase_need_quality + purchase_need_product_model
												+ purchase_need_affirm_price + purchase_affirm_price + purchase_arrival_examine;
			purchaseNeedFollowUpCount.add("purchase_need_follow_up_count", purchase_need_follow_up_count);
			purchaseNeedFollowUpCount.add("purchase_need_invoice", purchase_need_invoice);
			purchaseNeedFollowUpCount.add("purchase_need_drawback", purchase_need_drawback);
			purchaseNeedFollowUpCount.add("purchase_need_tag", purchase_need_tag);
			purchaseNeedFollowUpCount.add("purchase_need_tag_third", purchase_need_tag_third);
			purchaseNeedFollowUpCount.add("purchase_need_quality", purchase_need_quality);
			purchaseNeedFollowUpCount.add("purchase_need_product_model", purchase_need_product_model);
			purchaseNeedFollowUpCount.add("purchase_need_affirm_price", purchase_need_affirm_price);
			purchaseNeedFollowUpCount.add("purchase_affirm_price", purchase_affirm_price);
			purchaseNeedFollowUpCount.add("purchase_arrival_examine", purchase_arrival_examine);
			return purchaseNeedFollowUpCount;
		} 
		catch (Exception e) 
		{
			 throw new SystemException(e, "getPurchaseNeedFollowUpCount", log);
		}
	}
	
	/**
	 * 得到各产品线下需要跟进的采购单数
	 */
	public DBRow[] getProductLineNeedFollowUpPurchaseCount()throws Exception
	{
		try
		{
			//line_id	line_name	line_count
			HashMap<String, HashMap<String, String>> maps = new HashMap<String, HashMap<String,String>>();
			//发票的周期及采购单数
			int purchase_invoice_period			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_invoice_period"));
			int[] invoice_activitys				= {InvoiceKey.INVOICING};
			DBRow[] purchase_need_invoice		= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(purchase_invoice_period, PurchaseLogTypeKey.INVOICE, invoice_activitys);
			//退税的周期及采购单数
			int purchase_drawback_period		= Integer.parseInt(systemConfig.getStringConfigValue("purchase_drawback_period"));
			int[] drawback_activitys			= {DrawbackKey.DRAWBACKING};
			DBRow[] purchase_need_drawback		= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(purchase_drawback_period, PurchaseLogTypeKey.DRAWBACK, drawback_activitys);
			//制签的周期及采购单数
			int purchase_tag_period				= Integer.parseInt(systemConfig.getStringConfigValue("purchase_tag_period"));
			int[] need_tag_activitys			= {TransportTagKey.TAG};
			DBRow[] purchase_need_tag			= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(purchase_tag_period, PurchaseLogTypeKey.NEED_TAG, need_tag_activitys);
			//质检要求周期及采购单数
			int purchase_quality_period			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_quality_period"));
			int[] quality_activitys				= {QualityInspectionKey.NEED_QUALITY};
			DBRow[] purchase_need_quality		= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(purchase_quality_period, PurchaseLogTypeKey.QUALITY, quality_activitys);
			//商品范例周期及采购单数
			int purchase_product_model			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_product_model"));
			int[] product_activitys				= {PurchaseProductModelKey.NEED_PRODUCT_MODEL};
			DBRow[] purchase_need_product_model	= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(purchase_product_model, PurchaseLogTypeKey.PRODUCT_MODEL, product_activitys);
			//需要确认价格
			int purchase_price_period			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_price_period"));
			int[] need_affirm_price				= {PurchaseKey.OPEN};
			DBRow[] purchase_need_affirm_price	= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(purchase_price_period, PurchaseLogTypeKey.PRICE, need_affirm_price);
			//价格已确认
			int purchase_had_affirm_price		= Integer.parseInt(systemConfig.getStringConfigValue("purchase_had_affirm_price"));
			int[] had_affirm_price				= {PurchaseKey.AFFIRMTRANSFER};
			DBRow[] purchase_affirm_price		= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(purchase_had_affirm_price, PurchaseLogTypeKey.PRICE, had_affirm_price);
			//到货完成审核中
			int purchase_arrive_examine			= Integer.parseInt(systemConfig.getStringConfigValue("purchase_arrive_examine"));
			int[] had_arrive_examine			= {PurchaseKey.APPROVEING};
			DBRow[] purchase_arrival_examine	= floorPurchaseMgrZyj.getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(purchase_arrive_examine,PurchaseLogTypeKey.PRICE, had_arrive_examine);
			
			//遍历发票，得到某些产品线需要跟进数
			for (int i = 0; i < purchase_need_invoice.length; i++) {
				DBRow invoiceRow		= purchase_need_invoice[i];
				long productLineId		= invoiceRow.get("product_line_id", 0L);
				String productLineName	= invoiceRow.getString("product_line_name");
				int purchaseCount		= invoiceRow.get("purchase_need_follow_count", 0);
				HashMap<String, String> invoiceMap = maps.get(String.valueOf(productLineId));
				if(null == invoiceMap)
				{
					invoiceMap = new HashMap<String, String>();
					invoiceMap.put(productLineName, String.valueOf(purchaseCount));
					maps.put(String.valueOf(productLineId), invoiceMap);
				}
				else
				{
					invoiceMap.put(productLineName, String.valueOf(Integer.parseInt(invoiceMap.get(productLineName))+purchaseCount));
					maps.put(String.valueOf(productLineId), invoiceMap);
				}
			}
			//遍历退税
			for (int i = 0; i < purchase_need_drawback.length; i++) {
				DBRow drawbackRow		= purchase_need_drawback[i];
				long productLineId		= drawbackRow.get("product_line_id", 0L);
				String productLineName	= drawbackRow.getString("product_line_name");
				int purchaseCount		= drawbackRow.get("purchase_need_follow_count", 0);
				HashMap<String, String> drawbackMap = maps.get(String.valueOf(productLineId));
				if(null == drawbackMap)
				{
					drawbackMap = new HashMap<String, String>();
					drawbackMap.put(productLineName, String.valueOf(purchaseCount));
					maps.put(String.valueOf(productLineId), drawbackMap);
				}
				else
				{
					drawbackMap.put(productLineName, String.valueOf(Integer.parseInt(drawbackMap.get(productLineName))+purchaseCount));
					maps.put(String.valueOf(productLineId), drawbackMap);
				}
			}
			//遍历制签
			for (int i = 0; i < purchase_need_tag.length; i++) {
				DBRow needTagRow		= purchase_need_tag[i];
				long productLineId		= needTagRow.get("product_line_id", 0L);
				String productLineName	= needTagRow.getString("product_line_name");
				int purchaseCount		= needTagRow.get("purchase_need_follow_count", 0);
				HashMap<String, String> needTagMap = maps.get(String.valueOf(productLineId));
				if(null == needTagMap)
				{
					needTagMap = new HashMap<String, String>();
					needTagMap.put(productLineName, String.valueOf(purchaseCount));
					maps.put(String.valueOf(productLineId), needTagMap);
				}
				else
				{
					needTagMap.put(productLineName, String.valueOf(Integer.parseInt(needTagMap.get(productLineName))+purchaseCount));
					maps.put(String.valueOf(productLineId), needTagMap);
				}
			}
			//遍历质检要求
			for (int i = 0; i < purchase_need_quality.length; i++) {
				DBRow needQualityRow	= purchase_need_quality[i];
				long productLineId		= needQualityRow.get("product_line_id", 0L);
				String productLineName	= needQualityRow.getString("product_line_name");
				int purchaseCount		= needQualityRow.get("purchase_need_follow_count", 0);
				HashMap<String, String> needQualityMap = maps.get(String.valueOf(productLineId));
				if(null == needQualityMap)
				{
					needQualityMap = new HashMap<String, String>();
					needQualityMap.put(productLineName, String.valueOf(purchaseCount));
					maps.put(String.valueOf(productLineId), needQualityMap);
				}
				else
				{
					needQualityMap.put(productLineName, String.valueOf(Integer.parseInt(needQualityMap.get(productLineName))+purchaseCount));
					maps.put(String.valueOf(productLineId), needQualityMap);
				}
			}
			//遍历商品范例
			for (int i = 0; i < purchase_need_product_model.length; i++) {
				DBRow productModelRow	= purchase_need_product_model[i];
				long productLineId		= productModelRow.get("product_line_id", 0L);
				String productLineName	= productModelRow.getString("product_line_name");
				int purchaseCount		= productModelRow.get("purchase_need_follow_count", 0);
				HashMap<String, String> productModelMap = maps.get(String.valueOf(productLineId));
				if(null == productModelMap)
				{
					productModelMap = new HashMap<String, String>();
					productModelMap.put(productLineName, String.valueOf(purchaseCount));
					maps.put(String.valueOf(productLineId), productModelMap);
				}
				else
				{
					productModelMap.put(productLineName, String.valueOf(Integer.parseInt(productModelMap.get(productLineName))+purchaseCount));
					maps.put(String.valueOf(productLineId), productModelMap);
				}
			}
			
			//需要确认价格
			for (int i = 0; i < purchase_need_affirm_price.length; i++) {
				DBRow needNeedAffirmRow	= purchase_need_affirm_price[i];
				long productLineId		= needNeedAffirmRow.get("product_line_id", 0L);
				String productLineName	= needNeedAffirmRow.getString("product_line_name");
				int purchaseCount		= needNeedAffirmRow.get("purchase_need_follow_count", 0);
				HashMap<String, String> needNeedAffirmMap = maps.get(String.valueOf(productLineId));
				if(null == needNeedAffirmMap)
				{
					needNeedAffirmMap = new HashMap<String, String>();
					needNeedAffirmMap.put(productLineName, String.valueOf(purchaseCount));
					maps.put(String.valueOf(productLineId), needNeedAffirmMap);
				}
				else
				{
					needNeedAffirmMap.put(productLineName, String.valueOf(Integer.parseInt(needNeedAffirmMap.get(productLineName))+purchaseCount));
					maps.put(String.valueOf(productLineId), needNeedAffirmMap);
				}
			}
			//价格已确认
			for (int i = 0; i < purchase_affirm_price.length; i++) {
				DBRow needAffirmRow		= purchase_affirm_price[i];
				long productLineId		= needAffirmRow.get("product_line_id", 0L);
				String productLineName	= needAffirmRow.getString("product_line_name");
				int purchaseCount		= needAffirmRow.get("purchase_need_follow_count", 0);
				HashMap<String, String> needAffirmMap = maps.get(String.valueOf(productLineId));
				if(null == needAffirmMap)
				{
					needAffirmMap = new HashMap<String, String>();
					needAffirmMap.put(productLineName, String.valueOf(purchaseCount));
					maps.put(String.valueOf(productLineId), needAffirmMap);
				}
				else
				{
					needAffirmMap.put(productLineName, String.valueOf(Integer.parseInt(needAffirmMap.get(productLineName))+purchaseCount));
					maps.put(String.valueOf(productLineId), needAffirmMap);
				}
			}
			//到货完成审核中
			for (int i = 0; i < purchase_arrival_examine.length; i++) {
				DBRow needArrivalRow		= purchase_arrival_examine[i];
				long productLineId		= needArrivalRow.get("product_line_id", 0L);
				String productLineName	= needArrivalRow.getString("product_line_name");
				int purchaseCount		= needArrivalRow.get("purchase_need_follow_count", 0);
				HashMap<String, String> needArrivalMap = maps.get(String.valueOf(productLineId));
				if(null == needArrivalMap)
				{
					needArrivalMap = new HashMap<String, String>();
					needArrivalMap.put(productLineName, String.valueOf(purchaseCount));
					maps.put(String.valueOf(productLineId), needArrivalMap);
				}
				else
				{
					needArrivalMap.put(productLineName, String.valueOf(Integer.parseInt(needArrivalMap.get(productLineName))+purchaseCount));
					maps.put(String.valueOf(productLineId), needArrivalMap);
				}
			}
			
			//将map放在DBRow里面
			DBRow[] dbRows = new DBRow[maps.size()];
			Set<Map.Entry<String, HashMap<String,String>>> s = maps.entrySet();
			int count = 0;
	        for (Iterator iterator = s.iterator(); iterator.hasNext();) {
	        	DBRow dbRow = new DBRow();
				Map.Entry<String, HashMap<String, String>> entry = (Map.Entry<String, HashMap<String, String>>) iterator.next();
				dbRow.add("product_line_id", entry.getKey());
				HashMap<String, String> map = entry.getValue();
				Set<Map.Entry<String, String>> set = map.entrySet();
				for (Iterator iterator2 = set.iterator(); iterator2.hasNext();) {
					Map.Entry<String, String> entry2 = (Map.Entry<String, String>) iterator2.next();
					dbRow.add("product_line_name", entry2.getKey());
					dbRow.add("purchase_need_follow_count", entry2.getValue());
				}
				dbRows[count++] = dbRow;
			}
			return dbRows;
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "getProductLineNeedFollowUpPurchaseCount", log);
		}
	}
	
	private int getProcessPeriodDay(int processKey, int activity) throws Exception
	{
		try 
		{
			int period = 1;
			if(PurchaseLogTypeKey.INVOICE == processKey) 
			{
				period = Integer.parseInt(systemConfig.getStringConfigValue("purchase_invoice_period"));
			}
			else if(PurchaseLogTypeKey.DRAWBACK == processKey)
			{
				period = Integer.parseInt(systemConfig.getStringConfigValue("purchase_drawback_period"));
			}
			else if(PurchaseLogTypeKey.NEED_TAG == processKey)
			{
				period = Integer.parseInt(systemConfig.getStringConfigValue("purchase_tag_period"));
			}
			else if(PurchaseLogTypeKey.QUALITY == processKey)
			{
				period = Integer.parseInt(systemConfig.getStringConfigValue("purchase_quality_period"));
			}
			else if(PurchaseLogTypeKey.PRODUCT_MODEL == processKey)
			{
				period = Integer.parseInt(systemConfig.getStringConfigValue("purchase_product_model"));
			}
			else if(PurchaseLogTypeKey.PRICE == processKey)
			{
				if(PurchaseKey.OPEN == activity)
				{
					period = Integer.parseInt(systemConfig.getStringConfigValue("purchase_price_period"));
				}
				else if(PurchaseKey.AFFIRMTRANSFER == activity)
				{
					period = Integer.parseInt(systemConfig.getStringConfigValue("purchase_had_affirm_price"));
				}
				else if(PurchaseKey.APPROVEING == activity)
				{
					period = Integer.parseInt(systemConfig.getStringConfigValue("purchase_arrive_examine"));
				}
			}
			return period;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "getProcessPeriodDay", log);
		}
	}
	
	/**
	 * 通过产品线和阶段查询采购单列表
	 */
	public DBRow[] getPurchaseRowsByProductLineIdAndProcessKey(long productLineId, int processKey, int activity, PageCtrl pc) throws Exception
	{
		try 
		{
			return floorPurchaseMgrZyj.getPurchaseRowsByProductLineIdAndProcessKey(productLineId, processKey, activity, getProcessPeriodDay(processKey,activity), pc);
		}
		catch (Exception e)
		{
			throw new SystemException(e, "getPurchaseRowsByProductLineIdAndProcessKey", log);
		}
	}
	/**
	 * 由于采购单的创建者ID未存，因此需要将创建者Id更新进来
	 * 获取所有采购单的创建者名字和id,关联人员表
	 * @return
	 * @throws Exception
	 */
	@Override
	public void handPurchaseCreatorId()throws Exception
	{
		try
		{
			DBRow[] purchaseAdminRows = floorPurchaseMgrZyj.getAllPurchaseAdmin();
			for (int i = 0; i < purchaseAdminRows.length; i++) {
				long adid = purchaseAdminRows[i].get("adid", 0L);
				long purchaseId = purchaseAdminRows[i].get("purchase_id", 0L);
				DBRow row = new DBRow();
				row.add("proposer_id", adid);
				floorPurchaseMgr.updatePurchaseBasic(row, purchaseId);
			}
			
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "handPurchaseCreatorId", log);
		}
	}
	
	/**
	 * 根据当前登录者的Id，获取其上次创建的采购单的ID
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public String getPurchaseLastTimeCreateByAdid(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean	= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			DBRow row = floorPurchaseMgrZyj.getPurchaseLastTimeCreateByAdid(adminLoggerBean.getAdid());
			if(null == row)
			{
				return "";
			}
			else
			{
				return row.getString("purchase_id");
			}
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "handPurchaseCreatorId", log);
		}
	}
	
	/**
	 * 处理采购单的价格日志，如果没有，加日志
	 * @throws Exception
	 */
	@Override
	public void handlePurchaseDataByPriceState() throws Exception
	{
		try 
		{
			int count = 0;
			TDate tDate			= null;
			DBRow[] allPurchase = floorPurchaseMgrZyj.getAllPurchase();
			for (int i = 0; i < allPurchase.length; i++) {
				DBRow purchase = allPurchase[i];
				long purchase_id	= purchase.get("purchase_id", 0L);
				int purchase_state	= purchase.get("purchase_status", 0);
				if(PurchaseKey.OPEN == purchase_state || PurchaseKey.AFFIRMTRANSFER == purchase_state || PurchaseKey.APPROVEING == purchase_state)
				{
					long creatorId		= purchase.get("proposer_id", 0L);
					String creatorNa	= purchase.getString("proposer");
					String purchase_date = purchase.getString("purchase_date");
					tDate				= new TDate(purchase_date);
					
					if(0 == floorPurchaseMgrZyj.getPurchaseLogsByPurchaseIdAndProcessKey(purchase_id, PurchaseLogTypeKey.PRICE, purchase_state).get("purchaseCountByProcessActivity", 0))
					{
						count ++;
						DBRow dbrow=new DBRow();
						dbrow.add("followup_type", PurchaseLogTypeKey.PRICE);
						dbrow.add("followup_type_sub", purchase_state);
						dbrow.add("purchase_id", purchase_id);
						String context = "";
						if(PurchaseKey.OPEN == purchase_state)
						{
							context = creatorNa+"创建了采购单";
						}
						else if(PurchaseKey.AFFIRMTRANSFER == purchase_state)
						{
							context = "确认了价格";
							DBRow[] logs = floorPurchaseMgrZyj.getPurchaseLogsByPurchaseIdAndCountent(purchase_id, context);
							if(logs.length > 0)
							{
								creatorId = logs[0].get("follower_id", 0L);
								creatorNa = logs[0].getString("follower");
								context = logs[0].getString("followup_content");
								tDate	= new TDate(logs[0].getString("followup_date"));
							}
						}
						else if(PurchaseKey.APPROVEING == purchase_state)
						{
							context = "申请完成:采购单审核";
							DBRow[] logs = floorPurchaseMgrZyj.getPurchaseLogsByPurchaseIdAndCountent(purchase_id, context);
							if(logs.length > 0)
							{
								creatorId = logs[0].get("follower_id", 0L);
								creatorNa = logs[0].getString("follower");
								context = logs[0].getString("followup_content");
								tDate	= new TDate(logs[0].getString("followup_date"));
							}
						}
						dbrow.add("followup_content",context);
						dbrow.add("follower_id",creatorId);
						dbrow.add("follower",creatorNa);
						dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
						floorPurchaseMgr.addPurchasefollowuplogs(dbrow);
					}
				}
			}
			////system.out.println("count:"+count);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "handlePurchaseDataByPriceState", log);
		}
	}
	
	/**
	 * 检验采购单是否可以申请定金或创建交货
	 */
	public String getPurchaseTransportApplyPromptMessage(HttpServletRequest request) throws Exception
	{
		try 
		{
			long purchase_id				= StringUtil.getLong(request, "purchase_id");
			int deposit_or_delivery			= StringUtil.getInt(request, "deposit_or_delivery");
			StringBuffer prompt				= new StringBuffer();
			DBRow purchase					= floorPurchaseMgr.getPurchaseByPurchaseid(purchase_id);
			int purchase_status				= purchase.get("purchase_status", 0);
			boolean isNeedCheckNotStatus	= true;
			
			//申请采购单定金的判断
			if(1 == deposit_or_delivery)
			{
				//价格确认后且未取消的采购单才能申请资金
				if(!(purchase_status ==PurchaseKey.AFFIRMPRICE||purchase_status==PurchaseKey.AFFIRMTRANSFER 
						|| purchase_status==PurchaseKey.FINISH || purchase_status==PurchaseKey.DELIVERYING 
						|| purchase_status==PurchaseKey.APPROVEING))
				{
					prompt.append("价格确认后且未取消的采购单才能申请资金").append(",");
					isNeedCheckNotStatus = false;
				}
				//未申请过定金才能申请定金
				DBRow[] applyMoneyRows = floorApplyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100009,purchase_id,FinanceApplyTypeKey.PURCHASE_ORDER);
				if(applyMoneyRows.length > 0)
				{
					prompt.append("已申请过定金").append(",");
					isNeedCheckNotStatus = false;
				}
			}
			//创建交货单的判断
			else if(2 == deposit_or_delivery)
			{
				if(PurchaseKey.CANCEL == purchase_status || PurchaseKey.FINISH == purchase_status)
				{
					if(PurchaseKey.CANCEL == purchase_status)
					{
						prompt.append("采购单已取消").append(",");
					}
					else if(PurchaseKey.FINISH == purchase_status)
					{
						prompt.append("采购单已完成").append(",");
					}
					isNeedCheckNotStatus = false;
				}
				//价格确认后且未取消的采购单才能申请资金
				else if(!(purchase_status ==PurchaseKey.AFFIRMPRICE||purchase_status==PurchaseKey.AFFIRMTRANSFER 
							|| purchase_status==PurchaseKey.DELIVERYING || purchase_status==PurchaseKey.APPROVEING))
					{
						prompt.append("价格确认后且未取消的采购单才能创建交货").append(",");
						isNeedCheckNotStatus = false;
					}
			}
			if(isNeedCheckNotStatus)
			{
				String repair_terms_purchase	= purchase.getString("repair_terms");
				String labeling_terms_purchase	= purchase.getString("labeling_terms");
				String delivery_product_terms_purchase	= purchase.getString("delivery_product_terms");
				String purchase_terms_purchase	= purchase.getString("purchase_terms");
				
				if("".equals(repair_terms_purchase.trim()))
				{
					prompt.append("请完善返修条款").append(",");
				}
				if("".equals(labeling_terms_purchase.trim()))
				{
					prompt.append("请完善贴标条款").append(",");
				}
				if("".equals(delivery_product_terms_purchase.trim()))
				{
					prompt.append("请完善交货条款").append(",");
				}
				if("".equals(purchase_terms_purchase.trim()))
				{
					prompt.append("请完善其他条款").append(",");
				}
				//质检要求文件
				if(QualityInspectionKey.FINISH != purchase.get("quality_inspection", 0) && QualityInspectionKey.NO_NEED_QUALITY != purchase.get("quality_inspection", 0))
				{
					prompt.append("质检要求未完成").append(",");
				}
				//商品文件
				if(TransportProductFileKey.FINISH != purchase.get("product_model", 0) && TransportProductFileKey.NOPRODUCTFILE != purchase.get("product_model", 0))
				{
					prompt.append("商品范例未完成").append(",");
				}
			}
			if(prompt.length() > 0)
			{
				return prompt.substring(0, prompt.length()-1);
			}
			return "";
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "getPurchaseTransportApplyAlertMessage", log);
		}
	}
	
	/**
	 * 检验采购单商品文件是否可以完成
	 */
	public String checkPurchaseProductFileFinish(long purchase_id) throws Exception
	{
		try
		{
			StringBuffer prompt = new StringBuffer();
			int[]  types = {FileWithTypeKey.PURCHASE_PRODUCT_FILE};
			String value = systemConfig.getStringConfigValue("transport_product_file");
			if(!"".equals(value))
			{
				String[] arraySelected = value.split("\n");
				for (int i = 0; i < arraySelected.length; i++) {
					if(!"".equals(arraySelected[i]))
					{
						String[] typeIdName = arraySelected[i].split("=");
						if(2 == typeIdName.length)
						{
							int[] typeId 			= {Integer.parseInt(typeIdName[0])};
							String typeName			= typeIdName[1];
							DBRow[] productPackages	= floorPurchaseMgrZyj.getAllProductFilesByProductModelAndTypes(purchase_id,types, typeId);
							if(0 == productPackages.length)
							{
								prompt.append("请上传"+typeName+"的照片").append(",");
							}
						}
					}
				}
			}
			if(prompt.length() > 0)
			{
				return prompt.substring(0, prompt.length()-1);
			}
			return "";
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "checkPurchaseProductFileFinish", log);
		}
	}
	
	/**
	 * 将商品文件关联至采购单
	 */
	public void savePurchaseProductFileAssociation(HttpServletRequest request) throws Exception
	{
		try 
		{
			AdminLoginBean adminLoggerBean	= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			String file_ids		= StringUtil.getString(request, "file_ids");
			long file_with_id	= StringUtil.getLong(request, "file_with_id");
			int file_with_type	= StringUtil.getInt(request, "file_with_type");
			String pc_ids		= StringUtil.getString(request, "pc_id");
			if(!"".equals(pc_ids))
			{
				String[] pc_id = pc_ids.split(",");
				for (int m = 0; m < pc_id.length; m++) {
					if(!"".equals(file_ids))
					{
						String[] fileIdArray = file_ids.split(",");
						for (int i = 0; i < fileIdArray.length; i++) {
							long file_id			= Long.parseLong(fileIdArray[i]);
							DBRow pFileRow			= floorProductMgrZyj.getProductFileByProductFileId(file_id);
							String file_pc_id		= pFileRow.getString("pc_id");
							if(file_pc_id.equals(pc_id[m]))
							{
								String file_name		= pFileRow.getString("file_name");
								int product_file_type	= pFileRow.get("product_file_type", 0);
								
								DBRow file = new DBRow();
								file.add("file_name",file_name);
								file.add("file_with_id",file_with_id);
								file.add("file_with_type",file_with_type);
								file.add("product_file_type",product_file_type);
								file.add("upload_adid",adminLoggerBean.getAdid());
								file.add("upload_time",DateUtil.NowStr());
								file.add("pc_id", pc_id[m]);
								floorTransportMgrZr.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
							}
						}
					}
				}
			}
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "checkPurchaseProductFileFinish", log);
		}
	}
	/**
	 * 商品标签是否全部上传完全
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String checkPurchaseProductTagFileFinish(HttpServletRequest request) throws Exception
	{
		try
		{
			long file_with_id = StringUtil.getLong(request, "purchase_id");
			StringBuffer prompt	= new StringBuffer();
			
			int[] fileType	= {FileWithTypeKey.PRODUCT_TAG_FILE};
			String value = systemConfig.getStringConfigValue("purchase_tag_types");
			if(!"".equals(value))
			{
				String[] arraySelected = value.split("\n");
		  		for (int i = 0; i < arraySelected.length; i++) {
					if(!"".equals(arraySelected[i]))
					{
						String[] typeIdName	= arraySelected[i].split("=");
						if(2 == typeIdName.length)
						{
							int[] typeId	= {Integer.parseInt(typeIdName[0])};
							String typeName	= typeIdName[1];
							DBRow[] tagFile	= floorPurchaseMgrZyj.getAllProductFileByPcId(fileType, typeId, file_with_id);
							if(0 == tagFile.length)
							{
								prompt.append("请上传"+typeName+"的照片").append(",");
							}
						}
					}
				}
			}
			if(prompt.length() > 0)
			{
				return prompt.substring(0, prompt.length()-1);
			}
			return "";
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "checkPurchaseProductTagFileFinish", log);
		}
	}
	
	/**
	 * 商品标签是否全部上传完全
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public boolean checkPurchaseThirdTagFileFinish(long file_with_id, int[] fileType, String tag_types) throws Exception
	{
		try
		{
			boolean isHas = true;
			String value = systemConfig.getStringConfigValue(tag_types);
			if(!"".equals(value))
			{
				String[] arraySelected = value.split("\n");
		  		for (int i = 0; i < arraySelected.length; i++) {
					if(!"".equals(arraySelected[i]))
					{
						String[] typeIdName	= arraySelected[i].split("=");
						if(2 == typeIdName.length)
						{
							int[] typeId	= {Integer.parseInt(typeIdName[0])};
							DBRow[] tagFile	= floorPurchaseMgrZyj.getAllProductFileByPcId(fileType, typeId, file_with_id);
							if(0 == tagFile.length)
							{
								isHas = false;
								break;
							}
						}
					}
				}
			}
			return isHas;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "checkPurchaseThirdTagFileFinish", log);
		}
	}
	
	/**
	 * 修改商品图片流程的状态从完成到需要
	 */
	public void updatePurchasePcModelFinishToNeed(HttpServletRequest request) throws Exception
	{
		try 
		{
			//修改商品范例流程的状态
			long purchase_id	= StringUtil.getLong(request, "purchase_id");
			String context		= StringUtil.getString(request, "context");
			String e_finish_time= StringUtil.getString(request, "eta");
			DBRow row			= new DBRow();
			row.add("product_model", PurchaseProductModelKey.NEED_PRODUCT_MODEL);
			floorPurchaseMgr.updatePurchase(purchase_id, row);
			//加日志
			AdminLoginBean adminLoggerBean	=((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate						= new TDate();
			DBRow dbrow=new DBRow();
			dbrow.add("followup_type", ProcessKey.PURCHASE_RODUCTMODEL);//表示是商品范例
			//如果获取到的是0，说明select不可用，只有完成时不可用，所以现在质检处于完成状态
			dbrow.add("followup_type_sub", PurchaseProductModelKey.NEED_PRODUCT_MODEL);
			dbrow.add("purchase_id", purchase_id);
			dbrow.add("followup_content",context+"(从完成到需要)");
			dbrow.add("follower_id",adminLoggerBean.getAdid());
			dbrow.add("follower",adminLoggerBean.getEmploye_name());
			dbrow.add("followup_date",tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			dbrow.add("followup_expect_date", e_finish_time);
			floorPurchaseMgr.addPurchasefollowuplogs(dbrow);
			//加任务跟进
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(purchase_id, ModuleKey.PURCHASE_ORDER, ProcessKey.PURCHASE_RODUCTMODEL);
			if(null != schedule)
			{
				String contentQuality	= adminLoggerBean.getEmploye_name()+"确认了采购单:"+purchase_id+"(从完成到需要),"+context;
				scheduleMgrZr.addScheduleReplayExternal(purchase_id , ModuleKey.PURCHASE_ORDER ,
						ProcessKey.PURCHASE_RODUCTMODEL , contentQuality , false, request, "purchase_product_model");
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "updatePurchasePcModelFinishToNeed", log);
		}
	}
	
	//所有采购单
	public JSONArray findPurchaseFillter(PageCtrl pc)throws Exception
	{
		DBRow[] rows = floorPurchaseMgrZyj.findPurchase(pc);
		JSONArray purchases = DBRowUtils.dbRowArrayAsJSON(rows);
		for (int i = 0; i < purchases.length(); i++) 
		{
			JSONObject row = purchases.getJSONObject(i);
			DBRow[] logs = floorPurchaseMgr.getLastNumberLogs(row.getLong("purchase_id"), 0, 4);
			row.put("logs", DBRowUtils.dbRowArrayAsJSON(logs));
		}
//		for (int i = 0; i < purchases.length(); i++) 
//		{
//			JSONObject jo = purchases.getJSONObject(i);
//			//system.out.println(jo.getLong("purchase_id"));
//			JSONArray tes = jo.getJSONArray("logs");
//			for (int j = 0; j < tes.length(); j++) {
//				//system.out.println(tes.getJSONObject(j).getString("followup_content"));
//			}
//		}
		return purchases;
	}
	
	
	public void setFloorPurchaseMgrZr(FloorPurchaseMgrZr floorPurchaseMgrZr) {
		this.floorPurchaseMgrZr = floorPurchaseMgrZr;
	}


	public void setFloorPurchaseMgr(FloorPurchaseMgr floorPurchaseMgr) {
		this.floorPurchaseMgr = floorPurchaseMgr;
	}


	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}

	public void setFloorPurchaseMgrZyj(FloorPurchaseMgrZyj floorPurchaseMgrZyj) {
		this.floorPurchaseMgrZyj = floorPurchaseMgrZyj;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setFloorProductMgrZyj(FloorProductMgrZyj floorProductMgrZyj) {
		this.floorProductMgrZyj = floorProductMgrZyj;
	}

	public void setFloorApplyMoneyMgrZZZ(FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ) {
		this.floorApplyMoneyMgrZZZ = floorApplyMoneyMgrZZZ;
	}
	
	
	
	
}
