package com.cwc.app.api.zyj;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zyj.FloorStorageCatalogMgrZyj;
import com.cwc.app.iface.zyj.StorageCatalogMgrZyjIFace;
import com.cwc.app.key.GlobalKey;
import com.cwc.app.key.ImportStorageAddressErrorKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class StorageCatalogMgrZyj implements StorageCatalogMgrZyjIFace{

	private static Logger log = Logger.getLogger("ACTION");
	private FloorStorageCatalogMgrZyj floorStorageCatalogMgrZyj;
	private FloorProductMgr floorProductMgr;
	private FloorOrderMgr floorOrderMgr;
	
	
	/**
	 * 通过ID获取仓库信息
	 */
	@Override
	public DBRow getStorageCatalogById(long storageId) throws Exception {
		try {
			return floorStorageCatalogMgrZyj.getStorageCatalogById(storageId);
		} catch (Exception e) {
			throw new SystemException(e,"TrackStatusMgrZyj.getTrackStatusListByShipCom(shipCom)",log);
		}
	}

	
	/**
	 * 保存仓库信息的同时，将仓库与商品关联
	 */
	@Override
	public long addProductStorageCatalog(HttpServletRequest request)throws Exception{
		try{
			DBRow curCatalog	= new DBRow();
			String title		= StringUtil.getString(request,"title");
			long parentid		= StringUtil.getLong(request,"parentid");
			long ccid			= StringUtil.getLong(request,"ccid");//国家
			long pro_id			= StringUtil.getLong(request,"pro_id");//省
			int storage_type	= StringUtil.getInt(request, "storage_type");
			////system.out.println(pro_id);
			String pro_input	= "";
			if(-1 == pro_id || 0 == pro_id){
				pro_input		= StringUtil.getString(request, "address_state_input");
				curCatalog.add("pro_input", pro_input);
			}
			AdminMgr adminMgr			  = new AdminMgr();
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			curCatalog.add("title",title);
			curCatalog.add("parentid",parentid);
			curCatalog.add("native",ccid);
			curCatalog.add("pro_id",pro_id);
			curCatalog.add("storage_type", storage_type);
			curCatalog.add("creator", adminLoginBean.getAdid());
			curCatalog.add("create_time", DateUtil.NowStr());
			
			long ps_id			= floorStorageCatalogMgrZyj.addProductStorageCatalog(curCatalog);
				
//			DBRow[] products	= floorProductMgr.getAllProducts(null);
//			
//			for (int i = 0; i < products.length; i++) 
//			{
//				DBRow row = new DBRow();
//				row.add("post_date",DateUtil.NowStr());
//				row.add("cid",ps_id);
//				row.add("pc_id",products[i].get("pc_id",0l));
//				row.add("store_count",0);
//				row.add("store_count_alert",0);
//				long pid = floorProductMgr.addProductStorage(row);
//			}
			
			return(ps_id);
		}catch (Exception e){
			throw new SystemException(e,"addProductStorageCatalog",log);
		}
	}
	
	/**
	 * 更新
	 */
	public long updateProductStorageCatalogBasic(HttpServletRequest request)throws Exception{
		
		try{
			DBRow curCatalog	= new DBRow();
			long storId			= StringUtil.getLong(request, "storId");
			String title		= StringUtil.getString(request,"title");
			long parentid		= StringUtil.getLong(request,"parentid");
			long ccid			= StringUtil.getLong(request,"ccid");//国家
			long pro_id			= StringUtil.getLong(request,"pro_id");//省
			int storage_type	= StringUtil.getInt(request, "storage_type");
			String pro_input	= "";
			if(-1 == pro_id || 0 == pro_id){
				pro_input		= StringUtil.getString(request, "address_state_input");
				curCatalog.add("pro_input", pro_input);
			}
	
			curCatalog.add("title",title);
			curCatalog.add("parentid",parentid);
			curCatalog.add("native",ccid);
			curCatalog.add("pro_id",pro_id);
			curCatalog.add("storage_type", storage_type);
			return floorStorageCatalogMgrZyj.updateProductStorageCatalogBasic(curCatalog, storId);
		}catch (Exception e) {
			throw new SystemException(e,"updateProductStorageCatalogBasic",log);
		}
	}
	
	/**
	 * 更新发货地址
	 */
	@Override
	public long updateProductStorageCatalogDetailSend(HttpServletRequest request)throws Exception{
		try {
			
			
			DBRow curCatalog			= new DBRow();
			long storId					= StringUtil.getLong(request, "storId");
			String send_house_number	= StringUtil.getString(request,"send_house_number");
			String send_street			= StringUtil.getString(request,"send_street");
			String send_city			= StringUtil.getString(request,"send_city");
			String send_zip_code		= StringUtil.getString(request,"send_zip_code");
			String send_contact			= StringUtil.getString(request,"send_contact");
			String send_phone			= StringUtil.getString(request,"send_phone");
			long send_nation			= StringUtil.getLong(request, "send_nation");
			long send_pro_id			= StringUtil.getLong(request, "send_pro_id");
			
			if(-1 == send_pro_id || 0 == send_pro_id){
				String send_pro_input	= StringUtil.getString(request, "send_pro_input");
				curCatalog.add("send_pro_input", send_pro_input);
			}
			
			curCatalog.add("send_house_number",send_house_number);
			curCatalog.add("send_street",send_street);
			curCatalog.add("send_city",send_city);
			curCatalog.add("send_zip_code",send_zip_code);
			curCatalog.add("send_contact",send_contact);
			curCatalog.add("send_phone",send_phone);
			curCatalog.add("send_nation", send_nation);
			curCatalog.add("send_pro_id", send_pro_id);
			
			return floorStorageCatalogMgrZyj.updateProductStorageCatalogBasic(curCatalog, storId);
		}catch (Exception e) {
			throw new SystemException(e,"updateProductStorageCatalogBasic",log);
		}
	}
	
	/**
	 * 更新收货地址
	 */
	@Override
	public long updateProductStorageCatalogDetailDeliver(HttpServletRequest request)throws Exception{
		try {
			DBRow curCatalog			= new DBRow();
			long storId					= StringUtil.getLong(request, "storId");
			String deliver_house_number	= StringUtil.getString(request,"deliver_house_number");
			String deliver_street		= StringUtil.getString(request,"deliver_street");
			String deliver_city			= StringUtil.getString(request,"deliver_city");
			String deliver_zip_code		= StringUtil.getString(request,"deliver_zip_code");
			String deliver_contact		= StringUtil.getString(request,"deliver_contact");
			String deliver_phone		= StringUtil.getString(request,"deliver_phone");
			long deliver_nation			= StringUtil.getLong(request, "deliver_nation");
			long deliver_pro_id			= StringUtil.getLong(request, "deliver_pro_id");
			
			if(-1 == deliver_pro_id || 0 == deliver_pro_id){
				String deliver_pro_input	= StringUtil.getString(request, "deliver_pro_input");
				curCatalog.add("deliver_pro_input", deliver_pro_input);
			}
			
			curCatalog.add("deliver_house_number",deliver_house_number);
			curCatalog.add("deliver_street",deliver_street);
			curCatalog.add("deliver_city",deliver_city);
			curCatalog.add("deliver_zip_code",deliver_zip_code);
			curCatalog.add("deliver_contact",deliver_contact);
			curCatalog.add("deliver_phone",deliver_phone);
			curCatalog.add("deliver_nation", deliver_nation);
			curCatalog.add("deliver_pro_id", deliver_pro_id);
			
			return floorStorageCatalogMgrZyj.updateProductStorageCatalogBasic(curCatalog, storId);
		}catch (Exception e) {
			throw new SystemException(e,"updateProductStorageCatalogDetailDeliver",log);
		}
	}
	
	@Override
	public DBRow getProviceInfoById(long proId) throws Exception{
		try {
			return floorStorageCatalogMgrZyj.getProviceInfoById(proId);
		}catch (Exception e) {
			throw new SystemException(e,"updateProductStorageCatalogBasic",log);
		}
		
	}
	
	public DBRow[] getProductStorageCatalogByType(int storageType,PageCtrl pc) 
		throws Exception
	{
		try
		{
			return(floorStorageCatalogMgrZyj.getProductStorageCatalogByType(storageType,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductStorageCatalogByParentId",log);
		}
	}
	/**
	 * 导出仓库地址本
	 */
	public String exportStorageAddressAction(HttpServletRequest request)
			throws Exception {
		try {
			int storageType = StringUtil.getInt(request,"storageType");
			StorageTypeKey storageTypeKey = new StorageTypeKey();
			
			DBRow[] exportStorageAddress = floorStorageCatalogMgrZyj.getProductStorageCatalogByType(storageType, null);
			long[] storageAddressBook = new long[exportStorageAddress.length];
			if(exportStorageAddress.length > 0){
				POIFSFileSystem fs = null;   //获取模板
				fs = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product/ExportStorageAddress.xls"));
				HSSFWorkbook wb = new HSSFWorkbook(fs);
				HSSFSheet sheet = wb.getSheetAt(0);
				HSSFRow row = sheet.getRow(0);
				HSSFCellStyle style = wb.createCellStyle();
				style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				style.setLocked(false);
				style.setWrapText(true);
				
				
				HSSFCellStyle stylelock = wb.createCellStyle();
				stylelock.setLocked(true); //创建一个锁定样式
				stylelock.setAlignment(HSSFCellStyle.ALIGN_CENTER);
				
				for(int i = 0 ; i< exportStorageAddress.length ; i++){
					row = sheet.createRow((int)i+1);
					sheet.setDefaultColumnWidth(30);
					DBRow storageAddress = exportStorageAddress[i];
					//设置excel列的值
					storageAddressBook[i] = storageAddress.get("id",0l);
					row.createCell(0).setCellValue(storageAddress.getString("title"));
					row.getCell(0).setCellStyle(style);
					
					row.createCell(1).setCellValue(floorOrderMgr.getDetailCountryCodeByCcid(storageAddress.get("native",0l)).getString("c_country"));
					row.getCell(1).setCellStyle(style);
					
					String province="";
					if(storageAddress.get("pro_id",0l)!=0 && storageAddress.get("pro_id",0l)!=-1){
						province = floorStorageCatalogMgrZyj.getProviceInfoById(storageAddress.get("pro_id",0l)).getString("pro_name");
					}else{
						province = storageAddress.getString("pro_input");
					}
					row.createCell(2).setCellValue(province);
					row.getCell(2).setCellStyle(style);
					
					row.createCell(3).setCellValue(storageTypeKey.getStorageTypeKeyName(storageAddress.getString("storage_type")));
					row.getCell(3).setCellStyle(style);
					
					String deliverNation="";
					if(storageAddress.get("deliver_nation",0l)!=0){
					
						deliverNation=floorOrderMgr.getDetailCountryCodeByCcid(storageAddress.get("deliver_nation",0l)).getString("c_country");
					}else{
						deliverNation = "";
					}
					row.createCell(4).setCellValue(deliverNation);
					row.getCell(4).setCellStyle(style);
					
					String deliverProvince="";
					if(storageAddress.get("deliver_pro_id",0l)!=0 && storageAddress.get("deliver_pro_id",0l)!=-1){
						deliverProvince = floorStorageCatalogMgrZyj.getProviceInfoById(storageAddress.get("deliver_pro_id",0l)).getString("pro_name");
					}else{
						deliverProvince = storageAddress.getString("deliver_pro_input");
					}
					row.createCell(5).setCellValue(deliverProvince);
					row.getCell(5).setCellStyle(style);
					
					row.createCell(6).setCellValue(storageAddress.getString("deliver_zip_code"));
					row.getCell(6).setCellStyle(style);
					
					row.createCell(7).setCellValue(storageAddress.getString("deliver_city"));
					row.getCell(7).setCellStyle(style);
					
					row.createCell(8).setCellValue(storageAddress.getString("deliver_street"));
					row.getCell(8).setCellStyle(style);
					
					row.createCell(9).setCellValue(storageAddress.getString("deliver_house_number"));
					row.getCell(9).setCellStyle(style);
					
					row.createCell(10).setCellValue(storageAddress.getString("deliver_contact"));
					row.getCell(10).setCellStyle(style);
					
					row.createCell(11).setCellValue(storageAddress.getString("deliver_phone"));
					row.getCell(11).setCellStyle(style);
					
					String sendNation="";
					if(storageAddress.get("send_nation",0l)!=0){
					
						sendNation=floorOrderMgr.getDetailCountryCodeByCcid(storageAddress.get("send_nation",0l)).getString("c_country");
					}else{
						sendNation = "";
					}
					row.createCell(12).setCellValue(sendNation);
					row.getCell(12).setCellStyle(style);
					
					String sendProvince="";
					if(storageAddress.get("send_pro_id",0l)!=0 && storageAddress.get("send_pro_id",0l)!=-1){
						sendProvince = floorStorageCatalogMgrZyj.getProviceInfoById(storageAddress.get("send_pro_id",0l)).getString("pro_name");
					}else{
						sendProvince = storageAddress.getString("send_pro_input");
					}
					row.createCell(13).setCellValue(sendProvince);
					row.getCell(13).setCellStyle(style);
					
					row.createCell(14).setCellValue(storageAddress.getString("send_zip_code"));
					row.getCell(14).setCellStyle(style);
					
					row.createCell(15).setCellValue(storageAddress.getString("send_city"));
					row.getCell(15).setCellStyle(style);
					
					row.createCell(16).setCellValue(storageAddress.getString("send_street"));
					row.getCell(16).setCellStyle(style);
					
					row.createCell(17).setCellValue(storageAddress.getString("send_house_number"));
					row.getCell(17).setCellStyle(style);
					
					row.createCell(18).setCellValue(storageAddress.getString("send_contact"));
					row.getCell(18).setCellStyle(style);
					
					row.createCell(19).setCellValue(storageAddress.getString("send_phone"));
					row.getCell(19).setCellStyle(style);
				}
				//写文件  
				String path = "upl_excel_tmp/export_storageAddress_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date())+".xls";
				FileOutputStream os= new FileOutputStream(Environment.getHome()+path);
				wb.write(os);  
				os.close();  
				return (path);
			}else
			{
				return "0";
			}
			
		} catch (Exception e) {
			throw new SystemException(e,"exportStorageAddressAction",log);
		}
	}
	
	/**
	 * 导入仓库地址
	 */
	public HashMap<String, DBRow[]> excelShow(String filename, String type,int storageType)throws Exception {
		
		try 
		{
			String path = "";
			InputStream is;
			Workbook wrb;
			HashMap<String,DBRow[]> resultMap = new HashMap<String, DBRow[]>();//返回值
			if (type.equals("show")) 
			{
				path = Environment.getHome() + "upl_imags_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				
				DBRow[] storageAddress = storageAddressData(wrb);
				DBRow[] errorStorageAddress = checkStorageAddress(storageAddress,storageType);//检查容器
				resultMap.put("errorStorageAddress",errorStorageAddress);
			}
			else if(type.equals("data"))
			{
				path = Environment.getHome() + "upl_imags_tmp/" + filename;
				is = new FileInputStream(path);
				wrb = Workbook.getWorkbook(is);
				storageAddressData(wrb);
				
				DBRow[] StorageAddress = storageAddressData(wrb);//DBRow[]
				
				resultMap.put("StorageAddress",StorageAddress);
			}
			
			return resultMap;
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"excelShow",log);
		}
	}
	/**
	 * 检查地址信息
	 * @param storageAddress
	 * @return
	 * @throws Exception
	 */
	private DBRow[] checkStorageAddress(DBRow[] storageAddress,int storageType) throws Exception {
	 try 
	  {
		 StorageTypeKey storageTypeKey = new StorageTypeKey();
		ArrayList<DBRow> errorStorageAddress = new ArrayList<DBRow>();
		for (int i = 0; i < storageAddress.length; i++) 
		{
			boolean checkResult = false;//检查检查结果，true表示有错，false表示没错
			StringBuffer errorMessage = new StringBuffer("");//错误信息
			DBRow rows = storageAddress[i];
			if(rows.getString("title").trim().equals(""))   //检查仓库名是否为空
			{
				checkResult = true;
				errorMessage.append(ImportStorageAddressErrorKey.STORAGE_NAME+",");
				
			}
			String storage_type = storageTypeKey.getStorageTypeKeyName(storageType);
			if(!rows.getString("storage_type").equals(storage_type))   //检查类型是否正确
			{
				checkResult = true;
				errorMessage.append(ImportStorageAddressErrorKey.STORAGE_TYPE+",");
				
			}
			if(rows.get("native",0l)==0)   //检查国家是否存在
			{
				checkResult = true;
				errorMessage.append(ImportStorageAddressErrorKey.NATIVE+",");
				
			}
			if (checkResult)//一条记录检查有问题
			{
				rows.add("errorMessage", errorMessage.toString());
				rows.add("errorStorageAddressRow",i+2);
				errorStorageAddress.add(rows);
			}
		}
		return (errorStorageAddress.toArray(new DBRow[0]));
	} 
	catch (Exception e) 
	{
		throw new SystemException(e,"checkStorageAddress(rows)",log);
	}
	
}
	
	/**
	 * 转化成DBRow类型
	 * @param wrb
	 * @return
	 * @throws Exception
	 */
	public DBRow[] storageAddressData(Workbook wrb) throws Exception
	{
		try {
			HashMap storageAddressFiled = new HashMap();
			storageAddressFiled.put(0, "title");
			storageAddressFiled.put(1, "native");
			storageAddressFiled.put(2, "pro_id");
			storageAddressFiled.put(3, "storage_type");
			storageAddressFiled.put(4, "deliver_nation");
			storageAddressFiled.put(5, "deliver_pro_id");
			storageAddressFiled.put(6, "deliver_zip_code");
			storageAddressFiled.put(7, "deliver_city");
			storageAddressFiled.put(8, "deliver_street");
			storageAddressFiled.put(9, "deliver_house_number");
			storageAddressFiled.put(10, "deliver_contact");
			storageAddressFiled.put(11, "deliver_phone");
			storageAddressFiled.put(12, "send_nation");
			storageAddressFiled.put(13, "send_pro_id");
			storageAddressFiled.put(14, "send_zip_code");
			storageAddressFiled.put(15, "send_city");
			storageAddressFiled.put(16, "send_street");
			storageAddressFiled.put(17, "send_house_number");
			storageAddressFiled.put(18, "send_contact");
			storageAddressFiled.put(19, "send_phone");
			
			ArrayList<DBRow> al = new ArrayList<DBRow>();
			Sheet rs = wrb.getSheet(0);
			int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
			  DBRow drow;
			  for(int i=1;i<rsRows;i++)
			  {
			   DBRow pro = new DBRow();

			   
			   boolean result = false;//判断是否纯空行数据
			   for (int j=0;j<rsColumns;j++)
			   {
				   	String content = rs.getCell(j,i).getContents().trim();
				   	if(j==1 || j==4 || j==12){	
			     		if(!content.equals("")&&content!=null){		   				   		
			     			drow = floorOrderMgr.getDetailCountryCodeByCountry(content);
			     			if(drow!=null){
			     				content = drow.getString("ccid");
			     			}else{
			     				content="0";
			     			}
			     		}else{
			     			content = "0";
			     		}
				    }
				   	if(j==2 || j==5 || j==13){	
			     		if(!content.equals("")&&content!=null){		   				   		
			     			drow = floorStorageCatalogMgrZyj.getProviceInfoByName(content);
			     			if(drow!=null){
			     				content = drow.getString("pro_id");
			     			}else{
			     				content="0";
			     			}
			     		}else{
			     			content = "0";
			     		}
				    }
				 
					pro.add(storageAddressFiled.get(j).toString(),content);  //转换成DBRow数组输出,去掉空格
			   
			   ArrayList proName=pro.getFieldNames();//DBRow 字段名
			   for(int p=0;p<proName.size();p++)
			    {
			    	if(!pro.getString(proName.get(p).toString()).trim().equals(""))//去掉空格
			    	{
			    		result = true;
			    	}
			    }
			   }
			   if(result)//不是纯空行就加入到DBRow
			   {
				   al.add(pro);   
			   }
			  }
			  return (al.toArray(new DBRow[0]));
			  
		} catch (Exception e) {
			throw new SystemException(e,"storageAddressData",log);
		}
	}
	/**
	 * 上传的excel导出结果集
	 */
	public void uploadStorageAddress(HttpServletRequest request) throws Exception {
		
		try {
			String tempfilename = StringUtil.getString(request,"tempfilename");
			int storageType = StringUtil.getInt(request,"storageType");
			HashMap<String,DBRow[]> excelDBRow = excelShow(tempfilename,"data",storageType);//excel导出结果集
			DBRow[] rows = excelDBRow.get("StorageAddress");
			importStorageAddress(rows);
			
		} catch (Exception e) {
			throw new SystemException(e,"uploadStorageAddress",log);
		}
	}
	/**
	 * 保存上传的地址信息
	 * @param rows
	 * @throws Exception
	 */
	public void importStorageAddress(DBRow[] rows)throws Exception{
		
			try {
				if(rows!=null){
					for(int i = 0;i < rows.length; i++){
						DBRow storageAddress = rows[i];
						if(storageAddress.getString("title").equals("")){
							storageAddress.remove("title");
						}
						int storageType;
						if(storageAddress.getString("storage_type").equals("自有仓库")){
							storageType=StorageTypeKey.SELF;
						}
						else if (storageAddress.getString("storage_type").equals("转运仓库")){
							storageType=StorageTypeKey.TRANSFOR;
						}
						else if (storageAddress.getString("storage_type").equals("供应商仓库")){
							storageType=StorageTypeKey.SUPPLIER;
						}
						else if (storageAddress.getString("storage_type").equals("第三方仓库")){
							storageType=StorageTypeKey.THIRD;
						}
						else if (storageAddress.getString("storage_type").equals("客户仓库")){
							storageType=StorageTypeKey.CUSTOMER;
						}
						else if (storageAddress.getString("storage_type").equals("返修仓库")){
							storageType=StorageTypeKey.REPAIR;
						}
						else{
							storageType=0;
						}
						if(storageAddress.get("native",0l)==0){
							storageAddress.remove("native");
						}
						storageAddress.add("storage_type", storageType);
						floorStorageCatalogMgrZyj.addProductStorageCatalog(storageAddress);
					}
				}
			} catch (Exception e) {
				throw new SystemException(e,"importStorageAddress",log);
			}
	}
	
	
	/**
	 * 通过仓库ID获得商品信息
	 * @author Administrator
	 * @param storage_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductInfoByStorageId() throws Exception
	{
		try
		{
			return floorStorageCatalogMgrZyj.getProductInfoByStorageId();
		}
		catch (Exception e)
		{
			throw new SystemException(e,
					"getProductInfoByStorageId()", log);
		}
	}
	
	/**
	 * 通过仓库ID获取商品条码信息
	 * @author Administrator
	 * @param storage_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCodeInfoByStorageId() throws Exception
	{
		try
		{
			return floorStorageCatalogMgrZyj.getProductCodeInfoByStorageId();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,
					"getProductCodeInfoByStorageId(long storage_id)", log);
		}
		
	}
	
	/**
	 * 通过仓库Id获得商品序列号
	 * @author Administrator
	 * @param storage_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSerialProductByStorageId() throws Exception 
	{
		try
		{
			return floorStorageCatalogMgrZyj.getSerialProductByStorageId();
		}
		catch (Exception e) 
		{
			throw new SystemException(e,
					"getSerialProductByStorageId(long storage_id)", log);
		}
	}
	
	/**
	 * 获取所有省份
	 */
	public DBRow[] getStorageAllProvinces() throws Exception
	{
		try
		{
			return floorStorageCatalogMgrZyj.getStorageAllProvinces();
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "getStorageAllProvinces()", log);
		}
	}

	@Override
	public DBRow[] getProductCodeByBarCode(String barCode) throws Exception {
		try
		{
			//
			DBRow[] reslut = null ;
			reslut = floorStorageCatalogMgrZyj.getProductCodeInfoByBarCode(barCode);
			long id = 0l ;
			if(reslut == null || reslut.length < 1){
				try{
					id = Long.parseLong(barCode);
					if(id != 0l ){
						DBRow single = floorStorageCatalogMgrZyj.getProductByPcid(id) ;
						single.add("p_name", single.getString("p_name"));
						single.add("pcid", single.getString("pc_id"));
						single.add("p_code", single.getString("pc_id"));
						single.add("code_type", "0");
						if(single != null){
							reslut = new DBRow[]{single};
						}
					}
				}catch (Exception e) {
 				}
			}
			return reslut;
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "getProductCodeByBarCode(barCode)", log);
		}
	}
	
	/**
	 * 通过adid、titleId查询批次号
	 */
	public DBRow[] findProductLotNumberByTitlePcIdAdmin(boolean adidNullToLogin,long adid, String title_ids, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorStorageCatalogMgrZyj.findProductLotNumberByTitlePcIdAdmin(adid, this.parseStrToLongArr(title_ids), pc_id, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "getProductLotNumberByTitleIdAdmin(boolean adidNullToLogin,long adid, long title_id, PageCtrl pc, HttpServletRequest request)", log);
		}
	}
	
	/**
	 * 通过用户，title_id,仓库，是否有货查询库存
	 */
	public DBRow[] findProductStorages(boolean adidNullToLogin,String ps_id, int type, long adid, String title_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorStorageCatalogMgrZyj.findProductStorages(this.parseStrToLongArr(ps_id), type, adid, this.parseStrToLongArr(title_id), pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "findProductStorages(boolean adidNullToLogin,long ps_id, int type, long adid, long title_id, PageCtrl pc, HttpServletRequest request)", log);
		}
	}
	
	/**
	 * 通过用户ID,商品线，商品分类，商品名或条码名，是否套装，仓库id查询商品库存
	 */
	public DBRow[] findProductStorageByName(boolean adidNullToLogin,String pc_line_id, String catalog_id,String ps_id,String code,int union_flag,long adid, String title_id, int type, String lot_number_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = findAdminIdByCondition(adidNullToLogin, adid, request);
			String[] lotNumberArr = null;
			if(!StringUtil.isBlank(lot_number_id))
			{
				lotNumberArr = lot_number_id.split(",");
			}
			return floorStorageCatalogMgrZyj.findProductStorageByName(this.parseStrToLongArr(pc_line_id), this.parseStrToLongArr(catalog_id), this.parseStrToLongArr(ps_id), code, union_flag, adid, this.parseStrToLongArr(title_id), type, lotNumberArr, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "findProductStorageByName(boolean adidNullToLogin,long pc_line_id, long catalog_id,long ps_id,String code,int union_flag,long adid, long title_id, int type, String lot_number_id, PageCtrl pc, HttpServletRequest request)", log);
		}
	}
	
	private long findAdminIdByCondition(boolean adidNullToLogin, long adid, HttpServletRequest request) throws Exception
	{
		try
		{
			AdminMgr adminMgr 				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));//获取系统当前登录者
			if(GlobalKey.CLIENT_ADGID == adminLoggerBean.getAdgid() && adidNullToLogin && 0 == adid)
			{
				adid = adminLoggerBean.getAdid();
			}
			return adid;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"findAdminIdByCondition(boolean adidNullToLogin, long adid, HttpServletRequest request)",log);
		}	
	}
	
	/**
	 * 通过adid、titleId查询批次号
	 */
	public DBRow[] findProductLotNumberIdAndNameByTitlePcIdAdmin(boolean adidNullToLogin,long adid, String title_ids, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception
	{
		try
		{
			adid = findAdminIdByCondition(adidNullToLogin, adid, request);
			return floorStorageCatalogMgrZyj.findProductLotNumberIdAndNameByTitlePcIdAdmin(adid, this.parseStrToLongArr(title_ids), pc_id, pc);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "findProductLotNumberIdAndNameByTitlePcIdAdmin(boolean adidNullToLogin,long adid, long title_id, PageCtrl pc, HttpServletRequest request)", log);
		}
	}
	
	private Long[] parseStrToLongArr(String str) throws NumberFormatException, Exception
	{
		List<Long> longList = new ArrayList<Long>();
		if(!StringUtil.isBlank(str))
		{
			String[] arr = str.split(",");
			for (int i = 0; i < arr.length; i++) 
			{
				if(!StringUtil.isBlankAndCanParseLong(arr[i]))
				{
					longList.add(Long.valueOf(arr[i]));
				}
			}
		}
		return longList.toArray(new Long[0]);
	}
	
	
	public void setFloorStorageCatalogMgrZyj(
			FloorStorageCatalogMgrZyj floorStorageCatalogMgrZyj) {
		this.floorStorageCatalogMgrZyj = floorStorageCatalogMgrZyj;
	}

	
	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}


	public void setFloorOrderMgr(FloorOrderMgr floorOrderMgr) {
		this.floorOrderMgr = floorOrderMgr;
	}

	
}
