package com.cwc.app.api.zyj;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zyj.FloorProductMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zyj.ProductMgrZyjIFace;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class ProductMgrZyj implements ProductMgrZyjIFace{
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorTransportMgrZr floorTransportMgrZr;
	private FloorProductMgrZyj floorProductMgrZyj;
	
	@Override
	public void handleProductPictureUpload(HttpServletRequest request)throws Exception{
		try {
			// 这里上传的文件是保存在product_file这张表中pc_id
			//文件名重名为:商品名_yy-MM-dd HH:mm.后缀名
			//1=商品包装
			//2=商品本身
			//3=商品底贴
			//4=商品称重
			//5=商品标签
			// 记录日志 日志中的分类算在 单证下面
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			String pc_id = StringUtil.getString(request,"pc_id");	 //商品的Id
			String[] arrayPcId = pc_id.split(",");
			String first_pc_name = StringUtil.getString(request, "first_pc_name");
			int file_with_type = StringUtil.getInt(request,"file_with_type");
			int file_with_class = StringUtil.getInt(request,"file_with_class"); // 这个目前写成商品的那种标签 比如 是商品本身，称重ID等等。
			String path = StringUtil.getString(request,"path");
		    String fileNames = StringUtil.getString(request, "file_names");
		    int is_unable_to_provide = StringUtil.getInt(request, "is_unable_to_provide");
			if(1 == is_unable_to_provide)
			{
				this.handleProductPictureUpToUnableToProvidePhoto(arrayPcId,file_with_type,file_with_class,fileNames,adminLoggerBean.getAdid(),is_unable_to_provide);
			} 
			else
			{
				String[] fileNameArray = null ;
				if(fileNames.trim().length() > 0 ){
					fileNameArray = fileNames.trim().split(",");
				}
				
				if(fileNameArray != null && fileNameArray.length > 0){
					StringBuffer logFileNameContent = new StringBuffer();
					String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
					for(String tempFileName : fileNameArray){
						if(first_pc_name.contains("/"))
						{
							first_pc_name = first_pc_name.replaceAll("/", "_");
						}
						String tempSuffix = getSuffix(tempFileName);
						StringBuffer realyFileName = new StringBuffer(first_pc_name);
						String dateStr = DateUtil.FormatDatetime("yyMMddHHmmss",new Date());
						realyFileName.append("_").append(dateStr);
						int indexFile = floorTransportMgrZr.getIndexOfProductFileByFileName(realyFileName.toString());
						if(indexFile != 0){realyFileName.append("_").append(indexFile);}
						realyFileName.append(".").append(tempSuffix);
						logFileNameContent.append(",").append(realyFileName.toString());
						String  tempUrl =  baseTempUrl+tempFileName;
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realyFileName.toString());
						FileUtil.moveFile(tempUrl,url.toString());
						for(int index = 0 , count = arrayPcId.length ; index < count ; index++){
							DBRow file = new DBRow();
							file.add("file_name",realyFileName.toString());
							file.add("file_with_id",arrayPcId[index]);
							file.add("file_with_type",file_with_type);
							file.add("product_file_type",file_with_class);
							file.add("upload_adid",adminLoggerBean.getAdid());
							file.add("upload_time",DateUtil.NowStr());
							file.add("pc_id", arrayPcId[index]);
							floorTransportMgrZr.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
							// 添加一条日志的记录
						}
					}
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "handleProductPictureUpload", log);
		}
	}
	
	/**
	 * 将无法提供照片的图片关联给商品
	 * @param arrayPcId
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param fileNames
	 * @param adid
	 * @throws Exception
	 */
	public void handleProductPictureUpToUnableToProvidePhoto(String[] arrayPcId,int file_with_type,int file_with_class,String fileNames, long adid,int is_unable_to_provide)throws Exception{
		try 
		{
			for(int index = 0 , count = arrayPcId.length ; index < count ; index++)
			{
				//先查询是否存在无法提供照片，再确定是否添加
				int[] file_with_types = {file_with_type};
				int[] file_types = {file_with_class};
				DBRow[] isUnableToProvides = floorProductMgrZyj.getProductFileByPcIdFileTypesAndIsUnableProvide(Long.parseLong(arrayPcId[index]),Long.parseLong(arrayPcId[index]), file_with_types, file_types, is_unable_to_provide);
				if(0 == isUnableToProvides.length)
				{
					DBRow file = new DBRow();
					file.add("file_name",fileNames);
					file.add("file_with_id",arrayPcId[index]);
					file.add("file_with_type",file_with_type);
					file.add("product_file_type",file_with_class);
					file.add("upload_adid",adid);
					file.add("upload_time",DateUtil.NowStr());
					file.add("pc_id", arrayPcId[index]);
					file.add("is_unable_to_provide", is_unable_to_provide);
					floorTransportMgrZr.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
				}
			}
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "handleProductPictureUpToUnableToProvidePhoto", log);
		}
	}
	
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
	}
	
	/**
	 * 通过商品Id，类型查询文件
	 */
	public DBRow[] getAllProductFileByPcId(String pc_id, long file_with_type, String file_with_id, int is_unable_provide) throws Exception
	{
		try 
		{
			if(!file_with_id.equals("")&&!file_with_id.equals("\""))
			{
				file_with_id = file_with_id.replaceAll("\"","");
				file_with_id = file_with_id.replaceAll("'","");
				file_with_id = file_with_id.replace("\\*","");
			}
			return floorProductMgrZyj.getAllProductFileByPcId(pc_id, file_with_type, file_with_id, is_unable_provide);
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "getAllProductFileByPcId", log);
		}
	}
	
	/**
	 * 统计需要跟进的商品
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductCountTotalByFileType() throws Exception
	{
		try
		{
			return floorProductMgrZyj.getProductFileNeedUploadTotal();
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "getProductCountByProductLineGroupByAndFile", log);
		}
	}
	
	/**
	 * 将没有上传图片完全的商品按照产品线分类
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCountByProductLineGroupByAndFile() throws Exception
	{
		try 
		{
			return floorProductMgrZyj.getProductFileNeedUploadByProductLineGroupBy();
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "getProductCountByProductLineGroupByAndFile", log);
		}
	}
	
	/**
	 * 根据产品线查询需要上传文件的商品
	 */
	public DBRow[] getProductInfosProductLineProductCodeByLineId(long pc_line_id, PageCtrl pc) throws Exception
	{
		try 
		{
			return floorProductMgrZyj.getProductInfosProductLineProductCodeByLineId(pc_line_id, pc);
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "getProductInfosProductLineProductCodeByLineId(long pc_line_id)", log);
		}
	}
	
	/**
	 * 通过商品ID，查询商品相关信息
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findProductInfosByPcid(long pc_id) throws Exception
	{
		try 
		{
			return floorProductMgrZyj.findProductInfosByPcid(pc_id);
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "findProductInfosByPcid(long pc_line_id)", log);
		}
	}
	
	
	public DBRow deleteAllBasicDatas() throws Exception
	{
		try 
		{
			DBRow result = new DBRow();
			floorProductMgrZyj.deleteAllBasicDatas();
			result.add("flag", "true");
			return result;
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "findProductInfosByPcid(long pc_line_id)", log);
		}
	}
	
	public int getProductsByCategoryid(long catalog_id,PageCtrl pc)throws Exception
	{
		try 
		{
			return floorProductMgrZyj.getProductsByCategoryid(catalog_id, pc);
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "getProductsByCategoryid()", log);
		}
	}
	
	/**
	 * 功能：根据catalog_id和title_id统计商品数
	 * @param catalog_id
	 * @param title_id
	 * @return
	 * @throws Exception
	 * @author lixf  2015年5月22日
	 */
	public int getProductsByCategoryidAndTitleid(long catalog_id, long title_id) throws Exception
	{
		try 
		{
			return floorProductMgrZyj.getProductsByCategoryidAndTitleid(catalog_id, title_id);
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "ProductMgrZyj.getProductsByCategoryidAndTitleid(long catalog_id, long title_id)", log);
		}
	}
	
	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}

	public void setFloorProductMgrZyj(FloorProductMgrZyj floorProductMgrZyj) {
		this.floorProductMgrZyj = floorProductMgrZyj;
	}

}
