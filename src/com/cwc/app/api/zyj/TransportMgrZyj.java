package com.cwc.app.api.zyj;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.zj.TransportOutboundMgrZJ;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.MachineUnderFindException;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.bcs.XMLDataLengthException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.transport.NoExistTransportOrderException;
import com.cwc.app.exception.transport.TransportOrderCanNotOutboundException;
import com.cwc.app.exception.transport.TransportStockInHandleErrorException;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.ll.FloorFreightMgrLL;
import com.cwc.app.floor.api.ll.FloorTransportMgrLL;
import com.cwc.app.floor.api.tjh.FloorSupplierMgrTJH;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zwb.FloorPreparePurchaseFundsMgrZwb;
import com.cwc.app.floor.api.zyj.FloorPurchaseMgrZyj;
import com.cwc.app.floor.api.zyj.FloorStorageCatalogMgrZyj;
import com.cwc.app.floor.api.zyj.FloorTransportMgrZyj;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyMgrZZZ;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.tjh.SupplierMgrIFaceTJH;
import com.cwc.app.iface.zj.ContainerProductMgrIFaceZJ;
import com.cwc.app.iface.zj.SerialNumberMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zyj.DoorOrLocationOccupancyMgrZyjIFace;
import com.cwc.app.iface.zyj.ProduresMgrZyjIFace;
import com.cwc.app.iface.zyj.TransportMgrZyjIFace;
import com.cwc.app.key.ClearanceKey;
import com.cwc.app.key.DeclarationKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.NoticeTypeKey;
import com.cwc.app.key.PurchaseTransportPageTypesKey;
import com.cwc.app.key.TransportCertificateKey;
import com.cwc.app.key.TransportLogTypeKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportProductFileKey;
import com.cwc.app.key.TransportPutKey;
import com.cwc.app.key.TransportQualityInspectionKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.key.TransportTagKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class TransportMgrZyj implements TransportMgrZyjIFace{

	static Logger log = Logger.getLogger("ACTION");
	private FloorTransportMgrZyj floorTransportMgrZyj;
	private FloorTransportMgrLL floorTransportMgrLL;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private FloorStorageCatalogMgrZyj floorStorageCatalogMgrZyj;
	private FloorTransportMgrZr floorTransportMgrZr;
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private SupplierMgrIFaceTJH supplierMgr;
	private SystemConfigIFace systemConfig;
	private FloorProductMgr floorProductMgr;
	private FloorPurchaseMgrZyj floorPurchaseMgrZyj;
	private FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ;
	private FloorPurchaseMgr floorPurchaseMgr;
	private FloorPreparePurchaseFundsMgrZwb floorPreparePurchaseFundsMgrZwb;
	private FloorCatalogMgr floorCatalogMgr;
	private FloorSupplierMgrTJH floorSupplierMgrTJH;
	private FloorAdminMgr floorAdminMgr;
	private ProduresMgrZyjIFace produresMgrZyj;
	private FloorFreightMgrLL floorFreightMgrLL;
	private ContainerProductMgrIFaceZJ containerProductMgrZJ;
	private TransportOutboundMgrZJ transportOutboundMgrZJ;
	private SerialNumberMgrIFaceZJ serialNumberMgrZJ;
	private DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj;
	
	@Override
	public void updateTransportAllProdures(HttpServletRequest request)
			throws Exception {

		try
		{
			SystemConfigIFace systemConfig	=((SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig"));
			AdminMgr adminMgr 				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long logingUserId				 = adminLoggerBean.getAdid();
			String loginUserName			= adminLoggerBean.getEmploye_name();
			
			Long transport_id				= StringUtil.getLong(request, "transport_id");
			DBRow transportRow				= floorTransportMgrLL.getTransportById(transport_id+"");
			int trans_declaration			= null != transportRow ? transportRow.get("declaration", 0):0;
			int trans_clearance				= null != transportRow ? transportRow.get("clearance", 0):0;
			int trans_product_file			= null != transportRow ? transportRow.get("product_file", 0):0;
			int trans_tag					= null != transportRow ? transportRow.get("tag", 0):0;
			int trans_stock_in_set			= null != transportRow ? transportRow.get("stock_in_set", 0):0;
			int trans_certificate			= null != transportRow ? transportRow.get("certificate", 0):0;
			int trans_quality_inspection	= null != transportRow ? transportRow.get("quality_inspection", 0):0;
			
			//主流程
			String adminUserIdsTransport	= StringUtil.getString(request, "adminUserIdsTransport");
			String adminUserNamesTransport	= StringUtil.getString(request, "adminUserNamesTransport");
			int needMailTransport				= StringUtil.getInt(request, "needMailTransport");
			int needMessageTransport			= StringUtil.getInt(request, "needMessageTransport");
			int needPageTransport				= StringUtil.getInt(request, "needPageTransport");
			
			//出口报关	
			int declaration					= StringUtil.getInt(request, "declaration");
			String adminUserIdsDeclaration	= StringUtil.getString(request, "adminUserIdsDeclaration");
			String adminUserNamesDeclaration= StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration			= StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration		= StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration			= StringUtil.getInt(request, "needPageDeclaration");
			
			//进口清关
			int clearance					= StringUtil.getInt(request, "clearance");
			String adminUserIdsClearance	= StringUtil.getString(request, "adminUserIdsClearance");
			String adminUserNamesClearance	= StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance			= StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance		= StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance			= StringUtil.getInt(request, "needPageClearance");
			
			//商品图片
			int product_file				= StringUtil.getInt(request, "product_file");
			String adminUserIdsProductFile	= StringUtil.getString(request, "adminUserIdsProductFile");
			String adminUserNamesProductFile= StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile			= StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile		= StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile			= StringUtil.getInt(request, "needPageProductFile");
			
			//制签	
			int tag							= StringUtil.getInt(request, "tag");
			String adminUserIdsTag			= StringUtil.getString(request, "adminUserIdsTag");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag					= StringUtil.getInt(request, "needMailTag");
			int needMessageTag				= StringUtil.getInt(request, "needMessageTag");
			int needPageTag					= StringUtil.getInt(request, "needPageTag");
			
			//运费
			int stock_in_set				= StringUtil.getInt(request, "stock_in_set");
			String adminUserIdsStockInSet	= StringUtil.getString(request, "adminUserIdsStockInSet");
			String adminUserNamesStockInSet	= StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet			= StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet		= StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet			= StringUtil.getInt(request, "needPageStockInSet");
			
			//单证
			int certificate					= StringUtil.getInt(request, "certificate");
			String adminUserIdsCertificate	= StringUtil.getString(request, "adminUserIdsCertificate");
			String adminUserNamesCertificate= StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate			= StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate		= StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate			= StringUtil.getInt(request, "needPageCertificate");
			
			//质检
			int quality_inspection 			= StringUtil.getInt(request, "quality_inspection");
			String adminUserIdsQualityInspection	= StringUtil.getString(request, "adminUserIdsQualityInspection");
			String adminUserNamesQualityInspection	= StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection	= StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection= StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection	= StringUtil.getInt(request, "needPageQualityInspection");
			
			//发送的内容
			String sendContent = this.handleSendContent(transport_id, adminUserNamesTransport, declaration, adminUserNamesDeclaration,
					clearance, adminUserNamesClearance, product_file, adminUserNamesProductFile,
					tag, adminUserNamesTag, stock_in_set, adminUserNamesStockInSet, 
					certificate, adminUserNamesCertificate, quality_inspection, adminUserNamesQualityInspection);
	
			//报关，3.报关中，4.完成
			//原数据未填写是否需要报送且现在需要报送或原来不需要，现在需要，添加
			if((0 == trans_declaration && DeclarationKey.DELARATION == declaration) || (DeclarationKey.NODELARATION == trans_declaration && DeclarationKey.DELARATION == declaration)){
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsDeclaration, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[报关流程]", 
						sendContent,
						needPageDeclaration, needMailDeclaration, needMessageDeclaration, request, false, 7,
						DeclarationKey.DELARATION, loginUserName, "[报关流程][需要报关]");
				
			//如果原数据需要，现在不需要，删除
			}else if(DeclarationKey.DELARATION == trans_declaration && DeclarationKey.NODELARATION == declaration){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 7
						, DeclarationKey.NODELARATION, loginUserName, "[报关流程][不需要报关]" , logingUserId, request);
				
			//原来需要，现在需要，更新；更新的前提是看，是否添加过任务
			}else if((DeclarationKey.DELARATION == trans_declaration && DeclarationKey.DELARATION == declaration)){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsDeclaration, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[报关流程]", 
						sendContent,
						needPageDeclaration, needMailDeclaration, needMessageDeclaration, request, false, 7,
						DeclarationKey.DELARATION, loginUserName, "[报关流程][需要报关]"
						);
			//报送处于中，更新任务
			}else if(DeclarationKey.DELARATING == trans_declaration){
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsDeclaration, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[报关流程]", 
						sendContent,
						needPageDeclaration, needMailDeclaration, needMessageDeclaration, request, false, 7,
						DeclarationKey.DELARATING, loginUserName, "[报关流程]["+new DeclarationKey().getStatusById(trans_declaration)+"]"
						);
			}
			
			//清关	3.清关中，4.清关完成，5.查货中
			//原数据未填写是否需要清关且现在需要清关或原来不需要，现在需要，添加
			if((0 == trans_clearance && ClearanceKey.CLEARANCE == clearance) || (ClearanceKey.NOCLEARANCE == trans_clearance && ClearanceKey.CLEARANCE == clearance)){
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsClearance, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[清关流程]",
						sendContent,
						needPageClearance, needMailClearance, needMessageClearance, request, false, 6,
						ClearanceKey.CLEARANCE, loginUserName, "[清关流程][需要清关]");
			//原来需要，现在不需要，删除
			}else if(ClearanceKey.CLEARANCE == trans_clearance && ClearanceKey.NOCLEARANCE == clearance){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 6
						, ClearanceKey.NOCLEARANCE , loginUserName, "[清关流程][不需要清关]", logingUserId, request);
				
			//原来需要，现在也需要
			}else if((ClearanceKey.CLEARANCE == trans_clearance && ClearanceKey.CLEARANCE == clearance)){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsClearance, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[清关流程]",
						sendContent,
						needPageClearance, needMailClearance, needMessageClearance, request, false, 6
						,ClearanceKey.CLEARANCE, loginUserName, "[清关流程][需要清关]"
						);
				
			//原数据正在清关中			原数据处理查货中	
			}else if(ClearanceKey.CLEARANCEING == trans_clearance || ClearanceKey.CHECKCARGO == trans_clearance){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsClearance, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[清关流程]",
						sendContent,
						needPageClearance, needMailClearance, needMessageClearance, request, false, 6
						,ClearanceKey.CLEARANCE, loginUserName, "[清关流程]["+new ClearanceKey().getStatusById(trans_clearance)+"]"
						);
			}
			TDate tdateProductFile = new TDate(transportRow.getString("transport_date"));
			tdateProductFile.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_product_file_period")));
			String endTimeProductFile = tdateProductFile.formatDate("yyyy-MM-dd HH:mm:ss");
			//商品图片流程	1.无需商品文件，2.商品文件上传中，3.商品文件上传完成
			if((0 == trans_product_file && TransportProductFileKey.PRODUCTFILE == product_file) || (TransportProductFileKey.NOPRODUCTFILE == trans_product_file && TransportProductFileKey.PRODUCTFILE == product_file)){
				//添加
				this.addTransportScheduleOneProdure(transportRow.getString("transport_date"), endTimeProductFile, logingUserId,
						adminUserIdsProductFile, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[商品图片流程]",
						sendContent,
						needPageProductFile, needMailProductFile, needMessageProductFile, request, true, 10,
						TransportProductFileKey.PRODUCTFILE, loginUserName, "[商品图片流程][商品文件上传中]");
			}else if(TransportProductFileKey.PRODUCTFILE == trans_product_file && TransportProductFileKey.NOPRODUCTFILE == product_file){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 10
						,TransportProductFileKey.NOPRODUCTFILE, loginUserName, "[商品图片流程][无需商品文件]", logingUserId, request);
			}else if((TransportProductFileKey.PRODUCTFILE == trans_product_file && TransportProductFileKey.PRODUCTFILE == product_file)){
				//更新
				this.updateTransportScheduleOneProdure(transportRow.getString("transport_date"), endTimeProductFile, logingUserId,
						adminUserIdsProductFile, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[商品图片流程]",
						sendContent,
						needPageProductFile, needMailProductFile, needMessageProductFile, request, true, 10
						,TransportProductFileKey.PRODUCTFILE, loginUserName, "[商品图片流程][商品文件上传中]"
						);
			}
			TDate tdateTag = new TDate(transportRow.getString("transport_date"));
			tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_tag_period")));
			String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
			//制签流程	1.无需制签，2.需要制签，3.制签完成
			if((0 == trans_tag && TransportTagKey.TAG == tag) || (TransportTagKey.NOTAG == trans_tag && TransportTagKey.TAG == tag)){
				//添加
				this.addTransportScheduleOneProdure(transportRow.getString("transport_date"), endTimeTag, logingUserId,
						adminUserIdsTag, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[制签流程]",
						sendContent,
						needPageTag, needMailTag, needMessageTag, request, true, 8,
						TransportTagKey.TAG, loginUserName, "[制签流程][制签中]");
			}else if(TransportTagKey.TAG == trans_tag && TransportTagKey.NOTAG == tag){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 8
						, TransportTagKey.NOTAG, loginUserName, "[制签流程][无需制签]", logingUserId, request);
			}else if((TransportTagKey.TAG == trans_tag && TransportTagKey.TAG == tag)){
				//更新
				this.updateTransportScheduleOneProdure(transportRow.getString("transport_date"), endTimeTag, logingUserId,
						adminUserIdsTag, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[制签流程]",
						sendContent,
						needPageTag, needMailTag, needMessageTag, request, true, 8
						,TransportTagKey.TAG, loginUserName, "[需要制签][制签中]"
						);
			}
			
			
			//运费流程，阶段：3.运费已经设置，4.运费已申请，5.运费转账完
			if((0 == trans_stock_in_set && TransportStockInSetKey.SHIPPINGFEE_SET == stock_in_set) || (TransportStockInSetKey.SHIPPINGFEE_NOTSET == trans_stock_in_set && TransportStockInSetKey.SHIPPINGFEE_SET == stock_in_set)){
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsStockInSet, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[运费流程]",
						sendContent,
						needPageStockInSet, needMailStockInSet, needMessageStockInSet, request, false, 5,
						TransportStockInSetKey.SHIPPINGFEE_SET, loginUserName, "[运费流程][需要运费]");
			}else if((TransportStockInSetKey.SHIPPINGFEE_SET == trans_stock_in_set) && TransportStockInSetKey.SHIPPINGFEE_NOTSET == stock_in_set){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 5
						, TransportStockInSetKey.SHIPPINGFEE_NOTSET, loginUserName, "[运费流程][无需运费]", logingUserId, request);
				
			//原数据为需要，现在为需要，更新
			}else if((TransportStockInSetKey.SHIPPINGFEE_SET == trans_stock_in_set && TransportStockInSetKey.SHIPPINGFEE_SET == stock_in_set)){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						adminUserIdsStockInSet, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[运费流程]",
						sendContent,
						needPageStockInSet, needMailStockInSet, needMessageStockInSet, request, false, 5
						,TransportStockInSetKey.SHIPPINGFEE_SET, loginUserName, "[运费流程][需要运费]"
						);
				
			//原运费已设置			运费已申请，更新
			}
//			else if(TransportStockInSetKey.SHIPPINGFEE_SETOVER == trans_stock_in_set || TransportStockInSetKey.SHIPPINGFEE_APPLY == trans_stock_in_set){
//				//更新
//				this.updateTransportScheduleOneProdure("", "", logingUserId,
//						adminUserIdsStockInSet, "", 
//						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[运费流程]",
//						sendContent,
//						needPageStockInSet, needMailStockInSet, needMessageStockInSet, request, false, 5
//						,TransportStockInSetKey.SHIPPINGFEE_SET, loginUserName, "[运费流程]["+new TransportStockInSetKey().getStatusById(trans_stock_in_set)+"]"
//						);
//			}
			
			TDate tdateCertificate = new TDate(transportRow.getString("transport_date"));
			tdateCertificate.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_certificate_period")));
			String endTimeCertificate = tdateCertificate.formatDate("yyyy-MM-dd HH:mm:ss");
			//单证流程	1.无需单证，2.单证采集中，3，单证采集完成
			if((0 == trans_certificate && TransportCertificateKey.CERTIFICATE == certificate) || (TransportCertificateKey.NOCERTIFICATE == trans_certificate && TransportCertificateKey.CERTIFICATE == certificate)){
				//添加
				this.addTransportScheduleOneProdure(transportRow.getString("transport_date"), endTimeCertificate, logingUserId,
						adminUserIdsCertificate, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[单证流程]",
						sendContent,
						needPageCertificate, needMailCertificate, needMessageCertificate, request, true, 9,
						TransportCertificateKey.CERTIFICATE, loginUserName, "[单证流程][单证采集中]");
			}else if(TransportCertificateKey.CERTIFICATE == trans_certificate && TransportCertificateKey.NOCERTIFICATE == certificate){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 9
						, TransportCertificateKey.NOCERTIFICATE, loginUserName, "[单证流程][无需单证]", logingUserId, request);
			}else if((TransportCertificateKey.CERTIFICATE == trans_certificate && TransportCertificateKey.CERTIFICATE == certificate)){
				//更新
				this.updateTransportScheduleOneProdure(transportRow.getString("transport_date"), endTimeCertificate, logingUserId,
						adminUserIdsCertificate, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[单证流程]",
						sendContent,
						needPageCertificate, needMailCertificate, needMessageCertificate, request, true, 9
						, TransportCertificateKey.CERTIFICATE, loginUserName, "[单证流程][单证采集中]"
						);
			}
			TDate tdateQuality = new TDate(transportRow.getString("transport_date"));
			tdateQuality.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_quality_inspection")));
			String endTimeQuality = tdateQuality.formatDate("yyyy-MM-dd HH:mm:ss");
			//质检流程	3.质检中，4.质检完成
			if((0 == trans_quality_inspection && TransportQualityInspectionKey.NEED_QUALITY == quality_inspection) || (TransportQualityInspectionKey.NO_NEED_QUALITY == trans_quality_inspection && TransportQualityInspectionKey.NEED_QUALITY == quality_inspection)){
				//添加
				this.addTransportScheduleOneProdure(transportRow.getString("transport_date"), endTimeQuality, logingUserId,
						adminUserIdsQualityInspection, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[质检流程]",
						sendContent,
						needPageQualityInspection, needMailQualityInspection, needMessageQualityInspection, request, true, 11,
						TransportQualityInspectionKey.NEED_QUALITY, loginUserName, "[质检流程][需要质检]");
			}else if(TransportQualityInspectionKey.NEED_QUALITY == trans_quality_inspection && TransportQualityInspectionKey.NO_NEED_QUALITY == quality_inspection){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 11
						, TransportQualityInspectionKey.NO_NEED_QUALITY, loginUserName, "[质检流程][无需质检]", logingUserId, request);
				
			//原数据需要且现需要		原数据处于质检中
			}else if((TransportQualityInspectionKey.NEED_QUALITY == trans_quality_inspection && TransportQualityInspectionKey.NEED_QUALITY == quality_inspection)){
				//更新
				this.updateTransportScheduleOneProdure(transportRow.getString("transport_date"), endTimeQuality, logingUserId,
						adminUserIdsQualityInspection, "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[质检流程]",
						sendContent,
						needPageQualityInspection, needMailQualityInspection, needMessageQualityInspection, request, true, 11
						,TransportQualityInspectionKey.NEED_QUALITY, loginUserName, "[质检流程][需要质检]"
						);
			}
			
			String transport_date=DateUtil.NowStr();
			TDate tDate = new TDate();
			tDate.addDay(+1);
			DBRow transRow = new DBRow();
			//如果数据库中是需要，不需要，或者为0，才更新主单据
			if(DeclarationKey.NODELARATION == trans_declaration || DeclarationKey.DELARATION == trans_declaration || 0 == trans_declaration){
				transRow.add("declaration", declaration);
			}
			if(ClearanceKey.NOCLEARANCE == trans_clearance || ClearanceKey.CLEARANCE == trans_clearance || 0 == trans_clearance){
				transRow.add("clearance", clearance);
			}
			if(TransportProductFileKey.NOPRODUCTFILE == trans_product_file || TransportProductFileKey.PRODUCTFILE == trans_product_file || 0 == trans_product_file){
				transRow.add("product_file", product_file);
			}
			if(TransportTagKey.NOTAG == trans_tag || TransportTagKey.TAG == trans_tag || 0 == trans_tag){
				transRow.add("tag", tag);
			}
			if(TransportStockInSetKey.SHIPPINGFEE_NOTSET == trans_stock_in_set || TransportStockInSetKey.SHIPPINGFEE_SET == trans_stock_in_set || 0 == trans_stock_in_set){
				transRow.add("stock_in_set", stock_in_set);
			}
			if(TransportCertificateKey.NOCERTIFICATE == trans_certificate || TransportCertificateKey.CERTIFICATE == trans_certificate || 0 == trans_certificate){
				transRow.add("certificate", certificate);
			}
			if(TransportQualityInspectionKey.NO_NEED_QUALITY == trans_quality_inspection || TransportQualityInspectionKey.NEED_QUALITY == trans_quality_inspection || 0 == trans_quality_inspection){
				transRow.add("quality_inspection", quality_inspection);
			}
			transRow.add("updatedate",transport_date);
			floorTransportMgrZyj.updateTransport(transport_id, transRow);
			
		
		}catch (Exception e)
		{
			throw new SystemException(e,"updateTransportAllProdures",log);
		}
	}
	
	/**
	 * 更新转运单各个流程信息
	 */
	@Override
	public void updateTransportProdures(HttpServletRequest request) throws Exception
	{
		try 
		{
			SystemConfigIFace systemConfig	=((SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig"));
			AdminMgr adminMgr 				= new AdminMgr();
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			long logingUserId				 = adminLoggerBean.getAdid();
			String loginUserName			= adminLoggerBean.getEmploye_name();
			
			Long transport_id				= StringUtil.getLong(request, "transport_id");
			DBRow transportRow				= floorTransportMgrLL.getTransportById(transport_id+"");
			int trans_transport_status		= null != transportRow ? transportRow.get("transport_status", 0):0;
			int trans_declaration			= null != transportRow ? transportRow.get("declaration", 0):0;
			int trans_clearance				= null != transportRow ? transportRow.get("clearance", 0):0;
			int trans_product_file			= null != transportRow ? transportRow.get("product_file", 0):0;
			int trans_tag					= null != transportRow ? transportRow.get("tag", 0):0;
			int trans_tag_third				= null != transportRow ? transportRow.get("tag_third", 0):0;
			int trans_stock_in_set			= null != transportRow ? transportRow.get("stock_in_set", 0):0;
			int trans_certificate			= null != transportRow ? transportRow.get("certificate", 0):0;
			int trans_quality_inspection	= null != transportRow ? transportRow.get("quality_inspection", 0):0;
			String trans_transport_date		= null != transportRow ? transportRow.getString("transport_date"):"";
			
			//主流程
			String adminUserIdsTransport	= StringUtil.getString(request, "adminUserIdsTransport");
			String adminUserNamesTransport	= StringUtil.getString(request, "adminUserNamesTransport");
			int needMailTransport			= StringUtil.getInt(request, "needMailTransport");
			int needMessageTransport		= StringUtil.getInt(request, "needMessageTransport");
			int needPageTransport			= StringUtil.getInt(request, "needPageTransport");
			
			//出口报关	
			int declaration					= StringUtil.getInt(request, "declaration");
			String adminUserIdsDeclaration	= StringUtil.getString(request, "adminUserIdsDeclaration");
			String adminUserNamesDeclaration= StringUtil.getString(request, "adminUserNamesDeclaration");
			int needMailDeclaration			= StringUtil.getInt(request, "needMailDeclaration");
			int needMessageDeclaration		= StringUtil.getInt(request, "needMessageDeclaration");
			int needPageDeclaration			= StringUtil.getInt(request, "needPageDeclaration");
			
			//进口清关
			int clearance					= StringUtil.getInt(request, "clearance");
			String adminUserIdsClearance	= StringUtil.getString(request, "adminUserIdsClearance");
			String adminUserNamesClearance	= StringUtil.getString(request, "adminUserNamesClearance");
			int needMailClearance			= StringUtil.getInt(request, "needMailClearance");
			int needMessageClearance		= StringUtil.getInt(request, "needMessageClearance");
			int needPageClearance			= StringUtil.getInt(request, "needPageClearance");
			
			//商品图片
			int product_file				= StringUtil.getInt(request, "product_file");
			String adminUserIdsProductFile	= StringUtil.getString(request, "adminUserIdsProductFile");
			String adminUserNamesProductFile= StringUtil.getString(request, "adminUserNamesProductFile");
			int needMailProductFile			= StringUtil.getInt(request, "needMailProductFile");
			int needMessageProductFile		= StringUtil.getInt(request, "needMessageProductFile");
			int needPageProductFile			= StringUtil.getInt(request, "needPageProductFile");
			
			//内部标签	
			int tag							= StringUtil.getInt(request, "tag");
			String adminUserIdsTag			= StringUtil.getString(request, "adminUserIdsTag");
			String adminUserNamesTag		= StringUtil.getString(request, "adminUserNamesTag");
			int needMailTag					= StringUtil.getInt(request, "needMailTag");
			int needMessageTag				= StringUtil.getInt(request, "needMessageTag");
			int needPageTag					= StringUtil.getInt(request, "needPageTag");
			
			//第三方标签
			int tag_third					= StringUtil.getInt(request, "tag_third");
			String adminUserIdsTagThird		= StringUtil.getString(request, "adminUserIdsTagThird");
			String adminUserNamesTagThird	= StringUtil.getString(request, "adminUserNamesTagThird");
			int needMailTagThird			= StringUtil.getInt(request, "needMailTagThird");
			int needMessageTagThird			= StringUtil.getInt(request, "needMessageTagThird");
			int needPageTagThird			= StringUtil.getInt(request, "needPageTagThird");
			
			//运费
			int stock_in_set				= StringUtil.getInt(request, "stock_in_set");
			String adminUserIdsStockInSet	= StringUtil.getString(request, "adminUserIdsStockInSet");
			String adminUserNamesStockInSet	= StringUtil.getString(request, "adminUserNamesStockInSet");
			int needMailStockInSet			= StringUtil.getInt(request, "needMailStockInSet");
			int needMessageStockInSet		= StringUtil.getInt(request, "needMessageStockInSet");
			int needPageStockInSet			= StringUtil.getInt(request, "needPageStockInSet");
			
			//单证
			int certificate					= StringUtil.getInt(request, "certificate");
			String adminUserIdsCertificate	= StringUtil.getString(request, "adminUserIdsCertificate");
			String adminUserNamesCertificate= StringUtil.getString(request, "adminUserNamesCertificate");
			int needMailCertificate			= StringUtil.getInt(request, "needMailCertificate");
			int needMessageCertificate		= StringUtil.getInt(request, "needMessageCertificate");
			int needPageCertificate			= StringUtil.getInt(request, "needPageCertificate");
			
			//质检
			int quality_inspection 			= StringUtil.getInt(request, "quality_inspection");
			String adminUserIdsQualityInspection	= StringUtil.getString(request, "adminUserIdsQualityInspection");
			String adminUserNamesQualityInspection	= StringUtil.getString(request, "adminUserNamesQualityInspection");
			int needMailQualityInspection	= StringUtil.getInt(request, "needMailQualityInspection");
			int needMessageQualityInspection= StringUtil.getInt(request, "needMessageQualityInspection");
			int needPageQualityInspection	= StringUtil.getInt(request, "needPageQualityInspection");
			
			String transport_date=DateUtil.NowStr();
			TDate tDate = new TDate();
			tDate.addDay(+1);
			DBRow transRow = new DBRow();
			//如果数据库中是需要，不需要，或者为0，才更新主单据
			if(DeclarationKey.NODELARATION == trans_declaration || DeclarationKey.DELARATION == trans_declaration || 0 == trans_declaration){
				transRow.add("declaration", declaration);
			}
			if(ClearanceKey.NOCLEARANCE == trans_clearance || ClearanceKey.CLEARANCE == trans_clearance || 0 == trans_clearance){
				transRow.add("clearance", clearance);
			}
			if(TransportProductFileKey.NOPRODUCTFILE == trans_product_file || TransportProductFileKey.PRODUCTFILE == trans_product_file || 0 == trans_product_file){
				transRow.add("product_file", product_file);
			}
			if(TransportTagKey.NOTAG == trans_tag || TransportTagKey.TAG == trans_tag || 0 == trans_tag){
				transRow.add("tag", tag);
			}
			if(TransportTagKey.NOTAG == trans_tag_third || TransportTagKey.TAG == trans_tag_third || 0 == trans_tag_third){
				transRow.add("tag_third", tag_third);
			}
			if(TransportStockInSetKey.SHIPPINGFEE_NOTSET == trans_stock_in_set || TransportStockInSetKey.SHIPPINGFEE_SET == trans_stock_in_set || 0 == trans_stock_in_set){
				transRow.add("stock_in_set", stock_in_set);
			}
			if(TransportCertificateKey.NOCERTIFICATE == trans_certificate || TransportCertificateKey.CERTIFICATE == trans_certificate || 0 == trans_certificate){
				transRow.add("certificate", certificate);
			}
			if(TransportQualityInspectionKey.NO_NEED_QUALITY == trans_quality_inspection || TransportQualityInspectionKey.NEED_QUALITY == trans_quality_inspection || 0 == trans_quality_inspection){
				transRow.add("quality_inspection", quality_inspection);
			}
			transRow.add("updatedate",transport_date);
			floorTransportMgrZyj.updateTransport(transport_id, transRow);
			
			//发送的内容
			String sendContent = this.handleSendContent(transport_id, adminUserNamesTransport, declaration, adminUserNamesDeclaration,
					clearance, adminUserNamesClearance, product_file, adminUserNamesProductFile,
					tag, adminUserNamesTag, stock_in_set, adminUserNamesStockInSet, 
					certificate, adminUserNamesCertificate, quality_inspection, adminUserNamesQualityInspection);
			//主流程
			this.updateTransportTransport(logingUserId, adminUserIdsTransport, "", transport_id,
					ModuleKey.TRANSPORT_ORDER, needPageTransport, needMailTransport, needMessageTransport, request, true,
					4, trans_transport_status, loginUserName, sendContent);
			//报关
			this.updateTransportDelaration(logingUserId, adminUserIdsDeclaration, "", transport_id,
					ModuleKey.TRANSPORT_ORDER, needPageDeclaration, needMailDeclaration, needMessageDeclaration, request, false, 
					7, declaration, trans_declaration, loginUserName, sendContent);
			//清关
			this.updateTransportClearance(logingUserId, adminUserIdsClearance, "", transport_id,
					ModuleKey.TRANSPORT_ORDER, needPageClearance, needMailClearance, needMessageClearance, request, false,
					6, clearance, trans_clearance, loginUserName, sendContent);
			//实物图片
			this.updateTransportProductFile(logingUserId, adminUserIdsProductFile, "", transport_id,
					ModuleKey.TRANSPORT_ORDER, needPageProductFile, needMailProductFile, needMessageProductFile, request, true,
					10, product_file, trans_product_file, loginUserName, sendContent, trans_transport_date);
			//内部标签
			this.updateTransportTag(logingUserId, adminUserIdsTag, "", transport_id,
					ModuleKey.TRANSPORT_ORDER, needPageTag, needMailTag, needMessageTag, request, true,
					8, tag, trans_tag, loginUserName, sendContent, transport_date);
			//第三方标签
			this.updateTransportTagThird(logingUserId, adminUserIdsTagThird, "", transport_id,
					ModuleKey.TRANSPORT_ORDER, needPageTagThird, needMailTagThird, needMessageTagThird, request, true,
					TransportLogTypeKey.THIRD_TAG, tag_third, trans_tag_third, loginUserName, sendContent, transport_date);
			//运费
			this.updateTransportStockInSet(logingUserId, adminUserIdsStockInSet, "", transport_id,
					ModuleKey.TRANSPORT_ORDER, needPageStockInSet, needMailStockInSet, needMessageStockInSet, request, false, 
					5, stock_in_set, trans_stock_in_set, loginUserName, sendContent, transport_date);
			//单证
			this.updateTransportCertificate(logingUserId, adminUserIdsCertificate, "", transport_id,
					ModuleKey.TRANSPORT_ORDER, needPageCertificate, needMailCertificate, needMessageCertificate, request, true, 
					9, certificate, trans_certificate, loginUserName, sendContent, transport_date);
			//质检报告
			this.updateTransportQuality(logingUserId, adminUserIdsQualityInspection, "", transport_id, 
					ModuleKey.TRANSPORT_ORDER, needPageQualityInspection, needMailQualityInspection, needMessageQualityInspection, request, true,
					11, quality_inspection, trans_quality_inspection, loginUserName, sendContent, transport_date);
			
			
		} catch (Exception e) {
			throw new SystemException(e,"updateTransportProdures",log);
		}
	}
	
	/*
	 * 更新主流程
	 */
	private void updateTransportTransport(long logingUserId, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int pur_activity, String loginUserName, String sendContent) throws Exception
	{
		try
		{
			TransportOrderKey transportOrderKey = new TransportOrderKey();
			this.updateTransportScheduleOneProdure(DateUtil.NowStr(), "", logingUserId,
					executePersonIds, joinPersonIds, 
					transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[货物状态]", 
					sendContent,
					page, mail, message, request, false, 4,
					pur_activity, loginUserName, "["+transportOrderKey.getTransportOrderStatusById(pur_activity)+"]"
					);
		} catch (Exception e) {
			throw new SystemException(e,"updateTransportTransport",log);
		}
	}
	
	/*
	 * 更新报关流程
	 */
	private void updateTransportDelaration(long logingUserId, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent) throws Exception
	{
		try {
			DeclarationKey declarationKey = new DeclarationKey();
			//报关，3.报关中，4.完成
			//原数据未填写是否需要报送且现在需要报送或原来不需要，现在需要，添加
			if((0 == pur_activity && DeclarationKey.DELARATION == activity) || (DeclarationKey.NODELARATION == pur_activity && DeclarationKey.DELARATION == activity)){
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[报关流程]", 
						sendContent,
						page, mail, message, request, false, 7,
						DeclarationKey.DELARATION, loginUserName, "修改转运单:"+transport_id+"[需要报关]");
				
			//如果原数据需要，现在不需要，删除
			}else if(DeclarationKey.DELARATION == pur_activity && DeclarationKey.NODELARATION == activity){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 7
						, DeclarationKey.NODELARATION, loginUserName, "修改转运单:"+transport_id+"[不需要报关]" , logingUserId, request);
				
			//原来需要，现在需要，更新；更新的前提是看，是否添加过任务
			}else if((DeclarationKey.DELARATION == pur_activity && DeclarationKey.DELARATION == activity)){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[报关流程]", 
						sendContent,
						page, mail, message, request, false, 7,
						DeclarationKey.DELARATION, loginUserName, "修改转运单:"+transport_id+"[需要报关]"
						);
			//报送处于中，更新任务
			}else if(DeclarationKey.DELARATING == pur_activity){
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[报关流程]", 
						sendContent,
						page, mail, message, request, false, 7,
						DeclarationKey.DELARATING, loginUserName, "修改转运单:"+transport_id+"["+declarationKey.getStatusById(pur_activity)+"]"
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updateTransportDelaration",log);
		}
	}
	/*
	 * 更新清关流程
	 */
	private void updateTransportClearance(long logingUserId, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent) throws Exception
	{
		try 
		{
			//清关	3.清关中，4.清关完成，5.查货中
			//原数据未填写是否需要清关且现在需要清关或原来不需要，现在需要，添加
			if((0 == pur_activity && ClearanceKey.CLEARANCE == activity) || (ClearanceKey.NOCLEARANCE == pur_activity && ClearanceKey.CLEARANCE == activity)){
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[清关流程]",
						sendContent,
						page, mail, message, request, false, 6,
						ClearanceKey.CLEARANCE, loginUserName, "修改转运单:"+transport_id+"[需要清关]");
			//原来需要，现在不需要，删除
			}else if(ClearanceKey.CLEARANCE == pur_activity && ClearanceKey.NOCLEARANCE == activity){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 6
						, ClearanceKey.NOCLEARANCE , loginUserName, "修改转运单:"+transport_id+"[不需要清关]", logingUserId, request);
				
			//原来需要，现在也需要
			}else if((ClearanceKey.CLEARANCE == pur_activity && ClearanceKey.CLEARANCE == activity)){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[清关流程]",
						sendContent,
						page, mail, message, request, false, 6
						,ClearanceKey.CLEARANCE, loginUserName, "修改转运单:"+transport_id+"[需要清关]"
						);
				
			//原数据正在清关中			原数据处理查货中	
			}else if(ClearanceKey.CLEARANCEING == pur_activity){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[清关流程]",
						sendContent,
						page, mail, message, request, false, 6
						,ClearanceKey.CLEARANCE, loginUserName, "修改转运单:"+transport_id+"["+new ClearanceKey().getStatusById(pur_activity)+"]"
						);
			}	
			else if(ClearanceKey.CHECKCARGO == pur_activity)
			{
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[清关流程]",
						sendContent,
						page, mail, message, request, false, 6
						,ClearanceKey.CHECKCARGO, loginUserName, "修改转运单:"+transport_id+"["+new ClearanceKey().getStatusById(pur_activity)+"]"
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updateTransportClearance",log);
		}
	}
	/*
	 * 更新实物图片流程
	 */
	private void updateTransportProductFile(long logingUserId, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
	{
		try 
		{
			TDate tdateProductFile = new TDate(transport_date);
			tdateProductFile.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_product_file_period")));
			String endTimeProductFile = tdateProductFile.formatDate("yyyy-MM-dd HH:mm:ss");
			//商品图片流程	1.无需商品文件，2.商品文件上传中，3.商品文件上传完成
			if((0 == pur_activity && TransportProductFileKey.PRODUCTFILE == activity) || (TransportProductFileKey.NOPRODUCTFILE == pur_activity && TransportProductFileKey.PRODUCTFILE == activity)){
				//添加
				this.addTransportScheduleOneProdure(transport_date, endTimeProductFile, logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[商品图片流程]",
						sendContent,
						page, mail, message, request, true, 10,
						TransportProductFileKey.PRODUCTFILE, loginUserName, "修改转运单:"+transport_id+"[商品文件上传中]");
			}else if(TransportProductFileKey.PRODUCTFILE == pur_activity && TransportProductFileKey.NOPRODUCTFILE == activity){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 10
						,TransportProductFileKey.NOPRODUCTFILE, loginUserName, "修改转运单:"+transport_id+"[无需商品文件]", logingUserId, request);
			}else if((TransportProductFileKey.PRODUCTFILE == pur_activity && TransportProductFileKey.PRODUCTFILE == activity)){
				//更新
				this.updateTransportScheduleOneProdure(transport_date, endTimeProductFile, logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[商品图片流程]",
						sendContent,
						page, mail, message, request, true, 10
						,TransportProductFileKey.PRODUCTFILE, loginUserName, "修改转运单:"+transport_id+"[商品文件上传中]"
						);
			}	
		} catch (Exception e) {
			throw new SystemException(e,"updateTransportProductFile",log);
		}
	}
	
	/*
	 * 更新制签流程
	 */
	private void updateTransportTag(long logingUserId, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
		{
			try 
			{
				TDate tdateTag = new TDate(transport_date);
				tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_tag_period")));
				String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
				//制签流程	1.无需制签，2.需要制签，3.制签完成
				if((0 == pur_activity && TransportTagKey.TAG == activity) || (TransportTagKey.NOTAG == pur_activity && TransportTagKey.TAG == activity)){
					//添加
					this.addTransportScheduleOneProdure(transport_date, endTimeTag, logingUserId,
							executePersonIds, joinPersonIds, 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[内部标签流程]",
							sendContent,
							page, mail, message, request, true, 8,
							TransportTagKey.TAG, loginUserName, "修改转运单:"+transport_id+"[制签中]");
				}else if(TransportTagKey.TAG == pur_activity && TransportTagKey.NOTAG == activity){
					//删除
					this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 8
							, TransportTagKey.NOTAG, loginUserName, "修改转运单:"+transport_id+"[无需制签]", logingUserId, request);
				}else if((TransportTagKey.TAG == pur_activity && TransportTagKey.TAG == activity)){
					//更新
					this.updateTransportScheduleOneProdure(transport_date, endTimeTag, logingUserId,
							executePersonIds, joinPersonIds, 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[内部标签流程]",
							sendContent,
							page, mail, message, request, true, 8
							,TransportTagKey.TAG, loginUserName, "修改转运单:"+transport_id+"[制签中]"
							);
				}
			} 
			catch (Exception e) {
				throw new SystemException(e,"updateTransportTag",log);
			}
		}
	
	/*
	 * 更新制签流程
	 */
	private void updateTransportTagThird(long logingUserId, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
		{
			try 
			{
				TDate tdateTag = new TDate(transport_date);
				tdateTag.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_tag_period")));
				String endTimeTag = tdateTag.formatDate("yyyy-MM-dd HH:mm:ss");
				//制签流程	1.无需制签，2.需要制签，3.制签完成
				if((0 == pur_activity && TransportTagKey.TAG == activity) || (TransportTagKey.NOTAG == pur_activity && TransportTagKey.TAG == activity)){
					//添加
					this.addTransportScheduleOneProdure(transport_date, endTimeTag, logingUserId,
							executePersonIds, joinPersonIds, 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[第三方标签流程]",
							sendContent,
							page, mail, message, request, true, TransportLogTypeKey.THIRD_TAG,
							TransportTagKey.TAG, loginUserName, "修改转运单:"+transport_id+"[制签中]");
				}else if(TransportTagKey.TAG == pur_activity && TransportTagKey.NOTAG == activity){
					//删除
					this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.THIRD_TAG
							, TransportTagKey.NOTAG, loginUserName, "修改转运单:"+transport_id+"[无需制签]", logingUserId, request);
				}else if((TransportTagKey.TAG == pur_activity && TransportTagKey.TAG == activity)){
					//更新
					this.updateTransportScheduleOneProdure(transport_date, endTimeTag, logingUserId,
							executePersonIds, joinPersonIds, 
							transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[第三方标签流程]",
							sendContent,
							page, mail, message, request, true, TransportLogTypeKey.THIRD_TAG
							,TransportTagKey.TAG, loginUserName, "修改转运单:"+transport_id+"[制签中]"
							);
				}
			} 
			catch (Exception e) {
				throw new SystemException(e,"updateTransportTagThird",log);
			}
		}
	/*
	 * 更新运费流程
	 */
	private void updateTransportStockInSet(long logingUserId, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
	{
		try
		{
			//运费流程，阶段：3.运费已经设置，4.运费已申请，5.运费转账完
			if((0 == pur_activity && TransportStockInSetKey.SHIPPINGFEE_SET == activity) || (TransportStockInSetKey.SHIPPINGFEE_NOTSET == pur_activity && TransportStockInSetKey.SHIPPINGFEE_SET == activity)){
				//添加
				this.addTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[运费流程]",
						sendContent,
						page, mail, message, request, false, 5,
						TransportStockInSetKey.SHIPPINGFEE_SET, loginUserName, "修改转运单:"+transport_id+"[需要运费]");
			}else if((TransportStockInSetKey.SHIPPINGFEE_SET == pur_activity) && TransportStockInSetKey.SHIPPINGFEE_NOTSET == activity){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 5
						, TransportStockInSetKey.SHIPPINGFEE_NOTSET, loginUserName, "修改转运单:"+transport_id+"[无需运费]", logingUserId, request);
				
			//原数据为需要，现在为需要，更新
			}else if((TransportStockInSetKey.SHIPPINGFEE_SET == pur_activity && TransportStockInSetKey.SHIPPINGFEE_SET == activity)){
				//更新
				this.updateTransportScheduleOneProdure("", "", logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[运费流程]",
						sendContent,
						page, mail, message, request, false, 5
						,TransportStockInSetKey.SHIPPINGFEE_SET, loginUserName, "修改转运单:"+transport_id+"[需要运费]"
						);
				
			//原运费已设置，更新
			}
//			else if(TransportStockInSetKey.SHIPPINGFEE_SETOVER == pur_activity){
//				//更新
//				this.updateTransportScheduleOneProdure("", "", logingUserId,
//						executePersonIds, joinPersonIds, 
//						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[运费流程]",
//						sendContent,
//						page, mail, message, request, false, 5
//						,TransportStockInSetKey.SHIPPINGFEE_SETOVER, loginUserName, "[运费流程]["+new TransportStockInSetKey().getStatusById(pur_activity)+"]"
//						);
//			}	
			//运费已申请
//			else if(TransportStockInSetKey.SHIPPINGFEE_APPLY == pur_activity)
//			{
//				//更新
//				this.updateTransportScheduleOneProdure("", "", logingUserId,
//						executePersonIds, joinPersonIds, 
//						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[运费流程]",
//						sendContent,
//						page, mail, message, request, false, 5
//						,TransportStockInSetKey.SHIPPINGFEE_APPLY, loginUserName, "[运费流程]["+new TransportStockInSetKey().getStatusById(pur_activity)+"]"
//						);
//			}
		} catch (Exception e) {
			throw new SystemException(e,"updateTransportStockInSet",log);
		}
	}
	/*
	 * 更新单证流程
	 */
	private void updateTransportCertificate(long logingUserId, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
	{
		try 
		{
			TDate tdateCertificate = new TDate(transport_date);
			tdateCertificate.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_certificate_period")));
			String endTimeCertificate = tdateCertificate.formatDate("yyyy-MM-dd HH:mm:ss");
			//单证流程	1.无需单证，2.单证采集中，3，单证采集完成
			if((0 == pur_activity && TransportCertificateKey.CERTIFICATE == activity) || (TransportCertificateKey.NOCERTIFICATE == pur_activity && TransportCertificateKey.CERTIFICATE == activity)){
				//添加
				this.addTransportScheduleOneProdure(transport_date, endTimeCertificate, logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[单证流程]",
						sendContent,
						page, mail, message, request, true, 9,
						TransportCertificateKey.CERTIFICATE, loginUserName, "修改转运单:"+transport_id+"[单证采集中]");
			}else if(TransportCertificateKey.CERTIFICATE == pur_activity && TransportCertificateKey.NOCERTIFICATE == activity){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 9
						, TransportCertificateKey.NOCERTIFICATE, loginUserName, "修改转运单:"+transport_id+"[无需单证]", logingUserId, request);
			}else if((TransportCertificateKey.CERTIFICATE == pur_activity && TransportCertificateKey.CERTIFICATE == activity)){
				//更新
				this.updateTransportScheduleOneProdure(transport_date, endTimeCertificate, logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[单证流程]",
						sendContent,
						page, mail, message, request, true, 9
						, TransportCertificateKey.CERTIFICATE, loginUserName, "修改转运单:"+transport_id+"[单证采集中]"
						);
			}	
		} catch (Exception e) {
			throw new SystemException(e,"updateTransportCertificate",log);
		}
	}
	/*
	 * 更新质检报告流程
	 */
	private void updateTransportQuality(long logingUserId, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			int page, int mail,int message, HttpServletRequest request, 
			boolean isNow,int processType, int activity, int pur_activity, String loginUserName, String sendContent, String transport_date) throws Exception
	{

		try 
		{
			TDate tdateQuality = new TDate(transport_date);
			tdateQuality.addDay(Integer.parseInt(systemConfig.getStringConfigValue("transport_quality_inspection")));
			String endTimeQuality = tdateQuality.formatDate("yyyy-MM-dd HH:mm:ss");
			//质检流程	3.质检中，4.质检完成
			if((0 == pur_activity && TransportQualityInspectionKey.NEED_QUALITY == activity) || (TransportQualityInspectionKey.NO_NEED_QUALITY == pur_activity && TransportQualityInspectionKey.NEED_QUALITY == activity)){
				//添加
				this.addTransportScheduleOneProdure(transport_date, endTimeQuality, logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[质检流程]",
						sendContent,
						page, mail, message, request, true, 11,
						TransportQualityInspectionKey.NEED_QUALITY, loginUserName, "修改转运单:"+transport_id+"[需要质检]");
			}else if(TransportQualityInspectionKey.NEED_QUALITY == pur_activity && TransportQualityInspectionKey.NO_NEED_QUALITY == activity){
				//删除
				this.deleteTransportScheduleById(transport_id, ModuleKey.TRANSPORT_ORDER, 11
						, TransportQualityInspectionKey.NO_NEED_QUALITY, loginUserName, "修改转运单:"+transport_id+"[无需质检]", logingUserId, request);
				
			//原数据需要且现需要		原数据处于质检中
			}else if((TransportQualityInspectionKey.NEED_QUALITY == pur_activity && TransportQualityInspectionKey.NEED_QUALITY == activity)){
				//更新
				this.updateTransportScheduleOneProdure(transport_date, endTimeQuality, logingUserId,
						executePersonIds, joinPersonIds, 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[质检流程]",
						sendContent,
						page, mail, message, request, true, 11
						,TransportQualityInspectionKey.NEED_QUALITY, loginUserName, "修改转运单:"+transport_id+"[需要质检]"
						);
			}
		} catch (Exception e) {
			throw new SystemException(e,"updateTransportQuality",log);
		}
	}
	
	/**
	 * 添加任务
	 */
	@Override
	public void addTransportScheduleOneProdure(String beginTime,
			String endTime, long adid, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			String title, String content, int page, int mail,
			int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, String employeeName, String logContent) throws Exception {
		
		try
		{
			boolean needPage	= false;
			boolean needMail	= false;
			boolean needMessage	= false;
			if(2 == page){
				needPage = true;
			}
			if(2 == mail){
				needMail = true;
			}
			if(2 == message){
				needMessage = true;
			}
			//添加之前再确定一下，是否此流程的任务真的没有
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(transport_id, moduleId, processType);
			if(null == schedule){
				scheduleMgrZr.addScheduleByExternalArrangeNow(beginTime, endTime, 
						adid, executePersonIds, joinPersonIds,
						transport_id, moduleId, title, content,
						needPage, needMail, needMessage, request, isNow, processType);
				//添加日志
				this.insertLogs(transport_id, logContent, adid, employeeName, processType,"", activity);
			}
			
			
		}catch (Exception e)
		{
			throw new SystemException(e,"addTransportScheduleOneProdure",log);
		}
		
	}
	
	
	/**
	 * 添加任务
	 */
	@Override
	public void addTransportScheduleOneProdure(String beginTime,
			String endTime, long adid, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			String title, String content, int page, int mail,
			int message, AdminLoginBean adminLoggerBean, boolean isNow,
			int processType, int activity, String employeeName, String logContent) throws Exception {
		
		try
		{
			boolean needPage	= false;
			boolean needMail	= false;
			boolean needMessage	= false;
			if(2 == page){
				needPage = true;
			}
			if(2 == mail){
				needMail = true;
			}
			if(2 == message){
				needMessage = true;
			}
			//添加之前再确定一下，是否此流程的任务真的没有
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(transport_id, moduleId, processType);
			if(null == schedule){
				scheduleMgrZr.addScheduleByExternalArrangeNow(beginTime, endTime, 
						adid, executePersonIds, joinPersonIds,
						transport_id, moduleId, title, content,
						needPage, needMail, needMessage, adminLoggerBean, isNow, processType);
				//添加日志
				this.insertLogs(transport_id, logContent, adid, employeeName, processType,"", activity);
			}
			
			
		}catch (Exception e)
		{
			throw new SystemException(e,"addTransportScheduleOneProdure",log);
		}
		
	}
	
	
	/**
	 * 更新任务
	 */
	@Override
	public void updateTransportScheduleOneProdure(String beginTime, String endTime, long adid, 
			String executePersonIds, String joinPersonIds,
			long transport_id, int moduleId, String title, String content,
			int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, String employeeName, String logContent
//			long transport_id, int moduleid, int processtype, string content,
//			int page, int mail, int message, HttpServletRequest request, String executePersonIds
				) throws Exception{
		
		try
		{
			boolean needPage	= false;
			boolean needMail	= false;
			boolean needMessage	= false;
			if(2 == page){
				needPage = true;
			}
			if(2 == mail){
				needMail = true;
			}
			if(2 == message){
				needMessage = true;
			}
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(transport_id, moduleId, processType);
			DBRow[] executePersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(transport_id, moduleId, processType);
			if(!"".equals(executePersonIds)){
				String[] adminUserIds	= executePersonIds.split(",");
				if(executePersons.length != adminUserIds.length){
					if(null != schedule){
						scheduleMgrZr.updateScheduleExternal(transport_id, moduleId, processType, 
								content, executePersonIds, request, needMail, needMessage, needPage);
						//添加日志
						this.insertLogs(transport_id, logContent, adid, employeeName, processType,"", activity);
					}else{
						this.addTransportScheduleOneProdure(beginTime, endTime, adid, executePersonIds,
								joinPersonIds, transport_id, moduleId, title, 
								content, page, mail, message, request, isNow, processType,
								activity, employeeName, logContent);
					}
				}else{
					HashSet<String> executePersonSet = new HashSet<String>();
					for (int i = 0; i < executePersons.length; i++) {
						executePersonSet.add(executePersons[i].getString("schedule_execute_id"));
					}
					int executePersonLen = executePersonSet.size();
					for (int i = 0; i < adminUserIds.length; i++) {
						executePersonSet.add(adminUserIds[i]);
					}
					int executePersonArrLen = executePersonSet.size();
					if(executePersonLen != executePersonArrLen){
						if(null != schedule){
							scheduleMgrZr.updateScheduleExternal(transport_id, moduleId, processType, 
									content, executePersonIds, request, needMail, needMessage, needPage);
							//添加日志
							this.insertLogs(transport_id, logContent, adid, employeeName, processType,"", activity);
						}else{
							this.addTransportScheduleOneProdure(beginTime, endTime, adid, executePersonIds,
									joinPersonIds, transport_id, moduleId, title, 
									content, page, mail, message, request, isNow, processType,
									activity, employeeName, logContent);
						}
					}
				}
			}
		}catch (Exception e)
		{
			throw new SystemException(e,"addTransportScheduleOneProdure",log);
		}
	}
	
	
	/**
	 * 更新任务
	 */
	@Override
	public void updateTransportScheduleOneProdure(String beginTime, String endTime, long adid, 
			String executePersonIds, String joinPersonIds,
			long transport_id, int moduleId, String title, String content,
			int page, int mail, int message, AdminLoginBean adminLoggerBean, boolean isNow,
			int processType, int activity, String employeeName, String logContent
//			long transport_id, int moduleid, int processtype, string content,
//			int page, int mail, int message, HttpServletRequest request, String executePersonIds
				) throws Exception{
		
		try
		{
			boolean needPage	= false;
			boolean needMail	= false;
			boolean needMessage	= false;
			if(2 == page){
				needPage = true;
			}
			if(2 == mail){
				needMail = true;
			}
			if(2 == message){
				needMessage = true;
			}
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(transport_id, moduleId, processType);
			DBRow[] executePersons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(transport_id, moduleId, processType);
			if(!"".equals(executePersonIds)){
				String[] adminUserIds	= executePersonIds.split(",");
				if(executePersons.length != adminUserIds.length){
					if(null != schedule){
						scheduleMgrZr.updateScheduleExternal(transport_id, moduleId, processType, 
								content, executePersonIds, adminLoggerBean, needMail, needMessage, needPage);
						//添加日志
						this.insertLogs(transport_id, logContent, adid, employeeName, processType,"", activity);
					}else{
						this.addTransportScheduleOneProdure(beginTime, endTime, adid, executePersonIds,
								joinPersonIds, transport_id, moduleId, title, 
								content, page, mail, message, adminLoggerBean, isNow, processType,
								activity, employeeName, logContent);
					}
				}else{
					HashSet<String> executePersonSet = new HashSet<String>();
					for (int i = 0; i < executePersons.length; i++) {
						executePersonSet.add(executePersons[i].getString("schedule_execute_id"));
					}
					int executePersonLen = executePersonSet.size();
					for (int i = 0; i < adminUserIds.length; i++) {
						executePersonSet.add(adminUserIds[i]);
					}
					int executePersonArrLen = executePersonSet.size();
					if(executePersonLen != executePersonArrLen){
						if(null != schedule){
							scheduleMgrZr.updateScheduleExternal(transport_id, moduleId, processType, 
									content, executePersonIds, adminLoggerBean, needMail, needMessage, needPage);
							//添加日志
							this.insertLogs(transport_id, logContent, adid, employeeName, processType,"", activity);
						}else{
							this.addTransportScheduleOneProdure(beginTime, endTime, adid, executePersonIds,
									joinPersonIds, transport_id, moduleId, title, 
									content, page, mail, message, adminLoggerBean, isNow, processType,
									activity, employeeName, logContent);
						}
					}
				}
			}
		}catch (Exception e)
		{
			throw new SystemException(e,"addTransportScheduleOneProdure",log);
		}
	}
	
	/**
	 * 根据转运单Id,业务类型，流程类型，删除任务
	 */
	@Override
	public void deleteTransportScheduleById(long transport_id, int moduleId, int processType, int activity, String employeeName, String logContent, long adid, HttpServletRequest request) throws Exception{
		try 
		{
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(transport_id, moduleId, processType);
			if(null != schedule){
				long scheduleId	= schedule.get("schedule_id", 0L);
				scheduleMgrZr.deleteScheduleByScheduleId(scheduleId, request);
				this.insertLogs(transport_id, logContent, adid, employeeName, processType, "", activity);
			}
			
		}
		catch (Exception e)
		{
			throw new SystemException(e,"deleteTransportScheduleById",log);
		}
	}
	
	public String handleSendContent(long transport_id, String adminUserNamesTransport, int declaration, String declarationPerson, 
			int clearance, String clearancePerson,int productFile, String productPerson,
			int tag, String tagPerson,int stockInSet, String stockPerson,
			int certificate, String certificatePerson,int quality, String qualityPerson) throws Exception{
		
		try
		{
			String rnStr				= "";
			DBRow transportRow			= floorTransportMgrLL.getTransportById(transport_id+"");
			long purchase_id			= transportRow.get("purchase_id",0L);
			//获取提货仓库的名字
			long pro_cata_id_send		= transportRow.get("send_psid", 0L);
		
			if(0 == purchase_id)
			{
				String cata_name_send		= "";
				DBRow storageCatalogSend	= floorStorageCatalogMgrZyj.getStorageCatalogById(pro_cata_id_send);
				if(null != storageCatalogSend)
				{
					cata_name_send = storageCatalogSend.getString("title");
					rnStr += "提货仓库:"+cata_name_send+",";
				}
			}
			//如果是交货单
			else
			{
				DBRow supplierRow	= supplierMgr.getDetailSupplier(pro_cata_id_send);
				String supplierName	= supplierRow.getString("sup_name");
				rnStr += "供应商:"+supplierName+",";
			}
			//获取收货仓库的名字
			long pro_cata_id_deliver	= transportRow.get("receive_psid",0L);
			DBRow storageCatalogDeliver	= floorStorageCatalogMgrZyj.getStorageCatalogById(pro_cata_id_deliver);
			String cata_name_deliver 	= "";
			if(null != storageCatalogDeliver){
				cata_name_deliver = storageCatalogDeliver.getString("title");
				rnStr += "收货仓库:"+cata_name_deliver+",";
			}
			//获取eta
			String eta = transportRow.getString("transport_receive_date");
			if(!"".equals(eta)){
				TDate tdateEta = new TDate(eta);
				String tdateEtaStr = tdateEta.formatDate("yyyy-MM-dd");
				rnStr += "预计到货时间:"+tdateEtaStr+",";
			}
			
			//转运货物的总体积，如果 有值，添加，此处待完善
			rnStr += "总体积:" +floorTransportMgrZJ.getTransportVolume(transport_id);
			
//			rnStr += "\n[货物状态]"+adminUserNamesTransport;
//			if(DeclarationKey.NODELARATION != declaration){
//				rnStr += "\n[报关流程]"+declarationPerson;
//			}
//			if(ClearanceKey.NOCLEARANCE != clearance){
//				rnStr += "\n[清关流程]"+clearancePerson;
//			}
//			if(TransportProductFileKey.NOPRODUCTFILE != productFile){
//				rnStr += "\n[图片流程]"+productPerson;
//			}
//			if(TransportTagKey.NOTAG != tag){
//				rnStr += "\n[制签流程]"+tagPerson;
//			}
//			if(TransportStockInSetKey.SHIPPINGFEE_NOTSET != stockInSet){
//				rnStr += "\n[运费流程]"+stockPerson;
//			}
//			if(TransportCertificateKey.NOCERTIFICATE != certificate){
//				rnStr += "\n[单证流程]"+certificatePerson;
//			}
//			if(QualityInspectionKey.NO_NEED_QUALITY != quality){
//				rnStr += "\n[质检流程]"+qualityPerson;
//			}
				
			return rnStr;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"handleSendContent",log);
		}
	}
	
	@Override
	public String getSchedulePersonIdsByTransportIdAndType(String transport_id, int moduleId, int processType) throws Exception{
		try 
		{
			DBRow[] persons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(transport_id), moduleId, processType);
			String personsIds = "";
			if(persons.length > 0){
				personsIds += persons[0].getString("schedule_execute_id");
				for (int i = 1; i < persons.length; i++) {
					personsIds += ","+persons[i].getString("schedule_execute_id");
				}
			}
			return personsIds;
		} 
		catch (Exception e) {
			throw new SystemException(e,"getSchedulePersonIdsByTransportIdAndType",log);
		}
		
	}
	
	@Override
	public String getSchedulePersonNamesByTransportIdAndType(String transport_id, int moduleId, int processType) throws Exception{
		try 
		{
			DBRow[] persons	= scheduleMgrZr.getScheduleExecuteUsersInfoByAssociate(Long.parseLong(transport_id), moduleId, processType);
			String personNames = "";
			if(persons.length > 0){
				personNames += persons[0].getString("employe_name");
				for (int i = 1; i < persons.length; i++) {
					personNames += ","+persons[i].getString("employe_name");
				}
			}
			return personNames;
		} 
		catch (Exception e) {
			throw new SystemException(e,"getSchedulePersonNamesByTransportIdAndType",log);
		}
	}
	
	@Override
	public String saveProductTagFile(HttpServletRequest request) throws Exception{
		try {
			// 这里上传的文件是保存在product_file这张表中pc_id
			// 保存的文件名为T_product_121212_商品分类_index(商品分类)
			//1=AMZON标签
			//2=UPC标签
			// 记录日志 日志中
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
		 
	   
				 
				String  pc_id = StringUtil.getString(request, "pc_id"); //商品的Ids
				String[] arrayPcIds = pc_id.split(",");
				long file_with_id = StringUtil.getLong(request,"transport_id");	//关联的业务Id
				int file_with_type = StringUtil.getInt(request,"file_with_type");
				int file_with_class = StringUtil.getInt(request,"file_with_class"); // 这个目前写成商品的那种标签
				String file_with_className = StringUtil.getString(request,"file_with_class_name");  
				String sn = StringUtil.getString(request,"sn");
				String path = StringUtil.getString(request,"path");
			 
				//得到上传的临时文件
				String fileNames = StringUtil.getString(request, "file_names");
				String[] fileNameArray = null ;
				if(fileNames.trim().length() > 0 ){
					fileNameArray = fileNames.trim().split(",");
				}
				if(fileNameArray != null && fileNameArray.length > 0){
					StringBuffer logFileNameContent = new StringBuffer();
					String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
					for(String tempFileName : fileNameArray){
						String tempSuffix = getSuffix(tempFileName);
						String tempfileUpName = tempFileName.replace("."+tempSuffix, "");
						StringBuffer realyFileName = new StringBuffer(sn);
						realyFileName.append("_").append(file_with_id).append("_").append(tempfileUpName);
						int indexFile = floorTransportMgrZr.getIndexOfProductFileByFileName(realyFileName.toString());
						if(indexFile != 0){realyFileName.append("_").append(indexFile);}
						realyFileName.append(".").append(tempSuffix);
						logFileNameContent.append(",").append(realyFileName.toString());
						String  tempUrl =  baseTempUrl+tempFileName;
						StringBuffer url = new StringBuffer();
						url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realyFileName.toString());
					
						FileUtil.moveFile(tempUrl,url.toString());
						for(int index = 0 , count = arrayPcIds.length ; index < count ; index++){
						DBRow file = new DBRow();
							file.add("file_name",realyFileName.toString());
					file.add("file_with_id",file_with_id);
					file.add("file_with_type",file_with_type);
					file.add("product_file_type",file_with_class);
					file.add("upload_adid",adminLoggerBean.getAdid());
					file.add("upload_time",DateUtil.NowStr());
							file.add("pc_id", arrayPcIds[index]);
					floorTransportMgrZr.addProductFile(file); //// 都是插入的同一张表 所以用这个方法;
						}
					}
			}
				return "" ;
		} catch (Exception e) {
			throw new SystemException(e,"saveProductTagFile",log);
		}
		
	}
	
	private void insertLogs(long transport_id ,String transport_content , long adid ,String employe_name, int transport_type , String eta, int activity) throws Exception{
		try{
			
			TDate tDate = new TDate();
			String currentTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
			DBRow row = new DBRow();
			row.add("transport_id", transport_id);
			row.add("transport_content", transport_content);
			row.add("transporter_id", adid);
			row.add("transporter", employe_name);
			row.add("transport_type", transport_type);
			row.add("activity_id", activity);
			if(eta != null && eta.trim().length()> 0){
				row.add("time_complete", eta);
			}
			row.add("transport_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			floorTransportMgrZr.addTransportLog(row);
			
			// 同时更新 主信息上的 updatedate,updateby(id),updatename。
			DBRow transportRow = new DBRow();
			transportRow.add("updatedate", currentTime);
			transportRow.add("updateby", adid);
			transportRow.add("updatename", employe_name);
			floorTransportMgrZr.updateTransportByIdAndDBRow(transport_id, transportRow);
			
		}catch (Exception e) {
			throw new SystemException(e, "insertLogs", log);
		}
	}
	
	/**
	 * 通过采购单Id，获得所有交货记录
	 */
	@Override
	public DBRow[] getTransportRowsByPurchaseId(long purhcaseId) throws Exception{
		try {
			return floorTransportMgrZyj.getTransportRowsByPurchaseId(purhcaseId);
		} catch (Exception e) {
			throw new SystemException(e, "getTransportRowsByPurchaseId", log);
		}
	}
	
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	
	/**
	 * 删除转运单的日志
	 * @param transportId
	 * @throws Exception
	 */
	public void deleteTransportLogsByTransportId(long transportId) throws Exception
	{
		try 
		{
			floorTransportMgrZyj.deleteTransportLogsByTransportId(transportId);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "deleteTransportLogsByTransportId", log);
		}
	}
	/**
	 * 删除转运单的运费项目
	 */
	public void deleteTransportFreightByTransportId(long transportId) throws Exception
	{
		try 
		{
			floorTransportMgrZyj.deleteTransportFreightByTransportId(transportId);
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "deleteTransportFreightByTransportId", log);
		}
	}
	
	/**
	 * 删除转运单的相关文件
	 * @param transportId
	 * @param types
	 * @throws Exception
	 */
	@Override
	public void deleteTransportFileByTransportIdAndTypes(long transportId, int[] types) throws Exception
	{
		try
		{
			floorTransportMgrZyj.deleteTransportFileByTransportIdAndTypes(transportId, types);
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "deleteTransportLogsByTransportId", log);
		}
	}
	
	/**
	 * 删除与转运单相关的商品文件
	 */
	@Override
	public void deleteTransportProductFileByTransportIdAndTypes(long transportId, int[] types) throws Exception
	{
		try 
		{
			floorTransportMgrZyj.deleteTransportProductFileByTransportIdAndTypes(transportId, types);
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "deleteTransportProductFileByTransportIdAndTypes", log);
		}
	}
	
	/**
	 * 根据当前登录者的Id，获取其上次创建的转运单的ID
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public String getTransportLastTimeCreateByAdid(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean	= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			DBRow row = floorTransportMgrZyj.getTransportLastTimeCreateByAdid(adminLoggerBean.getAdid());
			if(null == row)
			{
				return "";
			}
			else
			{
				return row.getString("transport_id");
			}
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "getTransportLastTimeCreateByAdid", log);
		}
	}
	
	/**
	 * 根据当前登录者的Id，获取其上次创建的交货单的ID
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public String getTransportPurchaseLastTimeCreateByAdid(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean	= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			DBRow row = floorTransportMgrZyj.getTransportPurchaseLastTimeCreateByAdid(adminLoggerBean.getAdid());
			if(null == row)
			{
				return "";
			}
			else
			{
				return row.getString("transport_id");
			}
		} 
		catch (Exception e)
		{
			throw new SystemException(e, "getTransportPurchaseLastTimeCreateByAdid", log);
		}
	}
	/**
	 * 转运单到货派送
	 */
	public void updateTransportGoodsArriveDelivery(HttpServletRequest request) throws Exception
	{
		try
		{
			AdminLoginBean adminLoggerBean	= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid				= adminLoggerBean.getAdid();
			String employeeName		= adminLoggerBean.getEmploye_name();
			long transport_id		= StringUtil.getLong(request, "transport_id");
			String deliveryed_date	= StringUtil.getString(request, "deliveryed_date");
			long deliveryer_id		= StringUtil.getLong(request, "deliveryer_id");
			String deliveryer_name	= StringUtil.getString(request, "deliveryer_name");
			
			DBRow transportRow		= floorTransportMgrZJ.getDetailTransportById(transport_id);
			int transport_status	= transportRow.get("transport_status", 0);
			if(transport_status != TransportOrderKey.INTRANSIT)
			{
				throw new TransportStockInHandleErrorException("转运单入库状态不正确!");
			}
			DBRow row = new DBRow();
			row.add("deliveryed_date", deliveryed_date);
			row.add("deliveryer_id", deliveryer_id);
			row.add("transport_status", TransportOrderKey.AlREADYARRIAL);
			row.add("put_status",TransportPutKey.Puting);
			floorTransportMgrZJ.modTransport(transport_id, row);
			
			String content = employeeName+"确认了单据:T"+transport_id+"到货派送,仓库收货人:"+deliveryer_name+"送至时间:"+deliveryed_date;
			//日志
			this.insertLogs(transport_id, content, adid, employeeName, 4, "", TransportOrderKey.AlREADYARRIAL);
			//跟进
			scheduleMgrZr.addScheduleReplayExternal(transport_id , ModuleKey.TRANSPORT_ORDER ,
					4 , content , false, request, "transport_intransit_period");
			
			//给仓库人员创建任务，如果有任务了，就不创建了
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.STORAGEPERSON);
			if(null == schedule)
			{
				this.addTransportScheduleOneProdure(DateUtil.NowStr(), "", adid,
						String.valueOf(deliveryer_id), "", 
						transport_id, ModuleKey.TRANSPORT_ORDER, "转运单:"+transport_id+"[主流程]",
						content,2, 2, 2, request, true, TransportLogTypeKey.STORAGEPERSON,
						transport_status, employeeName,content);
			}
		}
		catch (TransportStockInHandleErrorException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e, "updateTransportGoodsArriveDelivery", log);
		}
	}
	
	/**
	 * 根据转运单ID及商品名严格查询返修单详细
	 * @param repair_order_id
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public DBRow getTransportDetailsByTransportProductName(HttpServletRequest request)throws Exception
	{
		try
		{
			long transport_id	= StringUtil.getLong(request, "transport_id");
			String name				= StringUtil.getString(request, "product_name");
			DBRow product			= floorProductMgr.getDetailProductByPname(name);
			DBRow result			= new DBRow();
			if(null == product)
			{
				result.add("flag", "product_not_exist");
			}
			else
			{
				DBRow[] details = floorTransportMgrZyj.getTransportDetailsByTransportProductName(transport_id, name);
				if(details.length == 0)
				{
					result.add("flag", "not_exist");
				}
				else
				{
					result.add("flag", "exist");
				}
			}
			return result;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getRepairDetailByRepairProductName",log);
		}
	}
	
	/**
	 * 跟进第三方标签流程
	 * @param request
	 * @throws Exception
	 */
	@Override
	public void handleTransportThirdTag(HttpServletRequest request)
	throws Exception {
		try{
			AdminLoginBean adminLoggerBean =((AdminMgrIFace) MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			TDate tDate = new TDate();
			//重新命名图片文件T_tag_transportId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			long file_with_id  = StringUtil.getLong(request, "transport_id");
			int file_with_type = StringUtil.getInt(request, "file_with_type");
			int file_with_class  = StringUtil.getInt(request, "tag_third");// 这个目前写成跟进的阶段 比如 是查货中。或者是完成
			String sn = StringUtil.getString(request, "sn");
			String path = StringUtil.getString(request, "path");
			
			String fileNames = StringUtil.getString(request, "file_names");
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false ) && (file_with_class == TransportTagKey.FINISH) ;
			StringBuffer fileNameSb = new StringBuffer(""); 
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							StringBuffer context = new StringBuffer();
							context.append(StringUtil.getString(request,"context"));
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(file_with_id)
							.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorTransportMgrZr.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							 
							fileNameSb.append(",").append(realFileName);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
							this.addTransportFileLogs(realFileName.toString(), file_with_id, file_with_type, file_with_class, adminLoggerBean.getAdid(), DateUtil.NowStr());
						}
					}
				}
			}
			// 添加日志
			String context = StringUtil.getString(request, "context");
			if(!isFileUp){
				String eta = StringUtil.getString(request, "eta");
				this.insertLogs(file_with_id, context ,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),TransportLogTypeKey.THIRD_TAG, eta, file_with_class);
			}else{
				this.insertLogs(file_with_id, context,  adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(),TransportLogTypeKey.THIRD_TAG, null, file_with_class);
			}		 
			DBRow updateTransportRow = new DBRow();
			updateTransportRow.add("tag_third", file_with_class);
			if(file_with_class == TransportTagKey.FINISH){
				//如果是运费转账完成的话
				DBRow transportRow = floorTransportMgrLL.getTransportById(file_with_id+"");
			 
			 	double diffDay = this.getDiffTimeByTransportCreateTime(transportRow.getString("transport_date"));
		
				updateTransportRow.add("tag_third_over", diffDay);
			}
			floorTransportMgrZr.updateTransportByIdAndDBRow(file_with_id, updateTransportRow);
		
			boolean isCompelte = (file_with_class == TransportTagKey.FINISH);
			String scheduleContext = "转运单:"+file_with_id+StringUtil.getString(request,"context");
			scheduleMgrZr.addScheduleReplayExternal(file_with_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.THIRD_TAG,scheduleContext.toString(), isCompelte, request, "transport_tag_period");
		 
		}catch (Exception e) {
			 throw new SystemException(e, "hanleTransportStockInSet", log);
		}
	}
	
	/**
	 * 文件不能为重复的。就是说文件如果有了，那么就会覆盖掉原来数据库中的数据
	 * @param fileName
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param upload_adid
	 * @param upload_time
	 * @throws Exception
	 */
	// 在File表中添加一个字段
	private void addTransportFileLogs(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception{
		try{
 
			DBRow file = new DBRow();
			file.add("file_name",fileName);
			file.add("file_with_id",file_with_id);
			file.add("file_with_type",file_with_type);
			file.add("file_with_class",file_with_class);
			file.add("upload_adid",upload_adid);
			file.add("upload_time",DateUtil.NowStr());
			floorTransportMgrZr.addFileNotExits(file); //// 都是插入的同一张表 所以用这个方法;
		}catch (Exception e) {
			 throw new SystemException(e, "addTransportLogs", log);
		}
	}
	
	private double getDiffTimeByTransportCreateTime(String transport_date) throws Exception{
		try {
			TDate tDate = new TDate();
			TDate endDate;
	
			endDate = new TDate(transport_date);
			double diffDay = tDate.getDiffDate(endDate.formatDate("yyyy-MM-dd HH:mm:ss"), "dd");
			return diffDay;
		} catch (Exception e) {
			 throw new SystemException(e, "getDiffTimeByTransportCreateTime", log);
		}
		
	}
	
	/**
	 * 检验转运单商品文件是否可以完成
	 */
	public String checkTransportProductFileFinish(long transport_id) throws Exception
	{
		try
		{
			StringBuffer prompt = new StringBuffer();
			int[]  types = {FileWithTypeKey.product_file};
			String value = systemConfig.getStringConfigValue("transport_product_file");
			if(!"".equals(value))
			{
				String[] arraySelected = value.split("\n");
				for (int i = 0; i < arraySelected.length; i++) {
					if(!"".equals(arraySelected[i]))
					{
						String[] typeIdName = arraySelected[i].split("=");
						if(2 == typeIdName.length)
						{
							int[] typeId 			= {Integer.parseInt(typeIdName[0])};
							String typeName			= typeIdName[1];
							DBRow[] productPackages	= floorPurchaseMgrZyj.getAllProductFilesByProductModelAndTypes(transport_id,types, typeId);
							if(0 == productPackages.length)
							{
								prompt.append("请上传"+typeName+"的照片").append(",");
							}
						}
					}
				}
			}
			if(prompt.length() > 0)
			{
				return prompt.substring(0, prompt.length()-1);
			}
			return "";
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "checkTransportProductFileFinish", log);
		}
	}
	
	/**
	 * 商品标签是否全部上传完全
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String checkTransportProductTagFileFinish(HttpServletRequest request) throws Exception
	{
		try
		{
			long file_with_id = StringUtil.getLong(request, "transport_id");
			StringBuffer prompt	= new StringBuffer();
			
			int[] fileType	= {FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE};
			String value = systemConfig.getStringConfigValue("transport_tag_types");
			if(!"".equals(value))
			{
				String[] arraySelected = value.split("\n");
		  		for (int i = 0; i < arraySelected.length; i++) {
					if(!"".equals(arraySelected[i]))
					{
						String[] typeIdName	= arraySelected[i].split("=");
						if(2 == typeIdName.length)
						{
							int[] typeId	= {Integer.parseInt(typeIdName[0])};
							String typeName	= typeIdName[1];
							DBRow[] tagFile	= floorPurchaseMgrZyj.getAllProductFileByPcId(fileType, typeId, file_with_id);
							if(0 == tagFile.length)
							{
								prompt.append("请上传"+typeName+"的照片").append(",");
							}
						}
					}
				}
			}
			if(prompt.length() > 0)
			{
				return prompt.substring(0, prompt.length()-1);
			}
			return "";
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "checkTransportProductTagFileFinish", log);
		}
	}
	
	/**
	 * 检查申请货款的提示
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String checkTransportApplyFundsPrompt(HttpServletRequest request) throws Exception
	{
		try
		{
//				转运单资金申请
//	  			1.交货单(有关联的) 
//	  			2.质检,标签,上图图片 (有并且都完成。或者是没有的)
//	  			显示资金申请的按钮
//	  			3.申请过后就不能再显示这个按钮
			StringBuffer prompt			= new StringBuffer();
			long transport_id			= StringUtil.getLong(request, "transport_id");
			DBRow transport				= floorTransportMgrLL.getTransportById(transport_id+"");
  			int qualityKeyValue			= transport.get("quality_inspection",TransportQualityInspectionKey.NO_NEED_QUALITY);
  			boolean isQuanlityOk 		=  ( qualityKeyValue == TransportQualityInspectionKey.FINISH || qualityKeyValue == TransportQualityInspectionKey.NO_NEED_QUALITY );
  			int tagKeyValue				=  transport.get("tag",TransportTagKey.NOTAG);
  			boolean isTagOk				= (tagKeyValue ==  TransportTagKey.FINISH || tagKeyValue == TransportTagKey.NOTAG);
  			int tagThirdKeyValue		=  transport.get("tag_third",TransportTagKey.NOTAG);
  			boolean isTagThirdOk		= (tagThirdKeyValue ==  TransportTagKey.FINISH || tagThirdKeyValue == TransportTagKey.NOTAG);
  			int productFileKeyValue		=  transport.get("product_file",TransportProductFileKey.NOPRODUCTFILE);
  			boolean isProductFileOk		= (productFileKeyValue == TransportProductFileKey.FINISH || productFileKeyValue == TransportProductFileKey.NOPRODUCTFILE);
  			DBRow[] applyMoneyRows		= floorApplyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100001,transport_id,FinanceApplyTypeKey.DELIVERY_ORDER);
  			boolean isNotHasApplyMoney	= true;
  			if(applyMoneyRows != null && applyMoneyRows.length > 0){
  				isNotHasApplyMoney		= false;
  			} 
  			if(!isQuanlityOk)
  			{
  				prompt.append("质检要求未完成").append(",");
  			}
  			if(!isTagOk)
  			{
  				prompt.append("内部标签未完成").append(",");
  			}
  			if(!isTagThirdOk)
  			{
  				prompt.append("第三方标签未完成").append(",");
  			}
  			if(!isProductFileOk)
  			{
  				prompt.append("实物图片未完成").append(",");
  			}
  			if(!isNotHasApplyMoney)
  			{
  				prompt.append("已申请过货款").append(",");
  			}
  			if(prompt.length() > 0)
			{
				return prompt.substring(0, prompt.length()-1);
			}
			return "";
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "checkTransportApplyFundsPrompt", log);
		}
	}
	
	/**
	 * 将转运单实物图片流程阶段从完成改成需要
	 */
	public void updateTransportPcModelFinishToNeed(HttpServletRequest request) throws Exception
	{
		try 
		{
			//修改转运单实物图片状态
			long transport_id	= StringUtil.getLong(request, "transport_id");
			String context		= StringUtil.getString(request, "context");
			String e_finish_time= StringUtil.getString(request, "eta");
			DBRow transportRow	= new DBRow();
			transportRow.add("product_file", TransportProductFileKey.PRODUCTFILE);
			floorTransportMgrZr.updateTransportByIdAndDBRow(transport_id, transportRow);
			//加日志
			AdminLoginBean adminLoggerBean	= ((AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr")).getAdminLoginBean(StringUtil.getSession(request));
			long adid				= adminLoggerBean.getAdid();
			String employeeName		= adminLoggerBean.getEmploye_name();
			TDate tDate = new TDate();
			DBRow row = new DBRow();
			row.add("transport_id", transport_id);
			row.add("transport_content", context+"(从完成到需要)");
			row.add("transporter_id", adid);
			row.add("transporter", employeeName);
			row.add("transport_type", 10);
			row.add("activity_id", TransportProductFileKey.PRODUCTFILE);
			row.add("time_complete", e_finish_time);
			row.add("transport_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			floorTransportMgrZr.addTransportLog(row);
			//任务跟进
			DBRow schedule	= scheduleMgrZr.getScheduleByAssociate(transport_id, ModuleKey.TRANSPORT_ORDER, 10);
			if(null != schedule)
			{
				String contentPcFile = adminLoggerBean.getEmploye_name()+"确认了转运单:"+transport_id+"(从完成到需要),"+context;
				scheduleMgrZr.addScheduleReplayExternal(transport_id, ModuleKey.TRANSPORT_ORDER, 10, contentPcFile, false, request, "transport_product_file_period");
			}
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "updateTransportPcModelFinishToNeed", log);
		}
	}
	
	/**
	 * 采购单余额
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public double checkTransportCanApplyMoneyToPurchase(long transport_id, long purchase_id) throws Exception
	{
		try
		{
			//查询所有该purchase 申请的交货单转运的金额。然后和现在的金额比较
			//用standard_money 进行计算
			double purchaseDetailMoneny = floorPurchaseMgr.getPurchasePrice(purchase_id);
			//此交货单所属采购单下所有的交货单申请资金
			DBRow[] rows = floorPreparePurchaseFundsMgrZwb.getApplyMoneysOfPurchase(purchase_id);
			DBRow[] applyMoneys = floorApplyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(100009,purchase_id,FinanceApplyTypeKey.PURCHASE_ORDER);
			double purchaseDeposit = 0.0;
			if(applyMoneys.length > 0)
			{
				purchaseDeposit = applyMoneys[0].get("standard_money", 0.0);
			}
			double sumOfApplyMoney = 0.0 ;
			if(rows != null && rows.length>0 )
			{
				for(DBRow row : rows)
				{
					sumOfApplyMoney += row.get("standard_money", 0.0d);
				}
			}
			double total = purchaseDetailMoneny - (sumOfApplyMoney + purchaseDeposit);
			return total;
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "checkTransportCanApplyMoneyToPurchase", log);
		}
	}
	
	/**
	 * 保存交货型转运单，手持设备
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public long addPurchaseTransportByXml(String xml, HttpServletRequest request) throws Exception
	{
		try
		{
			long transport_id	= !"".equals(StringUtil.getSampleNode(xml, "transport_id"))?Long.parseLong(StringUtil.getSampleNode(xml, "transport_id")):0L;
			int submitPageType	= !"".equals(StringUtil.getSampleNode(xml, "submitPageType"))?Integer.parseInt(StringUtil.getSampleNode(xml, "submitPageType")):1;
			long purchase_id	= !"".equals(StringUtil.getSampleNode(xml, "purchase_id"))?Long.parseLong(StringUtil.getSampleNode(xml, "purchase_id")):0L;
			String loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
			DBRow loginDetail = floorAdminMgr.getDetailAdminByAccount(loginAccount);
			if(null == loginDetail)
			{
				throw new MachineUnderFindException();
			}
			long adid = loginDetail.get("adid", 0L);
			String employee_name = loginDetail.getString("employe_name");
			//提货地址，更新保存
			if(PurchaseTransportPageTypesKey.SEND_ADDRESS == submitPageType)
			{
				transport_id = this.savePurchaseTransportSendAddress(transport_id, purchase_id, adid, employee_name, xml);
			}
			//收货地址，更新保存
			else if(PurchaseTransportPageTypesKey.DELIVER_ADDRESS == submitPageType)
			{
				transport_id = this.savePurchaseTransportDeliverAddress(transport_id, purchase_id, adid, employee_name, xml);
			}
			//其他，更新保存
			else if(PurchaseTransportPageTypesKey.OTHERS == submitPageType)
			{
				String remark = StringUtil.getSampleNode(xml, "remark");
				if(!"".equals(remark))
				{
					DBRow transportRow = new DBRow();
					transportRow.add("remark", remark);
					floorTransportMgrZyj.updateTransport(transport_id, transportRow);
				}
			}
			//转运单明细，更新保存
			else if(PurchaseTransportPageTypesKey.TRANSPORT_DETAILS == submitPageType)
			{
				this.saveTransportItems(transport_id, purchase_id, adid, employee_name, xml);
			}
			//流程指派，更新保存
			else if(PurchaseTransportPageTypesKey.PROCESS_INFO == submitPageType)
			{
				this.saveAllProdures(transport_id, adid, employee_name, xml, request);
			}
			//运输公司，更新保存
			else if(PurchaseTransportPageTypesKey.FREGHT_COMPANY == submitPageType)
			{
				this.saveFreightCompany(transport_id, xml);
			}
			//运费设置，更新保存
			else if(PurchaseTransportPageTypesKey.FREGHT_COST == submitPageType)
			{
				this.saveFreightCosts(transport_id, xml);
			}
			
			return transport_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addPurchaseTransportByXml(xml)", log);
		}
	}
	
	public long addPurchaseTransportByXmlStr(String xml, HttpServletRequest request) throws Exception
	{
		try
		{
			long transport_id	= !"".equals(StringUtil.getSampleNode(xml, "transport_id"))?Long.parseLong(StringUtil.getSampleNode(xml, "transport_id")):0L;
			if(!(transport_id>0))
			{
				transport_id = 0;
			}
//			int submitPageType	= !"".equals(StrUtil.getSampleNode(xml, "submitPageType"))?Integer.parseInt(StrUtil.getSampleNode(xml, "submitPageType")):1;
			long purchase_id	= !"".equals(StringUtil.getSampleNode(xml, "purchase_id"))?Long.parseLong(StringUtil.getSampleNode(xml, "purchase_id")):-1L;
			String loginAccount = StringUtil.getSampleNode(xml,"LoginAccount");
			DBRow loginDetail = floorAdminMgr.getDetailAdminByAccount(loginAccount);
			if(null == loginDetail)
			{
				throw new MachineUnderFindException();
			}
			long adid = loginDetail.get("adid", 0L);
			String employee_name = loginDetail.getString("employe_name");
			String machine = StringUtil.getSampleNode(xml, "Machine");
			//提货地址
			String send_psid			= StringUtil.getSampleNode(xml,"send_psid");
			String send_ccid			= StringUtil.getSampleNode(xml,"send_ccid");
			String send_pro_id			= StringUtil.getSampleNode(xml,"send_pro_id");
			String send_city			= StringUtil.getSampleNode(xml,"send_city");
			String send_house_number	= StringUtil.getSampleNode(xml,"send_house_number");
			String send_street			= StringUtil.getSampleNode(xml,"send_street");
			String send_zip_code		= StringUtil.getSampleNode(xml,"send_zip_code");
			String send_name			= StringUtil.getSampleNode(xml,"send_name");
			String send_linkman_phone	= StringUtil.getSampleNode(xml,"send_linkman_phone");
			String address_state_send	= StringUtil.getSampleNode(xml, "address_state_send");
			String transport_status		= StringUtil.getSampleNode(xml, "transport_status");
			DBRow sendProductStorage	= floorCatalogMgr.getDetailProductStorageCatalogById(Long.parseLong(send_psid));
			int from_ps_type			= (null != sendProductStorage?sendProductStorage.get("storage_type", 0):0);
			
			//收货地址
			String receive_psid			= StringUtil.getSampleNode(xml,"receive_psid");
			String deliver_ccid			= StringUtil.getSampleNode(xml,"deliver_ccid");
			String deliver_pro_id		= StringUtil.getSampleNode(xml,"deliver_pro_id");			
			String deliver_city			= StringUtil.getSampleNode(xml,"deliver_city");
			String deliver_house_number	= StringUtil.getSampleNode(xml,"deliver_house_number");
			String deliver_street		= StringUtil.getSampleNode(xml,"deliver_street");
			String deliver_zip_code		= StringUtil.getSampleNode(xml,"deliver_zip_code");
			String transport_linkman		= StringUtil.getSampleNode(xml,"transport_linkman");
			String transport_linkman_phone	= StringUtil.getSampleNode(xml,"transport_linkman_phone");
			String address_state_deliver	= StringUtil.getSampleNode(xml, "address_state_deliver");
			String transport_receive_date	= StringUtil.getSampleNode(xml, "transport_receive_date");
			DBRow receiveProductStorage	= floorCatalogMgr.getDetailProductStorageCatalogById(Long.parseLong(receive_psid));
			int target_ps_type			= (null != receiveProductStorage?receiveProductStorage.get("storage_type", 0):0);
			
			//其他
			String remark = StringUtil.getSampleNode(xml, "remark");
			
			//运输设置
			String fr_id					= StringUtil.getSampleNode(xml, "fr_id");
			String transport_waybill_name	= StringUtil.getSampleNode(xml, "transport_waybill_name");
			String transport_waybill_number	= StringUtil.getSampleNode(xml, "transport_waybill_number");
			String transportby				= StringUtil.getSampleNode(xml, "transportby");
			String carriers					= StringUtil.getSampleNode(xml, "carriers"); 
			String transport_send_place		= StringUtil.getSampleNode(xml, "transport_send_place");
			String transport_receive_place	= StringUtil.getSampleNode(xml, "transport_receive_place");
			
			
			DBRow transport = new DBRow();
			//采购单号
			transport.add("purchase_id", purchase_id);
			//提货信息
			transport.add("send_psid",send_psid);
			transport.add("send_ccid",send_ccid);
			transport.add("send_pro_id",send_pro_id);
			transport.add("send_city",send_city);
			transport.add("send_house_number",send_house_number);
			transport.add("send_street",send_street);
			transport.add("send_zip_code",send_zip_code);
			transport.add("send_name",new String(send_name.getBytes("utf-8")));
			transport.add("send_linkman_phone",send_linkman_phone);
			transport.add("address_state_send", address_state_send);
			transport.add("from_ps_type",from_ps_type);
			////system.out.println("address_state_send:"+address_state_send+","+address_state_deliver);
			//收货信息
			transport.add("receive_psid",receive_psid);
			transport.add("deliver_ccid",deliver_ccid);
			transport.add("deliver_pro_id",deliver_pro_id);
			transport.add("deliver_city",deliver_city);
			transport.add("deliver_house_number",deliver_house_number);
			transport.add("deliver_street",deliver_street);
			transport.add("deliver_zip_code",deliver_zip_code);
			transport.add("transport_address","");
			transport.add("transport_linkman",transport_linkman);
			transport.add("transport_linkman_phone",transport_linkman_phone);
			transport.add("address_state_deliver", address_state_deliver);
			transport.add("target_ps_type" ,target_ps_type);
			
			//其他信息(ETA,备注)
			if(null != transport_receive_date && !"null".equals(transport_receive_date) && !"".equals(transport_receive_date))
			{
				transport.add("transport_receive_date", transport_receive_date);
			}
			if(null!=remark && !"".equals(remark) && !"null".equals(remark))
			{
				transport.add("remark", remark);
			}
			
			
			//运输设置
			if(null != fr_id && !"".equals(fr_id) && !"null".equals(fr_id) &&!"0".equals(fr_id))
			{
				transport.add("fr_id",fr_id);
				transport.add("transport_waybill_name",transport_waybill_name);
				transport.add("transport_waybill_number",transport_waybill_number);
				transport.add("transportby",transportby);
				transport.add("carriers",carriers);
				transport.add("transport_send_place",transport_send_place);
				transport.add("transport_receive_place",transport_receive_place);
			}
			
			DBRow transportOriginal = floorTransportMgrZJ.getDetailTransportById(transport_id);
			int status_transport = 0;
			if(null != transportOriginal)
			{
				status_transport = transportOriginal.get("transport_status", 0);
			}
			if(0 != status_transport)
			{
				if(!"".equals(transport_status))
				{
					status_transport = Integer.parseInt(transport_status);
				}
				else
				{
					status_transport = TransportOrderKey.READY;
				}
			}
			
			
			//主流程信息
			transport.add("transport_status", status_transport);//创建的转运单都是准备中
			transport.add("updatedate",DateUtil.NowStr());
			transport.add("updateby",adid);//转运单的更新者
			transport.add("updatename",employee_name);//转运单的更新者
			if(0 == transport_id)
			{
				if(0 != purchase_id)
				{
					//设置交货型转运的批次
					DBRow[] purchaseTransportOrder = floorTransportMgrZJ.getTranposrtOrdersByPurchaseId(purchase_id);
					int number = 1;
					try 
					{
						if(purchaseTransportOrder.length>0)
						{
							String numbers = purchaseTransportOrder[purchaseTransportOrder.length-1].getString("transport_number");
							number = Integer.parseInt(numbers)+1;
						}
					}
					catch (NumberFormatException e) 
					{
						number = 1;
					}
					transport.add("transport_number",number);
				}
				transport.add("transport_date",DateUtil.NowStr());
				transport.add("create_account_id",adid);//转运单的创建者
				transport.add("create_account",employee_name);//转运单的创建者
				transport.add("machine", machine.toUpperCase());
				int is_not_page_sub =  !"".equals(StringUtil.getSampleNode(xml, "is_not_page_sub"))?Integer.parseInt(StringUtil.getSampleNode(xml, "is_not_page_sub")):1;
				transport.add("is_not_page_sub",is_not_page_sub);//是否是页面提交的
				transport_id = floorTransportMgrZJ.addTransport(transport);
			}
			else
			{
				floorTransportMgrZyj.updateTransport(transport_id, transport);
			}
			
			//流程指派，更新保存
			this.saveAllProdures(transport_id, adid, employee_name, xml, request);
			
			//运费设置，更新保存
			this.saveFreightCosts(transport_id, xml);
			
			String isCreate = 0!=transport_id?"更新":"创建";
			
			floorTransportMgrLL.insertLogs(Long.toString(transport_id), isCreate+"转运单:单号"+transport_id, (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getAdid(), (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name(),TransportLogTypeKey.Goods,TransportOrderKey.READY);//类型1跟进记录
			
			return transport_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addPurchaseTransportByXmlStr(xml)", log);
		}
	}
	
	/**
	 * 保存运费设置
	 * @param transport_id
	 * @param xml
	 * @throws Exception
	 */
	public void saveFreightCosts(long transport_id, String xml) throws Exception
	{
		try
		{
//			int changed = !"".equals(StrUtil.getSampleNode(xml, "changed"))?Integer.parseInt(StrUtil.getSampleNode(xml, "changed")):0;
			if(-1 != xml.indexOf("<freight>"))
			{
				xml = xml.substring(xml.indexOf("<freight>")+9, xml.lastIndexOf("</freight>"));
				String[] freightCostsSingle = xml.split("</freight><freight>");
				////system.out.println("freightCostsSingle:"+freightCostsSingle.length);
				floorFreightMgrLL.deleteTransportFreightCostByDeliveryId(String.valueOf(transport_id));
				for (int i = 0; i < freightCostsSingle.length; i++)
				{
					int changed=Integer.parseInt(StringUtil.getSampleNode(freightCostsSingle[i], "changed"));
					String tfc_id=StringUtil.getSampleNode(freightCostsSingle[i], "tfc_id");
					String tfc_project_name=StringUtil.getSampleNode(freightCostsSingle[i], "tfc_project_name");
					String tfc_way=StringUtil.getSampleNode(freightCostsSingle[i], "tfc_way");
					String tfc_unit=StringUtil.getSampleNode(freightCostsSingle[i], "tfc_unit");
					String tfc_unit_price=StringUtil.getSampleNode(freightCostsSingle[i], "tfc_unit_price");
					String tfc_unit_count=StringUtil.getSampleNode(freightCostsSingle[i], "tfc_unit_count");
					String tfc_currency=StringUtil.getSampleNode(freightCostsSingle[i], "tfc_currency");
					String tfc_exchange_rate=StringUtil.getSampleNode(freightCostsSingle[i], "tfc_exchange_rate");
					
					DBRow row = new DBRow();
	//				row.add("tfc_id", tfc_id);
					row.add("transport_id", transport_id);
					row.add("tfc_project_name", tfc_project_name);
					row.add("tfc_way", tfc_way);
					row.add("tfc_unit", tfc_unit);
					row.add("tfc_unit_price", tfc_unit_price);
					row.add("tfc_unit_count", tfc_unit_count);
					row.add("tfc_currency", tfc_currency);
					row.add("tfc_exchange_rate", tfc_exchange_rate);
					floorFreightMgrLL.insertTransportFreightCost(row);
				}
			
			}	
//			List<List<String>> list = new ArrayList<List<String>>();
//			String[] arr1 = xml.split("<freight>");
//			for (int i = 1; i < arr1.length; i++)
//			{
//				String[] arr2 = arr1[i].split("</freight>");
//				//for (int j = 0; j < arr2.length-1; j++)
//				for (int j = 0; j < arr2.length; j++)
//				{
//					int changed=Integer.parseInt(StrUtil.getSampleNode(arr2[j], "changed"));
//					String tfc_id=StrUtil.getSampleNode(arr2[j], "tfc_id");
//					String tfc_project_name=StrUtil.getSampleNode(arr2[j], "tfc_project_name");
//					String tfc_way=StrUtil.getSampleNode(arr2[j], "tfc_way");
//					String tfc_unit=StrUtil.getSampleNode(arr2[j], "tfc_unit");
//					String tfc_unit_price=StrUtil.getSampleNode(arr2[j], "tfc_unit_price");
//					String tfc_unit_count=StrUtil.getSampleNode(arr2[j], "tfc_unit_count");
//					String tfc_currency=StrUtil.getSampleNode(arr2[j], "tfc_currency");
//					String tfc_exchange_rate=StrUtil.getSampleNode(arr2[j], "tfc_exchange_rate");
//					if(changed == 2) 
//					{
//						floorFreightMgrLL.deleteTransportFreightCostByDeliveryId(String.valueOf(transport_id));
//					}
//					DBRow row = new DBRow();
//					row.add("tfc_id", tfc_id);
//					row.add("transport_id", transport_id);
//					row.add("tfc_project_name", tfc_project_name);
//					row.add("tfc_way", tfc_way);
//					row.add("tfc_unit", tfc_unit);
//					row.add("tfc_unit_price", tfc_unit_price);
//					row.add("tfc_unit_count", tfc_unit_count);
//					row.add("tfc_currency", tfc_currency);
//					row.add("tfc_exchange_rate", tfc_exchange_rate);
//					if(changed==2||row.getString("tfc_id").equals("") || row.getString("tfc_id").equals("0")) 
//					{
//						row.remove("tfc_id");
//						floorFreightMgrLL.insertTransportFreightCost(row);
//					}else 
//					{
//						floorFreightMgrLL.updateTransportFreightCost(row);
//					}		
//				}
//			}
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "saveFreightCosts(long transport_id, String xml)", log);
		}
	}
	
	/**
	 * 保存运输公司
	 * @param transport_id
	 * @param xml
	 * @throws Exception
	 */
	public void saveFreightCompany(long transport_id, String xml) throws Exception
	{
		try
		{
			String fr_id					= StringUtil.getSampleNode(xml, "fr_id");
			String transport_waybill_name	= StringUtil.getSampleNode(xml, "transport_waybill_name");
			String transport_waybill_number	= StringUtil.getSampleNode(xml, "transport_waybill_number");
			String transportby				= StringUtil.getSampleNode(xml, "transportby");
			String carriers					= StringUtil.getSampleNode(xml, "carriers"); 
			String transport_send_place		= StringUtil.getSampleNode(xml, "transport_send_place");
			String transport_receive_place	= StringUtil.getSampleNode(xml, "transport_receive_place");
			
			//运输设置
			DBRow transport = new DBRow();
			transport.add("fr_id",fr_id);
			transport.add("transport_waybill_name",transport_waybill_name);
			transport.add("transport_waybill_number",transport_waybill_number);
			transport.add("transportby",transportby);
			transport.add("carriers",carriers);
			transport.add("transport_send_place",transport_send_place);
			transport.add("transport_receive_place",transport_receive_place);
			
			floorTransportMgrZyj.updateTransport(transport_id, transport);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addPurchaseTransportByXml(xml)", log);
		}
	}
	
	/**
	 * 保存交货单各流程，保存更新可皆使此方法
	 * @param transport_id
	 * @param adid
	 * @param employee_name
	 * @param xml
	 * @throws Exception
	 */
	public void saveAllProdures(long transport_id, long adid, String employee_name, String xml, HttpServletRequest request) throws Exception
	{
		try
		{
			//将流程ID，阶段值，adminUserIds，isMail, isMessage, isPage拼的字符串放在List中
			xml = xml.substring(xml.indexOf("<produre>")+9, xml.lastIndexOf("</produre>"));
			String[] prodrueSingle = xml.split("</produre><produre>");
			
			DBRow transport		= floorTransportMgrZJ.getDetailTransportById(transport_id);
			DBRow transportRow = new DBRow();
			for (int i = 0; i < prodrueSingle.length; i++)
			{
				//数据格式：流程ID｜阶段值｜adminUserIds｜isMail｜ isMessage｜ isPage
				String[] arr3			= prodrueSingle[i].split("\\|");
				long produre_id			= Long.parseLong(arr3[0]);
				int activity			= Integer.parseInt(arr3[1]);
				String executePersonIds	= arr3[2];
				int mail				= Integer.parseInt(arr3[3]);
				int message				= Integer.parseInt(arr3[4]);
				int page				= Integer.parseInt(arr3[5]);
				
				//根据流程ID获取流程详细
				DBRow processRow			= produresMgrZyj.getProdureByProdureId(produre_id);
				String process_coloum		= processRow.getString("process_coloum");//相关单据的表的属性名
				
				//已保存的阶段d值，如果空值则无需，1表示不需要
				int tr_activity				= 0;
				if(null != transport)
				{
					tr_activity = transport.get(process_coloum, 0);
				}
				if(0 == activity)
				{
					if(0 != tr_activity)
					{
						activity = tr_activity;
					}
					else
					{
						activity = processRow.get("activity_default_selected", 0);
					}
				}
				
//				//system.out.println("produre_id:"+produre_id+",activity:"+activity+",executePersonIds:"+executePersonIds
//						+",mail:"+mail+",message:"+message+",page:"+page+",tr_activity:"+tr_activity);
				
				String beginTime	= "";
				String endTime		= "";
				boolean isNow		= false;
				if(YesOrNotKey.YES == processRow.get("process_start_time_is_create", 0))
				{
					beginTime = DateUtil.NowStr();
					isNow	  = true;
				}
				if(0 != processRow.get("process_period", 0))
				{
					TDate tDate = new TDate();
					tDate.addDay(processRow.get("process_period", 0));
					endTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
				}
				//单据类型字符串
				String moduleStr = new ModuleKey().getModuleName(processRow.get("associate_order_type", 0));
				//流程名称，如果是运输负责人，需要将其改成主流程
				String handleProcessName = (TransportLogTypeKey.Goods==processRow.get("process_key", 0)?"主流程":processRow.getString("process_name"));
				String title = moduleStr+":"+ transport_id+",["+handleProcessName+"]";
				//将对应的流程值放在转运单上
				transportRow.add(process_coloum, activity);
				//根据流程ID，阶段值查询此流程此阶段的信息
				DBRow produresDetailRow = produresMgrZyj.getProduresDetailsByProduresIdActivity(produre_id, activity);
				String content			= this.handleSendContent(transport_id);
				//原不需要，现在需要，创建
				if((0 == tr_activity || 1 == tr_activity) && 2 == activity)
				{
					this.addTransportScheduleOneProdure(beginTime,endTime, adid,
							executePersonIds, "", transport_id, processRow.get("associate_order_type", 0),
							title, content, page, mail, message, request, isNow,
							processRow.get("process_key", 0), activity, employee_name, "修改"+moduleStr+":"+ transport_id+",["+produresDetailRow.getString("activity_name")+"]");
				}
				//原需要，现在需要，更新；如果是主流程，一直是更新
				else if((2 == tr_activity && 2 == activity) || (TransportLogTypeKey.Goods == processRow.get("process_key", 0)))
				{
					this.updateTransportScheduleOneProdure(beginTime, endTime, adid,
							executePersonIds, "", transport_id, processRow.get("associate_order_type", 0),
							title, content, page, mail, message, request, isNow,
							processRow.get("process_key", 0), activity, employee_name, "修改"+moduleStr+":"+ transport_id+",["+produresDetailRow.getString("activity_name")+"]");
				}
				//原需要，现不需要，删除
				else if(2 == tr_activity && 1 == activity)
				{
					this.deleteTransportScheduleById(transport_id, processRow.get("associate_order_type", 0), processRow.get("process_key", 0)
							, activity, employee_name, "修改"+moduleStr+":"+ transport_id+",["+produresDetailRow.getString("activity_name")+"]", adid, request);
				}
				//其余更新，更新前先验证是否已有任务，如果没有创建，否则更新
				else if(1 != tr_activity && 2 != tr_activity && 0 != tr_activity)
				{
					this.updateTransportScheduleOneProdure(beginTime, endTime, adid,
							executePersonIds, "", transport_id, processRow.get("associate_order_type", 0),
							title, content, page, mail, message, request, isNow,
							processRow.get("process_key", 0), activity, employee_name, "修改"+moduleStr+":"+ transport_id+",["+produresDetailRow.getString("activity_name")+"]");
				}
			}
			
			floorTransportMgrZyj.updateTransport(transport_id, transportRow);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "saveAllProdures(long transport_id, long adid, String employee_name, String xml,request)", log);
		}
	}
	
	
	/**
	 * 保存货物列表
	 * @param transport_id
	 * @param purchase_id
	 * @param adid
	 * @param employee_name
	 * @param xml
	 * @throws Exception
	 */
	public void saveTransportItems(long transport_id, long purchase_id, long adid, String employee_name, String xml) throws Exception
	{
		try
		{
			//将商品条码，数量，箱号拼的字符串放在List中
			List<String> pNameCountBoxs = new ArrayList<String>();
			String[] arr1 = xml.split("<product>");
			for (int i = 0; i < arr1.length; i++)
			{
				if(1 == i%2)
				{
					String[] arr2 = arr1[i].split("</product>");
					for (int j = 0; j < arr2.length; j++) 
					{
						if(0 == j%2)
						{
							pNameCountBoxs.add(arr2[j]); 
						}
					}
				}
			}
			//先删除
			floorTransportMgrZJ.delTransportDetailByTransportId(transport_id);
			//保存转运单明细
			for (int i = 0; i < pNameCountBoxs.size(); i++)
			{
				//数据格式：BULB/90047-3/5K,1,1,2-1，商品条码，交货数，条件数，箱号
				String[] arr3		= pNameCountBoxs.get(i).split(",");
				String p_name		= arr3[0];
				int deliver_count	= (int)Float.parseFloat(arr3[1]);
				int backup_count	= (int)Float.parseFloat(arr3[2]);
				String box			= arr3[3];
				int count = deliver_count+backup_count;
				
				//得到商品ID及交货价
				DBRow product = floorProductMgr.getDetailProductByPname(p_name);
				long pc_id = product.get("pc_id", 0L);
				DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id, pc_id);
				double send_price = product.get("unit_price",0d);
				if(purchaseDetail!=null)
				{
					send_price = purchaseDetail.get("price",0d);
				}
				
				float union_count = 0;
				float normal_count = 0;
				if(product.get("union_flag",0)==1)
				{
					union_count = count;
				}
				else
				{
					normal_count = count;
				}
				
				DBRow transportDetail = new DBRow();
				transportDetail.add("transport_pc_id",pc_id);
				transportDetail.add("transport_delivery_count",deliver_count);
				transportDetail.add("transport_backup_count",backup_count);
				transportDetail.add("transport_count",count);
				transportDetail.add("transport_id",transport_id);
				transportDetail.add("transport_p_name",product.getString("p_name"));
				transportDetail.add("transport_p_code",product.getString("p_code"));
				transportDetail.add("transport_volume",product.getString("volume"));
				transportDetail.add("transport_weight",product.getString("weight"));
				transportDetail.add("transport_box",box);
				
				transportDetail.add("transport_send_price",send_price);
				transportDetail.add("transport_union_count",union_count);
				transportDetail.add("transport_normal_count",normal_count);
				
				floorTransportMgrZJ.addTransportDetail(transportDetail);
			}
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "saveTransportItems(long transport_id, long adid, String employee_name, String xml)", log);
		}
	}
	
	
	/**
	 * 保存提货地址
	 * @return
	 * @throws Exception
	 */
	public long savePurchaseTransportSendAddress(long transport_id, long purchase_id, long adid, String employee_name, String xml) throws Exception
	{
		try
		{
			//提货地址
			String send_psid			= StringUtil.getSampleNode(xml,"send_psid");
			String send_ccid			= StringUtil.getSampleNode(xml,"send_ccid");
			String send_pro_id			= StringUtil.getSampleNode(xml,"send_pro_id");
			String send_city			= StringUtil.getSampleNode(xml,"send_city");
			String send_house_number	= StringUtil.getSampleNode(xml,"send_house_number");
			String send_street			= StringUtil.getSampleNode(xml,"send_street");
			String send_zip_code		= StringUtil.getSampleNode(xml,"send_zip_code");
			String send_name			= StringUtil.getSampleNode(xml,"send_name");
			String send_linkman_phone	= StringUtil.getSampleNode(xml,"send_linkman_phone");
			String address_state_send	= StringUtil.getSampleNode(xml, "address_state_send");
			String transport_status		= StringUtil.getSampleNode(xml, "transport_status");
			DBRow sendProductStorage	= floorCatalogMgr.getDetailProductStorageCatalogById(Long.parseLong(send_psid));
			int from_ps_type			= (null != sendProductStorage?sendProductStorage.get("storage_type", 0):0);
			
			DBRow transport = new DBRow();
			//采购单号
			if(0 != purchase_id)
			{
				transport.add("purchase_id", purchase_id);
			}
			//提货信息
			transport.add("send_psid",send_psid);
			transport.add("send_ccid",send_ccid);
			transport.add("send_pro_id",send_pro_id);
			transport.add("send_city",send_city);
			transport.add("send_house_number",send_house_number);
			transport.add("send_street",send_street);
			transport.add("send_zip_code",send_zip_code);
			transport.add("send_name",new String(send_name.getBytes("utf-8")));
			transport.add("send_linkman_phone",send_linkman_phone);
			transport.add("address_state_send", address_state_send);
			transport.add("from_ps_type",from_ps_type);
			//主流程信息
			transport.add("transport_status",!"".equals(transport_status)?Integer.parseInt(transport_status):TransportOrderKey.READY);//创建的转运单都是准备中
			transport.add("updatedate",DateUtil.NowStr());
			transport.add("updateby",adid);//转运单的更新者
			transport.add("updatename",employee_name);//转运单的更新者
			if(0 == transport_id)
			{
				if(0 != purchase_id)
				{
					//设置交货型转运的批次
					DBRow[] purchaseTransportOrder = floorTransportMgrZJ.getTranposrtOrdersByPurchaseId(purchase_id);
					int number = 1;
					try 
					{
						if(purchaseTransportOrder.length>0)
						{
							String numbers = purchaseTransportOrder[purchaseTransportOrder.length-1].getString("transport_number");
							number = Integer.parseInt(numbers)+1;
						}
					}
					catch (NumberFormatException e) 
					{
						number = 1;
					}
					transport.add("transport_number",number);
				}
				transport.add("transport_date",DateUtil.NowStr());
				transport.add("create_account_id",adid);//转运单的创建者
				transport.add("create_account",employee_name);//转运单的创建者
				transport_id = floorTransportMgrZJ.addTransport(transport);
			}
			else
			{
				floorTransportMgrZyj.updateTransport(transport_id, transport);
			}
			
			return transport_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addPurchaseTransportSendAddress(xml)", log);
		}
	}
	
	/**
	 * 保存收货地址
	 * @return
	 * @throws Exception
	 */
	public long savePurchaseTransportDeliverAddress(long transport_id, long purchase_id, long adid, String employee_name, String xml) throws Exception
	{
		try
		{
			
			//收货信息
			String receive_psid			= StringUtil.getSampleNode(xml,"receive_psid");
			String deliver_ccid			= StringUtil.getSampleNode(xml,"deliver_ccid");
			String deliver_pro_id		= StringUtil.getSampleNode(xml,"deliver_pro_id");			
			String deliver_city			= StringUtil.getSampleNode(xml,"deliver_city");
			String deliver_house_number	= StringUtil.getSampleNode(xml,"deliver_house_number");
			String deliver_street		= StringUtil.getSampleNode(xml,"deliver_street");
			String deliver_zip_code		= StringUtil.getSampleNode(xml,"deliver_zip_code");
			String deliver_name			= StringUtil.getSampleNode(xml,"deliver_name");
			String deliver_linkman_phone	= StringUtil.getSampleNode(xml,"deliver_linkman_phone");
			String address_state_deliver	= StringUtil.getSampleNode(xml, "address_state_deliver");
			String transport_receive_date	= StringUtil.getSampleNode(xml, "transport_receive_date");
			String transport_status		= StringUtil.getSampleNode(xml, "transport_status");
			DBRow receiveProductStorage	= floorCatalogMgr.getDetailProductStorageCatalogById(Long.parseLong(receive_psid));
			int target_ps_type			= (null != receiveProductStorage?receiveProductStorage.get("storage_type", 0):0);
			
			DBRow transport = new DBRow();
			//采购单号
			//采购单号
			if(0 != purchase_id)
			{
				transport.add("purchase_id", purchase_id);
			}
			//收货信息
			transport.add("receive_psid",receive_psid);
			transport.add("deliver_ccid",deliver_ccid);
			transport.add("deliver_pro_id",deliver_pro_id);
			transport.add("deliver_city",deliver_city);
			transport.add("deliver_house_number",deliver_house_number);
			transport.add("deliver_street",deliver_street);
			transport.add("deliver_zip_code",deliver_zip_code);
			transport.add("transport_address","");
			transport.add("transport_linkman",deliver_name);
			transport.add("transport_linkman_phone",deliver_linkman_phone);
			transport.add("address_state_deliver", address_state_deliver);
			transport.add("target_ps_type" ,target_ps_type);
			//主流程信息
			transport.add("transport_status",!"".equals(transport_status)?Integer.parseInt(transport_status):TransportOrderKey.READY);//创建的转运单都是准备中
			transport.add("updatedate",DateUtil.NowStr());
			transport.add("updateby",adid);//转运单的更新者
			transport.add("updatename",employee_name);//转运单的更新者
			//其他信息(ETA,备注)
			if(!"".equals(transport_receive_date))
			{
				transport.add("transport_receive_date", transport_receive_date);
			}
			if(0 == transport_id)
			{
				if(0 != purchase_id)
				{
					//设置交货型转运的批次
					DBRow[] purchaseTransportOrder = floorTransportMgrZJ.getTranposrtOrdersByPurchaseId(purchase_id);
					int number = 1;
					try 
					{
						if(purchaseTransportOrder.length>0)
						{
							String numbers = purchaseTransportOrder[purchaseTransportOrder.length-1].getString("transport_number");
							number = Integer.parseInt(numbers)+1;
						}
					}
					catch (NumberFormatException e) 
					{
						number = 1;
					}
					transport.add("transport_number",number);
				}
				transport.add("transport_date",DateUtil.NowStr());
				transport.add("create_account_id",adid);//转运单的创建者
				transport.add("create_account",employee_name);//转运单的创建者
				transport_id = floorTransportMgrZJ.addTransport(transport);
			}
			else
			{
				floorTransportMgrZyj.updateTransport(transport_id, transport);
			}
			
			return transport_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addPurchaseTransportSendAddress(xml)", log);
		}
	}
	
	
	/**
	 * 保存转运单基本信息的接口
	 * @return
	 * @throws Exception
	 */
	public long addTransportBasicInfo(String purchase_id, long receive_psid, long deliver_ccid, long deliver_pro_id
			, String deliver_city, String deliver_house_number, String deliver_street, String deliver_zip_code
			, String deliver_name, String deliver_linkman_phone, String address_state_deliver
			, long send_psid, long send_ccid, long send_pro_id, String send_city, String send_house_number
			, String send_street, String send_zip_code, String send_name, String send_linkman_phone
			, String transport_receive_date, String address_state_send, int transport_status, String transport_date
			, long adid, String loginAccount, String employee_name, String updatedate) throws Exception
	{
		try
		{
			DBRow transport = new DBRow();
			if(null != purchase_id && !"".equals(purchase_id))
			{
				//采购单号
				transport.add("purchase_id",Long.parseLong(purchase_id));
			}
			DBRow receiveProductStorage	= floorCatalogMgr.getDetailProductStorageCatalogById(receive_psid);
			int target_ps_type			= (null != receiveProductStorage?receiveProductStorage.get("storage_type", 0):0);
			DBRow sendProductStorage	= floorCatalogMgr.getDetailProductStorageCatalogById(send_psid);
			int from_ps_type			= (null != sendProductStorage?sendProductStorage.get("storage_type", 0):0);
			
			//收货信息
			transport.add("receive_psid",receive_psid);
			transport.add("deliver_ccid",deliver_ccid);
			transport.add("deliver_pro_id",deliver_pro_id);
			transport.add("deliver_city",deliver_city);
			transport.add("deliver_house_number",deliver_house_number);
			transport.add("deliver_street",deliver_street);
			transport.add("deliver_zip_code",deliver_zip_code);
			transport.add("transport_address","");
			transport.add("transport_linkman",deliver_name);
			transport.add("transport_linkman_phone",deliver_linkman_phone);
			transport.add("address_state_deliver", address_state_deliver);
			
			//提货信息
			transport.add("send_psid",send_psid);
			transport.add("send_city",send_city);
			transport.add("send_house_number",send_house_number);
			transport.add("send_street",send_street);
			transport.add("send_ccid",send_ccid);
			transport.add("send_pro_id",send_pro_id);
			transport.add("send_zip_code",send_zip_code);
			transport.add("send_name",send_name);
			transport.add("send_linkman_phone",send_linkman_phone);
			transport.add("address_state_send", address_state_send);
			
			//其他信息(ETA,备注)
			if(!"".equals(transport_receive_date))
			{
				transport.add("transport_receive_date", transport_receive_date);
			}
			if(0 == adid && null != loginAccount && !"".equals(loginAccount))
			{
				DBRow loginDetail = floorAdminMgr.getDetailAdminByAccount(loginAccount);
				if(null == loginDetail)
				{
					throw new MachineUnderFindException();
				}
				adid = loginDetail.get("adid", 0L);
				employee_name = loginDetail.getString("employe_name");
			}
			else
			{
				DBRow loginDetail = floorAdminMgr.getDetailAdmin(adid);
				if(null == loginDetail)
				{
					throw new MachineUnderFindException();
				}
				loginAccount = loginDetail.getString("account");
				employee_name = loginDetail.getString("employe_name");
			}
			if(null != purchase_id && !"".equals(purchase_id) && !"0".equals(purchase_id))
			{
				//设置交货型转运的批次
				DBRow[] purchaseTransportOrder = floorTransportMgrZJ.getTranposrtOrdersByPurchaseId(Long.parseLong(purchase_id));
				int number = 1;
				try 
				{
					if(purchaseTransportOrder.length>0)
					{
						String numbers = purchaseTransportOrder[purchaseTransportOrder.length-1].getString("transport_number");
						number = Integer.parseInt(numbers)+1;
					}
				}
				catch (NumberFormatException e) 
				{
					number = 1;
				}
				transport.add("transport_number",number);
			}
			
			//主流程信息
			transport.add("transport_status",transport_status);//创建的转运单都是准备中
			transport.add("transport_date",transport_date);
			transport.add("create_account_id",adid);//转运单的创建者
			transport.add("create_account",employee_name);//转运单的创建者
			transport.add("updatedate",updatedate);
			transport.add("updateby",adid);//转运单的创建者
			transport.add("updatename",employee_name);//转运单的创建者
			
			//提货与收货仓库类型
			transport.add("target_ps_type" ,target_ps_type);
			transport.add("from_ps_type",from_ps_type);
			
			
			return floorTransportMgrZJ.addTransport(transport);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addPurchaseTransportByXml(xml)", log);
		}
	}
	
	
	/**
	 * 保存交货单基本信息
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public long addPurchaseTransportBasicInfoByXml(String xml) throws Exception
	{
		try
		{
			String purchase_id			= StringUtil.getSampleNode(xml, "purchase_id");
			//收货信息
			String receive_psid			= StringUtil.getSampleNode(xml,"receive_psid");
			String deliver_ccid			= StringUtil.getSampleNode(xml,"deliver_ccid");
			String deliver_pro_id		= StringUtil.getSampleNode(xml,"deliver_pro_id");			
			String deliver_city			= StringUtil.getSampleNode(xml,"deliver_city");
			String deliver_house_number	= StringUtil.getSampleNode(xml,"deliver_house_number");
			String deliver_street		= StringUtil.getSampleNode(xml,"deliver_street");
			String deliver_zip_code		= StringUtil.getSampleNode(xml,"deliver_zip_code");
			String deliver_name			= StringUtil.getSampleNode(xml,"deliver_name");
			String deliver_linkman_phone	= StringUtil.getSampleNode(xml,"deliver_linkman_phone");
			String address_state_deliver	= StringUtil.getSampleNode(xml, "address_state_deliver");
			
			//提货地址
			String send_psid			= StringUtil.getSampleNode(xml,"send_psid");
			String send_ccid			= StringUtil.getSampleNode(xml,"send_ccid");
			String send_pro_id			= StringUtil.getSampleNode(xml,"send_pro_id");
			String send_city			= StringUtil.getSampleNode(xml,"send_city");
			String send_house_number	= StringUtil.getSampleNode(xml,"send_house_number");
			String send_street			= StringUtil.getSampleNode(xml,"send_street");
			String send_zip_code		= StringUtil.getSampleNode(xml,"send_zip_code");
			String send_name			= StringUtil.getSampleNode(xml,"send_name");
			String send_linkman_phone	= StringUtil.getSampleNode(xml,"send_linkman_phone");
			String transport_receive_date	= StringUtil.getSampleNode(xml, "transport_receive_date");
			String address_state_send		= StringUtil.getSampleNode(xml, "address_state_send");
			
			String loginAccount			= StringUtil.getSampleNode(xml,"LoginAccount");
			
			//保存转运单的基本信息
			long transport_id = this.addTransportBasicInfo(purchase_id, Long.parseLong(receive_psid), Long.parseLong(deliver_ccid), Long.parseLong(deliver_pro_id),
					deliver_city, deliver_house_number, deliver_street, deliver_zip_code,
					deliver_name, deliver_linkman_phone, address_state_deliver,
					Long.parseLong(send_psid), Long.parseLong(send_ccid), Long.parseLong(send_pro_id), send_city, send_house_number,
					send_street, send_zip_code, send_name, send_linkman_phone,
					transport_receive_date, address_state_send, TransportOrderKey.READY, DateUtil.NowStr(),
					0, loginAccount, "", DateUtil.NowStr());
			
			return transport_id;
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addPurchaseTransportBasicInfoByXml(xml)", log);
		}
	}
	
	
	
	
	/**
	 * 创建转运单，处理各流程信息
	 * @param request
	 * @throws Exception
	 */
	public void addTransportXML(HttpServletRequest request) throws Exception
	{
		try
		{
			long transport_id	= StringUtil.getLong(request, "transport_id");
			long adid			= new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			String employeeName	= new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name();
			
			//组织发送邮件或短信的内容
			String content			= this.handleSendContent(transport_id);
			this.addTransportProdures(request, transport_id, adid, employeeName, content);
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addTransportTest(request)", log);
		}
	}
	
	
	public void addTransportProdures(HttpServletRequest request, long associate_id, long adid, String employeeName, String content) throws Exception
	{
		try
		{
			DBRow transportRow = new DBRow();
			String[] process_ids	= request.getParameterValues("process_ids");
			for (int i = 0; i < process_ids.length; i++) 
			{
				DBRow processRow			= produresMgrZyj.getProdureByProdureId(Long.parseLong(process_ids[i]));
				String process_coloum_page	= processRow.getString("process_coloum_page");//页面上的属性名
				String process_coloum		= processRow.getString("process_coloum");//相关单据的表的属性名
				int activity				= StringUtil.getInt(request, "radio_"+process_coloum_page);//阶段值
				if(0 == activity)
				{
					activity = processRow.get("activity_default_selected", 0);
				}
				if(2 == activity || (TransportLogTypeKey.Goods == processRow.get("process_key", 0)))
				{
					String executePersonIds		= StringUtil.getString(request, "adminUserIds_"+process_coloum_page);//执行人IDs
//					String exePersonNames		= StrUtil.getString(request, "adminUserNames_"+process_coloum_page);//执行人Names
					DBRow[] processNotices		= produresMgrZyj.getProduresNoticesByProduresId(Integer.parseInt(process_ids[i]), 1);
					int mail					= 0;//是否需要邮件通知
					int message					= 0;//是否需要短信通知
					int page					= 0;//是否需要页面通知
					for (int j = 0; j < processNotices.length; j++) 
					{
						String nodeName = processNotices[j].getString("notice_coloum_name")+"_"+process_coloum_page;
						if(NoticeTypeKey.MAIL == processNotices[j].get("notice_type", 0))
						{
							mail	= StringUtil.getInt(request, nodeName);
						}
						else if(NoticeTypeKey.MESSAGE == processNotices[j].get("notice_type", 0))
						{
							message = StringUtil.getInt(request, nodeName);
						}
						else if(NoticeTypeKey.PAGE == processNotices[j].get("notice_type", 0))
						{
							page = StringUtil.getInt(request, nodeName);	
						}
					}
					String beginTime	= "";
					String endTime		= "";
					boolean isNow		= false;
					if(YesOrNotKey.YES == processRow.get("process_start_time_is_create", 0))
					{
						beginTime = DateUtil.NowStr();
						isNow	  = true;
					}
					if(0 != processRow.get("process_period", 0))
					{
						TDate tDate = new TDate();
						tDate.addDay(processRow.get("process_period", 0));
						endTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
					}
					String moduleStr = new ModuleKey().getModuleName(processRow.get("associate_order_type", 0));
					String handleProcessName = (TransportLogTypeKey.Goods==processRow.get("process_key", 0)?"主流程":processRow.getString("process_name"));
					String title = moduleStr+":"+ associate_id+",["+handleProcessName+"]";
					
					transportRow.add(process_coloum, activity);
					DBRow produresDetailRow = produresMgrZyj.getProduresDetailsByProduresIdActivity(Long.parseLong(process_ids[i]), activity);
					this.addTransportScheduleOneProdure(beginTime,endTime, adid,
							executePersonIds, "", associate_id, processRow.get("associate_order_type", 0),
							title, content, page, mail, message, request, isNow,
							processRow.get("process_key", 0), activity, employeeName, "创建"+moduleStr+":"+ associate_id+",["+produresDetailRow.getString("activity_name")+"]");
				}
			}
			
			floorTransportMgrZyj.updateTransport(associate_id, transportRow);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addTransportProdures(HttpServletRequest request, long associate_id, long adid, String employeeName, String content)", log);
		}
	}
	
	/**
	 * 更新转运单
	 * @param request
	 * @throws Exception
	 */
	public void updateTransportXML(HttpServletRequest request) throws Exception
	{
		try
		{
			long transport_id	= StringUtil.getLong(request, "transport_id");
			long adid			= new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getAdid();
			String employeeName	= new AdminMgr().getAdminLoginBean(StringUtil.getSession(request)).getEmploye_name();
			
			//组织发送邮件或短信的内容
			String content			= this.handleSendContent(transport_id);
			this.updateTransportProdures(request, transport_id, adid, employeeName, content);
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addTransportTest(request)", log);
		}
	}
	
	public void updateTransportProdures(HttpServletRequest request, long associate_id, long adid, String employeeName, String content) throws Exception
	{
		try
		{
			DBRow transport		= floorTransportMgrZJ.getDetailTransportById(associate_id);
			DBRow transportRow	= new DBRow();
			String[] process_ids	= request.getParameterValues("process_ids");
			for (int i = 0; i < process_ids.length; i++) 
			{
				DBRow processRow			= produresMgrZyj.getProdureByProdureId(Long.parseLong(process_ids[i]));
				String process_coloum_page	= processRow.getString("process_coloum_page");//页面上的属性名
				String process_coloum		= processRow.getString("process_coloum");//相关单据的表的属性名
				int activity				= StringUtil.getInt(request, "radio_"+process_coloum_page);//阶段值
				int tr_activity				= transport.get(process_coloum, 1);//已保存的阶段值，如果空值则无需
				if(0 == activity)
				{
					activity = tr_activity;
				}
				
				String executePersonIds		= StringUtil.getString(request, "adminUserIds_"+process_coloum_page);//执行人IDs
				DBRow[] processNotices		= produresMgrZyj.getProduresNoticesByProduresId(Integer.parseInt(process_ids[i]), 1);
				int mail					= 0;//是否需要邮件通知
				int message					= 0;//是否需要短信通知
				int page					= 0;//是否需要页面通知
				for (int j = 0; j < processNotices.length; j++) 
				{
					String nodeName = processNotices[j].getString("notice_coloum_name")+"_"+process_coloum_page;
					if(NoticeTypeKey.MAIL == processNotices[j].get("notice_type", 0))
					{
						mail	= StringUtil.getInt(request, nodeName);
					}
					else if(NoticeTypeKey.MESSAGE == processNotices[j].get("notice_type", 0))
					{
						message = StringUtil.getInt(request, nodeName);
					}
					else if(NoticeTypeKey.PAGE == processNotices[j].get("notice_type", 0))
					{
						page = StringUtil.getInt(request, nodeName);	
					}
				}
				String beginTime	= "";
				String endTime		= "";
				boolean isNow		= false;
				if(YesOrNotKey.YES == processRow.get("process_start_time_is_create", 0))
				{
					beginTime = DateUtil.NowStr();
					isNow	  = true;
				}
				if(0 != processRow.get("process_period", 0))
				{
					TDate tDate = new TDate();
					tDate.addDay(processRow.get("process_period", 0));
					endTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
				}
				String moduleStr = new ModuleKey().getModuleName(processRow.get("associate_order_type", 0));
				String handleProcessName = (TransportLogTypeKey.Goods==processRow.get("process_key", 0)?"主流程":processRow.getString("process_name"));
				String title = moduleStr+":"+ associate_id+",["+handleProcessName+"]";
				
				transportRow.add(process_coloum, activity);
				DBRow produresDetailRow = produresMgrZyj.getProduresDetailsByProduresIdActivity(Long.parseLong(process_ids[i]), activity);
				
				//原不需要，现在需要，创建
				if(1 == tr_activity && 2 == activity)
				{
					this.addTransportScheduleOneProdure(beginTime,endTime, adid,
							executePersonIds, "", associate_id, processRow.get("associate_order_type", 0),
							title, content, page, mail, message, request, isNow,
							processRow.get("process_key", 0), activity, employeeName, "创建"+moduleStr+":"+ associate_id+",["+produresDetailRow.getString("activity_name")+"]");
				}
				//原需要，现在需要，更新；如果是主流程，一直是更新
				else if((2 == tr_activity && 2 == activity) || (TransportLogTypeKey.Goods == processRow.get("process_key", 0)))
				{
					this.updateTransportScheduleOneProdure(beginTime, endTime, adid,
							executePersonIds, "", associate_id, processRow.get("associate_order_type", 0),
							title, content, page, mail, message, request, isNow,
							processRow.get("process_key", 0), activity, employeeName, "修改"+moduleStr+":"+ associate_id+",["+produresDetailRow.getString("activity_name")+"]");
				}
				//原需要，现不需要，删除
				else if(2 == tr_activity && 1 == activity)
				{
					this.deleteTransportScheduleById(associate_id, processRow.get("associate_order_type", 0), processRow.get("process_key", 0)
							, activity, employeeName, "修改"+moduleStr+":"+ associate_id+",["+produresDetailRow.getString("activity_name")+"]", adid, request);
				}
				//其余更新，更新前先验证是否已有任务，如果没有创建，否则更新
				else if(1 != tr_activity && 2 != tr_activity)
				{
					this.updateTransportScheduleOneProdure(beginTime, endTime, adid,
							executePersonIds, "", associate_id, processRow.get("associate_order_type", 0),
							title, content, page, mail, message, request, isNow,
							processRow.get("process_key", 0), activity, employeeName, "修改"+moduleStr+":"+ associate_id+",["+produresDetailRow.getString("activity_name")+"]");
				}
			}
			
			floorTransportMgrZyj.updateTransport(associate_id, transportRow);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "addTransportProdures(HttpServletRequest request, long associate_id, long adid, String employeeName, String content)", log);
		}
	}
	
	/**
	 * 组织发送短信内容
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public String handleSendContent(long transport_id) throws Exception{
		
		try
		{
			String rnStr				= "";
			DBRow transportRow			= floorTransportMgrLL.getTransportById(transport_id+"");
			long purchase_id			= transportRow.get("purchase_id",0L);
			//获取提货仓库的名字
			long pro_cata_id_send		= transportRow.get("send_psid", 0L);
		
			if(0 == purchase_id)
			{
				String cata_name_send		= "";
				DBRow storageCatalogSend	= floorStorageCatalogMgrZyj.getStorageCatalogById(pro_cata_id_send);
				if(null != storageCatalogSend)
				{
					cata_name_send = storageCatalogSend.getString("title");
					rnStr += "提货仓库:"+cata_name_send+",";
				}
			}
			//如果是交货单
			else
			{
				DBRow supplierRow	= supplierMgr.getDetailSupplier(pro_cata_id_send);
				String supplierName	= supplierRow.getString("sup_name");
				rnStr += "供应商:"+supplierName+",";
			}
			//获取收货仓库的名字
			long pro_cata_id_deliver	= transportRow.get("receive_psid",0L);
			DBRow storageCatalogDeliver	= floorStorageCatalogMgrZyj.getStorageCatalogById(pro_cata_id_deliver);
			String cata_name_deliver 	= "";
			if(null != storageCatalogDeliver){
				cata_name_deliver = storageCatalogDeliver.getString("title");
				rnStr += "收货仓库:"+cata_name_deliver+",";
			}
			//获取eta
			String eta = transportRow.getString("transport_receive_date");
			if(!"".equals(eta)){
				TDate tdateEta = new TDate(eta);
				String tdateEtaStr = tdateEta.formatDate("yyyy-MM-dd");
				rnStr += "预计到货时间:"+tdateEtaStr+",";
			}
			
			//转运货物的总体积，如果 有值，添加，此处待完善
			rnStr += "总体积:" +floorTransportMgrZJ.getTransportVolume(transport_id);
			return rnStr;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"handleSendContent",log);
		}
	}
	
	/**
	 * 手机端保存转运单明细
	 * @author Administrator
	 * @param xml
	 * @param adminLoggerBean
	 * @return
	 * @throws Exception
	 */
	public List<DBRow> saveOrUpdateTransportItems(String xml,
			AdminLoginBean adminLoggerBean, HttpServletRequest request) throws Exception
	{
		try
		{
			long transport_id = Long.parseLong(StringUtil.getSampleNode(xml, "Transport"));
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
			String details = StringUtil.getSampleNode(xml, "Details");
			
			if("".equals(details))
			{
				if(null != transport && TransportOrderKey.READY == transport.get("transport_status", 0))
				{
					//改变交货单状态
					DBRow transportRow = new DBRow();
					transportRow.add("transport_status", TransportOrderKey.INTRANSIT);
					floorTransportMgrZyj.updateTransport(transport_id, transportRow);
					//加日志
					floorTransportMgrLL.insertLogs(String.valueOf(transport_id), "交货单:"+transport_id+"起运", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 4, TransportOrderKey.INTRANSIT);
					scheduleMgrZr.addScheduleReplayExternal(transport_id , ModuleKey.TRANSPORT_ORDER ,
							4 , "交货单:"+transport_id+"起运" , false, adminLoggerBean, "transport_intransit_period", "true");
					
					//如果预定了门或者位置，将其释放
					doorOrLocationOccupancyMgrZyj.updateDoorOrLocationStatusAndTime(transport_id, TransportOrderKey.INTRANSIT);
				}
				return new ArrayList<DBRow>();
			}
			String efficacy = StringUtil.getSampleNode(xml, "Efficacy");
			int length = Integer.parseInt(StringUtil.getSampleNode(xml,
					"Length"));
			String machine_id = StringUtil.getSampleNode(xml, "Machine");

			

//			if (transport == null)
//			{
//				throw new NoExistTransportOrderException();
//			}
//			else
//			{
//				if (transport.get("transport_status", 0) != TransportOrderKey.READY) { throw new TransportOrderCanNotOutboundException(); }
//			}

			if (!efficacy.equals(HashBase64(details))) 
			{
				throw new XMLDataErrorException();
			}
			if (length != details.length()) 
			{ 
				throw new XMLDataLengthException();
			}

			ArrayList<DBRow> list = new ArrayList<DBRow>();
			// 存入发货明细
			String[] transportItems = details.split("\\|");
			//已存在的序列号
			List<DBRow> hadSnItems = new ArrayList<DBRow>();
			
			for (int i = 0; i < transportItems.length; i++)
			{
				String[] transportItem = transportItems[i].split(",");
				if (transportItems[i].trim().equals("")|| transportItem.length != 8)
				{ 
					throw new XMLDataErrorException();
				}

				//sn是否已经提交了,已经提交的数据返回给客户端
				String sn = transportItem[2];
				DBRow[] hadSubSns = floorTransportMgrZyj.getTransportItemsByTransportIdAndSn(transport_id, sn);
				if(hadSubSns.length > 0)
				{
					DBRow hadSnItem = new DBRow();
					hadSnItem.add("sn", sn);
					hadSnItems.add(hadSnItem);
					break;
				}
				String lp = transportItem[3];
				long lp_id = 0;
				if(!StringUtil.isBlankAndCanParseLong(lp))
				{
					lp_id = Long.parseLong(lp);
				}
				
				////system.out.println("transportItem:"+transportItem[0]+","+transportItem[1]+","+transportItem[2]+","+transportItem[3]+","+lp_id);
				
				DBRow product = floorProductMgr.getDetailProductByPcid(Long.parseLong(transportItem[0].trim()));
//				product = floorProductMgr.getDetailProductByPcode(transportItem[0].trim());
//				if(null == product)
//				{
//					try
//					{
//						product = floorProductMgr.getDetailProductByPcid(Long.parseLong(transportItem[0].trim()));
//						
//					}
//					catch(NumberFormatException e)
//					{
//						//传过来的是code，通过code得到商品
//						product = floorProductMgr.getDetailProductByPcode(transportItem[0].trim());
//					}
//				}

				if (product == null) { throw new ProductNotExistException(); }

				DBRow db = new DBRow();
				db.add("to_transport_id",transport_id);
				db.add("to_pc_id",product.get("pc_id",0l));
				db.add("to_p_name",product.getString("p_name"));
				db.add("to_p_code",product.getString("p_code"));
				db.add("to_count",Float.parseFloat(transportItem[1]));
				db.add("to_send_adid",adminLoggerBean.getAdid());
				db.add("to_send_time",DateUtil.NowStr());
				db.add("to_machine_id",machine_id);
				db.add("to_send_adid",adminLoggerBean.getAdid());
				db.add("to_send_time",DateUtil.NowStr());
				
				if(lp_id!=0)
				{
					db.add("to_lp_id",lp_id);
				}
				
				if(!"".equals(sn) && !"NULL".equals(sn))
				{
					db.add("to_serial_number",sn);
				}
				
				db.add("clp_type_id", 0L);
				db.add("blp_type_id", 0L);
				//TLP-->BLP；CLP-->BLP；BLP；散件
				long container_type_id = Long.parseLong(transportItem[4]);
				String container_type  = transportItem[5];
				long sub_container_type_id = Long.parseLong(transportItem[6]);
				String sub_container_type = transportItem[7];
				if("TLP".equals(container_type) && "BLP".equals(sub_container_type))
				{
					db.add("blp_type_id", sub_container_type_id);
				}
				if("CLP".equals(container_type))
				{
					db.add("clp_type_id", container_type_id);
					if("BLP".equals(sub_container_type))
					{
						db.add("blp_type_id", sub_container_type_id);
					}
				}
				if("BLP".equals(container_type))
				{
					db.add("blp_type_id", container_type_id);
				}
				list.add(db);
			}
			
			if(0 == hadSnItems.size())
			{
				
				//保存出库信息
				//transportOutboundMgrZJ.addTransportOutboundSubNoDel(list.toArray(new DBRow[0]));
				
				//保存转运单明細
				this.saveTransportDetailsByOutbound(list, transport_id, transport.get("purchase_id", 0L));
				
				saveTransportDetaisToOutbound(transport_id, adminLoggerBean, machine_id, list);
				
				saveProductSnByTranpsort(transport_id, list);
				
				//改变交货单状态
				DBRow transportRow = new DBRow();
				transportRow.add("transport_status", TransportOrderKey.INTRANSIT);
				floorTransportMgrZyj.updateTransport(transport_id, transportRow);
				
				//加日志
				floorTransportMgrLL.insertLogs(String.valueOf(transport_id), "交货单:"+transport_id+"起运", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 4, TransportOrderKey.INTRANSIT);
				scheduleMgrZr.addScheduleReplayExternal(transport_id , ModuleKey.TRANSPORT_ORDER ,
						4 , "交货单:"+transport_id+"起运" , false, adminLoggerBean, "transport_intransit_period", "true");
				
				//如果预定了门或者位置，将其释放
				doorOrLocationOccupancyMgrZyj.updateDoorOrLocationStatusAndTime(transport_id, TransportOrderKey.INTRANSIT);
				
			}
			
			return hadSnItems;
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (TransportOrderCanNotOutboundException e)
		{
			throw e;
		}
		catch (NoExistTransportOrderException e)
		{
			throw e;
		}
		catch (XMLDataLengthException e)
		{
			throw e;
		}
		catch (XMLDataErrorException e)
		{
			throw e;
		}
		catch (NetWorkException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e, "", log);
		}
	}
	
	/**
	 * 更新转运单的状态为运输中，并释放门或位置
	 * @author Administrator
	 * @param xml
	 * @throws Exception
	 * 1,更新成功；2,转运单不存在；3,转运单的状态不是备货中；4,转运单id未传过来或有问题;5,未添加商品明细
	 */
	public int updateTransportToTransitAndReleaseOccXml(String xml, AdminLoginBean adminLoggerBean) throws Exception
	{
		try
		{
			String transportId = StringUtil.getSampleNode(xml, "transportId");
			long transport_id = 0L;
			if(null != transportId && !"".equals(transportId))
			{
				try
				{
					transport_id = Long.parseLong(transportId);
				}
				catch (NumberFormatException e) {
					transport_id = 0;
				}
				
				DBRow transport = floorTransportMgrZJ.getDetailTransportById(transport_id);
				if(null == transport)
				{
					return 2;
				}
				else
				{
					if(TransportOrderKey.READY == transport.get("transport_status", 0))
					{
						DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null,null, null);
						if(transportDetails.length > 0)
						{
							//改变交货单状态
							DBRow transportRow = new DBRow();
							transportRow.add("transport_status", TransportOrderKey.INTRANSIT);
							floorTransportMgrZyj.updateTransport(transport_id, transportRow);
							
							//加日志
							floorTransportMgrLL.insertLogs(String.valueOf(transport_id), "交货单:"+transport_id+"起运", adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), 4, TransportOrderKey.INTRANSIT);
							scheduleMgrZr.addScheduleReplayExternal(transport_id , ModuleKey.TRANSPORT_ORDER ,
									4 , "交货单:"+transport_id+"起运" , false, adminLoggerBean, "transport_intransit_period", "true");
							
							//如果预定了门或者位置，将其释放
							doorOrLocationOccupancyMgrZyj.updateDoorOrLocationStatusAndTime(transport_id, TransportOrderKey.INTRANSIT);
							return 1;
						}
						else
						{
							return 5;
						}
					}
					else
					{
						return 3;
					}
				}
			}
			else
			{
				return 4;
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateTransportToTransitAndReleaseOccXml",log);
		}
	}
	
	
	private void saveProductSnByTranpsort(long transport_id, List<DBRow> list) throws Exception
	{
		if(null != list)
		{
			for (int i = 0; i < list.size(); i++) 
			{
				DBRow row = list.get(i);
				serialNumberMgrZJ.addSerialProduct(row.get("to_pc_id",0l),row.getString("to_serial_number"),0l);
			}
		}
	}
	
	private void saveTransportDetaisToOutbound(long transport_id,AdminLoginBean adminLoggerBean, String machine_id, List<DBRow> list)throws Exception
	{
		try
		{
			if(list.size() > 0)
			{
				List<DBRow> list2 = new ArrayList<DBRow>();
				for (int i = 0; i < list.size(); i++)
				{
					DBRow detail = list.get(i);
					detail.remove("clp_type_id");
					detail.remove("blp_type_id");
					list2.add(detail);
				}
				transportOutboundMgrZJ.addTransportOutboundSub(list2.toArray(new DBRow[0]),transport_id, null);
			}
		}
		catch (ProductNotExistException e)
		{
			throw e;
		}
		catch (TransportOrderCanNotOutboundException e)
		{
			throw e;
		}
		catch (NoExistTransportOrderException e)
		{
			throw e;
		}
		catch (XMLDataLengthException e)
		{
			throw e;
		}
		catch (XMLDataErrorException e)
		{
			throw e;
		}
		catch (NetWorkException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e, "", log);
		}
	}
	
	
	/**
	 * 保存轉運單明細
	 * @author Administrator
	 * @param list
	 * @param transport_id
	 * @param purchase_id
	 * @throws Exception
	 */
	public void saveTransportDetailsByOutbound(List<DBRow> list, long transport_id, long purchase_id) throws Exception
	{
		
		//保存转运单
//		floorTransportMgrZJ.delTransportDetailByTransportId(transport_id);//批量删除转运明细
		DBRow[] transportDetails = floorTransportMgrZJ.getTransportDetailByTransportId(transport_id, null, null, null, null);
		//转运单之前没有时是添加，有不添加
		if(0 == transportDetails.length)
		{
			for (int i = 0; i < list.size(); i++) 
			{
				DBRow transportDetailRow = list.get(i);
				long pc_id = transportDetailRow.get("to_pc_id", 0L);
				float deliver_count = transportDetailRow.get("to_count", 0F);
				int count = (int)deliver_count;//+backup_count;
				
				DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id, pc_id);
				//DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
				double send_price = product.get("unit_price",0d);
				if(purchaseDetail!=null)
				{
					send_price = purchaseDetail.get("price",0d);
				}
				float union_count = 0;
				float normal_count = 0;
				if(product.get("union_flag",0)==1)
				{
					union_count = count;
				}
				else
				{
					normal_count = count;
				}
				
//				//system.out.println("pc_id:"+pc_id+",deliver_count:"+deliver_count+",count:"+count+",union_count:"+union_count+",normal_count:"+normal_count);
				DBRow transportDetail = new DBRow();
				transportDetail.add("transport_pc_id",pc_id);
				transportDetail.add("transport_delivery_count",deliver_count);
				transportDetail.add("transport_send_count", count);
				transportDetail.add("transport_count",count);
				transportDetail.add("transport_id",transport_id);
				transportDetail.add("transport_p_name",product.getString("p_name"));
				transportDetail.add("transport_p_code",product.getString("p_code"));
				transportDetail.add("transport_volume",product.getString("volume"));
				transportDetail.add("transport_weight",product.getString("weight"));
//				transportDetail.add("transport_box",box);
				transportDetail.add("transport_send_price",send_price);
				transportDetail.add("transport_union_count",union_count);
				transportDetail.add("transport_normal_count",normal_count);
				if(!"".equals(transportDetailRow.getString("to_serial_number")) && !"NULL".equals(transportDetailRow.getString("to_serial_number")))
				{
					transportDetail.add("transport_product_serial_number",transportDetailRow.getString("to_serial_number"));
				}
				transportDetail.add("clp_type_id",transportDetailRow.get("clp_type_id", 0L));
				transportDetail.add("blp_type_id",transportDetailRow.get("blp_type_id", 0L));
				floorTransportMgrZJ.addTransportDetail(transportDetail);
				
			}
		}
	}
	
	/**
	 * 加密算法
	 * @param str
	 * @return
	 */
	private  String HashBase64(String str)
		throws Exception
	{
		  try 
		  {
			String ret="";
			  try 
			  {
				    //Hash算法
				   MessageDigest sha = MessageDigest.getInstance("SHA-1");
				   sha.update(str.getBytes());  
				   ret= new String(new Base64().encode(sha.digest()));
			  }
			  catch (Exception e) 
			  {
				  throw e;
			  }
			  return ret;
		  } 
		  catch (Exception e) 
		  {
			throw new SystemException(e,"HashBase64",log);
		  }
	}
	
	public DBRow[] getTransportByStatusStroageCount(String transport_status, long send_storage_id, long receive_storage_id, 
			long purchase_id, int count) throws Exception
	{
		try
		{
			return floorTransportMgrZyj.getTransportByStatusStroageCount(transport_status, send_storage_id, receive_storage_id, purchase_id, count);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getTransportByStatusAndStroage(int transport_status, long send_storage_id, long receive_storage_id, int purchase_id)",log);
		}
	}
	
	/**
	 * 更新转运单明细
	 * @param request
	 * @throws Exception
	 */
	public void updateTransportDetailByLotNumber(HttpServletRequest request) throws Exception
	{
		try
		{
			long transport_detail_id = StringUtil.getLong(request, "transport_detail_id");
			String lot_number = StringUtil.getString(request, "lot_number");
			DBRow updateRow = new DBRow();
			updateRow.add("lot_number", lot_number);
			floorTransportMgrZJ.modTransportDetail(transport_detail_id, updateRow);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateTransportDetailByLotNumber(HttpServletRequest request)",log);
		}
	}
	
	public void setFloorTransportMgrZyj(FloorTransportMgrZyj floorTransportMgrZyj) {
		this.floorTransportMgrZyj = floorTransportMgrZyj;
	}

	public void setFloorTransportMgrLL(FloorTransportMgrLL floorTransportMgrLL) {
		this.floorTransportMgrLL = floorTransportMgrLL;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setFloorStorageCatalogMgrZyj(
			FloorStorageCatalogMgrZyj floorStorageCatalogMgrZyj) {
		this.floorStorageCatalogMgrZyj = floorStorageCatalogMgrZyj;
	}

	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}

	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}

	public void setSupplierMgr(SupplierMgrIFaceTJH supplierMgr) {
		this.supplierMgr = supplierMgr;
	}

	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setFloorPurchaseMgrZyj(FloorPurchaseMgrZyj floorPurchaseMgrZyj) {
		this.floorPurchaseMgrZyj = floorPurchaseMgrZyj;
	}

	public void setFloorApplyMoneyMgrZZZ(FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ) {
		this.floorApplyMoneyMgrZZZ = floorApplyMoneyMgrZZZ;
	}

	public void setFloorPurchaseMgr(FloorPurchaseMgr floorPurchaseMgr) {
		this.floorPurchaseMgr = floorPurchaseMgr;
	}

	public void setFloorPreparePurchaseFundsMgrZwb(
			FloorPreparePurchaseFundsMgrZwb floorPreparePurchaseFundsMgrZwb) {
		this.floorPreparePurchaseFundsMgrZwb = floorPreparePurchaseFundsMgrZwb;
	}

	public void setFloorCatalogMgr(FloorCatalogMgr floorCatalogMgr) {
		this.floorCatalogMgr = floorCatalogMgr;
	}

	public void setFloorSupplierMgrTJH(FloorSupplierMgrTJH floorSupplierMgrTJH) {
		this.floorSupplierMgrTJH = floorSupplierMgrTJH;
	}

	public void setFloorAdminMgr(FloorAdminMgr floorAdminMgr) {
		this.floorAdminMgr = floorAdminMgr;
	}

	public void setProduresMgrZyj(ProduresMgrZyjIFace produresMgrZyj) {
		this.produresMgrZyj = produresMgrZyj;
	}

	public void setFloorFreightMgrLL(FloorFreightMgrLL floorFreightMgrLL) {
		this.floorFreightMgrLL = floorFreightMgrLL;
	}

	public void setContainerProductMgrZJ(
			ContainerProductMgrIFaceZJ containerProductMgrZJ)
	{
		this.containerProductMgrZJ = containerProductMgrZJ;
	}

	public void setTransportOutboundMgrZJ(
			TransportOutboundMgrZJ transportOutboundMgrZJ)
	{
		this.transportOutboundMgrZJ = transportOutboundMgrZJ;
	}

	public void setSerialNumberMgrZJ(SerialNumberMgrIFaceZJ serialNumberMgrZJ)
	{
		this.serialNumberMgrZJ = serialNumberMgrZJ;
	}

	public void setDoorOrLocationOccupancyMgrZyj(
			DoorOrLocationOccupancyMgrZyjIFace doorOrLocationOccupancyMgrZyj)
	{
		this.doorOrLocationOccupancyMgrZyj = doorOrLocationOccupancyMgrZyj;
	}


}
