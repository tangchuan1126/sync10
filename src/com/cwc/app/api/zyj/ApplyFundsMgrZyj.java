package com.cwc.app.api.zyj;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.floor.api.zr.FloorTransportMgrZr;
import com.cwc.app.floor.api.zyj.FloorApplyFundsMgrZyj;
import com.cwc.app.floor.api.zyj.FloorRepairOrderMgrZyj;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyMgrZZZ;
import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.zr.AccountMgrIfaceZr;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zyj.ApplyFundsMgrZyjIFace;
import com.cwc.app.iface.zzz.ApplyMoneyLogsMgrIFaceZZZ;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.RepairLogTypeKey;
import com.cwc.app.key.RepairStockInSetKey;
import com.cwc.app.key.TransportLogTypeKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

public class ApplyFundsMgrZyj implements ApplyFundsMgrZyjIFace{

	static Logger log = Logger.getLogger("ACTION");
	private FloorApplyFundsMgrZyj floorApplyFundsMgrZyj;
	private FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ;
	private FloorTransportMgrZJ floorTransportMgrZJ;
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private FloorRepairOrderMgrZyj floorRepairOrderMgrZyj;
	private FloorTransportMgrZr floorTransportMgrZr;
	private ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ;
	private AdminMgr adminMgr = new AdminMgr();
	private AccountMgrIfaceZr accountMgrZr ;
	@Override
	public int handleApplyFundsAssociateTransport() throws Exception 
	{
		try {
			//获取types为6，即转运单的运费
			DBRow[] applyMoney = floorApplyFundsMgrZyj.getApplyMoneyByAssociateTypeAndTypes();
			int count = 0;
			for (int i = 0; i < applyMoney.length; i++) {
				String associateId = applyMoney[i].getString("association_id");
				if(associateId.startsWith("T"))
				{
					associateId = associateId.substring(1, associateId.length());
				}
				long applyMoneyId = applyMoney[i].get("apply_id", 0);
				DBRow transportRow = floorTransportMgrZJ.getDetailTransportById(Long.valueOf(associateId));
				long purchase_id	= transportRow.get("purchase_id", 0);
				if(0 != purchase_id)
				{
					++ count;
					DBRow row = new DBRow();
					row.add("association_type_id", 5);
					floorApplyMoneyMgrZZZ.updateApplyMoneyById(applyMoneyId,row);
				}
			}
			return count;
		} catch (Exception e) {
			throw new SystemException(e,"handleApplyFundsAssociateTransport",log);
		}
	}
	
	/**
	 * 通过资金的ID，得到其下所有申请转账的总额（转成RMB的）
	 */
	public DBRow getTotalApplyTransferByApplyId(long applyId) throws Exception
	{
		try {
			return floorApplyFundsMgrZyj.getTotalApplyTransferByApplyId(applyId);
		} catch (Exception e) {
			throw new SystemException(e,"getTotalApplyTransferByApplyId",log);
		}
	}

	/**
	 * 获取转运单申请运费总额
	 */
	public double getTransportFreightTotalApply(long transportId, int typeId) throws Exception
	{
		try
		{
			double totalApplyMoney = 0.0;
			DBRow[] applyMoneys = floorApplyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(10015,transportId,typeId);
			for (int i = 0; i < applyMoneys.length; i++) {
				DBRow applyMoney = applyMoneys[i];
				totalApplyMoney += applyMoney.get("standard_money", 0.0);
			}
			return totalApplyMoney;
			
		} catch (Exception e) {
			throw new SystemException(e,"getTransportFreightTotalApply",log);
		}
	}

	/**
	 * 通过关联ID,关联类型ID,查询所有资金申请
	 */
	public DBRow[] getApplyMoneyByAssociationIdAndAssociationTypeIdNoCancel(long association_id,int association_type_id) throws Exception
	{
		try 
		{
			return floorApplyFundsMgrZyj.getApplyMoneyByAssociationIdAndAssociationTypeIdNoCancel(association_id, association_type_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getApplyMoneyByAssociationIdAndAssociationTypeIdNoCancel",log);
		}
	}
	
	public long applyRepairFreightMoney(HttpServletRequest request) throws Exception {
		try
		{
			SystemConfigIFace systemConfig	=((SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig"));
			long payee_type_id			= StringUtil.getLong(request, "payee_type_id");
			long association_type_id	= StringUtil.getLong(request, "association_type_id");
			long types					= StringUtil.getLong(request, "types");
			String create_time			= StringUtil.getString(request, "create_time");
			long association_id			= StringUtil.getLong(request, "association_id");
			String payee				= StringUtil.getString(request, "payee");
			String last_time			= StringUtil.getString(request, "last_time");
			String creater				= StringUtil.getString(request, "creater");
			long center_account_id		= StringUtil.getLong(request, "center_account_id");
			long center_account_type_id	= StringUtil.getLong(request, "center_account_type_id");
			String remark				= StringUtil.getString(request, "remark");
			long payee_type1_id			= StringUtil.getLong(request, "payee_type1_id");
			long center_account_type1_id= StringUtil.getLong(request, "center_account_type1_id");
			String payment_information	= StringUtil.getString(request, "payment_information");
			long payee_id				= StringUtil.getLong(request, "payee_id");
			long creater_id				= StringUtil.getLong(request, "creater_id");
			String currency				= StringUtil.getString(request, "currency");
			double amount				= StringUtil.getDouble(request, "amount");
			
			DBRow row = new DBRow();
			row.add("payee_type_id", payee_type_id);
			row.add("association_type_id", association_type_id);
			row.add("types", types);
			row.add("create_time", create_time);
			row.add("association_id", association_id);
			row.add("payee", payee);
			row.add("last_time", last_time);
			row.add("creater", creater);
			row.add("center_account_id", center_account_id);
			row.add("center_account_type_id", center_account_type_id);
			row.add("remark", remark);
			row.add("payee_type1_id", payee_type1_id);
			row.add("center_account_type1_id", center_account_type1_id);
			row.add("payment_information", payment_information);
			row.add("payee_id", payee_id);
			row.add("creater_id", creater_id);
			row.add("currency", currency);
			row.add("amount", amount);
			
			if("RMB".equals(currency)){
				row.add("standard_money", amount);
			}else{
				row.add("standard_money", amount*Double.parseDouble(systemConfig.getStringConfigValue(currency)));
			}
			long apply_money_id = floorApplyFundsMgrZyj.insertApplyMoney(row);
			//保存凭证文件
			String fileNames = StringUtil.getString(request, "file_names");
			if(null!=fileNames && fileNames.trim().length()>0)
			{
				String sn = StringUtil.getString(request, "sn");
				String path = StringUtil.getString(request, "path");
				handleApplyMoneyFile(sn, path, fileNames, apply_money_id);
			}
			
			com.cwc.app.iface.zzz.ApplyMoneyMgrIFace applyMoneyMgrZZZ=(com.cwc.app.iface.zzz.ApplyMoneyMgrIFace)MvcUtil.getBeanFromContainer("proxyApplyMoneyMgr");
			applyMoneyMgrZZZ.editApplyMoneyIndex(apply_money_id,"add");
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			//加任务和日志
			this.addTransferFreightSchedule(request, apply_money_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), amount, currency,association_id,last_time);
			return apply_money_id;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"applyRepairFreightMoney",log);
		}
	}
	
	
	public void handleApplyMoneyFile(String sn, String path, String fileNames, long apply_money_id) throws Exception 
	{
		try{
			//重新命名图片文件F_applyMoney_applyId_文件的名称_index.jpg
			//移动图片位置
			//多个文件上传的时候要循环的处理
			String baseTempUrl = Environment.getHome()+"upl_imags_tmp/";
			boolean isFileUp = (fileNames.trim().length() > 0 ? true:false );
			if(isFileUp){
				if(fileNames.trim().length() > 0 ){
					String[] fileNameArray =  fileNames.split(",");
					if(fileNameArray != null && fileNameArray.length > 0){
						for(String tempFileName : fileNameArray){
							String suffix = getSuffix(tempFileName);
							String tempUrl =  baseTempUrl+tempFileName;
							StringBuffer realFileName = new StringBuffer(sn).append("_").append(apply_money_id)
							.append("_").append(tempFileName.replace("."+suffix, ""));
							int indexFileName = floorRepairOrderMgrZyj.getIndexOfFilebyFileName(realFileName.toString());
							if(indexFileName != 0){
								realFileName.append("_").append(indexFileName);
							}
							realFileName.append(".").append(suffix);
							StringBuffer url = new StringBuffer();
							url.append(Environment.getHome()).append("upload/").append(path).append("/").append(realFileName.toString());
							FileUtil.moveFile(tempUrl,url.toString());
							// file表上添加一条记录
							this.insertApplyMoneyFile(apply_money_id, 1, realFileName.toString());
						}
					}
				}
			}
 
		}catch (Exception e) {
			throw new SystemException(e, "handleApplyMoneyFile", log);
		}
	}
	
	private String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	
	/**
	 * 添加申请资金的文件
	 * @param association_id
	 * @param association_type
	 * @throws Exception
	 */
	public void insertApplyMoneyFile(long association_id, int association_type, String path) throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("association_id", association_id);
			row.add("association_type", association_type);
			row.add("path", path);
			floorApplyFundsMgrZyj.insertApplyMoneyFile(row);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e, "insertApplyMoneyFile", log);
		}
	}
	
	
	/**
	 * 加运费申请资金的任务
	 * @param request
	 * @param apply_id
	 * @param adid
	 * @param adName
	 * @param amount
	 * @param currency
	 * @throws Exception
	 */
	private void addTransferFreightSchedule(HttpServletRequest request, long apply_id, long adid, String adName, double amount, String currency, long association_id, String last_time) throws Exception
	{
		try
		{
			String adminUserNames	= StringUtil.getString(request, "adminUserNames");
			String adminUserIds		= StringUtil.getString(request, "adminUserIds");
			int isNeedMail			= StringUtil.getInt(request, "isNeedMail");
			int isNeedMessage		= StringUtil.getInt(request, "isNeedMessage");
			int isNeedPage			= StringUtil.getInt(request, "isNeedPage");
			boolean isSendMail = false;
			boolean isSendMessage = false;
			boolean isSendPage = false;
			if(2 == isNeedMail)
			{
				isSendMail = true;
			}
			if(2 == isNeedMessage)
			{
				isSendMessage = true;
			}
			if(2 == isNeedPage)
			{
				isSendPage = true;
			}
			String content = "创建了资金申请:"+apply_id+",金额:"+amount+currency+",负责人:"+adminUserNames;
			//加运费申请资金的任务
			scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), last_time+" 23:59:59", 
						adid,adminUserIds, "",
						apply_id, ModuleKey.APPLY_MONEY, "申请运费:"+apply_id, 
						adName+content,
						isSendPage, isSendMail, isSendMessage, request, true, ProcessKey.REPAIR_STOCK_APPLY_FUNDS);
			
			DBRow transport = floorRepairOrderMgrZyj.getDetailRepairOrderById(association_id);
			RepairStockInSetKey repairStockInSetKey = new RepairStockInSetKey();
			int transport_stock_in_set = transport.get("stock_in_set", 0);
			//跟进运费
			scheduleMgrZr.addScheduleReplayExternal(association_id, ModuleKey.REPAIR_ORDER, RepairLogTypeKey.Freight, adName+"确认了["+repairStockInSetKey.getStatusById(transport_stock_in_set)+"]阶段,"+content, false, request, "repair_stock_in_set_period");
			//加日志
			applyMoneyLogsMgrZZZ.addApplyMoneyLogs(apply_id, adid, adName, adName+"创建资金单:"+apply_id);
			floorRepairOrderMgrZyj.insertLogs(association_id, "["+repairStockInSetKey.getStatusById(transport_stock_in_set)+"]阶段,"+content, adid, adName, RepairLogTypeKey.Freight, "", transport_stock_in_set);
			
		} catch (Exception e) {
			throw new SystemException(e,"addTransferFreightSchedule",log);
		}
	}
	
	public long applyTransportFreightMoney(HttpServletRequest request) throws Exception {
		try
		{
			SystemConfigIFace systemConfig	=((SystemConfigIFace) MvcUtil.getBeanFromContainer("systemConfig"));
			long payee_type_id			= StringUtil.getLong(request, "payee_type_id");
			long association_type_id	= StringUtil.getLong(request, "association_type_id");
			long types					= StringUtil.getLong(request, "types");
			String create_time			= StringUtil.getString(request, "create_time");
			long association_id			= StringUtil.getLong(request, "association_id");
			String payee				= StringUtil.getString(request, "payee");
			String last_time			= StringUtil.getString(request, "last_time");
			String creater				= StringUtil.getString(request, "creater");
			long center_account_id		= StringUtil.getLong(request, "center_account_id");
			long center_account_type_id	= StringUtil.getLong(request, "center_account_type_id");
			String remark				= StringUtil.getString(request, "remark");
			long payee_type1_id			= StringUtil.getLong(request, "payee_type1_id");
			long center_account_type1_id= StringUtil.getLong(request, "center_account_type1_id");
			String payment_information	= StringUtil.getString(request, "payment_information");
			long payee_id				= StringUtil.getLong(request, "payee_id");
			long creater_id				= StringUtil.getLong(request, "creater_id");
			String currency				= StringUtil.getString(request, "currency");
			double amount				= StringUtil.getDouble(request, "amount");
			
			DBRow row = new DBRow();
			row.add("payee_type_id", payee_type_id);
			row.add("association_type_id", association_type_id);
			row.add("types", types);
			row.add("create_time", create_time);
			row.add("association_id", association_id);
			row.add("payee", payee);
			row.add("last_time", last_time);
			row.add("creater", creater);
			row.add("center_account_id", center_account_id);
			row.add("center_account_type_id", center_account_type_id);
			row.add("remark", remark);
			row.add("payee_type1_id", payee_type1_id);
			row.add("center_account_type1_id", center_account_type1_id);
			row.add("payment_information", payment_information);
			row.add("payee_id", payee_id);
			row.add("creater_id", creater_id);
			row.add("currency", currency);
			row.add("amount", amount);
			
			if("RMB".equals(currency)){
				row.add("standard_money", amount);
			}else{
				row.add("standard_money", amount*Double.parseDouble(systemConfig.getStringConfigValue(currency)));
			}
			long apply_money_id = floorApplyFundsMgrZyj.insertApplyMoney(row);
			//添加账户信息
			accountMgrZr.addAccount(apply_money_id, request);
			
			
			//保存凭证文件
			String fileNames = StringUtil.getString(request, "file_names");
			if(null!=fileNames && fileNames.trim().length()>0)
			{
				String sn = StringUtil.getString(request, "sn");
				String path = StringUtil.getString(request, "path");
				handleApplyMoneyFile(sn, path, fileNames, apply_money_id);
			}
			
			com.cwc.app.iface.zzz.ApplyMoneyMgrIFace applyMoneyMgrZZZ=(com.cwc.app.iface.zzz.ApplyMoneyMgrIFace)MvcUtil.getBeanFromContainer("proxyApplyMoneyMgr");
			applyMoneyMgrZZZ.editApplyMoneyIndex(apply_money_id,"add");
			
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			//加任务和日志
			this.addTransportTransferFreightSchedule(request, apply_money_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), amount, currency,association_id,last_time);
			return apply_money_id;
		}
		catch(Exception e)
		{
			throw new SystemException(e,"applyRepairFreightMoney",log);
		}
	}
	
	/**
	 * 加运费申请资金的任务
	 * @param request
	 * @param apply_id
	 * @param adid
	 * @param adName
	 * @param amount
	 * @param currency
	 * @throws Exception
	 */
	private void addTransportTransferFreightSchedule(HttpServletRequest request, long apply_id, long adid, String adName, double amount, String currency, long association_id, String last_time) throws Exception
	{
		try
		{
			String adminUserNames	= StringUtil.getString(request, "adminUserNames");
			String adminUserIds		= StringUtil.getString(request, "adminUserIds");
			int isNeedMail			= StringUtil.getInt(request, "isNeedMail");
			int isNeedMessage		= StringUtil.getInt(request, "isNeedMessage");
			int isNeedPage			= StringUtil.getInt(request, "isNeedPage");
			boolean isSendMail = false;
			boolean isSendMessage = false;
			boolean isSendPage = false;
			if(2 == isNeedMail)
			{
				isSendMail = true;
			}
			if(2 == isNeedMessage)
			{
				isSendMessage = true;
			}
			if(2 == isNeedPage)
			{
				isSendPage = true;
			}
			String content = "创建了资金申请:"+apply_id+",金额:"+amount+currency+",负责人:"+adminUserNames;
			//加运费申请资金的任务
			scheduleMgrZr.addScheduleByExternalArrangeNow(DateUtil.NowStr(), last_time+" 23:59:59", 
						adid,adminUserIds, "",
						apply_id, ModuleKey.APPLY_MONEY, "申请运费:"+apply_id, 
						adName+content,
						isSendPage, isSendMail, isSendMessage, request, true, ProcessKey.TRANSPORT_STOCK_APPLY_FUNDS);
			
			DBRow transport = floorTransportMgrZJ.getDetailTransportById(Long.valueOf(association_id));
			TransportStockInSetKey transportStockInSetKey = new TransportStockInSetKey();
			int transport_stock_in_set = transport.get("stock_in_set", 0);
			//跟进运费
			scheduleMgrZr.addScheduleReplayExternal(association_id, ModuleKey.TRANSPORT_ORDER, TransportLogTypeKey.Freight, adName+"确认了["+transportStockInSetKey.getStatusById(transport_stock_in_set)+"]阶段,"+content, false, request, "transport_stock_in_set_period");
			//加日志
			applyMoneyLogsMgrZZZ.addApplyMoneyLogs(apply_id, adid, adName, adName+"创建资金单:"+apply_id);
			TDate tDate = new TDate();
			String currentTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
			DBRow row = new DBRow();
			row.add("transport_id", association_id);
			row.add("transport_content", "["+transportStockInSetKey.getStatusById(transport_stock_in_set)+"]阶段,"+content);
			row.add("transporter_id", adid);
			row.add("transporter", adName);
			row.add("transport_type", TransportLogTypeKey.Freight);
			row.add("activity_id", transport_stock_in_set);
			row.add("transport_date", currentTime);
			floorTransportMgrZr.addTransportLog(row);
			
		} catch (Exception e) {
			throw new SystemException(e,"addTransportTransferFreightSchedule",log);
		}
	}
	
	
	public void setFloorApplyFundsMgrZyj(FloorApplyFundsMgrZyj floorApplyFundsMgrZyj) {
		this.floorApplyFundsMgrZyj = floorApplyFundsMgrZyj;
	}

	public void setFloorApplyMoneyMgrZZZ(FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ) {
		this.floorApplyMoneyMgrZZZ = floorApplyMoneyMgrZZZ;
	}

	public void setFloorTransportMgrZJ(FloorTransportMgrZJ floorTransportMgrZJ) {
		this.floorTransportMgrZJ = floorTransportMgrZJ;
	}

	public AdminMgr getAdminMgr() {
		return adminMgr;
	}

	public void setAdminMgr(AdminMgr adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setFloorRepairOrderMgrZyj(
			FloorRepairOrderMgrZyj floorRepairOrderMgrZyj) {
		this.floorRepairOrderMgrZyj = floorRepairOrderMgrZyj;
	}

	public void setFloorTransportMgrZr(FloorTransportMgrZr floorTransportMgrZr) {
		this.floorTransportMgrZr = floorTransportMgrZr;
	}

	public void setApplyMoneyLogsMgrZZZ(
			ApplyMoneyLogsMgrIFaceZZZ applyMoneyLogsMgrZZZ) {
		this.applyMoneyLogsMgrZZZ = applyMoneyLogsMgrZZZ;
	}

	public void setAccountMgrZr(AccountMgrIfaceZr accountMgrZr)
	{
		this.accountMgrZr = accountMgrZr;
	}
	



}
