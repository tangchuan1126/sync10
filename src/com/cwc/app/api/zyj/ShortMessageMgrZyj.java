package com.cwc.app.api.zyj;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zyj.FloorShortMessageMgrZyj;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.zyj.ShortMessageMgrZyjIFace;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.ShortMessageUtil;
import com.cwc.util.StringUtil;

public class ShortMessageMgrZyj implements ShortMessageMgrZyjIFace{

	private static Logger log = Logger.getLogger("ACTION");
	private FloorShortMessageMgrZyj floorShortMessageMgrZyj;
	

	@Override
	public DBRow[] getShortMessageList(PageCtrl pc, int moduleIdSearch, String receiverNameSearch, String receiverPhoneSearch, String messageContentSearch, String joinUserId) throws Exception {
		try {
			DBRow userInfo = new DBRow();
			if(!"".equals(joinUserId)){
				userInfo = getUserInfoByUserId(Long.parseLong(joinUserId));
			}
			if(null != userInfo){
				String userPhone	= userInfo.getString("mobilePhone");
				if(null != userPhone && !"".equals(userPhone)){
					receiverPhoneSearch = userPhone;
				}else{
					String userName		= userInfo.getString("employe_name");
					receiverNameSearch	= userName;
				}
			}
			return floorShortMessageMgrZyj.getShortMessageList(pc, moduleIdSearch, receiverNameSearch, receiverPhoneSearch, messageContentSearch);
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.getShortMessageList(pc)",log);
		}
	}

	@Override
	public DBRow[] getShortMessageList(PageCtrl pc, String joinUserId)throws Exception{
		try {
			DBRow userInfo = new DBRow();
			String receiverPhoneSearch	= "";
			String receiverNameSearch	= "";
			if(!"".equals(joinUserId)){
				userInfo = getUserInfoByUserId(Long.parseLong(joinUserId));
			}
			if(null != userInfo){
				String userPhone	= userInfo.getString("mobilePhone");
				if(null != userPhone && !"".equals(userPhone)){
					receiverPhoneSearch = userPhone;
				}else{
					String userName		= userInfo.getString("employe_name");
					receiverNameSearch	= userName;
				}
			}
			return floorShortMessageMgrZyj.getShortMessageList(pc, 0, receiverNameSearch, receiverPhoneSearch, "");
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.getShortMessageList(pc,String joinUserId)",log);
		}	
	}
	
	
	@Override
	public DBRow[] getShortMessageReceivers(long messageId) throws Exception {
		try {
			return floorShortMessageMgrZyj.getShortMessageReceivers(messageId);
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.getShortMessageReceivers(messageId)",log);
		}
	}

	/**
	 * 暂时不用
	 */
	@Override
	public boolean addShortMessage(HttpServletRequest request, DBRow[] receivers) throws Exception {
		try {
			int module_id			= StringUtil.getInt(request, "module_id");
			int business_id			= StringUtil.getInt(request, "business_id");
			String content			= StringUtil.getString(request, "content");
			String receiver_ids		= StringUtil.getString(request, "receiver_ids");
			//this.addShortMessage(module_id, business_id, content, receivers, request);
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.addShortMessage(HttpServletRequest request, DBRow[] receivers)",log);
		}
	}
	
	/**
	 * 模块ID,业务ID,短信内容，接收人的数组，保存短信信息及接收人信息
	 */
	@Override
	public boolean addShortMessage(long module_id, long business_id,
			String content, DBRow[] receivers, long adid) throws Exception {
		
		try 
		{
			ShortMessageUtil shortMessageUtil = new ShortMessageUtil();
			DBRow shortMessageInfo = new DBRow();
			shortMessageInfo.add("module_id", module_id);
			shortMessageInfo.add("business_id", business_id);
			content = this.handleShortMessageContent(content);
			shortMessageInfo.add("content", content);
  			shortMessageInfo.add("send_user", adid);//此值取当前登录系统的用户ID
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String sendTime = sdf.format(new Date());
			shortMessageInfo.add("send_time", sendTime);
			long shortMessageId = floorShortMessageMgrZyj
					.addShortMessage(shortMessageInfo);
			//对于接收者的数据，要求必须有用户名，用户电话，名字可为空
			if (null != receivers && receivers.length > 0) {
				for (int i = 0; i < receivers.length; i ++) {
					DBRow receiver = receivers[i];
					receiver.add("short_message_id", shortMessageId);
					int isExcuess = 0;
					//调用发送短信的工具类的发送短信方法
					try 
					{
						if(!"".equals(receiver.getString("receiver_phone")))
						{
							isExcuess = shortMessageUtil.sendPhoneMessage(content, receiver.getString("receiver_phone"));
						}
					} 
					catch (Exception e) 
					{
						isExcuess = 3;
						
					}finally{
						receiver.add("is_success", isExcuess);
						floorShortMessageMgrZyj.addShortMessageReceiver(receiver);
					}
				}
			}
			DBRow row = new DBRow();
			row.add("confvalue",shortMessageUtil.getBalance());
			floorShortMessageMgrZyj.updateSystemConfigShortMessage(row);
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.addShortMessage(int module_id, int business_id, String content, DBRow[] receivers)",log);
		}
		
	}
	
	
	
	
	/**
	 * 模块ID,业务ID,短信内容，接收人的数组，保存短信信息及接收人信息
	 */
	@Override
	public boolean addShortMessage(long module_id, long business_id,
			String content, DBRow[] receivers, AdminLoginBean adminLoggerBean) throws Exception {
		
		try 
		{
			ShortMessageUtil shortMessageUtil = new ShortMessageUtil();
			DBRow shortMessageInfo = new DBRow();
			shortMessageInfo.add("module_id", module_id);
			shortMessageInfo.add("business_id", business_id);
			content = this.handleShortMessageContent(content);
			shortMessageInfo.add("content", content);
		 	long adid = adminLoggerBean.getAdid(); 
			shortMessageInfo.add("send_user", adid);//此值取当前登录系统的用户ID
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String sendTime = sdf.format(new Date());
			shortMessageInfo.add("send_time", sendTime);
			long shortMessageId = floorShortMessageMgrZyj
					.addShortMessage(shortMessageInfo);
			//对于接收者的数据，要求必须有用户名，用户电话，名字可为空
			if (null != receivers && receivers.length > 0) {
				for (int i = 0; i < receivers.length; i ++) {
					DBRow receiver = receivers[i];
					receiver.add("short_message_id", shortMessageId);
					int isExcuess = 0;
					//调用发送短信的工具类的发送短信方法
					try 
					{
						if(!"".equals(receiver.getString("receiver_phone")))
						{
							isExcuess = shortMessageUtil.sendPhoneMessage(content, receiver.getString("receiver_phone"));
						}
					} 
					catch (Exception e) 
					{
						isExcuess = 3;
					}finally{
						receiver.add("is_success", isExcuess);
						floorShortMessageMgrZyj.addShortMessageReceiver(receiver);
					}
				}
			}
			DBRow row = new DBRow();
			row.add("confvalue",shortMessageUtil.getBalance());
			floorShortMessageMgrZyj.updateSystemConfigShortMessage(row);
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.addShortMessage(int module_id, int business_id, String content, DBRow[] receivers)",log);
		}
		
	}
	
	/**
	 * 模块ID,业务ID,短信内容，接收人的群组ID，保存短信信息及接收人信息
	 */
	@Override
	public boolean addShortMessage(long module_id, long business_id, String content, long adgId, HttpServletRequest request) throws Exception{
		
		try {
//			ShortMessageUtil shortMessageUtil = new ShortMessageUtil();
			DBRow shortMessageInfo = new DBRow();
			shortMessageInfo.add("module_id", module_id);
			shortMessageInfo.add("business_id", business_id);
			shortMessageInfo.add("content", content);
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
		 	long adid = adminLoggerBean.getAdid(); 
			shortMessageInfo.add("send_user", adid);//此值取当前登录系统的用户ID
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String sendTime = sdf.format(new Date());
			shortMessageInfo.add("send_time", sendTime);
			long shortMessageId = floorShortMessageMgrZyj
					.addShortMessage(shortMessageInfo);
			//对于接收者的数据，要求必须有用户ID,用户名，用户电话，ID可为空
			DBRow[] receivers = floorShortMessageMgrZyj.getUserListByAdgId(adgId);
			
			if (null != receivers && receivers.length > 0) {
				for (DBRow receiver : receivers) {
					receiver.add("short_message_id", shortMessageId);
					floorShortMessageMgrZyj.addShortMessageReceiver(receiver);
					//调用发送短信的工具类的发送短信方法
					if(!"".equals(receiver.getString("receiver_phone"))){
//						shortMessageUtil.sendPhoneMessage(content, receiver.getString("receiver_phone"));
					}
				}
			}
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.addShortMessage(int module_id, int business_id, String content, int adgId)",log);
		}
		
	}
	
	/**
	 * 模块ID,业务ID,短信内容，接收人的群组ID，角色Id,保存短信信息及接收人信息
	 */
	public boolean addShortMessage(int module_id, int business_id, String content, int adgId, int roleId, HttpServletRequest request) throws Exception{
		try {
//			ShortMessageUtil shortMessageUtil = new ShortMessageUtil();
			DBRow shortMessageInfo = new DBRow();
			shortMessageInfo.add("module_id", module_id);
			shortMessageInfo.add("business_id", business_id);
			shortMessageInfo.add("content", content);
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
		 	long adid = adminLoggerBean.getAdid(); 
			shortMessageInfo.add("send_user", adid);//此值取当前登录系统的用户ID
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String sendTime = sdf.format(new Date());
			shortMessageInfo.add("send_time", sendTime);
			long shortMessageId = floorShortMessageMgrZyj
					.addShortMessage(shortMessageInfo);
			//对于接收者的数据，要求必须有用户ID,用户名，用户电话，ID可为空
			DBRow[] receivers = floorShortMessageMgrZyj.getUserListByAdgIdRoleId(adgId, roleId);
			if (null != receivers && receivers.length > 0) {
				for (DBRow receiver : receivers) {
					receiver.add("short_message_id", shortMessageId);
					floorShortMessageMgrZyj.addShortMessageReceiver(receiver);
					//调用发送短信的工具类的发送短信方法
					if(!"".equals(receiver.getString("receiver_phone"))){
//						shortMessageUtil.sendPhoneMessage(content, receiver.getString("receiver_phone"));
					}
				}
			}
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.addShortMessage(int module_id, int business_id, String content, int adgId)",log);
		}
	}
	
	/**
	 * 模块ID,业务ID,短信内容，接收人的群组ID，角色Id数组,保存短信信息及接收人信息
	 */
	public boolean addShortMessage(long module_id, long business_id, String content, long adgId, long[] roleId, HttpServletRequest request) throws Exception{
		
		try {
//			ShortMessageUtil shortMessageUtil = new ShortMessageUtil();
			DBRow shortMessageInfo = new DBRow();
			shortMessageInfo.add("module_id", module_id);
			shortMessageInfo.add("business_id", business_id);
			shortMessageInfo.add("content", content);
			AdminMgrIFace adminMgr = (AdminMgrIFace)MvcUtil.getBeanFromContainer("adminMgr");
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
		 	long adid = adminLoggerBean.getAdid(); 
			shortMessageInfo.add("send_user", adid);//此值取当前登录系统的用户ID
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String sendTime = sdf.format(new Date());
			shortMessageInfo.add("send_time", sendTime);
			long shortMessageId = floorShortMessageMgrZyj
					.addShortMessage(shortMessageInfo);
			//对于接收者的数据，要求必须有用户ID,用户名，用户电话，ID可为空
			DBRow[] receivers = floorShortMessageMgrZyj.getUserListByAdgIdRoleIdArray(adgId, roleId);
			if (null != receivers && receivers.length > 0) {
				for (DBRow receiver : receivers) {
					receiver.add("short_message_id", shortMessageId);
					floorShortMessageMgrZyj.addShortMessageReceiver(receiver);
					//调用发送短信的工具类的发送短信方法
					if(!"".equals(receiver.getString("receiver_phone"))){
//						shortMessageUtil.sendPhoneMessage(content, receiver.getString("receiver_phone"));
					}
				}
			}
			return true;
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.addShortMessage(int module_id, int business_id, String content, int adgId)",log);
		}
	}
	
	/**
	 * 封装收信人的名字，电话
	 */
	@Override
	public DBRow generateReceiver(String receiverName,
			String receiverPhone) throws Exception {

		DBRow receiver = new DBRow();
		receiver.add("receiver_name", receiverName);
		receiver.add("receiver_phone", receiverPhone);
		return receiver;
	}
	
	/**
	 * 通过用户ID得到用户信息,组装成reciver_id,receiver_name,receiver_phone
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@Override
	public DBRow getUserRowByUserId(long userId) throws Exception{
		try {
			return floorShortMessageMgrZyj.getUserRowByUserId(userId);
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.getUserRowByUserId(int userId)",log);
		}
	}
	
	/**
	 * 通过用户ID得到用户名
	 */
	@Override
	public DBRow getUserInfoByUserId(long userId) throws Exception{
		
		try {
			return floorShortMessageMgrZyj.getUserInfoByUserId(userId);
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.getUserInfoByUserId(int userId)",log);
		}
		
	}
	
	public DBRow[] getUserListByAdgIdRoleId(long adgId, long[] roleId) throws Exception{
		try {
			return floorShortMessageMgrZyj.getUserListByAdgIdRoleIdArray(adgId, roleId);
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.getUserListByAdgIdRoleId(long adgId, long roleId)",log);
		}
	}
	
	public DBRow[] getUserListByAdgId(long adgId) throws Exception{
		try {
			return floorShortMessageMgrZyj.getUserListByAdgId(adgId);
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.getUserListByAdgId(long adgId)",log);
		}
	}
	
	@Override
	public String handleShortMessageContent(String content) throws Exception{
		try {
			DBRow[] wordList = floorShortMessageMgrZyj.getMessageContentWordList();
			if(wordList.length > 0){
				for (int i = 0; i < wordList.length; i++) {
					String originalWord = wordList[i].getString("original_word");
					String replaceWord	= wordList[i].getString("replace_word");
					if(content.contains(originalWord)){
						content = content.replaceAll(originalWord, replaceWord);
					}
				}
			}
			return content;
		} catch (Exception e) {
			throw new SystemException(e,"ShortMessageZyj.handleShortMessageContent(String content)",log);
		}
	}
	public void setFloorShortMessageMgrZyj(
			FloorShortMessageMgrZyj floorShortMessageMgrZyj) {
		this.floorShortMessageMgrZyj = floorShortMessageMgrZyj;
	}

	

}
