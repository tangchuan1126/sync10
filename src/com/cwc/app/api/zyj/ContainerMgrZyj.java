package com.cwc.app.api.zyj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zj.FloorLPTypeMgrZJ;
import com.cwc.app.floor.api.zr.FloorBoxTypeZr;
import com.cwc.app.floor.api.zr.FloorClpTypeMgrZr;
import com.cwc.app.floor.api.zyj.FloorContainerMgrZyj;
import com.cwc.app.iface.zyj.ContainerMgrIFaceZyj;
import com.cwc.app.key.ContainerHasSnTypeKey;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.util.StringUtil;

public class ContainerMgrZyj implements ContainerMgrIFaceZyj{
	
	static Logger log = Logger.getLogger("ACTION");
	private FloorContainerMgrZyj floorContainerMgrZyj;
	private FloorBoxTypeZr floorBoxTypeMgrZr;
	private FloorClpTypeMgrZr floorClpTypeMgrZr;
	private FloorProductMgr floorProductMgr;
	private FloorLPTypeMgrZJ floorLPTypeMgrZJ;
	private FloorProductStoreMgr productStoreMgr;
	
	/**
	 * 通过容器编号得到容器信息
	 * @param number 容器编号
	 * @return
	 * @throws Exception
	 */
	public DBRow findContainerByNumber(String number) throws Exception
	{
		try
		{
			return floorContainerMgrZyj.findContainerByNumber(number);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findContainerByNumber(String number)",log);
		}
	}
	
	
	/**
	 * 通过盒子类型ID，获取盒子相关信息
	 * @param type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findBoxTypeRelateInfoByTypeId(long type_id) throws Exception
	{
		try
		{
			return floorContainerMgrZyj.findBoxTypeRelateInfoByTypeId(type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findBoxTypeRelateInfoByTypeId(long type_id)",log);
		}
	}
	
	/**
	 * 通过CLP类型ID，获取CLP相关信息
	 * @param type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findClpTypeRelateInfoByTypeId(long type_id) throws Exception
	{
		try
		{
			return floorContainerMgrZyj.findClpTypeRelateInfoByTypeId(type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findClpTypeRelateInfoByTypeId(long type_id)",log);
		}
	}
	/**
	 * 通过内箱类型ID，获取内箱相关信息
	 * @param type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow  findInnerBoxTypeRelateInfoByTypeId(long type_id) throws Exception
	{
		try
		{
			return floorContainerMgrZyj.findInnerBoxTypeRelateInfoByTypeId(type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findInnerBoxTypeRelateInfoByTypeId(long type_id)",log);
		}
	}
	
	/**
	 * 通过TLP类型ID，获取TLP相关信息
	 * @param type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findTlpTypeRelateInfoByTypeId(long type_id) throws Exception
	{
		try
		{
			return floorContainerMgrZyj.findTlpTypeRelateInfoByTypeId(type_id);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"findTlpTypeRelateInfoByTypeId(long type_id)",log);
		}
	}
	
	public void exchangeClpToPsSort(HttpServletRequest request) throws Exception
	{
		try
		{
			long exchangeId		= StringUtil.getLong(request, "exchangeId");
			long exchangeIdTo	= StringUtil.getLong(request, "exchangeIdTo");
			int exchangeSort	= StringUtil.getInt(request, "exchangeSort");
			int exchangeSortTo	= StringUtil.getInt(request, "exchangeSortTo");
			
			DBRow updateRow = new DBRow();
			updateRow.add("ship_to_sort", exchangeSortTo);
			floorClpTypeMgrZr.updateCLpTypeShipTo(exchangeId, updateRow);
			
			DBRow updateToRow = new DBRow();
			updateToRow.add("ship_to_sort", exchangeSort);
			floorClpTypeMgrZr.updateCLpTypeShipTo(exchangeIdTo, updateToRow);
		}
		catch (Exception e) 
		{
			 throw new SystemException(e,"exchangeClpToPsSort(HttpServletRequest request)",log);
		}
	}
	
	
	/**
	 * 根据容器ID获取此容器下的子容器信息
	 */
	@Override
	public DBRow[] findContainerInnerInfoByContainerId(long containerId, PageCtrl pc) throws Exception {
		try
		{
			return floorContainerMgrZyj.findContainerInnerInfoByContainerId(containerId, pc);
		}
		catch (Exception e) 
		{
			 throw new SystemException(e,"findContainerInnerInfoByContainerId(long containerId, PageCtrl pc)",log);
		}
	}
	
	public DBRow findContainerSumByContainerId(long containerId) throws Exception{
		try{
			return this.floorContainerMgrZyj.findContainerSumByContainerId(containerId);
		}catch(Exception e){
			throw new SystemException(e,"findContainerSumByContainerId(long containerId, PageCtrl pc)",log);
		}
	}
	
	/**
	 * 批量添加容器
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] addBoxContainerBatch(HttpServletRequest request) throws Exception
	{
		try{
			long type_id  = StringUtil.getLong(request, "type_id");
			int container_type = StringUtil.getInt(request,"container_type");
			int is_has_sn = StringUtil.getInt(request, "is_has_sn");
			int print_count = StringUtil.getInt(request, "print_count");
			ContainerTypeKey containerTypeKey = new ContainerTypeKey();
			DBRow[] containerRows = new DBRow[print_count];
			for (int i = 0; i < print_count; i++) 
			{
				DBRow data = new DBRow();
				data.add("container_type", container_type);
				data.add("type_id", type_id );
				data.add("is_full", ContainerProductState.EMPTY);
				data.add("is_has_sn", is_has_sn);//张睿添加 	是否是SN
				long id = floorBoxTypeMgrZr.addBoxContainer(data);
				DBRow updateContainer = new DBRow();
				//String container = containerTypeKey.getContainerTypeKeyValue(container_type)+id;
				String container = id+"";
				updateContainer.add("container", container);
				data.add("container", container);
				data.add("con_id", id);
				DBRow ctp = floorClpTypeMgrZr.findContainerType(type_id);
				data.put("TYPEID", ctp!=null?ctp.getString("type_name"):"");
				floorBoxTypeMgrZr.updateBoxContainer(id, updateContainer);
				DBRow con_product = floorClpTypeMgrZr.findContainerProduct(id);
				if(con_product != null){
					DBRow productCode = floorProductMgr.getDetailProductByPcid(con_product.get("pc_id", 0l));
					data.put("TOTPDT", StringUtil.isBlank(con_product.getString("cp_quantity"))?"&nbsp;":con_product.getString("cp_quantity"));
					data.put("TOTPKG", StringUtil.isBlank(con_product.getString("cp_quantity"))?"&nbsp;":con_product.getString("cp_quantity"));
					data.put("LOT", StringUtil.isBlank(con_product.getString("lot_no"))?"&nbsp;":con_product.getString("lot_no"));
					data.put("PCODE", StringUtil.isBlank(productCode.getString("p_code"))?"NA":productCode.getString("p_code"));
				}else{
					data.put("TOTPDT", "&nbsp;");
					data.put("TOTPKG", "&nbsp;");
					data.put("LOT", "&nbsp;");
					data.put("PCODE", "NA");
				}
				data.put("CUSTOMER", "&nbsp;");
				data.put("TITLE", "&nbsp;");
				data.put("CONID", id);
				containerRows[i] = data;
				/* Commented out 库存计算用图数据库，生成容器时不扣库，不向图数据库写数据
				//向图数据库添加节点
				Map<String,Object> containerNode = new HashMap<String, Object>();
				containerNode.put("con_id",data.get("con_id",0l));
	 			containerNode.put("container_type",data.get("container_type", 0));
	 			containerNode.put("type_id",data.get("type_id", 0l));
	 			containerNode.put("container",data.getString("container"));
	 			containerNode.put("is_full",data.get("is_full", 0));
	 			containerNode.put("is_has_sn",data.get("is_has_sn", 0));
				productStoreMgr.addNode(0,NodeType.Container,containerNode);*/
			}
			return containerRows; 
 		}
		catch (Exception e)
 		{
			throw new SystemException(e, "addBoxContainerBatch", log);
 		}
 	}
	
	
	public void batchInsertContainerTypes() throws Exception
	{
		try
		{
			//添加container_type:内箱
			for (int i = 0; i < 1; i++) 
			{
				double container_length = (Math.random()+1)*200;
				double container_width  = (Math.random()+1)*100;
				double container_height = (Math.random()+1)*3;
				double container_weight = (Math.random()+1)*3 ;
				//容器类型
				container_length = container_width > container_length ? (container_width+100):container_length;
				
				long containerILPTypeId = 0;
//					this.insertContainerType(ContainerTypeKey.ILP, "ILP_基础容器类型"+i, container_length, 
//							container_width, container_height , container_weight,
//							(Math.random()+1)*300, (Math.random()+1)*50);//去掉ILP和BLP
				
				DBRow[] pcRows = floorContainerMgrZyj.findAllProducts();
				
				long pc_id = pcRows[new Random().nextInt(pcRows.length+1)].get("pc_id", 0L);
				int i_p_l = new Random().nextInt(5)+1;
				int i_p_w = new Random().nextInt(5)+1;
				int i_p_h = new Random().nextInt(5)+1;;
				
				i_p_l = i_p_w > i_p_l ? (i_p_w+1):i_p_l;
				int product_count = i_p_l * i_p_w * i_p_h;
				
//				//system.out.println("ilp:"+product_count+",||||,"+i_p_l +"*"+ i_p_w +"*"+ i_p_h);
				//内箱类型ID
				long innerBoxTypeId = 
					this.insertInnerBoxType(pc_id, i_p_l, i_p_w, i_p_h,
							container_length, container_width, container_height,
							container_weight, containerILPTypeId, product_count);
				
				int is_has_sn_ilp = new Random().nextInt(2)+1;
				
				//添加container，内箱容器
				long ilpContainerId = 0;
//					this.insertContainer("", innerBoxTypeId, ContainerTypeKey.ILP, 1, is_has_sn_ilp, 0L, "");//去掉ILP和BLP
				DBRow updateIlpContainer = new DBRow();
				updateIlpContainer.add("hardwareId", "ILP"+ilpContainerId);
				updateIlpContainer.add("container", "ILP"+ilpContainerId);
				floorContainerMgrZyj.updateContainer(updateIlpContainer, ilpContainerId);
				
				int b_p_l = new Random().nextInt(5)+1;
				int b_p_w = new Random().nextInt(5)+1;
				int b_p_h = new Random().nextInt(5)+1;
				
				b_p_l = b_p_w > b_p_l ? (b_p_w+1):b_p_l;
				
				int redirect_product = new Random().nextInt(2);//0,true,  1,false;
				int box_total = 0;
				int product_total = 0;
				
				
				long is_inner_box = 0L;
				if(0 == redirect_product)
				{
					box_total = b_p_l * b_p_w * b_p_h;
					product_total = box_total;
				}
				else
				{
					is_inner_box = innerBoxTypeId;
					box_total = b_p_l * b_p_w * b_p_h;
					product_total = box_total * product_count;
				}
				
				
				int box_type_sort =  floorBoxTypeMgrZr.countNumBy(pc_id)+1;
				
				double box_length = container_length*b_p_l;
				double box_width = container_width*b_p_w;
				
				box_length = box_width > box_length ? (box_width+100):box_length;
				
//				//system.out.println("Blp:"+product_total+","+box_total+","+redirect_product+",||||,"+b_p_l +"*"+ b_p_w +"*"+ b_p_h);
				//box type id
				long blpTypeId =
					this.insertBoxType(pc_id, b_p_l, b_p_w, b_p_h,
							is_inner_box, box_length, box_width, container_height*b_p_h,
							container_weight*5, 2, box_total, product_total, box_type_sort);
				
				int is_has_sn_blp = new Random().nextInt(2)+1;
				//添加container，BLP容器
				long blpContainerId = 0;
//					this.insertContainer("", blpTypeId, ContainerTypeKey.BLP, 1, is_has_sn_blp, 0L, "");//去掉ILP和BLP
				DBRow updateBlpContainer = new DBRow();
				updateBlpContainer.add("hardwareId", "BLP"+blpContainerId);
				updateBlpContainer.add("container", "BLP"+blpContainerId);
				floorContainerMgrZyj.updateContainer(updateBlpContainer, blpContainerId);
				
				
				DBRow[] storageCatalogs = floorContainerMgrZyj.findAllProductStoageCatalog();
				int stoageArrIndex = new Random().nextInt(storageCatalogs.length);
				long ps_id = storageCatalogs[stoageArrIndex].get("id", 0L);
				
				int box_sort = floorContainerMgrZyj.getMaxIndexBy(pc_id, ps_id);
				
				//box ps id
				long boxPsId = 
					this.insertBoxPs(pc_id, ps_id, blpTypeId, box_sort+1);
				
				int c_p_l = new Random().nextInt(5)+1;
				int c_p_w = new Random().nextInt(5)+1;
				int c_p_h = new Random().nextInt(5)+1;
				
				c_p_l = c_p_w > c_p_l ? (c_p_w+1):c_p_l;
				
				int clp_redire_pdt = new Random().nextInt(2);
				//TODO 商品和盒子数量还未算
				long is_box_cont = 0L;
				int sku_lp_total_box = 0;
				int sku_lp_total_piece = 0;
				if(0 == clp_redire_pdt)
				{
					sku_lp_total_box = c_p_l * c_p_w * c_p_h;
					sku_lp_total_piece = sku_lp_total_box;
				}
				else
				{
					is_box_cont = blpTypeId;
					sku_lp_total_box = c_p_l * c_p_w * c_p_h;
					sku_lp_total_piece = sku_lp_total_box * product_total;
				}
				
//				//system.out.println("clp:"+sku_lp_total_piece+","+sku_lp_total_box+","+clp_redire_pdt+",||||,"+c_p_l +"*"+ c_p_w +"*"+ c_p_h);
				
				
				DBRow sortRow=this.floorClpTypeMgrZr.findMaxSort(ps_id, pc_id, 0,0);
				int sort=sortRow.get("sort", 0)+1;
				
				long clpTypeId = 
					this.insertClpType(pc_id, 0L, ps_id, 1, is_box_cont,
							sku_lp_total_box, sku_lp_total_piece,
							c_p_l, c_p_w, c_p_h, sort);
				
				int is_has_sn_clp = new Random().nextInt(2)+1;
				//添加container，CLP容器
				long clpContainerId = 
					this.insertContainer("", clpTypeId, ContainerTypeKey.CLP, 1, is_has_sn_clp, 0L, "");
				DBRow updateClpContainer = new DBRow();
				updateClpContainer.add("hardwareId", "CLP"+clpContainerId);
				updateClpContainer.add("container", "CLP"+clpContainerId);
				floorContainerMgrZyj.updateContainer(updateClpContainer, clpContainerId);
				
				//建立容器与容器之间的关系
//				this.insertContainerLoading(ilpContainerId, ContainerTypeKey.ILP, innerBoxTypeId, blpContainerId, ContainerTypeKey.BLP, blpTypeId);
//				this.insertContainerLoading(blpContainerId, ContainerTypeKey.BLP, blpTypeId, clpContainerId, ContainerTypeKey.CLP, clpTypeId);//去掉ILP和BLP
				
				this.insertContainerLoadingChildList(ilpContainerId, ilpContainerId, blpContainerId, 0);
				this.insertContainerLoadingChildList(blpContainerId, blpContainerId, clpContainerId, 0);
				this.insertContainerLoadingChildList(blpContainerId, ilpContainerId, clpContainerId, 1);
				this.insertContainerLoadingChildList(clpContainerId, clpContainerId, 0L, 0);
				this.insertContainerLoadingChildList(clpContainerId, blpContainerId, 0L, 1);
				this.insertContainerLoadingChildList(clpContainerId, ilpContainerId, 0L, 2);
				
				//callContainerLoadingChildList
			} 
		}
		catch (Exception e)
 		{
			throw new SystemException(e, "batchInsertContainerDatas", log);
 		}
	}
	
	
	
	public long insertContainerType(int containerType, String typeName, 
			double length, double width, double height, double weight,
			double max_load, double max_height) throws Exception
	{
		DBRow addRow = new DBRow();
		addRow.add("type_name", typeName);
		addRow.add("length", length);
		addRow.add("width", width);
		addRow.add("height", height);
		addRow.add("weight", weight);
		addRow.add("max_load", max_load);
		addRow.add("max_height", max_height);
		addRow.add("container_type",containerType );
		
		return floorContainerMgrZyj.addContainerType(addRow);
	}
	
	public long insertInnerBoxType(long box_pc_id, int box_total_length, int box_total_width, int box_total_height,
			double box_length, double box_width, double box_height, double box_weight,
			long container_type_id, int box_total
	) throws Exception
	{
		DBRow data = new DBRow();
		data.add("ibt_pc_id", box_pc_id);
		data.add("ibt_total_length", box_total_length);
		data.add("ibt_total_width", box_total_width);
		data.add("ibt_total_height", box_total_height);
		data.add("ibt_total", box_total);	
		data.add("ibt_lp_type", container_type_id);
		data.add("ibt_length", box_length);
		data.add("ibt_width", box_width);
		data.add("ibt_height", box_height);
		data.add("ibt_weight", box_weight);

		return floorContainerMgrZyj.addInnerBoxType(data);
	}
	
	
	public long insertBoxType(long box_pc_id, int box_total_length, int box_total_width, int box_total_height,
			long bh_type, double box_length, double box_width, double box_height, double box_weight,
			long container_type_id, int box_total, int product_total, int box_type_sort) throws Exception
	{
		DBRow data = new DBRow();
		data.add("box_pc_id", box_pc_id);
		data.add("box_total_length", box_total_length);
		data.add("box_total_width", box_total_width);
		data.add("box_total_height", box_total_height);
		data.add("box_total", box_total);
		if(bh_type==0)
		{
			data.add("box_total_piece", box_total);
		}
		else
		{
			data.add("box_total_piece", product_total);
			data.add("box_inner_type", bh_type);
		}
		data.add("box_length", box_length);
		data.add("box_width", box_width);
		data.add("box_height", box_height);
		data.add("box_weight", box_weight);
		data.add("container_type_id", container_type_id);
		data.add("box_type_sort",box_type_sort);
		return floorContainerMgrZyj.addBoxType(data);
	}
	
	public long insertBoxPs(long box_pc_id, long to_ps_id, long box_type_id, int box_sort) throws Exception
	{
		DBRow row = new DBRow();
		row.add("box_pc_id", box_pc_id);
		row.add("to_ps_id", to_ps_id);
		row.add("box_type_id", box_type_id);
		row.add("box_sort", box_sort);
		return floorContainerMgrZyj.addBoxPs(row);
	}
	
	public long insertClpType(long sku_lp_pc_id, long sku_lp_title_id, long sku_lp_to_ps_id,
		long lp_type_id, long sku_lp_box_type, int sku_lp_total_box, int sku_lp_total_piece,
		int sku_lp_box_length, int sku_lp_box_width, int sku_lp_box_height, int sort) throws Exception
	{
		DBRow row = new DBRow();
		row.add("sku_lp_pc_id", sku_lp_pc_id);
		row.add("sku_lp_title_id", sku_lp_title_id);
		row.add("sku_lp_to_ps_id", sku_lp_to_ps_id);
		row.add("lp_type_id", lp_type_id);
		row.add("sku_lp_box_type", sku_lp_box_type);
	
		row.add("sku_lp_box_length", sku_lp_box_length);
		row.add("sku_lp_box_width", sku_lp_box_width);
		row.add("sku_lp_box_height", sku_lp_box_height);
		
		row.add("sku_lp_total_box", sku_lp_total_box);
		row.add("sku_lp_total_piece", sku_lp_total_piece);
		row.add("sku_lp_sort",sort);
		return floorContainerMgrZyj.addClpType(row);
	}
	
	public long insertContainerType(String type_name, double length, double width, double height, double weight, 
			int max_load, double max_height, int container_type) throws Exception
	{
		DBRow row = new DBRow();
		row.add("type_name", type_name);
		row.add("length", length);
		row.add("width", width);
		row.add("height", height);
		row.add("weight", weight);
		row.add("max_load", max_load);
		row.add("max_height", max_height);
		row.add("container_type", container_type);
		return floorContainerMgrZyj.addContainerType(row);
	}
	
	public long insertContainer(String hardwareId, long type_id, int container_type, int is_full,
			int is_has_sn, long title_id, String lot_number) throws Exception
	{
		DBRow row = new DBRow();
		row.add("hardwareId", hardwareId);
		row.add("type_id", type_id);
		row.add("container_type", container_type);
		row.add("is_full", is_full);
		row.add("is_has_sn", is_has_sn);
		row.add("title_id", title_id);
		row.add("lot_number", lot_number);
		return floorContainerMgrZyj.addContainer(row);
	}
	
	
	public long insertContainerLoading(long con_id, int container_type, long container_type_id, long parent_con_id, int parent_container_type, long parent_container_type_id) throws Exception
	{
		DBRow row = new DBRow();
		row.add("con_id", con_id);
		row.add("container_type", container_type);
		row.add("container_type_id", container_type_id);
		row.add("parent_con_id", parent_con_id);
		row.add("parent_container_type", parent_container_type);
		row.add("parent_container_type_id", parent_container_type_id);
		return floorContainerMgrZyj.addContainerLoading(row);
	}
	
	public long insertContainerLoadingChildList(long search_root_con_id, long cont_id, long parent_con_id, int levv) throws Exception
	{
		DBRow row = new DBRow();
		row.add("search_root_con_id", search_root_con_id);
		row.add("cont_id", cont_id);
		row.add("parent_con_id", parent_con_id);
		row.add("levv", levv);
		return floorContainerMgrZyj.addContainerLoadingChildList(row);
	}
	
	/**
	 * 生成容器类型及容器
	 * @param typeCount		生成容器类型次数
	 * @param containerCount	生成容器次数
	 * @throws Exception
	 */
	public void batchInsertContainerTypesAndContainers(long pc_id_gt, int typeCount, int containerCount, int tlpContainerCount, int containerRelateCount) throws Exception
	{
		try
		{
			List<Long> ilpTypeIdList = new ArrayList<Long>();
			List<HashMap<Long, Long>> blpTypeIdList = new ArrayList<HashMap<Long, Long>>();
			List<HashMap<Long, Long>> clpTypeIdList = new ArrayList<HashMap<Long, Long>>();
			
			DBRow[] pcRows = new DBRow[0];
			if(0 != pc_id_gt)
			{
				pcRows = floorContainerMgrZyj.findProductsByIdLtSome(pc_id_gt);
			}
			else
			{
				pcRows = floorContainerMgrZyj.findAllProducts();
			}
			//添加container_type:内箱
			for (int i = 0; i < typeCount; i++) 
			{
				int pcIdIndex = new Random().nextInt(pcRows.length);
				long pc_id = pcRows[pcIdIndex].get("pc_id", 0L);
				
				DBRow productRow = floorProductMgr.getDetailProductByPcid(pc_id);
				while(null == productRow)
				{
					pc_id = pcRows[new Random().nextInt(pcRows.length+1)].get("pc_id", 0L);
				}
				float product_length	= productRow.get("length", 0F);
				float product_width		= productRow.get("width", 0F);
				float product_heigth	= productRow.get("heigth", 0F);
				
				double container_length = (Math.random()+1)*200;
				double container_width  = (Math.random()+1)*100;
				double container_height = (Math.random()+1)*3;
				double container_weight = (Math.random()+1)*3 ;
				//容器类型
				container_length = container_width > container_length ? (container_width+100):container_length;
				
				DBRow[] containerILPTypes = floorContainerMgrZyj.findContainersByType(0);//去掉ILP和BLP,ContainerTypeKey.ILP
				long containerILPTypeId = 0L;
				if(containerILPTypes.length > 0)
				{
					containerILPTypeId = containerILPTypes[0].get("type_id", 0L);
				}
				else
				{
					containerILPTypeId = 0;
//						this.insertContainerType(ContainerTypeKey.ILP, "ILP_基础容器类型"+i, container_length, 
//								container_width, container_height , container_weight,
//								(Math.random()+1)*300, (Math.random()+1)*50);//去掉ILP和BLP
				}
				
				int i_p_l = new Random().nextInt(5)+1;
				int i_p_w = new Random().nextInt(5)+1;
				int i_p_h = new Random().nextInt(5)+1;;
				
				i_p_l = i_p_w > i_p_l ? (i_p_w+1):i_p_l;
				int product_count = i_p_l * i_p_w * i_p_h;
				
				double innerBoxLength = product_length*i_p_l;
				double innerBoxWidth  = product_width*i_p_w;
				double innerBoxHeight = product_heigth*i_p_h;
//				//system.out.println("ilp:"+product_count+",||||,"+i_p_l +"*"+ i_p_w +"*"+ i_p_h);
				//内箱类型ID
				long innerBoxTypeId = 
					this.insertInnerBoxType(pc_id, i_p_l, i_p_w, i_p_h,
							innerBoxLength, innerBoxWidth, innerBoxHeight,
							container_weight, containerILPTypeId, product_count);
				
				ilpTypeIdList.add(innerBoxTypeId);
				
				int b_p_l = new Random().nextInt(5)+1;
				int b_p_w = new Random().nextInt(5)+1;
				int b_p_h = new Random().nextInt(5)+1;
				
				b_p_l = b_p_w > b_p_l ? (b_p_w+1):b_p_l;
				
				int redirect_product = new Random().nextInt(2);//0,true,  1,false;
				int box_total = 0;
				int product_total = 0;
				
				double boxLength = 0d;
				double boxWidth  = 0d;
				double boxHeight = 0d;
				
				long is_inner_box = 0L;
				if(0 == redirect_product)
				{
					box_total = b_p_l * b_p_w * b_p_h;
					product_total = box_total;
					boxLength = product_length * b_p_l;
					boxWidth  = product_width * b_p_w;
					boxHeight = product_heigth * b_p_h;
				}
				else
				{
					is_inner_box = innerBoxTypeId;
					box_total = b_p_l * b_p_w * b_p_h;
					product_total = box_total * product_count;
					boxLength = innerBoxLength * b_p_l;
					boxWidth  = innerBoxWidth * b_p_w;
					boxHeight = innerBoxHeight * b_p_h;
				}
				
				int box_type_sort =  floorBoxTypeMgrZr.countNumBy(pc_id)+1;
				
//				//system.out.println("Blp:"+product_total+","+box_total+","+redirect_product+",||||,"+b_p_l +"*"+ b_p_w +"*"+ b_p_h);
				
				DBRow[] containerBLPTypes = floorContainerMgrZyj.findContainersByType(0);//去掉ILP和BLP，ContainerTypeKey.BLP
				long containerBLPTypeId = 0L;
				if(containerBLPTypes.length > 0)
				{
					containerBLPTypeId = containerBLPTypes[0].get("type_id", 0L);
				}
				else
				{
					containerBLPTypeId = 0;
//						this.insertContainerType(ContainerTypeKey.BLP, "BLP_基础容器类型"+i, container_length, 
//								container_width, container_height , container_weight,
//								(Math.random()+1)*300, (Math.random()+1)*50);//去掉ILP和BLP
				}
				
				//box type id
				long blpTypeId =
					this.insertBoxType(pc_id, b_p_l, b_p_w, b_p_h,
							is_inner_box, boxLength, boxWidth, boxHeight,
							container_weight*5, containerBLPTypeId, box_total, product_total, box_type_sort);
				
				HashMap<Long, Long> boxTypeInfo = new HashMap<Long, Long>();
				boxTypeInfo.put(blpTypeId, is_inner_box);
				blpTypeIdList.add(boxTypeInfo);
				
				DBRow[] storageCatalogs = floorContainerMgrZyj.findAllProductStoageCatalog();
				int stoageArrIndex = new Random().nextInt(storageCatalogs.length);
				long ps_id = storageCatalogs[stoageArrIndex].get("id", 0L);
				
				int box_sort = floorContainerMgrZyj.getMaxIndexBy(pc_id, ps_id);
				
				//box ps id
				this.insertBoxPs(pc_id, ps_id, blpTypeId, box_sort+1);
				
				int c_p_l = new Random().nextInt(5)+1;
				int c_p_w = new Random().nextInt(5)+1;
				int c_p_h = new Random().nextInt(5)+1;
				
				c_p_l = c_p_w > c_p_l ? (c_p_w+1):c_p_l;
				
				int clp_redire_pdt = new Random().nextInt(2);
				
				double clpLength = 0d;
				double clpWidth  = 0d;
				double clpHeight = 0d;
				
				long is_box_cont = 0L;
				int sku_lp_total_box = 0;
				int sku_lp_total_piece = 0;
				if(0 == clp_redire_pdt)
				{
					sku_lp_total_box = c_p_l * c_p_w * c_p_h;
					sku_lp_total_piece = sku_lp_total_box;
					clpLength = product_length * c_p_l;
					clpWidth  = product_width * c_p_w;
					clpHeight = product_heigth * c_p_h;
				}
				else
				{
					is_box_cont = blpTypeId;
					sku_lp_total_box = c_p_l * c_p_w * c_p_h;
					sku_lp_total_piece = sku_lp_total_box * product_total;
					clpLength = boxLength * c_p_l;
					clpWidth  = boxWidth * c_p_w;
					clpHeight = boxHeight * c_p_h;
				}
				
//				//system.out.println("clp:"+sku_lp_total_piece+","+sku_lp_total_box+","+clp_redire_pdt+",||||,"+c_p_l +"*"+ c_p_w +"*"+ c_p_h);
				
				DBRow sortRow=this.floorClpTypeMgrZr.findMaxSort(ps_id, pc_id, 0,0);
				int sort=sortRow.get("sort", 0)+1;
				
				
				DBRow[] containerCLPTypes = floorContainerMgrZyj.findContainersByType(ContainerTypeKey.CLP);
				long containerCLPTypeId = 0L;
				if(containerCLPTypes.length > 0)
				{
					containerCLPTypeId = containerCLPTypes[0].get("type_id", 0L);
				}
				else
				{
					containerCLPTypeId = 
						this.insertContainerType(ContainerTypeKey.CLP, "CLP_基础容器类型"+i, container_length, 
								container_width, container_height , container_weight,
								(Math.random()+1)*300, (Math.random()+1)*50);
				}
				
				long clpTypeId = 
					this.insertClpType(pc_id, 0L, ps_id, containerCLPTypeId, is_box_cont,
							sku_lp_total_box, sku_lp_total_piece,
							c_p_l, c_p_w, c_p_h, sort);
				
				HashMap<Long, Long> clpTypeInfo = new HashMap<Long, Long>();
				clpTypeInfo.put(clpTypeId, is_box_cont);
				clpTypeIdList.add(clpTypeInfo);
			}
			
			for (int i = 0; i < containerCount; i++) 
			{
				int typeIdSize = new Random().nextInt(ilpTypeIdList.size());
				
				//添加container，内箱容器
				int is_has_sn_ilp = new Random().nextInt(2)+1;
				
				long ilpContainerId = 0;
//					this.insertContainer("", ilpTypeIdList.get(typeIdSize), ContainerTypeKey.ILP, 1, is_has_sn_ilp, 0L, "");//去掉ILP和BLP
				DBRow updateIlpContainer = new DBRow();
				updateIlpContainer.add("hardwareId", "ILP"+ilpContainerId);
				updateIlpContainer.add("container", "ILP"+ilpContainerId);
				floorContainerMgrZyj.updateContainer(updateIlpContainer, ilpContainerId);
				
				
				
				//添加container，BLP容器
				int is_has_sn_blp = new Random().nextInt(2)+1;
				long blpContainerId = 0;
//					this.insertContainer("", findMapFirstKey(blpTypeIdList.get(typeIdSize)), ContainerTypeKey.BLP, 1, is_has_sn_blp, 0L, "");//去掉ILP和BLP
				DBRow updateBlpContainer = new DBRow();
				updateBlpContainer.add("hardwareId", "BLP"+blpContainerId);
				updateBlpContainer.add("container", "BLP"+blpContainerId);
				floorContainerMgrZyj.updateContainer(updateBlpContainer, blpContainerId);
				
				
				
				//添加container，CLP容器
				int is_has_sn_clp = new Random().nextInt(2)+1;
				long clpContainerId = 
					this.insertContainer("", findMapFirstKey(clpTypeIdList.get(typeIdSize)), ContainerTypeKey.CLP, 1, is_has_sn_clp, 0L, "");
				DBRow updateClpContainer = new DBRow();
				updateClpContainer.add("hardwareId", "CLP"+clpContainerId);
				updateClpContainer.add("container", "CLP"+clpContainerId);
				floorContainerMgrZyj.updateContainer(updateClpContainer, clpContainerId);
				
				
				
				//建立容器与容器之间的关系
				if(0 != findMapFirstValue(blpTypeIdList.get(typeIdSize)))
				{
//					this.insertContainerLoading(ilpContainerId, ContainerTypeKey.ILP, ilpTypeIdList.get(typeIdSize), blpContainerId, ContainerTypeKey.BLP, findMapFirstKey(blpTypeIdList.get(typeIdSize)));//去掉ILP和BLP
					
					this.insertContainerLoadingChildList(ilpContainerId, ilpContainerId, blpContainerId, 0);
					this.insertContainerLoadingChildList(blpContainerId, ilpContainerId, blpContainerId, 1);
					
				}
				else
				{
					this.insertContainerLoadingChildList(ilpContainerId, ilpContainerId, 0, 0);
				}
				if(0 != findMapFirstValue(clpTypeIdList.get(typeIdSize)))
				{
//					this.insertContainerLoading(blpContainerId, ContainerTypeKey.BLP, findMapFirstKey(blpTypeIdList.get(typeIdSize)), clpContainerId, ContainerTypeKey.CLP, findMapFirstKey(clpTypeIdList.get(typeIdSize)));
					
					this.insertContainerLoadingChildList(blpContainerId, blpContainerId, clpContainerId, 0);
					this.insertContainerLoadingChildList(clpContainerId, blpContainerId, clpContainerId, 1);
					if(0 != findMapFirstValue(blpTypeIdList.get(typeIdSize)))
					{
						this.insertContainerLoadingChildList(clpContainerId, ilpContainerId, blpContainerId, 2);
					}
				}
				else
				{
					this.insertContainerLoadingChildList(blpContainerId, blpContainerId, 0, 0);
				}
				
				this.insertContainerLoadingChildList(clpContainerId, clpContainerId, 0L, 0);
				
			}
			this.batchTlpContainersByIdLtSome(pc_id_gt, tlpContainerCount, containerRelateCount);
		}
		catch (Exception e)
 		{
			throw new SystemException(e, "batchInsertContainerTypesAndContainers", log);
 		}
	}
	
	
	private long findMapFirstKey(HashMap<Long, Long> map)
	{
		long reLong = 0L;
		if(null != map && map.size() > 0)
		{
			Set<Long> set = map.keySet();
			for (Iterator iterator = set.iterator(); iterator.hasNext();) {
				reLong = (Long) iterator.next();
			}
		}
		return reLong;
	}
	
	private long findMapFirstValue(HashMap<Long, Long> map)
	{
		long reLong = 0L;
		if(null != map && map.size() > 0)
		{
			Set<Long> set = map.keySet();
			for (Iterator iterator = set.iterator(); iterator.hasNext();) {
				reLong = map.get((Long) iterator.next());
			}
		}
		return reLong;
	}
	
	
	public void deleteContainerTypeAndExample() throws Exception
	{
		try
		{
			floorContainerMgrZyj.deleteAndContainerAndExample();
		}
		catch (Exception e)
 		{
			throw new SystemException(e, "deleteContainerTypeAndExample", log);
 		}
	}
	
	public DBRow[] findAllContainer(PageCtrl pc) throws Exception
	{
		try
		{
			return floorContainerMgrZyj.findAllContainer(pc);
		}
		catch (Exception e)
 		{
			throw new SystemException(e, "findAllContainer(PageCtrl pc)", log);
 		}
	}
	
	public DBRow[] findContainerByTypeContainerTypeName(String key, long type, int container_type, PageCtrl pc) throws Exception
	{
		try
		{
			return floorContainerMgrZyj.findContainerByTypeContainerTypeName(key, type, container_type, pc);
		}
		catch (Exception e)
 		{
			throw new SystemException(e, "findAllContainer(PageCtrl pc)", log);
 		}
	}
	
	public String findILPContainerWithPName(long container_id) throws Exception
	{
		try
		{
			String reStr = "";
			DBRow row = floorContainerMgrZyj.findILPContainerWithPName(container_id);
			if(null != row)
			{
				reStr += row.getString("p_name")+"_"+row.getString("stack_length_qty")+"*"+row.getString("stack_width_qty")
						+"*"+ row.getString("stack_height_qty");
			}
			return reStr;
		}
		catch (Exception e)
 		{
			throw new SystemException(e, "findILPContainerWithPName(long container_id)", log);
 		}
	}
	
	public void batchTlpContainersByIdLtSome(long pc_id_gt, int tlpContainerCount, int containerRelateCount) throws Exception
	{
		try
		{
			List<Long> ilpTypeIdList = new ArrayList<Long>();
			List<HashMap<Long, Long>> blpTypeIdList = new ArrayList<HashMap<Long, Long>>();
			
			DBRow[] pcRows = new DBRow[0];
			if(0 != pc_id_gt)
			{
				pcRows = floorContainerMgrZyj.findProductsByIdLtSome(pc_id_gt);
			}
			else
			{
				pcRows = floorContainerMgrZyj.findAllProducts();
			}
			//添加container_type:内箱
			for (int i = 0; i < tlpContainerCount; i++) 
			{
				int pcIdIndex = new Random().nextInt(pcRows.length);
				long pc_id = pcRows[pcIdIndex].get("pc_id", 0L);
				
				DBRow productRow = floorProductMgr.getDetailProductByPcid(pc_id);
				while(null == productRow)
				{
					pc_id = pcRows[new Random().nextInt(pcRows.length+1)].get("pc_id", 0L);
				}
				float product_length	= productRow.get("length", 0F);
				float product_width		= productRow.get("width", 0F);
				float product_heigth	= productRow.get("heigth", 0F);
				
				double container_length = (Math.random()+1)*200;
				double container_width  = (Math.random()+1)*100;
				double container_height = (Math.random()+1)*3;
				double container_weight = (Math.random()+1)*3 ;
				//容器类型
				container_length = container_width > container_length ? (container_width+100):container_length;
				
				DBRow[] containerILPTypes = floorContainerMgrZyj.findContainersByType(0);//去掉ILP和BLP,ContainerTypeKey.ILP
				long containerILPTypeId = 0L;
				if(containerILPTypes.length > 0)
				{
					containerILPTypeId = containerILPTypes[0].get("type_id", 0L);
				}
				else
				{
					containerILPTypeId = 0;
//						this.insertContainerType(ContainerTypeKey.ILP, "ILP_基础容器类型"+i, container_length, 
//								container_width, container_height , container_weight,
//								(Math.random()+1)*300, (Math.random()+1)*50);//去掉ILP和BLP
				}
				
				int i_p_l = new Random().nextInt(5)+1;
				int i_p_w = new Random().nextInt(5)+1;
				int i_p_h = new Random().nextInt(5)+1;;
				
				i_p_l = i_p_w > i_p_l ? (i_p_w+1):i_p_l;
				int product_count = i_p_l * i_p_w * i_p_h;
				
				double innerBoxLength = product_length*i_p_l;
				double innerBoxWidth  = product_width*i_p_w;
				double innerBoxHeight = product_heigth*i_p_h;
//				//system.out.println("ilp:"+product_count+",||||,"+i_p_l +"*"+ i_p_w +"*"+ i_p_h);
				//内箱类型ID
				long innerBoxTypeId = 
					this.insertInnerBoxType(pc_id, i_p_l, i_p_w, i_p_h,
							innerBoxLength, innerBoxWidth, innerBoxHeight,
							container_weight, containerILPTypeId, product_count);
				
				ilpTypeIdList.add(innerBoxTypeId);
				
				int b_p_l = new Random().nextInt(5)+1;
				int b_p_w = new Random().nextInt(5)+1;
				int b_p_h = new Random().nextInt(5)+1;
				
				b_p_l = b_p_w > b_p_l ? (b_p_w+1):b_p_l;
				
				int redirect_product = new Random().nextInt(2);//0,true,  1,false;
				int box_total = 0;
				int product_total = 0;
				
				double boxLength = 0d;
				double boxWidth  = 0d;
				double boxHeight = 0d;
				
				long is_inner_box = 0L;
				if(0 == redirect_product)
				{
					box_total = b_p_l * b_p_w * b_p_h;
					product_total = box_total;
					boxLength = product_length * b_p_l;
					boxWidth  = product_width * b_p_w;
					boxHeight = product_heigth * b_p_h;
				}
				else
				{
					is_inner_box = innerBoxTypeId;
					box_total = b_p_l * b_p_w * b_p_h;
					product_total = box_total * product_count;
					boxLength = innerBoxLength * b_p_l;
					boxWidth  = innerBoxWidth * b_p_w;
					boxHeight = innerBoxHeight * b_p_h;
				}
				
				int box_type_sort =  floorBoxTypeMgrZr.countNumBy(pc_id)+1;
				
//				//system.out.println("Blp:"+product_total+","+box_total+","+redirect_product+",||||,"+b_p_l +"*"+ b_p_w +"*"+ b_p_h);
				
				DBRow[] containerBLPTypes = floorContainerMgrZyj.findContainersByType(0);//去掉ILP和BLP,ContainerTypeKey.BLP
				long containerBLPTypeId = 0L;
				if(containerBLPTypes.length > 0)
				{
					containerBLPTypeId = containerBLPTypes[0].get("type_id", 0L);
				}
				else
				{
					containerBLPTypeId = 0;
//						this.insertContainerType(ContainerTypeKey.BLP, "BLP_基础容器类型"+i, container_length, 
//								container_width, container_height , container_weight,
//								(Math.random()+1)*300, (Math.random()+1)*50);//去掉ILP和BLP
				}
				
				//box type id
				long blpTypeId =
					this.insertBoxType(pc_id, b_p_l, b_p_w, b_p_h,
							is_inner_box, boxLength, boxWidth, boxHeight,
							container_weight*5, containerBLPTypeId, box_total, product_total, box_type_sort);
				
				HashMap<Long, Long> boxTypeInfo = new HashMap<Long, Long>();
				boxTypeInfo.put(blpTypeId, is_inner_box);
				blpTypeIdList.add(boxTypeInfo);
				
				DBRow[] storageCatalogs = floorContainerMgrZyj.findAllProductStoageCatalog();
				int stoageArrIndex = new Random().nextInt(storageCatalogs.length);
				long ps_id = storageCatalogs[stoageArrIndex].get("id", 0L);
				
				int box_sort = floorContainerMgrZyj.getMaxIndexBy(pc_id, ps_id);
				
				//box ps id
				this.insertBoxPs(pc_id, ps_id, blpTypeId, box_sort+1);
				
			}
			
			for (int i = 0; i < containerRelateCount; i++) 
			{
				int typeIdSize = new Random().nextInt(ilpTypeIdList.size());
				
				//添加container，内箱容器
				int is_has_sn_ilp = new Random().nextInt(2)+1;
				
				long ilpContainerId = 0;
//					this.insertContainer("", ilpTypeIdList.get(typeIdSize), ContainerTypeKey.ILP, 1, is_has_sn_ilp, 0L, "");//去掉ILP和BLP
				DBRow updateIlpContainer = new DBRow();
				updateIlpContainer.add("hardwareId", "ILP"+ilpContainerId);
				updateIlpContainer.add("container", "ILP"+ilpContainerId);
				floorContainerMgrZyj.updateContainer(updateIlpContainer, ilpContainerId);
				
				
				
				//添加container，BLP容器
				int is_has_sn_blp = new Random().nextInt(2)+1;
				long blpContainerId = 0;
//					this.insertContainer("", findMapFirstKey(blpTypeIdList.get(typeIdSize)), ContainerTypeKey.BLP, 1, is_has_sn_blp, 0L, "");//去掉ILP和BLP
				DBRow updateBlpContainer = new DBRow();
				updateBlpContainer.add("hardwareId", "BLP"+blpContainerId);
				updateBlpContainer.add("container", "BLP"+blpContainerId);
				floorContainerMgrZyj.updateContainer(updateBlpContainer, blpContainerId);
				
				
				//建立容器与容器之间的关系
				if(0 != findMapFirstValue(blpTypeIdList.get(typeIdSize)))
				{
//					this.insertContainerLoading(ilpContainerId, ContainerTypeKey.ILP, ilpTypeIdList.get(typeIdSize), blpContainerId, ContainerTypeKey.BLP, findMapFirstKey(blpTypeIdList.get(typeIdSize)));//去掉ILP和BLP
					
					this.insertContainerLoadingChildList(ilpContainerId, ilpContainerId, blpContainerId, 0);
					this.insertContainerLoadingChildList(blpContainerId, ilpContainerId, blpContainerId, 1);
				}
				else
				{
					this.insertContainerLoadingChildList(ilpContainerId, ilpContainerId, 0, 0);
				}
				
				
				double container_length = (Math.random()+1)*5000;
				double container_width  = (Math.random()+1)*3000;
				double container_height = new Random().nextInt(5)+1d;
				double container_load_weight = (Math.random()+1)*1000 ;
				//容器类型
				container_length = container_width > container_length ? (container_width+100):container_length;
				long tlpContainerTypeId = this.insertContainerType(ContainerTypeKey.TLP, "TLP_基础容器类型"+i, container_length, container_width, container_height, 1, container_load_weight, container_height);
				long tlpContainerId		= this.insertContainer("", tlpContainerTypeId, ContainerTypeKey.TLP, 1, 2, 0L, "");
				DBRow updateContainerRow = new DBRow();
				updateContainerRow.add("hardwareId", "TLP"+tlpContainerId);
				updateContainerRow.add("container", "TLP"+tlpContainerId);
				floorContainerMgrZyj.updateContainer(updateContainerRow, tlpContainerId);
				
				
				this.insertContainerLoadingChildList(blpContainerId, blpContainerId, tlpContainerId, 0);
				
//				this.insertContainerLoading(blpContainerId, ContainerTypeKey.BLP, findMapFirstKey(blpTypeIdList.get(typeIdSize)),
//						tlpContainerId, ContainerTypeKey.TLP, tlpContainerTypeId);//去掉ILP和BLP
				
				//创建TLP与BLP，ILP的关系
				this.insertContainerLoadingChildList(tlpContainerId, tlpContainerId, 0L, 0);
				this.insertContainerLoadingChildList(tlpContainerId, blpContainerId, tlpContainerId, 1);
				
				if(0 != findMapFirstValue(blpTypeIdList.get(typeIdSize)))
				{
					this.insertContainerLoadingChildList(tlpContainerId, ilpContainerId, blpContainerId, 2);
				}
				
			}
			
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "batchTlpContainersByIdLtSome(long container_id)", log);
		}
	}
	
	/**
	 * 通过商品ID和仓库ID，获取顺序为1的BLP类型
	 * @param pc_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findBlpTypeFirstForShipTo(long pc_id, long ps_id) throws Exception
	{
		try
		{
			DBRow[] rows = floorLPTypeMgrZJ.getBLPTypeForSkuToShip(pc_id, ps_id);
			DBRow row = null;
			if(1 == rows.length)
			{
				row = rows[0];
			}
			return row;
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "findBlpTypeFirstForShipTo(long container_id)", log);
		}
	}
	
	/**
	 * 通过商品ID和仓库ID，获取顺序为1的BLP类型
	 * @param pc_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findClpTypeFirstForShipTo(long pc_id, long title_id, long ps_id) throws Exception
	{
		try
		{
			DBRow[] rows = floorContainerMgrZyj.getCLPTypeForSkuToShip(pc_id, title_id, ps_id);
			DBRow row = null;
			if(1 == rows.length)
			{
				row = rows[0];
			}
			return row;
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "findClpTypeFirstForShipTo", log);
		}
	}
	
	public DBRow[] findIlpTypesByPcid(long pcid, int begin, int length)throws Exception
	{
		try
		{
			return floorContainerMgrZyj.findIlpTypesByPcid(pcid, begin, length);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "findIlpTypesByPcid", log);
		}
	}
	
	public DBRow[] findBoxTypesByPcId(long pcid, int begin, int length) throws Exception
	{
		try
		{
			return floorContainerMgrZyj.findBoxTypesByPcId(pcid, begin, length);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "findBoxTypesByPcId", log);
		}
	}
	
	//查询ilp根据商品
	public DBRow[] selectIlp(long productId, int length)throws Exception
	{
		try
		{
			return floorContainerMgrZyj.selectIlp(productId,  length);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "selectIlp", log);
		}
	}
	
	public DBRow[] getBoxSkuByPcId(long pc_id, long ship_to) throws Exception
	{
		try
		{
			return floorContainerMgrZyj.getBoxSkuByPcId(pc_id, ship_to);
		}
		catch (Exception e) 
		{
			throw new SystemException(e, "selectIlp", log);
		}
	}
	
	public void setFloorContainerMgrZyj(FloorContainerMgrZyj floorContainerMgrZyj) {
		this.floorContainerMgrZyj = floorContainerMgrZyj;
	}

	public void setFloorBoxTypeMgrZr(FloorBoxTypeZr floorBoxTypeMgrZr) {
		this.floorBoxTypeMgrZr = floorBoxTypeMgrZr;
	}
	
	public void setFloorClpTypeMgrZr(FloorClpTypeMgrZr floorClpTypeMgrZr) {
		this.floorClpTypeMgrZr = floorClpTypeMgrZr;
	}


	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}


	public void setFloorLPTypeMgrZJ(FloorLPTypeMgrZJ floorLPTypeMgrZJ) {
		this.floorLPTypeMgrZJ = floorLPTypeMgrZJ;
	}


	public void setProductStoreMgr(FloorProductStoreMgr productStoreMgr) {
		this.productStoreMgr = productStoreMgr;
	}

}
