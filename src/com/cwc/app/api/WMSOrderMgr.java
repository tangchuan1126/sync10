package com.cwc.app.api;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.cwc.app.floor.api.FloorCustomerIdMgr;
import com.cwc.app.floor.api.FloorWMSOrderSyncMgr;
import com.cwc.app.floor.api.FloorWMSOrdersMgr;
import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.app.iface.WMSOrderMgrIFace;
import com.cwc.app.key.WmsOrderStatusKey;
import com.cwc.app.key.WmsOrderUpdateTypeKey;
//import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.vvme.wms.Receipt;
import com.vvme.wms.ReceiptLine;

public class WMSOrderMgr implements WMSOrderMgrIFace {
	
	//private FloorProductMgr floorProductMgr;
	
	private TransactionTemplate sqlServerTransactionTemplate;
	
	private FloorWMSOrderSyncMgr floorWMSOrderSyncMgr;
	
	private FloorShipToMgrZJ floorShipToMgrZJ;
	
	private FloorCustomerIdMgr floorCustomerIdMgr;
	
	private FloorWMSOrdersMgr floorWMSOrdersMgr;
	
	private FloorB2BOrderMgrZyj floorB2BOrderMgrZyj;
	
	
	
	public void setFloorWMSOrdersMgr(FloorWMSOrdersMgr floorWMSOrdersMgr) {
		this.floorWMSOrdersMgr = floorWMSOrdersMgr;
	}

	public void setFloorWMSOrderSyncMgr(FloorWMSOrderSyncMgr floorWMSOrderSyncMgr) {
		this.floorWMSOrderSyncMgr = floorWMSOrderSyncMgr;
	}

	public void setFloorCustomerIdMgr(FloorCustomerIdMgr floorCustomerIdMgr) {
		this.floorCustomerIdMgr = floorCustomerIdMgr;
	}

	public void setFloorShipToMgrZJ(FloorShipToMgrZJ floorShipToMgrZJ) {
		this.floorShipToMgrZJ = floorShipToMgrZJ;
	}
	
	public void setSqlServerTransactionTemplate(TransactionTemplate sqlServerTransactionTemplate) {
		this.sqlServerTransactionTemplate = sqlServerTransactionTemplate;
	}

	public void setFloorB2BOrderMgrZyj(FloorB2BOrderMgrZyj floorB2BOrderMgrZyj) {
		this.floorB2BOrderMgrZyj = floorB2BOrderMgrZyj;
	}
	/**
	 * 回写WMS:更新状态，更新Load,Appointment信息
	 * 
	 * @author Liang Jie
	 * @param b2bOid
	 * @param updateType
	 * @throws Exception
	 */
	@Deprecated
	public void update(long b2bOid, int updateType) throws Exception{
		try{
			DBRow b2bOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2bOid);
			update(b2bOrderRow, updateType);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void update(long b2bOid, int updateType, DBRow data) throws Exception{
		
		try{
			DBRow b2bOrderRow = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2bOid);
			if (data.containsKey("carriers")) {
				b2bOrderRow.put("carriers", data.get("carriers"));
			}
			if (data.containsKey("pickup_appointment")) {
				b2bOrderRow.put("pickup_appointment", data.get("pickup_appointment"));
			}
			if (data.containsKey("load_no")) {
				b2bOrderRow.put("load_no", data.get("load_no"));
			}
			update(b2bOrderRow, updateType);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	/**
	 * 取消
	 * 
	 * @param companyId
	 * @param customer_id
	 * @param orderNo
	 * @throws Exception
	 */
	public void cancel(String companyId, long customer_id, String orderNo, long ps_id) throws Exception{
		if (isTF(customer_id)) {
			DBRow updateRow = new DBRow();
			String company_Id =  getCompanyID(companyId, customer_id);
			updateRow.add("CompanyID",company_Id);
			updateRow.add("OrderNo",orderNo);
			updateRow.add("STATUS",WmsOrderStatusKey.CLOSED);
			updateRow.add("ProNo","Cancel");
			updateRow.add("send_psid", ps_id);
			//update Orders set STATUS=? , CANCELEDDATE=? where CompanyID='W17' and OrderNo='60671'
			
			floorWMSOrderSyncMgr.updateOrder(updateRow, WmsOrderUpdateTypeKey.CANCEL);
			floorWMSOrderSyncMgr.cancelOrder(company_Id, orderNo);
		}
	}
	/**
	 * 删除
	 * @param companyId
	 * @param customer_id
	 * @param orderNo
	 * @param ps_id
	 * @throws Exception
	 */
	public void delete(String companyId, long customer_id, String orderNo, long ps_id) throws Exception{
		if (isTF(customer_id)) {
			String company_Id =  getCompanyID(companyId, customer_id);
			floorWMSOrderSyncMgr.deleteOrder(company_Id, orderNo);
		}
	}
	
	@Deprecated
	public void update(final DBRow order, int updateType) throws Exception {
		String companyId =  getCompanyID(order.getString("company_id", ""),order.get("customer_id", 0L));
		String orderNo = order.get("b2b_order_number","");
		if (!"".equals(orderNo)) {
			try {			
				DBRow updateRow = new DBRow();
				//同方的order可以close
				if ( isTF(order.get("customer_id",0L)) ) {
					if(updateType == WmsOrderUpdateTypeKey.CANCEL){
						updateRow.add("CompanyID",companyId);
						updateRow.add("OrderNo",orderNo);
						updateRow.add("Status",WmsOrderStatusKey.CLOSED);
						updateRow.add("ProNo","Cancel");
						
						floorWMSOrderSyncMgr.updateOrder(updateRow,updateType);
						floorWMSOrderSyncMgr.cancelOrder(companyId, orderNo);
					} else if(updateType == WmsOrderUpdateTypeKey.LOADAPPT) {
						updateRow.add("CompanyID",companyId);
						updateRow.add("OrderNo",orderNo);
						updateRow.add("LoadNo", order.get("load_no",""));
						updateRow.add("CarrierID", order.get("carriers",""));
						if (!"".equals(order.get("pickup_appointment",""))) {
							updateRow.add("AppointmentDate", DateUtil.showLocalTime(order.getString("pickup_appointment"), order.get("send_psid", 0L)));
						} else {
							updateRow.add("AppointmentDate","");
						}
						floorWMSOrderSyncMgr.updateOrder(updateRow,updateType);
					} else if(updateType == WmsOrderUpdateTypeKey.SO) {
						updateRow.add("CompanyID",companyId);
						updateRow.add("OrderNo",orderNo);
						updateRow.add("ReferenceNo", order.get("so",""));
						floorWMSOrderSyncMgr.updateOrder(updateRow,updateType);
					}
				} else {
					if(updateType == WmsOrderUpdateTypeKey.COMMIT){
						updateRow.add("CompanyID",companyId);
						updateRow.add("OrderNo",orderNo);
						updateRow.add("Status",WmsOrderStatusKey.OPEN);
						floorWMSOrderSyncMgr.updateOrder(updateRow,updateType);
					}
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	/**
	 * 获取line上的注释
	 * 
	 * @param note0
	 * @param orderlines
	 * @return
	 */
	private String getLineNote(String note0, DBRow[] orderlines) {
		Set<String> notes = new HashSet<String>();
		String rt = "";
		if (!"".equals(note0)) {
			rt= note0;
		}
		for (DBRow line: orderlines) {
			String note = line.get("note", "");
			if (!"".equals(note)) {
				if (!notes.contains(note)) {
					notes.add(note);
					rt += "\nLine"+line.get("line_no", 1) +":"+note+"";
				}
			}
		}
		if (notes.size()==1) {
			return note0+"\n"+notes.toArray()[0].toString();
		}
		return rt;
	}
	
	@Deprecated
	public void save(final DBRow order, final DBRow[] orderlines) throws Exception {
		//组织数据
		if ( isTF(order.get("customer_id",0L)) ) {
			sqlServerTransactionTemplate.execute(new TransactionCallbackWithoutResult() {
				protected void doInTransactionWithoutResult(TransactionStatus txStat) {
					long oid = order.get("b2b_oid", 0L);
					if (oid==0L) throw new RuntimeException("no oid");
					try {
						DBRow orderr = getWMSOrderDBRow(order);
						//System.out.println(DBRowUtils.dbRowAsString(orderr));
						List<DBRow> lines = new ArrayList<DBRow>();
						String companyid = orderr.get("CompanyID", "");
						//String companyId = getCompanyID(order.getString("company_id", ""), order.get("customer_id", 0L));
						long cnt=0;
						for (DBRow line :orderlines) {
							StringBuffer piece = new StringBuffer("");
							line.add("company_id", companyid);
							DBRow r = getWMSLineDBRow(line, order.get("receive_psid", 0L), order.get("customer_id", 0L), piece);
							cnt+= Long.parseLong(piece.toString());
							//补填PO
							
							r.add("COMPANYID", companyid);
							//po 来源于line
							//r.add("PONo", orderr.get("RETAIL_PO", ""));
							r.add("ReferenceNo", orderr.get("ReferenceNo", ""));
							r.add("WarehouseID", orderr.get("COMPANYID", ""));
							
							lines.add(r);
							//System.out.println(DBRowUtils.dbRowAsString(r));
						}
						//
						orderr.add("Note", getLineNote(order.get("remark",""),orderlines));
						
						if (cnt>0) {
							orderr.add("PickingType", "Piece Pick");
						} else {
							orderr.add("PickingType", "Case Pick");
						}
						if ("".equals(orderr.get("PONo", "")) && !"".equals(lines.get(0).get("PONo", "")) ) {
							orderr.add("PONo", lines.get(0).get("PONo", ""));
						}
						long wid = floorWMSOrderSyncMgr.saveOrder(orderr, lines.toArray(new DBRow[0]));
						
						floorWMSOrdersMgr.updateB2bOrder(oid, wid);
						//throw new Exception("test Tx");
					} catch (Exception e) {
						e.printStackTrace();
						throw new RuntimeException(e);
					}
				}
			});
		}
	}
	
	/**
	 * 同步line,开启对同方导入、手工创建数据的同步
	 */
	@Deprecated
	public void saveOrderLine(final long b2bOid) throws Exception {
		final DBRow order = floorB2BOrderMgrZyj.getB2BOrder(b2bOid);
		final DBRow[] orderlines = floorB2BOrderMgrZyj.getB2BOrderItems(b2bOid);
		if ( isTF(order.get("customer_id",0L)) ) {
			sqlServerTransactionTemplate.execute(new TransactionCallbackWithoutResult() {
						protected void doInTransactionWithoutResult(TransactionStatus txStat) {
					try {
						//手工创建时没有company_id
						if ("".equals(order.get("company_id", ""))) {
							order.add("company_id", "Valley");
						}
						String companyId = getCompanyID(order.getString("company_id", ""), order.get("customer_id", 0L));
						long orderNo = order.get("b2b_order_number",0L);
						DBRow orderr = getWMSOrderDBRow(order);
						if (orderNo>0) {
							List<DBRow> lines = new ArrayList<DBRow>();
							for (DBRow line :orderlines) {
								StringBuffer piece = new StringBuffer("");
								line.add("company_id", companyId);
								DBRow r = getWMSLineDBRow(line, order.get("receive_psid", 0L), order.get("customer_id", 0L), piece);
								r.add("COMPANYID", companyId);
								r.add("PONo", orderr.get("PONo", ""));
								r.add("ReferenceNo", orderr.get("ReferenceNo", ""));
								r.add("WarehouseID", companyId);
								lines.add(r);
							}
							floorWMSOrderSyncMgr.delOrderLine(companyId, String.valueOf(orderNo));
							if (lines.size()>0) {
								floorWMSOrderSyncMgr.saveOrderLine(b2bOid, String.valueOf(orderNo), lines.toArray(new DBRow[0]));
							}
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			});
		}
	}
	
	private String getCompanyID(String companyId, long customerid) {
		DBRow row = new DBRow();
		try {
			row = floorWMSOrdersMgr.getCompanyId(companyId, String.valueOf(customerid));
			return row.get("company_id", "");
		} catch (Exception e) {
			return "";
		}
	}
	private String getCustomerBrandName(long id) {
		PageCtrl ctrl  = new PageCtrl();
		ctrl.setPageSize(1);
		DBRow dbRow = new DBRow();
		dbRow.add("cb_id", id);
		DBRow[] list = null;
		try {
			list = floorCustomerIdMgr.fillterCustomerBrand(dbRow, ctrl);
			if (list!=null && list.length==1) {
				return list[0].get("brand_number", "");
			}
		} catch (Exception e) {
		}
		return ""+ id;	
	}
	private String getShipToName(long id) {
		DBRow shipto;
		try {
			shipto = floorShipToMgrZJ.getDetailShipToById(id);
			if (shipto==null) return "";
			return shipto.get("ship_to_name", "");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ""+id;
	}

	/**
	 * 转换订单
	 * 
	 * @param order
	 * @return
	 */
	private DBRow getWMSOrderDBRow(DBRow order) {
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String accountName = getShipToName(order.get("account_id",0L));
		if (isTF(order.get("customer_id", 0L))) {
			if ("".equals(order.get("company_id", ""))) {
				order.add("company_id", "Valley");
			}
		}
		String CompanyID =  getCompanyID(order.getString("company_id", ""),order.get("customer_id", 0L));
		String CustomerID = getCustomerBrandName(order.get("customer_id",0L));
		
		DBRow para = new DBRow();
		para.add("CompanyID",CompanyID);
		para.add("OrderNo",CompanyID);
		para.add("Status","Imported");//
		para.add("CustomerID", CustomerID);//装换
		para.add("AccountID", accountName);//装换
		para.add("OrderedDate",order.get("customer_order_date",""));
		try {
			para.add("RequestedDate", DateUtil.showLocationTime( (Date) order.get("requested_date"), order.get("send_psid", 0L)));
		} catch (Exception e) {
			para.add("RequestedDate", order.get("requested_date", ""));
		}
		para.add("ReferenceNo",order.get("customer_dn",""));
		para.add("PONo",order.get("retail_po",""));
		//para.add("StoreID",accountName);//装换
		para.add("StoreID", this.getShipToStoreID(CompanyID, CustomerID, accountName, order.get("ship_to_party_name", ""), order.get("deliver_house_number", ""), order.get("deliver_street", ""), order.get("b2b_order_linkman", ""), order.get("b2b_order_linkman_phone", "") ));
		para.add("StoreName", order.get("ship_to_party_name", ""));
		para.add("StoreAddress1", order.get("deliver_house_number", ""));
		para.add("StoreAddress2", order.get("deliver_street", ""));
		para.add("StoreCity", order.get("deliver_city", ""));
		para.add("StoreState", order.get("address_state_deliver", ""));
		para.add("StoreZipCode", order.get("deliver_zip_code", ""));
		para.add("StoreCountry","");//
		para.add("StoreContact","");
		para.add("StorePhone","");
		para.add("StoreExtension","");//
		para.add("StoreFax","");//
		para.add("StoreEmail","");// 新增字段
		
		para.add("StoreStoreNo","");//
		para.add("StoreBatchCode","");//
		para.add("StoreHome",0);//
		//para.add("ShipToID",accountName);///
		para.add("ShipToID", getShipToAddressID(CompanyID, CustomerID, accountName, order.get("ship_to_party_name", ""), order.get("deliver_house_number", ""), order.get("deliver_street", ""), order.get("b2b_order_linkman", ""), order.get("b2b_order_linkman_phone", "") ));
		para.add("ShipToName",order.get("ship_to_party_name", ""));
		para.add("ShipToAddress1",order.get("deliver_house_number", ""));
		para.add("ShipToAddress2",order.get("deliver_street", ""));
		para.add("ShipToCity",order.get("deliver_city", ""));
		para.add("ShipToState",order.get("address_state_deliver", ""));
		para.add("ShipToZipCode",order.get("deliver_zip_code", ""));
		para.add("ShipToCountry","");//转换
		para.add("ShipToContact",order.get("b2b_order_linkman", ""));
		para.add("ShipToPhone",order.get("b2b_order_linkman_phone", ""));
		para.add("ShipToExtension","");
		para.add("ShipToFax","");
		
		para.add("ShipToEmail","");// 新增字段
		
		para.add("ShipToStoreNo","");
		para.add("ShipToBatchCode","");
		para.add("ShipToHome",0);
		
		String BillID = "";
		if ("".equals(order.get("bill_to_name",""))) {
			para.add("BillToID", CustomerID);
			BillID = CustomerID;
		} else {
			para.add("BillToID", order.get("bill_to_name",""));
			BillID = order.get("bill_to_name","");
		}
		
		this.getBillName(CompanyID, CustomerID, accountName, BillID, para);
		
		if ("COLLECT".equalsIgnoreCase(order.get("freight_term",""))) {
			para.add("FreightTerm","Collect");
		} else if ("Prepaid".equalsIgnoreCase(order.get("freight_term",""))) {
			para.add("FreightTerm","Prepaid");
		} else {
			para.add("FreightTerm","Third Party");
		}
		//para.add("FreightTerm",order.get("freight_term",""));
		para.add("CarrierID", getCarrierId( CompanyID, order.get("carriers", "")));
		
		para.add("WarehouseID",CompanyID);
		para.add("PickingType","");//--------?
		//para.add("ScheduledDate","");//
		try {
			para.add("ScheduledDate", DateUtil.showLocationTime( (Date) order.get("requested_date"), order.get("send_psid", 0L)));
		} catch (Exception e) {
			para.add("ScheduledDate", order.get("requested_date", ""));
		}
		if (order.get("pickup_appointment")==null) {
			para.add("AppointmentDate", null);//
		} else {
			try {
				para.add("AppointmentDate", DateUtil.showLocationTime( (Date) order.get("pickup_appointment"), order.get("send_psid", 0L)));
			} catch (Exception e) {
				para.add("AppointmentDate", null);//
			}//
		}
		para.add("StagingAreaID", CompanyID);
		para.add("ProNo", order.get("order_number", ""));//
		para.add("LoadNo", order.get("load_no", ""));//
		para.add("ShippedDate",null);//
		para.add("ETADate",null);//
		para.add("OrderType","Order");//
		para.add("RetailerOrderType","");//
		para.add("DeptNo","");//
		para.add("CanceledDate",null);//
		para.add("VehicleNo","");//
		para.add("Seals","");//
		para.add("MasterReferenceNo","");//
		para.add("DivisionDescription","");//
		para.add("ShippingAccountNo","");//
		para.add("ShippingZipCode","");//
		para.add("DockID","");//
		para.add("PalletTypeID","");//
		para.add("LinkSequenceNo", order.get("b2b_oid", 0L));
		para.add("RFControlNo","");//
		para.add("SWControlNo","");//
		para.add("SHControlNo","");//
		para.add("INControlNo","");//
		
		para.add("Note",order.get("remark","")); //TODO 备注取行备注
		
		para.add("BOLNote","");
		/*
		try {
			para.add("BOLNote","MABD:"+ DateUtil.showLocationTime( (Date) order.get("mabd"), order.get("send_psid", 0L)) );
		} catch (Exception e) {
			para.add("BOLNote","MABD:"+ order.get("mabd") );
		}*/
		para.add("LabelNote","");//
		para.add("CartonLabelPrinted",0);
		para.add("PalletLabelPrinted",0);
		para.add("PickTicketPrinted",0);
		para.add("UCCLabelPrinted",0);
		para.add("ShippingLabelPrinted",0);
		para.add("ReturnLabelPrinted",0);
		para.add("TallySheetPrinted",0);
		para.add("PackingListPrinted",0);
		para.add("PackingSerialNosPrinted",0);
		para.add("BillOfLadingPrinted",0);
		para.add("EDIOrder",0);//
		para.add("Send753",0);//
		para.add("Send945",0);//
		para.add("Send856",0);//
		para.add("Send810",0);//
		para.add("Send214",0);//
		para.add("Send857",0);//
		para.add("CustomerInvoiceNo",0);//
		String now = format2.format(new Date());
		para.add("DateCreated",now);
		para.add("UserCreated","HON");//
		para.add("DateUpdated",now);//
		para.add("UserUpdated","HON");//
		
		if ( isTF(order.get("customer_id",0L)) ) {
			para.add("ReferenceNo", order.get("so",""));
			para.add("MasterReferenceNo", order.get("customer_dn",""));//
			para.add("ProNo", order.get("customer_dn", ""));
			
		}
		return para;
	}
	
	public String getCarrierId(String CompanyID, String scac) {
		if (scac==null || "".equals(scac)) return "";
		try {
			return floorWMSOrderSyncMgr.getCarrierId(CompanyID, scac);
		} catch (SQLException e) {
		}
		return "";
	}
	
	public String getShipToAddressID(String CompanyID, String CustomerID, String AccountID, String Name, String Address1, String Address2, String Contact, String Phone) {
		try {
			DBRow row = floorWMSOrderSyncMgr.getShipToAddressID(CompanyID, CustomerID, AccountID, Name, Address1, Address2, Contact, Phone);
			if (row!=null) return row.get("AddressID", "?");
		} catch (SQLException e) {
		}
		return "?";
	}
	
	public String getBillName(String CompanyID, String CustomerID, String AccountID, String BillID, DBRow order) {
		order.add("BillToName", "");
		order.add("BillToAddress1", "");
		order.add("BillToAddress2", "");
		order.add("BillToCity", "");
		order.add("BillToState", "");
		order.add("BillToZipCode", "");
		order.add("BillToCountry", "");
		order.add("BillToContact", "");
		order.add("BillToPhone", "");
		order.add("BillToExtension", "");//
		order.add("BillToFax", "");//
		
		order.add("BillToEmail", "");// 新增字段
		
		order.add("BillToStoreNo", "");
		order.add("BillToBatchCode", "");//
		order.add("BillToHome", 0);//
		if (CompanyID==null || CustomerID==null || AccountID== null) {
			if (BillID==null) {
				return "";
			} else {
				return BillID;
			}
		}
		if (BillID==null) {
			return "";
		}
		try {
			DBRow row = floorWMSOrderSyncMgr.getBillInfo(CompanyID, CustomerID, AccountID, BillID);
			if (row==null) {
				return "";
			}
			
			order.add("BillToName", row.get("BillToName",""));
			order.add("BillToAddress1", row.get("BillToAddress1",""));
			order.add("BillToAddress2", row.get("BillToAddress2",""));
			order.add("BillToCity", row.get("BillToCity",""));
			order.add("BillToState", row.get("BillToState",""));
			order.add("BillToZipCode", row.get("BillToZipCode",""));
			order.add("BillToCountry", row.get("BillToCountry",""));
			order.add("BillToContact", row.get("BillToContact",""));
			order.add("BillToPhone", row.get("BillToPhone",""));
			order.add("BillToExtension", row.get("BillToExtension",""));//
			order.add("BillToFax", row.get("BillToFax",""));//
			
			order.add("BillToEmail", row.get("BillToEmail",""));// 新增字段
			
			order.add("BillToStoreNo", row.get("BillToStoreNo",""));
			order.add("BillToBatchCode", row.get("BillToBatchCode",""));//
			order.add("BillToHome", row.get("BillToHome",0));//
			
		} catch (SQLException e) {
		}
		return "";
	}
	
	public String getShipToStoreID(String CompanyID, String CustomerID, String AccountID, String Name, String Address1, String Address2, String Contact, String Phone) {
		try {
			DBRow row = floorWMSOrderSyncMgr.getShipToAddressID(CompanyID, CustomerID, AccountID, Name, Address1, Address2, Contact, Phone);
			if (row!=null) return row.get("StoreNo", "?");
		} catch (SQLException e) {
		}
		return "?";
	}
	
	/**
	 * 获取最大包装重量，包装重量和配置。
	 * 
	 * @return
	 */
	public DBRow getPoundPerPackage(String CompanyID, String CustomerID, String ItemID) {
		try {
			DBRow[] rows = floorWMSOrderSyncMgr.getCustomerItem(CompanyID, CustomerID, ItemID);
			if (rows!=null && rows.length>0) {
				return rows[0];
			}
		} catch (Exception e) {}
		return new DBRow();
	}
	
	/**
	 * 转换明细
	 * 
	 * @param line
	 * @return
	 */
	private DBRow getWMSLineDBRow(DBRow line, long shipToId, long customer_id, StringBuffer piece) {
		DBRow para = new DBRow();
		float qty = line.get("b2b_count", 0f);
		para.add("CompanyID",line.getString("company_id", ""));//R
		para.add("OrderNo",0);
		para.add("LineNo",line.get("line_no", 1));
		para.add("ItemID",line.get("item_id", ""));
		para.add("WarehouseID",line.getString("company_id", ""));
		para.add("ZoneID","");//
		para.add("BuyerItemID",line.get("buyer_item_id", ""));
		para.add("SupplierID",line.get("title", ""));//装换
		para.add("LotNo",line.get("lot_number", ""));//R
		DBRow pound = new DBRow();
		try {
			pound = getPoundPerPackage(line.getString("company_id", ""), getCustomerBrandName(customer_id), line.get("item_id", ""));
			para.add("PoundPerPackage",line.get("PoundPerPackage", pound.get("PoundPerPackage", "")));//读取基础数据
		} catch (Exception e) {
			pound = new DBRow();
			para.add("PoundPerPackage",line.get("PoundPerPackage", ""));//读取基础数据
		}
		para.add("PONo",line.get("retail_po", ""));
		para.add("ReferenceNo",line.get("customer_dn", ""));
		para.add("ReferenceLineNo",line.get("customer_dn_line", ""));
		para.add("OrderedQty",line.get("b2b_count", 0f));
		para.add("Pallets",line.get("pallet_spaces", 0f));
		para.add("UnitPrice",line.get("unit_price", 0f));//读取基础数据
		para.add("PrePackQty",(pound.get("QtyPerUnit", 1)*pound.get("UnitsPerPackage", 1)));//?
		
		para.add("PrePackDescription","");
		para.add("CustomerPallets","");
		para.add("CommodityDescription",line.get("commodity_description", ""));
		para.add("NMFC",line.get("nmfc_code", ""));//读取基础数据
		para.add("FreightClass",line.get("freight_class", ""));//读取基础数据
		para.add("PalletWeight",line.get("PalletWeight", 0f));//读取基础数据
		para.add("Length",line.get("length", 0f));//读取基础数据
		para.add("Width",line.get("width", 0f));//读取基础数据
		para.add("Height",line.get("heigth", 0f));//读取基础数据
		para.add("Note",line.get("note", ""));
		piece.append(""+(int) (qty % (pound.get("QtyPerUnit", 1)*pound.get("UnitsPerPackage", 1)) / pound.get("QtyPerUnit", 1)));
		return para;
	}
	
	/**
	 * 是否为同方
	 * 
	 * @param customer_id
	 * @return
	 */
	public static boolean isTF(long customer_id) {
		if (customer_id==1 || customer_id==2 || customer_id==3) {
			return true;
		}
		return false;
	}
	
	/**
	 * 替换title
	 * @param title
	 * @param customerID
	 * @return
	 */
	public static String changTitle(String title, long customerID) {
		if (isTF(customerID)) {
			if ("Element".equalsIgnoreCase(title)) return "SEIDIG0005";
			if ("Seiki".equalsIgnoreCase(title)) return "SEIDIG0006";
			if ("Westinghouse".equalsIgnoreCase(title)) return "TINLLC0001";
		}
		return title;
	}
	
	
	@Deprecated
	public void saveOrder(final long b2bOid) throws Exception {
		/*
		sqlServerTransactionTemplate.execute(new TransactionCallbackWithoutResult() {
			protected void doInTransactionWithoutResult(TransactionStatus txStat) {
				try {
					DBRow order= floorB2BOrderMgrZyj.getDetailB2BOrderById(b2bOid);
					if (isTF(order.get("customer_id",0L))) {
						//String companyId =  getCompanyID(order.getString("company_id", ""),order.get("customer_id", 0L));
						long wid = order.get("b2b_order_number", 0L);
						if (wid>0) {
							DBRow worder = getWMSOrderDBRow(order);
							worder.add("OrderNo",String.valueOf(wid));
							worder.add("LinkSequenceNo", "");
							floorWMSOrderSyncMgr.updateOrder(worder);
						}
					}
				} catch (Exception e) {}
			}
		});
		*/
	}

	public long saveRN(Receipt main, Set<ReceiptLine> lines) throws Exception {
		String CustomerID = getCustomerBrandName(main.getCustomerKey());
		String carrierId =  getCarrierId( main.getCompanyId(), main.getCarrierId());
		main.setCustomerId(CustomerID);
		main.setCarrierId(carrierId);
		main.setSupplierId(changTitle(main.getSupplierId(),1));
		main.setLines(lines);
		return floorWMSOrderSyncMgr.saveRN(main.toDBRow(), main.toLines());
	}
	
	public long updateRN(Receipt main, Set<ReceiptLine> lines) throws Exception {
		String CustomerID = getCustomerBrandName(main.getCustomerKey());
		String carrierId =  getCarrierId( main.getCompanyId(), main.getCarrierId());
		main.setCustomerId(CustomerID);
		main.setCarrierId(carrierId);
		main.setSupplierId(changTitle(main.getSupplierId(),1));
		main.setLines(lines);
		return floorWMSOrderSyncMgr.updateRN(main.toDBRow(), main.toLines());
	}
	
	public boolean cancelRN(String CompanyID, long RNNo) throws Exception {
		return floorWMSOrderSyncMgr.cancelRN(CompanyID, RNNo);
	}
	
	public boolean deleteRN(String CompanyID, long RNNo) throws Exception {
		return floorWMSOrderSyncMgr.deleteRN(CompanyID, RNNo);
	}
}
