package com.cwc.app.api;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class EntryTicket implements Serializable {
	@XmlElement(name = "id")
	public String id;

	@XmlElement(name = "checkAction")
	public CheckAction checkAction;

	@XmlElement(name = "customizationId")
	public String customizationId;

	@XmlElement(name = "skipSteps")
	public List<String> skipSteps;

	@XmlElement(name = "spotId")
	public String spotId;

	@XmlElement(name = "dockId")
	public String dockId;

	@XmlElement(name = "expediteFee")
	public Boolean expediteFee;

	@XmlElement(name = "checkInStartTime")
	public LocalDateTime checkInStartTime;

	@XmlElement(name = "checkInEndTime")
	public LocalDateTime checkInEndTime;

	@XmlElement(name = "checkOutStartTime")
	public LocalDateTime checkOutStartTime;

	@XmlElement(name = "checkOutEndTime")
	public LocalDateTime checkOutEndTime;

	@XmlElement(name = "createdBy")
	public String createdBy;

	@XmlElement(name = "createdWhen")
	public LocalDateTime createdWhen;

	@XmlElement(name = "updatedBy")
	public String updatedBy;

	@XmlElement(name = "updatedWhen")
	public LocalDateTime updatedWhen;

	@XmlElement(name = "entryType")
	public String entryType;

	@XmlElement(name = "officialWindowCheckInTime")
	public LocalDateTime officialWindowCheckInTime;
}
