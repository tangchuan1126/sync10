package com.cwc.app.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.cwc.app.api.zj.QuoteMgrZJ;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.quote.CustomAddProductsBean;
import com.cwc.app.beans.quote.CustomChangeProductsBean;
import com.cwc.app.beans.quote.CustomRemoveProductsBean;
import com.cwc.app.beans.retailprice.RetailPriceQuoteBean;
import com.cwc.app.emailtemplate.velocity.quote.QuoteEmailPageString;
import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.exception.order.LostMoneyException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.exception.quote.CartQuoteErrorException;
import com.cwc.app.exception.quote.DuplicateOrderException;
import com.cwc.app.exception.quote.ErrorWholeSellDiscountRuleException;
import com.cwc.app.exception.quote.OrderNotFoundException;
import com.cwc.app.exception.quote.OrderNotWait4RecordException;
import com.cwc.app.exception.quote.OrderSourceErrorException;
import com.cwc.app.exception.quote.TotalMcGrossErrorException;
import com.cwc.app.exception.quote.WareHouseErrorException;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorQuoteMgr;
import com.cwc.app.iface.AdminMgrIFace;
import com.cwc.app.iface.CrmMgrIFace;
import com.cwc.app.iface.QuoteIFace;
import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.app.iface.zr.ScheduleMgrIfaceZR;
import com.cwc.app.iface.zr.ScheduleReplayMgrIfaceZR;
import com.cwc.app.key.CrmTraceTypeKey;
import com.cwc.app.key.HandStatusleKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.key.QuoteKey;
import com.cwc.app.key.RetailPriceErrorKey;
import com.cwc.app.key.RetailPriceKey;
import com.cwc.app.key.RetailPriceResultTypeKey;
import com.cwc.app.key.WorkFlowTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.app.wholeselldiscount.DiscountCommPolicy;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.email.Attach;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;
import com.cwc.reportcenter.ReportCenter;
import com.cwc.service.order.action.OrderCostBean;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;
import com.cwc.util.Tree;

public class QuoteMgr implements QuoteIFace
{
	static Logger log = Logger.getLogger("ACTION");

	private FloorQuoteMgr fqm;
	private FloorOrderMgr fom ;
	private CustomCartQuote customCart;
	private CartQuote cart;
	private FloorProductMgr fpm;
	private SystemConfig systemConfig ;
	private ExpressMgr expressMgr;
	private OrderMgr orderMgr;
	private ProductMgr productMgr;
	private FloorAdminMgr fam ;
	private FloorCatalogMgr fcm;
	private CrmMgrIFace crmMgr;
	private ProductMgrIFaceZJ productMgrZJ;
	private QuoteMgrZJ quoteMgrZJ;
	
	private ScheduleMgrIfaceZR scheduleMgrZr;
	private AdminMgrIFace adminMgr; 
	private ScheduleReplayMgrIfaceZR scheduleReplayMgrZr ;
	/**
	 * 获得所有报价列表
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] getAllQuotes(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getAllQuotes(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllQuotes",log);
		}
	}
		
	/**
	 * 获得报价商品
	 * @param qoid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getQuoteItemsByQoid(long qoid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getQuoteItemsByQoid( qoid, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getQuoteItemsByQoid",log);
		}
	}
	
	/**
	 * 获得详细报价信息
	 * @param qoid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailQuoteByQoid(long qoid)
		throws Exception
	{
		try
		{
			return(fqm.getDetailQuoteByQoid(qoid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailQuoteByQoid",log);
		}
	}
	
	public DBRow getDetailQuoteItemByQoidPid(long qoid,long pid)
		throws Exception
	{
		try
		{
			return(fqm.getDetailQuoteItemByQoidPid( qoid, pid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailQuoteItemByQoidPid",log);
		}
	}

	/**
	 * 创建报价单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long createQuote(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String cmd = StringUtil.getString(request, "cmd");
			int target_type = StringUtil.getInt(request,"target_type");
			int cc_id = StringUtil.getInt(request,"cc_id");
			
			String client_id = StringUtil.getString(request,"client_id");
			String address_name = StringUtil.getString(request,"address_name");
			String address_street = StringUtil.getString(request,"address_street");
			String address_city = StringUtil.getString(request,"address_city");
			String address_state = StringUtil.getString(request,"address_state");
			String address_zip = StringUtil.getString(request,"address_zip");
			String address_country = StringUtil.getString(request,"address_country");
			String tel = StringUtil.getString(request,"tel");
			String ccid = StringUtil.getString(request,"ccid");
			String note = StringUtil.getString(request,"note");
			String pro_id = StringUtil.getString(request,"pro_id");
			
			DBRow quote_porder = new DBRow();
			quote_porder.add("client_id",client_id);
			quote_porder.add("address_name",address_name);
			quote_porder.add("address_street",address_street);
			quote_porder.add("address_city",address_city);
			quote_porder.add("address_state",address_state);
			quote_porder.add("address_zip",address_zip);
			quote_porder.add("address_country",address_country);
			quote_porder.add("tel",tel);
			quote_porder.add("ccid",ccid);
			quote_porder.add("note",note);
			quote_porder.add("pro_id",pro_id);
			
			
			TDate tEDate = new TDate();
			tEDate.addDay(14);
			
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminInfo = adminMgr.getAdminLoginBean( StringUtil.getSession(request) );
			
			DBRow detailCountry = fom.getDetailCountryCodeByCcid(StringUtil.getLong(request, "ccid"));
			quote_porder.add("address_country",detailCountry.getString("c_country"));
			
			quote_porder.add("create_account", adminInfo.getAccount());
			quote_porder.add("post_date", DateUtil.NowStr());
			quote_porder.add("expiration_date", tEDate.getStringYear()+"-"+tEDate.getStringMonth()+"-"+tEDate.getStringDay());
			quote_porder.add("manager_discount", 0);
			quote_porder.add("manager_approve", 0);
			quote_porder.add("quote_status", QuoteKey.WAITING);
			
			long qoid = fqm.addQuote(quote_porder);
			
			//如果是从crm创建，还需要增加一条跟进记录
			if ( cmd.equals("crm") )
			{
				crmMgr.addTraceSub(target_type, cc_id, CrmTraceTypeKey.QUOTE, DateUtil.NowStr(), "创建报价单:"+qoid, adminInfo.getAdid(),qoid);		
				crmMgr.updateClientByCcid(cc_id, DateUtil.NowStr(), target_type);
			}

			return(qoid);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"createQuote",log);
		}
	}

	/**
	 * 修改报价基本信息
	 * @param request
	 * @throws Exception
	 */
	public void modQuote(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String client_id = StringUtil.getString(request,"client_id");
			String address_name = StringUtil.getString(request,"address_name");
			String address_street = StringUtil.getString(request,"address_street");
			String address_city = StringUtil.getString(request,"address_city");
			String address_state = StringUtil.getString(request,"address_state");
			String address_zip = StringUtil.getString(request,"address_zip");
			String address_country = StringUtil.getString(request,"address_country");
			String tel = StringUtil.getString(request,"tel");
			String ccid = StringUtil.getString(request,"ccid");
			String note = StringUtil.getString(request,"note");
			String pro_id = StringUtil.getString(request,"pro_id");
			
			DBRow quote_porder = new DBRow();
			quote_porder.add("client_id",client_id);
			quote_porder.add("address_name",address_name);
			quote_porder.add("address_street",address_street);
			quote_porder.add("address_city",address_city);
			quote_porder.add("address_state",address_state);
			quote_porder.add("address_zip",address_zip);
			quote_porder.add("address_country",address_country);
			quote_porder.add("tel",tel);
			quote_porder.add("ccid",ccid);
			quote_porder.add("note",note);
			quote_porder.add("pro_id",pro_id);
			
			
			
			long qoid = StringUtil.getLong(request, "qoid");
			
			DBRow detailCountry = fom.getDetailCountryCodeByCcid(StringUtil.getLong(request, "ccid"));
			quote_porder.add("address_country",detailCountry.getString("c_country"));

			fqm.modQuoteOrder(qoid, quote_porder);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modQuote",log);
		}
	}

	/**
	 * 保存报价单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public int saveQuoteProduct(HttpServletRequest request)
		throws Exception
	{
		try
		{
				long qoid = StringUtil.getLong(request,"qoid");
				long sc_id = StringUtil.getLong(request,"sc_id");
				long ccid = StringUtil.getLong(request,"ccid");
				long ps_id = StringUtil.getLong(request,"ps_id");		//发货仓库
				long pro_id = StringUtil.getLong(request,"pro_id");
	
				double currency = systemConfig.getDoubleConfigValue("USD");//获得汇率
				String client_id = StringUtil.getString(request, "client_id");
				
				fqm.delQuoteItem(qoid);
				//重新计算购物车数据后，获得详细信息
				cart.flush(StringUtil.getSession(request),client_id,sc_id,ccid,pro_id,ps_id);
				DBRow cartProducts[] = cart.getDetailProducts();
				
				for (int i=0; i<cartProducts.length; i++)
				{
							if (cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.NORMAL||cartProducts[i].get("cart_product_type", 0)==ProductTypeKey.UNION_STANDARD)
							{
								DBRow orderItem = new DBRow();
								orderItem.add("qoid",qoid);
								orderItem.add("pid",cartProducts[i].getString("cart_pid"));
								orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
								orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
								orderItem.add("unit_price",MoneyUtil.div(cartProducts[i].get("unit_price", 0d), currency));//美元
								orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
								orderItem.add("weight",cartProducts[i].get("weight", 0f));
								orderItem.add("name",cartProducts[i].getString("p_name"));
								orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
								orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
								orderItem.add("lowest_price",cartProducts[i].getString("lowest_price"));
								orderItem.add("quote_price",cartProducts[i].getString("quote_price"));
								orderItem.add("discount",cartProducts[i].getString("discount"));
								orderItem.add("price_type",cartProducts[i].getString("price_type"));
								
								fqm.addQuoteOrderItem(orderItem);
							}
							else  //定制 
							{
								//先把旧的定制商品删掉
								fpm.delProductCustomUnionBySetPid(StringUtil.getLong(cartProducts[i].getString("cart_pid")));
								DBRow productsCustomInSet[] = customCart.getFinalDetailProducts(StringUtil.getSession(request), StringUtil.getLong(cartProducts[i].getString("cart_pid")));
								for (int j=0; j<productsCustomInSet.length; j++)
								{
										//插入到定制表
										DBRow customPro = new DBRow();
										customPro.add("set_pid",cartProducts[i].getString("cart_pid"));
										customPro.add("pid",productsCustomInSet[j].getString("pc_id"));
										customPro.add("quantity",productsCustomInSet[j].getString("cart_quantity"));
										fpm.addProductCustomUnion(customPro);
								}

								//插入定制后的主商品
								DBRow orderItem = new DBRow();
								orderItem.add("qoid",qoid);
								orderItem.add("pid",cartProducts[i].getString("cart_pid"));
								orderItem.add("quantity",cartProducts[i].getString("cart_quantity"));
								orderItem.add("catalog_id",cartProducts[i].get("catalog_id", 0l));
								orderItem.add("unit_price",cartProducts[i].getString("unit_price"));//已经计算好美元
								orderItem.add("gross_profit",cartProducts[i].get("gross_profit", 0d));
								orderItem.add("weight",cartProducts[i].getString("weight"));
								orderItem.add("name",cartProducts[i].getString("p_name"));
								orderItem.add("unit_name",cartProducts[i].getString("unit_name"));
								orderItem.add("product_type",cartProducts[i].getString("cart_product_type"));
								orderItem.add("lowest_price",cartProducts[i].getString("lowest_price"));
								orderItem.add("quote_price",cartProducts[i].getString("quote_price"));
								orderItem.add("discount",cartProducts[i].getString("discount"));
								orderItem.add("price_type",cartProducts[i].getString("price_type"));

								fqm.addQuoteOrderItem(orderItem);
							}
				}
				cart.clearCart(StringUtil.getSession(request));				//清空购物车
				
				DBRow porder = new DBRow();
				porder.add("ps_id",ps_id);
				porder.add("sc_id",sc_id);
				porder.add("mc_gross",0);//实际通过paypal支付金额
				porder.add("quantity",cart.getSumQuantity());
				
				porder.add("product_cost",cart.getProductFee()+cart.getSumSpecialPrice());
				porder.add("shipping_cost",cart.getShippingFee());
				porder.add("total_cost",cart.getTotalPrice());
				porder.add("total_cost_rmb",MoneyUtil.mul(cart.getTotalPrice(),currency));
				String manager_discount = "";
				if(cartProducts==null)
				{
					manager_discount = "1";
				}
				else
				{
					if(cartProducts[0].getString("manager_discount").equals(""))
					{
						manager_discount = "1";
					}
					else
					{
						manager_discount = cartProducts[0].getString("manager_discount");
					}
					
				}
				porder.add("manager_discount",manager_discount);
				porder.add("total_weight",cart.getTotalWeight());
				porder.add("system_discount",cart.getSystemDiscount());
				porder.add("final_discount",cart.getFinalDiscount());
				porder.add("saving",cart.getSaving());
	
				fqm.modQuoteOrder(qoid,porder);
				
				return(HandStatusleKey.NORMAL);//原先在try外
		}
		catch(CartQuoteErrorException e)
		{
			throw e;
		}
		catch(Exception e) 
		{
			throw new SystemException(e,"saveQuoteProduct",log);
		}
		
		
	}
	
	/**
	 * 初始化报价购物车
	 * @param request
	 * @param qoid
	 * @throws Exception
	 */
	public void initQuoteCartFromOrder(HttpServletRequest request,long qoid)
		throws Exception
	{
		try
		{
			cart.clearCart(StringUtil.getSession(request));//情况主购物车
			customCart.clearCart(StringUtil.getSession(request));//清空定制购物车
			
			DBRow detailOrder = fqm.getDetailQuoteByQoid(qoid);
			
			DBRow orderItems[] = fqm.getQuoteItemsByQoid(qoid, null);
			for (int i=0; i<orderItems.length; i++)
			{
				cart.initCart(StringUtil.getSession(request),orderItems[i].get("pid", 0l), orderItems[i].get("quantity", 0f),orderItems[i].get("product_type", 0),detailOrder.get("manager_discount", 0f),orderItems[i].get("price_type", 0),orderItems[i].get("quote_price", 0d));
				//如果是定制商品，还需要初始化定制商品的session
				if (orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_CUSTOM)
				{
					DBRow inSetProducts[] = fpm.getProductsCustomInSetBySetPid(orderItems[i].get("pid", 0l));
					for (int j=0; j<inSetProducts.length; j++)
					{
						customCart.put2Cart(StringUtil.getSession(request), orderItems[i].get("pid", 0l), inSetProducts[j].get("pc_id", 0l), inSetProducts[j].get("quantity", 0f));
					}
				}
			}
			customCart.copy2FianlSession(StringUtil.getSession(request));
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"initQuoteCartFromOrder",log);
		}
	}

	/**
	 * 计算订单商品和运费成本
	 * @param oid
	 * @param sc_id
	 * @param ccid
	 * @return
	 * @throws WeightOutSizeException
	 * @throws WeightCrossException
	 * @throws CountryOutSizeException
	 * @throws LostMoneyException
	 * @throws Exception
	 */
	public OrderCostBean getQuoteOrderCost(long oid,long sc_id,long ccid)
		throws ProvinceOutSizeException,WeightOutSizeException,WeightCrossException,CountryOutSizeException,Exception
	{
		try
		{
			double product_cost = 0;//商品成本
			float weight = 0;		//商品重量
			double total_mc_gross = 0;//订单总成本

			DBRow orderItems[] = fqm.getQuoteItemsByQoid(oid, null);
			for (int i=0; i<orderItems.length; i++)
			{
				if ( orderItems[i].get("product_type", 0)==ProductTypeKey.NORMAL||orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD )
				{
					product_cost += MoneyUtil.mul(orderItems[i].get("unit_price", 0d), orderItems[i].get("quantity", 0d));
					weight += orderItems[i].get("weight", 0f)*orderItems[i].get("quantity", 0f);
				}
				else if ( orderItems[i].get("product_type", 0)==ProductTypeKey.UNION_STANDARD_MANUAL )//手工拼装
				{
					DBRow productsInSet[] = fpm.getProductsInSetBySetPid( orderItems[i].get("pid",0l) );
					for (int j=0; j<productsInSet.length; j++)
					{
						product_cost += MoneyUtil.mul(productsInSet[j].get("unit_price", 0d),orderItems[i].get("quantity", 0d)*productsInSet[j].get("quantity", 0d));
					}
					
					weight += orderItems[i].get("weight", 0f)*orderItems[i].get("quantity", 0f);
				}
				else	//定制
				{
					DBRow customProducts[] = fpm.getProductsCustomInSetBySetPid(orderItems[i].get("pid",0l));
					for (int j=0; j<customProducts.length; j++)
					{
						product_cost += MoneyUtil.mul(customProducts[j].get("unit_price", 0d), orderItems[i].get("quantity", 0d)*customProducts[j].get("quantity", 0d));
						weight += customProducts[j].get("weight", 0f)*orderItems[i].get("quantity", 0f)*customProducts[j].get("quantity", 0f);
					}
				}
			}
			orderItems = null;

			DBRow detailorder = fqm.getDetailQuoteByQoid(oid);
			total_mc_gross += detailorder.get("mc_gross", 0d);

			//需要乘以一个重量修正系数
			float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
			weight = weight*weight_cost_coefficient;

			////system.out.println("Weight:"+weight+" Cost:"+product_cost+" OrderCost:"+total_mc_gross);
			
			//计算运费
			ShippingInfoBean ShippingInfoBean = expressMgr.getShippingFee(sc_id, weight, ccid,detailorder.get("pro_id", 0l));//如果父快递公司不能配送，需要继续查找子快递公司
			double currency = systemConfig.getDoubleConfigValue( detailorder.getString("mc_currency") );//获得汇率

//			if (detailorder.getString("mc_currency").equals("USD")==false)
//			{
//				//system.out.println(detailorder.getString("mc_currency")+" - "+currency);
//			}

			OrderCostBean orderCostBean = new OrderCostBean();
			if (ShippingInfoBean!=null)
			{
				orderCostBean.setProductCost(orderMgr.getProductMergeCost(product_cost));								//商品成本
				orderCostBean.setShippingCost(orderMgr.getShippingMergeCost(ShippingInfoBean.getShippingFee()));			//运费成本
				orderCostBean.setTotalCost( orderMgr.getOrderMergeCost(OrderMgr.ORDER_SOURCE_DIRECTPAY,orderMgr.getProductMergeCost(product_cost), orderMgr.getShippingMergeCost(ShippingInfoBean.getShippingFee())) );		//订单总成本		
				orderCostBean.setMcGross(total_mc_gross);															//订单总金额
				orderCostBean.setMcCurrency("USD");									//货币类型
				orderCostBean.setWeight(weight);																	//订单总重
				orderCostBean.setExpressName(ShippingInfoBean.getCompanyName());									//快递公司
				orderCostBean.setRmbMcGross( MoneyUtil.round(MoneyUtil.mul(total_mc_gross, currency), 2) );
			}

			return(orderCostBean);
		}
		catch (CountryOutSizeException e)//国家不到达
		{
			throw e;
		}
		catch (WeightCrossException e)//重量数据段交叉
		{
			throw e;
		}
		catch (WeightOutSizeException e)//重量超出计算范围
		{
			throw e;
		}
		catch (ProvinceOutSizeException e)//省份设置错误
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getQuoteOrderCost",log);
		}
	}

	/**
	 * 获得商品批发报价
	 * @param ccid  收货国家
	 * @param discount 客服折扣
	 * @param unit_price 商品成本价
	 * @return
	 * @throws Exception
	 */
	public double getLowestProductPrice(long pid,long ccid,double discount)
		throws Exception
	{
		try
		{
//			pid = 166969;
			DBRow detailpro = fpm.getDetailProductByPcid(pid);
			//如果不为0，则为定制商品
			if (detailpro.get("orignal_pc_id",0l)!=0)
			{
				DBRow customDetailpro = fpm.getDetailProductCustomByPcPcid(pid);
				pid = customDetailpro.get("orignal_pc_id", 0l);
				detailpro = fpm.getDetailProductByPcid(pid);
			}
			
			double unit_price = detailpro.get("unit_price",0d);
			double profit = fqm.getDetailProductCountryProfitByPidCcId(pid, ccid).get("profit", 0d);

			profit = profit * discount;		
			//订单最少利润值
			profit = orderMgr.getOrderMergeCost(OrderMgr.ORDER_SOURCE_EBAY,profit, 0);
			
			double lowest_product_price = 0;
			//商品最低成本价
			double min_cost_price = orderMgr.getOrderMergeCost(OrderMgr.ORDER_SOURCE_EBAY,orderMgr.getProductMergeCost(unit_price), 0);
			lowest_product_price = MoneyUtil.add( min_cost_price,profit);
			
			return( MoneyUtil.round(lowest_product_price, 2) );
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getLowestProductPrice",log);
		}
	}
	
	/**
	 * 打折后的商品最终报价(不在使用)
	 * @param gross_profit
	 * @param unit_price
	 * @param discount
	 * @param manager_discount
	 * @return
	 * @throws Exception
	 */
//	public double getFinalProductPrice(double gross_profit,double unit_price,double discount,double manager_discount)
//		throws Exception
//	{
//		try
//		{
//			double final_product_price = 0;
//			
//			//商品最低成本价
//			double min_cost_price = orderMgr.getOrderMergeCost(OrderMgr.ORDER_SOURCE_DIRECTPAY,orderMgr.getProductMergeCost(unit_price), 0);
//			final_product_price = MoneyUtil.mul( min_cost_price,1+gross_profit*(discount-manager_discount));
//			
//			////system.out.println(gross_profit+"-"+unit_price+"-"+discount+"-"+manager_discount+" = "+MoneyUtil.round(final_product_price/6.65, 2));
//			
//			//最终报价=成本价x（1+批发毛利率x（订单折扣-主管折扣）） 
//			return( MoneyUtil.round(final_product_price, 2) );
//		}
//		catch (Exception e) 
//		{
//			throw new SystemException(e,"getFinalProductPrice",log);
//		}
//	}

	/**
	 * 根据折扣优惠规则，计算最终批发价
	 * @param client_id  下单人邮件
	 * @param mc_gross		订单商品总金额(美元)
	 * @param manager_discount	主管折扣
	 * @return
	 * @throws Exception
	 */
	public double getWholeSellDiscount(String buy_catalog,String client_id,double mc_gross)
		throws Exception
	{
		try
		{
			//计算客户可以获得的折扣
			DiscountCommPolicy generalPolicyImp = (DiscountCommPolicy)MvcUtil.getBeanFromContainer("GeneralPolicyImp");//折扣计算策略总入口
			generalPolicyImp.setClientId(client_id);
			String subPolicyName = generalPolicyImp.getDisCount();
	
			DiscountCommPolicy subPolicyImp = (DiscountCommPolicy)MvcUtil.getBeanFromContainer(subPolicyName.substring(1));
			subPolicyImp.setClientId(client_id);
			subPolicyImp.setCurrentMcGross(mc_gross);
			subPolicyImp.setBuyCatalog(buy_catalog);
			
			double discount = StringUtil.getDouble(subPolicyImp.getDisCount());
			////system.out.println(discount);
			return(discount);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getWholeSellDiscount",log);
		}
	}
	
	/**
	 * 获得最低运费
	 * @param shipping_fee
	 * @return
	 * @throws Exception
	 */
	public double getLowestShippingPrice(double shipping_fee)
		throws Exception
	{
		try
		{
			//成本价x运费修正率x订单最低利润率
			return( orderMgr.getOrderMergeCost(OrderMgr.ORDER_SOURCE_EBAY,orderMgr.getShippingMergeCost(shipping_fee), 0) );
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getLowestShippingPrice",log);
		}
	}
	
	/**
	 * 获得所有折扣设置
	 * @param wsd_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWholeSellDiscounts(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getWholeSellDiscounts(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getWholeSellDiscountsByWsdId",log);
		}
	}

	/**
	 * 获得折扣详细信息
	 * @param wsd_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailWholeSellDiscountByWsdId(long wsd_id)
		throws Exception
	{
		try
		{
			return(fqm.getDetailWholeSellDiscountByWsdId(wsd_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getWholeSellDiscountByWsdId",log);
		}
	}
	
	/**
	 * 增加批发折扣
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addWholeSellDiscount(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String name = StringUtil.getString(request, "name");
			String discount_policy = StringUtil.getString(request, "discount_policy");
			String imp_class = StringUtil.getString(request, "imp_class");
			String summary = StringUtil.getString(request, "summary");
			
			DBRow wholesell = new DBRow();
			wholesell.add("name", name);
			wholesell.add("discount_policy", discount_policy);
			wholesell.add("imp_class", imp_class);
			wholesell.add("summary", summary);
			
			long wsd_id = fqm.addWholeSellDiscount(wholesell);
	
			return(wsd_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addWholeSellDiscount",log);
		}
	}
	
	/**
	 * 修改批发折扣信息
	 * @param request
	 * @throws Exception
	 */
	public void modWholeSellDiscountByWsdId(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long wsd_id = StringUtil.getLong(request, "wsd_id");
			String name = StringUtil.getString(request, "name");
			String discount_policy = StringUtil.getString(request, "discount_policy");
			String imp_class = StringUtil.getString(request, "imp_class");
			String summary = StringUtil.getString(request, "summary");
			
			DBRow wholesell = new DBRow();
			wholesell.add("name", name);
			wholesell.add("discount_policy", discount_policy);
			wholesell.add("imp_class", imp_class);
			wholesell.add("summary", summary);
			
			fqm.modWholeSellDiscount(wsd_id, wholesell);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modWholeSellDiscountByWsdId",log);
		}
	}
	
	/**
	 * 删除批发折扣
	 * @param request
	 * @throws Exception
	 */
	public void delWholeSellDiscountByWsdId(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long wsd_id = StringUtil.getLong(request, "wsd_id");
	
			fqm.delWholeSellDiscount(wsd_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delWholeSellDiscountByWsdId",log);
		}
	}
	
	/**
	 * 通过名称获得折扣详细信息
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailWholeSellDiscountByName(String name)
		throws Exception
	{
		try
		{
			return(fqm.getDetailWholeSellDiscountByName(name));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailWholeSellDiscountByName",log);
		}
	}	
	
	/**
	 * 通过实现策略类获得折扣详细信息
	 * @param imp_class
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailWholeSellDiscountByImpClass(String imp_class)
		throws Exception
	{
		try
		{
			return(fqm.getDetailWholeSellDiscountByImpClass(imp_class));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailWholeSellDiscountByImpClass",log);
		}
	}	
	
	/**
	 * 用正则表达式分解规则
	 * 例子：1={0,300,HID套装} 
	 * @param rule
	 * @return 
	 * @throws Exception
	 */
	public String[][] getWholeSellRule2(String ruleStr)
		throws Exception
	{
		try
		{
			String reg = "(\\d+(\\.\\d+)*|\\$\\w+)=\\{(-*\\d+),(\\d+)(,(.+))*\\}";
			int generalPos[] = new int[]{1,3,4,6};
			int subPos[] = new int[]{1,3,4,6};
			int pos[];
			
			if (ruleStr.indexOf("$")>=0)
			{
				pos = generalPos;
			}
			else
			{
				pos = subPos;
			}
			
			String rule[][] = StringUtil.regMatchers(ruleStr, reg, pos);
			
			return(rule);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getWholeSellRule",log);
		}
	}	
	
	public String[][] getWholeSellRule3(String ruleStr)
		throws Exception
	{
		try
		{
			String reg = "(\\d+(\\.\\d+)*|\\$\\w+)=\\{(-*\\d+),(\\d+),(\\d+)(,(.+))*\\}";
			int pos[] = new int[]{1,3,4,5,7};
			
			String rule[][] = StringUtil.regMatchers(ruleStr, reg, pos);
			
			return(rule);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getWholeSellRule3",log);
		}
	}	
	
	/**
	 * 验证折扣设置的价格段是否连续(屏蔽，不做验证)
	 * @param rule
	 * @throws ErrorWholeSellDiscountRuleException
	 * @throws Exception
	 */
	public void validateDiscountRule(String rule[][])
		throws ErrorWholeSellDiscountRuleException, Exception
	{
	
		float preI=0,nexI=0;
		for (int i=0; i<rule.length; i++)
		{
			nexI = StringUtil.getFloat(rule[i][1]);
				
			if (i>0&&preI!=nexI)
			{
				//throw new ErrorWholeSellDiscountRuleException(rule[i][0]+" "+rule[i][1]+" "+rule[i][2]);
			}
			
			preI = StringUtil.getFloat(rule[i][2]);
		}
	}
			
	/**
	 * 成单
	 * @param request
	 * @throws Exception
	 */
	public void finishQuote(HttpServletRequest request)
		throws OrderNotWait4RecordException,OrderSourceErrorException,DuplicateOrderException,TotalMcGrossErrorException,OrderNotFoundException,Exception
	{
		try
		{
			long oid = StringUtil.getLong(request, "oid");
			long qoid = StringUtil.getLong(request, "qoid");
			
			DBRow detailOrder = fom.getDetailPOrderByOid(oid);
			if (detailOrder==null)
			{
				throw new OrderNotFoundException();//订单不存在
			}
//			if (detailOrder.getString("order_source").equals("DIRECTPAY")==false)
//			{
//				throw new OrderSourceErrorException();//订单来源不对
//			} 
//			if (detailOrder.get("handle",0)!=HandleKey.WAIT4_RECORD)
//			{
//				throw new OrderNotWait4RecordException();//订单不是待抄单状态
//			} 

			DBRow detailQuoteOrder = fqm.getDetailQuoteByOid(oid);
			if (detailQuoteOrder!=null)
			{
				throw new DuplicateOrderException(detailQuoteOrder.getString("qoid"));//已经被成单
			}
			
			detailQuoteOrder = fqm.getDetailQuoteByQoid(qoid);
//			if (detailQuoteOrder.get("total_cost", 0d)>orderMgr.getUSDOrderTotalMcGross(oid))
//			{
//				throw new TotalMcGrossErrorException();
//			} 			

			//拷贝采购单信息到关联订单
//			DBRow order = new DBRow();
//			order.add("address_name", detailQuoteOrder.getString("address_name"));
//			order.add("address_country", detailQuoteOrder.getString("address_country"));
//			order.add("address_city", detailQuoteOrder.getString("address_city"));
//			order.add("address_zip", detailQuoteOrder.getString("address_zip"));
//			order.add("address_street", detailQuoteOrder.getString("address_street"));
//			order.add("address_country_code", detailQuoteOrder.getString("address_country_code"));
//			order.add("address_state", detailQuoteOrder.getString("address_state"));
//			order.add("tel", detailQuoteOrder.getString("tel"));
//			order.add("ps_id", detailQuoteOrder.getString("ps_id"));
//			fom.modPOrder(oid, order);

			//先删除原来order_items
//			fom.delPOrderItem(oid);
//			DBRow quoteOrderItems[] = fqm.getQuoteItemsByQoid(qoid, null);
//			for (int i=0; i<quoteOrderItems.length; i++)
//			{
//				DBRow orderItem = new DBRow();
//				orderItem.add("pid", quoteOrderItems[i].getString("pid"));
//				orderItem.add("quantity", quoteOrderItems[i].getString("quantity"));
//				orderItem.add("catalog_id", quoteOrderItems[i].getString("catalog_id"));
//				orderItem.add("unit_price", quoteOrderItems[i].getString("unit_price"));
//				orderItem.add("gross_profit", quoteOrderItems[i].getString("gross_profit"));
//				orderItem.add("weight", quoteOrderItems[i].getString("weight"));
//				orderItem.add("unit_name", quoteOrderItems[i].getString("unit_name"));
//				orderItem.add("name", quoteOrderItems[i].getString("name"));
//				orderItem.add("product_type", quoteOrderItems[i].getString("product_type"));
//				orderItem.add("oid", oid);
//				fom.addPOrderItem(orderItem);
//			}
			
			//修改报价单状态
			DBRow quote = new DBRow();
			quote.add("oid",oid);
			quote.add("quote_status",QuoteKey.FINISH);
			fqm.modQuoteOrder(qoid, quote);
		}
		catch (OrderSourceErrorException e)
		{
			throw e;
		}
		catch (DuplicateOrderException e)
		{
			throw e;
		}
		catch (TotalMcGrossErrorException e)
		{
			throw e;
		}
		catch (OrderNotFoundException e)
		{
			throw e;
		}
		catch (OrderNotWait4RecordException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"finishQuote",log);
		}
	}
	
	/**
	 * 为了提供权限控制，成错单，主管可以修改
	 * @param request
	 * @throws OrderSourceErrorException
	 * @throws DuplicateOrderException
	 * @throws TotalMcGrossErrorException
	 * @throws OrderNotFoundException
	 * @throws Exception
	 */
	public void modFinishQuote(HttpServletRequest request)
		throws OrderNotWait4RecordException,OrderSourceErrorException,DuplicateOrderException,TotalMcGrossErrorException,OrderNotFoundException,Exception
	{
		this.finishQuote(request);
	}
	
	/**
	 * 主管审核
	 * @param request
	 * @throws OrderSourceErrorException
	 * @throws DuplicateOrderException
	 * @throws TotalMcGrossErrorException
	 * @throws OrderNotFoundException
	 * @throws Exception
	 */
	public void approveQuote(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long qoid = StringUtil.getLong(request, "qoid");
			int manager_approve = StringUtil.getInt(request,"manager_approve");
			String approve_note = StringUtil.getString(request, "approve_note");
			
			DBRow quote = new DBRow();
			quote.add("manager_approve",manager_approve);
			quote.add("approve_note",approve_note);
			fqm.modQuoteOrder(qoid, quote);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"approveQuote",log);
		}
	}
		
	/**
	 * 取消
	 * @param request
	 * @throws Exception
	 */
	public void cancelQuote(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long qoid = StringUtil.getLong(request, "qoid");
			
			DBRow quote = new DBRow();
			quote.add("quote_status",QuoteKey.CANCEL);
			fqm.modQuoteOrder(qoid, quote);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"cancelQuote",log);
		}
	}

	/**
	 * 搜索
	 * @param key
	 * @param st
	 * @param en
	 * @param create_account
	 * @param quote_status
	 * @param field
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResult(String key,String st,String en,String create_account,int quote_status,int field,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getSearchResult( key, st, en, create_account, quote_status, field, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getSearchResult",log);
		}
	}
	
	/**
	 * 获得报价单报表模板数据
	 * @param qoid
	 * @return
	 * @throws Exception
	 */
	public DBRow getQuotePDFDatas(long qoid)
		throws Exception
	{
		try
		{
			DBRow result = new DBRow();
			ArrayList<DBRow> itemsAL = new ArrayList<DBRow>();
			DBRow detailQuoteOrder = fqm.getDetailQuoteByQoid(qoid);
			DBRow quoteOrderItems[] = fqm.getQuoteItemsByQoid(qoid,null);
			DBRow detailQuoteAccount = fam.getDetailAdminByAccount(detailQuoteOrder.getString("create_account"));
			
			double total = 0,total_lowest_price = 0,total_quote_price = 0;
			for (int i=0; i<quoteOrderItems.length; i++)
			{
				total = MoneyUtil.mul(quoteOrderItems[i].get("quantity",0d), quoteOrderItems[i].get("quote_price",0d));
				
				//封装普通商品数据
				DBRow item = new DBRow();
				item.add("quantity", quoteOrderItems[i].getString("quantity"));
				item.add("unit_name", quoteOrderItems[i].getString("unit_name"));
				item.add("name", quoteOrderItems[i].getString("name"));
				item.add("lowest_price", quoteOrderItems[i].getString("lowest_price"));
				item.add("quote_price", quoteOrderItems[i].getString("quote_price"));
				item.add("discount",MoneyUtil.round(String.valueOf((10-quoteOrderItems[i].get("discount",0d)) *10), 1));
				item.add("total", String.valueOf(MoneyUtil.round(total, 2)));
				itemsAL.add(item);
				//如果是定制商品，封装定制商品数据
				DBRow customProductsInSet[] = productMgr.getProductsCustomInSetBySetPid( quoteOrderItems[i].get("pid",0l) );
				for (int customProductsInSet_i=0;customProductsInSet_i<customProductsInSet.length;customProductsInSet_i++)
				{
					DBRow customItem = new DBRow();
					customItem.add("name", "  - "+customProductsInSet[customProductsInSet_i].getString("p_name")+" X "+customProductsInSet[customProductsInSet_i].getString("quantity")+" "+customProductsInSet[customProductsInSet_i].getString("unit_name"));
					itemsAL.add(customItem);
				}
				
				total_lowest_price = MoneyUtil.add(total_lowest_price, quoteOrderItems[i].get("lowest_price",0d)*quoteOrderItems[i].get("quantity",0d));
				total_quote_price = MoneyUtil.add(total_quote_price, quoteOrderItems[i].get("quote_price",0d)*quoteOrderItems[i].get("quantity",0d));
			}
			
			//报价单基本数据
			HashMap<String, String> paras = new HashMap<String, String>();
			
			String date = detailQuoteOrder.getString("post_date");
			String expiration_date = detailQuoteOrder.getString("expiration_date");
			
			
			paras.put("qoid",String.valueOf(qoid));
			paras.put("date",DateUtil.gobleDate(date));
			paras.put("expiration_date",DateUtil.gobleDate(expiration_date));
			
			DBRow detailCompany = expressMgr.getDetailCompany(detailQuoteOrder.get("sc_id",0l));
			if (detailCompany==null)
			{
				paras.put("shipping","?");
			}
			else
			{
				paras.put("shipping",detailCompany.getString("name"));
			}
			
			String address2 = "";
			if (detailQuoteOrder.getString("address_city").equals("")==false)
			{
				address2 += detailQuoteOrder.getString("address_city")+",";
			}
			if (detailQuoteOrder.getString("address_state").equals("")==false)
			{
				address2 += detailQuoteOrder.getString("address_state")+",";
			}
			if (detailQuoteOrder.getString("address_zip").equals("")==false)
			{
				address2 += detailQuoteOrder.getString("address_zip")+",";
			}
			address2 += orderMgr.getDetailCountryCodeByCcid(detailQuoteOrder.get("ccid",0l)).getString("c_country");
			
			paras.put("create_account",detailQuoteOrder.getString("create_account")+" / Email: "+detailQuoteAccount.getString("email")+" / MSN: "+detailQuoteAccount.getString("msn"));
			paras.put("name",detailQuoteOrder.getString("address_name"));
			paras.put("address1",detailQuoteOrder.getString("address_street"));
			paras.put("address2",address2);
			paras.put("tel",detailQuoteOrder.getString("tel"));
			paras.put("email",detailQuoteOrder.getString("client_id"));
			
			paras.put("subtotal",String.valueOf(MoneyUtil.round(detailQuoteOrder.get("product_cost",0d), 2)));
			paras.put("shipping_fee",String.valueOf(MoneyUtil.round(detailQuoteOrder.get("shipping_cost",0d),2)));
			paras.put("total",StringUtil.formatNumber("#.00", detailQuoteOrder.get("total_cost",0d)));
			paras.put("saving",String.valueOf(MoneyUtil.round(MoneyUtil.sub(total_lowest_price, total_quote_price), 2)));
			
			//输出数据
			result.add("view_path", ReportCenter.REPORT_VIEW_ROOTPATH+"quote/quote_detail.jasper");//绑定数据的模板绝对路径
			result.add("paras", paras);
			result.add("items", (DBRow[])itemsAL.toArray(new DBRow[0]));
			
			return(result);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getQuoteItems4PDF",log);
		}
		
		//参考数据格式
		/**
		HashMap<String, String> paras = new HashMap<String, String>();
		
		paras.put("qoid","201212");
		paras.put("date","2011-1-23");
		paras.put("expiration_date","2011-1-23");
		
		paras.put("name","Tarif Queen");
		paras.put("address1","1679 Allegheny Dr");
		paras.put("address2","Blakeslee,PA,18610,United States");
		paras.put("tel","9739194081");
		paras.put("email","mr_valencia_h@yahoo.com");
		
		paras.put("subtotal","$213456");
		paras.put("shipping_fee","$213");
		paras.put("total","$5134444");
		
		ArrayList<DBRow> dataAl = new ArrayList<DBRow>();
		for (int i=0; i<40; i++)
		{
			DBRow row1 = new DBRow();
			row1.add("quantity", "1");
			row1.add("unit_name", "SET");
			row1.add("name", "KIT/12V55W/H1/5K");
			row1.add("lowest_price", "35.88");
			row1.add("quote_price", "35.88");
			row1.add("discount", "100");
			row1.add("total", "35.88");
			dataAl.add(row1);

			
			
			
			DBRow row2 = new DBRow();
			row2.add("quantity", "2");
			row2.add("unit_name", "ITEM");
			row2.add("name", "KIT/SLIM/9006/PINK(Customize)");
			row2.add("lowest_price", "45.38");
			row2.add("quote_price", "45.38");
			row2.add("discount", "100");
			row2.add("total", "45.38");
			dataAl.add(row2);
			
			DBRow row3 = new DBRow();
			row3.add("name", "  ├ BULB/9006/PINK X 1 PAIR");
			dataAl.add(row3);
			
			DBRow row4 = new DBRow();
			row4.add("name", "  ├ RELAY/8801H1H3H7H11 X 2 ITEM");
			dataAl.add(row4);
			
			DBRow row5 = new DBRow();
			row5.add("name", "  ├ BALLAST/SLIM/35W X 2 ITEM");
			dataAl.add(row5);
			
			
			
			
			
			DBRow row6 = new DBRow();
			row6.add("quantity", "2");
			row6.add("unit_name", "PAIR");
			row6.add("name", "BULB/H4-2/12K");
			row6.add("lowest_price", "10.03");
			row6.add("quote_price", "10.03");
			row6.add("discount", "100");
			row6.add("total", "20.06");
			dataAl.add(row6);
			
			
		}
		**/
	}
	
	/**
	 * 通过邮件发送报价单给顾客
	 * @param request
	 * @throws Exception
	 */
	public void sendQuotationViaEmail(HttpServletRequest request)
		throws Exception
	{
		FileOutputStream fos = null;

		try
		{
			long qoid = StringUtil.getLong(request, "qoid");
			DBRow detailQuoteOrder = fqm.getDetailQuoteByQoid(qoid);
			DBRow detailQuoteAccount = fam.getDetailAdminByAccount(detailQuoteOrder.getString("create_account"));
			String quotationPDFTmpFolder = "quotation_pdf_tmp";//存放报表附件的临时文件夹
			String quotationPDFPath;//PDF文件完整路径
			
			//先生成报价单PDF附件文件到临时目录
			File tmpFolder = new File(Environment.getHome()+quotationPDFTmpFolder);
			if (tmpFolder.exists()==false)//不存在则创建
			{
				tmpFolder.mkdir();
			}
			quotationPDFPath = Environment.getHome()+quotationPDFTmpFolder+"/VisionariQuote"+qoid+".pdf";
			//如果之前已经生成PDF，则先删除
			File pdfFile = new File(quotationPDFPath);
			if (pdfFile.exists())
			{
				pdfFile.delete();
			}
			//准备报表数据
			DBRow result = this.getQuotePDFDatas(qoid);
			String reporViewTemplatetFilePath = result.getString("view_path");
			DBRow dataRows[] = (DBRow[])result.get("items",new Object());
			HashMap<String, String> paras = (HashMap<String, String>)result.get("paras",new Object());
			//生成报表
			ReportCenter reportCenter = new ReportCenter(reporViewTemplatetFilePath, paras,dataRows); 
			JasperPrint printer = reportCenter.getPrinter();
			//生成PDF
			byte[] bReport = JasperExportManager.exportReportToPdf(printer);
			fos = new FileOutputStream(quotationPDFPath,true); 
			fos.write(bReport);   
			fos.flush(); 
			
			//发送邮件
			Attach attach = new Attach(quotationPDFPath);  
			
			QuoteEmailPageString returnVoiceEmailPageString = new QuoteEmailPageString(qoid);
			User user = new User(returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getPassWord(),returnVoiceEmailPageString.getHost(),returnVoiceEmailPageString.getPort(),returnVoiceEmailPageString.getUserName(),returnVoiceEmailPageString.getAuthor(),returnVoiceEmailPageString.getRePly());
            MailAddress mailAddress = new MailAddress(detailQuoteOrder.getString("client_id"));
            MailBody mailBody = new MailBody(returnVoiceEmailPageString,true,attach);

            Mail mail = new Mail(user);
            mail.setAddress(mailAddress);
            mail.setMailBody(mailBody);
            mail.send();
		}
		catch (Exception e)
		{
			throw new SystemException(e,"sendQuotationViaEmail",log);
		}
		finally
		{
			try
			{
				fos.close();
			}
			catch (Exception e)
			{
				
			}
		}
	}
	
	/**
	 * 通过商品ID获得商品国家毛利率
	 * @param pid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCountryProfitByPid(long pid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getProductCountryProfitByPid( pid, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getProductCountryProfitByPid",log);
		}
	}
	
	/**
	 * 导出报价表
	 * @param request
	 * @throws Exception
	 */
	public String exportProfitQuote(HttpServletRequest request) 
		throws WareHouseErrorException,Exception 
	{
		FileOutputStream fout = null;
		
		try 
		{
			AdminLoginBean adminLoginBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
			
			long pcid = StringUtil.getLong(request,"pcid");
			String cmd = StringUtil.getString(request,"cmd");
			String key = StringUtil.getString(request,"key");
			int union_flag = StringUtil.getInt(request,"union_flag");
			long pro_line_id = StringUtil.getLong(request,"pro_line_id");
			
			long ps_id = StringUtil.getLong(request,"ps_id");
			long ca_id = StringUtil.getLong(request,"ca_id");
			
			DBRow detailStorage = fcm.getDetailProductStorageCatalogById(ps_id);
			DBRow detailCountryArea = fpm.getDetailCountryAreaByCaId(ca_id);
			
			//先校验下发货仓库是否能发货
			boolean isDomesticFlag = expressMgr.isDomesticShipping(detailCountryArea.get("sig_country", 0l), ps_id);
			DBRow express[];
			if (isDomesticFlag)//国内发货
			{
				express = expressMgr.getDomesticExpressCompanyByPsIdProId( ps_id, detailCountryArea.get("sig_state", 0l), 1, null);
			}
			else
			{
				express = expressMgr.getInternationalExpressCompanyByPsIdProId(ps_id,detailCountryArea.get("sig_country", 0l),1,null);
			}
			if (express.length==0)
			{
				throw new WareHouseErrorException();
			}
			
			//根据页面查询条件，获得商品数据
			DBRow products[];
			if (cmd.equals("filter"))
			{
				products = productMgrZJ.filterProduct(pcid,pro_line_id,union_flag,0,0,null);
			}
			else if (cmd.equals("search"))
			{
				products = productMgr.getSearchProducts4CT(key,pcid,null,adminLoginBean);
			}
			else
			{
				products = productMgr.getAllProducts(null);	
			}
			
			//拼装第一个sheet的名字
			String sheetName = detailCountryArea.getString("name") + " - " + detailStorage.getString("title")+" warehouse ";
			
			//获得模板文件
			POIFSFileSystem exportProfitModel = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/product/ExportProfitModel.xls"));
			HSSFWorkbook workbook = new HSSFWorkbook(exportProfitModel);
			
			//先获得第一个sheet的title记录
			workbook.setSheetName(0,sheetName + " 报价");//把第一个sheet改名
			HSSFSheet firstSheet = workbook.getSheetAt(0);
			//firstSheet.protectSheet(String.valueOf(System.currentTimeMillis()));//密码保护
			
			//生成首页商品数据//ShippingInfoBean shippingInfoBeans[]
			Map<String,ShippingInfoBean[]> productsShipping = createSheetProducts(products,workbook,firstSheet,ca_id,ps_id);//国内发货，不属于任何分区
			
			//创建运费sheet
			HSSFSheet firstShippingSheet = workbook.getSheetAt(1);;
			//firstShippingSheet.protectSheet(String.valueOf(System.currentTimeMillis()));
			//插入快递数据
			int productsShippingRowI = 1;
			for(Map.Entry<String,ShippingInfoBean[]> entry : productsShipping.entrySet())
			{
				String pid = entry.getKey();
				ShippingInfoBean[] shippingInfoBeans = entry.getValue();
				
				for (int si=0; si<shippingInfoBeans.length; si++)
				{
					HSSFRow shippingRow = firstShippingSheet.createRow(productsShippingRowI++);
					
					HSSFCell shippingCell = shippingRow.createCell(0);
					shippingCell.setCellValue(pid);
					
					shippingCell = shippingRow.createCell(1);
					shippingCell.setCellValue(shippingInfoBeans[si].getCompanyName());
					
					shippingCell = shippingRow.createCell(2);
					shippingCell.setCellValue(shippingInfoBeans[si].getShippingFee());
					
					shippingCell = shippingRow.createCell(3);
					shippingCell.setCellValue(shippingInfoBeans[si].getSc_id());
				}
			}
			//生成数据页面
			HSSFSheet currencySheet = workbook.getSheetAt(2);
			//currencySheet.protectSheet(String.valueOf(System.currentTimeMillis()));
			HSSFRow currencyRow = currencySheet.createRow(0);
			HSSFCellStyle currencyStyle = workbook.createCellStyle();
			currencyStyle.setWrapText(true);
			currencyStyle.setLocked(true);
			
			HSSFCell currentCell = currencyRow.createCell(0);
			currentCell.setCellValue("USD");//美元汇率
			currentCell.setCellStyle(currencyStyle);
			currentCell = currencyRow.createCell(1);
			currentCell.setCellValue(systemConfig.getDoubleConfigValue("USD"));
			currentCell.setCellStyle(currencyStyle);
	
			currencyRow = currencySheet.createRow(1);
			currentCell = currencyRow.createCell(0);
			currentCell.setCellValue("ps_id");
			currentCell.setCellStyle(currencyStyle);
			currentCell = currencyRow.createCell(1);
			currentCell.setCellValue(String.valueOf(ps_id));
			currentCell.setCellStyle(currencyStyle);
			
			currencyRow = currencySheet.createRow(2);
			currentCell = currencyRow.createCell(0);
			currentCell.setCellValue("ca_id");
			currentCell.setCellStyle(currencyStyle);
			currentCell = currencyRow.createCell(1);
			currentCell.setCellValue(String.valueOf(ca_id));
			currentCell.setCellStyle(currencyStyle);
			
			//生成excel文件
			String path = "upl_excel_tmp/profit_quote_"+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()) + ".xls";
			fout = new FileOutputStream(Environment.getHome()+ path);
			workbook.write(fout);
			
			return(path);
	    } 
		catch (WareHouseErrorException e) 
		{
			throw e;
	    }
		catch (Exception e) 
		{
			throw new SystemException(e,"exportProfitQuote",log);
	    }
		finally
		{
			if (fout!=null)
			{
				fout.close();
			}
		}
	}
	
	/**
	 * 生成sheet商品内容
	 * @param products  商品
	 * @param workBook
	 * @param sheet
	 * @param ps_id	发货仓库
	 * @param ccid  收货国家
	 * @param pro_id  收货地区
	 * @throws Exception
	 */
	private HashMap<String,ShippingInfoBean[]> createSheetProducts(DBRow products[],HSSFWorkbook workBook,HSSFSheet sheet,long ca_id,long ps_id)
		throws Exception 
	{
		Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
		ShippingInfoBean shippingInfoBeans[] = null; //存放每个商品快递信息
		HashMap<String,ShippingInfoBean[]> productsShipping = new HashMap<String,ShippingInfoBean[]>();
		double quote_price = 0;//零售报价
		//获得销售地区代表国家和州的数据
		DBRow detailCountryArea = fpm.getDetailCountryAreaByCaId(ca_id);
		long ccid = detailCountryArea.get("sig_country", 0l);
		long pro_id = detailCountryArea.get("sig_state", 0l);
		//判断是否国内发货
		boolean isDomesticFlag = expressMgr.isDomesticShipping(ccid, ps_id);
		
		sheet.setColumnWidth(17,0);//设置第O列隐藏，原先的商品利润
		
		for (int j=0; j<products.length; j++)
		{
			HSSFRow productRow = sheet.createRow(j+1);
//			HSSFCellStyle style = workBook.createCellStyle();
//			style.setWrapText(true);
		
			
			HSSFCell currentCell = productRow.createCell(0);
			currentCell.setCellValue(products[j].getString("pc_id"));
//			currentCell.setCellStyle(style);
			
			currentCell = productRow.createCell(1);
			currentCell.setCellValue(products[j].getString("p_name"));
//			currentCell.setCellStyle(style);
			
			//处理货架显示形式
			StringBuffer sb = new StringBuffer();
			DBRow allFather[] = tree.getAllFather(products[j].get("catalog_id",0l));
			for (int jj=0; jj<allFather.length-1; jj++)
			{
			  	sb.append(allFather[jj].getString("title")+">");
			}
			DBRow catalog = fcm.getDetailProductCatalogById(products[j].get("catalog_id",0l));
			if (catalog!=null)
			{
			 	sb.append(catalog.getString("title"));
			}
			
			currentCell = productRow.createCell(2);
			currentCell.setCellValue(sb.toString());
//			currentCell.setCellStyle(style);
			
			currentCell = productRow.createCell(3);
			currentCell.setCellValue(products[j].getString("unit_name"));
//			currentCell.setCellStyle(style);
			
			currentCell = productRow.createCell(4);
			currentCell.setCellValue(products[j].getString("weight"));
//			currentCell.setCellStyle(style);
			
			//计算修正后的商品成本
			double unit_price = products[j].get("unit_price",0d);//orderMgr.getProductMergeCost(products[j].get("unit_price",0d));//orderMgr.getOrderMergeCost(OrderMgr.ORDER_SOURCE_EBAY,, 0);
			unit_price = MoneyUtil.round(unit_price,2);
			
			currentCell = productRow.createCell(5);
			currentCell.setCellValue(unit_price);
//			currentCell.setCellStyle(style);
			
			
			//对重量进行修正
//			float weight_cost_coefficient = systemConfig.getFloatConfigValue( "weight_cost_coefficient" );
			
//			修改报价詹洁
			float totalWeight = products[j].get("weight",0f);//* weight_cost_coefficient;
			products[j].add("cart_pid",products[j].get("pc_id",0l));
			//计算处理该地区适合发哪个快递和运费
			//先判断是国内发货还是国际发货
			DBRow express[];
			if (isDomesticFlag)//国内发货
			{
				express = expressMgr.getDomesticExpressCompanyByPsIdProId( ps_id, pro_id,products[j].get("weight",0f) , null);
			}
			else
			{
				express = expressMgr.getInternationalExpressCompanyByPsIdProId(ps_id,ccid,products[j].get("weight",0f),null);
			}
			
			shippingInfoBeans = new ShippingInfoBean[express.length];
			for (int k=0; k<express.length; k++)
			{
				ShippingInfoBean shippingInfoBean;
				try 
				{
					shippingInfoBean = expressMgr.getShippingFeeForProduct(express[k].get("sc_id",0l), ccid, pro_id,new DBRow[]{products[j]});//expressMgr.getShippingFee(express[k].get("sc_id",0l),totalWeight, ccid,pro_id);
					
					shippingInfoBean.setSc_id(express[k].get("sc_id",0l));//塞入快递公司ID
					//对于运费进行修正
//					shippingInfoBean.setShippingFee( getLowestShippingPrice(shippingInfoBean.getShippingFee()) );
					shippingInfoBeans[k] = shippingInfoBean;
				} 
				catch (ProvinceOutSizeException e)
				{
					continue;
				}
				catch (CountryOutSizeException e)
				{
					continue;
				}
				catch (WeightCrossException e)
				{
					continue;
				}
				catch (WeightOutSizeException e)
				{
					continue;
				}
			}
			
			//运费为0的快递从快递列表剔除
			ArrayList<ShippingInfoBean> shippingInfoBeanlist = new ArrayList<ShippingInfoBean>();
			for (int i = 0; i <shippingInfoBeans.length; i++) 
			{
				if(shippingInfoBeans[i].getShippingFee()>0)
				{
					shippingInfoBeanlist.add(shippingInfoBeans[i]);
				}
			}
			shippingInfoBeans = shippingInfoBeanlist.toArray(new ShippingInfoBean[0]);
			
			
			//按照运费升密排序(最小的排在前面)
			ShippingInfoBean tmpShippingInfoBean;
			for (int sorti=0; sorti<shippingInfoBeans.length; sorti++)
			{
				for (int sortj=sorti+1; sortj<shippingInfoBeans.length; sortj++)
				{
					if (shippingInfoBeans[sorti].getShippingFee()>shippingInfoBeans[sortj].getShippingFee()&&shippingInfoBeans[sortj].getShippingFee()>0)
					{
						tmpShippingInfoBean = shippingInfoBeans[sorti];
						shippingInfoBeans[sorti] = shippingInfoBeans[sortj];
						shippingInfoBeans[sortj] = tmpShippingInfoBean;
					}
				}
			}
			productsShipping.put(products[j].getString("pc_id"), shippingInfoBeans);

			/**
			 * 国内发货，使用默认毛利，计算所有商品零售价
			 * 国家、分区报价，都是直接读取国家毛利
			 **/
			double gross_profit = 0;
			
			double old_gross_profit = 0;//旧的利润
			DBRow detailProductCountryProfit = fqm.getDetailProductCountryProfitByPidCcId( products[j].get("pc_id",0l),ccid);
			if (detailProductCountryProfit!=null||(detailProductCountryProfit!=null&&detailProductCountryProfit.get("profit", 0d)>0))
			{
				gross_profit = detailProductCountryProfit.get("profit", 0d);
				old_gross_profit = detailProductCountryProfit.get("profit", 0d);
			}
			else
			{
      			//如果原来商品没有定价，则设定毛利=35%
				
				gross_profit = quoteMgrZJ.getProductProfitValue(ccid, pro_id,products[j].get("pc_id",0l), ps_id, 1);
				
				
				
				/**
	     		 * 如果没有合适快递，设置毛利为0
				 */
//				if (shippingInfoBeans.length>0)
//				{
//					
//					gross_profit = (unit_price + shippingInfoBeans[0].getShippingFee())*0.42857d;
//					gross_profit = MoneyUtil.round(gross_profit, 2); 
//				}
//				else
//				{
//					gross_profit = 0;
//				}
								
			}

//			HSSFCellStyle unLockShippStyle = workBook.createCellStyle();
//			unLockShippStyle.setLocked(false);
			
			currentCell = productRow.createCell(6);
			currentCell.setCellValue(gross_profit);
//			currentCell.setCellStyle(unLockShippStyle);
			
			
			/**
			 * 最后计算零售报价（美元）
			 * 如果没有合适快递，零售报价为0
			 */
			if (shippingInfoBeans.length>0)
			{
				quote_price = unit_price + gross_profit + shippingInfoBeans[0].getShippingFee();
				quote_price = MoneyUtil.div(String.valueOf(quote_price), systemConfig.getStringConfigValue("USD"));
				quote_price = MoneyUtil.round(quote_price, 2); 
			}
			else
			{
				quote_price = 0;
			}

			//发货快递
			
			currentCell = productRow.createCell(7);
			if (shippingInfoBeans.length>0)
			{
				currentCell.setCellValue(shippingInfoBeans[0].getCompanyName());
			}
			else
			{
				currentCell.setCellValue("");
			}
//			currentCell.setCellStyle(unLockShippStyle);
			
			if (shippingInfoBeans.length>0)
			{
				currentCell = productRow.createCell(15);
				currentCell.setCellValue(MoneyUtil.round(shippingInfoBeans[0].getShippingFee(), 2));
				
				currentCell = productRow.createCell(18);
				currentCell.setCellValue(shippingInfoBeans[0].getSc_id());
			}
			
			HSSFCellStyle unLockStyle = workBook.createCellStyle();
			unLockStyle.setLocked(false);
			currentCell = productRow.createCell(8);
			currentCell.setCellValue(quote_price);
			
			//临时存放运费
			currentCell = productRow.createCell(9);
			if (shippingInfoBeans.length>0)
			{
				currentCell.setCellValue(shippingInfoBeans[0].getShippingFee());
			}
			else
			{
				currentCell.setCellValue(0);
			}
			
			//之前的报价
			double old_price=0;
			if (detailProductCountryProfit!=null)
			{
				DBRow tmp = fqm.getDetailRetailPriceItemByRpiId(detailProductCountryProfit.get("rpi_id", 0l));
				if (tmp!=null)
				{
					old_price = tmp.get("new_retail_price", 0d);
				}
			}
			currentCell = productRow.createCell(16);
			currentCell.setCellValue(old_price);
			
			currentCell = productRow.createCell(14);
			double rate_of_marging = MoneyUtil.round(gross_profit/(Double.valueOf(systemConfig.getStringConfigValue("USD"))*quote_price),4);
			currentCell.setCellValue(rate_of_marging);
			
			currentCell = productRow.createCell(17);
			currentCell.setCellValue(old_gross_profit);
			
			currentCell = productRow.createCell(19);//eBay Fee ($)
			double eBayFee =  MoneyUtil.mul(quote_price,0.08d);
			currentCell.setCellValue(MoneyUtil.round(eBayFee,2));
			
			double paypalFee = MoneyUtil.mul(quote_price,0.03d);
			currentCell = productRow.createCell(20);//PayPal Fee ($)
			currentCell.setCellValue(MoneyUtil.round(paypalFee,2));
			
			double retained_profits = MoneyUtil.round(gross_profit/Double.valueOf(systemConfig.getStringConfigValue("USD"))-(eBayFee+paypalFee),2) ; 
			currentCell = productRow.createCell(21);//净利(￥)
			currentCell.setCellValue(MoneyUtil.mul(retained_profits,Double.valueOf(systemConfig.getStringConfigValue("USD"))));
			
			currentCell = productRow.createCell(22);//净利率
			currentCell.setCellValue(retained_profits/quote_price);
			
		}
		
		//测试合并单元格
		//sheet.addMergedRegion(new CellRangeAddress(2,3,2,3));
		
		return(productsShipping);
	}
	
	/**
	 * 导入商品价格EXCEL表
	 * @param request
	 * @return
	 * @throws Exception
	 * @throws FileTypeException
	 */
	public String importProductProfit(HttpServletRequest request)
		throws Exception,FileTypeException
	{
		try 
		{
			String permitFile = "xls";
			TUpload upload = new TUpload();
			String msg = new String();

			upload.setFileName("import_profit_quote_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			upload.setFileSize(1024*1024*10);
			
			int flag = upload.upload(request);
			
				
			if (flag==2)
			{
				msg = "只能上传:"+permitFile;
				throw new FileTypeException(msg);//采购单文件上传格式不对
			}
			else if (flag==1)
			{
				msg = "上传出错";
				throw new FileTypeException(msg);
			}
			else
			{		  
				msg = upload.getFileName();
			}
			
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"importProductProfit",log);
		}
	}
	
	/**
	 * 检查每行商品的零售报价格式
	 * @param filename
	 * @return
	 * @throws Exception
	 */
//	public HashMap<String,DBRow[]> checkExcel(String filename)
//		throws Exception
//	{
//		InputStream is = null;
//		Workbook rwb = null;  
//
//		try 
//		{
//			String path = "";
//			HashMap<String,DBRow[]> resultMap = new HashMap<String, DBRow[]>();//sheet_name-错误行
//			path = Environment.getHome() + "upl_excel_tmp/" + filename;
//			is = new FileInputStream(path);
//			rwb = Workbook.getWorkbook(is);
//			
//			Pattern p = Pattern.compile("^(\\d+)(\\.\\d+)?$");//整数或浮点数
//			
//			String sheetNames[] = rwb.getSheetNames();
//			for (int i=0; i<sheetNames.length; i++)
//			{
//				Sheet rs = rwb.getSheet(sheetNames[i]);
//				//int rsColumns = rs.getColumns();  //excel表字段数
//				int rsRows = rs.getRows();    //excel表记录行数
//				
//				ArrayList<DBRow> errorRowList = new ArrayList<DBRow>();
//				for (int y=1; y<rsRows; y++)
//				{
//					String price = rs.getCell(8,y).getContents().trim();
//					Matcher m = p.matcher(price);
//					boolean b = m.matches();
//					
//					if (price.equals("0")||b==false)
//					{
//						DBRow errorRow = new DBRow();
//						errorRow.add("name", rs.getCell(1,y).getContents().trim());
//						errorRow.add("quote_price", price);
//						errorRowList.add(errorRow);
//					}
//				}
//				resultMap.put(sheetNames[i], (DBRow[])errorRowList.toArray(new DBRow[0]));
//			}
//			
//			return resultMap;
//		} 
//		catch (Exception e) 
//		{
//			throw new SystemException(e,"checkExcel",log);
//		}
//		finally
//		{
//			if (is!=null)
//			{
//				is.close();
//			}
//			if (rwb!=null)
//			{
//				rwb.close();
//			}
//		}
//	}
	
	public DBRow[] checkExcel(String filename)
		throws Exception
	{
		InputStream is = null;
		Workbook rwb = null;  
	
		try 
		{
			String path = "";
			path = Environment.getHome() + "upl_excel_tmp/" + filename;
			is = new FileInputStream(path);
			rwb = Workbook.getWorkbook(is);
			ArrayList<DBRow> errorRowList = new ArrayList<DBRow>();
			
			Pattern p = Pattern.compile("^(\\d+)(\\.\\d+)?$");//整数或浮点数
			
			Sheet rs = rwb.getSheet(0);
			//int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
			
			//获得汇率
			Sheet paramRs = rwb.getSheet(2);
			
			for (int y=1; y<rsRows; y++)
			{
				//空行不做处理
				if (rs.getCell(0,y).getContents().trim().equals(""))
				{
					continue;
				}
				
				String price = rs.getCell(8,y).getContents().trim();
				Matcher m = p.matcher(price);
				boolean b = m.matches();
//				boolean IfLostMoney=
				
				boolean ifDataRelationShipRight = true;//判断数据间关系是否正确
				
				String maori = rs.getCell(6,y).getContents().trim();
				Matcher maoriM = p.matcher(maori);
				
				ifDataRelationShipRight = maoriM.matches();
				
				if(ifDataRelationShipRight==true&&b==true)
				{
					double new_retail_price = StringUtil.getDouble(rs.getCell(8,y).getContents().trim());
					double old_retail_price = ((NumberCell)rs.getCell(5,y)).getValue()+((NumberCell)rs.getCell(6,y)).getValue()+getShippingFeeByPidExpress(rwb.getSheet(1),rs.getCell(0,y).getContents().trim(),rs.getCell(7,y).getContents().trim());
					double exchangeRate = StringUtil.getDouble(paramRs.getCell(1,0).getContents().trim());
					old_retail_price = MoneyUtil.round(old_retail_price/exchangeRate, 2);
					
					if (new_retail_price!=old_retail_price)
					{
						ifDataRelationShipRight = false;
					}
				}
				
				
				
				//!this.rightRetailPrice( ((NumberCell)paramRs.getCell(1,0)).getValue() ,((NumberCell)rs.getCell(5,y)).getValue() , ((NumberCell)rs.getCell(9,y)).getValue(),((NumberCell)rs.getCell(8,y)).getValue() )
				if (price.equals("0")||b==false||ifDataRelationShipRight==false||(ifDataRelationShipRight==true&&Double.valueOf(maori)<0))
				{
					DBRow errorRow = new DBRow();
					errorRow.add("name", rs.getCell(1,y).getContents().trim());
					errorRow.add("quote_price", price);
					
					if (b==false)
					{
						errorRow.add("error_type", RetailPriceErrorKey.NOT_NUMBER);
					}
					else if (price.equals("0"))
					{
						errorRow.add("error_type", RetailPriceErrorKey.ZERO);
					}
					else if(ifDataRelationShipRight==false)
					{
						errorRow.add("error_type",RetailPriceErrorKey.FORMAT_ERRO);
					}
//					else
//					{
//						errorRow.add("error_type", RetailPriceErrorKey.LOST_MONEY);
//						errorRow.add("lowest_price", getLowestRetailPrice(((NumberCell)paramRs.getCell(1,0)).getValue() ,((NumberCell)rs.getCell(5,y)).getValue() , ((NumberCell)rs.getCell(9,y)).getValue()) );
//					}
					
					errorRowList.add(errorRow);
				}
			}
			
			return((DBRow[])errorRowList.toArray(new DBRow[0]));
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"checkExcel",log);
		}
		finally
		{
			if (is!=null)
			{
				is.close();
			}
			if (rwb!=null)
			{
				rwb.close();
			}
		}
	}
	
	/**
	 * 零售报价<商品成本+运费，则亏本，禁止导入
	 * @param price
	 * @param shipping_fee
	 * @param retail_price
	 * @return
	 */
	public boolean rightRetailPrice(double rate,double price,double shipping_fee,double retail_price)
	{
		////system.out.println( retail_price +" - "+price+" - "+shipping_fee+" => "+ MoneyUtil.round((price+shipping_fee)/rate,2) );
		////system.out.println( retail_price >= MoneyUtil.round((price+shipping_fee)/rate,2) );
		
		return( retail_price >= getLowestRetailPrice(rate, price,shipping_fee) );
	}

	/**
	 * 计算最低零售报价（商品成本+运费）
	 * @param rate
	 * @param price
	 * @param shipping_fee
	 * @return
	 */
	private double getLowestRetailPrice(double rate,double price,double shipping_fee)
	{
		return( MoneyUtil.round((price+shipping_fee)/rate,2) );
	}

	/**
	 * 从excel获得新修改零售报价商品
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public RetailPriceQuoteBean getChangeRetailPriceProducts(String filename)
		throws Exception
	{
		InputStream is = null;
		Workbook rwb = null;  
	
		try 
		{
			String path = "";
			path = Environment.getHome() + "upl_excel_tmp/" + filename;
			is = new FileInputStream(path);
			rwb = Workbook.getWorkbook(is);
			ArrayList<DBRow> changedProductsList = new ArrayList<DBRow>();
			
			Sheet sheet = rwb.getSheet(0);
			int rowCount = sheet.getRows();    //excel表记录行数
			
			Sheet paramSheet = rwb.getSheet(2);
			String currency = paramSheet.getCell(0,0).getContents().trim();
			double exchangeRate = StringUtil.getDouble(paramSheet.getCell(1,0).getContents().trim());
			long psId = StringUtil.getLong(paramSheet.getCell(1,1).getContents().trim());
			String psName = fcm.getDetailProductStorageCatalogById(psId).getString("title");
			long caId = StringUtil.getLong(paramSheet.getCell(1,2).getContents().trim());
			String countryArea = fpm.getDetailCountryAreaByCaId(caId).getString("name"); 

			for (int y=1; y<rowCount; y++)
			{
				//不处理空行
				if (sheet.getCell(0,y).getContents().trim().equals(""))
				{
					continue;
				}
				double new_retail_price = StringUtil.getDouble(sheet.getCell(8,y).getContents().trim());
				double old_retail_price = StringUtil.getDouble(sheet.getCell(16,y).getContents().trim());
				
				double maori = StringUtil.getDouble(sheet.getCell(6,y).getContents().trim());//当前毛利
				double oldMaori = StringUtil.getDouble(sheet.getCell(17,y).getContents().trim());//之前毛利
				
//				double old_retail_price = ((NumberCell)sheet.getCell(5,y)).getValue()+((NumberCell)sheet.getCell(6,y)).getValue()+getShippingFeeByPidExpress(rwb.getSheet(1),sheet.getCell(0,y).getContents().trim(),sheet.getCell(7,y).getContents().trim());
//				old_retail_price = MoneyUtil.round(old_retail_price/exchangeRate, 2);
				
				if (new_retail_price!=old_retail_price||maori!=oldMaori)
				{
					DBRow product = new DBRow();
					product.add("pid", sheet.getCell(0,y).getContents().trim());
					product.add("name", sheet.getCell(1,y).getContents().trim());
					product.add("new_retail_price", sheet.getCell(8,y).getContents().trim());
					product.add("old_retail_price", old_retail_price);
					product.add("price", ((NumberCell)sheet.getCell(5,y)).getValue());
					product.add("shipping_fee", ((NumberCell)sheet.getCell(9,y)).getValue());
					product.add("gross_profit",((NumberCell)sheet.getCell(6,y)).getValue());
					product.add("sc_id",((NumberCell)sheet.getCell(18,y)).getValue());

					changedProductsList.add(product);
				}
			}
			
			
			RetailPriceQuoteBean retailPriceQuoteBean = new RetailPriceQuoteBean();
			retailPriceQuoteBean.setProducts((DBRow[])changedProductsList.toArray(new DBRow[0]));
			retailPriceQuoteBean.setCaId(caId);
			retailPriceQuoteBean.setCountryArea(countryArea);
			retailPriceQuoteBean.setCurrency(currency);
			retailPriceQuoteBean.setExchangeRate(exchangeRate);
			retailPriceQuoteBean.setPsId(psId);
			retailPriceQuoteBean.setPsName(psName);
			
			return(retailPriceQuoteBean);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"getChangeRetailPriceProducts",log);
		}
		finally
		{
			if (is!=null)
			{
				is.close();
			}
			if (rwb!=null)
			{
				rwb.close();
			}
		}
	}
	
	/**
	 * 根据商品ID和快递，获得对应运费
	 * @param shippingSheet
	 * @param pid
	 * @param express
	 * @return
	 * @throws Exception
	 */
	private double getShippingFeeByPidExpress(Sheet shippingSheet,String pid,String express)
		throws Exception
	{
		int rowCount = shippingSheet.getRows();
		HashMap<String,String> shippingMap = new HashMap<String,String>();
		
		for (int y=1; y<rowCount; y++)
		{
			if ( shippingSheet.getCell(0,y).getContents().trim().equals(pid) )
			{
				shippingMap.put(shippingSheet.getCell(1,y).getContents().trim(), String.valueOf( ((NumberCell)shippingSheet.getCell(2,y)).getValue() ));
			}
		}
		
		return( StringUtil.getDouble(shippingMap.get(express)) );	
	}
	
	/**
	 * 申请调整商品零售价格
	 * @return
	 * @throws Exception
	 */
	public long addRetailPriceChange(HttpServletRequest request)
		throws Exception
	{
		try
		{
			AdminLoginBean adminInfo = adminMgr.getAdminLoginBean( StringUtil.getSession(request) );
			String date = DateUtil.NowStr();
			String excel_file = StringUtil.getString(request, "excel_file");
			
			String reasons = StringUtil.getString(request,"reasons");
			String ask = adminInfo.getAccount();
			String ask_date = date;
			String approve_date = date;
			
			RetailPriceQuoteBean retailPriceQuoteBean = getChangeRetailPriceProducts(excel_file);
			
			DBRow retailPrice = new DBRow();
			retailPrice.add("reasons", reasons);
			retailPrice.add("ask", ask);
			retailPrice.add("ask_date", ask_date);
			retailPrice.add("handle_status", 0);
			retailPrice.add("reject_reasons", "");
			retailPrice.add("approve_date", approve_date);
			retailPrice.add("ps_id", retailPriceQuoteBean.getPsId());
			retailPrice.add("ca_id", retailPriceQuoteBean.getCaId());
			retailPrice.add("result_type", RetailPriceResultTypeKey.APPLY);
			long rp_id = fqm.addRetailPrice(retailPrice);
			
			DBRow products[] = retailPriceQuoteBean.getProducts();
			
			for (int i=0; i<products.length; i++)
			{
				DBRow retailPriceItem = new DBRow();
				retailPriceItem.add("pid", products[i].getString("pid"));
				retailPriceItem.add("name", products[i].getString("name"));
				retailPriceItem.add("old_retail_price", products[i].getString("old_retail_price"));
				retailPriceItem.add("new_retail_price", products[i].getString("new_retail_price"));
				retailPriceItem.add("rp_id", rp_id);
				retailPriceItem.add("price", products[i].getString("price"));
				retailPriceItem.add("shipping_fee", products[i].getString("shipping_fee"));
				retailPriceItem.add("gross_profit",products[i].getString("gross_profit"));
				retailPriceItem.add("sc_id",products[i].getString("sc_id"));
				retailPriceItem.add("ca_id", retailPriceQuoteBean.getCaId());
				
				fqm.addRetailPriceItems(retailPriceItem);
			}
			//添加工作流的任务。zhangrui
			//给总经理添加一个任务调价任务
			String time = DateUtil.DatetimetoStr(new Date()) ;
			String execute_ids = scheduleMgrZr.getAdminMangerUserIds();
			if(execute_ids.length() > 1){
				String emailTitle = adminInfo.getEmploye_name()+"申请的调价记录";
				scheduleMgrZr.addWorkFlowSchedule(time, time, adminInfo.getAdid(), execute_ids, "", rp_id, ModuleKey.WORK_FLOW_RETAIL_PRICE , emailTitle, emailTitle,true, false, false, request, true, 0, 1, WorkFlowTypeKey.RETAIL_PRICE);
			}
			return(rp_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addRetailPriceChange",log);
		}
	}
	
	/**
	 * 获得所有调价记录
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllRetailPrice(PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getAllRetailPrice( pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllRetailPrice",log);
		}
	}
	
	/**
	 * 获得调价商品
	 * @param rp_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRetailPriceItemsByRpId(long rp_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getRetailPriceItemsByRpId( rp_id, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getRetailPriceItemsByRpId",log);
		}
	}
	
	/**
	 * 更新商品的国家毛利
	 * @param rp_id
	 * @throws Exception
	 */
	public void updateProductProfit(HttpSession ses,long rp_id)
		throws Exception
	{
		try
		{
			AdminLoginBean adminInfo = new AdminMgr().getAdminLoginBean( ses );
			
			DBRow detailRetailPrice = fqm.getDetailRetailPriceByRpId(rp_id);
			DBRow countrys[] = fpm.getCountryAreaMappingByCaId(detailRetailPrice.get("ca_id", 0l));
			DBRow products[] = fqm.getRetailPriceItemsByRpId(rp_id, null);
			double min_order_profit = systemConfig.getDoubleConfigValue( "min_order_profit_ebay" );//订单最小毛利率
			double usd_exchange_rate = systemConfig.getDoubleConfigValue("USD");
			double profit;
			long pid,ccid;
			

			for (int j=0; j<products.length; j++)
			{
				for (int i=0; i<countrys.length; i++)
				{
					pid = products[j].get("pid", 0l);
					ccid = countrys[i].get("ccid", 0l);
					
					//根据新零售报价，计算毛利
					profit = products[j].get("gross_profit",0d);//MoneyUtil.round( (products[j].get("new_retail_price", 0d)*usd_exchange_rate-products[j].get("price", 0d)-products[j].get("shipping_fee",0d))/(1+min_order_profit), 2);
//					if (profit<=0.1)//保留两位小数引起的精度问题
//					{
//						profit = 0;
//					}
					////system.out.println(products[j].getString("name")+" - "+products[j].get("new_retail_price", 0d)+" - "+products[j].get("price", 0d)+" - "+products[j].get("shipping_fee",0d)+" > "+profit);
					
					DBRow productProfit = new DBRow();
					
					
					productProfit.add("profit",profit);
					productProfit.add("rpi_id",products[j].get("rpi_id", 0l));
					
					//如果已经存在该商品国家毛利，则修改，反之新增
					if (fqm.getDetailProductProfitByPidCcid(pid, ccid)==null)
					{
						productProfit.add("pid", pid);
						productProfit.add("ccid", ccid);//数据调整好后可以去掉
						
						productProfit.add("profit",products[j].get("gross_profit",0d));
						productProfit.add("rpi_id",products[j].get("rpi_id",0l));
						
						productProfit.add("ca_id",products[j].get("ca_id",0l));
						productProfit.add("retail_price",products[j].get("new_retail_price",0d));
						productProfit.add("shipping_fee",products[j].get("shipping_fee",0d));
						productProfit.add("sc_id",products[j].get("sc_id",0l));
						
						fqm.addProductProfit(productProfit);
					}
					else
					{
						fqm.modProductProfitByPidCaid(pid,products[j].get("ca_id",0l), productProfit);
					}

				}
			}
			//更改调价记录状态
			DBRow updateRetailPrice = new DBRow();
			updateRetailPrice.add("handle_status", RetailPriceKey.PASS);
			updateRetailPrice.add("approve_date", DateUtil.NowStr());
			updateRetailPrice.add("approve", adminInfo.getAccount());
			fqm.modRetailPriceByRpId(rp_id, updateRetailPrice);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateProductProfit",log);
		}
	}
	
	/**
	 * 获得商品、国家毛利详细信息
	 */
	public DBRow getDetailProductProfitByPidCcid(long pid,long ccid)
		throws Exception
	{
		try
		{
			return(fqm.getDetailProductProfitByPidCcid( pid, ccid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailProductProfitByPidCcid",log);
		}
	}
	
	/**
	 * 得到申请价格审批详细信息
	 * @param rp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailRetailPriceByRpId(long rp_id)
		throws Exception
	{
		try
		{
			return(fqm.getDetailRetailPriceByRpId(rp_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailRetailPriceByRpId",log);
		}
	}
	
	/**
	 * 否决调价
	 * @param rp_id
	 * @param account
	 * @param reject_reasons
	 * @throws Exception
	 */
	public void rejectChangeRetailProductPrice(long rp_id,String account,String reject_reasons)
		throws Exception
	{
		try
		{
			//更改状态
			DBRow updateRetailPrice = new DBRow();
			updateRetailPrice.add("handle_status", RetailPriceKey.REJECT);
			updateRetailPrice.add("approve_date", DateUtil.NowStr());
			updateRetailPrice.add("approve", account);
			updateRetailPrice.add("reject_reasons", reject_reasons);
			fqm.modRetailPriceByRpId(rp_id, updateRetailPrice);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"updateProductProfit",log);
		}
	}
	
	/**
	 * 获得定制变化商品
	 * @param old_set_pid
	 * @param new_custom_pid
	 * @return
	 * @throws Exception
	 */
	public CustomChangeProductsBean getCustomChangeProductsFromCart(HttpSession session,long new_custom_pid)
		throws Exception
	{
		try
		{
			CustomChangeProductsBean customChangeProductsBean = new CustomChangeProductsBean();
			ArrayList<String> oldProductIdsList = new ArrayList<String>();
			ArrayList<String> customProductIdsList = new ArrayList<String>();
			HashMap<String, String> oldProductQuantitiesMap = new HashMap<String, String>();
			HashMap<String, String> customProductQuantitiesMap = new HashMap<String, String>();
			
			DBRow oldUnionProId = productMgr.getDetailProductCustomByPcPcid(new_custom_pid);//通过定制套装的ID获得定制套装的基础信息
			DBRow oldUnionProductsInSet[] = productMgr.getProductsInSetBySetPid(oldUnionProId.get("orignal_pc_id",0l));//获得原套装商品
			DBRow customProductsInSet[] = customCart.getFinalDetailProducts(session, new_custom_pid);//获得定制商品
			////system.out.println("customProductsInSet:"+customProductsInSet.length);
			
			//加载老商品到列表
			for (int i=0; i<oldUnionProductsInSet.length; i++)
			{
				oldProductIdsList.add(oldUnionProductsInSet[i].getString("pc_id"));
				oldProductQuantitiesMap.put(oldUnionProductsInSet[i].getString("pc_id"), oldUnionProductsInSet[i].getString("quantity"));
			}
			//加载定制商品到列表
			for (int i=0; i<customProductsInSet.length; i++)
			{
				customProductIdsList.add(customProductsInSet[i].getString("pc_id"));
				customProductQuantitiesMap.put(customProductsInSet[i].getString("pc_id"), customProductsInSet[i].getString("cart_quantity"));
			}
			
			//检查新增商品
			for (int i=0; i<customProductsInSet.length; i++)
			{
				if (oldProductIdsList.contains(customProductsInSet[i].getString("pc_id"))==false)
				{
					CustomAddProductsBean customAddProductsBean = new CustomAddProductsBean();
					customAddProductsBean.setName(customProductsInSet[i].getString("p_name"));
					customAddProductsBean.setPid(customProductsInSet[i].get("pc_id",0l));
					customAddProductsBean.setQuantity(StringUtil.getFloat(customProductsInSet[i].getString("cart_quantity")));
					
					customChangeProductsBean.addCustomAddProductsBean(customAddProductsBean);
				}
			}
			
			for (int i=0; i<oldUnionProductsInSet.length; i++)
			{
				//检查减少商品
				if (customProductIdsList.contains(oldUnionProductsInSet[i].getString("pc_id"))==false)
				{
					CustomRemoveProductsBean customRemoveProductsBean = new CustomRemoveProductsBean();
					customRemoveProductsBean.setName(oldUnionProductsInSet[i].getString("p_name"));
					customRemoveProductsBean.setPid(oldUnionProductsInSet[i].get("pc_id",0l));
					customRemoveProductsBean.setQuantity(oldUnionProductsInSet[i].get("quantity",0f));
					
					customChangeProductsBean.addCustomRemoveProductsBean(customRemoveProductsBean);
				}
				else
				{
					/**
					 * 如果老商品还存在于定制，再检查老商品是否数量有变化
					 * 如果变多了，则做新增商品处理
					 * 如果变少了，则做移除商品处理
					 */
					float customQuantity = StringUtil.getFloat(customProductQuantitiesMap.get(oldUnionProductsInSet[i].getString("pc_id")).toString());
					if ( oldUnionProductsInSet[i].get("quantity",0f)<customQuantity )
					{
						CustomAddProductsBean customAddProductsBean = new CustomAddProductsBean();
						customAddProductsBean.setName(oldUnionProductsInSet[i].getString("p_name"));
						customAddProductsBean.setPid(oldUnionProductsInSet[i].get("pc_id",0l));
						customAddProductsBean.setQuantity(customQuantity-oldUnionProductsInSet[i].get("quantity",0f));
						
						customChangeProductsBean.addCustomAddProductsBean(customAddProductsBean);
					}
					else if ( oldUnionProductsInSet[i].get("quantity",0f)>customQuantity )
					{
						CustomRemoveProductsBean customRemoveProductsBean = new CustomRemoveProductsBean();
						customRemoveProductsBean.setName(oldUnionProductsInSet[i].getString("p_name"));
						customRemoveProductsBean.setPid(oldUnionProductsInSet[i].get("pc_id",0l));
						customRemoveProductsBean.setQuantity(oldUnionProductsInSet[i].get("quantity",0f)-customQuantity);
						
						customChangeProductsBean.addCustomRemoveProductsBean(customRemoveProductsBean);
					}
				}
			}
			
			
			return(customChangeProductsBean);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getCustomChangeProducts",log);
		}
	}

	/**
	 * 计算定制后商品的报价
	 * @param custom_pid
	 * @param custom_quantity
	 * @param ccid
	 * @param discount
	 * @return
	 * @throws Exception
	 */
	public double getCustomProductPrice(HttpSession session,long custom_pid,long ccid,double discount)
		throws Exception
	{
		try
		{
			/**
			 * 计算定制零售价
			 * 基本价格：定制前套装的零售价
			 * 减掉：移除商品零售价(不减毛利,只用商品原始成本价)
			 * 加上：新增商品零售价
			 */
			double removeProductsPirce = 0;
			double addProductsPirce = 0;
			double baseCustomProductPirce = 0;
			double tmp1 = 0;
			double tmp2 = 0;
			CustomChangeProductsBean customChangeProductsBean = this.getCustomChangeProductsFromCart(session,custom_pid);//获得定制后变化的商品
			ArrayList<CustomRemoveProductsBean> customRemoveProductsBeanList = customChangeProductsBean.getCustomRemoveProductsBeanList();
			for (int k=0; k<customRemoveProductsBeanList.size(); k++)
			{
				////system.out.println( "remove name:"+customRemoveProductsBeanList.get(k).getName() );
				removeProductsPirce = MoneyUtil.add(removeProductsPirce,MoneyUtil.mul(fpm.getDetailProductByPcid(customRemoveProductsBeanList.get(k).getPid()).get("unit_price", 0d),customRemoveProductsBeanList.get(k).getQuantity()) );
			}
			ArrayList<CustomAddProductsBean> customAddProductsBeanList =  customChangeProductsBean.getCustomAddProductsBeanList();
			for (int k=0; k<customAddProductsBeanList.size(); k++)
			{
				double addProductPirce = MoneyUtil.mul(this.getLowestProductPrice(customAddProductsBeanList.get(k).getPid(),ccid,discount), customAddProductsBeanList.get(k).getQuantity());
				addProductsPirce = MoneyUtil.add(addProductsPirce,addProductPirce);
			}
			baseCustomProductPirce = this.getLowestProductPrice(custom_pid,ccid,discount);
			tmp1 = MoneyUtil.sub(baseCustomProductPirce, removeProductsPirce);
			tmp2 = MoneyUtil.add(tmp1, addProductsPirce);
			return(tmp2);
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getCustomProductPrice",log);
		}
	}
	
	/**
	 * 获得一个报价单有多少个商品
	 * @param qoid
	 * @return
	 * @throws Exception
	 */
	public int getQuoteItemCountByQoid(long qoid)
		throws Exception
	{
		try
		{
			return(fqm.getQuoteItemCountByQoid(qoid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getQuoteItemCountByQoid",log);
		}
	}
	
	/**
	 * 获得套装内没有设置毛利商品
	 * @param set_id
	 * @param ccid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNotProductProfitInSetBySetIdCcid(long set_id,long ccid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getNotProductProfitInSetBySetIdCcid( set_id, ccid, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getNotProductProfitInSetBySetIdCcid",log);
		}
	}
	
	/**
	 * 获得所有客服可以打折的范围
	 * @param adgid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceQuoteDiscountRange(long adgid)
		throws Exception
	{
		try
		{
			return(fqm.getServiceQuoteDiscountRange(adgid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getServiceQuoteDiscountRange",log);
		}
	}
	
	/**
	 * 批量修改客服报价折扣范围
	 * @param request
	 * @throws Exception
	 */
	public void batchModServiceQuoteDiscountRange(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String adid[] = request.getParameterValues("adid");
			String discount_range[] = request.getParameterValues("discount_range");
			
			fqm.cleanServiceQuoteDiscountRange();
			for (int i=0; i<adid.length; i++)
			{
				DBRow data = new DBRow();
				data.add("adid", adid[i]);
				data.add("discount_range", StringUtil.getFloat(discount_range[i])/100);
				fqm.addServiceQuoteDiscountRange(data);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"batchModServiceQuoteDiscountRange",log);
		}
	}
	
	/**
	 * 获得客服可以下调的折扣范围
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailServiceQuoteDiscountRangeByAdid(long adid)
		throws Exception
	{
		try
		{
			return(fqm.getDetailServiceQuoteDiscountRangeByAdid(adid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailServiceQuoteDiscountRangeByAdid",log);
		}
	}
		
	/**
	 * 通过下调折扣幅度计算客服折扣
	 * @param systemDiscount
	 * @param discountRate
	 * @return
	 */
	private float calculateManagerDiscount(float systemDiscount,float discountRate)
	{
		return(systemDiscount-systemDiscount*discountRate);
	}
	
	/**
	 * 通过下调折扣幅度计算客服折扣
	 * @param systemDiscount
	 * @param discountRate
	 * @return
	 */
	public float[] calculateManagerDiscounts(float systemDiscount,float discountRate)
	{
		float rate = discountRate;
		float step = 0.05f;//增长步长
		ArrayList<Float> result = new ArrayList<Float>();
		
		float i=step;
		for (;i<=rate; i=i+step)
		{
			result.add( this.calculateManagerDiscount(systemDiscount, i) );
		}
		
		if (i!=rate+step)
		{
			result.add( this.calculateManagerDiscount(systemDiscount, discountRate) );
		}
		
		float fla[] = new float[result.size()];
		for (int j=0; j<result.size(); j++)
		{
			fla[j] = result.get(j).floatValue();
		}
		
		return(fla);
	}
	
	/**
	 * 获得客服可以使用的折扣
	 * @param session
	 * @param systemDiscount
	 * @return
	 * @throws Exception
	 */
	public float[] calculateManagerDiscounts(HttpSession session,float systemDiscount)
		throws Exception
	{
		try
		{
			AdminMgr adminMgr = new AdminMgr();
			AdminLoginBean adminInfo = adminMgr.getAdminLoginBean( session );
			DBRow detaildiscountRate = fqm.getDetailServiceQuoteDiscountRangeByAdid(adminInfo.getAdid());
			
			if (detaildiscountRate==null)
			{
				return(new float[0]);
			}
			else
			{
				return( this.calculateManagerDiscounts(systemDiscount,detaildiscountRate.get("discount_range", 0f)) );				
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"flush",log);
		}
	}
	
	/**
	 * 获得商品销售地区
	 * @param pid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCountryAreaByPid(long pid,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getCountryAreaByPid(pid,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getCountryAreaByPid",log);
		}
	}
	
	/**
	 * 获得商品销售国家
	 * @param pid
	 * @param ca_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCountryByPidCaid(long pid,long ca_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			return(fqm.getCountryByPidCaid( pid, ca_id, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getCountryByPidCaid",log);
		}
	}
	/**
	 * 同意价格审批
	 * 执行原来的操作.
	 * 然后给申请人添加一个任务(关联原来的rp_id)。表示改任务完成
	 * 同时添加一个任务的回复	
	 */
	@Override
	public void agreeRetailPrice(HttpServletRequest request) throws Exception {
		 try{
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			long rp_id = StringUtil.getLong(request, "rp_id");
			long schedule_id = StringUtil.getLong(request, "schedule_id");
			long schedule_sub_id = StringUtil.getLong(request, "schedule_sub_id");
			long create_adid = StringUtil.getLong(request, "create_adid");
			
			//执行原来的调价操作
			this.updateProductProfit(StringUtil.getSession(request), rp_id);
			
			//添加一个回复
			DBRow replay = new DBRow();
			replay.add("sch_replay_context", adminLoggerBean.getEmploye_name() + " 同意该调价申请");
			replay.add("sch_replay_time", DateUtil.DatetimetoStr(new Date()));
			replay.add("sch_replay_type", 1);
			replay.add("schedule_id", schedule_id);
			replay.add("adid", adminLoggerBean.getAdid());
			replay.add("employe_name", adminLoggerBean.getEmploye_name());
			replay.add("sch_state", 10);
			scheduleReplayMgrZr.addSimpleScheduleReplay(replay);
			
			//完成该任务
			scheduleMgrZr.finishRefundSchedule(request);
			
			//给申请人一个任务
			DBRow updateRetailPriceRow = new DBRow();
			
			updateRetailPriceRow.add("result_type", RetailPriceResultTypeKey.AGREE);
			fqm.updateRetailPrice(updateRetailPriceRow, rp_id);
			addAgreeRetailPrice(schedule_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), create_adid, request);
			
			 
		 }catch (Exception e) {
			 throw new SystemException(e,"agreeRetailPrice",log);
		}
	}

	@Override
	public void notAgreeRetialPrice(HttpServletRequest request)
			throws Exception {
		try{
			AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(request.getSession()); 
			long rp_id = StringUtil.getLong(request, "rp_id");
			long schedule_id = StringUtil.getLong(request, "schedule_id");
			long schedule_sub_id = StringUtil.getLong(request, "schedule_sub_id");
			long create_adid = StringUtil.getLong(request, "create_adid");
			String result_text = StringUtil.getString(request, "result_text");
			//执行原来的调价操作
			//this.updateProductProfit(StrUtil.getSession(request), rp_id);
			
			//添加一个回复
			DBRow replay = new DBRow();
			replay.add("sch_replay_context", adminLoggerBean.getEmploye_name() + " 不同意该调价申请");
			replay.add("sch_replay_time", DateUtil.DatetimetoStr(new Date()));
			replay.add("sch_replay_type", 1);
			replay.add("schedule_id", schedule_id);
			replay.add("adid", adminLoggerBean.getAdid());
			replay.add("employe_name", adminLoggerBean.getEmploye_name());
			replay.add("sch_state", 10);
			scheduleReplayMgrZr.addSimpleScheduleReplay(replay);
			
			//完成该任务
			scheduleMgrZr.finishRefundSchedule(request);
			
			//给申请人一个任务
			DBRow updateRetailPriceRow = new DBRow();
			
			updateRetailPriceRow.add("result_type", RetailPriceResultTypeKey.NOT_AGREE);
			updateRetailPriceRow.add("result_text", result_text);
			fqm.updateRetailPrice(updateRetailPriceRow, rp_id);
			addNotAgreeRetailPrice(schedule_id, adminLoggerBean.getAdid(), adminLoggerBean.getEmploye_name(), create_adid, request);
			 
		 }catch (Exception e) {
			 throw new SystemException(e,"notAgreeRetialPrice",log);
		}
	}	
	private void addNotAgreeRetailPrice(long schedule_id,long handle_adid ,String hand_userName , long create_adid , HttpServletRequest request) throws Exception {
		try{
			 String time = DateUtil.DatetimetoStr(new Date());
			 DBRow schedule = scheduleMgrZr.getScheduleById(schedule_id);
			 String content =  hand_userName + "不同意" + schedule.getString("schedule_overview") ;
			 scheduleMgrZr.addWorkFlowSchedule(time, time, handle_adid, create_adid+"", "", schedule.get("associate_id", 0l), schedule.get("associate_type", 0), content,content, true, false, false, request, true, 0 , 1, schedule.get("task_type", 0));
			
		}catch (Exception e) {
			throw new SystemException(e,"addApplyerRejectSchedule",log);
		}
		
	}
		
	private void addAgreeRetailPrice(long schedule_id,long handle_adid ,String hand_userName , long create_adid , HttpServletRequest request) throws Exception {
		try{
			 String time = DateUtil.DatetimetoStr(new Date());
			 DBRow schedule = scheduleMgrZr.getScheduleById(schedule_id);
			 String content =  hand_userName + "同意了" + schedule.getString("schedule_overview") ;
			 scheduleMgrZr.addWorkFlowSchedule(time, time, handle_adid, create_adid+"", "", schedule.get("associate_id", 0l), schedule.get("associate_type", 0), content,content, true, false, false, request, true, 0 , 1, schedule.get("task_type", 0));
			
		}catch (Exception e) {
			throw new SystemException(e,"addApplyerRejectSchedule",log);
		}
		
	}
	
	public void setFqm(FloorQuoteMgr fqm)
	{
		this.fqm = fqm;
	}
		
	public void setFom(FloorOrderMgr fom)
	{
		this.fom = fom;
	}

	public void setCustomCart(CustomCartQuote customCart)
	{
		this.customCart = customCart;
	}

	public void setCart(CartQuote cart)
	{
		this.cart = cart;
	}

	public void setFpm(FloorProductMgr fpm)
	{
		this.fpm = fpm;
	}

	public void setSystemConfig(SystemConfig systemConfig)
	{
		this.systemConfig = systemConfig;
	}

	public void setExpressMgr(ExpressMgr expressMgr)
	{
		this.expressMgr = expressMgr;
	}

	public void setOrderMgr(OrderMgr orderMgr)
	{
		this.orderMgr = orderMgr;
	}
	
	public void setProductMgr(ProductMgr productMgr)
	{
		this.productMgr = productMgr;
	}
	
	public void setFam(FloorAdminMgr fam)
	{
		this.fam = fam;
	}
	

	public void setFcm(FloorCatalogMgr fcm) 
	{
		this.fcm = fcm;
	}
	
	public void setCrmMgr(CrmMgrIFace crmMgr) 
	{
		this.crmMgr = crmMgr;
	}
	
	public static void main(String args[]) throws Exception
	{
		QuoteMgr quoteMgr = new QuoteMgr();
		String rules[][] = quoteMgr.getWholeSellRule3("1={0,300,8}");
		
		for (int i=0; i<rules.length; i++)
		{
			for (int j=0; j<rules[i].length; j++)
			{
				////system.out.println(rules[i][j]);
			}
		}
		
	}

	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}

	public void setQuoteMgrZJ(QuoteMgrZJ quoteMgrZJ) {
		this.quoteMgrZJ = quoteMgrZJ;
	}

	public void setScheduleMgrZr(ScheduleMgrIfaceZR scheduleMgrZr) {
		this.scheduleMgrZr = scheduleMgrZr;
	}

	public void setAdminMgr(AdminMgrIFace adminMgr) {
		this.adminMgr = adminMgr;
	}

	public void setScheduleReplayMgrZr(ScheduleReplayMgrIfaceZR scheduleReplayMgrZr) {
		this.scheduleReplayMgrZr = scheduleReplayMgrZr;
	}
	
	

}








