package com.cwc.app.api;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.util.CellRangeAddress;

import com.cwc.app.api.zj.ContainerLoadingMethodMgrZJ;
import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.exception.product.ProductDataErrorException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.exception.quote.WareHouseErrorException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorExpressMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.iface.ExpressMgrIFace;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.util.TUpload;
import com.cwc.shipping.ShippingFeeCommPolicy;
import com.cwc.shipping.ShippingInfoBean;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ExpressMgr implements ExpressMgrIFace
{
	static Logger log = Logger.getLogger("ACTION");
	private FloorExpressMgr fem = null;
	private FloorCatalogMgr fcm;
	private FloorProductMgr floorProductMgr;
	private SystemConfig systemConfig;
	
	/**
	 * 获得所有快递公司
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllExpressCompany(PageCtrl pc)
		throws Exception
	{
		try 
		{
			
			return(fem.getAllExpressCompany(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllExpressCompany",log);
		}
	}
	
	/**
	 * 获得所有国际快递公司
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllInternationalExpressCompany(PageCtrl pc)
		throws Exception
	{
		try 
		{
			
			return(fem.getAllInternationalExpressCompany(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllInternationalExpressCompany",log);
		}
	}
	
	/**
	 * 获得相同国家，国内快递
	 * @param sc_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllDomesticExpressCompanyByScId(long sc_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			
			return(fem.getAllDomesticExpressCompanyByScId(sc_id,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllDomesticExpressCompanyByScId",log);
		}
	}

	/**
	 * 通过快递公司ID获得快递所有地区
	 * @param sc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getZoneByScId(long sc_id)
		throws Exception
	{
		try 
		{
			return(fem.getZoneByScId(sc_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getZoneByScId",log);
		}
	}
	
	/**
	 * 通过地区ID，获得所有费用
	 * @param sz_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFeeBySzId(long sz_id)
		throws Exception
	{
		try 
		{
			return(fem.getFeeBySzId(sz_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getFeeBySzId",log);
		}
	}
	
	/**
	 * 获得一个快递公司所有重量段
	 * @param sc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFeeWeightSection(long sc_id)
		throws Exception
	{
		try 
		{
			return(fem.getFeeWeightSection(sc_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getFeeWeightSection",log);
		}
	}

	/**
	 * 增加快递公司
	 * @param request
	 * @throws Exception
	 */
	public void addCompany(HttpServletRequest request)
		throws Exception
	{
		try
		{
			String name = StringUtil.getString(request,"name");
			float shipping_fee_discount = StringUtil.getFloat(request,"shipping_fee_discount");
			float oil_addition_fee = StringUtil.getFloat(request,"oil_addition_fee");
			long ps_id = StringUtil.getLong(request, "ps_id");
			float st_weight = StringUtil.getFloat(request,"st_weight");
			float en_weight = StringUtil.getFloat(request,"en_weight");
			double handle_fee = StringUtil.getDouble(request,"handle_fee");
			int domestic = StringUtil.getInt(request,"domestic");
			float print_weight_discount = StringUtil.getFloat(request,"print_weight_discount");
			
			DBRow company = new DBRow();
			company.add("name",name);
			company.add("shipping_fee_discount",shipping_fee_discount);
			company.add("oil_addition_fee",oil_addition_fee);
			company.add("locked",1);
			company.add("ps_id",ps_id);
			company.add("st_weight",st_weight);
			company.add("en_weight",en_weight);
			company.add("handle_fee",handle_fee);
			company.add("domestic",domestic);
			company.add("print_weight_discount",print_weight_discount);
			
			fem.addCompany(company);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addCompany",log);
		}
	}

	/**
	 * 增加重量段
	 * @param request
	 * @throws WeightCrossException
	 * @throws Exception
	 */
	public void addWeight(HttpServletRequest request)
		throws WeightCrossException,Exception
	{
		try
		{
			float st_weight = StringUtil.getFloat(request,"st_weight");
			float en_weight = StringUtil.getFloat(request,"en_weight");
			float weight_step = StringUtil.getFloat(request,"weight_step");
			long sc_id = StringUtil.getLong(request, "sc_id");
			
			DBRow weight = new DBRow();
			weight.add("st_weight",st_weight);
			weight.add("en_weight",en_weight);
			weight.add("weight_step",weight_step);
			weight.add("sc_id",sc_id);
			fem.addWeight(weight);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addWeight",log);
		}
	}
	
	/**
	 * 删除重量段
	 * @param request
	 * @throws WeightCrossException
	 * @throws Exception
	 */
	public void delWeight(HttpServletRequest request)
		throws WeightCrossException,Exception
	{
		try
		{
			long sw_id = StringUtil.getLong(request, "sw_id");
			
			DBRow weightFeeMapping[] = fem.getWeightFeeMappingBySwid(sw_id);
			//先删除重量段对应的运费
			for (int i=0; i<weightFeeMapping.length; i++)
			{
				fem.delFee(weightFeeMapping[i].get("sf_id", 0l));
			}
			
			fem.delWeightFeeMappingBySwId(sw_id);
			fem.delWeight(sw_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delWeight",log);
		}
	}
	
	/**
	 * 获得一个快递公司所有重量段
	 * @param sc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFeeWeightSectionOrderByEnWeightDesc(long sc_id)
		throws Exception
	{
		try 
		{
			return(fem.getFeeWeightSectionOrderByEnWeightDesc(sc_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getFeeWeightSectionOrderByEnWeightDesc",log);
		}
	}

	/**
	 * 获得快递公司详细信息
	 * @param sc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailCompany(long sc_id)
		throws Exception
	{
		try 
		{
			return(fem.getDetailCompany(sc_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailCompany",log);
		}
	}

	/**
	 * 通过快递公司名字获得快递公司详细信息
	 * @param sc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailCompanyByName(String name)
		throws Exception
	{
		try 
		{
			return(fem.getDetailCompanyByName(name));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailCompanyByName",log);
		}
	}
	
	/**
	 * 获得重量段详细信息
	 * @param sw_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailWeight(long sw_id)
		throws Exception
	{
		try 
		{
			return(fem.getDetailWeight(sw_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailWeight",log);
		}
	}
	
	/**
	 * 获得详细费用信息
	 * @param sc_id
	 * @param sw_id
	 * @param sz_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailFeeByCompanyWeightZone(long sc_id,long sw_id,long sz_id)
		throws Exception
	{
		try 
		{
			return(fem.getDetailFeeByCompanyWeightZone( sc_id, sw_id, sz_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailFeeByCompanyWeightZone",log);
		}
	}
	
	/**
	 * 添加、修改运费
	 * @param request
	 * @throws Exception
	 */
	public void saveFee(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sw_id = StringUtil.getLong(request,"sw_id");
			long sc_id = StringUtil.getLong(request,"sc_id");
			DBRow detailCompany = fem.getDetailCompany(sc_id);
			
			String sf_id,sz_id;
			
			Enumeration paraNames = request.getParameterNames();
			while(paraNames.hasMoreElements()) 
			{
				String paraName = (String)paraNames.nextElement();
				
				if (paraName.indexOf("f_weight_fee")>=0)
				{
					//f_weight_fee_2_1
					sf_id = paraName.split("_")[3];
					sz_id = paraName.split("_")[4];

					//>0更新 //sz_id
					if (StringUtil.getLong(sf_id)>0)
					{
						DBRow newFee = new DBRow();
						newFee.add("f_weight_fee",StringUtil.getFloat(request, "f_weight_fee_"+sf_id+"_"+sz_id)*detailCompany.get("currency_rate", 0f));
						newFee.add("k_weight_fee",StringUtil.getFloat(request, "k_weight_fee_"+sf_id+"_"+sz_id)*detailCompany.get("currency_rate", 0f));
						fem.modFee(StringUtil.getLong(sf_id), newFee);
						
					}
					else
					{
						DBRow newFee = new DBRow();
						newFee.add("f_weight_fee",StringUtil.getFloat(request, "f_weight_fee_"+sf_id+"_"+sz_id));
						newFee.add("k_weight_fee",StringUtil.getFloat(request, "k_weight_fee_"+sf_id+"_"+sz_id));
						newFee.add("sz_id",sz_id);
						long new_sf_id = fem.addFee(newFee);
						
						DBRow weightFeeMapping = new DBRow();
						weightFeeMapping.add("sw_id", sw_id);
						weightFeeMapping.add("sf_id", new_sf_id);
						fem.addWeightFeeMapping(weightFeeMapping);
					}
				}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"saveFee",log);
		}
	}
	
	/**
	 * 删除快递公司
	 * @param request
	 * @throws Exception
	 */
	public void delCompany(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sc_id = StringUtil.getLong(request, "sc_id");
			
			//删除运费
			DBRow zones[] = fem.getZoneByScId(sc_id);
			for (int i=0; i<zones.length; i++)
			{
				fem.delFeeBySzId(zones[i].get("sz_id", 0l));
			}
			//删除重量、运费mapping
			DBRow weights[] = fem.getFeeWeightSection(sc_id);
			for (int i=0; i<weights.length; i++)
			{
				fem.delWeightFeeMappingBySwId(weights[i].get("sw_id", 0l));
			}
			
			fem.delZoneByScId(sc_id);
			fem.delWeightByScId(sc_id);
			fem.delCompany(sc_id);
			fem.delZoneCountryMappingByScId(sc_id);
			fem.delZoneProvinceMappingByScId(sc_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delCompany",log);
		}
	}

	/**
	 * 修改快递公司基本信息
	 * @param request
	 * @throws Exception
	 */
	public void modCompany(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sc_id = StringUtil.getLong(request, "sc_id");
			String name = StringUtil.getString(request, "name");
			float shipping_fee_discount = StringUtil.getFloat(request, "shipping_fee_discount");
			float oil_addition_fee = StringUtil.getFloat(request,"oil_addition_fee");
			long ps_id = StringUtil.getLong(request, "ps_id");
			float st_weight = StringUtil.getFloat(request,"st_weight");
			float en_weight = StringUtil.getFloat(request,"en_weight");
			double handle_fee = StringUtil.getDouble(request,"handle_fee");
			float print_weight_discount = StringUtil.getFloat(request,"print_weight_discount");
			
			DBRow company = new DBRow();
			company.add("name",name);
			company.add("shipping_fee_discount",shipping_fee_discount);
			company.add("oil_addition_fee",oil_addition_fee);
			company.add("ps_id",ps_id);
			company.add("st_weight",st_weight);
			company.add("en_weight",en_weight);
			company.add("handle_fee",handle_fee);
			company.add("print_weight_discount",print_weight_discount);
			fem.modCompany(sc_id, company);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modCompany",log);
		}
	}

	/**
	 * 增加地区
	 * @param request
	 * @throws Exception
	 */
	public void addZone(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sc_id = StringUtil.getLong(request, "sc_id");
			String name = StringUtil.getString(request, "name");
			
			DBRow zone = new DBRow();
			zone.add("sc_id",sc_id);
			zone.add("name",name);
			fem.addZone(zone);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"addZone",log);
		}
	}

	/**
	 * 修改地区信息
	 * @param request
	 * @throws Exception
	 */
	public void modZone(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sz_id = StringUtil.getLong(request, "sz_id");
			String name = StringUtil.getString(request, "name");
			
			DBRow zone = new DBRow();
			zone.add("sz_id",sz_id);
			zone.add("name",name);
			fem.modZone(sz_id, zone);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modZone",log);
		}
	}
	
	/**
	 * 获得地区详细信息
	 * @param sz_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailZone(long sz_id)
		throws Exception
	{
		try 
		{
			return(fem.getDetailZone(sz_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailZone",log);
		}
	}
	
	/**
	 * 删除地区
	 * @param request
	 * @throws Exception
	 */
	public void delZone(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sz_id = StringUtil.getLong(request, "sz_id");

			DBRow fees[] = fem.getFeeBySzId(sz_id);
			//先删除映射表
			for (int i=0; i<fees.length; i++)
			{
				fem.delWeightFeeMappingBySfId(fees[i].get("sf_id", 0l));
			}
			//删除运费
			fem.delFeeBySzId(sz_id);
			//删除地区
			fem.delZoneBySzId(sz_id);
			//删除地区、国家关系映射
			fem.delZoneCountryMappingBySzId(sz_id);
			fem.delZoneProvinceMappingBySzId(sz_id);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"delZone",log);
		}
	}

	/**
	 * 修改重量段信息
	 * @param request
	 * @throws Exception
	 */
	public void modWeight(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sw_id = StringUtil.getLong(request, "sw_id");
			
			DBRow detailCompany = fem.getDetailCompany( fem.getDetailWeight(sw_id).get("sc_id", 0l) );
			
			float st_weight = StringUtil.getFloat(request, "st_weight");
			float en_weight = StringUtil.getFloat(request, "en_weight");
			float weight_step = StringUtil.getFloat(request, "weight_step");
			
			DBRow weight = new DBRow();
			weight.add("st_weight",st_weight/detailCompany.get("weight_rate", 0f));
			weight.add("en_weight",en_weight/detailCompany.get("weight_rate", 0f));
			weight.add("weight_step",weight_step/detailCompany.get("weight_rate", 0f));
			fem.modWeight( sw_id, weight);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modWeight",log);
		}
	}

	@Override
	public ShippingInfoBean[] getAllShippingFeeByScIdAndWeight(long ps_id , float weight,long ccid,long pro_id)
		throws ProvinceOutSizeException,CountryOutSizeException,WeightCrossException,WeightOutSizeException,Exception{
		DBRow[] result = floorProductMgr.getAllShippingCompanyByPsId(ps_id);
		ShippingInfoBean[] beans = null;
		if( result != null  && result.length > 0 ){
			beans = new ShippingInfoBean[result.length];
			for(int index =0 , count = result.length ; index < count ; ){
				long sc_id = result[index].get("sc_id", 0l);
				ShippingInfoBean bean = null;
				try{
					bean = this.getShippingFee(sc_id, weight, ccid, pro_id);
				}catch (Exception e) {
					bean = new ShippingInfoBean();
					bean.setCompanyName("error");
				}finally{
					beans[index] = bean;
					index++ ;
				}
			}
		}
		return beans;
	}
	/**
	 * 获得快递费用，地区等详细信息
	 * @param sc_id		快递公司ID
	 * @param weight	货物重量
	 * @param ccid		发送国家
	 * @return
	 * @throws CountryOutSizeException	不能递送国家
	 * @throws WeightCrossException		重量段设置交叉
	 * @throws WeightOutSizeException	没有合适重量段
	 * @throws Exception		
	 */
	
	public ShippingInfoBean getShippingFee(long sc_id,float weight,long ccid,long pro_id)
		throws ProvinceOutSizeException,CountryOutSizeException,WeightCrossException,WeightOutSizeException,Exception
	{
		ShippingFeeCommPolicy shippingFeeCommPolicy = null;
		
		try 
		{
			DBRow detailCompany = fem.getDetailCompany(sc_id);
			////system.out.println( "==> "+detailCompany );
			if (detailCompany==null)
			{
				return(null);
			}
			
			shippingFeeCommPolicy = (ShippingFeeCommPolicy)MvcUtil.getBeanFromContainer(detailCompany.getString("imp_class"));
			shippingFeeCommPolicy.initCompanyWeightCountry(sc_id, weight, ccid,pro_id);

			ShippingInfoBean shippingInfoBean = new ShippingInfoBean();
			shippingInfoBean.setCompanyName(shippingFeeCommPolicy.getCompany_name());
			shippingInfoBean.setShippingFee(shippingFeeCommPolicy.getShippingFee());
			shippingInfoBean.setWeight(shippingFeeCommPolicy.getPRO_TW());//经过计算后的重量
			shippingInfoBean.setZoneName(shippingFeeCommPolicy.getZone_name());
			shippingInfoBean.setUseType("");
			shippingInfoBean.setSc_id(sc_id);

			int begin_day =  detailCompany.get("begin_day",0);
			int begin_hours = detailCompany.get("begin_hour",0);
			int last_hours = detailCompany.get("last_hour",0);
			
			TDate tDate = new TDate();
			int now_week_day = tDate.getIntWeek();
			int week_day_spacing = now_week_day-begin_day;
			
			tDate.addDay(-week_day_spacing);
			
			String beginDateString = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay()+" "+begin_hours+":"+"00:00";
			
			TDate begin_date = new TDate(beginDateString);
			
			long time_spacing = System.currentTimeMillis()-begin_date.getDateTime();
			
			int isSelect = 0;
			if(0<time_spacing&&time_spacing<last_hours*3600*1000)
			{
				isSelect = 1;
			}

			
			shippingInfoBean.setSelectAuto(isSelect);
			
			return( shippingInfoBean );
		}
		//不再使用这种方式
//		catch (CountryOutSizeException e)
//		{
//			//再次查询子快递公司是否能达到配送地区
//			DBRow detailCompany = fem.getDetailCompanyByParentid(sc_id);
//			
//			if (detailCompany==null)//没有子快递公司
//			{
//				throw e;
//			}
//			else
//			{
//				shippingFeeCommPolicy = (ShippingFeeCommPolicy)MvcUtil.getBeanFromContainer(detailCompany.getString("imp_class"));
//				shippingFeeCommPolicy.initCompanyWeightCountry(detailCompany.get("sc_id", 0l), weight, ccid);
//				
//				ShippingInfoBean shippingInfoBean = new ShippingInfoBean();
//				shippingInfoBean.setCompanyName(shippingFeeCommPolicy.getCompany_name());
//				shippingInfoBean.setShippingFee(shippingFeeCommPolicy.getShippingFee());
//				shippingInfoBean.setWeight(shippingFeeCommPolicy.getPRO_TW());//经过计算后的重量
//				shippingInfoBean.setZoneName(shippingFeeCommPolicy.getZone_name());
//
//				return( shippingInfoBean );
//			}
//		}
		catch (ProvinceOutSizeException e)
		{
			throw e;
		}
		catch (CountryOutSizeException e)
		{
			throw e;
		}
		catch (WeightCrossException e)
		{
			throw e;
		}
		catch (WeightOutSizeException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getShippingFee",log);
		}
	}
	
	public ShippingInfoBean getNewShippingFee(long sc_id,long ccid,long pro_id,DBRow[] waybill)
		throws ProvinceOutSizeException,CountryOutSizeException,WeightCrossException,WeightOutSizeException,Exception
	{
		try 
		{
			float weight = 0;//运单总重量
			
			DBRow detailCompany = fem.getDetailCompany(sc_id);//快递公司
			////system.out.println( "==> "+detailCompany );
			if (detailCompany==null)
			{
				return(null);
			}
			
			String productString = "";
			productString+="  "+(waybill.length+1)+"  1 1 1 1 1 1 1 1	";//商品种类数量与防止只有一种商品
//			productString+="  "+(waybill.length)+" ";//商品种类数量与防止只有一种商品
			int sum = 0;
			for (int i = 0; i < waybill.length; i++) 
			{
				DBRow cartProduct;
				if(waybill[i].get("cart_product_type",0)==ProductTypeKey.UNION_CUSTOM)//定制商品
				{
					DBRow customProduct = floorProductMgr.getDetailProductByPcid(waybill[i].get("cart_pid",0l));
					
					DBRow[] products = floorProductMgr.getProductUnionsBySetPid(waybill[i].get("cart_pid",0l));
					
					cartProduct = floorProductMgr.getDetailProductByPcid(customProduct.get("orignal_pc_id",0l));//定制商品，需要从原商品上取长宽高
					
					for (int j = 0; j < products.length; j++)//定制商品组合关系
					{
						DBRow product = floorProductMgr.getDetailProductByPcid(products[j].get("pid",0l));
						//定制商品，定制组合关系数量X购买数量
						weight +=  product.get("weight",0f)*products[j].get("quantity",0f)*waybill[i].get("cart_quantity",0f);
					}
				}
				else//非定制商品
				{
					cartProduct = floorProductMgr.getDetailProductByPcid(waybill[i].get("cart_pid",0l));
					
					weight +=  cartProduct.get("weight",0f)*waybill[i].get("cart_quantity",0f);
					
					int length = (int)Double.parseDouble(cartProduct.getString("length"));
					int width = (int)Double.parseDouble(cartProduct.getString("width"));
					int heigth = (int)Double.parseDouble(cartProduct.getString("heigth"));
					if(length==0||width==0||heigth==0)
					{
						throw new ProductDataErrorException();
					}
				}
				
				 int one_item_count = (int)(waybill[i].get("cart_quantity",0f)*2);
				 
				 if(one_item_count%2==0)//抄单商品中没有0.5结尾
				 {
					 sum+=(int)(waybill[i].get("cart_quantity",0f));
					 productString+= "  	"+(i+2)+" "+(int)(cartProduct.get("length",0f))+" 1 "+(int)(cartProduct.get("width",0f))+" 1 "+(int)(cartProduct.get("heigth",0f))+" 1 "+(int)(waybill[i].get("cart_quantity",0f))+" ";
				 }
				 else//商品数量包含0.5，最长边除以2，商品打算装箱
				 {
					 sum+=one_item_count;
					 productString+= "  	"+(i+2)+" "+(int)(cartProduct.get("length",0f)/2)+" 1 "+(int)(cartProduct.get("width",0f))+" 1 "+(int)(cartProduct.get("heigth",0f))+" 1 "+one_item_count+" ";
				 }
			}
			
			ShippingFeeCommPolicy shippingFeeCommPolicy = null;
			
			shippingFeeCommPolicy = (ShippingFeeCommPolicy)MvcUtil.getBeanFromContainer(detailCompany.getString("imp_class"));
			shippingFeeCommPolicy.initCompanyWeightCountry(sc_id,weight,ccid,pro_id);//重量方式计算运费
			
			double basePrice = shippingFeeCommPolicy.getShippingFee();
			
			String baseMailpieceShape = "Parcel";
			String compareMailpieceShape = null;
			String useType = "";
			double useFee = basePrice;//运费
			double comparePrice = 0;
			
			if(sc_id==100006||sc_id==100014||sc_id==100028)//USPS-P计算体积方式运费
			{
				useType = baseMailpieceShape;
//				int count = 0;

				 ContainerLoadingMethodMgrZJ ContainerLoadingMgr = new ContainerLoadingMethodMgrZJ(this.floorProductMgr);
				 //小箱子能否装入所有货物
				 boolean isPacked = ContainerLoadingMgr.packed(219,137,41,3,waybill,false);//装下返回true，没装下返回false
				 if(isPacked)
				 {
					 compareMailpieceShape = "SmallFlatRateBox";
					 comparePrice = 5.20d*systemConfig.getDoubleConfigValue("USD");
					 if ( comparePrice>0&&basePrice>comparePrice )
					 {
						 useType = compareMailpieceShape;
						 useFee = comparePrice;
					 }
					 else
					 {
						 useType = baseMailpieceShape;
					 }
				 }
				 else//中箱子
				 {				 
					//第一个中箱子能否装入所有货物
					 isPacked = ContainerLoadingMgr.packed(351,307,91,3,waybill,false);
					 if(isPacked==false)
					 {
						isPacked = ContainerLoadingMgr.packed(284,221,145,3,waybill,false);
					 }
					 if(isPacked)
					 {
						 compareMailpieceShape = "MediumFlatRateBox";
						 comparePrice = 10.95d*systemConfig.getDoubleConfigValue("USD");
						 if ( comparePrice>0&&basePrice>comparePrice )
						 {
							 useType = compareMailpieceShape;
							 useFee = comparePrice;
						 }
						 else
						 {
							 useType = baseMailpieceShape;
						 }
					 }
					 else 
					 {
						 //第一个大箱子能否装入所有货物第一种算法
						 isPacked = ContainerLoadingMgr.packed(612,308,86,3,waybill,false); 
						 if(isPacked==false)
						 {
							//第二个大箱子能否装入所有货物
							isPacked = ContainerLoadingMgr.packed(315,315,150,3,waybill,false); 
						 }
						 if(isPacked)
						 {
							 compareMailpieceShape = "LargeFlatRateBox";
							 comparePrice = 14.95d*systemConfig.getDoubleConfigValue("USD");
							 if ( comparePrice>0&&basePrice>comparePrice )
							 {
								 useType = compareMailpieceShape;
								 useFee = comparePrice;
							 }
							 else
							 {
								 useType = baseMailpieceShape;
							 }
							 
							
						 }
						 else
						 {
							 useType = baseMailpieceShape;
						 }						 
					 }
				 }
			}
			
			ShippingInfoBean shippingInfoBean = new ShippingInfoBean();
			shippingInfoBean.setCompanyName(shippingFeeCommPolicy.getCompany_name());
			shippingInfoBean.setShippingFee(MoneyUtil.round(useFee,2));
			shippingInfoBean.setWeight(shippingFeeCommPolicy.getPRO_TW());//经过计算后的重量
			shippingInfoBean.setZoneName(shippingFeeCommPolicy.getZone_name());
			shippingInfoBean.setUseType(useType);
			shippingInfoBean.setSc_id(sc_id);
			
			int begin_day =  detailCompany.get("begin_day",0);
			int begin_hours = detailCompany.get("begin_hour",0);
			int last_hours = detailCompany.get("last_hour",0);
			
			TDate tDate = new TDate();
			int now_week_day = tDate.getIntWeek();
			int week_day_spacing = now_week_day-begin_day;
			
			tDate.addDay(-1*week_day_spacing);
			
			String beginDateString = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay()+" "+begin_hours+":"+"00:00";
			
			
			
			TDate begin_date = new TDate(beginDateString);
			
			long time_spacing =  System.currentTimeMillis()-begin_date.getDateTime();
			
			
			int isSelect = 0;
			if(0<time_spacing&&time_spacing<last_hours*3600*1000)
			{
				isSelect = 1;
			}

			
			shippingInfoBean.setSelectAuto(isSelect);
			return (shippingInfoBean);
		} 
		catch (ProvinceOutSizeException e)
		{
			throw e;
		}
		catch (CountryOutSizeException e)
		{
			throw e;
		}
		catch (WeightCrossException e)
		{
			throw e;
		}
		catch (WeightOutSizeException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getNewShippingFee",log);
		}
	}
	

	/**
	 * 给商品获得运费，包含体积长宽高算法
	 * @param sc_id
	 * @param ccid
	 * @param pro_id
	 * @param products(dbrow内必须包含cart_pid)
	 * @return
	 * @throws ProvinceOutSizeException
	 * @throws CountryOutSizeException
	 * @throws WeightCrossException
	 * @throws WeightOutSizeException
	 * @throws Exception
	 */
	public ShippingInfoBean getShippingFeeForProduct(long sc_id,long ccid,long pro_id,DBRow[] products)
		throws ProvinceOutSizeException,CountryOutSizeException,WeightCrossException,WeightOutSizeException,Exception
	{
		try 
		{
			float weight = 0;//运单总重量
			
			DBRow detailCompany = fem.getDetailCompany(sc_id);//快递公司
			////system.out.println( "==> "+detailCompany );
			if (detailCompany==null)
			{
				return(null);
			}

			for (int i = 0; i < products.length; i++) 
			{
				DBRow cartProduct;
				
				cartProduct = floorProductMgr.getDetailProductByPcid(products[i].get("cart_pid",0l));
					
				weight +=  cartProduct.get("weight",0f)*products[i].get("cart_quantity",1f);
					
				int length = (int)Double.parseDouble(cartProduct.getString("length"));
				int width = (int)Double.parseDouble(cartProduct.getString("width"));
				int heigth = (int)Double.parseDouble(cartProduct.getString("heigth"));
				if(length==0||width==0||heigth==0)
				{
					throw new ProductDataErrorException();
				}
			}
			
			ShippingFeeCommPolicy shippingFeeCommPolicy = null;
			
			shippingFeeCommPolicy = (ShippingFeeCommPolicy)MvcUtil.getBeanFromContainer(detailCompany.getString("imp_class"));
			shippingFeeCommPolicy.initCompanyWeightCountry(sc_id,weight,ccid,pro_id);//重量方式计算运费
			
			double basePrice = shippingFeeCommPolicy.getShippingFee();
			
			String baseMailpieceShape = "Parcel";
			String compareMailpieceShape = null;
			String useType = "";
			double useFee = basePrice;//运费
			double comparePrice = 0;
			
			if(sc_id==100006||sc_id==100014)//USPS-P计算体积方式运费
			{
				useType = baseMailpieceShape;
	//			int count = 0;
	
				 ContainerLoadingMethodMgrZJ ContainerLoadingMgr = new ContainerLoadingMethodMgrZJ(this.floorProductMgr);
				 //小箱子能否装入所有货物
				 boolean isPacked = ContainerLoadingMgr.packed(219,137,41,3,products,false);//装下返回true，没装下返回false
				 if(isPacked)
				 {
					 compareMailpieceShape = "SmallFlatRateBox";
					 comparePrice = 5.20d*systemConfig.getDoubleConfigValue("USD");
					 if ( comparePrice>0&&basePrice>comparePrice )
					 {
						 useType = compareMailpieceShape;
						 useFee = comparePrice;
					 }
					 else
					 {
						 useType = baseMailpieceShape;
					 }
				 }
				 else//中箱子
				 {				 
					//第一个中箱子能否装入所有货物
					 isPacked = ContainerLoadingMgr.packed(351,307,91,3,products,false);
					 if(isPacked==false)
					 {
						isPacked = ContainerLoadingMgr.packed(284,221,145,3,products,false);
					 }
					 if(isPacked)
					 {
						 compareMailpieceShape = "MediumFlatRateBox";
						 comparePrice = 10.95d*systemConfig.getDoubleConfigValue("USD");
						 if ( comparePrice>0&&basePrice>comparePrice )
						 {
							 useType = compareMailpieceShape;
							 useFee = comparePrice;
						 }
						 else
						 {
							 useType = baseMailpieceShape;
						 }
					 }
					 else 
					 {
						 //第一个大箱子能否装入所有货物第一种算法
						 isPacked = ContainerLoadingMgr.packed(612,308,86,3,products,false); 
						 if(isPacked==false)
						 {
							//第二个大箱子能否装入所有货物
							isPacked = ContainerLoadingMgr.packed(315,315,150,3,products,false); 
						 }
						 if(isPacked)
						 {
							 compareMailpieceShape = "LargeFlatRateBox";
							 comparePrice = 14.95d*systemConfig.getDoubleConfigValue("USD");
							 if ( comparePrice>0&&basePrice>comparePrice )
							 {
								 useType = compareMailpieceShape;
								 useFee = comparePrice;
							 }
							 else
							 {
								 useType = baseMailpieceShape;
							 }
							 
							
						 }
						 else
						 {
							 useType = baseMailpieceShape;
						 }						 
					 }
				 }
			}
			
			ShippingInfoBean shippingInfoBean = new ShippingInfoBean();
			shippingInfoBean.setCompanyName(shippingFeeCommPolicy.getCompany_name());
			shippingInfoBean.setShippingFee(MoneyUtil.round(useFee,2));
			shippingInfoBean.setWeight(shippingFeeCommPolicy.getPRO_TW());//经过计算后的重量
			shippingInfoBean.setZoneName(shippingFeeCommPolicy.getZone_name());
			shippingInfoBean.setUseType(useType);
			shippingInfoBean.setSc_id(sc_id);

			int begin_day =  detailCompany.get("begin_day",0);
			int begin_hours = detailCompany.get("begin_hour",0);
			int last_hours = detailCompany.get("last_hour",0);
			
			TDate tDate = new TDate();
			int now_week_day = tDate.getIntWeek();
			int week_day_spacing = now_week_day-begin_day;
			
			tDate.addDay(-week_day_spacing);
			
			String beginDateString = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay()+" "+begin_hours+":"+"00:00";
			
			TDate begin_date = new TDate(beginDateString);
			
			long time_spacing = System.currentTimeMillis()-begin_date.getDateTime();
			
			int isSelect = 0;
			if(0<time_spacing&&time_spacing<last_hours*3600*1000)
			{
				isSelect = 1;
			}

			
			shippingInfoBean.setSelectAuto(isSelect);
			
			return (shippingInfoBean);
		} 
		catch (ProvinceOutSizeException e)
		{
			throw e;
		}
		catch (CountryOutSizeException e)
		{
			throw e;
		}
		catch (WeightCrossException e)
		{
			throw e;
		}
		catch (WeightOutSizeException e)
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"getNewShippingFee",log);
		}
	}


	/**
	 * 得到某个地区可用的国家（没有被设置或被自己设置）
	 * @param sc_id
	 * @param sz_id
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] getZoneCountry(long sc_id,long sz_id)
		throws Exception
	{
		try 
		{
			return(fem.getZoneCountry(sc_id,sz_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getZoneCountry",log);
		}
	}
	
	/**
	 * 得到某个地区可用的省份（没有被设置或被自己设置）
	 * @param sc_id
	 * @param sz_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getZoneProvince(long sc_id,long sz_id)
		throws Exception
	{
		try 
		{
			return(fem.getZoneProvince( sc_id, sz_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getZoneProvince",log);
		}
	}
	
	/**
	 * 获得一个地区所有关联国家
	 * @param sz_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getZoneCountryMappingBySzId(long sz_id)
		throws Exception
	{
		try 
		{
			return(fem.getZoneCountryMappingBySzId(sz_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getZoneCountryMappingBySzId",log);
		}
	}
	
	/**
	 * 获得一个地区所有关联省份
	 * @param sc_id
	 * @param sz_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getZoneProvinceMappingBySzId(long sc_id,long sz_id)
		throws Exception
	{
		try 
		{
			return(fem.getZoneProvinceMappingBySzId( sc_id, sz_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getZoneProvinceMappingBySzId",log);
		}
	}

	/**
	 * 保存国家、地区映射设置
	 * @param request
	 * @throws Exception
	 */
	public void saveZoneCountryMapping(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sz_id = StringUtil.getLong(request, "sz_id");
			long sc_id = StringUtil.getLong(request, "sc_id");
			int domestic = StringUtil.getInt(request, "domestic");
			String ccid[] = request.getParameterValues("ccid");
			//两个表一起清除
			fem.delZoneCountryMappingBySzId(sz_id);
			fem.delZoneProvinceMappingBySzId(sz_id);
			
			for (int i=0; ccid!=null&&i<ccid.length; i++)
			{
				if ( StringUtil.getLong(ccid[i])>0 )
				{
					DBRow mapping = new DBRow();
					mapping.add("sc_id",sc_id);
					mapping.add("sz_id",sz_id);
					
					if (domestic==1)//国内
					{
						mapping.add("pro_id",ccid[i]);
						fem.addZoneProvinceMapping(mapping);
					}
					else
					{
						mapping.add("ccid",ccid[i]);
						fem.addZoneCountryMapping(mapping);
					}
					
				}
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"saveZoneCountryMapping",log);
		}
	}
	
	/**
	 * 只有解锁状态的快递公司才能被订单使用
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public int switchCompanyLock(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sc_id = StringUtil.getLong(request, "sc_id");
			DBRow detailCompany = fem.getDetailCompany(sc_id);
			int locked;
			
			if (detailCompany.get("locked", 0)==1)
			{
				locked = 0;
			}
			else
			{
				locked = 1;
			}
			
			DBRow lockRow = new DBRow();
			lockRow.add("locked",locked);
			fem.modCompany(sc_id, lockRow);
			
			return(locked);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"switchCompanyLock",log);
		}
	}
	
	/**
	 * 获得所有可用快递公司（没上锁）
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllValidateExpressCompany(PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fem.getAllValidateExpressCompany(pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllValidateExpressCompany",log);
		}
	}
	
	/**
	 * 通过快递ID和国家ID，获得对应的区
	 * @param sc_id
	 * @param ccid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailZoneByCcidScid(long sc_id,long ccid)
		throws Exception
	{
		try 
		{
			return(fem.getDetailZoneByCcidScid( sc_id, ccid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDetailZoneByCcidScid",log);
		}
	}

	/**
	 * 通过发货仓库，获得快递公司（没上锁）
	 * @param sc_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getExpressCompanysByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fem.getExpressCompanysByPsId( ps_id, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getExpressCompanysByPsId",log);
		}
	}
	
	/**
	 * 通过发货仓库，获得快递公司（没上锁/没上锁）
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllExpressCompanysByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fem.getAllExpressCompanysByPsId( ps_id, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllExpressCompanysByPsId",log);
		}
	}
	
	/**
	 * 通过发货仓库、国内/国外，获得快递公司（没上锁/没上锁）
	 * @param ps_id
	 * @param domestic
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllExpressCompanysByPsIdDomestic(long ps_id,int domestic,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fem.getAllExpressCompanysByPsIdDomestic( ps_id, domestic,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getAllExpressCompanysByPsIdDomestic",log);
		}
	}

	/**
	 * 复制快递公司数据
	 * @param sc_id
	 * @throws Exception
	 */
	public void copyCompany(long cur_sc_id,long sc_id,int domestic)
		throws Exception
	{
		try 
		{
			//先清空原有数据
			//删除运费
			DBRow oldZones[] = fem.getZoneByScId(cur_sc_id);
			for (int i=0; i<oldZones.length; i++)
			{
				fem.delFeeBySzId(oldZones[i].get("sz_id", 0l));
			}
			//删除重量、运费mapping
			DBRow oldWeights[] = fem.getFeeWeightSection(cur_sc_id);
			for (int i=0; i<oldWeights.length; i++)
			{
				fem.delWeightFeeMappingBySwId(oldWeights[i].get("sw_id", 0l));
			}
			fem.delZoneByScId(cur_sc_id);
			fem.delWeightByScId(cur_sc_id);
			fem.delZoneCountryMappingByScId(cur_sc_id);
			fem.delZoneProvinceMappingByScId(cur_sc_id);
			
			
			//复制插入新数据
			DBRow zones[] = fem.getZoneByScId(sc_id);
			DBRow weights[] = fem.getFeeWeightSection(sc_id);
			
			//复制重量
			for (int i=0; i<weights.length; i++)
			{
				DBRow weight = new DBRow();
				weight.add("st_weight",weights[i].getString("st_weight"));
				weight.add("en_weight",weights[i].getString("en_weight"));
				weight.add("weight_step",weights[i].getString("weight_step"));
				weight.add("sc_id",cur_sc_id);
				fem.addWeight(weight);
			}
			
			//复制地区
			for (int i=0; i<zones.length; i++)
			{
				DBRow zone = new DBRow();
				zone.add("sc_id",cur_sc_id);
				zone.add("name",zones[i].getString("name"));
				long sz_id = fem.addZone(zone);
				
				//复制国家、地区映射关系(区分国内和国际快递)
				if (domestic==1)
				{
					DBRow s_zone_country_mapping[] = fem.getZoneProvinceMappingBySzId(zones[i].get("sz_id", 0l));
					for (int ii=0; ii<s_zone_country_mapping.length; ii++)
					{
						DBRow zone_country_mapping = new DBRow();
						zone_country_mapping.add("sc_id",cur_sc_id);
						zone_country_mapping.add("sz_id",sz_id);
						zone_country_mapping.add("pro_id",s_zone_country_mapping[ii].getString("pro_id"));
						fem.addZoneProvinceMapping(zone_country_mapping);
					}
				}
				else
				{
					DBRow s_zone_country_mapping[] = fem.getZoneCountryMappingBySzId(zones[i].get("sz_id", 0l));
					for (int ii=0; ii<s_zone_country_mapping.length; ii++)
					{
						DBRow zone_country_mapping = new DBRow();
						zone_country_mapping.add("sc_id",cur_sc_id);
						zone_country_mapping.add("sz_id",sz_id);
						zone_country_mapping.add("ccid",s_zone_country_mapping[ii].getString("ccid"));
						fem.addZoneCountryMapping(zone_country_mapping);
					}					
				}
				
				//复制邮费
				//先获得被复制地区所有的运费，并关联查出重量段
				DBRow feeWeights[] = fem.getFeeBySzId(zones[i].get("sz_id", 0l));
				for (int ii=0; ii<feeWeights.length; ii++)
				{
					DBRow fee = new DBRow();
					fee.add("f_weight_fee",feeWeights[ii].get("f_weight_fee", 0d));
					fee.add("k_weight_fee",feeWeights[ii].get("k_weight_fee", 0d));
					fee.add("sz_id",sz_id);
					long sf_id = fem.addFee(fee);
					////system.out.println(sf_id);
					
					//通过重量段范围查出重量段ID
					DBRow curWeight = fem.getDetailWeightByStEnScId(cur_sc_id, feeWeights[ii].get("st_weight", 0f), feeWeights[ii].get("en_weight", 0f));
					////system.out.println(cur_sc_id+" - "+feeWeights[i].get("st_weight", 0f)+" - "+feeWeights[i].get("en_weight", 0f)+" - "+curWeight+" - "+curWeight.get("sw_id", 0l));
					
					//把复制过来重量和费用建立关联
					DBRow weightFeeMapping = new DBRow();
					weightFeeMapping.add("sw_id", curWeight.get("sw_id", 0l));
					weightFeeMapping.add("sf_id", sf_id);
					fem.addWeightFeeMapping(weightFeeMapping);
				}
				

			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"copyCompany",log);
		}
	}
	
	/**
	 * 判断是国内发货还是国际发货
	 * @param ccid		收货国家ID
	 * @param ps_id		发货仓库ID
	 * @return
	 * @throws Exception
	 */
	public boolean isDomesticShipping(long ccid,long ps_id)
		throws Exception
	{
		try 
		{
			boolean flag = false;
			////system.out.println( ccid+" - "+ ps_id);
			
			DBRow detailStorage = fcm.getDetailProductStorageCatalogById(ps_id);
			//如果收货国家ID=仓库所属国家ID，是国内发货
			if (detailStorage.get("native", 0l)==ccid)
			{
				flag = true;
			}
			
			return(flag);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"isDomesticShipping",log);
		}
	}	
	
	/**
	 * 通过 仓库ID 省份ID 重量，获得可以发货快递
	 * @param ps_id
	 * @param pro_id
	 * @param weight
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDomesticExpressCompanyByPsIdProId(long ps_id,long pro_id,float weight,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fem.getDomesticExpressCompanyByPsIdProId(ps_id, pro_id, weight, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDomesticExpressCompanyByPsIdProId",log);
		}
	}
	
	public DBRow[] getDomesticExpressCompanyByPsIdProId(long ps_id,long pro_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fem.getDomesticExpressCompanyByPsIdProId(ps_id, pro_id, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDomesticExpressCompanyByPsIdProId",log);
		}
	}
	
	/**
	 * 通过仓库ID ， 国家ID，重量，获得可以发货快递
	 * @param ps_id
	 * @param ccid
	 * @param weight
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getInternationalExpressCompanyByPsIdProId(long ps_id,long ccid,float weight,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fem.getInternationalExpressCompanyByPsIdProId( ps_id, ccid, weight, pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getInternationalExpressCompanyByPsIdProId",log);
		}
	}
	
	public DBRow[] getInternationalExpressCompanyByPsIdProId(long ps_id,long ccid,PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fem.getInternationalExpressCompanyByPsIdProId( ps_id, ccid,  pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getInternationalExpressCompanyByPsIdProId",log);
		}
	}
		
	/**
	 * 获得一组快递
	 * @param sc_ids
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getExpressCompanysByScIds(long sc_ids[],PageCtrl pc)
		throws Exception
	{
		try 
		{
			return(fem.getExpressCompanysByScIds(sc_ids,pc));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getExpressCompanysByScIds",log);
		}
	}
				

	/**
	 * 导入运费价格表
	 * @param request
	 * @return
	 * @throws Exception
	 * @throws FileTypeException
	 */
	public String importExpressRate(HttpServletRequest request)
		throws Exception,FileTypeException
	{
		try 
		{
			String permitFile = "xls";
			TUpload upload = new TUpload();
			String msg = new String();
	
			upload.setFileName("import_express_rate_"+DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()));
			upload.setRootFolder("/upl_excel_tmp/");
			upload.setPermitFile(permitFile);
			upload.setFileSize(1024*1024*10);
			
			int flag = upload.upload(request);
			
				
			if (flag==2)
			{
				msg = "只能上传:"+permitFile;
				throw new FileTypeException(msg);//采购单文件上传格式不对
			}
			else if (flag==1)
			{
				msg = "上传出错";
				throw new FileTypeException(msg);
			}
			else
			{		  
				msg = upload.getFileName();
			}
			
			return msg;
		} 
		catch (FileTypeException e) 
		{
			throw e;
		}
		catch (Exception e) 
		{
			throw new SystemException(e,"importExpressRate",log);
		}
	}
	

	/**
	 * 导出快递费用
	 * @param request
	 * @return
	 * @throws WareHouseErrorException
	 * @throws Exception
	 */
	public String exportExpressFee(HttpServletRequest request) 
		throws Exception 
	{
		FileOutputStream fout = null;
		
		try 
		{
			long sc_id = StringUtil.getLong(request,"sc_id");
			
			DBRow detailCompany = fem.getDetailCompany(sc_id);
			DBRow zones[] = this.getZoneByScId(sc_id);
			DBRow feeWeightSection[] = this.getFeeWeightSection(sc_id);
			
			String weight_unit = detailCompany.getString("weight_unit");
			float weight_rate = detailCompany.get("weight_rate",0f);
			String currency = detailCompany.getString("currency");
			float currency_rate = detailCompany.get("currency_rate",0f);
			
			//重新分装下数据，减少数据库查询
			DBRow zone_fee = new DBRow();
			for (int z=0; z<zones.length; z++)
			{
				String key;
				DBRow fee[] = this.getFeeBySzId(zones[z].get("sz_id",0l));
				
				for (int f=0; f<fee.length; f++)
				{
					key = zones[z].getString("sz_id")+"_"+fee[f].getString("st_weight")+"_"+fee[f].getString("en_weight");
					zone_fee.add(key,fee[f]);
				}		
			}
			
			//获得模板文件
			POIFSFileSystem exportProfitModel = new POIFSFileSystem(new FileInputStream(Environment.getHome()+"/administrator/shipping/template.xls"));
			HSSFWorkbook workbook = new HSSFWorkbook(exportProfitModel);
			
			//生成数据页面
			HSSFSheet currencySheet = workbook.getSheetAt(0);
			//currencySheet.protectSheet(String.valueOf(System.currentTimeMillis()));
			//单元格样式
			HSSFCellStyle currencyStyle = workbook.createCellStyle();
			currencyStyle.setWrapText(true);
			currencyStyle.setLocked(false);
			
			//画运费表信息
			HSSFRow infoRow = currencySheet.createRow(0);
			HSSFCell expressNameCell = infoRow.createCell(0);
			expressNameCell.setCellStyle(currencyStyle);
			expressNameCell.setCellValue(detailCompany.getString("name"));
			currencySheet.addMergedRegion(new CellRangeAddress(0,0,0,2));
			
			HSSFCell weightUnitCell = infoRow.createCell(3);
			weightUnitCell.setCellStyle(currencyStyle);
			weightUnitCell.setCellValue("重量:"+weight_unit);
			
			
			HSSFCell currencyUnitCell = infoRow.createCell(4);
			currencyUnitCell.setCellStyle(currencyStyle);
			currencyUnitCell.setCellValue("货币:"+currency);
			
			//表头样式
			HSSFFont f  = workbook.createFont();
			f.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);//加粗   
			
			HSSFCellStyle titleStyle = workbook.createCellStyle();
			titleStyle.setFont(f);
			titleStyle.setWrapText(true);
			titleStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			titleStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
			titleStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			titleStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			titleStyle.setLocked(true);
			//画表头
			HSSFRow titleyRow = currencySheet.createRow(1);
			for (int i=0; i<3; i++)
			{
				HSSFCell titleCell = titleyRow.createCell(i);
				titleCell.setCellStyle(titleStyle);
				titleCell.setCellValue("重量段");
			}
			int titleCellIndex = 3;
			for (int z=0; z<zones.length; z++)
			{
				HSSFCell titleCell = titleyRow.createCell(titleCellIndex++);
				titleCell.setCellStyle(titleStyle);
				titleCell.setCellValue(zones[z].getString("name"));
				
				titleCell = titleyRow.createCell(titleCellIndex++);
				titleCell.setCellStyle(titleStyle);
				titleCell.setCellValue(zones[z].getString("name"));
			}
			for (int z=0; z<zones.length; z++)
			{
				currencySheet.addMergedRegion(new CellRangeAddress(1,1,z*2+3,z*2+4));//合并重量段表头
			}
			currencySheet.addMergedRegion(new CellRangeAddress(1,1,0,2));//合并重量段表头
			
			int cellIndex = 0;
			for (int k=0; k<feeWeightSection.length; k++)
			{
				HSSFRow currencyRow = currencySheet.createRow(k+2);
				//先设置重量段和步长
				HSSFCell feeWeightSectionCell = currencyRow.createCell(cellIndex++);
				feeWeightSectionCell.setCellStyle(currencyStyle);
				feeWeightSectionCell.setCellValue(MoneyUtil.round(feeWeightSection[k].get("st_weight",0f)*detailCompany.get("weight_rate",0f), 2));
				
				feeWeightSectionCell = currencyRow.createCell(cellIndex++);
				feeWeightSectionCell.setCellStyle(currencyStyle);
				feeWeightSectionCell.setCellValue(MoneyUtil.round(feeWeightSection[k].get("en_weight",0f)*detailCompany.get("weight_rate",0f),2));
				
				feeWeightSectionCell = currencyRow.createCell(cellIndex++);
				feeWeightSectionCell.setCellStyle(currencyStyle);
				feeWeightSectionCell.setCellValue(MoneyUtil.round(feeWeightSection[k].get("weight_step",0f)*detailCompany.get("weight_rate",0f),2));
				
				for (int z=0; z<zones.length; z++)
				{
					Object obj = zone_fee.get(zones[z].getString("sz_id")+"_"+feeWeightSection[k].getString("st_weight")+"_"+feeWeightSection[k].getString("en_weight"),new Object());
					DBRow fee = (DBRow)obj;
					
					HSSFCell zonesCell = currencyRow.createCell(cellIndex++);
					zonesCell.setCellStyle(currencyStyle);
					zonesCell.setCellValue(MoneyUtil.round(fee.get("f_weight_fee",0d)/detailCompany.get("currency_rate",0f),2));
					
					zonesCell = currencyRow.createCell(cellIndex++);
					zonesCell.setCellStyle(currencyStyle);
					zonesCell.setCellValue(MoneyUtil.round(fee.get("k_weight_fee",0d)/detailCompany.get("currency_rate",0f),2));
				}
				cellIndex = 0;
			}
			
			//存放快递寄出数据
			HSSFSheet dataSheet = workbook.getSheetAt(1);
			dataSheet.createRow(0).createCell(0).setCellValue(String.valueOf(sc_id));//快递公司ID
			dataSheet.createRow(1).createCell(0).setCellValue(String.valueOf(weight_rate));//重量转换率
			dataSheet.createRow(2).createCell(0).setCellValue(String.valueOf(currency_rate));//货币汇率
			
			//生成excel文件
			String path = "upl_excel_tmp/express_fee_"+ DateUtil.FormatDatetime("yyyyMMddHHmmssSS",new Date()) + ".xls";
			fout = new FileOutputStream(Environment.getHome()+ path);
			workbook.write(fout);
			
			return(path);
	    } 
		catch (Exception e) 
		{
			throw new SystemException(e,"exportExpressFee",log);
	    }
		finally
		{
			if (fout!=null)
			{
				fout.close();
			}
		}
	}
	
	
	
	
	/**
	 * 更新运费
	 * @param filename
	 * @throws Exception
	 */
	public void updateExpressRateByExcel(String filename)
		throws Exception
	{
		InputStream is = null;
		Workbook rwb = null;  
	
		try 
		{
			String path = "";
			path = Environment.getHome() + "upl_excel_tmp/" + filename;
			is = new FileInputStream(path);
			rwb = Workbook.getWorkbook(is);
			
			Pattern p = Pattern.compile("^(\\d+)(\\.\\d+)?$");//整数或浮点数
//			Matcher m = p.matcher(price);
//			boolean b = m.matches();
			
			Sheet rs = rwb.getSheet(0);
			int rsColumns = rs.getColumns();  //excel表字段数
			int rsRows = rs.getRows();    //excel表记录行数
			//获得快递公司基础数据
			Sheet dataRs = rwb.getSheet(1);
			long sc_id = StringUtil.getLong( dataRs.getCell(0,0).getContents().trim() );
			double  weight_rate = StringUtil.getDouble( dataRs.getCell(0,1).getContents().trim() );
			double  currency_rate = StringUtil.getDouble( dataRs.getCell(0,2).getContents().trim() );
			
			//导入数据前，先清空原来重量段和运费数据
			DBRow weights[] = fem.getFeeWeightSection(sc_id);//获得该快递所有重量段
			DBRow zones[] = fem.getZoneByScId(sc_id);//获得该快递所有地区
			for (int i=0; i<weights.length; i++)
			{
				fem.delWeightFeeMappingBySwId(weights[i].get("sw_id", 0l));
			}
			for (int i=0; i<zones.length; i++)
			{
				fem.delFeeBySzId(zones[i].get("sz_id", 0l));
			}
			fem.delWeightByScId(sc_id);
			
			//开始插入数据
			for (int y=2; y<rsRows; y++)
			{
				//空行不做处理
				if (rs.getCell(0,y).getContents().trim().equals("")||y<2)
				{
					continue;
				}
				//先插入重量段
				DBRow weight = new DBRow();
				weight.add("st_weight", ((NumberCell)rs.getCell(0,y)).getValue()/weight_rate);
				weight.add("en_weight", ((NumberCell)rs.getCell(1,y)).getValue()/weight_rate);
				weight.add("sc_id", sc_id);
				weight.add("weight_step",((NumberCell)rs.getCell(2,y)).getValue()/weight_rate);
				long sw_id = fem.addWeight(weight);
				//计算有多少个地区
				int zoneCount = (rsColumns-3)/2;
				for (int x=0; x<zoneCount; x++)
				{
					int st = 2*x+3;
					int en = 2*x+4;
					String zoneName = rs.getCell(st,1).getContents();
					
					DBRow fee = new DBRow();
					fee.add("sz_id", fem.getDetailZoneByScidName(sc_id, zoneName).getString("sz_id"));
					fee.add("f_weight_fee", ((NumberCell)rs.getCell(st,y)).getValue()*currency_rate );
					fee.add("k_weight_fee", ((NumberCell)rs.getCell(en,y)).getValue()*currency_rate);
					long sf_id = fem.addFee(fee);
					
					DBRow weight_fee_mapping = new DBRow();
					weight_fee_mapping.add("sw_id", sw_id);
					weight_fee_mapping.add("sf_id", sf_id);
					fem.addWeightFeeMapping(weight_fee_mapping);
					
					////system.out.print(rs.getCell(st,1).getContents()+" st:"+rs.getCell(st,y).getContents()+" en:"+rs.getCell(en,y).getContents()+" | ");
				}
				////system.out.println("");
			}
			
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"updateExpressRateByExcel",log);
		}
		finally
		{
			if (is!=null)
			{
				is.close();
			}
			if (rwb!=null)
			{
				rwb.close();
			}
		}
	}
	
	/**
	 * 修改重量单位和货币单位
	 * @param request
	 * @throws Exception
	 */
	public void modWeightCurrency(HttpServletRequest request)
		throws Exception
	{
		try
		{
			long sc_id = StringUtil.getLong(request, "sc_id");
			String weight_unit = StringUtil.getString(request, "weight_unit");
			String currency = StringUtil.getString(request, "currency");
			
			float weight_rate = StringUtil.getFloat(request, "weight_rate");
			float currency_rate = StringUtil.getFloat(request,"currency_rate");
			
			DBRow company = new DBRow();
			company.add("weight_unit",weight_unit);
			company.add("currency",currency);
			company.add("weight_rate",weight_rate);
			company.add("currency_rate",currency_rate);
			fem.modCompany(sc_id, company);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"modWeightCurrency",log);
		}
	}
	
	public DBRow getDetailZone(long sc_id,long ccid,long pro_id)
		throws Exception
	{
		DBRow detailCompany = fem.getDetailCompany(sc_id);
		
		DBRow detailZone;
		if (detailCompany.get("domestic", 0)==1)//区分国内快递还是国际快递
		{
			detailZone = fem.getDetailZoneByProIdScid(sc_id, pro_id);
			if (detailZone==null)
			{
				throw new ProvinceOutSizeException();
			}
		}
		else
		{
			detailZone = fem.getDetailZoneByCcidScid(sc_id, ccid);
			if (detailZone==null)
			{
				throw new CountryOutSizeException();
			}
		}
		
		return detailZone;
	}
	
	/**
	 * 修改快递公司禁止自动选择时间
	 * @param request
	 * @throws Exception
	 */
	public void modCompanyUseTime(HttpServletRequest request)
		throws Exception
	{
		try 
		{
			long sc_id = StringUtil.getLong(request,"sc_id");
			int begin_day = StringUtil.getInt(request,"begin_day");
			int begin_hour = StringUtil.getInt(request,"begin_hour");
			int last_hour = StringUtil.getInt(request,"last_hour");
			
			DBRow modUseTimePara = new DBRow();
			modUseTimePara.add("begin_day",begin_day);
			modUseTimePara.add("begin_hour",begin_hour);
			modUseTimePara.add("last_hour",last_hour);
			
			fem.modCompany(sc_id, modUseTimePara);
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"modCompanyUseTime",log);
		}
		
	}
		
	
	public void setFem(FloorExpressMgr fem)
	{
		this.fem = fem;
	}
	
	public void setFcm(FloorCatalogMgr fcm)
	{
		this.fcm = fcm;
	}

	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}	
	
}




