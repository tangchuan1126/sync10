package com.cwc.app.advice;

import java.lang.reflect.Method;

import org.springframework.aop.ThrowsAdvice;

import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SystemExceptionAdvice implements ThrowsAdvice
{
	/**
	 * 只拦截SystemException
	 * @param method
	 * @param args
	 * @param target
	 * @param e
	 * @throws Throwable
	 */
    public void afterThrowing(Method method,Object[] args,Object target,SystemException e)
    	throws Throwable 
    {
    	String methodName = target.getClass().getName().toString().concat(".").concat(method.getName());
		//堆栈信息
		StackTraceElement[] ste = e.getStackTrace();
		StringBuffer sb = new StringBuffer();
		sb.append(e.getMessage() + "<br>");
		for (int i = 0;i < ste.length;i++)
		{
			sb.append(ste[i].toString() + "<br>");
		}
		
		String[] to = {"542589148@qq.com","329169773@qq.com"};
		String subject = methodName+" error:"+e.getMessage();
    	
		User user = new User("echo@vvme.com");
        MailAddress mailAddress = new MailAddress(to);
        MailBody mailBody = new MailBody(subject,sb.toString(),true,null);

        Mail mail = new Mail(user);
        mail.setAddress(mailAddress);
        mail.setMailBody(mailBody);
        mail.send();
        
        
        
        String to2 = "name.declare@gmail.com";
        MailAddress mailAddress2 = new MailAddress(to);
        
        Mail mail2 = new Mail(user);
        mail2.setAddress(mailAddress2);
        mail2.setMailBody(mailBody);
        mail2.send();
        
    }

    /**
     *     public void afterThrowing(
            Method method,
            Object[] args,
            Object target,
            RuntimeException throwable) {
        //system.out.println("Throw.afterThrowing()");
        //system.out.println("method name: " + method.getName());
        Type[] type = method.getGenericParameterTypes();
        for(int i = 0; i < type.length; i++) {
            //system.out.println(type[i].toString() + ": " + args[i]);
        }
        //system.out.println("target: " + target.toString());
        //system.out.println("throw: " + throwable.getClass().getName());
        throw new NumberFormatException();
    }
}
     */
    
}
