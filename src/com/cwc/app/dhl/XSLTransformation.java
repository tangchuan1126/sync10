package com.cwc.app.dhl;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;


// Imported java classes
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


/**
 *  Use the TraX interface to perform a transformation in the simplest manner possible
 *  (3 statements).
 */
public class XSLTransformation
{
	public XSLTransformation ()
	{
		//Default Constructor
	}

	public static void main(String[] args)
    throws TransformerException, TransformerConfigurationException, 
           FileNotFoundException, IOException
  {  


  }

 /**
  * XSL transforms the xml file to the output 
  */

 public void XSLTransformation (String XMLFilePath, String XSLFilePath, String OutputFilePath) 
	  throws TransformerException, TransformerConfigurationException, 
           FileNotFoundException, IOException
	{
		// Use the static TransformerFactory.newInstance() method to instantiate 
		// a TransformerFactory. The javax.xml.transform.TransformerFactory 
		// system property setting determines the actual class to instantiate --
		// org.apache.xalan.transformer.TransformerImpl.
		TransformerFactory tFactory = TransformerFactory.newInstance();

		// Use the TransformerFactory to instantiate a Transformer that will work with  
		// the stylesheet you specify. This method call also processes the stylesheet
		// into a compiled Templates object.
		Transformer transformer = tFactory.newTransformer(new StreamSource(XSLFilePath));

		// Use the Transformer to apply the associated Templates object to an XML document
		// (foo.xml) and write the output to a file (foo.out).
		transformer.transform(new StreamSource(XMLFilePath), new StreamResult(new FileOutputStream(OutputFilePath)));

		////system.out.println("      The result file is in " + OutputFilePath );  
  }
}