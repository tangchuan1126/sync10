package com.cwc.app.dhl;
                                    
import java.io.InputStream;                                                                                    
import java.io.DataOutputStream;                                        
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;                                           
import java.io.IOException;                                                             
import java.net.MalformedURLException;
// Net classes
import java.net.SocketException;
import java.net.URL;                                                                                      
import java .net.URLConnection;                                 

//Text Classes

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//Util classes

//Parse Packages








import javax.xml.parsers.*;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;









import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.util.Dom4j;
import com.cwc.util.StringUtil;


/**
 *   This class contains is a sample client class used to send request XML messages to XML Shipping service of DHL
 * 
 *   @author Dhawal Jogi (Infosys)
 **/
public class  DHLClient
{  
//	private String requestMessagePath = Environment.getHome()+"administrator/order/TransformXMLtoHTML/RequestXML/ShipmentValidate_vvme.xml";
	private String requestTrackingPath = Environment.getHome()+"administrator/order/TransformXMLtoHTML/RequestXML/track.xml";
//	private String httpURL =  "http://xmlpi.dhl-usa.com/XMLShippingServlet";
//	private String httpURL =  "http://xmlshippingtest.dhl-usa.com/XMLShippingServlet";
//	private String responseMessagePath =  Environment.getHome()+"administrator/order/TransformXMLtoHTML/ResponseXMLS/";	
	
	private String httpURL = "https://xmlpi-ea.dhl.com/XMLShippingServlet";
	private String requestMessagePath ="D:\\Sync Workspace STS\\Sync10\\WebRoot\\administrator\\order\\TransformXMLtoHTML\\RequestXML\\ShipmentValidate_vvme.xml";
	private String responseMessagePath = "D:\\DHL\\Waybill\\";
    private String Consignee_CompanyName ;
    private String Consignee_AddressLine;
    private String Consignee_City ;
    private String Consignee_DivisionCode;
    private String Consignee_PostalCode ;
    private String Consignee_CountryCode ;
    private String Consignee_CountryName ;
    
    private String Consignee_Contact_PersonName ;
    private String Consignee_Contact_PhoneNumber;
    
    private String Consignee_Contact_Email_From;
    private String Consignee_Contact_Email_To ;

    private String Dutiable_DeclaredValue ;
    private String ShipmentDetails_NumberOfPieces;
    private String ShipmentDetails_Weight;
    
    private static String oid;
    
    private static String airWayBillNumberIn;
    
    private static String errorPage;
    
    //--------------------
    private String Dev_CompanyName;
    private String Dev_AddressLine1;
    private String Dev_AddressLine2;
    private String Dev_AddressLine3;
    private String Dev_City;
    private String Dev_DivisionCode;
    private String Dev_PostalCode;
    private String Dev_CountryCode;
    private String Dev_CountryName;
    private String Dev_PhoneNumber;
    
    private String destination;
    
    private String dutyPaymentType;
    
    private String Contents;

    public String getDestination() {
		return destination;
	}


	static Logger log = Logger.getLogger("ACTION");
    
    public void setErrorPage(String errorPage)
    {
    	this.errorPage = errorPage;
    }
    
    public String getErrorPage()
    {
    	return(this.errorPage);
    }
    
    public void setOid(String oid)
    {
    	this.oid = oid;
    }
    
    public void setShipmentDetailsWeight(float ShipmentDetails_Weight)
    {
    	this.ShipmentDetails_Weight = StringUtil.formatNumber("0.00",ShipmentDetails_Weight);
    }
    
    public void setConsigneeCompanyName(String Consignee_CompanyName)
    {
    	this.Consignee_CompanyName = Consignee_CompanyName;
    }
	
    public void setConsigneeAddressLine(String Consignee_AddressLine)
    {
    	this.Consignee_AddressLine = Consignee_AddressLine;
    }
    
    public void setConsigneeCity(String Consignee_City)
    {
    	this.Consignee_City = Consignee_City;
    }
    
    public void setConsigneeDivisionCode(String Consignee_DivisionCode)
    {
    	this.Consignee_DivisionCode = Consignee_DivisionCode;
    }
    
    public void setConsigneePostalCode(String Consignee_PostalCode)
    {
    	this.Consignee_PostalCode = Consignee_PostalCode;
    }
    
    public void setConsigneeCountryCode(String Consignee_CountryCode)
    {
    	this.Consignee_CountryCode = Consignee_CountryCode;
    }
    
    public void setConsigneeCountryName(String Consignee_CountryName)
    {
    	this.Consignee_CountryName = Consignee_CountryName;
    }
	
    public void setConsigneeContactPersonName(String Consignee_Contact_PersonName)
    {
    	this.Consignee_Contact_PersonName = Consignee_Contact_PersonName;
    }
    
    public void setConsigneeContactPhoneNumber(String Consignee_Contact_PhoneNumber)
    {
    	this.Consignee_Contact_PhoneNumber = Consignee_Contact_PhoneNumber;
    }
    
    public void setConsigneeContactEmailFrom(String Consignee_Contact_Email_From)
    {
    	this.Consignee_Contact_Email_From = Consignee_Contact_Email_From;
    }
    
    public void setConsigneeContactEmailTo(String Consignee_Contact_Email_To)
    {
    	this.Consignee_Contact_Email_To = Consignee_Contact_Email_To;
    }
    
    public void setDutiableDeclaredValue(String Dutiable_DeclaredValue)
    {
    	this.Dutiable_DeclaredValue = Dutiable_DeclaredValue;
    }
    
    public void setShipmentDetailsNumberOfPieces(String ShipmentDetails_NumberOfPieces)
    {
    	this.ShipmentDetails_NumberOfPieces = ShipmentDetails_NumberOfPieces;
    }
    
	/**
	 * @param dev_AddressLine1 要设置的 dev_AddressLine1。
	 */
	public void setDev_AddressLine1(String dev_AddressLine1)
	{
		Dev_AddressLine1 = dev_AddressLine1;
	}

	/**
	 * @param dev_AddressLine2 要设置的 dev_AddressLine2。
	 */
	public void setDev_AddressLine2(String dev_AddressLine2)
	{
		Dev_AddressLine2 = dev_AddressLine2;
	}

	/**
	 * @param dev_AddressLine3 要设置的 dev_AddressLine3。
	 */
	public void setDev_AddressLine3(String dev_AddressLine3)
	{
		Dev_AddressLine3 = dev_AddressLine3;
	}

	/**
	 * @param dev_City 要设置的 dev_City。
	 */
	public void setDev_City(String dev_City)
	{
		Dev_City = dev_City;
	}

	/**
	 * @param dev_CompanyName 要设置的 dev_CompanyName。
	 */
	public void setDev_CompanyName(String dev_CompanyName)
	{
		Dev_CompanyName = dev_CompanyName;
	}

	/**
	 * @param dev_CountryCode 要设置的 dev_CountryCode。
	 */
	public void setDev_CountryCode(String dev_CountryCode)
	{
		Dev_CountryCode = dev_CountryCode;
	}

	/**
	 * @param dev_CountryName 要设置的 dev_CountryName。
	 */
	public void setDev_CountryName(String dev_CountryName)
	{
		Dev_CountryName = dev_CountryName;
	}

	/**
	 * @param dev_DivisionCode 要设置的 dev_DivisionCode。
	 */
	public void setDev_DivisionCode(String dev_DivisionCode)
	{
		Dev_DivisionCode = dev_DivisionCode;
	}

	/**
	 * @param dev_PhoneNumber 要设置的 dev_PhoneNumber。
	 */
	public void setDev_PhoneNumber(String dev_PhoneNumber)
	{
		Dev_PhoneNumber = dev_PhoneNumber;
	}

	/**
	 * @param dev_PostalCode 要设置的 dev_PostalCode。
	 */
	public void setDev_PostalCode(String dev_PostalCode)
	{
		Dev_PostalCode = dev_PostalCode;
	}
	
    
    
    
    
    

            	private org.dom4j.Document getXMLDoc(String path)
	        		throws Exception
	        	{
	        		Dom4j dom4j = new Dom4j();
	        		org.dom4j.Document doc = dom4j.getDocument(path);
	        		return(doc);
	        	}
            	
            	private org.dom4j.Document getXMLDocForStringXML(String xml)
            		throws Exception
            	{
            		Dom4j dom4j = new Dom4j();
	        		org.dom4j.Document doc = dom4j.getDocumentForString(xml);
	        		return(doc);
            	}
            	
            	/**
            	 * 自行加载XML，并修改属性，最后输出String
            	 * @return
            	 * @throws Exception 
            	 */
            	private String getXMLString(String path) throws Exception
            	{
                    

                    String ShipmentDetails_Date = DateUtil.DateStr();

                    
                    
                    ////system.out.println("++++++++++++++++++++++++++++++++++++++++++");
                    	//先判断address是否超长，并作分解
                    	boolean addressOutLength = Consignee_AddressLine.length()>35;
                    	String a1 = Consignee_AddressLine;
                    	String a2 = Consignee_AddressLine;
                    	
                    	if (addressOutLength)
                    	{
                    		String t = Consignee_AddressLine;
                    		
                    		String t2 = t.substring(0,35);
                    		int a = t2.lastIndexOf(" ");
                    		
                    		a1 = t.substring(0,a).trim();
                    		a2 = t.substring(a,t.length()).trim();
                    	}
                    
                    
	               		org.dom4j.Document doc = getXMLDoc(path);
	               		
	               		org.dom4j.Element element1 = (org.dom4j.Element)doc.selectSingleNode("//ServiceHeader");
	               	    Iterator iter1 = element1.elementIterator();
	               	    while(iter1.hasNext())
	                    {
	                    	org.dom4j.Element subelement = (org.dom4j.Element)iter1.next();
	                    	
	                    	if (subelement.getName().equals("MessageTime"))
	                    	{
	                    		String cc =  DateUtil.NowStrDHL();
	                    		subelement.setText(cc);
	                    	}
//	                    	else if (subelement.getName().equals("MessageReference"))
//	                    	{
//	                    		subelement.setText(String.valueOf(System.currentTimeMillis()));
//	                    	}
	                    }
	               	    
	               	    org.dom4j.Element elementBill = (org.dom4j.Element)doc.selectSingleNode("//Billing");
	               	    Iterator iterBill = elementBill.elementIterator();
	               	    while(iterBill.hasNext())
	                    {
	                    	org.dom4j.Element subelement = (org.dom4j.Element)iterBill.next();
	                    	
	                    	if (subelement.getName().equals("DutyPaymentType"))
	                    	{
	                    		if(this.dutyPaymentType.toLowerCase().equals("sender"))
	                    		{
	                    			subelement.setText("S");
	                    		}
	                    		else if(this.dutyPaymentType.toLowerCase().equals("recipient"))
	                    		{
	                    			subelement.setText("R");
	                    		}
	                    		
	                    	}
//	                    	else if (subelement.getName().equals("MessageReference"))
//	                    	{
//	                    		subelement.setText(String.valueOf(System.currentTimeMillis()));
//	                    	}
	                    }
	               	    
	               		
	               		org.dom4j.Element element = (org.dom4j.Element)doc.selectSingleNode("//Consignee");
	               	    Iterator iter=element.elementIterator();
	                    while(iter.hasNext())
	                    {
	                    	org.dom4j.Element subelement = (org.dom4j.Element)iter.next();
	                    	
	                    	if (subelement.getName().equals("CompanyName"))
	                    	{
	                    		subelement.setText(Consignee_CompanyName);
	 
	                    	}
	                    	else if (subelement.getName().equals("AddressLine"))
	                    	{
	                    		subelement.setText(a1);	                    		
	                    	}
	                    	else if (subelement.getName().equals("AddressLine2"))
	                    	{
	                    		if (addressOutLength)
	                    		{
	                    			subelement.setText(a2);
	                    			subelement.setName("AddressLine");	 
	                    		}
	                    		else
	                    		{
	                    			element.remove(subelement);
	                    		}	
	                    		                   		
	                    	}
	                    	else if (subelement.getName().equals("City"))
	                    	{
	                    		subelement.setText(Consignee_City);
	                    	}
	                    	else if (subelement.getName().equals("DivisionCode"))
	                    	{
	                    		subelement.setText(Consignee_DivisionCode);
	                    	}
	                    	else if (subelement.getName().equals("PostalCode"))
	                    	{
	                    		subelement.setText(Consignee_PostalCode);
	                    	}
	                    	else if (subelement.getName().equals("CountryCode"))
	                    	{
	                    		subelement.setText(Consignee_CountryCode);
	                    	}
	                    	else if (subelement.getName().equals("CountryName"))
	                    	{
	                    		subelement.setText(Consignee_CountryName);
	                    	}               	
	                   }
	                    
	               		element = (org.dom4j.Element)doc.selectSingleNode("//Consignee/Contact");
	               	    iter=element.elementIterator();
	                    while(iter.hasNext())
	                    {
	                    	org.dom4j.Element subelement = (org.dom4j.Element)iter.next();
	                    	
	                    	if (subelement.getName().equals("PersonName"))
	                    	{
	                    		subelement.setText(Consignee_Contact_PersonName);
	 
	                    	}
	                    	else if (subelement.getName().equals("PhoneNumber"))
	                    	{
	                    		subelement.setText(Consignee_Contact_PhoneNumber);
	                    	}            	
	                   }
	                    
	               		element = (org.dom4j.Element)doc.selectSingleNode("//Consignee/Contact/Email");
	               	    iter=element.elementIterator();
	                    while(iter.hasNext())
	                    {
	                    	org.dom4j.Element subelement = (org.dom4j.Element)iter.next();
	                    	
	                    	if (subelement.getName().equals("From"))
	                    	{
	                    		subelement.setText(Consignee_Contact_Email_From);
	 
	                    	}
	                    	else if (subelement.getName().equals("To"))
	                    	{
	                    		subelement.setText(Consignee_Contact_Email_To);
	                    	}            	
	                   }
	                    
	               		element = (org.dom4j.Element)doc.selectSingleNode("//Dutiable");
	               	    iter=element.elementIterator();
	                    while(iter.hasNext())
	                    {
	                    	org.dom4j.Element subelement = (org.dom4j.Element)iter.next();
	                    	
	                    	if (subelement.getName().equals("DeclaredValue"))
	                    	{
	                    		subelement.setText(Dutiable_DeclaredValue);
	                    	}
	                    	
	                    	if (subelement.getName().equals("TermsOfTrade"))
	                    	{
	                    		if(this.dutyPaymentType.toLowerCase().equals("sender"))
	                    		{
	                    			subelement.setText("DDP");
	                    		}
	                    		
	                    	}
	                    	
	                    	
	                   }
	                    
	               		element = (org.dom4j.Element)doc.selectSingleNode("//ShipmentDetails");
	               	    iter=element.elementIterator();
	                    while(iter.hasNext())
	                    {
	                    	org.dom4j.Element subelement = (org.dom4j.Element)iter.next();
	                    	
	                    	if (subelement.getName().equals("NumberOfPieces"))
	                    	{
	                    		subelement.setText(ShipmentDetails_NumberOfPieces);
	 
	                    	}     
	                    	else if (subelement.getName().equals("Date"))
	                    	{
	                    		subelement.setText(ShipmentDetails_Date);
	                    	}
	                    	else if (subelement.getName().equals("Weight"))
	                    	{
	                    		subelement.setText(ShipmentDetails_Weight);
	                    	}
	                    	else if(subelement.getName().equals("Contents"))
	                    	{
	                    		subelement.setText(Contents);
	                    	}
	                   }
	                    
	                    //修改发件人信息
	               		element = (org.dom4j.Element)doc.selectSingleNode("//Shipper");
	               	    iter=element.elementIterator();
	               	    int addressLineCounter = 0;
	                    while(iter.hasNext())
	                    {
	                    	org.dom4j.Element subelement = (org.dom4j.Element)iter.next();
	                    	
	                    	if (subelement.getName().equals("CompanyName"))
	                    	{
	                    		subelement.setText(Dev_CompanyName);
	 
	                    	}      
	                    	else if (subelement.getName().equals("City"))
	                    	{
	                    		subelement.setText(Dev_City);
	                    	}
	                    	else if (subelement.getName().equals("DivisionCode"))
	                    	{
	                    		subelement.setText(Dev_DivisionCode);
	                    	}
	                    	else if (subelement.getName().equals("PostalCode"))
	                    	{
	                    		subelement.setText(Dev_PostalCode);
	                    	}
	                    	else if (subelement.getName().equals("CountryCode"))
	                    	{
	                    		subelement.setText(Dev_CountryCode);
	                    	}
	                    	else if (subelement.getName().equals("CountryName"))
	                    	{
	                    		subelement.setText(Dev_CountryName);
	                    	}
	                    	else if (subelement.getName().equals("AddressLine"))
	                    	{
	                    		addressLineCounter++;
	                    		
	                    		if (addressLineCounter==1)
	                    		{
	                    			subelement.setText(Dev_AddressLine1);
	                    		}
	                    		else if (addressLineCounter==2)
	                    		{
	                    			subelement.setText(Dev_AddressLine2);
	                    		}
	                    		else if (addressLineCounter==3)
	                    		{
	                    			subelement.setText(Dev_AddressLine3);
	                    		}
	                    	}
	                   }
	               	
	               		element = (org.dom4j.Element)doc.selectSingleNode("//Shipper/Contact");
	               	    iter=element.elementIterator();
	                    while(iter.hasNext())
	                    {
	                    	org.dom4j.Element subelement = (org.dom4j.Element)iter.next();
	                    	
	                    	if (subelement.getName().equals("PhoneNumber"))
	                    	{
	                    		subelement.setText(Dev_PhoneNumber);
	 
	                    	}  
	                    	else if (subelement.getName().equals("PersonName"))
	                    	{
	                    		subelement.setText(Dev_CompanyName);
	                    	}
	                   }
	                    
	                    
	                    
	                    // //system.out.println(doc.asXML());
	                    
                    ////system.out.println("++++++++++++++++++++++++++++++++++++++++++");
	                    return(doc.asXML());
            	}
            	
               /**
                * Private method to write the response from the input stream to a file in local directory.
                * @param     strResponse  The string format of the response XML message
                **/
               private void fileWriter(String strResponse , String responseMessagePath)
                {
                     
                         //DateFormat today = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");

                         String path = responseMessagePath;
                         //String responseFileName = "Dhawal.xml";
                         String responseFileName;
                         
                         //如果是出错，文件名用OID
                         if(checkForRootElement(strResponse).indexOf("ShipmentValidateResponse")>=0)
                         {
                        	 responseFileName = new String(checkForRootElement(strResponse)+"_"+airWayBillNumberIn+".xml");
                         }
                         else
                         {
                        	 responseFileName = new String(checkForRootElement(strResponse)+"_"+oid+".xml");
                        	 setErrorPage(responseFileName);
                         }
                         
                         
                         String ufn = new String(path + responseFileName);

                        try
                        {
                                OutputStream output = new FileOutputStream(ufn);
                                PrintStream p = null; // declare a print stream object 
                                // Connect print stream to the output stream
                                p = new PrintStream( output );
                                p.println (strResponse);
                                p.close();
                                ////system.out.println("Response received and saved successfully at :" + path +"\n");
                                ////system.out.println("The file name is :" + responseFileName);
                        }
                        catch(Exception e)
                        {
                                System.err.println(e.getMessage());
                        }
                }// end of  fileWriter method
                
                /**
                * Returns the value of the root element of the response XML message send by DHL Server
                * @param     strResponse  The string format of the response XML message
                * @return      name of the root element of type string
                **/
                private static String checkForRootElement(String strResponse)
                {
                    Element element = null;
                    try
                    {
                        byte [] byteArray = strResponse.getBytes();
                        ByteArrayInputStream baip = new ByteArrayInputStream( byteArray);
                        DocumentBuilderFactory factory       = DocumentBuilderFactory.newInstance();
                        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
                        Document doc = documentBuilder.parse(baip); //Parsing the inputstream
                        element = doc.getDocumentElement(); //getting the root element           
                                                
                    }
                    catch(Exception e)
                    {
                        log.error("Exception in checkForRootElement "+e.getMessage());
                    }
                        String rootElement = element.getTagName();
                        //Check if root element has res: as prefix
                        
                        if(rootElement.startsWith("res:")||rootElement.startsWith("req:"))
                        {
                        
                            int index = rootElement.indexOf(":");
                        
                            rootElement = rootElement.substring(index+1);
                        }
                    return rootElement; // returning the value of the root element
                } //end of checkForRootElement method

		 /*
		       This constructor is used to do the following important operations
		        1) Read a request XML
		        2) Connect to Server
		        3) Send the request XML
		        4) Receive response XML message
		        5) Calls a private method to write the response XML message
		       
		        @param requestMessagePath  The path of the request XML message to be send to server
		        @param httpURL The http URL to connect ot the server (e.g. http://<ip address>:<port>/application name/Servlet name)
		        @param responseMessagePath The path where the response XML message is to be stored
		*/
		public DHLClient() throws Exception
		{     

        }
		
		
		public String commit() throws Exception
		{
            try
            {
            	String airWayBillNumber = "";
            	
    	 				/**  旧代码，我们使用新方式替换
                        //Preparing file inputstream from a file        
                        FileInputStream fis = new FileInputStream(requestMessagePath);

                         //Getting size of the stream
                         int fisSize = fis.available();
                         byte[] buffer = new byte[fisSize];
                         
                          //Reading file into buffer                                                                      
                         fis.read(buffer);

                        String clientRequestXML = new String(buffer);
                        
                        
                        **/
    	 
    	 				String clientRequestXML = getXMLString(requestMessagePath);
    	 				
    	 				////system.out.println(clientRequestXML);
                        /* Preparing the URL and opening connection to the server*/
                        URL servletURL = null;
                        servletURL = new URL(httpURL);

                        URLConnection servletConnection = null;
                        servletConnection = servletURL.openConnection();
                        servletConnection.setDoOutput(true);  // to allow us to write to the URL
                        servletConnection.setDoInput(true);
                        servletConnection.setUseCaches(false); 
                        
                        /*Code for sending data to the server*/
                        DataOutputStream dataOutputStream;
                        dataOutputStream = new DataOutputStream(servletConnection.getOutputStream());
                        
                        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();

                         byte[] dataStream = clientRequestXML.getBytes();
                         
                         
                        dataOutputStream.write(dataStream);  //Writing data to the servlet
                        dataOutputStream.flush();
                        dataOutputStream.close();

                         /*Code for getting and processing response from DHL's servlet*/
                        InputStream inputStream = null;
                        inputStream = servletConnection.getInputStream();
                        StringBuffer response = new StringBuffer();
                        int printResponse;
                        
                        
                        
                        //Reading the response into StringBuffer
                        while ((printResponse=inputStream.read())!=-1) 
                        {
                            response.append((char) printResponse);
                        }
                        inputStream.close();
                        
                        airWayBillNumber = getAirWayBillNumber(response.toString());
                        airWayBillNumberIn = airWayBillNumber;  //供内部使用
                       
                        this.initDestination(response.toString());
                         //Calling filewriter to write the response to a file
                        
                        ////system.out.println(response.toString());
                        
                        fileWriter(response.toString() , responseMessagePath);
                        
                        return(airWayBillNumber);
            }
            catch(SocketException e)
            {
            	throw e;
            }
            catch(MalformedURLException mfURLex)
            {
            	 log.error("MalformedURLException "+mfURLex);
            	 throw new MalformedURLException("MalformedURLException "+mfURLex);
            }
            catch(IOException e)
            {
            	log.error("IOException "+e);
            	throw new IOException("IOException "+e);
            }
		}
		
		private void initDestination(String response)
		{
			String des = new String(response);
			if (response.indexOf("<DestinationServiceArea>")>=0&&response.indexOf("<DestinationServiceArea>")>=0)
			{
				des = des.split("<DestinationServiceArea>")[1]; 
				des = des.split("</DestinationServiceArea>")[0]; 
				des = des.split("<ServiceAreaCode>")[1]; 
				des = des.split("</ServiceAreaCode>")[0]; 
				
				this.destination = des;
			}
			else
			{
				this.destination = null;
			}
			
		}
		
		private String getAirWayBillNumber(String xmlData)
		{
			String airWayBillNumber = "0000000000";
			
			String t1[] = xmlData.split("<AirwayBillNumber>");
			
			if (t1.length>1)
			{
				return(t1[1].split("</AirwayBillNumber>")[0]);
			}
			
			return(airWayBillNumber);
		}
		
		private String getXMLTracking(String trackingNumber) throws Exception
    	{
           org.dom4j.Document doc = getXMLDoc(requestTrackingPath);
//           org.dom4j.Document doc = getXMLDoc("D:\\track.xml");
           org.dom4j.Element element1 = (org.dom4j.Element)doc.selectSingleNode("//ServiceHeader");
           Iterator iter1 = element1.elementIterator();
           	    while(iter1.hasNext())
                {
                	org.dom4j.Element subelement = (org.dom4j.Element)iter1.next();
                	
                	if (subelement.getName().equals("MessageTime"))
                	{
                		String cc =  DateUtil.NowStrDHL();
                		subelement.setText(cc);
                	}
                }
           	    
           org.dom4j.Element elementAWBNumber = (org.dom4j.Element)doc.selectSingleNode("//AWBNumber");
           elementAWBNumber.setText(trackingNumber);
           
           return(doc.asXML());
    	}
		
		private DBRow[] getShippment(String xml) throws Exception
    	{
           try 
           {
//        	   //system.out.println(xml);
        	   org.dom4j.Document doc = getXMLDocForStringXML(xml);
			   org.dom4j.Element element = (org.dom4j.Element)doc.selectSingleNode("//AWBInfo/ShipmentInfo");
			   
			   if(element == null)
			   {
				   return new DBRow[0];
			   }
			   
			   Iterator iter = element.elementIterator();
			   ArrayList<DBRow> list = new ArrayList<DBRow>();
			   	    while(iter.hasNext())
			        {
			        	org.dom4j.Element subelement = (org.dom4j.Element)iter.next();
			        	Iterator subIter = subelement.elementIterator();
			        	if(subelement.getName().equals("ShipmentEvent"))
			        	{
			        		DBRow shipment = new DBRow();
			        		String date = "";
			        		String time = "";
			        		
			        		while (subIter.hasNext())
			        		{
			        			org.dom4j.Element subShipmentEvent  = (org.dom4j.Element) subIter.next();
				        		Iterator iterShipmentEvent = subShipmentEvent.elementIterator();
				        		
				        		if(subShipmentEvent.getName().equals("Date"))
				        		{
				        			 date = subShipmentEvent.getText();
				        		}
				        			
				        			
				        		if(subShipmentEvent.getName().equals("Time"))
				        		{
				        			time = subShipmentEvent.getText(); 
				        		}
				        			
				        		if(!date.equals("")&&!time.equals(""))
				        		{
				        			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						        	Date dd = sdf.parse(date+" "+time);
						        		
						        	shipment.add("date",dd.getTime());
						        	shipment.add("dateTime",date+" "+time);
						        	
						        	date = "";
						        	time = "";
				        		}
				        			
				        		if(subShipmentEvent.getName().equals("ServiceEvent"))
				        		{
				        			String status_code = subShipmentEvent.selectSingleNode("EventCode").getText();
					        		String description = subShipmentEvent.selectSingleNode("Description").getText();
					        		shipment.add("status_code",status_code);
					        		shipment.add("description",description);
				        		}
							}
			        		
			        		list.add(shipment);
			        	}
			        }
			   	 
			   	 DBRow[] rows = list.toArray(new DBRow[0]);   
			   	 
			   	 for (int j = 0; j < rows.length; j++) 
			   	 {
					for (int j2 = j+1; j2 <rows.length; j2++)
					{
						if(rows[j].get("date",0l)<rows[j2].get("date",0l))
						{
							DBRow temp = rows[j];
							
							rows[j] = rows[j2];
							
							rows[j2] = temp;
						}
					}
			   	 }
			   	 
			   	 
			   	 
			   	 ArrayList<DBRow> returnList = new ArrayList<DBRow>();
			   	 int count = 0;
			   	 while (count<rows.length&&count<3)
			   	 {
					returnList.add(rows[count]);
					count++;
			   	 }
			   	 
			   	 if(returnList.size()==0)
			   	 {
			   		 returnList.add(new DBRow());
			   	 }
			   	 
			   	 return returnList.toArray(new DBRow[0]);
		} 
           catch (Exception e) 
           {
        	   e.printStackTrace();
        	   throw e;
           }
    	}
		
		public Map commitTracking(String trackingNumber) throws Exception
		{
            try
            {
    	 			String clientRequestXML = getXMLTracking(trackingNumber);

                     /* Preparing the URL and opening connection to the server*/
                    URL servletURL = null;
                    servletURL = new URL(httpURL);

                    URLConnection servletConnection = null;
                    servletConnection = servletURL.openConnection();
                    servletConnection.setDoOutput(true);  // to allow us to write to the URL
                    servletConnection.setDoInput(true);
                    servletConnection.setUseCaches(false); 
                        
                        /*Code for sending data to the server*/
                        DataOutputStream dataOutputStream;
                        dataOutputStream = new DataOutputStream(servletConnection.getOutputStream());
                        
                        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();

                        byte[] dataStream = clientRequestXML.getBytes();
                         
                         
                        dataOutputStream.write(dataStream);  //Writing data to the servlet
                        dataOutputStream.flush();
                        dataOutputStream.close();

                         /*Code for getting and processing response from DHL's servlet*/
                        InputStream inputStream = null;
                        inputStream = servletConnection.getInputStream();
                        StringBuffer response = new StringBuffer();
                        int printResponse;
                        
                        
                        
                        
                        //Reading the response into StringBuffer
                        while ((printResponse=inputStream.read())!=-1) 
                        {
                            response.append((char) printResponse);
                        }
                        inputStream.close();
                        
                       
                        this.initDestination(response.toString());
                         //Calling filewriter to write the response to a file
                        
                        String responseXML = response.toString();
                        
                        DBRow[] shippments= getShippment(responseXML);
                        
//                        //system.out.println(responseXML);
                        
                        String weight = "0";
                        
                        org.dom4j.Element weightElement = (org.dom4j.Element) getXMLDocForStringXML(responseXML).selectSingleNode("//AWBInfo/ShipmentInfo/Weight");
                        
                        if(weightElement!=null)
                        {
                        	weight = weightElement.getText();
                        }
                        
                         //Float.valueOf(this.getSampleNode(responseXML,"Weight"));
//                        String trackingCode = this.getSampleNode(responseXML,"EventCode");
//                        String serviceEvent = this.getSampleNode(responseXML, "ServiceEvent");
//                        String definition = this.getSampleNode(serviceEvent,"Description");
                        
                        Map result = new HashMap();

                        result.put("weight",weight);
                        result.put("shippments",shippments);
                        
                        return result;
            }
            catch(SocketException e)
            {
            	throw new SocketException(e.toString());
            }
            catch(MalformedURLException mfURLex)
            {
            	 log.error("MalformedURLException "+mfURLex);
            	 throw new MalformedURLException("MalformedURLException "+mfURLex);
            }
            catch(IOException e)
            {
            	log.error("IOException "+e);
            	throw new IOException("IOException "+e);
            }
		}
		
        public static void main(String[] args) throws Exception
        {
        	try 
        	{
				DHLClient dhlClient = new DHLClient();
				Map c = dhlClient.commitTracking("8264079633");
				
				DBRow[] cc = (DBRow[]) c.get("shippments");
				
				for (int i = 0; i < cc.length; i++) 
				{
					////system.out.println(cc[i].getString("status_code"));
				}
			} 
        	catch (Exception e) 
        	{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }  

		public void setDutyPaymentType(String dutyPaymentType) {
			this.dutyPaymentType = dutyPaymentType;
		}
		
		public String getSampleNode(String xml,String name)
			throws Exception
		{
			try 
			{
				String xmlSplit1[] = xml.split("<"+name+">");
				String xmlSplit2[] = xmlSplit1[1].split("</"+name+">");
	
				return(xmlSplit2[0]);
			}
			catch (ArrayIndexOutOfBoundsException e)
			{
				throw new NetWorkException();
			}
			catch(Exception e)
			{
				throw new SystemException(e,"getSampleNode",log);
			}
	}

		public String getContents() {
			return Contents;
		}

		public void setContents(String contents) {
			Contents = contents;
		}
		
}// End of Class DHLClient
