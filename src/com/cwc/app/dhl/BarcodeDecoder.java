package com.cwc.app.dhl;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.apache.log4j.Logger;
import org.apache.soap.encoding.soapenc.Base64; 
import org.apache.xerces.parsers.DOMParser;





import com.cwc.app.util.Environment;

public class BarcodeDecoder 
{
	
	static String AWBNumber = new String() ;
	
//	String XMLFilePath = Environment.getHome()+"administrator/order/TransformXMLtoHTML/ResponseXMLS/";
	String processedXMLFilePath = Environment.getHome()+"administrator/order/TransformXMLtoHTML/ProcessedXMLS/";
//	String XSLFilePath = Environment.getHome()+"administrator/order/TransformXMLtoHTML/ShipmentValidationResponseMessage_nsAP.xsl";
//	String OutputFilePath = Environment.getHome()+"administrator/order/TransformXMLtoHTML/HTML/";

	String XMLFilePath = "D:\\DHL\\Waybill\\";
	String XSLFilePath = "D:\\Sync Workspace STS\\Sync10\\WebRoot\\administrator\\order\\TransformXMLtoHTML\\ShipmentValidationResponseMessage_nsAP.xsl";
	String OutputFilePath = "D:\\DHL\\Waybill\\HTML\\";
	
	static Logger log = Logger.getLogger("ACTION");

	public BarcodeDecoder(String airWayBillNumber) 
	{
	
		int processedFiles = 0 ;
		
		try {
			
			
			String fileName = "ShipmentValidateResponse_"+airWayBillNumber+".xml";
//			File xmlFile = new File(XMLFilePath);
//			String [] xmlFileList = xmlFile.list();
//			
//			if ( xmlFileList.length > 0 )
//			{
//				//system.out.println() ;
//				//system.out.println( "----------------------------------------------------------------------" ) ;
//				//system.out.println() ;
//			}
				
			//for(int iCount=0;iCount<xmlFileList.length;iCount++)
			//{
			//	fileName = xmlFileList[iCount];
				////system.out.println( "* Processing File : " + fileName ) ;
				
				if ( fileName.indexOf(fileName) == 0 )
				{
					
					String tmpXMLFilePath = XMLFilePath + fileName ;
					String prctmpXMLFilePath = processedXMLFilePath + fileName ;
					Document xmlDoc = getXMLDoc (tmpXMLFilePath );
					BarCodeDecoder(xmlDoc);
					String tmpOutputFilePath = OutputFilePath + AWBNumber + ".html" ;
		
					XSLTransformation  XSLT = new XSLTransformation ();
					XSLT.XSLTransformation (tmpXMLFilePath, XSLFilePath, tmpOutputFilePath); 
					File responsefile = new File( tmpXMLFilePath ) ;
					File processedfile = new File( prctmpXMLFilePath ) ;
					boolean flag = responsefile.renameTo( processedfile ) ;
					////system.out.println("      Response file " + fileName +" moved to " + processedXMLFilePath + " directory " ) ;
					tmpXMLFilePath = "" ;
					tmpOutputFilePath = "" ;
					prctmpXMLFilePath = "" ;
					
					processedFiles++ ;
				}
				else
				{
					////system.out.println("      File does not contain successful Shipment Validation Response." ) ;
				}
			//}

			if ( processedFiles > 0 )
			{
				////system.out.println() ;
				////system.out.println( "----------------------------------------------------------------------" ) ;
				////system.out.println( "Total Shipment Validation Responses processed : " + processedFiles ) ;
			}
			else
			{
				////system.out.println() ;
				////system.out.println( "----------------------------------------------------------------------" ) ;
				////system.out.println( "There were no successful Shipment Validation Responses in folder : \n" + XMLFilePath ) ;
			}
		} 
		catch (Exception e) 
		{
			log.error("An error occured while processing one or more response files:"+e) ;
		}
	
	}


//generates the barcode for AWB NUmber, Service area and the CustomerID
public void BarCodeDecoder (Document XMLResponseDoc)
{
	String DOCUMENT					= "D";
	String EXPRESS_DOCUMENT			= "X";
	String WPX						= "W";
	
	// "Origin-Destination-Product Code" Barcode Products
	String BARCODE_DOMESTIC			= "N";
	String BARCODE_EXPRESS_DOCUMENT = "X";
	String BARCODE_INTERNATIONAL_DOCUMENT = "D";
	String BARCODE_WPX				= "P";
	String barcodeProductCode		= null;
	String BaseFilePath = OutputFilePath;
	BaseFilePath = BaseFilePath + "BarCode/" ;
//	String BaseFilePath = "E:\\CA\\Appl\\XMLShipping\\ToolKit\\java\\AWBHTML\\BarCode\\";

	String suffix = ".png";
//	//system.out.println( "HERE") ;
	// get the AWB Number
	//Get the awb barcode image string
	String awbBarcodeImageData = nodevalue(XMLResponseDoc, "AWBBarCode");
	//get the AWB Number
//		//system.out.println( "HERE1") ;
	AWBNumber = nodevalue(XMLResponseDoc, "AirwayBillNumber");
	//Get the file path for saving the generated barcode
//		//system.out.println( "HERE2" + AWBNumber) ;
	String AWBFilePath = getFilePath(BaseFilePath, "AWB_", AWBNumber, suffix);
//ystem.out.println( "HERE4") ;
	// get the Service Area barcode
	//Get the image data
	String serviceBarcodeImageData = nodevalue(XMLResponseDoc, "OriginDestnBarcode");
	
	String dhlRoutingBarCodeImageData = nodevalue(XMLResponseDoc,"DHLRoutingBarCode");
	
	String LicensePlateBarCodeImageData = nodevalue(XMLResponseDoc,"LicensePlateBarCode");
////system.out.println( "HERE3") ;
	//Get the Destination area from the docuemnt
//	String destArea = nodevalue(XMLResponseDoc, "DestinationServiceArea");
	String destArea = getElementContent("ShipmentValidateResponse/DestinationServiceArea/ServiceAreaCode", XMLResponseDoc);
	//get the origin area from the docuemnt
//	//system.out.println( "HERE3.5" + destArea) ;
	String orgArea = getElementContent("ShipmentValidateResponse/OriginServiceArea/ServiceAreaCode", XMLResponseDoc);
//	String orgArea = nodevalue(XMLResponseDoc, "OriginServiceArea");
	//get the service area
//		//system.out.println( "HERE5" + orgArea) ;
    String consigneeCountryCode = getElementContent ("ShipmentValidateResponse/Consignee/CountryCode", XMLResponseDoc);
    String shipperCountryCode = getElementContent ("ShipmentValidateResponse/Shipper/CountryCode", XMLResponseDoc);

    String productCode = getElementContent ("ShipmentValidateResponse/GlobalProductCode", XMLResponseDoc);
	if ( productCode.equals(EXPRESS_DOCUMENT) )	{
		barcodeProductCode = BARCODE_EXPRESS_DOCUMENT;
	}
    else if ( productCode.equals(DOCUMENT) ) {
       	if (consigneeCountryCode.equals(shipperCountryCode) )	{
       		barcodeProductCode = BARCODE_DOMESTIC;
       	}
       	else{
       		barcodeProductCode = BARCODE_INTERNATIONAL_DOCUMENT;
       	}
    }
    else if ( productCode.equals(WPX) ) {
       	barcodeProductCode = BARCODE_WPX;
	}
	else
	{
		barcodeProductCode = productCode ;
	}

	StringBuffer serviceArea = new StringBuffer();
	serviceArea.append(orgArea).append(destArea).append(barcodeProductCode) ;
	//generates the file path
//		//system.out.println( "HERE6" + serviceArea) ;
	String ServiceFilePath = getFilePath(BaseFilePath, "ORGDEST_", serviceArea.toString(), suffix);
	
	String DHLRoutingFilePath = getFilePath(BaseFilePath, "ROUTING_",AWBNumber, suffix);
	
	String LicensePlateFilePath = getFilePath(BaseFilePath,"LicensePlate_",AWBNumber,suffix);
//	//system.out.println( "HERE7") ;
	//Client Barcode	
	try
	{
		//genertates the AWB barcode
		base64Decoding(awbBarcodeImageData, AWBFilePath);
		//genertates the Service area barcode
//			//system.out.println( "HERE9") ;
		base64Decoding(serviceBarcodeImageData, ServiceFilePath);
		
		base64Decoding(dhlRoutingBarCodeImageData,DHLRoutingFilePath);
		
		base64Decoding(LicensePlateBarCodeImageData, LicensePlateFilePath);
	}
	catch (IOException e)
	{
		////system.out.println ("      Error = " + e.getMessage());
	}

}

private String getFilePath (String BaseFilePath, String prefix, String barcodeValue, String suffix){
	StringBuffer filepath = new StringBuffer("");
	filepath.append(BaseFilePath).append(prefix).append(barcodeValue).append(suffix);
	return filepath.toString();
}

//Returns the value for teh specified node of teh XML document
private String nodevalue (Document XMLResponseDoc, String BarcodeNode)
{
	NodeList list = XMLResponseDoc.getElementsByTagName(BarcodeNode);

	StringBuffer imageData = new StringBuffer(list.item(0).getChildNodes().item(0).getNodeValue());
	return imageData.toString();

}

//method decodes the string and saves the file in the filepath
private void base64Decoding(String imageData, String FilePath) throws IOException
  {
		
    byte[] buffer=Base64.decode(imageData);	
    File file = new File(FilePath);
    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
    bos.write(buffer);
    bos.close();

  }

//Method Used for testing
public static void main(String[] args) {
}

//Method Used for testing
private Document getXMLDoc(String XMLFileName) throws Exception
{
	DOMParser _parser = new DOMParser();

	File file = new File(XMLFileName);
	try {
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		InputSource is = new InputSource(bis);
		_parser.parse(is);
	} catch (IOException e) {
		////system.out.println("In getXMLDoc got an exception " + e);
		throw e;
	} catch (SAXException e) {
		////system.out.println("In getXMLDoc got an exception " + e);
		throw e;
	}
	return _parser.getDocument();
}

    public String getElementContent(String elementType, Document doc)
   {

      Node node = searchDOMNode(elementType, doc.getDocumentElement(), (long)0);

      if (node == null) {
         return null;
      }

      Node childNode = node.getFirstChild();

      if (childNode == null) {
         return null;
      }

      return(childNode.getNodeValue());
   }


      private Node searchDOMNode(String name, Node root, long elementIndex)
   {
      long nodes_found = 0;

      if (root.getNodeName().equals(name)) {
         return root;
      }

      String newPath = name.substring(name.indexOf("/") + 1);

      if (newPath == null) {
         return null;
      }

      Node node = root;
      int index = newPath.indexOf("/");
      String substring;

      if (index == -1) {
         substring = newPath;
      }
      else {
         substring = newPath.substring(0, index);
      }

      NodeList nodeList = node.getChildNodes();

      for (int i=0;i<nodeList.getLength(); i++) {
         if (substring.equals(nodeList.item(i).getNodeName())) {
            root = searchDOMNode(newPath,nodeList.item(i), elementIndex);

            if ((root != null) && (nodes_found == elementIndex)) {
               return root;
            }
            else if (root != null) {
               nodes_found++;
            }
         }
      }

      return null;
   }

}