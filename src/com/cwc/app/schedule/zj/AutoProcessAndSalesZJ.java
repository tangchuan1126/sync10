package com.cwc.app.schedule.zj;




import org.apache.log4j.Logger;




import com.cwc.app.iface.tjh.OrderProcessMgrIFaceTJH;
import com.cwc.app.iface.tjh.ProductSalesMgrIFaceTJH;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.exception.SystemException;

public class AutoProcessAndSalesZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private OrderProcessMgrIFaceTJH orderProcessMgrTJH;
	private ProductSalesMgrIFaceTJH productSalesMgrTJH;
	
	

	public void run()
		throws Exception
	{
		try 
		{
			TDate date = new TDate();
			String o_st_date = orderProcessMgrTJH.orderProcessLastTime();
			if(o_st_date.equals(""))
			{
				o_st_date = date.formatDate("yyyy-MM-dd");
			}

			long orderProcess = orderProcessMgrTJH.statcProcuctNeedQuantityZJ(o_st_date,date.formatDate("yyyy-MM-dd"), 0);//统计发货数量
			
			String p_st_date = productSalesMgrTJH.productSalesLastTime();
			if(p_st_date.equals(""))
			{
				p_st_date = date.formatDate("yyyy-MM-dd");;
			}
			
			
			long salesCount = productSalesMgrTJH.statsProductSalesByDelivery(p_st_date,date.formatDate("yyyy-MM-dd"));//统计销售额

			log.info("orderProcess----start:'"+o_st_date+"',end:'"+date.formatDate("yyyy-MM-dd")+"',count:"+orderProcess+";ProductSales:start:'"+p_st_date+"',end:'"+date.formatDate("yyyy-MM-dd")+"',count:"+salesCount);
		}
		catch (Exception e) 
		{
			try
			{
			    throw new SystemException(e,"AutoProcessAndSalesZJ----"+DateUtil.NowStr(),log);
			}
			catch (Exception e2) 
			{
			    
			}
		}
		
		 
	}



	public void setOrderProcessMgrTJH(OrderProcessMgrIFaceTJH orderProcessMgrTJH) {
		this.orderProcessMgrTJH = orderProcessMgrTJH;
	}



	public void setProductSalesMgrTJH(ProductSalesMgrIFaceTJH productSalesMgrTJH) {
		this.productSalesMgrTJH = productSalesMgrTJH;
	}
}