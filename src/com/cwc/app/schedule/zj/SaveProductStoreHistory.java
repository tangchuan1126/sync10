package com.cwc.app.schedule.zj;

import org.apache.log4j.Logger;

import com.cwc.app.iface.zj.ProductStorageHistoryMgrIFaceZJ;
import com.cwc.app.util.DateUtil;
import com.cwc.exception.SystemException;

public class SaveProductStoreHistory {
	
	static Logger log = Logger.getLogger("ACTION");
	private ProductStorageHistoryMgrIFaceZJ productStorageHistoryMgrZJ;
	
	public void run()
		throws Exception
	{
		try 
		{
			productStorageHistoryMgrZJ.saveProductStorageHistroy();
		}
		catch (Exception e) 
		{
			try
			{
				throw new SystemException(e,"SaveProductStoreHistory-----"+DateUtil.NowStr(),log);
			}
			catch (Exception e2) 
			{
			    
			}
			
		}
	}

	public void setProductStorageHistoryMgrZJ(
			ProductStorageHistoryMgrIFaceZJ productStorageHistoryMgrZJ) {
		this.productStorageHistoryMgrZJ = productStorageHistoryMgrZJ;
	}
}
