package com.cwc.app.schedule.zj;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.asynchronized.ThreadManager;
import com.cwc.batch.WMSAppointment;
import com.cwc.db.DBRow;
public class DownLoadWMSAppointment {
	
	static Logger log = Logger.getLogger("ACTION");
	private YMSMgrAPI ymsMgrAPI;
//	private CatalogMgrIFace catalogMgr;
	
	/**
	 */
	public void scorecard()
	{
		try 
		{
//			//system.out.println("scorecard start");
			DBRow[] storages = ymsMgrAPI.getProductStorageCatalogForQuartz(StorageTypeKey.SELF);
			
			String now = DateUtil.NowStr();
			ThreadManager threadManager = ThreadManager.getInstance();
			
			for (int i = 0; i < storages.length; i++) 
			{
					long ps_id = storages[i].get("id",0l);
					Date location = DateUtil.locationZoneTime(now, ps_id);
					Calendar time = Calendar.getInstance();
					time.setTime(location);
					if (time.get(Calendar.HOUR_OF_DAY)==0)
					{
						time.add(Calendar.DAY_OF_YEAR,-1);
						String deal_date = DateUtil.FormatDatetime(DateUtil.defaultDateFormat,time.getTime());
						// 只更新前一天的约车时间数据
//						//system.out.println("deal_date:"+deal_date);
						WMSAppointment thread = new WMSAppointment(ps_id, ymsMgrAPI, deal_date, deal_date, true);
						
						threadManager.executeInBackground(thread);
					}
				}
//			//system.out.println("scorecard end");
		} 
		catch (Exception e) 
		{
			log.error("DownLoadWMSAppointment scorecard error:"+e);
		}
	}
	
	/**
	 * get appoinment_times from wmx, update old data
	 */
	public void forecast()
	{
		try 
		{
//			//system.out.println("forecast start");
			DBRow[] storages = ymsMgrAPI.getProductStorageCatalogForQuartz(StorageTypeKey.SELF);
			
			String now = DateUtil.NowStr();
			ThreadManager threadManager = ThreadManager.getInstance();
			
			for (int i = 0; i < storages.length; i++) 
			{
					long ps_id = storages[i].get("id",0l);
					Date location = DateUtil.locationZoneTime(now, ps_id);
					Calendar time = Calendar.getInstance();
					time.setTime(location);
					
					//如果现在仓库的local时间是17点，取后一天约车数据
					if (time.get(Calendar.HOUR_OF_DAY)==17)
					{
						time.add(Calendar.DAY_OF_YEAR, 1);
						// 从今天开始七天内的时间区间
						String start_date = DateUtil.FormatDatetime(DateUtil.defaultDateFormat,time.getTime());
						
						time.add(Calendar.WEEK_OF_YEAR, 1);
						String end_date = DateUtil.FormatDatetime(DateUtil.defaultDateFormat,time.getTime());
						
						// 更新未来7天内的约车时间记录
//						//system.out.println("start_date:"+start_date+" end_date:"+end_date);
						WMSAppointment thread = new WMSAppointment(ps_id, ymsMgrAPI, start_date, end_date, true);
						
						threadManager.executeInBackground(thread);
					}
			}
//			//system.out.println("forecast end");
		} 
		catch (Exception e) 
		{
			log.error("DownLoadWMSAppointment forecast error:"+e);
		}
	}
	
	public static void main(String[] args) throws Exception 
	{
		
	}

	public void setYmsMgrAPI(YMSMgrAPI ymsMgrAPI) {
		this.ymsMgrAPI = ymsMgrAPI;
	}

//	public void setCatalogMgr(CatalogMgrIFace catalogMgr) {
//		this.catalogMgr = catalogMgr;
//	}

}
