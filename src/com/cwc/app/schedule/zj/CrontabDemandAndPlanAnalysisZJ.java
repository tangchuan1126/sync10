package com.cwc.app.schedule.zj;



import org.apache.log4j.Logger;


import com.cwc.app.iface.tjh.OrderProcessMgrIFaceTJH;
import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.db.DBRow;


public class CrontabDemandAndPlanAnalysisZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private OrderProcessMgrIFaceTJH orderProcessMgrTJH;
	
	

	public void run()
	{
		try 
		{
			orderProcessMgrTJH.everyDayCrontabDemandAnalysisAndPlanAnalysis();
		} 
		catch (Exception e) 
		{
			log.error("PurchaseDetailDelayZJ error:"+e);
		}
	}



	public void setOrderProcessMgrTJH(OrderProcessMgrIFaceTJH orderProcessMgrTJH) {
		this.orderProcessMgrTJH = orderProcessMgrTJH;
	}
}