package com.cwc.app.schedule.zj;




import org.apache.log4j.Logger;




import com.cwc.app.iface.SystemConfigIFace;
import com.cwc.app.iface.tjh.OrderProcessMgrIFaceTJH;
import com.cwc.app.iface.tjh.ProductSalesMgrIFaceTJH;
import com.cwc.app.iface.zj.WaybillLogMgrIFaceZJ;
import com.cwc.app.jms.action.TrackingWaybillActionJMS;
import com.cwc.app.jms.action.UpdateTradeItemSkuFvfMpnJMS;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;

public class TrackingWaybillZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private WaybillLogMgrIFaceZJ waybillLogMgrZJ;
	private SystemConfigIFace systemConfig;
	
	

	public void run()
		throws Exception
	{
		try 
		{
			TDate tDate = new TDate();
			
			tDate.addDay(-systemConfig.getIntConfigValue("listorder_date_interval"));
			
			String st = tDate.getStringYear()+"-"+tDate.getStringMonth()+"-"+tDate.getStringDay();
			String en = DateUtil.getStrCurrYear()+"-"+DateUtil.getStrCurrMonth()+"-"+DateUtil.getStrCurrDay();
			
			
			DBRow[] waybillLogs = waybillLogMgrZJ.needTrackingWayBillLog(st, en);
			
			for (int  i= 0;  i< waybillLogs.length; i++) 
			{
				com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsTrackingMgr");
				jmsMgr.excuteAction(new TrackingWaybillActionJMS(waybillLogs[i].get("waybill_log_id",0l)));
			}
		}
		catch (Exception e) 
		{
			try
			{
			    throw new SystemException(e,"AutoProcessAndSalesZJ----"+DateUtil.NowStr(),log);
			}
			catch (Exception e2) 
			{
			    
			}
		}
		
		 
	}



	public void setWaybillLogMgrZJ(WaybillLogMgrIFaceZJ waybillLogMgrZJ) {
		this.waybillLogMgrZJ = waybillLogMgrZJ;
	}



	public void setSystemConfig(SystemConfigIFace systemConfig) {
		this.systemConfig = systemConfig;
	}
}