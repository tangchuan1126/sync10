package com.cwc.app.schedule.zj;

import org.apache.log4j.Logger;

import com.cwc.app.iface.zj.ProductMgrIFaceZJ;
import com.cwc.exception.SystemException;

public class DelImportProductZJ {
	static Logger log = Logger.getLogger("ACTION");
	private ProductMgrIFaceZJ productMgrZJ;
	
	public void run()
		throws Exception
	{
		try 
		{
			productMgrZJ.delAllImportDB();
		} 
		catch (Exception e) 
		{
			throw new SystemException(e,"",log);
		}
	}
	
	public void setProductMgrZJ(ProductMgrIFaceZJ productMgrZJ) {
		this.productMgrZJ = productMgrZJ;
	}
}
