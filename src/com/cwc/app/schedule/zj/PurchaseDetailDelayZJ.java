package com.cwc.app.schedule.zj;



import org.apache.log4j.Logger;


import com.cwc.app.iface.zj.PurchaseIFace;
import com.cwc.db.DBRow;


public class PurchaseDetailDelayZJ {
	
	static Logger log = Logger.getLogger("ACTION");
	private PurchaseIFace purchaseMgr;
	
	

	public void run()
	{
		try 
		{
			DBRow[] purchaseDetailDelay = purchaseMgr.getPurchaseDetailBydelay();
			if (purchaseDetailDelay!=null&&purchaseDetailDelay.length>0) 
			{
				for (int i = 0; i < purchaseDetailDelay.length; i++)
				{
					if(i==purchaseDetailDelay.length-1)
					{
						purchaseMgr.delayPurchaseForJbpm(purchaseDetailDelay[i].get("purchase_id",0l));
					}
					else if(i>0&&!purchaseDetailDelay[i].getString("purchase_id").equals(purchaseDetailDelay[i-1].getString("purchase_id")))
					{
						purchaseMgr.delayPurchaseForJbpm(purchaseDetailDelay[i-1].get("purchase_id",0l));
					}
				}
			}
		} 
		catch (Exception e) 
		{
			log.error("PurchaseDetailDelayZJ error:"+e);
		}
	}
	
	public void setPurchaseMgr(PurchaseIFace purchaseMgr) {
		this.purchaseMgr = purchaseMgr;
	}
}