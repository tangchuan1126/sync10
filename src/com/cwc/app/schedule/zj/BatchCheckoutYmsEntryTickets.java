package com.cwc.app.schedule.zj;

import java.util.List;
import com.cwc.app.api.GateCheckoutInfo;
import com.cwc.app.iface.zwb.CheckInMgrIfaceZwb;

public class BatchCheckoutYmsEntryTickets {
	private static boolean IS_RUNING = false;;
	private CheckInMgrIfaceZwb checkInMgrZwb;
	
	public void setCheckInMgrZwb(CheckInMgrIfaceZwb checkInMgrZwb) {
		this.checkInMgrZwb = checkInMgrZwb;
	}
	
	public void run() throws Exception {
		if (IS_RUNING) return;
		try {
			IS_RUNING = true;
			List<GateCheckoutInfo> list = checkInMgrZwb.getYmsCheckOutRequests("pending");
			if (list != null) {
				for (int i = 0; i < list.size(); i++) {
					try {
						checkInMgrZwb.ymsCheckOut(list.get(i));
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			IS_RUNING = false;
		}
	}
}
