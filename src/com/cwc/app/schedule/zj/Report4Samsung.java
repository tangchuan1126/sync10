package com.cwc.app.schedule.zj;

import com.cwc.app.iface.gql.PrintLabelMgrIfaceGql;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.email.Attach;
import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class Report4Samsung {
    private PrintLabelMgrIfaceGql printLabelMgr;
    private static final int MAX_TRY_TIMES = 5;
    private static String excelTempPath = Environment.getHome().replace("\\", File.separator) + "upl_pdf_tmp" + File.separator;
    private static final int COLUMN_LENGTH = 40;
    private static List<String> header = Arrays.asList(new String[]{"Reference#", "Container#", "Gate Check In Time", "Check Out Time"});

    public void setPrintLabelMgr(PrintLabelMgrIfaceGql printLabelMgr) {
        this.printLabelMgr = printLabelMgr;
    }

    public void run() {
        run(0);
    }

    public void run(int times) {
        String filePath = "";
        File file;
        try {
            System.out.println("=============Start Generate Samsung Report  ==============");
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
            SimpleDateFormat dfFileName = new SimpleDateFormat("yyyy-MM-dd HH");
            SimpleDateFormat dfSubject = new SimpleDateFormat("HH:00");
            Long ps_id = Long.valueOf(1000005L);

            GregorianCalendar gc = new GregorianCalendar(DateUtil.getTimeZone(ps_id.longValue()));
            Date date = new Date();
            String end_time = DateUtil.showLocalTime(df.format(date), ps_id.longValue());
            Date endTime = df.parse(end_time);
            int hour = endTime.getHours();
            int deltaHour = (hour == 0) ? -24 : -hour;
            gc.setTime(endTime);
            gc.add(10, deltaHour);
            String start_time = df.format(gc.getTime());

            String subject = "Samsung Report " + dfSubject.format(gc.getTime()) + "~" + dfSubject.format(endTime);
            MailBody mailBody = null;

            Date sysEndTime = df.parse(DateUtil.showUTCTime(end_time, ps_id.longValue()));
            Date sysStartTime = df.parse(DateUtil.showUTCTime(start_time, ps_id.longValue()));

            String queryStartTime = df.format(sysStartTime);
            String queryEndTime = df.format(sysEndTime);
            System.out.println("============= The Query Time  is :" + queryStartTime + "--" + queryEndTime + "==============");
            DBRow[] rows = this.printLabelMgr.getCheckInfo(queryStartTime, queryEndTime,"CJSOLU0003", ps_id);
            
            if ((rows != null) && (rows.length > 0)) {
                String fileName = dfFileName.format(gc.getTime()) + "--" + dfFileName.format(endTime) + ".xls";
                filePath = excelTempPath + File.separator + fileName;
				Path path = Paths.get(filePath, new String[0]);
				OutputStream outputStream = new FileOutputStream(path.toFile());
				WritableWorkbook writableWorkbook = Workbook.createWorkbook(outputStream);
				String sheetName = dfFileName.format(gc.getTime()) + "--" + dfFileName.format(endTime);
				WritableSheet excelSheet = writableWorkbook.createSheet(sheetName, 0);
				addColumnName(excelSheet, header, 0);
				setColumnLength(excelSheet, 4);
				setCellValue(excelSheet, rows, 1);
				writableWorkbook.write();
				writableWorkbook.close();
				outputStream.close();

                Attach attach = new Attach(filePath);
                mailBody = new MailBody(subject, subject, true, new Attach[]{attach});
                System.out.println("============= Record Count:" + rows.length + "==============");
            } else {
                mailBody = new MailBody(subject, "No Gate Activity", true, new Attach[0]);
            }

            String host = "mail.dospartner.com";
            int port = 587;
            String userName = "linc@logisticsteam.com";
            String password = "98rrd39";
            String from = userName;

            String receiver = "DavidJ@sea.samsung.com";
            String cc = "alan.yang@sbfreight.com";
            /*
            String receiver = "313381730@qq.com";
            String cc = "13917331474@sina.cn";
*/
            String author = "Versa";
            String reply = "linc@logisticsteam.com";

            MailAddress mailAddress = new MailAddress(receiver, cc);
            User user = new User(userName, password, host, port, from, author,
                reply);
            Mail mail = new Mail(user);

            mail.setAddress(mailAddress);
            mail.setMailBody(mailBody);
            mail.send(true);
            System.out.println("============= Generate Samsung Report  Success==============");
        } catch (Exception e) {
            System.out.println("============= Generate Samsung Report  Failed==============");
            e.printStackTrace();
            if (times < 5) {
                Long millis = Long.valueOf(10000L);
                try {
                    Thread.sleep(millis.longValue());
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                ++times;
                System.out.println("============= ReTry " + times + " Times==============");
                run(times);
            }
        } finally {
            if (!("".equals(filePath))) {
                file = new File(filePath);
                if (file.exists())
                    file.delete();
            }
        }
    }

    private int setCellValue(WritableSheet excelSheet, DBRow[] rows, int index)
        throws Exception {
        Long ps_id = Long.valueOf(1000005L);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int sheetRow = index;
        for (DBRow row : rows) {
            addCell(sheetRow, 0, row.getString("reference_no"), excelSheet,false, Alignment.LEFT);
            addCell(sheetRow, 1, row.getString("equipment_number"), excelSheet,false, Alignment.LEFT);
            String checkInTime = row.getString("gate_check_in_time");
            if ((checkInTime != null) && (!("".equals(checkInTime)))) {
                checkInTime = DateUtil.showLocalTime(df.format(df.parse(checkInTime + ":00")), ps_id.longValue());
            }

            addCell(sheetRow, 2, checkInTime, excelSheet, false, Alignment.CENTRE);
            String checkOutTime = row.getString("check_out_time");
            if ((checkOutTime != null) && (!("".equals(checkOutTime)))) {
                checkOutTime = DateUtil.showLocalTime(df.format(df.parse(checkOutTime + ":00")), ps_id.longValue());
            }

            addCell(sheetRow, 3, checkOutTime, excelSheet, false, Alignment.CENTRE);
            ++sheetRow;
        }
        return sheetRow;
    }

    private void setColumnLength(WritableSheet sheet, int columnQty) {
        for (int i = 0; i < columnQty; ++i)
            sheet.setColumnView(i, 40);
    }

    private void addColumnName(WritableSheet excelSheet, List<String> columnName, int row)
        throws WriteException {
        for (int i = 0; i < columnName.size(); ++i)
            addCell(row, i, (String) columnName.get(i), excelSheet, true,
                Alignment.CENTRE);
    }

    private int addCell(int row, int column, String content, WritableSheet sheet, boolean bold, Alignment alignment)
        throws WriteException {
        Label cell = new Label(column, row, content);
        setCellFormat(cell, bold, alignment);
        sheet.addCell(cell);
        return cell.getColumn();
    }

    private void setCellFormat(Label cell, boolean bold, Alignment alignment) throws WriteException {
        WritableCellFormat cellFormat = new WritableCellFormat();
        if (bold) {
            cellFormat = new WritableCellFormat(
                new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD));
        }

        cellFormat.setWrap(true);
        cellFormat.setAlignment(alignment);
        cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        cell.setCellFormat(cellFormat);
    }
}