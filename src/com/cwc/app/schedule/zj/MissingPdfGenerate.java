package com.cwc.app.schedule.zj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import net.sf.json.JSONArray;
import com.cwc.app.api.tc.PdfUtil;
import com.cwc.app.iface.gql.PrintLabelMgrIfaceGql;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;

public class MissingPdfGenerate {
	private PrintLabelMgrIfaceGql printLabelMgr;
	private static boolean IS_RUNING = false;;
	private static final long TIME_ZONE_ID = 1000005L;
	private static final String PDF_GENERATE_URL = "http://127.0.0.1:8000/Sync10/action/administrator/file_up/BatchGeneratePdfAction.action";
	private static final String LOGIN_URL = "http://127.0.0.1:8000/Sync10/action/admin/AccountLoginMgrAction.action";

	public void setPrintLabelMgr(PrintLabelMgrIfaceGql printLabelMgr) {
		this.printLabelMgr = printLabelMgr;
	}

	public void run() throws Exception {
		if (IS_RUNING){
			return;
		}
			
		try {
			IS_RUNING = true;
			
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("account", "wanglan");
			params.put("pwd", "damawl200");
			String sessionId = PdfUtil.login(LOGIN_URL, "wanglan", "damawl200");

			List<HashMap<String, String>> list = new ArrayList<>();
			Long ps_id = Long.valueOf(TIME_ZONE_ID);
			Date dtEnd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(DateUtil.NowStr());
			GregorianCalendar gc = new GregorianCalendar(
					DateUtil.getTimeZone(ps_id.longValue()));
			gc.setTime(dtEnd);
			gc.add(GregorianCalendar.MONTH, -1);

			
			 String start_time = new SimpleDateFormat("yyyy-MM-dd 00:00:00")
			  		.format(gc.getTime()); 
			 String end_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") .format(dtEnd);


			DBRow rows[] = printLabelMgr.getMissedPdfList(start_time, end_time);
			
			if (rows != null) {
				System.out.println("===========row count :"+rows.length+"=============");
				for (int i = 0; i < rows.length; i++) {
					DBRow row = rows[i];
					DBRow[] detailRows =(DBRow[])row.get("detail");
					for(DBRow dr:detailRows ){
					   HashMap<String, String> detail= new HashMap<>();
						
						detail.put("entry_id", dr.getString("entry_id"));
						detail.put("customer_id", dr.getString("customer_id"));
						detail.put("detail_id", dr.getString("detail_id"));
						detail.put("load_no", dr.getString("load_no"));
						list.add(detail);
						if (list.size() >= 1000 ) {
							break;
						}
					}
				}
			}

			HashMap<String, String> map = new HashMap<String, String>();
			map.put("data", "{\"list\":" + JSONArray.fromObject(list).toString() + "}");
			map.put("batch", "true");
			map.put("generateIncludeWithoutSign", "true");
			
			System.out.println("===========list size:"+list.size()+"=============");
			if(list.size()>0){
				PdfUtil.SentPostRequest(PDF_GENERATE_URL, map, sessionId);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			IS_RUNING = false;
		}

	}
}
