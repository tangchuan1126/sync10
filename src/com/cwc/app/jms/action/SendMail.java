package com.cwc.app.jms.action;


import org.apache.log4j.Logger;

import com.cwc.email.Mail;
import com.cwc.email.MailAddress;
import com.cwc.email.MailBody;
import com.cwc.email.User;
import com.cwc.exception.SystemException;
import com.cwc.jms.ActionObjMessage;

/**
 * 通过jms发送邮件
 * @author Administrator
 *
 */
public class SendMail extends ActionObjMessage
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7780158467765667441L;

	static Logger log = Logger.getLogger("ACTION");

	private User user;
	private MailAddress mailAddress;
	private MailBody mailBody;

	public SendMail(User user,MailAddress mailAddress,MailBody mailBody) 
	{
		this.user = user;
		this.mailAddress = mailAddress;
		this.mailBody = mailBody;
	}

	public void perform()
		throws Exception 
	{
		Mail mail = null;
		
		try
		{
			mail = new Mail(user);
            mail.setAddress(mailAddress);
            mail.setMailBody(mailBody);
            mail.send(false);
		}
		catch (Exception e)
		{
			if (e.getMessage().toLowerCase().indexOf("invalid addresses")>=0)//邮件地址错误，不重新发信
			{
				try 
				{
					throw new SystemException(e, "SendMail("+mailAddress.getTo()+")", log);
				}
				catch (Exception e2) 
				{
					// TODO: handle exception
				}				
			}
			else
			{
				throw e;
			}
		}
		finally
		{
			try
			{
				mail.close();	
			}
			catch (Exception e)
			{
			}
		}
		
	}

}
