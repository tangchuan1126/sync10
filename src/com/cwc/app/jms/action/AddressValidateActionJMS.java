package com.cwc.app.jms.action;

import com.cwc.jms.ActionObjMessage;
import com.cwc.spring.util.MvcUtil;

/**
 * 通过activeMQ验证fedex地址
 * @author Administrator
 *
 */
public class AddressValidateActionJMS extends ActionObjMessage
{
	private static final long serialVersionUID = 1L;
	private String toAddress;
	private String toZip;
	private long oid;

	public AddressValidateActionJMS(long oid,String toAddress,String toZip) 
	{
		this.toAddress=toAddress;
		this.toZip=toZip;
		this.oid=oid;
	}
	
	public void perform()
		throws Exception 
	{
		com.cwc.app.iface.qll.OrdersMgrIFaceQLL orderMgrQLL = (com.cwc.app.iface.qll.OrdersMgrIFaceQLL)MvcUtil.getBeanFromContainer("proxyOrdersMgrQLL"); 
     	orderMgrQLL.addressValidateFedex(oid,toAddress, toZip);
	}

}
