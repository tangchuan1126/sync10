package com.cwc.app.jms.action;

import java.util.Map;

import org.apache.log4j.Logger;

import com.cwc.app.api.zr.OrderMgrZr;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.jms.ActionObjMessage;
import com.cwc.spring.util.MvcUtil;

public class UpdateTradeItemSkuFvfMpnJMS extends ActionObjMessage{
	static Logger log = Logger.getLogger("JMS");
	
	private long oid ;
	
	public UpdateTradeItemSkuFvfMpnJMS(long oid) {
		this.oid = oid;
	}

	@Override
	public void perform() throws Exception {
		// 根据oid去查找FVK sku mpn然后更新Trade_item上的为oid的trade_item

		DBRow detailOrder = null;
		try
		{
			
			EbayMgrZrIFace ebayMgr = (EbayMgrZrIFace) MvcUtil.getBeanFromContainer("ebayMgrZr");
			com.cwc.app.iface.OrderMgrIFace orderMgr = (com.cwc.app.iface.OrderMgrIFace)MvcUtil.getBeanFromContainer("orderMgr");
			OrderMgrIfaceZR orderMgrZr = (OrderMgrIfaceZR) MvcUtil.getBeanFromContainer("orderMgrZr");
			detailOrder = orderMgr.getDetailPOrderByOid(oid);
			
			if(detailOrder!= null && detailOrder.getString("order_source").equals(OrderMgrZr.ORDER_SOURCE_EBAY)){
				//然后查询COI表把itemnumber 都查询出来
				
				DBRow[] rows = orderMgrZr.getTradeItemByOid(oid);
				if(rows != null && rows.length > 0){
					
					for(DBRow row : rows)
					{
						String itemNumber = row.getString("item_number");
						String ebay_txn_id = row.getString("ebay_txn_id");
						if((itemNumber!=null&&itemNumber.length()>0)&&(ebay_txn_id.length()>0))
						{
						 
							
							String sellerId = row.getString("seller_id");
						 
							String mpn =  row.getString("mpn");
							
							ebayMgr.updateTransactionsFeeSKU(sellerId, row.getString("item_number"), row.getString("ebay_txn_id") , row.get("trade_item_id",0l),mpn, oid);
							 
						}
					}
				}
			}
			
			 
			
		
			
		}
		catch (Exception e)
		{
			log.error("GetEbayOrderSellerIDJMS: "+detailOrder+" - "+oid);
			throw e;
		}
		
	
		
	}

}
