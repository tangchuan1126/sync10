package com.cwc.app.jms.action;

import java.net.ConnectException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cwc.jms.ActionObjMessage;
import com.cwc.spring.util.MvcUtil;
import com.cwc.app.api.zr.OrderMgrZr;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.ConnectErrorJmsException;
import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.call.GetItemCall;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;

/**
 * @author Administrator
 *
 */
public class GetEbayOrderSellerIDJMS extends ActionObjMessage
{
	private static final long serialVersionUID = 1L;
	static Logger log = Logger.getLogger("JMS");
	static Logger sellerlog = Logger.getLogger("ebay_seller");
	
	private long oid;

	public GetEbayOrderSellerIDJMS(long oid) 
	{
		this.oid = oid;
	}
	/*
	 *  1.修改成直接通过shoppingApi的方式去得到sellerId 
	 *  2.然后通过sellerId去登录。获取GetItemTransactions
	 *  3.得到Fee 添加到COI 上
	 * @see com.cwc.jms.ActionObjMessage#perform()
	 */
	public void perform()
		throws Exception  
	{
		DBRow detailOrder = null;
		
		try
		{
			sellerlog.info("oid:"+oid+" time:"+DateUtil.NowStr());
			
			EbayMgrZrIFace ebayMgr = (EbayMgrZrIFace) MvcUtil.getBeanFromContainer("ebayMgrZr");
			com.cwc.app.iface.OrderMgrIFace orderMgr = (com.cwc.app.iface.OrderMgrIFace)MvcUtil.getBeanFromContainer("orderMgr");
			OrderMgrIfaceZR orderMgrZr = (OrderMgrIfaceZR) MvcUtil.getBeanFromContainer("orderMgrZr");
			detailOrder = orderMgr.getDetailPOrderByOid(oid);
			
			sellerlog.info("oid:"+oid+"detailOrder:"+detailOrder+" time:"+DateUtil.NowStr());
			if(detailOrder!= null && detailOrder.getString("order_source").equals(OrderMgrZr.ORDER_SOURCE_EBAY))
			{
				//然后查询COI表把itemnumber 都查询出来
				sellerlog.info("oid:"+oid+" time:"+DateUtil.NowStr());
				DBRow[] rows = orderMgrZr.getTradeItemByOid(oid);
				sellerlog.info("oid:"+oid+" tradeItemLength:"+rows.length+" time:"+DateUtil.NowStr());
				if(rows != null && rows.length > 0)
				{
					for(DBRow row : rows)
					{
						String itemNumber = row.getString("item_number");
						String ebay_txn_id = row.getString("ebay_txn_id");
						sellerlog.info("oid:"+oid+" itemNumber:"+itemNumber+" txnid:"+ebay_txn_id+" time:"+DateUtil.NowStr());
						if((itemNumber!=null&&itemNumber.length()>0)&&(ebay_txn_id.length()>0))
						{
							Map<String,String> map =  ebayMgr.getSellerId(itemNumber,row.get("trade_item_id",0l));
							
							String sellerId = map.get("sellerId");
							
							if (sellerId==null||sellerId.equals("")) 
							{
								throw new ConnectException();
							}
							
							sellerlog.info("in_time:"+detailOrder.getString("post_date")+",oid:"+oid+",sellerId:"+sellerId+",getSellerTime:"+DateUtil.NowStr());
							
							orderMgr.modSellerId(oid, sellerId);

							DBRow temp = new DBRow();
							String mpn = map.get("MPN");
							temp.add("seller_id", sellerId);
							temp.add("mpn", mpn);
							ebayMgr.updateTradeItem(row.get("trade_item_id", 0), temp);
							try 
							{
								com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("jmsMgr");
								jmsMgr.excuteAction(new UpdateTradeItemSkuFvfMpnJMS(oid));
								//ebayMgr.updateTransactionsFeeSKU(sellerId, row.getString("item_number"), row.getString("ebay_txn_id") , row.get("trade_item_id",0l),mpn);
							} 
							catch (Exception e) 
							{
								
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error("GetEbayOrderSellerIDJMS: "+detailOrder+" - "+oid);
			throw e;
		}
		
	}

}
