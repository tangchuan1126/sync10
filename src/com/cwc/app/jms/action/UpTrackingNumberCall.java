package com.cwc.app.jms.action;

import com.cwc.app.beans.ebay.qll.CompleteSaleParaBeans;
import com.cwc.jms.ActionObjMessage;
import com.cwc.spring.util.MvcUtil;

/**
 * 通过jms发送邮件
 * @author Administrator
 *
 */
public class UpTrackingNumberCall extends ActionObjMessage
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CompleteSaleParaBeans bean;

	public UpTrackingNumberCall(CompleteSaleParaBeans bean) 
	{
		this.bean = bean;
	}

	public void perform()
		throws Exception 
	{
		com.cwc.app.iface.qll.EbayMgrIFace ebayMgr = (com.cwc.app.iface.qll.EbayMgrIFace)MvcUtil.getBeanFromContainer("proxyEbayMgrQLL"); 
		ebayMgr.UpTrackingNumberCall(bean);
	}

}
