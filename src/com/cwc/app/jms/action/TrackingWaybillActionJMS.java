package com.cwc.app.jms.action;

import com.cwc.jms.ActionObjMessage;
import com.cwc.spring.util.MvcUtil;

/**
 * 通过activeMQ验证fedex地址
 * @author Administrator
 *
 */
public class TrackingWaybillActionJMS extends ActionObjMessage
{
	private static final long serialVersionUID = 1L;
	private long waybill_log_id;

	public TrackingWaybillActionJMS(long waybill_log_id) 
	{
		this.waybill_log_id=waybill_log_id;
	}
	
	public void perform()
		throws Exception 
	{
		com.cwc.app.iface.zj.WaybillLogMgrIFaceZJ waybillLogMgrZJ = (com.cwc.app.iface.zj.WaybillLogMgrIFaceZJ)MvcUtil.getBeanFromContainer("proxyWaybillLogMgrZJ"); 
     	waybillLogMgrZJ.trackingWaybill(waybill_log_id);
	}

}
