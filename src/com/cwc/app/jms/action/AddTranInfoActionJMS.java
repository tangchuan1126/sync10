package com.cwc.app.jms.action;
import com.cwc.jms.ActionObjMessage;
import com.cwc.spring.util.MvcUtil;

/**
 * 通过jms添加交易信息
 * @author Administrator
 *
 */
public class AddTranInfoActionJMS extends ActionObjMessage
{
	private static final long serialVersionUID = 1L;
	private long oid;

	public AddTranInfoActionJMS(long oid) 
	{
		this.oid = oid;
	}

	public void perform()
		throws Exception 
	{
		com.cwc.app.iface.qll.TranInfoMgrIFaceQLL tranInfoMgr = (com.cwc.app.iface.qll.TranInfoMgrIFaceQLL)MvcUtil.getBeanFromContainer("proxyTranInfoMgrQLL"); 
		tranInfoMgr.addTranInfo(oid);
	}

}
