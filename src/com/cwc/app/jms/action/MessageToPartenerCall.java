package com.cwc.app.jms.action;

import com.cwc.app.beans.ebay.qll.MessageToPartnerParaBeans;
import com.cwc.jms.ActionObjMessage;
import com.cwc.spring.util.MvcUtil;

/**
 * 
 * @author Administrator
 *
 */
public class MessageToPartenerCall extends ActionObjMessage
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MessageToPartnerParaBeans bean;

	public MessageToPartenerCall(MessageToPartnerParaBeans bean) 
	{
		this.bean = bean;
	}

	public void perform()
		throws Exception 
	{
		
		com.cwc.app.iface.qll.EbayMgrIFace ebayMgr = (com.cwc.app.iface.qll.EbayMgrIFace)MvcUtil.getBeanFromContainer("proxyEbayMgrQLL"); 
		ebayMgr.MessageToPartenerCall(bean);
	}

}
