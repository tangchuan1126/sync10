package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.OperationNotPermitException;

public interface ProductReturnOrderMgrIFaceZJ 
{
	public long applicationWarranty(HttpServletRequest request)//申请质保
	throws Exception;
	
	public long markNormalWarrantying(HttpServletRequest request)//质保是否退货
	throws OperationNotPermitException,Exception;
	
	public void returnProductForFile(HttpServletRequest request)//使用文件标记已退货
	throws Exception;
	
	public DBRow[] getAllReturnReason(long rp_id)//获得质保原因
	throws Exception;
	
	public void hadCreateBill(long rp_id,long bill_id)//质保请求创建了账单
	throws Exception;
	
	public DBRow getDetailReturnProduct(long rp_id)//根据ID获得服务请求
	throws Exception;
	
	public DBRow[] filterReturnProduct(int status,int product_status,PageCtrl pc)//过滤服务请求
	throws Exception;
	
	public DBRow[] filterReturnProductReason(String st,String en,int warranty_type,long catalog_id,long product_line_id,String p_name,PageCtrl pc)//过滤客服反馈意见
	throws Exception;
	
	public DBRow[] searchReturnProductReasonByRpid(String rp_id,PageCtrl pc)
	throws Exception;
	
	public void warrantyInvoiceHasPay(long bill_id,long oid)//账单付款创建订单后管理服务
	throws Exception;
}
