package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface LPTypeMgrIFaceZJ {
	public DBRow[] getCLPTypeForSkuToShip(long pc_id,long title_id,long to_ps_id)
	throws Exception;
	
	public DBRow[] getBLPTypeForSkuToShipNotNull(long pc_id,long to_ps_id)
	throws Exception;
	
	public String getCLPName(long clp_type_id)
	throws Exception;
	
	public String getBLPName(long box_type_id)
	throws Exception;
	
	public String getBLPSelectForJqgrid(HttpServletRequest request)
	throws Exception;
	
	public String getCLPSelectForJqgrid(HttpServletRequest request)
	throws Exception;
	
	public DBRow calculateProductLPCount(long pc_id,int count,long clp_type_id,long blp_type_id)
	throws Exception;
	
	public DBRow getDetailCLPType(long clp_type_id)
	throws Exception;
	
	public String getLPTypeSelectForJqgrid()
	throws Exception;
	
	public DBRow[] getCLPNameSelectForJqgridByPname(long title_id,String p_name)
	throws Exception;
	
	public DBRow[] getBLPNameSelectForJqgridByPname(String p_name)
	throws Exception;
	
	public DBRow[] getILPNameSelectForJqgridByPname(String p_name)
	throws Exception;
	
	public DBRow[] getCLPTypeForSKUShipTo(long pc_id,long ship_to_id)
	throws Exception;

	public DBRow findClpTypesOrShiptoByPcId(long pcid, int length)
	throws Exception;
	
}
