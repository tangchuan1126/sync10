package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;


public interface PurchaseApproveMgrIFaceZJ 
{
	public void addPuchaseApprove(HttpServletRequest request)//申请采购单审核
	throws Exception;
	
	public DBRow[] allPurchaseApprove(long ps_id,PageCtrl pc,int approve_status,String sorttype)//获得全部采购单审核，支持按仓库查找
	throws Exception;
	
	public DBRow[] getPurchaseApproveDifferenceByPaid(long pa_id,PageCtrl pc)//根据采购单审核单ID获得采购单需审核差异详细
	throws Exception;
	
	public void approvePurchaseDifferent(long pa_id,long[] pdd_ids,String[] notes,AdminLoginBean adminLoggerBean)//采购单差异进行审核
	throws Exception;
	
	public long purcahseApproveForJbpm(long pa_id)//提供工作流拦截采购单申请审核
	throws Exception;
}
