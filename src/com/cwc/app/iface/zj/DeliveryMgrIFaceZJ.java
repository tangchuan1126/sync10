package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;







import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.util.StringUtil;

public interface DeliveryMgrIFaceZJ {
	
	public long addDeliveryOrder(HttpServletRequest request)//添加交货单
	throws Exception;
	
	public long addDeliveryOrderDetail(HttpServletRequest request)//添加交货单详细
	throws Exception;
	
	public DBRow[] getDeliveryOrders(long purchase_id)//根据采购单获得对应的交货单
	throws Exception;
	
	public DBRow[] getDeliveryOrderDetails(long delivery_order_id,long purchase_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)//根据交货单ID获得交货单明细
	throws Exception;
	
	public DBRow getDetailDeliveryOrder(long delivery_order_id)//根据交货单ID获得交货单详细
	throws Exception;
	
	public String downloadDeliveryOrder(HttpServletRequest request)//下载交货单
	throws Exception;
	
	public void saveDeliveryOrder(HttpServletRequest request)//保存交货单
	throws Exception;
	
	public String[] uploadDeliveryOrderDetail(HttpServletRequest request)//上传交货单明细
	throws Exception,FileTypeException,FileException;
	
	public DBRow[] excelShow(String filename)//excel文件转成DBRow显示
	throws Exception;
	
	public void saveDeliveryOrderDetails(HttpServletRequest request)//保存交货单明细
	throws Exception;
	
	public DBRow[] virtualDeliveryOrderDetailsByPurchaseId(long purchase_id)//根据采购单号虚拟生成交货单
	throws Exception;
	
	public DBRow[] getAllDeliveryOrder(PageCtrl pc)//获得全部交货单
	throws Exception;
	
	public DBRow[] filterDeliveryOrders(long supplier,int status,long ps_id,PageCtrl pc,int declaration, int clearance,int invoice, int drawback, int day, long productline_id,int stock_in_set, long delivery_create_account_id)//检索交货单
	throws Exception;
	
	public DBRow[] searchDeliveryOrders(String number,PageCtrl pc,long supplier_id)//检索交货单
	throws Exception;
	
	public void IntransitDelivery(HttpServletRequest request)//交货单路途中
	throws Exception;
	
	public DBRow[] getDeliveryOrdersByPsStatus(int status,long ps_id)//根据仓库，交货单状体过滤交货单
	throws Exception;
	
	public DBRow getDetailDeliveryOrderByDeliveryNumber(String delivery_order_number)//根据交货单号获得交货单详细
	throws Exception;
	
	public void deliveryWarehousingSub(long delivery_order_id,DBRow[] deliveryOrderDetails,AdminLoginBean adminLoggerBean,String machine_id)//交货单交货提交
	throws Exception;	
	
	public String deliveryOrderIntransitForJbpm(long delivery_order_id,long ps_id)//提供工作流拦截交货单已起运
	throws Exception;
	
	public void modDeliveryOrderDetail(HttpServletRequest request)//修改交货单详细
	throws Exception;
	
	public void delDelvieryOrderDetail(HttpServletRequest request)//删除交货单详细
	throws Exception;

	public DBRow getDeliveryOrderDetail(HttpServletRequest request)//根据交货单明细ID获得交货单明细
	throws Exception;
	
	public DBRow[] getNoSaveDeliveryOrderByPurchaseID(HttpServletRequest request)//获得采购单所有未保存的交货单
	throws Exception;
	
	public long createDeliveryOrder(HttpServletRequest request)//创建交货单（未保存状态）
	throws Exception;
	
	public DBRow[] getDeliveryOrderNoFinishBySupplier(long supplier_id)//获得供应商未完成的交货单
	throws Exception;
	
	public void delDeliveryOrder(HttpServletRequest request)//删除交货单
	throws Exception;
	
	public void delDeliveryOrderNoFinishByPurchaseFinish(long purchase_id)//采购单完成删除未完成的交货单
	throws Exception;
	
	public float getDeliveryOrderWeight(long delivery_order_id)
	throws Exception;
	
	public DBRow[] getDeliveryOrderDetailsById(long delivery_order_id)
	throws Exception;
	
	public void printDeliveryWayBill(String airWayBillNumber,long ccid,long pro_id,long sc_id,long id,String send_name,String send_zip_code,String send_address1,String send_address2,String send_address3,String send_linkman_phone,String send_city,String deliver_name,String deliver_zip_code,String deliver_address1,String deliver_address2,String deliver_address3,String deliver_linkman_phone,String deliver_city,float weight,String shipingName,String hs_code,int pkcount,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public void uploadDeliveryInvoice(HttpServletRequest request)
	throws Exception,FileTypeException,FileException;
}
