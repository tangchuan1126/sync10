package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface SerialNumberMgrIFaceZJ {
	
	public void addSerialNumber(HttpServletRequest request)//添加序列号
	throws Exception;
	
	public DBRow getSerialNumberDetail(long serial_number)
	throws Exception;
	
	public DBRow madeSerialNumber(HttpServletRequest request)//生成序列号（只生成，不存入数据库）
	throws Exception;
	
	public DBRow getDetailSerialProduct(String serial_number)
	throws Exception;
	
	public void initSerialProduct()
	throws Exception;
	
	public void putSerialProduct(String serial_number,long pc_id,long slc_id)
	throws Exception;
	
	public void addSerialProduct(long pc_id,String serial_number,long supplier_id)
	throws Exception;
	
	public void serialProductPositioning(String serial_number,long slc_id)
	throws Exception;
	
	public void outSerialProduct(String serial_number,long pc_id)
	throws Exception;
}
