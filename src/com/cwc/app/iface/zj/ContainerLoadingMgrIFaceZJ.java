package com.cwc.app.iface.zj;

import java.util.HashMap;

import com.cwc.db.DBRow;

public interface ContainerLoadingMgrIFaceZJ {

	public HashMap<String,DBRow[]> getContainerLoadingSonList(long parent_con_id)
	throws Exception;
	
	public HashMap<String,DBRow> getContainerLoadingParentList(long con_id)
	throws Exception;
}
