package com.cwc.app.iface.zj;

import java.util.ArrayList;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.db.DBRow;

public interface ProductStoreLocationMgrIFaceZJ 
{
	public void putProductToLocation(long ps_id,long pc_id,float quantity,long system_bill_id,int system_bill_type,long lay_up_adid,String location,String serial_number,long lp_id,long title_id,String lot_number,String machine,ProductStoreLogBean inProductStoreLogBean)//放商品
	throws Exception;
	
	public void reserveTheoretical(long ps_id,long pc_id,float quantity,long system_bill_id,int system_bill_type,long adid,String serial_number,String reserve_type)
	throws Exception;
	
	public void pickUpPhysical(long ps_id,long slc_id,long pc_id,float quantity,long pick_up_adid,long out_id,String serial_number,long lp_id,String machine,String foType,int from_container_type,long from_container_type_id,long from_con_id,int pick_contianer_type,long pick_container_type_id,long pick_con_id,long opation_con_id)
	throws Exception;
	
	public void initProductStoreLocation(long lay_up_adid)
	throws Exception;
	
	public long getAreaForBill(long system_bill_id,int system_bill_type)
	throws Exception;
	
	public void cancelOutListDetail(long system_bill_id,int system_bill_type)//单据取消，删除拣货单
	throws Exception;
	
	public DBRow[] getProductStorePhysicaLogLayUpForTransport(long transport_id)
	throws Exception;
	
	public ArrayList<DBRow> pickUpException(long out_id,long slc_id,long pc_id,long ps_id,float quantity,ProductStoreLogBean deInProductStoreLogBean)
	throws Exception;
	
	public void takeStockApprove(long ps_id,long slc_id,long pc_id,float quantity,long sld_id,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public DBRow[] getOutListDetails(long system_bill_id,int system_bill_type)
	throws Exception;
}
