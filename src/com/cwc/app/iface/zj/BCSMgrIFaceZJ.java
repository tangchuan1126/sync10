package com.cwc.app.iface.zj;

import java.util.HashMap;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;

public interface BCSMgrIFaceZJ 
{
	
	public int productLocation(String xml,AdminLoginBean adminLoggerBean,String machine)//条码机上传定位信息
	throws Exception;
	
	
	public String compareProductStorage(String xml,AdminLoginBean adminLoggerBean,String machine)//条码机比较库存
	throws Exception;
	
	public String category()//条码机下载商品分类
	throws Exception;
	
	public String ware()//条码机下载仓库信息
	throws Exception;
	
	public String getSampleNode(String xml,String name)//获得节点数据（只适合唯一名称节点）
	throws Exception;
	
	public String location(long ps_id)//获得使用者仓库位置
	throws Exception;
	
	public String product()//获得所有商品信息
	throws Exception;
	
	public int purchaseInbondScan(String xml,AdminLoginBean adminLoggerBean,String machine_id)//条码机采购单商品入库
	throws Exception;
	
	public int inbondScan(String xml,AdminLoginBean adminLoggerBean,String machine_id)//条码机采购单商品入库
	throws Exception;
	
	public HashMap waitingForDelivery(String xml,AdminLoginBean adminLoggerBean)//待发货
	throws Exception;

	public HashMap shipped(String xml,AdminLoginBean adminLoggerBean)//已发货
	throws Exception;
	
	public String deliveryDownload(String xml,AdminLoginBean adminLoggerBean)//下载登录账户所属仓库已起运交货单
	throws Exception;
	
	public String compareDelivery(String xml,AdminLoginBean adminLoggerBean)//交货比较
	throws Exception;
	
	public void deliverySuccess(String xml,AdminLoginBean adminLoggerBean,String machine_id)//确认交货
	throws Exception;
	
	public String compareTransport(String xml,AdminLoginBean adminLoggerBean)//比较转运单（包含上传部分）
	throws Exception;
	
	public String transportDownload(String xml,AdminLoginBean adminLoggerBean)//下载目的仓库是登录账户所属仓库的已发货的转运单
	throws Exception; 
	
	public String transportPacking(String xml,AdminLoginBean adminLoggerBean)//下载转运仓库是登录账户所属仓库的装箱中的转运单
	throws Exception; 
	
	public void transportSuccess(String xml,AdminLoginBean adminLoggerBean,String machine_id)//确认转运入库
	throws Exception;
	
	public String compareTransportPacking(String xml,AdminLoginBean adminLoggerBean)//比较转运装箱
	throws Exception;
	
	public void transportDelivery(String xml,AdminLoginBean adminLoggerBean,String machine_id)//转运单起运
	throws Exception;
	
	public String damagedRepairPacking(String xml,AdminLoginBean adminLoggerBean)//下载返修仓库是登录账户所属仓库的装箱中的返修单
	throws Exception;
	
	public String compareDamagedRepairPacking(String xml,AdminLoginBean adminLoggerBean)//返修单上传比较
	throws Exception;
	
	public void damagedRepairDelivery(String xml, AdminLoginBean adminLoggerBean,String machine_id)//返修单确认起运
	throws Exception;
	
	public String productCode()
	throws Exception;
	
	public byte[] downLoadOutStoreBillBasicData(String xml,String machine)
	throws Exception;
	
	public String xmlOutStoreBillOrderDetailByPsid(long ps_id)
	throws Exception;
	
	
	public String downLoadOutBillLocationPoroduct(String xml,long ps_id)//拣货单下载当前位置需要拣货商品
	throws Exception;
	
	public String pickUpOut(String xml,AdminLoginBean adminLoggerBean)//拣货单拣货
	throws Exception;
	
	public int printLicensePlate(String xml)//打托盘
	throws Exception;
	
	public byte[] downLoadContainerProductData(String xml,String machine)//下载托盘信息
	throws Exception;
	
	public int transportReceiveAndroid(String xml,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public int putProductToLocation(String xml,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public String transportReciveCompare(String xml)
	throws Exception;
	
	public String transportReceiveCompleteResult(String xml)
	throws Exception;
	
	public String getTransportWareHouse(String xml)
	throws Exception;
	
	public String transportReciveSerialNumberRepeat(String xml)
	throws Exception;
	
	public String transportReceivePutLocationCompare(String xml)
	throws Exception;
	
	public String alreadyPutTransport(String xml)
	throws Exception;
	
	public int transportOutboundAndroid(String xml,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public String transportOutboundCompare(String xml)
	throws Exception;
	
	public String transportOutboundCompleteResult(String xml)
	throws Exception;
	
	public void transportReceiveOver(String xml,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public void transportOutboundOver(String xml,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public void putOver(String xml,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public int productTakeStock(String xml,AdminLoginBean adminLoggerBean,String machine)
	throws Exception;
	
	public String getOperatorArea(long ps_id)
	throws Exception;
	
	public String pickUpException(String xml,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public int printTransportLicensePlate(String xml)//打托盘 .打托和收托盘的接口。主要是 在以前的基础上删除了。托盘原来的信息。加入新的托盘数据
	throws Exception;
}
