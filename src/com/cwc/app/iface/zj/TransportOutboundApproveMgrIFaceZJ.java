package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface TransportOutboundApproveMgrIFaceZJ {
	public DBRow[] fillterTransportOutboundApprove(long send_psid,long receive_psid, PageCtrl pc,int approve_status,String sorttype)//过滤转运单发货审核，支持按转运仓库，接收仓库查找
	throws Exception;
	
	public DBRow[] getTransportOutboundApproverDetailsByTsaid(long tsa_id,PageCtrl pc)//根据转运发货审核单ID获得需审核差异详细
	throws Exception;
	
	public void approveTransportOutboundDifferent(long tsa_id, long[] tsad_ids,String[] notes,long[] tsads_ids,String[] noteSNs,AdminLoginBean adminLoggerBean)//转运单差异进行审核
	throws Exception;
	
	public long approveTransportOutboundForJbpm(long tsa_id)//工作流拦截转运单发货审核
	throws Exception;
	
	public void addTransportOutboundApproveSub(long transport_id,AdminLoginBean adminLoggerBean)//转运单申请审核提交
	throws Exception;
	
	public void addTransportOutbound(HttpServletRequest request)//转运单发货审核（测试）
	throws Exception;
	
	public DBRow[] checkApproveStorage(long tsa_id,long transport_id)
	throws Exception;
	
	public DBRow[] getTransportOutboundApproveDetailSNsByTsaid(long tsa_id,PageCtrl pc)
	throws Exception;
	
}
