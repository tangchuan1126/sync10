package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface TransportMgrIFaceZJ {
	public long addTransport(HttpServletRequest request)// 添加转运单
			throws Exception;
	
	public void modTransport(HttpServletRequest request)// 修改转运单
			throws Exception;
	
	public void delTransport(HttpServletRequest request)// 删除转运单
			throws Exception;
	
	public void addTransportDetail(HttpServletRequest request)// 添加转运单详细
			throws Exception;
	
	public void modTransportDetail(HttpServletRequest request)// 修改转运单详细
			throws Exception;
	
	public void delTransportDetail(HttpServletRequest request)// 删除转运单详细
			throws Exception;
	
	public DBRow[] fillterTransport(long send_psid, long receive_psid, PageCtrl pc, int status, int declaration,
			int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id)// 过滤转运单
			throws Exception;
	
	public DBRow getDetailTransportById(long transport_id)// 根据转运单ID获得转运单详细
			throws Exception;
	
	public DBRow[] getTransportDetailByTransportId(long transport_id, PageCtrl pc, String sidx, String sord,
			FilterBean fillterBean)// 根据转运单ID获得转运单明细
			throws Exception;
	
	public DBRow[] searchTransportByNumber(String key, int search_mode, PageCtrl pc)// 根据转运单号查询转运单
			throws Exception;
	
	public String[] uploadTransportDetail(HttpServletRequest request)// 上传转运单明细
			throws Exception, FileTypeException, FileException;
	
	public DBRow[] excelshow(String filename)// excel转换DBRow
			throws Exception;
	
	public void saveTransportDetail(HttpServletRequest request)// 保存转运单明细
			throws Exception;
	
	public DBRow getTransportDetailById(HttpServletRequest request)// 获得转运单明细
			throws Exception;
	
	public void packingTransport(HttpServletRequest request)// 转运单装箱
			throws Exception;
	
	public DBRow[] getTransportDetailByPsWithStatus(long send_psid, long receive_psid, int status)// 过滤转运单明细（为条码机下载提供）
			throws Exception;
	
	public void deliveryTransport(long transport_id, AdminLoginBean adminLoggerBean)// 转运单出库起运
			throws Exception;
	
	public String deliveryTransportForJbpm(long transport_id, long receive_id)// 工作流拦截转运单出库起运
			throws Exception;
	
	public void transportWarehousingSub(long transport_id, DBRow[] transportDetailsWarehouse,
			AdminLoginBean adminLoggerBean, String machine_id)// 转运入库
			throws Exception;
	
	public DBRow[] checkStorage(long transport_id)// 检查库存
			throws Exception;
	
	public String downloadTransportOrder(HttpServletRequest request) throws Exception;
	
	public void rebackTransport(HttpServletRequest request)// 转运单停止装箱，回退库存
			throws Exception;
	
	public void reStorageTransport(HttpServletRequest request)// 转运单中止运输
			throws Exception;
	
	public float getTransportDetailsSendWeight(long transport_id)// 获得转运单商品重量
			throws Exception;
	
	public void printTransportWayBill(String airWayBillNumber, long ccid, long pro_id, long sc_id, long id,
			String send_name, String send_zip_code, String send_address1, String send_address2, String send_address3,
			String send_linkman_phone, String send_city, String deliver_name, String deliver_zip_code,
			String deliver_address1, String deliver_address2, String deliver_address3, String deliver_linkman_phone,
			String deliver_city, float weight, String shipingName, String hs_code, int pkcount,
			AdminLoginBean adminLoggerBean) throws Exception;
	
	public void uploadTransportInvoice(HttpServletRequest request) throws Exception, FileTypeException, FileException;
	
	public void deliveryChangeToTransport() throws Exception;
	
	public void transportPsType() throws Exception;
	
	public void updateCreateIdAndPackingId() throws Exception;
	
	// 回退差异装箱
	public void rebackDiffTransport(long transport_id, AdminLoginBean adminLoggerBean) throws Exception;
	
	// 差异装箱
	public void packingDiffTransport(long transport_id, AdminLoginBean adminLoggerBean) throws Exception;
	
	public long addTransportForPurchase(HttpServletRequest request)// 添加采购交货转运单
			throws Exception;
	
	public float getTransitCountForPurchase(long purchase_id, long pc_id)// 获得采购单内商品的在途数量
			throws Exception;
	
	public float getTransportVolume(long transport_id)// 获得转运单的体积
			throws Exception;
	
	public DBRow[] getTransportBySupplierId(long supplier_id, PageCtrl pc)// 根据供应商ID获得转运单
			throws Exception;
	
	public DBRow[] supplierSearchTransport(String searchKey, long supplier_id, PageCtrl pc) throws Exception;
	
	public void editTransportIndex(long transport_id, String type)// 对转运单索引操作
			throws Exception;
	
	public void initTranpsortDetailVolume() throws Exception;
	
	public String getPurchaseTransportDetailPrefill(HttpServletRequest request)// 根据采购单号获得需交货商品
			throws Exception;
	
	public double getTransportSendPrice(long transport_id)// 获得转运单出库金额
			throws Exception;
	
	public float getTransportWeight(long transport_id)// 获得转运单重量
			throws Exception;
	
	public void updateTransportDetailVW(long transport_id) throws Exception;
	
	public long updateTransportBasic(HttpServletRequest request) throws Exception;
	
	public long updateTransportForPurchaseBasic(HttpServletRequest request) throws Exception;
	
	public void initSendPrice() throws Exception;
	
	public DBRow trackDeliveryAndTransportCountFirstMenu()// 转运交货跟进一级跟进
			throws Exception;
	
	public DBRow[] trackDeliveryCountGroupByProductLine()// 需跟进交货按产品线分组
			throws Exception;
	
	public DBRow[] trackSendTransportCountGroupByPs()// 需跟进发货仓库分组
			throws Exception;
	
	public DBRow[] trackReciveTransportCountGroupByPs()// 需跟进收货仓库分组
			throws Exception;
	
	public DBRow[] getNeedTrackReadyDelivery(long product_line_id, PageCtrl pc) throws Exception;
	
	public DBRow trackSendTransportPsid(long ps_id) throws Exception;
	
	public DBRow trackReciveTransportPsid(long ps_id) throws Exception;
	
	public DBRow[] getNeedTrackSendTransportByPsid(long ps_id, String cmd, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackReceiveTransportByPsid(long ps_id, String cmd, PageCtrl pc) throws Exception;
	
	public DBRow[] getDeliveryTrackByProductLine(long product_line_id) throws Exception;
	
	public DBRow[] getNeedTrackProductFileDelivery(long product_line_id, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackQualityInspectionDelivery(long product_line_id, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackTagDelivery(long product_line_id, PageCtrl pc) throws Exception;
	
	public DBRow[] trackOceanShippingCount() throws Exception;
	
	public DBRow[] trackOceanShippingTransport(String cmd, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackThirdTagDelivery(long product_line_id, PageCtrl pc) throws Exception;
	
	public String importTransportDetail(String fileName) throws Exception;
	
	public DBRow[] errorImportTransportDetail(String fileName) throws Exception;
	
	public void transportReceiveOver(long transport_id, AdminLoginBean adminLoggerBean) throws Exception;
	
	public void transportPutOver(long transport_id, AdminLoginBean adminLoggerBean) throws Exception;
	
	public DBRow[] sendReceiveDifferents(long transport_id) throws Exception;
	
	public DBRow[] planSendDifferents(long transport_id) throws Exception;
	
	public DBRow[] sendReceiveDifferentsSN(long transport_id) throws Exception;
	
	public DBRow[] planSendDifferentsSN(long transport_id) throws Exception;
	
	public String simulationOutIn(long transport_id, String file_name, int simulation_type) throws Exception;
	
	public DBRow[] simulationOutInError(String systemTimeFile, int simulation_type) throws Exception;
	
	public void transportSimulationOut(HttpServletRequest request) throws Exception;
	
	public void transportSimulationIn(HttpServletRequest request) throws Exception;
	
	public DBRow[] showReceiveAllocate(long transport_id) throws Exception;
	
	public long addB2BOrderTransport(HttpServletRequest request) throws Exception;
	
	public DBRow[] getDetailTransportsByB2BOid(long b2b_oid) throws Exception;
	
	public long[] getTransportIdsByB2BOid(long b2b_oid) throws Exception;
	
	/**
	 * 
	 * 根据B2BOrder创建转运单,json数据格式
	 * 
	 * @param b2b_oid
	 * @return <b>Date:</b>2015-1-9上午9:23:03<br>
	 * @author: cuicong
	 * @throws Exception
	 */
	public long addB2BOrderTransport(long b2b_oid, HttpServletRequest request) throws Exception;
}
