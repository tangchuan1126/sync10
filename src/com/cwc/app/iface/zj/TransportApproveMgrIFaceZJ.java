package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface TransportApproveMgrIFaceZJ {
	
	public DBRow[] fillterTransportApprove(long send_psid,long receive_psid, PageCtrl pc,int approve_status,String sorttype)//过滤转运单审核，支持按转运仓库，接收仓库查找
	throws Exception;
	
	public DBRow[] getTransportApproverDetailsByTaid(long ta_id,PageCtrl pc)//根据交货审核单ID获得采购单需审核差异详细
	throws Exception;
	
	public void approveTransportDifferent(long ta_id,long[] tad_ids,String[] notes,long[] tads_ids,String[] noteSNs,AdminLoginBean adminLoggerBean)//转运单差异进行审核
	throws Exception;
	
	public long approveTransportOrderForJbpm(long ta_id)//工作流拦截交货单审核
	throws Exception;
	
	public void addTransportApproveSub(long transport_id,AdminLoginBean adminLoggerBean)//转运单申请审核提交
	throws Exception;
	
	public void addTransportApprove(HttpServletRequest request)//转运单申请审核
	throws Exception;
	
	public void stockIn(long transport_id,AdminLoginBean adminLoggerBean,int bill_type,int product_storep_oeration)
	throws Exception;//转运单入库
	
	public DBRow[] getTransportApproveDetailsSNByTaid(long ta_id,PageCtrl pc)
	throws Exception;
}
