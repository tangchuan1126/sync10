package com.cwc.app.iface.zj;


import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;


public interface SQLServerMgrIFaceZJ {
	public DBRow[] findMasterBolsByLoadNo(String loadNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request) throws Exception;
	public DBRow[] getLoading(HttpServletRequest request) throws Exception; 
	public DBRow[] findB2BOrderItemPlatesWmsByMasterBolNo(long masterBOLNo, String companyId, long adid, HttpServletRequest request) throws Exception;
	public DBRow findLocationWmsByPlateNo(int plateNo, String companyId) throws Exception;
	public DBRow findOrderCaseQtyByItemIdQty(String ItemID, String CompanyID, double qty,  String CustomerID) throws Exception;
	public DBRow findReceiptByContainerNo(String containerNo, long adid,String[] companyID, HttpServletRequest request) throws Exception;
	public DBRow findReceiptByBolNo(String BolNo, long adid,String[] companyID, HttpServletRequest request) throws Exception;
	public DBRow findOrderInfoByLoadNo(String loadNo, long adid, HttpServletRequest request) throws Exception;
	public DBRow findPalletsByOrderNoCompanyId(String CompanyID, long OrderNo) throws Exception;
	public void updateLoadMasterOrderShipDateSealVehicleDockCarrier(String shipDate, String seals, String vehicle, String dockId, String carrierId, String order_no,String orderNo,int number_type, long adid, String companyID,String customerID, HttpServletRequest request)throws Exception;
	public DBRow[] findMasterBolByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	public DBRow findMasterBolSomeInfoByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findLoadNoOrdersItemInfoByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findLoadNoOrdersPONoInfoByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	public DBRow findOrderSomeInfoByOrderNo(long orderNo, String companyId, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findOrderItemsInfoByOrderNo(long orderNo, String companyId, long adid, HttpServletRequest request)throws Exception;
	public DBRow findOrderPONoInfoByOrderNo(long orderNo, String companyId, long adid, HttpServletRequest request)throws Exception;
	public DBRow findCarrierInfoByCarrierId(String carrierId, String companyId)throws Exception;
	public DBRow findCarrierInfoByCarrierName(String carrierName, String companyId) throws Exception;
	public DBRow[] findMasterBolLinesByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	public void addMasterBolAndOrderDatas(String startTime, String endTime, String companyId, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findReceiptsByBolNoOrContainerNo(String bolNo, String containerNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request) throws Exception;
	public DBRow[] findReceiptLinesPalteByBolNoOrCtnNO(String bolNo, String containerNo, long receiptsNo, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findOrdersNoAndCompanyByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findMasterBolsSomeInfoByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	public DBRow findItemSumCountWeightByMasterBolCompany(long mbol, String companyID, long adid, HttpServletRequest request)throws Exception;
	public void updateReceiptInYardDateByBolOrCtnrNo(String inYardDate, String bolNo, String ctnNo, String supplierID,String companyID, long adid, HttpServletRequest request) throws Exception;
	//CarrierName
	public DBRow[] findAllCarriersNameNoRepeat() throws Exception;
	public long addOrders(DBRow row)throws Exception;
	public long addOrderLines(DBRow row)throws Exception;
	public long findMaxOrderNoByCompanyId(String companyID) throws Exception;
	public DBRow[] findItemInfoByItemId(String ItemID, String CompanyID, String CustomerID) throws Exception;
	public DBRow[] findCarrierWarehouseInfo() throws Exception;
	public DBRow findShipToInfoByCustomerAndAccountAddress(String CustomerID, String AccountID, String address) throws Exception;
	public DBRow[] findMasterOrBolLabelFormatByAccountCompanyCustomer(String AccountID, String CompanyID, String CustomerID) throws Exception;
	public String findMasterBolNoOrderNoStrByLoadNo(String loadNo, long adid) throws Exception;
	public int findMasterBolNoCountByLoadNo(String loadNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findOrdersSomeInfoWmsByLoadNo(String loadNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request) throws Exception;
	public DBRow[] findOrdersItemsWmsByOrderNo(long OrderNo, String companyId) throws Exception;
	public DBRow[] findLoadCompanyIdCustomerIdByLoadNo(String loadNo, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findOrderCompanyIdCustomerIdByLoadNo(String loadNo, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findReceiptsrCompanyIdCustomerIdByBolNoOrContainerNo(String no, long adid, HttpServletRequest request) throws Exception;
	public DBRow findReceiptByContainerBolNo(String No, long adid,String[] companyID) throws Exception;
	public DBRow findReceiptsrCompanyIdCustomerIdTypeByBolNoOrContainerNo(String no, long adid, HttpServletRequest request) throws Exception;
	public DBRow[] findLoadNoAppointTimeBetweenTime(String startTime, String endTime, String[] companyId, long adid, HttpServletRequest request) throws Exception;
	public DBRow[] findReceiptBolCtnAppointTimeBetweenTime(String startTime, String endTime, String[] companyId, long adid, HttpServletRequest request) throws Exception;
	public DBRow[] findBillOfLadingTemplateByLoadCompanyCustomer(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception;
	public DBRow[] findLoadNoOrdersItemInfoGroupByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	public DBRow[] findOrderItemsInfoForCountingByOrderNo(long orderNo, String companyId, long adid, HttpServletRequest request)throws Exception;
	/**
	 * Generic 通过MasterBol查询Items,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersItemInfoGroupByMasterBolNo(long MasterBOLNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	/**
	 * Generic 通过MasterBol查询Ponos,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersPONoInfoByMasterBolNo(long MasterBOLNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	
	
	public DBRow[] findMasterBolsSomeInfoNotOneByLoadNo(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	/**
	 * 通过load查询bill of lading模版,有Master
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByLoad(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception;
	/**
	 * 通过MasterBolNo字符串查询一条MasterBol地址等
	 * @param masterBols
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsSomeInfoOneByMasterBolStr(String loadNo,String masterBols, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	/**
	 * Generic 通过MasterBol字符串查询Items,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersItemInfoGroupByMasterBolNoStr(String loadNo, String MasterBOLNoStr, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	/**
	 * Generic 通过MasterBol查询Ponos,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersPONoInfoByMasterBolNoStr(String loadNo,String MasterBOLNos, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	/**
	 * 通过MasterBol数组查询Orders
	 * @param loadNo
	 * @param masterBolArr
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolLinesByMasterBolStr(String loadNo, String masterBolStr, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	/**
	 * 通过Order数组查询Orders
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersNoAndCompanyByLoadNoOrderStr(String loadNo, String orderStr, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	/**
	 * 通过Nos和类型类型，查询SupplierID
	 * @param no
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSupplierIDByLoadNosBolNosCtnrs(String no, String type, long adid, HttpServletRequest request)throws Exception;
	/**
	 * shipping label
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * ShipFromName,ShipFromAddress1,City,ShipFromAddress2,
	 * ShipToName,ShipToAddress1,ShipToAddress2,
	 * SCACCode,OrderNo,ProNo,ReferenceNo,PONo,BOLNote,OrderedQty,Pallets
	 * @throws Exception
	 */
	public DBRow[] findOrderShipPalletByLoadNo(String loadNo,long masterBolNo, String customerId, String companyId, long adid, HttpServletRequest request) throws Exception;
	/**通过loadno查询order信息
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return DBRow
	 * load_no, total_pallets, order_info(arr)
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletByLoadNo(String loadNo, String customerId, String companyId, long adid, HttpServletRequest request) throws Exception;
	/**
	 * shipping label
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * ShipFromName,ShipFromAddress1,City,ShipFromAddress2,
	 * ShipToName,ShipToAddress1,ShipToAddress2,
	 * SCACCode,OrderNo,ProNo,ReferenceNo,PONo,BOLNote,OrderedQty,Pallets
	 * @throws Exception
	 */
	public DBRow[] findOrderShipPalletByOrderNo(long orderNo, String customerId, String companyId, long adid, HttpServletRequest request) throws Exception;
	
	/**
	 * 更新load及order状态为Closed
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @throws Exception
	 */
	public void updateLoadMasterOrderStatus(final String loadNo, final String companyId, final String customerId, long adid, HttpServletRequest request)throws Exception;
	
	public void updateOrderPalletsByOrderNo(long orderNo, String customerId, String companyId, int palletsOriginal, int palletsCurrent, long adid, HttpServletRequest request) throws Exception;
	
	public void updateOrderPalletsByLoadNo(String loadNo, String customerId, String companyId, DBRow[] orderPallets) throws Exception;
	/**
	 * 查询捡货之后的提货单
	 * @param loadNo
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadedMasterAndOrders(String loadNo, String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception;
	/**
	 * 通过仓库获取托盘类型
	 * @param CompanyID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPalletTypesByCompanyID(String CompanyID) throws Exception;
	/**
	 * 列出order信息，为做consolidate
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersInfoForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID, long adid, HttpServletRequest request)throws Exception;
	/**
	 * 查询orderPallet ， 装货
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletForLoading(int order_type, String order_no, String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception;
	/**
	 * 通过OrderNo获取MasterBol和load的列表
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolOrLoadByOrderNo(long orderNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception;
	/**
	 * 通过 MasterBolNo获取LoadNo和MasterBOLNo
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolOrLoadByMasterBolNo(long MasterBolNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception;
	/**
	 * 通过orderNo, itemNo, lineNo更改数量
	 * @param orderNo
	 * @param itemId
	 * @param changeQty
	 * @param lineNo
	 * @param companyId
	 * @param customerId
	 * @throws Exception
	 */
	public int updateOrderItemPalletQty(long orderNo, String itemId, int changeQty, int lineNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	

	/**
	 * 通过load查询bill of lading模版
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByOrderNo(long orderNo, String companyId, String customerId, long adid, HttpServletRequest request) throws Exception;
	
	
	/**
	 * 通过MasterBolno字符串，获取最早时间的order的bolNote
	 * @param masterBols
	 * @param companyID
	 * @param customerID
	 * @return
	 * @throws Exception
	 */
	public String findFirstTimeOrderBolNoteByMasterBols(String masterBols, String companyID, String customerID) throws Exception;
	
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param masterBOLNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersSomeInfoWmsByOrderNo(long OrderNo,String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception;

	public DBRow[] getCustomerServices(String startTime,String endTime,String companyId,String customerId) throws Exception;
	/**
	 * checkin回车验证
	 * @param orderNo
	 * @param companyId
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow checkOrderTypeAndChangeOrderTypeByNo(String orderNo, String[] companyId) throws Exception;
	/**
	 * 通过OrderNo，更新orderNo的ProNo
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @throws Exception
	 */
	public void updateOrderPronoByOrderNo(final long orderNo, final String companyId, final String customerId, final String status, final String prono,final String shipDate,final String seals,final String vehicle,final String dockName,final String carrierName,String userUpdated) throws Exception;
	/**
	 * 通过OrderNo，更新orderNo的ProNo
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @throws Exception
	 */
	public void updateMasterPronoByOrderNo(final long MasterBOLNo, final String companyId, final String customerId, final String status, final String prono,String shipDate, String seals, String vehicle, String dockName, String carrierName,String userUpdated) throws Exception;
	/**
	 * 查预约到达时间为某天的Load
	 * @param time
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderNoAppointTimeBetweenTime(String startTime, String endTime, String[] companyId, long adid, HttpServletRequest request) throws Exception;

	/**
	 * Parking list
	 * @param OrderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow findOrderSomeInfosByOrderNo(long OrderNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	/**
	 * Parking list items
	 * @param OrderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderItemsSomeInfoGroupByOrderNo(long OrderNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	
	/**
	 * Generic 通过MasterBol查询Ponos,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow findLoadNoOrdersPONoInfoByLoadMasterBolNoStr(String loadNo,String MasterBOLNos, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	 /**consolidate
	 * 列出order信息
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersInfoByOrderTypeForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID,String status_one, long adid, HttpServletRequest request)throws Exception;
	/**
	 * 通过账号得到查询单据需要的状态
	 * @param pickOrDeliver
	 * @param adid
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String[] getStatusExceptClosed(int pickOrDeliver, long adid, HttpServletRequest request) throws Exception;
	/**
	 * 查出所有AccountID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllAccountIDs() throws Exception;
	/**
	 * 查出所有SupplierID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllSupplierIDs() throws Exception;
	/**
	 * 查出所有CustomerIDs
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllCustomerIDs() throws Exception;
/**
	 * 通过 loadNo获取Orders
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderInfosByLoadNo(String loadNo, String companyId, String customerId, String[] status) throws Exception;
	/**
	 * @param pono
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findOrderInfoByPoNo(String pono, String companyId, String customerId, String status)throws Exception;
	
	
	/**
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow findOrderInfoByOrderNo(long orderNo, String companyId, String customerId, String status)throws Exception;
	/**
	 * 通过numberType、number等查询相应单据的shipto信息
	 * @param para
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月3日 下午4:29:37
	 */
	public DBRow[] findShipToInfoByNumberAndType(DBRow para) throws Exception;
	/**
	 * 根据单据类型、单据号，查询order信息
	 * @param number_type 单据类型
	 * @param number 单据号
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 上午8:48:33
	 */
	public DBRow[] findOrdersByNumberAndType(int number_type, String number, String companyId, String customerId) throws Exception;
	/**
	 * 通过OrderNo，companyid，查询SupplierIds，从plate和line上抓
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月24日 上午10:43:27
	 */
	public String findSupplierIdsByOrderNoCompanyID(long orderNo, String companyId) throws Exception;
	/**
	 * 通过OrderNo查询order信息
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月24日 下午2:10:07
	 */
	public DBRow[] findOrderByOrderNo(long orderNo, String companyId) throws Exception;
	/**
	 * 通过bolNo，ContainerNo查询
	 * @param bolNo
	 * @param containerNo
	 * @param CompanyID
	 * @param CustomerID
	 * @param SupplierID
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年2月13日 下午4:45:36
	 */
	public DBRow[] findReceiptsByBolNoOrContainerNo(String bolNo, String containerNo,String CompanyID,String CustomerID,String SupplierID) throws Exception;
	/**
	 * checkFnOrderSerialnoFinalStatus
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月18日 下午5:14:20
	 */
	public String checkFnOrderSerialnoFinalStatus(long orderNo, String companyId) throws Exception;
	/**
	 * Check Load SnScaned
	 * @param LoadNo
	 * @param masterBolArr
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月20日 上午11:22:18
	 */
	public DBRow CheckLoadFnOrderSerialnoFinalStatus(String LoadNo, Long[] masterBolArr,  String companyId, String customerId) throws Exception;
	/**
	 * Check Po SnScaned
	 * @param Po
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月20日 上午11:22:18
	 */
	public DBRow CheckPoFnOrderSerialnoFinalStatus(String PoNo,  String companyId, String customerId) throws Exception;
	/**
	 * checkOrderFnOrderSerialnoFinalStatus
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月18日 下午5:14:20
	 */
	public DBRow checkOrderFnOrderSerialnoFinalStatus(long orderNo, String companyId) throws Exception;

	
	public DBRow addOrderInfoByMasterBol(String loadNo, String companyId, String customerId, long adid,long masterBol, HttpServletRequest request) throws Exception;
	/**
	 * 通过loadCompany,ponoCompany,orderCompany查询DN
	 * @param loadCompany
	 * @param ponoCompany
	 * @param orderCompany
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月10日 下午3:46:09
	 */
	public DBRow[] findDNByLoadCompanyPoCompanyOrderCompany(String[] loadCompany, String[] ponoCompany, String[] orderCompany) throws Exception;
	
	public DBRow[] findDNByLoadCompanyPoCompanyOrderCompany(String[] loadCompany, String[] ponoCompany, String[] orderCompany,String refNo) throws Exception;
    /**
     * 通过 companys，customers， carrier，ship_time查询Order
     * @param companys
     * @param customers
     * @param carrier
     * @param ship_time
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2015年5月10日 下午4:45:35
     */
    public DBRow[] findOrderByCompanyCustomerCarrierDate(String[] companys, String[] customers, String[] carrier, String ship_time) throws Exception;
  //通过bolNo,containerNo、Receipt查询Receipts
  	public DBRow[] findReceiptsByReceiptBolNoOrContainerNo(long receiptNo, String bolNo, String containerNo,String CompanyID,String CustomerID, long adid, HttpServletRequest request) throws Exception;
  	public DBRow[] getMasterBOLNos( DBRow[] filterRows) throws Exception;
  	
  	public DBRow[] findPOTypeOfDN(String companyId) throws Exception;

}
