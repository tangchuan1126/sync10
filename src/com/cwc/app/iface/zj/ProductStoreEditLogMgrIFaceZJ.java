package com.cwc.app.iface.zj;

import com.cwc.app.beans.AdminLoginBean;

public interface ProductStoreEditLogMgrIFaceZJ {
	public void addProductStoreLogForPcidAndPsid(long pc_id,long ps_id,AdminLoginBean adminLoggerBean,int edit_reason)
	throws Exception;
	
	public void addProductStoreLogForPid(long pid,AdminLoginBean adminLoggerBean,int edit_reason)
	throws Exception;
}
