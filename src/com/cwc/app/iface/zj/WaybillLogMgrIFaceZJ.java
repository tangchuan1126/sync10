package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface WaybillLogMgrIFaceZJ {
	
	public DBRow[] filterWayBillLog(String trackingNumber,String st,String en,int waybill_internal_status,int waybill_external_status,long adid,PageCtrl pc)//过滤运单日志
	throws Exception;
	
	public DBRow[] needTrackingWayBillLog(String st,String en)//需要跟踪的运单日志
	throws Exception;
	
	public void trackingWaybill(long waybill_log_id)
	throws Exception;
	
	public void initWayBillLog(int waybill_status,String st,String en)
	throws Exception;
	
	public DBRow[] getWayBillLogByWaybillId(long waybill_id)
	throws Exception;
	
	public void updateArtificialWaybillTracking(HttpServletRequest request)
	throws Exception;
}
