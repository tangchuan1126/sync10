package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;


public interface LocationMgrIFaceZJ {
	public long addLocationArea(HttpServletRequest request)//增加仓库区域
	throws Exception;
	
	public long addLocationAreaSub(DBRow row)//增加仓库区域的提交
	throws Exception;
	
	public void modLocationArea(HttpServletRequest request)//修改仓库区域
	throws Exception;
	
	public void delLocationArea(HttpServletRequest request)//删除区域
	throws Exception;
	
	public DBRow[] getLocationAreaByPsid(HttpServletRequest request)//根据仓库ID获得所有区域
	throws Exception;
	
	public DBRow getDetailLocationAreaById(long area_id)//根据区域ID获得区域详细
	throws Exception;
	
	public DBRow[] getLocationAreaByPsid(long psid)//根据仓库ID获得所有区域,页面用
	throws Exception;
	
	public void modLocationAreaWithImg(HttpServletRequest request) //修改区域同时修改图片
	throws Exception;
	
	public DBRow[] getAllLocationCatalogByAreaId(long area_id,PageCtrl pc,String position,String type)//根据区域ID获得区域下所有位置
	throws Exception;
	
	public long addLocationCatalog(HttpServletRequest request)//添加位置
	throws Exception;
	
	public long addLocationCatalogSub(DBRow row)//添加位置提交，创建图数据库节点
	throws Exception;
	
	public DBRow getDetailLocationCatalogById(long slc_id)//获得位置详细,页面使用
	throws Exception;
	
	public DBRow getDetailLocationCatalogById(HttpServletRequest request)//获得位置详细
	throws Exception;
	
	public void delLocationCatalog(HttpServletRequest request)//删除位置
	throws Exception;
	
	public void modLocationCatalog(HttpServletRequest request)//修改位置
	throws Exception;
	
	public void delLocationCatalogs(HttpServletRequest request)//批量删除位置
	throws Exception;
	
	public DBRow[] getAreaDoorOrLocationByAreaId(long area_id,long type)
	throws Exception;//获取区域所对应的卸货门或装卸位置

	public DBRow[] getLocationAreaByPsidAndTitleId(long psid, long title_id) throws Exception;
}
