package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface StorageApproveMgrIFaceZJ {
	public int storageTakeStock(long area_id,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public DBRow[] getDifferentsForArea(long area_id)
	throws Exception;
	
	public DBRow[] filterStorageApproveArea(long ps_id,long area_id,int status,String sortby,boolean sort,PageCtrl pc)
	throws Exception;
	
	public DBRow getDetailStorageApproveArea(long saa_id)
	throws Exception;
	
	public DBRow[] getStorageApproveLocationBySaaId(long saa_id)
	throws Exception;
	
	public DBRow[] getStorageLocationDifferentsBySal(long sal_id)
	throws Exception;
	
	public void approveStorageApproveDifferents(HttpServletRequest request)
	throws Exception;
	
	public int storageTakeStockNew(long area_id,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public DBRow[] getStorageApproveContainers(long saa_id,long slc_id)
	throws Exception;
	
	public DBRow[] getLocationDifferents(long saa_id,long con_id)
	throws Exception;

	public DBRow[] storageContainerProductCountDifferentByArea(long area_id)
	throws Exception;
	
	public DBRow[] getContainerTreeToDTree(long ps_id,long con_id)
	throws Exception;
	
	public DBRow getDetailStorageLocationDifferentsContainer(long saa_id,long con_id)
	throws Exception;
	
}
