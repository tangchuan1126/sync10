package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface DoubtGoodsMgrIFaceZJ {
	
	public long addDoubtProduct(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] filterDoubtProduct(long product_line_id,int is_answerd,PageCtrl pc)
	throws Exception;
	
	public void answerDoubtProduct(HttpServletRequest request)
	throws Exception;
	
	public DBRow getDetailDoubtProduct(long dp_id)
	throws Exception;
	
	public void initDoubtGoodImg()
	throws Exception;
	
	public int waitAnswerDoubtGoodCount()
	throws Exception;
}
