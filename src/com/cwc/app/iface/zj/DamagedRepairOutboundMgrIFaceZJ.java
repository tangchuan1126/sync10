package com.cwc.app.iface.zj;

import com.cwc.db.DBRow;

public interface DamagedRepairOutboundMgrIFaceZJ {
	public void addDamagedRepairOutboundSub(DBRow[] dbs,long repair_id)//伪返修发货
	throws Exception;
	
	public void delDamagedRepairOutboundByDamagedRepairId(long repair_id)//删除返修发货
	throws Exception;
	
	public DBRow[] compareDamagedRepairOutboundByDamagedRepairId(long repair_id)//返修发货比较
	throws Exception;
	
	public DBRow[] getDamagedRepairOutboundByDamagedRepairId(long repair_id)//根据返修单ID获得返修单出库
	throws Exception;
}
