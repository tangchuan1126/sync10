package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductLogsMgrIFaceZJ {
	
	public DBRow[] searchProductLogs(String p_name,String start,String end,long adid,int edit_type,int edit_reason,PageCtrl pc)
	throws Exception;
	
	public void AllProductAddLogs()
	throws Exception;
}
