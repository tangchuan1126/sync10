package com.cwc.app.iface.zj;

import com.cwc.db.DBRow;

public interface TransportOutboundMgrIFaceZJ {
	public void addTransportOutboundSub(DBRow[] dbs,long transport_id,String machine_id)//伪转运提交
	throws Exception;
	
	public void delTransportOutboundByTransportId(long transport_id,String machine_id)//删除交货比较
	throws Exception;
	
	public DBRow[] compareTransportOutboundByTransportId(long transport_id)//伪交货比较
	throws Exception;
	
	public DBRow[] getTransportOutboundByTransportId(long transport_id)//根据交货单ID获得伪交货入库
	throws Exception;
	
	public DBRow[] compareTransportDetailOutByTransportId(long transport_id)
	throws Exception;
	
	public DBRow[] transportOutboundCompareCount(long transport_id)
	throws Exception;
	
	public DBRow[] getTransportOutboundBySNNotMachine(String sn,String machine_id,long transport_id)
	throws Exception;
}
