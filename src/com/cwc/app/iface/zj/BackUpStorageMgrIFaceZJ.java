package com.cwc.app.iface.zj;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;

public interface BackUpStorageMgrIFaceZJ {
	
	public void combinationBackUpUnion(long ps_id,long pc_id,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public void addBackUpStore(long back_up_pc_id,long ps_id,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public void addBackUpStorage(long back_up_pc_id,long ps_id)
	throws Exception;
	
	public DBRow getDetailBackUpStore(long pc_id,long ps_id)
	throws Exception;
	
	public Map<String,DBRow> splitRequire(long out_id)
	throws Exception;
	
	public void performSplitRequireDetail(HttpServletRequest request)
	throws Exception;
	
	public boolean existsSplitRequireDetailForWaybill(long waybill_id)
	throws Exception;
	
	public boolean existsSplitRequireDetailForOut(long out_id)
	throws Exception;
	
	public DBRow[] getToPcidsByOut(long out_id)
	throws Exception;
}
