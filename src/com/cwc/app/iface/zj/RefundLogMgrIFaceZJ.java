package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface RefundLogMgrIFaceZJ {
	
	public long addRefundLog(String refund_delivery_code,String refund_people,int refund_status)//添加退款记录
	throws Exception;
	
	public void modRefundLog(long refund_id,int refund_status,String refund_people)//修改退款记录
	throws Exception;
	
	public DBRow[] getRefundLogs(String begintime,String endtime,int refund_status,PageCtrl pc)//获得退款记录
	throws Exception;
	
	public DBRow[] searchRefundLogByDeliveryCode(String refund_delivery_code,PageCtrl pc)//根据运单号模糊查询退款记录
	throws Exception;
	
	public void applyForRefundLog(HttpServletRequest request)//申请退款
	throws Exception;
}
