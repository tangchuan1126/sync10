package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface ShipProductMgrIFaceZJ {
	
	public DBRow[] getTodayShipProduct()//今天产生的所有外部运单号
	throws Exception;
	
	public String upload(HttpServletRequest request)//上传外部运单号
	throws Exception ;
	
	public DBRow[] excelDBRow(String filename) //上传excel转化为DBRow
	throws Exception;
	
	public DBRow[] errorDeliveryCode(HttpServletRequest request)//比较运单号，得到空白运单
	throws Exception;
}
