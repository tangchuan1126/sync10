package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ReturnStepMgrIFaceZJ {
	public DBRow[] filterReturnSteps(long ps_id,long catalog_id,String product_line_id,PageCtrl pc)//过滤库存残损件
	throws Exception;
	
	public DBRow[] searchReturnStepByName(long catalog_id, long ps_id,String product_line_id, String name, PageCtrl pc)//搜索库存残损件
	throws Exception;
	
	public DBRow[] getReturnStepLocationByPcid(long ps_id,long pc_id,PageCtrl pc)//获得残损件存放位置
	throws Exception;
	
	public void spiltReturnStepUnion(HttpServletRequest request)//残损件套装拆散
	throws Exception;
}
