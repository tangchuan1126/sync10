package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.db.DBRow;

public interface CartReturnProductReasonIFace {
	public void putToReturnProductReasonCart(HttpServletRequest request)//放入购物车
	throws Exception;
	
	public void removeReturnProductReasonCart(HttpServletRequest request)//移除购物车
	throws Exception;
	
	public void clearReturnProductReasonCart(HttpSession session)//清空session
	throws Exception;
	
	public void flush(HttpSession session)
	throws Exception;
	
	public DBRow[] getProductReason()
	throws Exception;
	
	public void modReturnQuantityReason(HttpServletRequest request)
	throws Exception;
	
	public float getSumQuantity()
	throws Exception;
	
	public void putToReturnProductReasonCartForSelect(HttpServletRequest request)//选择方式将商品放入购物车
	throws Exception;
}
