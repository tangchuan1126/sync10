package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

public interface WayBillB2BMgrIFaceZJ {
	public void wayBillB2BReceiveStore(HttpServletRequest request)
	throws Exception;
	
	public void wayBillB2BAllocateStore(HttpServletRequest request)
	throws Exception;
}
