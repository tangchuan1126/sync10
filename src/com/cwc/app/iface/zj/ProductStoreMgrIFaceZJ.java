package com.cwc.app.iface.zj;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuB2BOrderBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.db.DBRow;

public interface ProductStoreMgrIFaceZJ {
	public void  reserveProductStore(long ps_id, long pc_id, long title_id,float quantity, long system_bill_id, int system_type_id,String lot_number,AdminLoginBean adminLoggerBean,DBRow[] containerTypes)
	throws Exception;
	
	public ArrayList<PreCalcuB2BOrderBean> calcuB2BOrderNotOptimalWareHouse(long transport_id,long optimal_psid,HttpSession session)
	throws Exception;
	
	public PreCalcuB2BOrderBean calcuB2bOrderOptimalWareHouse(long b2b_oid,long optimal_psid,HttpSession session)
	throws Exception;
	
	public void pickUpPhysical(long ps_id,long slc_id,long pc_id,float quantity,AdminLoginBean adminLoggerBean,long out_id,String serial_number,String machine,String foType,int from_type,long from_type_id,long from_con_id,int pick_type,long pick_type_id,long pick_con_id,long opation_con_id)
	throws Exception;//物理拣货
	
	public DBRow[] pickUpException(long ps_id,long slc_id,long pc_id,float error_quantity,long out_id,String serial_number,String machine,String foType,int from_type,long from_type_id,long from_con_id,int pick_type,long pick_type_id,long pick_con_id,long error_con_id,AdminLoginBean adminLoggerBean)
	throws Exception;//拣货异常
	
	public PreCalcuB2BOrderBean calcuOrderB2B(HttpSession session,long ps_id,long title_id)
	throws Exception;
	
	public void putProductToLocation(long ps_id,long pc_id,float quantity,long system_bill_id,int system_bill_type,AdminLoginBean adminLoggerBean,String location,String serial_number,long lp_id,long title_id,String lot_number,String machine,ProductStoreLogBean inProductStoreLogBean) 
	throws Exception;
	
	public int getContainerStoreAvailableCount(long ps_id,long pc_id,long title_id,int container_type,long container_type_id,String lot_number)
	throws Exception;
	
	public String containerLoadProduct(long ps_id,long con_id)
	throws Exception;
	
	public HashMap productMongoDB(long pc_id)
	throws Exception;
	
	public void differentContainerApprove(long ps_id,long slc_id,long pc_id,int quantity,long con_id,long title_id,int physicalOperation,int operation,int system_bill_type,long system_bill_id,String lot_number,String machine,String serial_number,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public void differentContainerApproveCopyTree(long ps_id,long con_id,long slc_id,long pc_id,long title_id,String number,long time_number)
	throws Exception;
	
	public DBRow[] getProductStoreCount(String ps_id, long slc_area, long slc_id,String titleIds, long pc_id, String lot_number, String catalogs, long product_line)
	throws Exception;
	
	public void copyTempContainerToContainer(long con_id)
	throws Exception;
	
	public DBRow[] getProductStoreCountGroupByTitle(String ps_id, long slc_area, long slc_id,String titleIds, long pc_id, String lot_number, String catalogs,long product_line)
	throws Exception; 
	
	public DBRow[] getProductStoreCountGroupByLotNumber(long ps_id,long title_id, long pc_id, String lot_number)
	throws Exception; 
	
	public DBRow[] getProductStoreCountGroupBySlc(long ps_id,long title_id,long pc_id,String lot_number)
	throws Exception;
	
	public DBRow[] getProductStoreCountGroupByContainer(long ps_id,long slc_id,int container_type,long title_id,long pc_id,String lot_number)
	throws Exception;
	
	public boolean hasInnerContainer(long ps_id,long con_id)
	throws Exception;
	
	public DBRow[] productCountOfContainer(long ps_id,long con_id,long title_id,long pc_id)
	throws Exception;
	
	public HashMap getContainerInnerTree(long ps_id,long con_id,long pc_id)
	throws Exception;
	
	public DBRow[] getProductStoreContainerCount(long ps_id,long slc_id,long title_id,long pc_id,String lot_number)
	throws Exception;
}
