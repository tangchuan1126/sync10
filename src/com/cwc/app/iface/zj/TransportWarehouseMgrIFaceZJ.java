package com.cwc.app.iface.zj;

import com.cwc.db.DBRow;

public interface TransportWarehouseMgrIFaceZJ {
	
	public void addTransportWareHouseSub(DBRow[] dbs,long transport_id,String machine_id)//伪转运提交
	throws Exception;
	
	public void delTransportWareHouseByTransportId(long transport_id,String machine_id)//删除交货比较
	throws Exception;
	
	public DBRow[] compareTransportByTransportId(long transport_id)//伪交货比较
	throws Exception;
	
	public DBRow[] getTransportWareHouseByTransportId(long transport_id)//根据交货单ID获得伪交货入库
	throws Exception;
	
	public DBRow[] compareTransportOutWareByTransportId(long transport_id)
	throws Exception;
	
	public DBRow[] transportReceiveCompareCount(long transport_id)
	throws Exception;
	
	public DBRow[] getTransportWareHouseBySNNotMachine(String sn,String machine_id,long transport_id)
	throws Exception;
	
	public DBRow[] getTransportWareHousePutLocationCompare(long transport_id)
	throws Exception;
}
