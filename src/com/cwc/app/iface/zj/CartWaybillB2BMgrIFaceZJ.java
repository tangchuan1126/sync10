package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.db.DBRow;


public interface CartWaybillB2BMgrIFaceZJ {
	public void putToCart(HttpServletRequest request)//放入购物车
	throws Exception;
	
	public void removeProduct(HttpServletRequest request)//移出购物车
	throws Exception;
	
	public void cleanCart(HttpServletRequest request)//清空购物车
	throws Exception;
	
	public DBRow[] getCartWaybillB2bProduct()//获得购物车内的商品
	throws Exception;
	
	public void flush(HttpSession session)//加载B2B购物车
	throws Exception;
}
