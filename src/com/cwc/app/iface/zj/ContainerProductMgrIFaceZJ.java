package com.cwc.app.iface.zj;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public interface ContainerProductMgrIFaceZJ {
	public void addContainerProduct(long cp_pc_id,String cp_sn,float cp_quantity,long cp_lp_id)
	throws Exception;
	
	public void printLicensePlate(long lp_id,ArrayList<DBRow> containerProducts)
	throws Exception;
	
	public DBRow getDetailContainer(String searchKey)
	throws Exception;
	
	public long addContainer(DBRow row)
	throws Exception;
	
	public DBRow[] getContainerProductByContainer(long lp_id)
	throws Exception;
	
	public DBRow[] getProductForContainer(long lp_id)
	throws Exception;
	
	public DBRow[] getProductCodeForContainer(long lp_id)
	throws Exception;
	
	public DBRow[] getSerialProductForContainer(long lp_id)
	throws Exception;
	
	public DBRow getDetailContainerByContainer(String container)
	throws Exception;

	public void delContainerProductByLP(long lp_id)
	throws Exception;

	public DBRow getDetailContainer(long lp_id)
	throws Exception;
}
