package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;

public interface ProductCodeMgrIFaceZJ
{
	public DBRow addProductCode(HttpServletRequest request)
	throws Exception;
	
	public DBRow getDetailProductCode(String p_code)
	throws Exception;

	public DBRow[] getProductCodesByPcid(long pc_id)
	throws Exception;
	
	public void initProductCode(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getUseProductCodes(long pc_id)
	throws Exception;
	
	public long addProductCodeSubNoLog(long pc_id,String p_code,int code_type,AdminLoginBean adminLoggerBean) 
	throws Exception;
	
	public long addProductCodeSub(long pc_id,String p_code,int code_type,AdminLoginBean adminLoggerBean) 
	throws Exception ;
	/**
	 * 通过pcid、codeType、code查询code
	 * @param codeType
	 * @param pcCode
	 * @param pc_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月5日 上午9:59:08
	 */
	public DBRow[] getProductCodeByTypeAndCodePcid(int codeType, String pcCode, long pc_id) throws Exception;
}
