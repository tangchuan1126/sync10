package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface DamagedRepairOutboundApproveMgrIFaceZJ {
	public DBRow[] fillterDamagedRepairOutboundApprove(long send_psid,long receive_psid, PageCtrl pc,int approve_status,String sorttype)//过滤返修单发货审核，支持按返修仓库，接收仓库查找
	throws Exception;
	
	public DBRow[] getDamagedRepairOutboundApproverDetailsByTsaid(long rsa_id,PageCtrl pc)//根据返修发货审核单ID获得需审核差异详细
	throws Exception;
	
	public void approveDamagedRepairOutboundDifferent(long rsa_id,long[] rsad_ids,String[] notes,AdminLoginBean adminLoggerBean)//返修单差异进行审核
	throws Exception;
	
	public void addDamagedRepairOutboundApproveSub(long repair_id,AdminLoginBean adminLoggerBean)//返修单申请审核提交
	throws Exception;
	
	public long approveDamagedRepairOutboundForJbpm(long tsa_id)//工作流拦截返修单发货审核
	throws Exception;
	
	public void addDamagedRepairOutboundApprove(HttpServletRequest request)
	throws Exception;
	
}
