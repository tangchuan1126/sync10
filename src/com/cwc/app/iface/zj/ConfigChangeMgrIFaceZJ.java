package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuConfigChangeBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ConfigChangeMgrIFaceZJ {
	public long addConfigChange(HttpServletRequest request)
	throws Exception;
	
	public long addConfigChangeItemJqgrid(HttpServletRequest request)
	throws Exception;
	
	public void modConfigChangeItemJqgrid(HttpServletRequest request)
	throws Exception;
	
	public void delConfigChangeItemJqgrid(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getSearchConfigChange(long ps_id,long title_id,int cc_status,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getConfigChangeItemByCcid(long cc_id,PageCtrl pc,String sidx,String sord,FilterBean filterBean)
	throws Exception;
	
	public DBRow getDetailConfigChangeByCcid(long cc_id)
	throws Exception;
	
	public DBRow getDetailConfigChangeItemJqgrid(HttpServletRequest request)
	throws Exception;
	
	public PreCalcuConfigChangeBean preCalcuConfigChange(long cc_id)
	throws Exception;
	
	public void allocateConfigChange(HttpServletRequest request)
	throws Exception;
	
	public void addConfigChangeForSystemBill(long ps_id,long title_id,AdminLoginBean adminLoggerBean,int system_bill_type,long system_bill_id,DBRow[] configChangeProducts)
	throws Exception;
	
	public DBRow[] uploadFilteToDBRow(String filename)
	throws Exception;
	
	public void saveUploadConfigChangeItem(HttpServletRequest request)
	throws Exception;
}
