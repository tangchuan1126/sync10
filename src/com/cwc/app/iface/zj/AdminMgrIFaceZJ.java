package com.cwc.app.iface.zj;


import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.AccountOrPwdIncorrectException;
import com.cwc.db.DBRow;

public interface AdminMgrIFaceZJ 
{
	public DBRow MachineLogin(String account, String password)
	throws AccountOrPwdIncorrectException,AccountNotPermitLoginException,Exception;
}
