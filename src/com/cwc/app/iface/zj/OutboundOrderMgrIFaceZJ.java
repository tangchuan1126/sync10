package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface OutboundOrderMgrIFaceZJ {
	
	public DBRow[] compensationValueForPs(long ps_id,int[] status,long pc_id)//根据仓库，状态查看补偿值（集合型）
	throws Exception;
	
	public DBRow compensationValueForPsAndP(long ps_id,int[] status,long pc_id)//根据仓库，状态，商品ID查看补偿值
	throws Exception;
	
	public DBRow[] getOutOrderProductItems(long out_id,int[] status)//根据出库单获得出库单内运单的商品（标准拼装与定制套装已拆散）
	throws Exception;
	
	public DBRow getDetailOutboundById(long out_id)//根据出库单ID获得出库单
	throws Exception;
	
	public DBRow[] getWayBillIdsByOutIdAndP(long out_id,long pc_id,int[] status)
	throws Exception;
	
	public void modOutboundOrder(HttpServletRequest request)
	throws Exception;
	
	public DBRow outStoreValueForPsAndP(long ps_id,int[] status,long pc_id)//根据仓库，状态查看出口值
	throws Exception;
	
	public DBRow[] getOutOrderItemLocation(long out_id,int[] status)//根据出库单获得需出库商品
	throws Exception;
	
	public void summarizeWaybillOutListDetail(long out_id)
	throws Exception;
	
	public void summarizeTransportOutListDetail(long out_id)
	throws Exception;
	
	public void addOutStorebillOrderForConfigChange(HttpServletRequest request)
	throws Exception;
	
	public int getOutListContainerCountBySystemBill(int system_bill_type,long system_bill_id)
	throws Exception;
	
}
