package com.cwc.app.iface.zj;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface TradeMgrIFaceZJ {
	public DBRow[] searchTrades(String search_key,int search_mode,PageCtrl pc)
	throws Exception;
	
	public DBRow getCreateOrderTradeByOid(long oid)
	throws Exception;
	
	public DBRow[] getTradeItemsByTradeId(long trade_id)
	throws Exception;
}
