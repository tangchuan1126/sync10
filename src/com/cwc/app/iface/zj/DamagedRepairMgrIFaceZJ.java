package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface DamagedRepairMgrIFaceZJ {
	public void addDamagedRepair(HttpServletRequest request)//添加残损返修单
	throws Exception;
	
	public void modDamagedRepair(HttpServletRequest request)//修改残损返修单
	throws Exception;
	
	public void delDamagedRepair(HttpServletRequest request)//删除残损返修单
	throws Exception;
	
	public DBRow[] filterDamagedRepair(long send_pdid,long receive_psid,int status,PageCtrl pc)//过滤返修单
	throws Exception;
	
	//jqgrid获得返修单明细
	public DBRow[] getDamagedRepairDetailsByRepairId(long repair_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)
	throws Exception;
	
	public DBRow getDetailDamagedRepair(long repair_id)//根据返修单ID获得返修明细
	throws Exception;
	
	public void addDamagedRepairDetail(HttpServletRequest request)//添加返修单明细 
	throws Exception;
	
	public void modDamagedRepairDetail(HttpServletRequest request)//修改返修明细 
	throws Exception;
	
	public void delDamagedRepairDetail(HttpServletRequest request)//删除返修明细 
	throws Exception;
	
	public DBRow getDamagedRepairDetailById(HttpServletRequest request)//根据ID获得返修单明细
	throws Exception;
	
	public String[] uploadDamagedRepairDetail(HttpServletRequest request)//上传返修明细
	throws Exception,FileTypeException,FileException;
	
	public DBRow[] excelshow(String filename,String type)//返修上传excel转换DBRow[]
	throws Exception;
	
	public void saveDamagedRepairDetail(HttpServletRequest request)//保存返修明细
	throws Exception;
	
	public String downloadDamagedRepairOrder(HttpServletRequest request)//下载返修单
	throws Exception;
	
	public DBRow[] checkDamagedRepairStorage(long repair_id)//检查返修库存
	throws Exception;
	
	public void packingDamagedRepair(HttpServletRequest request)//返修单装箱
	throws Exception;
	
	public DBRow[] getDamagedRepairDetailByPsWithStatus(long send_psid,long receive_psid,int status)//获得返修单明细，支持根据返修仓库，接收仓库，返修单状态过滤
	throws Exception;
	
	public void deliveryDamagedRepair(long repair_id)//返修单起运
	throws Exception;
	
	public DBRow[] searchDamagedRepairByNumber(String key,PageCtrl pc)//根据转运单号查询转运单
	throws Exception;
}
