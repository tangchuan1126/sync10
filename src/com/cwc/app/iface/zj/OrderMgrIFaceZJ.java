package com.cwc.app.iface.zj;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;


public interface OrderMgrIFaceZJ {
	public int getOrderTaskSum(long order_id)//根据订单返回订单产生的订单任务
	throws Exception;
	
	public DBRow getOrderBadReview(String client_id)//根据client_id邮箱查询该账户历史差评次数
	throws Exception;
	
	public void modOrdersHandle(long[] oids,int handle)//批量修改订单状态
	throws Exception;
	
	public DBRow[] getResembleOrderByOid(long oid)//根据订单ID搜索有可能与此订单合并发货的订单
	throws Exception;
	
	public void checkOrderShip(long oid)//检查订单的所有运单是否都是完结状态
	throws Exception;
	
	public long saveWarrantyOrder(String business,float quantity,String mc_currency,String client_id,long rp_id,long ccid,long pro_id,String address_name,String address_street,String address_city,String address_state,String address_zip,String tel,long bill_id,long sid)//创建质保订单
	throws Exception;
	
	public DBRow[] trackingOrder(String st,String en,long product_line_id,long catalog_id,String p_name,long ca_id,long ccid,long pro_id,int internal_tracking_status,int daytype,int lastday,int extended_day,PageCtrl pc)
	throws Exception;
}
