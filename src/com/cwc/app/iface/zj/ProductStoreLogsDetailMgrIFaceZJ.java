package com.cwc.app.iface.zj;

import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductStoreLogsDetailMgrIFaceZJ {
	
	public void inStoreProdcutLogsDetail(long psl_id,ProductStoreLogBean inProductStoreLog,long from_psl_id) 
	throws Exception;
	
	public void outStoreProductLogsDetail(long psl_id,ProductStoreLogBean outProductStoreLog)
	throws Exception;
	
	public void rebackStoreProductLogsDetail(long psl_id,ProductStoreLogBean rebackStoreLog)
	throws Exception;
	
	public DBRow[] getProductStoreDetailLogsByPslid(long psl_id)
	throws Exception;
	
	public DBRow getDetailProductStoreDetailLogsByPsldid(long psld_id)//根据ID获得批次
	throws Exception;
	
	public DBRow[] getProductStoreLogDetailsByFromPsldId(long from_psld_id)//获得批次的下级批次
	throws Exception;
	
	public DBRow[] getProductStorelogDetailsByToPsld(long to_psld_id)//根据组合的目标批次ID获得来源批次
	throws Exception;
	
	public void initProductStoreLogDetail(long ps_id)
	throws Exception;
	
	public DBRow[] getProductStorelogDetailsFromUnionLogByFromPsld(long from_psld_id)
	throws Exception;
	
	public DBRow[] backlogOfGoods(long product_line_id,long catalog_id,long ps_id,String p_name,int backlog_day,PageCtrl pc)//积压货物
	throws Exception;
	
	public DBRow[] backlogGoodsStoreLogDetals(long pc_id,long ps_id,int backlog_day)
	throws Exception;
	
	public DBRow[] salesRate(String st,String en,long product_line_id,long catalog_id,String p_name,long ps_id,int sales_rate,int greater_less,PageCtrl pc)
	throws Exception;
}
