package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface InvoiceMgrIFaceZJ {
	
	public DBRow getDetailDeliveryInfoByInvoiceId(long invoice_id)//根据发票模板获得发件人信息
	throws Exception;
	
	public DBRow getFullInvoiceByInvoiceId(long invoice_id)//根据发票模板获得完整发票信息（包含发件人，发票信息）
	throws Exception;
	
	public DBRow getDetailInvoiceForRound(long invoice_id)//根据发票模板获得发票信息（经过轮循）
	throws Exception;
	
	public DBRow[] getAllRoundInvoicesByInvoiceId(long invoice_id)//根据发票模板获得发票循环信息
	throws Exception;
	
	public long addRoundInvoice(HttpServletRequest request)//添加模板轮循样式
	throws Exception;
	
	public void delRoundInvoice(HttpServletRequest request)//删除模板轮循样式
	throws Exception;
	
	public DBRow getDetailRoundInvoiceById(long round_invoice_id)//根据ID获得模板样式
	throws Exception;
	
	public void modRoundInvoiceById(HttpServletRequest request)//修改模板样式
	throws Exception;
}
