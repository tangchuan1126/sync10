package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface QAMgrIFaceZJ 
{
	public long addQuestion(HttpServletRequest request)//添加问题
	throws Exception;
	
	public DBRow[] getAllQuestion(PageCtrl pc,int question_status)//获得所有问题
	throws Exception;
	
	public String uploadQuestionImage(HttpServletRequest request,HttpServletResponse response)//CKEditor上传图片
	throws Exception;
	
	public DBRow getDetailQuestionById(Long id)//根据问题ID获得问题详细
	throws Exception;
	
	public DBRow[] getAnswersByQuestionId(long question_id)//根据问题ID检索该问题的答案
	throws Exception;
	
	public long addAnswer(HttpServletRequest request)//添加答案
	throws Exception;
	
	public String addAnswerForJbpm(long question_id)//提供工作流拦截
	throws Exception;
	
	public void modQuestionById(HttpServletRequest request)//修改问题
	throws Exception;
	
	public long modAnswer(HttpServletRequest request)//修改答案
	throws Exception;
	
	public DBRow[] searchQuestionByCatalog(long catalog_id,String pname,PageCtrl pc,int question_status)//根据商品分类搜索问题
	throws Exception;
	
	public DBRow[] searchQuestionByProductId(long product_id,PageCtrl pc,int question_status)//根据商品ID搜索问题
	throws Exception;
	
	public DBRow[] searchQuestionByIndexKey(String key,long catalog_id,String pname,PageCtrl pc,int question_status)//关键词检索
	throws Exception;
	
	public void delQuestionById(HttpServletRequest request)//删除问题
	throws Exception;
	
	public long modQuestionHasAnswer(HttpServletRequest request)//修改已回答问题
	throws Exception;
}
