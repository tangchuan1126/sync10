package com.cwc.app.iface.zj;


import javax.servlet.http.HttpServletRequest;







import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.purchase.AffirmPriceException;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.exception.purchase.NullPurchaseException;
import com.cwc.app.exception.purchase.TransferException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface PurchaseIFace {
	//采购单
	public long addPurchase(HttpServletRequest request)//生成采购单
	throws Exception;
	
	public void updatePurchaseBasic(HttpServletRequest request) throws Exception;
	public DBRow[] getPurchaseALL(PageCtrl pc,long supplier_id)//根据条件查询全部采购单
	throws Exception;
	
	public DBRow[] getPurchaseFOLLOWUP(HttpServletRequest request,PageCtrl pc,long ps_id,int day)//根据条件查询采购单需跟进
	throws Exception;
	
	public DBRow[] getPurchaseAFFIRMPRICE(PageCtrl pc,long supplier_id)//根据条件查询采购单需确认价格
	throws Exception;
	
	public DBRow[] getPurchaseAFFIRMTRANSFER(PageCtrl pc,long supplier_id)//根据条件查询采购单价格审核
	throws Exception;
	
	public DBRow[] getPurchaseNOTTRANSFER(PageCtrl pc,long supplier_id)//根据条件查询采购单未转账
	throws Exception;
	
	public DBRow[] getPurchasePARTTRANSFER(PageCtrl pc,long supplier_id)//根据条件查询采购单部分转账
	throws Exception;
	
	public DBRow[] getPurchaseALLTRANSFER(PageCtrl pc,long supplier_id)//根据条件查询采购单全部转账
	throws Exception;
	
	public DBRow[] getPurchaseFINISH(PageCtrl pc,long supplier_id)//根据条件查询采购单已完成
	throws Exception;
	
	public DBRow[] getPurchaseCANCEL(PageCtrl pc,long supplier_id)//根据条件查询采购单已取消
	throws Exception;
	
	public DBRow[] getPurchasePAYMENT(PageCtrl pc,long supplier_id)//根据条件查询采购单需结清
	throws Exception;
	
	public DBRow[] getPurchaseNoArrive(PageCtrl pc,long supplier_id)//未到货采购单
	throws Exception;
	 
	public DBRow[] getPurchasePartArrive(PageCtrl pc,long supplier_id)//部分到货采购单
	throws Exception;
	
	public DBRow[] getPurchaseAllArrive(PageCtrl pc,long supplier_id)//到货完成采购单
	throws Exception;
	
	public DBRow getDetailPurchaseByIdAndSupplierId(HttpServletRequest request)//根据采购单ID，供应商ID获得采购单详细
	throws Exception;
	
	public DBRow[] getPurchasesBySupplier(long supplier_id,PageCtrl pc)//根据供应商过滤采购单
	throws Exception;
	
	public DBRow[] getPurchaseMore(PageCtrl pc,long supplier_id)//到货多订单
	throws Exception;

	public DBRow getDetailPurchaseByPurchaseid(String id)//根据id查询采购单
	throws Exception;
	
	public long purchaseTransfer(HttpServletRequest request)//采购单转账
	throws Exception,TransferException;
	
	public void followupPurchase(HttpServletRequest request)//采购单跟进 
	throws Exception;
	
	public void cancelPurchase(HttpServletRequest request) //采购单取消
	throws Exception;
	
	public void logsPurchase(HttpServletRequest request)//采购员备注
	throws Exception;
	
	public void affirmPricePurchase(HttpServletRequest request)//采购员确认价格 
	throws Exception,NullPurchaseException,AffirmPriceException;
	
	public void affirmTransferPurchase(HttpServletRequest request) //确认财务可以转账
	throws Exception;
	
	public void finishPurchase(HttpServletRequest request)//采购单强制完成 
	throws Exception;
	
	public void notice(HttpServletRequest request)//通知采购
	throws Exception;
	
	public void modPurchase(HttpServletRequest request)//修改采购单（合同条款）
	throws Exception;
	
	public void modPurchaseDelivery(HttpServletRequest request)//修改采购单交货地址
	throws Exception;

	
	//采购单详细
	
	public DBRow[] getPurchaseDetailByPurchaseid(long purchase_id,PageCtrl pc,String type)//通过采购单ID获得采购单详细
	throws Exception;
	
	public DBRow[] getPurchaseDetailByPurchasename(long purchase_id,String purchase_name)//查找采购单内是否有某商品
	throws Exception;
	
	public DBRow getPurchaseDetailOneByPurchaseId(String purchase_id) throws Exception;
	
	public String[] uploadPurchaseDetail(HttpServletRequest request, String purchase_id_str, String tempfilename, String expectArrTime, int isOutterUpdate)//上传采购单详细
	throws Exception,FileTypeException;
	
	public DBRow[] excelshow(String filename,String type) //excel文件转换成DBRow[]
	throws Exception;
	
	public void savePurchaseDetail(HttpServletRequest request)//保存采购单详细（增加，删除，修改采购单详细）
	throws Exception,FileException;
		
	public void clearPurchaseDetailByPurchaseid(HttpServletRequest request)//清空采购单列表
	throws Exception;
	
	public String[] uploadIncoming(HttpServletRequest request)//上传入库文件 
	throws Exception;
	
	public DBRow[] csvDBRow(String filename,String type)//cvs转换为DBRow
	throws Exception;
	
	public DBRow getPurchaseDetailByproductbarcod(long purchase_id,String proudct_barcod)//根据商品条码在采购单内查询商品
	throws Exception;
	
	public void incoming(HttpServletRequest request)//商品入库
	throws Exception;
	
	public DBRow[] getPurchaseDetails(HttpServletRequest request)//获得采购单详细（打印）
	throws Exception;
	
	//日志
	public DBRow[] getfollowuplogs(long purchase_id,int followup_type)//根据采购单获得日志
	throws Exception;
	
	public void incomingSub(DBRow[] purchasedetails,long purchase_id,String machine_id,AdminLoginBean adminLoggerBean)//采购单入库提交
	throws Exception;
	/**
	 * 提供接口
	 */
	
	public DBRow getDetailPurchaseDetailByPnameNew(String pname,long ps_id) //根据商品查询最后一次采购信息
	throws Exception; 
	
	public DBRow getDetailPurchaseDetailByProductbarcodNew(String proudct_barcod,long ps_id)//根据商品条码查询最后一次采购信息
	throws Exception;
	
	public DBRow getDetailPurchaseDetailByProductIdNew(long product_id,long ps_id)//根据商品ID查询最后一次采购信息
	throws Exception;
	
	public DBRow[] getPurchaseDetailBydelay()//查询所有预计到货时间小于今天且未到货采购单详细
	throws Exception;
	
	public String productPriceChange(long purchase_id)//价格改变调用一次
	throws Exception;
	
	public long purchaseTransferForJbpm(long purchase_id)//为工作流提供拦截，采购单转账
	throws Exception;
	
	public long affirmPricePurchaseForJbpm(long purchase_id)//采购单价格确定，提供工作流拦截
	throws Exception;
	
	public long affirmTransferPurchaseForJbpm(long purchase_id)//采购单可转账，工作流拦截
	throws Exception;
	
	public long paymentForJbpm(long purchase_id)//采购单需结清，工作流拦截
	throws Exception;
		
	public long noticePurchaserForJbpm(long purchase_id)//工作流拦截通知采购员
	throws Exception;
	
	public long warehouseForJbpm(long purchase_id)//工作流拦截采购单有商品入库
	throws Exception;
	
	//采购单过滤
	public DBRow[] getPurchaseFillter(long supplier_id,long ps_id,int purchase_status,int money_status,int arrival_time,String beginTime,String endTime,PageCtrl pc, int day, long productline_id,int invoice,int drawback,int need_tag,int quality_inspection)
	throws Exception;

	//采购单供应商过滤，不可检索出已取消的
	public DBRow[] getPurchaseSupplierFillter(long supplier_id,long ps_id,int purchase_status,int money_status,int arrival_time,String beginTime,String endTime,PageCtrl pc)
	throws Exception;
	
	public DBRow[] searchPurchaseBySupplierOrId(String key,PageCtrl pc)//采购单搜索
	throws Exception;
	
	public long delayPurchaseForJbpm(long purchase_id)//采购单有货物预计到达日期内未到
	throws Exception;
	
	public DBRow[] supplierSearchPurchaseBySupplierOrId(String key,PageCtrl pc,long supplier_id)
	throws Exception;
	
	public DBRow[] getNoFinishPurchase(long supplier_id)
	throws Exception;
	
	public DBRow[] getfollowuplogsLastNumber(long purchase_id,int followup_type , int number)//根据 获取最近的number条 采购单获得日志
	throws Exception;
	
	public DBRow getSendAddressAndDeliveryAddressForPurchase(HttpServletRequest request)//根据采购单获得供应商地址与交货地址
	throws Exception;
	
	public void updatePurchaseTerms(HttpServletRequest request) throws Exception;//更新采购单的条款
	public void addPurchaseTerms(HttpServletRequest request, long purchase_id) throws Exception;//添加采购单的条款
	
	public String[] updatePurchaseTermsAndQuality(HttpServletRequest request) throws Exception;//更新采购单的条款及质检上传
	public DBRow[] getPurchaseDetailByPurchaseId(long purchase_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)//根据转运单ID获得转运单明细
	throws Exception;
	
	public void modPurchaseDetailGrid(HttpServletRequest request)//jqgrid修改采购明细
	throws Exception;
	
	public void addPurchaseDetailGrid(HttpServletRequest request)//jqgrid添加采购单明细
	throws Exception;
	
	public void delPurchaseDetailGrid(HttpServletRequest request)//jqgrid删除采购单明细
	throws Exception;
	
	public DBRow getPurchaseDetailById(HttpServletRequest request)
	throws Exception;
	
	public String downloadPurchase(HttpServletRequest request)//采购单下载
	throws Exception;
	
	public void updatePurchaseAllProcedures(HttpServletRequest request) throws Exception;

	public void addPurchaseAllProcedures(HttpServletRequest request, long purchase_id) throws Exception;
	
	public DBRow[] searchPurchase(String search_key,int search_mode,PageCtrl pc)
	throws Exception;
	
	public float getPurchaseVolume(long purchase_id)//获得采购单总体积
	throws Exception;
	
	public void updatePurchaseDetailVolume()//初始化采购单明细体积
	throws Exception;
	
	public float getPurchaseWeight(long purchase_id)//获得采购单总重量
	throws Exception;
	
	public double getPurchasePrice(long purchase_id)
	throws Exception;
	
	public long addPurchaseAll(HttpServletRequest request) //保存采购单所有的信息
	throws Exception;
	
	/**
	 * 添加采购单所有流程
	 * @param request
	 * @throws Exception
	 */
	public void addPurchaseProcedures(HttpServletRequest request, long purchase_id) throws Exception;
	/**
	 * 更新采购单所有流程
	 * @param request
	 * @throws Exception
	 */
	public void updatePurchaseProdures(HttpServletRequest request) throws Exception;
	/*
	 * 加日志
	 */
	public long addPurchasefollowuplogs(int followup_type,int followTypeSub, long purchase_id,String followup_content,long follower_id,String follower)
	throws Exception;
	public void updatePurchaseVW(long purchase_id)
	throws Exception;
	
}
