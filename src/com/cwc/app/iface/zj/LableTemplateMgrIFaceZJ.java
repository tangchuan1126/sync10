package com.cwc.app.iface.zj;


import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface LableTemplateMgrIFaceZJ {
	public long addLableTemplate(HttpServletRequest request)//添加标签模板
	throws Exception;
	
	public void modLableTemplate(HttpServletRequest request)//修改标签模板
	throws Exception;
	
	public void delLableTemplate(HttpServletRequest request)//删除标签模板
	throws Exception;
	
	public DBRow[] getAllLableTemplate(PageCtrl pc)//获得全部标签呢模板
	throws Exception;
	
	public DBRow getDetailLableTemplateById(HttpServletRequest request)//获得标签模板详细
	throws Exception;
	
	public DBRow getDetailLableTemplateById(long lable_template_id)//获得标签模板详细
	throws Exception;
	
	public DBRow[] getAllProductTemplate(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getAllTemplateLabel(PageCtrl pc)//获得全部模板
	throws Exception;
}
