package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;
import com.cwc.db.DBRow;

public interface FileMgrIFaceZJ {
	public DBRow[] getFileByWithIdAndType(long with_id,int with_type) throws Exception;
	public void addFile(HttpServletRequest request) throws Exception;
	public void delFile(long file_id) throws Exception;
}
