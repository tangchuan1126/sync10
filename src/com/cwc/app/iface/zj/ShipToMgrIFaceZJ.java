package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ShipToMgrIFaceZJ {
	public DBRow[] getAllShipTo() throws Exception;
	
	public DBRow[] getShipTosWithCLP(long clp_type_id) throws Exception;
	
	/**
	 * 
	 * 获取仓库类型为客户仓库下，所以门店仓库
	 * 
	 * @param typeId
	 * @param shipToId
	 * @return
	 * @throws Exception <b>Date:</b>2014-8-5下午3:08:02<br>
	 * @author: cuicong
	 */
	public DBRow[] GetAllShipToCustomer(long typeId, long shipToId) throws Exception;
	public DBRow[] getAllShipToOrderByName(PageCtrl pc) throws Exception;
	public DBRow addShipTo(HttpServletRequest request) throws Exception;
	public boolean deleteShipTo(HttpServletRequest request) throws Exception;
	public DBRow updateShipTo(HttpServletRequest request) throws Exception;
	public DBRow getShipToById(long id) throws Exception;

	public DBRow[] getStorageCatalogOfShipToByTitle(HttpServletRequest request) throws Exception;

	public DBRow addStorageCatalog(HttpServletRequest request) throws Exception;

	public DBRow[] getStorageCatalogsOfShipTo(HttpServletRequest request) throws Exception;
	
	public DBRow[] getStorageCatalogsOfShipTo(long storage_type_id, int storage_type) throws Exception;

	public boolean deleteStorageCatalog(HttpServletRequest request) throws Exception;

	public DBRow updateStorageCatalog(HttpServletRequest request) throws Exception;
	
	public DBRow getStorageCatalogInfoById(long storageId) throws Exception;

	public DBRow[] getShipToListByIds(String ids, PageCtrl pc) throws Exception;
	
	public DBRow[] getShipToStorageListByIds(String ids, PageCtrl pc) throws Exception;
	//Yuanxinyu 导出excel
	public String exportProduct(HttpServletRequest request) throws Exception;
	public DBRow updateActivePause(HttpServletRequest request,int active)throws Exception;
	public DBRow[] ShipToAdvanceSearch(DBRow filter, PageCtrl pc) throws Exception;
    /*
	 * 查询地址本
	 * @param storage_type_id
	 * @param storage_type
	 * @param pc
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年7月1日 下午6:51:19
	 */
	public DBRow[] getAddressBook(long storage_type_id, int storage_type, PageCtrl pc) throws Exception;
	public DBRow[] GetAllShipToCustomer0(long typeId, long shipToId) throws Exception;

}
