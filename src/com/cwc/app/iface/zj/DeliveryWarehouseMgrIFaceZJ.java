package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;

public interface DeliveryWarehouseMgrIFaceZJ 
{
	public DBRow[] addCompareDeliveryWareHouse(String filename,long delivery_order_id)//添加交货比较
	throws Exception;
	
	public void addDeliveryWareHouseSub(DBRow[] dbs,String delivery_order_number)//伪交货提交
	throws Exception;
	
	public void delDeliveryWareHouseByDeliveryOrderNumber(String delivery_order_number)//删除交货比较
	throws Exception;
	
	public DBRow[] compareDeliveryByDeliveryOrderNumber(String delivery_order_number)//伪交货比较
	throws Exception;
	
	public String[] uploadIncoming(HttpServletRequest request)//伪交货文件提交
	throws Exception,FileTypeException,FileException;
	
	public DBRow[] getDeliveryByDeliveryOrderId(long delivery_order_id)//根据交货单ID获得伪交货入库
	throws Exception;
}
