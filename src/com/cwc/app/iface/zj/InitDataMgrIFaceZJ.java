package com.cwc.app.iface.zj;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface InitDataMgrIFaceZJ {
	
	public Map<String,DBRow[]> initData(HttpServletRequest request)
	throws Exception;
	
	public String initDataExcelRequest(HttpServletRequest request)
	throws Exception;
}
