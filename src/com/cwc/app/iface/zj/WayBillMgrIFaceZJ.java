package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface WayBillMgrIFaceZJ {
	public void addWayBillOrder(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getWayBillOrderByOid(long oid)
	throws Exception;
	
	public long addWayBillOrderSub(String address_name,String address_country,String address_city,String address_zip,String address_street,String address_state,long ccid,long pro_id,String client_id,String tel,long sc_id,String tracking_number,double shipping_cost,long ps_id,String destination,HttpSession session,long inv_di_id,String inv_uv,String inv_rfe,String inv_dog,String inv_tv,String mail_piece_shape,String delivery_note,AdminLoginBean adminLoggerBean,int waybill_from_type,String hs_code,DBRow[] products,int pkcount,String material,String dtp,String inv_rfe_chinese,String inv_dog_chinese,String material_chinese,double shippment_rate) 
	throws Exception;
	
	public void cancelWayBillOrder(HttpServletRequest request)//客服取消运单
	throws Exception;
	
	public void cancelWayBillOrderFromStore(HttpServletRequest request)//仓库取消运单
	throws Exception;
	
	public DBRow[] getWayBillOrderItems(long wayBill_id)
	throws Exception;
	
	public DBRow[] filterWayBillOrder(String st_date,String en_date,String p_name,long product_line_id,long catalog_id,String order_source,long ps_id,long ca_id,long ccid,long pro_id,long out_id,PageCtrl pc,int status,long sc_id,int product_status,int internal_tracking_status)
	throws Exception;
	
	public DBRow getDetailInfoWayBillById(long wayBillId) 
	throws Exception;
	
	public void upLoadTrackingNumber(HttpServletRequest request)
	throws Exception;
	
	public void printWayBill(long waybill_id,long adid,HttpServletRequest request)
	throws Exception;
	
	public void shipWayBill(long waybill_id,float ship_weight,long adid)
	throws Exception;
	
	public String getHsCode(long waybill_id)
	throws Exception;
	
	public DBRow[] returnOrdersByWayBillId(long waybill_id)
	throws Exception;
	
	public void caluteWaybillItem(long newWayBillId,long wayBillId,String ids, String numbers ,String tracking_number,double total_weight , double total_shipping_cost,long adid)
	throws Exception;
	 
	public DBRow[] getWayBillByOId(long oid , PageCtrl pc) 
	throws Exception;
	public void upLoadTrackingNumberToEbay(long waybill_id)
	throws Exception;
	
	public DBRow[] getWayBillByTrackingNumber(String number , PageCtrl pc)
	throws Exception;
	// 拆分WayBIll的时候添加WOI
	public void addWayBillItemAndUpdateOldWayBillItem(long newWayBillId,String id , String number , String tracking_number ,double total_weight , double total_shipping_cost)
	throws Exception;
	
	public DBRow[] getWayBillOrdersByOids(long[] oids)//根据订单ID集合获得运单集合
	throws Exception;
	
	public DBRow[] getWayBillOrderByPcid(long pc_id,long ps_id,int[] product_status)
	throws Exception;
	
	public void reCheckLackingWayBillOrdersSub(AdminLoginBean adminLoggerBean,long ps_id)
	throws Exception;
	
	public DBRow[] statisticsWayBillOrder(String start_date,String end_date,int date_type,long sc_id,float differences,int differences_type,int cost,int cost_type,long ps_id,int unfinished,int store_out_unfinished,PageCtrl pc)
	throws Exception;
	
	public void errorUpdate(String trackingNumber)
	throws Exception;
	
	public DBRow getWayBillOrderByWayBillId(long waybill_id)
	throws Exception;
	
	public void wayBillReturn(HttpServletRequest request)//运单异常
	throws Exception;
	
	public void wayBillResult(HttpServletRequest request)//运单异常处理结果
	throws Exception;
	
	public String exportWayBill(HttpServletRequest request)//导出运单
	throws Exception;
	
	public DBRow[] getDeliveryDateByOidandPcid(long oid,long pc_id)//根据商品ID，订单ID获得发货时间
	throws Exception;
	
	public void updateResidentialStatus(long waybill_id,String residentialStatus)//跟新运单的地址的residentialStatus验证（Fedex使用）
	throws Exception;
	
	public void addOrderNoteWithWaybill(long waybill_id,int operation_type,AdminLoginBean adminLoggerBean)//运单回填订单日志
	throws Exception;
	
	public DBRow[] getStockoutWaybillConutGroupByPs()
	throws Exception;
	
	public DBRow[] getOverdueSendWaybillOrderCountByPs()
	throws Exception;
	
	public DBRow[] getStockoutWaybillByPs(long ps_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getOverdueSendWaybillOrderByPs(long ps_id,PageCtrl pc)
	throws Exception;
	
	public void addWaybillNote(HttpServletRequest request)
	throws Exception;
	
	public String waybillSendDownload(HttpServletRequest request)
	throws Exception;
	
	public String exportWayBillCount(HttpServletRequest request)
	throws Exception;
	
	public void reCheckLackingWaybillOrder(HttpServletRequest request)
	throws Exception;
	
	public void reCheckLackingWaybillOrderSub(long waybill_id,AdminLoginBean adminLoggerBean)
	throws Exception;
}
