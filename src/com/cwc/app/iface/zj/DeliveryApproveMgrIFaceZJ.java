package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;


public interface DeliveryApproveMgrIFaceZJ 
{
	public void addDeliveryApprove(HttpServletRequest request)//申请交货单审核
	throws Exception;
	
	public DBRow[] allDeliveryApprove(long ps_id,PageCtrl pc,int approve_status,String sorttype)//获得全部采购单审核，支持按仓库查找
	throws Exception;
	
	public DBRow[] getDeliveryApproverDetailsByDaid(long da_id,PageCtrl pc)//根据交货审核单ID获得采购单需审核差异详细
	throws Exception;
	
	public void approveDeliveryDifferent(long da_id,long[] dod_ids,String[] notes,AdminLoginBean adminLoggerBean)//采购单差异进行审核
	throws Exception;
	
	public long approveDeliveryOrderForJbpm(long da_id)//工作流拦截交货单审核
	throws Exception;
	
	public void addDeliveryApproveSub(long delivery_order_id,AdminLoginBean adminLoggerBean)//交货单申请审核提交
	throws Exception;
	public void stockIn(long delivery_order_id,AdminLoginBean adminLoggerBean, int product_storep_oeration) throws Exception;//交货单入库
}
