package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductStorageMgrIFaceZJ {
	public DBRow[] getAllProductStorageByPsid(String[] ps_id, long catalog_id, long pro_line_id) throws Exception;
	
	public String downLoadProductStorage(HttpServletRequest request)// 下载商品库存警戒值
			throws Exception;
	
	public String[] importStorageAlert(HttpServletRequest request)// 上传商品库存警戒值
			throws Exception, FileTypeException;
	
	public DBRow[] checkImportStorageAlert(String filename, String storage_title)// 检查导入警戒数据
			throws Exception;
	
	public void saveImportStorageAlert(HttpServletRequest request)// 保存上传库存警戒值
			throws Exception;
	
	public DBRow[] getAllProductStorageByPcid(long catalogid, long psid, int type, long product_line_id,
			int union_flag, PageCtrl pc)// 过滤搜索库存信息
			throws Exception;
	
	public DBRow[] getProductStorageByName(long catalogid, long ps_id, long product_line_id, String name,
			int union_flag, PageCtrl pc)// 模糊搜索
			throws Exception;
	
	public DBRow[] getPurchaseCount(long ps_id, long pc_id, int[] purchase_status, boolean do_group) throws Exception;
	
	public DBRow[] getDeliveryingCount(long ps_id, long pc_id, int[] delivery_status, int[] transport_status,
			boolean do_group) throws Exception;
	
	public DBRow[] xmlProductStore(long product_line_id) throws Exception;
	
	public DBRow[] searchProductStorageCatalog(int def_flag, int return_flag, int sample_flag, int storage_type,
			PageCtrl pc) throws Exception;
	
	public String loadProductStorageForNexSearch() throws Exception;
	
	public DBRow[] getProductStorageCatalogsByTypeShipTo(int storage_type, long ship_to_id) throws Exception;
	
	public DBRow getProductStorageCatalogByPsid(int ps_id) throws Exception;
	
	/**
	 * 
	 * 根据title模糊查询
	 * 
	 * @param name
	 * @return
	 * @throws Exception <b>Date:</b>2015年2月4日下午3:56:47<br>
	 * @author: cuicong
	 */
	public DBRow[] getProductStorageCatalogLikeName(String name) throws Exception;
	
	public DBRow[] getProductStorageCatalogsByTypeShipTo0(int storage_type, long storage_type_id) throws Exception;
}
