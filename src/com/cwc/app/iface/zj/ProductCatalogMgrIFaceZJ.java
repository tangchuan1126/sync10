package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface ProductCatalogMgrIFaceZJ {
	public DBRow[] getAllProductCatalogChild()//获得全部分类平铺
	throws Exception;
	
	public void moveCatalog(HttpServletRequest request)//移动商品分类
	throws Exception;
	
	public DBRow[] getFatherTree(long catalog_id)//根据分类ID获得父类树形结构
	throws Exception;
}
