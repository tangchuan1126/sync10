package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

public interface QuoteMgrIFaceZJ {
	public void qutoPriceChangeGrossProfit(HttpServletRequest request)//根据当前零售报价更新毛利（未报价商品可初始化毛利）
	throws Exception;
}
