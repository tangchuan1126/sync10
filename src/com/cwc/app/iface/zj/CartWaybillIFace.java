package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.db.DBRow;

public interface CartWaybillIFace {
	public void putToWaybillCart(HttpServletRequest request)//放入购物车
	throws Exception;
	
	public void removeProduct(HttpServletRequest request)//移除购物车
	throws Exception;
	
	public void clearCart(HttpSession session)//清空session
	throws Exception;
	
	public float getCartWeight(HttpSession session)//获得运单购物车重量
	throws Exception;
	
	public void flush(HttpSession session)//加载运单购物车
	throws Exception;
	
	public DBRow[] getDetailProduct()
	throws Exception;
}
