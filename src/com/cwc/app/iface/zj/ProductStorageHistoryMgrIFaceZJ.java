package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.quote.WareHouseErrorException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductStorageHistoryMgrIFaceZJ {
	public void saveProductStorageHistroy()//记录当天库存历史
	throws Exception;
	
	public DBRow[] getAllProductStorageHistory(PageCtrl pc) //查询库存历史
	throws Exception;
	public DBRow[] getProductStorageHistoryByPsid(String st,String en,String p_name,long psId , PageCtrl pc) //高级查询
	throws Exception;
	
	public String exportProductStorageHistory(HttpServletRequest request) //导出库存历史记录
	throws WareHouseErrorException,Exception;
	
}


