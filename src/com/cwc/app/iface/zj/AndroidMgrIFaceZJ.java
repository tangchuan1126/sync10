package com.cwc.app.iface.zj;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;

public interface AndroidMgrIFaceZJ {
	public byte[] downLoadProductStoreLocationBaseData(String xml,String machine)
	throws Exception;
	
	public String getProductStoreLocationBySlc(String xml,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public String storageTakeStockApprove(String xml,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public DBRow[] storageTakeStockDifferents(String xml,AdminLoginBean adminBean)
	throws Exception;
	
	public DBRow[] storageTakeStockDifferents(long area_id)
	throws Exception;
	
	public int storageTakeStockApprove(long area_id,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public void takeStoreStorageLocation(String floorContainers,long slc_id)
	throws Exception;
}
