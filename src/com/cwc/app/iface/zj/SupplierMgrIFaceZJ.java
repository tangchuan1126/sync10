package com.cwc.app.iface.zj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface SupplierMgrIFaceZJ {
	
	public DBRow supplierLogin(HttpServletRequest request)
	throws Exception;
	
	public void modSupplierPwd(HttpServletRequest request)
	throws Exception;
	
	public void passwordInit()
	throws Exception;
}
