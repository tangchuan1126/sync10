package com.cwc.app.iface.zj;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.product.ProductCantBeSetException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductHasRelationStorageException;
import com.cwc.app.exception.product.ProductInUnionException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductUnionIsInSetException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductMgrIFaceZJ 
{
	public String exportProduct(HttpServletRequest request)//导出
	throws Exception;
	
	public String[] importProduct(HttpServletRequest request)//导入
	throws Exception;
	
	public void productByUpload(HttpServletRequest request)//上传商品文件修改商品
	throws Exception;
	
	public DBRow[] exportProductShow(long catalog_id,long pro_line_id,int union_flag,PageCtrl pc)//导出商品检索显示
	throws Exception;
	
	public String exportProductBarcode(HttpServletRequest request)//条码导出
	throws Exception;
	
	public String importProductBarcode(HttpServletRequest request)//条码导入
	throws Exception,FileTypeException;
	
	public DBRow[] productBarcode(String path)//条码导入后预览
	throws Exception;
	
	public String exportProductBarcodeByPurchase(HttpServletRequest request)//根据采购单导出条码
	throws Exception;
	
	public DBRow[] getProductsByPurchaseId(long purchase_id,PageCtrl pc)//根据采购单ID获得采购单内商品
	throws Exception;
	
	public DBRow[] searchProductsByPurchaseIdKey(long purchase_id,String key,PageCtrl pc)//在采购单商品内检索
	throws Exception;
	
	public void modProductWithJqgrid(HttpServletRequest request)//修改商品grid
	throws Exception;
	//接口：修改商品
	public void modProductSub(long pc_id,String p_name,String p_code,long catalog_id,String unit_name,double unit_price,float length,float width,float heigth,float weight, int sn_size ,AdminLoginBean adminLoggerBean)	//张睿添加SN_size
	throws ProductCodeIsExistException,ProductNameIsExistException,Exception;
	
	//添加商品基础数据  接口
	public long gridAddProductSub(String p_name,String p_code,long catalog_id,String unit_name,double unit_price,float length,float width,float heigth,float weight,int sn_size,AdminLoginBean adminLoggerBean)
	throws ProductCodeIsExistException,ProductNameIsExistException,Exception;
	
	public void gridAddProduct(HttpServletRequest request)//使用grid添加商品
	throws ProductCodeIsExistException,ProductNameIsExistException,Exception;
	//接口：删除商品
	public void gridDelProductSub(long pcid) throws ProductInUnionException,ProductHasRelationStorageException,Exception;
	
	public void gridDelProduct(HttpServletRequest request)//使用grid删除商品
	throws ProductInUnionException,ProductHasRelationStorageException,Exception;
	
	public void modUnionWithJqgrid(HttpServletRequest request)//修改套装关系grid
	throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception;
	//接口：修改套装关系
	public void modUnionWithJqgridSub(String p_name,long set_pid,long pid,float quantity,double unit_price,float weight,String unit_name,String p_code,AdminLoginBean adminLoggerBean)
	throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception;
	
	public DBRow[] getUnionProduct(HttpServletRequest request)//根据配件ID获得套装信息
	throws Exception;
	
	//接口：删除商品套装关系
	public void delProductUnionSub(long set_pid,long pid,AdminLoginBean adminLoggerBean)throws Exception;
	public void delProductUnion(HttpServletRequest request)
	throws Exception;
	
	public HashMap<String,DBRow[]> importCheckProduct(String filename,int checkType)//数据库检查文件错误
	throws Exception;
	
	public void importDB(HttpServletRequest request)//数据库检查导入
	throws Exception;
	
	public void delImportDB(String filename)//取消导入删除临时文件
	throws Exception;
	
	public void delAllImportDB()//删除全部导入数据
	throws Exception;
	
	public void addedProductStorage(long ps_id)//补全商品库存信息
	throws Exception;
	
	public DBRow[] filterProduct(long catalog_id, long product_line_id, int union_flag, int product_file_types, int product_upload_status, PageCtrl pc)//过滤商品
	throws Exception;
	
	public DBRow[] getProvinceByCountryCode(HttpServletRequest request)//根据国家编码获得省份列表
	throws Exception;
	
	//接口：修改商品分类 
	public void modProductCatalogSub(long pc_id,long catalog_id)
	throws Exception;
	public DBRow modProductCatalog(HttpServletRequest request)//修改商品分类
	throws Exception;
	
	public DBRow[] getDetailProductLikeSearch(String key,PageCtrl pc)//模糊查询商品模糊商品名货商品条码
	throws Exception;
	
	public DBRow[] getLackingProduct(long pc_id,long ps_id,float count)
	throws Exception;
	
	public DBRow[] splitAdviceProduct(long[] pc_ids,long ps_id,long waybill_id)
	throws Exception;
	
	public DBRow[] getLackingProductByPsid(long[] pids)
	throws Exception;
	
	public void returnProduct(HttpServletRequest request)
	throws Exception;
	
	public String exportProductUnion(HttpServletRequest request)
	throws Exception;
	
	public long addImportProductApprove(HttpServletRequest request)//上文件检查后确认申请审核
	throws Exception;
	
	public DBRow[] filterImportProductApprove(long import_adid,long approve_adid,String sort_colume,String sort_type,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getImportProductByIpaId(long ipa_id)
	throws Exception;
	
	public DBRow[] getImportUnionByIpaId(long ipa_id)
	throws Exception;
	
	public DBRow[] getImportProductUnionByIpaIdWithSetpid(long ipa_id,long set_pid)
	throws Exception;
	
	public void approveImportProduct(HttpServletRequest request)
	throws Exception;
	
	public String exportProductFile(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getProductFileByPcid(long pc_id, boolean isBarrierPurchase, boolean isBarrierTransport)
	throws Exception;
	
	public void initVolume()//初始化一次体积
	throws Exception;
	
	public DBRow[] getLackingProductByPs(long ps_id,long product_line_id,long catalog_id,int[] status,String p_name,PageCtrl pc)//获得缺货商品
	throws Exception;
	
	public void updatePairToItem()
	throws Exception;
	
	public DBRow[] adviceNeedProductUnion(long [] pc_ids,long set_pid)//获得建议拆散商品内拥有散件数量
	throws Exception;
	
	public void splitProductForWaybill(HttpServletRequest request)//运单拆货
	throws Exception;
	
	public DBRow[] splitAdviceBackUpProduct(long[] pc_ids,long ps_id,long waybill_id)//不完整套装建议
	throws Exception;
	
	public void updateProductByDBRow(DBRow row ,long id)	//修改machine
	throws Exception ;
	/**
	 * 查询套装商品所含的子商品数量
	 * @param set_pid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月7日 下午12:20:27
	 */
	 public int getProductUnionCountByPid(long set_pid) throws Exception;
	 
	 public void updateCustomerTitleAndCategoryLineRelation(long pcId,long categoryId) throws Exception;
}
