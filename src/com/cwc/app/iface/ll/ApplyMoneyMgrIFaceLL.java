package com.cwc.app.iface.ll;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ApplyMoneyMgrIFaceLL {
	public DBRow getRowById(String id) throws Exception;
	public DBRow insertRow(DBRow row) throws Exception;
	public DBRow[] getMoneyAssociationTypeAll() throws Exception;
	public DBRow getMoneyAssociationTypeById(String id) throws Exception;
	public boolean checkApplyMoneyAssociationById(String className, String id, int type) throws Exception;
	public DBRow[] getAllByTable(String tableName) throws Exception;
	public DBRow[] getAllByReadView(String readViewName) throws Exception;
	public DBRow[] getApplyTransferByBusiness(String businessId,int associationTypeId) throws Exception;
	public DBRow[] getApplyMoneyByBusiness(String businessId,int associationTypeId) throws Exception;
	public DBRow[] getApplyTransferByApplyMoney(String id) throws Exception;
	public DBRow insertApplyMoney(HttpServletRequest request) throws Exception;
	public String uploadImage(HttpServletRequest request) throws Exception;
	public List getImageList(String association_id, String association_type) throws Exception;
	public void deleteImage(HttpServletRequest request) throws Exception;
	public DBRow getApplyMoneyCreate(String user_id) throws Exception;
	public void remark(HttpServletRequest request) throws Exception;
	public DBRow insertApplyMoneyByTransport(HttpServletRequest request) throws Exception;
	public DBRow[] getApplyMoneyByIds(String ids, PageCtrl pc) throws Exception;
	public DBRow[] getApplyTransferByIds(String ids, PageCtrl pc) throws Exception;
	public String uploadImageApplyMoneyNoAssociate(HttpServletRequest request) throws Exception;
	public String uploadOldImage(HttpServletRequest request) throws Exception;
}
