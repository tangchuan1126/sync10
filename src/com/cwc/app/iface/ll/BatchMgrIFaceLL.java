package com.cwc.app.iface.ll;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface BatchMgrIFaceLL {
	public DBRow getBatchByBatchId(long batch_id) throws Exception;
	public DBRow[] getBatchByBatchId(long batch_id, String search_key, int business_type, PageCtrl pc) throws Exception;
	public DBRow[] getBatchDetailByBatchId(long batch_id, long ps_id, String pc_name, PageCtrl pc) throws Exception;
	public DBRow[] getBatchDetailByBatchDetailId(long batch_detail_id, PageCtrl pc) throws Exception;
	public DBRow[] getBatchchildDetails(long batch_detail_id, PageCtrl pc) throws Exception;
	public DBRow[] getBatchRelationByBatchId(long batch_id, long batch_child_detail_id, PageCtrl pc) throws Exception;
	public DBRow getBatchRelationByRelationId(long relation_id) throws Exception;
	public void batchDetailSaveOrUpdate(HttpServletRequest request) throws Exception;
	public void batchRelationSaveOrUpdate(HttpServletRequest request) throws Exception;
	public void batchDeliveryOrderStockin(long delivery_order_id, AdminLoginBean adminLoggerBean) throws Exception;
	public long batchAssemblyProduct(long batch_id, long ps_id, long set_pc_id, float assembly_count, long adid) throws Exception;
	public void batchSplitProduct(long ps_id, long pc_id, float split_quantity, long adid) throws Exception;
	public void batchDamagedProduct(long ps_id, long pc_id, float damaged_count, long adid) throws Exception;
	public void batchConvertProduct(long ps_id, long old_pc_id, long new_pc_id, float convert_count, long adid) throws Exception;
	public void batchStorageApprove(long approve_id, long adid) throws Exception;
	public void batchRenovateProduct(long ps_id, long pc_id, float renovate_count, long adid) throws Exception;
	public void batchTransportStockOut(long transport_id, AdminLoginBean adminLoggerBean) throws Exception;
	public void batchStockOutDelete(long business_id, int business_type) throws Exception;
	public void initStock(HttpServletRequest request) throws Exception;
	public DBRow[] getBatchDetailSumByBatchDetailId(long batch_detail_id, PageCtrl pc) throws Exception;
	public DBRow[] getBatchDetailSumByBatchId(long batch_id, long ps_id, String pc_name, PageCtrl pc) throws Exception;
	public void batchDeliveryStockInChangeFreight(long delivery_order_id, AdminLoginBean adminLoggerBean) throws Exception;
	public void batchReturnProduct(long batch_id, long return_pc_id, float return_pc_count, long ps_id, AdminLoginBean adminLoggerBean) throws Exception;
	public boolean ifTransportSplit(long batch_id, long ps_id, long pc_id) throws Exception;
	public float getApplyMoneyFreightCost(int association_type_id, long association_id) throws Exception;
	public DBRow[] getBatchStockIn1(long batch_id, PageCtrl pc) throws Exception;
	public DBRow[] getBatchStockIn2(long batch_id, PageCtrl pc) throws Exception;
	public DBRow[] getBatchStockOut1(long batch_id, PageCtrl pc) throws Exception;
	public DBRow[] getBatchTransportSplit(long batch_id, PageCtrl pc) throws Exception;
	public List<DBRow> getBatchHistoryForward(long batch_id, long pc_id, long ps_id, float batch_cost) throws Exception;
	public List<DBRow> getBatchHistoryBack(long batch_id, long pc_id, long ps_id, float batch_cost) throws Exception;
}
