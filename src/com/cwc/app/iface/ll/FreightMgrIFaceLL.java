package com.cwc.app.iface.ll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface FreightMgrIFaceLL {
	public DBRow[] getAllFreightResources() throws Exception;
	public DBRow[] getFreightResourcesByCompany(String company,PageCtrl pc) throws Exception;
	public DBRow getCountryById(String id) throws Exception;
	public DBRow[] getCompanys() throws Exception;
	public DBRow[] getDeliveryFreightCostByDeliveryId(String delivery_id) throws Exception;
	public DBRow[] getTransportFreightCostByTransportId(String transport_id) throws Exception;
	public DBRow[] getFreightCostByfrId(String ft_id) throws Exception;
	public DBRow transportSetFreight(HttpServletRequest request) throws Exception;
	public DBRow[] getAllFreightCost() throws Exception;
	
	public void transportSetFreight(long transport_id,HttpServletRequest request) 
	throws Exception; 
	
	public void updateTransportShippingFeeToProductStoreLogDetail(long transport_id)//更新转运与交货单运费到批次
	throws Exception;
}
