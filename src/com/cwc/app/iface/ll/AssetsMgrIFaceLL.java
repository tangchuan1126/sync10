package com.cwc.app.iface.ll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface AssetsMgrIFaceLL {
	public DBRow[] search(HttpServletRequest request,PageCtrl pc) throws Exception;
	public DBRow[] filter(HttpServletRequest request,PageCtrl pc) throws Exception;
}
