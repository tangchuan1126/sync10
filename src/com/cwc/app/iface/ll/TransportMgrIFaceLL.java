package com.cwc.app.iface.ll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface TransportMgrIFaceLL {
	public DBRow getTransportById(String id) throws Exception;
	public void updateTransport(HttpServletRequest request) throws Exception;
	public void freightTransport(HttpServletRequest request) throws Exception;
	public void wayoutTransport(HttpServletRequest request) throws Exception;
	public DBRow[] getCounty() throws Exception;
	public DBRow getCountyById(String id) throws Exception;
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type) throws Exception;
	public DBRow[] getTransportLogsByType(long transport_id, int transport_type) throws Exception;
	public void transportLogsInsert(HttpServletRequest request) throws Exception;
	public void transportRemark(HttpServletRequest request) throws Exception;
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception;
	public void transportAproveCancel(String transport_approve_id,AdminLoginBean adminLoggerBean) throws Exception;
	public DBRow[] getTransportDetailByTransportId(String transport_id) throws Exception;
	public void transportOutboundAproveCancel(String transport_approve_id,AdminLoginBean adminLoggerBean) throws Exception;
	public DBRow getDrawback(String transport_id) throws Exception;
	public void transportSetDrawback(HttpServletRequest request) throws Exception;
	public DBRow[] getTransportLogsByTransportId(long transport_id) throws Exception;
	public DBRow[] getTransportLogsByTransportIdAndType(long transport, int[] types) throws Exception;
}
