package com.cwc.app.iface.ll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface StorageIFaceMgrLL {
	public String downloadStorageCost(HttpServletRequest request) throws Exception;
	public DBRow[] checkImportStorageAlert(String filename, String storage_title) throws Exception;
	public void saveImportStorageCost(HttpServletRequest request) throws Exception;
	public DBRow[] getCostLog(long ps_id, String p_name, PageCtrl pc) throws Exception;
} 
