package com.cwc.app.iface.ll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface DeliveryMgrIFaceLL {
	public DBRow getDeliveryOrderById(String id) throws Exception;
	public void updateDeliveryOrder(HttpServletRequest request) throws Exception;
	public void freightDeliveryOrder(HttpServletRequest request) throws Exception;
	public void wayoutDeliveryOrder(HttpServletRequest request) throws Exception;
	public DBRow[] getCounty() throws Exception;
	public DBRow[] getDeliveryOrderLogsByType(long delivery_order_id, int delivery_order_type) throws Exception;
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type) throws Exception;
	public void deliveryLogsInsert(HttpServletRequest request) throws Exception;
	public void deliveryRemark(HttpServletRequest request) throws Exception;
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception;
	public void deliveryStockTempIn(HttpServletRequest request) throws Exception;
	public void deliveryAproveCancel(String da_id,AdminLoginBean adminLoggerBean) throws Exception;
	public DBRow[] getDeliveryOrderDetailByDeliveryOrderId(String delivery_order_id) throws Exception;
	public DBRow getProductById(String product_id) throws Exception;
	public void deliveryFreightFinish(HttpServletRequest request) throws Exception;
	public void deliverySetDrawback(HttpServletRequest request) throws Exception;
	public DBRow getDrawback(String delivery_order_id) throws Exception;
}
