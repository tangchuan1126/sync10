package com.cwc.app.iface.ll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface PurchaseMgrIFaceLL {
	public void updatePurchase(HttpServletRequest request) throws Exception;
	public DBRow getPurchaseById(String id) throws Exception;
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type) throws Exception;
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type, int typeSub) throws Exception;
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception;
}
