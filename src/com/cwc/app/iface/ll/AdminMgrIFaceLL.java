package com.cwc.app.iface.ll;

import com.cwc.db.DBRow;

public interface AdminMgrIFaceLL {
	public DBRow getAdminById(String id) throws Exception;
}
