package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductNotProfitException;
import com.cwc.db.DBRow;

public interface CartQuoteIFace 
{
	public void put2Cart(HttpSession session,long pid,float quantity,int product_type,int price_type,long ps_id) 
	throws  Exception;
	
	public DBRow[] getSimpleProducts(HttpSession session) throws Exception;
	
	public void clearCart(HttpSession session);
	
	public boolean isEmpty(HttpSession session) throws Exception;
	
	public void removeProduct(HttpSession session,long pid,int product_type)
	throws Exception;
	
	public void modQuantity(HttpSession session,String pids[],String quantitys[],String product_type[],String quote_prices[],String price_type[]) 
	throws  Exception;
	
	public void flush(HttpSession session,String client_id,long sc_id,long ccid,long pro_id,long ps_id)
	throws Exception;
	
	public void put2Cart(HttpServletRequest request)
	throws ProductNotProfitException,ProductNotCreateStorageException,ProductNotExistException,Exception;
	
	public DBRow[] getDetailProducts();
	
	public float getSumQuantity();
	
	public boolean isHasLackingProduct();
	
	public void convert2CustomProduct(HttpSession session,long old_pid,long new_pid) 
	throws  Exception;
	
	public double getProductFee();
	
	public double getShippingFee();
	
	public double getTotalPrice();
	
	public void initCart(HttpSession session,long pid,float quantity,int product_type,float manager_discount,int price_type,double quote_price) 
	throws  Exception;
	
	public void modifyCartManagerDiscount(HttpSession session,float discount) 
	throws  Exception;
	
	public float getTotalWeight();
	
	public double getFinalDiscount();
	
	public double getSystemDiscount();
	
	public double getSaving();
	
	public double getSumSpecialPrice();
	
	public String getOrderCatalog();
	
	public void updateSessionPsId(HttpSession session , long ps_id) throws Exception;
}
