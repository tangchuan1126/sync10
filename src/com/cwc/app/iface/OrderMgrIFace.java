package com.cwc.app.iface;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.client.OrderNotExistException;
import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.exception.order.DeliveryInfoBeUsedException;
import com.cwc.app.exception.order.DeliveryStorageNotSameException;
import com.cwc.app.exception.order.InvoiceTemplateBeUsedException;
import com.cwc.app.exception.order.LostMoneyException;
import com.cwc.app.exception.order.MergeOrderNotCancelException;
import com.cwc.app.exception.order.MergeOrderNotExistException;
import com.cwc.app.exception.order.MergeOrderNotSelfException;
import com.cwc.app.exception.order.MergeOrderStatusConflictException;
import com.cwc.app.exception.order.MergeOrderStatusIncorrectException;
import com.cwc.app.exception.order.MergeOrderStorageNotMatchException;
import com.cwc.app.exception.order.NotNormalOrderException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.service.order.action.OrderCostBean;

public interface OrderMgrIFace 
{
	public long addPOrder(HttpServletRequest request) throws Exception;
	
	public DBRow getDetailPOrderByTxnid(String id) throws Exception;
	
	public DBRow[] getAllOrders(PageCtrl pc) throws Exception;
	
//	public DBRow[] getSearchOrdersByField(String field,String value,PageCtrl pc) throws Exception;
	
	public DBRow[] colorFilter(DBRow data[]) throws Exception;
	
	public void markDeliver(HttpServletRequest request) throws Exception;
	
	public DBRow[] getOrdersByFilter(HttpServletRequest request,PageCtrl pc) throws Exception;
	
	public void modPOrderByChangePaymentStatus(HttpServletRequest request)	throws Exception;
	
	public void modPOrderNote(HttpServletRequest request) throws Exception;
	
	public void modPOrderHandleStatus(HttpServletRequest request) throws Exception;
	
//	public DBRow[] getSearchOrdersByKeyLike(String field,String value,PageCtrl pc) throws Exception;
	
	public int recordOrder(HttpServletRequest request) throws Exception;
	
	public void modPOrderProductInfo(HttpServletRequest request) throws Exception;
	
	public void modPOrderDeliveryInfo(HttpServletRequest request) throws Exception;
	
	public DBRow[] getSearchOrders(int search_field,String value,int search_mode,PageCtrl pc) throws Exception;
	
	public DBRow getDetailPOrderByOid(long oid) throws Exception;
	
	public void printUpdateEMS(HttpServletRequest request) throws Exception;
	
	public void printUpdateHandle(HttpServletRequest request) throws Exception;
	
	public void printUpdateHandleStatus(HttpServletRequest request) throws Exception;
	
	public void delOrder(HttpServletRequest request) throws Exception;
	
	public long addPOrderByHandle(HttpServletRequest request) throws Exception;
	
	public DBRow[] getRelationOrders(String txn_id) throws Exception;
	
	public String uploadOrderAppend(HttpServletRequest request,String filename) throws Exception;
	
	public void delOrderAppend(HttpServletRequest request) throws Exception;

	public DBRow getStatOrdersCount(String st,String en,String business,int product_type_int)	throws Exception;
	
	public DBRow getStatNormalOrdersCount(String st,String en,String business,int product_type_int)	throws Exception;
	
	public DBRow getStatPendingOrdersCount(String st,String en,String business,int product_type_int)	throws Exception;
	
	public DBRow getStatArchiveOrdersCount(String st,String en,String business,int product_type_int)	throws Exception;
	
	public DBRow getStatPrintedOrdersCount(String st,String en,String business,int product_type_int)	throws Exception;
	
	public DBRow getStatDeliveryOrdersCount(String st,String en,String business,int product_type_int)	throws Exception;
	
	public DBRow[] getTimeRangeOrders(String st,String en,String business,int product_type_int)	throws Exception;
	
	public DBRow getStatNotPrintOrdersCount(String st,String en,String business,int product_type_int)	throws Exception;
	
	public int getOrderCountByClientId(String client_id)	throws Exception;
	
	public double getSumMcGrossByClientId(String client_id)	throws Exception;
	
	public DBRow[] getAllInvoiceTemplates()	throws Exception;
	
	public void addDelivererInfo(HttpServletRequest request)	throws Exception;
	
	public void modDelivererInfo(HttpServletRequest request)	throws Exception;
	
	public void delDelivererInfo(HttpServletRequest request)	throws DeliveryInfoBeUsedException,Exception;
	
	public DBRow[] getAllDelivererInfo()	throws Exception;
	
	public DBRow getRandomDelivererInfo(long rand_group)	throws Exception;
	
	public void modInvoiceTemplates(HttpServletRequest request)	throws Exception;
	
	public DBRow getDetailDelivererInfo(long di_id)	throws Exception;
	
	public DBRow getDetailInvoiceTemplate(long invoice_id)	throws Exception;
	
	public void updateOrderSource()	throws Exception;
	
	public HashMap getAllCountryCodeHM()	throws Exception;
	
	public DBRow[] getAllCountryCode()	throws Exception;
	
	public DBRow getDetailCountryCodeByCcid(long ccid)	throws Exception;
	
	public DBRow getDetailCountryCodeByCountry(String country)	throws Exception;
	
	public void convertCountryCode() 	throws Exception;
	
	public void addPOrderNote(HttpServletRequest request)	throws Exception;
	
	public DBRow[] getPOrderNoteByOid(long oid)	throws Exception;
	
	public void modDeliveryCountry(HttpServletRequest request)	throws Exception;
	
	public DBRow getDetailDelivererInfo(HttpServletRequest request)	throws Exception;
	
	public void addInvoiceTemplates(HttpServletRequest request)	throws Exception;
	
	public void delInvoiceTemplatesByInvoiceId(HttpServletRequest request)	throws InvoiceTemplateBeUsedException,Exception;
	
	public DBRow[] getPOrderItemsByOid(long oid)	throws Exception;
	
	public void initCartFromOrder(HttpServletRequest request,long oid)	throws Exception;
	
	public void mergeOrder(HttpServletRequest request)	throws MergeOrderStorageNotMatchException,MergeOrderStatusConflictException,MergeOrderNotSelfException,MergeOrderNotExistException,MergeOrderStatusIncorrectException,Exception;
	
	public DBRow[] getSonOrderItemsByParentOid(long parent_oid)	throws Exception;
	
	public void undoMergeOrder(HttpServletRequest request)	throws Exception;

	public DBRow[] getSonOrders(long parent_oid,PageCtrl pc)	throws Exception;
	
	public boolean validateMergeorderDeliveryInfo(long parent_oid,long oid)	throws Exception;
	
	public void cancelOrder(HttpServletRequest request)	throws MergeOrderNotCancelException,Exception;
	
	public void archiveOrder(HttpServletRequest request)	throws Exception;
	
	public DBRow getDetailPOrderNoteByOnid(long on_id)	throws Exception;
	
	public DBRow[] getWait4PrintOrders(long ps_id,String business,long product_type,String st,String en,PageCtrl pc)	throws Exception;
	
	public DBRow[] getStatLeavingStorageOrderProductsByDatePsidPscidCatalogid(String print_date,long psid,long pscid,long catalog_id)	throws Exception;
	
	public DBRow[] getStatLeavingStorageOrderPCByDatePsidPscid(String print_date,long psid,long pscid)	throws Exception;
	
	public DBRow[] getStatLeavingStorageOrderPCByDatePsidPscidView(String print_date,long psid,long pscid)	throws Exception;
	
	public DBRow[] getStatLeavingStorageOrderProductsByDatePsidPscidCatalogidView(String print_date,long psid,long pscid,long catalog_id)	throws Exception;
	
	public DBRow[] getStatLackingProductsPCByPscid(long pscid)	throws Exception;
	
	public DBRow[] getStatLackingProductsByPscidCatalogid(long pscid,long catalog_id) throws Exception;
	
	public int getWait4PrintOrdersCount(long ps_id,String st,String en)	throws Exception;
	
	public DBRow[] getInvoiceTemplatesByPsId(long ps_id)	throws Exception;
	
	public boolean checkOrderPrinted(HttpServletRequest request)	throws Exception;
	
	public DBRow[] getProfileWait4HandleOrders(String st,String en)	throws Exception;

	public DBRow[] getProfileLackingOrders(String st,String en,long ps_id)	throws Exception;
	
	public DBRow[] getProfileWait4PrintOrders(String st,String en,long ps_id)	throws Exception;
	
	public DBRow[] getProfileDoubtOrders(String st,String en,long ps_id)	throws Exception;
	
	public DBRow[] getProfilePrintedOrders(String st,String en,long ps_id)	throws Exception;
	
	public DBRow[] getProfileDate(String st,String en)	throws Exception;
	
	public DBRow getDataByDate(DBRow rows[]);
	
	public void updateDeliveryDate(int year,int month)	throws Exception;
	
	public OrderCostBean getOrderCost(long oid,long sc_id,long ccid)	throws ProvinceOutSizeException,WeightOutSizeException,WeightCrossException,CountryOutSizeException,Exception;
	
	public float compareOrderCost(long oid,long sc_id,long ccid)	throws WeightOutSizeException,WeightCrossException,CountryOutSizeException,LostMoneyException,Exception;
	
	public void markDoubtCost(HttpServletRequest request)	throws Exception;
	
	public void verifyOrderCost(HttpServletRequest request)	throws Exception;
	
	public void updateNotDeliverySonOrders(HttpServletRequest request)	throws Exception;
	
	public DBRow[] getProfileDoubtAddressOrders(String st,String en,long ps_id)	throws Exception;
	
	public DBRow[] getProfileDoubtCostOrders(String st,String en,long ps_id)	throws Exception;
	
	public DBRow[] getProfileDoubtPayOrders(String st,String en,long ps_id)	throws Exception;
	
	public void addPOrderNotePrivate(long oid,String note,int trace_type,HttpSession ses,long rel_id)	throws Exception;
	
	public void validateTraceService(long role_id,long oid,int trace_type)	throws Exception;
	
	public int getTraceDoubtOrderCount(String st,String en,int tracking_day)
	throws Exception;
	
	public int getTraceDoubtAddressOrderCount(String st,String en,int tracking_day)
	throws Exception;
	
	public int getTraceDoubtPayOrderCount(String st,String en,int tracking_day)
	throws Exception;
	
	public int getTraceLackingOrderCount(long trace_operator_role_id)
	throws Exception;
	
	public DBRow[] getTraceDoubtOrders(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getTraceDoubtAddressOrders(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getTraceDoubtPayOrders(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getTraceLackingOrders(long trace_operator_role_id,PageCtrl pc)
	throws Exception;
	
	public boolean validateUSAZipCode(String zipCode);
	
	public void printVerifyAddress(HttpServletRequest request)
	throws Exception;
	
	public float getOrderWeightByOid(long oid)
	throws Exception;
	
	public DBRow getMonitorTraceCount(HttpServletRequest request)
	throws Exception;
	
	public int getVerifyCostOrderCount(long trace_operator_role_id)
	throws Exception;
	
	public DBRow[] getVerifyCostOrders(long trace_operator_role_id,PageCtrl pc)
	throws Exception;
	
	public DBRow getDetailOrderDeliveryInfoByOid(long oid)
	throws Exception;
	
	public DBRow[] getTraceDelay(PageCtrl pc)
	throws Exception;
	
	public void hangUpLongDoubtOrder(HttpServletRequest request)
	throws Exception;
	
	public boolean hasBeCreditVerify(long oid)
	throws Exception;
	
	public void updateOldOrderShippingWays(long ps_id,int year,int month)
	throws Exception;
	
	public void updateOldOrderCost(int month)
	throws Exception;
	
	public DBRow[] getOrderLackingProductListByOid(long oid,PageCtrl pc)
	throws Exception;
	
	public int getPrintDoubtOrderCount(long trace_operator_role_id)
	throws Exception;
	
	public DBRow[] getTracePrintDoubtOrders(long trace_operator_role_id,PageCtrl pc)
	throws Exception;
	
	public void additionMoneyOrder(HttpServletRequest request)
	throws NotNormalOrderException,OrderNotExistException,Exception;
	
	public void unDoAdditionMoneyOrder(HttpServletRequest request)
	throws Exception;
	
	public long markNormalRefunding(HttpServletRequest request)
	throws Exception;
	
	public void markDisputeRefunding(HttpServletRequest request)
	throws Exception;
	
	public void markPartRefunded(HttpServletRequest request)
	throws Exception;
	
	public void markAllRefunded(HttpServletRequest request)
	throws Exception;
	
	public long markNormalWarrantying(HttpServletRequest request)
	throws Exception;
	
	public void markDisputeWarrantying(HttpServletRequest request)
	throws Exception;
	
	public void markWarrantied(HttpServletRequest request)
	throws OrderNotExistException,Exception;
	
	public void markOtherDispute(HttpServletRequest request)
	throws Exception;
	
	public void cancelRefund(HttpServletRequest request)
	throws Exception;
	
	public void cancelWarranty(HttpServletRequest request)
	throws Exception;
	
	public void cancelDispute(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getTraceDisputeRefundOrders(long trace_operator_role_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getTraceDisputeWarrantyOrders(long trace_operator_role_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getTraceOtherDisputeOrders(long trace_operator_role_id,PageCtrl pc)
	throws Exception;
	
	public int getDisputeRefundCount(long trace_operator_role_id)
	throws Exception;
	
	public int getDisputeWarrantyCount(long trace_operator_role_id)
	throws Exception;
	
	public int getOtherDisputeCount(long trace_operator_role_id)
	throws Exception;
	
	public void markFailureRefund(HttpServletRequest request)
	throws Exception;
	
	public long markFreeWarranty(HttpServletRequest request)
	throws Exception;
	
	public void markFailureWarranty(HttpServletRequest request)
	throws Exception;
	
	public void markFailureDispute(HttpServletRequest request)
	throws Exception;
	
	
	public void markBadRefund(HttpServletRequest request)
	throws Exception;
	
	public void markBadWarranty(HttpServletRequest request)
	throws Exception;
	
	public long markFeeShippingFeeWarranty(HttpServletRequest request)
	throws Exception;
	
	public void markBadDispute(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getProfileDisputeRefundOrders(String st,String en,long ps_id)
	throws Exception;
	
	public DBRow[] getProfileOtherDisputingOrders(String st,String en,long ps_id)
	throws Exception;
	
	public DBRow[] getProfileDisputeWarrantyOrders(String st,String en,long ps_id)
	throws Exception;

	public void checkDeliveryStorage(HttpServletRequest request)
	throws DeliveryStorageNotSameException,Exception;
	
	public String getHsCode(long oid)
	throws Exception;
	
	public void updateOldOrderCostTMP()
	throws Exception;
	
	public DBRow[] getSonOrderDeliveryNoteByParentOid(long parent_oid)
	throws Exception;
	
	public double getOrgOrderTotalMcGross(long oid)
	throws Exception;
	
	public double getUSDOrderTotalMcGross(long oid)
	throws Exception;

	public long addPOrderByHandle4LoadTest(HttpServletRequest request)
	throws Exception;
	
	public void modPOrderDeliveryInfo4LoadTest(HttpServletRequest request)
	throws Exception;
	
	public float calculateWayBillWeight(long sc_id,float weight) 
	throws Exception;
	
	public void modAddressZip(long oid,String address_zip)
	throws Exception;
	
	public long addReturnProducts(HttpServletRequest request,boolean flag)
	throws Exception;
	
	public DBRow[] getReturnProductItemsByRpId(long rp_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getReturnProductSubItemsByRpId(long rp_id,PageCtrl pc)
	throws Exception;
	
	public DBRow getDetailReturnProductByRpId(long rp_id)
	throws Exception;
	
	public void initCartReturn(HttpServletRequest request,long rp_id)
	throws Exception;
	
	public DBRow[] getAllReturnProduct(PageCtrl pc)
	throws Exception;
	
	public DBRow[] searchReturnProductByRpId(long rp_id,PageCtrl pc)
	throws Exception;
	
	public void sigReturnProducts(HttpServletRequest request)
	throws Exception;
	
	public void cancelReturn(HttpServletRequest request)
	throws Exception;
	               
	public DBRow[] getAllReturnProductReasons()
	throws Exception;
	
	public String getReturnProductReasonById(int id)
	throws Exception;
	
	public String getBadFeedBackReasonById(int id)
	throws Exception;
	
	public DBRow[] getAllBadFeedBackReasons()
	throws Exception;
	
	public void markBadFeedBack(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getBadFeedBacksByOid(long oid)
	throws Exception;
	
	public void cancelExpireReturnInfo()
	throws Exception;
	
	public void updateOrderScIdTMP()
	throws Exception;
	
	public void updateShippingScIdTMP()
	throws Exception;
	
	public void printUpdateWayBillNumber(HttpServletRequest request)
	throws Exception;
	
	public DBRow getRandomDelivererInfoByPsId(long ps_id)
	throws Exception;
	
	public float calculateWayBillWeight4ST(float weight) 
	throws Exception;
	
	public String getPrintedOrderReplyMail(String business)
	throws Exception;
	
	public void modSellerId(long oid,String seller_id)
	throws Exception;
	
	public void modBuyerId(long oid,String buyer_id)
	throws Exception;
	
	public void updateSellerID()
	throws Exception;
	
	public DBRow[] getAllCountrysRejPsid(long ps_id)
	throws Exception;
	
	public void modReturnProductWareHouse(HttpServletRequest request)
	throws Exception;
	
	public void updateAfterServiceStatus()
	throws Exception;
	
	public long addWarrantyOrder(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getAllWarrantyOrders(PageCtrl pc)
	throws Exception;
	
	public long saveWarrantyOrder(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getWarrantyOrderItemsByWoId(long woid)
	throws Exception;
	
	public DBRow[] getWarrantyOrdersByPayerEmail(String email,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getWarrantyOrdersByOid(long oid)
	throws Exception;
	
	public DBRow[] getWarrantyOrdersByWoid(long woid)
	throws Exception;
	
	public String getWarrantyReasonById(int id)
	throws Exception;
	
	public DBRow[] getAllWarrantyReasons()
	throws Exception;
	
	public void linkWarrantied(HttpServletRequest request)
	throws OrderNotExistException,Exception;
	
	public DBRow getDetailShipping(long oid)
	throws Exception;
	
	public double getProductMergeCost(double product_cost)
	throws Exception;
	
	public double getOrderMergeCost(String order_source,double product_cost,double shipping_cost)
	throws Exception;
	
	public void sendMail(long oid,long rp_id)
	throws Exception;
	
	public void newIndexByOid(long oid,String ems_id)//根据订单ID重建订单索引
	throws Exception;
	
	public OrderCostBean getNowOriginalOrderCost(long oid,long sc_id,long ccid)//根据订单计算商品成本运费成本（最新商品的重量价格现从商品表抓取）
	throws ProvinceOutSizeException,WeightOutSizeException,WeightCrossException,CountryOutSizeException,Exception;
	
	public int newRecordOrder(HttpServletRequest request) 
	throws Exception;
	
	public void recordOrderTrue(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] waitQuantityNotZero(long oid)//返回待发货数>0的订单明细
	throws Exception;
	
	public void orderLacking(HttpServletRequest request)//标记订单缺货
	throws Exception;
	
	public void printUpdateWayBillNumber(long oid,String tracking_number)
	throws Exception;
	
	public void orderShipment(long oid)//订单所有商品全上运单
	throws Exception;
	
	public void orderNoShipment(long oid)//订单非所有商品上运单
	throws Exception;
	
	public void changeOrderHandle(long oid)//运单操作后，修改订单的状态
	throws Exception;
	
	public DBRow getDetailOrderItemByIid(long iid)
	throws Exception;
	
	public DBRow[] statisticsOrder(String st,String en,String p_name,long product_line_id,long catalog_id,long ca_id,long ccid,long pro_id,int cost,int cost_type,int unfinished,PageCtrl pc)
	throws Exception;	
	
	public void addPOrderNotePrivate(long oid,String note,int trace_type,AdminLoginBean adminLoggerBean,long rel_id)
	throws Exception;
	
	public DBRow[] getDeliveryExceptionOrder(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getCustomExceptionOrder(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getNotYetDeliveryedOrder(PageCtrl pc)
	throws Exception;
}





