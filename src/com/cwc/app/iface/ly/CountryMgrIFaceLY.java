package com.cwc.app.iface.ly;

import com.cwc.db.DBRow;

/**
 * @author liuyi
 * @time:Aug 28, 2014
 * 功能说明：
 */
public interface CountryMgrIFaceLY
{
	/**
	 * 获取country_code 表所有数据 
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllCountry() throws Exception;
	 /**
     * 查询所有国家和省份
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2015年3月23日 下午6:22:56
     */
    public DBRow[] findCountryAndProvinces() throws Exception;
}
