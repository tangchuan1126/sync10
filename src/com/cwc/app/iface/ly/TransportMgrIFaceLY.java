package com.cwc.app.iface.ly;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface TransportMgrIFaceLY 
{
	
	/**
	 * 收货管理 默认查询( 查询到达本库，并且运输状态为 运输中 或已到货的)
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransport(long send_psid, long receive_psid,
			PageCtrl pc, int status, int declaration, int clearance,
			int invoice, int drawback, int day, int stock_in_set,
			long create_account_id) throws Exception;
	
	/**
	 * 过滤从本仓库发出的，或者到达本仓库的数据
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransportSelf(long self_psid,PageCtrl pc,int status,int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id)//过滤转运单
	throws Exception;
	
	/**
	 * 获取默认需要处理的单据(有 报关，清关，运费的流程)
	 * @param self_psid
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSelfDefaultTransport(long self_psid,PageCtrl pc,int status,int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id)
	throws Exception;
	
}
