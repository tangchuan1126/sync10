package com.cwc.app.iface;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.helpCenter.DoubleCatalogNameException;
import com.cwc.app.exception.helpCenter.DoubleTopicNameException;
import com.cwc.app.exception.helpCenter.NotBottomCatalogException;
import com.cwc.app.exception.helpCenter.ParentCatalogException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface HelpCenterIFace 
{
	public DBRow[] getHelpCenterCatalogTree()
	throws Exception;
	
	public DBRow[] getTopicsByCid(long cid,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getTopicsByCidSortAsc(long cid,PageCtrl pc)
	throws Exception;
	
	public DBRow getDetailTopicsByHTCID(long hct_id)
	throws Exception;
	
	public DBRow[] getCatalogByParentid(long parentid,PageCtrl pc)
	throws Exception;
	
	public long addCatalog(HttpServletRequest request)//添加帮助中心分类 
	throws DoubleCatalogNameException,Exception;
	
	public DBRow getDetailCatalog(HttpServletRequest request)//根据ID获得帮助中心分类
	throws Exception;
	
	public DBRow getDetailCatalog(long id)//根据ID获得帮助中心分类页面使用
	throws Exception;
	
	public void modCatalog(HttpServletRequest request) //修改帮助中心分类
	throws Exception;
	
	public void modCatalogSort(HttpServletRequest request)//修改帮助中心分类排序
	throws Exception;
	
	public void delCatalog(HttpServletRequest request)//删除帮助中心分类
	throws ParentCatalogException,Exception;
	
	public DBRow[] getTopic(long cid,PageCtrl pc)//获得文章列表
	throws Exception;
	
	public long addTopic(HttpServletRequest request)//添加文章
	throws DoubleTopicNameException,Exception;
	
	public String[] modTopicIssue(int issue,String ids,AdminLoginBean adminLoggerBean)//文章发布
	throws Exception;
	
	public String[] modTopicCancelIssue(int issue,String ids,AdminLoginBean adminLoggerBean)//文章取消发布
	throws Exception;
	
	public void delTopic(HttpServletRequest request)//删除文章
	throws Exception;
	
	public void modTopic(HttpServletRequest request)//修改文章
	throws DoubleTopicNameException,Exception;
	
	public void modTopicCatalog(HttpServletRequest request)//修改文章分类
	throws NotBottomCatalogException,Exception;
	
	public String uploadTopicImage(HttpServletRequest request,HttpServletResponse response)//上传文件
	throws Exception;
	
	public DBRow[] getSearchTopics4CT(String key,PageCtrl pc)//根据索引查询
	throws Exception;
	
	public DBRow[] getSearchTopics4CTUse(String key,PageCtrl pc)//根据索引查询（前台）
	throws Exception;
	
	public DBRow[] getTopicsUse(long cid,PageCtrl pc)//前台列表
	throws Exception;
}
