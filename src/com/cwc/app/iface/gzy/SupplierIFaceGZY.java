package com.cwc.app.iface.gzy;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface SupplierIFaceGZY {
	public DBRow[] getSupplierInfoList(PageCtrl pc) throws Exception;//获取所有供应商信息的接口
	public DBRow getDetailSupplier(long supplierid)throws Exception;//根据id获取供应商详细信息的接口
	public DBRow[] getSearchSupplier(String key, PageCtrl pc)throws Exception;//根据查询条件获取供应商信息的接口
	public void addSupplier(HttpServletRequest request) throws Exception;//增加供应商信息的接口
	public void modSupplier(HttpServletRequest request)	throws Exception;//修改供应商的接口
	public DBRow[] getSupplierInfoListByCondition(HttpServletRequest request,PageCtrl pc) throws Exception;//根据条件进行查询供应商信息
}
