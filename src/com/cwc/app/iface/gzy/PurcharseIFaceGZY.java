package com.cwc.app.iface.gzy;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface PurcharseIFaceGZY {
	public DBRow[] getSupplierByProductline(long productLineId)throws Exception;//根据产品线id获得供应商信息
	public long addPurchase(HttpServletRequest request)throws Exception;//生成采购单
}
