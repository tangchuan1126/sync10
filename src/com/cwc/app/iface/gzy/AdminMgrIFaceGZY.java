package com.cwc.app.iface.gzy;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.AccountOrPwdIncorrectException;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.app.exception.admin.OldPwdIncorrectException;
import com.cwc.app.exception.admin.RoleIsExistException;
import com.cwc.app.exception.admin.SuperAdminException;
import com.cwc.app.exception.admin.SuperRoleException;
import com.cwc.app.exception.admin.VerifyCodeIncorrectException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface AdminMgrIFaceGZY 
{
	public void addAdmin(HttpServletRequest request) throws AdminIsExistException,Exception;
	
	public void delAdmin(HttpServletRequest request) throws SuperAdminException,Exception;
	
	public void lockAdmin(HttpServletRequest request) throws SuperAdminException,Exception;
	
	public DBRow getDetailAdmin(long adid) throws Exception;
	
	public DBRow[] getAllAdmin(PageCtrl pc) throws Exception;
	
	public void unLockAdmin(HttpServletRequest request) throws Exception;
	
	public DBRow[] getAllAdminGroup(PageCtrl pc) throws Exception;
	
	public DBRow getDetailAdminGroup(long adgid) throws Exception;
	
	public DBRow getDetailAdminByAccount(String account) throws Exception;
	
	public void modAdminPwd(HttpServletRequest request)	throws OldPwdIncorrectException,Exception;
	
	public void adminLogin(HttpServletRequest request,HttpServletResponse response) throws AccountOrPwdIncorrectException,VerifyCodeIncorrectException,AccountNotPermitLoginException,Exception;
	
	public void adminLogin(HttpServletRequest request,HttpServletResponse response,String account,String pwd,String licence,int remember,int loginType,DBRow attach)	throws AccountOrPwdIncorrectException,VerifyCodeIncorrectException,AccountNotPermitLoginException,Exception;
	
	public void adminLogout(HttpServletRequest request) throws Exception;
	
	public void markLocal(HttpServletResponse response,HttpServletRequest request) throws Exception;
	
	public void isRightAdminPath(HttpServletResponse response,HttpServletRequest request) throws Exception;
	
	public DBRow[] getControlTreeByParentid(long parentid) throws Exception;
	
	public DBRow getDetailControlTreeByLink(String link) throws Exception;
	
	public DBRow[] getControlTree()	throws Exception;
	
	public ArrayList getPermitPageAdgid(String page) throws Exception;
	
	public ArrayList getPermitActionAdgid(String action) throws Exception;

	public void modifyRole(HttpServletRequest request) throws RoleIsExistException,Exception;
	
	public void grantRolePageActionRights(HttpServletRequest request) throws SuperRoleException,Exception;
	
	public void delRole(HttpServletRequest request)	throws SuperRoleException,Exception;
	
	public void addRole(HttpServletRequest request)	throws RoleIsExistException,Exception;
	
	public DBRow getDetailRoleByName(String name) throws Exception;
	
	public DBRow getDetailRoleByAdgid(long adgid) throws Exception;
	
	public DBRow[] getActionsByPage(PageCtrl pc,long id) throws Exception;

	public ArrayList getPermitActionAdid(String action)	throws Exception;
	
	public void grantAdminPageActionRights(HttpServletRequest request)	throws SuperRoleException,Exception;
	
	public DBRow[] getControlTreeByParentidOrderBySort(long parentid)	throws Exception;
	
	public DBRow[] getAdminByAdgid(long adgid,PageCtrl pc)	throws Exception;
	
	public void modAdmin(HttpServletRequest request)	throws Exception;
	
	public DBRow getDetailControlTreeById(long id)	throws Exception;
	
	public DBRow[] getDetailAdminByAccount(PageCtrl pc,String account)throws Exception;
	
	public DBRow[] getadvanSearch(PageCtrl pc,HttpServletRequest request)throws Exception;
	
	public int checkAdminAndUpdateToken(HttpServletRequest request) throws Exception;
	
	public DBRow[] getAdminGroupRelation(long adid) throws Exception; //根据用户ID获取部门角色关系信息
	
	public DBRow getAdminByEmployeName(String Employe_name) //根据员工名获取管理员信息
	throws Exception;

}
