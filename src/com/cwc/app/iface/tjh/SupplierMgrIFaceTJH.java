package com.cwc.app.iface.tjh;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface SupplierMgrIFaceTJH {
	
	public abstract DBRow [] getAllSupplier(PageCtrl pc)      //分页查询所有的供应商信息
	throws Exception;

	public abstract void addSupplier(HttpServletRequest request)   //添加供应商的相关信息
	throws Exception;   

	public abstract void delSupplier(HttpServletRequest request)    //删除供应商信息
	throws Exception;      
	
	public abstract DBRow getDetailSupplier(long sid)     //查看某一具体的供应商信息
	throws Exception;     

	public abstract void modSupplier(HttpServletRequest request)  //修改某一供应商信息
	throws Exception;  
	
	public abstract void uploadQualification(HttpServletRequest request)  //上传供应商的资质文件
	throws Exception; 
	
	public abstract DBRow [] getAptitudeBySupId(long id)   //根据供应商的id查询该供应商的所有的资质文件
	throws Exception;   

	public abstract void delSupplierOfAptitude(HttpServletRequest request) //删除供应商的 资质文件
	throws Exception;    

	public abstract DBRow [] getSearchSupplier(String key,PageCtrl pc)     //模糊查询供应商信息
	throws Exception;

	public abstract DBRow [] getAllCountryCode()   //获取所有的国家信息
	throws Exception;

	public abstract DBRow[] getProvinceByCountryId(long nation_id)  //根据国家id获取该国家下的省、州
	throws Exception;  
	
	public abstract DBRow getDetailCountry(long nation_id)   //获取具体的国家名称
	throws Exception;
	
	public abstract DBRow getDetailProvinceById(long provinceId)   //根据id获取省、州的详细信息
	throws Exception;
}
