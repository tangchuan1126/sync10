package com.cwc.app.iface.tjh;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface StorageTranspondMgrIFaceTJH {

	public abstract void addStorageTranspond(HttpServletRequest request)  //添加代发货仓库信息
	throws Exception;

	public abstract DBRow [] getAllStorageTranspond(PageCtrl pc)  //分页查询仓库间商品代发信息
	throws Exception;
	
	public abstract DBRow getDetailStorageTrandspond(long id)     //查看仓库间商品代发的详细信息
	throws Exception;

	public abstract void modStorageTranspond(HttpServletRequest request)  //修改某一具体的仓库间的商品代发
	throws Exception;

	public abstract void delStorageTranspond(HttpServletRequest request)   //删除仓库间商品的代发关系
	throws Exception;
}
