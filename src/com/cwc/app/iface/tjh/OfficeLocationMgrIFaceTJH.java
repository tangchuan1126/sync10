package com.cwc.app.iface.tjh;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface OfficeLocationMgrIFaceTJH {

	public abstract DBRow [] getOfficeLocationTree()    //获取办公地点的tree
	throws Exception;

	public abstract void addOfficeLocation(HttpServletRequest request)   //添加办公地点
	throws Exception;

	public abstract DBRow getDetailOfficeLocation(long id)    //获取办公地点的详细信息
	throws Exception;

	public abstract void modOfficeLocation(HttpServletRequest request)   //修改某一办公地点的详细信息
	throws Exception;

	public abstract void delOfficeLocation(HttpServletRequest request)    //删除办公地点信息
	throws Exception;
	
	public abstract DBRow [] getParentOfficeLocation()      //查询所有的父类的办公位置信息
	throws Exception;
	
	public abstract DBRow [] getChildOfficeLocation(long parentid)   //根据父类id获取该父类下的所有子类
	throws Exception;
	
	public abstract DBRow [] getAllOfficeLocation()      //获取所有的办公地点的信息
	throws Exception;
	
	public abstract DBRow getLocationIdByName(String office_name) //根据办公地点名称获取办公地点信息
	throws Exception;
	
	public abstract Map<String, DBRow[]> getAllOfficeLocationTree() //根据办公地点名称获取办公地点信息
			throws Exception;

	public abstract DBRow  addOfficeAddress(DBRow row,HttpServletRequest request)throws Exception;  //办公地址树节点添加
	
	public abstract DBRow updateOfficeaddress(DBRow row,long id) throws Exception;  //修改某一办公地点的详细信息
	public abstract DBRow updateOfficeaddressDetailed(DBRow row,long id) throws Exception;  //修改某一办公地点的详细信息
	
	public abstract DBRow delOfficeAddress(long id) throws Exception;  //删除办公地点信息
	
	public abstract boolean getNode(long id) throws Exception;  //删除办公地点信息

	
	
}
