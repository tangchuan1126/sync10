package com.cwc.app.iface.tjh;

import javax.servlet.http.HttpServletRequest;



import com.cwc.db.DBRow;

public interface ProductLineMgrIFaceTJH {
	
	public abstract DBRow [] getAllProductLine()   //获取所有的产品线定义信息
	throws Exception;      
	
	public abstract DBRow [] getProductCatalogByProductLineId(long productLineId)   //根据产品线id获取该产品线下的产品类别
	throws Exception;
	
	public DBRow[] getProductCatalogByProductLineId(long productLineId,long parent_catalog_id) 
	throws Exception;

	public abstract DBRow addProductLine(HttpServletRequest request)   //添加产品线信息
	throws Exception;
	
	public abstract void addProductCatalogById(HttpServletRequest request)   //给某一产品线添加产品类别
	throws Exception;
	
	public abstract void modProductLine(long productLineId)   //修改某一产品线下的产品类别
	throws Exception;
	
	
	public abstract DBRow getProductLineById(long id)   //获取某一详细的产品线信息
	throws Exception;

	public abstract DBRow deleteProductLine(HttpServletRequest request)   //删除产品线信息
	throws Exception;

	public abstract void modProductLine(HttpServletRequest request)   //修改某一产品线信息
	throws Exception;
	
	public void cleanProductCatalogForProductLine(HttpServletRequest request)//从产品线内清除产品分类
	throws Exception;
	
	public abstract DBRow getProductLineByName(String name) //根据产品线名称获取产品线信息
	throws Exception;
	
	public abstract DBRow findProductCategoryLevel(HttpServletRequest request) throws Exception;  // 查询产品线下的产品分类级数
}
