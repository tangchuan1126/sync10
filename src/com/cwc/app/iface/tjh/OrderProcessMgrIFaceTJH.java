package com.cwc.app.iface.tjh;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface OrderProcessMgrIFaceTJH {

	public abstract DBRow [] getAllOrderProcess(PageCtrl pc)    //获取所有的订单批处理
	throws Exception; 
	
	public abstract DBRow getProductById(long id)   //根据id获取商品的名称
	throws Exception;
	
	public abstract DBRow [] statsProductSpread(PageCtrl pc,String deliveryDate,long cid, String end_date)   //根据时间段统计产品需求分布
	throws Exception;

	public abstract DBRow getProductCategoryById(long id)   //根据id查询商品分类的最顶层名称
	throws Exception;
	
	public abstract DBRow [] getAllTheoryStroage(PageCtrl pc)    //分页查询理论仓库货物需求信息
	throws Exception;
	
	public abstract DBRow getProductCategory(long category_id)  //根据资产类别id获取父类信息
	throws Exception;
	
	public abstract DBRow [] getAllPlanStorageSales(PageCtrl pc)   //分页查询计划仓库的货物需信息
	throws Exception;
	
	public abstract DBRow [] getAllOriginalStorageSales(PageCtrl pc)   //分页查询代发产品后的本来仓库的产品需求
	throws Exception;

	public abstract DBRow[] getFusionMapsBySalesSpreadJSONAction(HttpServletRequest request)   //查询全世界各国以及该国家的省份以及州的产品需求分布情况
	throws Exception;
	
	public abstract void getProvinceSpreadbyMap(String fusion_id, long catalog, long p_line_id, long product_id, String start_date, String end_date)             //根据国家的fusionId查询该国家下的区域的商品的销售总额
	throws Exception;
	
	public abstract DBRow getCountryMapFlashByFusionId(String fusion_id)    //根据国家的fusionMap的id查询该国家的flash
	throws Exception;
	
	public abstract void statsProductSpreadByMaps(long catalog_id, long p_line_id, String p_name, String st_date, String end_date)		//查看地图中各国家的产品需求分布
	throws Exception;

	public abstract void statcProcuctNeedQuantity(PageCtrl pc,String start_date, long cid, String end_date)  //重新计算商品的需求量
	throws Exception;

	public abstract DBRow[] getPlanStorageProductQuantity(String start_date,String end_date, long cid)  //获取某一时间段内的所有的计划仓库的数据
	throws Exception;   
	
	public long statcProcuctNeedQuantityZJ(String start,String end,long cid)//第一张表
	throws Exception;
	
	public String orderProcessLastTime()//获得最后一条记录的发货时间（智能定时任务）
	throws Exception;
	
	public DBRow[] getActualStorageProductQuantity(String start_date,String end_date, long catalog_id, long pro_line_id,long ccid,long pro_id,String product_name,PageCtrl pc,String cmd)//根据产品线、产品类别、或商品名称过滤查询某一时间段内实际仓库的货物需求量的情况 
	throws Exception; 
	
	public DBRow[] productDemandAnalysis(String start_date,String end_date,long catalog_id,long pro_line_id,String product_name,long ca_id,long ccid,long pro_id,int type,PageCtrl pc)
	throws Exception;
	
	public String exportProductDemandAnalysis(HttpServletRequest request)
	throws Exception;
	
	public void everyDayCrontabDemandAnalysisAndPlanAnalysis()
	throws Exception;
}
