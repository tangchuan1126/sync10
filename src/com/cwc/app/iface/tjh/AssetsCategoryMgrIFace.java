package com.cwc.app.iface.tjh;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

/**
 * 
 * @author Administrator
 *	资产类别业务处理接口
 */
public interface AssetsCategoryMgrIFace {
	
	public abstract DBRow[] getAssetsCategoryTree() throws Exception;  //查询所有的资产类别信息

	public abstract void addAssetsCategory(HttpServletRequest request) throws Exception;  //添加资产类别信息

	public abstract void delAssetsCategory(HttpServletRequest request) throws Exception;       //删除选中的资产类别信息

	public abstract DBRow getDetailAssetsCategory(long id) throws Exception;      //根据id获取详细的资产信息

	public abstract DBRow[] getAssetsProductLine(long productLineId) throws Exception;
	
	public abstract void modAssetsCategory(HttpServletRequest request) throws Exception;    //修改选择的资产类别信息

	public abstract DBRow[] getAssetsCategoryChildren(long parentId) throws Exception;   //根据父类id获取该类下的所有子类
	
	public DBRow[] getAssetsCategoryChildren(long categoryId,String type)  throws Exception;   //区分样品与固定资产
	
	public abstract DBRow[] getAllAssetsCategoryTree() 
	throws Exception;
	
	public abstract DBRow [] getProductParentCatalog()     //获取parentid=0或为null的商品分类信息
	throws Exception;
	
	public abstract DBRow [] getProductCatalogByParentId(long parentid)   //根据父类id获取该类下所有商品分类子类信息
	throws Exception;

}
