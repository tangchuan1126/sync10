package com.cwc.app.iface.tjh;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface StorageSortMgrIFace {
	
	 //根据id获取该仓库下的发货人基本信息
	public abstract DBRow[] getDetailStorageSortById(long id) throws Exception;
	
	public abstract void addStorageSort(HttpServletRequest request) throws Exception; 


}
