package com.cwc.app.iface.tjh;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface AssetsMgrIFace {
	
	public DBRow [] AssetsList(PageCtrl pc,String type) throws Exception;    //分页查询所有的资产信息

	public void addAssets(HttpServletRequest request) throws Exception;   //增加一条资产记录

	public DBRow getDetailAssets(long aid) throws Exception;   //查询一条具体的资产详细信息
	
	public DBRow[] getAccounts() throws Exception;//查询账户信息
	
	public DBRow[] getAdminGroup() throws Exception;//获得部门
	
	public DBRow[] getAdmin() throws Exception;//获得员工
	
	public void modAssets(HttpServletRequest request) throws Exception;  //修改某一具体的资产信息

	public void delAssets(HttpServletRequest request) throws Exception;   //删除某一条资产信息

	public DBRow[] getAssetsByCategoryIdAndLocationId(int applyState,int goodsState,long locationId,long categoryId, PageCtrl pc,long productLineId,String type) //过滤查询资产信息
		throws Exception;   

	public DBRow[] getSearchAssets(String key, PageCtrl pc, String type) throws Exception;  

	public void batchModAssets(HttpServletRequest request) throws Exception;

	public String exportAssets(HttpServletRequest request) throws Exception;
	
	public String importAssets(HttpServletRequest request) throws Exception;
	
	public HashMap<String, DBRow[]> excelshow(String filename,String type,String asserttype) throws Exception;

	public void uploadAssets(HttpServletRequest request) throws Exception;
	
	//添加上传图片
	public void uploadImage(HttpServletRequest request) throws Exception;
	
}
