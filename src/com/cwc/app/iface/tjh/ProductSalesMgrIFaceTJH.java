package com.cwc.app.iface.tjh;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductSalesMgrIFaceTJH {
	
	public abstract DBRow[] getAllProductSales(PageCtrl pc)   //查询所有的已发货的产品销售量
	throws Exception;

	public long statsProductSalesByDelivery(String deliveryDate, String end_date)  //统计某一段时间内已发货的产品销售信息
	throws Exception;

	public abstract void statsAllCountryProductSales(long catalog_id,long p_line_id,long cid, String cmd, String p_name, String order_source,String monetary_unit,String start_date, String end_date)  //查询所有国家的产品的销售额、利润、成本的分布情况
	throws Exception;

	public abstract void getProvinceProductSalesRommByFusionId(String fusion_id,long catalog_id, String cmd, long p_line_id, long product_id, String order_source,String monetary_unit,String start_date, String end_date,long cid)  //根据国家ID获取该国家的产品销售额分布情况
	throws Exception;

	public abstract DBRow [] getAllProductSalesByDate(String start_date,String end_date)  //获取某一时间段内所有的产品销售的信息
	throws Exception;
	
	public abstract DBRow [] getAllProductSalesByCondition(String input_st_date,String input_en_date,long catalog_id,long pro_line_id,String product_name,String order_source,PageCtrl pc)   //根据不同的条件查询产品销售的情况
	throws Exception;

	
	public String productSalesLastTime()//判定最后的发货时间是哪天（智能定时任务）
	throws Exception;
	
	public DBRow[] getAllProductSalesByCondition(String input_st_date,String input_en_date, long catalog_id, long pro_line_id,String product_name, long ca_id,long ccid,long pro_id,long cid,int type,String order_source,String day_source,PageCtrl pc)//销售发货分析 
	throws Exception;
	
	public String exportSalesDeliveryAnalysis(HttpServletRequest request)//导出发货分析
	throws Exception;

}
