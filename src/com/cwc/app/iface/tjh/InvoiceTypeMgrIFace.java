package com.cwc.app.iface.tjh;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface InvoiceTypeMgrIFace {
	
	public abstract DBRow[] getAllInvoice() throws Exception;      //获取所有的出口发件人信息
	
	public abstract DBRow[] getDetailInvoiceById(long ps_id) throws Exception;   // 根据仓库ID显示该仓库下的数据
	
	public abstract DBRow getDetailInvoiceCompanyNameByDiId(long di_id) throws Exception; //根据发件人信息Id查询出口发件人
	
	public abstract void addInvoiceInfo(HttpServletRequest request) throws Exception;    //添加发票模板信息
	
	public abstract DBRow getDetailInvoiceTemplateById(long invoiceId) throws Exception;

	public abstract void modInvoiceTemplate(HttpServletRequest request) throws Exception;   //修改发票模板信息
	
	public abstract DBRow getStorageCategory(long id) throws Exception;   //根据id查询仓库信息

}
