package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface SystemConfigIFace 
{
	public com.cwc.db.DBRow getConfigValueByName(java.lang.String s) throws java.lang.Exception;
	
	public int getIntConfigValue(java.lang.String s) throws java.lang.Exception;
	
	public java.lang.String getStringConfigValue(java.lang.String s) throws java.lang.Exception;
	
	public java.lang.String getStringConfigDescription(java.lang.String s) throws java.lang.Exception;
	
	public com.cwc.db.DBRow[] getAllConfig(com.cwc.db.PageCtrl s) throws java.lang.Exception;
	
	public void modifySystemConfig(javax.servlet.http.HttpServletRequest r) throws java.lang.Exception;
	
	public void batchModifySystemConfig(HttpServletRequest request) throws Exception;
	
	public void backupDB() throws Exception;
	
	public void restoreTable(String tbname) throws Exception;
	
	public void checkCount() throws Exception;
	
	public String[] getAllDBBK() throws Exception;
	
	public DBRow[] getAllDBBKTableSql(String folder) throws Exception;
	
	public void importDB(HttpServletRequest request) throws Exception;
	
	public void delDataBaseBK(HttpServletRequest request) throws Exception;
	
	public void backupDBNow() throws Exception;
	
	public void getTableSqlString(String file,JspWriter out) throws Exception;
	
	public String[] getAllEmailTemplate() throws Exception;
	
	public String getDetailEmailTemplate(String name) throws Exception;
	
	public void modEmailTemplate(HttpServletRequest request) throws Exception;
	
	public String replaceHtmlImgSrc(String content) throws Exception;
	
	public String getReviewEmailTemplate(String name) throws Exception;
	
	public void flushTemplate() throws Exception;
	
	public void markUsedTemplate(HttpServletRequest request) throws Exception;
	
	public DBRow[] getAllTemplate() throws Exception;
	
	public String getUsedTemplateName() throws Exception;
	
	public void flushAllHtmlCache() throws Exception;

	public void addSysLog(AdminLoginBean adminLoggerBean,int opertate_type,String operation,String operate_para,String ip,String ref,String visitor)	throws Exception;
	
	public DBRow[] getSysLogsByAdidDate(long st_datemark,long en_datemark,long adid,int opertate_type,PageCtrl pc)	throws Exception;
	
	public DBRow[] getSysLogsByAdgidDate(long st_datemark,long en_datemark,long adgid,int opertate_type,PageCtrl pc)	throws Exception;
	
	public DBRow[] getAllSysLogsByDate(long st_datemark,long en_datemark,int opertate_type,PageCtrl pc)	throws Exception;
	
	public DBRow getDetailSysLogBySlid(long sl_id)	throws Exception;
	
	public double getCurrencyRate(String currency)
	throws Exception;
	
	public void batchUpdateCurrencyRate()
	throws Exception;
	
	public float getFloatConfigValue(String confname)
	throws Exception;
	
	public double getDoubleConfigValue(String confname)
	throws Exception;
}
