package com.cwc.app.iface.zzz;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface ApplyFundsCategoryMgrIfaceZZZ 
{
	public void addApplyFundsCategory(HttpServletRequest request)
	throws Exception;
	public DBRow[] getAllApplyMoneyCategory()
	throws Exception;
	public DBRow getApplyMoneyCategoryByCategoryId(HttpServletRequest request)
	throws Exception;
	public void deleteApplyFundsCategoryByCategoryId(HttpServletRequest request)
	throws Exception;
	public void updateApplyFundsCategoryByCategoryId(HttpServletRequest request)
	throws Exception;
	public DBRow[] getApplyFundsCategoryChildren(long id)
	throws Exception;
}
