package com.cwc.app.iface.zzz;
import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ApplyMoneyMgrIFace
{
	/**
	 * 提供增加申请资金的接口
	 * @param categoryId
	 * @param association_id
	 * @param amount
	 * @param paymentInformation
	 * @param remark
	 * @throws AdminIsExistException
	 * @throws Exception
	 */
	public DBRow addApplyMoney(String lastTime,String adid,long categoryId,long association_id,double amount,String payee,String paymentInformation,String remark,long center_account_id,long product_line_id,int association_type_id,long center_account_type_id,long center_account_type1_id,long payee_type_id,long payee_type1_id,long payee_id,String currency,AdminLoginBean adminLoggerBean,HttpServletRequest request) 
	throws Exception;
	
	public DBRow addApplyMoneyAndSchedule(String lastTime,String adid,long categoryId,long association_id,double amount,
			String payee,String paymentInformation,String remark,long center_account_id,long product_line_id,
			int association_type_id,long center_account_type_id,long center_account_type1_id,long payee_type_id,
			long payee_type1_id,long payee_id,String currency, AdminLoginBean adminLoggerBean,
			String sn,String path,String fileNames,
			String adminUserIds,int isNeedMail,int isNeedMessage,int isNeedPage,
			HttpServletRequest request) throws Exception ;
	
	public DBRow addApplyMoney(HttpServletRequest request) 
	throws Exception;
	
	//确认可以添加样品资金申请
	public void affirmAddApplyMoney(HttpServletRequest request)throws Exception; 
	
	public void updateApplyMoney(long apply_id,String adid,long categoryId,String association_id,double amount,String payee,String paymentInformation,String remark,long center_account_id,long product_line_id,String association_type_id, long center_account_type_id, long center_account_type1_id,long payee_type_id,long payee_type1_id,long payee_id,String currency, AdminLoginBean adminLoggerBean) 
	throws Exception;
	public void updateApplyMoney(HttpServletRequest request) 
	throws Exception;
	/**
	 * 获取所有的资金申请信息（带分页的）
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllApplyMoney(PageCtrl pc)
	throws Exception;
	
	//排序
	public DBRow[] getApplyMoneyLastTimeSort(PageCtrl pc)throws Exception;
	
	public DBRow[] getApplyMoneyCreateTimeSort(PageCtrl pc)throws Exception;
	
	/**
	 * 根据ID获取资金申请信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyMoneyById(String id)
	throws Exception;
    /**
     * 根据条件查询资金申请
     * @param request
     * @param pc
     * @return
     * @throws Exception
     */
	public DBRow[] getApplyMoneyByCondition(HttpServletRequest request,PageCtrl pc)
    throws Exception;
	/**
	 * 根据关联ID和类型查询资金申请
	 * @param types
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getApplyMoneyByTypeAndAssociationId(int types,long id)
    throws Exception;
	
	public void cancelApplyMoneyByTypeAndAssociationId(int type,long id)
    throws Exception;
	public DBRow[] updateApplyMoney(DBRow[] row)
	throws Exception;
	
	public long addAssets(HttpServletRequest request) 
	throws Exception;
	public void updateAssetsState(long assetsId,int state)
	throws Exception;
	
	public  void updateApplyMoneyStatusByApplyId(HttpServletRequest request)
	throws Exception;
	
	public void updateRemarkByApplyId(HttpServletRequest request)
	throws Exception;
	
	 public void deleteApplyMoneyByApplyId(HttpServletRequest request)
	   throws Exception;
	public void deleteApplyMoneyByApplyId(long id)
	throws Exception;
	
	public DBRow[] getApplyMoneyByInfo(String info,PageCtrl pc)
	throws Exception;
	public DBRow[] getApplyMonyByIndexMgr(String key,int search_mode,PageCtrl pc) throws Exception ;
	
	public DBRow[] getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(int types,long association_id,int association_type_id)
	throws Exception;
	
	public DBRow getApplyMoneyTransferTotalByTypeAndAssociationIdAndAssociationNoCancel(int types,long association_id,int association_type_id)
	throws Exception;
	
	public void editApplyMoneyIndex(long apply_money_id,String type)//资金申请编辑索引
	throws Exception;
	
	//上传图片信息
	public void addUploadImage(DBRow row) throws Exception ;
}
