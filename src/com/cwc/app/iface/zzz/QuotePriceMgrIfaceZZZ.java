package com.cwc.app.iface.zzz;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface QuotePriceMgrIfaceZZZ {
	public DBRow[] getPrentProductCategory() 
	throws Exception;
	public DBRow[] getProductCategoryById(long id)
	throws Exception;
	public DBRow[] getProductLine()
	throws Exception;
	public DBRow[] getProductStorageCatalogChildrenByParentId(long parentId)
	throws Exception;
	public DBRow[] getParentProductStorageCatalogByParentId()
	throws Exception;
	public List getQuoPriceByProductLine(HttpServletRequest request,boolean flag)
	throws Exception;
	public List getQuoPriceByProductName(HttpServletRequest request)
	throws Exception;
	public List getQuotedPriceByProductCatelog(HttpServletRequest request,boolean flag)
	throws Exception;
	public DBRow getWholesellDiscountByName()
	throws Exception;
	public List createTransportCost(long sc_id,long caId,float startWT,float endWT,float stepWT)
	throws Exception;
	public String createQuotedPDF(HttpServletRequest request) 
	throws Exception ;
	public void downLoadPDF(HttpServletResponse response,String filedownload)
	throws Exception;
}
