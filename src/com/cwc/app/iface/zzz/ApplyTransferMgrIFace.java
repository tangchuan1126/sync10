package com.cwc.app.iface.zzz;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.db.*;

public interface ApplyTransferMgrIFace
{
	
	//按最迟转款时间排序
	public DBRow[] getApplyTransferLastTimeSort(PageCtrl pc)throws Exception;
	//按转账完成时间排序
	public DBRow[] getApplyTransferEndTimeSort(PageCtrl pc)throws Exception;
	
	/**
	 * 增加转账申请
	 * @param request
	 * @throws AdminIsExistException
	 * @throws Exception
	 */
    public DBRow addApplyTransfer(HttpServletRequest request)
    throws Exception;
    /**
     * 获取所有的转账申请记录
     * @param pc
     * @return
     * @throws Exception
     */
    public DBRow[]  getAllApplyTransfer(PageCtrl pc)
    throws Exception;    
    /**
     * 根据资金申请单的ID，来获取关于该ID的转账申请信息
     * @param request
     * @param pc
     * @return
     * @throws Exception
     */
    public DBRow[] getApplyTransferByApplyMoneyId(long id,PageCtrl pc)
    throws Exception;
    /**
     * 上传转账凭证
     * @param request
     * @throws AdminIsExistException
     * @throws Exception
     */
    public long uploadFile(HttpServletRequest request)
    throws Exception;
    public long confirmTransferUploadVoucher(HttpServletRequest request) throws Exception;
    /**
     * 上传完转账凭证后更新资金申请表和转账申请表
     * @param id
     * @param path
     * @throws AdminIsExistException
     * @throws Exception
     */
    public long updateApplyTransferById(long id,String path,double amount,String payment_information, String receiver_account, AdminLoginBean adminLoggerBean,HttpServletRequest request)
	throws Exception;
    
    public DBRow[] getApplyTransferById(String id)
	throws Exception;
    
    public DBRow[] getApplyTransferByCondition(HttpServletRequest request,PageCtrl pc)
    throws Exception;
    
    public void deleteApplyTransferByTransferId(long id)
	throws Exception;
    public void deleteApplyTransferAndHandleApplyMoneyState(long applyId, long transportMoneyId,HttpServletRequest request)
    throws Exception;
    
    public   DBRow[] getApplyTransferByKey(String key,int search_mode,PageCtrl pc)
    throws Exception;
    public void updateTransferLastTime(HttpServletRequest request)throws Exception;
    public void updateApplyTransferMoney(long transfer_id, DBRow transferRow)throws Exception;
}
