package com.cwc.app.iface.zzz;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ApplyMoneyLogsMgrIFaceZZZ {

	public void addApplyMoneyLogs(long applyId,long userName_id, String name,String context)
	throws Exception;
	public  DBRow[] getApplyMoneyLogsByApplyId(long id,PageCtrl pc)
	throws Exception;
	/**
	 * 添加日志及任务日志
	 * @param request
	 * @throws Exception
	 */
	public void addApplyMoneyLogAndSchedule(HttpServletRequest request) throws Exception;
}
