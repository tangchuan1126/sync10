package com.cwc.app.iface.zzz;

import com.cwc.db.DBRow;

public interface OrderMgrIfaceZZZ {

	public DBRow[] getDetailProductByOid(long oid)
	throws Exception;
}
