package com.cwc.app.iface.hlb;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

/**
 * <p>ClassName：CheckInMgrIfaceHlb</p>
 * <p>Description：check_in 模块所调用方法的接口</p>
 * @author huanglianbin
 * @version 1.0 V
 * <p>createTime: 2015 年 2 月 28 日</p>
 */
public interface CheckInMgrIfaceHlb {
	public DBRow findDoorInfoById(String loadNo, long entry_Id) throws Exception;
	public DBRow[] findMasterBolLinesByLoadNo(String loadNo)throws Exception;
	public DBRow[] findOrdersNoAndCompanyByLoadNo(String loadNo)throws Exception;
	
	// 查询 loading 表
//	public DBRow getDoorByStagingOrTitle(long type, String search_number, long adid, HttpServletRequest request) throws Exception ;
	// 根据运输公司名字查询 mc_dot
	public DBRow[] findMcDotByCompanyName(String companyName) throws Exception;
	// 根据 mc_dot 查询运输公司名字
	public DBRow[] findCompanyNameByMcDot(String mc_dot) throws Exception;	
	// 根据 license plate 查询 driver name
	public DBRow[] findNameByLicense(String licenseNo) throws Exception;
	// 根据运输公司名字更新 mc_dot
	public long ModMcdotByCompanyName(String companyName, DBRow row) throws Exception;
	// 根据 mc_dot更新运输公司名字
	public long ModCompanyNameByMcdot(String mc_dot, DBRow row) throws Exception;
	// 根据运输公司名字查询  carrier_scac_mcdot 表信息
	public DBRow[] findCarrierInfoByCompanyName(String companyName) throws Exception;
	// 根据mc_dot查询  carrier_scac_mcdot 表信息
	public DBRow[] findCarrierInfoByMcdot(String mc_dot) throws Exception;
	// 添加 carrier 信息
	public long addCarrier(DBRow carrierInfo) throws Exception;
	// carrier 自动提示
	public DBRow[] getSearchCheckInDBCarrierJSON(String carrier) throws Exception;
	// mc_dot 自动提示
	public DBRow[] getSearchCheckInDBMcdotJSON(String mc_dot) throws Exception;
	// 查询同时匹配 mc_dot 和 carrier 的记录
	public DBRow matchNameAndMcdot(String carrierName, String mc_dot) throws Exception;
	// 更新司机信息
	public long updateDrivenInfo(long main_id, DBRow row) throws Exception;
 // 查询同时匹配 mc_dot 和 carrier 的记录
 	public DBRow matchNameAndMcdotInMain(String carrierName, String mc_dot) throws Exception;
	// 根据运输公司名字查询 主表mc_dot
	public DBRow[] findMcDotByCompanyNameInMain(String companyName) throws Exception;
	// 根据 mc_dot 查询主表运输公司名字
	public DBRow[] findCompanyNameByMcDotInMain(String mc_dot) throws Exception;
	// 更新主表信息
	public DBRow gateCheckInUpdate(DBRow row, HttpServletRequest request) throws Exception;
	// 获取匹配结果
	public DBRow getMatchResult(DBRow row, DBRow[] rows_mc_dot, DBRow[] rows_carrier) throws Exception;	
	
	
	/**
	 * 通过一个detailId去获取 扫描的Pallet
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月16日
	 */
	public DBRow[] getTaskScanPallets(long detail_id , int number_type ) throws Exception;
	
	/**
	 * MethodName： CloseNoticeByEid
	 * Description：通过 entryId 关闭主单据的所有 window schedule
	 * @author: huanglianbin
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow CloseNoticeByEid( HttpServletRequest request ) throws Exception;
	
	/**
	 * MethodName：CanOrNoGateEdit
	 * Description：判断 window 是否发了 schedule
	 * @author: huanglianbin
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow CanOrNoGateEdit( HttpServletRequest request ) throws Exception;
}

