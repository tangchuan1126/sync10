package com.cwc.app.iface.zzq;

import com.cwc.app.api.zr.AndroidPushMessge;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


public interface PushMessageByOpenfireMgrIfaceZzq {

	public Boolean sendNotice(long adid , String chatIds, String msg, String msgType) throws Exception ;
	
	public Boolean login(String adid, String password, Boolean isAdid, Boolean isReceiveMsg, Boolean isAnonymously) throws Exception ;

	public Boolean logout(String adid, Boolean isAdid) throws Exception ;
	
	public JSONObject aquireContacts(String account) throws Exception ;
	
	public Boolean sendMsg(String account , String chatId, String msg) throws Exception ;

	public JSONArray roundRobin(String account , String delTemp, String delHistroy, String chatId, String msgType) throws Exception ;
	
	/**
	 * 系统发送指令给Android 调用的接口
	 * 通过配置一个固定的帐号
	 * @param accounts
	 * @param msg
	 * @param msgType
	 * @return
	 * @throws Exception
	 */
	public boolean sysSendNotice(String accounts , AndroidPushMessge androidPushMessage) throws Exception ;
}
