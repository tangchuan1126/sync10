package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.task.NotTaskOwnerException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface TaskIFace 
{
	public long createOrderTask(AdminLoginBean adminLoggerBean,int t_reason,int t_operationsA[],long oid,String t_note,long t_target_admin,long t_target_group,long ps_id)
	throws Exception;
	
	public DBRow[] getOrderTasks(PageCtrl pc)	throws Exception;
	
	public DBRow getDetailOrderTaskByOtid(long ot_id) throws Exception;

	public void completeOrderTask(HttpServletRequest request)	throws NotTaskOwnerException,Exception;
	
	public void unCompleteOrderTask(HttpServletRequest request)	throws NotTaskOwnerException,Exception;
	
	public DBRow getTasksCountByOidStatus(long oid,int status)	throws Exception;
	
	public DBRow[] getOrderTasksByOid(long oid,PageCtrl pc)	throws Exception;
	
	public DBRow[] getOrderTasksByPsId(long ps_id,PageCtrl pc)
	throws Exception;
	
	public int getOrderTasksCount()
	throws Exception;
	
	public DBRow[] getTodayUnDoOrderTasksByOid(long oid,long ps_id,PageCtrl pc)
	throws Exception;
}
