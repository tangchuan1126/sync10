package com.cwc.app.iface.cc;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface GpsLocationMgrIfaceCc {
	/**
	 * 查询实时位置
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCurrentLocation(Map query) throws Exception;
}
