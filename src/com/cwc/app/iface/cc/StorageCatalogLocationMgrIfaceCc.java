package com.cwc.app.iface.cc;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface StorageCatalogLocationMgrIfaceCc {
/**
 * 按条件查询库存位置及数量
 * @param adidNullToLogin
 * @param pc_line_id  产品线
 * @param catalog_id  分类id
 * @param ps_id		  仓库id
 * @param code		  搜索关键字
 * @param union_flag  商品类型
 * @param adid		  admin_id
 * @param title_id	  title
 * @param type		  库存类型
 * @param lot_number_id	批次
 * @param pc		  分页
 * @param request
 * @return
 * @throws Exception
 */
	public DBRow[] findStorageCatalogLocation(boolean adidNullToLogin,String pc_line_id, String catalog_id,String ps_id,String code,int union_flag,long adid, String title_id, int type, String lot_number_id, HttpServletRequest request) throws Exception;
}
