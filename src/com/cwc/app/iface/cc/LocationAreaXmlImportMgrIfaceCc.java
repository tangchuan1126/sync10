package com.cwc.app.iface.cc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface LocationAreaXmlImportMgrIfaceCc {
	/**
	 * 保存kml解析结果到内存表
	 * @param rows
	 */
	public void savePraseRusult(List<DBRow> rows) throws Exception;
	/**
	 * 截断folder_from_kml表
	 * @return
	 * @throws Exception
	 */
	public void truncateTableFolderFromKml() throws Exception;
	/**
	 * 获取当前仓库现有和KML导入数据
	 * @return
	 * @throws Exception
	 */
	public DBRow getAllAreaInfoForStorage(DBRow storage) throws Exception;
	/**
	 * 保存结果
	 * @param request
	 * @throws Exception
	 */
	public void saveLocationArea(HttpServletRequest request) throws Exception;
	/**
	 * 通过id删除folder_from_kml表数据
	 * @param request
	 * @throws Exception
	 */
	public void deleteDataByid(String ps_id) throws Exception;
	/**
	 * 从storage_location_catelog表中获得location数据
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationData(String ps_id) throws Exception;
	/**
	 * 从storage_location_area表中获得area数据
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaData(String ps_id) throws Exception;
	/**
	 * 从storage_load_unload_location表中获得staging数据
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStagingData(String ps_id) throws Exception;
	/**
	 *从storage_yard_control表中获得parking数据 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getParkingData(String ps_id) throws Exception;
	/**
	 * 从storage_door表中获得docks数据 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDocksData(String ps_id) throws Exception;
	/**
	 * 通过ps_id查询area
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaByPsId(String ps_id) throws Exception;
	/**
	 * 通过ps_id,type查询area
	 * @param ps_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaByPsidAndType(String ps_id,String type) throws Exception;
	/**
	 * 通过ps_id查询door
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorByPsId(String ps_id) throws Exception;
	/**
	 * 查询所有title
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllTitle() throws Exception;
	/**
	 *  查询没一个 area图层下的location 
	 * @param slc_area
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationbyArea(String slc_area,long ps_id)throws Exception;
	/**
	 * 查询网络摄像头信息
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWebcamData(String ps_id) throws Exception;
	/**
	 * 查询打印机信息
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPrinterData(String ps_id) throws Exception;
	/**
	 * @author tanwei
	 * @param position_id
	 * @param sdId
	 */
	public void ddStorageAreaDoor(long position_id ,long sdId);

}
