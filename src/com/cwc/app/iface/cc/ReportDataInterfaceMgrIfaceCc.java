package com.cwc.app.iface.cc;

import java.util.Map;

import com.cwc.db.DBRow;

public interface ReportDataInterfaceMgrIfaceCc {
	/**
	 * 查询CustomerID对应的所有Item.
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getItemsByCustomerId(String customerId) throws Exception;
	
	/**
	 * 查询一段时间内customer对应的所有receiving
	 * @param customerId
	 * @param from
	 * @param to
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReceiveByCustomerIdAndDate(String customerId, String from, String to) throws Exception;
	
	/**
	 * 查询customer某天的所有receiving
	 * @param customerId
	 * @param companyId
	 * @param date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getIntradayReceiveByCustomerId(String customerId, String companyId, String date) throws Exception;
	
	/**
	 * 
	 * @param map  Map<String, String[]>  String:ps_name,仓库名称；String[]：companyId
	 * @return
	 * @throws Exception
	 */
	public Map appointmentCount(Map<String, String[]> map) throws Exception;
}
