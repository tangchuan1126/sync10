package com.cwc.app.iface.cc;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface GoogleMapsMgrIfaceCc {
	/**
	 * 获取GoogleMap边界
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getGoogleMapsBoundaries(HttpServletRequest request) throws Exception;
	/**
	 * 查询所有gps设备 并分组
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAssetWithGroup() throws Exception;
	/**
	 * 查询命令
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getContextCommand() throws Exception;
	/**
	 * 查询所有仓库KML
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllStorageKml() throws Exception;
	/**
	 * 根据ps_id查询仓库KML
	 * @return
	 * @throws Exception
	 */
	public DBRow getStorageKmlByPsId(int psId) throws Exception;
	/**
	 * 查询parking  docks占用情况
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getParkingDocksOccupancy(HttpServletRequest request) throws Exception;
	/**
	 * 查询AlarmLabel
	 * @param row
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getGeoAlarmLabel(DBRow row, PageCtrl pc) throws Exception;
	/**
	 * AlarmLabel数量统计
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getGeoAlarmLabelCount() throws Exception;
	/**
	 * 保存围栏
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long saveGeoALarmLabel(HttpServletRequest request) throws Exception;
	/**
	 * 删除围栏
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void removeAlarmLabel(HttpServletRequest request) throws Exception;
	/**
	 * 查询asset和alarm关联关系
	 * @param row
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAssetAlarm(DBRow row, PageCtrl pc) throws Exception;
	/**
	 * 取消asset关联的alarm
	 * @param id
	 * @throws Exception
	 */
	public void removeAssetAlarm(HttpServletRequest request)throws Exception;
	/**
	 * 添加asset关联的alarm
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addAssetAlarm(HttpServletRequest request) throws Exception;
	/**
	 * 查询所有分组
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllGroup() throws Exception;

	/**
	 * 通过ip和端口查询摄像图信息
	 * @param ip
	 * @param port
	 * @return
	 * @throws Exception
	 */
	public DBRow getCamInfo(String ip,String port) throws Exception;
	
	/**
	 * 插入webcam信息
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addWebcam(DBRow row)throws Exception;
	
	/**
	 * 更新webcam信息
	 * @param row
	 * @throws Exception
	 */
	public int updateWebcamInfo(DBRow row)throws Exception;
	/**
	 * 根据名称查询area坐标
	 * @param psId
	 * @param areaName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaPositionByName(long psId, String areaName) throws Exception;
	/**
	 * 根据名称查询location
	 * @param psId
	 * @param slc_position
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationInfoByName(long psId, String positionName) throws Exception;
	/**
	 * 
	 * @param psId
	 * @param slc_position
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStagingInfoByName(long psId, String positionName) throws Exception;
	/**
	 * 
	 * @param psId
	 * @param slc_position
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDocksInfoByName(long psId, String positionName) throws Exception;
	/**
	 * 
	 * @param psId
	 * @param slc_position
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getParkingInfoByName(long psId, String positionName) throws Exception;
	/**
	 * 更新storage_location_area表
	 * @param areaId
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updateStorageArea(long areaId,DBRow row) throws Exception;
	/**
	 * 更新folder_from_kml表
	 * @param data
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public int updateFolderkml(DBRow data,DBRow condition) throws Exception;
	/**
	 * 查询仓库area和title的关系
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaTitleByPsId(String psId) throws Exception;
	/**
	 * 查询仓库area和door的关系
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaDoorByPsId(String psId) throws Exception;
	
	
	public DBRow[] getAreaDoorCountsByPsId(long psId) throws Exception;
	
	/**
	 * 将坐标系坐标转换为经纬度
	 * @param psId
	 * @param x
	 * @param y
	 * @param xPosition
	 * @param yPosition
	 * @param angle
	 * @return
	 * @throws Exception
	 */
	public String convertCoordinateToLatlng(long psId,double x,double y,double xPosition, double yPosition, double angle) throws Exception;
	public String convertCoordinateToLatlng(String psId,String x,String y,String xPosition, String yPosition, String angle) throws Exception;
	/**
	 * 将坐标系坐标转换为经纬度
	 * @param psId
	 * @param x
	 * @param y
	 * @return
	 * @throws Exception
	 */
	public String convertCoordinateToLatlng(long psId,double x,double y) throws Exception;
	public String convertCoordinateToLatlng(String psId,String x,String y) throws Exception;
	/**
	 * 通过位置ID查询摄像头位置
	 * @param positionId
	 * @return
	 * @throws Exception
	 */
	public DBRow getCamPositionInfo(String positionId,int type) throws Exception;
	/**
	 * 地图上修改 location
	 * @param slc_id
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public int updateStorageLocation(long slc_id,DBRow data) throws Exception;
	/**
	 * 地图上修改 Parking
	 * @param slc_id
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public int updateStorageParking(long slc_id, DBRow data) throws Exception;
	/**
	 * 地图上修改 Docks
	 * @param slc_id
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public int updateStorageDocks(long slc_id, DBRow data) throws Exception;
	/**
	 * 地图上修改 Staging
	 * @param slc_id
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public int updateStorageStaging(long slc_id, DBRow data) throws Exception;
	/**
	 * 修改dock可用状态
	 * @param sd_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public int setStorageDoorStatus(long sd_id, int status) throws Exception;
	/**
	 * 直接从数据库中参数得到图像所需要发的经纬度信息
	 * 方便扩展以后删除kml
	 * @param type
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLayerData(int type ,Long ps_id )throws Exception;
	/**
	 * 通过经纬度计算坐标
	 * @param psId
	 * @param lat
	 * @param lng
	 * @return
	 * @throws Exception
	 */
	public String convertLatlngToCoordinate(long psId, double lat, double lng)throws Exception;
	
	public String convertLatlngToCoordinate(String psId, String lat, String lng)throws Exception;
	/**
	 * 添加打印机
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addPrinter(DBRow row)throws Exception;
	/**
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updatePrinter(DBRow row) throws Exception ;
	/**
	 *  添加printer和Zone关系
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addPrinterArea(DBRow row) throws Exception;
	/**
	 *通过 p_id删除printer和Zone的关系
	 * @param p_id
	 * @return
	 * @throws Exception
	 */
	public long deletePrinterArea(long p_id) throws Exception;
	/**

	 * 计算一个点到另外两个点所在直线上的最近点
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 * @param m
	 * @param n
	 * @return
	 * @throws Exception
	 */
	public String calculatePointtoLine(String a, String b, String c, String d,
			String m, String n) throws Exception;
	/**
	 *  计算一个点到另外两个点所在的直线的最近点
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 * @param m
	 * @param n
	 * @return
	 * @throws Exception
	 */
	 String calculatePointtoLine(double a, double b, double c, double d,
			 double m, double n) throws Exception;
	 /** 查询StorageRoad
	  * @param psId
	  * @return
	  * @throws Exception
	  */
	public DBRow[] getStorageRoadByPsid(long psId)throws Exception;
	/**
	 * 查询StorageRoadPoint
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageRoadPointByPsid(long psId)throws Exception;
	/**
	 * 查询location entery road
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationEnteryRoadByPsid(long psId)throws Exception;
	/**
	 * 获取导航路径
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRoutePath(long psId, int fromType, String fromPosition, String fromLatlng, int toType, String toPosition, String toLatlng) throws Exception;
	/**
	 * 张睿(根据PrintServer去查询他关联的Print)
	 * @param printServerId
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getPrintByPrintServer(long printServerId) throws Exception ;
	/**
	 * 
	 * @param psId
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public  DBRow[] getAreaDoorByPsIdandArea(long psId, long area_id) throws Exception;
	/**
	 * 
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPersonforAreaByPsId(long psId) throws Exception;
	/**
	 * 
	 * @param psId
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaPersonByPsIdandArea(long psId, long area_id) throws Exception;
	/**
	 * 通过area_id查询对应的打印机服务器
	 * @param area_id
	 * @param p_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPrinterServerByAreaIdandPtype( long area_id) throws Exception;
	/**
	 * 查询仓库坐标系数据
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageCoordinateSys(long psId) throws Exception;
	/**
	 * 
	 * @param psId
	 * @param name
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLayerInfoBypsIdAndName(long psId, String name, int type)
			throws Exception;
	/* 查询打印机名字
	 * @param row
	 * @return DBRow
	 * @throws Exception
	 */
	public DBRow checkPrinterNameIsExist(String name,long psId) throws Exception;
	/**
	 * 根据员工id和区域id更新该区域的员工
	 * @author tanwei
	 * @param id
	 * @param areaid
	 */
	public void updatePersonAreaByAdid(long id,int areaId) throws Exception;
	/**
	 * 删除指定区域指定员工id的员工
	 * @author tanwei
	 * @param id
	 */
	public void deletePersonAreaByAdid(long id) throws Exception;
	/**
	 * 根据仓库id查询未分配的员工
	 * @author tanwei
	 * @param psId
	 */
	public DBRow[] getPersonByPsId(long psId) throws Exception;
	/**
	 * @author tanwei
	 * @param
	 * @param
	 */
	public void deleteAreaDoorByAreaId(long position_id);
	public DBRow[] getLightData(long psId) throws Exception;
	/**
	 * 
	 * @param adminId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllStorageKmlByAdmin(AdminLoginBean bean) throws Exception;
	public long addLight(DBRow row) throws Exception;
	public long updateLight(DBRow row) throws Exception;
	public DBRow queryLightSingle(long psId, String name) throws Exception;
	public DBRow[] selectContainer(int status) throws Exception;
	public DBRow[] selectContainerPlates(long ic_id,long staging) throws Exception;
	public DBRow[] distinctContainerPlates(long ic_id, long stagingId)throws Exception;
	public DBRow[] queryContainerCounts(long ic_id, long psId) throws Exception;
	DBRow[] getStorageTimeOutBypsId(long ps_id) throws Exception;
	long addStorageTimeOut(DBRow row) throws Exception;
	long updateStorageTimeOutByPsId(DBRow row) throws Exception;
	DBRow[] getParkingDocksOccupancyNew(HttpServletRequest request)
			throws Exception;

	
}
