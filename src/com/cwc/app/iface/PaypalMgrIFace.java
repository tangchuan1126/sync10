package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;

import com.cwc.exception.SystemException;

public interface PaypalMgrIFace 
{
	public void paypalNotify(HttpServletRequest request) throws SystemException,Exception;
}
