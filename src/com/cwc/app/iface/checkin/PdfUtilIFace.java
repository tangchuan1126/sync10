package com.cwc.app.iface.checkin;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

public interface PdfUtilIFace {
	public boolean deletedExistPrintedLabel(HttpServletRequest req) throws NumberFormatException, Exception;
	public boolean checkExist(String entryId) throws NumberFormatException, Exception;
	public  int createPdf(String entryId,String detailId,HttpServletRequest req,boolean isManual) throws NumberFormatException, Exception;
	public  void createBolPdfByUrl(String url,String entryId,String prefix,boolean withDnname,String sessionId) throws NumberFormatException, Exception;
	public  int createPdf(String entryId,String detailId,HttpServletRequest req,boolean isManual,String loadNo) throws Exception;
	//public void batchGeneratePdf(JSONArray arr,HttpServletRequest request );
}
