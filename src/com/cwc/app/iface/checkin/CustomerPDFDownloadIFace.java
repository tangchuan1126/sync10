package com.cwc.app.iface.checkin;

import com.cwc.db.DBRow;

public interface CustomerPDFDownloadIFace {
	public DBRow[] findAllStorage() throws Exception;
    public DBRow[] getAllCustomer() throws Exception;

}
 


