package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.catalog.HaveProductException;
import com.cwc.app.exception.catalog.HaveRelationStorageException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface CatalogMgrIFace 
{
	public DBRow[] getProductStorageCatalogTree() throws java.lang.Exception;
	
	public long addProductStorageCatalog(HttpServletRequest request) throws Exception;
	
	public int modProductStorageCatalog(HttpServletRequest request) throws Exception;

	public void delProductStorageCatalog(HttpServletRequest request) throws HaveRelationStorageException,Exception;
	
	public DBRow[] getProductStorageCatalogByParentId(long parentid,PageCtrl pc) throws Exception;
	
	public DBRow getDetailProductStorageCatalogById(long id)	throws Exception;
	
	public DBRow[] geAllFather(long cid) throws Exception;
	
	public DBRow getProductStorageDetailCatalogByTitle(String title) throws Exception;
	
//	public DBRow[] getProductStorageCatalogByParentIdByCount(long parentid,int count) throws Exception;
	
	public DBRow[] getAllFatherCatalog(long cid) throws Exception;

	public DBRow[] getProductCatalogTree()	throws Exception;
	
	public DBRow addProductCatalog(HttpServletRequest request)	throws Exception;
	
	public int modProductCatalog(HttpServletRequest request)	throws Exception;
	
	public DBRow delProductCatalog(HttpServletRequest request)	throws HaveProductException,Exception;
	
	public DBRow[] getProductCatalogByParentId(long parentid,PageCtrl pc)	throws Exception;
	
	public DBRow getDetailProductCatalogById(long id)	throws Exception;
	
	public void markDevFlag(HttpServletRequest request)
	throws Exception;
	
	public void unMarkDevFlag(HttpServletRequest request)
	throws Exception;
	
	public void markReturnFlag(HttpServletRequest request)
	throws Exception;
	
	public void unMarkReturnFlag(HttpServletRequest request)
	throws Exception;
	
	public void markSampleFlag(HttpServletRequest request)
	throws Exception;
	
	public void unMarkSampleFlag(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getProductDevStorageCatalogByParentId(long parentid,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getProductDevStorageCatalogTree()
	throws Exception;
	
	public DBRow[] getProductReturnStorageCatalogTree()
	throws Exception;
	
	public void modProductStorageCatalogCountry(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getNativeProductDevStorageCatalogByParentId(long parentid,long nativ,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getNonNativeProductDevStorageCatalogByParentId(long parentid,long nativ,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getAllProductCatalog()
	throws Exception;
	public DBRow getDetailProductCatalogByName(String name) throws Exception;

	public void updateCategoryProductLine(HttpServletRequest request) throws Exception;
	
	// 产品线转化产品分类
	public DBRow addProductCatalogByTransfer(HttpServletRequest request)	throws Exception;
	
	// 产品分类转化产品线
	public DBRow addProductLineByTransfer(HttpServletRequest request)	throws Exception;
	
	public DBRow getProductLineByName(String title) throws Exception;
}



