package com.cwc.app.iface.wcr;

import com.cwc.db.DBRow;

public interface OutboundOrderMgrIFaceWCR {
	/**
	 * 获取明细中的容器
	 * 
	 * @param pc_id
	 * @param system_bill_type
	 * @param system_bill_id
	 * @param container_type
	 * @param clp_type_id
	 * @return
	 */
	public DBRow[] getOutListDetail(long pc_id,int system_bill_type,long system_bill_id,int container_type,long clp_type_id) throws Exception;
	
	/**
	 * 获取包装名称
	 * 
	 * @param container_type
	 * @param lp_type_id
	 * @return
	 * @throws Exception
	 */
	public String getLPName(int container_type, long lp_type_id) throws Exception;
	
	/**
	 * 获取Original个数
	 * 
	 * @param pc_id
	 * @param system_bill_type
	 * @param system_bill_id
	 * @return
	 * @throws Exception
	 */
	public int getOutListDetailOriginalCount (long pc_id, int system_bill_type, long system_bill_id)  throws Exception;
}