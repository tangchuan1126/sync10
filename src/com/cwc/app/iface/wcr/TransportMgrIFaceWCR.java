package com.cwc.app.iface.wcr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface TransportMgrIFaceWCR {
	/**
	 * 发货时过滤转运单，状态指定失效,仅(备货，运输，装箱)
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] fillterTransportDefault(long send_psid,long receive_psid,PageCtrl pc,int status,int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id) throws Exception;
	
	/**
	 * 我管理仓库（发货或收货）过滤转运单
	 * 
	 * @param psid
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @param qualityInspection
	 * @param productFile
	 * @param tag
	 * @param tagThird
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransport(long psid, PageCtrl pc,int status,int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id, int qualityInspection, int productFile, int tag, int tagThird) throws Exception;
	
	/**
	 * 该我处理的转运单-质量管理
	 * 
	 * @param psid
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @param qualityInspection
	 * @param productFile
	 * @param tag
	 * @param tagThird
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDefTransport(long psid, PageCtrl pc,int status,int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id, int qualityInspection, int productFile, int tag, int tagThird) throws Exception;
	
	/**
	 * 学习式获取流程默认方式, 
	 * 1获取订单相似的运单，
	 * 2获取客户发给客户的运单
	 * 
	 * @param psid 接收仓库id
	 * @param title_id 客户id
	 * @param order_id 订单id
	 * @return
	 */
	public DBRow getTransportForDefault(long psid, long title_id, long order_id) throws Exception;
	
	/**
	 * 订单生成运单
	 * 获取负责方
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long addB2BOrderTransport(HttpServletRequest request) throws Exception;
}
