package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import java.util.Map;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;

import java.util.*;
public interface TransportMgrIfaceZr {
	public DBRow getSupplierWarehouse(long supplier_id) throws Exception ;
	/**
	 * 如果from_ps_type表示的是供应商。那么send_psid就是供应商的Id。根据供应商Id去查询供应商的名称
	 * 其他的情况都是本系统的Ps_id
	 * @param from_ps_type
	 * @param send_psid
	 * @return
	 * @throws Exception
	 */
	public DBRow getSendPsByFromPsTypeAndSendPsid(long from_ps_type,long send_psid) throws Exception;
	public void addTransportLog(HttpServletRequest request)  throws Exception ;
	public void addTransportCertificateAddFile(HttpServletRequest request) throws Exception ;
	public void commonFileDelete(HttpServletRequest request) throws Exception;
	public String getRealPathByFileId(HttpServletRequest request) throws Exception ;
	public void handleTransportClearance(HttpServletRequest request) throws Exception ;
	public void handleTransportDeclaration(HttpServletRequest request) throws Exception ;
	public void handleTransportProductPictureUp(HttpServletRequest request) throws Exception ;
	public DBRow[] getAllProductFileByPcId(String pc_id , long file_with_type , long file_with_id) throws Exception ;
	public void updateTransport(long id , DBRow row) throws Exception ;
	public void updateTransportCertificateAndLogs(HttpServletRequest request ) throws Exception ;
	public void updateTransportProductFileAndLogs(HttpServletRequest request ) throws Exception ; 
	public DBRow getFileByFileWithIdANdFileTypeFileClass(String tableName , long file_with_id , long file_with_class , int file_with_type) throws Exception;
	public DBRow[] getFileByFilwWidthIdAndFileType(String tableName , long file_with_id , int file_with_type)throws Exception ;
	
 	public DBRow[] getAllUserAndDept(long proJsId,long ps_id)throws Exception ;
	public DBRow[] getAllUserAndDept(long proJsId ,long ps_id, long group) throws Exception ;
	/**
	 * 改成只上传文件记录日志。不会触发任务的跟进接口
	 * 任务跟进接口的调用在质检跟进里面去触发
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void hanleTransportQualityInspection(HttpServletRequest request) throws Exception ;
	public void hanleTransportStockInSet(HttpServletRequest request) throws Exception ;
	public DBRow[] getProductFileByFileTypeAndWithId(long file_with_id ,int file_with_type ,int product_file_type , PageCtrl pc) throws Exception ;
	public void handleTransportTag(HttpServletRequest request) throws Exception ;
	public DBRow[] getTransportLogs(long transport_id , int number) throws Exception ;
	public void transportCertificateFollowUp(HttpServletRequest request) throws Exception ;
	public void transportProductFileFollowUp(HttpServletRequest request) throws Exception ;
	/**
	 * 查询流程的所需要的时间(运费,清关,报关)
	 * @param transport_id
	 * @param processKey
	 * @return
	 * @throws Exception
	 */
	public String getProcessSpendTime(long transport_id ,int processKey, int activity_id) throws Exception ;
	public DBRow[] getProductByPcIds(String pc_ids) throws Exception ;
	public void transportApplyMoney(HttpServletRequest request) throws Exception ;
	public DBRow getFileNumberByFileWithIdAndFileWithType(String table ,long file_with_id,int file_with_type) throws Exception ;
	public void fixedScheduleAndPurchase() throws Exception ;
	public Map<Integer,DBRow> getProductFileAndTagFile(long pc_id , long fileWithId,int file_with_type) throws Exception ;
	public long insertLogs(long transport_id ,String transport_content , long adid ,String employe_name, int transport_type , String timeComplete ,int stage) throws Exception;
	public long insertTransportLogs(long transport_id ,String transport_content , long adid ,String employe_name, int transport_type , String timeComplete ,int stage) throws Exception;
	public void addTransportFileLogs(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception;
	public void addTransportFile(String fileName,long file_with_id,int file_with_type,int file_with_class , long upload_adid,String upload_time) throws Exception;
}
