package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;

public interface ScheduleReplayMgrIfaceZR {
	
	public long addScheduleReplay(DBRow row ,HttpServletRequest request) throws Exception;
	public long addScheduleReplay(DBRow row ,AdminLoginBean adminLoggerBean, String is_need_notify_executer) throws Exception;
	public DBRow[] getRowsByScheduleId(long id) throws Exception;
	public long addSimpleScheduleReplay(DBRow replay ) throws Exception ;
	

	
}
