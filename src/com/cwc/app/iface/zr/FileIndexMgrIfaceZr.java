package com.cwc.app.iface.zr;

import java.io.File;
import java.util.Map;

import org.apache.lucene.document.Document;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface FileIndexMgrIfaceZr
{
	public void createIndex(String attach_content , String question_title , String content , long question_id , long product_id ,long product_catalog_id , long question_catalog_id) throws Exception ;
	public  DBRow[] search(String keyWords,long product_id , long[] product_catalog_ids ,long[] question_catalog_ids , PageCtrl pc) throws Exception ;
	public void deleteIndex(long knowledgeId) throws Exception ;
	public void updateIndex(long question_id,String question_title , String content ,String attach_content , long product_id ,long product_catalog_id , long question_catalog_id) throws Exception ;
	public void batchCreateIndex(Document[] docs) throws Exception ;
	public void deleteIndexAll() throws Exception ;
	
}	
