package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;


public interface GisMgrIfaceZr { 
	
	/**
	 * 得到一个仓库的坐标
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月20日
	 */
	public DBRow[] getAllStorageLocations() throws Exception ;
	
	/**
	 * 得到一个仓库所有的Spot
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月20日
	 */
	public DBRow[] getSpots(long ps_id ) throws Exception ;
	
	/**
	 * 得到一个仓库的所有的Doors
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月20日
	 */
	public DBRow[] getDoors(long ps_id) throws Exception ;
	
}
