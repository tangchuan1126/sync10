package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface QuestionMgrIfaceZr
{
	public void addQuestionIndex(HttpServletRequest request ,long question_id,long product_id ,long product_catalog_id , long question_catalog_id) throws Exception ;
 
 
	public void deleteQuestionIndexById(long question_id) throws Exception ;
	public void deleteIndexFile(long fileId) throws Exception ;
	public void updateQuestionIndex(HttpServletRequest request) throws Exception ;
	public DBRow[] fileIndexSpellCheck(String keyWorld) throws Exception ;
	public DBRow[] fileIndexSuggest(String keyWorld) throws Exception ;
	public DBRow[] getSubProductCatalogByProductLineId(long product_line_id) throws Exception ;
	public DBRow getQuestionById(long question_id) throws Exception ;
	 
	public String getQuestionFilesContent(long question_id) throws Exception;
}
