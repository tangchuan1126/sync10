package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface BoxTypeMgrIfaceZr {

	public DBRow[] getAllBoxTypeByPage(PageCtrl page) throws Exception ;
	public DBRow addBoxTypeAdd(HttpServletRequest request) throws Exception ;
	public void deleteBoxType(HttpServletRequest request) throws Exception ;
	public DBRow updateBoxType(HttpServletRequest request) throws Exception ;
	
	public void deleteBoxTypeById(long type_id) throws Exception ;
	public void updateBoxType(long type_id , DBRow data)throws Exception ;
	public DBRow getBoxTypeBuId(long type_id) throws Exception ;
	/**
	 * 检查上传的xls.中是否有格式错误的。
	 * 1.商品是否存在。
	 * 2.length,width,height 
	 * 3.total_length,total_width,total_height 
	 * 4.weight
	 * 5.没有考虑上传的xls .里面只有头.没有数据的情况
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkBoxXls(String fileName) throws Exception ;
	public void addBoxByFile(HttpServletRequest request) throws Exception ;
	public DBRow[] getBoxContainerType(int container_type) throws Exception ;
	 
	public DBRow[] queryBoxTypeBySearchKeyAndContainerType(String search_key , long container_type_id , PageCtrl pc) throws Exception ;
	public DBRow exportBoxType(HttpServletRequest request) throws Exception ;
	
	public DBRow[] getBoxTypesByPcId(long pcid , int limit) throws Exception ;
	
	public DBRow[] getBoxTypes(long pc_id, int container_type_id,int title_id, int ship_to_id) throws Exception ;
	
	
	public DBRow[] getBoxTypes(long pc_id, int container_type_id,int title_id, int ship_to_id, int active) throws Exception ;
	
	public DBRow[] getBoxTypes(long pc_id, int container_type_id,int title_id, int ship_to_id, int customer_id,int active) throws Exception ;
	
	public DBRow[] getClpTypesByTitleAndShipTo(long pc_id,long title_id, long ship_to_id) throws Exception ;
	public DBRow[] getClpTypesByTitleAndShipTo(long pc_id,long title_id, long customer_id, long ship_to_id, long active) throws Exception ;
	
	/**
	 * 根据pcid 和 container_type_id 查询 对应盒子信息
	 * @param pcid
	 * @param container_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBoxTypesByPcIdAndContainerTypeId(long pcid , long container_type_id ) throws Exception ;
	public DBRow addBoxSkuTypeAdd(HttpServletRequest request) throws Exception ;
	public DBRow updateBoxSkuType(HttpServletRequest request) throws Exception ;
	public DBRow exportBoxSkuType(HttpServletRequest request) throws Exception ;
	/**
	 * 查询某种商品对应的盒子信息
	 * (Tatoo 2,3,4  == 24)
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBoxSkuTypeByPcId(long pc_id) throws Exception ;
	
	public DBRow addBoxContainer(HttpServletRequest request) throws Exception ;
	
	public DBRow[] getBoxContainerBy(PageCtrl page , long box_type_id ,long container_type) throws Exception ;
	/**
	 * 删除某个具体的容器。
	 * 如果是容器上面已经有商品,那么应该提示不能删除(现在没有做)
	 * @param con_id
	 * @throws Exception
	 */
	public void deleteBoxContainer(long con_id) throws Exception ;
	/**
	 * 修改某个具体的容器。
	 * 如果是容器上面已经有商品,那么应该提示不能修改(现在没有做)
	 * @param con_id
	 * @throws Exception
	 */
	public DBRow updateBoxContainer(HttpServletRequest request) throws Exception;
	/**
	 * 页面查询
	 * @param page
	 * @param box_type_id
	 * @param search_ket
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBoxContainerBySearchKey(PageCtrl page , long box_type_id , String search_ket ,long containerType) throws Exception ;
	
	public boolean isExitsHardwareId(String hardwareId ) throws Exception  ;
	
	public String getBlpTypeName(long box_type_id) throws Exception ;
	
	/**
	 * 包含 p_name 商品名
	 * 包含 title 仓库名
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public String getBlpTypeName(DBRow row) throws Exception ;
	
	/**
	 * 得到默认的顺序
	 * @param box_pc_id
	 * @return
	 * @throws Exception
	 */
	public int getBoxTypeSort(long box_pc_id) throws Exception;
	
	
	public void exchangeBoxTypeSort(long change_box_type_id ,long change_to_box_type_id ) throws Exception ;
	
	public boolean isExitsHardwareId(String hardwareId , long  con_id) throws Exception ;
	
	public String getBlpTypeNameNeedPName(DBRow row) throws Exception ;
	public String getBlpTypeNameNeedPName(long box_type_id) throws Exception ;
	
	//查询ilp根据商品
	public DBRow[] selectIlp(long productId)throws Exception;
	//查询ilp类型
	public DBRow[] selectIlpType(long productId)throws Exception;
	//查询ilp容器类型
	public DBRow[] selectContainerType()throws Exception;
	//添加ilp 类型
	public DBRow addBoxIlp(HttpServletRequest request)throws Exception;
	//根据id查询ilp
	public DBRow findIlpByIlpId(long ilpId)throws Exception;
	
	//级联clp type
	public DBRow getCascadeClpType(long id) throws Exception;
		
	//ilp 类型查询
	public DBRow[] seachIlpContainerType(long productId,long ilp_container_type)throws Exception;
	//更新ilp
	public DBRow updateIlpSkuType(HttpServletRequest request) throws Exception;
	//删除ilp
	public void deleteIlpType(HttpServletRequest request) throws Exception;
	/**zyj
	 * 验证添加的ILP类型是否已经存在
	 * @param pc_id：商品ID
	 * @param basic_type：基础容器类型ID
	 * @param len：长
	 * @param wid：宽
	 * @param hei：高
	 * @return
	 * @throws Exception
	 */
	public int findIlpTypeByBasicLWW(long pc_id,long basic_type, int len, int wid, int hei)throws Exception;
	/**zyj
	 * 验证添加的BLP类型是否已经存在
	 * @param pc_id：商品ID
	 * @param basic_type：基础容器类型ID
	 * @param len：长
	 * @param wid：宽
	 * @param hei：高
	 * @return
	 * @throws Exception
	 */
	public int findBlpTypeByBasicLWW(long pc_id,long basic_type,long inner_type, int len, int wid, int hei)throws Exception;
	/**zyj
	 * 查询所有ILP基础容器类型，再加上所有ILP基础容器类型这一项，用于页面选择
	 * @return
	 * @throws Exception
	 */
	public DBRow[] selectContainerTypeAddAllSel(int container_type)throws Exception;
	
	
	public DBRow getBoxTypesOrShiptoByPcId(long pcid, int limit) throws Exception ;
	/**
	 * 通过商品ID和容器名称查询容器类型
	 * @param pc_id
	 * @param lpName
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月11日 下午6:41:26
	 */
	public DBRow findLpTypeByPcidLpTypeName(long pc_id, String lpName) throws Exception;
	
	//public void updateLock(long id ,DBRow data) throws Exception ;
	public void updateLock(long id,long lockId) throws Exception ;
	
}	
