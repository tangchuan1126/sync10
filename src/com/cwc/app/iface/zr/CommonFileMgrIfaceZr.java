package com.cwc.app.iface.zr;

 
import com.cwc.db.DBRow;

public interface CommonFileMgrIfaceZr {

	public long addCommonFile(DBRow row) throws Exception ;   
	public DBRow getCommonFileById(long id) throws Exception ;
	
}
