package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface CheckReturnProductItemMgrIfaceZr {
	//添加商品需要测试的内容
	public long addCheckReturnProductItem(HttpServletRequest request) throws Exception ;
	//根据rpiId 得到某些图片已经关联上商品过后的数据
	public DBRow[] getCheckReturnProductItemByRpIId(long rpi_id) throws Exception ;
	
	public DBRow getCheckReturnProductItemById(long check_return_product_id) throws Exception ;
	
	public void replyCheckReturnProductItem(HttpServletRequest request) throws Exception;
	
	public void updateCheckReturnProductItem(HttpServletRequest request) throws Exception;
	
} 
