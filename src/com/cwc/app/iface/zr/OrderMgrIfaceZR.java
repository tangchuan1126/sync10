package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

/**
 *  首先进来就要判断交易的类型。
 *  如果是直接交易-->判断地址和用户的信息。
 *  如果地址信息在Client表中是没有的那么就添加一条记录。如果是直接交易的话那么就是要有一个client_id的字段
 *  如果是已经有了一条记录的话那么就是要update那条记录
 *  然后根据client_id 去地址信息的一张表中查询 address_name,address_street,contact_phone 判断是不是相等。如果是不相等的话就要新插入一条地址信息的记录 
 *  然后判断订单来源。
 *  是webSite ebay 走添加新的交易
 *  
 * @author Administrator
 *
 */
public interface OrderMgrIfaceZR {
	
	/**
	 * @param request
	 * @throws com.cwc.exception.SystemException
	 * @throws java.lang.Exception
	 */
	public long notifyIPN(HttpServletRequest request) throws com.cwc.exception.SystemException, java.lang.Exception;
	
	/**
	 * 判断交易的类型。 直接交易、间接交易、关联交易
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String tradeType(HttpServletRequest request) throws Exception;
	
	public DBRow[] getAllTrade(PageCtrl page) throws Exception;
	
	public DBRow[] getQuartationOrderAndItemsById(long quartationOrderId) throws Exception;
	
	public DBRow[] getWarrantyOrderAndItemsById(long warrantyOrderId) throws Exception;
	
	
	
	
	
	public DBRow[] flushNew(HttpSession session, String client_id, long sc_id,long ccid, long pro_id, long ps_id) throws Exception;
	// 根据billId 重新去构造购物车。
	public void addItemsInCart(HttpSession session ,long billId , long ps_id , double rate) throws Exception;
	
	
	public DBRow getDetailById(long id , String tableName , String pk) throws Exception;
	
	public DBRow[] getAddressInfoListByClientId(String client_id) throws Exception;
	
	public  void orderPreHandle(long oid)throws Exception;
	
	public DBRow getClientInfoByEmail(String email) throws Exception;
	/**
	 * 手工创建订单和交易。这个时候注意关联ID
	 * @param orderDBRow
	 * @param tradeDBRow
	 * @return
	 * @throws Exception
	 */
	public DBRow addOrderAndTradeByHandle(HttpServletRequest request) throws Exception;
	
	public DBRow getOrderByTxnId(String txn_id)  throws Exception;
	
	public DBRow setCcidAndProIdByStringValue(String address_country , String address_zip,String address_state,String code) throws Exception;
	
	public DBRow[] getRelationTradeByOid(long oid) throws Exception;
	
	public DBRow[] getAllRelationTradeByOid(long oid) throws Exception;
	
	public DBRow[] getTradeItemByOid(long oid) throws Exception ;
	
	public void fixPayMentMethodAndPayChannal() throws Exception;
	
	public DBRow[] filterTrade(HttpServletRequest request , PageCtrl pc) throws Exception;
	
	public DBRow getTradeByTradeId(long tradeId) throws Exception;
	
}
