package com.cwc.app.iface.zr;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ClpTypeMgrIfaceZr {

	public DBRow addClpType(HttpServletRequest request) throws Exception ;
	
	public void deleteClpType(long sku_lp_type_id) throws Exception ;
	
	public DBRow[] getCLpTypeBy(long ps_id , long pc_id) throws Exception ;
	
	public DBRow getCLpTypeByTypeId(long sku_lp_type_id) throws Exception ;
	
	public DBRow updateClpType(HttpServletRequest request) throws Exception ;
	
	
	public DBRow[] getClpContainerBy(PageCtrl pc,long sku_lp_type_id) throws Exception ;
	
	/**
	 * 查询的时候用
	 * @param pc
	 * @param sku_lp_type_id
	 * @param search_key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getClpContainerBySearchKey(PageCtrl pc,long sku_lp_type_id,String search_key) throws Exception ;
	
	public DBRow getClpTypeAndPsInfo(long sku_lp_type_id) throws Exception ;
	
	public String getClpTypeName(long sku_lp_type_id) throws Exception ;
	
	/**
	 * 包含 tile  仓库名
	 * 包含 p_name  商品名
	 * @param clpType
	 * @return
	 * @throws Exception
	 */
	public String getClpTypeName(DBRow clpType) throws Exception ;
	/**
	 * 添加clpType_container
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow addClpContainer(HttpServletRequest request) throws Exception ;
	
	
	public DBRow updateClpContainer(HttpServletRequest request) throws Exception ;
	
	public String getClpTypeNameNeedPName(DBRow row) throws Exception ;
	
	public String getClpTypeNameNeedPName(long clp_type_id) throws Exception ;
	
	public DBRow findContainerById(long id)throws Exception;
	
	//查询容器下的商品
	public DBRow findContainerProduct(long pc_id)throws Exception;
	
	 //根据id查询 clp
    public DBRow selectContainerClpById(long id)throws Exception;
    
    //根据id查询blp
	public DBRow selectContainerBlpById(long id)throws Exception;
	
	 //根据id查询ilp
    public DBRow selectContainerIlpById(long id)throws Exception;
    
    //根据clp id 查询仓库关系
    public DBRow[] selectPsByClpId(long id)throws Exception;
    
    /**
     * 根据pc_id 去查询CLPType
     * @param pc_id
     * @return
     * @throws Exception
     */
    public DBRow[] getClpTypeByPcid(long pc_id) throws Exception ;
    public DBRow findIlpTypeProductByIlpTypeId(long id)throws Exception;
    public DBRow findBlpTypeProductByBlpTypeId(long id)throws Exception;
    
    public DBRow[] findLpTliteAndShipTo(long id,int title_id,int ship_to_id)throws Exception;
  
    public DBRow[] findLpTliteAndShipTo(long id,int title_id,int customerId,int ship_to_id)throws Exception;
    
	public DBRow addLpTitleShipTo(HttpServletRequest request) throws Exception ;
	
	public void deleteLpTitleAndShipTo(long sku_lp_type_id) throws Exception ;
	
	// 导出产品的CLP以及title和shipTo
	public String ajaxDownCLPAndTitleAndShipTo(HttpServletRequest request)throws Exception;
	/**
	  * 通过PackagingType查询CLP Type
	  * @param pkg_id
	  * @return
	  * @throws Exception
	  * @author:zyj
	  * @date: 2015年3月25日 下午2:23:23
	  */
	public int findClpTypeCnByPackagingType(long pkg_id) throws Exception;
	
	public DBRow[] findClpTitle(long id,int title_id,int ship_to_id)throws Exception;
	public DBRow[] findClpShipTo(long id,int title_id,int ship_to_id)throws Exception;
	public Map findContainerTypeAndCLpType(List<Integer> type_id) throws Exception ;
	public DBRow findContainerCon(long type_id)throws Exception;
}
