package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;

public interface ConvertFileToSwfMgrIfaceZr
{
	/**
	 * 1.在上传的时候就调用文件的转换
	 * 2.所有的文件的转换不应该有事务的回滚,应该只是在这个文件上标识没有转换成功
	 * 3.文件的转换应该是在后台执行。这个时候前台可以关闭该窗口
	 * 4.在文件转换的时候应该使用Dwr.推送给该用户文件转换完成。(提示上带有连接,可以点击查看.这个时候查看因该是调用在线阅读的页面)
	 * @param srcFilePath
	 * @param desFilePath
	 * @param tempPdfPath
	 * @throws Exception
	 * result DBRow(){flag:success}
	 */
	public DBRow conver(String srcFilePath , String desFilePath , String tempPdfPath) throws Exception ;
	
}
