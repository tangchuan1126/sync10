package com.cwc.app.iface.zr;

import com.cwc.app.key.ModuleKey;
import com.cwc.db.DBRow;
import com.cwc.service.android.action.mode.HoldDoubleValue;

/**
 * 停车位的操作状态yc_id
 * @author win7zr
 *
 */
public interface StorageYardControlIfaceZr {


	
	/**
	 * @param yc_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public void freeSpot(long yc_id ,long adid , long associate_id , int associate_type) throws Exception ;
	 
	/**
	 * 占用某个停车位
	 * @param yc_id
	 * @param associate_type
	 * @param associate_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public void ocupiedSpot(long yc_id ,int associate_type , long associate_id , long adid) throws Exception ;
	
	/**
	 * 得到可用的门
	 * @param ps_id
	 * @param zone_id 可以为0
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getFreeSpot(long ps_id , long zone_id) throws Exception ;
	
	/**
	 * @param ps_id
	 * @param occupied_status
	 * @param zone_id 可以为0
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getSpotByPsIdAndOccupiedStatusAndZoneId(long ps_id ,  int occupied_status , long zone_id) throws Exception ;
	
	/**
	 * 这个方法 包含getFreeSpot 
	 * 返回结果为 getFreeSpot() + 当前这个associate_id占用
	 * @param ps_id
	 * @param occupied_status
	 * @param associate_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getAvailableSpotsBy(long ps_id , int associate_type , long  associate_id) throws Exception ;
	
	/**
	 * 传递一个spots的数组 ，去返回一个双数最小的 和一个单数最小的,没有就返回NULL
	 * 前体条件spots是已经按照yc_no(int) 从小到大排好序的
	 * @param spots
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public HoldDoubleValue<DBRow,DBRow> getMinSinleAndMinDoubleBySortedArray(DBRow[] spots) throws Exception ; 
	
	/**
	 * 通过一个zone_id 获取这个zone下面可以用的Spot + 这个单据在这个zone 下可以用的门
	 * 如果zone_id == 0l 那么返回 所有的 + 　这个单据在所有可以用的门getAvailableSpotsBy();
	 * @param ps_id
	 * @param zone_id
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getAvailableSpotsByZoneId(long ps_id , long zone_id , long associate_id ,int associate_type ) throws Exception;

	/**
	 * 查询那些门是被占用的(占用的)
	 * @param ps_id
	 * @param zone_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月16日
	 */
	public DBRow[] getUsedSpots(long ps_id , long zone_id ) throws Exception  ;
	
	/**
	 * 查询Check 模块占用门的信息
	 * @param sd_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月16日
	 */
	public DBRow getCheckInSpotsDetailInfo(long yc_id) throws Exception ;
	
	/**
	 * 当前的这个Spot是否是可以用的(如果是这个associate_id +　associate_type)　已经占用那么对于他来说是可以占用的
	 * 占用  占用
	 * @param sd_id
	 * @param associate_type
	 * @param associate_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月17日
	 */
	public boolean isCanUseSpot(long yc_id , int associate_type , long associate_id) throws Exception ;
	/**
	 * 通过spotId获取spot详细信息
	 * @param yc_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月17日 下午4:33:15
	 */
	public DBRow findSpotDetaiBySpotId(long yc_id) throws Exception;

    /**
	 * 得到当前 associate_type + associate_id (占用 的停车位)
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月17日
	 */
	public DBRow[] getUseSpotByAssociateIdAndType(long associate_id , int associate_type) throws Exception ;
/**
	 * 释放停车位
	 * @param associate_id
	 * @param associate_type
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月19日
	 */
	public void freeSpotsByAssociateId(long associate_id , int associate_type) throws Exception ;

/**
	 * 通过spotId、关联信息获取停车位信息
	 * @param spotId
	 * @param associate_type
	 * @param associate_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月20日 上午9:13:36
	 */
	public DBRow findSpotsBySpotIdAssociateId(long spotId , int associate_type, long associate_id) throws Exception;
	
	public DBRow[] getCheckInModuleByAssociateId(long dlo_id , int associate_type )throws Exception ;
}
