package com.cwc.app.iface.zr;

import java.util.Map;

import com.cwc.db.DBRow;
import com.cwc.model.BaseItem;
import com.ebay.soap.eBLBaseComponents.ItemType;

import java.util.*;
public interface EbayMgrZrIFace {
	
	public Map<String,String> getSellerId(String itemNumber,long trade_item_id) throws Exception;
	public DBRow  getEbayTokenBySellerId(String sellerId) throws Exception;
	public void updateTransactionsFeeSKU(String sellerId ,String itemNumber, String ebayTxnId , long itemId , String mpn,long oid)	throws Exception;
	public void getItemCall() throws Exception;
	/**
	 * 
	 * @param itemNumbers 根据Itemnumbers的数组去读取更多的信息包括有ItemCompatibilityList,
	 * 						Variations,ItemSpecifics
	 * @throws Exception
	 */
	public void getMoreInfoByItemNumbers(String[] itemNumbers,String filePath, String title , String token) throws Exception;
	/**
	 * 
	 * @param filePath
	 * @param title
	 * @return filePath的地址
	 * @throws Exception
	 */
	public DBRow getMoreInfoByXML(String filePath , String title, String outPutFile, String token) throws Exception;
	
	
	public DBRow findItemsAdvanced(String sellerId , String keywords,int startCount, int endCount , String site , String fileName , String loginSellerId) throws Exception;
	
	
	public void getMoreInfoByItemNumbersAndNotAjax(String  itemNumbers,String filePath, String title , String token , String usreId) throws Exception;
	/**
	 * DWR  读取EbayItem
	 * @param itemNumber
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public BaseItem readEbayItemByNumberAndToken(String itemNumber , String token) throws Exception;
	public void setReadStop(String userId) throws Exception;
	/**
	 * updateEbayItem
	 * @param ids
	 * @param loginSellerId
	 * @param systemUserId
	 * @throws Exception
	 */
	public void setUpdateEbayItem(String ids , String loginSellerId , String systemUserId) throws Exception ;
	
	public List<ItemType> getItemTypeByFileId(String fileId) throws Exception;
	public void updateTradeItem(long trade_item_id , DBRow row) throws Exception ;
	
	
	
	
	public DBRow sendEbayMessage(long oid , String content  ) throws Exception ;
	
	/**
	 * ebay account manger 
	 */
	
	public DBRow[] getAllEbayAccount() throws Exception ;
	
	public void updateEbayAccount(long ebay_account_id , String user_id , String user_token , String password ) throws Exception ;
	
	public long addEbayAccount( String user_id , String  user_token , String password )  throws Exception ;
	
	public DBRow getEbayAccountById(long ebay_account_id) throws Exception ;
	
	public DBRow getSessionId() throws Exception ;
	public DBRow getEbayTokenAndUpdateEbayToken(long ebay_account_id , String session_id , String run_name) throws Exception ;
	public DBRow getEbaySellerIdAndUpdateOrder(long oid , String item_number) throws Exception ;
}
