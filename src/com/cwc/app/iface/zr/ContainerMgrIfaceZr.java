package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;

public interface ContainerMgrIfaceZr {
	
	public DBRow addContainer(DBRow container) throws Exception ;
	
	/**
	 * 如果是创建C，C里面包含B，B里面包含I,那么都会一起创建出来
	 * 根据container_type_id ,  container_type 创建Container
	 * @param container_type_id
	 * @param container_type
	 * @param number			（需要创建的个数）
	 * @throws Exception
	 */
	public DBRow[] createContainer(long container_type_id, int container_type, int number , long title_id , String lotnumber) throws Exception ;
	
	
	public DBRow[] getContainerBaseType(int containerType) throws Exception ;
}
