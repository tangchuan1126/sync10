package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;

public interface WmsLoadMgrZrIface {
	
	/**
	 * 添加一组load 和orders的对应的关系
	 * @param load
	 * @param orders
	 * @throws Exception
	 */
	public long addLoad(DBRow load) throws Exception;
	
	/**
	 * 
	 * @param orderRow
	 * @return
	 * @throws Exception
	 */
	public long addWmsLoadOrder(DBRow orderRow) throws Exception ;
	
	/**
	 * 添加wms 的信息
	 * @param wmsRow
	 * @return
	 * @throws Exception
	 */
	public long addWmsMastBolRow(DBRow wmsRow) throws Exception;
	
	/**
	 * 添加或者是更新master_bol 的数据
	 * @param wmsMasterBolRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
	public long saveOrUpdateWmsMasterBolRow(DBRow wmsMasterBolRow) throws Exception;
	
	/**
	 * 根据load_id去查询MastBol
	 * @param wms_load_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getMastBolRowByLoadId(long wms_load_id)throws Exception ;
	
	/**
	 * 更新一个Order的状态
	 * @param order_id
	 * @throws Exception
	 */
	public void updateOrderStatus(long order_id , DBRow updateRow) throws Exception ;
	
	 
	
	/**
	 * (根据loadnumber,customer_id,company_id)去查询本地数据库是否已经有了这个load
	 * @param loadnumber
	 * @param customer_id
	 * @param company_id
	 * @return
	 * @throws Exception
	 */
	public DBRow queryLoadBy(String loadnumber , String customer_id , String company_id) throws Exception ;
	
	/**
	 * 根据load查询order
	 * @param load_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] queryOrderByLoad(long load_id) throws Exception ;
	
	/**
	 * 删除sync系统中master_bol 表中的数据，order的数据，order_type_count 的数据
	 * @param wms_master_bol_id
	 * @throws Exception
	 */
	public void deleteWmsMasterBolInfo(long wms_master_bol_id) throws Exception ;
	/**
	 * 根据wms_load_id 的数据去查询Load info
	 * @param wms_load_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getWmsLoadInfo(long wms_load_id) throws Exception ;
	
	/**
	 * 
	 * @param master_bol_no
	 * @param customer_id
	 * @param company_id
	 * @return
	 * @throws Exception
	 */
	public DBRow queryMasterBolBy(String master_bol_no , String customer_id , String  company_id ) throws Exception ;
	/**
	 * 
	 * @param wms_master_bol_id
	 * @param order_number
	 * @return
	 * @throws Exception
	 */
	public DBRow queryOrderBy(long wms_master_bol_id , String order_number) throws Exception;
	/**
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addPalletTypeRow(long dlo_detail_id,String order_number,int order_type,int order_system,String wms_pallet_type,int wms_pallet_type_count,String wms_pallet_number,String wms_consolidate_pallet_number,int wms_consolidate_reason , long wms_order_id , int delivery_or_pick_up , long lr_id , long resources_id , int resources_type , long scan_adid) 
	throws Exception; 
	
	/**
	 * update or save pallet type 
	 * @param row
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月13日
	 */
	public long saveOrUpdatePalletTypeRow(DBRow row) throws Exception ;
	/**
	 * 
	 * @param load_no
	 * @return
	 * @throws Exception
	 */
	//public DBRow getLoadByLoadNumber(String load_no ,long entry_id) throws Exception ;
	
 
	 
 	/**
	 * 
	 * @param dlo_detail_id
	 * @param order_number
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月9日
	 */
	public DBRow getOrderByLRIDAndOrderNumbers(long LR_ID , String order_number) throws Exception ;
	
	/**
	 * 根据wms_order_id 和 pallet_number去更新 WmsLoadOrderPalletType
	 * @param wms_order_id
	 * @param pallet_number
	 * @param udpateRow
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
	public void updateWmsLoadOrderPalletTypeBy(long wms_order_id , String pallet_number , DBRow udpateRow) throws Exception ;
	
	/**
	 * 通过wms_order_type_id 去更新
	 * @param wms_order_type_id
	 * @param udpateRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月29日
	 */
	public void updateWmsLoadOrderPalletType(long wms_order_type_id ,   DBRow udpateRow) throws Exception ;
	/**
	 * 通过 master_bol_id ， lr_id 去查询orderInfos
	 * @param lr_id
	 * @param master_bol_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
	public DBRow[] getOrderInfoByLrId(long lr_id , long master_bol_id) throws Exception ;
	/**
	 * 删除wms_Order 以及Pallet type info
	 * @param wms_order_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月11日
	 */
	public void deleteOrderInfoAndPalletInfoByOrderId(long wms_order_id) throws Exception ;
	/**
	 * 根据wms_order_id去查询pallet type.
	 * @param wms_order_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月11日
	 */
 	public DBRow[] getOrderPalletInfoByWmsOrderId(long dlo_detail_id, long wms_order_id)throws Exception ; 
	
	/**
	 * 更新这个dlo_detail_id 对应master_bol_no 的Order的信息
	 * @param dlo_detail_id
	 * @param master_bol_no
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月12日
	 */
	public void updateLoadOrderStatus(long lr_id , String master_bol_no , String pro_no) throws Exception ;

	/**
	 * @param dlo_detail_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月12日
	 */
	public void updateOrderByLrId(long lr_id) throws Exception ;
	/**
	 * 
	 * @param lr_id
	 * @param master_bol_no
	 * @param pro_no
	 * @author zhangrui
	 * @Date   2014年9月12日
	 */
	public void updateOrderProNo(long lr_id , String master_bol_no , String pro_no) throws Exception ;
	/**
	 * @param LR_ID
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月15日
	 */
	public DBRow[] getMasterBolBy(long lr_id) throws Exception ;
	
	/**
	 * 通过Detail_id 和Master_bol_no 去获取Order的信息
	 * @param dlo_detail_id
	 * @param master_bol_no
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月15日
	 */
	public DBRow[] getOrderInfoByLrIdAndMasterBol(long lr_id , String master_bol_no) throws Exception ;
	
	/**
	 * 根据dlo_detail_id , 和master_bol_no去查询MasterBol
	 * @param dlo_detail_id
	 * @param master_bol_no
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月15日
	 */
	public DBRow getMasterBolByLrIdAndMasterBolNo(long lr_id , String master_bol_no) throws Exception ;
	/**
	 * 更新wms_master_bol_id
	 * @param wms_master_bol_id
	 * @param updateRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月15日
	 */
	public int updateMasterBol(long wms_master_bol_id , DBRow updateRow) throws Exception ;
	/**
	 * @param wms_order_id
	 * @param updateRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月15日
	 */
	public int upateOrder(long wms_order_id , DBRow updateRow) throws Exception ;
	
	/**
	 * master_bol 中的total pallet 数量-1
	 * @param wms_master_bol_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月21日
	 */
	public void subtractMasterBolTotalPallet(long wms_master_bol_id) throws Exception ;
	/**
	 * 获取Order做了Consolidate的数量
	 * @param wms_order_id
	 * @return
	 * @throws Excepiton
	 * @author zhangrui
	 * @Date   2014年9月25日
	 */
	public DBRow getWmsOrderConsolidateNumber(long wms_order_id) throws Exception ;
	
	/**
	 * @param wms_order_type_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月13日
	 */
	public int deletePalletBy(long wms_order_type_id) throws Exception ;
	
	/**
	 * 通过dlo_detail_id order_number_pallet_no 去查询扫描的一个Pallet
	 * @param order_number
	 * @param dlo_detail_id
	 * @param pallet_no 
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月13日
	 */
	public DBRow getPalletByNumber(String order_number , long dlo_detail_id , String pallet_no) throws Exception ;
	
	/**
	 * 
	 * @param order_number
	 * @param dlo_detail_id
	 * @param pallet_no
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月13日
	 */
	public int deletePalletBy(String order_number , long dlo_detail_id , String pallet_no) throws Exception ;
	/**
	 * 查询所有的WmsLoadOrders
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月24日 下午12:45:01
	 */
	public DBRow[] findWmsLoadOrders(long start, long end) throws Exception;
	
	/**
	 * 删除某个Task 扫描的 Pallet
	 * @param dlo_detail_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月29日
	 */
	public void deleteScanPalletType(long dlo_detail_id) throws Exception ;
	
	 /**
	  * 通过LR_Id 去获取 当前Load的Pallet
	  * @param lr_id
	  * @return
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2015年1月5日
	  */
	 public DBRow[] commonLoadRefresh(long lr_id ) throws Exception ;
	 
	 /**
	  * 扫描一个Pallet 或者是输入一个Pallet Type + 一个数量
	  * @param insertRow
	  * @return
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2015年1月5日
	  */
	 public long commonLoadPallet(DBRow insertRow) throws Exception ;
	 /**
	  * 删除一个扫描的一个Pallet
	  * @param wms_order_type_id
	  * @return
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2015年1月5日
	  */
	 public int commonLoadDelete(long wms_order_type_id ) throws Exception ;
	 
	 /**
	  * @param wms_order_type_id
	  * @param updateRow
	  * @return
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2015年1月5日
	  */
	 public int commonLoadModify(long wms_order_type_id , DBRow updateRow) throws Exception ;
	 
	 /**
	  * reload common 删除lr_id的扫描的记录,
	  * 如果是一个单据号 多个Task，那么现在是reload 都删除的
	  * @param lr_id
	  * @param detail_id
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2015年1月5日
	  */
	 public void commonLoadReload(long lr_id , long detail_id) throws Exception ;
	 
	 /**
	  * checkInIndex查询pallet用的方法  wfh 按照detailId，masterbolNo查询order
	  * @param dlo_detail_id
	  * @param masterBolNo
	  * @return
	  * @throws Exception
	  */
	 public DBRow[] getOrderByDetailAndMasterBol(long dlo_detail_id, String masterBolNo)throws Exception;
	 
	 /**
	  * checkInIndex查询pallet用的方法  wfh 按照detailId，masterbolNo查询order包括Pallet为0的
	  * @param dlo_detail_id
	  * @param masterBolNo
	  * @return
	  * @throws Exception
	  */
	 public DBRow[] getOrderByDetailAndMasterBolIncludeZeroPallet(long dlo_detail_id, String masterBolNo)throws Exception;
	 
	 /**
	  * checkInIndex查询pallet用的方法  wfh  按照detailId查询masterBol
	  * @param dlo_detail_id
	  * @param masterBolNo
	  * @return
	  * @throws Exception
	  */
	 public DBRow[] getMasterBolInfoByDetailId(long dlo_detail_id) throws Exception;
	
}
