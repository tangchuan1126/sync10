package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;

public interface SysNotifyIfaceZr {
	public void handleScheduleSendEmail(DBRow[] executeUsers ,String emailTitle, String context ,DBRow[] assignUserRow , DBRow[] scheduleJoinExecuteRow) throws Exception;
	
	/**
	 * @param executeUsers  发给谁
	 * @param associate_type
	 * @param associate_id
	 * @param context
	 * @param adid			谁发的
	 * @param associate_main_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月4日
	 */
	public void handleScheduleAddShortMessage(DBRow[] executeUsers,long adid , DBRow schedule) throws Exception;
 
	public void handleSheduleNotifify(DBRow shedule , String emailTitle) throws Exception;
 	//页面提醒用显示文本,不用art
	public void hanleDwrSendAlertShow(DBRow[] executeUsersRow,String content) throws Exception ;
	public void handleScheduleReplay(long schedule_id , String title , String context , HttpServletRequest request)throws Exception ;
	public void handleScheduleReplay(long schedule_id, String title,
			String context,AdminLoginBean adminLoggerBean, String isNeedNotifyExecuter) throws Exception;
 	public void handleUpdateScheduleSendEmail(String usersId,DBRow shedule ,HttpServletRequest request) throws Exception ;
	public void setSysNofifySessionFlag(String adid) throws Exception ;
	public void handleSheduleNotifify(DBRow shedule, String emailTitle,
			long adid) throws Exception;
	public void handleDwrSend(DBRow[] executeUsersRow) throws Exception;
	public void handleUpdateScheduleSendEmail(String usersId, DBRow shedule,
			AdminLoginBean adminLoggerBean) throws Exception;
	public void handleScheduleCancle(String usersId, DBRow shedule ,AdminLoginBean adminLoggerBean)
	throws Exception;
	/**
	 * @param data 
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年7月1日
	 */
	public void handleSystemNotify(DBRow data ) throws Exception ;
}
