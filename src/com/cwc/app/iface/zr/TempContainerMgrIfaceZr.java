package com.cwc.app.iface.zr;

import java.util.Map;

import com.cwc.db.DBRow;

/**
 * container 的操纵
 * @author zhangrui
 *
 */
public interface TempContainerMgrIfaceZr {
	
	
	public DBRow findContainerByNumber(String number) throws Exception;

	/**
	 * 把TLP上面的一个容器打散
	 * 1.首先是只能是TLP才可以打散。打散的商品记录在TLP上
	 * @param parentContainerId
	 * @param container_id
	 * @throws Exception
	 */
	public void scatterContainer(long parentContainerId , long container_id) throws Exception ;
	
	/**  
	 * @param container
	 * @param title_id
	 * @throws Exception
	 * 1.在临时的Container表上创建Container <br />
	 * 2.只能是TLP
	 * 3.is_full , is_sn 
	 */
	public long addTempTLPContainer(String container , long title_id) throws Exception ;
	
	/**
	 * 添加Container到temp表中。同时，写入图数据库中
	 * @param container
	 * @return
	 * @throws Exception
	 */
	public long addContainer(DBRow container) throws Exception ;
	/**
	 * 提交container
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public DBRow submitContainerTemp(Map<String,Object> data,long ps_id) throws Exception ;
}
