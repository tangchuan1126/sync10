package com.cwc.app.iface.zr;

import com.cwc.app.key.ModuleKey;
import com.cwc.db.DBRow;
import com.cwc.service.android.action.mode.HoldDoubleValue;

/**
 * 张睿对门的操作2014-11-15  storage_door 新添加字段
 * @author win7zr
 *
 */
public interface StorageDoorMgrIfaceZr {
	
	/**
	 * @param sd_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public void freeDoor(long sd_id , long adid , long associate_id , int associate_type ) throws Exception ;
	 
	/**
	 * 占用某个门
	 * @param sd_id
	 * @param associate_type
	 * @param associate_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public void ocupiedDoor(long sd_id ,int associate_type , long associate_id , long adid) throws Exception ;
	 
	/**
	 * 保留某个门
	 * @param sd_id
	 * @param associate_type
	 * @param associate_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public void reserveredDoor(long sd_id , int associate_type , long associate_id , long adid ) throws Exception ;
	
	/**
	 * 得到可用的门
	 * @param ps_id
	 * @param zone_id 可以为0
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getFreeDoor(long ps_id , long zone_id) throws Exception ;
	
	/**
	 * @param ps_id
	 * @param occupied_status
	 * @param zone_id 可以为0
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getDoorByPsIdAndOccupiedStatusAndZoneId(long ps_id ,  int occupied_status , long zone_id) throws Exception ;
	
	/**
	 * 这个方法 包含getFreeDoor 
	 * 返回结果为 getFreeDoor() + 当前这个associate_id占用和保留的门
	 * @param ps_id
	 * @param occupied_status
	 * @param associate_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getAvailableDoorsBy(long ps_id , int associate_type , long  associate_id) throws Exception ;
	
	/**
	 * 传递一个doors的数组 ，去返回一个双数最小的 和一个单数最小的,没有就返回NULL
	 * 前体条件doors是已经按照doorName(int) 从小到大排好序的
	 * @param doors
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public HoldDoubleValue<DBRow,DBRow> getMinSinleAndMinDoubleBySortedArray(DBRow[] doors) throws Exception ; 
	
	/**
	 * 通过一个zone_id 获取这个zone下面可以用的door + 这个单据在这个zone 下可以用的门
	 * 如果zone_id == 0l 那么返回 所有的 + 　这个单据在所有可以用的门getAvailableDoorsBy();
	 * @param ps_id
	 * @param zone_id
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getAvailableDoorsByZoneId(long ps_id , long zone_id , long associate_id ,int associate_type ) throws Exception;

	/**
	 * 查询那些门是被占用的(保留+占用的)
	 * @param ps_id
	 * @param zone_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月16日
	 */
	public DBRow[] getUsedDoors(long ps_id , long zone_id ) throws Exception  ;
	
	/**
	 * 查询Check 模块占用门的信息
	 * @param sd_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月16日
	 */
	public DBRow getCheckInDoorDetailInfo(long sd_id) throws Exception ;
	
	/**
	 * 当前的这个门是否是可以用的(如果是这个associate_id +　associate_type)　已经占用那么对于他来说是可以占用的
	 * 占用 保留 + 占用
	 * @param sd_id
	 * @param associate_type
	 * @param associate_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月17日
	 */
	public boolean isCanUseDoor(long sd_id , int associate_type , long associate_id) throws Exception ;
	
	/**
	 * 判断门是否被占用
	 * @param doorId（reserved,Occupid）
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月16日 
	 */
	public boolean checkDoorIsFree(long doorId) throws Exception;
	
	/**
	 * 判断门是否被当前单据占用（reserved,Occupid）
	 * @param doorId
	 * @param associate_type
	 * @param associate_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月16日
	 */
	public boolean checkDoorIsUsedByAssociation(long doorId, int associate_type , long associate_id) throws Exception;
	/**
	 * 通过门ID，获取门的详细信息
	 * @param doorId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月17日 上午8:29:26
	 */
	public DBRow findDoorDetaiByDoorId(long doorId) throws Exception;

		
	/**
	 * 得到当前 associate_type + associate_id (保留 + 占用 的门)
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月17日
	 */
	public DBRow[] getUseDoorByAssociateIdAndType(long associate_id , int associate_type) throws Exception ;
	/**
	 * 通过门ID，关联信息查询门
	 * @param doorId
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月20日 上午9:07:33
	 */
	public DBRow findDoorByDoorIdAssociation(long doorId,  int associate_type, long associate_id  ) throws Exception;
	
	/**
	 * 根据一个关联的associate_id  + associate_type 释放他对应的door(包含占用+ 保留的)
	 * @param associate_id
	 * @param associate_type
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月19日
	 */
	public void freeDoorsByAssociateId(long associate_id ,  int associate_type) throws Exception ;
	
	
	/**
	 * 根据占用状态 返回当前associate_id
	 * @param associate_id
	 * @param occupancy_status
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月21日
	 */
	public DBRow[] getCheckInModuleByAssociateId(long associate_id , int occupancy_status ) throws Exception ;
	
}
