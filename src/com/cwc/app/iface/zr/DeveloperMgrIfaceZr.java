package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface DeveloperMgrIfaceZr {

	public long addDeveloper(HttpServletRequest request) throws Exception;
	public DBRow[] getAllDeveloper(PageCtrl pc)throws Exception ;
}
