package com.cwc.app.iface.zr;

import java.util.List;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.UpdatePalletTypeFailedException;
import com.cwc.db.DBRow;

public interface LoadPaperWorkMgrIfaceZr {
	 
	/**
	 * @param dlo_detail_id
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月5日
	 * 在转货的时候通过子单据去获取Order的详细，并且同步这些Order的信息到Sync库中
	 * 对于Load的情况，因为Load有可能包含多个Master_bol，所以也保存了Master_bol的信息
	 * 1.查询的时候一定是有CustomerID 和 CompanyId
     * 2.如果是按照Order来的，那么只会保存在 wms_load_order,同时在这张表要记录entrydetail 表中的ID
     * 3.在查询的时候就应该把entry_detail 的状态改成在处理中
     * 4.如果是通过Load来的那么会保存wms_load_master_bol,同时也会添加wms_load_order的数据
	 */
	public DBRow findOrderPalletByLoadNo(long dlo_detail_id ,  long adid) throws Exception ;
	/**
	 * 在Confirm的时候提交的数据,
	 * 主要是添加Pallet Type的数据
	 * @param data
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月9日
	 */
	//public void submitLoadPalletInfo(DBRow data) throws Exception  ;
	/**
	 * @param result
	 * @param entry_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月9日
	 */
	public void addSearchEntryValue(DBRow result , long entry_id ,long equipment_id , AdminLoginBean adminLoggerBean) throws Exception ;
	
	/**
	 * 刷新的时候调用的方法，如果Load查询出来有多个Master_bol 那么会 根据Master_bol 值返回他当前Master_bol 对应的数据
	 * @param dlo_detail_id
	 * @param adid
	 * @param master_bol_no
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
	public DBRow refreshOrderInfos(long dlo_detail_id, long adid, String master_bol_no,int isClearData , long lr_id) throws Exception ;
	
	/**
	 * 把from_pallet consolidate 到另外的Pallet上面
	 * @param from_pallet
	 * @param to_pallet
	 * @param reason
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
	public void loadConsolidate(final String from_pallet,final String to_pallet,final int reason ,final long dlo_detail_id,final long order_number ,final long adid ,final int current_pallets , final int original_pallets,long LR_ID ,final int order_type , final int  system_type , 
			final DBRow data , final AdminLoginBean adminLoggerBean) throws Exception;

	/**
	 * 每次扫描一个Pallet的时候,提交数据
	 * 插入pallet number 的数据到pallet_type这张表
	 * 
	 * 修改Pallet  和 pro_no 都是同一个接口
	 * @param data
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月10日
	 */
	public void loadScanAndPalletTypeChange(DBRow data , AdminLoginBean adminLoggerBean) throws  UpdatePalletTypeFailedException ,Exception ;
	
	/**
	 * 1.首先关闭我们系统中的数据，就是在wms_order 表,和wms_master_bol表修改一个状态
	 * 2.如果dlo_detail_id 是一个Load，那么就应该去查询这个Load的master_bol_no关闭，以及他下面的Order
	 * 3.其他的情况只需要关闭这个dlo_detail_id 下面的Order就可以了
	 * @param dlo_detail_id
	 * @param master_bol_no
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月12日
	 */
	public void closeSyncBill(final long dlo_detail_id,final String master_bol_no ,final String pro_no , final long adid) throws Exception ;
	/**
	 * 是否load的所有的MasterBol 都关闭了。
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月12日
	 */
	public boolean isLoadMasterBolAllClose(long dlo_detail_id) throws Exception ;
	
	/**
	 * 查询这个单据在wms系统中是否关闭了，每次关闭都会在我们的系统中添加 is_close_wms 为1 
	 * @param dlo_detail_id
	 * @param master_bol_no
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月15日
	 */
	public boolean isCloseMasteOrOrder(long dlo_detail_id ,String master_bol_no ) throws Exception ;
	/**
	 * @param dlo_detail_id
	 * @param master_bol_no
	 * @param pro_no
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月16日
	 */
	public void closeSyncDetailInfo(long dlo_detail_id , String master_bol_no, String pro_no , DBRow detailRow) throws Exception ;
	
	/**
	 * @param detailRow
	 * @param pro_no
	 * @param adid
	 * @param shipDate
	 * @param seals
	 * @param vehicle
	 * @param dockName
	 * @param carrierName
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月17日
	 */
	public void closeWmsMasterBol(DBRow detailRow, String pro_no, long adid , String shipDate , String seals ,String vehicle,String dockName,String carrierName , String userName , int closeType) throws Exception ;
	 /**
	  * 
	  * @param detailRow
	  * @param pro_no
	  * @param adid
	  * @param shipDate
	  * @param seals
	  * @param vehicle
	  * @param dockName
	  * @param carrierName
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2014年9月17日
	  */
	public void closeWmsOrder(DBRow detailRow, String pro_no, long adid , String shipDate , String seals ,String vehicle,String dockName,String carrierName , String userName) throws Exception ;

	 
	/**
	 * 计算这个Pallet number 是否有consolidate的数据
	 * @param palletNumber
	 * @param consolidateRows
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月21日
	 */
	public DBRow getConsolidatePalletType(String palletNumber , List<DBRow> consolidateRows) throws Exception ;
 
	/**
	 * 李君浩 打印的时候根据EntryId 去查询子Task
	 * @param result
	 * @param entry_id
	 * @param adminLoginBean
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年10月11日
	 */
	public void getEntryPrint( DBRow result ,long entry_id , AdminLoginBean adminLoginBean) throws Exception ;
	
	/**
	* 
	* @param dlo_detail_id
	* @param need_help
	* @throws Exception
	* @author zhangrui
	* @Date   2014年10月21日
	*/
	public void updateDetailNeedHelp(long dlo_detail_id , int flag_help) throws Exception ;
	
	/**
	 * 张睿查询这个Entry下面是否还有在处理中的子任务
	 * @param dlo_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月20日
	 */
	 public boolean isEntryDetailHasProcessing(long dlo_id) throws Exception ;
	 
	 /**
	  * @param order_number
	  * @param pallet_no
	  * @return
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2014年12月13日
	  */
	 public int deletePalletNo(long dlo_detail_id , String order_number , String pallet_no) throws Exception ;
	 /**
	  * Load 在Confirm的界面添加图片然后上传
	  * @param data
	  * @param adminLoginBean
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2014年12月27日
	  */
	 public void loadConfirmSubmitPhoto(DBRow data, AdminLoginBean adminLoginBean)  throws Exception ;
	 /**
	  * task下所有的sn是否都已扫描完成
	  * @param dlo_detail_id
	  * @return
	  * @throws Exception
	  */
	 public DBRow CheckSerialnoFinalStatus(long dlo_detail_id,long  masterBol) throws Exception ;
	 
	 public DBRow getOrderInfosByMasterBol(long dlo_detail_id,long master_bol_no, long adid) throws  Exception;
	 
 	 
}
  