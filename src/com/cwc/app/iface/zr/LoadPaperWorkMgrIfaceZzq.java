package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

/**
 * @author 	Zhengziqi
 * 2015年1月15日
 *
 */
public interface LoadPaperWorkMgrIfaceZzq {
	 
	/**
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @param adid
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPalletCombinationFromByOrder(String order_no, String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception ;
 	 
}
  