package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
public interface PurchaseMgrIfaceZr {

	public String getLastPurchaseDetailEta(long purchase_id) throws Exception ;
	public void AddPurchaseLog(HttpServletRequest request) throws Exception ;
	public DBRow[] getPurchaseLogsByPurchaseIdAndType(long purchase, int[] types) throws Exception;
}
