package com.cwc.app.iface.zr;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;

public interface FileMgrIfaceZr
{
	public DBRow getFileByFileId(long fileId) throws Exception ;
	public DBRow[] getFilesByFileWithIdAndFileWithType(long file_with_id , int file_with_type) throws Exception ;
	/**
	 * 这个方法目前只支持file表中的文件转换
	 * 用于在页面链接的时候如果是想在线阅读某个office文件,文件没有转换那么就应该先对文件进行转换。
	 * 在转换成功了就跳到在线阅读的页面.
	 * @param file_id
	 * @param tableName
	 * @return
	 * @throws Exception
	 */
	public DBRow convertExitsFile(long file_id , String tableName , String path) throws Exception ;
	public void UploadScannerFileOnTempFile(HttpServletRequest request) throws Exception ;
	public DBRow[] getFilesByFileWithIdAndFileWithTypeAndFileWithClass(long file_with_id,int file_with_type,int file_with_class)throws Exception;
	//查询商品文件
	public DBRow[] getProductFileByFileTypeAndWithIdAndPcId(long file_with_id ,int file_with_type ,int product_file_type , String pc_Id) throws Exception ;
	public int getIndexOfFilebyFileName(String realyFileName) throws Exception ;
	public long saveFile(DBRow row) throws Exception ;
	public void updateFile(DBRow row ,long file_id ) throws Exception ;
	public void deleteFilesByFileWidthIdAndFileWithType(long file_with_id , int file_with_type) throws Exception ;
	public DBRow convertExitsFileReturnFile(long file_id, String tableName, String path)throws Exception;
	
	/**
	 * @param row
	 * @param loginRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月11日
	 */
	public DBRow[] androidUpZipPictrue(DBRow row ,AdminLoginBean adminLoginBean) throws  Exception ;
	/**
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月11日
	 */
	public DBRow[] androidGetFileUri(long file_with_id, int file_with_type, int file_with_class , long file_with_detail_id , String option_relative_param) throws  Exception ;
	
	
	public int deleteFileBy(long file_id) throws Exception ;
	
	
	/**
	 * 得到 所有的图片 CheckInModel
	 * [
	 * 	{	dir_name:
	 * 	 	dir_photo_count:
	 * 		photos:
	 * 		[
	 * 			{
	 * 				url:
	 * 				file_name:
 	 * 			}
	 * 		]	
	 *  }
	 * ]
	 * 	
	 * 对于以前的数据是没有分类的，那么添加一个No Group
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月15日
	 */
	public DBRow[] getCheckInModelPhotos(long entry_id) throws Exception ;
	
	/**
	 * 通过entry_id 和 Detail_id 获取CountSheet的图片 
	 * @param entry_id
	 * @param detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCheckInCountingSheet(long entry_id , long detail_id ,String option_file_param) throws Exception;
	/**
	 * 通过entry_id 和 Detail_id 获取TaskProcessing的图片 
	 * @param entry_id
	 * @param detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCheckInTaskProcessing(long entry_id, long detail_id)throws Exception;
	/**
	 * 从文件服务器下载文件到web服务器临时文件夹
	 * @param descBasePath
	 * @param fileId
	 * @param sessionId
	 * @return	下载文件保存路径
	 * @throws Exception
	 * @author chenchen
	 */
	public String downloadFromFileServ(String descBasePath, long fileId, String sessionId) throws Exception;
	/**
	 * 上传文件到文件服务器
	 * @param fullPath
	 * @param sessionId
	 * @return
	 * @throws Exception
	 * @author chenchen
	 */
	public DBRow[] uploadToFileServ(String fullPath, String sessionId) throws Exception;
	/**
	 * 从文件服务器删除文件
	 * @param fileIds
	 * @param sessionId
	 * @return 删除成功文件个数
	 * @throws Exception
	 * @author chenchen
	 */
	public int deleteFromFileServ(long[] fileIds, String sessionId) throws Exception;
	/**
	 * 把整个文件夹 的文件上传到文件服务器
	 * @param dirPath	文件夹路径
	 * @param updateFileFields	file表需要更新的字段，其中expire_in_secs默认写0，file_name默认为prefix+文件名
	 * @param sessionId	会话ID
	 * @param prefix	文件名前缀
	 * @throws Exception
	 * @author chenchen
	 */
	public void uploadDirToFileServ(String dirPath, DBRow updateFileFields, String sessionId, String prefix) throws Exception;
}
