package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;

public interface ReturnProductSubItemMgrIfaceZr {
	public long addReturnProductSubItem(DBRow row) throws Exception ;
	public DBRow[] getReturnProductSubItemsByRpiId(long rpi_id) throws Exception ;
	public void updateSubReturnProductItem(DBRow row,long rpsi_id) throws Exception ;
}
