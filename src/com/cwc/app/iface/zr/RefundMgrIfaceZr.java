package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface RefundMgrIfaceZr {
	public long addRefund(HttpServletRequest request) throws Exception ;
	public DBRow getRefundByRefundId(long refund_id) throws Exception ;
	public void refundReject(HttpServletRequest request) throws Exception ;
	public DBRow[] getAllRejectReasonsByRefundId(long refund_id) throws Exception ;
	/**
	 * 同意退款成功后改变Refund的状态
	 * @param request
	 * @throws Exception
	 */
	public void refundAgree(HttpServletRequest request) throws Exception ;
}
 