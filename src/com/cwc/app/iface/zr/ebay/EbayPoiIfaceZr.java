package com.cwc.app.iface.zr.ebay;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.ebay.soap.eBLBaseComponents.ItemCompatibilityListType;
import com.ebay.soap.eBLBaseComponents.NameValueListArrayType;
import com.ebay.soap.eBLBaseComponents.ProductListingDetailsType;
import com.ebay.soap.eBLBaseComponents.ShippingDetailsType;
import com.ebay.soap.eBLBaseComponents.VariationsType;

public interface EbayPoiIfaceZr {
	
	public int createNewSheetAndSheetTitle(Workbook wb , Sheet sheet , String[] tilteName ,Integer width) throws Exception;
	public void setData(Sheet sheet , List<String[]> list , boolean isNeedSpcialColor, boolean isMerge,Workbook  wb) throws Exception;
	public void setDataString(Sheet sheet , List<String> list) throws Exception;
	public void createVariationsSetAndVariations(VariationsType variationsType , Workbook wb , String itemNumber , String title, Sheet variationsSet , Sheet variations , boolean isCreateSheet) throws Exception;
	public void createItemSpecifics( NameValueListArrayType arrayType ,Workbook wb , Sheet sheet , String itemNumber ,String title, boolean isCreateSheet) throws Exception;
	public void createNewSheetByItemCompatibilityListType(ItemCompatibilityListType itemCompatibilityListType , Workbook wb , Sheet sheet ,String itemNumber, String title ,boolean isCreateSheet) throws Exception;
	public void createShipping(Workbook wb , String itemNumber , String title ,Sheet sheet,ShippingDetailsType  type)throws Exception;
	public void createProductListingDetails(Workbook wb , String itemNumber , String title ,Sheet sheet,ProductListingDetailsType   type) throws Exception;
	/**
	 * 根据一个filePath读取里面的ItemNumbers 
	 * @param filePath
	 * @return
	 * @throws Exception
	 */
	public String[] readXLSItemNumbers(String filePath) throws Exception;
	
}
