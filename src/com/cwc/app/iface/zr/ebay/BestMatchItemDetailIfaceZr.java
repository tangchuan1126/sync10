package com.cwc.app.iface.zr.ebay;

import com.cwc.db.DBRow;

public interface BestMatchItemDetailIfaceZr {
	public String  bestMatch(String sellerId , String name ,String keyWord,int topNumber, String[] sellerIds, String ebaySite) throws Exception;
	public DBRow[] getAllEbayUserOnPaypal20() throws Exception;
}
