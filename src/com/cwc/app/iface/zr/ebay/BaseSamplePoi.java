package com.cwc.app.iface.zr.ebay;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

import com.cwc.util.ReflectUtil;
 

 
/**
 * 
 * @author Administrator
 * @param <T>
 * 把xls.的标头和title 做好。具体填充数据 让子类去实现
 */
public abstract class BaseSamplePoi<T> {
	
	static Logger log = Logger.getLogger("ACTION");
	private static short headerHeight = 45;
	private static short titleHeight = 25;
	 
	
	public void createXLS(List<T> list, String outXlsFilePath,String title, String firstSheetName) throws Exception{
		try{
			Workbook wb = new HSSFWorkbook();
		    Sheet sheet = wb.createSheet(firstSheetName);
 			setHeaderAndTitle(sheet,wb,title);
			setData(sheet,wb,list);
			FileOutputStream fileOut = new FileOutputStream(outXlsFilePath);
			wb.write(fileOut);
			fileOut.close();
			log.info("create xls success   " + outXlsFilePath);
		}catch (Exception e) {
			e.printStackTrace();
			log.error("create xls error");
		}
	}
	 
	public abstract Class getClazz();
	protected abstract void setData(Sheet sheet , Workbook wb , List<T> list) throws Exception;
 
	private void setHeaderAndTitle(Sheet sheet , Workbook wb , String title) throws Exception {
		try{
			Map<String,String[]> map = ReflectUtil.returnTitleAndAWidthOfSamplePoiAnnotation(getClazz());
			String[] widths = map.get("widths");
			String[] titles = map.get("titles");
			for(int index = 0 , count = widths.length ;  index < count ; index++ ){
				sheet.setColumnWidth(index, Integer.parseInt(widths[index]));
			}
		    Row row = sheet.createRow((short)0);
		    Cell cell = row.createCell(0);
		    cell.setCellValue(title);
		    Font font = wb.createFont();
		    font.setFontHeightInPoints((short)16);
		    font.setFontName("Arial");
		    font.setItalic(false);
		    font.setColor(IndexedColors.WHITE.getIndex());
		    row.setHeightInPoints(headerHeight);
		    CellStyle style = wb.createCellStyle();
		    style.setFont(font);
		    style.setAlignment(CellStyle.ALIGN_CENTER);
		    style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		    style.setFillForegroundColor(IndexedColors.MAROON.getIndex());
		    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		    cell.setCellStyle(style);
		    int meLength = widths.length - 1; // 合并单元格的长度
		    sheet.addMergedRegion(new CellRangeAddress(
		            0, //first row (0-based)
		            0, //last row  (0-based)
		            0, //first column (0-based)
		            meLength  //last column  (0-based)
		    ));
		    setTitle(sheet,wb,titles,map.get("colors"),map.get("isHiddens"));
		}catch (Exception e) {
			e.printStackTrace();
			 log.error("setsetHeaderAndTitle error ");
			 throw new RuntimeException("setsetHeaderAndTitle error ");
		}
		
	}
	private void setTitle(Sheet sheet , Workbook wb , String[] titles,String[] colors , String[] isHiddens) throws Exception {
		try{
			if(titles == null ){
				throw new RuntimeException("titles error");
			}
			Row title = sheet.createRow(1);
			
			title.setHeightInPoints(titleHeight);
			List<int[]> colorList= new ArrayList<int[]>();
			int colorIndex = 10 ;
			CellStyle style = null;
		    Font font = wb.createFont();
		    font.setFontHeightInPoints((short)9);
		    font.setFontName("Arial");
		    font.setItalic(false);
		  
		    
			for(int index = 0 , count = titles.length ; index < count ; index++ ){
				Cell cell = title.createCell(index);
				// 设置背景的颜色
				int[] intColors = getColorFrom16To10(colors[index]);
				boolean isNewColor = this.arrayInList(colorList, intColors);
				if(!isNewColor){
					style = getNewStyle(wb,intColors,colorIndex);
					colorIndex++;
					colorList.add(intColors);
					colorIndex++;
				}
				style.setFont(font);
				cell.setCellStyle(style);
				cell.setCellValue(titles[index]);
			 
				if(isHiddens[index].equals("true")){
					sheet.setColumnHidden(index, true);
				}
				
			}
		}catch (Exception e) {
			log.error("setTitle error ");
			throw new RuntimeException("setTitle error ");
		}
		
		
	}
	private CellStyle getNewStyle( Workbook wb ,  int[] intColors ,int  colorIndex) {
		CellStyle style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setFontHeightInPoints((short)12);
		 
		font.setFontName("华文中宋");
		 
		style.setAlignment(CellStyle.ALIGN_CENTER);
	    style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFont(font);
		style.setBorderBottom(CellStyle.BORDER_THIN);
	    style.setBottomBorderColor(IndexedColors.GREEN.getIndex());
	    style.setBorderLeft(CellStyle.BORDER_THIN);
	    style.setLeftBorderColor(IndexedColors.GREEN.getIndex());
	    style.setBorderRight(CellStyle.BORDER_THIN);
	    style.setRightBorderColor(IndexedColors.GREEN.getIndex());
	    style.setBorderTop(CellStyle.BORDER_MEDIUM_DASHED);
	    style.setTopBorderColor(IndexedColors.GREEN.getIndex());
	  
	    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		HSSFPalette palette = ((HSSFWorkbook) wb).getCustomPalette();
 		palette.setColorAtIndex((short)colorIndex, (byte) intColors[0],(byte) intColors[1], (byte) intColors[2] );
		style.setFillForegroundColor((short)colorIndex);
		return style;
	}
	private int[] getColorFrom16To10(String value){
		 String[] colorString = 	this.split(value,3);
		 if(colorString == null || colorString.length < 1){
			 return new int[]{255,255,51};
		 }
		 int[] colorInt = new int[colorString.length];
		 for(int index = 0, count = colorString.length ; index < count ; index++ ){
			 colorInt[index] = Integer.valueOf(colorString[index],16); 
		 }
		return colorInt;
	}
	public  String[] split(String msg, int num) {  
        int len = msg.length();  
        if (len <= num)  
          return new String[] {msg};  

        int count = len / (num - 1);  
        count += len > (num - 1) * count ? 1 : 0;   

        String[] result = new String[count];  

        int pos = 0;  
        int splitLen = num-1;  
        for (int i = 0; i < count; i++) {  
          if (i == count - 1)  
            splitLen = len - pos;  

          result[i]  = msg.substring(pos,  pos+ splitLen);  
          pos += splitLen;  
        }  
        return result;  
    }  
	
 
	// 查看array 是不是在list zhong 
	private boolean arrayInList(List<int[]> list , int[] array){
		boolean flag = false;
		for(int[] a : list){
			 // 1=  false == true
			
			if(compare(a , array)){
				return true;
			}
		}
		return flag ;
	}
	
	private  boolean compare(int[] a,int[] b){
		int aNum = a.length;
		int bNum = b.length;
		if(aNum!=bNum){
			return false;
		}else{
			int num =0;
			for(int i=0;i<aNum;i++){
				if(a[i] == (b[i])){
					num+=1;
				}
			}
			if(num==aNum){
				return true;
			}else{
				return false;
			}
		}

	}
}
