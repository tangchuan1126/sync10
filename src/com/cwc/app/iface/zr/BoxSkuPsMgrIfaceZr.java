package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface BoxSkuPsMgrIfaceZr {
	
	public DBRow[] getAllBoxProductByPage(PageCtrl page) throws Exception ;
	
	public void addBoxSkuPs(HttpServletRequest request) throws Exception ;
	
	public void addBoxSkuPs(DBRow row) throws Exception ;
	
	public void deleteBoxSkuPs(long id) throws Exception ;
	
	public DBRow getFirstBoxSkuPs(HttpServletRequest requset ) throws Exception ;
	
	public DBRow[] getAllBoxSkuByPcId(long pc_id) throws Exception ;
	
	public int getIndexByPsIdPcId(HttpServletRequest request) throws Exception ;
	/**
	 * 验证 pc_id , ps_id , box_type_id 去查询这条记录是否已经存在了。如果存在就不能让其添加
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public boolean validateBoxSkuPsExits(HttpServletRequest request) throws Exception;
	
	public void exchangeBoxSkuPsSort(HttpServletRequest request) throws Exception ;
	
	public DBRow[] getAllBoxSkuByPcIdAndPsId(long pc_id , long to_ps_id, long basic_container_type) throws Exception ;
	public int getMaxIndexBy(long box_pc_id, long to_ps_id)throws Exception;
	public DBRow[] findShipToNamesByBoxShipTo(long box_pc_id)throws Exception ;
	public DBRow[] findShipToBoxCountByPcid(long pc_id, int limit, int type) throws Exception;
}
