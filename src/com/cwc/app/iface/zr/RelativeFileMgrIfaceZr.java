package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface RelativeFileMgrIfaceZr {
	/**
	 * 
	 * @param row
	 * @throws Exception
	 */
	public long addRelativeFile(DBRow row) throws Exception ;
	/**
	 * 
	 * @param relative_value
	 * @return
	 * @throws Exception
	 */
	public DBRow queryReativeFile(String relative_value , int type) throws Exception ;
	/**
	 * 
	 * @param file_id
	 * @return
	 * @throws Exception
	 */
	public void updateFileWithType(String file_id) throws Exception ;
	/**
	 * 批量上传签字
	 * @param 
	 * @return
	 * @throws Exception
	 */
	public void batchUploadSignature(HttpServletRequest request) throws Exception ;
	
}
