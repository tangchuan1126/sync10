package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;

public interface InventoryMgrIfaceZr {
	
 
	public DBRow[] getOperatorArea(long ps_id) throws Exception ;
	
	public DBRow getTitleByName(String title) throws Exception ;
}
