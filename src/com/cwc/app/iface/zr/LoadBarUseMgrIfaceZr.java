package com.cwc.app.iface.zr;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AddLoadBarUseFoundException;
import com.cwc.db.DBRow;

public interface LoadBarUseMgrIfaceZr { 
	
	
	public long addLoadBarUse(DBRow inserRow) throws AddLoadBarUseFoundException ,Exception ;
	
	/**
	 * 对于同一个设备一个LoadBarId 只有有一条记录
	 * @param equipment_id
	 * @param load_bar_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月10日
	 */
	public void saveOrUpdateLoadUse(long equipment_id , long load_bar_id,  DBRow insertRow) throws Exception ;
	
	/**
	 * 得到某个设备用过的LoadBar group by load_bar_id 
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月11日
	 */
	public DBRow[] getLoadBarUseInfo(long equipment_id , long entry_id) throws Exception  ;
	
	/**
	 * android get Load Bar use
	 * @param equipment_id
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月11日
	 */
	public DBRow[] androidGetLoadBarUse(long equipment_id , long entry_id) throws Exception ;
	
	/**
	 * androidUpdateLoadBarUse
	 * @param inserRow
	 * @param adminLoginBean
	 * @throws AddLoadBarUseFoundException
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月17日
	 */
	public void androidUpdateLoadBarUse(DBRow inserRow , AdminLoginBean adminLoginBean) throws AddLoadBarUseFoundException, Exception ;
		
}
