package com.cwc.app.iface.zr;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface WayBillMgrIfaceZR {

public DBRow[] getWalbill(DBRow queryRow , PageCtrl pc) throws Exception;
	
	public long addNewStoreBill(DBRow row) throws Exception;
	
	public DBRow assignStore(DBRow row) throws Exception;
	
	public DBRow[] getAllStoreIsOpenAndPsId(long psId)  throws Exception;
	public void updateWallBillByOutId(String ids , long psId) throws Exception;
	public  DBRow[] getAllOrderItemsInWayBillById(long wayBillid) throws Exception;
	// 根据ids和numbers和wayId去拆分新的运单。并且要返回一些这个运单所有拆分的运单
	public DBRow splitWayBill(String ids , String numbers , long wayId) throws Exception;
	public DBRow[] getSplitInfoByWayBillId(long id) throws Exception;
	public void deleteSplitWayBillBy(long wayBillId , long parentWayId,AdminLoginBean adminLoggerBean) throws Exception;
	public DBRow[] getWayOrderItem(String ids, String numbers) throws Exception;
	public DBRow getWaybillInfo(long wayBillId) throws Exception;
	public DBRow getSaleRoomByWalBillId(long wayBillId) throws Exception;
	// 得到 发货 发货人 时间 或者 创建、创建人、 创建时间 
	public DBRow getUserNameById(long adid) throws Exception;
	public void updateWayBillStatus(long wayBillId , DBRow updateRow) throws Exception;
	public DBRow getPostDateByOid(long oid) throws Exception;
	
	public DBRow[] getOutBoundByPsId(long ps_id , PageCtrl pc) throws Exception;
	public DBRow[] getWayBillListByOutId(long out_id) throws Exception;
	public DBRow[] getOutBoundByRow(DBRow queryRow , PageCtrl pc) throws Exception;
	
	public void updateOutBoundByRow(DBRow updateRow) throws Exception ;
	public DBRow[] getWayBillByOutIdPc(long out_id , PageCtrl pc) throws Exception;
	public DBRow[] getWayBillBySearchKey(String searchKey,int search_mode,PageCtrl pc) throws Exception;
	
	public DBRow[] getWayBillNoteBy(long waybillId , int number) throws Exception ;
}
