package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;

public interface ScheduleSetMgrIfaceZR {

	public void updateScheduleSet(long id , DBRow row) throws Exception;
	public DBRow getScheduleSet() throws Exception;
}
