package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ScheduleMgrIfaceZR {
	/**
	 * @param row
	 * @return
	 * @throws Exception
	 *             (根据row中的数据添加一条记录)
	 */
	public long addSchedule(DBRow row,HttpServletRequest  request) throws Exception;

	/**
	 * @param id
	 * @param row
	 * @throws Exception
	 *             (根据row中的数据更新一条记录，这个方法只能是更新时间 是否是全天的任务)
	 */
	public void updateSchedule(long id, DBRow row,HttpServletRequest request) throws Exception;

	/**
	 * @param row
	 * @return
	 * @throws Exception
	 *             (根据传入userId,开始时间,结束时间,已经安排 查询数据)
	 */
	public DBRow[] getAllScheduleByExecuteUserIdAndTime(DBRow row)
			throws Exception;

	/**
	 * @param row
	 * @return
	 * @throws Exception
	 *             (根据userId 查找他的没有安排的任务)
	 */
	public DBRow[] getScheduleByUnAssign(DBRow row) throws Exception;

	/**
	 * @param id
	 * @param row
	 * @return
	 * @throws Exception
	 *             (根据传入的Id值。和新的数据copy一个任务。主要是copy旧任务的安排人和执行人)
	 */
	public long copySchedule(long id, DBRow row) throws Exception;

	/**
	 * @return
	 * @throws 获取所有人的
	 *             部门-部门id ,姓名 , Id
	 */
	public DBRow[] getAllUserInfo() throws Exception;

	public DBRow getScheduleById(long id) throws Exception;

	public DBRow[] getScheduleAndUserInfoById(long id) throws Exception;

	public DBRow[] getScheduleByDetailAndUserId(DBRow row) throws Exception;

	/**
	 * 根据传入的adgid 查询该部门下的所有的主管和副主管
	 */

	public DBRow[] getManagerIdsByAdgid(long adgid) throws Exception;

	/**
	 * 
	 * @return 得到总经理部门下的主管或者是副主管
	 * @throws Exception
	 */
	public DBRow[] getMangerIds() throws Exception;

	/**
	 * 
	 * @return
	 * @throws Exception
	 *             (得到一个任务的任务参与人)
	 */
	public DBRow[] getScheduleJoinInfo(long schedule_id) throws Exception;

	/**
	 * @param row
	 * @return
	 * @throws Exception
	 *             (根据任务执行的开始和时间结束时间和任务状态以及任务的参与人查询出 他参与的任务)
	 */
	public DBRow[] getScheduleJoinSchedule(DBRow row) throws Exception;

	/**
	 * 
	 * @param row
	 * @return 根据DBRow 获取join表和scheduleSub表的任务实例;
	 * @throws Exception
	 */
	public DBRow getScheduleByAjax(DBRow row) throws Exception;

	/**
	 * @param row
	 * @return
	 * @throws Exception
	 *             (根据row来生成重复的任务)
	 */
	public DBRow[] addScheduleRepeat(DBRow row) throws Exception;

	public DBRow getRepeatInfoById(long id) throws Exception;

	public DBRow[] updateRepeatScheduleBy(long id, DBRow row) throws Exception;

	// 一下是订单抄单的东西
	public DBRow[] getWalbill(DBRow queryRow, PageCtrl pc) throws Exception;

	public long addNewStoreBill(DBRow row) throws Exception;

	public DBRow assignStore(DBRow row) throws Exception;

	public DBRow[] getAllStoreIsOpenAndPsId(long psId) throws Exception;

	public void updateWallBillByOutId(String ids, long psId) throws Exception;

	public DBRow[] getAllOrderItemsInWayBillById(long wayBillid)
			throws Exception;

	// 根据ids和numbers和wayId去拆分新的运单。并且要返回一些这个运单所有拆分的运单
	public DBRow splitWayBill(String ids, String numbers, long wayId)
			throws Exception;

	public DBRow[] getSplitInfoByWayBillId(long id) throws Exception;

	public void deleteSplitWayBillBy(long wayBillId, long parentWayId)
			throws Exception;
	/**
	 * 
	 * @param start_time 			任务开始时间
	 * @param end_time				任务结束时间
	 * @param assign_user_id		任务安排人
	 * @param execute_user_id		任务执行人
	 * @param schedule_join_execute_id任务参与人
	 * @param associate_id			任务关联的业务Id
	 * @param associate_type		ModuleKey取值,表示某个模块下的
	 * @param emailTitle			邮件标题	
	 * @param context				邮件内容
	 * @param isNeedDwr				是否页面提醒
	 * @param isNeedEmail			是否邮件提醒 
	 * @param isNeedShortMessage	是否短信提醒
	 * @param request				
	 * @param isArrangeNow			是否立即安排
	 * @param associate_process		ProcessKey值
	 * @throws Exception		
	 */
	public void addScheduleByExternalArrangeNow(String start_time,
			String end_time, long assign_user_id, String execute_user_id,
			String schedule_join_execute_id, long associate_id,
			int associate_type,String emailTitle,String context, boolean isNeedDwr,
			boolean isNeedEmail, boolean isNeedShortMessage,
			HttpServletRequest request,boolean isArrangeNow ,int associate_process) throws Exception;
	
	
	
	
	
	
	public void addScheduleByExternalArrangeNow(String start_time,
			String end_time, long assign_user_id, String execute_user_id,
			String schedule_join_execute_id, long associate_id,
			int associate_type, String emailTitle,String context, boolean isNeedDwr,
			boolean isNeedEmail, boolean isNeedShortMessage, AdminLoginBean adminLoggerBean ,boolean isArangeNow, int associate_process) throws Exception ;
	
	public DBRow getScheduleByAssociate(long associate_id , int associate_type ,int associate_process) throws Exception ;
	
	public DBRow[] getScheduleByAssociateIdAndType(long associate_id , int[] associate_types) throws Exception ;
 
	public DBRow[] getScheduleExecuteUsersInfoByAssociate(long associate_id , int associate_type ,int associate_process) throws Exception ;

	public void addScheduleReplayExternal(long associate_id , int associate_type ,int associate_process , String context ,boolean isCompelte, HttpServletRequest request,String configName) throws Exception ;
	public void addScheduleReplayExternal(long associate_id,int associate_type, int associate_process, String context,boolean isCompelte ,AdminLoginBean adminLoggerBean , String configName, String is_need_notify_executer)throws Exception;
	
	public void updateScheduleExternal(long associate_id,int associate_type , int associate_process , String context , String userIds,HttpServletRequest request,boolean isNeedEmail ,boolean isNeedShortMessage ,boolean isNeedDwr )	throws Exception ;

	public void deleteScheduleByScheduleId(long schedule_id , AdminLoginBean adminLoggerBean) throws Exception ;
	public void deleteScheduleByScheduleId(long schedule_id , HttpServletRequest request) throws Exception ;

	
	public DBRow[] deleteMutiRepeatScheduleById(long id , String flag) throws Exception;
	
	public DBRow[] getScheduleNotComplete(long adid ,int number) throws Exception ;
	/**
	 * 差评任务(改工作流)
	 * @param request
	 * @throws Exception
	 */
	public void addScheduleByWorkFlowOrder(HttpServletRequest request) throws Exception ;
	
	public DBRow[] getScheduleWorkFlow(long adid , int finish_type ) throws Exception ;
	
	public void finishWorkFlow(HttpServletRequest request) throws Exception ;
	
	public void addWorkFlowSchedule(String start_time,
			String end_time, long assign_user_id, String execute_user_id,
			String schedule_join_execute_id, long associate_id,
			int associate_type,String emailTitle,String context, boolean isNeedDwr,
			boolean isNeedEmail, boolean isNeedShortMessage,
			HttpServletRequest request,boolean isArrangeNow ,int associate_process,int is_task , int task_type) throws Exception;
	
	public void finishRefundSchedule(HttpServletRequest request) throws Exception ;
	
	/**
	 * 得到工作流任务的完成和没有完成的数量
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getTaskFlowCount(long adid) throws Exception ;
	public String getAdminMangerUserIds() throws Exception ;
	
	public void udpateScheduleAndScheduleInfo(long id, DBRow row) throws Exception;
	
	public void updateSchedule(long id,DBRow row,AdminLoginBean adminLoggerBean) throws Exception;
	public void updateScheduleExternal(long associate_id, int associate_type,
			int associate_process, String context,String userId, AdminLoginBean adminLoggerBean,boolean isNeedEmail ,boolean isNeedShortMessage ,boolean isNeedDwr)
			throws Exception;
	
	/**
	 * @param adid
	 * @param assign 1.表示已经安排的任务
	 * @return根据adid 去查询当前这个人是否有安排任务
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月11日
	 */
	public DBRow[] getAllNotFinishScheduleByAdid(long adid , int assign , long schedule_id ,  int pageSize) throws Exception ;
	
	/**
	 * 查询（某个人没有完成的任务）
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月11日
	 */
	public int countNotFinishSchedule(long adid) throws Exception ;
	
	/**
	 * 新的任务添加接口
	 * @param scheduleModel
	 * @throws Exception
	 * @author zhangrui
	 * @return 
	 * @Date   2014年12月3日
	 */
	public long addSchedule(ScheduleModel scheduleModel) throws Exception ;
	
	/**
	 * 查询某个人的没有完成的任务 && 这个任务是没有和任何的业务单据关联的
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月5日
	 */
	public DBRow[] queryScheduleUnFinishAndNoAssociate(DBRow loginRow) throws Exception ;

	/**
	 * 得到没有完成任务种类的分别的数量
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月6日
	 */
  	public DBRow[] getNotFinishScheduleType(long adid) throws Exception  ;
  	
  	/**
  	 * 单纯的更新schedule
  	 * @param schedule_id
  	 * @param updateRow
  	 * @throws Exception
  	 * @author zhangrui
  	 * @Date   2014年12月6日
  	 */
  	public void simpleUdateSchedule(long schedule_id ,DBRow updateRow ) throws Exception ;
  	
  	/**
  	 * 更新sub表通过 
  	 * @param schedule_id
  	 * @param updateRow
  	 * @throws Exception
  	 * @author zhangrui
  	 * @Date   2014年12月6日
  	 */
  	public void simpleUpdateScheduleSub(long schedule_id , DBRow updateRow) throws Exception ;

  	/**
  	 * 其他的业务模块通过业务单据获取 schedule
  	 * @param associate_id
  	 * @param associate_process
  	 * @param associate_type
  	 * @return
  	 * @throws Exception
  	 * @author zhangrui
  	 * @Date   2014年12月6日
  	 */
  	public DBRow[] getScheduleBy(long associate_id , int associate_process ,int associate_type ) throws Exception ;
  	
  	/**
  	 * 获取当前这个人最新的一个Schedule
  	 * @param adid
  	 * @return
  	 * @throws Exception
  	 * @author zhangrui
  	 * @Date   2014年12月9日
  	 */
  	public DBRow newSchedule(long adid) throws Exception ;
 
  	/**
  	 * 通过关联关系更新数据
  	 * @param associate_id
  	 * @param associate_process
  	 * @param associate_type
  	 * @param updateRow
  	 * @throws Exception
  	 * @author zhangrui
  	 * @Date   2014年12月13日
  	 */
  	public void simpleUpdateScheduleBy(long associate_id , int associate_process ,int associate_type ,DBRow updateRow) throws Exception ;
  	
  	/**
  	 * 查询有关Detail 所有的没有完成的任务
  	 * @param detail_id
  	 * @return
  	 * @throws Exception
  	 * @author zhangrui
  	 * @Date   2014年12月26日
  	 */
  	public DBRow[] getCheckInModelDetailNotFinishSchedule(long detail_id ,long entry_id) throws Exception ;
  	/**
	 * 通过module和processkeys查询schedule数量
	 * @param module
	 * @param module_id
	 * @param processKeys
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月5日 下午6:30:49
	 */
	public int findScheduleAssociateCountByAssociateMainIdType(int module, long module_id, int[] processKeys) throws Exception;
	
	/**
	* @Title: finishScheduleByAssociateParams
	* @Description: TODO(通过AssociateParams,完成当前schedule)
	* @param @param associate_type
	* @param @param associate_process
	* @param @param associate_id
	* @param @return
	* @param @throws Exception    设定文件
	* @return int    返回类型
	* @throws
	 */
	public void finishScheduleByAssociateParams(int associate_type ,int  associate_process , long associate_id , long adid ) throws Exception ;

}
