package com.cwc.app.iface.zr;

import java.io.File;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.qa.ProductNotFindException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface AndroidMgrIfaceZr {
	
	//查询checkIn的transport那些商品
	//同时更新这个单据的状态是在收货中的
	public DBRow[] queryTransportOutBoundByTransportId(long transport_id) throws Exception ;
	/**
	 * 获得拣货单所需的商品信息
	 * @param transport_id(checkIn transportId)
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductForTransport(long transport_id )throws Exception ;
	
	/**
	 * 拣货单所需商品条码信息
	 * @param transport_id(checkIn transportId)
	 * @return
	 * @throws Exception15314551640 
	 */
	public DBRow[] getProductCodeForTransport(long transport_id) throws Exception ;
	
	/**
	 * sn-sku(sn 和sku对应的关系) 
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSerialProductForTransport(long transport_id) throws Exception ;
	
	/**
	 * 获取这个lp上的基本的信息
	 * @param lp
	 * @return
	 * @throws Exception
	 */
	public DBRow getLpInfo(long cp_lp_id) throws Exception ;
	
	
	public DBRow getLpInfo(String lp_name) throws Exception ;
	
	
	public DBRow[] getCheckInTransport(long psid) throws Exception ;
	
	public DBRow getSupplier(long purchase_id)throws Exception;
	
	/**
	 * transport purchase_id 为0的
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutboundTransport(long psid) throws Exception ;
	
	/**
	 * 获取发货的时候的商品(transport_detail)
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportDetail(long transport_id) throws Exception ;
	
	//得到已经收到的transport (收货放货)
	public DBRow[] getTransportReceived(long psid ) throws Exception ;
	 
	/**根据transport_id 查询transport_warehouse表
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPositionProduct(long transport_id) throws Exception ;
	
	/**
	 * 获取position_product_code
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPositionProductCode(long transport_id) throws Exception ;
	
	/**
	 * 获取position的序列号
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPositionSerialProduct(long transport_id) throws Exception ;
	
	/**
	 * 获取transportWarehouse product
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportWarehouseProduct(long transport_id) throws Exception ;
	
	/**
	 * 1.处理Android上传的文件
	 * 2.保存在对应的文件夹下
	 * 3.在file表中添加记录
	 * @param file_with_id
	 * @param file_with_type
	 * @param file_with_class
	 * @param upload_adid
	 * @param path
	 * @throws Exception
	 */
	public void handleUploadFile(String file_with_id , String file_with_type , 
			String file_with_class , String upload_adid ,String upload_time ,  
			String path, List<FileItem> fileItems) throws Exception ;
	
	
	/**
	 * 获取detail中的对应的商品的基本信息
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutboundProductInfo(long transport_id) throws Exception ;
	/**
	 * barcode -- pcid 对应关系的
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCodeForOutboundTransport(long transport_id) throws Exception ;
	/**
	 * sn-sku 对应的表
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSerialProductForOutboundTransport(long transport_id) throws Exception ;
	
	
	/**
	 * 区域门
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorNameByTransportId(long transport_id , int type) throws Exception ;
	
	/**
	 * 区域位置
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] getLocationNameByTransportId(long transport_id , int type) throws Exception ;
	
	public DBRow[] getTransportByMachine(long ps_id) throws Exception ;
	
	//public DBRow[] getPositionedTransportAddDoors(long id) throws Exception;
	
	/**
    *  1.会返回商品的详细的信息
    *  2.会返回图片的信息
    */
	public DBRow[] getProductFileBy( long maxPcid , int length ) throws Exception ;
	public DBRow[] getProductFileDifferent(long pcid , String  fileNames) throws Exception ;
	public DBRow[] getFileByFileWithIdAndFileWithType(long file_with_id ,  int file_with_type , String fileNames) throws Exception ;
	
	public void clearDoorAndLocation(long transportId , int type) throws Exception ;
	public long addNewLp(String lpName , long bill_id , int bill_type) throws Exception ;
	
	public long addBaseProductByzip(String parentDir,AdminLoginBean adminLoggerBean) throws Exception ;
	
	
	public DBRow containerLoadProduct(String container) throws Exception ;
 
	/**
	 * 先考虑所有的Container，已经ContainerType都是系统存在的，不先做验证
	 * 1.这里要做检查.
	 * 		a.	是否所有的Container都是满的(按照系统规定的类型都添加慢TLP 除外)
	 * 		b.  同时要更新Container表CLP,BLP,ILP 都是满的。如果都是有SN那么都应该加上sn的标志
	 * 		c.	title_id 和 lot_number要反填上
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public DBRow submitContainer(DBRow data) throws Exception ;
	
	/**
	 * 1.在Container_id 上添加商品
	 * 2.在Container上面添加Container
	 * @param container_id 
	 * @param subContainers
	 * @param products
	 * @return
	 */
	public void appendContainer(long container_id,  List<DBRow> subContainers, List<DBRow> products) throws Exception ;
	
	
	/**
	 * 根据PCID获取商品的详细信息
	 * @param p_id
	 * @return
	 * @throws ProductNotFindException
	 * @throws Exception
	 */
	public DBRow getBaseProductInfo(long p_id) throws ProductNotFindException, Exception ;
 
	/**
	 * 获取登录人的title
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLoginTitles(long adid) throws Exception ;
	
	/**
	 * 删除商品的图片
	 * @param pf_id
	 * @throws Exception
	 */
	public void deleteProductFile(long pf_id) throws Exception ;
	
	/**
	 * 添加商品的条码
	 * 然后返回所有条码信息
	 * @param row
	 * @throws Exception
	 */
	public DBRow[] addProductCode(DBRow insertRow,DBRow loginBean) throws Exception ;
	
	/**
	 * 更新Product的基本信息
	 * @param updateRow
	 * @throws Exception
	 */
	public void updateSimpleProduct(DBRow updateRow) throws Exception ; 
	
	/**
	 * 获取Inbound Transport List
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getInboundTransportList(DBRow loginDBRow ) throws Exception ;
	
	/**
	 * 获取position Transport List
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPositionTransportList(DBRow loginDBRow) throws Exception ;
	
	/**
	 * 
	 *  1.在Inbound的时候根据TransportId 去下载基础数据
	 *  2.{ret:1;err:2;productCode:[{},{},{}] ; productInfo:[{},{}];datas[{},{},{}]}
	 *  3.productCode表示的是所收商品的p_code(每种商品都加上 了PCID--PCID)
	 *  4.productInfo 表示商品的基本信息比如length,width,hight,weight
	 *  5.datas所要收的商品的总和
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getInboundBaseInfo(long transport_id) throws Exception ;
	
	/**
	 * 1.放货的时候下载的所有的基础数据
	 * 2.{ret:1;err:2;productCode:[{},{},{}] ; productInfo:[{},{}];datas[{},{},{}]}
 	 * 3.productCode表示的是所收商品的p_code(每种商品都加上 了PCID--PCID)
	 * 4.productInfo 表示商品的基本信息比如length,width,hight,weight
	 * 5.datas所要放的商品的总和
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getPositionBaseInfo(long transport_id) throws Exception ;
	
	/**
	 * 获取一个product的图片
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public File getProductMainPicture(long pc_id) throws Exception ;
	
	/**
	 * 在File表中通过FileWithId 和 FileWithType 去获取图片s
	 * @param file_with_id
	 * @param file_with_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFilesAndCreateSmall(long file_with_id ,  int file_with_type , String sys_file_dir) throws Exception ;

	/**
	 * 删除FIle 表中的数据
	 * @param file_id
	 * @throws Exception
	 */
	public void deleteFileTableFile(long file_id) throws Exception ;
	
	/**
	 * android 请求的接口返回商品的基础信息
	 * @param page
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBaseProductBy(PageCtrl page) throws Exception ;
	
	
	public DBRow[] getBaseProductBy(long maxPcId , int length ,String flag ) throws Exception ;
	
	
	public double getContainerTypeTotalPiece(int containerType,long containerTypeId) throws Exception ;
	

	/**
	 * 根据一个PCID去查询所有的Contiainer的配置信息
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getContainerInfoByPcId(long pc_id) throws Exception ;
}
