package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface PayMentMethodIfaceZR {

	public DBRow[] getAllPayMentMethod(PageCtrl page) throws Exception;
	public DBRow[] getPayMentMethodByKey(PageCtrl page , int key , int isTip) throws Exception;
	
	public void addPayMentMethod(DBRow row) throws Exception;
	public void updatePayMentMethod(DBRow row , int key) throws Exception;
	public boolean isHasTipPayMent(int key) throws Exception;
	public void deletePayMentMethod(long id) throws Exception;
	
}
