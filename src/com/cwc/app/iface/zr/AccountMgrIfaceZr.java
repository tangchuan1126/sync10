package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface AccountMgrIfaceZr
{
	public DBRow addAccount(long account_with_id , HttpServletRequest  request) throws Exception ;
	public DBRow[] getAccountByAccountIdAndAccountWithType(long account_with_id , int account_with_type) throws Exception ;
	public void updateAccount(long account_id , DBRow updateRow) throws Exception ;
	public void addAccountByPage(HttpServletRequest request) throws Exception ;
	public void deleteAccount(long account_id) throws Exception ;
	public DBRow getAccountById(long account_id) throws Exception;
}
