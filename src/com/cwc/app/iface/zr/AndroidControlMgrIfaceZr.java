package com.cwc.app.iface.zr;

import com.cwc.app.api.zr.androidControl.AndroidPermission;
import com.cwc.db.DBRow;

public interface AndroidControlMgrIfaceZr {
	/**
	 * @param insertPersonal
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月28日
	 */
	public long addPersonalPermission(DBRow insertPersonal) throws Exception ;
	
	/**
	 * @param insertAdgid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月28日
	 */
	public void addAdgidPermission(DBRow[] insertAdgid) throws Exception ;
	/**
	 * 得到所有的Perssion
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月28日
	 */
	public DBRow[] getAllPermission() throws Exception ;
	
	/**
	 * 1.把数组的permission 装货成tree的结构
	 * @param arrays
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月29日
	 */
	public AndroidPermission[] fixAndroidPermissionTree(AndroidPermission[] arrays) throws Exception ;
	
	/**
	 * 1.把DBRow convert to permission
	 * @param arrays
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月29日
	 */
	public AndroidPermission[] convertDBRowToAndroidPermission(DBRow[] arrays) throws Exception ;
}
