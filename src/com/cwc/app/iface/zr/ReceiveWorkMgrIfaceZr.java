package com.cwc.app.iface.zr;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;

public interface ReceiveWorkMgrIfaceZr {
	/**
	 * 
	 * @param receiveRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月11日
	 */
	public long addReceiveInfo(DBRow receiveRow) throws Exception ;
	/**
	 * 
	 * @param receiveRows
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月11日
	 */
	public void addReceiveInfo(long ic_id , long dlo_detail_id , long entry_id, DBRow[] receiveRows, DBRow loginRow ,String filePath) throws Exception ;
	/**
	 * 
	 * @param entry_id
	 * @param adminLoginBean
	 * @return返回Ctnr的列表
	 * @throws CheckInNotFoundException
	 * @throws NoPermiessionEntryIdException
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月11日
	 */
	public DBRow getSumSangCtnrDetails(long entry_id  , AdminLoginBean adminLoginBean ) throws  Exception;

	/**
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月13日
	 */
	public DBRow[] getReceivedSumSangDetails(long dlo_detail_id , long ic_id) throws Exception ;
	
	/**
	 * 表示三星的收货完成，这个时候把我们的子单据关闭，并且在import_container_info 把状态改了
	 * @param dlo_detail_id
	 * @param ic_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月13日
	 */
	public void finishReceived(DBRow data,DBRow loginRow) throws Exception ;
	
	/**
	 * @param ps_id(根据仓库Id查询Ps_id)
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月17日
	 */
	public DBRow[] getStatingByPsId(long ps_id) throws Exception ;
	

	
	/**
	 * @param pallet_no
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月25日
	 */
	public DBRow getPalletInfo(String pallet_no) throws Exception ;
	
	/**
	 * 修改PalletLocation 
	 * @param rows
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月25日
	 */
	public void updatePallet(DBRow[] rows) throws Exception ;
	/**
	 * @param ic_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月25日
	 */
	public DBRow getContainerInfo(long ic_id) throws Exception ;
	/**
	 * 收货完成的时候那么更新一下 总共收货到了多少个box
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月25日
	 */
	public void updateContainerInfoReceiveQty(long ic_id) throws Exception ;
	
	/**
	 * 通过 icp_id 修改
	 * @param icp_id
	 * @param updateRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月25日
	 */
	public void updatePallet(long icp_id , DBRow updateRow ) throws Exception ;
	
	
	/**
	 * 三星收货的时候，出现异常的处理
	 * 在dlo_detail_id写入Exception的原因
	 * @param ic_id
	 * @param dlo_detail_id
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月26日
	 */
	public void receivedException(DBRow data ,long adid) throws Exception ;
	
	
	/**
	 * 三星装货exception
	 * @param data
	 * @param loginRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月26日
	 */
	public void sumsangLoadException(DBRow data , DBRow loginRow) throws Exception ;
	
	/**
	 * 三星Load提交的接口
	 * @param data
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月19日
	 */
	public void sumsangLoadFinish(DBRow data , DBRow loginRow) throws Exception ;
	/**
	 * 三星在装货的时候 完成的时候Next 提交的接口
	 * @param data
	 * @param loginRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月26日
	 */
	public void SumsangLoadPhoto(DBRow data , DBRow loginRow) throws Exception ;
	
	public void reloadSumsang(long ic_id , long dlo_detail_id) throws Exception ;
	
	/**
	 * 得到当前的业务单据收货的情况
	 * @param lr_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月3日
	 */
	public DBRow[] getCommonReceived(long lr_id ) throws Exception ;
	
	/**
	 * 收一个Pallet
	 * @param data
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月3日
	 */
	public long addCommonReceived(DBRow data) throws Exception ;
	/**
	 * 
	 * @param lr_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月3日
	 */
	public DBRow getOrderSystemInfo(long lr_id) throws Exception ;
	
	/**
	 * 删除一个扫描的记录
	 * @param receive_pallet_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月3日
	 */
	public int commonReceiveDelete(long receive_pallet_id ) throws Exception ;
	/**
	 * 更新pallet type
	 * @param commonReceive
	 * @param palletType
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月3日
	 */
	public int updateCommonReceive(long  commonReceive , DBRow updateRow) throws Exception ;
	
}
