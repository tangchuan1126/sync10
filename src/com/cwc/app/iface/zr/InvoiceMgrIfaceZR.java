package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;

public interface InvoiceMgrIfaceZR {
	//账单
	/**
	 *  创建成功要在Bill上添加一个Invoice的字段。
	 *  如果是不成功的话那么就要 返回错误的信息
	 *  如果是页面上的已经有invoice的那么是应该没有创建按钮的
	 *  cmd 为createandsend 和create
	 */
	public DBRow createInvoiceByBillId(long billId, String cmd) throws Exception;
	
	public DBRow sendInvoiceByInvoiceId(long billId) throws Exception;
	
	public DBRow updateInvoiceByBillId(long billId) throws Exception;
}
