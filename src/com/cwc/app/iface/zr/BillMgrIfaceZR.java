package com.cwc.app.iface.zr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

 
public interface BillMgrIfaceZR {
	 
	public long addBill(DBRow billRow , List<DBRow> itemList) throws Exception ;
	public long addBillByOrder(DBRow billRow , List<DBRow> itemList)throws Exception ;
	 
	
	public DBRow[] getAllBill(PageCtrl  pc) throws Exception;
	public DBRow[] getItemsByBillId(long billId) throws Exception;
	public DBRow[] getAccountInfoList(long key) throws Exception;
	public void updateBill(long billId ,DBRow billRow ,List<DBRow> itemList) throws Exception;
	public void deleletBillByBillId(long billId) throws Exception;
	public DBRow getPDFDatas(long billId) throws Exception;
	public DBRow sendBillViaEmail(HttpServletRequest request) throws Exception;
	public DBRow getBillByBillId(long billId) throws Exception;
	public DBRow[] getBillBySearchKey(String searchKey,int search_mode,PageCtrl pc) throws Exception;
	public boolean hasAuthority(long adid , long createAdid) throws Exception;
	public DBRow[] filterBill(HttpServletRequest request , PageCtrl page) throws Exception;
	public DBRow getBillAndBillItemByBillId(long bill_id) throws Exception;
	public DBRow[] getProductUnionByPcid(long pc_id) throws Exception;
	public DBRow getBillItemByClientIdAndBillId(long billId, String clientId) throws Exception; 
 
	
	 
}
