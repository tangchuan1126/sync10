package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.db.DBRow;

public interface ReturnProductItemsFixMgrIfaceZr
{
	
	public long addReturnProductItmesFix(HttpServletRequest request) throws Exception ;
	public DBRow[] getReturnProductItmesFix(long rp_id) throws Exception ;
	
	
	public void addReturnProductItems(HttpServletRequest request) throws Exception ;
	public DBRow flushSession(HttpSession session) throws Exception ;
	public DBRow[] getSimpleProducts(HttpSession session) throws Exception;
	public void clearAllReturnProductCart(HttpSession session) throws Exception; //清空session
	public void clearReturnProductCart(HttpServletRequest request) throws Exception ; //删除一项
	public void updateReturnProductCart(HttpServletRequest requset) throws Exception ;
	public void putToReturnProductReasonCartForSelect(HttpServletRequest request) throws Exception;
	public boolean isEmpty(HttpSession session) throws Exception;
	
	
	
	public void addReturnProductItemsFormSession(HttpServletRequest request) throws Exception ;
	public DBRow[] getReturnProductReason(long rp_id) throws Exception ;
	public long addReturnProductItemFormPictrue(HttpServletRequest request) throws Exception ;
	public DBRow[] getReturnProductItemByRPIdAndFixId(long items_fix_rpi_id ,long rp_id) throws Exception ;
	public void setReturnProductItemFixNotMatch(HttpServletRequest request) throws Exception ;
	public DBRow[] getReturnItemsByRpId(long rp_id) throws Exception ;
	public void updateReturnProductItem(HttpServletRequest request) throws Exception ;
	
	
	//删除已经关联上的商品
	public void deleteReturnProductItem(HttpServletRequest request) throws Exception ;
	
	public void relationReturnProduct(HttpServletRequest request) throws Exception ;
	
	public void deleteReturnProductItemFix(HttpServletRequest request) throws Exception ;
	
	public boolean isRelationOver(long rp_id) throws Exception ;
	
	public void mergeReturnProductItems(HttpServletRequest request) throws Exception ;
	
	public DBRow getReturnProductItemByRpiId(long rpi_id) throws Exception ;
	
	//更新表示某个匹配商品需要标识是要测试的
	public void updateReturnProductItemFixIsNeedCheck(long rp_id , long rpi_id) throws Exception ;
	
	//删除该关联商品的对应的CheckReturnProductInfo的信息
	public void deleteCheckReturnProductInfo(long rpi_id) throws Exception ;
	
	//更新return_product_item 表中的returnProductCheck为完成
	public void updateReturnProductItemAndReturnProductCheckIsFinish(long rp_id , long rpi_id) throws Exception ;
	
	public void updateReturnProductItemHandResult(HttpServletRequest  request) throws Exception ;
	/**
	 * 如果是所有的item_fix 都关联完了。那么就更新主单据上面的信息
	 * 不是的话。就要提示
	 * @param request 
	 * @throws Exception
	 */
	public DBRow returnProductItemRecognitionFinish(HttpServletRequest request) throws Exception ;
	
	public DBRow[] getReturnProductItemfixByRpiId(long rpi_id) throws Exception ;
	
	public DBRow getReturnProductItemfFixByFixRpiId(long fix_rpi_id) throws Exception ;
	//更新return_product_item_fix
	public void updateReturnProductItemFix(HttpServletRequest request) throws Exception ;
	/**
	 * 有RMA情况下
	 * 但是客户有部分退件并没有退回来
	 * @param request
	 * @throws Exception
	 */
	public void updateReturnProductItemZrNoPictrue(HttpServletRequest request) throws Exception ;
	/**
	 * 如果是客服先联系客户,然后仓库获取图片。时候点击图片识别完成
	 * @param request
	 * @throws Exception
	 */
	public DBRow recognitionReturnProductPictureFinishByStorage(HttpServletRequest request) throws Exception ;
	
	public void addNewReturnProductItemZr(HttpServletRequest request) throws Exception ;
	
	public void relationOneReturnProduct(HttpServletRequest request) throws Exception ;
	
	public void handleReturnProductItem(HttpServletRequest request) throws Exception ;
	
	/**
	 * 有RMA的情况如果有新上传的图片,关联已经有的商品
	 * @param request
	 * @throws Exception
	 */
	public void RmaPictureRelationReturnProductItem(HttpServletRequest request) throws Exception;
	
	
	public DBRow returnProductItemHandleFinish(HttpServletRequest request ) throws Exception ;
}
