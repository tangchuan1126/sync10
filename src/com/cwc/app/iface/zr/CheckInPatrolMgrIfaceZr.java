package com.cwc.app.iface.zr;

import com.cwc.db.DBRow;

public interface CheckInPatrolMgrIfaceZr {
	
	/**
	 * 根据ps_id 获取当前所在仓库的spot_area
	 * @param ps_id
	 * @param area_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSpotArea(long ps_id) throws Exception ;
	
	
	/**
	 * @param zone_id
	 * @param ps_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDockCheckDetailsInfo(long zone_id , long ps_id ,int status ) throws Exception ;


	public DBRow[] getDoorArea(long ps_id) throws Exception;
} 
