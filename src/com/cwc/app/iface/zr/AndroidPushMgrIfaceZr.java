package com.cwc.app.iface.zr;

import com.cwc.app.api.zr.AndroidPushMessge;
import com.cwc.db.DBRow;




public interface AndroidPushMgrIfaceZr {
	
	public void pushMessageToDevice(long business_id, long module_id, String msg, String targetIds, long adid) throws Exception;
		
	/**
	 * @param devices
	 * @param androidPushMessage
	 * @param senderAdid
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月10日
	 */
    public void pushMessaeToDevice(DBRow[] devices , AndroidPushMessge androidPushMessage , long senderAdid) throws Exception ;
		
	public void updateRegisterReflection(long adid , String register) throws Exception ;
	
	/**
	 * 读取消息总数
	 * @param adid
	 * @param status
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月13日
	 */
	public int countMessage(long adid , int status) throws Exception ;
	
	/**
	 * 查询消息列表
	 * @param short_message_id
	 * @param pageCount
	 * @param adid
	 * @param short_message_status
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月13日
	 */
	public DBRow[] queryMessage(long short_message_id ,  int pageCount  , long adid  , int short_message_status) throws Exception ;
	
	/**
	 * 更新一个人的信息
	 * @param adid
	 * @param updateRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月5日
	 */
	public void updateAdmin(long adid , DBRow updateRow) throws Exception ;
}
