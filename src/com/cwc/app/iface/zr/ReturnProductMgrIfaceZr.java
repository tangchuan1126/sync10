package com.cwc.app.iface.zr;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ReturnProductMgrIfaceZr
{
	public long addReturnProductMgrZr(DBRow row) throws Exception ;
	public long addReturnProductMgrZr(HttpServletRequest request) throws Exception ;
	public DBRow[] getReturnProductBypage(PageCtrl pc) throws Exception ;
	public void updateReturnProduct(DBRow row,long rp_id) throws Exception ;
	public void updateReturnProductRelationOver(HttpServletRequest request) throws Exception ;
	public void updateReturnProductPrintLabelOver(HttpServletRequest request) throws Exception ;
	public DBRow getReturnProductByRpId(long rp_id) throws Exception ;
	public DBRow[] filterReturnProductRows(String st , String end , long create_adid ,int return_product_source,int status ,int is_print_ove ,int return_product_type ,PageCtrl pc , int picture_recognition , int check_item_type , int gather_picture_status, int is_need_return) throws Exception ;
	public void returnProductRecognitionFinish(HttpServletRequest request) throws Exception ;
	public void returnProductRegister(HttpServletRequest request) throws Exception;
	/**
	 * 采集图片完成(仓库的人操作)
	 * @param request
	 * @throws Exception
	 */
	public void gatherPictureStatus(HttpServletRequest request) throws Exception ;
	
	/**
	 * 关联单据(oid,wid,sid)
	 * @param request
	 * @throws Exception
	 */
	public void relationOrder(HttpServletRequest request) throws Exception ;
	
	public DBRow checkReturnProductIsCreateByServices(HttpServletRequest request) throws Exception ;
	public void returnProductReturnHandleRequest(HttpServletRequest request) throws Exception ;
}
