package com.cwc.app.iface.zr;

import com.cwc.app.exception.schedule.ScheduleGenerateException;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.util.StringUtil;

interface TitleAndScheduleOverviewGenerater{
	public String title(DBRow row);
}
class CheckInModelTitle implements TitleAndScheduleOverviewGenerater{
	
	public static String TitleCheckInModelWindowCheckInFormate = "Entry[%s] %s:%s Need Assign";
	public static String TitleCheckInModelGateCheckInFormate = "Entry[%s] Need Window";
	public static String TitleCheckInModelWareHouseCheckInFormate = "Accept Entry[%s] %s:%s";
	
	private ModuleKey moduleKey = new ModuleKey();
	@Override
	public String title(DBRow row) {
		int processKey = row.get("processKey", 0);
		long entry_id = row.get("entry_id", 0l);
		String number = row.getString("number");
		int numberTypeInt = row.get("number_type", 0);
		String title = "" ;
		String numberType = moduleKey.getModuleName(numberTypeInt);
		switch (processKey) {
			case ProcessKey.CHECK_IN_WINDOW:
				title = String.format(TitleCheckInModelWindowCheckInFormate, entry_id,numberType,number);
				break;		//表示需要Warehouse的主管，分配任务
			case ProcessKey.GateHasTaskNotifyWindow : 
				title = String.format(TitleCheckInModelGateCheckInFormate, entry_id);
				break ;	//表示window的人需要去添加
			case ProcessKey.CHECK_IN_WAREHOUSE :
				title = String.format(TitleCheckInModelWareHouseCheckInFormate, entry_id,numberType,number);
				break ;
			case ProcessKey.GateNotifyWareHouse :
				title = String.format(TitleCheckInModelWindowCheckInFormate, entry_id,numberType,number);
				break ;
			case ProcessKey.GateNoTaskNotifyWindow : 
				title = String.format(TitleCheckInModelGateCheckInFormate, entry_id);
				break ;	//表示window的人需要去添加
		}
		return title ;
	
	}
}










/**
 * 系统中添加任务的一个实体
 * @author win7zr
 *
 */
public class ScheduleModel {

	public final static int isAllDayFlag = 1 ;
	public final static int unAllDayFlag = 0 ;
	
	private long assignUserId ;						//任务安排人			Required
	private String executeUserId ;					//任务执行的人 			Required	schedule_sub 表中的记录
	private long associateId ;						//任务关联的业务单据ID	Required
	private int associateType ;						//任务关联业务单据的Key	Required	 取值 ModuleKey
	private int associateProcess ;					//任务关联某个业务下面的某个流程  Required  比如DockCheckIn，WindowCheckIn 取值ProcessKey
	
	private String emailTitle ;						//email title 同时也是  在schedule上显示的 Title schedule_overview(对应数据库的字段)
	
	private long associateMainId	;				//任务有可能关联一个子任务，这个字段是新添加的字段表示主单据号 。 比如CheckIn关联的任务是DetailId，那么这个字段表示他的EntryID
	
	private String scheduleJoinExecuteId ;			//任务参与的人					    schedule_join 表中的记录
	private String context ;						//以前是当成任务的内容来使用现在的内容都是通过手动拼接 字符串的方式生成
	private String memo ;							//memo 
	
	private boolean isNeedDwr  = true;				//是否页面提醒 true				
	private boolean isNeedEmail  = true ;			//是否邮件提醒 true
	private boolean isNeedShortMessage = true ;		//是否短信提醒 true
	private boolean isArrangeNow = true ;			//当前任务是否立即安排	true	如果是立即安排，那么在任务的查看界面就会看见当前这个任务已经是在任务Table上
	private int isAllDay  =  unAllDayFlag ;					//是否是全天的任务
	
	private String startTime ; 						//任务执行的开始时间	 表示有可能任务持续一段时间 (08:00~14:00) 默认为当前 00:00:00
	private String endTime ;						//任务执行的结束时间   	默认为当前 23:59:59
	
	private EmailBody emailBody ;					//为以后的邮件提供
 	
	private ScheduleModel(long assign_user_id, String execute_user_id,
			long associate_id, int associate_type, int associate_process) {
		super();
		if(assign_user_id <= 0l 
				|| StringUtil.isNull(execute_user_id) 
				|| associate_id <= 0l
				|| associate_type <= 0
				|| associate_process <= 0){
			throw new ScheduleGenerateException();
		}
		this.assignUserId = assign_user_id;
		this.executeUserId = execute_user_id;
		this.associateId = associate_id;
		this.associateType = associate_type;
		this.associateProcess = associate_process;
	}
	
	/**
	 * * isNeedDwr = true ;
	 * isNeedEmail = true ;
	 * isNeedShortMessage = true ;
	 * 获取CheckIn发送Schedule Model
	 * 
 	 * @param entry_id
	 * @param detail_id	
	 * @param processKey processKey
	 * @param adid		当前登录人	
	 * @param execute_user_id 任务的执行人[,]分隔
	 * @param context		   任务的内容		
	 * @param taskNumber		detail number : 
	 * @return
	 * @author zhangrui
	 * @Date   2014年12月4日
	 */
	public static  ScheduleModel getCheckInScheduleModel(long entry_id , long detail_id , int processKey ,long adid , String execute_user_id ,String context , String taskNumber , int number_type ) throws ScheduleGenerateException {
		ScheduleModel scheduleModel = new ScheduleModel(adid, execute_user_id, 
				detail_id, ModuleKey.CHECK_IN, processKey);
		HoldDoubleValue<String, String> staredAndEndTimeOfDay =	getCurrentDayStartTimeAndEndTime();
		scheduleModel.setStartTime(staredAndEndTimeOfDay.a);
		scheduleModel.setEndTime(staredAndEndTimeOfDay.b);
		scheduleModel.setAssociateMainId(entry_id);
		scheduleModel.setContext(context);
		//格式化Title schedule_overview 
		DBRow data = new DBRow();
		data.add("number", taskNumber);
		data.add("processKey", processKey);
		data.add("entry_id", entry_id);
		data.add("number_type", number_type);
		scheduleModel.setEmailTitle(new CheckInModelTitle().title(data));
 		//这是表示给Warehouse的主管的任务，那么任务的开始时间 就是任务创建的时间 ,如果是Warehouse 员工在开始任务，那么会重新写入这个时间回去
		scheduleModel.setStartTime(DateUtil.NowStr());
		
		return scheduleModel ;
	}
	/**
	 * 2015-01-13 给window的人发的schedule
	 * @param entry_id
	 * @param processKey
	 * @param adid
	 * @param execute_user_id
	 * @param context
	 * @return
	 * @author zhangrui
	 * @Date   2015年1月13日
	 */
	public static  ScheduleModel getCheckInNotifyWindowScheduleModel(long entry_id , int processKey ,long adid , String execute_user_id ,String context  ){
		ScheduleModel scheduleModel = new ScheduleModel(adid, execute_user_id, 
				entry_id, ModuleKey.CHECK_IN, processKey);
		HoldDoubleValue<String, String> staredAndEndTimeOfDay =	getCurrentDayStartTimeAndEndTime();
		scheduleModel.setStartTime(staredAndEndTimeOfDay.a);
		scheduleModel.setEndTime(staredAndEndTimeOfDay.b);
		scheduleModel.setAssociateMainId(entry_id);
		scheduleModel.setContext(context);
		//格式化Title schedule_overview 
		DBRow data = new DBRow();
 		data.add("processKey", processKey);
		data.add("entry_id", entry_id);
		scheduleModel.setEmailTitle(new CheckInModelTitle().title(data));
 		scheduleModel.setStartTime(DateUtil.NowStr());
		
		return scheduleModel ;
	}
	
	
	
	
	public static  ScheduleModel getFinanceScheduleModel( long adid , String execute_user_id ,long detail_id ,int associate_type , int processKey ,String title , String context  ){
		ScheduleModel scheduleModel = new ScheduleModel(adid, execute_user_id, 
				detail_id, associate_type , processKey);
		HoldDoubleValue<String, String> staredAndEndTimeOfDay =	getCurrentDayStartTimeAndEndTime();
		scheduleModel.setStartTime(staredAndEndTimeOfDay.a);
		scheduleModel.setEndTime(staredAndEndTimeOfDay.b);
		scheduleModel.setContext(context);
		//格式化Title schedule_overview 
		//这是表示给Warehouse的主管的任务，那么任务的开始时间 就是任务创建的时间 ,如果是Warehouse 员工在开始任务，那么会重新写入这个时间回去
		scheduleModel.setStartTime(DateUtil.NowStr());
		scheduleModel.setEmailTitle(title);
		return scheduleModel ;
	}
	
	public static ScheduleModel getInventroyScheduleModel(long adid , String execute_user_id , long inventry_id , String title , String startTime , String endTime , int inventry_type) throws ScheduleGenerateException {
		ScheduleModel scheduleModel = new ScheduleModel(adid, execute_user_id, 
				inventry_id, ModuleKey.Inventory ,  inventry_type);
	//	HoldDoubleValue<String, String> staredAndEndTimeOfDay =	getCurrentDayStartTimeAndEndTime();
		scheduleModel.setStartTime(startTime);
		scheduleModel.setEndTime(endTime);
		scheduleModel.setContext(title);
		scheduleModel.setEmailTitle(title);
		return scheduleModel ;
	}
	
	public static ScheduleModel getSpecialTaskScheduleModel(long adid , String execute_user_id , long task_id , String detail,String overview , String startTime , String endTime,String main_task_id) throws ScheduleGenerateException {
		ScheduleModel scheduleModel = new ScheduleModel(adid, execute_user_id, 
				task_id, ModuleKey.SPECIAL_TASK ,  ProcessKey.SPECIAL_TASK);
	//	HoldDoubleValue<String, String> staredAndEndTimeOfDay =	getCurrentDayStartTimeAndEndTime();
		scheduleModel.setStartTime(startTime);
		scheduleModel.setEndTime(endTime);
		scheduleModel.setContext(detail);
		scheduleModel.setEmailTitle(overview);
		scheduleModel.setAssociateMainId(Long.parseLong(main_task_id));
		scheduleModel.setNeedEmail(false);
		return scheduleModel ;
	}
	private static HoldDoubleValue<String, String> getCurrentDayStartTimeAndEndTime(){
 		String baseTime =  DateUtil.FormatDatetime(DateUtil.defaultDateFormat);
		return new HoldDoubleValue<String, String>(baseTime + " 00:00:00", baseTime +" 23:59:59");
	}

	
	
	
	
	
	
	
	
	
	public long getAssignUserId() {
		return assignUserId;
	}

	public void setAssignUserId(long assignUserId) {
		this.assignUserId = assignUserId;
	}

	public String getExecuteUserId() {
		return executeUserId;
	}

	public void setExecuteUserId(String executeUserId) {
		this.executeUserId = executeUserId;
	}

	public long getAssociateId() {
		return associateId;
	}

	public void setAssociateId(long associateId) {
		this.associateId = associateId;
	}

	public int getAssociateType() {
		return associateType;
	}

	public void setAssociateType(int associateType) {
		this.associateType = associateType;
	}

	public int getAssociateProcess() {
		return associateProcess;
	}

	public void setAssociateProcess(int associateProcess) {
		this.associateProcess = associateProcess;
	}

	public long getAssociateMainId() {
		return associateMainId;
	}

	public void setAssociateMainId(long associateMainId) {
		this.associateMainId = associateMainId;
	}

	public String getScheduleJoinExecuteId() {
		return scheduleJoinExecuteId;
	}

	public void setScheduleJoinExecuteId(String scheduleJoinExecuteId) {
		this.scheduleJoinExecuteId = scheduleJoinExecuteId;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public boolean isNeedDwr() {
		return isNeedDwr;
	}

	public void setNeedDwr(boolean isNeedDwr) {
		this.isNeedDwr = isNeedDwr;
	}

	public boolean isNeedEmail() {
		return isNeedEmail;
	}

	public void setNeedEmail(boolean isNeedEmail) {
		this.isNeedEmail = isNeedEmail;
	}

	public boolean isNeedShortMessage() {
		return isNeedShortMessage;
	}

	public void setNeedShortMessage(boolean isNeedShortMessage) {
		this.isNeedShortMessage = isNeedShortMessage;
	}

	public boolean isArrangeNow() {
		return isArrangeNow;
	}

	public void setArrangeNow(boolean isArrangeNow) {
		this.isArrangeNow = isArrangeNow;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getEmailTitle() {
		return emailTitle;
	}

	public void setEmailTitle(String emailTitle) {
		this.emailTitle = emailTitle;
	}

	public int getIsAllDay() {
		return isAllDay;
	}

	public void setIsAllDay(int isAllDay) {
		this.isAllDay = isAllDay;
	}
	
	
}
