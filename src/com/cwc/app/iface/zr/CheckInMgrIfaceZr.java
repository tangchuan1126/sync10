package com.cwc.app.iface.zr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.android.AddLoadBarUseFoundException;
import com.cwc.app.exception.android.CheckInEntryIsLeftException;
import com.cwc.app.exception.android.CheckInNotFoundException;
import com.cwc.app.exception.android.EntryTaskHasFinishException;
import com.cwc.app.exception.android.NoPermiessionEntryIdException;
import com.cwc.app.exception.android.NoRecordsException;
import com.cwc.app.exception.checkin.EquipmentHadOutException;
import com.cwc.app.exception.checkin.EquipmentNotFindException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.cwc.service.android.action.mode.HoldThirdValue;

public interface CheckInMgrIfaceZr {

	public DBRow getDockCloseDetailByEntryId(DBRow data , AdminLoginBean adminLoginBean) throws Exception ;

	public DBRow DockCloseBill(DBRow data,AdminLoginBean adminLoggerBean) throws Exception ;

	public long DockCloseReleaseDoor(DBRow data,AdminLoginBean adminLoggerBean) throws Exception;
	
	
 	public void dockCloseUpPhoto(DBRow data, DBRow loginRow) throws Exception ;
	
	/**
	 * 多个子单据一起关闭
	 * @param data
	 * @param loginRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月5日
	 */
 	public DBRow mutiDockClose(DBRow data, AdminLoginBean adminLoggerBean) throws Exception ;
	
 	/**
 	 * @param loadSetRows
 	 * @return(验证输入是否合法的)
 	 * @throws Exception
 	 * @author zhangrui
 	 * @Date   2014年11月6日
 	 */
 	public void validateLoadBarSet(DBRow[] loadSetRows) throws Exception ; 
 	
 	/**
	 * @param loadSetRows
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月4日
	 */
	public void addLoadBarSet(DBRow[] loadSetRows) throws Exception ;
	

	 public DBRow[] goingToWareHouse(long ps_id , PageCtrl pc) throws Exception ;
	 public DBRow[] remainJobs(long ps_id  , PageCtrl pc) throws Exception ;
	 public DBRow[] workingOnWarehouse(long ps_id ,int lr_type, PageCtrl pc) throws Exception ;
	 public DBRow[] cargoOnSpot(long ps_id , PageCtrl pc) throws Exception ; 
	 public DBRow[] emptyCTNR(long ps_id , PageCtrl pc) throws Exception ;
	 public DBRow[] cargoOnDoor(long ps_id , PageCtrl pc) throws Exception ;
	 public DBRow[] parking3rdPartyNone(long ps_id , PageCtrl pc) throws Exception ;
	 public DBRow[] goingToWindow(long ps_id , PageCtrl pc) throws Exception ;
	 public DBRow[] waitingList(long ps_id , PageCtrl pc) throws Exception ;
	 public DBRow[] forgetCloseTask(long ps_id , PageCtrl pc) throws Exception ;
	 public DBRow[] leaving(long ps_id , PageCtrl pc) throws Exception ;  
	 public DBRow[] forgetCheckOut(long ps_id , PageCtrl pc) throws Exception  ;
	 public DBRow[] remainJobsLabor(long ps_id,PageCtrl pc) throws Exception ;
	 public DBRow[] noTaskLabor(long ps_id,long[] adgids,long[] adgroles,PageCtrl pc) throws Exception ;
	 public DBRow[] noTaskEntry(long ps_id, PageCtrl pc) throws Exception;
	 public DBRow[] loadReceiveCloseToday(long ps_id ,PageCtrl pc) throws Exception ;
	 public DBRow[] ghostCTNR(long ps_id, PageCtrl pc) throws Exception;

	 /**
	  * android dockCheckIn
	  * @param request
	  * @param login_id
	  * @param adminLoginBean
	  * @return
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2014年11月25日
	  */
	// public DBRow dockCheckInAssignTask(HttpServletRequest request,AdminLoginBean adminLoginBean)throws Exception;
	 /**
	  * 通过EntryId 获取assign的信息
	  * @param infoId
	  * @param adminLoginBean
	  * @return
	  * @throws Exception
	  * @author zhangrui
	  * @Date   2014年11月25日
	  */
	// public DBRow dockCheckInSearchEntryDetail(long infoId , long equipment_id ,AdminLoginBean adminLoginBean)throws Exception;
	  /**
	   * android 删除 dockCheckIn的通知
	   * @param request
	   * @param adminLoginBean
	   * @return
	   * @throws Exception
	   * @author zhangrui
	   * @Date   2014年11月27日
	   */
	 // public DBRow dockCheckInDeleteNotice(HttpServletRequest request, AdminLoginBean adminLoginBean , boolean isAndroid)throws Exception;
	  
	  /**
	   * dock check in 的时候提交的动作
	   * 保存图片 && 保存seal
	   * @param infoId
	   * @param adid
	   * @param delivery_seal
	   * @param pickup_seal
	   * @param filePath
	   * @param isAndroid
	   * @param request
	   * @throws Exception
	   * @author zhangrui
	   * @Date   2014年11月29日
	   */
	  public void dockCheckInSubmit(long entry_id,long adid ,String delivery_seal,String pickup_seal, String filePath , boolean isAndroid ,HttpServletRequest request) throws Exception ;
	 
	 
 	  	/**
	  	 * 根据entry_id 去获取所有Task的 设备
	  	 * @param entry_id
	  	 * @return
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月1日
	  	 */
		public DBRow[] getEntryTaskEquipment(long entry_id) throws Exception ;
		
	    /**
	     * android 请求门 + 停车位 
	     * @param ps_id
	     * @param info_id
	     * @param request_type (1:ALL. 2:Free)
	     * @return
	     * @throws Exception
	     * @author zhangrui
	     * @Date   2014年12月1日
	     */
	  	public HoldDoubleValue<DBRow[], DBRow[]> androidGetResources(long ps_id,long entry_id , int request_type , int resources_type )throws Exception;
	  	
	  	/**
	  	 * 得到!Free 的资源
	  	 * @param ps_id
	  	 * @param entry_id
	  	 * @param request_type
	  	 * @param resources_type
	  	 * @return
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月26日
	  	 */
	  	public HoldDoubleValue<DBRow[], DBRow[]> androidGetNotFreeResources(long ps_id, int resources_type)throws Exception;
	  	/**
	  	 * 下面方法的重载
	  	 * @param detail_id
	  	 * @param rl_id
	  	 * @param row
	  	 * @param adid
	  	 * @param entry_id
	  	 * @param resourcesType
	  	 * @param equipment_id
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月20日
	  	 */
	 	public void finishDetail(DBRow detailRow, long rl_id, DBRow row, long adid, int resourcesType) throws Exception ;
	  	/**
	  	 * 写入数据 door的值，释放资源 + 添加日志
	  	 * 任务完成的时候调用的方法
	  	 * @param detail_id
	  	 * @param rl_id
	  	 * @param row
	  	 * @param adid
	  	 * @param entry_id
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月3日
	  	 */
	 	public void finishDetail(DBRow detailRow, long rl_id, DBRow row, long adid,  int resourcesType , DBRow equipment ) throws Exception ;
	 	
	 	
	 	/**
	 	 * 
	 	 * {
	 	 *  equipment_id:
	 	 *  equipment_type:
	 	 *  equipment_number:
	 	 *  task:{
	 	 *  
	 	 *  }
	 	 * }
	 	 * 通过detail_id 查询所有没有分配的Task集合
	 	 * @param entry_id
	 	 * @return
	 	 * @throws Exception
	 	 * @author zhangrui
	 	 * @Date   2014年12月5日
	 	 */
	 	public DBRow[] getWareHouseManagerAssignTaskBy(long entry_id) throws Exception; 
	 	
	 	/**
	 	 * 得到当前登录人需要AssignTask的list(管理员级别的人)
	 	 *[{
	 	 * 	 entry:
	 	 * 	 equiment : 
	 	 *   number:
	 	 *   equiment_type:
	 	 *   equiment_type_value:
 	 	 * }]
	 	 * @param adid
	 	 * @return
	 	 * @throws Exception
	 	 * @author zhangrui
	 	 * @Date   2014年12月6日
	 	 */
	 	public DBRow[] getAssignTaskList(long adid) throws Exception ;
	 	
	 	
	 	/**
	  	 * 获取 wareHouse 管理员 Assign Task Detail
	  	 * 不用实际分配给他啊的为准，而是这个Entry + equipment的为准去查询
	  	 * @param infoId
	  	 * @param equipment_id
	  	 * @param adminLoginBean
	  	 * @return
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月6日
	  	 */
	  //	public DBRow getAssignTaskDetail(long infoId , long equipment_id ,DBRow loginRow) throws Exception  ;
	 	
	  	/**
	  	 * 
	  	 * @param execute_user_ids
	  	 * @param schedule_ids
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月6日
	  	 */
	  	//public void singleWareHouseManagerAssignTask(String execute_user_ids, String schedule_ids , DBRow loginRow) throws Exception ;

	  	
	  	/**
	  	 * assign任务的时候，移动任务在具体的那个门上执行
	  	 * @param entry_id
	  	 * @param equipment_id
	  	 * @param sd_id
	  	 * @param moved_resource_type
	  	 * @param move_resouce_id
	  	 * @param adminLoggerBean
	  	 * @return
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月8日
	  	 */
	  	public void assignTaskMoveToDoor(long entry_id, long equipment_id, long sd_id ,int moved_resource_type,long move_resouce_id, long adid) throws EquipmentHadOutException, Exception ;
	  	
		/**
	  	 * 获取当前adid 需要 Execute loader的执行任务
	  	 * @param adid
	  	 * @return
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月8日
	  	 */
	   	public DBRow[] executeTaskList(long adid) throws Exception ; 
	   	
	  	
	  	//>>>>>>>>>>>>>>>>>Android Assign Task<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<,
	  	/**
	  	 * 
	  	 * @param entry_id
	  	 * @return
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月8日
	  	 */
	  	public DBRow[] assginTaskListByEntry(long entry_id , AdminLoginBean adminLoginBean) throws NoRecordsException ,NoPermiessionEntryIdException ,CheckInNotFoundException,Exception ;
	  	/**
	  	 * 
	  	 * 通过EntryID + equipment 去过滤这个Entry当前设备下，还没有Close的任务
		 *　1.有可能没有管理员 　+ 　所以任务压根就没有分配。
		 *　2.列出所有没有Close的detail ， 只不过有分配过的，那么现实一个人名。没有分配的就没有人名
		 * 3. 如果他重新勾选人，那么就update 原来的任务的表的记录。
		 * 4.没有记录的那么assign
		 * 5.通过number_stutas 去过滤 (processing + unprocess)
	  	 * @param entry_id
	  	 * @param equiment_id
	  	 * @return
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月8日
	  	 */
	   	public DBRow assginTaskDetaillByEntryAndEquipment(long entry_id , long equiment_id , boolean isAndroid) throws Exception ;
	   	
	   	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Android Assign Task End <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	   	
	   	/**
	   	 * 通过EntryId 获取当前人的在这个entry下的任务 按照资源 + 设备 分组
	   	 * [
	   	 * 	{
	   	 * 		resources_id:
	   	 * 		resources_type:
	   	 * 		resources_name:
	   	 * 		resources_type_value:
	   	 * 		
	   	 * 		entry_id:
	   	 * 		equipment_id:
	   	 * 		equipment_type:
	   	 * 		equipment_number:
	   	 * 		equipment_type_value:
	   	 * 
	   	 *  }
	   	 * 
	   	 * ]
	   	 * 
	   	 * 
	   	 * @param entry_id
	   	 * @return
	   	 * @throws Exception
	   	 * @author zhangrui
	   	 * @Date   2014年12月9日
	   	 */
	   	public DBRow[] taskProcessingListByEntry(long entry_id , AdminLoginBean adminLoginBean) throws NoPermiessionEntryIdException,CheckInEntryIsLeftException ,CheckInNotFoundException,Exception;
	   	
	   	/**
	   	 * 
	   	 * @param entry_id
	   	 * @return
	   	 * @throws Exception
	   	 * @author zhangrui
	   	 * @Date   2014年12月19日
	   	 */
	   	public DBRow[] entryEquipmentWithResouces(long entry_id) throws Exception ;
	   	
	   	/**
	   	 * @param entry_id
	   	 * @param equipment_id
	   	 * @param resources_type
	   	 * @param resources_id
	   	 * @param adminLoginBean
	   	 * @return
	   	 * @throws Exception
	   	 * @author zhangrui
	   	 * @Date   2014年12月9日
	   	 */
	   	public DBRow[] getTaskProcessingDetailByEntryAndEquipmentAndResouces(long entry_id , long equipment_id , int resources_type , long resources_id , AdminLoginBean adminLoginBean) throws Exception ;

	   	/**
	   	 * 通过EntryId查询，然后AssignTask的情况
	   	 * @param execute_user_ids
	   	 * @param datas
	   	 * @param loginRow
	   	 * @throws Exception
	   	 * @author zhangrui
	   	 * @Date   2014年12月9日
	   	 */
	  	public void assignTaskByEntry(String execute_user_ids, List<HoldDoubleValue<Long, Integer>> datas , AdminLoginBean  adminLoggerBean) throws EntryTaskHasFinishException,Exception ;
	  	/**
	  	 * details start 
	  	 * @param equipment_id
	  	 * @param entry_id
	  	 * @param details
	  	 * @param sd_id
	  	 * @param adid
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月10日
	  	 */
	  	public void taskProcessingStartDetails(long equipment_id , long entry_id ,List<HoldDoubleValue<Long, String>> details , long sd_id , long adid , int resources_type) throws Exception ;
	  	
	  	/**
	  	 * 关闭Task 同时完成Detail对应的任务
	  	 * @param data
	  	 * @param adminLoggerBean
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月10日
	  	 */
	  	public int taskProcessingCloseDetail(DBRow data , AdminLoginBean adminLoggerBean) throws Exception ;
	  	
	  	/**
	  	 * 得到CheckIN模块的图片 名字android端需要使用
	  	 * @param entry_id
	  	 * @param file_with_class
	  	 * @return
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月10日
	  	 */
	  	public DBRow[] getCheckInFile(long entry_id ,int file_with_class ) throws Exception  ;
	  	
	  	/**
	  	 * 在任务处理的界面，改变门
	  	 * 
	  	 * 把处理中的或者是为处理的Task移动到新的门上 (关联表中的记录)
	  	 * @param entry_id
	  	 * @param equipment_id
	  	 * @param sd_id
	  	 * @param moved_resource_type
	  	 * @param move_resouce_id
	  	 * @param adminLoggerBean
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月10日
	  	 */
	  	public void taskProcessingMoveTaskToDoor(long entry_id, long equipment_id, long sd_id ,int moved_resource_type,long move_resouce_id, AdminLoginBean adminLoggerBean) throws Exception ;
	  	/**
	  	 * 设备的CheckIn
	  	 * @param equipment_id
	  	 * @param sd_id
	  	 * @param adid
	  	 * @param entry_id
	  	 * @param filePath
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月10日
	  	 */
	  	//public void taskProcessingEquipmentCheckInSubmit(long equipment_id , long sd_id , long adid , long entry_id , String in_seal , String out_seal ,String filePath) throws Exception ;

	  	/**
	  	 * equipment checkin 得到设备的信息
	  	 * @param equipment_id
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月10日
	  	 */
	  //	public DBRow taskProcessingEquipmentCheckIn(long equipment_id ,long entry_id ) throws EquipmentNotFindException , Exception;

	  	/**
	  	 * 当前的
	  	 * 
	  	 * 
	  	 * 
	  	 * @param equipment_id
	  	 * @param entry_id
	  	 * @param sd_id
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月11日
	  	 */
	  	public int taskProcessingFinish(long equipment_id, long entry_id, long sd_id , String filePath , long adid) throws EquipmentHadOutException ,Exception  ;

	  	/**
	  	 * 添加一个Loadbar ， 有可能是负数 ，如果是负数的情况，那么应该检查当前的Load的数量是否是够他减少的
	  	 * @param data
	  	 * @param adminLoginBean
	  	 * @return
	  	 * @throws AddLoadBarUseFoundException
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月11日
	  	 */
	  	public long addLoadBarUse(DBRow data , AdminLoginBean adminLoginBean )throws AddLoadBarUseFoundException , Exception ;
	  	
	  	/**
	  	 * 得到equipment的seal
	  	 * @param equipment_id
	  	 * @return
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月11日
	  	 */
	  	public DBRow getEquipmentSeals(long equipment_id) throws AddLoadBarUseFoundException ,Exception ;
	  	/**
	  	 * 将任务安排给自己 或者是吧任务分配给自己
	  	 * @param data
	  	 * @param adminLoginBean
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月11日
	  	 */
	  	public void taskProcessingTakeOver(DBRow data , AdminLoginBean adminLoginBean ) throws Exception ;
	  	
	  	/**
	  	 * close的最后一个界面提交
	  	 * @param data
	  	 * @param adminLoginBean
	  	 * @throws Exception
	  	 * @author zhangrui
	  	 * @Date   2014年12月12日
	  	 */
		public int taskProcessingFinishSubmit(DBRow data , AdminLoginBean adminLoginBean ) throws Exception ;
		
		/**
		 * 门下最后一个Task 提示他是否releaseDoor
		 * 设备离开门
		 * @param data
		 * @param adminLoggerBean
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月12日
		 */
		public void taskProcessingReleaseDoor(DBRow data , AdminLoginBean adminLoggerBean) throws Exception ;
		
		/**
		 * 选择停留 或者离开
		 * @param data
		 * @param adminLoggerBean
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月12日
		 */
		public void taskProcessingEquipmentLeavingInyard(DBRow data , AdminLoginBean adminLoggerBean) throws Exception ;
		
		/**
		 * android shuttle 移动到door
		 * @param entry_id
		 * @param equipment_id
		 * @param sd_id
		 * @param adminLoginBean
		 * @param exeucut_use_ids
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月12日
		 */
		public void androidMoveToDoor(long entry_id , long equipment_id , long sd_id, AdminLoginBean adminLoginBean , String exeucut_use_ids)throws Exception ;
		/**
		 * android shuttle 移动到spot
		 * @param entry_id
		 * @param equipment_id
		 * @param yc_id
		 * @param adminLoginBean
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月12日
		 */
		public void androidMoveToSpot(long entry_id , long equipment_id , long yc_id, AdminLoginBean adminLoginBean) throws Exception ;

		/**
		 * 修改equipment的seal
		 * @param data
		 * @param adminLoginBean
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月16日
		 */
		public void updateEquipmentSeal(DBRow data ,AdminLoginBean adminLoginBean ) throws Exception ;
		
		/**
		 * 判断设备是否离开
		 * @param equipment
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月20日
		 */
		public boolean isEquipmentCheckOut(DBRow equipment)  throws Exception ;
		
		/**
		 * 判断设备是否离开
		 * @param equipment_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月20日
		 */
		public boolean isEquipmentCheckOut(long equipment_id)  throws Exception ;

		
		/**
		 * 查询一个List的界面
		 * @param entry_id
		 * @param pageSize
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月23日
		 */
		public DBRow[] windowCheckInList(long ps_id ,PageCtrl pc) throws Exception ;
		
		/**
		 * 获取一个Entry所有的设备 （以及他Pick Up的设备）
		 * @param entry_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月23日
		 */
		public DBRow[] windowCheckInEntryEquipmentList(long entry_id) throws Exception ;
		
		/**
		 * 获取 Equipment 对应的Entry 和自己的信息 
		 * 以及 Task的信息 Close info 
		 * @param equipment_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月23日
		 */
		public DBRow windowCheckInGetEquipmentInfos(long equipment_id, long entry_id) throws NoRecordsException ,Exception ;
		
		/**
		 * window checkin 获取EntryInfos
		 * @param entry_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月24日
		 */
		public DBRow getWindowCheckInEntryInfos(long entry_id) throws Exception ;
		
		/**
		 * searchValue , searchType
		 * @param searchValue
		 * @param searchType
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月24日
		 */
		public DBRow[] getWindowCheckInSearchValue(String searchValue, int searchType , AdminLoginBean adminLoginBean) throws NoPermiessionEntryIdException ,CheckInNotFoundException ,CheckInEntryIsLeftException , Exception ;
		
		/**
		 * 转换carrier to phone number
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月25日
		 */
		public void convertCarrierPhoneNumber() throws Exception ;
		
		public void convertGateDriverLiscense() throws Exception ;
		/**
		 * @param value
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月25日
		 */
		public DBRow[] getCarrierByPhoneNumber(String value) throws Exception ;
		
		/**
		 * 查询 Mc_dot like '%dd'
		 * android( window checkin or gate check_in) input mc_dot 
		 * @param mc_dot
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月25日
		 */
		public DBRow[] getSearchMcDot(String mc_dot) throws Exception ;
		
		/**
		 * 更新UpdateCheckInEntry
		 * @param updateRow
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月25日
		 */
		public DBRow windowCheckInUpdateEntryDriverInfo(DBRow updateRow , long entry_id , AdminLoginBean adminLoginBean) throws Exception ;		
		/**
		 * 处理 windowCheckInCarrier
		 * @param data
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月26日
		 */
		public void handMcDotAndCarrier(DBRow data) throws Exception ;
		
		/**
		 * 获取
		 * LoadReceive 流程 查询IncomingToWindow,Load/Receive ,Waiting Assing Labor,Remain Jobs,Under Processing,Closed Today
		 * @param ps_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月30日
		 */
		public DBRow getLoadReceiveProcess(long ps_id) throws Exception ;
		
		/**
		 * 得到LoadReceiveScreen 的参数
		 * @param ps_id
		 * @param screenTypekey
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月31日
		 */
		public HoldThirdValue<DBRow, DBRow, DBRow> getLoadReceiveScreenParamSet(long  ps_id , int screenTypekey ) throws Exception ;
		
		/**
		 * 添加或者是修改 params
		 * @param data
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年12月31日
		 */
		public  void saveOrUpdateSetLoadReceiveScreemParams(DBRow data) throws Exception  ;
		/**
		 * 检查当前的Entry是否已经离开了。
		 * （所有的设备离开了。就算离开）
		 * @param entry_id
		 * @throws CheckInEntryIsLeftException
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月6日
		 */
		public void checkEntryIsLeft(long entry_id) throws CheckInEntryIsLeftException ,  Exception ;

		/**
		 * 当前没有完成的单据
		 * @param ps_id
		 * @param pc
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月7日
		 */
		public DBRow[] loadReceiveNotFinish(long ps_id , PageCtrl pc ) throws Exception ;
	/*	*//**
		 * android  每隔1分钟会调用它的Schedule的方法，同时刷新一下当前这个的人在线的时间
		 * @param adid
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月12日
		 *//*
		public void refreshLastRefreshTime(long adid) throws Exception ;*/
		/**
		 * 判断当前的人 是否在线 adid
		 * @param adid
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月12日
		 */
		public boolean isUserOnLine(long adid ) throws Exception ;
		/**
		 * 判断当前的人是否在线
		 * @param row
		 * @return
		 * @throws Exeption
		 * @author zhangrui
		 * @Date   2015年1月12日
		 */
		public boolean isUserOnLine(DBRow row) throws Exception ;
		/**
		 * 统计成员名下未完成的任务数
		 * @param ps_id 仓库
		 * @param proJsId 角色
		 * @param adgid 部门
		 * @param processKeys 
		 * @return
		 * @throws Exception
		 * @author lixf
		 * @Date   2015-01-12
		 */
		public DBRow[] countMemberUncompletedTask(int ps_id, int proJsId, int adgid, int[] processKeys) throws Exception;
		/**
		 * 获取仓库下的superior的在线状态
		 * 没有在线的人返回所有的superisor
		 * @param ps_id 仓库
		 * @return
		 * @throws Exception
		 * 
		 * @author  lixf      
		 * @date    2015年1月13日
		 * @version 1.0
		 */
		public DBRow[] getPresenceOfSuperior(long ps_id ,long adgid) throws Exception;

		/**
		 * 查询成员名下任务所关联设备，且设备上有未分配的任务，统计设备下任务总数及未分配任务数
		 * @param adid
		 * @return
		 * @throws Exception
		 * 
		 * @author  lixf      
		 * @date    2015年1月14日
		 * @version 1.0
		 */
		public DBRow[] countMemberTaskRelEqptTask(long adid) throws Exception;
		
		/**
		 * 查询GateDriverLicense
		 * @param searchValue
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月15日
		 */
		public DBRow[] searchGateDriverLiscense(String searchValue ) throws Exception ;
		
		/**
		 * task process 有包含他自己的 entry
		 * @param adid
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月15日
		 */
		public DBRow[] getTaskProcessingByAdid(long adid)  throws Exception ;
		/**
		 * 这里是直接完成某个任务(那么同时更新 字表的完成)
		 * 
		 * finishSchedule 的时候写上任务的结束时间
		 * @param schedule_id
		 * @param adid
		 * @throws Exception
		 * @author zhangrui
		 * @Date 2014年12月6日
		 */
		public void finishSchedule(long schedule_id, long adid) throws Exception ;
		
		/**
		 * 得到这个Entry 带来的设备
		 * @param entry_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月16日
		 */
		public DBRow[] getEntryEquipmentsWithResources(long entry_id) throws Exception ;
		/**
		 * @param detail_id
		 * @param number_type
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月16日
		 */
		public DBRow[] getTaskScanPallets(long detail_id ,  int number_type) throws Exception  ;
		
		
		/**
		 * 查看当前的设备是否和某个资源有关系
		 * @param resouces_id
		 * @param resouces_type
		 * @param equipment_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月21日
		 */
		public boolean isEquipmentHasResouces(long resouces_id , int resouces_type , long equipment_id) throws Exception ;

		/**
		 * 通过设备号 和 equipment_type 去查询没有离开的设备 以及占用的资源 left join
		 * @param equipment_number
		 * @param equipment_type
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月22日
		 */
		public DBRow[] getEquipmentJoinResouces(String equipment_number , int equipment_type) throws Exception ;
		
		/**
		 * @param entry_id
		 * @throws CheckInEntryIsLeftException
		 * @author zhangrui
		 * @Date   2015年1月22日
		 */
		public void checkGateCheckOutEntryHasLeft(long entry_id ) throws CheckInEntryIsLeftException ,CheckInNotFoundException ,Exception ;
		/**
		 * @param equipmentiids
		 * @param operationType
		 * @param entry_id
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年1月25日
		 */
		public void checkOutSameEquipments(String equipmentiids ,   long entry_id  ,long adid) throws Exception ;
		
		/**
		 * android登录的时候 检查是否在线，如果是已经在线了，那么就不能让他登录
		 * @param account
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年2月3日
		 */
		public boolean checkAndroidLoginIsOnLine(String account ) throws Exception ;
		
		/**
		 * Assign task的时候，每一个task的门 改变门
		 * @param detail_ids
		 * @param resouces_id
		 * @param resouces_type
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年2月10日
		 */
		public void assignTaskChangeDoorByDetails(String detail_ids,
				long resouces_id, int resouces_type, long equipment_id , AdminLoginBean adminLoginBean , long entry_id) throws EquipmentHadOutException ,Exception ;
 		
		/**
		 * 得到Spot的名字
		 * @param yc_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年2月12日
		 */
		public String getSpotName(long yc_id) throws Exception  ;
		/**
		 * 得到门的名字
		 * @param sd_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2015年2月12日
		 */
		public String getDoorName(long sd_id) throws Exception ;
		
		
		public List<DBRow> searchGateCheckOutEquipmentByEntryId(long entry_id , AdminLoginBean adminLoginBean) throws Exception ;
		/**
		 * 查询在线人的字符串
		 * @return
		 * @author zhangrui
		 * @Date   2015年2月13日
		 */
	 	public String getOnlineUserString() ;
}
