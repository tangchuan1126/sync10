package com.cwc.app.iface.zr;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpRequest;

import com.cwc.app.exception.android.PrintServerNoPrintException;
import com.cwc.app.exception.printTask.AndroidPrintServerUnLineException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.service.android.action.mode.HoldDoubleValue;
import com.fr.report.io.PageToSheetExcelExporter;


public interface AndroidPrintMgrIfaceZr {

	
	public void addCreatePdfTask(final long entry_id , final long detail_id) throws Exception ;
	
	
	public void addCreatePdfTask(final long entry_id , final long detail_id,HttpServletRequest req) throws Exception ;
	
 	/**
 	 * 给某个人添加一个打印的任务
 	 * @param data
 	 * @throws Exception
 	 */
	public void addPrintTask(DBRow data) throws Exception ;
	
	/**
	 * 同时添加多个打印任务
	 * @param datas
	 * @throws Exception
	 */
	public void addPrintTask(DBRow[] datas) throws Exception ;
	
	
	public boolean setPrintTaskNofityWho(String who) throws Exception ;
	
	
	/***
	 * 查询正在执行中的打印的数据总和
	 * @param who
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public int countPrintTask(String who , int status) throws Exception ;
 
	/**
	 *  页面 ， 为了解决server重新启动的问题，需要往session中写入当前登录人的信息
	 * @param who
	 * @param status
	 * @param httpSession
	 * @return
	 * @throws Exception
	 */
	public DBRow queryPrintTask(String who, int status , HttpSession httpSession) throws Exception ;
	/**
	 * 改变task状态
	 * @param task_id
	 * @param status
	 * @throws Exception
	 */
	public void updatePrintTaskState(long task_id , int status) throws Exception ;
	/**
	 * set界面获取所有的android printer server
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAndroidPrinterServerByPsId(long ps_id) throws Exception ;
	
 
	/**
	 * 添加androidPrinterServer，
	 * 首先查询android_server_name 是否存在，不存在在添加，如果存在就报错
	 * @param row
	 * @return 
	 * @throws Exception
	 */
	public long addAndroidPrintServer(DBRow row,DBRow addRow) throws Exception ;	
	
	/**
	 * 删除printer_server_id
	 * @param printer_server_id
	 * @throws Exception
	 */
	public void deleteAndroidPrintServer(long printer_server_id) throws Exception ;
	/**
	 * 当前登录是否具有打印的功能
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getLoginUserHasPrint(long adid) throws Exception ;
	
	/**
	 * 是否设置了PrintServerOnline
	 * @param httpSession
	 * @param adid
	 * @param who
	 * @return
	 * @throws Exception
	 */
	public boolean isSetPrintServerOnLine(HttpSession httpSession , long adid  ) throws Exception ;
	
	/**
	 * 得到所有的打印服务器
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllPrintServer() throws Exception ;
	
	/**
	 * 在print_server_select的时候，左侧是区域，右侧是对应的print_server
	 * @param arrays
	 * @return
	 */
	public Map<String, List<DBRow>> fixAllPrintServer(DBRow[] arrays) ;
	
	/**
	 * 添加CheckInPrint 的打印任务
	 * @param data
	 * @param loginRow
	 * @throws Exception
	 */
	public void addBillOfLoadingPrintTask(DBRow data,DBRow loginRow) throws AndroidPrintServerUnLineException , PrintServerNoPrintException , Exception ;
	
	public void addBillOfLoadingPrintTask(DBRow data,DBRow loginRow,HttpServletRequest request) throws AndroidPrintServerUnLineException , PrintServerNoPrintException , Exception ;
	
	/**
	 * 查询某个打印任务的将要打印 和 打印失败的数据
	 * @param print_server_id
	 * @return
	 * @throws Exception
	 */
	public DBRow queryWillPrintAndPrintFailTask(long print_server_id) throws Exception ;
	
	/**
	 * update print Task 
	 * @param print_task_id
	 * @param row
	 * @throws Exception
	 */
	public void updatePrintTask(long print_task_id , DBRow row) throws Exception ;
	
	/**
	 * 
	 * @param data	
	 * @param loginRow
	 * @throws Exception
	 */
	public void loadingTickPrintTask(DBRow data,DBRow loginRow )throws AndroidPrintServerUnLineException , PrintServerNoPrintException ,Exception ;
	
	/**
	 * @param data
	 * @param loginRow
	 * @throws Exception
	 */
	public void countingSheetPrintTask(DBRow data,DBRow loginRow) throws  AndroidPrintServerUnLineException , PrintServerNoPrintException  , Exception ;
	
	/**
	 * 
	 * @param data
	 * @param loginRow
	 * @throws Exception
	 */
	public void receiptsTicketPrintTask(DBRow data,DBRow loginRow) throws AndroidPrintServerUnLineException , PrintServerNoPrintException , Exception ;
	
	/**
	 * 1.根据登录人的信息去选择打印服务器
	 * 如果这个人有区域 
	 * 	那么默认选中登录人所在区域的默认打印机
	 * 
	 * 2.返回当前区域所以可以用的打印服务器
	 * 
	 * @param adid
	 * @param ps_id
	 * @param area_id
	 * @param type   	Label:0 ,Letter : 1
	 * @return
	 * @throws Exception
	 */
	public DBRow getPrintServerByLoginUser(long ps_id , long area_id , int type) throws Exception ;
	
	/**
	 * 1.如果是PrintType:1 就是提交的LoadingNumber ， 如果是为2 那么提交的时候orders 逗号分隔 
	 * @param data
	 * @param loginRow
	 * @return
	 * @throws Exception
	 */
	public void addShippingLabelPrintTask(DBRow data , DBRow loginRow) throws Exception ;
    /**
     *  添加打印机服务器和区域的对应关系
     * @param row
     * @return
     * @throws Exception
     */
	public long addPrinterArea(DBRow row) throws Exception;
	/**
	 * 删除打印机和区域的对应关系
	 * @param p_id
	 * @return
	 * @throws Exception
	 */
	public long deletePrinterArea(long p_id) throws Exception;
	
	/**
	 * 打印Packing List
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月17日
	 */
	public void addPackingList(DBRow  data, DBRow loginRow) throws Exception ;
	/**
	 * 根据Entry号码去打印几个Plate 
	 * @param data
	 * @param loginRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年10月14日
	 */
	public void entryCtnrPrintPlateLabel(DBRow data ,  DBRow loginRow) throws Exception ;
	/**
	 * 检查当前的PrintServer 是否已经被使用了
	 * @param print_server_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年10月18日
	 */
	public boolean isPrintServerInUse(long print_server_id) throws Exception ;
	
	/**
	 * 
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月10日
	 */
	public DBRow[] getPrintServerByBigScreen(long ps_id , PageCtrl pc) throws Exception ;
	/**
	 * 获取 可用和总数的Printer 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月15日
	 */
	public HoldDoubleValue<Integer, Integer> getAvailableAndAll(long ps_id) throws Exception  ;
	
	/**
	 * Gate print label
	 * @param data
	 * @param loginRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月4日
	 */
	public void addGatePrintLabelTask(DBRow data , DBRow loginRow) throws AndroidPrintServerUnLineException,Exception  ;
	
	
	/**
	 * TLP print label
	 * @author zhengziqi
	 * 2015年3月18日
	 * @param data
	 * @param loginRow
	 * @throws AndroidPrintServerUnLineException
	 * @throws Exception
	 *
	 */
	public void addTLPPrintLabelTask(DBRow data , DBRow loginRow) throws AndroidPrintServerUnLineException,Exception;

	/**
	 * RN print label
	 * @author zhengziqi
	 * 2015年4月3日
	 * @param data
	 * @param loginRow
	 * @throws AndroidPrintServerUnLineException
	 * @throws Exception
	 *
	 */
	public void addRNPrintLabelTask(DBRow data , DBRow loginRow) throws AndroidPrintServerUnLineException,Exception;
	
	/**
	 * basic label print
	 * @author yuanxinyu
	 * 2015年7月14日 15:54:56
	 * @param data
	 * @param loginRow
	 * @throws AndroidPrintServerUnLineException
	 * @throws Exception
	 *
	 */
	public void addBasicLabelPrintTask(DBRow data , DBRow loginRow) throws AndroidPrintServerUnLineException,Exception;
	/**
	 * pdf server 
	 * @author zhangtao
	 * 2015年7月23日 15:54:56
	 * @param data
	 * @param loginRow
	 * @throws AndroidPrintServerUnLineException
	 * @throws Exception
	 *
	 */
	public void sendDataToPDFServerHtml(DBRow data) throws Exception;
}
