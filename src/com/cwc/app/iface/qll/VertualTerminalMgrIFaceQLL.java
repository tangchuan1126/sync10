package com.cwc.app.iface.qll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface VertualTerminalMgrIFaceQLL {

	public abstract void addVertualTerminal(HttpServletRequest request)
	throws Exception;
	
	public abstract DBRow[] getAllVertualTerminal(PageCtrl pc)
	throws Exception;
	
	public DBRow getPaymentStatus(long oid)
	throws Exception;
	public DBRow[] getPaymentVertualTerminal(PageCtrl pc , String txnId , String state) throws Exception ;
}
