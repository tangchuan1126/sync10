package com.cwc.app.iface.qll;

import com.cwc.app.beans.ebay.qll.CompleteSaleParaBeans;
import com.cwc.app.beans.ebay.qll.MessageToBidderParaBeans;
import com.cwc.app.beans.ebay.qll.MessageToPartnerParaBeans;



public interface EbayMgrIFace {
		
	public   void MessageToBidderCall( MessageToBidderParaBeans bean)throws Exception ;

	public  void MessageToPartenerCall(MessageToPartnerParaBeans bean)throws Exception ;

	public  void UpTrackingNumberCall( CompleteSaleParaBeans bean)throws Exception ;
	
	public String getSellerID(String ItemNumber) throws Exception; 
	
	public void MessageToPartenerCallJMS(MessageToPartnerParaBeans bean);
	
	public void MessageToBidderCallJMS(MessageToBidderParaBeans bean);
	
	public void UpTrackingNumberCallJMS(CompleteSaleParaBeans bean);
	
}
