package com.cwc.app.iface.qll;
 
import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface TranInfoMgrIFaceQLL {

	public abstract void addTranInfo( long  oid)throws Exception ;
	
	public abstract DBRow getTranInfo(String tranID)throws Exception ;
	
	public abstract void addTranInfoJMS(long oid);
	
	public abstract String[] getProductInfo(String tranID)throws Exception; 
	
	public abstract DBRow checkAddress(HttpServletRequest request) throws Exception;
	
	public String getBuyerIp(String txn_id,String source,String note)
	throws Exception;
 
}
