package com.cwc.app.iface.qll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.email.HaveEmailSceneException;
import com.cwc.app.exception.email.HaveEmailTemplateException;
import com.cwc.db.DBRow;

public interface EmailSceneMgrIFace {
	
	public abstract DBRow[] getSceneTree() throws Exception;  //查询所有的邮件场景信息

	public abstract void addScene(HttpServletRequest request) throws Exception;  //添加场景类别信息

	public abstract void delScene(HttpServletRequest request) throws HaveEmailSceneException,HaveEmailTemplateException,Exception;       //删除选中的场景类别信息

	public abstract DBRow getDetailScene(long id) throws Exception;      //根据id获取详细的场景信息

	public abstract void modScene(HttpServletRequest request) throws Exception;    //修改选择的场景类别信息

	public abstract DBRow[] getSceneChildren(long parentId) throws Exception;   //根据父类id获取该类下的所有子类
	
	public abstract DBRow[] getAllSceneTree() throws Exception;
	
	public DBRow getDetailSceneByName(String sname) throws Exception;//通过场景名获得ID 为filter提供参数
	 
}
