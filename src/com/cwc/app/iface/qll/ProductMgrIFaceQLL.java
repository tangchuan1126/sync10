package com.cwc.app.iface.qll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductMgrIFaceQLL {

	public DBRow[] getDefultStockWarn(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getAllFatherCatalog(long cid)
	throws Exception ;
	
	public String exportStockWarn(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getStockWarn(PageCtrl pc,String cmd,String cidf,int dayCountf,int categoryf,float coefficientf,String search_product_namef,String productLinef,String part_or_allf)
	throws Exception;
	
	public DBRow[] preparationPlanAnalysis(String start_date,String end_date,long catalog_id,long pro_line_id,String product_name,long ps_id,int isAlert,float coefficient,PageCtrl pc)//获得备货计划分析
	throws Exception;
	
	public String exportPreparationPlanAnalysis(HttpServletRequest request)
	throws Exception;
}
