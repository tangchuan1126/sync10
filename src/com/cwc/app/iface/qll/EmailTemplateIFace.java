package com.cwc.app.iface.qll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.email.SameEmailTemplateException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface EmailTemplateIFace {
	
	public DBRow[] getAllTemplate(PageCtrl pc)throws Exception;//获取分页查询
	
	public void addTemplate(HttpServletRequest request) throws SameEmailTemplateException,Exception;//增加
	
	public void delTemplate(HttpServletRequest request) throws Exception ;//删除
	
	public void modTemplate(HttpServletRequest request) throws SameEmailTemplateException,Exception;//修改
	
	public DBRow getDetailTemplate(long tid) throws Exception;//获得具体一条数据
	 
	public DBRow[] getTemplateById(long tid,String receiver, PageCtrl pc) throws Exception;//过滤分页查询
	
	public DBRow[] getSearchTemplate(String key, PageCtrl pc) throws Exception;

	public String [] getTemplateFileName()throws Exception;
	
	public DBRow[] getEmailScene()throws Exception;
	
	public String[][]  getTemplateFolder() throws Exception;
	
	public DBRow[]  getTemplateFile(String path) throws Exception;
	
	public void modScene(HttpServletRequest request)
	throws Exception;
	
	public void modVelocityTemplate(HttpServletRequest request)
	throws Exception;
	
	public DBRow getDetailTemplateByName(String name)
	throws Exception;
	
	public DBRow getDetailTemplateByNameBusines(String name,String busines) 
	throws Exception;
 
}
