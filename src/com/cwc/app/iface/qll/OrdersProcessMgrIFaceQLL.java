package com.cwc.app.iface.qll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface OrdersProcessMgrIFaceQLL {

	public abstract DBRow[] buildPlanDate(long storage_id ,long catalog_id,String startDate,String endDate,PageCtrl pc)
	throws Exception ;

	public abstract DBRow[] getDate(String startDate,String endDate)
	throws Exception ;
	
	public abstract DBRow[] buildPlanLineDate(long storage_id ,String catalog_id,String startDate,String endDate,PageCtrl pc)
	throws Exception;
	
	public abstract DBRow[] buildPidDate(String pid,String startDate,String endDate,long storageID,PageCtrl pc)
	throws Exception;
	
	public String exportPlanAndStockNew(HttpServletRequest request)
	throws Exception;
}
