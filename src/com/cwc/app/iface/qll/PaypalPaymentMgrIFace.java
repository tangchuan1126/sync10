package com.cwc.app.iface.qll;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.paypal.SetExpressCheckOutFalException;
import com.cwc.app.exception.paypal.SetExpressCheckOutSufException;

public interface PaypalPaymentMgrIFace {
	
	public abstract void doDirectPayment(HttpServletRequest request) throws Exception;
	 
	public abstract String doSetExpressCheckOut(HttpServletRequest request) throws SetExpressCheckOutFalException,SetExpressCheckOutSufException,Exception;
	 
	public abstract String getExpressCheckOut(String token) throws Exception;
	
	public abstract String doExpressCheckOut(HttpServletRequest request)throws Exception;
	
	public abstract String rufundTransaction(String refundType,String transactionId,double amount,String note,String currencyCode) throws  Exception;
	
	public abstract void virtualTerminal(double amount,String cardType,String acct,String expdate,String cvv2, String firstName,
			String lastName, String street, String city, String state, String zip, String countryCode,String shipping,String taxRate ,double itemAmount,String currencyCode)throws Exception;
	
	public abstract String ExceuteVertualTerminal(HttpServletRequest request)throws Exception;

	public String ManagePendingTransactionStatus(HttpServletRequest request )
	throws Exception;
	public abstract String ExceuteVertualTerminalByPayMentPage(HttpServletRequest request) throws Exception;
}
