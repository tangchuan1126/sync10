package com.cwc.app.iface.qll;

import javax.servlet.http.HttpServletRequest;

public interface OrdersMgrIFaceQLL {
	
	public abstract void addressValidateFedexJMS(long oid,String toAddress,String toZip);

	public abstract void addressValidateFedex(long oid,String toAddress,String toZip)
	throws Exception;
	
	public abstract void startRefundProcess(HttpServletRequest request,String manager)
		throws Exception;
	
	public abstract void examineRefundProcess(HttpServletRequest request)
	throws Exception;
	 
	public abstract void reStartRefundProcess(HttpServletRequest request)
	throws Exception;
	
	 
	 

}
	 
