package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.shipping.ShippingInfoBean;


public interface ExpressMgrIFace 
{
	public DBRow[] getAllExpressCompany(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getZoneByScId(long sc_id)
	throws Exception;
	
	public DBRow[] getFeeBySzId(long sz_id)
	throws Exception;
	
	public DBRow[] getFeeWeightSection(long sc_id)
	throws Exception;
	
	public void addCompany(HttpServletRequest request)
	throws Exception;
	
	public void addWeight(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getFeeWeightSectionOrderByEnWeightDesc(long sc_id)
	throws Exception;
	
	public void delWeight(HttpServletRequest request)
	throws WeightCrossException,Exception;
	
	public DBRow getDetailCompany(long sc_id)
	throws Exception;
	
	public DBRow getDetailWeight(long sw_id)
	throws Exception;
	
	public DBRow getDetailFeeByCompanyWeightZone(long sc_id,long sw_id,long sz_id)
	throws Exception;
	
	public void saveFee(HttpServletRequest request)
	throws Exception;
	
	public void delCompany(HttpServletRequest request)
	throws Exception;
	
	public void modCompany(HttpServletRequest request)
	throws Exception;
	
	public void addZone(HttpServletRequest request)
	throws Exception;
	
	public void modZone(HttpServletRequest request)
	throws Exception;
	
	public DBRow getDetailZone(long sz_id)
	throws Exception;
	
	public void delZone(HttpServletRequest request)
	throws Exception;
	
	public void modWeight(HttpServletRequest request)
	throws Exception;
	
	public ShippingInfoBean getShippingFee(long sc_id,float weight,long ccid,long pro_id)
	throws ProvinceOutSizeException,CountryOutSizeException,WeightCrossException,WeightOutSizeException,Exception;
	
	public ShippingInfoBean getNewShippingFee(long sc_id,long ccid,long pro_id,DBRow[] waybill)
	throws ProvinceOutSizeException,CountryOutSizeException,WeightCrossException,WeightOutSizeException,Exception;
	
	public DBRow[] getZoneCountry(long sc_id,long sz_id)
	throws Exception;
	
	public void saveZoneCountryMapping(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getZoneCountryMappingBySzId(long sz_id)
	throws Exception;
	
	public int switchCompanyLock(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getAllValidateExpressCompany(PageCtrl pc)
	throws Exception;
	
	public DBRow getDetailZoneByCcidScid(long sc_id,long ccid)
	throws Exception;
	
	public DBRow getDetailCompanyByName(String name)
	throws Exception;
	               
	public DBRow[] getExpressCompanysByPsId(long ps_id,PageCtrl pc)
	throws Exception;
	
	public void copyCompany(long cur_sc_id,long sc_id,int domestic)
	throws Exception;
	
	public DBRow[] getAllExpressCompanysByPsId(long ps_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getZoneProvince(long sc_id,long sz_id)
	throws Exception;
	
	public DBRow[] getZoneProvinceMappingBySzId(long sc_id,long sz_id)
	throws Exception;
	
	public DBRow[] getAllExpressCompanysByPsIdDomestic(long ps_id,int domestic,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getAllDomesticExpressCompanyByScId(long sc_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getAllInternationalExpressCompany(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getDomesticExpressCompanyByPsIdProId(long ps_id,long pro_id,float weight,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getInternationalExpressCompanyByPsIdProId(long ps_id,long ccid,float weight,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getExpressCompanysByScIds(long sc_ids[],PageCtrl pc)
	throws Exception;
	
	public DBRow[] getDomesticExpressCompanyByPsIdProId(long ps_id,long pro_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getInternationalExpressCompanyByPsIdProId(long ps_id,long ccid,PageCtrl pc)
	throws Exception;
	
	public boolean isDomesticShipping(long ccid,long ps_id)
	throws Exception;
	
	public String importExpressRate(HttpServletRequest request)
	throws Exception,FileTypeException;
	
	public void updateExpressRateByExcel(String filename)
	throws Exception;
	
	public String exportExpressFee(HttpServletRequest request) 
	throws Exception;
	
	public void modWeightCurrency(HttpServletRequest request)
	throws Exception;
	
	public DBRow getDetailZone(long sc_id,long ccid,long pro_id)
	throws Exception;
	
	public ShippingInfoBean[] getAllShippingFeeByScIdAndWeight(long ps_id , float weight,long ccid,long pro_id)
	throws ProvinceOutSizeException,CountryOutSizeException,WeightCrossException,WeightOutSizeException,Exception;
	
	public void modCompanyUseTime(HttpServletRequest request)
	throws Exception;
}





