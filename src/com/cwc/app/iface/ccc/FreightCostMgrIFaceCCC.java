package com.cwc.app.iface.ccc;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface FreightCostMgrIFaceCCC {
	/**
	 * 添加货运运费
	 * @param request
	 * @throws Exception
	 */
	public void addFreightCost(HttpServletRequest request) throws Exception;
	
	/**
	 * 通过条件获得货运运费
	 * @param fr_id
	 * @param request
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFreightCostByCondition(String fr_id ,HttpServletRequest request,PageCtrl pc)
    throws Exception;
	
	/**
	 * 
	 * @param request
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] listFreightCost(HttpServletRequest request,PageCtrl pc)
    throws Exception;
	
	/**
	 * 通过货运运费ID获得货运运费
	 * @param fc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getFreightCostById(String fc_id) throws Exception;
	
	/**
	 * 通过货运运费ID更新货运运费
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long updateFreightCost(HttpServletRequest request) throws Exception;
	
	/**
	 * 通过货运运费ID删除货运运费
	 * @param request
	 * @throws Exception
	 */
	public void deleteFreightCost(HttpServletRequest request) throws Exception;
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getCountryById(String id) throws Exception;
	
	public boolean changeFreightIndex(HttpServletRequest request) throws Exception;
}
