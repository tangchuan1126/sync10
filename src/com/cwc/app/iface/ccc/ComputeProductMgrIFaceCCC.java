package com.cwc.app.iface.ccc;

import com.cwc.db.DBRow;

public interface ComputeProductMgrIFaceCCC {
	
	/**
	 * 计算库存成本
	 * @param delivery_order_id
	 * @throws Exception
	 */
	public void computeFreightForDelivery(int delivery_order_id) throws Exception;
	public DBRow getSumTransportFreightCost(String transport_id) throws Exception;
	public DBRow getSumDeliveryFreightCost(String delivery_order_id) throws Exception;

}
