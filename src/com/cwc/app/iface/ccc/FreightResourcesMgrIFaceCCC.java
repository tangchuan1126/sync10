package com.cwc.app.iface.ccc;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface FreightResourcesMgrIFaceCCC {
	
	/**
	 * 添加货运资源
	 * @param request
	 * @throws Exception
	 */
	public void addFreightResources(HttpServletRequest request) throws Exception;
	
	/**
	 * 通过条件获得货运资源
	 * @param request
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFreightResourcesByCondition(HttpServletRequest request,PageCtrl pc)
    throws Exception;
	
	/**
	 * 
	 * @param request
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] listFreightResources(HttpServletRequest request,PageCtrl pc)
    throws Exception;
	
	/**
	 * 通过货运ID获得货运资源
	 * @param fr_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getFreightResourcesById(String fr_id) throws Exception;
	
	/**
	 * 更新货运资源
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long updateFreightResources(HttpServletRequest request) throws Exception;
	
	/**
	 * 删除货运资源
	 * @param request
	 * @throws Exception
	 */
	public void deleteFreightResources(HttpServletRequest request) throws Exception;
	
	/**
	 * 通过国家ID获得国家名称等信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getCountryById(String id) throws Exception;
	
	
}
