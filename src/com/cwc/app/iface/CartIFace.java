package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.db.DBRow;

public interface CartIFace 
{
	public void put2Cart(HttpSession session,long iid,long pid,float quantity,int product_type,float wait_quantity) 
	throws  Exception;
	
	public DBRow[] getSimpleProducts(HttpSession session)throws Exception;
	
	public void clearCart(HttpSession session);
	
	public boolean isEmpty(HttpSession session)throws Exception;
	
	public void removeProduct(HttpSession session,long pid,int product_type)
	throws Exception;
	
	public void modQuantity(HttpSession session,String pids[],String quantitys[],String product_type[]) 
	throws  Exception;
	
	public void flush(HttpSession session)
	throws Exception;
	
	public void put2Cart(HttpServletRequest request)
	throws ProductNotCreateStorageException,ProductNotExistException,Exception;
	
	public DBRow[] getDetailProducts();
	
	public float getSumQuantity();
	
	public double getSumPrice();
	
	public boolean isHasLackingProduct();
	
	public void convert2CustomProduct(HttpSession session,long old_pid,long new_pid) 
	throws  Exception;
}
