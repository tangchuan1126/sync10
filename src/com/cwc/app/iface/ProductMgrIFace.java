package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;

import us.monoid.json.JSONObject;

import com.cwc.app.beans.AddInProductBean;
import com.cwc.app.beans.AddShipProductBean;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuOrderBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.exception.client.OrderNotExistException;
import com.cwc.app.exception.product.ConvertNotBeSelfException;
import com.cwc.app.exception.product.ConvertNotSameTypeException;
import com.cwc.app.exception.product.CustomProductNotExistException;
import com.cwc.app.exception.product.ProductCantBeSetException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductHasRelationStorageException;
import com.cwc.app.exception.product.ProductInUnionException;
import com.cwc.app.exception.product.ProductIsInStorageException;
import com.cwc.app.exception.product.ProductNameIsExistException;
import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductUnionIsInSetException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.app.exception.product.StorageApproveIsExistException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductMgrIFace 
{
	public void addProductStorage(HttpServletRequest request)	throws ProductIsInStorageException,Exception;
	
	public void modProductStorage(HttpServletRequest request)	throws Exception;
	
	public void addInProduct(HttpServletRequest request)	throws Exception;
	
	public void addOutProduct(HttpServletRequest request)	throws Exception;
	
	public void addShipProduct(HttpServletRequest request)	throws Exception;
	
	public DBRow[] getInProductsByStEnPc(long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getOutProductsByStEnPc(long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getShipProductsByStEnPc(long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getInProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getShipProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getOutProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getAmountProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getAmountOutProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getAmountShipProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public String[] getCsv(String filename)	throws Exception;
	
	public boolean isIncatalog(String name,long pcid)	throws Exception;
	
	public DBRow[] getShipProductsFromSearchName(String name,PageCtrl pc)	throws Exception;
	
	public DBRow addProduct(HttpServletRequest request)	throws ProductCodeIsExistException,ProductNameIsExistException,Exception;
	
	public DBRow getDetailProductByPcid(long pcid)	throws Exception;
	
	public DBRow getDetailProductByPname(String pname)	throws Exception;
	
//	商品条码可不惟一 zyj
	public DBRow[] getDetailProductByPcode(String pcode)	throws Exception;
	
	public DBRow[] getAllProducts(PageCtrl pc)	throws Exception;
	
	public void modProduct(HttpServletRequest request)	throws ProductNameIsExistException,ProductCodeIsExistException,Exception;
	
	public DBRow[] getProductByPcid(long pcid,int union_flag,PageCtrl pc)	throws Exception;
	
	//public DBRow[] getProductNonUsed()	throws Exception;
	
	public void modStoreCatalog(HttpServletRequest request)	throws ProductIsInStorageException,Exception;
	
	public DBRow[] getAllProductStorageByPcid(long catalogid,long psid,int type,PageCtrl pc)	throws Exception;
	
	public DBRow[] getProductStorages(long ps_id,int type,PageCtrl pc)	throws Exception;
	 
	public DBRow[] getNewProductByCid(long cid,PageCtrl pc)	throws Exception;
	
	public DBRow[] getProductByCid(long catalog_id,PageCtrl pc)	throws Exception;
	
	public void delProduct(HttpServletRequest request)	throws ProductInUnionException,ProductHasRelationStorageException,Exception;
	
	public void delProductStorage(HttpServletRequest request)	throws Exception;
	
	public DBRow[] getShipProductsByName(String name)	throws Exception;

	public DBRow[] getSearchProducts(String key,PageCtrl pc,AdminLoginBean adminLoginBean)	throws Exception;
	
	public DBRow[] getSearchProducts4CT(String key,PageCtrl pc,AdminLoginBean adminLoginBean)	throws Exception;
	
	public void addProductUnion(HttpServletRequest request)	throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception;
	//创建接口：添加组合套装
	public void addProductUnionSub(long pc_id,float quantity,long set_pid,AdminLoginBean adminLoggerBean)throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception;
	
	public void delProductUnion(HttpServletRequest request)	throws Exception;
	
	public void modProductUnion(HttpServletRequest request)	throws ProductCantBeSetException,ProductUnionSetCanBeProductException,ProductUnionIsInSetException,ProductNotExistException,Exception;
	
	public DBRow getDetailProductInSet(long set_pid,long pid)	throws Exception;
	public void addProductUnionSub(String pc_name, float quantity, long set_pid,AdminLoginBean adminLoggerBean) throws Exception;
	
	public DBRow[] getProductsInSetBySetPid(long set_pid)	throws Exception;
	
	//public boolean isLackingProduct(DBRow product,float quantity,int product_type)	throws Exception;
	
	public DBRow getDetailProductProductStorageByPcid(long root_storage_catalog_id,long pcid)	throws Exception;
	
	public void customProduct(HttpServletRequest request)	throws ProductUnionSetCanBeProductException,CustomProductNotExistException,Exception;
	
	public DBRow getDetailProductCustomByPcPcid(long pcpcid)	throws Exception;
	
	public DBRow[] getProductsCustomInSetBySetPid(long set_pid)	throws Exception;
	
	//public boolean isLackingProduct4OrderPage(long pid,float quantity,int product_type)	throws Exception;
	
	public void addStorageCountry(HttpServletRequest request)	throws Exception;
	
	public void moveStorageCountry(HttpServletRequest request)	throws Exception;
	
	public DBRow[] getStorageCountryBySid(long sid)	throws Exception;
	
	public DBRow getDetailStorageCountryByCid(long cid)	throws Exception;
	
	public DBRow[] getProductStoragesByPcid(long pcid)	throws Exception;
	
	public DBRow[] getStatProductStorage(long ps_id,long pscid,long catalog_id)	throws Exception;
	
	public DBRow[] getStatProductStoragePC(long pscid)	throws Exception;
	
	public void batchModProduct(HttpServletRequest request)	throws Exception;

	public DBRow[] getStatProductPC(long pscid)	throws Exception;

	public DBRow[] getStatProduct(long catalog_id)	throws Exception;
	
	public DBRow[] getProductUnionsByPid(long pid)	throws Exception;
	
	public long addProductStoreLogs(ProductStoreLogBean productStoreLogBean)	throws Exception;
	
	public void validateProductsCreateStorage(long ps_id,DBRow products[])	throws ProductNotCreateStorageException,Exception;
	
	public void convertProduct(HttpServletRequest request)	throws ProductNotExistException,ConvertNotBeSelfException,ProductNotCreateStorageException,ConvertNotSameTypeException,Exception;
	
	public void reCheckLackingOrders(HttpServletRequest request)	throws Exception;
	
	public void addDamageProduct(HttpServletRequest request)	throws Exception;
	
	public void addReturnProduct(HttpServletRequest request)	throws OrderNotExistException,Exception;
	
	public DBRow[] getBookProductsPC(String st,String en,int keep_days,long pscid)	throws Exception;
	
	public DBRow[] getBookProducts(String st,String en,int keep_days,long ps_id,long pscid,long catalog_id)	throws Exception;
	
	public DBRow[] getInProductsByStEnPcCidLogType(long cid,long log_type,long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getOutProductsByStEnPcCidLogType(long cid,long log_type,long st_datemark,long en_datemark,PageCtrl pc)	throws Exception;
	
	public DBRow[] getStatDamagedProducts(long pscid,long catalog_id)	throws Exception;
	
	public DBRow[] getStatDamagedProductsPC(long pscid)	throws Exception;
	
	public DBRow[] getProductStoreSysLogs(PageCtrl pc)	throws Exception;
	
	public DBRow[] getSearchProductStoreSysLogs(String st,String en,long oid,String p_name,String account,int do_group_by,PageCtrl pc)	throws Exception;
	
	public DBRow[] getInProductsByStEnPcCidLogTypePname(String p_name,long cid,long log_type,long st_datemark,long en_datemark,PageCtrl pc)
	throws Exception;
	
	public void customProductQuote(HttpServletRequest request)
	throws ProductUnionSetCanBeProductException,CustomProductNotExistException,Exception;
	
	public void addStorageLocation(HttpServletRequest request)
	throws ProductNotExistException,Exception;
	
	public DBRow[] getAllStorageLocation(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getStorageLocationByPsId(long ps_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getIncorrectStorageByCidPsid(long cid,long ps_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[]  getStorageLocationByBarcode(long ps_id,long pc_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getProductUnionsBySetPid(long set_pid)
	throws Exception;
	
	public PreCalcuOrderBean preCalcuOrder(HttpServletRequest request,long ps_id)
	throws Exception;
	
	public void commmitStorageLocalDifference(HttpServletRequest request)
	throws StorageApproveIsExistException,Exception;
	
	public DBRow[] getAllStorageApproves(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getStorageLocationDifferencesBySaId(long sa_id,PageCtrl pc)
	throws Exception;
	
	public void correctionStore(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getSearchProducts4RecordOrder(String key,PageCtrl pc)
	throws Exception;
	
	public void swichProductAlive(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getStorageLocationScannerByPsId(long ps_id,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getStorageLocationByPsIdMachineID(long ps_id,String machine_id,PageCtrl pc)
	throws Exception;
	
	public void batchDelStorageLocation(HttpServletRequest request)
	throws Exception;
	
	public void addInProductLog(String p_code,long cid,float quantity,long adid,long datemark,int stat_flag,int log_type)
	throws Exception;
	
	public void addInProductLog(String p_code,long cid,float quantity,long adid,long datemark,int stat_flag,int log_type,long oid)
	throws Exception;
	
	public void updateSetPriceAndWeightByPid(long pid,double oldPrice,double newPrice,float oldWeight,float newWeight,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public void addStorageLocation(long ps_id,long pc_id,long slc_id,float quantity,String machine_id,String sn,long con_id,long title_id,AdminLoginBean adminLoggerBean)
	throws ProductNotExistException,Exception;
	
	public DBRow[]  getStorageLocationByBarcodeMachineID(long ps_id,long pc_id,String machine_id,PageCtrl pc)
	throws Exception;
	
	public void reCheckLackingOrdersSub(AdminLoginBean adminLoggerBean) 
	throws Exception;
	
	public void addInProductSub(long current_ps_id,AdminLoginBean adminLoggerBean,AddInProductBean addInProductBean[],long datemark,String machine_id)
	throws ProductNotExistException,Exception;
	
	public void addShipProductSub(long current_ps_id,String dev_date,long datemark,AdminLoginBean adminLoggerBean,AddShipProductBean addShipProductBean[])
	throws Exception;
	
	public DBRow[] getSearchProducts4CT(String key,long cid,PageCtrl pc,AdminLoginBean adminLoginBean)
	throws Exception;
	
	public DBRow getDetailProductProductStorageByPName(long root_storage_catalog_id,String p_name)
	throws Exception;
	
	public DBRow[] getStorageApprovesByPsidStatus(long ps_id,int status,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getAllStorageApprovesSortByPostDate(long ps_id,int status,boolean sort,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getAllStorageApprovesSortByApproveDate(long ps_id,int status,boolean sort,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getFreeStorageCountrys(long psid)
	throws Exception;
	
	public DBRow[] getShipProductsByStEnPcCidScid(long cid,long scid,long st_datemark,long en_datemark,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getShipProductsByStEnPcCidScid(long cid,long scid,long st_datemark,long en_datemark)
	throws Exception;
	
	public void batchModStorageCountry(HttpServletRequest request)
	throws Exception;
	
	public void batchModStorageProvince(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getFreeStorageProvinces(long psid)
	throws Exception;
	
	public DBRow[] getStorageProvinceBySid(long sid)
	throws Exception;
	
	public void delStorageProvinceBySid(HttpServletRequest request)
	throws Exception;
	
	public void delStorageCountryBySid(HttpServletRequest request)
	throws Exception;
	
	public DBRow getDetailProductStorageByPid(long pid)
	throws Exception;
	
	public void renewProduct(HttpServletRequest request)
	throws Exception;
	
	public int getPrepareUnionCount(long psid,long set_id)
	throws Exception;
	
	public void combinationProduct(HttpServletRequest request)
	throws Exception;
	
	public long getPriorDeliveryWarehouse(long ccid,long pro_id)
	throws Exception;
	
	public DBRow[] getStorageProvinceByCcid(long ccid)
	throws Exception;
	
	public DBRow[] getDevProductStorageByNative(long ccid)
	throws Exception;
	
	public DBRow getDetailDevProductStorageByProvince(long ccid,long pro_id)
	throws Exception;
	
	public DBRow getDetailProvinceByPCode(String p_code)
	throws Exception;
	
	public DBRow getDetailProvinceByNationIDPCode(long nation_id,String p_code)
	throws Exception;
	
	public void splitProduct(HttpServletRequest request)
	throws Exception;
	
	public long addCountryArea(HttpServletRequest request)
	throws Exception;
	
	public void modCountryArea(HttpServletRequest request)
	throws Exception;
	
	public void delCountryAreaByCaId(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getCountryAreaMappingByCaId(long ca_id)
	throws Exception;
	
	public DBRow[] getFreeAreaCountrys(long ca_id)
	throws Exception;
	
	public void batchModCountryArea(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getAllCountryAreas(PageCtrl pc)
	throws Exception;
	
	public DBRow getDetailCountryAreaByCaId(long ca_id)
	throws Exception;
	
	public DBRow getDetailProvinceByProId(long pro_id)
	throws Exception;
	
	public void customProductWarranty(HttpServletRequest request)
	throws ProductUnionSetCanBeProductException,CustomProductNotExistException,Exception;
	
	public void upLoadEbayTrackingNumber(long oid,String trackingNumber,long sc_id)//订单上传trackingNumber给Ebay
	throws Exception;
	
	public DBRow[] getSearchProductStoreSysLogsNew(String st,String en,long product_line_id,long catalog_id,String p_name,long bill_id,int bill_type,int operotion,int quantity_type,long ps_id,long adid,int onGroup,int in_or_out,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getSearchReturnProductStoreSysLogsNew(String st,String en,long product_line_id,long catalog_id,String p_name,long bill_id,int bill_type,int quantity_type,long ps_id,long adid,int onGroup,PageCtrl pc)
	throws Exception;
	
	public String exportReturnStoreSysLogs(HttpServletRequest request)//导出退货日志
	throws Exception;
	
	public DBRow getAllDetailProduct(long pc_id)//根据商品ID获得商品全部明细
	throws Exception;
	
	public void combinationProductSystem(long psid,long pcid,int combination_quantity,int bill_type,long bill_id,AdminLoginBean adminLoggerBean,int operation_type)
	throws Exception;
	
	public void splitProductSystem(long psid,long pcid,int split_quantity,int bill_type,long bill_id,AdminLoginBean adminLoggerBean)
	throws Exception;
	
	public void ReBack()
	throws Exception;
	
	public void reBackAllBill(long bill_id,int bill_type,AdminLoginBean adminLoggerBean)//单据回退
	throws Exception;
	
	public DBRow[] outStockProductForOrder(HttpServletRequest request,long ps_id)//订单缺货商品与数量
	throws Exception;
	
	public DBRow[] outStockProductForWaybill(long waybill_id)//运单缺货商品与数量
	throws Exception;
	
	public DBRow[] getProductCodeByTypeAndCode(int codeType, String pcCode) throws Exception;
	
	/**
	 * （张睿）
	 * android print container 的时候利用keyword 去查询
	 * support like %keyword% , barCode , pcid 
	 * 1.首先查询PCID
	 * 2.最后查询%ProductName%
	 * 3.然后查询KeyWord
	 * @param keyWord
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductByKeyWord(String keyWord) throws Exception ;

	public DBRow isProductExistByPname(String name) throws Exception;
	
	public DBRow[] getProductSnByPcId(long pc_id) throws Exception;
	
	public DBRow updateProductSn(HttpServletRequest request) throws Exception;
	
	public void validateProductSuit(HttpServletRequest request) throws ProductCantBeSetException,Exception;
	
	public DBRow updateProductIndex(long id) throws Exception;
	
	public DBRow updateProductIndex(DBRow param) throws Exception;
}
