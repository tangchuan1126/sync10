package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.app.exception.product.ProductNotProfitException;
import com.cwc.app.exception.product.ProductUnionSetCanBeProductException;
import com.cwc.db.DBRow;

public interface CustomCartWarrantyIFace 
{
	public void put2Cart(HttpSession session,long set_pid,long pid,float quantity) 
		throws  Exception;
	
	public void clearCart(HttpSession session);
	
	public boolean isEmpty(HttpSession session,long set_pid);
	
	public DBRow[] getSimpleProducts(HttpSession session,long set_pid);
	
	public void removeProduct(HttpSession session,long set_pid,long pid)
		throws Exception;
	
	public void modQuantity(HttpSession session,long set_pid,String pids[],String quantitys[]) 
		throws  Exception;
	
	public void put2Cart(HttpServletRequest request)
		throws ProductNotProfitException,ProductUnionSetCanBeProductException,ProductNotCreateStorageException,ProductNotExistException,Exception;
	
	public void modQuantity(HttpServletRequest request)
		throws Exception;
	
	public DBRow[] getDetailProducts(HttpSession session,long set_pid)
		throws Exception;

	public void removeSetProduct(HttpSession session,long set_pid)
		throws Exception;
	
	public void copy2FianlSession(HttpSession session);
	
	public DBRow[] getFinalSimpleProducts(HttpSession session,long set_pid);
	
	public DBRow[] getFinalDetailProducts(HttpSession session,long set_pid)
		throws Exception;
	
	public void copyFinalSet2TempSet(HttpSession session,long set_pid);
	
	public void copyTempSet2FinalSet(HttpSession session,long set_pid,long new_set_pid);
}
