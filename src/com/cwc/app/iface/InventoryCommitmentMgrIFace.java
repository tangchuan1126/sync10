package com.cwc.app.iface;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.cwc.db.DBRow;

/**  
 * @author Liang Jie
 * Created on 2014年12月16日
 * 
 */
public interface InventoryCommitmentMgrIFace {

	/**
	 * 
	 * @param companyID
	 * @param customerID
	 * @return
	 * @throws Exception
	 */
	public Map<String,Integer> getProductCountOnHand(String companyID, String customerID, List<String> dnList) throws Exception;
	
	/**
	 * 
	 * @param companyId
	 * @param customerId
	 * @param orders
	 * @return
	 * @throws Exception
	 */
	public String[] commitInventory(HttpServletRequest request) throws Exception;
	
	
	/**
	 * 
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @param orders
	 * @param productCountMap
	 * @param bookedOrderList
	 * @param sheet
	 * @param rownum
	 * @param style
	 * @return
	 * @throws Exception
	 * wangcr 2015/06/16 add sheets(End User,New Order)
	 */
	public int commitInventorySelectedOrders(String companyID, String customerID, List<String> orders, Map<String,Integer> productCountMap, 
		DBRow[] bookedOrderList, DBRow[] recsList, HSSFSheet sheet, HSSFSheet sheet1, HSSFSheet sheet2, HSSFSheet sheet3, HSSFSheet sheet4, 
		int rownum, HSSFWorkbook wb, long logingUserId, String logingUserName) throws Exception;
}
