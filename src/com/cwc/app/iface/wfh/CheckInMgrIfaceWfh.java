package com.cwc.app.iface.wfh;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.service.android.action.mode.HoldDoubleValue;

public interface CheckInMgrIfaceWfh {
	
	
	
	/**
	 * 通过AccountId获取Packlist打印的模板
	 * @param accountId
	 * @param companyId
	 * @return 如果AccountId对应的模板存在则返回配置的模板，否则返回默认的模板
	 * @throws Exception
	 */
	
	public DBRow getPacklistPrintTemplate(String accountId,String companyId)throws Exception;
	/**
	 * 查找所有的pallet
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 * 	wfh
	 */
	public DBRow[] getAllPallet(long ps_id,int invenStatus,int title, PageCtrl pc) throws Exception;
	
	/**
	 * 修改或者添加pallet
	 * @param para
	 * @return
	 * @throws Exception
	 */
	public long saveOrEditPallet(DBRow para, long adid) throws Exception;
	
	/**
	 * 修改pallet时添加日志
	 * @param logs
	 * @throws Exception
	 * wfh
	 */
	public void palletAddLog(DBRow logs) throws Exception ;
	
	/**
	 * 按照palle的Id查找Pallet
	 * @param palletId
	 * @return
	 * @throws Exception
	 * wfh
	 */
	public DBRow findPalletInventoryById(int palletId) throws Exception;
	
	/**
	 * 查找pallet的日志
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPalletLog(String startTime, String endTime,String palletType,long ps_id, PageCtrl pc) throws Exception;
	
	/**
	 * 查找所有的Title
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllTitle(String palletType, long ps_id) throws Exception;
	/**
	 * 按照pallet的仓库 title  type 查找
	 * @return
	 * @throws Exception
	 */
	public DBRow findPalletCountByIdTypeTitle(long ps_id,String palletType,long titleId) throws Exception;
	
	public DBRow getPalletDetails(int palletId) throws Exception;
	
	/**
	 * seal导入xls 
	 * @param request
	 * @return
	 * @thrws Exception
	 * wfh
	 */
	public DBRow importSeal4xls(HttpServletRequest request) throws Exception;
	
	/**
	 * seal导出xls 
	 * @param request
	 * @return
	 * @thrws Exception
	 * wfh
	 */
	public String exportSeal(HttpServletRequest request) throws Exception;
	
	/**
	 * 当当前的detail所属的资源为占用或者保留则查询space表中本资源下所有的task
	 * @param resources_id	资源ID
	 * @param entry_id		mainID
	 * @param resources_type	资源类型（门、停车位）
	 * @return
	 * @throws Exception
	 */
	public DBRow[] selectSpaceBySpaceAndEntryId(int resources_id,long entry_id,int resources_type,int equipmentId) throws Exception;

	/**
	 * 按照资源id 查找门的号码
	 * @param resources_id	资源id
	 * @return
	 * @throws Exception
	 */
	public DBRow findDoorByResourcesId(int resources_id) throws Exception;
	
	/**
	 * 按照资源id 查找停车位的号码
	 * @param resources_id	资源id
	 * @return
	 * @throws Exception
	 */
	public DBRow findSpotByResourcesId(int resources_id) throws Exception;
	/**
	 * 按照mainID查找车头车尾  
	 * @param entry_id
	 * @param equipment_type  1----车头 2-----车尾
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findTractorByEntryId(long entry_id,int equipment_type) throws Exception;
	
	/**
	 * 查找所有占用的资源
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOccupancySpaceByMainIdAndEquipmentId(long entry_id,int equipmentId) throws Exception;
	
	
	/**
	 * 查找所有释放的资源
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReleaseSpaceByMainIdAndEquipmentId(long entry_id,int equipmentId) throws Exception;
	
	/**
	 * 查找设备占用的资源
	 * @param equipmentId
	 * @return
	 * @throws Exception
	 */
	public DBRow findEquipmentOccupancySpace(int equipmentId) throws Exception;
	
	/**
	 * 查找checkOut时带走的entry
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCheckOutIdByEntryId(long entry_id) throws Exception;
	
	/**
	 * 查找关闭资源下的task
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findTaskClosedByEntry(long entry_id,int equipment_id) throws Exception;

	/**
	 * 查找设备下所有的customer或者title
	 * @param entry_id
	 * @param equipmentId
	 * @param relType
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCustomerOrTitleByEntryEquipment(long entry_id,int equipmentId, int relType) throws Exception;
	

	/**
	 * 查找设备下的task个数
	 * @param equipmentId
	 * @param relType
	 * @return
	 * @throws Exception
	 */
	public int findTaskCountByEquipmentId(long entry_id,int equipmentId) throws Exception;
	
	/**
	 * 添加loadbar
	 * @param para
	 * @return
	 * @throws Exception
	 */
	public long saveLoadBar(DBRow para) throws Exception;
	
	/**
	 * 查询EntryAnd设备下的loadBar
	 * @param dloId
	 * @param equipmentId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadBarByMainId(long dloId,int equipmentId) throws Exception;
	
	/**
	 * 查询所有的taskAppointment
	 * @param entry_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllTaskByMainId(long entry_id,long ps_id) throws Exception;
	
	/**
	 * 查询忘了checkout的设备
	 * @param ctnr
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findForgetCheckOutEquipmentByCtnr(String gate_container_no,String liscense_plate,long dlo_id,long ps_id,String equipmentIds) throws Exception;
	
	/**
	 * 关闭忘记checkout的设备
	 * @param request
	 * @throws Exception
	 */
	public void checkOutEquipment(long outByEntryId,DBRow data) throws Exception;
	/**
	 * checkout其它相同设备时给这条entry添加日志
	 * @param request
	 * @param adid
	 * @param main_id  添加的entryID
	 * @throws Exception
	 */
	public void addCheckOutLog(DBRow data, long main_id) throws Exception;

	/**
	 * 查询通知
	 * @param entry_id
	 * @param windowCheckInTime
	 * @param scheduleType
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findScheduleByMainId(long entry_id, int scheduleType) throws Exception;
	
	/**
	 * 按照通知ID查询通知
	 * @param scheduleId
	 * @return
	 * @throws Exception
	 */
	public DBRow findScheduleByScheduleId(int scheduleId) throws Exception;
	
	/**
	 * 按照taskId查询warehouse的通知
	 * @param detail_id
	 * @param scheduleType
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findScheduleByDetailId(int detail_id, int scheduleType) throws Exception;
	
	/**
	 * 更新seal
	 * @param pickUpSeal
	 * @param deliverySeal
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 */
	public long saveSealByEquipmentId(String pickUpSeal,String deliverySeal,int equipment_id) throws Exception;
	
	/**
	 * 按照设备ID查找设备
	 * @param equipmentId
	 * @return
	 * @throws Exception
	 */
	public DBRow findEquipmentByEquipmentId(int equipmentId) throws Exception; 
	
	/**
	 * 查询为关闭的task
	 * @param entry_id
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 */
	public int getNotCloseTaskByEquipmentId(long entry_id,int equipment_id) throws Exception;
	
	/**
	 * 查找load pallet
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getPalletInfoByDetailId(long dlo_detail_id) throws Exception;
	
	/**
	 * 查找receive pallet
	 * @author xujia
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getReceivePalletInfoByDetailId(long dlo_detail_id) throws Exception;
	
	/**
	 * 按照task number 查找所有的task
	 * @param number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findTaskByNumber(String number,int numberType) throws Exception;
	
	/**
	 * 查看task是否已经开始，返回未开始的task
	 * @param tasks
	 * @return
	 * @throws Exception
	 */
	public List<HoldDoubleValue<Long, String>> findTaskIsStart(List<HoldDoubleValue<Long, String>> tasks) throws Exception;
	
	/**
	 * 得到所有的customer
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllCustomer() throws Exception;
	/**
	 * 分页查找sn
	 * @param con_id
	 * @param pageNo
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSNByConID(long con_id,int pageNo,int pageSize, long ps_id) throws Exception;
	
	/**
	 * 查找所有的customer
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCustomer() throws Exception;
	/**
	 * 查找所有的small_parcel carrier
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSmallParcelCarrier() throws Exception;
	
	/**
	 * reload所有的order 有可能会保存扫描的orders的状态
	 * @param carrier_id
	 * @param customer_id
	 * @param is_keep_orders
	 * @return
	 * @throws Exception
	 */
	public DBRow reloadOrder(long detail_id,int is_keep_order) throws Exception;
	/**
	 * 过滤出来smallParcel这次需要的order   ?不必要存mysql？
	 * @param carrier_id
	 * @param customer_id
	 * @return
	 * @throws Exception
	 */
	public DBRow filterSmallParcel(long carrier_id,long customer_id,long detail_id) throws Exception;
	
	/**
	 * 扫描所有的smallParcel
	 * @param detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow scanAllPalletOrTackNO(long detail_id,long adid) throws Exception;
	
	/**
	 * 去掉pallet trackNO的扫描状态
	 * @param wms_order_type_id
	 * @param wms_order_id
	 * @param wms_scan_number_type
	 * @return
	 * @throws Exception
	 */
	public DBRow removeScanState(long wms_order_type_id,long detail_id,int wms_scan_number_type) throws Exception;
	
	/**
	 * 保存一条pallet 的扫描记录
	 * @param wms_order_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow scanPalletOrTrackNo(String scan_number,long detail_id,long adid) throws Exception;
	
	/**
	 * 查找扫描的order与pallet
	 * @param detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findLoadSmallParcelOrderByDetailId(long detail_id) throws Exception;
	
	/**
	 * 清掉task扫描的其他carrier的order
	 * @param carrier_id
	 * @param detail_id
	 * @throws Exception
	 */
	public void freshOtherCarrierOrder(long carrier_id,long detail_id) throws Exception;
	
	/**
	 * 查找设备下没有占用资源的task
	 * @param entry_id
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findNoSpaceTaskByEntryId(long entry_id,long equipment_id) throws Exception;
	
	/**
	 * 获得ctnr 的信息
	 * @param ctnr
	 * @param type
	 * @param time
	 * @param check_in_code
	 * @return
	 * @throws Exception
	 */
	public DBRow getCheckMessage(String ctnr,int type,String time,String check_in_code)throws Exception;
	
	/**
	 * 在checkout 的时候查询出来的数据不存在，则由用户选择是否新建一条，并返回
	 * @return
	 * @throws Exception
	 */
	public DBRow creatNewEquipmentOnCheckOut(HttpServletRequest request) throws Exception;
	
	/**
	 * 查找仓库的名称 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findPSNameByPsId(long ps_id) throws Exception;
	public DBRow getCheckInIndexJson(HttpServletRequest request) throws Exception;
	
	/**
	 * 发送通知
	 * @return
	 * @throws Exception
	 */
	public void assignSchedule(HttpServletRequest request) throws Exception;
} 



