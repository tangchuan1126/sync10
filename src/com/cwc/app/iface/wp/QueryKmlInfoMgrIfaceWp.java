package com.cwc.app.iface.wp;


import com.cwc.db.DBRow;

public interface QueryKmlInfoMgrIfaceWp {
	
	
	/**
	 * 查询位置Id
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long getPositionId(long psId, String position,int position_type) throws Exception;
	
	/**
	 * 删除多余的KML 文件
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public boolean  deleteInvalidKml(String path) throws Exception;
	

}
