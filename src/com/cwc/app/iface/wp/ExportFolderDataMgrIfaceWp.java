package com.cwc.app.iface.wp;


public interface ExportFolderDataMgrIfaceWp {


	/**
	 * 导出Excel
	 * @param ps_id
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public boolean createExcelNew(String ps_id, String path) throws Exception;

}
