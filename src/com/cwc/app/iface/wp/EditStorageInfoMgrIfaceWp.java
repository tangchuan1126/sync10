package com.cwc.app.iface.wp;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
public interface EditStorageInfoMgrIfaceWp {
	/**
	 * 编辑storage 对面的下面图层信息
	 * @param request
	 * @return 返回新的经纬度字符串
	 * @throws Exception
	 */
	public String editStorageKmlLayer(HttpServletRequest request) throws Exception;
	
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public  DBRow saveStorageLayer(HttpServletRequest request)throws Exception;


}