package com.cwc.app.iface.wp;

import java.util.List;

import com.cwc.db.DBRow;

public interface CreateKmlMgrIfaceWp {
	
	/**
	 * 查询基础信息
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public  DBRow[] qureyBaseData(String ps_id) throws Exception;

	/**
	 * 生产KML 文件  分开取各表数据
	 * @param ps_id
	 * @param storageTitle
	 * @return
	 * @throws Exception
	 */
	public List<DBRow> createKmlNew(String ps_id) throws Exception;
	
}
