package com.cwc.app.iface.wp;

import javax.servlet.http.HttpServletRequest;

public interface  DeleteKmlMgrIfaceWp {
	/**
	 * 删除门的kml
	 * @param ps_id
	 * @param tablename
	 * @return
	 * @throws Exception
	 */
	public  boolean  deleteDoor(String ps_id) throws Exception ;
	/**
	 * 清楚erea KML信息
	 * @param ps_id
	 * @param tablename
	 * @return
	 * @throws Exception
	 */
	public  boolean deleteErea(String ps_id) throws Exception ;
	/**
	 * 清除YardControl KML
	 * @param ps_id
	 * @param tablename
	 * @return
	 * @throws Exception
	 */
	public  boolean deleteYardControl(String ps_id) throws Exception ;
	/**
	 * 清除CateLog KML信息
	 * @param ps_id
	 * @param tablename
	 * @return
	 * @throws Exception
	 */

	public  boolean deleteCatelog(String ps_id) throws Exception ;
	/**
	 * 清除Location 信息
	 * @param ps_id
	 * @param tablename
	 * @return
	 * @throws Exception
	 */
	
	public  boolean deleteLocation(String ps_id) throws Exception ;

	/**
	 *  执行所有清除操作
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public  void  doAllDelete(String ps_id) throws Exception ;
	/**
	 * 清除摄像图对应的位置信息
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public boolean  deleteWebcam(String ps_id) throws Exception;
	
	/**
	 * 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public boolean  deleteCheckInZone(String ps_id) throws Exception;
	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	void doDeleteKml(HttpServletRequest request) throws Exception;
	

 
}
