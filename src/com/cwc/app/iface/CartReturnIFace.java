package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.exception.product.ProductNotCreateStorageException;
import com.cwc.app.exception.product.ProductNotExistException;
import com.cwc.db.DBRow;

public interface CartReturnIFace 
{
	public void put2Cart(HttpSession session,long pid,float quantity,int product_type) 
	throws  Exception;
	
	public DBRow[] getSimpleProducts(HttpSession session)throws Exception;
	
	public void clearCart(HttpSession session);
	
	public boolean isEmpty(HttpSession session)throws Exception;
	
	public void removeProduct(HttpSession session,long pid,int product_type)
	throws Exception;
	
	public void removeProductSelect(HttpSession session ,long pc_id)
	throws Exception;
	
	public void modQuantity(HttpSession session,String pids[],String quantitys[],String product_type[]) 
	throws  Exception;
	
	public void flush(HttpSession session)
	throws Exception;
	
	public void put2Cart(HttpServletRequest request)
	throws ProductNotCreateStorageException,ProductNotExistException,Exception;
	
	public void put2CartSelect(HttpServletRequest request)
	throws ProductNotCreateStorageException,ProductNotExistException,Exception;
	
	public DBRow[] getDetailProducts();
	
	public float getSumQuantity();
	
	public double getSumPrice();
	
	public boolean isHasLackingProduct();
	
}
