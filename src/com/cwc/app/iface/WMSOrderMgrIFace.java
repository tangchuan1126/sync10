package com.cwc.app.iface;

import java.util.Set;

import com.cwc.db.DBRow;
import com.vvme.wms.Receipt;
import com.vvme.wms.ReceiptLine;

public interface WMSOrderMgrIFace {

	/**
	 * Order 同步写到 WMS Orders OrderLines
	 * 
	 * @param order [b2b_order]
	 * @param orderlines [b2b_order_item]
	 * @return 返回 WMS OrderNo
	 * 
	 * @throws Exception
	 */
	public void save(DBRow order, DBRow[] orderlines) throws Exception;
	
	/**
	 * 同步 OrderLine
	 * 
	 * @param oid
	 * @param OrderNo
	 * @param lines
	 * @throws Exception
	 */
	public void saveOrderLine(long oid) throws Exception;
	
	/**
	 * 回写WMS:更新状态，更新Load,Appointment信息
	 * 
	 * @author Liang Jie
	 * @param b2bOid
	 * @param updateType WmsOrderUpdateTypeKey
	 * @throws Exception 
	 */
	public void update(long b2bOid, int updateType) throws Exception;
	
	public void update(long b2bOid, int updateType,DBRow data) throws Exception;
	
	/**
	 * 用于删除sync中order， 取消wms中order，
	 * 
	 * @param companyId
	 * @param customer_id
	 * @param orderNo
	 * @throws Exception
	 */
	public void cancel(String companyId, long customer_id, String orderNo, long ps_id) throws Exception;
	
	/**
	 * 删除wms中order，
	 * @param companyId
	 * @param customer_id
	 * @param orderNo
	 * @param ps_id
	 * @throws Exception
	 */
	public void delete(String companyId, long customer_id, String orderNo, long ps_id) throws Exception;
	/**
	 * 同步Order主信息 到WMS
	 * 
	 * @author wangcr
	 * @param b2bOid
	 * @throws Exception
	 */
	public void saveOrder(long b2bOid) throws Exception;
	
	
	/**
	 * 保存收货
	 * 
	 * @param main
	 * @param lines
	 * @throws Exception
	 */
	public long saveRN(Receipt main, Set<ReceiptLine> lines) throws Exception;
	
	/**
	 * 更新收货
	 * 
	 * @param main
	 * @param lines
	 * @return
	 * @throws Exception
	 */
	public long updateRN(Receipt main, Set<ReceiptLine> lines) throws Exception;
	
	/**
	 * 取消收货
	 * 
	 * @param CompanyID
	 * @param RNNo
	 * @return
	 * @throws Exception
	 */
	public boolean cancelRN(String CompanyID, long RNNo) throws Exception;
	
	/**
	 * 删除收货
	 * 
	 * @param CompanyID
	 * @param RNNo
	 * @return
	 * @throws Exception
	 */
	public boolean deleteRN(String CompanyID, long RNNo) throws Exception;
}