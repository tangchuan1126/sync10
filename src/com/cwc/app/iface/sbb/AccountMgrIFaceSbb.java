package com.cwc.app.iface.sbb;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jxl.Workbook;

import com.cwc.db.DBRow;

public interface AccountMgrIFaceSbb{
	
	public Map<String,DBRow[]> getAccountContext(long id) throws Exception;
	
	public Map<String,DBRow[]> getAccountList(Map<String,Object> parameter) throws Exception;
	
	public DBRow addAccount(DBRow parameter)throws Exception;
	
	public void dropAccount(long id)throws Exception;
	
	public DBRow modifyAccount(DBRow parameter,String flag)throws Exception;
	
	public boolean accountValidator(Map<String,String> parameter) throws Exception;
	
	public DBRow[] getPermissionContextList(long id) throws Exception;
	
	public DBRow modifyAccountPermission(DBRow parameter) throws Exception;
	
	public String croppedImage(Map<String,Object> parameter) throws Exception;
	
	public Map<String,DBRow[]> getAccountTitle(long id,String has,String unHas) throws Exception;
	
	public DBRow[] swapAccountTitle(DBRow parameter) throws Exception;
	
	public DBRow[] getAccountPhotoList(long id) throws Exception;
	
	public int modifyAccountPhoto(DBRow parameter) throws Exception;
	
	public void dropAccountPhoto(long id) throws Exception;
	
	public String exportAccountTitleBySearchResult(Map<String,String> parameter) throws Exception;
	
	public void modifyAccountTitleSort(DBRow parameter) throws Exception;
	
	public DBRow registrationFingerprint(Long adid,String fingerCode,String fingerBase64) throws Exception;
	
	public boolean accountLoginValidator(String account,String password) throws Exception;
	
	public Map<String,DBRow[]> getAccountDeptPostAndWareArea() throws Exception;
	
	public Map<String,String> addAccountExcel(DBRow param) throws Exception;
	
	public Map<String,Object> validateImportTitle(Workbook workbook) throws Exception;
	
	public void updateImportTitle(Workbook workbook) throws Exception;
	
	public Map<String,Object> accountLogin(Map<String,Object> params) throws Exception;
	
	public DBRow[] getAccountNavigationBar(long adid) throws Exception;
	
	public String getAccountDefaultHomePage(long adid) throws Exception;
	
	public Map<String,Object> accountInternaLoginNoThird(Map<String,Object> params) throws Exception;
	
	public Map<String,Object> accountInternaLoginThird(Map<String,Object> params) throws Exception;
	
	public Map<String,Object> accountSupplierLogin(Map<String,Object> params) throws Exception;

	public DBRow[] getUserMobileMenuRecordByAdid(long adid) throws Exception;
	
	public long addAccountImage(DBRow parameter) throws Exception;
	
	public DBRow getRequestData(HttpServletRequest request) throws IOException,Exception;
	
	public String[] changeTofficialFile(DBRow param) throws Exception;
	
	public DBRow[] getPostByDeptId(long deptId) throws Exception;
	
	public void refreshOpenfireMember(long adid, String account) throws Exception;
	/**
	 * 通过账号ID，查询账号角色和职务
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月24日 下午8:28:52
	 */
	public DBRow[] findAdminDepartmentPosts(long adid) throws Exception;
	/**
	 * 通过账号ID查询账号仓库和区域
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月24日 下午8:29:13
	 */
	public DBRow[] findAdminWarehouseAreas(long adid) throws Exception;
	/**
	 * 通过账号ID查询账号所有图像信息
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:wp
	 * @date:
	 */
	
	public DBRow[] findAccountPortrait(long adid) throws Exception;
	/**
	 * 通过账号ID查询账号信息
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月24日 下午8:29:13
	 */
	public DBRow GetUserInfoByAccount(String accont) throws Exception;
	// yuanxinyu
	public DBRow[] getCustomerId() throws Exception;
	
	public DBRow[] getCustomerTitleRelation(Map<String,Object> param) throws Exception;
	
       public DBRow[] getCustomerAddressByCustomerKey(long customer_key) throws Exception;

	/**
	 * 功能：根据warehouse_id查询仓库下所有的账号
	 * @param warehouse_id
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015年5月12日
	 */
	public DBRow[] findAllAccountByWarehouseId(long warehouse_id) throws Exception;
	
	public DBRow getAccountByAccount(String account) throws Exception;
}
