package com.cwc.app.iface.sbb;

import java.util.Map;

import us.monoid.json.JSONObject;

import com.cwc.db.DBRow;

public interface RoleMgrIFaceSbb{
	
	public DBRow[] getSearchRoleList(Map<String,Object> parameter) throws Exception;
	
	public boolean roleValidator(Map<String,String> parameter) throws Exception;
	
	public DBRow addRole(DBRow parameter) throws Exception;
	
	public DBRow modifyRole(DBRow parameter) throws Exception;
	
	public Map<String,String> dropRole(long id) throws Exception;
	
	public DBRow[] getPermissionContextList(long id) throws Exception;
	
	public DBRow modifyRolePermission(DBRow parameter) throws Exception;
	
	//袁新宇
	public DBRow[] getLefts(long adg_id) throws Exception;
	public DBRow[] getRights(long adg_id) throws Exception;
	public DBRow addGroupPost(JSONObject jsonData) throws Exception;
	public DBRow delGroupPost(String adg_id,String hasTitle) throws Exception;
	public DBRow getRoleById(long id) throws Exception;
	public JSONObject getGpDetail(JSONObject jsonData) throws Exception;
	public JSONObject getGpDetailById(long adg_id) throws Exception;
	public DBRow[] getSearchPostList(Map<String,Object> parameter) throws Exception;
	public DBRow modifyPost(DBRow parameter) throws Exception;
	public boolean postValidator(Map<String,String> parameter) throws Exception;
	public DBRow addPost(DBRow parameter) throws Exception;
}
