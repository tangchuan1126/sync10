package com.cwc.app.iface.sbb;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

/**
 * 新框架实施后废除此类
 * 
 * */
public interface UserTitleMgrIFaceSbb{
	
	public void changeUserTitlePriority(HttpServletRequest request) throws Exception;
	public DBRow[] getAdminHasTitleList(long adid) throws Exception;
	public DBRow[] getAdminUnhasTitleList(long adid) throws Exception;
	
}
