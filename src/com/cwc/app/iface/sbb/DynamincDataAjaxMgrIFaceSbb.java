package com.cwc.app.iface.sbb;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

/**
 * 新框架实施后废除此类
 * 
 * */
public interface DynamincDataAjaxMgrIFaceSbb{
	
	public DBRow[] getAreaByPsId(HttpServletRequest request) throws Exception;
	
}
