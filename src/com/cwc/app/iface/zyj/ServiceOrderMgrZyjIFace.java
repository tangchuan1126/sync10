package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ServiceOrderMgrZyjIFace {
	
	public void putToServiceProductReasonCartForSelect(HttpServletRequest request) throws Exception;
	public void removeServiceProductReason(HttpServletRequest request) throws Exception;
	public long applicationOrderOrWaybillPcServing(HttpServletRequest request) throws Exception;
	public void putToServiceProductReasonCartForAdd(HttpServletRequest request) throws Exception;
	public DBRow getServiceOrderDetailBySid(long sid) throws Exception;
	public DBRow[] getServiceOrderItemsByServiceId(long sid) throws Exception;
	public void saveServiceOrderReasonSolutions(HttpServletRequest request) throws Exception;
	public DBRow[] getServiceOrdersByPOrderId(long oid) throws Exception;
	public DBRow[] getServiceOrdersByWaybillId(long wid) throws Exception;
	public int getServiceOrderCountByWaybillId(long wid) throws Exception;
	public DBRow[] getServiceOrderReasonSolutionsBySid(long sid) throws Exception;
	public DBRow[] getServiceOrderReasonSolutionsBySid(long sid, int num) throws Exception;
	public int getServiceOrderReasonSolutionCountBySid(long sid) throws Exception;
	public void saveServiceIsNeedReturn(HttpServletRequest request) throws Exception;
	public DBRow[] getServiceBillOrdersBySid(long sid) throws Exception;
	public void loadServiceItemToCart(HttpServletRequest request) throws Exception;
	public void modServiceItems(HttpServletRequest request) throws Exception;
	public void modServiceQuantitySession(HttpServletRequest request) throws Exception;
	public DBRow[] getServiceOrderByPage(PageCtrl pc) throws Exception;
	public DBRow[] getServiceOrderByPage(PageCtrl pc, int status, int is_need_return, String st, String end, long create_adid) throws Exception;
	public DBRow[] searchServiceOrderByNumber(String search_key,int search_mode,PageCtrl pc) throws Exception ;
	public int getServiceOrderCountByPOrderId(long oid) throws Exception;
	public DBRow getServiceReasonSolutionBySrsid(long srs_id) throws Exception;
	public void updateServiceReasonSolutionBySrsid(HttpServletRequest request) throws Exception;
	public void updateServiceOrderBySid(DBRow row, long sid) throws Exception;
	public void handleReturnProductData()throws Exception;
}
