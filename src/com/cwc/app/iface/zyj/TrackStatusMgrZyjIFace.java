package com.cwc.app.iface.zyj;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface TrackStatusMgrZyjIFace {

	//暂时不用
	public DBRow[] getTrackStatusListByShipCom(String shipCom) throws Exception;
	//暂时不用
	public DBRow[] getShipCompanyList() throws Exception;
	
	public Map<String, DBRow[]> getTrackStatusMapByShipCom() throws Exception;
	public boolean addTrackStatus(HttpServletRequest request) throws Exception;
	public boolean updateTrackStatus(HttpServletRequest request) throws Exception;
	public DBRow getTrackStatusById(HttpServletRequest request) throws Exception;
	public void deleteById(HttpServletRequest request) throws Exception;
	
}
