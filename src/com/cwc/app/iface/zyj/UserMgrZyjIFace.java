package com.cwc.app.iface.zyj;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface UserMgrZyjIFace {
	public long addUser(String userName , String password , String phone , String address , int age , double weight) throws Exception ; 
	public DBRow[] getAllUser() throws Exception ;
	public DBRow[] getUsersByPage(PageCtrl pc) throws Exception; 
}
