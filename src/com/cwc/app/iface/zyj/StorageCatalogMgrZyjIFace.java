package com.cwc.app.iface.zyj;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface StorageCatalogMgrZyjIFace {
	
	public DBRow getStorageCatalogById(long storageId) throws Exception;
	public long addProductStorageCatalog(HttpServletRequest request)throws Exception;
	public long updateProductStorageCatalogBasic(HttpServletRequest request)throws Exception;
	public long updateProductStorageCatalogDetailSend(HttpServletRequest request)throws Exception;
	public long updateProductStorageCatalogDetailDeliver(HttpServletRequest request)throws Exception;
	public DBRow getProviceInfoById(long proId) throws Exception;
	public DBRow[] getProductStorageCatalogByType(int storageType,PageCtrl pc) throws Exception;
	
	public String exportStorageAddressAction(HttpServletRequest request)throws Exception;
	
	public HashMap<String, DBRow[]> excelShow(String filename,String type,int storageType)throws Exception; 
	
	public void uploadStorageAddress(HttpServletRequest request)throws Exception; 
	
	public DBRow[] getProductInfoByStorageId() throws Exception;
	public DBRow[] getProductCodeInfoByStorageId() throws Exception;
	public DBRow[] getSerialProductByStorageId() throws Exception ;
	public DBRow[] getStorageAllProvinces() throws Exception;
	public DBRow[] getProductCodeByBarCode(String barCode) throws Exception ;
	
	public DBRow[] findProductLotNumberByTitlePcIdAdmin(boolean adidNullToLogin,long adid, String title_ids, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	
	public DBRow[] findProductStorages(boolean adidNullToLogin,String ps_id, int type, long adid, String title_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProductStorageByName(boolean adidNullToLogin,String pc_line_id, String catalog_id,String ps_id,String code,int union_flag,long adid, String title_id, int type, String lot_number_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProductLotNumberIdAndNameByTitlePcIdAdmin(boolean adidNullToLogin,long adid, String title_ids, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception;

}
