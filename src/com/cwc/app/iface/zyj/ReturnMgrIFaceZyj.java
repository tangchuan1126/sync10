package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ReturnMgrIFaceZyj 
{
	public DBRow[] findReturnPalletRows(long return_receive_id, PageCtrl pc) throws Exception;
	public void addReturnPalletRowByReceive(String pallet_no, long return_receive_id, long loginUserId) throws Exception;
	public void addOrUpdateReturnPalletRow(HttpServletRequest request) throws Exception;
	public void deleteReturnPalletRow(HttpServletRequest request) throws Exception;
	public DBRow findReturnPalletRowById(long pallet_view_id) throws Exception;
	public DBRow[] findReturnPalletItemRows(long pallet_view_id, PageCtrl pc) throws Exception;
	public void addOrUpdateReturnPalletItemRow(HttpServletRequest request) throws Exception;
	public DBRow findReturnPalletItemRowById(long item_id) throws Exception;
	public void deleteReturnPalletItemRow(long item_id) throws Exception;
	public String changeServicePackagingKeyStrToNameStr(String packaging) throws Exception ;
	public String changeServiceAccessoriesKeyStrToNameStr(String accessories) throws Exception;
	public int countPalletItemCountByPalletId(long pallet_id)throws Exception;
	public int countPalletCountByReceiveId(long receive_id)throws Exception;
	public DBRow[] getReturnPalletsByPara(long return_receive_id,String palletNo,String ra_rv,String cust_ref_type,String cust_ref_no,PageCtrl pc) throws Exception;//根据参数查询
}
