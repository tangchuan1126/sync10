package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ContainerMgrIFaceZyj {

	public DBRow findContainerByNumber(String number) throws Exception;
	public DBRow findBoxTypeRelateInfoByTypeId(long type_id) throws Exception;
	public DBRow findClpTypeRelateInfoByTypeId(long type_id) throws Exception;
	public DBRow findInnerBoxTypeRelateInfoByTypeId(long type_id) throws Exception;
	public DBRow findTlpTypeRelateInfoByTypeId(long type_id) throws Exception;
	public void exchangeClpToPsSort(HttpServletRequest request) throws Exception;
	public DBRow[] findContainerInnerInfoByContainerId(long containerId, PageCtrl pc) throws Exception;
	public DBRow[] addBoxContainerBatch(HttpServletRequest request) throws Exception;
	public void batchInsertContainerTypes() throws Exception;
	public void deleteContainerTypeAndExample() throws Exception;
	public DBRow[] findAllContainer(PageCtrl pc) throws Exception;
	public DBRow[] findContainerByTypeContainerTypeName(String key , long type ,int container_type, PageCtrl pc) throws Exception;
	public String findILPContainerWithPName(long container_id) throws Exception;
	public void batchInsertContainerTypesAndContainers(long pc_id_gt, int typeCount, int containerCount, int tlpContainerCount, int containerRelateCount) throws Exception;
	public DBRow findBlpTypeFirstForShipTo(long pc_id, long ps_id) throws Exception;
	public DBRow findClpTypeFirstForShipTo(long pc_id, long title_id, long ps_id) throws Exception;
	public DBRow findContainerSumByContainerId(long containerId) throws Exception;
	/**
	 * 根据商品查询ilp
	 * @param pcid 商品Id
	 * @param begin 数据开始，可为0
	 * @param length  数据条数，可为0
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findIlpTypesByPcid(long pcid, int begin, int length)throws Exception;
	/**
	 * 根据商品查询Blp类型
	 * @param pcid
	 * @param begin
	 * @param length
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBoxTypesByPcId(long pcid, int begin, int length) throws Exception;
	public DBRow[] selectIlp(long productId, int length)throws Exception;
	public DBRow[] getBoxSkuByPcId(long pc_id, long ship_to) throws Exception;
	

}
