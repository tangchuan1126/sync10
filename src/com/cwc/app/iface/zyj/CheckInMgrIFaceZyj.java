package com.cwc.app.iface.zyj;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.fr.base.core.json.JSONObject;

public interface CheckInMgrIFaceZyj {
	
	public String checkInImportContainers(HttpServletRequest request)throws Exception;
 
	/**
	 * 整体提货
	 * @param ic_id
	 * @param ship_entryd_id
	 * @throws Exception
	 */
	public void updateBatchContainerPlatesShipByIcid(long ic_id, long ship_entryd_id) throws Exception;
	/**
	 *单个plate提货
	 * @param icp_id
	 * @param dlo_detail_id
	 * @throws Exception
	 */
	public void updateContainerPlateShipByIcpid(long icp_id, long dlo_detail_id,DBRow loginRow ) throws Exception;
	/**
	 *单个plate提货
	 * @param plateNo
	 * @param ship_entryd_id
	 * @throws Exception
	 */
	public void updateContainerPlateShipByPlateNo(String plateNo, long ship_entryd_id) throws Exception;
	/**
	 * 更新导入的containerInfo的状态
	 * @param status
	 * @param icid
	 * @throws Exception
	 */
	public void updateContainerInfoStatus(int status, long icid) throws Exception;
	/**
	 * 通过entryID查询container信息
	 * @param entryId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerInfosByEntryId(long entryId, PageCtrl pc) throws Exception;
	/**
	 * 通过ContainerInfoID查询Plates
	 * @param containerNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlatesByIcid(long ic_id, PageCtrl pc)throws Exception;
	/**
	 * 批量添加plate
	 * @param dlo_detail_id
	 * @param ic_id
	 * @param plates
	 * @return
	 * @throws Exception
	 */
	public void batchAddContainerPlatesReceived(int receive_order_type,long dlo_detail_id, long ic_id, DBRow[] plates, long adid, String receive_time, long ps_id) throws Exception;
	/**
	 * 查询容器哪些staging有哪个托盘
	 * @param icid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findStagingPlatesByIcid(long icid, PageCtrl pc)throws Exception;
	/**
	 * 通过entryID查询pono信息
	 * @param entryId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPonoInfosByEntryId(long entryId, PageCtrl pc) throws Exception;
	/**
	 * 表报用 查询出入库状态
	 * @return
	 * @throws SystemException 
	 */
	public DBRow[] findPlateNoInOutInfo(String startTime,String endTime, long ps_id) throws Exception ;
	/**
	 * filter托盘库存
	 * @param type
	 * @param startTime
	 * @param endTime
	 * @param ps_id_in
	 * @param status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchPlatesLevelInventary(int type, String startTime, String endTime, long ps_id_in, int status,long entry_id_in, long entry_id_out, long ic_id_in, long ic_id_out, PageCtrl pc)throws Exception;
	/**
	 * 得到两个时间相差多少天
	 * @param strStartDate
	 * @param strEndDate
	 * @param rtnType
	 * @return
	 * @throws ParseException
	 */
	public double getDiffDateOrhhmmss(String strStartDate, String strEndDate,String rtnType)throws ParseException ;
	public float floatRemain1(double day_count);

	/**
	 * 通过entryId获取收到的托盘总数
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByEntryIdIn(long entry_id) throws Exception ;
	/**
	 * 通过entryId获取运出的托盘总数
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByEntryIdOut(long entry_id) throws Exception ;
	/**
	 * 通过ic_id获取收到的托盘总数
	 * @param ic_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByIcidIn(long ic_id) throws Exception ;
	/**
	 * 通过ic_id获取运出的托盘总数
	 * @param ic_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByIcidOut(long ic_id) throws Exception ;
	/**
	 * 通过entry Detail ID获取进来的plate总数
	 * @param dlod_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByEntryDetailIdOut(long dlod_id) throws Exception;
	/**
	 * 从wms下载load信息，存在本地
	 * @param loadNo
	 * @param companyId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findLoadInfosWmsAndInsertLocalForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID, String status, long adid, HttpServletRequest request) throws Exception;
	/**
	 * 判断entry是否有托盘进出
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow checkEntryHasPlateInOrOut(long entry_id) throws Exception;
	
	/**
	 * 根据icid更新导入container信息表
	 * @param icid
	 * @param row
	 * @throws Exception
	 */
	public void updateContainerInfoByIcid(long icid, DBRow row) throws Exception;
	
	/**
	 * 添加托盘货库存
	 * @param plate_no
	 * @param customer_id
	 * @param customer
	 * @param box_count
	 * @param staging_area_id
	 * @param staging_area_name
	 * @param associate_order_type
	 * @param associate_order
	 * @param receive_user
	 * @param receive_time
	 * @param receive_order_type
	 * @param receive_order
	 * @return
	 * @throws Exception
	 */
	public DBRow insertPlateLevelProductInventary(String plate_no, long customer_id, String customer, double box_count
			, long staging_area_id, int associate_order_type, long associate_order
			, long receive_user, String receive_time, int receive_order_type, long receive_order, double weight, long ps_id) throws Exception;
	/**
	 * 更新提货信息
	 * @param plate_inventary_id
	 * @param shipped_user
	 * @param shipped_time
	 * @param shipped_order_type
	 * @param shipped_order
	 * @throws Exception
	 */
	public void updatePlateLevelProductInventary(long plate_inventary_id
			, long shipped_user, String shipped_time, int shipped_order_type, long shipped_order)throws Exception;
	/**
	 * 更新托盘位置
	 * @param statgingAreaId
	 * @throws Exception
	 */
	public void updatePlateLevelProductInventaryStaging(long plate_inventary_id,long statgingAreaId) throws Exception;
	/**
	 * 从本地查询数据
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @param status_one
	 * @param adid
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadInfosLocalForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID, String status_one, long adid, HttpServletRequest request) throws Exception;
	/**
	 * 判断从本地或者wms查询数据
	 * @param order_type
	 * @param order_no
	 * @param CompanyID
	 * @param CustomerID
	 * @param status_one
	 * @param adid
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadInfosLocalOrWmsForConsolidate(int order_type, String order_no, String CompanyID, String CustomerID, String status_one, long adid, HttpServletRequest request) throws Exception;
	/**
	 * 查询托盘信息
	 * @param receiveType
	 * @param receiveOrder
	 * @param shippedType
	 * @param shippedOrder
	 * @param ps_id
	 * @param startTime
	 * @param endTime
	 * @param plateNo
	 * @param plate_inventary_id
	 * @param associateType
	 * @param associateOrder
	 * @param timeType
	 * @param plateStatus
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventary(int receiveType, long receiveOrder, int shippedType, long shippedOrder
			,long ps_id, String startTime, String endTime, String plateNo, long plate_inventary_id
			, int associateType, long associateOrder, int timeType, int plateStatus, PageCtrl pc)throws Exception;
	/**
	 * 通过主键查询：plate_level_product_inventary
	 * @param pi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findPlateLevelProductInventaryById(long pi_id) throws Exception;
	/**
	 * 托盘商品库存索引
	 * @param search_key
	 * @param search_mode
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchPlateProductInventarys(String search_key,int search_mode,PageCtrl pc)throws Exception;
	/**
	 * 通过索引查询托盘信息
	 * @param receiveType
	 * @param receiveOrder
	 * @param shippedType
	 * @param shippedOrder
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryAllInfoByOrderType(int receiveType, long receiveOrder, int shippedType, long shippedOrder, PageCtrl pc)throws Exception;
	/**
	 * 通过关联信息更新托盘
	 * @param associateType
	 * @param associateOrder
	 * @param shipped_user
	 * @param shipped_time
	 * @param shipped_order_type
	 * @param shipped_order
	 * @param statgingAreaId
	 * @throws Exception
	 */
	public void updatePlateShippedInfoByAssociate(int associateType, long associateOrder
			, long shipped_user, String shipped_time, int shipped_order_type, long shipped_order, long statgingAreaId) throws Exception;


	/**
	 * 添加account
	 * @throws Exception
	 */
	public void addAllWmsAccountIDToOSO() throws Exception;
	/**
	 * 添加account
	 * @throws Exception
	 */
	public void addAllWmsSupplierIDToOSO() throws Exception;
	/**
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月4日
	 */
	public void addAllWmsCustomerIDToOSO() throws Exception;
	/**
	 * 通过shippedInfo更新托盘
	 * @param shippedOrderType
	 * @param shippedOrder
	 * @throws Exception
	 * @Date   2014年10月22日
	 */
	public void clearPlateLevelProductInventaryShipped(int shippedOrderType, long shippedOrder) throws Exception;
	/**
	 * 通过entryin prupose查询设备
	 * @param equipment_purpose
	 * @param check_in_entry
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:07:11
	 */
	public DBRow[] checkInWindowFindEquipmentByEntryInIdpurpose(int equipment_purpose, long check_in_entry, int equipment_type) throws Exception;
	/**
	 * 通过entryOut prupose查询设备
	 * @param equipment_purpose
	 * @param check_in_entry
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:07:11
	 */
	public DBRow[] checkInWindowFindEquipmentByEntryOutIdpurpose(int equipment_purpose, long check_in_entry, int equipment_type) throws Exception;
	/**
	 * 添加设备
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:13:10
	 */
	public long checkInWindowAddEquipment(HttpServletRequest request)  throws Exception;
	/**
	 * 通过设备ID，查询设备、资源信息
	 * @param id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午6:45:06
	 */
	public DBRow findEquipmentReOccupyById(long id) throws Exception;
	
	/**
	 * 更新设备
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午6:48:56
	 */
	public DBRow updateEquipmentById(HttpServletRequest request) throws Exception;
	/**
	 * 删除设备及关系
	 * @param id
	 * @throws Exception
	 * @author:zyj
	 * @return 
	 * @date: 2014年11月25日 下午9:59:12
	 */
	public int deleteEquipmentById(HttpServletRequest request) throws Exception;
	/**
	 * 通过entry查询设备列表
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月26日 下午8:37:22
	 */
	public DBRow[] windowCheckInFindEquipments(long entry_id) throws Exception;
	/**
	 * 通过entryId查询entry的task所在的门或停车位
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月26日 下午9:09:45
	 */
	public DBRow[] findTasksOccupysEquipmentsByEntryId(long entry_id)throws Exception;
	/**
	 * windowCheckIn修改信息
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月26日 下午9:16:40
	 */
	public void windowCheckInMain(HttpServletRequest request)throws Exception;
	/**
	 * 通过设备号查询在yardDrop设备
	 * @param number
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午9:38:48
	 */
	public DBRow[] findInYardDropEquipmentsByNumber(String number, long ps_id,int equipment_type) throws Exception;
	/**
	 * 删除任务
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月1日 下午2:38:22
	 */
//	public void windowCheckInDeleteTask(HttpServletRequest request) throws Exception;
	/**
	 * 通过设备ID查task
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月2日 上午9:27:22
	 */
	public DBRow[] findTasksByEquipmentId(long equipment_id) throws Exception;
	/**
	 * 通过设备号查询在yardDrop设备
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午9:38:48
	 */
	public DBRow[] findInYardDropEquipmentsByNumber(HttpServletRequest request) throws Exception;
	/**
	 * 通过task添加order信息
	 * @param taskId
	 * @param adid
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 上午10:06:15
	 */
	public void addTaskOrdersByTaskId(long taskId, long adid) throws Exception;
	/**
	 * 通过资源类型和资源ID，获取资源名称
	 * @param resourceType
	 * @param resourceId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午4:47:47
	 */
	public String windowCheckInHandleOccupyResourceName(int resourceType, long resourceId) throws Exception;
	/**
	 * 更新主单据和设备的windowCheckIn时间
	 * @param entry_id
	 * @param adid
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月13日 下午8:00:16
	 */
	public void windowCheckInUpdateMainTimeEquipment(long entry_id, long adid) throws Exception;
	/**
	 * 更改equipment类型
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月16日 下午1:09:23
	 */
	public void windowCheckInChangeEquipmentType(HttpServletRequest request) throws Exception;
	/**
	 * web端添加或者更新设备
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午4:19:19
	 */
	public DBRow webAddOrUpdateEquipment(HttpServletRequest request) throws Exception;
	/**
	 * 通过Json更新或者添加设备
	 * @param json
	 * @param adminLoginBean
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午4:19:35
	 */
	public DBRow addOrUpdateEquipment(JSONObject json, AdminLoginBean adminLoginBean) throws Exception;
	/**
	 * web端删除设备
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午4:35:26
	 */
	public int webDeleteEquipment(HttpServletRequest request) throws Exception;
	/**
	 * 删除设备
	 * @param json
	 * @param adminLoginBean
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午4:35:48
	 */
	public int deleteEquipment(JSONObject json, AdminLoginBean adminLoginBean)throws Exception;
	/**
	 * web端windowCheckIn添加task
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午5:08:44
	 */
	public void webCheckInWindowAddOrUpdateTask(HttpServletRequest request) throws Exception;
	/**
	 * 添加task
	 * @param json
	 * @param adminLoginBean
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午5:08:57
	 */
	public void checkInWindowAddOrUpdateTask(final JSONObject json, final AdminLoginBean adminLoginBean) throws Exception;
	/**
	 * web端删除task
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午5:17:46
	 */
	public void webWindowCheckinDeleteTask(HttpServletRequest request) throws Exception;
	/**
	 * 删除task
	 * @param json
	 * @param adminLoginBean
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午5:18:04
	 */
	public void windowCheckInDeleteTask(JSONObject json, AdminLoginBean adminLoginBean) throws Exception;
	/**
	 * 更改equipment类型
	 * @param request
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月16日 下午1:09:23
	 */
	public void webWindowCheckInChangeEquipmentType(HttpServletRequest request) throws Exception;
	/**
	 * 更改设备类型
	 * @param json
	 * @param adminLoginBean
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午7:54:36
	 */
	public void webWindowCheckInChangeEquipmentType(JSONObject json, AdminLoginBean adminLoginBean) throws Exception;
	/**
	 * 查询inYardDropOffEquipment
	 * @param equipmentNumber
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午8:56:27
	 */
	public DBRow[] findInYardDropOffEquipmentByNumberNotThisEntry(String equipmentNumber, long ps_id, long entry_id) throws Exception;
	/**
	 * 通过设备号查询在yardDrop设备，非本entry
	 * @param request
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午9:38:48
	 */
	public DBRow[] findInYardDropEquipmentsByNumberNotThisEntry(HttpServletRequest request) throws Exception;
	/**
	 * 通过设备ID查询设备及tasks
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月25日 上午11:35:36
	 */
	public DBRow findEquipmentInfoAndEquipmentTasks(long equipment_id) throws Exception;
	/**
	 * 通过entry明细ID，查询task设备资源
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月2日 下午3:20:33
	 */
	public DBRow findTaskEquipmentResourceByDetailId(long detail_id) throws Exception;
	/**
	 * 查询supervisors
	 * @param ps_id
	 * @param group_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月26日 下午12:32:40
	 */
	public DBRow findSupervisors(long ps_id, long group_id) throws Exception;
	/**
	 * 通过entry明细ID，查询task设备资源，返回值类似于orderCheck
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月2日 下午3:20:33
	 */
	public DBRow findTaskEquipmentResourceByDetailIdSameCheck(long detail_id) throws Exception;
	/**
	 * task修改门、停车位或设备
	 * @param json
	 * @param adminLoginBean
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月27日 下午5:04:41
	 */
	public void windowCheckInTaskChangeResources(JSONObject json, AdminLoginBean adminLoginBean) throws Exception;
	/**
	 * 查某个entry下未离开的设备及task信息
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月31日 上午11:25:44
	 */
	public DBRow[] getNotLeftEquipmentByEntryAndTotalTask(long entry_id) throws Exception;
	/**
	 * 判断entry的所有task有没有发通知
	 * @param entryId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月5日 下午6:50:42
	 */
	public boolean whetherWriteWindowCheckInTime(long entryId) throws Exception;
	/**
	 * 调整entry明细的数据
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年2月13日 下午4:57:09
	 */
	public void adjustReceiptNoInEntryDetailOrderSystem() throws Exception;
	

}
