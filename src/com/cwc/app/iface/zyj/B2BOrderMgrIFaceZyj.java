package com.cwc.app.iface.zyj;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.json.JsonObject;

public interface B2BOrderMgrIFaceZyj {
	
	public DBRow[] fillterB2BOrder(long send_psid, long receive_psid, PageCtrl pc, int status, int declaration,
			int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id)
			throws Exception;
	
	public DBRow[] searchB2BOrderByNumber(String search_key, int search_mode, PageCtrl pc) throws Exception;
	
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc)
			throws Exception;
	
	public DBRow[] getNeedTrackReadyB2BOrder(long product_line_id, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackTagB2BOrder(long product_line_id, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackThirdTagB2BOrder(long product_line_id, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackQualityInspectionB2BOrder(long product_line_id, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackProductFileB2BOrder(long product_line_id, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackSendB2BOrderByPsid(long ps_id, String cmd, PageCtrl pc) throws Exception;
	
	public DBRow[] getNeedTrackReceiveB2BOrderByPsid(long ps_id, String cmd, PageCtrl pc) throws Exception;
	
	public DBRow[] trackOceanShippingB2BOrder(String cmd, PageCtrl pc) throws Exception;
	
	public DBRow[] getAllB2BOrder(PageCtrl pc) throws Exception;
	
	public String getB2BOrderLastTimeCreateByAdid(HttpServletRequest request) throws Exception;
	
	public String importB2BOrderDetail(String fileName) throws Exception;
	
	public DBRow[] errorImportB2BOrderDetail(String fileName) throws Exception;
	
	public long addB2BOrder(HttpServletRequest request) throws Exception;
	
	public float getB2BOrderVolume(long b2b_oid) throws Exception;
	
	public float getB2BOrderWeight(long b2b_oid) throws Exception;
	
	public double getB2BOrderSendPrice(long b2b_oid) throws Exception;
	
	public DBRow getDetailB2BOrderById(long b2b_oid) throws Exception;
	
	public DBRow[] getB2BOrderLogs(long b2b_oid, int number) throws Exception;
	
	public DBRow[] getB2BOrderItemByB2BOrderId(long b2b_oid, PageCtrl pc, String sidx, String sord,
			FilterBean fillterBean) throws Exception;
	
	public DBRow getSumB2BOrderFreightCost(String b2b_oid) throws Exception;
	
	public DBRow[] findB2BOrderFreightCostByB2BOrderId(long b2b_oid) throws Exception;
	
	public DBRow[] getB2BOrderLogsByB2BOrderIdAndType(long b2b_oid, int[] types) throws Exception;
	
	public long updateB2BOrderBasic(HttpServletRequest request) throws Exception;
	
	public void saveB2BOrderItem(HttpServletRequest request) throws Exception;
	
	public void modB2BOrderItem(HttpServletRequest request) throws Exception;
	
	public void addB2BOrderItem(HttpServletRequest request) throws Exception;
	
	public void delB2BOrderItem(HttpServletRequest request) throws Exception;
	
	public String downloadB2BOrder(HttpServletRequest request) throws Exception;
	
	public void updateB2BOrderProdures(HttpServletRequest request) throws Exception;
	
	public void updateB2BOrderFreight(HttpServletRequest request) throws Exception;
	
	public void uploadB2BOrderInvoice(HttpServletRequest request) throws Exception, FileTypeException, FileException;
	
	public DBRow[] getB2BOrderFreightCostByB2BOrderId(long b2b_oid, PageCtrl pc) throws Exception;
	
	public DBRow b2BOrderSetFreight(HttpServletRequest request) throws Exception;
	
	public void updateB2BOrderProductFileAndLogs(HttpServletRequest request) throws Exception;
	
	public void handleB2BOrderProductPictureUp(HttpServletRequest request) throws Exception;
	
	public void hanleB2BOrderStockInSet(HttpServletRequest request) throws Exception;
	
	public void handleB2BOrderDeclaration(HttpServletRequest request) throws Exception;
	
	public void handleB2BOrderClearance(HttpServletRequest request) throws Exception;
	
	public void handleB2BOrderTag(HttpServletRequest request) throws Exception;
	
	public String checkB2BOrderProductTagFileFinish(HttpServletRequest request) throws Exception;
	
	public void handleB2BOrderThirdTag(HttpServletRequest request) throws Exception;
	
	public String saveB2BOrderProductTagFile(HttpServletRequest request) throws Exception;
	
	public void b2BOrderCertificateFollowUp(HttpServletRequest request) throws Exception;
	
	public void addB2BOrderCertificateAddFile(HttpServletRequest request) throws Exception;
	
	public String checkB2BOrderProductFileFinish(long b2b_oid) throws Exception;
	
	public void b2BOrderProductFileFollowUp(HttpServletRequest request) throws Exception;
	
	public void updateB2BOrderPcModelFinishToNeed(HttpServletRequest request) throws Exception;
	
	public void hanleB2BOrderQualityInspection(HttpServletRequest request) throws Exception;
	
	public void updateB2BOrderCertificateAndLogs(HttpServletRequest request) throws Exception;
	
	public void addB2BOrderLog(HttpServletRequest request) throws Exception;
	
	public void updateB2BOrderItemVW(long b2b_oid) throws Exception;
	
	public void delB2BOrder(HttpServletRequest request) throws Exception;
	
	public DBRow[] getB2BOrderProductByName(long b2b_oid, String name) throws Exception;
	
	public String exportProductBarcodeByB2BOrder(HttpServletRequest request) throws Exception;
	
	public DBRow getB2BOrderItemById(HttpServletRequest request) throws Exception;
	
	public DBRow[] showReceiveAllocate(long b2b_oid) throws Exception;
	
	public DBRow[] getB2BOrderOrProductCountGroupCountry(HttpServletRequest request) throws Exception;
	
	public DBRow[] getB2BOrderOrProductCountGroupProvince(HttpServletRequest request) throws Exception;
	
	public JsonObject getB2BOrderOrProductCountGroupCountryJson(HttpServletRequest request) throws Exception;
	
	public JsonObject getB2BOrderOrProductCountGroupProvinceJson(HttpServletRequest request) throws Exception;
	
	public DBRow[] getB2BOrderOrProductCountAnalyze(String start_time, String end_time, String catalog_id,
			String pro_line_id, String pc_name, long ca_id, long ccid, long pro_id, long cid, int type,
			String title_id, PageCtrl pc) throws Exception;
	
	public String exportB2BOrderOrProductCountAnalyze(HttpServletRequest request) throws Exception;
	
	public DBRow importB2BOrder(String fileName, int type, String[] orderOrWms) throws Exception;
	
	public DBRow addB2BOrderByJN(String fileName, int type, String[] orderOrWms) throws Exception;
	
	/**
	 * 
	 * 根据订单id获取运单信息
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception <b>Date:</b>2014-8-15下午3:25:13<br>
	 * @author: cuicong
	 */
	public List<List<DBRow>> getTransportByOderId(long b2b_oid) throws Exception;
	
	/**
	 * 
	 * 添加订单
	 * 
	 * @param dbRow
	 * @throws Exception <b>Date:</b>2014-12-23下午8:06:09<br>
	 * @author: cuicong
	 */
	public long addB2BOrder(DBRow dbRow) throws Exception;
	
	/**
	 * 
	 * 添加订单明细
	 * 
	 * @param dbRow
	 * @throws Exception <b>Date:</b>2014-12-23下午8:06:46<br>
	 * @author: cuicong
	 */
	
	public long addB2BOrderItem(DBRow dbRow) throws Exception;
	
	/**
	 * 
	 * 删除订单明细
	 * 
	 * @param b2b_detail_id
	 * @throws Exception <b>Date:</b>2014-12-24下午3:39:07<br>
	 * @author: cuicong
	 */
	public void deleteB2BOrderItem(long b2b_detail_id) throws Exception;
	
	/**
	 * 
	 * 删除订单主体
	 * 
	 * @param b2b_oid
	 * @throws Exception <b>Date:</b>2014-12-24下午3:39:45<br>
	 * @author: cuicong
	 */
	public void deleteB2BOrder(long b2b_oid) throws Exception;
	
	/**
	 * 
	 * 更新明细
	 * 
	 * @param b2b_detail_id
	 * @param dbRow
	 * @throws Exception <b>Date:</b>2014-12-24下午5:29:31<br>
	 * @author: cuicong
	 */
	public void updateB2BOrderItem(long b2b_detail_id, DBRow dbRow) throws Exception;
	
	/**
	 * 
	 * 更新主体
	 * 
	 * @param b2b_oid
	 * @throws Exception <b>Date:</b>2014-12-24下午3:39:45<br>
	 * @author: cuicong
	 */
	public void updateB2BOrder(Long b2b_oid, DBRow dbRow) throws Exception;
	
	/**
	 * 
	 * {获取订单主体}
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-25上午9:44:17<br>
	 * @author: cuicong
	 */
	public DBRow getB2BOrder(long b2b_oid) throws Exception;
	
	/**
	 * 
	 * 获取订单明细列表
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-25上午9:44:50<br>
	 * @author: cuicong
	 */
	public DBRow[] getB2BOrderItems(long b2b_oid) throws Exception;
	
	/**
	 * 
	 * 获取订单明细
	 * 
	 * @param b2b_oid_detal
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-25上午9:45:23<br>
	 * @author: cuicong
	 */
	public DBRow getB2BOrderItem(long b2b_oid_detal) throws Exception;
	
	/**
	 * 
	 * {过滤订单}
	 * 
	 * @param dbRow
	 * @param ctrl
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-26上午11:02:26<br>
	 * @author: cuicong
	 */
	public DBRow[] fillterB2BOrder(DBRow dbRow, PageCtrl ctrl) throws Exception;
	
	/**
	 * 
	 * 
	 * 
	 * @param dbRow
	 * @param ctrl
	 * @return
	 * @throws Exception <b>Date:</b>2015-1-5下午4:12:30<br>
	 * @author: cuicong
	 */
	public DBRow[] fillterCustomerId(DBRow dbRow, PageCtrl ctrl) throws Exception;
	
	/**
	 * 
	 * @param dbRow
	 * @param ctrl
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterCustomerBrand(DBRow dbRow, PageCtrl ctrl) throws Exception;
	
	/**
	 * 
	 * {过滤订单item}
	 * 
	 * @param dbRow
	 * @param ctrl
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-26上午11:02:26<br>
	 * @author: cuicong
	 */
	public DBRow[] fillterB2BOrderItem(long b2b_oid, String name,String title) throws Exception;
	
	public long addB2BOrderLog(DBRow dbRow) throws Exception;
	
	public String [] getLoadAndApp(long oid) throws Exception;
	
	public DBRow[] getB2BOrders(List<Long> ids) throws Exception;
}
