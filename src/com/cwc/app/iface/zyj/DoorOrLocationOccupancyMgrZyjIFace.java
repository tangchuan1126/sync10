package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface DoorOrLocationOccupancyMgrZyjIFace {

	public DBRow addDoorOrLocationOccupancys(HttpServletRequest request) throws Exception;
	public void updateDoorOrLocationOccupancyStatus(HttpServletRequest request) throws Exception;
	public DBRow[] getDoorOrLocationOccupancys(HttpServletRequest request) throws Exception;
	public DBRow[] getDoorOrLocationOccupancysByTime(HttpServletRequest request) throws Exception;
	public DBRow[] getDoorOrLocationOccupancys(int occupancy_type, long rl_id, int rel_type, long rel_id, int occupancy_status,
			String book_start_time, String book_end_time, String actual_start_time, String actual_end_time, long ps_id, int is_after_now, int rel_occupancy_use) throws Exception;
	public DBRow[] getDoorOrLocationOccupancysByTime(int occupancy_type, long rl_id, int occupancy_status, String book_start_time, String book_end_time) throws Exception;
	public DBRow[] getBookDoorInfoCanChoose(String start_time, String end_time, long ps_id, PageCtrl pageCtrl) throws Exception;
	public DBRow[] getBookLocationInfoCanChoose(String start_time, String end_time, long ps_id, PageCtrl pageCtrl) throws Exception;
	public DBRow[] getBookDoorInfoProbableChoose(String start_time, String end_time, long ps_id, PageCtrl pc) throws Exception;
	public DBRow[] getBookLocationInfoProbableChoose(String start_time, String end_time, long ps_id, PageCtrl pc) throws Exception;
	public DBRow[] getStorageDoorsByNamePsRel(String doorName, long ps_id, int occupancyType, int rel_type, long rel_id, PageCtrl pc) throws Exception;
	public DBRow[] getStorageLocationsByNamePsRel(String locName, long ps_id, int occupancyType, int rel_type, long rel_id, PageCtrl pc) throws Exception;
	public void updateDoorOrLocationStatusAndTime(long transport_id, int status) throws Exception;
	public DBRow addDoorOrLocation(HttpServletRequest request) throws Exception;
	public DBRow addDoorOrLocationOccupancysFromXml(String xml, AdminLoginBean adminLoggerBean) throws Exception;
}
