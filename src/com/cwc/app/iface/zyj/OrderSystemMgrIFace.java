package com.cwc.app.iface.zyj;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;


public interface OrderSystemMgrIFace {

	/**
	 * 处理单据表
	 * @param entry_detail entry明细ID
	 * @param ps_id 仓库ID
	 * @param order_status 查询的单据的状态
	 * @param system 单据所属系统
	 * @param adminLoginBean 账号信息
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月17日 下午4:31:16
	 */
	public DBRow addOrUpdateOrderSystems(long entry_detail, long ps_id, String order_status, int system,AdminLoginBean adminLoginBean) throws Exception;
	/**
	 * 通过taskId判断是否删除业务单据表
	 * @param entry_detail
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月18日 下午4:25:03
	 */
	public DBRow deleteOrNotOrderSystemByDetailInfos(long entry_detail) throws Exception;
	/**
	 * 通过docName得到单据是提货还是送货
	 * @param docType
	 * @return
	 * @author:zyj
	 * @date: 2014年12月30日 下午7:31:04
	 */
	public int returnOrderTypeByDocType(int docType);
	public DBRow findOrderSystemById(long id ) throws Exception;
	public void updateOrderSystemById(long id, DBRow row) throws Exception;
}
