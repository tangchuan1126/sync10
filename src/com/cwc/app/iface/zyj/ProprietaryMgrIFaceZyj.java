package com.cwc.app.iface.zyj;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zyj.model.ProductCustomer;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProprietaryMgrIFaceZyj{
	
	public long addProprietary(long user_adid, long title_id, int title_priority) throws Exception;
	public long addProprietary(HttpServletRequest request) throws Exception;
	public long addProprietaryOrProprietaryAdmin(String title_name, long user_adid, long title_id, int title_priority, AdminLoginBean adminLoggerBean) throws Exception;
	public long addProprietaryOrProprietaryAdmin(String title_name,long user_adid) throws Exception;
	public long addProprietaryAdmins(HttpServletRequest request) throws Exception;
	public long updateProprietary(HttpServletRequest request) throws Exception;
	public DBRow findProprietaryByTitleId(long title_id) throws Exception;
	public DBRow[] findProprietaryAllOrByAdidTitleId(HttpServletRequest request) throws Exception;
	public DBRow[] findProprietaryAllOrByAdidTitleId(boolean adidNullToLogin,long adid, String title_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProprietaryAllByAdid(long adid, PageCtrl pc) throws Exception;
	public int deleteProprietary(HttpServletRequest request) throws Exception;
	public int deleteProprietary(long title_id) throws Exception;
	public void deleteProprietaryAdmin(HttpServletRequest request) throws Exception;
	public void deleteProprietaryAdmin(long title_admin_id, long adid, long title_admin_title_id) throws Exception;
	public DBRow[] findProprietaryByAdidAndTitleId(long adid, String title_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProprietaryByAdidProduct(long adid, long title_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findTitleByAdidAndProduct(long adid, long pc_id, HttpServletRequest request) throws Exception;

	
	
	
	public DBRow[] findProprietaryAllByAdidAndProductLineId(long adid, long product_line_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProprietaryByAdidAndProductLineId(long adid, long product_line_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProprietaryAllByAdidAndProductCatagoryId(long adid, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProprietaryByAdidAndProductCatagoryId(long adid, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] getCustomerAndTitleByCategory(long adid, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProprietaryByPcSelf(long adid, long pc_line_id, long product_catagory_id, long pc_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public long addProprietaryProductSeleteds(HttpServletRequest request) throws Exception;
	public void deleteProprietaryProduct(HttpServletRequest request) throws Exception;
	public long addProprietaryProduct(HttpServletRequest request) throws Exception;
	public DBRow addProudctTitle(HttpServletRequest request) throws Exception;
	public DBRow[] findProductTitleParentNode(long pc_id,String titleId) throws Exception ;
	public DBRow[] findAdminsByTitleId(boolean adidNullToLogin, long adid, long title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProductLinesByTitleId(boolean adidNullToLogin, long adid, String pc_line_ids, String title_ids, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProductCatagorysByTitleId(boolean adidNullToLogin, long adid, String pc_line_id, String category_parent_id, String pc_cata_id, String title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProductCatagoryParentsByTitleId(boolean adidNullToLogin, long adid, String pc_line_id, String category_parent_id, String pc_cata_id, String title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProductsByTitleId(boolean adidNullToLogin, long adid, long pc_id, String pc_name, long title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProprietaryAllRelatesByAdminProductNameLineCatagory(boolean adidNullToLogin, long adid, String product_line_ids, String category_parent_ids,String product_catagory_ids, String pc_name, int begin, int length, PageCtrl pc, HttpServletRequest request)throws Exception;
	
	public DBRow[] filterProductByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status,String title_id, long adid, int equal_or_not, long product_id, int active, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] filterProductByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status, String title_id, long adid, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request, long active) throws Exception;
	public DBRow[] filterChoiceProductByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status,
			String title_id, long adid, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request) throws Exception;  //商品过滤，不包含套装
	
	public DBRow[] findDetailProductLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int union_flag, int equal_or_not, long product_id,int active,PageCtrl pc, HttpServletRequest request)throws Exception;
	public DBRow[] findDetailProductLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int union_flag, int equal_or_not, long product_id,PageCtrl pc, HttpServletRequest request, long active)throws Exception;
	public DBRow[] findChoiceDetailProductLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int union_flag, int equal_or_not, long product_id,PageCtrl pc, int active, HttpServletRequest request)throws Exception;
	
	public DBRow[] findProductInfosProductLineProductCodeByLineId(boolean adidNullToLogin, String pc_line_id, String title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findAllProductsByTitleAdid(boolean adidNullToLogin, String title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findAllProductsByTitleAdid(boolean adidNullToLogin, String title_id, long adid, PageCtrl pc, HttpServletRequest request, long active) throws Exception;
	public DBRow[] findCohiceProductsByTitleAdid(boolean adidNullToLogin, String title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception;//过滤套装商品
	
	public HashMap<String, DBRow[]> uploadAdminTitleExcelShow(String filename,String type)throws Exception; 
	public DBRow addProprietaryAdminsByAdmins(HttpServletRequest request) throws Exception;
	public DBRow[] findAdminsTitleByAdminNameAdgidStorageStatus(String cmd, long proPsId, String proJsId, long proAdgid, long filter_lid, int lock_state, String employe_name, PageCtrl pc) throws Exception;
	public String exportAdminTitlesByAdminNameAdgidStorageStatus(HttpServletRequest request) throws Exception;
	
	public HashMap<String, DBRow[]> uploadProductTitleExcelShow(String filename,String type, HttpServletRequest request)throws Exception; 
	public DBRow addProprietaryProductsByProducts(HttpServletRequest request) throws Exception;
	
	public DBRow[] filterExportProductTitlesByPcLineCategoryTitleId(boolean adidNullToLogin, long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status,long title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception;
	
	public DBRow[] findExportDetailProductLikeSearch(boolean adidNullToLogin, String key,long title_id, long adid,PageCtrl pc, HttpServletRequest request)throws Exception;
	
	public DBRow[] findExportProductInfosProductLineProductCodeByLineId(boolean adidNullToLogin, long pc_line_id, long title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findExportAllProductsByTitleAdid(boolean adidNullToLogin, long title_id, long adid, PageCtrl pc, HttpServletRequest request) throws Exception;
	
	public DBRow[] findProprietaryByAdminProduct(boolean adidNullToLogin,long adid, long pc_id, int begin, int length, PageCtrl pc, int dataOrCount, HttpServletRequest request)throws Exception;
	public DBRow[] getProductHasTitleList(long pc_id)throws Exception;//商品已有title
	public DBRow[] getProudctUnhasTitleList(long pc_id)throws Exception;//商品已有title
	
	public DBRow[] filterProductAndRelateInfoByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status,String title_id, long adid, int begin, int length, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] filterProductAndRelateInfoByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status,String title_id, long adid, int begin, int length, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request, long active) throws Exception;
	
	public DBRow[] filterProductAndRelateInfoByPcLineCategoryTitleId(boolean adidNullToLogin, String catalog_id, String product_line_id, int union_flag, int product_file_type, int product_upload_status,String title_id, long adid, int begin, int length, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request,long active,int customerId) throws Exception;

	
	public DBRow[] findDetailProductRelateInfoLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int begin, int length, int union_flag, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request)throws Exception;
	public DBRow[] findDetailProductRelateInfoLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int begin, int length, int union_flag, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request, long active)throws Exception;
	
	/**
	 * 根据指定条件，搜索与特定品牌商相关的产品
	 * @param adidNullToLogin
	 * @param key
	 * @param title_id
	 * @param adid
	 * @param begin
	 * @param length
	 * @param union_flag
	 * @param equal_or_not
	 * @param product_id
	 * @param pc
	 * @param request
	 * @param customerId  品牌商ID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findDetailProductRelateInfoLikeSearch(boolean adidNullToLogin, String key,String title_id, long adid, int begin, int length, int union_flag, int equal_or_not, long product_id, PageCtrl pc, HttpServletRequest request,long active,int customerId)throws Exception;
	
	public DBRow[] findAllProductsRelateInfoByTitleAdid(boolean adidNullToLogin, String title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception;
	
	public DBRow[] findAllProductsRelateInfoByTitleAdid(boolean adidNullToLogin, String title_id, long adid, int begin, int length, PageCtrl pc, HttpServletRequest request, long active) throws Exception;
	
	/**
	 * 查询与指定生产商相关的产品
	 * @param adidNullToLogin
	 * @param adid
	 * @param begin
	 * @param length
	 * @param pc
	 * @param request
	 * @param titleId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllProductsRelateInfoByTitle(boolean adidNullToLogin,  long adid, int begin, int length, PageCtrl pc, HttpServletRequest request,int titleId) throws Exception;
	
	public DBRow[] findAllProductsRelateInfoByTitle(boolean adidNullToLogin,  long adid, int begin, int length, PageCtrl pc, HttpServletRequest request,int titleId, long active) throws Exception;
	
	/**
	 * 	/**
	 * 查询与指定品牌商相关的产品
	 * @param adidNullToLogin
	 * @param adid
	 * @param begin
	 * @param length
	 * @param pc
	 * @param request
	 * @param titleId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllProductsRelateInfoByCustomer(boolean adidNullToLogin,String title_ids,  long adid, int begin, int length, PageCtrl pc, HttpServletRequest request,long active,int customerId) throws Exception;
	
	public DBRow findAdminByGroupClientAdid(long adminId) throws Exception ;
	public long addProprietaryProductByMaxPriority(long pc_id, AdminLoginBean adminLoggerBean) throws Exception;
	
	public DBRow[] findProductLinesIdAndNameByTitleId(boolean adidNullToLogin, long adid, String pc_line_ids, String title_ids, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception;
	public DBRow[] findProductCatagoryParentsIdAndNameByTitleId(boolean adidNullToLogin, long adid, String pc_line_id, String category_parent_id, String pc_cata_id, String title_id, int begin, int length, PageCtrl pc, HttpServletRequest request) throws Exception;
	
	public DBRow[] searchTitleByNumber(String search_key,int search_mode,PageCtrl pc, int begin, int length, HttpServletRequest request) throws Exception;
	
	public long addProprietaryTitleProductByDBRow(DBRow insertRow) throws Exception ;
	public DBRow getDetailTitleByTitleId(long title_id) throws Exception;
	
	public DBRow[] findPropretarysByAdid(long adid) throws Exception;
	
	public DBRow findProprietaryByTitleName(String titleName) throws Exception ;		//zhangrui添加
	public DBRow[] findParentProductCatagorysByTitleId(boolean adidNullToLogin, long adid, String pc_line_id, String title_id, PageCtrl pc, HttpServletRequest request) throws Exception;
	public long addProprietaryProduct(long pc_id, long title_id) throws Exception;
	
	public DBRow findProprietaryByCategoryId(long categoryId) throws Exception;
	
	public DBRow[] findProductLinesUnExistLineId(long lineId, HttpServletRequest request) throws Exception;
	
	public DBRow getProductDetailByPcId(long pc_id) throws Exception;  //Yuanxinyu
	public DBRow getProductCatalogById(long id) throws Exception;   //Yuanxinyu
	public DBRow[] getProductTitleByPcId(long pc_id) throws Exception; //Yuanxinyu
	public DBRow[] getProductSnByPcId(long pc_id) throws Exception; //Yuanxinyu
	/**
	 * 通过商品与title的关系重新计算产品分类、产品线与title的关系
	 * @param pc_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月22日 下午6:52:00
	 */
	 public void calculateCatelogAndLineTitle(long pc_id) throws Exception;
	 /**
	     * 查询商品归属的title
	     * @param pc_id
	     * @param adid
	     * @return
	     * @throws Exception
	     * @author:zyj
	     * @date: 2015年4月7日 下午4:32:18
	     */
	    public DBRow[] getProudctHasTitleListByPcAdmin(boolean adidNullToLogin, long pc_id, long adid, HttpServletRequest request) throws Exception;
	    /**
	     * 商品不归属的title
	     * @param pc_id
	     * @param adid
	     * @return
	     * @throws Exception
	     * @author:zyj
	     * @date: 2015年4月7日 下午4:32:42
	     */
	    public DBRow[] getProudctUnHasTitleListByPcAdmin(boolean adidNullToLogin, long pc_id, long adid, HttpServletRequest request) throws Exception;
public DBRow[] getTitleByEso(String search_key,int count) throws Exception;

public void customerTitleAndLine(long lineId) throws Exception;
	
	public void customerTitleAndCategory(long categoryId) throws Exception;
	
	public DBRow[] compareCusomerTitle(DBRow[] row1, DBRow[] row2) throws Exception;
	
	public void updateCustomerTitleAndCategoryLineRelation(long pcId) throws Exception;
	
	public DBRow[] findProductByCategory(long id) throws Exception;
	public DBRow findDetailProductByPcId(long pc_id) throws Exception;
	/**
	 * 分页查询与特定产品相关的ProductCustomer
	 * @param productId
	 * @return
	 */
	public List<ProductCustomer>  getByProduct(int productId);

}
