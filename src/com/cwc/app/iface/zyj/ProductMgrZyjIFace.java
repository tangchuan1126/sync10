package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductMgrZyjIFace {
	
	public void handleProductPictureUpload(HttpServletRequest request)throws Exception;
	public DBRow[] getAllProductFileByPcId(String pc_id, long file_with_type, String file_with_id, int is_unable_provide) throws Exception;
	public DBRow getProductCountTotalByFileType() throws Exception;
	public DBRow[] getProductCountByProductLineGroupByAndFile() throws Exception;
	public DBRow[] getProductInfosProductLineProductCodeByLineId(long pc_line_id, PageCtrl pc) throws Exception;
	/**
	 * 通过商品ID，查询商品相关信息
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findProductInfosByPcid(long pc_id) throws Exception;
	public DBRow deleteAllBasicDatas() throws Exception;
	/**
	 * 通过分类ID查商品
	 * @param catalog_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public int getProductsByCategoryid(long catalog_id,PageCtrl pc)throws Exception;
	
	/**
	 * 功能：根据catalog_id和title_id统计商品数
	 * @param catalog_id
	 * @param title_id
	 * @return
	 * @throws Exception
	 * @author lixf  2015年5月22日
	 */
	public int getProductsByCategoryidAndTitleid(long catalog_id, long title_id) throws Exception;
}
