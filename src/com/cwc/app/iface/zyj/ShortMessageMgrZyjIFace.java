package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ShortMessageMgrZyjIFace {
	
	public DBRow[] getShortMessageList(PageCtrl pc, int moduleIdSearch, String receiverNameSearch, String receiverPhoneSearch, String messageContentSearch, String joinUserId) throws Exception;
	public DBRow[] getShortMessageList(PageCtrl pc, String joinUserId)throws Exception;
	public DBRow[] getShortMessageReceivers(long messageId) throws Exception;
	public boolean addShortMessage(HttpServletRequest request, DBRow[] receivers) throws Exception;
	public boolean addShortMessage(long module_id, long business_id, String content, DBRow[] receivers, long adid)throws Exception;
	public boolean addShortMessage(long module_id, long business_id, String content, DBRow[] receivers, AdminLoginBean adminLoggerBean) throws Exception;
	public boolean addShortMessage(long module_id, long business_id, String content, long adgId, HttpServletRequest request) throws Exception;
	public boolean addShortMessage(int module_id, int business_id, String content, int adgId, int roleId, HttpServletRequest request) throws Exception;
	public boolean addShortMessage(long module_id, long business_id, String content, long adgId, long[] roleId, HttpServletRequest request) throws Exception;
	public DBRow generateReceiver(String receiverName, String receiverPhone) throws Exception;
	public DBRow getUserInfoByUserId(long userId) throws Exception;
	public DBRow[] getUserListByAdgId(long adgId) throws Exception;
	public DBRow[] getUserListByAdgIdRoleId(long adgId, long[] roleId) throws Exception;
	public DBRow getUserRowByUserId(long userId) throws Exception;
	public String handleShortMessageContent(String content) throws Exception;

}
