package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.exception.purchase.FileException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface RepairOrderMgrZyjIFace {

	//transportMgrIFaceZJ
	public DBRow[] fillterRepairOrder(long send_psid,long receive_psid,PageCtrl pc,int status,int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id)//过滤转运单
	throws Exception;
	public DBRow[] searchTransportByNumber(String key,int search_mode,PageCtrl pc)//根据转运单号查询转运单
	throws Exception;
	public DBRow[] getNeedTrackReadyDelivery(long product_line_id,PageCtrl pc)
	throws Exception;
	public DBRow[] getNeedTrackSendRepairOrderByPsid(long ps_id,String cmd,PageCtrl pc)
	throws Exception;
	public DBRow[] getNeedTrackReceiveRepairOrderByPsid(long ps_id,String cmd,PageCtrl pc)
	throws Exception;
	public float getRepairOrderVolume(long transport_id)//获得转运单的体积
	throws Exception;
	public float getRepairWeight(long transport_id)//获得转运单重量
	throws Exception;
	public double getRepairSendPrice(long transport_id)//获得转运单出库金额
	throws Exception;
	public void rebackRepair(HttpServletRequest request)//转运单停止装箱，回退库存
	throws Exception;
	public void reStorageRepair(HttpServletRequest request)//转运单中止运输
	throws Exception;
	public long addRepairOrder(HttpServletRequest request) //添加返修单
	throws Exception ;
	public void delRepairOrder(HttpServletRequest request) 
	throws Exception ;
	public DBRow[] checkStorage(long transport_id)
	throws Exception;
	public DBRow trackDeliveryAndRepairOrderCountFirstMenu()
	throws Exception;
	public DBRow[] trackDeliveryCountGroupByProductLine()
	throws Exception;
	public DBRow[] trackSendRepairCountGroupByPs()
	throws Exception;
	public DBRow[] trackReciveRepairCountGroupByPs()
	throws Exception;
	public DBRow[] trackOceanShippingCount()
	throws Exception;
	public DBRow trackSendRepairPsid(long ps_id)
	throws Exception;
	public DBRow trackReciveRepairPsid(long ps_id)
	throws Exception;
	public void packingRepair(HttpServletRequest request)
	throws Exception;
	public DBRow getDetailRepairOrderById(long transport_id)//根据转运单ID获得转运单详细
	throws Exception;
	public DBRow[] getRepairOrderDetailByRepairId(long transport_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)//根据转运单ID获得转运单明细
	throws Exception;
	public long updateRepairBasic(HttpServletRequest request) 
	throws Exception;
	public String downloadRepairOrder(HttpServletRequest request)
	throws Exception;
	public void uploadRepairInvoice(HttpServletRequest request)
	throws Exception,FileTypeException,FileException;
	public DBRow[] excelshow(String filename)
	throws Exception;
	public DBRow[] getPurchaseRepairDetailPrefill(long purchase_id)
	throws Exception;
	public void saveRepairDetail(HttpServletRequest request)
	throws Exception;
	public DBRow getRepairDetailById(HttpServletRequest request)
	throws Exception;
	public String deliveryRepairForJbpm(long transport_id,long receive_id) throws Exception;
	public long approveRepairOutboundForJbpm(long tsa_id) 
	throws Exception;
	public float getRepairDetailsSendWeight(long transport_id)
	throws Exception;
	public void updateRepairDetailVW(long transport_id)
	throws Exception;
	public void modRepairDetail(HttpServletRequest request) 
	throws Exception;
	public void addRepairDetail(HttpServletRequest request) 
	throws Exception;
	public void delRepairDetail(HttpServletRequest request) 
	throws Exception;
	
	//DamagedRepairMgrZJ
	public void packingDamagedRepair(HttpServletRequest request)
	throws Exception;
	
	
	//DeliveryMgrIFaceZJ
	public DBRow[] getAllDeliveryOrder(PageCtrl pc)//获得全部交货单
	throws Exception;
	public long addDeliveryOrder(HttpServletRequest request)//添加交货单
	throws Exception;
	
	//TransportMgrIFaceLL
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception;
	public DBRow getCountyById(String id) throws Exception;
	public DBRow[] getRepairLogsByRepairIdAndType(long transport, int[] types) throws Exception;
	public void updateRepairFreight(HttpServletRequest request) throws Exception;
	public DBRow[] getRepairFreightCostByRepairId(String transport_id) throws Exception;
	public DBRow[] getRepairLogsByType(long transport_id, int transport_type) throws Exception;
	public DBRow repairSetFreight(HttpServletRequest request) throws Exception;
	public void repairStockTempOut(HttpServletRequest request) 
	throws Exception;
	public void repairStockTempIn(HttpServletRequest request) throws Exception;
	
	//ApplyMoneyMgrIFaceLL
//	public DBRow[] getAllByTable(String tableName) throws Exception;
//	public DBRow[] getAllByReadView(String readViewName) throws Exception;
	public DBRow[] getAllAssetsCategory() throws Exception;
	public DBRow[] getAllAdminGroup() throws Exception;
	public DBRow[] getAllProductLineDefine() throws Exception;
	public DBRow[] getAccountView() throws Exception;
	
	//transportMgrZr
	public DBRow[] getRepairOrderLogs(long transport_id, int number)
	throws Exception;
	public void repairProductFileFollowUp(HttpServletRequest request)
	throws Exception;
	public void handleRepairDeclaration(HttpServletRequest request)
	throws Exception;
	public void handleRepairClearance(HttpServletRequest request)
	throws Exception;
	public void hanleRepairStockInSet(HttpServletRequest request)
	throws Exception;
	public void hanleRepairQualityInspection(HttpServletRequest request)
	throws Exception;
	public void handleRepairTag(HttpServletRequest request)
	throws Exception ;
	public void addRepairLog(HttpServletRequest request) throws Exception ;
	public void repairCertificateFollowUp(HttpServletRequest request)
	throws Exception;
	public DBRow[] getProductFileByFileTypeAndWithId(long file_with_id,
			int file_with_type, int product_file_type, PageCtrl pc)
			throws Exception;
	public void updateRepairCertificateAndLogs(HttpServletRequest request)
	throws Exception;
	public void addRepairCertificateAddFile(HttpServletRequest request) throws Exception;
	public void handleRepairProductPictureUp(HttpServletRequest request)
	throws Exception;
	
	//transportMgrZyj
	public String getRepairLastTimeCreateByAdid(HttpServletRequest request) throws Exception;
	public void updateRepairProdures(HttpServletRequest request) throws Exception;
	public String saveProductTagFile(HttpServletRequest request) throws Exception;
	public void updateRepairGoodsArriveDelivery(HttpServletRequest request) throws Exception;
	
	//ComputeProductMgrIFaceCCC
	public DBRow getSumRepairFreightCost(String transport_id) throws Exception;
	
	public void updateHandleRepairProduct(HttpServletRequest request)throws Exception;
	public DBRow[] getRepairOrderDetailByRepairId(long transport_id) throws Exception;
	public boolean isRepairOrderExist(long repairOrderId) throws Exception;
	public DBRow[] fillterRepairApprove(long send_psid, long receive_psid,PageCtrl pc, int approve_status, String sorttype) throws Exception;
	public long approveRepairOrderForJbpm(long ta_id) throws Exception;
	public DBRow[] getRepairOrderApproverDetailsByTaid(long ta_id, PageCtrl pc) throws Exception ;
	public void approveRepairOrderDifferent(long ta_id, long[] tad_ids,String[] notes, AdminLoginBean adminLoggerBean) throws Exception;
	public void rebackRepairDamage(HttpServletRequest request) throws Exception;
	public DBRow getRepairDetailsByRepairProductName(HttpServletRequest request)throws Exception;
	//transportMgrLL
	public void repairAproveCancel(String transport_approve_id,AdminLoginBean adminLoggerBean) throws Exception;
	
	//transportMgrZwb
	public DBRow[] getRepairDetailByRepairProductName(long repair_order_id, String name)throws Exception;
	
}
