package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface ApplyFundsMgrZyjIFace {

	public int handleApplyFundsAssociateTransport()throws Exception;
	public DBRow getTotalApplyTransferByApplyId(long applyId) throws Exception;
	public double getTransportFreightTotalApply(long transportId, int typeId) throws Exception;
	public DBRow[] getApplyMoneyByAssociationIdAndAssociationTypeIdNoCancel(long association_id,int association_type_id) throws Exception;
	public long applyRepairFreightMoney(HttpServletRequest request) throws Exception;
	public void handleApplyMoneyFile(String sn, String path, String fileNames, long apply_money_id) throws Exception;
	public long applyTransportFreightMoney(HttpServletRequest request) throws Exception;
}
