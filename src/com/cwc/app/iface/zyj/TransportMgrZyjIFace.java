package com.cwc.app.iface.zyj;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;


public interface TransportMgrZyjIFace {
	
	public void updateTransportAllProdures(HttpServletRequest request) throws Exception;
	public void addTransportScheduleOneProdure(String beginTime, String endTime, long adid, 
			String executePersonIds, String joinPersonIds,
			long transport_id, int moduleId, String title, String content,
			int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, String employeeName, String logContent) throws Exception;
	
	public String handleSendContent(long transport_id, String adminUserNamesTransport, int declaration, String declarationPerson, 
			int clearance, String clearancePerson,int productFile, String productPerson,
			int tag, String tagPerson,int stockInSet, String stockPerson,
			int certificate, String certificatePerson,int quality, String qualityPerson) throws Exception;
	
	public void deleteTransportScheduleById(long transport_id, int moduleId, int processType, int activity, String employeeName, String logContent,long adid, HttpServletRequest request) throws Exception;
	
	public void updateTransportScheduleOneProdure(String beginTime, String endTime, long adid, 
			String executePersonIds, String joinPersonIds,
			long transport_id, int moduleId, String title, String content,
			int page, int mail, int message, HttpServletRequest request, boolean isNow,
			int processType, int activity, String employeeName, String logContent) throws Exception;
	
	
	public String getSchedulePersonIdsByTransportIdAndType(String transport_id, int moduleId, int processType) throws Exception;
	public String getSchedulePersonNamesByTransportIdAndType(String transport_id, int moduleId, int processType) throws Exception;
	
	public String saveProductTagFile(HttpServletRequest request) throws Exception;
	public DBRow[] getTransportRowsByPurchaseId(long purhcaseId) throws Exception;
	/*
	 * 更新转运单的各个流程信息
	 */
	public void updateTransportProdures(HttpServletRequest request) throws Exception;
	
	/**
	 * 删除转运单的日志
	 * @param transportId
	 * @throws Exception
	 */
	public void deleteTransportLogsByTransportId(long transportId) throws Exception;
	/**
	 * 删除转运单的相关文件
	 * @param transportId
	 * @param types
	 * @throws Exception
	 */
	public void deleteTransportFileByTransportIdAndTypes(long transportId, int[] types) throws Exception;
	public void deleteTransportProductFileByTransportIdAndTypes(long transportId, int[] types) throws Exception;
	public void deleteTransportFreightByTransportId(long transportId) throws Exception;
	public String getTransportLastTimeCreateByAdid(HttpServletRequest request) throws Exception;
	public String getTransportPurchaseLastTimeCreateByAdid(HttpServletRequest request) throws Exception;
	public void updateTransportGoodsArriveDelivery(HttpServletRequest request) throws Exception;
	public DBRow getTransportDetailsByTransportProductName(HttpServletRequest request)throws Exception;
	public void handleTransportThirdTag(HttpServletRequest request) throws Exception;
	public String checkTransportProductFileFinish(long transport_id) throws Exception;
	public String checkTransportProductTagFileFinish(HttpServletRequest request) throws Exception;
	public String checkTransportApplyFundsPrompt(HttpServletRequest request) throws Exception;
	public void updateTransportPcModelFinishToNeed(HttpServletRequest request) throws Exception;
	public double checkTransportCanApplyMoneyToPurchase(long transport_id, long purchase_id) throws Exception;
	public long addPurchaseTransportByXml(String xml, HttpServletRequest request) throws Exception;
	public long addPurchaseTransportByXmlStr(String xml, HttpServletRequest request) throws Exception;
	public void addTransportProdures(HttpServletRequest request, long associate_id, long adid, String employeeName, String content) throws Exception;
	public void addTransportXML(HttpServletRequest request) throws Exception;
	public void updateTransportXML(HttpServletRequest request) throws Exception;
	public List<DBRow> saveOrUpdateTransportItems(String xml, AdminLoginBean adminLoggerBean, HttpServletRequest request) throws Exception;
	public DBRow[] getTransportByStatusStroageCount(String transport_status, long send_storage_id, long receive_storage_id, long purchase_id, int count) throws Exception;
	public void addTransportScheduleOneProdure(String beginTime,
			String endTime, long adid, String executePersonIds,
			String joinPersonIds, long transport_id, int moduleId,
			String title, String content, int page, int mail,
			int message, AdminLoginBean adminLoggerBean, boolean isNow,
			int processType, int activity, String employeeName, String logContent) throws Exception ;
	
	public void updateTransportScheduleOneProdure(String beginTime, String endTime, long adid, 
			String executePersonIds, String joinPersonIds,
			long transport_id, int moduleId, String title, String content,
			int page, int mail, int message, AdminLoginBean adminLoggerBean, boolean isNow,
			int processType, int activity, String employeeName, String logContent
				) throws Exception;
	public int updateTransportToTransitAndReleaseOccXml(String xml, AdminLoginBean adminLoggerBean) throws Exception;
	public void updateTransportDetailByLotNumber(HttpServletRequest request) throws Exception;
}
