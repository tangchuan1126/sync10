package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ReturnProductOrderMgrZyjIFace{

	public void putProductToReturnCartSession(HttpServletRequest request) throws Exception;
	public void removeReturnProductSession(HttpServletRequest request) throws Exception;
	public void modReturnProductQuantitySession(HttpServletRequest request) throws Exception;
	public void putToReturnProductCartForAdd(HttpServletRequest request) throws Exception;
	public DBRow[] getReturnOrderBySid(long sid) throws Exception;
	public long addReturnOrder(HttpServletRequest request) throws Exception;
	public void loadReturnProductToSesssion(HttpServletRequest request) throws Exception;
	public void modReturnProduct(HttpServletRequest request) throws Exception;
	public void addReturnOrderByVirtualAndNot(HttpServletRequest request) throws Exception;
	public DBRow[] getReturnSubItemsByRpid(long rp_id) throws Exception;
	public DBRow[] searchReturnOrderByNumber(String search_key,int search_mode,PageCtrl pc) throws Exception;
	public DBRow serviceRelevanceReturnProductOrder(HttpServletRequest request) throws Exception;
	public DBRow statReturnProductOrderNeedHandleCount() throws Exception;
	public DBRow[] statNeedGatherPicReturnOrders(PageCtrl pc) throws Exception;
	public DBRow[] statNeedRecognitionReturnOrders(PageCtrl pc) throws Exception;
	public DBRow[] statNeedRegisterReturnOrders(PageCtrl pc) throws Exception;
	public DBRow[] statRegisterFinishReturnOrders(PageCtrl pc) throws Exception;
	public DBRow[] statReturnOrderNotRelationOtherOrders(PageCtrl pc) throws Exception;
	public void modReturnItemCount(HttpServletRequest request) throws Exception;
	public DBRow[] getReturnProductItemByReceiveHandleInfo(int is_receive_item, long rp_id) throws Exception;
	public void handleReturnProductAllItems(HttpServletRequest request) throws Exception;
	public void modProductOrderPsId(HttpServletRequest request) throws Exception;
	public void modReturnSubItemCount(HttpServletRequest request) throws Exception;
	public DBRow getReturnSubItemsByRpsiId(long rpsi_id) throws Exception;
	public long addReturnProductLog(long rp_id, long rpi_id, long rpsi_id, long fix_rpi_id,
			int rp_status, String content, int follow_type, long operator, String operate_time) throws Exception;
	public DBRow[] getReturnProductLogsByIdTypeStatus(long rp_id, int follow_type, int status) throws Exception;
	public int getReturnProductLogsCountByIdTypeStatus(long rp_id, int follow_type, int status) throws Exception;
}
