package com.cwc.app.iface.zyj;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface StuMgrZyjIFace {

	public long addStu(String sname, String sno, String home_addr) throws Exception;
	public DBRow[] getStuListByPage(String snameSearch, PageCtrl pc) throws Exception;
	public void deleteById(String id) throws Exception;
}
