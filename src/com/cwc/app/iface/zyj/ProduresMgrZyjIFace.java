package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface ProduresMgrZyjIFace {
	
	
	public DBRow[] getProduresByOrderType(int order_type) throws Exception;
	public DBRow[] getProduresDetailsByProduresId(long produres_id, int[] activities, int[] activitiesStatus) throws Exception;
	public DBRow[] getProduresNoticesByProduresId(long produres_id, int is_display) throws Exception;
	public DBRow getProdureByProdureId(long produres_id) throws Exception;
	public DBRow getProduresDetailsByProduresDetailId(long produres_details_id) throws Exception;
	public DBRow getProduresNoticeByNoticeId(long notice_id) throws Exception;
	public DBRow getProduresNoticeByProduresIdIsDisplayType(long produres_id, int is_display, int notice_type) throws Exception;
	public DBRow getProduresDetailsByProduresIdActivity(long produres_id, int activity) throws Exception;
	public DBRow addProdure(HttpServletRequest request) throws Exception;
	public DBRow updateProdure(HttpServletRequest request) throws Exception;
}
