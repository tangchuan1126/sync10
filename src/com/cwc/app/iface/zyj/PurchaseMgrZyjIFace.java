package com.cwc.app.iface.zyj;

import javax.servlet.http.HttpServletRequest;

import us.monoid.json.JSONArray;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface PurchaseMgrZyjIFace {

	public void handlePurchaseInvoiceCertificationState(HttpServletRequest request) throws Exception;
	public String handlePurhcaseInvoiceCertificationAjax(HttpServletRequest request) throws Exception;
	public DBRow[] getPurhcaseFileInfosByPurchaseIdTypes(long purhcaseId, int[] types) throws Exception;
	public String handlePurchaseProductTagTypesFile(HttpServletRequest request) throws Exception;
	public DBRow[] getAllProductTagFilesByPcId(long purhcaseId, int[] types) throws Exception;
	public DBRow getFollowUpLogsByPurchaseIdAndTypeActivity(long purchaseId, int type, int typeSub) throws Exception;
	public double getPurchaseDiffTime(long purchase_id,int follow_up_type,int followup_type_sub,String diffTime_type)
	throws Exception;
	public String handlePurchaseQualityInspectionAjax(HttpServletRequest request) throws Exception;
	public void handlePurchaseProductPictureUp(HttpServletRequest request)throws Exception;
	public String handlePurchaseProductModelAjax(HttpServletRequest request) throws Exception;
	public void uploadPurchaseQualityInspection(HttpServletRequest request, long purchase_id, int quality_inspection)//�ϴ��ɹ������ʼ�Ҫ��
	throws Exception;
	public DBRow getPurchaseNeedFollowUpCount(long product_line_id)throws Exception;
	public DBRow[] getProductLineNeedFollowUpPurchaseCount()throws Exception;
	public DBRow[] getPurchaseRowsByProductLineIdAndProcessKey(long productLineId, int processKey, int activity, PageCtrl pc) throws Exception;
	public void handPurchaseCreatorId()throws Exception;
	public String getPurchaseLastTimeCreateByAdid(HttpServletRequest request) throws Exception;
	public void handlePurchaseDataByPriceState() throws Exception;
	public String checkPurchaseProductFileFinish(long purchase_id) throws Exception;
	public String getPurchaseTransportApplyPromptMessage(HttpServletRequest request) throws Exception;
	public void savePurchaseProductFileAssociation(HttpServletRequest request) throws Exception;
	public String checkPurchaseProductTagFileFinish(HttpServletRequest request) throws Exception;
	public boolean checkPurchaseThirdTagFileFinish(long file_with_id, int[] file_types, String tag_types) throws Exception;
	public void updatePurchasePcModelFinishToNeed(HttpServletRequest request) throws Exception;
	public JSONArray findPurchaseFillter(PageCtrl pc)throws Exception;
}
