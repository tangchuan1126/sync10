package com.cwc.app.iface.zwb;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface QuestionCatalogMgrIfaceZwb {

	//根据树等级查询问题分类
	public DBRow[] getQuestionCatalog(long grade) throws Exception;
	
	
	//根据等级和产品线查询
	public DBRow[] getQuestionCatalogByPid(long productlineId) throws Exception;
	
	//查询所有问题分类数据
	public DBRow[] getAllQuestionCatalog() throws Exception; 
	
	//添加问题分类
	public long addQuestionCatalog(HttpServletRequest request) throws Exception;
	
	//添加子分类
	public long addQuestionCatalogSmall(HttpServletRequest request)throws Exception;
	
	public DBRow getQuestionCatalogById(long catalogId) throws Exception;
	
	public String getParenttitle(long catalogId)throws Exception;
	
	//添加问题在产品线上
	public long addQuestionByProductLine(HttpServletRequest request)throws Exception;
	
	//通过问题分类ID查询各分类下的所有问题
	public long[] getAllQuestionByQid(long catalogId) throws Exception; 
	
	
	//修改问题分类名
	public void updateQuestionCatalogSmall(HttpServletRequest request) throws Exception;
	
	//添加问题  在问题分类上
	public long addQuestionByCatalogId(HttpServletRequest request)throws Exception;
	 
	 //根据商品id查询下面的所有子级
	public long[] getAllProductCatalog(long productCatalogId)throws Exception;
	//修改问题时 查询 目录
	public String getTitleByUpdateQuestion(long question_id)throws Exception;
	
	//根据问题分类下的问题id 查询问题信息
	public DBRow getQuestionByCatlogId(long QuestionId) throws Exception;
	//问题回答
	public DBRow addQuestionAnswer(HttpServletRequest request) throws Exception ;
	//更改问题回答状态为已回答
	public void modQuestionAnswerState(long question_id,int state)throws Exception;
	//根据问题ID查询回答内容
	public DBRow getAnswerByQuestionId(long question_id) throws Exception;
	//删除答案
	public void deleteQuestionAnswer(long answer_id,long question_id)throws Exception;
	//修改答案
	public DBRow modAnswer(HttpServletRequest request)throws Exception;
	//根据答案ID查询答案详情
	public DBRow getAnswerById(long answer_id)throws Exception;
	//添加问题的时候查询是否存在问题
	public DBRow[] searchHint(HttpServletRequest request) throws Exception;
	//数据库下查询问题
	public DBRow[] getAllQuestionByFlool(long questionCatalogId,String op,PageCtrl pc)throws Exception;
	//添加问题时 提示 查询
	public DBRow[] getQuestionSingle(long question_id) throws Exception;
	//转移问题时用根据产品线id查询该产品线下所有分类详细
	public DBRow[] moveGetQuestionCatalogByLineId(long question_line_id)throws Exception;
	//转移问题  到其他分类 并重新创建索引
	public String moveQuestionCatalog(HttpServletRequest request)throws Exception;
	//查询所有为回答问题的总数
	public DBRow getQuestionCountByStatus()throws Exception;
	//查询某条产品线下为回答问题的总数
	public DBRow[] getQuestionCountByProductLine(HttpServletRequest request)throws Exception;
	//查询某条产品线下为回答问题详细
	public DBRow[] getQuestionproductLineStatus(long productLineId,PageCtrl pc)throws Exception;
	//查询附件里的内容
	public String selectAppendixContent(String fileName)throws Exception;
	//查询是否有问题需要提示
	public DBRow[] getQuestionCallByUserId(long userId)throws Exception;
	//查询某用户下的问题提醒详细
	public DBRow[] getAllQuestionCallById(long userId)throws Exception;
	//标记为已经阅读
	public long detQuestionCallById(long userId,long questionId)throws Exception;
	//删除问题时 取消所有用户对该问题的提醒
	public long detQuestionCallAllByQuestionId(long questionId)throws Exception;
	
}
