package com.cwc.app.iface.zwb;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.db.PageCtrl;

public interface BolCustomerIfaceZwb {
	
	public void getBolList(HttpServletRequest request,HttpServletResponse response,PageCtrl pc)throws Exception;  //获得bol list列表
	
	public void addBol(HttpServletRequest request,HttpServletResponse response)throws Exception;   //添加bol 模版
	
	public void editBol(HttpServletRequest request,HttpServletResponse response)throws Exception;   //编辑bol 模版
	
	public void detBol(HttpServletRequest request,HttpServletResponse response)throws Exception;   // 删除bol 模版
	
}
