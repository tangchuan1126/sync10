package com.cwc.app.iface.zwb;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.api.EntryTicketCheck;
import com.cwc.app.api.GateCheckoutInfo;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.service.android.action.mode.HoldDoubleValue;

public interface CheckInMgrIfaceZwb {
	
	
	//删除子单据
	public long detOccupancyDetails(long detail_id)throws Exception;
	
	//更新主表类型
	public DBRow updateRelTypeMain(HttpServletRequest request)throws Exception;
	
	//查询门是否存在或占用
	public DBRow selectDoorisExistByDoorName(long ps_id,String doorName)throws Exception;
	//查询门列表
	public DBRow[] selectAllDoor(long ps_id,String doorName,long dlo_id)throws Exception;
	
	//查询门位置关系
	public DBRow[] findDoorAndLocation(long doorId,long psId)throws Exception;
	
	//根据主单id 查询loding
	public DBRow[] findloadingByInfoId(long dlo_id)throws Exception;
	 
	//添加数据到detail表
	public long addOccupancyDetails(long adid,HttpServletRequest request)throws Exception;
	
	 //load生成标签    
    public DBRow[] createLoadBiaoQian(long mainId)throws Exception;
    //通过entryID查询Bol不能为空的子单据
    public DBRow[] findBolNoDetailsByEntryId(long EntryId)throws Exception;
    //通过entryID查询Container不能为空的子单据
    public DBRow[] findContainerNoDetailsByEntryId(long EntryId)throws Exception;
    //通过entryID查询Containera或者bol不能为空的子单据
    public DBRow[] findBolNoContainerNoNotDetailsByEntryId(long EntryId)throws Exception;
	
	 //查询详细表去重 门
	public DBRow[] selectDoorByInfoId(long infoId)throws Exception;
	
	//根据门查询load数据和主单据id
	public DBRow[] selectLotNumberByDoorId(long doorId,long infoId)throws Exception;
	
	//根据id查询主单据信息--打印用
	public DBRow findGateCheckInById(long id)throws Exception;
	public DBRow gateCheckInAddDBRow(HttpServletRequest request)throws Exception;
	//添加数据到主表
	public DBRow gateCheckInAdd(final HttpServletRequest request,final DBRow row,final AdminLoginBean adminLoginBean,final boolean isAndroid)throws Exception;
	//查询GPS号是否存在
	public DBRow findAssetByGPSNumber(String gps_tracker) throws Exception;
	
	//验证gps 是否存在
	public DBRow androidVerificationGps(String gps_tracker)throws Exception;
	
	//添加GPS设备
	public long addAsset(String gps_tracker ,String gate_liscense_plate,long groupId,String groupName,String assetName,String callnum) throws Exception;
	//查询loading表
	public DBRow[] getLoading(String area_name,String title_name,long type,String search_number,long main_id,int mark,long ps_id) throws Exception ;
	//查询loading表
	public DBRow getDoorByStagingOrTitle(long type,String search_number,long mainId,long ps_id, long adid, HttpServletRequest request) throws Exception ;
	//自动指定门
	public DBRow autoAssignDoor(String area_name,String title_name,long type,String search_number,long mainId,long ps_id) throws Exception;
	
	//查询loading表
	public DBRow[] findZoneByTitleName(String title_name,long ps_id) throws Exception ;
	//重载
	public DBRow androidGetLoading(DBRow data,HttpServletRequest request) throws Exception ;
	
	//安卓获得checkout数据
    public DBRow androidGetCheckOutData(long info_id , AdminLoginBean adminLoginBean)throws Exception;
	
	//查询主单据功能 带分页
	public DBRow[] selectAllMain(long id,PageCtrl pc)throws Exception;
	
	//查询GPS根据id
	public DBRow findAssetByGpsId(long gps_id) throws Exception;

	//根据id和type查询上传文件
	public DBRow[] getAllFileByFileWithIdAndFileWithType(long file_with_id , int file_with_type)throws Exception;
	//查询load与 order关系
	public DBRow[] findOrderByLoadId(String loadno)throws Exception;
	
	 //根据id查询主表
	public DBRow selectMainByEntryId(long id)throws Exception;
	
	//查询被占用的门 
	public DBRow[] selectUseDoor(long ps_id,long area_id)throws Exception;
	 
    // 查询被占用门的详细信息
    public DBRow[] selectUseDoorDetail(long doorId)throws Exception;
	//修改主表
	public long modCheckIn(HttpServletRequest request)throws Exception;
	//修改seal
	public long modPickUpSeal(HttpServletRequest request)throws Exception;
	//关联id查询detail表
	public DBRow[] findOccupancyDetails(long main_id)throws Exception;
	//根据doorId和entryId查询详细表	
	public DBRow[] selectDetails(long doorId,long main_id)throws Exception;
	//查询子表信息
	public DBRow[] selectDetailsInfo(long doorId,long main_id) throws Exception;
	//条件过滤
 	public DBRow[] filterCheckIn(String entryType,String start_time,String end_time,String mainStatus,String tractorStatus,String numberStatus, String waitingType, String comeStatus,String purposeStatus,long ps_id, long loadBar ,long priority ,PageCtrl pc,String customer_id) throws Exception ;

	// 查询停车位信息
    public DBRow[] getParkingOccupancy(String yc,long ps_id,long flag,long spotArea,long mainId)throws Exception;
    
    //查询通知 反填
    public DBRow[] findNotices(long id,long type)throws Exception;
    
    //查询window的通知
    public DBRow[] findWindowNotices(long id,long window,long warehouse)throws Exception;
    
    //查询所有通知
    public DBRow[] findAllNotices(long id,long type)throws Exception;
    
    //android用 查询通知 反填
    public DBRow[] androidfindNotice(long id,String type)throws Exception;
    
    //根据mainId  查询该单据下所有load 
    public DBRow[] findAllLoadByMainId(long mainId)throws Exception;
    
    //根据主单据和门id查询 子单据号
    public DBRow[] findOrderByEntryIdAndDoorId(long entry_id,long door_id)throws Exception;
    
    //根据主单据号 查询 子单据 被占用门
    public DBRow[] findUseDoorByMainId(long maind_id)throws Exception;
    
    //根据mainId 查询该单据下 去重门
    public DBRow[] findDoorNoRepeat(long mainId)throws Exception;
    
    //window 签 查询门 不要load
    public DBRow[] findDoorNoRepeatNotPickUp(long mainId)throws Exception;
    
    //根据mainId 查询该单据下 去重门
    public DBRow[] findSpotOrDoorByCtnr(String ctnr,long ps_id)throws Exception;
   
    //android 用返回停车位 的区域 与停车位 
    /**
     * android 请求门 + 停车位 
     * @param ps_id
     * @param info_id
     * @param request_type (1:ALL. 2:Free)
     * @return
     * @throws Exception
     * @author zhangrui
     * @Date   2014年12月1日
     */
  	public DBRow androidGetSpotArea(long ps_id,long info_id , int request_type )throws Exception;
  	
    //安卓  gate check in
  	public long gateAdnroidCheckInAdd(DBRow data , String filePath , AdminLoginBean adminLoginBean) throws Exception;
  	// 根据entryId,trailerNo  Yuanxinyu
  	public DBRow findResourceByEquipment(HttpServletRequest request , String trailerNo)throws Exception;
  	// 获得设备，及其关联的资源 Yuanxinyu
  	public DBRow[] getCheckOutEntryUseResource(long entry_id)throws Exception;
  	/**
	 * 第一步
	 * 更新entry表的check_out_time和设备表的check_out_time
	 * 如果资源为door，则记录check_out_warehouse_time
	 * @author yuanxinyu
	 */
  	public void checkOutTime(long equipment_id,long adid,long entry_id,String resources_type,long equipment_type,String ...checkOutTimes)throws Exception;
  	/**
	 * 第二步
	 * 把任务释放的资源记回task表
	 * @param equipment_id
	 * @throws Exception
	 * @author yuanxinyu
	 */
  	public void checkOutReturnTheTask(long equipment_id)throws Exception;
  	/**
	 * 第三步
	 * 查看task的状态，如果为processing，unprocess。更新details表中的is_forget_task为true
	 * @param equipment_id
	 * @throws Exception
	 * @author yuanxinyu
	 */
  	public void checkOutTaskStatus(boolean isAndroid ,long adid, long isSearch,long check_in_entry_id, long entry_id, long equipment_id)throws Exception;
	/**
	 * 第四步
	 * 调用詹洁的释放资源方法
	 * @param equipment_id
	 * @throws Exception
	 * @author yuanxinyu
	 */
  	public void checkOutByZj(long equipment_id,long adid,long entry_id,String ...checkOutTimes )throws Exception;
  	// 第五步 根据equipment_id记录seal
  	public void checkOutTheSeal(long equipment_id,String seal)throws Exception;
  	// checkout记录日志 Yuanxinyu
  	public void checkOutTheLog(String mark,String data,boolean isAndroid,long adid,long entry_id, String ...checkOutTimes)throws Exception;
    /**
	 * 根据转运单号搜索转运单
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchCheckInByNumber(String search_key,int search_mode,PageCtrl pc,AdminLoginBean adminLoginBean)
		throws Exception ;
	//修改详细表
	public void modOccupancyDetails(DBRow row)throws Exception;
	
	//添加check in等待信息
    public long addCheckInWait(HttpServletRequest request)throws Exception;
    
    //添加checkin等待信息
    public DBRow[] selectAllCheckInWait(long zoneId)throws Exception;
    
    //根据 id 查询等待信息
    public DBRow findWaitById(long id)throws Exception;
    
    //区域可用门
    public DBRow[] zoneReadyDoor(long zone_id,long dlo_id,String doorName)throws Exception;
    
    //区域占用门
    public DBRow[] zoneUseDoor(long zone_id)throws Exception;
    
   //根据门id 查询门信息
   public DBRow findDoorById(long id)throws Exception;
   
   //根据主单据id 查询单据
   public DBRow findMainById(long dlo_id)throws Exception;
   
   //checkin wahouse
   public long wahouseCheckIn(HttpServletRequest request)throws Exception;
   
   //android checkin wahouse
   public long androidWahouseCheckIn(long infoId,long adid ,String delivery_seal,String pickup_seal, String filePath)throws Exception;
    
	//修改子单据和门状态
//	public DBRow modOccupancyDetailsNumberStatus(HttpServletRequest request)throws Exception;
	
	//check out 
    public void checkInOut(long infoId , String ctnr_no , String isLive , String seal , long adid , long ps_id , boolean isAndroid , HttpServletRequest request)throws Exception;
    //查询停车问区域
    public DBRow[] findSpotArea(long ps_id)throws Exception;
    
    //android check out 
    public void androidCheckInOut(final HttpServletRequest request, long ps_id , long infoId , String isLive ,  String seal , long adid ,String ctnr_no , String filePath)throws Exception;
    
    //web 根据货柜号查询最新单据
    public DBRow getUseDoorOrSpot(String container_no,long ps_id)throws Exception;
    
    //web 根据货柜号查询所有单据
    public DBRow[] getAllDoorOrSpot(String container_no,long ps_id)throws Exception;
    
    //android 根据货柜号查询占用的门和停车位
    public DBRow androidGetUseDoorOrSpot(String container_no,long ps_id)throws Exception;
    
    //pick up 搜索 ctnr方法  得到 门或 停车位方法
  	public DBRow androidFindSpotOrDoorByCtnr(String ctnr,long ps_id) throws Exception ;
    
    //释放门时查看是否有人等待
    public DBRow findWaitingListByDoorId(long doorId)throws Exception;
     
	
    	
    //安卓 巡逻 释放停车位  占用门
  	public DBRow androidMoveToDoor(HttpServletRequest request,long login_id)throws Exception;
	
	//android 区域可用门
    public DBRow[] androidZoneReadyDoor(long dlo_id,long zone_id)throws Exception;
    
    public DBRow[] androidZoneUseDoor(long zone_id)throws Exception;
    
 
	
	
	//查询子单据是否存在
	public DBRow[] findBillIsExist(String order,String type,long ps_id,String company_id)throws Exception;
	
	//跟新子单据状态
	public DBRow androidUpdateDetailsNumberStatus(DBRow row , AdminLoginBean adminLoginBean)throws Exception;
	
	//android check out 扫描entryid
	public DBRow androidCheckOutSearchEntryId(HttpServletRequest request)throws Exception;
	
	//android 扫门 选择停车位
    public long androidCheckInWindowStop(HttpServletRequest request)throws Exception;
    
  //根据停车位id 查询停车位
	public DBRow findStorageYardControl(long yc_id)throws Exception;
	
    //gateCheckOut 查询没有释放的停车位和门
    public DBRow findParkAndDoorByEntryId(long mainId)throws Exception; 
    
    //查询本仓库所有zone
    public DBRow[] findAllZone(long psId)throws Exception;
    
    //根据货柜号查询停车位
	public DBRow findSpotByCtnNo(String container_no,long ps_id)throws Exception;
	
	//关闭ctnr其他的多余的单据  wfh
    public void checkOutCTNR(HttpServletRequest request,DBRow data) throws Exception;
    //查询同一个ctnr 多余的单据 wfh
    public void getcheckoutByCTNR(HttpServletRequest request,long dlo_id,String ctnr,long ps_id)throws Exception;
	
	//根据主单据号查询被占用的门
	public DBRow[] findUseDoorByDloId(long dlo_id)throws Exception;
	
	//查询是否有通知
	public DBRow[] findSchedule(long associate_id,long typeKey)throws Exception;
	
	//查询门下是否有包含没发通知的单据
	public Boolean findScheduleByDoor(long dlo_id,long door_id)throws Exception;
	
    
    //巡逻人员用释放门
  	public DBRow androidMoveToSpot(long info_id , long equipment_id, long spot_id ,long adid , String spot_name)throws Exception;
    
    //根据zoneId查询门
    public DBRow[] findDoorsByZoneId(long zoneId,long mainId,long ps_id,long flag)throws Exception;
    
    //add checkin日志
    public long addCheckInLog(long operator_id,String note,long dlo_id,long log_type,String operator_time,String data)throws Exception;
    //查看  checkin日志
    public DBRow[] findCheckInLog(long dlo_id)throws Exception;
    
    //根据单据号查询 为关闭的门和 停车位
  	public DBRow[] androidGetDoorOrlocationByMainId(long info_id,long ps_id,AdminLoginBean adminLoginBean,String ctnr)throws Exception;
    
    //window 查询通知
  	public DBRow[] windowFindSchedule(long schedule_id)throws Exception;
  	/**
	 * 通过associate，process查询schedule
	 * @param associate_id
	 * @param processKeys
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月5日 上午10:08:22
	 */
	public DBRow[] windowFindSchedule(long associate_id, int[] processKeys)throws Exception;
  	//添加文件
  public void addFiles(HttpServletRequest request,long dlo_id,long createManId) throws Exception;
  //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>张睿添加<<<<<<<<<<<<<<<<<<<<<<<
	/**
	 * 张睿添加(查询一个ID是否有对应的记录)
	 * @param dlo_id
	 * @return
	 * @throws Exception
	 */
	public DBRow countDloId(long dlo_id,AdminLoginBean adminLoginBean) throws Exception ;
	/**
	 * 张睿添加(根据一个ID去查询对应的记录)
	 * (包含详细)
	 * (包含windowCheckIn的信息)
	 * @param dlo_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDoorOrLocationOccupancyMainById(long dlo_id , HttpServletRequest request) throws Exception ;
	
	/**
	 * 查询门的占用信息
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCheckInGetOccupyDoor(long ps_id) throws Exception ;
	
	
	public void addWindowCheckInByAndroid(DBRow dataRow , HttpServletRequest request) throws Exception ;
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>张睿添加<<<<<<<<<<<<<<<<<<<<<<<
	//实时显示停车位状况
	public DBRow[] rtOccupiedSpot(long ps_id,long spot_area,long spotStatus,PageCtrl pc) throws Exception ;
	//查询主表信息
	public DBRow[] findMainMesByCondition(long ps_id,long entryId,String license_plate,String trailerNo) throws Exception ;
	public DBRow[] findDockMainMesByCondition(long ps_id,long entryId,String license_plate,String trailerNo) throws Exception ;
   	//实时显示门状况
	public DBRow[] rtOccupiedDoor(long ps_id,long zone_area,long doorStatus,PageCtrl pc) throws Exception ;
	//根据entryId查询loadnum
	public DBRow[] findLoadNumberByEntryId(long entryId) throws Exception ;
	
	//查找 打印标签title
	public DBRow[] findTitleByAdminId(long adminId)throws Exception;
	
	//根据 titleid 查询 title 与 模版关系表
	public DBRow[] selectTitleLableTemplate(long title_id)throws Exception;
	
	//check in 导出
	public String ajaxDownEntryExport(long adid ,HttpServletRequest request)throws Exception;
	//根据lable name 查询详细
	public DBRow[] findLableByName(DBRow[] rows,long number_type) throws SystemException;

	//安卓链接web 端实现打印功能接口
	public DBRow[] androidLinkWebPrint(String number,long entry_id,String companyId ,String customerId, long number_type,long order_no, long adid, HttpServletRequest request) throws Exception;
	
	//android 根据load companyId customerId获取打印的url
    public DBRow androidLinkWebPrintGetPath(String number, String companyId, String customerId,long entry_id,int number_type,long order_no, long adid, HttpServletRequest request)throws Exception;
	
	//安卓链接web 端实现打印功能接口    送货标签
	public DBRow androidCtnrWebPrint(String ctnr_number,String bol_number,long entry_id, long adid, HttpServletRequest request)throws Exception;
	
	/**
	 * AndroidAutoComplete 查询车尾searchTrailer
	 * @param ps_id
	 * @param containerNo
	 * @param equipment_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月22日
	 */
	public String searchEquipmentByEquipmentType(long ps_id ,String containerNo , int equipment_type) throws Exception ; 
 
	/**
	 * AndroidAutoComplete 查询 searchEquipment
	 * @param ps_id
	 * @param licensePlate
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月22日
	 */
	public String searchEquipment(long ps_id ,String licensePlate) throws Exception ;
	
	//根据live load 、drop off、swap CNTR 查询停车位
	public DBRow[] getAreaBydropOffKey(long ps_id, int dropOffKey,int type,long mainId) throws Exception;
	
	/**
	 * update phone number的数据
	 * @throws Exception
	 */
	public void updatePhoneNumbers() throws Exception ; //zhangrui
	
	/**
	 * 
	 * @param entryId	
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEntryDetailsByEntryId(long entryId) throws Exception ;
	
	/**
	 * 根据一个loadNumber 去查询companyId,或者是customerId，如果是有多个的话那么就应该让他们选择。
	 * 如果是只有一个那么直接返回数据（自己需要的数据 ）
	 * @param loadNumber
	 * @return
	 * @throws Exception
	 * zhangrui -cancel
	 */
	//public DBRow fixCompanyIdAndCustomerId(String loadNumber, long adid, HttpServletRequest request) throws Exception ;
	
	/**
	 * 
	 * @param loadNumber
	 * @return
	 * @throws Exception
	 */
	public DBRow findOrderPalletByLoadNo(int order_type, String orderNumber , String customerId , String companyId, long adid, HttpServletRequest request) throws Exception ;

	/**
	 * 把Entry 查询 和Load查询都放到一个接口中
	 * long searchValue = StringUtil.getLong(request, "searchValue");
			if(searchValue <= 0l){ //包含非数字的searchValue
				//loadNumber
			}else{
				if(searchValue > max(entryid)){
					//loadNumber  //loadNumber 是数字 && 操作系统中最大在Yard Entry 值
				}else{
					//entry .
					//查询是否能够得到数据 （5,10） 加上状态条件 ,
					if(queryIsNull()){
						
					}
					查询Load()
					 
				}
		}
	 * @param searchValue
	 * @return
	 * @throws Exception
	 * zhangrui-cancel
	 */
	//public DBRow findEntryOrLoadBySearchValue(String searchValue) throws Exception ;
	
	/**
	 * 装货的时候关闭Load
	 * @param row
	 * @throws Exception
	 */
	public int closeLoad(DBRow row , AdminLoginBean adminLoginBean) throws Exception ;
	
	/**
	 * @param row
	 * @param loginRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年10月31日
	 * 张睿添加Load的时候Exception的处理
	 */
	public int exceptionLoad(DBRow row , AdminLoginBean adminLoginBean) throws Exception ;
	
	/**
	 * load 提示停留或者离开的时候提示
	 * @param data
	 * @param loginRow
	 * @throws Exception
	 */
	public void closeLoadStayOrLeave(DBRow data , DBRow loginRow) throws Exception ;
	/**
 	 * 在yard里，没到达门
	 * @return
	 * @throws Exception
	 */
	public DBRow[] selectGateCheckIn(long ps_id,PageCtrl pc) throws Exception;
	public DBRow[] selectWareHouseCheckIn(long ps_id,PageCtrl pc) throws Exception;
	public DBRow[] selectWareHouseNumberProcessing(long ps_id,PageCtrl pc) throws Exception;

	/**
 	 * 查询所有loadbar
	 * @return
	 * @throws Exception
	 */
	public DBRow[] selectLoadBar() throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] handFindOrderPalletForLoading(int order_type, String order_no, String CompanyID, String CustomerID, long adid, HttpServletRequest request) throws Exception ;
	
	/**
	 * @param data
	 * @throws Exception
	 */
	public void submitLoadPalletInfo(DBRow data) throws Exception ;
	
	
	/**
	 * @param checkin报表
	 * @throws Exception
	 */
	public DBRow[] checkInExcel(String start_time, String end_time, long ps_id) throws Exception ;
	/**
	 * 
	 * @param checkin报表登录
	 * @return
	 * @throws Exception
	 */
	public  DBRow[] CheckInExcelLogin(HttpServletRequest request) throws Exception ;

	/**
	 * 
	 * @param row
	 * @param loginRow
	 * @throws Exception
	 */
	public void loadEntryLastStayOrLeave(DBRow row , AdminLoginBean adminLoginBean) throws Exception ;
	/**
	 * 是门下面的最后一个子单据的时候
	 * （停留 或者离开执行的代码）
	 * @param row
	 * @param loginRow
	 * @throws Exception
	 */
	public void loadEntryLastReleaseDoor(DBRow row , AdminLoginBean adminLoginBean) throws Exception ;
	
	/**
	 * 
	 * @param entry_id
	 * @param door_name
	 * @throws Exception
	 * @author win7zr
	 */
	public DBRow[] loadSearchEntryDetail(long entry_id , String door_name) throws Exception ;
	/**
	 * 
	 * @param result
	 * @param entry_id
	 * @throws Exception
	 */
	//public void addSearchEntryValue(DBRow result , long entry_id ) throws Exception ;
	
	
	public DBRow[] findSelfStorage() throws Exception;
	
	/**
	 * spot占用情况
	 * @return
	 * @throws Exception
	 */
	public DBRow[] selectSpotSituation(long ps_id,PageCtrl pc) throws Exception;
	/**
	 * dock占用情况
	 * @return
	 * @throws Exception
	 */
	public DBRow[] selectDockSituation(long ps_id,PageCtrl pc) throws Exception;

	/**
	 * 装货的时候load提交图片
	 * @param data
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月11日
	 */
 	public void loadSubmitPhoto(DBRow data , DBRow loginRow) throws Exception ;
 	/**
 	 * 添加一个接口
 	 * @param dlo_detail_id
 	 * @throws Exception
 	 * @author zhangrui
 	 * @Date   2014年9月12日
 	 */
 	public DBRow selectDetailByDetailId(long dlo_detail_id) throws Exception ;
 	
 	/**
 	 * 把private的方法改成public
 	 * @param filePath
 	 * @param adid
 	 * @param dlo_id
 	 * @param prefix
 	 * @author zhangrui
 	 * @Date   2014年9月13日
 	 */
 	public void androidSaveMoveFile(String filePath , long adid , long dlo_id , String prefix) throws Exception ;
 	/**
 	 * 将文件夹内文件上传至文件服务器
 	 * @param dirPath	文件夹路径
 	 * @param dlo_id
 	 * @param prefix	文件名前缀
 	 * @param sessionId		会话ID
 	 * @throws Exception
 	 * @author chenchen
 	 */
 	public void androidSaveMoveFile(String dirPath, long dlo_id , String prefix, String sessionId) throws Exception;

	public String checkInImportSeals(HttpServletRequest request)throws Exception ;
	public DBRow[] checkInCheckSeals(HttpServletRequest request) throws Exception ;

	public DBRow[] selectSpotSituation(int ps_id, PageCtrl pc)throws Exception;

	public DBRow[] selectDockSituation(int ps_id, PageCtrl pc)throws Exception;
	/**
	 * 根据checkout时间查询load
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLoadBarReport(String startTime,String endTime,long ps_id) throws Exception;

	/**
	 * 根据checkout时间查询load
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSealReport(String startTime,String endTime,long ps_id) throws Exception;
	/**
	 * 根据checkout 时间查询 seal
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTask(String startTime,String endTime,long ps_id) throws Exception;
	
	/**
	 * 报表查找 load
	 * @param request
	 * @param adid
	 * @return
	 * @throws SystemException
	 */
	public List<DBRow> getLoadNo(HttpServletRequest request,long adid,String startTime,String endTime) throws Exception;
	
	/**
	 * 报表查找 order Po
	 * @param request
	 * @param adid
	 * @return
	 * @throws SystemException
	 */
	public List<DBRow> getOrderAndPoNo(HttpServletRequest request,long adid,String startTime,String endTime,long ps_id) throws Exception;
	
	/**
	 * 报表查找 receiptBol
	 * @param request
	 * @param adid
	 * @return
	 * @throws SystemException
	 */
	public List<DBRow> getReceiptBol(HttpServletRequest request,long adid,String startTime,String endTime , long ps_id) throws Exception;
	
	/**
	 * 报表 查找占用的门的task
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public DBRow[] getOccupyDoor( long ps_id) throws Exception;
	
	/**
	 * 报表 查找占用的停车位的task
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public DBRow[] getOccupyYard( long ps_id) throws Exception;
	
	/**
	 * 查找每个order对应的palletCount	新版本的系统使用这个方法查找palletTypeCount
	 * @param ps_id
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public DBRow[] newFindWMSOrderPalletCount(long ps_id, String startTime, String endTime) throws Exception;
	

	public DBRow[] selectDockCloseNoCheckOut(int ps_id, PageCtrl pc) throws Exception;
	/**
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年10月13日
	 */
	public boolean isDetailLastOfEntry(long dlo_detail_id , long equipment_id) throws Exception ;
 	
	
	public DBRow findLoadBar(long load_bar_id)throws Exception;
 	public long addFile(String fileName,long file_with_id,int file_with_type,int file_with_class , long creator,String upload_time) throws Exception;
	public DBRow[] getUseSpot(long ps_id) throws Exception;
	public DBRow[] selectTaskBy(long rl_id, long main_id,int occupancy_type) throws Exception;
	public DBRow[] fixAndroidGateCheckOutOccupiedAndRDoorInfo(DBRow[] doors);
	public DBRow[] fixAndroidGateCheckOutAccupiedAndSpotInfo(DBRow[] spots);
	public DBRow[] selectLocationByEntryId(long dlo_id) throws Exception;
	public DBRow[] selectUnavailableSpot(long ps_id,long area_id,long dlo_id)throws Exception;
	public void addPrintedLabel(HttpServletRequest request) throws Exception ;
	
	public void editCheckInIndex(long dlo_id,String type)throws Exception;
 	
	
	public DBRow[] getEntryDetailHasResources(long entry_id,long equipment_id) throws Exception ;
  
	/*
	 * 查询可用的停车位
	 */
	public DBRow[] getVacancySpot(long ps_id,long area_id,long entry_id)throws Exception;
	/*
	 * 查询被占用的停车位
	 */
	public DBRow[] getOccupiedSpot(long ps_id,long area_id,long entry_id)throws Exception;
	/*
	 * 根据yc_no模糊查询可用的停车位
	 */
	public DBRow[] getVacancySpotByYcno(long entry_id,long area_id,long ps_id,String yc_no) throws Exception;
	
 
	
	public void checkEntryPermission(long entry_id ,AdminLoginBean adminLoginBean)  throws Exception  ;
	/**
	 * @param sd
	 * @param entry_id
	 * @param isAndroid
	 * @param adminLoginBean
	 * @param request
	 * @param filePath
	 * @throws Exception
	 * @author yuanxinyu
	 */
	public DBRow checkOut(boolean isAndroid, HttpServletRequest request) throws Exception ;
	public void ymsCheckOut( GateCheckoutInfo checkOutInfo)throws Exception ;

	public DBRow[] filterDetailsInDoor( long resources_id,int resources_type , DBRow[] details) throws Exception  ;
 	public String[] getAndroidCheckOutFiles(long info_id) throws Exception ;
	// 查询可用的门
	public DBRow[] getVacancyDoor(long ps_id,long area_id,long entry_id)throws Exception;
	//根据doorName等条件模糊查询可用的门
	public DBRow[] getCanUseDoorByDoorName(long entry_id,long ps_id,long area_id,String door_name) throws Exception;
	// 根据doorName等条件精确查询门
	public DBRow[] getDoorByDoorName(long entry_id,long ps_id,long area_id,String door_name) throws Exception;
	// 查询不可用的门
	public DBRow[] getUnavailableDoor(long ps_id,long area_id) throws Exception;
	// 模糊查询不可用的门
	public DBRow[] getUnavailableDoorByDoorName(long ps_id,long area_id,String door_name) throws Exception;
	// 精确查询不可用的门
	public DBRow[] findUnavailableDoorByDoorName(long ps_id,long area_id,String door_name) throws Exception;
	// window check in 时查找被占用的门
	public DBRow[] getOccupiedDoorOfWindow(long ps_id,long area_id) throws Exception;
 	/**
 	 * 当前Entry detail 是否已经完成了
 	 * @param row
 	 * @return
 	 * @author zhangrui
 	 * @Date   2014年12月1日
 	 */
 	public boolean isEntryDetailIsFinish(DBRow row ) ; 
 	public DBRow[] getTaskByEquimentId(long equipment_id,int number_status[])throws Exception;
 	public long updateDetailByIsExist(long detail_id,DBRow row)throws Exception;

 	
 	/**
 	 * 处理停留或者是离开
 	 * @param equipment_id
 	 * @param status
 	 * @throws Exception
 	 * @author zhangrui
 	 * @Date   2014年12月3日
 	 */
 	public void leavingOrStay(long equipment_id , int status , long adid) throws Exception ; 
 
 	
 	/**
	 * 根据主单据号查询占用的信息
	 * @param module_id
	 * @param module_type
	 * @return
	 * @throws Exception
	 */
	public DBRow getResourcesByModuleId(long module_id)throws Exception;
	
	/**
	 * 根据主单据id查询 设备占用的停车位   gatecheckin 标签用  zwb
	 * @param module_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getEquipmentUseSpot(long module_id) throws Exception ;
	
	/**
	 * 根据主单据id查询 task占用的门 gatecheckin 标签用 zwb
	 * @param module_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTaskUseDoor(long module_id) throws Exception ;
	
	/**
	 * 根据设备id 查询任务
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTaskByEquipmentId(long equipmentId)throws Exception;
	
	/**
	 * 根据entry_id和task查询设备表信息      geql
	 * @return
	 * @throws Exception
	 */
	public DBRow getEquipmentByTask(long entry_id,String number)throws Exception;

	/**通过entryId查询车头信息
	 * @author xujia
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow selectTractorByEntryId(long entry_id) throws Exception;
	/**通过entryId查询车尾信息
	 * @author xujia
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow selectTrailerByEntryId(long entry_id) throws Exception;
	/**通过entryId查询带走的设备
	 * @author xujia
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow selectPickUpCTNRByEntryId(long entry_id) throws Exception;
	
	/**根据主单据id查询 设备表  gatecheckin 标签用 zwb

	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEquipmentByEntryId(long entry_id) throws Exception ;
	/**根据entryId  查询ctnr 所占用的资源  门卫标签用 geqingling
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCTNRResourceByEntryId(long entry_id) throws Exception ;
	public DBRow getEntryPrint(long entry_id,AdminLoginBean adminLoggerBean) throws Exception ;
	/**
	 * 更新gps
	 * @param gps
	 * @param row
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月15日 下午9:03:11
	 */
	public void updateGpsInfos(long gps, DBRow row) throws Exception;
	
	
	/**
	 * 根据yc_no精确查询可用的停车位  zwb
	 * @param entry_id
	 * @param area_id
	 * @param ps_id
	 * @param yc_no
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getVacancySpotByYcnoAccurate(long entry_id,long area_id,long ps_id,String yc_no)throws Exception;
	
	/**
	 * 下面一个方法的重载
	 * @param equipment_id
	 * @param entry_id
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月20日
	 */
	public int closeDetailReturnFlag(DBRow equipment , long entry_id , long dlo_detail_id ) throws Exception ;
	/**
	 * 关闭门的时候调用是否是最后一个
	 * @param equipment_id
	 * @param entry_id
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月16日
	 */
	public int closeDetailReturnFlag(long equipment_id , long entry_id , long dlo_detail_id ) throws Exception ;
	
	/**
	 * Detail 数据 fixResoucesIdAndResourceType 
	 * 适合于Close的Detail 和Space表中的数据  detail left join space
	 * @param currentLoadRow
	 * @return
	 * @author zhangrui
	 * @Date   2014年12月18日
	 */
	public HoldDoubleValue<Long, Integer> returnFixResourcesIdAndResourcesType(DBRow currentLoadRow)  ;
	/**
	 * check in index 显示通知
	 * @xujia
	 * @param entry_id
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findNoticesByAssociateProcess(long entry_id,int associate_process)throws Exception;
	
	/**
     * 查询一条通知详细
     * @xujia
     * @param schedule_sub_id
     * @return
     * @throws Exception
     */
    public DBRow findNoticeByscheduleSubId(long schedule_sub_id)throws Exception;

    /**
	 * 查询gate发的所有通知详细
	 * @param id
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
    public DBRow[] findGateotices(long entry_id)throws Exception;

    //
    public DBRow getSpaceResorceRelation(int relation_type,long relation_id)throws Exception;
    /**
	 * 验证单据类型
	 * @param type
	 * @param search_number
	 * @param mainId
	 * @param ps_id
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月22日 下午5:25:36
	 */
	public DBRow checkOrderTypeSystem(long type,String search_number,long mainId,long ps_id, long adid) throws Exception;
	
	 /**
     * 查找每个order对应的palletCount
     * @param ps_id
     * @param startTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public DBRow[] findWMSOrderPalletCount(long ps_id, String startTime, String endTime) throws Exception;
    
    /**
     * task是否扫描过Pallet
     * @param detail_id
     * @return
     * @throws Exception
     */
    public boolean isTaskScanPallet(long detail_id) throws Exception;
    /**
     * 
     * @param fileWithId
     * @param fileWithType
     * @param fileWithClass
     * @return
     * @author:chenchen
     * @throws Exception
     */
    public DBRow[] getFileBy(long fileWithId, int fileWithType, int fileWithClass) throws Exception;
    
    public String gateCheckInAddNotice(long entry_id,long detail_id,long door_id,long spot_id,AdminLoginBean adminLoginBean,String number,int number_type,String equipment_number,int equipment_type)throws Exception;
    
    //bol标签用根据entry id 查询 车头的设备
  	public DBRow findTractorByEntryId(long entry_id)throws Exception;
  	
  	//处理image转PDF
  	public String handleImageToPDF(HttpServletRequest request) throws Exception;
  	
  //	处理image转PDF
  	public String handleHtmlToPDF(HttpServletRequest request) throws Exception;
  	
  	
  	//根据entryId查询去重的所有dn对应的文件id，下载pdf用
  	public DBRow[] getFileIdByEntryId(long EntryId,HttpServletRequest request)throws Exception;
  	
    //查询货柜号android
  	public DBRow androidGetCtnr(long entry_id,String number,int number_type)throws Exception;
  	
  	/**
	 * 根据 设备id 查询设备详细信息
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getEquipmentByEquipmentId(long equipment_id)throws Exception;
	
	//删除load下的所有的pdf 1 先删除文件服务器上的pdf 2 然后再printed_label清表
	public void deletPdfByLoad(HttpServletRequest request,long entryId,String loadNo,String customerId);
	//判断是否显示Time keeper 信息。a4print.jsp签使用
	public boolean showTimeKeeper(String accountId,String carrierId);
	/**
	 * 安卓打印Tlp标签
	 * @param PalletIds
	 * @return
	 * @throws Exception
	 */
	public DBRow[] queryPalletsByLineAndId(String PalletIds)throws Exception;
	
	public DBRow[] queryLinesByRNForTicketPrint(long receipt_no, String company_id) throws Exception;
 
	public DBRow[] queryPalletsInfoByLineIdForTicketPrint(long receipt_line_id) throws Exception;
	//安卓 收货打印标签 end
	
	/**
	 * 组合counting sheet页面的显示的数据
	 * a4_counting_sheet.jsp页面使用
	 * @author gql
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @param adid
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOrderItemRowOfCountingSheet(String loadNo, String companyId, String customerId, long adid, HttpServletRequest request)throws Exception;
	
	public DBRow[] findPOTypeOfDN(Long ps_id) throws Exception;
	public  DBRow[] findOrderLineByBundle(String comanyId,DBRow[] lines)throws Exception;
	public DBRow findOrderByBundle(String comanyId,DBRow line)throws Exception;
	public DBRow[] findOrderByBundle(String comanyId, DBRow[] lines)throws Exception;
	public DBRow  findOrderByBundle(String orderNo,String comanyId)throws Exception;
	public void addYmsCheckOutRequest(String entryId,long psId,String json) throws Exception; 
	public List<GateCheckoutInfo> getYmsCheckOutRequests(String status) throws Exception;
	//public  DBRow ymsGateCheckInAdd(HttpServletRequest request) throws Exception;
	public  DBRow ymsGateCheckInAdd(HttpServletRequest request,EntryTicketCheck entryTicketCheck) throws Exception;
	public   List<EntryTicketCheck> getYmsCheckinRequests(String status)throws Exception;
	public EntryTicketCheck getYmsEntryTicket(String entryId,long psId) throws Exception;
	public void addYmsCheckInRequest(String entryId,long psId,String json) throws Exception ;

	public DBRow[] searchCheckInByNumber(String search_key, int search_mode,
			PageCtrl pc, AdminLoginBean adminLoginBean,
			HttpServletRequest request) throws Exception;
	
	public String getRabbitMqServer();
	public String getRabbitMqUserName() ;
	public String getRabbitMqPassword() ;
	public String getCheckInQueue() ;
	public String getCheckOutQueue() ;
}
 


