package com.cwc.app.iface.zwb;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface TransportMgrIfaceZwb {

	public DBRow getTransport(long transportId) throws Exception;
	
	//根据转运单号 查出该转运单下的商品
	public DBRow[] getTransportProduct(long transportId) throws Exception; 
	
	//添加转运单跟进
	public long addTransferLog(HttpServletRequest request)throws Exception;
	
	//根据转账id查询 跟进日志信息
	public DBRow[] getTransferlogs(long transferId)throws Exception;
	
	//根据商品名检索 转运单 里商品
	public DBRow[] getTransportProductByName(long transport_id,String name)throws Exception;
	
	//查询根据出库仓库 到货仓库转运单
	public DBRow[] findTransport(long send_psid,long receive_psid)throws Exception;
	
	//创建出库单  并更新转运单  //出货仓库send_psid
	public DBRow addOutUpdateTransport(HttpServletRequest request)throws Exception;
	
	//根据拣货单id 查询转运单
	public DBRow[] selectTransportByOutId(long out_id)throws Exception;
	
	//根据拣货单id  查询拣货单里所有货物
	public DBRow[] selectOutDetail(long out_id)throws Exception;
	
	//查询货物位置
	public DBRow[] findSlcId(long out_id)throws Exception;
	
	//根据区域id查询位置 去重
	public DBRow[] findSlcByAreaId(long areaId,long out_id)throws Exception;
	
	//查询区域
	public DBRow[] findArea(long out_id)throws Exception;
	
	//查询转运单总数量 根据出库仓库和到货仓库
	public DBRow findTransportCount(long send_psid,long receive_psid)throws Exception;
	
	//查询区域是2d 还是3d
	public DBRow findCollocationDetail(String areaName)throws Exception;
	
	
	
	
	
}
