package com.cwc.app.iface.zwb;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface PreparePurchaseMgrIfaceZwb {

	
	//添加预申请资金
	public DBRow prepareAddApplyMoney(String lastTime,String adid,long categoryId,long association_id,double amount,String payee,String paymentInformation,String remark,long center_account_id,long product_line_id,int association_type_id,long center_account_type_id,long center_account_type1_id,long payee_type_id,long payee_type1_id,long payee_id,String currency, AdminLoginBean adminLoggerBean) 
	throws Exception ;
	
	//调度确定可以转账
	public void affirmTransferPurchase(HttpServletRequest request) 
	throws Exception;
	

	//确认可以添加预付款
	public void affirmTransferPurchaseDeposit(HttpServletRequest request)
			throws Exception;
	
	//添加预付款功能
	public DBRow addPurchaseDeposit(String lastTime,String adid,
			long categoryId, long association_id, double amount, String payee,
			String paymentInformation, String remark, long center_account_id,
			long product_line_id, int association_type_id,
			long center_account_type_id, long center_account_type1_id,
			long payee_type_id, long payee_type1_id, long payee_id,
			String currency, AdminLoginBean adminLoggerBean) throws Exception;
	
	/**
	 * 获取所有预申请信息（带分页的）
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllPreparePurchase(PageCtrl pc)
	throws Exception;
	
	 /**
     * 根据条件查询预申请信息（带分页）
     * @param request
     * @param pc
     * @return
     * @throws Exception
     */
	public DBRow[] getPreparePurchase(HttpServletRequest request,PageCtrl pc)
    throws Exception;
	
	//根据采购单id 查询是否申请了定金
	public long findPuerchaseDeposit(long purchaseId)throws Exception;
	
	//根据id删除预申请
	public long detApplyMoney(long id)throws Exception;
	
	//根据id 查询预申请
	public DBRow getByApplyMoney(long id)throws Exception;
	
	//根据id更新预申请
	public void updateApplyMoney(long apply_id,String adid,long categoryId,String association_id,double amount,String payee,String paymentInformation,String remark,long center_account_id,long product_line_id,String association_type_id, long center_account_type_id, long center_account_type1_id,long payee_type_id,long payee_type1_id,long payee_id,String currency, AdminLoginBean adminLoggerBean) 
	throws Exception; 
	
	//根据采购单id查询 预申请表 是否申请过   预申请只能申请一次
	public DBRow getPreparePurchaseFunds(long purchaseId)throws Exception;
	
	//上传
	public String uploadImage(HttpServletRequest request) throws Exception;
	public DBRow transportApplyMoney(HttpServletRequest reqeust) throws Exception;
	/**
	 * 查询某个业务是不是已经申请了资金。(查询是否有包含没有取消的资金申请)
	 * @param association_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyMoneyByAssociationIdAndTypeHasApply(long association_id , long type) throws Exception ;
	
	public boolean isTransportApplyMoneyAllow(long purchase_id , double money) throws Exception ;
}
