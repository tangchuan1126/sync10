package com.cwc.app.iface.zwb;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.db.DBRow;

public interface ProductLineMgrIfaceZwb {

	//查询所有生产线
	public DBRow[] getAllProductLine() throws Exception;

	//查询所有供应商
	public DBRow[] getAllSupplier() throws Exception;

	//查询所有采购单
	public DBRow[] getAllPurchase() throws Exception;
	
	//根据生产线id查询供应商
	public DBRow[] getSupplier(long id) throws Exception ;
	
	public DBRow[] getPurchase(String supplierId) throws Exception;
	
	//根据商品id 采购单id查询 工厂类型
	public DBRow getFactorType(long purchaseId,long productId)throws Exception;
	
	//android查询产品信息
	public DBRow[] seachProductByCatalogIdType(long catalogId,String type,String seachValue)throws Exception;
	
	//根据产品线id查询产品分类
	public DBRow[] getProductCatalogByProductLine(long productId) throws Exception;
	
	//根据父id查询产品分类
	public DBRow[] getProductCatalogByParentId(long id)throws Exception;
	
	//根据产品线查询下面所有商品
	public DBRow[] selectProductByLineId(long productLineId,String type,String seachValue)throws Exception;
	
	//按搜索的值查询
	public DBRow[] seachProductBySeachValue(String type,String seachValue)throws Exception;
	
	//根据商品id查询商品
	public DBRow selectProductByProductId(long productId)throws Exception;
	
	//添加title 产品线
	public void addProductLineTitle(DBRow row)throws Exception;
	
	//查询title根据产品线id
	public DBRow[] selectTitleByProductLine(long productId)throws Exception;
	
	//添加产品分类和title 关系
	public void addProductCatalogTitle(long productCatalogId,long titleId)throws Exception;
	
	//根据产品分类id 查询title关系
	public DBRow[] selectTitleByProductCatalog(long productCatalogId)throws Exception;
	
	//删除产品线 title关系 -产品分类title关系-商品title关系
	public void detTitleByProductLine(long productLineId,long titleId)throws Exception;
	
	//删除产品分类 title关系    子产品分类-商品
	public int detTitleByProductCatalog(long productCatalogId,long titleId)throws Exception;
	
	//变更产品线与title关系
	public void updateTitleByProductLine(long productLineId,long titleId,long toTitleId)throws Exception;
	
	//变更产品分类与title关系
	public void updateTitleByProductCatalog(long productCatalogId,long titleId,long toTitleId)throws Exception;
	
	//根据产品id查询 商品与title关系表
	public DBRow[] selectProductTitleByProductId(long productId)throws Exception;
	
	//查询title下  可用总数量
	public DBRow selectTitleCount(long titleId,long productId)throws Exception;
	
	
	public DBRow[] selectLotNumberCount(long titleId,long productId,long ps_id,long lotNumber)throws Exception;
	
	//商品 title 下某批次的 位置数量
	public DBRow[] selectLotNumberLocationCount(long pcId,long titleId,long lot_number,long ps_id)throws Exception;
	
	//商品title 位置下所有托盘 数量
	public DBRow[] selectLotNuberLpCount(long productId,long titleId,long locationId,long lotNumber)throws Exception;
	
	//容器类型总数
	public DBRow[] selectContainerTypeNumber(long productId,long titleId,long lotNumberId)throws Exception;
	
	//查询根据容器类型id0
	public DBRow[] selectContainerDetail(long productId,long titleId,long lotNumberId,long containerType)throws Exception;
	
	//根据托盘id查询托盘
	public DBRow selectLpByLpId(long lpId)throws Exception;
	
	//上传
	public String ajaxDownTitleList(HttpServletRequest request)throws Exception;
	
	//上传
	public DBRow[] ajaxUploadTitleList(String filename,long adminId)throws Exception;
	
	//产品线导出title关系
	public String ajaxDownProductLineAndTitle(HttpServletRequest request)throws Exception;
	
	//产品线导入title 关系
	public DBRow[] ajaxUploadProductLineAndTitle(String filename,long adminId)throws Exception,FileTypeException;
	
	//导出产品分类与title的关系
	public String ajaxDownCatalogAndTitle(HttpServletRequest request)throws Exception;
	
	//产品分类与title关系导入
	public DBRow[] ajaxUploadCatalogLineAndTitle(String filename,long adminId)throws Exception,FileTypeException;
	
	//导出产品分类与title的关系
	public String ajaxDownProductgAndTitle(HttpServletRequest request)throws Exception;
	
	//根据title查询产品下
	public DBRow[] selectProductLinewByTitleId(long titleId)throws Exception;
	
	//查询所有title不区分帐号
	public DBRow[] selectAllTitle()throws Exception;
	
	//admin登录 根据titleid 查询产品线
	public DBRow[] selectProductLineAllByTitleId(String titleId)throws Exception;
	
	//非客户 根据title查询产品分类
	public DBRow[] selectProductCatalogByTitleId(long titleId)throws Exception;
	
	//分仓库查库存
	public DBRow[] selectWarehouse(long productId,long titleId,long lot_number)throws Exception;
	
}
