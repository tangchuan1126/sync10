package com.cwc.app.iface.zwb;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ProductLableTempMgrIfaceWFH {
	//查询所有的lable
	public DBRow[] findAllLable(String lableName,String type,int templateType,PageCtrl pc) throws Exception;
	
	//新建基础标签模板信息
	public long addLableTemp(DBRow row) throws Exception;
	
	//根据id查询基础标签模板信息
	public DBRow getLableTempById(long id)throws Exception;
	
	//根据id修改
	public void modifyLableTemp(long id,DBRow row) throws Exception;
	
	//根据id删除
	public void delLabelTemp(long id)throws Exception;
	
	
	
	
}
