package com.cwc.app.iface.zwb;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface WaybillMgrIfaceZwb {
	
	//查询商品在该产品线下的运单数量
	public DBRow getWaybillByCountLineId(long lineId) throws Exception;
	
	//包含件数  
	public DBRow seachPkcount(long num)throws Exception;
	
	//包含多件数  
	public DBRow seachPkcountMuch(long num)throws Exception;
	
	public DBRow getSeachWaybillByLineId(long lineId,long sc_id,long ps_id,long all_weight,long pkcount)throws Exception;
	
	public DBRow getCountPkcountMuch(long lineId,long sc_id,long ps_id,long all_weight,long pkcount)throws Exception;
	
	//条件查询包含数量 123
	public DBRow getCountPkcount(long sc_id,long ps_id,long all_weight,long pkcount) throws Exception;
	
	//条件查询包含数量 大于4   没有产品线
	public DBRow getCountPkcountMuchNoLine(long sc_id,long ps_id,long all_weight,long pkcount)throws Exception;
	
	//查询 运单详细没有产品线条件 detailed
	public DBRow[] seachWaybillDetailedNoLine(long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception;
	
	//查询 运单详细没有产品线条件 detailed
	public DBRow[] seachWaybillDetailedNoLineFour(long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception;
	
	//查询 运单详细有产品线条件 detailed
	public DBRow[] seachWaybillDetailed(long lineId,long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception;
	
	//查询 运单详细有产品线条件 detailed
	public DBRow[] seachWaybillDetailedFour(long lineId,long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception;
	
	//创建拣货单、 返回当前拣货单id
	public DBRow addOutOrder(HttpServletRequest request)throws Exception;
	
	//根据拣货单id查询运单
	public DBRow[] findWaybillByOutId(long outId)throws Exception;
	
	//查询出库单 先查所有
	public DBRow[] selectAllOutOrder(long out_id,long ps_id,long outRorType,PageCtrl pc)throws Exception;
	
	//扫描货物打印
	public DBRow printScanProduct(HttpServletRequest request)throws Exception;
	
   //扫描货物打印1票多件
	public DBRow[] printScanProductMuch(HttpServletRequest request)throws Exception;
	
	//查询快递名
	public DBRow findExpressDetailed(long sc_id)throws Exception;
	
	//计算出库单状态
	public String printStatusOutOrder(long out_id)throws Exception;
	
	//根据id查询 拣货单
	public DBRow selectOutOrder(long out_id)throws Exception;
	
	//商品
	public DBRow findProductById(long pc_id)throws Exception;
	
	//查询位置
	public DBRow findStorageLocationCatalogById(long slc_id)throws Exception;
	
	//查询区域
	public DBRow findStorageLocationAreaById(long area_id)throws Exception;
	
	//拣完货，需要放到的门和位置  根据类型 是拣货单  和拣货单id
	public DBRow[] getStorageOrDoorByOutId(long rel_type,long rel_id)throws Exception;
	
	//运单创建拣货单分配人
	public DBRow[] getPeopleList(long ps_id)throws Exception;
	
}
