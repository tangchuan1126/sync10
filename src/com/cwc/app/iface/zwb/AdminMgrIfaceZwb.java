package com.cwc.app.iface.zwb;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;

public interface AdminMgrIfaceZwb {
	
	  public DBRow[] androidLogin(String name,String pwd)throws Exception;
	  
	  public DBRow[] getAdminDetil(long psId)throws Exception;
	  
	  public DBRow[] getProprietaryByadminId(long adminId)throws Exception;
	
	public int findAdminByGroupClientAdid(long adminId) throws Exception;
		
	public Boolean findAdminGroupByAdminId(long adminId)throws Exception;
	
	public DBRow findAdminPs(long adminId)throws Exception;
}
