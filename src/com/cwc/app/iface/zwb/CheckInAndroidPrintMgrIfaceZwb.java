package com.cwc.app.iface.zwb;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;

public interface CheckInAndroidPrintMgrIfaceZwb {

	public DBRow androidSelectLabel(long adid,long entry_id,long detail_id,  HttpServletRequest request)throws Exception;
	
 }



