package com.cwc.app.iface.xj;


import javax.servlet.http.HttpServletRequest;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface CheckInMgrIfaceXj {
	//实时显示停车位状况
	public DBRow[] rtOccupiedSpot(long ps_id,long spot_area,int spotStatus,PageCtrl pc) throws Exception ;
	//调换停车位
	/**
	 * 
	 * @param dlo_id(原来SpotId 上对应的EntryId)
	 * @param searchdlo_id(被添加的EntryID)
	 * @param spotId(点击的停车位ID)
	 * @param searchSpotId(查询出来的SpotId)
	 * @throws Exception
	 */
    public void modifySpotEquipment(long adid,long equipment_id,int resource_type,long  resource_id) throws Exception ;
	//实时显示门状况
	public DBRow[] rtOccupiedDoor(long ps_id,long zone_area,int doorStatus,PageCtrl pc) throws Exception ;
    //数据校验
    public void verifyData(long adminId,long ps_id,long mainId,String license_plate,String trailerNo,int flag,int doubt)throws Exception;
    //停车位提交审核
    public long checkInPatrolApprove(HttpServletRequest request)throws Exception;
    //修正错误数据
    public void verifyCheckOut(HttpServletRequest request)throws Exception;
    //修正一条错误数据
    public void verifyCheckOutSingle(long equipment_id,String note,AdminLoginBean adminLoginBean) throws Exception;
    //查询停车位提交审核
    public DBRow[] findCheckInPatrolApprove(long ps_id,long approve_status,PageCtrl pc)throws Exception;
    //查询停车位提交审核详细
    public DBRow[] findCheckInPatrolApproveDetails(long ps_id,long pa_id,PageCtrl pc)throws Exception;
    //confirm停车位
    public long ConfirmPatrolSpot(long yc_id,long dlo_id,long area_id)throws Exception;
    //confirm门
    public long ConfirmPatrolDock(long sd_id,long dlo_id,long area_id)throws Exception;
    //重新patrol停车位
    public DBRow AgainPatrolSpot(long ps_id,long area_id)throws Exception;
    //重新patrol门
    public DBRow AgainPatrolDock(long ps_id,long area_id)throws Exception;
    
    public DBRow againPatrolResource(long resource_id,int resource_type,long area_id,AdminLoginBean adminLoginBean) throws Exception;
    //清空区域完成patrol时间
    public long ClearAreaPatrolDoneTime(long ps_id,long area_type ,long area_id)throws Exception;
    //仓库是否全部patrol完成
    public long findStoragePatrolTimeByPsId(long ps_id) throws Exception;
    public long AndroidConfirmPatrolSpot(long yc_id,long dlo_id,long area_id) throws Exception ;
    public long AndroidConfirmPatrolDock(long sd_id,long dlo_id,long area_id) throws Exception;
    
  	public DBRow[] verifySpot(long ps_id,PageCtrl pc) throws Exception;
  //patrol时 ，创建entryId
  	public long createEntryId(HttpServletRequest request)throws Exception;
  	//android 提交审核
  	public long AndroidcheckInPatrolApprove(HttpServletRequest request) throws Exception;
  	
    //ctnr自动提示
    public DBRow[] getSearchCheckInCTNRJSON(HttpServletRequest request,String trailerNo)throws Exception;
    //车牌号自动提示
    public DBRow[] getSearchCheckInLicensePlateJSON(HttpServletRequest request,String license_plate)throws Exception;
    
    public DBRow[] returnSpot(long ps_id, long area_id, int spot_status,int is_finish, PageCtrl pc) throws Exception;
    
    public DBRow[] returnDoor(long ps_id, long area_id, int door_status,int is_finish, PageCtrl pc) throws Exception;
    
  
    public DBRow findResourceByEquipment (long ps_id, String license_plate, String trailerNo)throws Exception;
    
    public void delSpaceResourcesRelation(int relation_type ,long relation_id ,long adid) throws Exception;
    
    public DBRow[] getEquipmentByDoorId(long door_id) throws Exception ;
	public DBRow[] getEquipmentBySpotId(long yc_id) throws Exception;
	
	public DBRow[] getInYardEquipmentByEquipmentNumberLast (long ps_id, String equipment_number)throws Exception;
	
	public void batchModifySpotEquipment(long adminId, String equipment_id,int  resource_type, long resource_id)throws Exception;
	
	public void updateEquipment(HttpServletRequest request) throws Exception;
	
	public DBRow confirmPatrolResource(long resource_id, int resource_type,long area_id,AdminLoginBean adminLoginBean) throws Exception;
	
	public void updateCtnrStatus(long ps_id) throws Exception ;
 }



