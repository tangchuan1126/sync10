package com.cwc.app.iface.xj;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;



public interface ProductChangeMgrIFaceXJ 
{
	//向主表和详细表里添加数据
	public long addProductChange(HttpServletRequest request) throws Exception;
	//根据主单据id查找详细表的记录
	public DBRow[] findProductChangeItems(long cid,PageCtrl pc) throws Exception;
	//查询主表记录
	public DBRow[] findProductChange(PageCtrl pc,String ps_id,long is_open,String startTime,String endTime) throws Exception;
	//修改详细表记录
	public void modProductChangeDetail(HttpServletRequest request) throws Exception;
	//删除详细表记录
	public void delProductChangeDetail(HttpServletRequest request) throws Exception ;
	//向详细表添加记录
	public void addProductChangeDetail(HttpServletRequest request) throws Exception;
}
