 package com.cwc.app.iface.xj;

 


import java.util.Map;

import javax.servlet.http.HttpServletRequest;



public interface CarsCommandsMgrIFaceXJ {
	
	
    /**
     * 查询历史轨迹
     * @param request
     * @return
     */
    public String queryHistory(HttpServletRequest request);
    /**
     * 查询实时位置
     * @param ids
     * @return
     */
	public String getLastPos(String ids);
	/**
	 * 写入指令
	 * @param assetid
	 * @param name
	 * @param call
	 * @param loctype
	 * @param cmdtype
	 * @param params
	 * @throws Exception
	 */
	public void insertCmd(String assetid,String name,String call,String loctype,int cmdtype,Map params) throws Exception;
}
