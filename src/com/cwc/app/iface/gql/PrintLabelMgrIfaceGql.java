package com.cwc.app.iface.gql;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.db.DBRow;

public interface PrintLabelMgrIfaceGql {
	/**
	 * 根据条件查询是否转pdf，是否需要发送请求
	 * @param conver_pdf
	 * @return
	 * @throws Exception
	 */
	public DBRow[] queryPrintTaskBy(int conver_pdf) throws Exception;
	/**
	 * 根据print_task_id更改print_task状态
	 * @param print_task_id
	 * @return
	 * @throws Exception
	 */
	public long updateConvertStatusByPintTaskId(long print_task_id) throws Exception;
	/**
	 * 查询masterbol和bol签的url
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow getPrintUrlByParam(HttpServletRequest request) throws Exception;
	/**
	 * 查询masterbol和bol签的url
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPrintUrlByTime(HttpServletRequest request) throws Exception;
	
	/**
	 * counting sheet 追加到pdf
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String addCountingSheetToPdf(HttpServletRequest request) throws Exception;
	/**
	 * 将trailer loader 和freight counted的值记录到label_template_detail表
	 * 记录页面上trailer loader 和freight counted勾选状态
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public long editLabelTempDetail(HttpServletRequest request) throws Exception;
	
	/**
	 * 查询label_template_detail表信息
	 * @param entryId
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow findLabelTempDetail(String entryId,String loadNo) throws Exception;
	

	
    
    /**
	  * 得到需要的dn
	  */
   public DBRow[] getDnNameBySearchInfo(HttpServletRequest request, HttpServletResponse response,String ...customerIds) throws Exception;
   
   public DBRow[] getDnNameBySearchInfo(HttpServletRequest request, HttpServletResponse response) throws Exception;
   
   
   public DBRow[] getSearchPdfDnJSON(String dn) throws Exception ;
   public DBRow[] getSearchPdfDnJSON(String dn,String ...customerIds) throws Exception ;
   /**
    * 所有Carrier信息
    */
   public DBRow[] getAllCarrier() throws Exception;
   /**
    * 当天gate check in的生成pdf的load no
    */
   public DBRow[] getSearchPdfLoadJSON(String load_no) throws Exception;
   
   public DBRow[] getSearchPdfLoadJSON(String load_no,String ...customerIds) throws Exception;
   /**
    * masterBol和bol标签，web端打印时，根据entryId和detailId生成pdf使用 。gql 2015/04/24
    */
   public DBRow getDetailByEntryId(long entry_id,String load_no) throws Exception;



   public void downLoadPdfZip(HttpServletRequest request, HttpServletResponse response) throws Exception;

  
   /**
    * print 调用pdf服务生成pdf
    */
   public void createPdfByPdfServer(HttpServletRequest request, HttpServletResponse response) throws Exception;

   public DBRow[] getMissedPdfList(HttpServletRequest request, HttpServletResponse response) throws Exception;
   
   public DBRow[] getMissedPdfStat(HttpServletRequest request,String ... customerIds) throws Exception;
   
   public DBRow[] getCheckInfo(String start_time, String end_time, String company_id,Long ps_id) throws Exception ;
   
   public DBRow[] getMissedPdfList(String startTime,String endTime) throws Exception;
   
}
