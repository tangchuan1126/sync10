package com.cwc.app.iface.gql;

import com.fr.base.core.json.JSONArray;
import com.fr.base.core.json.JSONObject;

public interface BOLCombinationPrintIfaceGql {
	 
	//return bol and masterBol Combination Print Message
	public JSONArray bolCombinationPrintMessage(JSONArray jsons, String out_seal, long entry_id, long adid, String printType, String pageName)throws Exception;
	
	//return load Combination Print Message
	public JSONObject loadCombinationPrintMessage(String loadNo, String orderNo, String companyId, String customerId, long adid, String window_check_in_time, String printType)throws Exception;
		
	//return conting sheet Combination Print Message
	public JSONArray countingSheetMessage(JSONArray jsons,long entry_id, long adid, String printType)throws Exception;		

	public JSONArray packingMessage(JSONArray jsons, long adid, String printType)throws Exception;
	
	public JSONObject cargoTrackingNoteTicketMessage(String door_name, long entryId, long adid, String bolNo, String ctnNo, String companyId, String customerId, String printType)throws Exception;
		
	public JSONObject gateCheckMessage(long infoId, long psId, String printType)throws Exception;
		
}
