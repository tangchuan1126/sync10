package com.cwc.app.iface.gql;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.json.JsonObject;

public interface CustomSeachConditionsIfaceGql {
	
	//添加数据到商品详细模版表
	public void addProductDetailLable(HttpServletRequest request)throws Exception;
	
	//查询商品模版详细表
	public DBRow getLableDetail(DBRow row,long pc_id,long title_id)throws Exception;
	
	//获取 商品列表
	public DBRow[] getProductList(String title_id,String product_catclog,String product_line,PageCtrl pc,HttpServletRequest request,int productType)throws Exception;
	
	//获取 商品线列表
	public JSONObject getProductLineList(String title_id,HttpServletRequest request)throws Exception;
	
	//根据产品线 查询1级分类
	public JSONObject  getOneCatalog(String product_line_id,String title_id,HttpServletRequest request)throws Exception;
	
	//根据1级分类查询2级
	public JSONObject getTwoCatalog(String oneCatalog,String title_id,HttpServletRequest request)throws Exception;
	
	//根据2级分类查询三级
	public JSONObject getThreeCatalog(String twoCatalog,String title_id,HttpServletRequest request)throws Exception;
	
	//单个商品上添加模版
    public long addSingleDetailLable(HttpServletRequest request)throws Exception;
    
    //根据 详细模版id 查询是否 有多商品 使用该标签
  	public DBRow getCountDetailLableTemplate(long lable_template_detail_id)throws Exception;
	
    //删除 商品与模版关系
  	public long detLableTemplateRelation(long relation_id,long detail_lable_id)throws Exception;
  	
    //查询仓库条件
    public Map<String, Object> getWarehouseData()throws Exception;
  	
    //获得商品 类型 查询条件
    public Map<String, Object> getProductType()throws Exception;
  	
    //修改模版详细
    public long updateDetailLableTemplate(DBRow row)throws Exception;
   
    //查处商品的 商品标签
    public DBRow[] getLableLableTemplateByType(long pc_id,long login_id,long lable_template_type,long detail_type,HttpServletRequest request)throws Exception;
    
   //根据商品名索引查询商品 
  	public DBRow[] getProductByName(HttpServletRequest request,PageCtrl pc)throws Exception;
  	
   //根据模版名字查询模版 删除用
  	public DBRow[] getLableByName(HttpServletRequest request)throws Exception;
  	
    //批量删除商品模版
  	public void batchDelete(HttpServletRequest request)throws Exception;
  	
    //根据详细模版id查询 模版信息
  	public DBRow getLableTemplateById(long detail_lable_id)throws Exception;
  	
	//添加商品是，取基础模板第一个商品标签和clp标签，建立商品和标签模板的关系
	public void addLableTempByPcId(long pc_id,long title_id) throws Exception;
	
	//商品列表，item Lable 按钮打印标签用
	public DBRow[] getLableTemp(long labelType) throws Exception;
	
}
