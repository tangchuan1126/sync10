package com.cwc.app.iface.gql;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.db.DBRow;


public interface CreatePdfFileByServerIfaceGql {
	/**
	 * 将HTML传递给前台页面
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void sendHtml(DBRow bean) throws Exception;
	/**
	 * 处理htmltopdf
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public void handleHtmlToPdf(HttpServletRequest request,HttpServletResponse response) throws Exception;
}
