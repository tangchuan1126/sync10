package com.cwc.app.iface.gql;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ReturnOrderMgrIfaceGql {
	
	public DBRow[] getReturnOrdet(PageCtrl pc)throws Exception;//查询主单列表，带分页
	public DBRow[] getReturnOrdetsByPara(String receive_from,String receive_category,String input_st_date,String input_en_date,PageCtrl pc)throws Exception;//根据查询条件查询主单列表
	public DBRow[] getReturnOrdersById(long id,PageCtrl pc)throws Exception;//根据receiveNo查询主单列表
	public DBRow returnOrderById(long id)throws Exception;//根据id查询return主单信息
	public DBRow addReturnOrder(HttpServletRequest request) throws Exception;//新增return主单
	public void updateReturnOrderById(HttpServletRequest request) throws Exception;//根据receiver_id修改主单
	public DBRow[] addReturnPallet(long pallet_view_id,long logingUserId,int count,String time) throws Exception;//根据pallet_count或者package_count添加ReturnPallet
	
	
}
