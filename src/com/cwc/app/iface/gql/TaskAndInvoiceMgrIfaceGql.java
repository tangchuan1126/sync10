package com.cwc.app.iface.gql;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.db.DBRow;

public interface TaskAndInvoiceMgrIfaceGql {
	
	/**
	 * 添加schedule任务
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void addAndUpdateSchedule(HttpServletRequest request,HttpServletResponse response) throws Exception;
	/**
	 * 删除schedule任务
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void deleteScheduleByScheduleId(HttpServletRequest request,HttpServletResponse response) throws Exception;
	/**
	 * 添加账单
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void addAndUpdateInvoice(HttpServletRequest request, HttpServletResponse response) throws Exception;
	/**
	 * 查询task
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void getTaskAndDetail(HttpServletRequest request, HttpServletResponse response) throws Exception;
	/**
	 * 查询invoice
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void getInvoiceAndDetail(HttpServletRequest request, HttpServletResponse response) throws Exception;
	/**
	 * 删除invoice
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void deleteInvoiceByInvoiceId(HttpServletRequest request, HttpServletResponse response) throws Exception;
	/**
	 * 得到账户信息
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void getAccountInfoList(HttpServletRequest request, HttpServletResponse response) throws Exception;
	/**
	 * 得到country
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void getAllCountryCode(HttpServletRequest request, HttpServletResponse response) throws Exception;
	/**
	 * 得到country下的state
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void getStorageProvinceByCcid(HttpServletRequest request, HttpServletResponse response) throws Exception;
	/**
	 * 得到admin信息
	 */
	public void getAdminInformation(HttpServletRequest request, HttpServletResponse response) throws Exception;
	/**
	 * 得到configvalue
	 */
    public void getConfigValue(HttpServletRequest request, HttpServletResponse response) throws Exception;
    /**
	 * 根据client_id得到客户信息
	 */
    public void getAddressInfoListByClientId(HttpServletRequest request, HttpServletResponse response) throws Exception;
    /**
  	  * 根据客户关键字搜索   索引方式
  	  * @param 是否搜索schedule  是 schedule 否 invoice 
  	  */
    public void getSearchResultByIndex(HttpServletRequest request, HttpServletResponse response,boolean isScheduleSearch) throws Exception;
    /**
 	  * 得到用户的所有信息
 	  */
    public void getAllUserInformationAction(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 所有customer信息
    */
   public void getAllCustomerId(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 生成pdf invoice_id
    */
   public void exportPdf(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 对子任务排序
    */
   public void sortSubScheduleNumber(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 关闭任务
    */
   public void closeTask(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 关闭invoice
    */
   public void closeInvoice(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 增加log日志
    */
   public void addLogToSchedule(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 根据主任务Id查询子任务
    */
   public void getSubTaskByMainId(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 根据warehouseid及groupid查询所有的用户
    */
   public void getUserByWarehouseIdAndGroupId(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 生成invoice pdf
    */
   public void createPdfOfInvoice(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * assign main task
    */
   public void assignMainTask(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * get project by search_param
    */
   public void getProjectBySearchParam(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /*android 开始*/
   /**
    * 根据adid查询project
    */
   public DBRow[] androidGetProjectByAdid(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 根据schedule_id查询maintask的详细信息
    */
   public DBRow androidMainTaskByScheduleId(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 根据schedule_id查询maintask的按时间分组
    */
   public DBRow androidGetMainTaskGroupByTime(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 根据mainTaskId查询子任务
    */
   public DBRow androidGetSubTaskInformationByMainId(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * assign subtask
    */
   public void androidAssignSubTask(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * notice special task
    */
   public DBRow[] androidGetNoticeSpecialTask(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * close sub task
    */
   public DBRow androidCloseSubTask(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * invoice open 邮件通知
    */
   public void assignInvoice(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 批量删除
    */
   public void androidBatchDelTask(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 批量增加
    */
   public DBRow[] androidBatchAddTask(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /**
    * 修改
    */
   public void androidUpdateTask(HttpServletRequest request, HttpServletResponse response) throws Exception;
   /*android 结束*/
}
