package com.cwc.app.iface.zl;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ContainerMgrIFaceZYZ {
	
	public DBRow[] getAllContainer(PageCtrl pc) throws Exception; //查询所有容器托盘
	
	public DBRow getDetailContainer(long containerId) throws Exception;//查询一个容器托盘的详细信息
	
	public DBRow[] getSearchContainer(String key,  long type,int container_type  , PageCtrl pc)throws Exception;//高级查询
	
	public long addContainer(HttpServletRequest request) throws Exception; //添加容器托盘信息
	
	public void modContainer(HttpServletRequest request) throws Exception; //修改容器托盘信息
	
	public void deleteContainer(long containerId) throws Exception; //删除容器托盘信息
		
	public DBRow[] getAllContainerType(PageCtrl pc, String searchKey) throws Exception; //查询所有容器托盘类型
	
	public DBRow getDetailContainerType(long typeId) throws Exception; //查询单个容器托盘类型
	
	public void modContainerType(HttpServletRequest request)throws Exception; //修改类型
	
	public long addContainerType(HttpServletRequest request)throws Exception; //增加类型
	
	public long deleteContainerType(long typeId, int container_type) throws Exception; //删除容器托盘类型
	
	public String exportContainer(HttpServletRequest request)throws Exception; //导出容器
		
	public HashMap<String, DBRow[]> excelShow(String filename,String type)throws Exception; //导入容器
	
	public void uploadContainers(HttpServletRequest request)throws Exception; //保存导入容器
	
	public DBRow[] findContainerTypeByContainerType(int containerType) throws Exception;
	
	public DBRow[] getClpContainerType() throws Exception;
	
	public DBRow[] getTlpContainerType() throws Exception;
}
