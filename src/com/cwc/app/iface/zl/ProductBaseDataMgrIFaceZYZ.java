package com.cwc.app.iface.zl;

import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.exception.bcs.XMLDataErrorException;
import com.cwc.app.exception.product.ProductCodeIsExistException;
import com.cwc.app.exception.product.ProductNameIsExistException;

public interface ProductBaseDataMgrIFaceZYZ {
	/**XML创建基础数据接口
	 * xml
	 * @param xml
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public long addProductXML(String xml,AdminLoginBean adminLoggerBean)//添加商品基础数据接口
	throws ProductNameIsExistException, ProductCodeIsExistException ,XMLDataErrorException ,NetWorkException,   Exception;
	
	public String getSampleNode(String xml,String name) //获得节点数据（只适合唯一名称节点）
	throws Exception ;
	
	public void addProductUnionXML(String xml,AdminLoginBean adminLoggerBean) //创建组合关系  
	throws Exception;
	
	public void delProductXML(String xml)throws Exception; //删除商品
	
	public  void delProductUnionXML(String xml,AdminLoginBean adminLoggerBean) //删除商品套装关系
	throws Exception;
	
	public void modProductCatalogXML(String xml,AdminLoginBean adminLoggerBean) //修改商品类型
	throws Exception;
	
	public void modProductXML(String xml, AdminLoginBean adminLoggerBean)  //修改商品
	throws Exception;
	
	public void modProductUnionXML(String xml, AdminLoginBean adminLoggerBean)  //修改商品套装
	throws Exception;
	
	public int addProductCodeXML(String xml, AdminLoginBean adminLoggerBean)  //添加商品条码
	throws Exception;
	
	public void handleUploadProductFile(String file_with_id, String file_with_type,String file_with_class, //上传商品图片
	String upload_adid,String upload_time, String path , List<FileItem> fileItems) throws Exception;
	
	public void delProductFile(String xml)throws Exception; //删除商品图片 
	public int addProductCodeXMLForCheck(String xml, AdminLoginBean adminLoggerBean) throws Exception;//条码类型及名查询条码
	
	public int addProductCodeForCheck(String pc_id_str, String p_code, int code_type) throws Exception ;
	
 	public long gridAddProductSub(String p_name, String p_code,
			long catalog_id, String unit_name, double unit_price, float length,
			float width, float heigth, float weight, int sn_size,
			AdminLoginBean adminLoggerBean) throws ProductCodeIsExistException,ProductNameIsExistException,Exception ;
 	
 	public void saveProductCodeByXml(String xml, long pc_id, long adid) throws Exception ;
}
