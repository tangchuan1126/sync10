package com.cwc.app.iface.zl;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface StorageDoorLocationIFaceZYZ {
	
	public DBRow[] getAllStorageDoor(PageCtrl pc) throws Exception; //获取所有卸货门
	
	public DBRow[] getSearchStorageDoor(String doorId,long pcId,PageCtrl pc)throws Exception;//卸货门高级查询
	
	public DBRow getDetailStorageDoor(long sdId)throws Exception; //获取某个装卸门信息
	
	public long addStorageDoor(HttpServletRequest request)throws Exception;//添加卸货门
	
	public void modStorageDoor(HttpServletRequest request) throws Exception; //修改装卸门信息
	
	public void deleteStorageDoor(long sdId) throws Exception; //删除装卸门
	
	public DBRow[] getAllLoadUnloadLocation(PageCtrl pc) throws Exception;//获取所有装卸位置
	
	public DBRow[] getSearchLoadUnloadLocation(String locationName,long pscId,PageCtrl pc)throws Exception;//高级查询
	
	public DBRow getDetailLoadUnloadLocation(long locationId) throws Exception;//获取某个装卸位置
	
	public long addLoadUnloadLoaction(HttpServletRequest request) throws Exception;//添加装卸位置
	
	public void modLoadUnloadLoaction(HttpServletRequest request)throws Exception;//修改装卸位置
	
	public void deleteLoadUnloadLocation(long locationId)throws Exception;//删除装卸位置
	
	public DBRow[] getStorageDoorByPsid(long psid) throws Exception; //根据仓库ID获取装卸门信息
	
	public DBRow[] getLoadUnloadLocationByPsid(long psid) throws Exception; //根据仓库ID获取装卸位置信息

}
