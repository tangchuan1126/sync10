package com.cwc.app.iface.zl;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface CodeMissMgrIFaceZYZ {
	
	public DBRow[] getAllCodeMiss(PageCtrl pc) throws Exception; //获取所有基础数据信息（带分页）
	
	public DBRow[] getItemNumber(String miss_code) throws Exception; //根据miss_code获取ItemNumber
	
	public DBRow[] getMissID(String miss_code) throws Exception; //根据miss_code获取Miss_id
	

}
