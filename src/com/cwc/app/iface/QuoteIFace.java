package com.cwc.app.iface;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.beans.quote.CustomChangeProductsBean;
import com.cwc.app.beans.retailprice.RetailPriceQuoteBean;
import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.exception.purchase.FileTypeException;
import com.cwc.app.exception.quote.DuplicateOrderException;
import com.cwc.app.exception.quote.ErrorWholeSellDiscountRuleException;
import com.cwc.app.exception.quote.OrderNotFoundException;
import com.cwc.app.exception.quote.OrderNotWait4RecordException;
import com.cwc.app.exception.quote.OrderSourceErrorException;
import com.cwc.app.exception.quote.TotalMcGrossErrorException;
import com.cwc.app.exception.quote.WareHouseErrorException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.service.order.action.OrderCostBean;

public interface QuoteIFace 
{
	public DBRow[] getAllQuotes(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getQuoteItemsByQoid(long qoid,PageCtrl pc)
	throws Exception;
	
	public DBRow getDetailQuoteByQoid(long qoid)
	throws Exception;
	
	public long createQuote(HttpServletRequest request)
	throws Exception;
	
	public int saveQuoteProduct(HttpServletRequest request)
	throws Exception;
	
	public void initQuoteCartFromOrder(HttpServletRequest request,long qoid)
	throws Exception;
	
	public OrderCostBean getQuoteOrderCost(long oid,long sc_id,long ccid)
	throws WeightOutSizeException,WeightCrossException,CountryOutSizeException,Exception;
	
	public double getLowestProductPrice(long pid,long country,double discount)
	throws Exception;
	
	public void modQuote(HttpServletRequest request)
	throws Exception;
	
	public DBRow getDetailQuoteItemByQoidPid(long qoid,long pid)
	throws Exception;
	
	public DBRow[] getWholeSellDiscounts(PageCtrl pc)
	throws Exception;
	
	public DBRow getDetailWholeSellDiscountByWsdId(long wsd_id)
	throws Exception;
	
	public long addWholeSellDiscount(HttpServletRequest request)
	throws Exception;
	
	public void modWholeSellDiscountByWsdId(HttpServletRequest request)
	throws Exception;
	
	public void delWholeSellDiscountByWsdId(HttpServletRequest request)
	throws Exception;
	
	public DBRow getDetailWholeSellDiscountByName(String name)
	throws Exception;
	
	public DBRow getDetailWholeSellDiscountByImpClass(String imp_class)
	throws Exception;
	
	public void validateDiscountRule(String rule[][])
	throws ErrorWholeSellDiscountRuleException, Exception;
	
	public double getWholeSellDiscount(String buy_catalog,String client_id,double mc_gross)
	throws Exception;
	
//	public double getFinalProductPrice(double gross_profit,double unit_price,double discount,double manager_discount)
//	throws Exception;
	
	public void finishQuote(HttpServletRequest request)
	throws OrderNotWait4RecordException,OrderSourceErrorException,DuplicateOrderException,TotalMcGrossErrorException,OrderNotFoundException,Exception;
	
	public void modFinishQuote(HttpServletRequest request)
	throws OrderNotWait4RecordException,OrderSourceErrorException,DuplicateOrderException,TotalMcGrossErrorException,OrderNotFoundException,Exception;
	
	public void approveQuote(HttpServletRequest request)
	throws Exception;
	
	public void cancelQuote(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getSearchResult(String key,String st,String en,String create_account,int quote_status,int field,PageCtrl pc)
	throws Exception;
	
	public DBRow getQuotePDFDatas(long qoid)
	throws Exception;
	
	public void sendQuotationViaEmail(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getProductCountryProfitByPid(long pid,PageCtrl pc)
	throws Exception;
	
	public String exportProfitQuote(HttpServletRequest request) 
	throws WareHouseErrorException,Exception;
	
	public String importProductProfit(HttpServletRequest request)
	throws Exception,FileTypeException;
	
	public DBRow[] checkExcel(String filename)
	throws Exception;
	
	public boolean rightRetailPrice(double rate,double price,double shipping_fee,double retail_price);
	
	public RetailPriceQuoteBean getChangeRetailPriceProducts(String filename)
	throws Exception;
	
	public long addRetailPriceChange(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getAllRetailPrice(PageCtrl pc)
	throws Exception;
	
	public DBRow[] getRetailPriceItemsByRpId(long rp_id,PageCtrl pc)
	throws Exception;
	
	public void updateProductProfit(HttpSession ses,long rp_id)
	throws Exception;
	
	public DBRow getDetailProductProfitByPidCcid(long pid,long ccid)
	throws Exception;
	
	public DBRow getDetailRetailPriceByRpId(long rp_id)
	throws Exception;
	
	public void rejectChangeRetailProductPrice(long rp_id,String account,String reject_reasons)
	throws Exception;
	
	public CustomChangeProductsBean getCustomChangeProductsFromCart(HttpSession session,long new_custom_pid)
	throws Exception;
	
	public double getCustomProductPrice(HttpSession session,long custom_pid,long ccid,double discount)
	throws Exception;
	
	public int getQuoteItemCountByQoid(long qoid)
	throws Exception;
	
	public DBRow[] getNotProductProfitInSetBySetIdCcid(long set_id,long ccid,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getServiceQuoteDiscountRange(long adgid)
	throws Exception;
	
	public void batchModServiceQuoteDiscountRange(HttpServletRequest request)
	throws Exception;
	
	public DBRow getDetailServiceQuoteDiscountRangeByAdid(long adid)
	throws Exception;
	
	public float[] calculateManagerDiscounts(float systemDiscount,float discountRate);
	
	public float[] calculateManagerDiscounts(HttpSession session,float systemDiscount)
	throws Exception;
	
	public String[][] getWholeSellRule2(String ruleStr)
	throws Exception;
	
	public String[][] getWholeSellRule3(String ruleStr)
	throws Exception;
	
	public DBRow[] getCountryAreaByPid(long pid,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getCountryByPidCaid(long pid,long ca_id,PageCtrl pc)
	throws Exception;
	
	/**
	 * 总经理同意价格审批
	 * @param request
	 * @throws Exception
	 */
	public void agreeRetailPrice(HttpServletRequest  request) throws Exception ;
	
	/**
	 * 总经理不同意价格审批
	 * @param request
	 * @throws Exception
	 */
	public void notAgreeRetialPrice(HttpServletRequest request) throws Exception ;

}





