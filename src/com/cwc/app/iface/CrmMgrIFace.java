package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;



public interface CrmMgrIFace 
{
	public void synchronizeClients()
	throws Exception;
	
	public DBRow[] getFrequentlyReadyClients(long adid,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getFrequentlyMustTraceClients(long adid,int period,int target_type,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getTraceByCcid(long ccid,PageCtrl pc)
	throws Exception;
	
	public long addTrace(HttpServletRequest request)
	throws Exception;
	
	public void addSpecialClients(HttpServletRequest request)
	throws Exception;
	
	public DBRow[] getSpecialClients(long adid,int period,PageCtrl pc)
	throws Exception;
	
	public void delSpecialClientByCscId(HttpServletRequest request)
	throws Exception;
	
	public long addTraceSub(int target_type,int cc_id,int trace_type,String trace_date,String memo,long adid,long qoid)
	throws Exception;
	
	public long addTraceSub(int target_type,int cc_id,int trace_type,String trace_date,String memo,long adid)
	throws Exception;
	
	public void updateClientByCcid(long cc_id,String trace_date,int target_type)
	throws Exception;
	
	public void delClientByCcId(HttpServletRequest request)
	throws Exception;
}


