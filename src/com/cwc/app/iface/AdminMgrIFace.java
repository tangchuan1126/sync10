package com.cwc.app.iface;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import us.monoid.json.JSONArray;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.admin.AccountNotPermitLoginException;
import com.cwc.app.exception.admin.AccountOrPwdIncorrectException;
import com.cwc.app.exception.admin.AdminIsExistException;
import com.cwc.app.exception.admin.OldPwdIncorrectException;
import com.cwc.app.exception.admin.RoleIsExistException;
import com.cwc.app.exception.admin.SuperAdminException;
import com.cwc.app.exception.admin.SuperRoleException;
import com.cwc.app.exception.admin.TokenCheckNotPassException;
import com.cwc.app.exception.admin.VerifyCodeIncorrectException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface AdminMgrIFace {
	public void addAdmin(HttpServletRequest request) throws AdminIsExistException, Exception;
	
	public void delAdmin(HttpServletRequest request) throws SuperAdminException, Exception;
	
	public void lockAdmin(HttpServletRequest request) throws SuperAdminException, Exception;
	
	public DBRow getDetailAdmin(long adid) throws Exception;
	
	public DBRow[] getAllAdmin(PageCtrl pc) throws Exception;
	
	public void unLockAdmin(HttpServletRequest request) throws Exception;
	
	public DBRow[] getAllAdminGroup(PageCtrl pc) throws Exception;
	
	public DBRow getDetailAdminGroup(long adgid) throws Exception;
	
	public DBRow getDetailAdminByAccount(String account) throws Exception;
	
	public void modAdminPwd(HttpServletRequest request) throws OldPwdIncorrectException, Exception;
	
	public void adminLogin(HttpServletRequest request, HttpServletResponse response)
			throws AccountOrPwdIncorrectException, VerifyCodeIncorrectException, AccountNotPermitLoginException,
			TokenCheckNotPassException, AdminIsExistException, Exception;
	
	public void adminLogin(HttpServletRequest request, HttpServletResponse response, String account, String pwd,
			String licence, int remember, int loginType, DBRow attach) throws AccountOrPwdIncorrectException,
			VerifyCodeIncorrectException, AccountNotPermitLoginException, Exception;
	
	public void adminLogout(HttpServletRequest request) throws Exception;
	
	public AdminLoginBean getAdminLoginBean(HttpSession ses);
	
	public void markLocal(HttpServletResponse response, HttpServletRequest request) throws Exception;
	
	public void isRightAdminPath(HttpServletResponse response, HttpServletRequest request) throws Exception;
	
	public DBRow[] getControlTreeByParentid(long parentid) throws Exception;
	
	public DBRow getDetailControlTreeByLink(String link) throws Exception;
	
	public DBRow[] getControlTree() throws Exception;
	
	public ArrayList getPermitPageAdgid(String page) throws Exception;
	
	public ArrayList getPermitActionAdgid(String action) throws Exception;
	
	public void modifyRole(HttpServletRequest request) throws RoleIsExistException, Exception;
	
	public void grantRolePageActionRights(HttpServletRequest request) throws SuperRoleException, Exception;
	
	public void delRole(HttpServletRequest request) throws SuperRoleException, Exception;
	
	public void addRole(HttpServletRequest request) throws RoleIsExistException, Exception;
	
	public DBRow getDetailRoleByName(String name) throws Exception;
	
	public DBRow getDetailRoleByAdgid(long adgid) throws Exception;
	
	public DBRow[] getActionsByPage(PageCtrl pc, long id) throws Exception;
	
	public ArrayList getPermitActionAdid(String action) throws Exception;
	
	public void grantAdminPageActionRights(HttpServletRequest request) throws SuperRoleException, Exception;
	
	public DBRow[] getControlTreeByParentidOrderBySort(long parentid, int control_type) throws Exception;
	
	public DBRow[] getAdminByAdgid(long adgid, PageCtrl pc) throws Exception;
	
	public void modAdmin(HttpServletRequest request) throws Exception;
	
	public DBRow getDetailControlTreeById(long id) throws Exception;
	
	public void adminLogin(int loginType, HttpServletRequest request, HttpServletResponse response)
			throws AccountOrPwdIncorrectException, VerifyCodeIncorrectException, AccountNotPermitLoginException,
			AdminIsExistException, TokenCheckNotPassException, Exception;
	
	public DBRow[] getAccountByDeptId(long adgid) throws Exception;
	
	/**
	 * zyj 索引搜索账号
	 * 
	 * @param key
	 * @param search_mode
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchAdmins(String key, int search_mode, PageCtrl pc) throws Exception;
	
	/**
	 * 左侧树JSON格式
	 * 
	 * @return
	 * @throws Exception
	 */
	public JSONArray getLeftTree() throws Exception;
	
	/**
	 * 获得当前登录者的访问权限
	 * 
	 * @param adid
	 * @param control_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminControlTreeByAdid(long adid, int control_type) throws Exception;
	
	public DBRow[] getControlTreeAllByControlTypeSort(int control_type) throws Exception;
	
	/**
	 * 根据仓库id和分组id 查询人员列表 zwb
	 * 
	 * @param ps_id
	 * @param group_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminsByPsIdAndGId(long ps_id, long group_id) throws Exception;
	
	/**
	 * 根据仓库ID和分组ID，返回人员ids和names
	 * 
	 * @param ps_id
	 * @param group_id
	 * @return DBRow中有adids和adnames
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月8日 下午12:10:07
	 */
	public DBRow getAdminIdsNamesByPsIdAndGId(long ps_id, long group_id) throws Exception;
	
	public DBRow[] getAdminsEmployeName(String employe_name) throws Exception;
	
}
