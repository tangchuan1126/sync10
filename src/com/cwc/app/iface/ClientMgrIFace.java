package com.cwc.app.iface;

import javax.servlet.http.HttpServletRequest;

import com.cwc.app.exception.client.MarkOrderIsExistException;
import com.cwc.app.exception.client.OrderNotExistException;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;


public interface ClientMgrIFace 
{
	public void addClients(HttpServletRequest request)	throws Exception;
	
	public DBRow[] getClients(PageCtrl pc)	throws Exception;
	
	public DBRow[] getSearchClients(String value,PageCtrl pc)	throws Exception;
	
	public void modClient(HttpServletRequest request)	throws Exception;
	
	public DBRow[] getClientTraceByCount(long cid,int c)	throws Exception;
	
	public void addClientTrace(HttpServletRequest request)	throws OrderNotExistException,MarkOrderIsExistException,Exception;
	
	public int getClientTraceCountByCid(long cid)	throws Exception;
	
	public DBRow[] getClientTracesByCid(long cid,PageCtrl pc)	throws Exception;
	
	public DBRow getDetailClientByCid(long cid)	throws Exception;
	
	public DBRow[] getClientsSortBySumMcGross(String st,String en,String account,PageCtrl pc)	throws Exception;
	
	public DBRow[] getClientsSortBySumMcGrossDown(String st,String en,String account,PageCtrl pc)	throws Exception;
	
	public DBRow[] getClientsSortByModDate(String st,String en,String account,PageCtrl pc)	throws Exception;
	
	public DBRow[] getClientsSortByModDateDown(String st,String en,String account,PageCtrl pc)	throws Exception;
	
	public DBRow[] getClientsByCidRange(long st_cid,long en_cid,PageCtrl pc)	throws Exception;
	
	public void addAsk(HttpServletRequest request)	throws Exception;
	
	public DBRow[] getSearchServiceByFilter(String inService[],String st,String en,String account,PageCtrl pc)	throws Exception;
	
	public int getSearchClientResultCount(HttpServletRequest request)	throws Exception;
	
	public void delClientByCid(HttpServletRequest request)	throws Exception;
	
	public DBRow getClientsServiceStat(String st,String en,String account,int clients_service)	throws Exception;
	
	public DBRow[] getClientsServiceStatDate(String st,String en,String account)	throws Exception;
	
	public double getClientsServiceOrderMcGrossStat(String date,String account)	throws Exception;
	
	
	
}
