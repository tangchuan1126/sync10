package com.cwc.app.epacket;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;
import org.apache.tools.ant.taskdefs.Sleep;

import com.cwc.app.api.Cart;
import com.cwc.app.exception.bcs.NetWorkException;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.cwc.http.Client;
import com.cwc.util.StringUtil;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class EPacketClient {
	static Logger log = Logger.getLogger("ACTION");
	
	
	private String server = "http://www.ems.com.cn/partner/api/public/p/order";
	private String geturl = "http://www.ems.com.cn/partner/api/public/p/order/";
	private String saveFilePath = Environment.getHome()+"administrator/order/EPacket/";
	
	private long orderid;//要求的orderid，使用我方内部其中的一个oid
	private String customercode = "f8bb3c37f7f03335919b010bc3c5d5e4";	//账号
	private int clcttype = 1;	//1上门揽货，0用户自送
	private boolean pod = false;	//电子签收
	private String untread = "Returned";		//退回类型
	private int volweight;		//体积重
	private String startdate;		//揽货起始预约时间
	private String enddate;		//揽货终止预约时间
	private String remark;		//备注
	private String printcode = "03";	//标签类型，00标识A4，01标识4X4标签，03标识4X6标签
	
	//发货地址信息
	private String sendName;		//姓名
	private String sendPostCode;	//邮编
	private String sendPhone;		//电话
	private String sendMobile;		//移动电话
	private String sendCountry = "CN";	//发货国
	private String sendProvince;		//发货省份，需要E邮宝自己的省份缩略码
	private String sendCity;			//发货城市，需要E邮宝自己的城市缩略码
	private String sendCounty;			//发货区县，需要E邮宝自己的区域缩略码
	private String sendCompany = "Visionari";	//发货公司
	private String sendStreet;			//发货街道
	private String sendEmail = "";			//发货人电子邮箱，可为空
	
	//收货地址
	private String receiverName;	//收件姓名
	private String receiverPostCode;	//收件邮编
	private String receiverPhone = "";	//收件人电话，可空
	private String receiverMobile = "";	//收件人手机，可空
	private String receiverCountry = "UNITED STATES OF AMERICA";	//收件国家，固定美国
	private String receiverProvince;	//收件省份，看似省份缩写
	private String receiverCity;		//收件城市
	private String receiverCounty;		//收件区，将城市的值复制给他
	private String receiverStreet;		//收件街道
	private String receiverEmail = "";		//收件人邮件
	
	private String origin = "CN";	//货物明细需要，CN中国
	
	
	
	int timeout = 6000; //连接服务器超时时间
	
	private String errorMessage = "";
	
	
	public String comit(DBRow[] waybillProducts,double delcarevalue)
		throws Exception
	{
		String para = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		
		para += "<orders xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
		para += "<order>";
		para += "<orderid>"+this.orderid+"</orderid>";
		para += "<customercode>"+this.customercode+"</customercode>";
		para += "<clcttype>"+this.clcttype+"</clcttype>";
		para += "<pod>"+this.pod+"</pod>";
		para += "<untread>"+this.untread+"</untread>";
		para += "<volweight>"+this.volweight+"</volweight>";
		para += "<startdate>"+this.startdate+"</startdate>";
		para += "<enddate>"+this.enddate+"</enddate>";
		para += "<printcode>"+this.printcode+"</printcode>";
		
		para += "<sender>";
		para +=	"<name>"+this.sendName+"</name>";
		para += "<postcode>"+this.sendPostCode+"</postcode>";
		para += "<phone>"+this.sendPhone+"</phone>";
		
		para += "<country>"+this.sendCountry+"</country>";
		para += "<province>"+this.sendProvince+"</province>";
		para += "<city>"+this.sendCity+"</city>";
		para += "<county>"+this.sendCounty+"</county>";
		para += "<company>"+this.sendCompany+"</company>";
		para += "<street>"+this.sendStreet+"</street>";
		
		if(!this.sendEmail.equals(""))
		{
			para += "<email>"+this.sendEmail+"</email>";
		}
		para +=	"</sender>";
		
		para += "<collect>";
		para +=	"<name>"+this.sendName+"</name>";
		para += "<postcode>"+this.sendPostCode+"</postcode>";
		para += "<phone>"+this.sendPhone+"</phone>";
		para += "<mobile>"+this.sendMobile+"</mobile>";
		para += "<country>"+this.sendCountry+"</country>";
		para += "<province>"+this.sendProvince+"</province>";
		para += "<city>"+this.sendCity+"</city>";
		para += "<county>"+this.sendCounty+"</county>";
		para += "<company>"+this.sendCompany+"</company>";
		para += "<street>"+this.sendStreet+"</street>";
		para += "<email>"+this.sendEmail+"</email>";
		para +=	"</collect>";
		
		
		para += "<receiver>";
		para += "<name>"+this.receiverName+"</name>";
		para += "<postcode>"+this.receiverPostCode+"</postcode>";
		
		if(!this.receiverPhone.equals(""))
		{
			para += "<phone>"+this.receiverPhone+"</phone>";
		}
		
		if(!this.receiverMobile.equals(""))
		{
			para += "<mobile>"+this.receiverMobile+"</mobile>";
		}
		
		para += "<country>"+this.receiverCountry+"</country>";
		para += "<province>"+this.receiverProvince+"</province>";
		para += "<city>"+this.receiverCity+"</city>";
		para += "<county>"+this.receiverCounty+"</county>";
		para += "<street>"+this.receiverStreet+"</street>";
		
		if(!this.receiverEmail.equals(""))
		{
			para += "<email>"+this.receiverEmail+"</email>";
		}
		para += "</receiver>";
		
		para +="<items>";
		for (int i = 0; i < waybillProducts.length; i++)
		{
			para +="<item>";
			
			para += "<enname>Part#"+waybillProducts[i].getString("pc_id");
			if (waybillProducts[i].get("cart_product_type",0)==ProductTypeKey.UNION_STANDARD_MANUAL)
			{
				para +=" *";
			}
			para += "</enname>";
			para += "<count>"+(int)waybillProducts[i].get("cart_quantity",0f)+"</count>";
			para += "<unit>"+waybillProducts[i].getString("unit_name")+"</unit>";
			para += "<weight>"+waybillProducts[i].get("weight",0f)+"</weight>";
			para += "<delcarevalue>"+delcarevalue+"</delcarevalue>";
			para += "<origin>"+this.origin+"</origin>";
			
			para +="</item>";
		}
		para += "</items>";
		para += "<remark/>";
		para += "</order>";
		para += "</orders>";
	
		
		String responseXmlStr = Client.httpPostEPacket(server, para,timeout);
		
		String mailnum = "0";
		try 
		{
			mailnum = getSampleNode(responseXmlStr,"mailnum");
			
			if(!mailnum.equals(""))
			{
				String md5url = StringUtil.getMD5("vvme_f8bb3c37f7f03335919b010bc3c5d5e4"+mailnum).toLowerCase();
				
				String url = "http://labels.ems.com.cn/partner/api/public/p/static/label/download/"+md5url+"/"+mailnum+".pdf";
				
				log.error(url);
				
				Thread.sleep(5000);
				
				Client.httpGetDownLoad(url,saveFilePath+mailnum+".pdf");
			}
		}
		catch (NetWorkException e) 
		{
			try 
			{
				this.errorMessage = getSampleNode(responseXmlStr, "description");
			}
			catch (Exception e1) 
			{
				
			}
		}
		
		return (mailnum);
		
		
//		File file = new File("D:\\"+mailnum+".pdf");
//
//		RandomAccessFile raf = new RandomAccessFile(file, "r");
//
//		FileChannel channel = raf.getChannel();
//
//		MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
//
//		PDFFile pdffile = new PDFFile(buf);
//
//		for (int i = 1; i <= pdffile.getNumPages(); i++) 
//		{
//
//			PDFPage page = pdffile.getPage(i);
//
//			Rectangle rect = new Rectangle(0, 0, ((int) page.getBBox().getWidth()), ((int) page.getBBox().getHeight()));
//
//			Image img = page.getImage(rect.width, rect.height, rect,
//
//			null, // null for the ImageObserver
//
//			true, // fill background with white
//
//			true // block until drawing is done
//
//			);
//
//			BufferedImage tag = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);
//
//			tag.getGraphics().drawImage(img, 0, 0, rect.width, rect.height, null);
//
//			FileOutputStream out = new FileOutputStream("D:\\"+mailnum+".jpg"); // 输出到文件流
//
//			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
//
//			JPEGEncodeParam param2 = encoder.getDefaultJPEGEncodeParam(tag);
//
//			param2.setQuality(1f, false);// 1f是提高生成的图片质量
//
//			encoder.setJPEGEncodeParam(param2);
//
//			encoder.encode(tag); // JPEG编码
//
//			out.close();
//
//			}
//
//			channel.close();
//
//			raf.close();
//			unmap(buf);//如果要在转图片之后删除pdf，就必须要这个关闭流和清空缓冲的方法

		
		
		
//		  int pagen= 1;  
//		      File file = new File("D:\\"+mailnum+".pdf");  
//		    
//		       PDFFile pdffile=null;  
//		 //      set up the PDF reading  
//		      try{  
//		      RandomAccessFile raf = new RandomAccessFile(file, "r");  
//		      FileChannel channel = raf.getChannel();  
//		      ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());  
//		      pdffile = new PDFFile(buf);  
//		      }  
//		      catch(Exception e){}  
//		        
//		      if(pagen<pdffile.getNumPages())  
//		       return;  
//		      //print出该pdf文档的页数  
//		      //system.out.println(pdffile.getNumPages());  
//		        
//		      //设置将第pagen也生成png图片  
//		      PDFPage page = pdffile.getPage(pagen);  
//		 //      create and configure a graphics object  
//		      int width = (int)page.getBBox().getWidth();  
//		      int height =(int)page.getBBox().getHeight();  
//		      BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);  
//		      Graphics2D g2 = img.createGraphics();  
//		      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);  
//		 //      do the actual drawing  
//		      PDFRenderer renderer = new PDFRenderer(page, g2,  
//		          new Rectangle(0, 0, width, height), null, Color.WHITE);  
//		      try
//		      {  
//		         page.waitForFinish();  
//		      }
//		      catch(Exception e)
//		      {  
//		         e.printStackTrace();  
//		      }  
//		      
//		      renderer.run();  
//		      g2.dispose();  
//		        
//		      try
//		      {  
//		        ImageIO.write(img, "png", new File("D:/"+mailnum+".png"));  
//		      }  
//		      catch(Exception ex)
//		      {
//		    	  
//		      }  
		
		
//			PDDocument doc = PDDocument.load("D:\\"+mailnum+".pdf");
//	        int pageCount = doc.getPageCount(); 
//	        //system.out.println(pageCount); 
//	        List pages = doc.getDocumentCatalog().getAllPages(); 
//	        for(int i=0;i<pages.size();i++)
//	        {
//	            PDPage page = (PDPage)pages.get(i); 
//	            
//	            BufferedImage image = page.convertToImage(); 
//	            Iterator iter = ImageIO.getImageWritersBySuffix("jpg"); 
//	            ImageWriter writer = (ImageWriter)iter.next(); 
//	            File outFile = new File("D:/"+mailnum+"_"+i+".jpg"); 
//	            FileOutputStream out = new FileOutputStream(outFile); 
//	            ImageOutputStream outImage = ImageIO.createImageOutputStream(out); 
//	            writer.setOutput(outImage); 
//	            writer.write(new IIOImage(image,null,null));
//	        }
	}
	
	
	public String getSampleNode(String xml,String name)
		throws Exception
	{
		try 
		{
			String xmlSplit1[] = xml.split("<"+name+">");
			String xmlSplit2[] = xmlSplit1[1].split("</"+name+">");
	
			return(xmlSplit2[0]);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			throw new NetWorkException();
		}
		catch(Exception e)
		{
			throw new SystemException(e,"getSampleNode",log);
		}
	}
	
	public static void main(String[] args) 
	{
		String ser = "http://www.ems.com.cn/partner/api/public/p/area/cn/county/list/110100";
		
//		String server = "http://www.baidu.com";
		
		try 
		{
//			responseXmlStr = Client.httpGetEPacket(ser,60000);
			EPacketClient ePackClient = new EPacketClient();
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			DBRow waybillProduct = new DBRow();
			waybillProduct.add("p_name","BULB");
			waybillProduct.add("quantity",5);
			waybillProduct.add("unit_name","PAIR");
			waybillProduct.add("weight",5);
			
			list.add(waybillProduct);
			
			DBRow product = new DBRow();
			product.add("p_name","PROJECT");
			product.add("quantity",1);
			product.add("unit_name","SET");
			product.add("weight",9);
			
			list.add(product);
			
			ePackClient.setOrderid(490456);
			ePackClient.setVolweight(500);
			ePackClient.setStartdate("2012-07-04T14:00:00");
			
			ePackClient.setEnddate("2012-07-04T16:00:00");
			
			//发件人地址
			ePackClient.setSendName("zhanjie");
			ePackClient.setSendPostCode("101100");
			ePackClient.setSendPhone("010-84827066");
			ePackClient.setSendMobile("1590106824");
			ePackClient.setSendProvince("110000");
			ePackClient.setSendCity("110100");
			ePackClient.setSendCounty("111149");
			ePackClient.setSendStreet("Huiminhutong24");
			ePackClient.setSendEmail("zhanjie19861128@126.com");
			
			//收件人地址
			ePackClient.setReceiverName("feihan");
			ePackClient.setReceiverPostCode("80012");
			ePackClient.setReceiverProvince("CO");
			ePackClient.setReceiverCity("Aurora");
			ePackClient.setReceiverStreet("234 S Troy Circle");
			
//			ePackClient.comit(list.toArray(new DBRow[0]),5.5);
			
			String mailnum = "LN011992134CN";
			String md5url = StringUtil.getMD5("vvme_f8bb3c37f7f03335919b010bc3c5d5e4"+mailnum).toLowerCase();
			
			String url = "http://labels.ems.com.cn/partner/api/public/p/static/label/download/"+md5url+"/"+mailnum+".pdf";
			
			////system.out.println(url);
		}
//		catch (SocketTimeoutException e) 
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public long getOrderid() {
		return orderid;
	}

	public void setOrderid(long orderid) {
		this.orderid = orderid;
	}

	public int getClcttype() {
		return clcttype;
	}

	public void setClcttype(int clcttype) {
		this.clcttype = clcttype;
	}

	public boolean isPod() {
		return pod;
	}

	public void setPod(boolean pod) {
		this.pod = pod;
	}

	public String getUntread() {
		return untread;
	}

	public void setUntread(String untread) {
		this.untread = untread;
	}

	public int getVolweight() {
		return volweight;
	}

	public void setVolweight(int volweight) {
		this.volweight = volweight;
	}


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPrintcode() {
		return printcode;
	}

	public void setPrintcode(String printcode) {
		this.printcode = printcode;
	}

	public String getSendName() {
		return sendName;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	public String getSendPostCode() {
		return sendPostCode;
	}

	public void setSendPostCode(String sendPostCode) {
		this.sendPostCode = sendPostCode;
	}

	public String getSendPhone() {
		return sendPhone;
	}

	public void setSendPhone(String sendPhone) {
		this.sendPhone = sendPhone;
	}

	public String getSendMobile() {
		return sendMobile;
	}

	public void setSendMobile(String sendMobile) {
		this.sendMobile = sendMobile;
	}

	public String getSendCountry() {
		return sendCountry;
	}

	public void setSendCountry(String sendCountry) {
		this.sendCountry = sendCountry;
	}

	public String getSendProvince() {
		return sendProvince;
	}

	public void setSendProvince(String sendProvince) {
		this.sendProvince = sendProvince;
	}

	public String getSendCity() {
		return sendCity;
	}

	public void setSendCity(String sendCity) {
		this.sendCity = sendCity;
	}

	public String getSendCounty() {
		return sendCounty;
	}

	public void setSendCounty(String sendCounty) {
		this.sendCounty = sendCounty;
	}

	public String getSendCompany() {
		return sendCompany;
	}

	public void setSendCompany(String sendCompany) {
		this.sendCompany = sendCompany;
	}

	public String getSendStreet() {
		return sendStreet;
	}

	public void setSendStreet(String sendStreet) {
		this.sendStreet = sendStreet;
	}

	public String getSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(String sendEmail) {
		this.sendEmail = sendEmail;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverPostCode() {
		return receiverPostCode;
	}

	public void setReceiverPostCode(String receiverPostCode) {
		this.receiverPostCode = receiverPostCode;
	}

	public String getReceiverPhone() {
		return receiverPhone;
	}

	public void setReceiverPhone(String receiverPhone) {
		this.receiverPhone = receiverPhone;
	}

	public String getReceiverMobile() {
		return receiverMobile;
	}

	public void setReceiverMobile(String receiverMobile) {
		this.receiverMobile = receiverMobile;
	}

	public String getReceiverCountry() {
		return receiverCountry;
	}

	public void setReceiverCountry(String receiverCountry) {
		this.receiverCountry = receiverCountry;
	}

	public String getReceiverProvince() {
		return receiverProvince;
	}

	public void setReceiverProvince(String receiverProvince) {
		this.receiverProvince = receiverProvince;
	}

	public String getReceiverCity() {
		return receiverCity;
	}

	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}

	public String getReceiverCounty() {
		return receiverCounty;
	}

	public void setReceiverCounty(String receiverCounty) {
		this.receiverCounty = receiverCounty;
	}

	public String getReceiverStreet() {
		return receiverStreet;
	}

	public void setReceiverStreet(String receiverStreet) {
		this.receiverStreet = receiverStreet;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}


	public String getErrorMessage() {
		return errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
