package com.cwc.app.asynaction;

import com.cwc.app.lucene.ClientIndexMgr;
import com.cwc.asynchronized.ThreadAction;


public class UpdateClientIndex extends ThreadAction
{
	private long cid;
	private String email;
	private String summary;
	private String mod_date;

	public UpdateClientIndex(long cid,String email,String summary,String mod_date)
	{
		this.cid = cid;
		this.email = email;
		this.summary = summary;
		this.mod_date = mod_date;
	}
	
	protected void perform()
		throws Exception
	{
		ClientIndexMgr.getInstance().updateIndex(cid, email, summary, mod_date);
	}
}
