package com.cwc.app.asynaction;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.asynchronized.ThreadAction;
import com.cwc.spring.util.MvcUtil;

/**
 * 异步记录系统日志
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AddSysLog extends ThreadAction
{
	private AdminLoginBean adminLoggerBean;
	private int opertate_type;
	private String operation;
	private String operate_para;
	private String ip;
	private String ref;
	private String visitor;
	
	public AddSysLog(AdminLoginBean adminLoggerBean,int opertate_type,String operation,String operate_para,String ip,String ref,String visitor) 
	{
		this.adminLoggerBean = adminLoggerBean;
		this.opertate_type = opertate_type;
		this.operation = operation;
		this.operate_para = operate_para;
		this.ip = ip;
		this.ref = ref;
		this.visitor = visitor;
	}

	protected void perform()
		throws Exception
	{
		SystemConfig systemConfig = (SystemConfig)MvcUtil.getBeanFromContainer("systemConfig");
		try
		{
			systemConfig.addSysLog(adminLoggerBean, opertate_type, operation,operate_para, ip, ref, visitor);
		}
		catch (Exception e)
		{
		}
	}
}
