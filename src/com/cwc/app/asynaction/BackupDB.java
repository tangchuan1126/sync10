package com.cwc.app.asynaction;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.util.Environment;
import com.cwc.exception.SystemException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;

/**
 * 该定时任务除了备份数据库外，还清除系统多余日志和删除图片临时文件夹
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class BackupDB 
{
	static Logger log = Logger.getLogger("ACTION");
	
	public void run()
	{
		try
		{
			SystemConfig systemConfig = (SystemConfig)MvcUtil.getBeanFromContainer("systemConfig");
			if ( systemConfig.getIntConfigValue("backup_db_flag")==1 )
			{
				log.info("BackupDB start...");
				//systemConfig.checkCount();	//检查备份数
				//systemConfig.backupDBByMysqlCommand();	
				log.info("BackupDB successful!");
			}
			
			//清空多余系统日志
			log.info("DelOutDayLogs start...");
			systemConfig.delOutDayLogs();
			log.info("DelOutDayLogs successful!");		
			
			//批量更新汇率
			systemConfig.batchUpdateCurrencyRate();
			
			//删除系统临时文件夹
			//对配置路径做一些检查，防止把整个根目录删了
			ArrayList tmpFolderAl = new ArrayList();
			tmpFolderAl.add(Environment.getHome()+"quotation_pdf_tmp");
			tmpFolderAl.add(Environment.getHome()+"upl_excel_tmp");
			tmpFolderAl.add(Environment.getHome()+"upl_imags_tmp");
			tmpFolderAl.add(Environment.getHome()+"upl_incoming_tmp");
			tmpFolderAl.add(Environment.getHome()+"upload_pro_img_tmp");

			for (int i=0; i<tmpFolderAl.size(); i++)
			{
				try
				{
					FileUtil.cleanFolder(tmpFolderAl.get(i).toString());
					log.info("Clean temporary img folder ["+tmpFolderAl.get(i).toString()+"] successful!");
				}
				catch (Exception e)
				{
					//e.printStackTrace();
				}
			}
			
			//检查清理过期退货单
			com.cwc.app.iface.OrderMgrIFace orderMgr = (com.cwc.app.iface.OrderMgrIFace)MvcUtil.getBeanFromContainer("proxyOrderMgr");
			orderMgr.cancelExpireReturnInfo();
		} 
		catch (Exception e)
		{
			try
			{
				throw new SystemException(e,"BackupDB error",log);	
			}
			catch (Exception e2)
			{
				
			}
		}
	}


}


