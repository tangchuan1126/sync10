package com.cwc.app.asynaction;

import com.cwc.app.lucene.ClientIndexMgr;
import com.cwc.asynchronized.ThreadAction;


public class DelClientIndex extends ThreadAction
{
	private long val;

	public DelClientIndex(long val)
	{
		this.val = val;
	}
	
	protected void perform()
		throws Exception
	{
		ClientIndexMgr.getInstance().deleteIndex( val);
	}
}
