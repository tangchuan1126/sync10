package com.cwc.app.asynaction;

import java.util.ArrayList;

import com.cwc.app.lucene.ProductIndexMgr;
import com.cwc.asynchronized.ThreadAction;


public class AddProductIndex extends ThreadAction
{
	private long pc_id;
	private String p_name;
	private String p_code;
	private long catalog_id;
	private String unit_name;
	private int alive;
	
	
	public AddProductIndex(long pc_id,String  p_name,String  p_code,long  catalog_id,String unit_name,int alive)
	{
		this.pc_id = pc_id;
		this.p_name = p_name;
		this.catalog_id = catalog_id;
		this.p_code = p_code;
		this.unit_name = unit_name;
		this.alive = alive;
	}
	
	protected void perform()
		throws Exception
	{
		ProductIndexMgr.getInstance().addIndex(pc_id, p_name, p_code, catalog_id,unit_name,alive);
	}
}
