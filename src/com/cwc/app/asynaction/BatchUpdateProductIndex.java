package com.cwc.app.asynaction;

import com.cwc.app.lucene.ProductIndexMgr;
import com.cwc.asynchronized.ThreadAction;


public class BatchUpdateProductIndex extends ThreadAction
{
	private String pc_id[];
	private String p_name[];
	private String p_code[];
	private String catalog_id[];
	private String unit_name[];
	private String alive[];

	public BatchUpdateProductIndex(String pc_id[],String p_name[],String p_code[],String catalog_id[],String unit_name[],String alive[])
	{
		this.pc_id = pc_id;
		this.p_name = p_name;
		this.catalog_id = catalog_id;
		this.p_code = p_code;
		this.unit_name = unit_name;
		this.alive = alive;
	}
	
	protected void perform()
		throws Exception
	{
		ProductIndexMgr.getInstance().batchUpdateIndex( pc_id, p_name, p_code, catalog_id, unit_name, alive);
	}
}
