package com.cwc.app.asynaction;

import com.cwc.app.lucene.ProductIndexMgr;
import com.cwc.asynchronized.ThreadAction;


public class DelProductIndex extends ThreadAction
{
	private long pc_id;

	public DelProductIndex(long pc_id)
	{
		this.pc_id = pc_id;
	}
	
	protected void perform()
		throws Exception
	{
		ProductIndexMgr.getInstance().deleteIndex(pc_id);
	}
}
