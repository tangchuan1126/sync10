package com.cwc.app.order.print;

public class ExpressPrintBean
{
	private String name;//快递名字
	private String page;//绑定打印页面
	private long scID;//快递公司ID
	double shippingFee;	//估算运费
	private String useType;//USPS使用的方式
	private String internalPage;
	private boolean selectAuto;
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public String getPage() 
	{
		return page;
	}
	
	public void setPage(String page) 
	{
		this.page = page;
	}
	
	public long getScID() 
	{
		return scID;
	}

	public void setScID(long scID) 
	{
		this.scID = scID;
	}

	public double getShippingFee() {
		return shippingFee;
	}

	public void setShippingFee(double shippingFee) 
	{
		this.shippingFee = shippingFee;
	}

	public String getUseType() {
		return useType;
	}

	public void setUseType(String useType) {
		this.useType = useType;
	}

	public String getInternalPage() {
		return internalPage;
	}

	public void setInternalPage(String internalPage) {
		this.internalPage = internalPage;
	}

	public boolean isSelectAuto() {
		return selectAuto;
	}

	public void setSelectAuto(boolean selectAuto) {
		this.selectAuto = selectAuto;
	}

}
