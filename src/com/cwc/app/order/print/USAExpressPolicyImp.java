package com.cwc.app.order.print;

import com.cwc.app.api.OrderMgr;
import com.cwc.db.DBRow;

public class USAExpressPolicyImp extends ExpressPolicyCore
{
	private OrderMgr orderMgr;
	
	public void setOrderMgr(OrderMgr orderMgr)
	{
		this.orderMgr = orderMgr;
	}

	public long[] getExpressID() throws Exception
	{
		long sc_ids[] = new long[0];
		DBRow express[];

		/**
		 * 智能选择快递主逻辑：
		 * 
		 * 国内发货：匹配地区+重量
		 * 国际发货：匹配国家+重量
		 * 
		 */
		
		//先判断是国内发货还是国际发货
		boolean isDomesticFlag = this.getExm().isDomesticShipping(this.getCcid(), this.getPsId());
//		float total_weight = orderMgr.getOrderWeightByOid(this.getOid());
//		total_weight = orderMgr.calculateWayBillWeight4ST(total_weight);
		float total_weight = this.getWeight();
		
		if (isDomesticFlag)//国内发货
		{
			express = this.getExm().getDomesticExpressCompanyByPsIdProId( this.getPsId(), this.getProId(), total_weight, null);
			////system.out.println("Domestic express length:"+express.length);
		}
		else
		{
			express = this.getExm().getInternationalExpressCompanyByPsIdProId(this.getPsId(),this.getCcid(),total_weight,null);
			////system.out.println("International express length:"+express.length);
		}
		
		sc_ids = new long[express.length];
		for (int i=0; i<express.length; i++)
		{
			sc_ids[i] = express[i].get("sc_id", 0l);
		}
		
		return(sc_ids);
	}

	/**
	 * 0 < weight < 13安士 --- First Class Mail
	 * 14安士 <= weight <= 4套超薄单灯( (0.67+0.27)*4 ) --- Priority Mail
	 * 4套超薄单灯 < weight --- Fedex 
	 */
	//13安士=0.3686 kg
//	long sc_id = 0;
//
//	float total_weight = orderMgr.getOrderWeightByOid(this.getOid());
//	////system.out.println("1>"+total_weight);
//	total_weight = orderMgr.calculateWayBillWeight4ST(total_weight);//重新修正后的重量，用于打印计费
//	////system.out.println("2>"+total_weight);
//	if ( total_weight<EndiciaClient.MIN_WEIGHT )
//	{
//		sc_id = 100009l;//USPS-F
//	}
//	else if ( total_weight>=EndiciaClient.MIN_WEIGHT&&total_weight<EndiciaClient.SLIM_SET_WEIGHT*4f )
//	{
//		sc_id = 100006l;//USPS-P
//	}
//	else
//	{
//		sc_id = 100008l;//FEDEX
//	}
	
	
}





