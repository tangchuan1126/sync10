package com.cwc.app.order.print;

import com.cwc.db.DBRow;

/**
 * 该策略提供给“更多快递”按钮使用
 * 该策略不限制订单重量
 * @author Administrator
 *
 */
public class NotLimitWeightExpressPolicyImp extends ExpressPolicyCore
{
	public long[] getExpressID() throws Exception
	{
		long sc_ids[] = new long[0];
		DBRow express[];

		boolean isDomesticFlag = this.getExm().isDomesticShipping(this.getCcid(), this.getPsId());
		
		if (isDomesticFlag)//国内发货
		{
			express = this.getExm().getDomesticExpressCompanyByPsIdProId( this.getPsId(), this.getProId(),  null);
		}
		else
		{
			express = this.getExm().getInternationalExpressCompanyByPsIdProId(this.getPsId(),this.getCcid(),null);
		}
		
//		//对Puerto Rico地方做特殊操作
//		DBRow express2[] = new DBRow[express.length];
//		int count = 0;
//		if(this.getPsId()==100043&&this.getCcid()==10929)//如果是美国仓库，国家ID是Puerto Rico的，认为USPS可以国内发货
//		{
//			express2 = new DBRow[express.length+2];
//			express2[0] = this.getExm().getDetailCompany(100006); 
//			express2[1] = this.getExm().getDetailCompany(100009);
//			count = 2;
//		}
//		
//		for (int i = 0; i < express.length; i++) 
//		{
//			express2[i+count] = express[i];
//		}
		
		
		
		sc_ids = new long[express.length];
		for (int i=0; i<express.length; i++)
		{
			sc_ids[i] = express[i].get("sc_id", 0l);
		}
		
		return(sc_ids);
	}
	
}





