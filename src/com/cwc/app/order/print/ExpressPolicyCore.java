package com.cwc.app.order.print;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.cwc.app.api.ExpressMgr;
import com.cwc.app.api.OrderMgr;
import com.cwc.app.api.zj.CartWaybill;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.shipping.ShippingInfoBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public abstract class ExpressPolicyCore 
{
	static Logger log = Logger.getLogger("ACTION");
	
	private ExpressMgr exm;
	

	public long getProId() {
		return proId;
	}

	public void setProId(long proId) {
		this.proId = proId;
	}
	
	public ExpressMgr getExm() {
		return exm;
	}

	public void setExm(ExpressMgr exm) {
		this.exm = exm;
	}

	public long getPsId()
	{
		return psId;
	}
	
	public void setPsId(long psId)
	{
		this.psId = psId;
	}
	
	public long getCcid()
	{
		return ccid;
	}
	
	public void setCcid(long ccid)
	{
		this.ccid = ccid;
	}
	
	public long getOid()
	{
		return oid;
	}

	public void setOid(long oid)
	{
		this.oid = oid;
	}


	public void setWeight(float weight) 
	{
		this.weight = weight;
	}
	
	/**
	 * 获得系统推荐快递(具体算法，由继承子类实现)
	 * @return
	 * @throws Exception
	 */
	protected abstract long[] getExpressID() throws Exception;
	
	/**
	 * 获得推荐快递打印页面
	 * @return
	 * @throws Exception
	 */
	public ExpressPrintBean[] getExpressPrintPage()
		throws Exception
	{
		DBRow express[] = exm.getExpressCompanysByScIds(this.getExpressID(),null);
		ArrayList<ExpressPrintBean> expressPrintList = new ArrayList<ExpressPrintBean>();
		
		for (int i=0; i<express.length; i++)
		{
			//计算运费
			//log.info("ExpressPolicyCore:"+express[i].get("sc_id",0l)+" - "+this.weight+" - "+this.ccid+" - "+this.proId);
			ShippingInfoBean shippingInfoBean;
			try 
			{
				shippingInfoBean = this.exm.getShippingFee(express[i].get("sc_id",0l), this.weight, this.ccid,this.proId);
				ExpressPrintBean expressPrintBean = new ExpressPrintBean();
				expressPrintBean.setName(express[i].getString("name"));
				expressPrintBean.setPage(express[i].getString("create_url"));
				expressPrintBean.setScID(express[i].get("sc_id",0l));
				expressPrintBean.setShippingFee(shippingInfoBean.getShippingFee());
				expressPrintBean.setUseType(shippingInfoBean.getUseType());
				expressPrintBean.setInternalPage(express[i].getString("internal_create_url"));
				expressPrintBean.setSelectAuto(shippingInfoBean.isSelectAuto());

				expressPrintList.add(expressPrintBean);
			}
			catch (WeightOutSizeException e) 
			{
				continue;
			}
		}
		return( (ExpressPrintBean[])expressPrintList.toArray(new ExpressPrintBean[0]) );
	}
	
	
	public ExpressPrintBean[] getNewExpressPrintPage(DBRow[] waybillCart)
		throws Exception
	{
		DBRow express[] = exm.getExpressCompanysByScIds(this.getExpressID(),null);

		ArrayList<ExpressPrintBean> expressPrintList = new ArrayList<ExpressPrintBean>();
		
		for (int i=0; i<express.length; i++)
		{
			//计算运费
			//log.info("ExpressPolicyCore:"+express[i].get("sc_id",0l)+" - "+this.weight+" - "+this.ccid+" - "+this.proId);
			ShippingInfoBean shippingInfoBean;
			try 
			{
//				shippingInfoBean = this.exm.getShippingFee(express[i].get("sc_id",0l), this.weight, this.ccid,this.proId);
				shippingInfoBean = this.exm.getNewShippingFee(express[i].get("sc_id",0l),this.ccid,this.proId,waybillCart);
				ExpressPrintBean expressPrintBean = new ExpressPrintBean();
				expressPrintBean.setName(express[i].getString("name"));
				expressPrintBean.setPage(express[i].getString("create_url"));
				expressPrintBean.setScID(express[i].get("sc_id",0l));
				expressPrintBean.setShippingFee(shippingInfoBean.getShippingFee());
				expressPrintBean.setUseType(shippingInfoBean.getUseType());
				expressPrintBean.setInternalPage(express[i].getString("internal_create_url"));
				expressPrintBean.setSelectAuto(shippingInfoBean.isSelectAuto());
				
				expressPrintList.add(expressPrintBean);
			}
			catch (WeightOutSizeException e) 
			{
				continue;
			}
		}
		return( (ExpressPrintBean[])expressPrintList.toArray(new ExpressPrintBean[0]) );
	}
	
	
	
	//protected abstract Document getDataBaseDocument(ResultSet rs) throws Exception;
	
	
	
	private long psId = 0;			//仓库ID
	private long ccid = 0;			//递送国家
	private long oid = 0;			//订单号
	private long proId = 0;			//递送省份
	private float weight = 0;		//重量


	public float getWeight() {
		return weight;
	}

	
}





