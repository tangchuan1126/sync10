package com.cwc.app.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.auth.PermitMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.ConfigBean;
import com.cwc.authentication.AuthActionList;
import com.cwc.db.DBRow;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class PermissionFilter implements Filter {
	
	Logger log = Logger.getLogger(PermissionFilter.class);
	FilterConfig config;
	String DEFAULT_NOT_PERMIT_PAGE = "not_permit";

	@Override
	public void init(FilterConfig paramFilterConfig) throws ServletException {
		this.config = paramFilterConfig;
	}

	@Override
	public void doFilter(ServletRequest sReq, ServletResponse sRes, FilterChain chain) throws IOException, ServletException{
		HttpServletRequest req = (HttpServletRequest) sReq;
		HttpServletResponse res = (HttpServletResponse) sRes;
		
		boolean isAuthorized = false;
		
		/**  获取请求的URI、Method **/
		String requestURI = req.getRequestURI();
		String requestMethod = req.getMethod();
		
//		log.info(requestURI + " : " + requestMethod);
//		//system.out.println("requestURI:"+ requestURI);
//		//system.out.println("requestMethod:"+ requestMethod);
		
		/**  获取用户登录信息  **/
		AdminMgr adminMgr = (AdminMgr) MvcUtil.getBeanFromContainer("adminMgr");
		AdminLoginBean adminLoginBean = (AdminLoginBean) adminMgr.getAdminLoginBean(StringUtil.getSession(req));
		if(adminLoginBean == null || StringUtil.isBlank(requestMethod)){
			//throw new ServletException("PermissionFilter doFilter: Permission denied!");
			writePrompt(res, "Permission denied");
			return; //若用户未登录或未给出http请求方式，直接返回
		}
		
		/**  判断URI是否需要权限控制  **/
		if(AuthActionList.isNeed2AuthUri(requestURI) && !"admin".equals(adminLoginBean.getAccount())){
			// 获取个人权限，判断用户是否有相应的权限   requestUri=action_uri，requestMethod=request_method
			
			PermitMgr permitMgr = (PermitMgr) MvcUtil.getBeanFromContainer("permitMgr");
			
			/**  角色的Uri权限  **/			
			HashMap<String, DBRow> userRolesAuthUriMap = null;
			try {
				userRolesAuthUriMap = permitMgr.getUserRolesAuthUri(adminLoginBean.getAdid());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(userRolesAuthUriMap.containsKey(requestURI)){
				isAuthorized = requestMethod.equals(userRolesAuthUriMap.get(requestURI).getString("request_method"));
			}
			
			/**  用户扩展的Uri权限 **/
			HashMap<String, DBRow> userExtendAuthUriMap = null;
			try {
				userExtendAuthUriMap = permitMgr.getUserExtendAuthUri(adminLoginBean.getAdid());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(userExtendAuthUriMap.containsKey(requestURI)){
				isAuthorized = isAuthorized || requestMethod.equals(userExtendAuthUriMap.get(requestURI).getString("request_method"));
			}
		}else{
			isAuthorized = true; //不需要权限控制
		}
		
		if(isAuthorized){
			chain.doFilter(req, res);
		}else{
			//throw new ServletException("PermissionFilter doFilter: Permission denied!");
			writePrompt(res, "Permission denied");
			return; //无权限，直接返回
		}
	}

	@Override
	public void destroy() {
		this.config = null;
	}
	
	private void writePrompt(HttpServletResponse res, String msg) throws IOException{
		
		res.setContentType("application/json;charset=UTF-8");
		res.setHeader("pragma", "no-cache");
		res.setHeader("cache-control", "no-cache");
		res.setStatus(HttpServletResponse.SC_FORBIDDEN);
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("error", msg);
		
		PrintWriter writer = res.getWriter();
		writer.write(JSONObject.fromObject(result).toString());
		writer.flush();
		writer.close();
	}
}
