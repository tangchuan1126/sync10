package com.cwc.app.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 管理后台鉴权
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class AdminAuthorizationFilter implements Filter
{
	private FilterConfig config;
	static Logger log = Logger.getLogger("ACTION");

	public void init(FilterConfig config) throws ServletException
	{
		this.config = config;
	}
	
	public void destroy()
	{
	}

	private boolean isRightAdminPath(HttpServletRequest request,String path)
	{
		String uri = request.getRequestURI();

		if (uri.lastIndexOf("/")!=uri.length()-1)
		{
			uri += "/";
		}

		String t1 = uri.split("administrator")[1];
		String t2;
		
		//判断是否从正确入口进入
		if ( t1.length()>1&&t1.indexOf(".")==-1 )
		{
			t2 = t1.substring(1, t1.length());
			
			int pos = t2.indexOf("/");
			if (pos>=0)
			{
				t2 = t2.substring(0,pos);	
			}
			
			if (t2.equals(path))
			{
				return(true);
			}
		}
		
		return(false);
	}
	
	public void doFilter(ServletRequest srequest,ServletResponse sresponse,FilterChain chain)
		throws IOException, ServletException
	{
		HttpServletRequest request = (HttpServletRequest)srequest; 
		HttpServletResponse response = (HttpServletResponse)sresponse;
		HttpSession session = request.getSession(true);
		String adminPath = "admin";//默认
 		try
		{
			String uri = request.getRequestURI() ;
			//因为打印控件问题，不对DHL图片做任何鉴权处理
			if (request.getRequestURI()!=null &&(request.getRequestURI().indexOf("TransformXMLtoHTML/HTML/BarCode")>=0||request.getRequestURI().indexOf("TransformXMLtoHTML/HTML/images")>=0||request.getRequestURI().indexOf("administrator/imgs/print")>=0||request.getRequestURI().indexOf("administrator/order/UspsPrintLabel")>=0||request.getRequestURI().indexOf("administrator/order/FedexPrintLabel")>=0||request.getRequestURI().indexOf("administrator/order/FedexInternationalPrintLabel")>=0||request.getRequestURI().indexOf("action/bcs/BCSTest.zip")>=0))
			{
				chain.doFilter(srequest,sresponse);			
				return;//防止往下继续执行
			}
			if(uri.indexOf(".css") != -1 || uri.indexOf(".js") != -1){
				chain.doFilter(srequest,sresponse);			 //不拦截 css .js
				return;
			}
			SystemConfig systemConfig = (SystemConfig)MvcUtil.getBeanFromContainer("systemConfig");
			adminPath = systemConfig.getStringConfigValue("admin_login_path");	//获得登录入口
			
			/**
			 * 判断登录与否，并非以adminSesion session是否存在为依据
			 * 而是根据adminSesion session内的登录标志为依据
			 */
			
			if ( session.getAttribute(Config.adminSesion) == null )
			{
				AdminLoginBean adminLoggerBean = new AdminLoginBean();
				//为了整合FineReport报表服务器，这里需要做特殊处理
				if ( request.getRequestURI().indexOf("ReportServer")>=0||isRightAdminPath(request,adminPath) )
				{
					//没有登录的话，先创建一个AdminLoggerBean，并标记已经从正确登录入口进入
					adminLoggerBean.setIsLoginRightPath();
				}
				StringUtil.getSession(request).setAttribute(Config.adminSesion,adminLoggerBean);	
//				//system.out.println(ConfigBean.getStringValue("systenFolder")+config.getInitParameter("adminLoginFalse"));
				response.sendRedirect(ConfigBean.getStringValue("systenFolder")+config.getInitParameter("adminLoginFalse"));
				return;//防止往下继续执行
			}
			else
			{
				AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
					
					if (adminLoggerBean.isLogin())//已经登录
					{
						//因为已经对管理后台所有访问的jsp页面做了html映射，这里禁止直接访问对应的jsp文件，提示没有找到相关页面
						String currentURL = StringUtil.getCurrentURL(request);
						////system.out.println( currentURL );
						if (currentURL!=null&&currentURL.indexOf("administrator")>=0&&currentURL.indexOf(".jsp")>=0)
						{
							response.sendRedirect("error");
							return;
						}
						else
						{
							//在登录状态下，管理员有可能重新输入administrator/admin这样的url
							//为了整合FineReport报表服务器，这里需要做特殊处理
							if ( request.getRequestURI().indexOf("ReportServer")==-1&&isRightAdminPath(request,adminPath) )
							{
								response.sendRedirect(ConfigBean.getStringValue("systenFolder")+"administrator/index.html");
							}
							else
							{
								chain.doFilter(srequest,sresponse);								
							}
						}
					}
					else
					{
						//为了整合FineReport报表服务器，这里需要做特殊处理
						if (request.getRequestURI().indexOf("ReportServer")==-1&&isRightAdminPath(request,adminPath) )
						{
							adminLoggerBean.setIsLoginRightPath();
							StringUtil.getSession(request).setAttribute(Config.adminSesion,adminLoggerBean);
						}
						
						response.sendRedirect(ConfigBean.getStringValue("systenFolder")+config.getInitParameter("adminLoginFalse"));
						
//						response.sendRedirect(config.getInitParameter("adminLoginFalse"));
						return;//防止往下继续执行
					}		
			}

		} 
		catch (Exception e)
		{
			//把堆栈信息记录到日志
			StackTraceElement[] ste = e.getStackTrace();
			StringBuffer sb = new StringBuffer();
			sb.append(e.getMessage() + "\r\n");
			for (int i = 0;i < ste.length;i++)
			{
				sb.append(ste[i].toString() + "\r\n");
			}
			log.error("=== AdminAuthorizationFilter ===");
			log.error(sb.toString());
			
			throw new ServletException("AdminAuthorizationFilter doFilter:" + e);		
		}
	}

	public static void main(String args[])
	{
		String a = "/administrator/";
		
		String t = "/administrator/order/ct_order.jsp";
		////system.out.println(t.substring(a.length(),t.length()));
	}
}



