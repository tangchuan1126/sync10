/*
 * 创建日期 2008-8-18
 *
 */
package com.cwc.app.exactprivilege.helpcenter.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.helpCenter.ModHelpCenterTopicIssueNotOwnerException;
import com.cwc.app.floor.api.FloorHelpCenterMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exactprivilege.ExactPrivilegeIFace;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 发布/撤销文章细粒度权限
 */
public class ModHelpCenterTopicIssueExactPrivilege implements ExactPrivilegeIFace
{
	/**
	 * 
	 * @param action
	 * @throws OperationNotPermitException
	 * @throws Exception
	 */
	public void validate(String action,Object[] pob) 
		throws Throwable
	{
		//request能获得到，登录管理员状态bean能得到，没什么不可以做的了
//		HttpSession session = StrUtil.getSession(request);
		AdminLoginBean adminLoggerBean = (AdminLoginBean)pob[2];		
		
		FloorHelpCenterMgr fhcMgr = (FloorHelpCenterMgr)MvcUtil.getBeanFromContainer("floorHelpCenterMgr");
		String[] ids = ((String)pob[1]).split(",");
		for (int i = 0; i < ids.length; i++) 
		{
			long hct_id = Long.parseLong(ids[i]);
				DBRow detail=fhcMgr.getDetailTopicsByHTCID(hct_id);

				if(!adminLoggerBean.getAccount().equals(detail.getString("author"))&&adminLoggerBean.getAdgid()!=10000l)
				{
					throw new ModHelpCenterTopicIssueNotOwnerException();
				}
		}
		
	}
	
}
