/*
 * 创建日期 2008-8-18
 *
 */
package com.cwc.app.exactprivilege.helpcenter.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.helpCenter.DelHelpCenterTopicNotOwnerException;
import com.cwc.app.floor.api.FloorHelpCenterMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.exactprivilege.ExactPrivilegeIFace;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 删除文章细粒度权限
 */
public class DelHelpCenterTopicExactPrivilege implements ExactPrivilegeIFace
{
	/**
	 * 
	 * @param action
	 * @throws OperationNotPermitException
	 * @throws Exception
	 */
	public void validate(String action,Object[] pub) throws Throwable
	{
		//request能获得到，登录管理员状态bean能得到，没什么不可以做的了
		HttpServletRequest request = (HttpServletRequest)pub[0];
		HttpSession session = StringUtil.getSession(request);
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(session);		
		
		FloorHelpCenterMgr fhcMgr = (FloorHelpCenterMgr)MvcUtil.getBeanFromContainer("floorHelpCenterMgr");
		long hct_id=StringUtil.getLong(request,"hct_id");
		DBRow detail=fhcMgr.getDetailTopicsByHTCID(hct_id);

		if(!adminLoggerBean.getAccount().equals(detail.getString("author"))&&adminLoggerBean.getAdgid()!=10000l)
		{
			throw new DelHelpCenterTopicNotOwnerException();
		}
		
		
//		long oid = StrUtil.getLong(request, "oid");
//		String account = adminLoggerBean.getAccount();
//		//system.out.println(account+" - "+oid);
//		if (oid==417280l)
//		{
//			throw new ProtectOrderException(action);
//		}
	}
	
}
