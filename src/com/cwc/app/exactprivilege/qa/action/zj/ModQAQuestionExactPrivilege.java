/*
 * 创建日期 2008-8-18
 *
 */
package com.cwc.app.exactprivilege.qa.action.zj;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.exception.qa.ModQAQuestionNotQuestionerException;
import com.cwc.app.floor.api.zj.FloorQuestionMgrZJ;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.cwc.exactprivilege.ExactPrivilegeIFace;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 修改问题细粒度权限
 */
public class ModQAQuestionExactPrivilege implements ExactPrivilegeIFace
{
	/**
	 * 
	 * @param action
	 * @throws OperationNotPermitException
	 * @throws Exception
	 */
	public void validate(String action,Object[] pob) 
		throws Throwable
	{
		//request能获得到，登录管理员状态bean能得到，没什么不可以做的了
		HttpServletRequest request = (HttpServletRequest)pob[0];
		HttpSession session = StringUtil.getSession(request);
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(session);		
		
		FloorQuestionMgrZJ floorQuestionMgrZJ = (FloorQuestionMgrZJ)MvcUtil.getBeanFromContainer("floorQuestionMgrZJ");
		long question_id=StringUtil.getLong(request,"question_id");
		DBRow detail=floorQuestionMgrZJ.getDetailQuestionById(question_id);

		if(!adminLoggerBean.getAccount().equals(detail.getString("questioner")))//只允许作者本人修改
		{
			throw new ModQAQuestionNotQuestionerException();
		}
	}
	
}
