package com.cwc.app.prehandle;

import java.util.ArrayList;

import com.cwc.app.prehandle.order.OrderPreHandleFather;

public class OrderPreHandle 
{
	private ArrayList<OrderPreHandleFather> orderPreHandleClass;

	
	public ArrayList<OrderPreHandleFather> getOrderPreHandleClass() 
	{
		return orderPreHandleClass;
	}

	public void setOrderPreHandleClass(ArrayList<OrderPreHandleFather> orderPreHandleClass) 
	{
		this.orderPreHandleClass = orderPreHandleClass;
	}
}
