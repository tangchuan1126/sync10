package com.cwc.app.prehandle.order;

public abstract class OrderPreHandleFather 
{
	
	/**
	 * 子类必须实现核心业务处理
	 * @param oid
	 * @throws Exception
	 */
	public abstract void perform(long oid) throws Exception;
}
