package com.cwc.app.prehandle.order.action;

import java.net.ConnectException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cwc.app.prehandle.order.*;
import com.cwc.app.util.DateUtil;
import com.cwc.spring.util.MvcUtil;
import com.cwc.app.api.zr.OrderMgrZr;
import com.cwc.app.iface.zr.EbayMgrZrIFace;
import com.cwc.app.iface.zr.OrderMgrIfaceZR;
import com.cwc.app.jms.action.GetEbayOrderSellerIDJMS;
import com.cwc.app.jms.action.UpdateTradeItemSkuFvfMpnJMS;
import com.cwc.db.DBRow;

/**
 * 通过ebay接口，查询当前订单是那个ebay帐号的
 * @author Administrator
 *
 */
public class GetEbayOrderSellerID extends OrderPreHandleFather
{
	static Logger sellerlog = Logger.getLogger("ebay_seller");
	public void perform(final long oid)
		throws Exception
	{
		try 
		{
			sellerlog.info("GetEbayOrderSellerID Oid:"+oid);
			sellerlog.info("GetSellerIdStart:"+DateUtil.NowStr());
			long start = System.currentTimeMillis();
			
			sellerlog.info("oid:"+oid+" time:"+DateUtil.NowStr());
			
			EbayMgrZrIFace ebayMgr = (EbayMgrZrIFace) MvcUtil.getBeanFromContainer("ebayMgrZr");
			com.cwc.app.iface.OrderMgrIFace orderMgr = (com.cwc.app.iface.OrderMgrIFace)MvcUtil.getBeanFromContainer("orderMgr");
			OrderMgrIfaceZR orderMgrZr = (OrderMgrIfaceZR) MvcUtil.getBeanFromContainer("orderMgrZr");
			DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
			
			sellerlog.info("oid:"+oid+"detailOrder:"+detailOrder+" time:"+DateUtil.NowStr());
			if(detailOrder!= null && detailOrder.getString("order_source").equals(OrderMgrZr.ORDER_SOURCE_EBAY))
			{
				//然后查询COI表把itemnumber 都查询出来
				sellerlog.info("oid:"+oid+" time:"+DateUtil.NowStr());
				DBRow[] rows = orderMgrZr.getTradeItemByOid(oid);
				sellerlog.info("oid:"+oid+" tradeItemLength:"+rows.length+" time:"+DateUtil.NowStr());
				if(rows != null && rows.length > 0)
				{
					for(DBRow row : rows)
					{
						String itemNumber = row.getString("item_number");
						String ebay_txn_id = row.getString("ebay_txn_id");
						sellerlog.info("oid:"+oid+" itemNumber:"+itemNumber+" txnid:"+ebay_txn_id+" time:"+DateUtil.NowStr());
						if((itemNumber!=null&&itemNumber.length()>0)&&(ebay_txn_id.length()>0))
						{
							Map<String,String> map =  ebayMgr.getSellerId(itemNumber,row.get("trade_item_id",0l));
							
							String sellerId = map.get("sellerId");
							
							if (sellerId==null||sellerId.equals("")) 
							{
								throw new ConnectException();
							}
							
							sellerlog.info("in_time:"+detailOrder.getString("post_date")+",oid:"+oid+",sellerId:"+sellerId+",getSellerTime:"+DateUtil.NowStr());
							
							orderMgr.modSellerId(oid, sellerId);

							DBRow temp = new DBRow();
							String mpn = map.get("MPN");
							temp.add("seller_id", sellerId);
							temp.add("mpn", mpn);
							ebayMgr.updateTradeItem(row.get("trade_item_id", 0), temp);
							
						}
					}
					new Thread(
						new Runnable() {
							@Override
							public void run() {
								try {
									com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("jmsMgr");
									jmsMgr.excuteAction(new UpdateTradeItemSkuFvfMpnJMS(oid));
									//ebayMgr.updateTransactionsFeeSKU(sellerId, row.getString("item_number"), row.getString("ebay_txn_id") , row.get("trade_item_id",0l),mpn);
								} 
								catch (Exception e) 
								{
								}								
							}
						}
					).start();
					
					
				}
			}
			
			long end = System.currentTimeMillis();
			sellerlog.info("GetSellerIdEnd:"+DateUtil.NowStr()+" useTime:"+(end-start));
		}
		catch (Exception e) 
		{
			sellerlog.error(e);
		}
	}

}
