package com.cwc.app.prehandle.order.action;

import com.cwc.app.jms.action.AddTranInfoActionJMS;
import com.cwc.app.prehandle.order.*;
import com.cwc.spring.util.MvcUtil;

/**
 * 通过ebay接口，
 * @author Administrator
 *
 */
public class AddTranInfo extends OrderPreHandleFather
{
	public void perform(long oid)
		throws Exception
	{
		com.cwc.jms.JmsMgrIFace jmsMgr = (com.cwc.jms.JmsMgrIFace)MvcUtil.getBeanFromContainer("proxyJmsMgr");
		jmsMgr.excuteAction(new AddTranInfoActionJMS(oid));//应该使用OID做参数
	}

}
