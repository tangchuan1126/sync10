package com.cwc.app.plroute;

import java.util.ArrayList;

/**
 * 仓库区域
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ZoneBean extends FatherBean
{
	private long psID;//仓库ID
	private String barcode;//商品条码
	private String name;//区域名称
	private float quantity;//该区域总数量
	private ArrayList<ContainerBean> containerBeanAL = new ArrayList<ContainerBean>();//该区域下存放该商品容器集合
	
	public ZoneBean(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public float getQuantity()
	{
		return quantity;
	}
	
	public void setQuantity(float quantity)
	{
		this.quantity = quantity;
	}
	
	/**
	 * 返回容器数组
	 * @return
	 */
	public ContainerBean[] getContainerBeanAL()
	{
		return (ContainerBean[])containerBeanAL.toArray(new ContainerBean[0]);
	}
	
	/**
	 * 装载容器
	 * @param containerBean
	 */
	public void setContainerBean(ContainerBean containerBean)
	{
		this.containerBeanAL.add(containerBean);
	}
	
	

	public long getPsID()
	{
		return psID;
	}

	public void setPsID(long psID)
	{
		this.psID = psID;
	}

	public String getBarcode()
	{
		return barcode;
	}

	public void setBarcode(String barcode)
	{
		this.barcode = barcode;
	}
	
	
}
