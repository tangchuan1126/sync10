package com.cwc.app.emailtemplate.velocity.applymoney;

import org.apache.velocity.context.Context;

import com.cwc.app.api.OrderMgr;
import com.cwc.app.api.qll.EmailTemplateMgr;
import com.cwc.app.page.core.VelocityEmailPageStringFather;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;

/**
 * 资金申请通知邮件模板
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ApplyMoneyEmailPageString extends VelocityEmailPageStringFather 
{
	private DBRow detailTemplate = null;
	private DBRow detailOrder = null;
	private OrderMgr orderMgr = null;

	private String TEMPLATE_NAME = "ApplicationMoney ";//绑定模板
	
	public ApplyMoneyEmailPageString(long oid) 
		throws Exception
	{
		orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
		this.detailOrder = orderMgr.getDetailPOrderByOid(oid);
		
		EmailTemplateMgr emailTemplateMgr = (EmailTemplateMgr)MvcUtil.getBeanFromContainer("emailTemplateMgr"); 
		this.detailTemplate = emailTemplateMgr.getDetailTemplateByName(TEMPLATE_NAME);//根据模版名称获得模版
	}

	public String getTemplate(Context context,String encodeing) 
		throws Exception 
	{
		context.put("name", this.detailOrder.getString("address_name"));
		
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("email_template"));
		}
	}

	public String getAuthor()
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("send_name"));
		}
	}

	public String getHost() 
		throws Exception
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("smpt_server"));
		}
	}

	public String getPassWord() 
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("send_pwd"));
		}
	}

	public int getPort()
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return(0);
		}
		else
		{
			return(Integer.parseInt(detailTemplate.getString("smpt_port")));
		}
	}

	public String getRePly()
		throws Exception 
	{
		//打印通知邮件，根据不同收款帐号，需要不同回复帐号
		return(orderMgr.getPrintedOrderReplyMail(detailOrder.getString("business")));
	}

	public String getUserName() 
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("send_id"));
		}
	}
}





