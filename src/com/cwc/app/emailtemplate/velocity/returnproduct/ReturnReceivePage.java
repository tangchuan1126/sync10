package com.cwc.app.emailtemplate.velocity.returnproduct;


import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.cwc.app.api.OrderMgr;
import com.cwc.app.page.core.VelocityPageFileFather4Email;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ReturnReceivePage extends VelocityPageFileFather4Email 
{
	private long rp_id;
	private long oid;
	
	public ReturnReceivePage(long rp_id,long oid)
	{
		this.rp_id = rp_id;
		this.oid = oid;
	}

	public Template getTemplate(VelocityEngine ve,Context context,String encodeing) 
		throws Exception 
	{
		OrderMgr orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
		DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
		
		context.put("rp_id", this.rp_id);
		context.put("name", detailOrder.getString("address_name"));
		
		Template t = ve.getTemplate("return_product/return_received.html",encodeing);

		return(t);
	}
}
