package com.cwc.app.emailtemplate.velocity.returnproduct;

import org.apache.velocity.context.Context;

import com.cwc.app.api.CatalogMgr;
import com.cwc.app.api.OrderMgr;
import com.cwc.app.api.ProductMgr;
import com.cwc.app.api.zr.ReturnProductMgrZr;
import com.cwc.app.api.zyj.ReturnProductOrderMgrZyj;
import com.cwc.app.iface.zr.ReturnProductMgrIfaceZr;
import com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace;
import com.cwc.app.page.core.VelocityEmailPageStringFather;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ReturnVoiceEmailPageString extends VelocityEmailPageStringFather 
{
	private long rp_id;
	private long oid;
	private DBRow detail = null;

	private long TEMPLATE_ID = 100013l;//绑定模板ID
	
	public ReturnVoiceEmailPageString(long rp_id,long oid) 
		throws Exception
	{
		this.rp_id = rp_id;
		this.oid = oid;
		
		com.cwc.app.api.qll.EmailTemplateMgr emailTemplateMgr = (com.cwc.app.api.qll.EmailTemplateMgr)MvcUtil.getBeanFromContainer("emailTemplateMgr"); 
		detail = emailTemplateMgr.getDetailTemplate(TEMPLATE_ID);
	}

	public String getTemplate(Context context,String encodeing) 
		throws Exception 
	{
		OrderMgr orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
		CatalogMgr catalogMgr = (CatalogMgr)MvcUtil.getBeanFromContainer("catalogMgr");
		ProductMgr productMgr = (ProductMgr)MvcUtil.getBeanFromContainer("productMgr");
		com.cwc.app.iface.zr.ReturnProductMgrIfaceZr returnProductMgrZr = (com.cwc.app.iface.zr.ReturnProductMgrIfaceZr)MvcUtil.getBeanFromContainer("proxyReturnProductMgrZr");;
		com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace returnProductOrderMgrZyj = (com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace)MvcUtil.getBeanFromContainer("proxyReturnProductOrderMgrZyj");
		
		DBRow returnProduct = returnProductMgrZr.getReturnProductByRpId(this.rp_id);
		DBRow returnSubProducts[] = returnProductOrderMgrZyj.getReturnSubItemsByRpid(rp_id);
		DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
		DBRow detailCatalog = catalogMgr.getDetailProductStorageCatalogById(returnProduct.get("ps_id", 0l));
		DBRow detailProvince  = productMgr.getDetailProvinceByProId(detailCatalog.get("pro_id",0l));
		DBRow detailCountry  = orderMgr.getDetailCountryCodeByCcid(detailCatalog.get("native",0l));
		
		
		context.put("returnSubProducts", returnSubProducts);
		context.put("rp_id", this.rp_id);
		context.put("contact", detailCatalog.getString("send_contact"));
		String address = detailCatalog.getString("send_house_number")+"<br/>"+detailCatalog.getString("send_street")+"<br/>"+detailCatalog.getString("send_city")+","+detailProvince.getString("p_code")+" "+detailCatalog.getString("deliver_zip_code")+"<br/>"+detailCountry.getString("c_code");
		context.put("address",address);
		context.put("phone", detailCatalog.getString("send_phone"));
		context.put("ps_id", returnProduct.get("ps_id", 0l));
		
		context.put("name", detailOrder.getString("address_name"));
		
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("email_template"));
		}
	}

	public String getAuthor()
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("send_name"));
		}
	}

	public String getHost() 
		throws Exception
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("smpt_server"));
		}
	}

	public String getPassWord() 
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("send_pwd"));
		}
	}

	public int getPort()
		throws Exception 
	{
		if (detail==null)
		{
			return(0);
		}
		else
		{
			return(Integer.parseInt(detail.getString("smpt_port")));
		}
	}

	public String getRePly()
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("reply_id"));
		}
	}

	public String getUserName() 
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("send_id"));
		}
	}
	
	 
}





