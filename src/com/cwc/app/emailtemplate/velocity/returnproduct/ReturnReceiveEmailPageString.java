package com.cwc.app.emailtemplate.velocity.returnproduct;


import org.apache.velocity.context.Context;

import com.cwc.app.api.OrderMgr;
import com.cwc.app.page.core.VelocityEmailPageStringFather;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ReturnReceiveEmailPageString extends VelocityEmailPageStringFather
{
	private long rp_id;
	private long oid;
	private DBRow detail = null;
	private long TEMPLATE_ID = 100015l;//绑定模板ID
	
	public ReturnReceiveEmailPageString(long rp_id,long oid) 
		throws Exception
	{
		this.rp_id = rp_id;
		this.oid = oid;
		
		com.cwc.app.api.qll.EmailTemplateMgr emailTemplateMgr = (com.cwc.app.api.qll.EmailTemplateMgr)MvcUtil.getBeanFromContainer("emailTemplateMgr"); 
		detail = emailTemplateMgr.getDetailTemplate(TEMPLATE_ID);
	}

	public String getTemplate(Context context,String encodeing) 
		throws Exception 
	{
		OrderMgr orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
		DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
		
		context.put("rp_id", this.rp_id);
		context.put("name", detailOrder.getString("address_name"));
		
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("email_template"));
		}
	}

	public String getAuthor()
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("send_name"));
		}
	}
	
	public String getHost() 
		throws Exception
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("smpt_server"));
		}
	}
	
	public String getPassWord() 
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("send_pwd"));
		}
	}
	
	public int getPort()
		throws Exception 
	{
		if (detail==null)
		{
			return(0);
		}
		else
		{
			return(Integer.parseInt(detail.getString("smpt_port")));
		}
	}
	
	public String getRePly()
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("reply_id"));
		}
	}
	
	public String getUserName() 
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("send_id"));
		}
	}
}






