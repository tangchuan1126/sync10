package com.cwc.app.emailtemplate.velocity.returnproduct;


import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.cwc.app.api.CatalogMgr;
import com.cwc.app.api.OrderMgr;
import com.cwc.app.page.core.VelocityPageFileFather4Email;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 
 * @author TurboShop
 *已不使用
 * TurboShop.cn all rights reserved.
 */
public class ReturnVoicePage extends VelocityPageFileFather4Email 
{
	private long rp_id;
	private long oid;
	
	public ReturnVoicePage(long rp_id,long oid)
	{
		this.rp_id = rp_id;
		this.oid = oid;
	}

	public Template getTemplate(VelocityEngine ve,Context context,String encodeing) 
		throws Exception 
	{
		OrderMgr orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
		CatalogMgr catalogMgr = (CatalogMgr)MvcUtil.getBeanFromContainer("catalogMgr");
		
		DBRow returnProduct = orderMgr.getDetailReturnProductByRpId(this.rp_id);
		DBRow returnSubProducts[] = orderMgr.getReturnProductSubItemsByRpId( rp_id,null);
		DBRow detailOrder = orderMgr.getDetailPOrderByOid(oid);
		DBRow detailCatalog = catalogMgr.getDetailProductStorageCatalogById(returnProduct.get("ps_id", 0l));
		
		context.put("returnSubProducts", returnSubProducts);
		context.put("rp_id", this.rp_id);
		context.put("contact", detailCatalog.getString("contact"));
		context.put("address", StringUtil.ascii2Html(detailCatalog.getString("address")));
		context.put("phone", detailCatalog.getString("phone"));
		context.put("ps_id", returnProduct.get("ps_id", 0l));
		
		context.put("name", detailOrder.getString("address_name"));
		
		Template t = ve.getTemplate("return_product/return_invoice.html",encodeing);

		return(t);
	}
	

	
	
}





