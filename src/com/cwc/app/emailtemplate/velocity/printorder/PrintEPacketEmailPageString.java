package com.cwc.app.emailtemplate.velocity.printorder;

public class PrintEPacketEmailPageString extends PrintWayBillFatherEmailPageString 
{

	public PrintEPacketEmailPageString(long oid, String trackingNumber)
		throws Exception 
	{
		super(oid, trackingNumber);
	}

	public String getWayBill() 
		throws Exception 
	{
		return ("EPACKET");
	}

}
