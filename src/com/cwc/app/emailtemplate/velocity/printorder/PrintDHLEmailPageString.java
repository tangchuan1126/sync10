package com.cwc.app.emailtemplate.velocity.printorder;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PrintDHLEmailPageString extends PrintWayBillFatherEmailPageString 
{
	public PrintDHLEmailPageString(long oid,String trackingNumber) 
		throws Exception 
	{
		super(oid,trackingNumber);
	}

	public String getWayBill()
		throws Exception 
	{
		return("DHL");
	}
		 
}





