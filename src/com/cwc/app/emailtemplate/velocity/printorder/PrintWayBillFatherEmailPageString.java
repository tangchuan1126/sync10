package com.cwc.app.emailtemplate.velocity.printorder;

import org.apache.velocity.context.Context;

import com.cwc.app.api.OrderMgr;
import com.cwc.app.api.qll.EmailTemplateMgr;
import com.cwc.app.page.core.VelocityEmailPageStringFather;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public abstract class PrintWayBillFatherEmailPageString extends VelocityEmailPageStringFather 
{
	private DBRow detailTemplate = null;
	private DBRow detailOrder = null;
	private OrderMgr orderMgr = null;
	private String trackingNumber;

	private String TEMPLATE_NAME = "USPS";//绑定模板
	
	public abstract String getWayBill()	throws Exception;
	
	public PrintWayBillFatherEmailPageString(long oid,String trackingNumber) 
		throws Exception
	{
		this.trackingNumber = trackingNumber;
		orderMgr = (OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
		this.detailOrder = orderMgr.getDetailPOrderByOid(oid);
	
		this.TEMPLATE_NAME = this.getWayBill();//由子类实现，设置模版名称
		
		EmailTemplateMgr emailTemplateMgr = (EmailTemplateMgr)MvcUtil.getBeanFromContainer("emailTemplateMgr"); 
		this.detailTemplate = emailTemplateMgr.getDetailTemplateByNameBusines(TEMPLATE_NAME,this.detailOrder.getString("business"));//根据模版名称和收款帐号，获得模版
	}

	public String getTemplate(Context context,String encodeing) 
		throws Exception 
	{
		context.put("name", this.detailOrder.getString("address_name"));
		context.put("address_street", this.detailOrder.getString("address_street"));
		context.put("address_city", this.detailOrder.getString("address_city"));
		context.put("address_state", this.detailOrder.getString("address_state"));
		context.put("address_zip", this.detailOrder.getString("address_zip"));
		context.put("address_country", this.detailOrder.getString("address_country"));
		context.put("ems_id", this.trackingNumber);
		context.put("oid", this.detailOrder.getString("oid"));
		
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("email_template"));
		}
	}

	public String getAuthor()
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("send_name"));
		}
	}

	public String getHost() 
		throws Exception
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("smpt_server"));
		}
	}

	public String getPassWord() 
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("send_pwd"));
		}
	}

	public int getPort()
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return(0);
		}
		else
		{
			return(Integer.parseInt(detailTemplate.getString("smpt_port")));
		}
	}

	public String getRePly()
		throws Exception 
	{
		//打印通知邮件，根据不同收款帐号，需要不同回复帐号
		return(orderMgr.getPrintedOrderReplyMail(detailOrder.getString("business")));
	}

	public String getUserName() 
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("send_id"));
		}
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}
	
	 
}





