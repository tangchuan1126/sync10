package com.cwc.app.emailtemplate.velocity.financialmanagement.zzz;

import org.apache.velocity.context.Context;
import com.cwc.app.api.qll.EmailTemplateMgr;
import com.cwc.app.iface.zzz.ApplyMoneyMgrIFace;
import com.cwc.app.iface.zzz.ApplyTransferMgrIFace;
import com.cwc.app.page.core.VelocityEmailPageStringFather;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;;

/**
 * 转账申请通知邮件模板
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class TransferMoneyEmailPageString extends VelocityEmailPageStringFather 
{
	private DBRow detailTemplate = null;
	private DBRow detailApplyTransfer[] = null;
	private DBRow detailApplyMoney[] = null;
	private ApplyTransferMgrIFace applyTransferMgr = null;
	private ApplyMoneyMgrIFace applyMoneyMgr = null;
	private DBRow userInfo=null;

	public void setUserInfo(DBRow userInfo) {
		this.userInfo = userInfo;
	}

	private String TEMPLATE_NAME = "TransferMoney";//绑定模板
	
	public TransferMoneyEmailPageString(String oid) 
		throws Exception
	{
		applyTransferMgr = (ApplyTransferMgrIFace)MvcUtil.getBeanFromContainer("proxyApplyTransferMgr");
		applyMoneyMgr = (ApplyMoneyMgrIFace)MvcUtil.getBeanFromContainer("proxyApplyMoneyMgr");
		this.detailApplyTransfer = applyTransferMgr.getApplyTransferById(oid);
		this.detailApplyMoney = applyMoneyMgr.getApplyMoneyById(detailApplyTransfer[0].getString("apply_money_id"));
		EmailTemplateMgr emailTemplateMgr = (EmailTemplateMgr)MvcUtil.getBeanFromContainer("emailTemplateMgr"); 
		this.detailTemplate = emailTemplateMgr.getDetailTemplateByName(TEMPLATE_NAME);//根据模版名称获得模版
	}

	public String getTemplate(Context context,String encodeing) 
		throws Exception 
	{
		context.put("catagory", detailApplyMoney[0].getString("categoryName"));
		context.put("name",userInfo.getString("receiver"));
		context.put("groupName", userInfo.getString("groupName"));
		context.put("senderName", userInfo.getString("employe_name"));
		context.put("msn", userInfo.getString("msn"));
		context.put("email",userInfo.getString("email"));
		context.put("transferId", detailApplyTransfer[0].get("transfer_id", 0l));
		
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("email_template"));
		}
	}

	public String getAuthor()
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("send_name"));
		}
	}

	public String getHost() 
		throws Exception
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("smpt_server"));
		}
	}

	public String getPassWord() 
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("send_pwd"));
		}
	}

	public int getPort()
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return(0);
		}
		else
		{
			return(Integer.parseInt(detailTemplate.getString("smpt_port")));
		}
	}

	public String getRePly()
		throws Exception 
	{
		//打印通知邮件，根据不同收款帐号，需要不同回复帐号
		return(detailTemplate.getString("reply_id"));
	}

	public String getUserName() 
		throws Exception 
	{
		if (detailTemplate==null)
		{
			return("");
		}
		else
		{
			return(detailTemplate.getString("send_id"));
		}
	}
}





