package com.cwc.app.emailtemplate.velocity.quote;

import org.apache.velocity.context.Context;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.QuoteMgr;
import com.cwc.app.page.core.VelocityEmailPageStringFather;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class QuoteEmailPageString extends VelocityEmailPageStringFather
{
	private DBRow detail = null;
	private DBRow detailQuoteOrder = null;
	private DBRow detailQuoteAccount = null;
	
	private long TEMPLATE_ID = 100043l;//绑定模板ID
	
	public QuoteEmailPageString(long qoid) 
		throws Exception
	{
		com.cwc.app.api.qll.EmailTemplateMgr emailTemplateMgr = (com.cwc.app.api.qll.EmailTemplateMgr)MvcUtil.getBeanFromContainer("emailTemplateMgr"); 
		detail = emailTemplateMgr.getDetailTemplate(TEMPLATE_ID);
		
		QuoteMgr quoteMgr = (QuoteMgr)MvcUtil.getBeanFromContainer("quoteMgr");
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		
		detailQuoteOrder = quoteMgr.getDetailQuoteByQoid(qoid);
		detailQuoteAccount = adminMgr.getDetailAdminByAccount(detailQuoteOrder.getString("create_account"));
	}

	public String getTemplate(Context context,String encodeing) 
		throws Exception 
	{
		context.put("create_account", detailQuoteOrder.getString("create_account"));
		context.put("address_name", detailQuoteOrder.getString("address_name"));
		context.put("month", DateUtil.getStrCurrMonth());
		context.put("day", DateUtil.getStrCurrDay());
		context.put("year", DateUtil.getStrCurrYear());
		context.put("quote_account_email", detailQuoteAccount.getString("email"));
		context.put("qoid", detailQuoteOrder.getString("qoid"));

		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("email_template"));
		}
	}

	public String getAuthor()
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("send_name"));
		}
	}
	
	public String getHost() 
		throws Exception
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("smpt_server"));
		}
	}
	
	public String getPassWord() 
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("send_pwd"));
		}
	}
	
	public int getPort()
		throws Exception 
	{
		if (detail==null)
		{
			return(0);
		}
		else
		{
			return(Integer.parseInt(detail.getString("smpt_port")));
		}
	}
	
	public String getRePly()
		throws Exception 
	{
		if (detailQuoteAccount==null)
		{
			return("");
		}
		else
		{
			return(detailQuoteAccount.getString("email"));
		}
	}
	
	public String getUserName() 
		throws Exception 
	{
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("send_id"));
		}
	}
}






