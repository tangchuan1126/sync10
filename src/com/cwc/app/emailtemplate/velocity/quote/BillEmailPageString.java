package com.cwc.app.emailtemplate.velocity.quote;

import org.apache.velocity.context.Context;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.QuoteMgr;
import com.cwc.app.api.zr.BillMgrZr;
import com.cwc.app.page.core.VelocityEmailPageStringFather;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;

public class BillEmailPageString extends VelocityEmailPageStringFather {

	private DBRow detail = null;
	private DBRow billOrder = null;
	private DBRow detailBillAccount = null;
	private long TEMPLATE_ID = 100059;//绑定模板ID
	
	
	
	public BillEmailPageString(long billId) throws Exception {
		com.cwc.app.api.qll.EmailTemplateMgr emailTemplateMgr = (com.cwc.app.api.qll.EmailTemplateMgr)MvcUtil.getBeanFromContainer("emailTemplateMgr"); 
		detail = emailTemplateMgr.getDetailTemplate(TEMPLATE_ID);
		BillMgrZr billMgrZr = (BillMgrZr)MvcUtil.getBeanFromContainer("billMgrZr");
		AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
		billOrder = billMgrZr.getBillByBillId(billId);
		detailBillAccount = adminMgr.getDetailAdmin(billOrder.get("create_adid", 0l));
	}
	@Override
	public String getTemplate(Context context, String encodeing) throws Exception {
		context.put("create_account", billOrder.getString("create_account"));
		context.put("address_name", billOrder.getString("address_name"));
		context.put("month", DateUtil.getStrCurrMonth());
		context.put("day", DateUtil.getStrCurrDay());
		context.put("year", DateUtil.getStrCurrYear());
		context.put("quote_account_email", detailBillAccount.getString("email"));
		context.put("bill_id", billOrder.getString("bill_id"));
		context.put("client_id", billOrder.getString("payer_email"));
		if (detail==null)
		{
			return("");
		}
		else
		{
			return(detail.getString("email_template"));
		}
	}
	@Override
	public String getAuthor() throws Exception {
		if (detail == null) {
			return ("");
		} else {
			return (detail.getString("send_name"));
		}
	}
	@Override
	public String getHost() throws Exception {
		if (detail == null) {
			return ("");
		} else {
			return (detail.getString("smpt_server"));
		}
	}
	@Override
	public String getPassWord() throws Exception {
		if (detail == null) {
			return ("");
		} else {
			return (detail.getString("send_pwd"));
		}
	}
	@Override
	public int getPort() throws Exception {
		if (detail == null) {
			return (0);
		} else {
			return (Integer.parseInt(detail.getString("smpt_port")));
		}
	}
	@Override
	public String getRePly() throws Exception {
		if (detailBillAccount == null) {
			return ("");
		} else {
			return (detailBillAccount.getString("email"));
		}
	}
	@Override
	public String getUserName() throws Exception {
		if (detail == null) {
			return ("");
		} else {
			return (detail.getString("send_id"));
		}
	}
	
}
