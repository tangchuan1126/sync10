package com.cwc.app.lucene.zwb;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class CheckInIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static CheckInIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static CheckInIndexMgr getInstance(){
		if (test==null){
			test = new CheckInIndexMgr();
		}
		
		return(test);
	}
	 
	public CheckInIndexMgr() 
	{
		super.initIndexPath("checkin_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);  
 	}

	private Document packetField2Document(long dlo_id,long ps_id,String gate_container_no, String gate_driver_liscense,String gate_liscense_plate, String company_name,String loadNo,String ... yms_entry_ids)
		throws Exception
	{	 
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("dlo_id",String.valueOf(dlo_id),Store.YES));
		 doc.add(new TextField("ps_id",String.valueOf(ps_id),Store.YES));
		 if(yms_entry_ids!=null && yms_entry_ids.length>0){
		 	doc.add(new TextField("merge_field",("E"+dlo_id+" "+dlo_id+" "+gate_container_no+" "+gate_driver_liscense+" "+gate_liscense_plate+" "+company_name+" "+loadNo+ " "+yms_entry_ids[0]).toLowerCase(),Store.YES));	
		 }
		 else{
		 	 doc.add(new TextField("merge_field",("E"+dlo_id+" "+dlo_id+" "+gate_container_no+" "+gate_driver_liscense+" "+gate_liscense_plate+" "+company_name+" "+loadNo).toLowerCase(),Store.YES));
		 }
		 
		 return(doc);
	}
	
	public void addIndex(long dlo_id,long ps_id,String gate_container_no, String gate_driver_liscense,String gate_liscense_plate, String company_name,String loadNo,String ... yms_entry_ids) 
		throws Exception
	{
		try
		{	 
			 Document doc = null;
			 if(yms_entry_ids!= null &&  yms_entry_ids.length>0){
			 	doc = this.packetField2Document(dlo_id,ps_id,gate_container_no,gate_driver_liscense,gate_liscense_plate,company_name,loadNo,yms_entry_ids[0]);	
			 }
			 else{
			 	doc = this.packetField2Document(dlo_id,ps_id,gate_container_no,gate_driver_liscense,gate_liscense_plate,company_name,loadNo);
			 }
			 
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CheckInIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long dlo_id,long ps_id, String gate_container_no, String gate_driver_liscense,String gate_liscense_plate, String company_name,String loadNo) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(dlo_id,ps_id,gate_container_no,gate_driver_liscense,gate_liscense_plate,company_name,loadNo);
 	 		 
			 super.updateIndex(analyzer,"dlo_id",String.valueOf(dlo_id),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CheckInIndexMgr.updateIndex",log);
		}
	}
 	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc,AdminLoginBean adminLoggerBean )
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
			
		
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("dlo_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query, page_count, sort, pc);//  (query,page_count,pc);
			
	 
			FloorCheckInMgrZwb floorCheckInMgrZwb = (FloorCheckInMgrZwb)MvcUtil.getBeanFromContainer("floorCheckInMgrZwb");
			
//			long[] billIds = new long[docs.length];
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow chekin = floorCheckInMgrZwb.selectMainByEntryId(Long.parseLong(docs[i].get("dlo_id")),adminLoggerBean.isAdministrator(),adminLoggerBean.getPs_id());
				if(chekin != null){
					list.add(chekin);
				}
				
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"CheckInIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CheckInIndexMgr.getSearchResults("+key+")",log);
		}
	}
	public void deleteIndex(long  dlo_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "dlo_id", String.valueOf(dlo_id));
		}
		catch (Exception e){
			throw new SystemException(e,"CheckInIndexMgr.deleteIndex",log);
		}
	}
	
	public DBRow[] mergeSearch(String mergeKey, String q,PageCtrl pc,AdminLoginBean adminLoginBean)
			throws Exception
		{
			try
			{
				ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
				
				QueryParser queryParser = new QueryParser(Version.LUCENE_40,mergeKey,new WhitespaceAnalyzer(Version.LUCENE_40));
				queryParser.setDefaultOperator(Operator.OR);
				Query query = queryParser.parse(q.toLowerCase());
				
				Query resultSearch = query;
				
				if (!adminLoginBean.isAdministrator())
				{
					BooleanQuery bqf = new BooleanQuery();
					bqf.add(query,BooleanClause.Occur.MUST);
					
					Query fquery = new TermQuery(new Term("ps_id",String.valueOf(adminLoginBean.getPs_id())));
			        bqf.add(fquery,BooleanClause.Occur.MUST);
			        resultSearch = bqf;
				}
				
				Document docs[] = getDocumentsBySearch(resultSearch,pc.getPageSize(),pc);
				
				
				for (int i=0; i<docs.length; i++)
				{
					DBRow row = new DBRow();
					for(IndexableField f: docs[i].getFields()){
						row.add(f.name(), f.stringValue());
					}//for
					
					resultAL.add(row);
				}
				
				return((DBRow[])resultAL.toArray(new DBRow[0]));
			}
			catch (FileNotFoundException e)
			{
				throw e;
			} 
			catch (ParseException e)
			{
				throw new SystemException(e,"IndexCore.mergeSearch("+q+")",log);
			} 
			catch (Exception e)
			{
				throw new SystemException(e,"IndexCore.mergeSearch("+q+")",log);
			}
		}
}
