package com.cwc.app.lucene.zwb;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.util.Version;

import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;

public class CheckInCarrierIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static CheckInCarrierIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static CheckInCarrierIndexMgr getInstance(){
		if (test==null){
			test = new CheckInCarrierIndexMgr();
		}
		
		return(test);
	}
	 
	public CheckInCarrierIndexMgr() 
	{
		super.initIndexPath("checkin_carrier_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);  
 	}

	private Document packetField2Document(long dlo_id,String company_name)
		throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("dlo_id",String.valueOf(dlo_id),Store.YES));
		 doc.add(new TextField("company_name",String.valueOf(company_name),Store.YES));
		 doc.add(new TextField("merge_field",company_name.toLowerCase(),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long dlo_id,String company_name) 
		throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(dlo_id,company_name);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CheckInCarrierIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long dlo_id,String company_name) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(dlo_id,company_name);
 	 		 
			 super.updateIndex(analyzer,"dlo_id",String.valueOf(dlo_id),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CheckInCarrierIndexMgr.updateIndex",log);
		}
	}
/**	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("name",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
	 
			FloorCheckInMgrZwb floorCheckInMgrZwb = (FloorCheckInMgrZwb)MvcUtil.getBeanFromContainer("floorCheckInMgrZwb");
			
//			long[] billIds = new long[docs.length];
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow chekin = floorCheckInMgrZwb.selectMainByEntryId(Long.parseLong(docs[i].get("carrierId")));
				
				list.add(chekin);
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"CheckInCarrierIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CheckInCarrierIndexMgr.getSearchResults("+key+")",log);
		}
	}*/
	public void deleteIndex(long  dlo_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "dlo_id", String.valueOf(dlo_id));
		}
		catch (Exception e){
			throw new SystemException(e,"CheckInCarrierIndexMgr.deleteIndex",log);
		}
	}
}
