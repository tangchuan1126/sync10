package com.cwc.app.lucene.zwb;


import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;

import com.cwc.app.floor.api.zr.FloorBillMgrZr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class ScheduleIndexMgr  extends IndexCore {

	static Logger log = Logger.getLogger("ACTION");
	private static ScheduleIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static ScheduleIndexMgr getInstance(){
		if (test==null){
			test = new ScheduleIndexMgr();
		}
		
		return(test);
	}
	
	public ScheduleIndexMgr() {
		super.initIndexPath("schedule_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);  
 	}

//	private Document packetField2Document(long bill_id,DBRow data){
//		 Document doc = new Document();
//		 doc.add(new StringField("index", "1", Store.NO));
//		 doc.add(new TextField("bill_id",String.valueOf(bill_id), Store.YES));
//		 //合并检索字段
//		 doc.add(new TextField("merge_field", (bill_id+this.converDataToString(data)).toLowerCase(), Store.YES));
//		 return(doc);
//	}
	/**
	 * 增加索引
	 * @param bill_id
	 * @param data 把要检索的字段包装
	 * @throws Exception
	 */
	public void addIndex(long schedule_id,DBRow data) throws Exception{
		try{
			 
//			 Document doc = this.packetField2Document(bill_id,data);
//			 Document mergeDoc[] = {doc};
			 //super.addIndex(analyzer,mergeDoc);
			 String[] ids = new String[1];
			 ids[0] = String.valueOf(schedule_id);
			 
			 String[] fields = new String[1];
			 fields[0] = (schedule_id+this.converDataToString(data)).toLowerCase();
			 super.updateIndex("schedule_id","merge_field",ids, fields,0);
		}catch (Exception e){
			throw new SystemException(e,"ScheduleIndexMgr.addIndex",log);
		}
	}
	
	
	private String converDataToString(DBRow data) {
		String result=" ";
		if(!data.isEmpty()){
				Set<String> set=data.keySet();
				Iterator<String> iterator=set.iterator();
				while (iterator.hasNext()) {
					  String key=iterator.next();
					  Object value=data.get(key);
					  result+=String.valueOf(value)+" ";
				}
				
		}
		return result;
	}

	public void updateIndex(long schedule_id,DBRow data) throws Exception{
		try{
//			 Document doc = this.packetField2Document(bill_id,data);
	 		 String[] ids = new String[1];
			 ids[0] = String.valueOf(schedule_id);
			 String[] fields = new String[1];
			 fields[0] = (schedule_id+this.converDataToString(data)).toLowerCase();
			 super.updateIndex("schedule_id","merge_field",ids, fields,0);
		}catch (Exception e){
			throw new SystemException(e,"ScheduleIndexMgr.updateIndex",log);
		}
	}
	
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			
			switch (search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			
			Query query = queryParser.parse(key.toLowerCase());
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("schedule_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,sort,pc);
			
			FloorBillMgrZr floorBillMgrZr = (FloorBillMgrZr)MvcUtil.getBeanFromContainer("floorBillMgrZr");
			
			long[] billIds = new long[docs.length];
			for (int i=0; i<docs.length; i++)
			{
				billIds[i]= Long.parseLong(docs[i].get("schedule_id"));
			}
			
			return(floorBillMgrZr.getBillByBillId4Search(billIds));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"ScheduleIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"ScheduleIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	
	
	public void deleteIndex(long schedule_id)
		throws Exception
	{
		try
		{
			super.deleteIndex(analyzer, "schedule_id", String.valueOf(schedule_id));
		}
		catch (Exception e){
			throw new SystemException(e,"ScheduleIndexMgr.deleteIndex",log);
		}
	}

}
