package com.cwc.app.lucene.zr;


import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zj.FloorDeliveryMgrZJ;
import com.cwc.app.floor.api.zr.FloorApplyMoneyMgrZr;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyMgrZZZ;
import com.cwc.app.floor.api.zzz.FloorApplyTransferMgrZZZ;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.service.product.action.SwichProductAliveAction;
import com.cwc.spring.util.MvcUtil;

public class ApplyTransferIndexMgr extends IndexCore{

	static Logger log = Logger.getLogger("ACTION");
	private static ApplyTransferIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static ApplyTransferIndexMgr getInstance(){
		if (test==null){
			test = new ApplyTransferIndexMgr();
		}
		
		return(test);
	}
	 
 
	public ApplyTransferIndexMgr() {
		super.initIndexPath("apply_transfer_index");//索引存放路径
		analyzer = new IKAnalyzer();
 	}
	
	private Document packetField2Document(long transfer_id,long apply_money_id,String receiver , String receiver_information ,String receiver_account ) throws Exception{
		 Document doc = new Document();
		 doc.add(new StringField("index", "1", Store.NO));
		 doc.add(new TextField("transfer_id",String.valueOf(transfer_id),Store.YES));
		 doc.add(new TextField("merge_field", getMergeField(transfer_id,apply_money_id,receiver,receiver_information,receiver_account),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long transfer_id,long apply_money_id,String receiver , String receiver_information ,String receiver_account) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(transfer_id, apply_money_id, receiver, receiver_information, receiver_account);
			 Document docs[] = {doc};
			 super.addIndex(analyzer, docs);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ApplyTransferIndexMgr.addIndex",log);
		}
	}
	
	
	public void updateIndex(long transfer_id,long apply_money_id,String receiver , String receiver_information ,String receiver_account) throws Exception{
		try
		{
			 Document doc = this.packetField2Document(transfer_id,apply_money_id,receiver,receiver_information,receiver_account);
			 super.updateIndex(analyzer,"transfer_id",String.valueOf(transfer_id),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ApplyTransferIndexMgr.updateIndex",log);
		}
	}
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc) 
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key); 
	
			 //排序方式(订单号降序排序)
//			Sort sort =  new Sort();  
//			SortField sortField = new SortField("transfer_id",SortField.Type.LONG,true);  
//			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
			FloorApplyTransferMgrZZZ floorApplyTransferMgrZZZ = (FloorApplyTransferMgrZZZ)MvcUtil.getBeanFromContainer("floorApplyTransferMgrZZZ");
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow applyTransfer = floorApplyTransferMgrZZZ.getDetailApplyTransferById(Long.parseLong(docs[i].get("transfer_id")));
				
				list.add(applyTransfer);
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"ApplyTransferIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ApplyTransferIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	
	
	public void deleteIndex(long transfer_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "transfer_id", String.valueOf(transfer_id));
		}
		catch (Exception e){
			throw new SystemException(e,"ApplyTransferIndexMgr.deleteIndex",log);
		}
	}
	
	
	private String  getMergeField(long transfer_id, long apply_money_id,String receiver, String receiver_information,String receiver_account) throws Exception {
		/**
		 * transfer_id 加上W
		 * apply_money_id 加上F
		 * 然后根据 apply_money_id 去查询apply_money表 根据associationType( 4的时候用P,6的时候用T,5的时候用association_id 去查询delivery_order获取delivery_order_number显示出来)
		 * 
		 */
		StringBuffer sb = new StringBuffer("W"+transfer_id);
		sb.append(" ").append("F").append(apply_money_id);
	 
		// 处理关联ID
		FloorApplyMoneyMgrZZZ floorApplyMoneyMgrZZZ =( FloorApplyMoneyMgrZZZ) MvcUtil.getBeanFromContainer("floorApplyMoneyMgrZZZ");
		
		DBRow[] rows = floorApplyMoneyMgrZZZ.getApplyMoneyById(apply_money_id);
		if(rows != null && rows.length > 0)
		{
			sb.append(" |");
			
			for(DBRow row : rows)
			{
 				int association_type_id = row.get("association_type_id", 0);
 				long association_id = row.get("association_id", 0l);
 				
 				if(association_id!=0)
 				{
 					switch (association_type_id) 
 					{
 						case 4:
 							sb.append(" P").append(row.get("association_id", 0l));
 						break;
 						
 						case 5:
 							sb.append(" T").append(row.get("association_id", 0l));
 						break;
 						
 						case 6:
 							sb.append(" T").append(row.get("association_id", 0l));
 						break;
 						
 						default:
 							sb.append(row.get("association_id", 0l));
 							break;
 					}
 				}
			}
			sb.append(" |");
		}
		//处理receiver,receiver_information,receiver_account
		sb.append(" ").append(receiver.replaceAll(" ", ""));
		sb.append(" ").append(receiver_account.replaceAll(" ", ""));
		//receiver_information
		
		 String[] arrayList = receiver_information.split("\n");
		 if(arrayList != null && arrayList.length > 0){
			 for(int index = 0 , count = arrayList.length ; index < count ; index++ ){
				 	String temp = arrayList[index]; 
     	 		int splitIndex =  temp.indexOf("：");
     	 		if(splitIndex == -1){
     	 			splitIndex = temp.indexOf(":");
     	 		}
     	 		if(splitIndex != -1){
     	 			sb.append(" ").append(temp.substring(splitIndex+1).replaceAll(" ","")) ;
     	 		}else{
     	 			sb.append(" ").append(temp);
     	 		}
     	 			
				  
			 }
		 }
		 else
		 {
			 if(receiver_information.length() > 0)
			 {
				 sb.append(" ").append(receiver_information);
			 }
		 }
		return sb.toString();
	}

}
