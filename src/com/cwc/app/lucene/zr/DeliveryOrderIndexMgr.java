package com.cwc.app.lucene.zr;

import java.sql.ResultSet;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;

import com.cwc.app.floor.api.zj.FloorDeliveryMgrZJ;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class DeliveryOrderIndexMgr extends IndexCore{

	static Logger log = Logger.getLogger("ACTION");
	private static DeliveryOrderIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static DeliveryOrderIndexMgr getInstance(){
		if (test==null){
			test = new DeliveryOrderIndexMgr();
		}
		
		return(test);
	}
	public DeliveryOrderIndexMgr() {
		super.initIndexPath("deliveryOrder_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);  
 	}
	
	
	private Document packetField2Document(long delivery_order_id ,String delivery_order_number,String waybill_number,String waybill_name ,String carriers){
		 Document doc = new Document();
		 doc.add(new StringField("index", "1", Store.NO));
		 doc.add(new LongField("delivery_order_id",delivery_order_id, Store.YES));
		 //合并检索字段
		 doc.add(new TextField("merge_field", (delivery_order_id +" "+ delivery_order_number+" "+waybill_number+" "+waybill_name+" "+carriers).toLowerCase(), Store.NO));
		 return(doc);
	}
	public void addIndex(long delivery_order_id ,String delivery_order_number,String waybill_number,String waybill_name ,String carriers) throws Exception{
		try{
			 Document doc = this.packetField2Document(delivery_order_id,delivery_order_number, waybill_number, waybill_name, carriers);
			 Document mergeDoc[] = {doc};
			 //super.addIndex(analyzer,mergeDoc);
			 String[] ids = new String[1];
			 ids[0] = String.valueOf(delivery_order_id);
			 
			 String[] fields = new String[1];
			 fields[0] = (delivery_order_id +" "+delivery_order_number+" "+waybill_number+" "+waybill_name+" "+carriers).toLowerCase();
			 super.updateIndex("delivery_order_id","merge_field",ids, fields,0);
		}catch (Exception e){
			throw new SystemException(e,"DeliveryOrderIndexMgr.addIndex",log);
		}
	}
	public void updateIndex(long delivery_order_id,String delivery_order_number,String waybill_number,String waybill_name ,String carriers) throws Exception{
		try{
			 Document doc = this.packetField2Document(delivery_order_id,delivery_order_number, waybill_number, waybill_name, carriers);
	 		 String[] ids = new String[1];
			 ids[0] = String.valueOf(delivery_order_id);
			 String[] fields = new String[1];
			 fields[0] = (delivery_order_id +" "+delivery_order_number+" "+waybill_number+" "+waybill_name+" "+carriers).toLowerCase();
			 super.updateIndex("delivery_order_id","merge_field",ids, fields,0);
		}catch (Exception e){
			throw new SystemException(e,"DeliveryOrderIndexMgr.updateIndex",log);
		}
	}
	
	public DBRow[] getSearchResults(String key,PageCtrl pc) throws Exception{
		try{
 
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			Query query = queryParser.parse(key+"*");
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("delivery_order_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,100,sort,pc);
			
	 
			FloorDeliveryMgrZJ floorDeliveryMgrZJ = (FloorDeliveryMgrZJ)MvcUtil.getBeanFromContainer("floorDeliveryMgrZJ");
			
			long[] billIds = new long[docs.length];
			for (int i=0; i<docs.length; i++)
			{
				billIds[i]=  Long.parseLong((docs[i].get("delivery_order_id")));
			}
			
			return(floorDeliveryMgrZJ.getBillByBillId4Search(billIds));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"DeliveryOrderIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"DeliveryOrderIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	
	public void deleteIndex(String  delivery_order_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "delivery_order_id", String.valueOf(delivery_order_id));
		}
		catch (Exception e){
			throw new SystemException(e,"DeliveryOrderIndexMgr.deleteIndex",log);
		}
	}
}
