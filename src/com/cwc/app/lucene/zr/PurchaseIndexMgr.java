package com.cwc.app.lucene.zr;


import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zr.FloorPurchaseMgrZr;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class PurchaseIndexMgr extends IndexCore{

	static Logger log = Logger.getLogger("ACTION");
	private static PurchaseIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static PurchaseIndexMgr getInstance()
	{
		if (test==null)
		{
			test = new PurchaseIndexMgr();
		}
		
		return(test);
	}
	

	
 
	public PurchaseIndexMgr() {
		super.initIndexPath("purchase_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}
	
	private Document packetField2Document(long purchase_id,String supplier_name,long apply_money_id,long[] apply_transfer_ids)
	{
		 Document doc = new Document();
		 doc.add(new StringField("index", "1", Store.NO));
		 doc.add(new TextField("purchase_id",String.valueOf(purchase_id), Store.YES));
		 
		 String applyTransFerIds = "";
		 for (int i = 0; i < apply_transfer_ids.length; i++) 
		 {
			 applyTransFerIds  += " W"+apply_transfer_ids[i];
		 }
		 //合并检索字段
		 doc.add(new TextField("merge_field", ("P"+purchase_id +" "+ supplier_name+" F"+apply_money_id+applyTransFerIds), Store.YES));
		 return(doc);
	}
	
	public void addIndex(long purchase_id,String supplier_name,long apply_money_id,long[] apply_transfer_ids) 
		throws Exception
	{
		try
		{
			 Document doc = packetField2Document(purchase_id, supplier_name, apply_money_id, apply_transfer_ids);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"PurchaseIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long purchase_id,String supplier_name,long apply_money_id,long[] apply_transfer_ids) throws Exception{
		try
		{
			 Document doc = this.packetField2Document(purchase_id, supplier_name, apply_money_id, apply_transfer_ids); 
			 super.updateIndex(analyzer,"purchase_id",String.valueOf(purchase_id), doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"PurchaseIndexMgr.updateIndex",log);
		}
	}
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
	
			 //排序方式(订单号降序排序)
//			Sort sort =  new Sort();  
//			SortField sortField = new SortField("purchase_id",SortField.Type.LONG,true);  
//			sort.setSort(sortField);
			
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
			FloorPurchaseMgr floorPurchaseMgr = (FloorPurchaseMgr)MvcUtil.getBeanFromContainer("floorPurchaseMgr");
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			
			for (int i=0; i<docs.length; i++)
			{
				DBRow purchase = floorPurchaseMgr.getPurchaseByPurchaseid(Long.parseLong(docs[i].get("purchase_id")));
				
				list.add(purchase);
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"PurchaseIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"PurchaseIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	
	public void deleteIndex(String  purchase_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "purchase_id", String.valueOf(purchase_id));
		}
		catch (Exception e){
			throw new SystemException(e,"PurchaseIndexMgr.deleteIndex",log);
		}
	}

}
