package com.cwc.app.lucene.zr;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zj.FloorApplyMoneyMgrZJ;
import com.cwc.app.floor.api.zj.FloorTransportMgrZJ;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class TransportIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static TransportIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static TransportIndexMgr getInstance(){
		if (test==null){
			test = new TransportIndexMgr();
		}
		
		return(test);
	}
	 
	public TransportIndexMgr() 
	{
		super.initIndexPath("transport_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}

	private Document packetField2Document(long transport_id,long purchase_id,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers)
		throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("transport_id",String.valueOf(transport_id),Store.YES));
		 //合并检索字段
		 String purchase = "";
		 if(purchase_id !=0)
		 {
			 purchase = "P"+purchase_id;
		 }
		 
		 FloorApplyMoneyMgrZJ floorApplyMoneyMgrZJ = (FloorApplyMoneyMgrZJ)MvcUtil.getBeanFromContainer("floorApplyMoneyMgrZJ");
		 DBRow[] applyMoneys = floorApplyMoneyMgrZJ.getApplyMoneyByAssociationIdAndType(transport_id,11);
		 DBRow[] applyTransfers  = new DBRow[0];
		 String applyMoney = "";
		 for (int i = 0; i < applyMoneys.length; i++) 
		 {
			  applyMoney += "(";
			  
			  applyMoney +="F"+applyMoneys[i].get("apply_id",0l);
			  applyTransfers = floorApplyMoneyMgrZJ.getApplyTransferForApplyMoneyId(applyMoneys[i].get("apply_id",0l));
			  for (int j = 0; j < applyTransfers.length; j++) 
			  {
				applyMoney +=" W"+applyTransfers[j].get("transfer_id",0l);
			  }
			  applyMoney = ") ";
		 }
		 doc.add(new TextField("merge_field",("T"+transport_id+" "+purchase+" "+pickUpStorage+" "+receiveStorage+" "+trackingNumber+" "+shippingCompany+" "+carriers+" "+applyMoney),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long transport_id,long purchase_id,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers) 
		throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(transport_id,purchase_id,pickUpStorage,receiveStorage,trackingNumber,shippingCompany,carriers);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"TransportIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long transport_id,long purchase_id,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(transport_id,purchase_id,pickUpStorage,receiveStorage,trackingNumber,shippingCompany,carriers);
 	 		 
			 super.updateIndex(analyzer,"transport_id",String.valueOf(transport_id),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"TransportIndexMgr.updateIndex",log);
		}
	}
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("transport_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
	 
			FloorTransportMgrZJ floorTransportMgrZJ = (FloorTransportMgrZJ)MvcUtil.getBeanFromContainer("floorTransportMgrZJ");
			
//			long[] billIds = new long[docs.length];
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow transport = floorTransportMgrZJ.getDetailTransportById(Long.parseLong((docs[i].get("transport_id"))));
				
				list.add(transport);
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"TransportIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"TransportIndexMgr.getSearchResults("+key+")",log);
		}
	}
	public void deleteIndex(long  transport_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "transport_id", String.valueOf(transport_id));
		}
		catch (Exception e){
			throw new SystemException(e,"TransportIndexMgr.deleteIndex",log);
		}
	}
}
