package com.cwc.app.lucene.zr;


import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;

 


import com.cwc.app.floor.api.zr.FloorBillMgrZr;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class BillIndexMgr  extends IndexCore {

	static Logger log = Logger.getLogger("ACTION");
	private static BillIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static BillIndexMgr getInstance(){
		if (test==null){
			test = new BillIndexMgr();
		}
		
		return(test);
	}
	
	public BillIndexMgr() {
		super.initIndexPath("bill_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);  
 	}

	private Document packetField2Document(long bill_id,String client_id,long porder_id,String payer_email,String address_name){
		 Document doc = new Document();
		 doc.add(new StringField("index", "1", Store.NO));
		 doc.add(new TextField("bill_id",String.valueOf(bill_id), Store.YES));
		 //合并检索字段
		 doc.add(new TextField("merge_field", (bill_id+" "+client_id+" "+porder_id+" "+payer_email+" "+address_name).toLowerCase(), Store.YES));
		 return(doc);
	}
	public void addIndex(long bill_id,String client_id,long porder_id,String payer_email,String address_name) throws Exception{
		try{
			 Document doc = this.packetField2Document(bill_id,client_id,porder_id,payer_email,address_name);
			 Document mergeDoc[] = {doc};
			 //super.addIndex(analyzer,mergeDoc);
			 String[] ids = new String[1];
			 ids[0] = String.valueOf(bill_id);
			 
			 String[] fields = new String[1];
			 fields[0] = (bill_id+" "+client_id+" "+porder_id+" "+payer_email+" "+address_name).toLowerCase();
			 super.updateIndex("bill_id","merge_field",ids, fields,0);
		}catch (Exception e){
			throw new SystemException(e,"BillIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long bill_id,String client_id,long porder_id,String payer_email,String address_name) throws Exception{
		try{
			 Document doc = this.packetField2Document(bill_id,client_id,porder_id,payer_email,address_name);
	 		 String[] ids = new String[1];
			 ids[0] = String.valueOf(bill_id);
			 String[] fields = new String[1];
			 fields[0] = (bill_id+" "+client_id+" "+porder_id+" "+payer_email+" "+address_name).toLowerCase();
			 super.updateIndex("bill_id","merge_field",ids, fields,0);
		}catch (Exception e){
			throw new SystemException(e,"BillIndexMgr.updateIndex",log);
		}
	}
	
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			
			switch (search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			
			Query query = queryParser.parse(key.toLowerCase());
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("bill_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,sort,pc);
			
			FloorBillMgrZr floorBillMgrZr = (FloorBillMgrZr)MvcUtil.getBeanFromContainer("floorBillMgrZr");
			
			long[] billIds = new long[docs.length];
			for (int i=0; i<docs.length; i++)
			{
				billIds[i]= Long.parseLong(docs[i].get("bill_id"));
			}
			
			return(floorBillMgrZr.getBillByBillId4Search(billIds));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"BillIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"BillIndexMgr.getSearchResults("+key+")",log);
		}
	}
	public void deleteIndex(long bill_id)
		throws Exception
	{
		try
		{
			super.deleteIndex(analyzer, "bill_id", String.valueOf(bill_id));
		}
		catch (Exception e){
			throw new SystemException(e,"BillIndexMgr.deleteIndex",log);
		}
	}

}
