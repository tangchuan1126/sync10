package com.cwc.app.lucene.zr;


import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
 
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;
 



import com.cwc.app.floor.api.zj.FloorApplyMoneyMgrZJ;
import com.cwc.app.floor.api.zr.FloorApplyMoneyMgrZr;
import com.cwc.app.floor.api.zzz.FloorApplyFundsCategoryMgrZZZ;
import com.cwc.app.floor.api.zzz.FloorApplyMoneyMgrZZZ;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class ApplyMoneyIndexMgr  extends IndexCore{

	static Logger log = Logger.getLogger("ACTION");
	private static ApplyMoneyIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static ApplyMoneyIndexMgr getInstance()
	{
		if (test==null){
			try {
				test = new ApplyMoneyIndexMgr();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return(test);
	}
	

	
 
	public ApplyMoneyIndexMgr() {
		super.initIndexPath("apply_money_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}
	
	private Document packetField2Document(long apply_id,long association_id,long creater_id,String payment_information ,String payee,int association_type_id) 
		throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index", "1", Store.NO));
		 doc.add(new TextField("apply_id",String.valueOf(apply_id),Store.YES));
		 doc.add(new TextField("merge_field",getMergeField(apply_id, association_id, creater_id ,payment_information,payee,association_type_id),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long apply_id,long association_id,long creater_id , String payment_information ,String payee,int association_type_id)
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(apply_id, association_id, creater_id, payment_information, payee,association_type_id);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ApplyMoneyIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long apply_id,long association_id,long creater_id , String payment_information ,String payee,int association_type_id) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(apply_id, association_id, creater_id, payment_information, payee,association_type_id);
	 		 super.updateIndex(analyzer,"apply_id",String.valueOf(apply_id), doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ApplyMoneyIndexMgr.updateIndex",log);
		}
	}
	
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc) throws Exception{
		try
		{
			 QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			 switch(search_mode) 
			 {
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			 }
			 Query query = queryParser.parse(key); 
	
			 //排序方式(订单号降序排序)
//			Sort sort =  new Sort();  
//			SortField sortField = new SortField("apply_id",SortField.Type.LONG,true);  
//			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
			FloorApplyMoneyMgrZJ floorApplyMoneyMgrZJ = (FloorApplyMoneyMgrZJ)MvcUtil.getBeanFromContainer("floorApplyMoneyMgrZJ");
			FloorApplyFundsCategoryMgrZZZ floorApplyFundsCategoryMgrZZZ = (FloorApplyFundsCategoryMgrZZZ)MvcUtil.getBeanFromContainer("floorApplyFundsCategoryMgrZZZ");
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow applyMoney = floorApplyMoneyMgrZJ.getApplyMoneyById(Long.parseLong(docs[i].get("apply_id")));
				if(applyMoney!=null)
				{
					DBRow data = new DBRow();
					data.add("category_id", applyMoney.get("types",0l));
	    			
					DBRow fundsCategory = floorApplyFundsCategoryMgrZZZ.getApplyMoneyCategoryByCategoryId(data);
	    			
					if(fundsCategory !=null)
					{
						applyMoney.add("categoryId",fundsCategory.getString("category_id"));
		    			applyMoney.add("categoryName",fundsCategory.getString("category_name"));
					}
	    			list.add(applyMoney);
				}
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"ApplyMoneyIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ApplyMoneyIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	
	public void deleteIndex(long apply_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "apply_id", String.valueOf(apply_id));
		}
		catch (Exception e){
			throw new SystemException(e,"ApplyMoneyIndexMgr.deleteIndex",log);
		}
	}
	
	 
	
	private String  getMergeField(long apply_id,long association_id,long creater_id , String payment_information ,String payer, int association_type_id) 
		throws Exception 
	{
		//查询出 创建人(要处理),收款信息(要处理), 已经转账的(有可能是多个ID);	
		 //合并检索字段
		 //查询出 创建人(要处理),收款信息(要处理), 已经转账的(有可能是多个ID);
		 FloorApplyMoneyMgrZr floorApplyMoneyMgrZr =( FloorApplyMoneyMgrZr) MvcUtil.getBeanFromContainer("floorApplyMoneyMgrZr");
		 DBRow userNameRow = floorApplyMoneyMgrZr.getUserNameFromAdid(creater_id);
		 String createName =( userNameRow !=null ?  userNameRow.getString("employe_name") : " ");
		 
		 DBRow[] transfer = floorApplyMoneyMgrZr.getAllTransMoneyIds(apply_id);
		 StringBuffer transferIds =  new StringBuffer();
		 String strTransfer = "";
		 if(transfer != null && transfer.length > 0 )
		 {
			 for(DBRow row :  transfer)
			 {
				 transferIds.append(" ").append("W"+row.get("transfer_id", 0l));
			 }
		 }
		 
		 if(transferIds.length() > 0)
		 {
			 transferIds = transferIds.append("|");
			 strTransfer = "|"+transferIds.toString(); 
		 }
		 else
		 {
			 strTransfer = " "; 
		 }
		 
		 String associationIdStr = "";
		 	switch (association_type_id) 
			{
				case 4:
					associationIdStr = " P"+association_id;
				break;
				
				case 5:
					associationIdStr = " T"+association_id;
				break;
				
				case 6:
					associationIdStr = " T"+association_id;
				break;
				
				default:
					associationIdStr = ""+association_id;
					break;
			}
		 
		 StringBuffer mergeField = new StringBuffer();
		 mergeField.append("F"+apply_id).append(" ").append(createName).append(" ").append(associationIdStr).append(" ").append(strTransfer);
		 mergeField.append(" ").append(payer);

		 
		 //添加收款信息
		 StringBuffer paymentInformation = new StringBuffer("");	
		 String[] arrayList = payment_information.split("\r\n");
		 if(arrayList != null && arrayList.length > 0)
		 {
			 for(int index = 0 , count = arrayList.length ; index < count ; index++ )
			 {
				String temp = arrayList[index]; 
     	 		temp = StringUtil.full2HalfChange(temp);
     	 		
     	 		int splitIndex = temp.indexOf(":");
     	 		
     	 		if(splitIndex != -1)
     	 		{
     	 			paymentInformation.append(" ").append(temp.substring(splitIndex+1).replaceAll(" ","")) ;
     	 		}
     	 		else
     	 		{
     	 			paymentInformation.append(" ").append(temp);
     	 		}
     	 			
				  
			 }
		 }
		 else
		 {
			 if(payment_information.length() > 0)
			 {
				 paymentInformation.append(" ").append(payment_information);
			 }
		 }
		 
		 mergeField.append(paymentInformation);
		
		return mergeField.toString();
	}

}
