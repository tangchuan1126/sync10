package com.cwc.app.lucene.zr;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiPhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleFragmenter;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;

public class QuestionIndexMgr extends IndexCore
{
	static Logger log = Logger.getLogger("ACTION");
	private static QuestionIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static QuestionIndexMgr getInstance()
	{
		if (test==null){
			try {
				test = new QuestionIndexMgr();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return(test);
	}
	
	
	public QuestionIndexMgr() {
		super.initIndexPath("question_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}
	
	
	private Document packetField2Document(String attach_content , String question_title , String content , long question_id , long product_id ,long product_catalog_id , long question_catalog_id) throws Exception
	{
		 Document document = new Document();
		 document.add(new StringField("question_id", question_id + "",Store.YES));
		 
		 //产品Id
		 document.add(new StringField("product_id", product_id + "",Store.YES));
		 
		 //产品线分类Id
		 document.add(new StringField("product_catalog_id", product_catalog_id + "",Store.YES));
		 
		 //问题分类Id
		 document.add(new StringField("question_catalog_id", question_catalog_id + "",Store.YES));
		  
		 document.add(new TextField("question_title", question_title, Store.YES));
	 
		 document.add(new TextField("content", content + attach_content, Store.YES));
		 
 		 return(document);
	}
	
	
	public void addIndex(String attach_content , String question_title , String content , long question_id , long product_id ,long product_catalog_id , long question_catalog_id) throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(attach_content, question_title, content, question_id, product_id,product_catalog_id,question_catalog_id);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long question_id,String question_title , String content ,String attach_content , long product_id ,long product_catalog_id , long question_catalog_id) throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(attach_content, question_title, content, question_id, product_id,product_catalog_id,question_catalog_id);
	 		 super.updateIndex(analyzer,"question_id",String.valueOf(question_id), doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.updateIndex",log);
		}
	}
	public DBRow[] getSearchResults(String keyWords,long product_id , long[] product_catalog_ids ,long[] question_catalog_ids , PageCtrl pc) throws Exception{
		DirectoryReader ireader = null ;
		IndexSearcher isearcher = null ;
		Directory	directory  =  null ;
		List<DBRow> arrayDBRow = new ArrayList<DBRow>();
		try{
			Analyzer analyzer = new IKAnalyzer();
			String INDEX_FILE_PATH = Environment.getHome()+ "/WEB-INF/index/question_index/";
			File file = new File(INDEX_FILE_PATH);
			if(!file.exists() || ( file.exists() && file.list().length < 1 )){
				return null ;
			}
			  directory = FSDirectory.open(file);
		      ireader = DirectoryReader.open(directory);
		      isearcher = new IndexSearcher(ireader);
		    
			String[] filds = new String[]{"question_title","content","attach_content"};
			//设置多个query的组合
			BooleanQuery booleanQuery = new BooleanQuery(false);
			//产品Id
			if(product_id != 0l){
				TermQuery productIdQuery = new TermQuery(new Term("product_id",""+product_id));
				booleanQuery.add(productIdQuery,Occur.MUST);
			}
			 
			//产品分类id
			if(product_catalog_ids != null && product_catalog_ids.length > 0 ){
				MultiPhraseQuery queryProductCatalogId  = new MultiPhraseQuery();
				Term[] terms = new Term[product_catalog_ids.length] ;
				for(int index = 0 , count = product_catalog_ids.length ; index < count ; index++ ){
					if(product_catalog_ids[index] != 0l){
						Term temp = new Term("product_catalog_id",product_catalog_ids[index]+"");
						terms[index] = temp ;
					}
				}
				queryProductCatalogId.add(terms);
				booleanQuery.add(queryProductCatalogId,Occur.MUST);
			}
			
			//问题分类Id
			if(question_catalog_ids != null && question_catalog_ids.length > 0 ){
				MultiPhraseQuery queryQuestionCatalogId  = new MultiPhraseQuery();
				Term[] terms = new Term[question_catalog_ids.length] ;
				for(int index = 0 , count = question_catalog_ids.length ; index < count ; index++ ){
					Term temp = new Term("question_catalog_id",question_catalog_ids[index]+"");
					terms[index] = temp ;
				}
				queryQuestionCatalogId.add(terms);
				booleanQuery.add(queryQuestionCatalogId,Occur.MUST);
			}
			
			//keyWords query
			Query queryKeywords = null ;
			if(keyWords != null && keyWords.trim().length() > 0 ){
				MultiFieldQueryParser multiQueryParser = new MultiFieldQueryParser(Version.LUCENE_40,filds,analyzer);
				multiQueryParser.setDefaultOperator(QueryParser.Operator.OR);
				queryKeywords = multiQueryParser.parse(keyWords);
				booleanQuery.add(queryKeywords,Occur.MUST);
			}
			//query.setBoost(1.0f);
			
			SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter("<b style='color:red;'>","</b>");
			 
			QueryScorer queryScorer = new QueryScorer(queryKeywords);
			
			queryScorer.setMaxDocCharsToAnalyze(1000);
			 
			Highlighter highlighter = new Highlighter(htmlFormatter, queryScorer) ;
			//设置显示的字数
			//highlighter.setTextFragmenter(new SimpleFragmenter(61));
			
 	 
 			
 			int start = pc.getPageNo() >= 1 ? (pc.getPageNo() -1) * pc.getPageSize() :0 ;
 			
 			
			ScoreDoc[] hits =   isearcher.search(booleanQuery, 100).scoreDocs;
			
			int end = Math.min(pc.getPageNo() * pc.getPageSize() , hits.length);
			//设置page里面的数据
			pc.setAllCount(hits.length);
			int pageSize = pc.getPageSize();
			int pageCount = hits.length % pageSize >= 1 ?  (hits.length / pageSize + 1) : hits.length / pageSize;
	 
			pc.setPageCount(pageCount);
			
			
			for (int i = start; i < end; i++) {
				Document hitDoc = isearcher.doc(hits[i].doc);
				DBRow temp = new DBRow();
				List<IndexableField> fields = hitDoc.getFields();
		    	for(IndexableField field : fields){
		    		if(field.name().equals("question_title")){
		    			highlighter.setTextFragmenter(new SimpleFragmenter(500));
		    		}else{
		    			highlighter.setTextFragmenter(new SimpleFragmenter(61));
		    		}
		    		//高亮内容
		    		TokenStream tokenStream = TokenSources.getTokenStream(hitDoc, field.name(), analyzer);
					String v = highlighter.getBestFragment(tokenStream, hitDoc.get(field.name()));
					if(v != null &&  v.trim().length() > 0){
						temp.add(field.name(),v);
					}else{
						//取出前200个字符
						String value = field.stringValue();
						if( (!field.name().equals("question_title")) && value.length() > 61){
							value = value.substring(0, 61) ;
							value+="...";
							
						}
						temp.add(field.name(),value);
					}
					 
		    	}
		    	
		    	arrayDBRow.add(temp);
			}
			return arrayDBRow.toArray(new DBRow[arrayDBRow.size()]);
		}catch (Exception e) {
			throw new SystemException(e, "search", log);
		}finally{
			if(ireader != null) { ireader.close(); }
			if(directory != null ){directory.close();}
		
		}
	}
 
	public void deleteIndex(long knowledge_id) throws Exception
	{
		 IndexWriter writer  = null ;
		 Directory directory = null ;
		try{
			String INDEX_FILE_PATH = Environment.getHome()+ "/WEB-INF/index/question_index/";
			 directory = FSDirectory.open(new File(INDEX_FILE_PATH)) ;
			 Analyzer analyzer = new IKAnalyzer(); 
			 IndexWriterConfig writerConfig= new IndexWriterConfig(Version.LUCENE_40,analyzer);
			 writer = new IndexWriter(directory,writerConfig);
			 Term term = new Term("question_id",knowledge_id+"");
			 writer.deleteDocuments(term);
		 
		}catch (Exception e) {
			throw new SystemException(e, "deleteIndex", log);
		}finally{
			if(writer != null){ writer.close();}
			if(directory != null){directory.close();}
		}
	}
}
