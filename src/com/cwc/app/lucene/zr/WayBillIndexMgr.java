package com.cwc.app.lucene.zr;


import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;

 


import com.cwc.app.floor.api.zr.FloorWayBillOrderMgrZR;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

/**
 * 运单索引
 * @author Administrator
 *
 */
public class WayBillIndexMgr  extends IndexCore {

	static Logger log = Logger.getLogger("ACTION");
	private static WayBillIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static WayBillIndexMgr getInstance(){
		if (test==null){
			test = new WayBillIndexMgr();
		}
		
		return(test);
	}
	
	public WayBillIndexMgr() {
		super.initIndexPath("waybill_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);  
 	}
	
	public void addIndex(long waybill_id,String tracking_number ,String client_id,String address_name, String address_street,String address_zip ,   String address_city, String address_state , String address_country) throws Exception{
		try
		{
			 // 根据waybill_id 去查询oids 
		 	FloorWayBillOrderMgrZR floorWayBillMgrZr = (FloorWayBillOrderMgrZR)MvcUtil.getBeanFromContainer("floorWayBillOrderMgrZr");
			 DBRow[] rows = floorWayBillMgrZr.getItemOidsByWayBillId(waybill_id);
			StringBuffer oids = new StringBuffer("");
			
			 if(rows != null && rows.length > 0 ){
				for(DBRow row : rows){
					oids.append(" "+row.getString("oid"));
				}
			} 
			String oidStr = oids.length() > 0 ?oids.toString().substring(1):"";
			if(oidStr.length() > 0){
				oidStr = "| " + oidStr + " |";
			}
			String[] ids = new String[1];
			ids[0] = String.valueOf(waybill_id);
			
			String[] fields = new String[1];
			fields[0] = (waybill_id+" "+oidStr+" "+tracking_number+" "+client_id+" "+address_name+" "+address_street+" "+ address_city +" "+address_state +" "+address_zip+ " "+address_country).toLowerCase();
			super.updateIndex("waybill_id","merge_field",ids, fields,0);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"WayBillIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long waybill_id,String tracking_number ,String client_id,String address_name, String address_street,String address_zip ,   String address_city, String address_state , String address_country) throws Exception{
		try{
			 // 根据waybill_id 去查询oids
			FloorWayBillOrderMgrZR floorWayBillMgrZr = (FloorWayBillOrderMgrZR)MvcUtil.getBeanFromContainer("floorWayBillOrderMgrZr");
			DBRow[] rows = floorWayBillMgrZr.getItemOidsByWayBillId(waybill_id);
			StringBuffer oids = new StringBuffer("");
			
			if(rows != null && rows.length > 0 ){
				for(DBRow row : rows){
					oids.append(","+row.getString("oid"));
				}
			}
			String oidStr = oids.length() > 0 ?oids.toString().substring(1):"";
			if(oidStr.length() > 0){
				oidStr = "| " + oidStr + " |";
			}
 	 		 String[] ids = new String[1];
			 ids[0] = String.valueOf(waybill_id);
			 String[] fields = new String[1];
			 fields[0] = (waybill_id+" "+oidStr+" "+tracking_number+" "+client_id+" "+address_name+" "+address_street+" "+ address_city +" "+address_state +" "+address_zip+ " "+address_country).toLowerCase();
			 super.updateIndex("waybill_id","merge_field",ids, fields,0);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"WayBillIndexMgr.updateIndex",log);
		}
	}
	
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc) throws Exception{
		try{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			switch (search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			
			Query query = queryParser.parse(key);
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("waybill_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,sort,pc);
			
			FloorWayBillOrderMgrZR floorWayBillMgrZr = (FloorWayBillOrderMgrZR)MvcUtil.getBeanFromContainer("floorWayBillOrderMgrZr");
			
			long[] billIds = new long[docs.length];
			for (int i=0; i<docs.length; i++){
				billIds[i]= Long.parseLong(docs[i].get("waybill_id"));
			}
			
			return(floorWayBillMgrZr.getWayBillBySearchKey(billIds));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"WayBillIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"WayBillIndexMgr.getSearchResults("+key+")",log);
		}
	}
	public void deleteIndex(long bill_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "waybill_id", String.valueOf(bill_id));
		}
		catch (Exception e){
			throw new SystemException(e,"WayBillIndexMgr.deleteIndex",log);
		}
	}

}
