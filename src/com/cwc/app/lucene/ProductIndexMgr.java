package com.cwc.app.lucene;

import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;

import com.cwc.app.asynaction.AddProductIndex;
import com.cwc.app.asynaction.BatchAddProductIndex;
import com.cwc.app.asynaction.BatchUpdateProductIndex;
import com.cwc.app.asynaction.UpdateProductIndex;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zj.FloorProductCodeMgrZJ;
import com.cwc.app.util.Environment;
import com.cwc.asynchronized.ThreadManager;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;
import com.cwc.app.floor.api.zyj.service.*;
import com.cwc.app.floor.api.zyj.model.*;

public class ProductIndexMgr extends IndexCore
{
	static Logger log = Logger.getLogger("ACTION");
	private ProductCustomerSerivce productCustomerSerivce; 
	private TitleService titleService;
	
	private static ProductIndexMgr test = null;
	private Analyzer analyzer = null; 
	
	public ProductIndexMgr()
	{
		super.initIndexPath("product_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);//new SimpleAnalyzer();//new IKAnalyzer();//new WhitespaceAnalyzer();//中文分词器
		this.productCustomerSerivce = (ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce");
		this.titleService = (TitleService)MvcUtil.getBeanFromContainer("titleService");
		//super.setOptimizeThreshold(500);//每增加多少条索引进行一次索引优化
	}

	public synchronized static ProductIndexMgr getInstance()
	{
		if (test==null)
		{
			test = new ProductIndexMgr();
			
			test.setProductCustomerSerivce((ProductCustomerSerivce)MvcUtil.getBeanFromContainer("productCustomerSerivce"));
			test.setTitleService((TitleService)MvcUtil.getBeanFromContainer("titleService"));
		}
		
		return(test);
	}
	
	/**
	 * 把需要做成索引的字段封装成Document
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @return
	 * @throws Exception 
	 */
	private Document packetField2Document(long pc_id,String p_name,String p_code,long catalog_id,String unit_name,int alive) 
		throws Exception
	{
		 Document doc = new Document();
		
		 doc.add(new StringField("index", "1", Store.NO));
		 
		 doc.add(new StringField("pc_id", String.valueOf(pc_id), Store.YES));
		 doc.add(new StringField("p_name", p_name, Store.YES));
		 doc.add(new StringField("p_code", p_code, Store.YES));   
		 doc.add(new LongField("catalog_id", catalog_id, Store.YES));   
		 doc.add(new StringField("unit_name", unit_name, Store.YES));
		 doc.add(new TextField("alive",String.valueOf(alive), Store.YES));
		 
		 
		 String merge_p_name = p_name.replaceAll("/"," ");
		 
		 
		 FloorProductCodeMgrZJ floorProductCodeMgrZJ = (FloorProductCodeMgrZJ) MvcUtil.getBeanFromContainer("floorProductCodeMgrZJ");
		 
		 DBRow[] rows = floorProductCodeMgrZJ.getProductCodesByPcid(pc_id);
		 
		 String merge_p_code = "";
		 String all_code = "";
		 for (int i = 0; i < rows.length; i++) 
		 {
			 merge_p_code += " "+rows[i].getString("p_code").replaceAll("/"," ").trim();
			 
			 all_code += " "+rows[i].getString("p_code").trim();
		 }
		 doc.add(new TextField("all_code",all_code,Store.YES));
		 
		 String all_customer = "";
		 String all_title = "";
		 
		 List<Customer> list_customer = this.productCustomerSerivce.getCustomers(Integer.parseInt(pc_id+""));
		 List<Title> list_title = this.productCustomerSerivce.getTitles(Integer.parseInt(pc_id+""));
		 
		 
		 for(int i=0;i<list_customer.size();i++){
			 all_customer += " " + list_customer.get(i).getId() ;
		 }
		 
		 for(int i=0;i<list_title.size();i++){
			 all_title += " " + list_title.get(i).getId();
		 }
		 
		 doc.add(new TextField("all_customer",all_customer,Store.YES));
		 doc.add(new TextField("all_title",all_title , Store.YES));
		 
		 //整合索引字段
		 doc.add(new TextField("merge_info",(pc_id+" "+merge_p_name+" "+merge_p_code).toLowerCase(), Store.NO));   
		 
		 return(doc);
	}

	/**
	 * 增加索引
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void addIndex(long pc_id,String p_name,String p_code,long catalog_id,String unit_name,int alive)
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document( pc_id, p_name, p_code, catalog_id, unit_name,alive);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ProductIndexMgr.addIndex",log);
		}
	}
	
	/**
	 * 批量增加索引
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @param unit_name
	 * @param alive
	 * @throws Exception
	 */
	public void batchAddIndex(String pc_id[],String p_name[],String p_code[],String catalog_id[],String unit_name[],String alive[])
		throws Exception
	{
		try
		{
			for (int i=0; i<pc_id.length; i++)
			{
				 Document doc = this.packetField2Document( StringUtil.getLong(pc_id[i]), p_name[i], p_code[i],StringUtil.getLong(catalog_id[i]) ,unit_name[i],StringUtil.getInt(alive[i]));
				 Document mergeDoc[] = {doc};
				 super.addIndex(analyzer,mergeDoc);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ProductIndexMgr.batchAddIndex",log);
		}
	}
	
	/**
	 * 批量增加索引
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @param unit_name
	 * @param alive
	 * @throws Exception
	 */
	public void batchAddIndexAsyn(String pc_id[],String p_name[],String p_code[],String catalog_id[],String unit_name[],String alive[])
		throws Exception
	{
		ThreadManager.getInstance().executeInBackground( new BatchAddProductIndex(pc_id, p_name, p_code, catalog_id, unit_name, alive) );
	}

	/**
	 * 异步接口
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void addIndexAsyn(long pc_id,String p_name,String p_code,long catalog_id,String unit_name,int alive)
		throws Exception
	{
		ThreadManager.getInstance().executeInBackground(new AddProductIndex( pc_id, p_name, p_code, catalog_id,unit_name,alive));
		//ProductIndexMgr.getInstance().addIndex(pc_id, p_name, p_code, catalog_id,unit_name,alive);
	}
	
	/**
	 * 更新索引
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void updateIndex(long pc_id,String p_name,String p_code,long catalog_id,String unit_name,int alive)
		throws Exception
	{
		try
		{
			Document doc = this.packetField2Document( pc_id, p_name, p_code, catalog_id, unit_name,alive);
			super.updateIndex(analyzer, "pc_id", String.valueOf(pc_id), doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ProductIndexMgr.updateIndex",log);
		}
	}

	/**
	 * 批量更新索引
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @param unit_name
	 * @param alive
	 * @throws Exception
	 */
	public void batchUpdateIndex(String pc_id[],String p_name[],String p_code[],String catalog_id[],String unit_name[],String alive[])
		throws Exception
	{
		try
		{
			for (int i=0; i<pc_id.length; i++)
			{
				Document doc = this.packetField2Document(StringUtil.getLong(pc_id[i]), p_name[i], p_code[i],StringUtil.getLong(catalog_id[i]) ,unit_name[i],StringUtil.getInt(alive[i]));
				super.updateIndex(analyzer, "pc_id", pc_id[i], doc);
			}
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ProductIndexMgr.batchUpdateIndex",log);
		}
	}
	
	public void batchUpdateIndexAsyn(String pc_id[],String p_name[],String p_code[],String catalog_id[],String unit_name[],String alive[])
		throws Exception
	{
		ThreadManager.getInstance().executeInBackground( new BatchUpdateProductIndex(pc_id, p_name, p_code, catalog_id, unit_name, alive) );
	}
		
	/**
	 * 
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void updateIndexAsyn(long pc_id,String p_name,String p_code,long catalog_id,String unit_name,int alive)
		throws Exception
	{
		ThreadManager.getInstance().executeInBackground(new UpdateProductIndex( pc_id, p_name, p_code, catalog_id,unit_name,alive));
		//ProductIndexMgr.getInstance().updateIndex(pc_id, p_name, p_code, catalog_id,unit_name,alive);
	}
	
	/**
	 * 删除所有
	 * @param pc_id
	 * @throws Exception
	 */
	public void deleteIndex(long pc_id)
		throws Exception
	{
		try
		{
			super.deleteIndex(analyzer, "pc_id", String.valueOf(pc_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ProductIndexMgr.deleteIndex",log);
		}
	}
	
	public void deleteIndexAsyn(long pc_id)
		throws Exception
	{
		//ThreadManager.getInstance().executeInBackground(new DelProductIndex( pc_id));
		ProductIndexMgr.getInstance().deleteIndex(pc_id);
	}
	
	/**
	 * 普通搜索
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,PageCtrl pc,AdminLoginBean adminLoginBean)
		throws FileNotFoundException,Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_info",new WhitespaceAnalyzer(Version.LUCENE_40));
			queryParser.setDefaultOperator(Operator.OR);
			
			Query query = queryParser.parse(key);
			
			Query resultSearch = query;
			
			//如果是customer
			if (adminLoginBean.getCorporationType() == 1){
				
				long customer_id = adminLoginBean.getCorporationId();
			
				BooleanQuery bqf = new BooleanQuery();
				
				QueryParser parser = new QueryParser(Version.LUCENE_40,"all_customer",new WhitespaceAnalyzer(Version.LUCENE_40));
				
				parser.setDefaultOperator(Operator.OR);
				
				Query query1 = parser.parse(String.valueOf(customer_id));
				
		        bqf.add(query1,BooleanClause.Occur.MUST);
		        bqf.add(query,BooleanClause.Occur.MUST);
		        resultSearch = bqf;
			}
			
			Document docs[] = super.getDocumentsBySearch(resultSearch,100,pc);
			
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				row.add("pc_id",docs[i].get("pc_id"));
				row.add("p_name",docs[i].get("p_name"));
				row.add("catalog_id",docs[i].get("catalog_id"));
				row.add("unit_name",docs[i].get("unit_name"));
				row.add("alive",docs[i].get("alive"));
				
				String all_code = docs[i].get("all_code");
				all_code = all_code.trim().replaceAll(" ","】【");
				
				row.add("merge_info",docs[i].get("p_name")+"【"+all_code+"】");
				
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (FileNotFoundException e)
		{
			throw e;
		} 
		catch (ParseException e)
		{
			throw new SystemException(e,"ProductIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"ProductIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	/**
	 * 可以过滤搜索结果中，某个字段某些值
	 * @param key
	 * @param field
	 * @param fieldVal
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,DBRow filter,PageCtrl pc)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();

			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_info",new WhitespaceAnalyzer(Version.LUCENE_40));
			Query query = queryParser.parse(key);
			
			BooleanQuery bqf = new BooleanQuery();
			bqf.add(query,BooleanClause.Occur.MUST);
			
			/**
			 * 一个字段多值过滤
			 * 原理：单值多次条件查询合并
			 */
			if (filter!=null&&filter.getFieldNames().size()>0)
			{
				ArrayList filterNames = filter.getFieldNames();
	            for(int i=0;i<filterNames.size();i++)
	            {
	            	String filterName = filterNames.get(i).toString().toLowerCase();
	    			
	            	Query fquery = new TermQuery(new Term(filterName, filter.getString(filterName)));
	                
	            	bqf.add(fquery,BooleanClause.Occur.MUST);
	            }               
			}
			
			Document docs[] = super.getDocumentsBySearch(bqf,100,pc);
			
			
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				row.add("pc_id",docs[i].get("pc_id"));
				row.add("p_name",docs[i].get("p_name"));
				row.add("catalog_id",docs[i].get("catalog_id"));
				row.add("alive",docs[i].get("alive"));
				row.add("unit_name",docs[i].get("unit_name"));
				
				String all_code = docs[i].get("all_code");
				all_code = all_code.trim().replaceAll(" ","】【");
				
				row.add("merge_info",docs[i].get("p_name")+"【"+all_code+"】");
				
				resultAL.add(row);
			}

			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"ProductIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"ProductIndexMgr.getSearchResults("+key+")",log);
		}
	}

	/**
	 * 数据库批量转换索引必须实现
	 * 由父类数据库批量转换索引函数调用
	 * @param rs
	 * @return
	 */
	protected Document getDataBaseDocument(ResultSet rs)
		throws Exception
	{
		try
		{
			String pc_id = StringUtil.handleNull(rs.getString("pc_id"),"");
			String p_name = StringUtil.handleNull(rs.getString("p_name"),"");
			String p_code = StringUtil.handleNull(rs.getString("p_code"),"");
			String catalog_id = StringUtil.handleNull(rs.getString("catalog_id"),"");
			String unit_name = StringUtil.handleNull(rs.getString("unit_name"),"");
			String alive = StringUtil.handleNull(rs.getString("alive"),"");
			
			return( this.packetField2Document( StringUtil.getLong(pc_id), p_name, p_code, StringUtil.getLong(catalog_id), unit_name,StringUtil.getInt(alive)));
		} 
		catch (SQLException e)
		{
			throw new SystemException(e,"ProductIndexMgr.getDataBaseDocument",log);
		}
	}
	
	/**
	 * 重建索引
	 * @param rs
	 * @throws Exception
	 */
//	 public void rebuildIndexFromDataBase(ResultSet rs)
//	 	throws Exception
//	 {
//		try
//		{
//			super.rebuildIndexFromDataBase(analyzer, rs);
//		} 
//		catch (Exception e)
//		{
//			throw new SystemException(e,"ProductIndexMgr.rebuildIndexFromDataBase",log);
//		}
//	 }

	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	public static void main(String args[])
		throws Exception
	{
		ProductIndexMgr.getInstance().initIndexPath("E:\\tomcat5.5.26\\webapps\\paypal20\\WEB-INF\\index\\product_index");
		//ProductIndexMgr.getInstance().getAllIndex(new String[]{"p_name","catalog_id"});
		DBRow filter = new DBRow();
		filter.add("alive", "1");
		
		long st = System.currentTimeMillis();
		DBRow rows[] = ProductIndexMgr.getInstance().getSearchResults("h1", filter, null);
		long en = System.currentTimeMillis();
		
		for (int i=0; i<rows.length; i++)
		{
			////system.out.println(rows[i].getString("p_name")+" - "+rows[i].getString("catalog_id"));
		}
	}

	public void setProductCustomerSerivce(
			ProductCustomerSerivce productCustomerSerivce) {
		this.productCustomerSerivce = productCustomerSerivce;
	}

	public void setTitleService(TitleService titleService) {
		this.titleService = titleService;
	}


	
}



