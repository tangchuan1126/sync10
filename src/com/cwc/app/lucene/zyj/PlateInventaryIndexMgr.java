package com.cwc.app.lucene.zyj;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;

import com.cwc.app.api.zyj.CheckInMgrZyj;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class PlateInventaryIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static PlateInventaryIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static PlateInventaryIndexMgr getInstance(){
		if (test==null){
			test = new PlateInventaryIndexMgr();
		}
		return(test);
	}
	 
	public PlateInventaryIndexMgr() 
	{
		super.initIndexPath("plate_inventary_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);  
 	}

	private Document packetField2Document(long plate_inventary_id)
		throws Exception
	{
		Document doc = new Document();
		doc.add(new StringField("index","1",Store.NO));
		doc.add(new TextField("plate_inventary_id",String.valueOf(plate_inventary_id),Store.YES));
		String info = new String();
		CheckInMgrZyj checkInMgrZyj = (CheckInMgrZyj)MvcUtil.getBeanFromContainer("checkInMgrZyj");
		DBRow plateInfo = checkInMgrZyj.findPlateLevelProductInventaryById(plate_inventary_id);
		if(null != plateInfo)
		{
			info += plate_inventary_id+" ";
			info += plateInfo.getString("plate_no");
			info += plateInfo.getString("customer_id");
			
			LinkedHashMap<String, String> receiveInfoMap = checkInMgrZyj.plateInventaryHandleReceiveInfo(plateInfo.get("receive_order_type", 0), plateInfo.get("receive_order", 0L), plateInfo);
			LinkedHashMap<String, String> shippedInfoMap = checkInMgrZyj.plateInventaryHandleReceiveInfo(plateInfo.get("shipped_order_type", 0), plateInfo.get("shipped_order", 0L), plateInfo);;
			LinkedHashMap<String, String> associateMap   = checkInMgrZyj.plateInventaryHandleAssociateInfo(plateInfo.get("associate_order_type", 0), plateInfo.get("associate_order", 0L), plateInfo);
			
			if(receiveInfoMap.size() > 0)
			{
				info += receiveInfoMap.get("Receive").replaceAll("/"," ").trim()+" ";
			}
			if(shippedInfoMap.size() > 0)
			{
				info += shippedInfoMap.get("Shipped").replaceAll("/"," ").trim()+" ";
			}
			if(associateMap.size() > 0)
			{
				info += associateMap.get("Associate").replaceAll("/"," ").trim()+" ";
			}
		}
		doc.add(new TextField("merge_field",info.toLowerCase(),Store.YES));
		return(doc);
	}
	
	public void addIndex(long plate_inventary_id) 
		throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(plate_inventary_id);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"PlateInventaryIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long plate_inventary_id) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(plate_inventary_id);
 	 		 
			 super.updateIndex(analyzer,"plate_inventary_id",String.valueOf(plate_inventary_id),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"PlateInventaryIndexMgr.updateIndex",log);
		}
	}
 	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc )
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("plate_inventary_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
	 
			CheckInMgrZyj checkInMgrZyj = (CheckInMgrZyj)MvcUtil.getBeanFromContainer("checkInMgrZyj");
			
//			long[] billIds = new long[docs.length];
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = null;
				DBRow[] plateInfo = checkInMgrZyj.findPlateLevelProductInventary(0, 0L, 0, 0L, 0L, "", "", ""
						, Long.parseLong(docs[i].get("plate_inventary_id")), 0, 0L, 0, 0, null);
				if(plateInfo.length >0)
				{
					row = plateInfo[0];
				}
				if(plateInfo != null){
					list.add(row);
				}
			}
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"PlateInventaryIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"PlateInventaryIndexMgr.getSearchResults("+key+")",log);
		}
	}
	public void deleteIndex(long  adid)throws Exception{
		try{
			super.deleteIndex(analyzer, "plate_inventary_id", String.valueOf(adid));
		}
		catch (Exception e){
			throw new SystemException(e,"PlateInventaryIndexMgr.deleteIndex",log);
		}
	}
}
