package com.cwc.app.lucene.zyj;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zj.FloorApplyMoneyMgrZJ;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class B2BOrderIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static B2BOrderIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static B2BOrderIndexMgr getInstance(){
		if (test==null)
		{
			test = new B2BOrderIndexMgr();
		}
		return(test);
	}
	 
	public B2BOrderIndexMgr() 
	{
		super.initIndexPath("b2b_order_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}

	private Document packetField2Document(long b2b_oid,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers) throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("b2b_oid",String.valueOf(b2b_oid),Store.YES));
		 FloorApplyMoneyMgrZJ floorApplyMoneyMgrZJ = (FloorApplyMoneyMgrZJ)MvcUtil.getBeanFromContainer("floorApplyMoneyMgrZJ");
		 DBRow[] applyMoneys = floorApplyMoneyMgrZJ.getApplyMoneyByAssociationIdAndType(b2b_oid,11);
		 DBRow[] applyTransfers  = new DBRow[0];
		 String applyMoney = "";
		 for (int i = 0; i < applyMoneys.length; i++) 
		 {
			  applyMoney += "(";
			  
			  applyMoney +="F"+applyMoneys[i].get("apply_id",0l);
			  applyTransfers = floorApplyMoneyMgrZJ.getApplyTransferForApplyMoneyId(applyMoneys[i].get("apply_id",0l));
			  for (int j = 0; j < applyTransfers.length; j++) 
			  {
				applyMoney +=" W"+applyTransfers[j].get("transfer_id",0l);
			  }
			  applyMoney = ") ";
		 }
		 doc.add(new TextField("merge_field",("B"+b2b_oid+" "+pickUpStorage+" "+receiveStorage+" "+trackingNumber+" "+shippingCompany+" "+carriers+" "+applyMoney),Store.YES));
		 return(doc);
	}
	
	private Document packetField2Document(long b2b_oid,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers, String dn, String po) throws Exception
	{
		Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("b2b_oid",String.valueOf(b2b_oid),Store.YES));
		 String applyMoney = "";
		 /*
		 FloorApplyMoneyMgrZJ floorApplyMoneyMgrZJ = (FloorApplyMoneyMgrZJ)MvcUtil.getBeanFromContainer("floorApplyMoneyMgrZJ");
		 DBRow[] applyMoneys = floorApplyMoneyMgrZJ.getApplyMoneyByAssociationIdAndType(b2b_oid,11);
		 DBRow[] applyTransfers  = new DBRow[0];
		
		 for (int i = 0; i < applyMoneys.length; i++) 
		 {
			  applyMoney += "(";
			  
			  applyMoney +="F"+applyMoneys[i].get("apply_id",0l);
			  applyTransfers = floorApplyMoneyMgrZJ.getApplyTransferForApplyMoneyId(applyMoneys[i].get("apply_id",0l));
			  for (int j = 0; j < applyTransfers.length; j++) 
			  {
				applyMoney +=" W"+applyTransfers[j].get("transfer_id",0l);
			  }
			  applyMoney = ") ";
		 }
		 */
		 doc.add(new TextField("merge_field",("B"+b2b_oid+" "+pickUpStorage+" "+receiveStorage+" "+trackingNumber+" "+shippingCompany+" "+carriers+" "+applyMoney +" "+dn+" "+po),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long b2b_oid,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers) 
		throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(b2b_oid,pickUpStorage,receiveStorage,trackingNumber,shippingCompany,carriers);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer, mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"B2BOrderIndexMgr.addIndex",log);
		}
	}
	
	public void mageIndexs(DBRow[] rows, String psid) {
		synchronized(psid) {
			for (DBRow row : rows) {
				try {
					this.updateIndex(
						row.get("b2b_oid", 0L),
						row.getString("send_psname"),
						row.getString("receive_psname"),
						row.getString("b2b_order_waybill_number"),
						row.getString("b2b_order_waybill_name"),
						row.getString("carriers"),
						row.getString("customer_dn"),
						row.getString("retail_po")
					);
					
				} catch (Exception e) {
					try {
						this.addIndex(
							row.get("b2b_oid", 0L),
							row.getString("send_psname"),
							row.getString("receive_psname"),
							row.getString("b2b_order_waybill_number"),
							row.getString("b2b_order_waybill_name"),
							row.getString("carriers"),
							row.getString("customer_dn"),
							row.getString("retail_po")
						);
					} catch (Exception e2) {
						
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @param b2b_oid
	 * @param pickUpStorage
	 * @param receiveStorage
	 * @param trackingNumber
	 * @param shippingCompany
	 * @param carriers
	 * @param dn Deliver Notes
	 * @param po 采购单号
	 * @throws Exception
	 */
	public void addIndex(long b2b_oid,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers, String dn, String po) 
		throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(b2b_oid,pickUpStorage,receiveStorage,trackingNumber,shippingCompany,carriers, dn, po);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"B2BOrderIndexMgr.addIndex",log);
		}
		
	}
	
	public void updateIndex(long b2b_oid,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(b2b_oid,pickUpStorage,receiveStorage,trackingNumber,shippingCompany,carriers);
			 super.updateIndex(analyzer,"b2b_oid",String.valueOf(b2b_oid),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"B2BOrderIndexMgr.updateIndex",log);
		}
	}
	
	/**
	 * 
	 * @param b2b_oid
	 * @param pickUpStorage
	 * @param receiveStorage
	 * @param trackingNumber
	 * @param shippingCompany
	 * @param carriers
	 * @param dn
	 * @param po
	 * @throws Exception
	 */
	public void updateIndex(long b2b_oid,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers, String dn, String po) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(b2b_oid,pickUpStorage,receiveStorage,trackingNumber,shippingCompany,carriers,dn,po);
			 super.updateIndex(analyzer,"b2b_oid",String.valueOf(b2b_oid),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"B2BOrderIndexMgr.updateIndex",log);
		}
	}
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();
			SortField sortField = new SortField("b2b_oid",SortField.Type.LONG,true);
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
	 
			FloorB2BOrderMgrZyj floorB2BOrderMgrZyj = (FloorB2BOrderMgrZyj)MvcUtil.getBeanFromContainer("floorB2BOrderMgrZyj");
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow b2bOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(Long.parseLong((docs[i].get("b2b_oid"))));
				list.add(b2bOrder);
			}
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"B2BOrderIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"B2BOrderIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	public void deleteIndex(long b2b_oid)throws Exception{
		try{
			super.deleteIndex(analyzer, "b2b_oid", String.valueOf(b2b_oid));
		}
		catch (Exception e){
			throw new SystemException(e,"B2BOrderIndexMgr.deleteIndex",log);
		}
	}
}
