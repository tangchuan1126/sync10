package com.cwc.app.lucene.zyj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;

import com.cwc.app.floor.api.sbb.FloorAccountMgrSbb;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class AdminIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static AdminIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static AdminIndexMgr getInstance(){
		if (test==null){
			test = new AdminIndexMgr();
		}
		
		return(test);
	}
	 
	public AdminIndexMgr() 
	{
		super.initIndexPath("admin_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);  
 	}

	private Document packetField2Document(long adid, String account, String employe_name,String email,String mobilePhone,String register_token)
		throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("adid",String.valueOf(adid),Store.YES));
		 doc.add(new TextField("merge_field",(adid+" "+account+" "+employe_name+" "+email+" "+mobilePhone+" "+register_token).toLowerCase(),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long adid, String account, String employe_name,String email,String mobilePhone,String register_token) 
		throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(adid, account, employe_name,email,mobilePhone,register_token);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"AdminIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long adid, String account, String employe_name,String email,String mobilePhone,String register_token) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(adid, account, employe_name,email,mobilePhone,register_token);
 	 		 
			 super.updateIndex(analyzer,"adid",String.valueOf(adid),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"AdminIndexMgr.updateIndex",log);
		}
	}
 	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc )
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("adid",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
	 
			FloorAccountMgrSbb floorAccountMgrSbb = (FloorAccountMgrSbb)MvcUtil.getBeanFromContainer("floorAccountMgr");
			
//			long[] billIds = new long[docs.length];
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				Map<String,Object> parameter = new HashMap<String,Object>();
				parameter.put("searchConditions", docs[i].get("adid"));
				parameter.put("searchDeptment", "");
				parameter.put("searchPost", "");
				parameter.put("searchWarehouse", "");
				parameter.put("searchArea", "");
				parameter.put("searchLocation", "");
				
				parameter.put("searchFingerprint", "");
				parameter.put("searchToken", "");
				parameter.put("searchPermission", "");
				parameter.put("searchTitle", "");
				parameter.put("searchUploadPhoto", "");
				parameter.put("searchShield", "");
				
				DBRow[] admins = floorAccountMgrSbb.getAccountList(parameter);
				DBRow admin = null;
				if(admins.length > 0)
				{
					admin = admins[0];
				}
				if(admin != null){
					list.add(admin);
				}
			}
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"AdminIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"AdminIndexMgr.getSearchResults("+key+")",log);
		}
	}
	public void deleteIndex(long  adid)throws Exception{
		try{
			super.deleteIndex(analyzer, "adid", String.valueOf(adid));
		}
		catch (Exception e){
			throw new SystemException(e,"AdminIndexMgr.deleteIndex",log);
		}
	}
}
