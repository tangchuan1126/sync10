package com.cwc.app.lucene.zyj;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;

import com.cwc.app.floor.api.fa.FloorCustomerMgr;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.DBUtilAutoTran_Optm;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

/**
 * @author Administrator
 *
 */
public class CustomerIndexMgr extends IndexCore {
	
	static Logger log = Logger.getLogger("ACTION");
	private static CustomerIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static CustomerIndexMgr getInstance(){
		if (test==null){
			test = new CustomerIndexMgr();
		}
		return(test);
	}
	 
	public CustomerIndexMgr() 
	{
		super.initIndexPath("customer_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);  
 	}
//	customer_key customer_id customer_name house_address state
	private Document packetField2Document(long customer_key ,String customer_id ,String customer_name,String send_house_number,String send_pro_input)
		throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("customer_key",String.valueOf(customer_key),Store.YES));
		 doc.add(new TextField("merge_field",(customer_key+" "+customer_id+" "+customer_name+" "+send_house_number+" "+send_pro_input).toLowerCase(),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long customer_key ,String customer_id ,String customer_name,String send_house_number,String send_pro_input) 
		throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(customer_key ,customer_id ,customer_name,send_house_number,send_pro_input);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CustomerIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long customer_key ,String customer_id ,String customer_name,String send_house_number,String send_pro_input) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(customer_key ,customer_id ,customer_name,send_house_number,send_pro_input);
 	 		 
			 super.updateIndex(analyzer,"customer_key",String.valueOf(customer_key),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CustomerIndexMgr.updateIndex",log);
		}
	}
	
 	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc )
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
			
			 //排序方式(customer_key降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("customer_key",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);

			FloorCustomerMgr customerMgr = (FloorCustomerMgr) MvcUtil.getBeanFromContainer("customerMgr");
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow dbRow = new DBRow();
				dbRow.add("storage_type", StorageTypeKey.CUSTOMER);
				dbRow.add("customer_key", docs[i].get("customer_key"));
				DBRow[] customers = customerMgr.getAllCustomerList(dbRow, pc);
				DBRow customer = null;
				if(customers.length > 0)
				{
					customer = customers[0];
				}
				if(customer != null){
					list.add(customer);
				}
			}
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"CustomerIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"CustomerIndexMgr.getSearchResults("+key+")",log);
		}
	}
 	
}
