package com.cwc.app.lucene.zyj;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.iface.zyj.ProprietaryMgrIFaceZyj;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class TitleIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static TitleIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static TitleIndexMgr getInstance(){
		if (test==null)
		{
			test = new TitleIndexMgr();
		}
		return(test);
	}
	 
	public TitleIndexMgr() 
	{
		super.initIndexPath("title_index");//索引存放路径
		//analyzer = new IKAnalyzer();
		
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
 	}

	private Document packetField2Document(long title_id) throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("title_id",String.valueOf(title_id),Store.YES));
		 
		 ProprietaryMgrIFaceZyj proprietaryMgrZyj = (ProprietaryMgrIFaceZyj)MvcUtil.getBeanFromContainer("proxyProprietaryMgrZyj");
		 DBRow titleRow = proprietaryMgrZyj.findProprietaryByTitleId(title_id);
		 
		 String titleName = "";
		 if(null != titleRow)
		 {
			 titleName = titleRow.getString("title_name");
		 }
		 
		 String adminUsers = "";
		 
		 DBRow[] adminRows = proprietaryMgrZyj.findAdminsByTitleId(true, 0L, title_id, 0, 0, null, null);
		 for (int i = 0; i < adminRows.length; i++) 
		 {
			 adminUsers += " "+adminRows[i].getString("employe_name");
		 }
		 
		 String productLines = "";
		 DBRow[] pcLineRows = proprietaryMgrZyj.findProductLinesByTitleId(true, 0L, "", title_id+"", 0, 0, null, null);
		 for (int i = 0; i < pcLineRows.length; i++)
		 {
			 productLines += " "+pcLineRows[i].getString("name");
		 }
		 
		 String productCategorys = "";
		 DBRow[] pcCataRows = proprietaryMgrZyj.findProductCatagorysByTitleId(true, 0L, "","", "", title_id+"", 0, 0, null, null);
		 for (int i = 0; i < pcCataRows.length; i++)
		 {
			 productCategorys += " "+pcCataRows[i].getString("title");
		 }
		 
		 String productNames = "";
		 DBRow[] productRows = proprietaryMgrZyj.findProductsByTitleId(true, 0L, 0L,null, title_id, 0, 0, null, null);
		 for (int i = 0; i < productRows.length; i++)
		 {
			 productNames += " "+productRows[i].getString("p_name");
		 }
		 
		 doc.add(new TextField("merge_field",(titleName),Store.YES));	//+" "+adminUsers+" "+productLines+" "+productCategorys+" "+productNames
		 return(doc);
	}
	
	public void addIndex(long title_id) 
		throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(title_id);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"TitleIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long title_id) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(title_id);
			 super.updateIndex(analyzer,"title_id",String.valueOf(title_id),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"TitleIndexMgr.updateIndex",log);
		}
	}
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
//			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40)); //IKAnalyzer()
//			switch(search_mode) 
//			{
//				case 1:
//					queryParser.setDefaultOperator(Operator.AND);
//				break;
//				case 2:
//					queryParser.setDefaultOperator(Operator.OR);
//				break;
//			}
//			Query query = queryParser.parse(key);
//			 //排序方式(订单号降序排序)
//			Sort sort =  new Sort();  
//			SortField sortField = new SortField("title_id",SortField.Type.LONG,true);  
//			sort.setSort(sortField);
//			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
			
			
			
			
			
			
			
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			
			Query query = queryParser.parse(key);
			
			Query resultSearch = query;
			
			if(isNumeric(key)){
				System.out.println(key);
				BooleanQuery bqf = new BooleanQuery();
				
				QueryParser parser = new QueryParser(Version.LUCENE_40,"title_id",new WhitespaceAnalyzer(Version.LUCENE_40));
				
				parser.setDefaultOperator(Operator.OR);
				
				Query query1 = parser.parse(String.valueOf(key));
				
		        bqf.add(query1,BooleanClause.Occur.SHOULD);
		        bqf.add(query,BooleanClause.Occur.SHOULD);
		        resultSearch = bqf;
			}
			
			Sort sort =  new Sort();  
			SortField sortField = new SortField("title_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(resultSearch,page_count,pc);
			
			
			
			
			
	 
			FloorProprietaryMgrZyj floorProprietaryMgrZyj = (FloorProprietaryMgrZyj)MvcUtil.getBeanFromContainer("floorProprietaryMgrZyj");
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow titleRow = floorProprietaryMgrZyj.findProprietaryByTitleId(Long.parseLong((docs[i].get("title_id"))));
				list.add(titleRow);
			}
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"TitleIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"TitleIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	public static boolean isNumeric(String str){  
		   for(int i=str.length();--i>=0;){  
		      int chr=str.charAt(i);  
		      if(chr<48 || chr>57)  
		         return false;  
		   }  
		   return true;  
		}   
	
	public void deleteIndex(long  title_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "title_id", String.valueOf(title_id));
		}
		catch (Exception e){
			throw new SystemException(e,"TitleIndexMgr.deleteIndex",log);
		}
	}
	
	
	public DBRow[] getTitleByEso(String key,int count) throws Exception
		{
			try
			{
				QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
				
				queryParser.setDefaultOperator(Operator.AND);
				
				Query query = queryParser.parse(key);
				 //排序方式(订单号降序排序)
				Sort sort =  new Sort();  
				SortField sortField = new SortField("title_id",SortField.Type.LONG,true);  
				sort.setSort(sortField);
				Document docs[] = super.getDocumentsBySearch(query,count);
		 
				FloorProprietaryMgrZyj floorProprietaryMgrZyj = (FloorProprietaryMgrZyj)MvcUtil.getBeanFromContainer("floorProprietaryMgrZyj");
				
				ArrayList<DBRow> list = new ArrayList<DBRow>();
				for (int i=0; i<docs.length; i++) {
					DBRow titleRow = floorProprietaryMgrZyj.findProprietaryByTitleId(Long.parseLong((docs[i].get("title_id"))));
					list.add(titleRow);
				}
				return(list.toArray(new DBRow[0]));
			}
			catch (Exception e)
			{
				throw new SystemException(e,"TitleIndexMgr.getSearchResults("+key+")",log);
			}
		}
}
