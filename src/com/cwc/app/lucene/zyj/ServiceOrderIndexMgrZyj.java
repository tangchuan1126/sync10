package com.cwc.app.lucene.zyj;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zyj.FloorServiceOrderMgrZyj;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class ServiceOrderIndexMgrZyj extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static ServiceOrderIndexMgrZyj test = null;
	private Analyzer analyzer = null;
	
	public synchronized static ServiceOrderIndexMgrZyj getInstance(){
		if (test==null){
			test = new ServiceOrderIndexMgrZyj();
		}
		
		return(test);
	}

	public ServiceOrderIndexMgrZyj() 
	{
		super.initIndexPath("service_order_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}
	
	private Document packetField2Document(long sid) throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("sid",String.valueOf(sid),Store.YES));
		 com.cwc.app.iface.zyj.ServiceOrderMgrZyjIFace serviceOrderMgrZyj = (com.cwc.app.iface.zyj.ServiceOrderMgrZyjIFace)MvcUtil.getBeanFromContainer("proxyServiceOrderMgrZyj");
		 DBRow serviceRow	= serviceOrderMgrZyj.getServiceOrderDetailBySid(sid);
		 //合并检索字段
		 long oid			= serviceRow.get("oid", 0L);
		 String widStr		= 0 != serviceRow.get("wid", 0L)?" w"+serviceRow.get("wid", 0L):"";
		 String rpIdStr		= "";
		 com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace returnProductOrderMgrZyj = (com.cwc.app.iface.zyj.ReturnProductOrderMgrZyjIFace)MvcUtil.getBeanFromContainer("proxyReturnProductOrderMgrZyj");
		 DBRow[] returnOrders = returnProductOrderMgrZyj.getReturnOrderBySid(sid);
		 if(returnOrders.length > 0)
		 {
			 rpIdStr		= " R"+returnOrders[0].get("rp_id", 0L);
		 }
		 DBRow[] billRows	= serviceOrderMgrZyj.getServiceBillOrdersBySid(sid);
		 String billIdStr	= "";
		 if(billRows.length > 0)
		 {
			 billIdStr		= " B"+billRows[0].get("bill_id", 0L);
		 }
		 doc.add(new TextField("merge_field",("S"+sid+" O"+oid+widStr+rpIdStr+billIdStr),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long sid) 
	throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(sid);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ServiceOrderIndexMgrZyj.addIndex",log);
		}
	}
	
	public void updateIndex(long sid) 
	throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(sid);
		 		 
			 super.updateIndex(analyzer,"sid",String.valueOf(sid),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ServiceOrderIndexMgrZyj.updateIndex",log);
		}
	}
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
	throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
			
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("sid",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
	 
			FloorServiceOrderMgrZyj floorServiceOrderMgrZyj = (FloorServiceOrderMgrZyj)MvcUtil.getBeanFromContainer("floorServiceOrderMgrZyj");
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow service = floorServiceOrderMgrZyj.getServiceOrderDetailBySid(Long.parseLong((docs[i].get("sid"))));
				list.add(service);
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"ServiceOrderIndexMgrZyj.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ServiceOrderIndexMgrZyj.getSearchResults("+key+")",log);
		}
	}
	
	public void deleteIndex(long  sid)throws Exception{
		try{
			super.deleteIndex(analyzer, "sid", String.valueOf(sid));
		}
		catch (Exception e){
			throw new SystemException(e,"ServiceOrderIndexMgrZyj.deleteIndex",log);
		}
	}
}
