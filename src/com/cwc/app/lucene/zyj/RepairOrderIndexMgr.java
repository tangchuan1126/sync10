package com.cwc.app.lucene.zyj;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zj.FloorApplyMoneyMgrZJ;
import com.cwc.app.floor.api.zyj.FloorRepairOrderMgrZyj;
import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class RepairOrderIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static RepairOrderIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static RepairOrderIndexMgr getInstance(){
		if (test==null){
			test = new RepairOrderIndexMgr();
		}
		
		return(test);
	}

	public RepairOrderIndexMgr() 
	{
		super.initIndexPath("repair_order_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}
	
	private Document packetField2Document(long repair_order_id,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers)
	throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("repair_order_id",String.valueOf(repair_order_id),Store.YES));
		 //合并检索字段
		 
		 FloorApplyMoneyMgrZJ floorApplyMoneyMgrZJ = (FloorApplyMoneyMgrZJ)MvcUtil.getBeanFromContainer("floorApplyMoneyMgrZJ");
		 DBRow[] applyMoneys = floorApplyMoneyMgrZJ.getApplyMoneyByAssociationIdAndType(repair_order_id,FinanceApplyTypeKey.REPAIR_ORDER);
		 DBRow[] applyTransfers  = new DBRow[0];
		 String applyMoney = "";
		 for (int i = 0; i < applyMoneys.length; i++) 
		 {
			  applyMoney += "(";
			  
			  applyMoney +="F"+applyMoneys[i].get("apply_id",0l);
			  applyTransfers = floorApplyMoneyMgrZJ.getApplyTransferForApplyMoneyId(applyMoneys[i].get("apply_id",0l));
			  for (int j = 0; j < applyTransfers.length; j++) 
			  {
				applyMoney +=" W"+applyTransfers[j].get("transfer_id",0l);
			  }
			  applyMoney = ") ";
		 }
		 doc.add(new TextField("merge_field",("R"+repair_order_id+" "+pickUpStorage+" "+receiveStorage+" "+trackingNumber+" "+shippingCompany+" "+carriers+" "+applyMoney),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long repair_order_id,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers) 
	throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(repair_order_id,pickUpStorage,receiveStorage,trackingNumber,shippingCompany,carriers);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"RepairOrderIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long repair_order_id,String pickUpStorage,String receiveStorage,String trackingNumber,String shippingCompany,String carriers) 
	throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(repair_order_id,pickUpStorage,receiveStorage,trackingNumber,shippingCompany,carriers);
		 		 
			 super.updateIndex(analyzer,"repair_order_id",String.valueOf(repair_order_id),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"RepairOrderIndexMgr.updateIndex",log);
		}
	}
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
	throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("repair_order_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
	 
			FloorRepairOrderMgrZyj floorRepairOrderMgrZyj = (FloorRepairOrderMgrZyj)MvcUtil.getBeanFromContainer("floorRepairOrderMgrZyj");
			
	//		long[] billIds = new long[docs.length];
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow repair = floorRepairOrderMgrZyj.getDetailRepairOrderById(Long.parseLong((docs[i].get("repair_order_id"))));
				
				list.add(repair);
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"RepairOrderIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"RepairOrderIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	public void deleteIndex(long  repair_order_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "repair_order_id", String.valueOf(repair_order_id));
		}
		catch (Exception e){
			throw new SystemException(e,"RepairOrderIndexMgr.deleteIndex",log);
		}
	}
}
