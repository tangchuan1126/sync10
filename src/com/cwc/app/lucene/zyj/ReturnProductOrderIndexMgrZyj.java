package com.cwc.app.lucene.zyj;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zr.FloorReturnProductMgrZr;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class ReturnProductOrderIndexMgrZyj extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static ReturnProductOrderIndexMgrZyj test = null;
	private Analyzer analyzer = null;
	
	public synchronized static ReturnProductOrderIndexMgrZyj getInstance(){
		if (test==null){
			test = new ReturnProductOrderIndexMgrZyj();
		}
		
		return(test);
	}

	public ReturnProductOrderIndexMgrZyj() 
	{
		super.initIndexPath("return_order_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}
	
	private Document packetField2Document(long rp_id) throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("rp_id",String.valueOf(rp_id),Store.YES));
		 com.cwc.app.iface.zr.ReturnProductMgrIfaceZr returnProductMgrZr = (com.cwc.app.iface.zr.ReturnProductMgrIfaceZr)MvcUtil.getBeanFromContainer("proxyReturnProductMgrZr");
		 DBRow returnRow	= returnProductMgrZr.getReturnProductByRpId(rp_id);
		 //合并检索字段
		 String oidStr		= 0 != returnRow.get("oid", 0L)?" O"+returnRow.get("oid", 0L):"";
		 String widStr		= 0 != returnRow.get("wid", 0L)?" W"+returnRow.get("wid", 0L):"";
		 String sidStr		= 0 != returnRow.get("sid", 0L)?" S"+returnRow.get("sid", 0L):"";
		 doc.add(new TextField("merge_field",("R"+rp_id+oidStr+widStr+sidStr),Store.YES));
		 return(doc);
	}
	
	public void addIndex(long rp_id) 
	throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(rp_id);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ReturnProductOrderIndexMgrZyj.addIndex",log);
		}
	}
	
	public void updateIndex(long rp_id) 
	throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(rp_id);
		 		 
			 super.updateIndex(analyzer,"rp_id",String.valueOf(rp_id),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ReturnProductOrderIndexMgrZyj.updateIndex",log);
		}
	}
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
	throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
			
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("rp_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
			FloorReturnProductMgrZr floorReturnProductMgrZr = (FloorReturnProductMgrZr)MvcUtil.getBeanFromContainer("floorReturnProductMgrZr");
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow returnRow = floorReturnProductMgrZr.getRetrunProductByRpId(Long.parseLong((docs[i].get("rp_id"))));
				list.add(returnRow);
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"ReturnProductOrderIndexMgrZyj.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ReturnProductOrderIndexMgrZyj.getSearchResults("+key+")",log);
		}
	}
	
	public void deleteIndex(long  rp_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "rp_id", String.valueOf(rp_id));
		}
		catch (Exception e){
			throw new SystemException(e,"ReturnProductOrderIndexMgrZyj.deleteIndex",log);
		}
	}
}

