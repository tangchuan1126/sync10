package com.cwc.app.lucene.zj;

import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zj.FloorQuestionMgrZJ;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class QuestionIndexMgr extends IndexCore
{
	static Logger log = Logger.getLogger("ACTION");
	 
	private static QuestionIndexMgr test = null;
	private Analyzer analyzer = null; 
	
	public QuestionIndexMgr()
	{
		super.initIndexPath("question_index");//索引存放路径
		analyzer = new IKAnalyzer();//中文分词器
		
		//super.setOptimizeThreshold(500);//每增加多少条索引进行一次索引优化
	}

	public synchronized static QuestionIndexMgr getInstance()
	{
		if (test==null)
		{
			test = new QuestionIndexMgr();
		}
		
		return(test);
	}
	
	/**
	 *  把需要做成索引的字段封装成Document
	 * @param hct_id
	 * @param title
	 * @param content
	 * @param date
	 * @return
	 */
	private Document packetField2Document(long question_id,String question_title,String content,String question_update_time,String questioner,String p_name)
	{
		 Document doc = new Document();
		
		 doc.add(new StringField("index", "1", Store.NO));
		 
		 doc.add(new LongField("question_id",question_id, Store.YES));
		 doc.add(new StringField("title", question_title, Store.YES));
		 doc.add(new StringField("questioner", questioner, Store.YES));
		 doc.add(new StringField("question_update_time", question_update_time, Store.YES));
		 doc.add(new StringField("content", content, Store.YES));
		 doc.add(new StringField("p_name",p_name, Store.YES));
		 
		 doc.add(new TextField("merge_field", question_title+" "+content+" "+question_update_time+" "+questioner, Store.NO));
		 
		 return(doc);
	}

	/**
	 * 添加索引
	 * @param question_id
	 * @param question_title
	 * @param content
	 * @param mod_question_update_time
	 * @throws Exception
	 */
	public void addIndex(long question_id,String question_title,String content,String mod_question_update_time,String questioner,String p_name)
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document( question_id,question_title, content, mod_question_update_time,questioner,p_name);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.addIndex",log);
		}
	}


	
	/**
	 * 更新索引
	 * @param question_id
	 * @param cid
	 * @param question_title
	 * @param content
	 * @param question_update_time
	 * @throws Exception
	 */
	public void updateIndex(long question_id,String question_title,String content,String question_update_time,String questioner,String p_name)
		throws Exception
	{
		try
		{
			Document doc = this.packetField2Document(question_id,question_title,content,question_update_time,questioner,p_name);
			super.updateIndex(analyzer, "question_id", String.valueOf(question_id), doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.upquestion_update_timeIndex",log);
		}
	}

	
	/**
	 * 删除索引
	 * @param pc_id
	 * @throws Exception
	 */
	public void deleteIndex(long question_id)
		throws Exception
	{
		try
		{
			super.deleteIndex(analyzer, "question_id", String.valueOf(question_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.deleteIndex",log);
		}
	}
	
	
	/**
	 * 只根据关键字搜索
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,PageCtrl pc,int question_status,long catalog_id)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();

			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query = IKQueryParser.parse("merge_field",key);
			
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			
			Query query = queryParser.parse(key);
			
			Document docs[] = super.getDocumentsBySearch(query,100,pc);
									
			FloorQuestionMgrZJ floorQuestionMgrZJ = (FloorQuestionMgrZJ)MvcUtil.getBeanFromContainer("floorQuestionMgrZJ");
			for (int i=0; i<docs.length; i++)
			{
				long question_id=Long.parseLong(docs[i].get("question_id"));
				DBRow row = floorQuestionMgrZJ.searchQuestionByIndex(question_id, catalog_id, question_status);
				if (row!=null) 
				{
					if(row.get("question_status",0)!=question_status&&question_status!=0)//问题状态不是选择状态跳出
					{
						continue;
					}
					row.add("contentindex", docs[i].get("content"));
					resultAL.add(row);
				}
			}
			
			PageCtrl keywordpc = new PageCtrl(); //为了保证页面数据总数
			keywordpc.setPageSize(pc.getPageSize());
			keywordpc.setPageNo(pc.getPageNo());
			if(docs.length>0)//搜到结果
			{
				DBRow[] keyword;
				try 
				{
					keyword = QuestionKeyIndexMgr.getInstance().getSearchResults(key,keywordpc);
				} 
				catch (FileNotFoundException e) 
				{
					QuestionKeyIndexMgr.getInstance().addIndex(key,docs.length);
					return((DBRow[])resultAL.toArray(new DBRow[0]));
				}
				if(keyword.length>0)
				{
					QuestionKeyIndexMgr.getInstance().updateIndex(key,docs.length);
				}
				else
				{
					QuestionKeyIndexMgr.getInstance().addIndex(key,docs.length);
				}
			}
			else
			{
				QuestionKeyIndexMgr.getInstance().deleteIndex(key); 
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	
	/**
	 * 根据关键字、商品名索引搜索取交集
	 * @param key
	 * @param catalog_id
	 * @param pname
	 * @param pc
	 * @param question_status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,long catalog_id,String pname,PageCtrl pc,int question_status)
	throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
	
			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query =IKQueryParser.parse("merge_field",key); 
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			
			Query query = queryParser.parse(key);
			
			Document docs[] = super.getDocumentsBySearch(query,100,pc);
			
//			Query queryN = IKQueryParser.parseMultiField(new String[]{"merge_field","p_name"},new String[]{key,pname},new Occur[]{Occur.MUST,Occur.MUST}); 
				//IKQueryParser.parse("p_name",pname);
			
			Query queryN = queryParser.parse(key);
			
			Document docsN[] = super.getDocumentsBySearch(queryN,100,pc);
			
			FloorQuestionMgrZJ floorQuestionMgrZJ = (FloorQuestionMgrZJ)MvcUtil.getBeanFromContainer("floorQuestionMgrZJ");
			long[] questions = new long[docsN.length];
			for(int i = 0;i<docsN.length;i++)
			{
				questions[i] = Long.parseLong(docsN[i].get("question_id"));
			}
			
			
			
			if (questions.length>0) 
			{
				DBRow[] keyword = QuestionKeyIndexMgr.getInstance().getSearchResults(key,pc);
				if(keyword.length>0)
				{
					QuestionKeyIndexMgr.getInstance().updateIndex(key,docs.length);
				}
				else
				{
					QuestionKeyIndexMgr.getInstance().addIndex(key,docs.length);
				}
				
				//为每个检索出的问题DBRow加上索引文本
				for (int j = 0; j < questions.length; j++) 
				{
					DBRow result = floorQuestionMgrZJ.searchQuestionByIndex(questions[j], catalog_id,question_status);//根据索引检索关键字后附加各项条件检索
					if (result!=null) 
					{
//						Query queryS = IKQueryParser.parse("question_id",result.getString("question_id"));
						QueryParser queryParserS = new QueryParser(Version.LUCENE_40,"question_id",new IKAnalyzer());
						
						Query queryS = queryParserS.parse(key);
						
						Document doc[] = super.getDocumentsBySearch(queryS,100, pc);
						result.add("contentIndex", doc[0].get("content"));
						resultAL.add(result);
					}
				}
			}
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	/**
	 * 只根据商品名搜索索引
	 * @param catalog_id
	 * @param pname
	 * @param pc
	 * @param question_status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(long catalog_id,String pname,PageCtrl pc,int question_status)
	throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
	
			 pname = super.filterWords(pname);//使用父类提供的过滤器过滤关键字的一些特殊字符
			 //String[] dd = pname.split("[^A-Za-z0-9\u4e00-\u9fa5]");
//			 Query queryN =IKQueryParser.parse("p_name",pname);
			 QueryParser queryParser = new QueryParser(Version.LUCENE_40,"p_name",new IKAnalyzer());
				
			 Query query = queryParser.parse(pname);
			 
	         Document docsN[] = super.getDocumentsBySearch(query,100,pc);
	         
			FloorQuestionMgrZJ floorQuestionMgrZJ = (FloorQuestionMgrZJ)MvcUtil.getBeanFromContainer("floorQuestionMgrZJ");
			Long[] questions = new Long[docsN.length];
			for (int i=0; i<docsN.length; i++)
			{
				long question_id=Long.parseLong(docsN[i].get("question_id"));
				questions[i] = question_id; 
			}
			
			if (questions.length>0) 
			{
				for (int j = 0; j < questions.length; j++) 
				{
					DBRow result = floorQuestionMgrZJ.searchQuestionByIndex(questions[j], catalog_id,question_status);//根据索引检索关键字后附加各项条件检索
					if (result!=null) 
					{
						//为每个检索出的问题DBRow加上索引文本
//						Query queryS = IKQueryParser.parse("question_id", result.getString("question_id"));
						QueryParser queryParserS = new QueryParser(Version.LUCENE_40,"question_id",new IKAnalyzer());
						
						Query queryS = queryParserS.parse(result.getString("question_id"));
						Document doc[] = super.getDocumentsBySearch(queryS,100,pc);
						result.add("contentIndex", doc[0].get("content"));
						resultAL.add(result);
					}
				}
			}
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+pname+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+pname+")",log);
		}
	}

	/**
	 * 可以过滤搜索结果中，某个字段某些值
	 * @param key
	 * @param field
	 * @param fieldVal
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,String field,String fieldVal[],PageCtrl pc)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
	
			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query = IKQueryParser.parse("merge_field",key);
			
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			
			Query query = queryParser.parse(key);
			
			/**
			 * 一个字段多值过滤
			 * 原理：单值多次条件查询合并
			 */
			Filter filter = null;
			if (field!=null&&fieldVal!=null&&fieldVal.length>0)
			{
	            BooleanQuery bqf = new BooleanQuery();
	            for(int i=0;i<fieldVal.length;i++)
	            {
	    			Query fquery = new TermQuery(new Term(field, fieldVal[i]));
	                bqf.add(fquery,BooleanClause.Occur.SHOULD);
	            }               
	            filter= new QueryWrapperFilter(bqf);   
			}
            
			Document docs[] = super.getDocumentsBySearch(query,filter,100,pc);
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				row.add("cid",docs[i].get("cid"));
				row.add("email",docs[i].get("email"));
				row.add("summary",docs[i].get("summary"));
				row.add("mod_question_update_time",docs[i].get("mod_question_update_time"));
				
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		}
	}

	/**
	 * 数据库批量转换索引必须实现
	 * 由父类数据库批量转换索引函数调用
	 * @param rs
	 * @return
	 */
	protected Document getDataBaseDocument(ResultSet rs)
		throws Exception
	{
		try
		{
			long question_id=Long.parseLong(StringUtil.handleNull(rs.getString("question_id"),""));
			long cid =Long.parseLong( StringUtil.handleNull(rs.getString("cid"),""));
			String question_title = StringUtil.handleNull(rs.getString("question_title"),"");
			String content = StringUtil.handleNull(rs.getString("content"),"");
			String last_mod_question_update_time = StringUtil.handleNull(rs.getString("last_mod_question_update_time"),"");
			String questioner = StringUtil.handleNull(rs.getString("questioner"),"");
			
			return null;//( this.packetField2Document(question_id,question_title,content,last_mod_question_update_time,questioner));
		} 
		catch (SQLException e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getDataBaseDocument",log);
		}
	}
}






