package com.cwc.app.lucene.zj;

import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.exception.admin.SuperAdminException;
import com.cwc.app.floor.api.zj.FloorQuestionMgrZJ;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class QuestionKeyIndexMgr extends IndexCore
{
	static Logger log = Logger.getLogger("ACTION");
	 
	private static QuestionKeyIndexMgr test = null;
	private Analyzer analyzer = null; 
	
	public QuestionKeyIndexMgr()
	{
		super.initIndexPath("question_index/keyword");//索引存放路径
		analyzer = new IKAnalyzer();//中文分词器
		
		//super.setOptimizeThreshold(500);//每增加多少条索引进行一次索引优化
	}

	public synchronized static QuestionKeyIndexMgr getInstance()
	{
		if (test==null)
		{
			test = new QuestionKeyIndexMgr();
		}
		
		return(test);
	}
	
	/**
	 *  把需要做成索引的字段封装成Document
	 * @param hct_id
	 * @param title
	 * @param content
	 * @param date
	 * @return
	 */
	private Document packetField2Document(String keyword,int count)
	{
		 Document doc = new Document();
		
		 doc.add(new StringField("index", "1", Store.NO));
		 
		 doc.add(new StringField("keyword", keyword, Store.YES));
		 doc.add(new IntField("count",count, Store.YES));
		 doc.add(new TextField("merge_field",keyword, Store.NO));
		 
		 return(doc);
	}

	/**
	 * 添加索引
	 * @param question_id
	 * @param question_title
	 * @param content
	 * @param mod_question_update_time
	 * @throws Exception
	 */
	public void addIndex(String keyword,int count)
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(keyword,count);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.addIndex",log);
		}
	}


	
	/**
	 * 更新索引
	 * @param question_id
	 * @param cid
	 * @param question_title
	 * @param content
	 * @param question_update_time
	 * @throws Exception
	 */
	public void updateIndex(String keyword,int count)
		throws Exception
	{
		try
		{
			Document doc = this.packetField2Document(keyword,count);
			super.updateIndex(analyzer, "keyword",keyword, doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.upquestion_update_timeIndex",log);
		}
	}

	
	/**
	 * 删除索引
	 * @param pc_id
	 * @throws Exception
	 */
	public void deleteIndex(String keyword)
		throws Exception
	{
		try
		{
			super.deleteIndex(analyzer, "keyword", keyword);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.deleteIndex",log);
		}
	}
	
	
	/**
	 * 普通搜索
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,PageCtrl pc)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();

			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query = IKQueryParser.parse("merge_field",key);
			
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			
			Query query = queryParser.parse(key);
			Document docs[] = super.getDocumentsBySearch(query,100,pc);
			
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				row.add("keyword",docs[i].get("keyword"));
				row.add("count",docs[i].get("count"));
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch(FileNotFoundException e)
		{
			return(new DBRow[0]);
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	
	
//	public DBRow[] getSearchResults(String key,long catalog_id,String pname,PageCtrl pc)
//	throws Exception
//	{
//		try
//		{
//			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
//	
//			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
//			
//			Query query = IKQueryParser.parse("merge_field",key);
//			Document docs[] = super.getDocumentsBySearch(query,new Sort(),100,pc);
//			FloorQuestionMgrZJ floorQuestionMgrZJ = (FloorQuestionMgrZJ)MvcUtil.getBeanFromContainer("floorQuestionMgrZJ");
//			long[] questions = new long[docs.length];
//			for (int i=0; i<docs.length; i++)
//			{
//				long question_id=Long.parseLong(docs[i].get("question_id"));
//				questions[i]=question_id;
//			}
//			
//			DBRow[] result = floorQuestionMgrZJ.searchQuestionByIndex(questions, catalog_id, pname, pc);//根据索引检索关键字后附加各项条件检索
//			//为每个检索出的问题DBRow加上索引文本
//			for(int j = 0;j<result.length;j++)
//			{
//				Query queryS = IKQueryParser.parse("question_id",result[j].getString("question_id"));
//				Document doc[] = super.getDocumentsBySearch(queryS,new Sort(),100,pc);
//				result[j].add("contentIndex",doc[0].get("content"));
//				resultAL.add(result[j]);
//			}
//			
//			return((DBRow[])resultAL.toArray(new DBRow[0]));
//		}
//		catch (ParseException e)
//		{
//			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
//		} 
//		catch (Exception e)
//		{
//			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
//		}
//	}
	

	/**
	 * 可以过滤搜索结果中，某个字段某些值
	 * @param key
	 * @param field
	 * @param fieldVal
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,String field,String fieldVal[],PageCtrl pc)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
	
			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query = IKQueryParser.parse("merge_field",key);

			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			
			Query query = queryParser.parse(key);
			/**
			 * 一个字段多值过滤
			 * 原理：单值多次条件查询合并
			 */
			Filter filter = null;
			if (field!=null&&fieldVal!=null&&fieldVal.length>0)
			{
	            BooleanQuery bqf = new BooleanQuery();
	            for(int i=0;i<fieldVal.length;i++)
	            {
	    			Query fquery = new TermQuery(new Term(field, fieldVal[i]));
	                bqf.add(fquery,BooleanClause.Occur.SHOULD);
	            }               
	            filter= new QueryWrapperFilter(bqf);   
			}
            
			Document docs[] = super.getDocumentsBySearch(query,filter,100,pc);
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				row.add("cid",docs[i].get("cid"));
				row.add("email",docs[i].get("email"));
				row.add("summary",docs[i].get("summary"));
				row.add("mod_question_update_time",docs[i].get("mod_question_update_time"));
				
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getSearchResults("+key+")",log);
		}
	}

	/**
	 * 数据库批量转换索引必须实现
	 * 由父类数据库批量转换索引函数调用
	 * @param rs
	 * @return
	 */
	protected Document getDataBaseDocument(ResultSet rs)
		throws Exception
	{
		try
		{
			long question_id=Long.parseLong(StringUtil.handleNull(rs.getString("question_id"),""));
			long cid =Long.parseLong( StringUtil.handleNull(rs.getString("cid"),""));
			String question_title = StringUtil.handleNull(rs.getString("question_title"),"");
			String content = StringUtil.handleNull(rs.getString("content"),"");
			String last_mod_question_update_time = StringUtil.handleNull(rs.getString("last_mod_question_update_time"),"");
			String questioner = StringUtil.handleNull(rs.getString("questioner"),"");
			
			return(null);
		} 
		catch (SQLException e)
		{
			throw new SystemException(e,"QuestionIndexMgr.getDataBaseDocument",log);
		}
	}
	

	 
	 
	 
	 
	 
	public static void main(String args[])
		throws Exception
	{
		QuestionKeyIndexMgr.getInstance().initIndexPath("D:\\tomcat6.0.18\\webapps\\paypal20\\WEB-INF\\index\\question_index\\keyword");
		QuestionKeyIndexMgr.getInstance().addIndex("关键字",0);
//		QuestionIndexMgr.getInstance().getAllIndex();
		DBRow results[] = QuestionKeyIndexMgr.getInstance().getSearchResults("关键", null);
		for (int i=0; i<results.length; i++)
		{
			////system.out.println(results[i].getString("keyword")+"--------"+results[i].getString("count"));
		}
	}


}






