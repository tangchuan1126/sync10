package com.cwc.app.lucene.zj;


import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;

import com.cwc.app.floor.api.zj.FloorTradeMgrZJ;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class TradeIndexMgr extends IndexCore {

	static Logger log = Logger.getLogger("ACTION");
	 
	private static TradeIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static TradeIndexMgr getInstance()
	{
		if (test==null)
		{
			test = new TradeIndexMgr();
		}
		
		return(test);
	}
	
	public TradeIndexMgr()
	{
		super.initIndexPath("trade_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40); //IKAnalyzer();//中文分词器
		
		//super.setOptimizeThreshold(500);//每增加多少条索引进行一次索引优化
	}
	
	private Document packetField2Document(long trade_id,String txn_id,String client_id,long relation_id,String payer_email,String delivery_address_name)
	{
		 Document doc = new Document();
		
		 doc.add(new StringField("index", "1", Store.NO));
		 
		 doc.add(new TextField("trade_id",String.valueOf(trade_id), Store.YES));
		 //合并检索字段
		 doc.add(new TextField("merge_field",(trade_id+" "+txn_id+" "+client_id+" "+relation_id+" "+payer_email+" "+delivery_address_name).toLowerCase(), Store.YES));
		 
		 return(doc);
	}
	
	/**
	 * 添加索引
	 * @param trade_id
	 * @param txn_id
	 * @param client_id
	 * @param relation_id
	 * @param payer_email
	 * @param delivery_address_name
	 * @throws Exception
	 */
	public void addIndex(long trade_id,String txn_id,String client_id,long relation_id,String payer_email,String delivery_address_name)
		throws Exception
	{
		try
		{
			 //super.addIndex(analyzer,mergeDoc);
			 String[] ids = new String[1];
			 ids[0] = String.valueOf(trade_id);
			 
			 String[] fields = new String[1];
			 fields[0] = (trade_id+" "+txn_id+" "+client_id+" "+relation_id+" "+payer_email+" "+delivery_address_name).toLowerCase();
			 super.updateIndex("trade_id","merge_field",ids, fields,0);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.addIndex",log);
		}
	}
	
	/**
	 * 更新索引
	 * @param trade_id
	 * @param txn_id
	 * @param client_id
	 * @param relation_id
	 * @param payer_email
	 * @param delivery_address_name
	 * @throws Exception
	 */
	public void updateIndex(long trade_id,String txn_id,String client_id,long relation_id,String payer_email,String delivery_address_name)
		throws Exception
	{
		try
		{
			Document doc = this.packetField2Document(trade_id,txn_id,client_id,relation_id,payer_email,delivery_address_name);
			 String[] ids = new String[1];
			 ids[0] = String.valueOf(trade_id);
			 
			 String[] fields = new String[1];
			 fields[0] = (trade_id+" "+txn_id+" "+client_id+" "+relation_id+" "+payer_email+" "+delivery_address_name).toLowerCase();
			 super.updateIndex("trade_id","merge_field",ids, fields,0);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.updateIndex",log);
		}
	}
	
	/**
	 * 删除所有
	 * @param pc_id
	 * @throws Exception
	 */
	public void deleteIndex(long trade_id)
		throws Exception
	{
		try
		{
			super.deleteIndex(analyzer, "trade_id", String.valueOf(trade_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.deleteIndex",log);
		}
	}
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
			//key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
	//		key = processQueryTerms(key); 
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			switch (search_mode)
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;

				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			
			Query query = queryParser.parse(key);
	
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("trade_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,sort,pc);
			
			FloorTradeMgrZJ floorTradeMgrZJ = (FloorTradeMgrZJ)MvcUtil.getBeanFromContainer("floorTradeMgrZJ");
			
			long[] tradeIds = new long[docs.length];
			for (int i=0; i<docs.length; i++)
			{
				tradeIds[i]= Long.parseLong(docs[i].get("trade_id"));
			}
			
			return(floorTradeMgrZJ.getTradeByTradeId4Search(tradeIds));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"OrderIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.getSearchResults("+key+")",log);
		}
	}
}
