package com.cwc.app.lucene.ytl;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;

public class ShipToAddressIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static ShipToAddressIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static ShipToAddressIndexMgr getInstance(){
		if (test==null){
			test = new ShipToAddressIndexMgr();
		}
		
		return(test);
	}
	
	public ShipToAddressIndexMgr() 
	{
		super.initIndexPath("ship_to_storage_index");//索引存放路径
		analyzer = new IKAnalyzer();
 	}
	
	public DBRow[] getSearchResults(String title,String address,String city,String state,PageCtrl pc) throws Exception{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
			BooleanQuery bqf = new BooleanQuery();
			
			QueryParser queryParser1 = new QueryParser(Version.LUCENE_40,"title",analyzer);
        	Query query1 = queryParser1.parse(title);
        	bqf.add(query1,BooleanClause.Occur.MUST);
        	
			QueryParser queryParser2 = new QueryParser(Version.LUCENE_40,"city",analyzer);
        	Query query2 = queryParser2.parse(city);
        	bqf.add(query2,BooleanClause.Occur.MUST);

//			QueryParser queryParser3 = new QueryParser(Version.LUCENE_40,"street",analyzer);
//        	Query query3 = queryParser3.parse(address);
//        	bqf.add(query3,BooleanClause.Occur.MUST);
//        	
//			QueryParser queryParser4 = new QueryParser(Version.LUCENE_40,"state",analyzer);
//        	Query query4 = queryParser4.parse(state);
//        	bqf.add(query4,BooleanClause.Occur.MUST);
			
			Sort sort =  new Sort();  
			SortField sortField = new SortField("sorce",SortField.Type.INT,false);  
			sort.setSort(sortField);
        	
			Document docs[] = super.getDocumentsBySearch(bqf,100,pc);
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				for(IndexableField f: docs[i].getFields()){
					row.add(f.name(), f.stringValue());
				}//for
				
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			e.printStackTrace();
			throw new SystemException(e,"ShipToAddressIndexMgr.getSearchResults()",log);
		}
		catch (Exception e)
		{	
			e.printStackTrace();
			throw new SystemException(e,"ShipToAddressIndexMgr.getSearchResults()",log);
		}
		}
	
}
