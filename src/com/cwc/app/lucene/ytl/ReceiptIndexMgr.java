package com.cwc.app.lucene.ytl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;

/**
 * 
 * @ClassName: ReceiptIndexMgr
 * @Description: RN lucene索引管理
 * @author yetl
 * @date 2015年7月14日
 *
 */
public class ReceiptIndexMgr extends IndexCore{
	static Logger log = Logger.getLogger("ACTION");
	private static ReceiptIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static ReceiptIndexMgr getInstance(){
		if (test==null)
		{
			test = new ReceiptIndexMgr();
		}
		return(test);
	}
	 
	public ReceiptIndexMgr() 
	{
		super.initIndexPath("receipt_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}

	public synchronized Document packetField2Document(long receiptID,String referenceNo,String poNo,String containerNo,String scac) throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("receipt_id",String.valueOf(receiptID),Store.YES));
		 doc.add(new TextField("merge_field",("R"+receiptID+" "+referenceNo+" "+poNo+" "+containerNo+" "+scac),Store.YES));
		 return(doc);
	}

	public synchronized void addIndex(long receiptID,String referenceNo,String poNo,String containerNo,String scac) 
		throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(receiptID,referenceNo,poNo,containerNo,scac);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ReceiptIndexMgr.addIndex",log);
		}
		
	}
	
	public synchronized void updateIndex(long receiptID,String referenceNo,String poNo,String containerNo,String scac) 
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(receiptID,referenceNo,poNo,containerNo,scac);
			 super.updateIndex(analyzer,"receipt_id",String.valueOf(receiptID),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ReceiptIndexMgr.updateIndex",log);
		}
	}
	
	public List<Long> getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();
			SortField sortField = new SortField("receipt_id",SortField.Type.LONG,true);
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
			ArrayList<Long> list = new ArrayList<Long>();
			for (int i=0; i<docs.length; i++)
			{
				list.add(Long.parseLong((docs[i].get("receipt_id"))));
			}
			return list;
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"B2BOrderIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"B2BOrderIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	public synchronized void deleteIndex(long receiptID)throws Exception{
		try{
			super.deleteIndex(analyzer, "receipt_id", String.valueOf(receiptID));
		}
		catch (Exception e){
			throw new SystemException(e,"ReceiptIndexMgr.deleteIndex",log);
		}
	}
	
	public synchronized void updateIndex(String [] ids,String [] fields) throws Exception{
		IndexWriter writer = null;
		//批量创建索引分词器
		Analyzer analyzer = new IKAnalyzer(); 
		
		try
		{
			Directory dir = this.getDirectory();   
			IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_40,analyzer);
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			iwc.setRAMBufferSizeMB(256);
			iwc.setMaxThreadStates(50);
			iwc.setMaxBufferedDocs(1000);
			try
			{
				writer = new IndexWriter(dir,iwc);
			}
			catch(java.io.FileNotFoundException fe){ //容错，更新时按需创建，Frank
				log.warn("Empty index, re-init "+dir);
				writer = new IndexWriter(dir,iwc);
			}
			
			for(int i=0; i<ids.length; i++){
				Document doc = new Document();
				String docId = ids[i];
				doc.add(new StringField("index", "1", Store.NO));
				doc.add(new TextField("receipt_id",docId, Store.YES));
				doc.add(new TextField("merge_field",fields[i].toLowerCase(),Store.YES));

				writer.updateDocument(new Term("receipt_id",docId ), doc);
			}//for
			
		}
		catch (CorruptIndexException e)
		{
			throw new SystemException(e,"ReceiptIndexMgr.updateIndex",log);
		} 
		catch (LockObtainFailedException e)
		{
			throw new SystemException(e,"ReceiptIndexMgr.updateIndex",log);
		} 
		catch (IOException e)
		{
			throw new SystemException(e,"ReceiptIndexMgr.updateIndex",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"ReceiptIndexMgr.updateIndex",log);
		}   
		finally
		{
			if(writer!=null) writer.close();
		}
	}
	
	
	public DBRow[] mergeSearch(String mergeKey, String q,PageCtrl pc) throws Exception {
		try {
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,mergeKey,new IKAnalyzer());
			queryParser.setDefaultOperator(Operator.OR);
			Query query = queryParser.parse(q.toLowerCase());

			Document docs[] = getDocumentsBySearch(query,pc.getPageSize(),pc);
				
				for (int i=0; i<docs.length; i++)
				{
					DBRow row = new DBRow();
					for(IndexableField f: docs[i].getFields()){
						row.add(f.name(), f.stringValue());
					}
					resultAL.add(row);
				}
				return((DBRow[])resultAL.toArray(new DBRow[0]));
			} catch (FileNotFoundException e) {
				throw e;
			} catch (ParseException e) {
				throw new SystemException(e,"ReceiptIndexMgr.mergeSearch("+q+")",log);
			} catch (Exception e) {
				throw new SystemException(e,"ReceiptIndexMgr.mergeSearch("+q+")",log);
			}
	}
}
