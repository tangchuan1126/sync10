package com.cwc.app.lucene.ytl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.lucene.zyj.B2BOrderIndexMgr;
import com.cwc.lucene.IndexCore;

/**
 * 
 * @ClassName: LoadIndexMgr
 * @Description: 操作load索引
 * @author yetl
 * @date 2015年3月26日
 *
 */
public class LoadIndexMgr extends IndexCore{
	private static LoadIndexMgr bean=null;
	private Analyzer analyzer;
	
	private LoadIndexMgr(){
		super.initIndexPath("load_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
	}
	
	public synchronized static LoadIndexMgr getInstance(){
		if (bean==null)
			synchronized (LoadIndexMgr.class) {
				if(bean==null)
					bean = new LoadIndexMgr();
			}

		return(bean);
	}
	
	public void addLoadIndex(String loadNo,int loadId){
		Document doc=new Document();
		doc.add(new TextField("loadNo",loadNo,Store.YES));
		doc.add(new TextField("merge_field",loadNo,Store.YES));
	}
}
