package com.cwc.app.lucene;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;

import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class OrderIndexMgr extends IndexCore
{
	static Logger log = Logger.getLogger("ACTION");
	 
	private static OrderIndexMgr test = null;
	private Analyzer analyzer = null; 
	
	public OrderIndexMgr()
	{
		super.initIndexPath(Environment.getHome()+"/WEB-INF/index/order_index");//索引存放路径
		analyzer = new WhitespaceAnalyzer(Version.LUCENE_40); //IKAnalyzer();//中文分词器
		
		//super.setOptimizeThreshold(500);//每增加多少条索引进行一次索引优化
	}

	public synchronized static OrderIndexMgr getInstance()
	{
		if (test==null)
		{
			test = new OrderIndexMgr();
		}
		
		return(test);
	}
	
	/**
	 * 把需要做成索引的字段封装成Document
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @return
	 */
	private Document packetField2Document(String address_name,String client_id,long oid,String txnid,String auctionbuyer,String itemnumber,String ems_id,String country)
	{
		 Document doc = new Document();
		
		 doc.add(new StringField("index", "1", Store.NO));
		 
		 doc.add(new TextField("oid",String.valueOf(oid),Store.YES));
		 //合并检索字段
		 doc.add(new TextField("merge_field",(oid+" "+address_name+" "+client_id+" "+txnid+" "+auctionbuyer+" "+ems_id+" "+itemnumber+" "+country).toLowerCase(), Store.YES));
		 
		 return(doc);
	}
	
	/**
	 * 增加索引
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void addIndex(String address_name,String payeremail,long oid,String txnid,String auctionbuyer,String itemnumber,String ems_id,String country)
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(address_name, payeremail, oid, txnid, auctionbuyer, itemnumber, ems_id, country);
			 Document mergeDoc[] = {doc};
			 //super.addIndex(analyzer,mergeDoc);
			 String[] ids = new String[1];
			 ids[0] = String.valueOf(oid);
			 
			 String[] fields = new String[1];
			 fields[0] = (oid+" "+address_name+" "+payeremail+" "+txnid+" "+auctionbuyer+" "+ems_id+" "+itemnumber+" "+country).toLowerCase();
			 super.addIndex(analyzer, mergeDoc);//("oid","merge_field",ids, fields,0);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.addIndex",log);
		}
	}
	
	/**
	 * 更新索引
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void updateIndex(long oid,String address_name,String payeremail,String txnid,String auctionbuyer,String itemnumber,String ems_id,String country)
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(address_name, payeremail,oid, txnid, auctionbuyer, itemnumber, ems_id,country);
			 String[] ids = new String[1];
			 ids[0] = String.valueOf(oid);;
			 
			 String[] fields = new String[1];
			 fields[0] = (oid+" "+address_name+" "+payeremail+" "+txnid+" "+auctionbuyer+" "+ems_id+" "+itemnumber+" "+country).toLowerCase();
			 
			 
			 super.updateIndex(analyzer,"oid",ids[0], doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.updateIndex",log);
		}
	}

	/**
	 * 
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
//	public void updateIndexAsyn(long oid,String address_name,String payeremail,String txnid,String auctionbuyer,String itemnumber,String ems_id,String country)
//		throws Exception
//	{
//		ThreadManager.getInstance().executeInBackground(null);
//	}
	
	/**
	 * 删除所有
	 * @param pc_id
	 * @throws Exception
	 */
	public void deleteIndex(long oid)
		throws Exception
	{
		try
		{
			super.deleteIndex(analyzer, "oid", String.valueOf(oid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.deleteIndex",log);
		}
	}
	
//	public void deleteIndexAsyn(long oid)
//		throws Exception
//	{
//		ThreadManager.getInstance().executeInBackground(null);
//	}
	
	/**
	 * 普通搜索
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,int mode,int page_count,PageCtrl pc)
		throws Exception
	{
		try
		{
			//key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
//			key = processQueryTerms(key); 
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new WhitespaceAnalyzer(Version.LUCENE_40));
			switch(mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			
			
			Query query = queryParser.parse(key);

			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("oid",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,sort,pc);
			
			
			FloorOrderMgr fom = (FloorOrderMgr)MvcUtil.getBeanFromContainer("floorOrderMgr");
			ArrayList oid = new ArrayList();
			for (int i=0; i<docs.length; i++)
			{
				oid.add(docs[i].get("oid"));
			}
			
			return( fom.getOrdersInOids4Search(oid) );
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"OrderIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	/**
	 * 指定搜索字段
	 * @param field
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getMultiplFieldsExactSearchResults(String fields[],String key,PageCtrl pc)
		throws Exception
	{
		try
		{
			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			key = key.toLowerCase();
			
			BooleanQuery booleanQuery = new BooleanQuery();
				
			for (int i=0; i<fields.length; i++)
			{
				TermQuery subQuery = new TermQuery(new Term(fields[i],key)); 
				booleanQuery.add(subQuery,  BooleanClause.Occur.SHOULD);
			}

			 //排序方式(订单号降序排序)
			 Sort sort =  new Sort();  
			 SortField sortField = new SortField("oid",SortField.Type.LONG,true);  
			 sort.setSort(sortField);
			 
			Document docs[] = super.getDocumentsBySearch(booleanQuery,100,sort,pc);
			
			FloorOrderMgr fom = (FloorOrderMgr)MvcUtil.getBeanFromContainer("floorOrderMgr");
			ArrayList oid = new ArrayList();
			for (int i=0; i<docs.length; i++)
			{
				oid.add(docs[i].get("oid"));
			}
			
			return( fom.getOrdersInOids4Search(oid) );
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"OrderIndexMgr.getSearchResultsByField("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.getSearchResultsByField("+key+")",log);
		}
	}

	/**
	 * 可以过滤搜索结果中，某个字段某些值
	 * @param key
	 * @param field
	 * @param fieldVal
	 * @param pc
	 * @return
	 * @throws Exception
	 */
//	public DBRow[] getSearchResults(String key,String field,String fieldVal[],PageCtrl pc)
//		throws Exception
//	{
//		try
//		{
//			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
//	
//			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
//			
//			Query query = IKQueryParser.parse("p_name",key);
//			
//			/**
//			 * 一个字段多值过滤
//			 * 原理：单值多次条件查询合并
//			 */
//			Filter filter = null;
//			if (field!=null&&fieldVal!=null&&fieldVal.length>0)
//			{
//	            BooleanQuery bqf = new BooleanQuery();
//	            for(int i=0;i<fieldVal.length;i++)
//	            {
//	    			Query fquery = new TermQuery(new Term(field, fieldVal[i]));
//	                bqf.add(fquery,BooleanClause.Occur.SHOULD);
//	            }               
//	            filter= new QueryWrapperFilter(bqf);   
//			}
//            
//			Document docs[] = super.getDocumentsBySearch(query,filter,100,pc);
//			for (int i=0; i<docs.length; i++)
//			{
//				DBRow row = new DBRow();
//				row.add("pc_id",docs[i].get("pc_id"));
//				row.add("p_name",docs[i].get("p_name"));
//				row.add("p_code",docs[i].get("p_code"));
//				row.add("catalog_id",docs[i].get("catalog_id"));
//				
//				resultAL.add(row);
//			}
//			
//			return((DBRow[])resultAL.toArray(new DBRow[0]));
//		}
//		catch (ParseException e)
//		{
//			throw new SystemException(e,"OrderIndexMgr.getSearchResults("+key+")",log);
//		} 
//		catch (Exception e)
//		{
//			throw new SystemException(e,"OrderIndexMgr.getSearchResults("+key+")",log);
//		}
//	}

}



