package com.cwc.app.lucene;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.util.StringUtil;

public class ClientIndexMgr extends IndexCore
{
	static Logger log = Logger.getLogger("ACTION");
	 
	private static ClientIndexMgr test = null;
	private Analyzer analyzer = null; 
	
	public ClientIndexMgr()
	{
		super.initIndexPath("client_index");//索引存放路径
		analyzer = new IKAnalyzer();//中文分词器
		
		//super.setOptimizeThreshold(500);//每增加多少条索引进行一次索引优化
	}

	public synchronized static ClientIndexMgr getInstance()
	{
		if (test==null)
		{
			test = new ClientIndexMgr();
		}
		
		return(test);
	}
	
	/**
	 * 把需要做成索引的字段封装成Document
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @return
	 */
	private Document packetField2Document(String cid,String email,String summary,String mod_date)
	{
		 Document doc = new Document();
		
		 doc.add(new StringField("index", "1", Store.NO));
		 
		 doc.add(new StringField("cid", cid, Store.YES));
		 doc.add(new StringField("email", email, Store.YES));
		 doc.add(new StringField("summary", summary, Store.YES));
		 doc.add(new StringField("mod_date", mod_date, Store.YES));
		 
		 doc.add(new TextField("merge_field", cid+" "+email+" "+summary, Store.NO));
		 
		 return(doc);
	}

	/**
	 * 增加索引
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void addIndex(String cid,String email,String summary,String mod_date)
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document( cid, email, summary, mod_date);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ClientIndexMgr.addIndex",log);
		}
	}

	/**
	 * 异步接口
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void addIndexAsyn(String cid,String email,String summary,String mod_date)
		throws Exception
	{
		//ThreadManager.getInstance().executeInBackground(new AddClientIndex( cid, email, summary, mod_date));
		ClientIndexMgr.getInstance().addIndex( cid, email, summary, mod_date);
	}
	
	/**
	 * 更新索引
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void updateIndex(long cid,String email,String summary,String mod_date)
		throws Exception
	{
		try
		{
			Document doc = this.packetField2Document( String.valueOf(cid), email, summary, mod_date);
			super.updateIndex(analyzer, "cid", String.valueOf(cid), doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ClientIndexMgr.updateIndex",log);
		}
	}

	/**
	 * 
	 * @param pc_id
	 * @param p_name
	 * @param p_code
	 * @param catalog_id
	 * @throws Exception
	 */
	public void updateIndexAsyn(long cid,String email,String summary,String mod_date)
		throws Exception
	{
		//ThreadManager.getInstance().executeInBackground(new UpdateClientIndex( cid, email, summary, mod_date));
		ClientIndexMgr.getInstance().updateIndex(cid, email, summary, mod_date);
	}
	
	/**
	 * 删除所有
	 * @param pc_id
	 * @throws Exception
	 */
	public void deleteIndex(long cid)
		throws Exception
	{
		try
		{
			super.deleteIndex(analyzer, "cid", String.valueOf(cid));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ClientIndexMgr.deleteIndex",log);
		}
	}
	
	public void deleteIndexAsyn(long cid)
		throws Exception
	{
		//ThreadManager.getInstance().executeInBackground(new DelClientIndex( cid));
		ClientIndexMgr.getInstance().deleteIndex(cid);
	}
	
	/**
	 * 普通搜索
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,PageCtrl pc)
		throws Exception
	{
		try
		{
			////system.out.println(key);
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();

			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query = IKQueryParser.parse("merge_field",key);
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			Query query = queryParser.parse(key);
			Document docs[] = super.getDocumentsBySearch(query,100,new Sort(),pc);
			
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				row.add("cid",docs[i].get("cid"));
				row.add("email",docs[i].get("email"));
				row.add("summary",docs[i].get("summary"));
				row.add("mod_date",docs[i].get("mod_date"));
				
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"ClientIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"ClientIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	/**
	 * 指定搜索字段
	 * @param field
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getMultiplFieldsExactSearchResults(String fields[],String key,PageCtrl pc)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
			
			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			key = key.toLowerCase();
			
			BooleanQuery booleanQuery = new BooleanQuery();
				
			for (int i=0; i<fields.length; i++)
			{
				TermQuery subQuery = new TermQuery(new Term(fields[i],key)); 
				booleanQuery.add(subQuery,  BooleanClause.Occur.SHOULD);
			}

			 //排序方式(订单号降序排序)
			 Sort sort =  new Sort();  
			 SortField sortField = new SortField("cid",SortField.Type.LONG,true);  
			 sort.setSort(sortField);
			 
			Document docs[] = super.getDocumentsBySearch(booleanQuery,100,sort,pc);
			
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				row.add("cid",docs[i].get("cid"));
				row.add("email",docs[i].get("email"));
				row.add("summary",docs[i].get("summary"));
				row.add("mod_date",docs[i].get("mod_date"));
				
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"OrderIndexMgr.getSearchResultsByField("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"OrderIndexMgr.getSearchResultsByField("+key+")",log);
		}
	}

	/**
	 * 可以过滤搜索结果中，某个字段某些值
	 * @param key
	 * @param field
	 * @param fieldVal
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,String field,String fieldVal[],PageCtrl pc)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
	
			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query = IKQueryParser.parse("merge_field",key);
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			
			Query query = queryParser.parse(key);
			
			/**
			 * 一个字段多值过滤
			 * 原理：单值多次条件查询合并
			 */
			Filter filter = null;
			if (field!=null&&fieldVal!=null&&fieldVal.length>0)
			{
	            BooleanQuery bqf = new BooleanQuery();
	            for(int i=0;i<fieldVal.length;i++)
	            {
	    			Query fquery = new TermQuery(new Term(field, fieldVal[i]));
	                bqf.add(fquery,BooleanClause.Occur.SHOULD);
	            }               
	            filter= new QueryWrapperFilter(bqf);   
			}
            
			Document docs[] = super.getDocumentsBySearch(query,filter,100,pc);
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				row.add("cid",docs[i].get("cid"));
				row.add("email",docs[i].get("email"));
				row.add("summary",docs[i].get("summary"));
				row.add("mod_date",docs[i].get("mod_date"));
				
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"ClientIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"ClientIndexMgr.getSearchResults("+key+")",log);
		}
	}

	/**
	 * 数据库批量转换索引必须实现
	 * 由父类数据库批量转换索引函数调用
	 * @param rs
	 * @return
	 */
	protected Document getDataBaseDocument(ResultSet rs)
		throws Exception
	{
		try
		{
			String cid = StringUtil.handleNull(rs.getString("cid"),"");
			String email = StringUtil.handleNull(rs.getString("email"),"");
			String summary = StringUtil.handleNull(rs.getString("summary"),"");
			String mod_date = StringUtil.handleNull(rs.getString("mod_date"),"");
			
			return( this.packetField2Document( cid, email, summary,mod_date));
		} 
		catch (SQLException e)
		{
			throw new SystemException(e,"ClientIndexMgr.getDataBaseDocument",log);
		}
	}
	
	 
	 
	 
	 
	 
	public static void main(String args[])
		throws Exception
	{
		ClientIndexMgr.getInstance().initIndexPath("E:\\tomcat5.5.26\\webapps\\paypal20\\WEB-INF\\index\\client_index");
		
		ClientIndexMgr.getInstance().getAllIndex(new String[]{"email"});
	}


}






