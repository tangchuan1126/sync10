package com.cwc.app.lucene;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.FloorHelpCenterMgr;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

public class HelpCenterTopicIndexMgr extends IndexCore
{
	static Logger log = Logger.getLogger("ACTION");
	 
	private static HelpCenterTopicIndexMgr test = null;
	private Analyzer analyzer = null; 
	
	public HelpCenterTopicIndexMgr()
	{
		super.initIndexPath("help_center_index");//索引存放路径
		analyzer = new IKAnalyzer();//中文分词器
		
		//super.setOptimizeThreshold(500);//每增加多少条索引进行一次索引优化
	}

	public synchronized static HelpCenterTopicIndexMgr getInstance()
	{
		if (test==null)
		{
			test = new HelpCenterTopicIndexMgr();
		}
		
		return(test);
	}
	
	/**
	 *  把需要做成索引的字段封装成Document
	 * @param hct_id
	 * @param title
	 * @param content
	 * @param date
	 * @return
	 */
	private Document packetField2Document(long hct_id,long cid,String title,String content,String date,String author)
	{
		 Document doc = new Document();
		
		 doc.add(new StringField("index", "1", Store.NO));
		 
		 doc.add(new LongField("hct_id",hct_id, Store.YES));
		 doc.add(new LongField("cid",cid, Store.YES));
		 doc.add(new StringField("title", title, Store.YES));
		 doc.add(new StringField("author", author, Store.YES));
		 doc.add(new StringField("date", date, Store.YES));
		 doc.add(new StringField("content", content, Store.YES));
		 
		 doc.add(new TextField("merge_field", title+" "+content+" "+date+" "+author, Store.NO));
		 
		 return(doc);
	}

	/**
	 * 
	 * @param hct_id
	 * @param title
	 * @param content
	 * @param mod_date
	 * @throws Exception
	 */
	public void addIndex(long hct_id,long cid,String title,String content,String mod_date,String promulgator)
		throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document( hct_id,cid,title, content, mod_date,promulgator);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.addIndex",log);
		}
	}


	
	/**
	 * 更新索引
	 * @param hct_id
	 * @param cid
	 * @param title
	 * @param content
	 * @param date
	 * @throws Exception
	 */
	public void updateIndex(long hct_id,long cid,String title,String content,String date,String author)
		throws Exception
	{
		try
		{
			Document doc = this.packetField2Document(hct_id,cid,title,content,date,author);
			super.updateIndex(analyzer, "hct_id", String.valueOf(hct_id), doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.updateIndex",log);
		}
	}

	
	/**
	 * 删除索引
	 * @param pc_id
	 * @throws Exception
	 */
	public void deleteIndex(long hct_id)
		throws Exception
	{
		try
		{
			super.deleteIndex(analyzer, "hct_id", String.valueOf(hct_id));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.deleteIndex",log);
		}
	}
	
	
	/**
	 * 普通搜索
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,PageCtrl pc)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();

			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query = IKQueryParser.parse("merge_field",key);
			
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			
			Query query = queryParser.parse(key);
			Document docs[] = super.getDocumentsBySearch(query,100,pc);
			FloorHelpCenterMgr fhcMgr = (FloorHelpCenterMgr)MvcUtil.getBeanFromContainer("floorHelpCenterMgr");
			for (int i=0; i<docs.length; i++)
			{
				long hct_id=Long.parseLong(docs[i].get("hct_id"));
				DBRow row = fhcMgr.getDetailTopicsByHTCID(hct_id);
				row.add("contentindex",docs[i].get("content"));
//				row.add("cid",docs[i].get("cid"));
//				row.add("hct_id",docs[i].get("hct_id"));
//				row.add("title",docs[i].get("title"));
//				row.add("content",docs[i].get("content"));
//				row.add("last_mod_date",docs[i].get("date"));
//				row.add("author",docs[i].get("author"));
//				DBRow row=new DBRow();
//				row.add("merge_field",docs[i].get("merge_field"));
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	/**
	 * 前台根据索引查文章
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchUseResults(String key,PageCtrl pc)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
	
			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query = IKQueryParser.parse("merge_field",key);
			
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			
			Query query = queryParser.parse(key);
			
			Document docs[] = super.getDocumentsBySearch(query,100,null);
			
			ArrayList<String> hctids=new ArrayList<String>();
			
			FloorHelpCenterMgr fhcMgr = (FloorHelpCenterMgr)MvcUtil.getBeanFromContainer("floorHelpCenterMgr");
			
			HashMap indexMap=new HashMap();
			for (int i=0; i<docs.length; i++)
			{
				hctids.add(docs[i].get("hct_id"));
				indexMap.put(docs[i].get("hct_id"),docs[i].get("content"));
			}
			DBRow[] topics=new DBRow[]{};
			if(hctids.size()!=0)
			{
				topics=fhcMgr.getSearchTopics4CTUse(hctids.toArray(new String[0]), pc);
			}
			
			for(int i=0;i<topics.length;i++)
			{
				DBRow topic=topics[i];
				String contentindex=(String) indexMap.get(topic.getString("hct_id"));
				topic.add("contentindex",contentindex);
			}
			
			
			return (topics);
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.getSearchResults("+key+")",log);
		}
	}

	/**
	 * 可以过滤搜索结果中，某个字段某些值
	 * @param key
	 * @param field
	 * @param fieldVal
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchResults(String key,String field,String fieldVal[],PageCtrl pc)
		throws Exception
	{
		try
		{
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
	
			key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符
			
//			Query query = IKQueryParser.parse("merge_field",key);
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			
			Query query = queryParser.parse(key);
			
			/**
			 * 一个字段多值过滤
			 * 原理：单值多次条件查询合并
			 */
			Filter filter = null;
			if (field!=null&&fieldVal!=null&&fieldVal.length>0)
			{
	            BooleanQuery bqf = new BooleanQuery();
	            for(int i=0;i<fieldVal.length;i++)
	            {
	    			Query fquery = new TermQuery(new Term(field, fieldVal[i]));
	                bqf.add(fquery,BooleanClause.Occur.SHOULD);
	            }               
	            filter= new QueryWrapperFilter(bqf);   
			}
            
			Document docs[] = super.getDocumentsBySearch(query,filter,100,pc);
			for (int i=0; i<docs.length; i++)
			{
				DBRow row = new DBRow();
				row.add("cid",docs[i].get("cid"));
				row.add("email",docs[i].get("email"));
				row.add("summary",docs[i].get("summary"));
				row.add("mod_date",docs[i].get("mod_date"));
				
				resultAL.add(row);
			}
			
			return((DBRow[])resultAL.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.getSearchResults("+key+")",log);
		} 
		catch (Exception e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.getSearchResults("+key+")",log);
		}
	}

	/**
	 * 数据库批量转换索引必须实现
	 * 由父类数据库批量转换索引函数调用
	 * @param rs
	 * @return
	 */
	protected Document getDataBaseDocument(ResultSet rs)
		throws Exception
	{
		try
		{
			long hct_id=Long.parseLong(StringUtil.handleNull(rs.getString("hct_id"),""));
			long cid =Long.parseLong( StringUtil.handleNull(rs.getString("cid"),""));
			String title = StringUtil.handleNull(rs.getString("title"),"");
			String content = StringUtil.handleNull(rs.getString("content"),"");
			String last_mod_date = StringUtil.handleNull(rs.getString("last_mod_date"),"");
			String author = StringUtil.handleNull(rs.getString("author"),"");
			
			return( this.packetField2Document(hct_id, cid, title,content,last_mod_date,author));
		} 
		catch (SQLException e)
		{
			throw new SystemException(e,"HelpCenterTopicIndexMgr.getDataBaseDocument",log);
		}
	}
	

	 
	 
	 
	 
	 
	public static void main(String args[])
		throws Exception
	{
		HelpCenterTopicIndexMgr.getInstance().initIndexPath("D:\\tomcat6.0.18\\webapps\\paypal20\\WEB-INF\\index\\help_center_index");
		HelpCenterTopicIndexMgr.getInstance().addIndex(1111, 2222, "测试", "<p>玩儿玩儿</p><p>从vxdfer</p><p>收入为儿童</p><p>空间好地方看机会</p>", "2010-12-30 17:26:02", "author");
		//HelpCenterTopicIndexMgr.getInstance().getAllIndex();
		DBRow results[] = HelpCenterTopicIndexMgr.getInstance().getSearchResults("收入", null);
		for (int i=0; i<results.length; i++)
		{
			////system.out.println(results[i].getString("content"));
		}
	}


}






