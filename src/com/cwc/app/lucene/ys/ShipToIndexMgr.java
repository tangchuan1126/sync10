package com.cwc.app.lucene.ys;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class ShipToIndexMgr extends IndexCore{
	
	static Logger log = Logger.getLogger("ACTION");
	private static ShipToIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static ShipToIndexMgr getInstance(){
		if (test==null){
			test = new ShipToIndexMgr();
		}
		
		return(test);
	}

	public ShipToIndexMgr() 
	{
		super.initIndexPath("ship_to_index");//索引存放路径
		analyzer = new IKAnalyzer();  
 	}
	
	private Document packetField2Document(long shipToId) throws Exception
	{
		 Document doc = new Document();
		 doc.add(new StringField("index","1",Store.NO));
		 doc.add(new TextField("ship_to_id",String.valueOf(shipToId),Store.YES));
		 com.cwc.app.iface.zj.ShipToMgrIFaceZJ shipToMgr = (com.cwc.app.iface.zj.ShipToMgrIFaceZJ)MvcUtil.getBeanFromContainer("proxyShipToMgrZJ");
		 DBRow returnRow	= shipToMgr.getShipToById(shipToId);
		 //合并检索字段
		 String name = returnRow.getString("ship_to_name");
		 
		 doc.add(new TextField("ship_to_name",name,Store.YES));
		 doc.add(new TextField("merge_field",name, Store.YES));
		 return(doc);
	}
	
	public void addIndex(long rp_id) 
	throws Exception
	{
		try
		{	 
			 Document doc = this.packetField2Document(rp_id);
			 Document mergeDoc[] = {doc};
			 super.addIndex(analyzer,mergeDoc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ShipToIndexMgr.addIndex",log);
		}
	}
	
	public void updateIndex(long rp_id) 
	throws Exception
	{
		try
		{
			 Document doc = this.packetField2Document(rp_id);
		 		 
			 super.updateIndex(analyzer,"ship_to_id",String.valueOf(rp_id),doc);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ShipToIndexMgr.updateIndex",log);
		}
	}
	
	public DBRow[] getSearchResults(String key,int search_mode,int page_count,PageCtrl pc)
	throws Exception
	{
		try
		{
			QueryParser queryParser = new QueryParser(Version.LUCENE_40,"ship_to_name",new IKAnalyzer());
			switch(search_mode) 
			{
				case 1:
					queryParser.setDefaultOperator(Operator.AND);
				break;
				
				case 2:
					queryParser.setDefaultOperator(Operator.OR);
				break;
			}
			Query query = queryParser.parse(key);
			
			 //排序方式(订单号降序排序)
			Sort sort =  new Sort();  
			SortField sortField = new SortField("ship_to_id",SortField.Type.LONG,true);  
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(query,page_count,pc);
			
			FloorShipToMgrZJ floorShipToMgrZJ = (FloorShipToMgrZJ)MvcUtil.getBeanFromContainer("floorShipToMgrZJ");
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			for (int i=0; i<docs.length; i++)
			{
				DBRow returnRow = floorShipToMgrZJ.getDetailShipToById(Long.parseLong((docs[i].get("ship_to_id"))));
				list.add(returnRow);
			}
			
			return(list.toArray(new DBRow[0]));
		}
		catch (ParseException e)
		{
			throw new SystemException(e,"ShipToIndexMgr.getSearchResults("+key+")",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"ShipToIndexMgr.getSearchResults("+key+")",log);
		}
	}
	
	public void deleteIndex(long  rp_id)throws Exception{
		try{
			super.deleteIndex(analyzer, "ship_to_id", String.valueOf(rp_id));
		}
		catch (Exception e){
			throw new SystemException(e,"ShipToIndexMgr.deleteIndex",log);
		}
	}
	
	
	
	
}

