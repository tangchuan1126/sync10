package com.cwc.app.lucene.ys;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.SystemException;
import com.cwc.lucene.IndexCore;
import com.cwc.spring.util.MvcUtil;

public class ShipToStorageIndexMgr extends IndexCore {
	
	static Logger log = Logger.getLogger("ACTION");
	private static ShipToStorageIndexMgr test = null;
	private Analyzer analyzer = null;
	
	public synchronized static ShipToStorageIndexMgr getInstance() {
		if (test == null) {
			test = new ShipToStorageIndexMgr();
		}
		
		return (test);
	}
	
	public ShipToStorageIndexMgr() {
		super.initIndexPath("ship_to_storage_index");// 索引存放路径
		analyzer = new IKAnalyzer();
	}
	
	private Document packetField2Document(long storageId, String title, String city, String street, String house,
			String zipcode, String contact, String country, String proName, String sendProInput, String shipToId,
			String shipToName, String storageType) throws Exception {
		Document doc = new Document();
		doc.add(new StringField("index", "1", Store.NO));
		doc.add(new TextField("id", String.valueOf(storageId), Store.YES));
		
		String state = proName.equals("") ? sendProInput : proName;
		
		StringBuilder sb = new StringBuilder();
		sb.append(title).append(" ").append(city).append(" ").append(street).append(" ").append(house).append(" ")
				.append(zipcode).append(" ");
		sb.append(contact).append(" ").append(country).append(" ").append(state);
		doc.add(new TextField("title", title, Store.YES));
		doc.add(new TextField("city", city, Store.YES));
		doc.add(new TextField("street", street, Store.YES));
		doc.add(new TextField("house", house, Store.YES));
		doc.add(new TextField("zipcode", zipcode, Store.YES));
		doc.add(new TextField("contact", contact, Store.YES));
		doc.add(new TextField("country", country, Store.YES));
		doc.add(new TextField("state", state, Store.YES));
		doc.add(new TextField("storage_type_id", shipToId, Store.YES));
		doc.add(new TextField("ship_to_name", shipToName, Store.YES));
		doc.add(new TextField("storage_type", storageType, Store.YES));
		
		doc.add(new TextField("merge_field", sb.toString(), Store.YES));
		return (doc);
	}
	
	public void addIndex(long storageId, String title, String city, String street, String house, String zipcode,
			String contact, String country, String proName, String sendProInput, String shipToId, String shipToName,
			String storageType) throws Exception {
		try {
			Document doc = this.packetField2Document(storageId, title, city, street, house, zipcode, contact, country,
					proName, sendProInput, shipToId, shipToName, storageType);
			Document mergeDoc[] = { doc };
			super.addIndex(analyzer, mergeDoc);
		} catch (Exception e) {
			throw new SystemException(e, "ShipToStorageIndexMgr.addIndex", log);
		}
	}
	
	public void updateIndex(long storageId, String title, String city, String street, String house, String zipcode,
			String contact, String country, String proName, String sendProInput, String shipToId, String shipToName,
			String storageType) throws Exception {
		try {
			Document doc = this.packetField2Document(storageId, title, city, street, house, zipcode, contact, country,
					proName, sendProInput, shipToId, shipToName, storageType);
			
			super.updateIndex(analyzer, "id", String.valueOf(storageId), doc);
		} catch (Exception e) {
			throw new SystemException(e, "ShipToStorageIndexMgr.updateIndex", log);
		}
	}
	
	public DBRow[] getSearchResults(String key, int page_count, PageCtrl pc, String storageType, DBRow filter,
			long shipToId) throws Exception {
		try {
			
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
			BooleanQuery bqf = new BooleanQuery();
			
			if (key != "") {
				key = super.filterWords(key);// 使用父类提供的过滤器过滤关键字的一些特殊字符
				
				QueryParser queryParser = new QueryParser(Version.LUCENE_40, "merge_field", new WhitespaceAnalyzer(
						Version.LUCENE_40));
				Query query = queryParser.parse(key);
				bqf.add(query, BooleanClause.Occur.MUST);
			}
			
			QueryParser storageTypeQueryParser = new QueryParser(Version.LUCENE_40, "storage_type",
					new WhitespaceAnalyzer(Version.LUCENE_40));
			Query storageTypeQuery = storageTypeQueryParser.parse(storageType);
			bqf.add(storageTypeQuery, BooleanClause.Occur.MUST);
			
			/*
			 * Query storageTypeQuery = new TermQuery(new Term("storage_type", storageType));
			 * 
			 * bqf.add(storageTypeQuery,BooleanClause.Occur.MUST);
			 */
			/**
			 * 一个字段多值过滤 原理：单值多次条件查询合并
			 */
			if (filter != null && filter.getFieldNames().size() > 0) {
				ArrayList filterNames = filter.getFieldNames();
				for (int i = 0; i < filterNames.size(); i++) {
					String filterName = filterNames.get(i).toString().toLowerCase();
					
					Query fquery = new TermQuery(new Term(filterName, filter.getString(filterName)));
					
					bqf.add(fquery, BooleanClause.Occur.MUST);
				}
			}
			
			Sort sort = new Sort();
			SortField sortField = new SortField("ship_to_name", SortField.Type.STRING, false);
			sort.setSort(sortField);
			Document docs[] = super.getDocumentsBySearch(bqf, 100, sort, pc);
			
			for (int i = 0; i < docs.length; i++) {
				DBRow row = new DBRow();
				for (IndexableField f : docs[i].getFields()) {
					row.add(f.name(), f.stringValue());
				}// for
				
				resultAL.add(row);
			}
			
			return ((DBRow[]) resultAL.toArray(new DBRow[0]));
			
			/*
			 * QueryParser queryParser = new QueryParser(Version.LUCENE_40,"merge_field",new IKAnalyzer());
			 * switch(search_mode) { case 1: queryParser.setDefaultOperator(Operator.AND); break;
			 * 
			 * case 2: queryParser.setDefaultOperator(Operator.OR); break; } Query query = queryParser.parse(key);
			 * 
			 * //排序方式(订单号降序排序) Sort sort = new Sort(); SortField sortField = new
			 * SortField("id",SortField.Type.LONG,true); sort.setSort(sortField); Document docs[] =
			 * super.getDocumentsBySearch(query,page_count,pc);
			 * 
			 * FloorShipToMgrZJ floorShipToMgrZJ = (FloorShipToMgrZJ)MvcUtil.getBeanFromContainer("floorShipToMgrZJ");
			 * 
			 * ArrayList<DBRow> list = new ArrayList<DBRow>(); for (int i=0; i<docs.length; i++) { DBRow returnRow =
			 * floorShipToMgrZJ.getDetailStorageCatalogById(Long.parseLong((docs[i].get("id")))); list.add(returnRow); }
			 * 
			 * return(list.toArray(new DBRow[0]));
			 */
		} catch (ParseException e) {
			throw new SystemException(e, "ShipToStorageIndexMgr.getSearchResults(" + key + ")", log);
		} catch (Exception e) {
			throw new SystemException(e, "ShipToStorageIndexMgr.getSearchResults(" + key + ")", log);
		}
	}
	
	public void deleteIndex(long rp_id) throws Exception {
		try {
			super.deleteIndex(analyzer, "id", String.valueOf(rp_id));
		} catch (Exception e) {
			throw new SystemException(e, "ShipToStorageIndexMgr.deleteIndex", log);
		}
	}
	
	public DBRow[] getSearchResults(DBRow filter, long shipToId, PageCtrl pc, String storageType) throws Exception {
		try {
			ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
			
			/* key = super.filterWords(key);//使用父类提供的过滤器过滤关键字的一些特殊字符 */
			BooleanQuery bqf = new BooleanQuery();

			QueryParser storageTypeQueryParser = new QueryParser(Version.LUCENE_40, "storage_type",
					new WhitespaceAnalyzer(Version.LUCENE_40));
			Query storageTypeQuery = storageTypeQueryParser.parse(storageType);
			bqf.add(storageTypeQuery, BooleanClause.Occur.MUST);
			
			if (filter != null && filter.getFieldNames().size() > 0) {
				ArrayList filterNames = filter.getFieldNames();
				for (int i = 0; i < filterNames.size(); i++) {
					String filterName = filterNames.get(i).toString().toLowerCase();
					QueryParser queryParser = new QueryParser(Version.LUCENE_40, filterName, new WhitespaceAnalyzer(
							Version.LUCENE_40));
					Query query = queryParser.parse(filter.getString(filterName));
					bqf.add(query, BooleanClause.Occur.MUST);
				}
			}
			PageCtrl internalPc = new PageCtrl();
			internalPc.setPageNo(1);
			internalPc.setPageSize(10000);
			
			Sort sort = new Sort();
			SortField sortField = new SortField("ship_to_name", SortField.Type.STRING, false);
			sort.setSort(sortField);
			
			Document docs[] = super.getDocumentsBySearch(bqf, 100, sort, internalPc);
			
			if (docs.length > 0) {
				
				int pageSize = pc.getPageSize();
				int pageNo = pc.getPageNo();
				int start = (pageNo - 1) * pageSize;
				int end = pageNo * pageSize;
				
				if (shipToId == 0) {
					
					ArrayList<String> shipToIds = new ArrayList<String>();
					ArrayList<String> shipToNames = new ArrayList<String>();
					for (int i = 0; i < docs.length; i++) {
						
						if (!docs[i].get("storage_type_id").trim().equals("") && !docs[i].get("ship_to_name").trim().equals("")) {
							
							if (!shipToIds.contains(docs[i].get("storage_type_id"))) {
								
								shipToIds.add(docs[i].get("storage_type_id"));
								shipToNames.add(docs[i].get("ship_to_name"));
							}
						}
					}
					for (int i = start; i < end && i < shipToIds.size(); i++) {
						DBRow r = new DBRow();
						r.add("storage_type_id", shipToIds.get(i));
						r.add("ship_to_name", shipToNames.get(i));
						resultAL.add(r);
					}
					
					int pageCount = ((shipToIds.size() + pageSize) - 1) / pageSize;
					if (pageCount < pageNo) {
						pageNo = pageCount;
					}
					pc.setPageNo(pageNo);
					pc.setPageSize(pageSize);
					pc.setPageCount(pageCount);
					pc.setAllCount(shipToIds.size());
					
					return ((DBRow[]) resultAL.toArray(new DBRow[0]));
					
				} else {
					
					FloorShipToMgrZJ floorShipToMgrZJ = (FloorShipToMgrZJ) MvcUtil.getBeanFromContainer("floorShipToMgrZJ");
					
					for (int i = 0; i < docs.length; i++) {
						
						DBRow returnRow = floorShipToMgrZJ.getStorageCatalogInfoById(Long.parseLong((docs[i].get("id"))));
						
						DBRow shipTo = floorShipToMgrZJ.getShipToById(shipToId);
						
						if(shipTo != null){
							
							returnRow.put("ship_to_name", shipTo.get("ship_to_name", ""));
						}
						
						resultAL.add(returnRow);
					}
					
					return (resultAL.toArray(new DBRow[0]));
				}
				
			} else {
				
				return (resultAL.toArray(new DBRow[0]));
			}
			
		} catch (ParseException e) {
			throw new SystemException(e, "ShipToIndexMgr.getSearchResults", log);
		} catch (Exception e) {
			throw new SystemException(e, "ShipToIndexMgr.getSearchResults", log);
		}
	}
	
	public DBRow[] getSearchResults(String title) throws FileNotFoundException, Exception {
		ArrayList<DBRow> resultAL = new ArrayList<DBRow>();
		QueryParser queryParser1 = new QueryParser(Version.LUCENE_40, "title", analyzer);
		Query query1 = queryParser1.parse(title);
		BooleanQuery bqf = new BooleanQuery();
		bqf.add(query1, BooleanClause.Occur.MUST);
		Document docs[] = super.getDocumentsBySearch(bqf, 100, new PageCtrl());
		for (int i = 0; i < docs.length; i++) {
			DBRow row = new DBRow();
			for (IndexableField f : docs[i].getFields()) {
				row.add(f.name(), f.stringValue());
			}// for
			
			resultAL.add(row);
		}
		
		return ((DBRow[]) resultAL.toArray(new DBRow[0]));
	}
	
}
