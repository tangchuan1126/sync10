package com.cwc.app.crm;

import com.cwc.app.iface.CrmMgrIFace;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

/**
 * 特别关注顾客
 * @author Administrator
 *
 */
public class SpecialClients  implements ClientsIFrace
{
	private CrmMgrIFace crmMgr;
	private int period = 1;

	public DBRow[] getMustTraceClients(long adid,PageCtrl pc) 
		throws Exception 
	{
		return(crmMgr.getSpecialClients(adid, this.getPeriod() , pc));
	}

	public DBRow[] getReadyClients(long adid,PageCtrl pc) 
		throws Exception 
	{
		return(null);//特别关注客户，只有一个列表
	}

	public int getPeriod() 
	{
		return(period);
	}

	public int getTargetType() 
	{
		return(0);//特别关注客户，无需类型
	}

	public void setCrmMgr(CrmMgrIFace crmMgr) 
	{
		this.crmMgr = crmMgr;
	}


}
