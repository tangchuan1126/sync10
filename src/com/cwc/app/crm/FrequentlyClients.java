package com.cwc.app.crm;

import com.cwc.app.iface.CrmMgrIFace;
import com.cwc.app.key.CrmClientKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

/**
 * 频繁购买顾客
 * @author Administrator
 *
 */
public class FrequentlyClients  implements ClientsIFrace
{
	private CrmMgrIFace crmMgr;
	private int period = 3;//3天跟进一次

	public DBRow[] getMustTraceClients(long adid,PageCtrl pc) 
		throws Exception 
	{
		return(crmMgr.getFrequentlyMustTraceClients(adid, this.getPeriod() , this.getTargetType(), pc));
	}

	public DBRow[] getReadyClients(long adid,PageCtrl pc) 
		throws Exception 
	{
		return(crmMgr.getFrequentlyReadyClients(adid, pc));
	}

	public int getPeriod() 
	{
		return(period);
	}

	public int getTargetType() 
	{
		return(CrmClientKey.FREQUENTLY_CLIENTS);
	}

	public void setCrmMgr(CrmMgrIFace crmMgr) 
	{
		this.crmMgr = crmMgr;
	}


}
