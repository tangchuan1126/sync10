package com.cwc.app.crm;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface ClientsIFrace 
{
	public DBRow[] getMustTraceClients(long adid,PageCtrl pc)  throws Exception;//当天必须跟进客户
	public DBRow[] getReadyClients(long adid,PageCtrl pc) throws Exception;//潜力客户
	public int getPeriod();//跟进生命周期
	public int getTargetType();//目标客户类型
}
