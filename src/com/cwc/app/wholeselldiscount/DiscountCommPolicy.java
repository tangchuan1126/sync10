package com.cwc.app.wholeselldiscount;

import org.apache.log4j.Logger;

import com.cwc.app.iface.QuoteIFace;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;


/**
 * 不要配置成Sigletone!!!
 * 
 * 通用折扣计费策略
 * @author 
 *
 * 通用折扣计费策略接口用于规范、扩展用费计算模式
 */
public abstract class DiscountCommPolicy
{
	static Logger log = Logger.getLogger("ACTION");
	
	protected QuoteIFace quoteMgr;
	protected String clientId;//客户ID
	protected double currentMcGross;
	protected String buyCatalog;
	
	public void setBuyCatalog(String buyCatalog) 
	{
		this.buyCatalog = buyCatalog;
	}

	public void setCurrentMcGross(double currentMcGross)
	{
		this.currentMcGross = currentMcGross;
	}


	public void setQuoteMgr(QuoteIFace quoteMgr)
	{
		this.quoteMgr = quoteMgr;
	}
	
	/**
	 * 经过计算对比，获得折扣
	 * 这里使用字符串，因为有可能得到的不是折扣，而是策略名称
	 * @return
	 * @throws Exception
	 */
	public String getDisCount()
		throws Exception
	{
		try
		{
			
			DBRow detail = quoteMgr.getDetailWholeSellDiscountByName(this.getName());
			String discount_policy = detail.getString("discount_policy");
			
			return(this.caculateDiscount(discount_policy));
		}
		catch (Exception e)
		{
			throw new SystemException(e,"getDisCount",log);
		}
	}
	
	/**
	 * 用正则表达式分解规则
	 * @param rule
	 * @return 
	 * @throws Exception
	 */
	public String[][] getWholeSellRule2(String ruleStr)
		throws Exception
	{
		return(quoteMgr.getWholeSellRule2(ruleStr));
	}	
	
	public String[][] getWholeSellRule3(String ruleStr)
		throws Exception
	{
		return(quoteMgr.getWholeSellRule3(ruleStr));
	}	
	
	/**
	 * 需要子类实现
	 * 提供用于折扣规则的对比数据
	 * 可以为：金额和数量等，具体由子类不同策略实现决定
	 * @return
	 * @throws Exception
	 */
	protected abstract double getGross() throws Exception;
	
	/**
	 * 由子类实现
	 * 因为每个子类会绑定一个数据库设置
	 * @return
	 * @throws Exception
	 */
	protected abstract String getName() throws Exception;
	
	/**
	 * 由子类实现计算折扣
	 * @return
	 * @throws Exception
	 */
	protected abstract String caculateDiscount(String discount_policy) throws Exception;

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	
}
