package com.cwc.app.wholeselldiscount;

import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.util.StringUtil;

public class GeneralPolicyImp extends DiscountCommPolicy
{
	/**
	 * 每个实现类都有自己唯一的名字
	 * 跟数据设置对应
	 */
	private String name = "GeneralPolicy";
	
	private FloorOrderMgr fom ;
	

	protected double getGross() throws Exception
	{
		//获得当前批发客户历史购买总金额
		double sumMcGross = fom.getSumMcGrossByClientId(this.clientId);
		////system.out.println(sumMcGross);
		return(sumMcGross);
	}

	protected String getName() throws Exception
	{
		return(this.name);
	}

	
	
	
	
	public void setFom(FloorOrderMgr fom)
	{
		this.fom = fom;
	}

	protected String caculateDiscount(String discount_policy) 
		throws Exception
	{
		String rules[][] = super.getWholeSellRule2(discount_policy);
		
		double st,en;
		
		//对比数据，获得折扣
		for (int i=0; i<rules.length; i++)
		{
			st = StringUtil.getDouble(rules[i][1]);
			en = StringUtil.getDouble(rules[i][2]);
			////system.out.println("@@@@>"+getGross());
			if ( getGross()>st&&getGross()<=en )
			{
				return(rules[i][0]);
			}
		}
		
		return(null);
	}
	
}
