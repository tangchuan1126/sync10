package com.cwc.app.wholeselldiscount;

import com.cwc.util.StringUtil;


public class GiveDiscountBySingleOrderGrossImp extends DiscountCommPolicy
{
	/**
	 * 每个实现类都有自己唯一的名字
	 * 跟数据设置对应
	 */
	private String name = "GiveDiscountBySingleOrderGross";
	
	protected double getGross() throws Exception
	{
		return(this.currentMcGross);
	}

	protected String getName() throws Exception
	{
		return(this.name);
	}

	protected String caculateDiscount(String discount_policy) 
		throws Exception
	{
		String rules[][] = super.getWholeSellRule2(discount_policy);
		
		double st,en;
		String catalog,discount = "1";
		
		//对比数据，获得折扣
		for (int i=0; i<rules.length; i++)
		{
			st = StringUtil.getDouble(rules[i][1]);
			en = StringUtil.getDouble(rules[i][2]);
			catalog = rules[i][3];
				
			if ( getGross()>st&&getGross()<=en )
			{
				if (catalog!=null)
				{
					if (catalog.trim().equals(this.buyCatalog.trim()))
					{
						discount = rules[i][0];
						break;
					}
				}
				else
				{
					discount = rules[i][0];
				}
			}
		} 
		
		return(discount);
	}

	
}
