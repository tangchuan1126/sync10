package com.cwc.app.wholeselldiscount;

import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.util.StringUtil;


public class GiveDiscountByHistoryOrdersGrossImp extends DiscountCommPolicy
{
	/**
	 * 每个实现类都有自己唯一的名字
	 * 跟数据设置对应
	 */
	private String name = "GiveDiscountByHistoryOrdersGross";
	private FloorOrderMgr fom ;
	
	protected double getGross() throws Exception
	{
		//获得当前批发客户历史购买总金额
		double sumMcGross = fom.getSumMcGrossByClientId(this.clientId);
		return(sumMcGross);
	}

	protected String getName() throws Exception
	{
		return(this.name);
	}

	protected String caculateDiscount(String discount_policy) 
		throws Exception
	{
		String rules[][] = super.getWholeSellRule3(discount_policy);
		double st,en,sill,historyGross;
		String catalog,discount = "1";
		
		//对比数据，获得折扣
		for (int i=0; i<rules.length; i++)
		{
			st = StringUtil.getDouble(rules[i][1]);
			en = StringUtil.getDouble(rules[i][2]);
			sill = StringUtil.getDouble(rules[i][3]);
			catalog = rules[i][4];
			
			historyGross = getGross();
			
			////system.out.println(this.currentMcGross+" - "+historyGross +" - "+sill);
			if ( historyGross>st&&historyGross<=en&&this.currentMcGross>=sill)
			{
				/**
				 * 有限选择匹配分类的，如果没有匹配分类，则使用通用策略
				 * 1={0,300,HID套装} 
				 * 1={0,300} 
				 */
				if (catalog!=null)
				{
					if (catalog.trim().equals(this.buyCatalog.trim()))
					{
						discount = rules[i][0];
						break;
					}
				}
				else
				{
					discount = rules[i][0];
				}
			}
		}
		
		return(discount);
	}

	public void setFom(FloorOrderMgr fom)
	{
		this.fom = fom;
	}
	
}
