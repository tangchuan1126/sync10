/*
 * 创建日期 2008-8-18
 *
 */
package com.cwc.exactprivilege;


import com.cwc.exception.OperationNotPermitException;

public interface ExactPrivilegeIFace
{
	/**
	 * 细粒度实现类需要实现唯一方法，鉴权失败抛出异常OperationNotPermitException
	 * @param action
	 * @throws OperationNotPermitException
	 * @throws Exception
	 */
	public void validate(String action,Object[] pob) throws Throwable;
}
