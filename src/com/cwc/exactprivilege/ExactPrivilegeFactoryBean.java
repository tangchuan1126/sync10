package com.cwc.exactprivilege;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * 在系统启动时加载所有细粒度权限实现
 * @author turboshop
 *
 */
public class ExactPrivilegeFactoryBean 
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	private Map<String, ExactPrivilegeIFace> actionExactPrivilege = new HashMap<String, ExactPrivilegeIFace>();
	
	/**
	 * 系统启动时，加载所有需要实现细粒度权限的类的配置到内存
	 *
	 */
	public ExactPrivilegeFactoryBean()
	{
		//log.info("-------->ExactPrivilegeFactoryBean created!"+this.actionName.size());
	}
	
	/**
	 * 系统关闭时，释放占用的内存资源
	 *
	 */
	public void desctroy()
	{
		if (actionExactPrivilege!=null)
		{
			actionExactPrivilege.clear();
			actionExactPrivilege = null;
		}
		
		log.info("Destroy ExactPrivilegeAction successful.");
	}
	
	/**
	 * 判断事件是否需要执行细粒度鉴权
	 * @param action
	 * @return
	 */
	public boolean isNeed2Authen(String action)
	{
		return( this.actionExactPrivilege.containsKey(action) );
	}

	public ExactPrivilegeIFace getExactPrivilegeInstance(String action)
	{
		return( (ExactPrivilegeIFace)actionExactPrivilege.get(action) );
	}
	
	/**
	 * 从spring容器设置参数
	 * @param actionExactPrivilege
	 */
	public void setActionExactPrivilege(Map<String, ExactPrivilegeIFace> actionExactPrivilege)
	{
		this.actionExactPrivilege = actionExactPrivilege;
	}
}




