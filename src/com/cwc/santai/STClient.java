package com.cwc.santai;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.santai.AddOrderRequest;
import com.santai.AddOrderRequestInfoArray;
import com.santai.AddOrderResponse;
import com.santai.GoodsDetailsArray;
import com.santai.HeaderRequest;
import com.santai.ShipRate_Service;

public class STClient {
	String token = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDcygw3Ix09hHzQ7vtIwCzgeTeCZU8AO5fR54hGmXKHiKcqiD63mFltb9KVnlgwY7pjppzWYRRXEo13tWCQa3wsJsZH1JFShvxysJrg8sZ+rr1Elwu85VmG3MxDqmDA8Gg9x0RKCD70cDFCwxlUQtua3ZPoLvvXvLU0lZ1xWl1gxwIDAQAB";
	String key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDseIQJxXsvgfRlsdMXfYa2uIFGAEh99stzhsFNCRRgPdwsYkxG5SjNVbt4GRylNM1Rfs/ofa6GFokBj+OIKep4iAVzkINtK5IxEc/rEi16sL2IY36SNIfkive2NGr8TTUwVi3P0OMOs2dwMyC4Q5JJYOgVbUb49JJ4aAW61e52XwIDAQAB";
	String userId = "T7094";
	
	/**
	 * --------------------------------------info-------------------------------------------- 
	 * customerOrderNo xsd:string * required 客户的订单标识，每个订单号码唯一 ; 
	 * shipperAddresstype xsd:int Required 发货地址类型，1为用户系统默认地址，2为用户传送的地址信息;
	 * shipperName * xsd:string required 发件人姓名 ;  
	 * recipientCountry xsd:string * Required 收件人国家（三态公司提供的对应国家英文名称） 
	 * recipientName xsd:string 1-35 Required 收件人姓名 
	 * recipientState xsd:string 1-30 * Required 收件人州或者省份 
	 * recipientCity xsd:string 1-35 Required 收件人城市 
	 * recipientAddress xsd:string 5-70 Required * 收件人地址/街道详细地址 
	 * recipientZipCode xsd:string 1-10 Required 收件人邮编 
	 * recipientPhone xsd:string 1-20 Required 收件人电话
	 * goodsDescription xsd:string 1-100 Required 包裹内物品描述 
	 * goodsQuantity xsd:int Required 包裹内物品数量 goodsDeclareWorth
	 * xsd:float Required 包裹内物品申报价值（单位美元USD） 
	 * orderStatus xsd:string Require 提交订单confirmed，订单预提交状态 preprocess，提交且交寄订单sumbmitted，默认为交寄状态
	 * evaluate xsd:float Required 投保价值 
	 * taxesNumber xsd:string 0-30 Required 税号
	 * isRemoteConfirm xsd:int 0-30 Required 是否同意收偏远费0不同意，1同意
	 * 
	 * 
	 * 
	 * ----------------------------------------details------------------------------------- 
	 * detailDescription xsd:string * 1-140 Required 详细物品描述 
	 * detailQuantity xsd:int Required 详细物品数量  
	 * detailWorth xsd:float Required 详细物品价值（单位美元USD）
	 * ---------------------------------------------------------------------
	 * 
	 * @param dbRow
	 * @return <b>Date:</b>2014-10-20下午4:15:51<br>
	 * @author: cuicong
	 * @throws Exception
	 */
	public String addOrder(Map<String, Object> info, List<Map<String, Object>> details) 
		throws Exception 
	{
		
		ShipRate_Service shipRate_Service = new ShipRate_Service();
		AddOrderRequest addOrderRequest = new AddOrderRequest();
		AddOrderRequestInfoArray add = new AddOrderRequestInfoArray();
		
		BeanUtils.populate(add, info);
		
		List<GoodsDetailsArray> arrays = new ArrayList<GoodsDetailsArray>();
		for (Map<String, Object> d : details)
		{
			GoodsDetailsArray array = new GoodsDetailsArray();
			BeanUtils.populate(array, d);
			arrays.add(array);
		}
		add.setGoodsDetails(arrays);
		add.setShippingMethod("CNAM");
		add.setOrderStatus("sumbmitted");
		
		addOrderRequest.setAddOrderRequestInfo(add);
		HeaderRequest request = new HeaderRequest();
		request.setAppKey(key);
		request.setToken(token);
		request.setUserId(userId);
		addOrderRequest.setHeaderRequest(request);
		AddOrderResponse addOrderResponse = shipRate_Service.getShipRateSOAP().addOrder(addOrderRequest);
		
		String trackingNumber = addOrderResponse.getOrderCode();
				
		return trackingNumber;
	}
	
	public String print(String orderCode) throws IOException {
		String url = "http://www.sendfromchina.com/api/label?orderCodeList=" + orderCode
				+ "&%20printType=1&print_type=html&printSize=3";
		URL url2 = new URL(url);
		URLConnection connection = url2.openConnection();
		connection.setDoOutput(true);
		OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "8859_1");
		out.write("username=kevin&password=*********"); // post的关键所在！
		out.flush();
		out.close();
		
		String sCurrentLine;
		String sTotalString;
		sCurrentLine = "";
		sTotalString = "";
		InputStream l_urlStream;
		l_urlStream = connection.getInputStream();
		// 传说中的三层包装阿！
		BufferedReader l_reader = new BufferedReader(new InputStreamReader(l_urlStream));
		while ((sCurrentLine = l_reader.readLine()) != null) {
			sTotalString += sCurrentLine;
			
		}
		
		return sTotalString;
	}
	
}
