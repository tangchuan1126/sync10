package com.cwc.db.zj;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class FloorProductSalesMgrZJ {
	private DBUTilAutoTranZJ dbUAutoTranZJ;
	
	/**
	 * 销售额
	 * @param deliveryDate
	 * @param end
	 * @return
	 * @throws Exception
	 */
	public long statsProdyctSalesByDelivey(String deliveryDate,String end)
		throws Exception
	{
		try 
		{
			return (dbUAutoTranZJ.statsProdyctSalesByDelivey(deliveryDate, end));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductSalesMgrZJ statsProdyctSalesByDelivey error:"+e);
		}
	}
	
	/**
	 * 获得销售的最后一条记录
	 * @return
	 * @throws Exception
	 */
	public DBRow productSalesLastTime()
		throws Exception
	{
		try 
		{
			String sql = "select * from product_sales order by delivery_date desc limit 1";
			
			return (dbUAutoTranZJ.selectSingle(sql));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductSalesMgrZJ ProductSalesLastTime error:"+e);
		}
	}
	
	public DBRow[] fillterProductSalesZJ(long product_id,long catalog_id,long pro_line_id,String order_source,String start_date,String end_date,PageCtrl pc)
		throws Exception
	{
		
		String cid = "";
		if(catalog_id>0)
		{
			cid = " join pc_child_list as pcl on ps.catalog_id = pcl.pc_id and search_rootid="+catalog_id+" ";
		}
		
		String productline="";
		if(pro_line_id>0)
		{
			productline = " and pc.product_line_id="+pro_line_id+" ";
		}
		
		String product = "";
		if(product_id !=0)
		{
			product = " and product_id ="+product_id+" ";
		}
		
		String orderSource = "";
		//订单来源
		if(!order_source.equals(""))
		{
			orderSource = " and order_source ='"+order_source+"'";
		}
		
		//产品线，产品分类实际上查的的都是产品类别
		String sql = "select * from "+ConfigBean.getStringValue("product_sales")+" as ps " 
					+cid
					+"join product_catalog as pc on ps.catalog_id = pc.id "+productline
					+" where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59'"+product+orderSource;
		
		
		
		return dbUAutoTranZJ.selectMutliple(sql.toString(),pc);
	}
	
	/**
	 * 查询国家销售额，成本，利润
	 * @param country_id
	 * @param cmd
	 * @param catalog_ids
	 * @param product_id
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception
	 */
	public DBRow getAllCountryProductSalesOrProfitOrCost(long country_id,String cmd,long catalog_id,long pro_line_id,long cid,long product_id,String order_source,String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String whereProductLine = "";
			if(pro_line_id>0)
			{
				whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
			}
			
			String whereCatalog = "";
			if(catalog_id>0)
			{
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = ps.catalog_id and pcl.search_rootid = "+catalog_id+" ";
				
				
			}
			
			String whereProduct = "";
			if(product_id>0)
			{
				whereProduct = " and ps.product_id = "+product_id+" ";
			}
			
			String whereStorage = "";
			if(cid>0)
			{
				whereStorage = " and ps.storage_id = "+cid+" ";
			}
			
			String select = "";
			if(cmd.equals("salesRoom"))
			{
				select = " sum(saleroom) as price,";
			}
			else if(cmd.equals("cost"))
			{
				select = " sum(product_cost) as price,";
			}
			else if(cmd.equals("profit"))
			{
				select = " sum(profit) as price,";
			}
			
			StringBuffer sql = new StringBuffer("select"+select+" country_id from "+ConfigBean.getStringValue("product_sales")+" as ps "
											   +"join product_catalog as pc on ps.catalog_id = pc.id "+whereProductLine
											   +whereCatalog
											   +"where country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59'"+whereProduct+whereStorage);
			DBRow para = new DBRow();
			para.add("country_id",country_id);
			
			if(product_id !=0)
			{
				sql.append(" and product_id=?");
				para.add("product_id",product_id);
			}
			
			if(!order_source.equals(""))
			{
				sql.append(" and order_source ='"+order_source+"'");
			}
			
			sql.append(" group by country_id");
			
			return (dbUAutoTranZJ.selectPreSingle(sql.toString(),para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllCountryProductSalesOrProfitOrCostByLineId(long country_id,String pro_line_id, String cmd, String start_date, String end_date) error:"+e);
		}
	}
	
	/**
	 * 根据产品来源查询产品销售额、利润和成本在各国的各区域的分布情况
	 * @param province_id
	 * @param cmd
	 * @param order_source
	 * @param end_date 
	 * @param start_date 
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceSalesOrProfitOrCostByOrderSource(long province_id,String cmd,long product_id,long catalog_id,long pro_line_id,String order_source, String start_date, String end_date,long cid) 
		throws Exception 
	{
		try 
		{
			String whereProductLine = "";
			if(pro_line_id>0)
			{
				whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
			}
			
			String whereCatalog = "";
			if(catalog_id>0)
			{
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = ps.catalog_id and pcl.search_rootid = "+catalog_id+" ";
				
				
			}
			
			String whereProduct = "";
			if(product_id>0)
			{
				whereProduct = " and ps.product_id = "+product_id+" ";
			}
			
			
			String select = "";
			if(cmd.equals("salesRoom"))
			{
				select = " sum(saleroom) as price,";
			}
			else if(cmd.equals("cost"))
			{
				select = " sum(product_cost) as price,";
			}
			else if(cmd.equals("profit"))
			{
				select = " sum(profit) as price,";
			}
			
			StringBuffer sql = new StringBuffer("select"+select+" country_id from "+ConfigBean.getStringValue("product_sales")+" as ps "
											   +"join product_catalog as pc on ps.catalog_id = pc.id "+whereProductLine
											   +whereCatalog					   
											   +" where ps.province_id = ? and ps.delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59'"+whereProduct);
			DBRow para = new DBRow();
			para.add("province_id",province_id);
			
			if(product_id !=0)
			{
				sql.append(" and product_id=?");
				para.add("product_id",product_id);
			}
			
			if(!order_source.equals(""))
			{
				sql.append(" and order_source ='"+order_source+"'");
			}
			
			sql.append(" group by province_id");
			
			return (dbUAutoTranZJ.selectPreSingle(sql.toString(),para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceSalesOrProfitOrCostByOrderSource(long province_id,String cmd, String order_source) error："+e);
		}
	}
	
	public DBRow[] salesDeliveryAnalysis(String start_date,String end_date,long catalog_id,long pro_line_id,long pc_id,long ca_id,long ccid,long pro_id,long cid,int type,String order_sorce,String day_source,PageCtrl pc)
		throws Exception
	{
		//type 1代表求和计算，group by以选中的地域层级  2代表分组求和group by 选中级别的下一级别
		String innerGroupBy = "";//构成基础数据group by条件
		String formGroupBy = "";//分日期统计group by条件
		
		
		String whereProductLine = "";
		if(pro_line_id>0)
		{
			whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
		}
		
		String whereCatalog = "";
		if(catalog_id>0)
		{
			whereCatalog = " join pc_child_list as pcl on pcl.pc_id = ps.catalog_id and pcl.search_rootid = "+catalog_id+" ";
		}
		
		String whereStorage = "";
		if(cid>0)
		{
			whereStorage = " and psc.id="+cid+" ";
		}
		
		String whereProduct = "";
		if(pc_id>0)
		{
			whereProduct = " and ps.product_id = "+pc_id+" ";
		}
		
		String whereArea = "";
		if(ca_id>0)
		{
			whereArea = " and ca.ca_id = "+ca_id+" ";	
		}
		else
		{
			if(type == 2)//求和时销售区域已经自动求和
			{
				innerGroupBy = ",ca.ca_id ";
				formGroupBy = ",ca_id ";
			}
		}
		
		String whereCountry = "";
		if(ccid>0)
		{
			whereCountry = " and ps.country_id = "+ccid+" ";
		}
		else
		{
			if(type==2&&ca_id>0)
			{
				innerGroupBy = ",ps.country_id ";
				formGroupBy = ",nation_id ";
			}
		}
		
		String whereProvince = "";
		if(pro_id>0)
		{
			whereProvince = " and ps.province_id = "+pro_id+" ";
		}
		else
		{
			if(type==2&&ccid>0)
			{
				innerGroupBy = ",ps.province_id ";
				formGroupBy = ",province_id ";
			}
		}
		
		String whereOrderSource = "";
		if(!order_sorce.trim().equals(""))
		{
			whereOrderSource = " and ps.order_source='"+order_sorce+"'";
		}
		
		DBRow[] dateStr =getDate(start_date,end_date);
		String sql = " SELECT product_id,p_name,pl_name,title,storage_title,area_name,c_country,pro_name,sum(sum_quantity) as sum_quantity,sum(sum_product_cost) as sum_product_cost,sum(sum_freight) as sum_freight,sum(sum_weight) as sum_weight,sum(sum_saleroom) as sum_saleroom,sum(sum_profit) as sum_profit,order_source,";
		
		for(int i=0; i<dateStr.length; i++)
		{
			sql +="  SUM(CASE delivery_date  WHEN '"+dateStr[i].getString("date")+"' THEN "+day_source+" ELSE 0  END)  date_"+i;
			if(i!=(dateStr.length-1))
			{
				sql +=" , ";
			}
		}
		
		sql +=" FROM (";
		sql += "select p.pc_id as product_id,p.p_name,pld.`name` as pl_name,pc.title,psc.title as storage_title,ca.ca_id,ca.`name` as area_name,ps.country_id as nation_id,cc.c_country,ps.province_id,cp.pro_name,left(ps.delivery_date,10) as delivery_date,ps.order_source,sum(ps.quantity) as sum_quantity,sum(ps.product_cost) as sum_product_cost,sum(ps.freight) as sum_freight,sum(ps.weight) as sum_weight,sum(ps.saleroom) as sum_saleroom,sum(ps.profit) as sum_profit "
			  +"from product_sales as ps "
			  +"join product_catalog as pc on ps.catalog_id = pc.id "+whereProductLine
			  +whereCatalog
			  +"join product as p on p.pc_id = ps.product_id "+whereProduct
			  +"join country_area_mapping as cam on cam.ccid = ps.country_id "+whereCountry
			  +"join country_area as ca on ca.ca_id = cam.ca_id "+whereArea
			  +"join country_code as cc on cc.ccid = cam.ccid "
			  +"left join country_province as cp on cp.pro_id = ps.province_id "+whereProvince//左联，省份有可能为others-1
			  +"left join product_line_define as pld on pld.id = pc.product_line_id "//左联有可能商品未定义产品线
			  +"join product_storage_catalog as psc on psc.id = ps.storage_id "+whereStorage
			  +"where ps.delivery_date BETWEEN '"+start_date+" 0:00:00' AND '"+end_date+" 23:59:59' "+whereOrderSource
			  +"group by ps.product_id,Left(delivery_date, 10) "+innerGroupBy;
		sql +=" ) as basedata GROUP BY product_id "+formGroupBy;
		sql +=" ORDER BY pl_name,title,p_name ";

		//system.out.println(sql);
		return (dbUAutoTranZJ.selectMutliple(sql, pc));
	}
	
	public DBRow[]  getDate(String startDate,String endDate)
		throws Exception 
	{
	
		//缺少后面天大于 等于前面天的判断
		 long  DAY = 24L * 60L * 60L * 1000L;   
		 Date dateTemp = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
	     Date dateTemp2 = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
		 long n =  (dateTemp2.getTime() - dateTemp.getTime() ) / DAY   ;
		 int num = Long.valueOf(n).intValue();
		 DBRow[] row = new DBRow[num+1];
		 Calendar calendarTemp = Calendar.getInstance();
		 calendarTemp.setTime(dateTemp);
	     int i= 0;
	     while (calendarTemp.getTime().getTime()!= dateTemp2.getTime())
	     {
	            String temp = new SimpleDateFormat("yyyy-MM-dd").format(calendarTemp.getTime()) ;
	            row[i] = new DBRow();
	            row[i].add("date",temp );
	            calendarTemp.add(Calendar.DAY_OF_YEAR, 1);
	            i++;
	     }
	     row[i] = new DBRow();
	     row[i].add("date", new SimpleDateFormat("yyyy-MM-dd").format(dateTemp2.getTime()) );
		 return row;
	}

	public void setDbUAutoTranZJ(DBUTilAutoTranZJ dbUAutoTranZJ)
	{
		this.dbUAutoTranZJ = dbUAutoTranZJ;
	}
}
