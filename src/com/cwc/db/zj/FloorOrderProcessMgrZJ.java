package com.cwc.db.zj;

import com.cwc.app.key.CodeTypeKey;
import com.cwc.db.DBRow;

public class FloorOrderProcessMgrZJ {
	private DBUTilAutoTranZJ dbUTilAutoTranZJ;
	/**
	 * 第一张表
	 * @param start_date
	 * @param cid
	 * @param end_date
	 * @throws Exception
	 */
	public long statcProcuctNeedQuantity(String start_date, long cid, String end_date) 
		throws Exception
	{
		try 
		{
			return (dbUTilAutoTranZJ.statcProcuctNeedQuantity(start_date, cid, end_date));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderProcessMgrZJ statcProcuctNeedQuantity error:"+e);
		}
	}
	/**
	 * 第二张表
	 * @param start_date
	 * @param end_date
	 * @throws Exception
	 */
	public long statsTheoryStorageForProduct(String start_date, String end_date) 
		throws Exception
	{
		try 
		{
			return (dbUTilAutoTranZJ.statsTheoryStorageForProduct(start_date, end_date));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderProcessMgrZJ statsTheoryStorageForProduct error:"+e);
		}
	}
	
	/**
	 * 第三张表
	 * @param start_date
	 * @param end_date
	 * @throws Exception
	 */
	public long statsOriginalStoragesProduct(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			return (dbUTilAutoTranZJ.statsOriginalStoragesProduct(start_date, end_date));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderProcessMgrZJ statsOriginalStoragesProduct error:"+e);
		}
	}
	
	/**
	 * 第四张
	 * @param start_date
	 * @param end_date
	 * @throws Exception
	 */
	public long statsPlanStorageProductSales(String start_date, String end_date) 
		throws Exception
	{
		try 
		{
			return (dbUTilAutoTranZJ.statsPlanStorageProductSales(start_date, end_date));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderProcessMgrZJ statsPlanStorageProductSales error:"+e);
		}
	}
	
	/**
	 * 获得最后一条的的发货时间
	 * @return
	 * @throws Exception
	 */
	public DBRow getLastTime()
		throws Exception
	{
		try 
		{
			String sql = "select * from actual_storage_sales order by delivery_date desc limit 1";
			
			return (dbUTilAutoTranZJ.selectSingle(sql));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderProcessMgrZJ getLastTime error:"+e);
		}
	}
	
	/**
	 * 查询国家所需货物数量
	 * @param country_id
	 * @param product_id
	 * @param catalog_ids
	 * @param st_date
	 * @param end_date
	 * @return
	 * @throws Exception
	 */
	public DBRow getCountryProductNeedNumZJ(long country_id,long product_id,long catalog_id,long pro_line_id,String st_date,String end_date)
		throws Exception
	{
		try 
		{
			String whereProductLine = "";
			if(pro_line_id>0)
			{
				whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
			}
			
			String whereCatalog = "";
			if(catalog_id>0)
			{
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = p.catalog_id and pcl.search_rootid = "+catalog_id+" ";
			}
			
			String whereProduct = "";
			if(product_id>0)
			{
				whereProduct = " and p.pc_id= "+product_id+" ";
			}
			
			String sql = "select sum(quantity) as total_quantity,nation_id from (" 
					+"select pi.quantity as quantity,po.ccid as nation_id from porder_item as pi "
					+"join porder as po on pi.oid = po.oid and (pi.product_type=1 or pi.product_type=2) "
					+"join product as p on pi.pid = p.pc_id "+whereProduct
					+"join product_catalog as pc on p.catalog_id = pc.id "+whereProductLine
					+whereCatalog
					+"left join product_line_define as pld on pld.id = pc.product_line_id "
					+"where po.post_date>'"+st_date+" 0:00:00' and po.post_date<'"+end_date+" 23:59:59' and po.ccid="+country_id+" "
					+"union all "
					+"select (pi.quantity*pu.quantity) as quantity,po.ccid as nation_id from porder_item as pi "
					+"join porder as po on pi.oid = po.oid "
					+"join product_union as pu on pu.set_pid = pi.pid and pi.product_type = 4 "
					+"join product as p on p.pc_id = pu.pid "+whereProduct
					+"join product_catalog as pc on p.catalog_id = pc.id "+whereProductLine
					+whereCatalog
					+"left join product_line_define as pld on pld.id = pc.product_line_id "
					+"where po.post_date>'"+st_date+" 0:00:00' and po.post_date<'"+end_date+" 23:59:59' and po.ccid="+country_id+" "
					+") as country_count group by nation_id ";
			
			
//			StringBuffer sql = new StringBuffer("select sum(total_quantity) as total_quantity,nation_id from "+ConfigBean.getStringValue("actual_storage_sales")+" as ass " 
//											   +"join product_catalog as pc on ass.catalog_id = pc.id "+whereProductLine
//											   +whereCatalog
//											   +" where nation_id = "+country_id+" and delivery_date between '"+st_date+" 0:00:00' and '"+end_date+" 23:59:59'"+whereProduct);
//			
//			
//			if(product_id !=0)
//			{
//				sql.append(" and product_id = "+product_id+" ");
//			}
			
//			sql.append(" group by nation_id");
			
			return (dbUTilAutoTranZJ.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorOrderProcessMgrTJH getCountryProductNeedNumZJ error:"+e);
		}
	}
	
	/**
	 * 根据产品线查询某一时间段内某一个国家下的某一区域的实际的产品需求总额
	 * @param province_id
	 * @param pro_line_id
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception
	 */
	public DBRow getAllProvinceSpreadProductNeedByLineIdZJ(long province_id,long catalog_id,long pro_line_id,long product_id,String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String whereProductLine = "";
			if(pro_line_id>0)
			{
				whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
			}
			
			String whereCatalog = "";
			if(catalog_id>0)
			{
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = p.catalog_id and pcl.search_rootid = "+catalog_id+" ";
				
				
			}
			
			String whereProduct = "";
			if(product_id>0)
			{
				whereProduct = " and p.pc_id = "+product_id+" ";
			}
			
			String sql = "select sum(quantity) as total_quantity,province_id from (" 
				+"select pi.quantity as quantity,po.pro_id as province_id from porder_item as pi "
				+"join porder as po on pi.oid = po.oid and (pi.product_type=1 or pi.product_type=2) "
				+"join product as p on pi.pid = p.pc_id "+whereProduct
				+"join product_catalog as pc on p.catalog_id = pc.id "+whereProductLine
				+whereCatalog
				+"left join product_line_define as pld on pld.id = pc.product_line_id "
				+"where po.post_date>'"+start_date+" 0:00:00' and po.post_date<'"+end_date+" 23:59:59' and po.pro_id="+province_id+" "
				+"union all "
				+"select (pi.quantity*pu.quantity) as quantity,po.pro_id as province_id from porder_item as pi "
				+"join porder as po on pi.oid = po.oid "
				+"join product_union as pu on pu.set_pid = pi.pid and pi.product_type = 4 "
				+"join product as p on p.pc_id = pu.pid "+whereProduct
				+"join product_catalog as pc on p.catalog_id = pc.id "+whereProductLine
				+whereCatalog
				+"left join product_line_define as pld on pld.id = pc.product_line_id "
				+"where po.post_date>'"+start_date+" 0:00:00' and po.post_date<'"+end_date+" 23:59:59' and po.pro_id="+province_id+" "
				+") as province_count group by province_id ";
			
//			StringBuffer sql = new StringBuffer("select sum(total_quantity) as total_quantity,province_id from "+ConfigBean.getStringValue("actual_storage_sales")+" as ass "
//											   +"join product_catalog as pc on ass.catalog_id = pc.id "+whereProductLine
//											   +whereCatalog
//											   +" where province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59'"+whereProduct);
//			DBRow para = new DBRow();
//			para.add("province_id",province_id);
//			
//			sql.append(" group by province_id");
			
			return (dbUTilAutoTranZJ.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProvinceSpreadProductNeedByLineId(long province_id,String pro_line_id, String start_date, String end_date) error:"+e);
		}
	}
	
	/**
	 * 根据开始截止时间，获得商品需求数量（定制商品打散，所有商品未运行代发关系）
	 * @param pc_id
	 * @param st
	 * @param en
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductDemandAnalysisGroupByPs(long pc_id,long ps_id,String st,String en)
		throws Exception
	{
		try 
		{
			String whereProduct = "";
			if(pc_id>0)
			{
				whereProduct = " and p.pc_id ="+pc_id+" ";
			}
			
			String whereStore = "";
			if(ps_id>0)
			{
				whereStore = " and pi.prefer_ps_id = "+ps_id+" ";
			}
			
			
			String sql = "select pc_id,p_name,p_code,avg(quantity) as demand_analysis_count,ccid,pro_id,prefer_ps_id from "
				+"("
				+"	select p.pc_id,p.p_name,pcode.p_code,pi.quantity as quantity,po.ccid,po.pro_id,pi.prefer_ps_id,pi.plan_ps_id,pi.modality from porder_item as pi "
				+"	join porder as po on pi.oid = po.oid and (pi.product_type=1 or pi.product_type=2) "
				+"	join product as p on pi.pid = p.pc_id "+whereProduct
				+"	join product_code as pcode on p.pc_id = pcode.pc_id and code_type="+CodeTypeKey.Main+" "
				+"	where po.post_date>'"+st+" 0:00:00' and po.post_date<'"+en+" 23:59:59'"+whereStore
//				+"	group by p.pc_id,pi.prefer_ps_id "
				+"	union all "
				+"	select p.pc_id,p.p_name,pcode.p_code,(pi.quantity*pu.quantity) as quantity,po.ccid,po.pro_id,pi.prefer_ps_id,pi.plan_ps_id,pi.modality from porder_item as pi "
				+"	join porder as po on pi.oid = po.oid "
				+"	join product_union as pu on pu.set_pid = pi.pid and pi.product_type = 4 "
				+"	join product as p on p.pc_id = pu.pid "+whereProduct
				+"	join product_code as pcode on p.pc_id = pcode.pc_id and code_type="+CodeTypeKey.Main+" "
				+"	where po.post_date>'"+st+" 0:00:00' and po.post_date<'"+en+" 23:59:59'"+whereStore
//				+"	group by p.pc_id,pi.prefer_ps_id "
				+") as demandAnalysis group by pc_id,prefer_ps_id";
			
			return dbUTilAutoTranZJ.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderProcessMgrZJ getProductDemandAnalysisGroupByPs error:"+e);
		}
	}
	
	/**
	 * 根据开始结束时间，统计备货需求（运行过代发关系）
	 * @param pc_id
	 * @param ps_id
	 * @param st
	 * @param en
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductPlanAnalysisGroupByPs(long pc_id,long ps_id,String st,String en)
		throws Exception
	{
		try 
		{
			String whereProduct = "";
			if(pc_id>0)
			{
				whereProduct = " and p.pc_id = "+pc_id+" ";
			}
			
			String whereStore = "";
			if(ps_id>0)
			{
				whereStore = " and plan_ps_id="+ps_id+" ";
			}
			
			String sql ="select pc_id,p_name,avg(quantity) as plan_analysis_count,plan_ps_id from "
						+"("
						+"	select p.pc_id,p.p_name,(pi.quantity*pu.quantity) as quantity,plan_ps_id,pi.modality from porder_item as pi " 
						+"	join porder as po on pi.oid=po.oid and (pi.modality = 2 or pi.product_type=4) "
						+"	join product_union as pu on pi.pid = pu.set_pid "
						+"	join product as p on p.pc_id= pu.pid "+whereProduct
						+"	where po.post_date between '"+st+" 0:00:00' and '"+en+" 23:59:59'"+whereStore
						+"	union all "
						+"	select p.pc_id as product_id,p.p_name,pi.quantity as quantity,plan_ps_id,pi.modality from porder_item as pi " 
						+"	join porder as po on pi.oid=po.oid and (pi.modality = 1) "
						+"	join product as p on p.pc_id= pi.pid "+whereProduct
						+"	where po.post_date between '"+st+" 0:00:00' and '"+en+" 23:59:59'"+whereStore
						+") as planAnalysis group by pc_id,plan_ps_id";
			
			return dbUTilAutoTranZJ.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderProcessMgrZJ getProductPlanAnalysisGroupByPs("+pc_id+","+ps_id+","+st+","+en+")");
		}
	}
	
	public DBRow getProductSendAnalysisGroupByPs(long pc_id,long ps_id,String st,String en)
		throws Exception
	{
		try 
		{
			String sql ="select pc_id,avg(quantity) as all_send_quantity from "
						+"("
						+"select woi.pc_id,quantity,wo.ps_id from waybill_order_item as woi "
						+"join waybill_order as wo on woi.waybill_order_id = wo.waybill_id "
						+"join product p on p.pc_id = woi.pc_id "
						+"where wo.delivery_date BETWEEN '"+st+"' and '"+en+" 23:59:59' "
						+"and (woi.product_type =1 or woi.product_type = 2) and woi.ps_id = "+ps_id+" " 
						+"and woi.pc_id = "+pc_id+" "
						+"union all "
						+"select p.pc_id,(woi.quantity*pu.quantity)as quantity,wo.ps_id from waybill_order_item as woi "
						+"join waybill_order as wo on woi.waybill_order_id = wo.waybill_id "
						+"join product_union as pu on pu.set_pid = woi.pc_id and (woi.product_type = 3 or woi.product_type = 4)"
						+"join product as p on p.pc_id = pu.pid and p.pc_id = "+pc_id+" "
						+"where wo.delivery_date BETWEEN '"+st+"' and '"+en+" 23:59:59' "
						+"and wo.ps_id = "+ps_id+" "
						+")as send_count_view group by ps_id,pc_id";
			
			
			return dbUTilAutoTranZJ.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorOrderProcessMgrZJ getProductSendAnalysisGroupByPs error:"+e);
		}
	}
	
	

	public void setDbUTilAutoTranZJ(DBUTilAutoTranZJ dbUTilAutoTranZJ) {
		this.dbUTilAutoTranZJ = dbUTilAutoTranZJ;
	}
}
