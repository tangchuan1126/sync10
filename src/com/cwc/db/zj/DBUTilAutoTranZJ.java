package com.cwc.db.zj;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.cwc.app.api.SystemConfig;
import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.service.order.action.OrderCostBean;
import com.cwc.spring.util.MvcUtil;
public class DBUTilAutoTranZJ extends DBUtilAutoTran {
    
	static Logger log = Logger.getLogger("DB");
	private SystemConfig systemConfig;
	
	public DBUTilAutoTranZJ() throws Exception {
		super();
	}
	
	public long statsProdyctSalesByDelivey(String start,String end)
		throws SQLException
	{
		String error = null;
		try 
		{
			Connection conn = DataSourceUtils.getConnection(dataSource);
			String querySql = null;
			
			Statement ps = null;
			Statement ps2 = null;
			Statement ps3 = null;
			Statement ps4 = null;
			
			ResultSet rs = null;
			ResultSet rs2 = null;
			
			try 
			{
				ps4 = conn.createStatement();
				ps4.executeUpdate("delete from product_sales where delivery_date between '"+start+" 00:00:00' and '"+end+" 23:59:59'");
				closeConn(null,ps4,null);
				
				querySql = "(select `o`.`post_date` AS `post_date`,`o`.`parent_oid` AS `parent_oid`,`o`.`ps_id` AS `ps_id`,`o`.`print_date` AS `print_date`,`o`.`handle` AS `handle`,`o`.`handle_status` AS `handle_status`,`o`.`product_status` AS `product_status`,`o`.`delivery_date` AS `delivery_date`,`o`.`ccid` AS ccid,`o`.`pro_id` AS pro_id,`o`.`sc_id` AS `sc_id`,`o`.`order_source` AS `order_source`,`o`.`mc_gross_rmb` AS `mc_gross_rmb`,`o`.`total_weight` AS `total_weight`,`o`.`product_cost` AS `product_cost`,`o`.`shipping_cost` AS `shipping_cost`,`oi`.`lacking` AS `lacking`,`oi`.`pid` AS `p_pid`,`oi`.`name` AS `p_name`,`oi`.`quantity` AS `p_quantity`,`oi`.`pid` AS `set_pid`,`oi`.`name` AS `set_name`,`oi`.`quantity` AS `set_quantity`,`oi`.`iid` AS `iid`,`oi`.`oid` AS `oid`,`oi`.`catalog_id` AS `catalog_id`,`oi`.`unit_price` AS `unit_price`,`oi`.`gross_profit` AS `gross_profit`,`oi`.`weight` AS `weight`,`oi`.`unit_name` AS `unit_name`,`oi`.`product_type` AS `product_type`"
						  +"from (`porder` `o` join `porder_item` `oi` on((`o`.`oid` = `oi`.`oid`))) where (((`oi`.`product_type` = 1) or (`oi`.`product_type` = 2) or (`oi`.`product_type` = 3)) and (`o`.`delivery_date` between '"+start+" 0:00:00' and '"+end+" 23:59:59'))) " 
						  +"union all "
						  +"(select `o`.`post_date` AS `post_date`,`o`.`parent_oid` AS `parent_oid`,`o`.`ps_id` AS `ps_id`,`o`.`print_date` AS `print_date`,`o`.`handle` AS `handle`,`o`.`handle_status` AS `handle_status`,`o`.`product_status` AS `product_status`,`o`.`delivery_date` AS `delivery_date`,`o`.`ccid` AS ccid,`o`.`pro_id` AS pro_id,`o`.`sc_id` AS `sc_id`,`o`.`order_source` AS `order_source`,`o`.`mc_gross_rmb` AS `mc_gross_rmb`,`o`.`total_weight` AS `total_weight`,`o`.`product_cost` AS `product_cost`,`o`.`shipping_cost` AS `shipping_cost`,`oi`.`lacking` AS `lacking`,`p`.`pc_id` AS `p_pid`,`p`.`p_name` AS `p_name`,(`pu`.`quantity` * `oi`.`quantity`) AS `p_quantity`,`oi`.`pid` AS `set_pid`,`oi`.`name` AS `set_name`,`oi`.`quantity` AS `set_quantity`,`oi`.`iid` AS `iid`,`oi`.`oid` AS `oid`,`p`.`catalog_id` AS `catalog_id`,`p`.`unit_price` AS `unit_price`,`p`.`gross_profit` AS `gross_profit`,`p`.`weight` AS `weight`,`p`.`unit_name` AS `unit_name`,`oi`.`product_type` AS `product_type` from (((`porder` `o` join `porder_item` `oi` on((`o`.`oid` = `oi`.`oid`))) join `product_union` `pu` on((`oi`.`pid` = `pu`.`set_pid`))) join `product` `p` on((`pu`.`pid` = `p`.`pc_id`))) where (((`oi`.`product_type` = 4)) and (`o`.`delivery_date` between '"+start+" 0:00:00' and '"+end+" 23:59:59')))";
				ps = conn.createStatement();
				
				rs = ps.executeQuery(querySql);

				long allcount = 0;
				while(rs!=null&&rs.next())
				{
					allcount++;
					String sql;//插入的jdbc用sql
					
					long ps_id = getSequance("product_sales");
					
					long oid = rs.getLong("oid");
					long product_id = rs.getLong("p_pid");
					double quantity = rs.getDouble("p_quantity");
					String delivery_date = rs.getString("delivery_date");
					long country_id = rs.getLong("ccid");
					long sc_id = rs.getLong("sc_id");//运输公司ID
					long province_id = rs.getLong("pro_id");
					double unit_price = rs.getDouble("unit_price");
					double weight = rs.getDouble("weight");
					String single_date = rs.getString("post_date");
					long storage_id = rs.getLong("ps_id");
					
					com.cwc.app.api.ProductMgr productMgr = (com.cwc.app.api.ProductMgr)MvcUtil.getBeanFromContainer("productMgr");
					DBRow product = productMgr.getDetailProductByPcid(product_id);
					
					double one_product_cost = product.get("unit_price",0d)*quantity;   //计算商品成本(已乘成本修正率)
					double revise_weight = product.get("weight",0f)*quantity;   //计算该订单中所订商品的总的重量(已乘重量修正率)
					String order_source = rs.getString("order_source");//订单来源
					long catalog_id = rs.getLong("catalog_id");
					
					//若是子订单需获得父订单的值
					double gross_sales = rs.getDouble("mc_gross_rmb");  //订单总销售额(已转化为人民币)
					double total_weight = rs.getDouble("total_weight");  //订单总重量,未乘修正率
					double total_cost = rs.getDouble("product_cost");//商品总成本(已转化为人民币)，未乘修正率
					
					double save_one_product_cost = 0;
					
					double now_product_cost = 0;//现在的商品成本
					double now_total_weight = 0;//现在的商品总重量
					
					error = "sc_id:"+sc_id+",oid:"+oid;
					try 
					{
						OrderCostBean orderCostBean = getOrderCostBean(oid,0,0);
						
						now_total_weight = orderCostBean.getWeight();
						
						now_product_cost = orderCostBean.getProductCost();
						
					}
					catch (WeightOutSizeException e) 
					{
					}
					catch (WeightCrossException e) 
					{
					}
					catch (CountryOutSizeException e) 
					{
					}
					catch(ProvinceOutSizeException e)
					{
					}
					

					double shoppcost = rs.getDouble("shipping_cost");   //订单总运费(已转化为人民币)，未乘订单来源利润率
					long parentId = rs.getLong("parent_oid");

					if(parentId>0)
					{
						
							
							ps3 = conn.createStatement();
							
							rs2 = ps3.executeQuery("select * from porder where oid="+parentId);
							if(rs2.next())
							{
								total_weight = rs2.getDouble("total_weight");//    //主订单的总重量
								gross_sales = rs2.getDouble("mc_gross_rmb");//order.get("mc_gross_rmb", 0d);  //订单总销售额(已转化为人民币)
								total_cost = rs2.getDouble("product_cost");//总订单商品成本
								
								oid = parentId;//便于统计
								
								try 
								{
									if (gross_sales!=0)
									{
										OrderCostBean orderCostBean = getOrderCostBean(parentId,0,0);
										
										now_total_weight = orderCostBean.getWeight();
										
										now_product_cost = orderCostBean.getProductCost();
									}
								} 
								catch (WeightOutSizeException e) 
								{
								}
								catch (WeightCrossException e) 
								{
								}
								catch (CountryOutSizeException e) 
								{
								}
								catch(ProvinceOutSizeException e)
								{
								}
								
								
								shoppcost = rs2.getDouble("shipping_cost");//order.get("shipping_cost", 0d);   //订单总运费(已转化为人民币)
							}
							closeConn(rs2,ps3,null);
					}
					
					 double cost_ratio = one_product_cost/now_product_cost;;  // 成本比率(公式：商品成本/订单总成本[已乘成本修正率])
					
					 double weight_ratio = revise_weight/now_total_weight;  //重量比率  （公式：商品的总重量/订单总重量[已乘以重量修正率]）
					 
					 save_one_product_cost = cost_ratio*total_cost;//计算商品成本
					 
                  	 double freight = weight_ratio*shoppcost;  //运费的计算(公式：重量比率*订单总运费)
					
					 double saleroom = cost_ratio*gross_sales;  //计算销售额（公式：成本比率×订单的总销售额 ）
					 
					 weight = total_weight*weight_ratio;	//计算商品所占重量（订单总重量*重量比率）
					
					 double profit = saleroom-freight-save_one_product_cost;  //计算出的利润(公式：该产品销售额-运费-商品成本)
					 
					 sql = "insert into product_sales (ps_id,oid,product_id,country_id,province_id,delivery_date,quantity,single_date,product_cost,weight,order_source,catalog_id,saleroom,freight,profit,storage_id) " +
					 "values ("+ps_id+","+oid+","+product_id+","+country_id+","+province_id+",'"+delivery_date+"',"+quantity+",'"+single_date+"',"+save_one_product_cost+","+weight+",'"+order_source+"',"+catalog_id+","+saleroom+","+freight+","+profit+","+storage_id+")";
					 
//					if(gross_sales != 0 && now_total_weight != 0)
//					{
//						
//						
//					}
//					else
//					{
//						sql = "insert into product_sales (ps_id,oid,product_id,country_id,province_id,delivery_date,quantity,single_date,product_cost,weight,order_source,catalog_id) " +
//						 "values ("+ps_id+","+oid+","+product_id+","+country_id+","+province_id+",'"+delivery_date+"',"+quantity+",'"+single_date+"',"+save_one_product_cost+","+weight+",'"+order_source+"',"+catalog_id+")";
//					}
					
					ps2 = conn.createStatement();
					ps2.executeUpdate(sql);
					closeConn(null, ps2, null);
					
				}
				
				return (allcount);
			}
			catch (SQLException e) 
			{
				 //system.out.println(e.getMessage());
				 throw new SQLException(e.getMessage()+error);
			}
			finally
			{
			    closeConn(rs, ps, conn);
			    closeConn(null, ps2,null);
			    closeConn(rs2,ps3,null);
			    closeConn(null,ps4,null);
			}
			
			
		}
		catch (Exception e) 
		{
			log.error("error:" + e+"error--Message:"+error);
	        throw new SQLException("error:" + e);
		}
	}
	
	/**
	 * 根据订单查询实际产品需求分布并保存数据（第一张表）
	 * @param pc
	 * @param start_date
	 * @param cid
	 * @param end_date
	 * @throws Exception
	 */
	public long statcProcuctNeedQuantity(String start_date, long cid, String end_date) 
		throws Exception
	{
		try 
		{
			Connection conn = DataSourceUtils.getConnection(dataSource);
			StringBuffer querySql = null;
			
			Statement psQuery = null;
			Statement psInsert = null;
			Statement psDel = null;
			
			ResultSet rs = null;
			
			com.cwc.app.api.ProductMgr productMgr = (com.cwc.app.api.ProductMgr)MvcUtil.getBeanFromContainer("productMgr");
			
			long allCount = 0;
			
			try 
			{
				//删除选定时间内的以前统计的数据
				psDel = conn.createStatement();
				psDel.executeUpdate("delete from actual_storage_sales where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'");
				closeConn(null,psDel,null);
				
				psQuery = conn.createStatement();
				
				String cidString = "";
				if(cid != 0)
				{
					cidString = " and p.ps_id = "+cid;
				}
				String sql ="select post_date,parent_oid,ps_id,print_date,handle,handle_status,product_status,delivery_date,ccid,pro_id,lacking,p_pid,p_name,sum(p_quantity) as p_quantity,set_pid,set_name,set_quantity,iid,oid,catalog_id,unit_price,gross_profit,weight,unit_name,product_type from" 
							+"(" 
							+"("
							+"select `o`.`post_date` AS `post_date`,`o`.`parent_oid` AS `parent_oid`,`o`.`ps_id` AS `ps_id`,`o`.`print_date` AS `print_date`,`o`.`handle` AS `handle`,`o`.`handle_status` AS `handle_status`,`o`.`product_status` AS `product_status`,`o`.`delivery_date` AS `delivery_date`,`o`.`ccid` AS `ccid`,`o`.`pro_id` AS `pro_id`,`oi`.`lacking` AS `lacking`,`oi`.`pid` AS `p_pid`,`oi`.`name` AS `p_name`,`oi`.`quantity` AS `p_quantity`,`oi`.`pid` AS `set_pid`,`oi`.`name` AS `set_name`,`oi`.`quantity` AS `set_quantity`,`oi`.`iid` AS `iid`,`oi`.`oid` AS `oid`,`oi`.`catalog_id` AS `catalog_id`,`oi`.`unit_price` AS `unit_price`,`oi`.`gross_profit` AS `gross_profit`,`oi`.`weight` AS `weight`,`oi`.`unit_name` AS `unit_name`,`oi`.`product_type` AS `product_type` from (`porder` `o` join `porder_item` `oi` on((`o`.`oid` = `oi`.`oid`)))" 
							+"where ((`oi`.`product_type` = 1) or (`oi`.`product_type` = 2) or (`oi`.`product_type` = 3)) "
							+"and delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59' "//+cidString
							+")"
							+"union all " 
							+"("
							+"select `o`.`post_date` AS `post_date`,`o`.`parent_oid` AS `parent_oid`,`o`.`ps_id` AS `ps_id`,`o`.`print_date` AS `print_date`,`o`.`handle` AS `handle`,`o`.`handle_status` AS `handle_status`,`o`.`product_status` AS `product_status`,`o`.`delivery_date` AS `delivery_date`,`o`.`ccid` AS `ccid`,`o`.`pro_id` AS `pro_id`,`oi`.`lacking` AS `lacking`,`p`.`pc_id` AS `p_pid`,`p`.`p_name` AS `p_name`,(`pu`.`quantity` * `oi`.`quantity`) AS `p_quantity`,`oi`.`pid` AS `set_pid`,`oi`.`name` AS `set_name`,`oi`.`quantity` AS `set_quantity`,`oi`.`iid` AS `iid`,`oi`.`oid` AS `oid`,`p`.`catalog_id` AS `catalog_id`,`p`.`unit_price` AS `unit_price`,`p`.`gross_profit` AS `gross_profit`,`p`.`weight` AS `weight`,`p`.`unit_name` AS `unit_name`,`oi`.`product_type` AS `product_type` from (((`porder` `o` join `porder_item` `oi` on((`o`.`oid` = `oi`.`oid`))) join `product_union` `pu` on((`oi`.`pid` = `pu`.`set_pid`))) join `product` `p` on((`pu`.`pid` = `p`.`pc_id`)))"
							+"where (`oi`.`product_type` = 4) "
							+"and delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59' "//+cidString
							+")"
							+") as 4sel group by left(delivery_date,10),p_pid,ccid,pro_id,ps_id";
//				querySql = new StringBuffer("delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59' ");
//				
//				querySql.append(" group by p.delivery_date,ov.p_pid,p.ccid,p.pro_id,ov.ps_id");
				rs = psQuery.executeQuery(sql);
				
				while(rs!=null&&rs.next())
				{
					allCount++;
					long op_id = getSequance("actual_storage_sales");

					long prior_storage_id = productMgr.getPriorDeliveryWarehouse(rs.getLong("ccid"),rs.getLong("pro_id"));  //根据国家和地区获取优先发货仓库
					
					long nation_id = rs.getLong("ccid");
					long province_id = rs.getLong("pro_id");
					long product_id = rs.getLong("p_pid");
					double unit_price = rs.getDouble("unit_price");
					long catalog_id = rs.getLong("catalog_id");
					double total_quantity = rs.getDouble("p_quantity");
					
					String stats_date = DateUtil.DatetimetoStr(new Date());
					long storage_id = rs.getLong("ps_id");
					String delivery_date = rs.getString("delivery_date");
					
					String insertsql = "insert into actual_storage_sales (op_id,nation_id,province_id,product_id,unit_price,catalog_id,total_quantity,stats_date,storage_id,prior_storage_id,delivery_date)"
									  +" values("+op_id+","+nation_id+","+province_id+","+product_id+","+unit_price+","+catalog_id+","+total_quantity+",'"+stats_date+"',"+storage_id+","+prior_storage_id+",'"+delivery_date+"')";
					psInsert = conn.createStatement();
					psInsert.executeUpdate(insertsql);
					closeConn(null,psInsert,null);
				}
				
				
			}
			catch (SQLException e) 
			{
				throw new SQLException(e.getMessage());
			}
			finally
			{
				closeConn(rs,psQuery,conn);
				closeConn(null,psDel,null);
				closeConn(null,psInsert,null);
			}
			
			return (allCount);
		} 
		catch (Exception e) 
		{
			log.error("error:" + e);
	        throw new SQLException("error:" + e);
		}
	}
	
	
	/**
	 * 根据国家和省份/州查询理论仓库后统计理论仓库需要的货物需求量并保存数据(第二张表)
	 * @param end_date 
	 * @param pc
	 * @throws Exception
	 */
	public long statsTheoryStorageForProduct(String start_date, String end_date) 
		throws Exception
	{
		try 
		{
			Connection conn = DataSourceUtils.getConnection(dataSource);
			String querySql = null;
			
			Statement psQuery = null;
			Statement psInsert = null;
			Statement psDel = null;
			
			ResultSet rs = null;
			long count = 0;
			try
			{
				psDel = conn.createStatement();
				psDel.executeUpdate("delete from theory_storage_sales where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'");
				closeConn(null,psDel,null);
				
				psQuery = conn.createStatement();
				querySql = "select sum(total_quantity) as quantity,product_id,prior_storage_id,unit_price,catalog_id,delivery_date from actual_storage_sales"
						  +" where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by product_id,prior_storage_id,left(delivery_date,10)";
				rs = psQuery.executeQuery(querySql);
				
				
				while(rs!=null&&rs.next())
				{
					count++;
					long theory_id = getSequance("theory_storage_sales"); 
					long storage_id = rs.getLong("prior_storage_id");
					long product_id = rs.getLong("product_id");
					long catalog_id = rs.getLong("catalog_id");
					double quantity = rs.getDouble("quantity");
					String deal_date = DateUtil.NowStr();
					double unit_price = rs.getDouble("unit_price");
					String delivery_date = rs.getString("delivery_date");
					
					String sql = "insert into theory_storage_sales (theory_id,storage_id,product_id,catalog_id,quantity,deal_date,unit_price,delivery_date)"
								+" value ("+theory_id+","+storage_id+","+product_id+","+catalog_id+","+quantity+",'"+deal_date+"',"+unit_price+",'"+delivery_date+"')";
					
					psInsert = conn.createStatement();
					psInsert.executeUpdate(sql);
					closeConn(null,psInsert,null);
				}
				
				return (count);
			} 
			catch (SQLException e) 
			{
				throw new SQLException(e.getMessage());
			}
			finally
			{
				closeConn(rs,psQuery, conn);
				closeConn(null,psDel,null);
				closeConn(null,psInsert,null);
			}
		} 
		catch (Exception e) 
		{
			log.error("error:" + e);
	        throw new SQLException("error:" + e);
		}
	}
	
	/**
	 * 统计代发后本来仓库的货物需求并保存数据（第三张表）
	 * @param start_date
	 * @param end_date
	 * @throws Exception
	 */
	public long statsOriginalStoragesProduct(String start_date, String end_date) 
		throws Exception 
	{
		
		try 
		{
			String errorsql="";
			
			Connection conn = DataSourceUtils.getConnection(dataSource);
			String querySql = null;
			
			Statement psQuery = null;
			Statement psInsert = null;
			Statement psDel = null;
			Statement psQueryTransd = null;
			
			ResultSet rs = null;
			ResultSet rs2 = null;
			
			try 
			{
				psDel = conn.createStatement();
				psDel.executeUpdate("delete from original_storage_sales where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'");
				closeConn(null,psDel,null);
				
				psQuery = conn.createStatement();
				querySql = "select * from theory_storage_sales where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59'";
				rs = psQuery.executeQuery(querySql);
				
				com.cwc.app.api.ProductMgr productMgr = (com.cwc.app.api.ProductMgr)MvcUtil.getBeanFromContainer("productMgr");
				
				long count = 0;
				
				while(rs!=null&&rs.next())
				{
					count++;
					long original_id = getSequance("original_storage_sales");
					
					long product_id = rs.getLong("product_id");
					long d_storage_id = rs.getLong("storage_id");//代发仓库ID
					long prior_storage_id = rs.getLong("storage_id");//优先发货仓库ID
					long catalog_id = rs.getLong("catalog_id");
					double quantity = rs.getDouble("quantity");
					double unit_price = rs.getDouble("unit_price");
					String deal_date = DateUtil.NowStr();
					String delivery_date = rs.getString("delivery_date");
					
					boolean ifinsert = true;
					String sql="";//插入执行语句
					
					long start_trand = System.currentTimeMillis();
					psQueryTransd = conn.createStatement();
					String sqlTransd = "select * from storage_transpond as st "
									  +"join pc_child_list as pcl on st.catalog_id = pcl.search_rootid and pcl.pc_id="+catalog_id+" and y_sid="+prior_storage_id;
						//"select * from storage_transpond where catalog_id in ("+catalog_id+") and y_sid="+prior_storage_id;
					rs2 = psQueryTransd.executeQuery(sqlTransd);
					//DBRow trandsponds = floorOrderProcessMgrTJH.statsOriginalStorageSales(d_storage_id,product_id,delivery_date);  //判断仓库间的产品是否需要代发
					long end_trand = System.currentTimeMillis();
					
					if(rs2!= null&rs2.next())
					{
						//有代发关系，代发仓库，优先发货仓库随代发关系
						d_storage_id = rs2.getLong("d_sid");
						prior_storage_id = rs2.getLong("y_sid");
						
						int type_id = rs2.getInt("modality_id");
						DBRow [] union_product_rows = productMgr.getProductsInSetBySetPid(product_id);  //判断商品是否是套装
						if(type_id == 2&&union_product_rows.length != 0)  //2：套装代发形式为散件的，打散代发
						{
							for (int j = 0; j < union_product_rows.length; j++)  //获取套装下的各商品
							{
								long pid = union_product_rows[j].get("pid", 0l);
								double union_quantity = union_product_rows[j].get("quantity", 0d);
								
								quantity = quantity*union_quantity;//散件代发后，需要数量X组合所需数量
								
								DBRow product = productMgr.getDetailProductByPcid(pid);  //根据商品id获取套装代发后的各商品的类别
								if(product != null)
								{
									catalog_id = product.get("catalog_id", 0l);
									unit_price = product.get("unit_price",0d);
								}
									
								product_id = pid;//代发散件后的散件商品ID
								
								if(j!=0)
								{
									original_id = getSequance("original_storage_sales");
									count++;
								}
								
								//套装代发为散件后，按散件插入，数据为批量数据
								sql = "insert into original_storage_sales (original_id,product_id,d_storage_id,prior_storage_id,catalog_id,quantity,unit_price,deal_date,delivery_date)"
									+" values ("+original_id+","+product_id+","+d_storage_id+","+prior_storage_id+","+catalog_id+","+quantity+","+unit_price+",'"+deal_date+"','"+delivery_date+"')";
								
								//打散后散件数据插入，无法批量，只可一个个插
								psInsert = conn.createStatement();
								psInsert.executeUpdate(sql);
								closeConn(null,psInsert,null);
								
								ifinsert = false;//套装已散件代发循环散件插入过，不许再插入
							}
						}
						else//代发形式为套装或者代发商品本身是散件的
						{
							sql = "insert into original_storage_sales (original_id,product_id,d_storage_id,prior_storage_id,catalog_id,quantity,unit_price,deal_date,delivery_date)"
								+" values ("+original_id+","+product_id+","+d_storage_id+","+prior_storage_id+","+catalog_id+","+quantity+","+unit_price+",'"+deal_date+"','"+delivery_date+"');";
						}
					}
					else//无代发关系
					{
						sql = "insert into original_storage_sales (original_id,product_id,d_storage_id,prior_storage_id,catalog_id,quantity,unit_price,deal_date,delivery_date)"
							+" values ("+original_id+","+product_id+","+d_storage_id+","+prior_storage_id+","+catalog_id+","+quantity+","+unit_price+",'"+deal_date+"','"+delivery_date+"')";
					}
					
					closeConn(rs2,psQueryTransd, conn);
					
					//散件代发已经插入过，不用再插入，不需最外层循环插入
					if (ifinsert) 
					{
						errorsql = sql;
						psInsert = conn.createStatement();
						psInsert.executeUpdate(sql);
						closeConn(null, psInsert, null);
					}
				}
				
				return (count);
			} 
			catch (SQLException e) 
			{
				throw new SQLException(e.getMessage()+"-----errorSQL:"+errorsql);
			}
			finally
			{
				closeConn(rs,psQuery,conn);
				closeConn(rs2,psQueryTransd,null);
				closeConn(null,psDel,null);
				closeConn(null,psInsert,null);
				
			}
		}
		catch (Exception e) 
		{
			log.error("error:" + e);
	        throw new SQLException("error:" + e);
		}
	}
	
	/**
	 * 根据仓库代发后的本来仓库所需的产品量统计计划仓库的产品需求并保持到数据库（第四张）
	 * @param start_date
	 * @param end_date
	 * @throws Exception
	 */
	public long statsPlanStorageProductSales(String start_date, String end_date) 
		throws Exception
	{
		try 
		{
			Connection conn = DataSourceUtils.getConnection(dataSource);
			String querySql = null;
			
			Statement psQuery = null;
			Statement psInsert = null;
			Statement psDel = null;
			
			ResultSet rs = null;
			
			long count = 0;
			try 
			{
				psDel = conn.createStatement();
				psDel.executeUpdate("delete from plan_storage_sales where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'");
				closeConn(null,psDel,null);
				
				psQuery = conn.createStatement();
				querySql = "select sum(quantity) as quantity,catalog_id,product_id,d_storage_id,unit_price,left(delivery_date,10) as delivery_date from original_storage_sales where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by product_id,d_storage_id,left(delivery_date,10)";
				rs = psQuery.executeQuery(querySql);

				while(rs!=null&&rs.next())
				{
					count++;
					long plan_id = getSequance("plan_storage_sales");
					long plan_storage_id = rs.getLong("d_storage_id");
					long product_id = rs.getLong("product_id");
					long catalog_id = rs.getLong("catalog_id");
					double quantity = rs.getDouble("quantity");
					double unit_price = rs.getDouble("unit_price");
					String deal_date = DateUtil.NowStr();
					String delivery_date = rs.getString("delivery_date");
					
					String sql = "insert into plan_storage_sales (plan_id,plan_storage_id,product_id,catalog_id,quantity,unit_price,deal_date,delivery_date)"
								+" values("+plan_id+","+plan_storage_id+","+product_id+","+catalog_id+","+quantity+","+unit_price+",'"+deal_date+"','"+delivery_date+"')";
					
					psInsert = conn.createStatement();
					psInsert.executeUpdate(sql);
					closeConn(null,psInsert,null);
				}
				
				return (count);
			} 
			catch (SQLException e) 
			{
				throw new SQLException(e.getMessage());
			}
			finally
			{
				closeConn(rs,psQuery, conn);
				closeConn(null, psDel, null);
				closeConn(null, psInsert, null);
			}
		} 
		catch (Exception e) 
		{
			log.error("error:" + e);
	        throw new SQLException("error:" + e);
		}
	}
	
	private OrderCostBean getOrderCostBean(long oid,long sc_id,long ccid)
		throws Exception
	{
		   //记录订单商品成本、运费成本和运输公司
		   //成本为货币转换后的金额
		   //把错误屏蔽一下
		   try
		   {
			   com.cwc.app.api.OrderMgr orderMgr = (com.cwc.app.api.OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
			   OrderCostBean orderCostBean = orderMgr.getNowOriginalOrderCost(oid,sc_id,ccid);
			   return orderCostBean;
		   }
		   catch (WeightOutSizeException e) 
		   {
			   throw e;
		   }
		   catch (WeightCrossException e) 
		   {
			   throw e;
		   }
		   catch (CountryOutSizeException e) 
		   {
			   throw e;
		   }
	}
	
//	/**
//	 * 算出订单总重量(百分比计算相对精确)
//	 * @param oid
//	 * @return
//	 * @throws Exception
//	 */
//	private float nowTotalWeight(long oid)
//		throws Exception
//	{
//		try
//		   {
//			   com.cwc.app.api.OrderMgr orderMgr = (com.cwc.app.api.OrderMgr)MvcUtil.getBeanFromContainer("orderMgr");
//			   OrderCostBean orderCostBean = orderMgr.getOrderCost(oid,0,0);
//			   return (orderCostBean.getWeight());
//		   }
//		   catch (WeightOutSizeException e) 
//		   {
//			   throw e;
//		   }
//		   catch (WeightCrossException e) 
//		   {
//			   throw e;
//		   }
//		   catch (CountryOutSizeException e) 
//		   {
//			   throw e;
//		   }
//	}

	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}
}
