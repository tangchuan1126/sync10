package com.cwc.initconf;

import java.util.ArrayList;









import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.dom4j.Document;
import org.dom4j.Element;

import com.cwc.util.Dom4j;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.Environment;
import com.cwc.authentication.AuthActionList;
import com.cwc.authentication.AuthPageList;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.init.StorageJetLagInit;

/**
 * 封装系统配置到内存中
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class WrapConfigValue
{
	static Logger log = Logger.getLogger("PLATFORM");
	public WrapConfigValue(DBUtilAutoTran dBUtilAutoTran,SystemConfig systemConfig,AuthActionList authActionList,AuthPageList authPageList,String configFile,StorageJetLagInit storageJetLagInit)
		throws Exception
	{
		//作为turboshop第一个启动的BEAN，先初始化下系统安装路径
    	//系统安装绝对路径配置文件
		log.info("Start Loading Visionari Syncing config.xml ....");

        String appRealPath = System.getProperty("webapp.root").replace('\\','/');
        Environment.setHome(appRealPath);
		
		String tagName = null;
		String tagValue = null;
		
		if ( appRealPath==null||appRealPath.equals("") )
		{
			log.error("appRealPath is null or blank:" + appRealPath);
			throw new Exception("appRealPath is null or blank:" + appRealPath);
		}

		try 
		{
			//解析XML
			String urlMappingPath = appRealPath+configFile;

			Dom4j dom4j = new Dom4j();
			Document doc = dom4j.getDocument(urlMappingPath);
			
			Element root = dom4j.getRootElement(doc);
			ArrayList applicationElementAl = dom4j.getElement(root);
			for (int i=0; i<applicationElementAl.size(); i++)
			{
				ArrayList al = dom4j.getElement( (Element)applicationElementAl.get(i) );
				for (int j=0; j<al.size(); j++)
				{
					Element ne = (Element)al.get(j);
					tagName = ne.attributeValue("name");
					tagValue = ne.attributeValue("value");
					
					if (ConfigBean.getStringValue(tagName)==null)
					{
						ConfigBean.putBeans(tagName,tagValue);						
					}
				}
			}

			log.info("Visionari Syncing config.xml loaded successful!");
			
		}
		catch (Exception e)
		{
			log.error("WrapConfigValue() error:" + e);
			throw new Exception("WrapConfigValue() error:" + e);
		}

		//设置下当前使用的数据库类型
		String dbType = dBUtilAutoTran.getDatabaseType().toLowerCase();
		if (dbType.indexOf("mysql")>=0)
		{
			Environment.setDatabaseType(Environment.DATABASE_MYSQL);
		}
		else if (dbType.indexOf("sql server")>=0)
		{
			Environment.setDatabaseType(Environment.DATABASE_SQLSERVER);
		}
		
		
		
		log.info("Start Loading Visionari Syncing Language Resource File");
		initResource();																			//初始化语言资源文件
		log.info("Start Loading Visionari Syncing Configration Parameters from Database");
		systemConfig.loadAppSystemConfig();														//加载数据库系统配置
		log.info("Start Loading Visionari Syncing ActionList Authroization info from Database");
		authActionList.init();																	//加载所有需要鉴权事件到内存
		log.info("Start Loading Visionari Syncing PageList Authroization info from Database");
		authPageList.init();																	//加载所有需要鉴权页面资源到内存
		log.info("Finished WrapConfigValue.");
		storageJetLagInit.init(dBUtilAutoTran);
		log.info("Finished JetLag.");
	}
	
	/**
	 * 初始化语言资源文件
	 * @throws Exception
	 */
	public void initResource()
		throws Exception
	{
		new Resource();
	}
	
	public static void desctroy()
	{
		ConfigBean.desctroy();
		log.info("Desctroy config.xml");
	}
}
