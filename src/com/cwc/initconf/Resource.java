package com.cwc.initconf;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.NoSuchMessageException;

import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.FileUtil;
import com.cwc.util.StringUtil;

/**
 * 语言资源文件
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Resource
{
	static Logger log = Logger.getLogger("PLATFORM");
	private static HashMap resourceMap = new HashMap();
	
	/**
	 * 加载所有资源文件，扩展名为.pro
	 * @throws Exception
	 */
	public Resource()
	{
//		//String path = "D:\\Tomcat 5.0\\webapps\\turbo_cwcshop\\conf";
//		String path =Environment.getHome()+Config.confFileFolder+"/";
//		String files[] = FileUtil.getFileList(path);
//		String resourceFile;
//		int pos;
//		ResourceBundle rb;
//		
//		for (int i=0; i<files.length; i++)
//		{
//			resourceFile = files[i];
//			pos = resourceFile.lastIndexOf(".");
//			
//			if ( pos>=0&&resourceFile.substring(pos+1).equals("pro") )
//			{
//				DBRow data = getResource(path+resourceFile);
//				resourceMap.put(resourceFile,data);
//				log.info("Load resource:"+resourceFile);
//			}
//		}
	}

	/**
	 * 获得资源文件配置，转换为dbrow
	 * @param file
	 * @return
	 * @throws Exception
	 */
	private DBRow getResource(String file)
		throws Exception
	{
		try 
		{
			String c[] = FileUtil.readFile2Array(file,"UTF-8");
			DBRow data = new DBRow();
			String tmp[];
			
			for (int i=0; i<c.length; i++)
			{
				tmp = c[i].split("=");
				data.add(tmp[0],tmp[1]);
				////system.out.println(c[i]);
			}
			
			return(data);
		} 
		catch (Exception e) 
		{
			log.error("Resource getResource error:"+e);
			throw new Exception("Resource getResource error:"+e);
		}
	}
	
	/**
	 * 采用spring国际化，废弃了原来的模式 2009-01-28
	 * 获得字符串类型值
	 * @param file			资源文件名
	 * @param key
	 * @param def
	 * @return
	 */
	public static String getStringValue(String file,String key,String def)
	{
		ApplicationContext ctx = MvcUtil.getApplicationContext();
		
		String val;
		
		try
		{
			val = ctx.getMessage(key,null, Environment.getLocale());
		}
		catch (NoSuchMessageException e)
		{
			return(def);
		}
		
		return(val);
	}

	/**
	 * 把中文转换为unicode
	 * @param s
	 * @return
	 */
	public static String ChineseToUnicode(String s)
	{
		String result="";
		for(int i=0;i<s.length();i++)
		{
			result=result+"\\u"+Integer.toHexString((int)s.charAt(i) & 0xffff);
		}
		return result;
	}
	
	/**
	 * 获得数值型值
	 * @param file			资源文件名
	 * @param key
	 * @param def
	 * @return
	 */
	public static int getIntValue(String file,String key,String def)
	{
		return(StringUtil.getInt(getStringValue(file,key,def)));
	}
	
	/**
	 * 释放所有资源（系统关闭时被调用）
	 *
	 */
	public static void destroy()
	{
		resourceMap.clear();
		resourceMap = null;
		log.info("Destroy resource");
	}
	
	public static String getStringValue(String key)
	{
		ApplicationContext ctx = MvcUtil.getApplicationContext();
		String val = ctx.getMessage(key,null, Environment.getLocale());
		return(val);
	}
	
	public static void main(String args[])
		throws Exception
	{
		new Resource();
		//system.out.println( Resource.getStringValue("alt_msg","markAlive","") );
	}
}
