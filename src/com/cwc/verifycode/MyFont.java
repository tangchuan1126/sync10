package com.cwc.verifycode;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

/**
 * 验证码字体
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class MyFont 
{
	private static Font onePoint=null;
	
	public static Font getFont()
	{ 
		InputStream fontStream = null;
		
		try 
		{
			if ( onePoint==null )
			{
				fontStream = LoginLicence.class.getResourceAsStream("32pages.ttf");
				onePoint = Font.createFont(Font.TRUETYPE_FONT,fontStream);
			}
		} 
		catch (FontFormatException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if ( fontStream!=null )
				{
					fontStream.close();	
				}
			}
			catch (IOException e1)
			{
			}
		}
		////system.out.println(onePoint);
		return(onePoint);
	}
}
