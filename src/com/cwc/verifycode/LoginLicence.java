package com.cwc.verifycode;

import javax.servlet.*;
import javax.servlet.http.*;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import java.io.*;
import java.util.Random;
import java.awt.*;
import java.awt.image.*;

import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.util.StringUtil;

/**
 * 验证码
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class LoginLicence extends HttpServlet
{

	/**
	 * 输出验证码
	 */
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
		throws ServletException, IOException
	{
		//System.setProperty("java.awt.headless", "true");
		//或者在catalina.sh中增加CATALINA_OPTS="$CATALINA_OPTS -Djava.awt.headless=true"

		HttpSession ses = request.getSession(true);
		String sesID = ses.getId();
		
		synchronized(sesID)
		{
			response.setHeader("Pragma","No-cache");
			response.setHeader("Cache-Control","no-cache");
			response.setDateHeader("Expires", 0);
			response.setHeader("JSESSIONID", sesID);
			
			//从配置文件获取验证码配置信息
			String CONTENT_TYPE = ConfigBean.getStringValue("licence_content_type_type");
			int x = ConfigBean.getIntValue("licence_x");
			int y = ConfigBean.getIntValue("licence_y");
			String fontColor = ConfigBean.getStringValue("licence_fontColor");
			int fontSize = ConfigBean.getIntValue("licence_fontSize");

			//计算验证码数字显示的坐标
			int imgWidth = ConfigBean.getIntValue("licence_width");
			int imgHeight = ConfigBean.getIntValue("licence_height");
			
			String licence = getLicence();

			ses.setAttribute(Config.loginlicenceSesion,licence);
			String text = licence;
			
			response.setContentType(CONTENT_TYPE);

			BufferedImage image = new BufferedImage(imgWidth,imgHeight,BufferedImage.TYPE_INT_RGB);
			Graphics g = image.getGraphics();

			//背景色
			//g.setColor(new Color(221,221,0));
			g.setColor(new Color(255,255,255));
			g.fillRect(0,0,imgWidth, imgHeight);
			
			//噪点
//			Random random = new Random();
//			for (int i=0;i<50;i++)
//			{
//				g.setColor(getRandColor(100,255));
//				
//				int x1 = random.nextInt(imgWidth);
//				int y1 = random.nextInt(imgHeight);
//				int xl = 3;
//				int yl = 0;
//				g.drawLine(x1,y1,x1+xl,y1+yl);
//			}
			  int arraySize = ((int)(Math.random()*200));
	          int[] xPoints = new int[arraySize];
	          int[] yPoints = new int[arraySize];
	        
	          for(int i=0; i < xPoints.length; ++i) {
	            xPoints[i] = ((int)(Math.random()*200)); 
	            yPoints[i] = ((int)(Math.random()*200)); 
	          }
	          g.setColor(getRandColor());
	          g.drawPolyline(xPoints, yPoints, xPoints.length);

			
			//画边框
			g.setColor(new Color(102,102,102));
			g.drawRect(0,0,imgWidth-1,imgHeight-1);
			
			g.setColor(new Color(Integer.parseInt(fontColor,16)));
			Font myfont = MyFont.getFont();
            Font mFont = myfont.deriveFont(Font.PLAIN,fontSize);
			//Font mFont = new Font(fontName,Font.BOLD,fontSize);	
			g.setFont(mFont);
			g.drawString(text,x,y);

			ServletOutputStream output = response.getOutputStream();
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(output);
			encoder.encode(image);
			output.close();
		}
	}
	
	/**
	 * 绘制线条
	 * @param fc
	 * @param bc
	 * @return
	 */
	Color getRandColor(int fc,int bc)
	{
		Random random = new Random();
		if(fc>255) fc=255;
		if(bc>255) bc=255;
		int r=fc+random.nextInt(bc-fc);
		int g=fc+random.nextInt(bc-fc);
		int b=fc+random.nextInt(bc-fc);
		return new Color(r,g,b);
	}
	
	/**
	 * 获得随机颜色
	 * @return
	 */
	Color getRandColor()
	{
		Random random = new Random();
		int r = random.nextInt(3);
		r = random.nextInt(3);

		int colorP[][]={{0,204,255},{153,255,0},{255,102,255},{255,51,51},{255,255,0}};

		return new Color(colorP[r][0],colorP[r][1],colorP[r][2]);
	}

	/**
	 * 获得验证码文本
	 * @return
	 */
	String getLicence()
	{
		Random random = new Random();
		String num = "";
		for (int i=0; i<4; i++)
		{
			num += random.nextInt(10);
		}
		
		return(num);
	}
	
	/**
	 * 从session获取验证码
	 * @param request
	 * @return
	 */
	public static String getTheLicence(HttpServletRequest request)
	{
		//在页面中被调用后马上从session清除，安全
		String licence = (String)StringUtil.getSession(request).getAttribute(Config.loginlicenceSesion); 
		StringUtil.getSession(request).removeAttribute(Config.loginlicenceSesion);

		////system.out.println("--=11"+licence);
		if ( licence==null )
		{
			return("");
		}
		else
		{
			return(licence);
		}
	}
	
	public static void main(String args[])
	{
		Random random = new Random();
		int r = random.nextInt(3);
		//system.out.println(r);
	}
}
