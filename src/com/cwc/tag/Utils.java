package com.cwc.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.http.*;

/**
 * 标签工具包
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Utils {
    public static String filter(String value)
    {

        if ( value == null )
            return (null);

        char content[] = new char[value.length()];
        value.getChars(0, value.length(), content, 0);
        StringBuffer result = new StringBuffer(content.length + 50);
        for (int i = 0; i < content.length; i++)
        {
            switch (content[i])
            {
	            case '<':
	                result.append("&lt;");
	                break;
	            case '>':
	                result.append("&gt;");
	                break;
	            case '&':
	                result.append("&amp;");
	                break;
	            case '"':
	                result.append("&quot;");
	                break;
	            case '\'':
	                result.append("&#39;");
	                break;
	            default:
	                result.append(content[i]);
            }
        }
        
        return (result.toString());

    }


    /**
     * Write the specified text as the response to the writer associated with
     * this page.  <strong>WARNING</strong> - If you are writing body content
     * from the <code>doAfterBody()</code> method of a custom tag class that
     * implements <code>BodyTag</code>, you should be calling
     * <code>writePrevious()</code> instead.
     *
     * @param pageContext The PageContext object for this page
     * @param text The text to be written
     *
     * @exception JspException if an input/output error occurs (already saved)
     */
    public static void write(PageContext pageContext, String text)
        throws JspException
	{

        JspWriter writer = pageContext.getOut();
        try
        {
            writer.print(text);
        }
        catch (IOException e)
        {
	        throw new JspException("write(" + pageContext + "," + text + ")" + e.getMessage());
        }

    }
    
	/**
	 * Write the specified text as the response to the writer associated with
	 * the body content for the tag within which we are currently nested.
	 *
	 * @param pageContext The PageContext object for this page
	 * @param text The text to be written
	 *
	 * @exception JspException if an input/output error occurs (already saved)
	 */
	public static void writePrevious(PageContext pageContext, String text)
		throws JspException {

		JspWriter writer = pageContext.getOut();
		if ( writer instanceof BodyContent ) writer = ((BodyContent) writer).getEnclosingWriter();
		try
		{
			writer.print(text);
		}
		catch (IOException e)
		{
			throw new JspException ("writePrevious(" + pageContext + "," + text + ")" + e.getMessage());
		}

	}
	
	public static HttpServletRequest getRequest(PageContext pageContext)
	{
		return((HttpServletRequest)pageContext.getRequest());
	}
	
	public static HttpServletResponse getResponse(PageContext pageContext)
	{
		return((HttpServletResponse)pageContext.getResponse());
	}

	public static HttpSession getSession(PageContext pageContext)
	{
		return((HttpSession)pageContext.getSession());
	}
}






