package com.cwc.tag;

import java.util.HashMap;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import javax.servlet.http.*;
import com.cwc.util.StringUtil;

/**
 * 表单标签
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class FormTag extends TagSupport
{
	String name = null;
	String value = "";
	String type = null;
	String size = null;
	String maxlength = null;
	String cols = null;
	String rows = null;
		
	String condition = null;
	String alt = null;
	String tableTextColor = "#000000";
	String tableBorderColor = "#FF0000";
	int tableTextSize = 2;
	int tableBorderSize = 1;
	String id = null;
	String tabindex = null;
	String oclass = null;
	String style = null;
	
	HttpServletRequest request = null;
	String sesName = "errorField";
	String sesName2 = "hashForm";

	public String getOclass()
	{
		return oclass;
	}

	public String getStyle()
	{
		return style;
	}
	
	public String getTabindex()
	{
		return tabindex;
	}
	
	public String getAlt()
	{
		return alt;
	}

	public String getId()
	{
		return id;
	}
	
	public String getCondition()
	{
		return condition;
	}

	public String getName()
	{
		return name;
	}

	public String getValue()
	{
		return(value);
	}

	public void setAlt(String string)
	{
		alt = string;
	}
	
	public void setId(String string)
	{
		id = string;
	}

	public void setOclass(String string)
	{
		oclass = string;
	}
	
	public void setStyle(String string)
	{
		style = string;
	}
	
	public void setTabindex(String string)
	{
		tabindex = string;
	}
	
	public void setCondition(String string)
	{
		condition = string;
	}

	public void setName(String string)
	{
		name = string;
	}

	public void setValue(String string)
	{
		value = string;
	}
	
	public String getTableTextColor()
	{
		return tableTextColor;
	}

	public int getTableTextSize()
	{
		return tableTextSize;
	}

	public void setTableTextColor(String string)
	{
		tableTextColor = string;
	}

	public void setTableTextSize(int i)
	{
		tableTextSize = i;
	}
	
	public String getTableBorderColor()
	{
		return tableBorderColor;
	}
	
	public int getTableBorderSize()
	{
		return tableBorderSize;
	}

	public void setTableBorderColor(String string)
	{
		tableBorderColor = string;
	}
	
	public void setTableBorderSize(int string)
	{
		tableBorderSize = string;
	}
	
	public String getType()
	{
		return type;
	}
	public void setType(String string)
	{
		type = string;
	}
	
	public String getMaxlength()
	{
		return maxlength;
	}
	public String getSize()
	{
		return size;
	}
	
	public void setMaxlength(String string)
	{
		maxlength = string;
	}
	public void setSize(String string)
	{
		size = string;
	}
	
	public String getCols()
	{
		return cols;
	}
	
	public String getRows()
	{
		return rows;
	}
	
	public void setCols(String string)
	{
		cols = string;
	}

	public void setRows(String string)
	{
		rows = string;
	}
	
	public void release()
	{
		super.release();
		
		name = "";
		value = "";
		condition = "";
		alt = "";
		tableTextColor = "#000000";
		tableBorderColor = "#FF0000";
		tableTextSize = 2;
		id = "";
		tabindex = "";
		oclass = "";
		style = "";
	}

	public int doStartTag()
		throws JspException
	{
		request = Utils.getRequest(pageContext);
		return (EVAL_BODY_INCLUDE);
	}

	public int doEndTag()
		throws JspException
	{
		try
		{		
			Utils.write(pageContext,getFormElement());
		}
		catch (JspException e)
		{
			throw new JspException("doEndTag() error: " + e.getMessage());
		}
		
		return (EVAL_PAGE);
	}

	/**
	 * 
	 * @return
	 */
	private String getFormElement()
	{
		String formStr = "";
		String needCheck = "<input type='hidden' name='" + getCondition() + "' value='" + getName() + "'>";
		
		String tv = getValue();
		if ( StringUtil.getString(request,"checkForm").equals("false") )
		{
			tv = getValueFromSession(getName());
		}
		
		FormElement fel = new FormElement(this.getType());
		fel.setCols(this.getCols());
		fel.setMaxlength(this.getMaxlength());
		fel.setName(this.getName());
		fel.setRows(this.getRows());
		fel.setSize(this.getSize());
		fel.setValue(tv);
		fel.setOclass(this.getOclass());
		fel.setStyle(this.getStyle());
		fel.setTabindex(this.getTabindex());
		formStr = fel.getFormElement();

		if ( StringUtil.getString(request,"checkForm").equals("false") && fieldIsError(getName()) )
		{
			formStr = addToTable(formStr,getAlt());
			formStr = formStr + needCheck;
		}
		else if ( getCondition() != null )	
		{
			formStr = formStr + needCheck;
		}

		return(formStr);
	}

	private String addToTable(String formElement,String alt)
	{
		StringBuffer tableStr = new StringBuffer();
		
		tableStr.append("<table width='100%' border='0' cellspacing='0' cellpadding='2'>");
		tableStr.append("<tr>");
		tableStr.append("<td style='border: " + getTableBorderSize() + "px solid " + getTableBorderColor() + ";'>");
		tableStr.append("<font color=" + getTableTextColor() + " size=" + getTableTextSize() + ">" + alt + "</font><br>" + formElement);
		tableStr.append("</td>");
		tableStr.append("</tr>");
		tableStr.append("</table>");
		
		return(tableStr.toString());
	}
	
	private String getValueFromSession(String fieldName)
	{
		HttpSession ses = request.getSession(true);
		HashMap hashForm = (HashMap)ses.getAttribute(sesName2);
		if ( hashForm != null )
		{
			return((String)hashForm.get(fieldName));
		}
		else
		{
			return("");
		}
		
	}

	private boolean fieldIsError(String fieldName)
	{
		HttpSession ses = request.getSession(true);
		HashMap errorField = (HashMap)ses.getAttribute(sesName);
		if ( errorField!=null && errorField.get(fieldName) != null )
		{
			return(true);
		}
		else
		{
			return(false);
		}
	}






}



