package com.cwc.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;

import com.cwc.app.api.sbb.AccountMgrSbb;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.Config;
import com.cwc.authentication.AdminAuthenCenter;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;

/**
 * 该标签主要用在后台页面制作，用其包围一些具备权限验证的HTML模块，从而实现不同角色显示不同内容
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
/**
 * 返回值列表
 * EVAL_BODY_INCLUDE：把Body读入存在的输出流中，doStartTag()函数可用 
 * EVAL_PAGE：继续处理页面，doEndTag()函数可用 
 * SKIP_BODY：忽略对Body的处理，doStartTag()和doAfterBody()函数可用 
 * SKIP_PAGE：忽略对余下页面的处理，doEndTag()函数可用 
 * EVAL_BODY_TAG：已经废止，由EVAL_BODY_BUFFERED取代 
 * EVAL_BODY_BUFFERED：申请缓冲区，由setBodyContent()函数得到的BodyContent对象来处理tag的body，如果类实现了BodyTag，那么doStartTag()可用，否则非法 
 * EVAL_BODY_AGAIN：请求继续处理body，返回自doAfterBody()，这个返回值在你制作循环tag的时候是很有用的。
 */
public class AuthenticationTag extends BodyTagSupport{
	
	String bindAction = null;
	HttpServletRequest request = null;

	public void setBindAction(String bindAction){
		this.bindAction = bindAction;
	}
	
	public String getBindAction(){
		return(bindAction);
	}
	
	public void release(){
		super.release();

		bindAction = null;
		request = null;
	}

	public int doStartTag() throws JspException {
		
		request = Utils.getRequest(pageContext);
		
		if ( pageAuthorityValidate() ){
			
			return (EVAL_BODY_BUFFERED);
		} else {
			
			return (SKIP_BODY);			
		}
	}

	public int doAfterBody() throws JspException {
		
		try {		
			BodyContent body = getBodyContent();
			String content = body.getString().trim();
			body.clearBody();

			Utils.writePrevious(pageContext,content);
			
		} catch (Exception e) {
			throw new JspException("doAfterBody() error:" + e.getMessage());
		}
		
		return(SKIP_BODY);
	}
	
	public int doEndTag() throws JspException {
		
		return (EVAL_PAGE);
	}
	
	private boolean pageAuthorityValidate(){
		
		boolean result = false;
		
		AdminLoginBean adminBean = new AccountMgrSbb().getAdminLoginBean(StringUtil.getSession(request).getAttribute(Config.adminSesion));
		
		long adid = adminBean.getAdid();
		
		DBRow[] dept = adminBean.getDepartment();
		
		String bindActions[] = bindAction.split("\\|");
		
		for(int i = 0; i< bindActions.length; i++){
			
			if(AdminAuthenCenter.pageAuthorityValidate(adid,dept,bindActions[i])){
				
				result = true;
			}
		}
		
		return result;
	}
	
	/**
	 * 判断该管理员是否具有对该action的操作权限
	 * @return
	 */
	/*private boolean isPermit()
	{
		HttpSession ses = StringUtil.getSession(request);
		AdminLoginBean adminLoggerBean = new AdminMgr().getAdminLoginBean(ses);
		
		String bindActions[] = bindAction.split("\\|");

		for (int i=0; adminLoggerBean!=null&&i<bindActions.length; i++)
		{
			if ( AdminAuthenCenter.isPermitAction(bindActions[i],adminLoggerBean.getAdgid(),adminLoggerBean.getAdid()) )
			{
				return(true);
			}
		}
		
		
		return(false);
	}*/
}


