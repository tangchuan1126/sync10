package com.cwc.tag;

/**
 * 表单元素标签
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class FormElement
{
	String name = null;
	String value = null;
	String type = null;
	String id = null;
	
	String size = null;
	String maxlength = null;
	
	String cols = null;
	String rows = null;
	
	String tabindex = null;
	String oclass = null;
	String style = null;

	public FormElement(String type)
	{
		this.type = type;
	}

	public String getFormElement()
	{
		String element = "";
		type = type.toLowerCase();
		
		if ( type.equals("text") )
		{
			element = getText();
		}
		else if ( type.equals("textarea") )
		{
			element = getTextArea();
		}
		else if ( type.equals("hidden") )
		{
			element = getHidden();
		}
		else if ( type.equals("password") )
		{
			element = getPassword();
		}
		
		return(element);
	}

	private String getText()
	{
		StringBuffer strBuf = new StringBuffer();
		
		strBuf.append("<input name='");
		strBuf.append(this.name);
		strBuf.append("' value='");
		strBuf.append(this.value);
		strBuf.append("' type='text' ");
		
		if ( this.size != null )
		{
			strBuf.append(" size='");
			strBuf.append(this.size);
			strBuf.append("' ");
		}
		
		if ( this.maxlength != null )
		{
			strBuf.append(" maxlength=' ");
			strBuf.append(this.maxlength);
			strBuf.append(" ' ");
		}
		
		if ( this.id != null )
		{
			strBuf.append(" id=' ");
			strBuf.append(this.id);
			strBuf.append(" ' ");
		}
		
		if ( this.oclass != null )
		{
			strBuf.append(" class=' ");
			strBuf.append(this.oclass);
			strBuf.append(" ' ");
		}
		
		if ( this.style != null )
		{
			strBuf.append(" style=' ");
			strBuf.append(this.style);
			strBuf.append(" ' ");
		}
		
		if ( this.tabindex != null )
		{
			strBuf.append(" tabindex=' ");
			strBuf.append(this.tabindex);
			strBuf.append(" ' ");
		}

		strBuf.append(">");
		
		return(strBuf.toString());
	}

	private String getPassword()
	{
		StringBuffer strBuf = new StringBuffer();
		
		strBuf.append("<input name='");
		strBuf.append(this.name);
		strBuf.append("' value='");
		strBuf.append(this.value);
		strBuf.append("' type='password' ");
		
		if ( this.size != null )
		{
			strBuf.append(" size='");
			strBuf.append(this.size);
			strBuf.append("' ");
		}
		
		if ( this.maxlength != null )
		{
			strBuf.append(" maxlength=' ");
			strBuf.append(this.maxlength);
			strBuf.append(" ' ");
		}
		
		if ( this.oclass != null )
		{
			strBuf.append(" class=' ");
			strBuf.append(this.oclass);
			strBuf.append(" ' ");
		}
		
		if ( this.style != null )
		{
			strBuf.append(" style=' ");
			strBuf.append(this.style);
			strBuf.append(" ' ");
		}

		strBuf.append(">");
		
		return(strBuf.toString());
	}
	
	private String getTextArea()
	{
		String str = "<textarea name='textarea' cols='50' rows='20'>hj</textarea>";
		StringBuffer strBuf = new StringBuffer();
		
		strBuf.append("<textarea name='");
		strBuf.append(this.name);
		strBuf.append("' ");

		if ( this.cols != null )
		{
			strBuf.append(" cols='");
			strBuf.append(this.cols);
			strBuf.append("' ");
		}

		if ( this.rows != null )
		{
			strBuf.append(" rows='");
			strBuf.append(this.rows);
			strBuf.append("' ");
		}
		
		if ( this.oclass != null )
		{
			strBuf.append(" class=' ");
			strBuf.append(this.oclass);
			strBuf.append(" ' ");
		}
		
		if ( this.style != null )
		{
			strBuf.append(" style=' ");
			strBuf.append(this.style);
			strBuf.append(" ' ");
		}
		
		strBuf.append(">");
		strBuf.append(this.value);
		strBuf.append("</textarea>");

		return(strBuf.toString());
	}

	private String getHidden()
	{
		String v = "<input type='hidden' name='hiddenField' value='dd'>";
		StringBuffer strBuf = new StringBuffer();
		
		strBuf.append("<input type='hidden' name='");
		strBuf.append(this.name);
		strBuf.append("' value='");
		strBuf.append(this.value);
		strBuf.append("'>");
		
		return(strBuf.toString());
	}
	
	public void setCols(String string)
	{
		cols = string;
	}

	public void setMaxlength(String string)
	{
		maxlength = string;
	}

	public void setName(String string)
	{
		name = string;
	}

	public void setRows(String string)
	{
		rows = string;
	}

	public void setSize(String string)
	{
		size = string;
	}
	
	public void setValue(String string)
	{
		value = string;
	}
	
	public void setOclass(String string)
	{
		oclass = string;
	}
	
	public void setTabindex(String string)
	{
		tabindex = string;
	}
	
	public void setStyle(String string)
	{
		style = string;
	}

}
