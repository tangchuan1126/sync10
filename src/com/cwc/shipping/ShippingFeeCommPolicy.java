package com.cwc.shipping;

import org.apache.log4j.Logger;

import com.cwc.app.exception.express.CountryOutSizeException;
import com.cwc.app.exception.express.ProvinceOutSizeException;
import com.cwc.app.exception.express.WeightCrossException;
import com.cwc.app.exception.express.WeightOutSizeException;
import com.cwc.app.floor.api.FloorExpressMgr;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;

/**
 * 不要配置成Sigletone!!!
 * 
 * 通用配送计费策略
 * @author 
 *
 * 用费策略接口用于规范、扩展用费计算模式
 */
public class ShippingFeeCommPolicy
{
	static Logger log = Logger.getLogger("ACTION");
	
	private FloorExpressMgr fem = null;
	private float product_total_weight;

	private long companyID;	//快递公司ID
	private long sz_id;		//地区ID
	private long sw_id;		//重量段ID
	
	private float weight_step;
	private float f_weight_fee;
	private float k_weight_fee;
	private float shipping_fee_discount;
	private float oil_addition_fee;
	private String company_name;
	private String zone_name;
	private float handle_fee;	//每笔订单的处理费
	private boolean selectAuto;


	/**
	 * 设置商品总重
	 * @param product_weight
	 * @throws Exception
	 */
	private void setPRO_TW(float product_total_weight) throws Exception
	{
		this.product_total_weight = product_total_weight;
	}

	/**
	 * 获得商品续重
	 * 
	 * 如果运费设置首重>0，续重=总重-步长
	 * 如果运费设置首重=0，续重=总重
	 * 
	 * @return
	 * @throws Exception
	 */
	public double getPRO_KW() throws Exception
	{
		if ( this.getSHIPPING_FW_FEE()==0 )
		{
			return( this.getPRO_TW() );
		}
		else
		{
			return( (this.getPRO_TW()-this.getSHIPPING_WeightStep_FEE())/this.getSHIPPING_WeightStep_FEE() );
		}
	} 

	/**
	 * 该重量已经经过重新计算
	 * @return
	 */
	public float getPRO_TW()
	{
		return(this.product_total_weight);
	}
	
	/**
	 * 重新计算商品总重，并获得其他相关数据
	 * @param sc_id	快递公司ID
	 * @param actualWeight  订单实际重量
	 * @return 多少份所属重量段步长
	 * @throws WeightCrossException	重量段数据交叉设置
	 * @throws WeightOutSizeException	重量没有找到合适重量段数据范围
	 * @throws Exception
	 */
	public float getCalculateWeight(long sc_id,float actualWeight) 
		throws WeightCrossException,WeightOutSizeException,Exception
	{
		
		DBRow targetWeightData = null;

		try
		{
			//根据所属重量段，重新计算商品总重量
			//先查找数据库匹配重量段，如果找到，就按重量段内数据重新计算重量
			targetWeightData = fem.getDetailWeightByScidWeightRange(sc_id,actualWeight);
		} 
		catch (Exception e)
		{
			throw new WeightCrossException();//匹配记录不唯一
		}

		if (targetWeightData == null)//不匹配，则重量+1，重新匹配重量段，然后取该重量段最低值
		{
			actualWeight = actualWeight + 1;
			targetWeightData = fem.getDetailWeightByScidWeightRange(sc_id,actualWeight);
			
			if (targetWeightData == null)
			{
				throw new WeightOutSizeException();//完全没有匹配数据
			}
			
			actualWeight = targetWeightData.get("st_weight", 0f);
			
			this.sw_id = targetWeightData.get("sw_id", 0l);	//设置sw_id
			this.weight_step = targetWeightData.get("weight_step", 0f);
			
			return(actualWeight);
		}
		else
		{
			float weight_step = targetWeightData.get("weight_step", 0f);//该重量段重量增长步长
			
			this.sw_id = targetWeightData.get("sw_id", 0l);	//设置sw_id
			this.weight_step = targetWeightData.get("weight_step", 0f);//设置重量所属重量段步长
			
			float n = actualWeight / weight_step;
			
			//计算结果为：按照重量所属步长，对重量重新计算
			//整数为原值，小数则按照步长进位
			if ( n == (int)n)
			{
				return(n*weight_step);  
			}
			else
			{
				return( ((int)(n+1))*weight_step );
			}
		}
	}

	/**
	 * 获得运费折扣
	 * @return
	 * @throws Exception
	 */
	public double getSHIPPING_FEE_DISCOUNT() throws Exception
	{
		return(this.shipping_fee_discount);
	}

	/**
	 * 获得首重运费
	 * @return
	 * @throws Exception
	 */
	
	public double getSHIPPING_FW_FEE() throws Exception
	{
		return(this.f_weight_fee);
	}

	/**
	 * 获得续重运费
	 * @return
	 * @throws Exception
	 */
	public double getSHIPPING_KW_FEE() throws Exception
	{
		return(this.k_weight_fee);
	}
	
	/**
	 * 获得燃油附加费
	 * @return
	 */
	public float getOil_addition_fee()
	{
		return(this.oil_addition_fee);
	}

	/**
	 * 获得重量段步长
	 * @return
	 * @throws Exception
	 */
	public double getSHIPPING_WeightStep_FEE() throws Exception
	{
		return(this.weight_step);
	}

	/**
	 * 设置快递公司信息
	 * @param companyID		快递公司ID
	 * @param weight		货物重量
	 * @param ccid			递送国家
	 * @param pro_id		地区省份
	 * @throws CountryOutSizeException		不能送达国家（已经不再抛出，因为printcenter已经做了检查）
	 * @throws WeightCrossException			重量段设置交叉
	 * @throws WeightOutSizeException		重量段设置有空白地区
	 * @throws Exception
	 */
	public void initCompanyWeightCountry(long companyID,float weight,long ccid,long pro_id)
		throws ProvinceOutSizeException,CountryOutSizeException,WeightCrossException,WeightOutSizeException,Exception
	{
		this.companyID = companyID;

		DBRow detailCompany = fem.getDetailCompany(companyID);
		this.shipping_fee_discount = detailCompany.get("shipping_fee_discount", 0f);		//折扣
		this.oil_addition_fee = detailCompany.get("oil_addition_fee", 0f);					//燃油附加费
		this.company_name = detailCompany.getString("name");								//快递名称
		this.handle_fee = detailCompany.get("handle_fee", 0f);

		DBRow detailZone;
		if (detailCompany.get("domestic", 0)==1)//区分国内快递还是国际快递
		{
			detailZone = fem.getDetailZoneByProIdScid(companyID, pro_id);
			if (detailZone==null)
			{
				throw new ProvinceOutSizeException();
			}
		}
		else
		{
			detailZone = fem.getDetailZoneByCcidScid(companyID, ccid);
			if (detailZone==null)
			{
				throw new CountryOutSizeException();
			}
		}

		this.sz_id = detailZone.get("sz_id", 0l);
		this.zone_name = detailZone.getString("name");

		//重新计算重量并设置一些相关参数
		this.setPRO_TW(getCalculateWeight(companyID,weight)); 
		//获得具体运费
		//log.info("ShippingFeeCommPolicy:"+this.companyID+" - "+this.sw_id+" - "+this.sz_id);
		DBRow detailFee = fem.getDetailFeeByCompanyWeightZone(this.companyID, this.sw_id, this.sz_id);
		this.f_weight_fee = detailFee.get("f_weight_fee", 0f);
		this.k_weight_fee = detailFee.get("k_weight_fee", 0f);
	}

	/**
	 * 获得最终计算后的运费
	 * @return
	 * @throws Exception
	 */
	public double getShippingFee() throws Exception
	{
		// (首重运费+续重运费)*折扣*(1+燃油附加费) 
		double shipping_fee = ( ( this.getSHIPPING_FW_FEE()+this.getPRO_KW()*this.getSHIPPING_KW_FEE() )*this.getSHIPPING_FEE_DISCOUNT() )*(1+this.getOil_addition_fee()) + this.getHandle_fee() ;
		
		////system.out.println("( ( "+this.getSHIPPING_FW_FEE()+"+"+this.getPRO_KW()+"*"+this.getSHIPPING_KW_FEE()+" )*"+this.getSHIPPING_FEE_DISCOUNT()+" )*(1+"+this.getOil_addition_fee()+")+"+this.getHandle_fee());
		
		shipping_fee = MoneyUtil.round(shipping_fee, 2);
		return(shipping_fee);
	}
	
	
	
	
	public void setFem(FloorExpressMgr fem)
	{
		this.fem = fem;
	}

	public String getCompany_name()
	{
		return company_name;
	}

	public String getZone_name()
	{
		return zone_name;
	}
	
	public float getHandle_fee() 
	{
		return handle_fee;
	}
	
	
	public static void main(String args[])
	{
		float actualWeight = 4.3f;
		float weight_step = 0.23f;
		
		float n = actualWeight / weight_step;
		
		if ( n == (int)n)
		{
			//system.out.println(n*weight_step);  
		}
		else
		{
			//system.out.println( ((int)(n+1))*weight_step );
		}
	}

	
	

}
