package com.cwc.shipping;

public class ShippingInfoBean
{
	private String companyName;
	private String zoneName;
	private float weight;
	private double shippingFee;
	private long sc_id;//快递公司ID
	private String useType;//USPS使用的方式
	private boolean selectAuto;
	
	public String getCompanyName()
	{
		return companyName;
	}
	
	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}
	
	public String getZoneName()
	{
		return zoneName;
	}
	
	public void setZoneName(String zoneName)
	{
		this.zoneName = zoneName;
	}
	
	public float getWeight()
	{
		return weight;
	}
	
	public void setWeight(float weight)
	{
		this.weight = weight;
	}
	
	public double getShippingFee()
	{
		return shippingFee;
	}
	
	public void setShippingFee(double shipping_fee)
	{
		this.shippingFee = shipping_fee;
	}

	public long getSc_id() {
		return sc_id;
	}

	public void setSc_id(long sc_id) {
		this.sc_id = sc_id;
	}

	public String getUseType() {
		return useType;
	}

	public void setUseType(String useType) {
		this.useType = useType;
	}

	public void setSelectAuto(int selectAuto) {
		
		if(selectAuto==0)
		{
			this.selectAuto = true;
		}
		else
		{
			this.selectAuto = false;
		}
		
	}

	public boolean isSelectAuto() {
		return selectAuto;
	}
}
