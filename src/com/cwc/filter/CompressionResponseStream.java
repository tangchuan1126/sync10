package com.cwc.filter;

import java.io.IOException;
import java.util.zip.GZIPOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * GZIP压缩，暂时放弃使用，因为在大并发量时不太稳定
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class CompressionResponseStream extends ServletOutputStream 
{

    public CompressionResponseStream(HttpServletResponse response)
    	throws IOException
    {
        super();
        closed = false;
        this.response = response;
        this.output = response.getOutputStream();
    }


    protected int compressionThreshold = 0;

    protected byte[] buffer = null;

    protected int bufferCount = 0;

    protected GZIPOutputStream gzipstream = null;

    protected boolean closed = false;

    protected int length = -1;

    protected HttpServletResponse response = null;

    protected ServletOutputStream output = null;

    protected void setBuffer(int threshold)
    {
        compressionThreshold = threshold;
        buffer = new byte[compressionThreshold];
    }

    public void close() 
    	throws IOException 
    {
        if (closed) 
        {
            //throw new IOException("This output stream has already been closed");
        	return;
        }

        if (gzipstream != null) 
        {
            flushToGZip();
            gzipstream.close();
            gzipstream = null;
        }
        else
        {
            if (bufferCount > 0) 
            {
                output.write(buffer, 0, bufferCount);
                bufferCount = 0;
            }
        }

        output.close();
        closed = true;
    }


    public void flush()
    	throws IOException 
    {
        if (closed) 
        {
            //system.out.println("Cannot flush a closed output stream");
        }

        if (gzipstream != null) 
        {
            gzipstream.flush();
        }
    }

    public void flushToGZip()
    	throws IOException 
    {
        if (bufferCount > 0) 
        {
            writeToGZip(buffer, 0, bufferCount);
            bufferCount = 0;
        }
    }


    public void write(int b) 
    	throws IOException 
    {
        if (closed)
       	{
        	//throw new IOException("Cannot write to a closed output stream");
        	return;
       	}

        if (bufferCount >= buffer.length) 
        {
            flushToGZip();
        }

        buffer[bufferCount++] = (byte) b;
    }

    public void write(byte b[]) 
    	throws IOException 
    {
        write(b, 0, b.length);
    }

    public void write(byte b[], int off, int len) 
    	throws IOException 
    {
        if (closed)
        {
            //throw new IOException("Cannot write to a closed output stream");
        	return;
        }

        if (len == 0) return;

        if (len <= (buffer.length - bufferCount))  
        {
            System.arraycopy(b, off, buffer, bufferCount, len);
            bufferCount += len;
            return;
        }

        flushToGZip();

        if (len <= (buffer.length - bufferCount)) 
        {
            System.arraycopy(b, off, buffer, bufferCount, len);
            bufferCount += len;
            return;
        }

        writeToGZip(b, off, len);
    }

    public void writeToGZip(byte b[], int off, int len) 
    	throws IOException 
    {
        if (gzipstream == null) 
        {
            response.addHeader("Content-Encoding", "gzip");
            gzipstream = new GZIPOutputStream(output);
        }
        gzipstream.write(b, off, len);
    }
    
    public boolean closed()
    {
        return (this.closed);
    }

}
