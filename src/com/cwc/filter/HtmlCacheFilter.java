package com.cwc.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cwc.app.key.LocalKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.cache._l1l1l1l1l1l1l1l1l;
import com.cwc.util.StringUtil;

/**
 * 生成静态页面过滤器
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class HtmlCacheFilter implements Filter
{
	private FilterConfig config;
	static Logger log = Logger.getLogger("PLATFORM");
	
	public void init(FilterConfig config) throws ServletException
	{
		this.config = config;
	}
	
	public void destroy()
	{
		this.config = null;
	}
	
	public FilterConfig getFilterConfig()
	{ 
		return config; 
	} 

	
	public void doFilter(ServletRequest srequest,ServletResponse sresponse,FilterChain chain)
		throws IOException, ServletException
	{
		  HttpServletRequest req = (HttpServletRequest)srequest;
		  HttpServletResponse resp = (HttpServletResponse)sresponse;		  
		  
		  String targetURL = null;
		  		
  	      String sysFolder = ConfigBean.getStringValue("systenFolder");				//系统安装文件夹
	      String groups = StringUtil.getString(req,"groups");						//当前访问URL绑定表
	      int nocache = StringUtil.getInt(req,"nocache",0);							//是否生成静态缓存
		  String home = Environment.getHome();										//系统安装绝对路径
		  String currentURL = req.getRequestURI();							//当前访问URL
		  String htmlUriKey = LocalKey.getLocalKey(currentURL);				 	//根据当前语言地区追加后序
				      
	      _l1l1l1l1l1l1l1l1l l1l1l1111l1l1l1l111 = null;							//静态缓存策略
	      String l1l1lll111l1l1l1l111 = null;										//从URL分析出来的文件夹
	      String l1l1l11l1l1l1l111 = null;											//从URL分析出来的文件名
	      
	      String htmlFolder;
	      String htmlFile;
	      String htmlUri;

	      try
		  {
	    	  //如果系统配置为不缓存或者当前URL配置为不缓存，则不生成静态文件
	      	  if ( !ConfigBean.getStringValue("htmlcache").equals("true")||nocache==1 )
	      	  {
				  chain.doFilter(srequest,sresponse);
				  return;
	      	  }
	      	  else
	      	  {
		      	  l1l1l1111l1l1l1l111 = new _l1l1l1l1l1l1l1l1l();

		      	  	  //当前访问URL为HTML扩展名并且还没有缓存
	      	  	  	  if ( currentURL!=null&&currentURL.indexOf(".html")>=0&&!l1l1l1111l1l1l1l111.isContainKey(htmlUriKey) )
	      	  	  	  {
	      	  	  		  /**
	      	  	  		   * 先获得写HTML的锁
	      	  	  		   * 解决了第一次生成页面同时遭遇爆炸访问时404访问错误
	      	  	  		   * 
	      	  	  		   * 只有一个线程能去生成静态文件，其他线程直接经过dofilter输出
	      	  	  		   */
	      	  	  		  if (HtmlWriteLock.isNotLock(htmlUriKey))
	      	  	  		  {
								////system.out.println(Thread.currentThread()+" ----------------------------- get!!!");
	      	  	  			  //log.info(Thread.currentThread()+" "+currentURL+" HtmlWriteLock");
								
	      	  	  			  //实现一个自己的response，为了获得输出的页面内容
								HtmlResponseWrapper wrapper = new HtmlResponseWrapper(resp);
								
								//执行一次dofilter把页面输出内容装载到response中
								chain.doFilter(req, wrapper);
								
								byte[] l1l1lll111l1l1l1l1 = wrapper.toByteArray();
								String l1l1l1l1ll11l1l1l1l111 = new String(l1l1lll111l1l1l1l1,"UTF-8");			//获得页面输出内容，编码为UTF8；

								if (!l1l1l1l1ll11l1l1l1l111.equals(""))					//页面输出内容不为空才写HTML文件
								{
									htmlFolder = l1l1l1111l1l1l1l111.l1l1lll11l1l1l1l1(home,currentURL,sysFolder);								//创建需要存放静态文件的文件夹
									htmlFile = l1l1l1111l1l1l1l111.l1l1l1ll1l1l1l1(home,currentURL,sysFolder,l1l1l1l1ll11l1l1l1l111);			//生成静态文件
									
									//拼成uri
							    	if (!htmlFolder.equals(""))
							    	{
							    		htmlFolder += "/";
							    	}

							    	htmlUri = "/"+htmlFolder + htmlFile;
		
							    	//生成文件后，把生成标记记录在内存中，key为URI，并且绑定所有表名
							    	////system.out.println("HtmlCacheFilter:"+htmlUriKey+" - "+htmlUri);
									l1l1l1111l1l1l1l111.putObject(groups.split("\\|"),htmlUriKey,htmlUri); //只有真正生成了文件，才标记
									HtmlWriteLock.releaseLock(htmlUriKey);	//释放写HTML锁
																		  
									writeOut(l1l1lll111l1l1l1l1,resp);  //因为上面已经调用过chain.doFilter，所以这里只能直接输出，否则调用两次，会有奇怪问题
								}
								else
								{
									l1l1l1111l1l1l1l111.removeObject(currentURL);
								}
	      	  	  		  }
	      	  	  		  else
	      	  	  		  {
	      	  	  			  chain.doFilter(srequest,sresponse);	  	      	  	  			  
	      	  	  		  }
	
	      	  	  		  	return;
	      	  	  	  }

	      	  	 //如果页面已经缓存了，则直接分发到静态页面
				if (currentURL!=null)
				{
					  l1l1lll111l1l1l1l111 = l1l1l1111l1l1l1l111.l1l1l11l11l1l1l1l1(sysFolder,currentURL);
					  l1l1l11l1l1l1l111 = l1l1l1111l1l1l1l111.l1l1l11l11l1l1l1l1l1(sysFolder,currentURL);
								  
					  if (!l1l1lll111l1l1l1l111.equals(""))
					  {
					  		l1l1lll111l1l1l1l111 += "/";
					  }
								  
					  targetURL = "/"+l1l1lll111l1l1l1l111 + l1l1l11l1l1l1l111;
					  getFilterConfig().getServletContext().getRequestDispatcher( targetURL ).forward(srequest,sresponse);					
				}
				else
				{
					resp.sendRedirect(ConfigBean.getStringValue("systenFolder"));
				}

	      	  }

		  }
	      catch (Exception e) 
		  {
	    	  log.error("HtmlCacheFilter.doFilter["+currentURL+"] error:"+e);
	    	  StackTraceElement[] ste = e.getStackTrace();
	    	  StringBuffer sbe = new StringBuffer();
	    	  sbe.append(e.getMessage() + "\r\n");
	    	  for (int i = 0;i < ste.length;i++)
	    	  {
	    		  sbe.append(ste[i].toString() + "\r\n");
	    	  }
	    	  log.error(sbe.toString());	
	    	  throw new ServletException("HtmlCacheFilter.doFilter["+currentURL+"] error:"+e);
		  }
	}

	private void writeOut(byte[] c,HttpServletResponse resp)
		throws Exception
	{
		try 
		{
			ServletOutputStream webOutput = resp.getOutputStream();
			webOutput.write(c);
			webOutput.flush();
			webOutput.close();
		} 
		catch (IOException e) 
		{
			throw new Exception("writeOut error:"+e);
		}
	}

	public static void main(String args[])
	{

	}
}

