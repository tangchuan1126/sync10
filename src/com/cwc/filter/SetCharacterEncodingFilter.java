package com.cwc.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;







import org.apache.log4j.Logger;

import com.cwc.app.api.AdminMgr;
import com.cwc.app.api.SystemConfig;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.Environment;
import com.cwc.app.util.ThreadContext;
import com.cwc.authentication.AuthActionList;
import com.cwc.authentication.AuthPageList;
import com.cwc.initconf.Resource;
import com.cwc.initconf.WrapConfigValue;
import com.cwc.spring.util.MvcUtil;
import com.cwc.util.StringUtil;

/**
 * 乱码处理FILTER
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class SetCharacterEncodingFilter implements Filter
{
	static Logger log = Logger.getLogger("PLATFORM");
    protected String encoding;						//页面编码
    protected FilterConfig filterConfig;
    protected boolean ignore;
    private String notPermitPage;					//没有权限页面

//    private int compressionThreshold = 128;
    
    public SetCharacterEncodingFilter()
    {
        encoding = null;
        filterConfig = null;
        ignore = true;
    }

    //系统关闭时需要释放的资源
    public void destroy()
    {
        encoding = null;
        filterConfig = null;
        
        //SerializableManager.destroySerializable();
        Environment.desctroy();								//释放环境参数
        SystemConfig.cleanCache();							//释放系统配置
        AuthActionList.destroy();							//释放需要鉴权事件
        AuthPageList.destroy();								//释放需要鉴权页面资源
        WrapConfigValue.desctroy();        					//释放系统配置
        Resource.destroy();									//释放语言资源
        //ThreadManager.getInstance().shutdown(); 			//释放异步线程池
    }

    public void doFilter(ServletRequest servletrequest, ServletResponse servletresponse, FilterChain filterchain)
        throws IOException, ServletException
    {
    	boolean supportCompression = false;
    	final HttpServletResponse response = (HttpServletResponse)servletresponse;
    	final HttpServletRequest request = (HttpServletRequest)servletrequest;

    	try
		{
        	//把request response存放到线程变量中
//        	ThreadContext.put("request",request);
//        	ThreadContext.put("response",response);
        	String originalRequestURI = request.getRequestURI();//未经过filter处理过的requestURI
        	String paraString = request.getQueryString();
        	String originalRequestURL = request.getContextPath()+request.getServletPath().toString();//未经过filter处理过的requestURL
        	if(paraString!=null&&!paraString.equals(""))
        	{
        		originalRequestURL = originalRequestURL+"?"+paraString;
        	}        	
        	
        	servletrequest.setAttribute("originalRequestURI",originalRequestURI);
        	servletrequest.setAttribute("originalRequestURL",originalRequestURL);

        	//设置页面编码
			if(ignore || servletrequest.getCharacterEncoding() == null)
			{
			    String s = selectEncoding(servletrequest);
			    if(s != null)
			    {
			    	servletrequest.setCharacterEncoding(s);
			    }
			}
			String parString = request.getQueryString();
			
//	        if (supportCompression)
//	        {       	
//	            try 
//                {
//	            	GZIPResponseWrapper wrappedResponse = new GZIPResponseWrapper(response); 
//                	filterchain.doFilter(servletrequest, wrappedResponse);
//                	wrappedResponse.finishResponse();
//                }
//	            catch (Exception supportCompressionE) 
//                {                	
//                	log.error("GZIPResponseWrapper error:"+supportCompressionE);
//                	supportCompressionE.printStackTrace();
//                	filterchain.doFilter(servletrequest, servletresponse);                	
//                }
//                return;
//	        }
//	        else 
//	        {
	        	filterchain.doFilter(servletrequest, servletresponse);
	            return;
	        //}
		}
//        catch (UnsupportedEncodingException e)
//		{
//        	e.printStackTrace();
//			log.error("SetCharacterEncodingFilter.doFilter UnsupportedEncodingException:" + e);
//			throw new ServletException("SetCharacterEncodingFilter.doFilter UnsupportedEncodingException:" + e);
//		}
        catch (Throwable e)
		{
				StringBuffer sb = new StringBuffer("");
				Enumeration enu = request.getParameterNames();
				String key;
				while( enu.hasMoreElements() )
				{
					key = (String)enu.nextElement();
					sb.append(key);
					sb.append("=");
					sb.append(StringUtil.getString(request,key));
					sb.append("&");
				}
							
				log.error("SetCharacterEncodingFilter.doFilter:" + e);
				log.error("Referer:" + request.getHeader("referer"));
				log.error("Current URL:" + request.getRequestURI());
				log.error("Visitor:" + request.getHeader("User-Agent"));
				log.error("Parameters:" + sb.toString());
				log.error("IP:" + request.getRemoteAddr());

				//如果是系统管理员操作，则记录下操作人
				AdminMgr adminMgr = (AdminMgr)MvcUtil.getBeanFromContainer("adminMgr");
				AdminLoginBean adminLoggerBean = adminMgr.getAdminLoginBean(StringUtil.getSession(request));
				if (adminLoggerBean!=null)
				{
					log.error("System operator:" + adminLoggerBean.getAccount());
				}
				else
				{
					log.error("System operator:NULL");
				}
				
				StackTraceElement[] ste = e.getStackTrace();
				StringBuffer sbe = new StringBuffer();
				sbe.append(e.getMessage() + "\r\n");
				for (int i = 0;i < ste.length;i++)
				{
					sbe.append(ste[i].toString() + "\r\n");
				}
				log.error(sbe.toString());	
				
				
				try
				{
					MvcUtil.dispatch2Template(request, response,"systemInternalError", null);
				} 
				catch (Exception er)
				{
					log.error("SetCharacterEncodingFilter.doFilter:" + er);
				}
				
				return;
		}
    }

    /**
     * 初始化FILTER
     * @param filterconfig
     * @throws ServletException
     */
    public void init(FilterConfig filterconfig)
        throws ServletException
    {
    	//把ServletContext保存起来，方便程序内部不方便获取的地方进行调用
//    	Environment.setServletContext(filterconfig.getServletContext());
    	
        filterConfig = filterconfig;
        encoding = filterconfig.getInitParameter("encoding");
        String s = filterconfig.getInitParameter("ignore");
        notPermitPage = filterconfig.getInitParameter("operationNotPermit");
        
        if(s == null)
            ignore = true;
        else
        if(s.equalsIgnoreCase("true"))
            ignore = true;
        else
        if(s.equalsIgnoreCase("yes"))
            ignore = true;
        else
            ignore = false;
    }

    protected String selectEncoding(ServletRequest servletrequest)
    {
        return encoding;
    }
    
    public static void main(String args[])
    {
    	String t = "javax.servlet.ServletException: AdminAuthorizationFilter doFilter:com.cwc.exception.OperationNotPermitException: PageOperationNotPermitException:admin/config.jsp";
    	int p = t.lastIndexOf(":");
    	//system.out.println(t.substring(p+1));
    }
    
}





