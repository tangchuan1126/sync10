package com.cwc.filter;

import java.util.HashMap;

/**
 * 生成静态文件锁
 * 在一个时刻，只有一个线程获得写某个文件权限
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class HtmlWriteLock 
{
	private static HashMap URILock = new HashMap();
	
	/**
	 * 判断URI是否已经锁定
	 * @param uri
	 * @return
	 */
	public static synchronized boolean isNotLock(String uri)
	{
		//发现没有被锁时，马上锁定
		if (URILock.get(uri)==null)
		{
			URILock.put(uri, 1);
			return(true);
		}
		else
		{
			return(false);
		}
	}

	public static synchronized void releaseLock(String uri)
	{
		URILock.remove(uri);
	}
	
}
