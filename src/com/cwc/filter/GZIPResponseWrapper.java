package com.cwc.filter;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * GZIP压缩，暂时放弃使用，因为在大并发量时不太稳定
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class GZIPResponseWrapper  extends HttpServletResponseWrapper
{
	protected HttpServletResponse wrappedResponse = null;   
	  protected ServletOutputStream stream = null;   
	  protected PrintWriter writer = null;   
	  
	  public GZIPResponseWrapper(HttpServletResponse response) 
	  {   
	    super(response);   
	    wrappedResponse = response;   
	  }   
	  
	  public ServletOutputStream createOutputStream()
	  throws IOException 
	  {   
	    return (new GZIPResponseStream(wrappedResponse));   
	  }   
	  
	  public void finishResponse() 
	  {   
	    try {   
	      if (writer != null) {   
	        writer.close();   
	      } else {   
	        if (stream != null) {   
	          stream.close();   
	        }   
	      }   
	    } catch (IOException e) {   
	    }   
	  }   
	  
	  public void flushBuffer()
	  throws IOException {   
	    stream.flush();   
	  }   
	  
	  public ServletOutputStream getOutputStream()
	  throws IOException
	  {   
	    if (writer != null) {   
	      throw new IllegalStateException("getWriter() has already been called!");   
	    }   
	  
	    if (stream == null)   
	      stream = createOutputStream();   
	    return (stream);   
	  }   
	  
	  public PrintWriter getWriter() 
	  throws IOException 
	  {   
	    if (writer != null) {   
	      return (writer);   
	    }   
	  
	    if (stream != null) {   
	      throw new IllegalStateException("getOutputStream() has already been called!");   
	    }   
	  
	    stream = createOutputStream();   
	    writer = new PrintWriter(new OutputStreamWriter(stream, "UTF-8"));   
	    return (writer);   
	  }   
	  
	  public void setContentLength(int length) 
	  {   
	  }   

}
