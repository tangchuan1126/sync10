package com.cwc.http;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.StringEntity; 
import org.apache.log4j.Logger;

import com.cwc.exception.SystemException;

public class Client
{
	static Logger log = Logger.getLogger("ACTION");
	
	public static void httpGetDownLoad(String url,String filepath)
		throws SocketTimeoutException,Exception
	{
		DefaultHttpClient httpclient = null;
		BufferedReader reader = null;
		
		
		
		try 
		{
			httpclient = new DefaultHttpClient();
			
			HttpGet httpget = new HttpGet(url);
			
			
			HttpResponse response = httpclient.execute(httpget); 
			
			
			if(HttpStatus.SC_OK == response.getStatusLine().getStatusCode())
			{
				HttpEntity entity = response.getEntity();
				
				if(entity !=null)
				{
					File  file = new File(filepath);
					
					FileOutputStream output = new FileOutputStream(file);
					
					InputStream is = entity.getContent();
					
					byte[] b = new byte[1024];
					
					int j = 0;
					
					while((j = is.read(b))!=-1)
					{
						output.write(b,0,j);
					}
					
					output.flush();
					output.close();
					is.close();
				}
				else
				{
					log.error("entity is null");
				}
				
				if(entity !=null)
				{
//					entity.consumeContent();
					entity.getContent().close();
				}
			}
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String httpGetEPacket(String url,int timeout)
		throws SocketTimeoutException,Exception
	{
		DefaultHttpClient httpclient = null;
		BufferedReader reader = null;
		
		try
		{
		
			httpclient = new DefaultHttpClient();  
		    // 访问的目标站点，端口和协议  
		    // 目标地址  
		    HttpGet httpget = new HttpGet(url);
		    httpget.setHeader("version","international_eub_us_1.1");
		    httpget.setHeader("authenticate","vvme_f8bb3c37f7f03335919b010bc3c5d5e4");
		    // 执行  
		    HttpResponse response = httpclient.execute(httpget);  
		    HttpEntity entity = response.getEntity();  
		    
		 // 显示结果  
		    reader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));  
		    String line = null; 
		    StringBuffer resultSB = new StringBuffer();
		    while ((line = reader.readLine()) != null) {  
		      resultSB.append(line);
		    }  
		    
		    return(resultSB.toString());
		}
		catch (ClientProtocolException e)
		{
			throw new SystemException(e,"createPrintLabelBase64Img",log);
		}
		catch (SocketTimeoutException e)
		{
			throw e;
		}
		catch (IOException e)
		{
			throw new SystemException(e,"IOException",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"Exception",log);
		}
		finally
		{
			if (httpclient!=null)
			{
				httpclient.getConnectionManager().shutdown();
			}
			if (reader!=null)
			{
				reader.close();
			}
		}
	}
	
	public static String httpGet(String url,int timeout)
		throws SocketTimeoutException,Exception
	{
		DefaultHttpClient httpclient = null;
		BufferedReader reader = null;
		
		try
		{
		
			httpclient = new DefaultHttpClient();  
		    // 访问的目标站点，端口和协议  
		    // 代理的设置  
		    //HttpHost proxy = new HttpHost("10.60.8.20", 8080);  
		    //httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);  
		    // 目标地址  
		    HttpGet httpget = new HttpGet(url);
		    // 执行  
		    
		    //httpclient.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, 10000);
		    HttpResponse response = httpclient.execute(httpget);  
		    HttpEntity entity = response.getEntity();  
//		    // 显示结果  
//		    reader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));  
//		    String line = null;  
//		    while ((line = reader.readLine()) != null)
//		    {  
//		      //system.out.println(line);  
//		    }  
//		    
//		    if (entity != null)
//		    {  
//		      entity.consumeContent();  
//		    } 
		    
		 // 显示结果  
		    reader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));  
		    String line = null; 
		    StringBuffer resultSB = new StringBuffer();
		    while ((line = reader.readLine()) != null) {  
		      resultSB.append(line);
		    }  
		    
		    return(resultSB.toString());
		}
		catch (ClientProtocolException e)
		{
			throw new SystemException(e,"createPrintLabelBase64Img",log);
		}
		catch (SocketTimeoutException e)
		{
			throw e;
		}
		catch (IOException e)
		{
			throw new SystemException(e,"IOException",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"Exception",log);
		}
		finally
		{
			if (httpclient!=null)
			{
				httpclient.getConnectionManager().shutdown();
			}
			if (reader!=null)
			{
				reader.close();
			}
		}
	}
	
	public static String httpPostEPacket(String url,String para,int timeout)
		throws SocketTimeoutException,Exception
	{
		DefaultHttpClient httpclient = null;
		BufferedReader reader = null;
		HttpEntity entity = null;
		
		try
		{
			httpclient = new DefaultHttpClient();  
		     
		    StringEntity reqEntity = new StringEntity(para);  // 构造最简单的字符串数据 
		    reqEntity.setContentType("application/x-www-form-urlencoded;charset=utf-8");   // 设置类型  
		    
		    // 目标地址  
		    HttpPost httppost = new HttpPost(url); 
		   
		    httppost.setHeader("version","international_eub_us_1.1");
		    httppost.setHeader("authenticate","vvme_f8bb3c37f7f03335919b010bc3c5d5e4");
		    
		    httppost.setEntity(reqEntity);  // 设置请求的数据  
		    // 执行  
		    httpclient.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");  
		    HttpResponse response = httpclient.execute(httppost);  
		    entity = response.getEntity();  
		    
		    // 显示结果  
		    reader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));  
		    String line = null; 
		    StringBuffer resultSB = new StringBuffer();
		    while ((line = reader.readLine()) != null) {  
		      resultSB.append(line);
		    }  
		    
		    return(resultSB.toString());
		}
		catch (ClientProtocolException e)
		{
			throw new SystemException(e,"ClientProtocolException",log);
		}
		catch (SocketTimeoutException e)
		{
			throw e;
		}
		catch (IOException e)
		{
			throw new SystemException(e,"IOException",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"Exception",log);
		}
		finally
		{
			if (httpclient!=null)
			{
				httpclient.getConnectionManager().shutdown();
			}
			
			if (entity != null)    
			{
//				entity.consumeContent();
				entity.getContent().close();
			}
			
			if (reader!=null)
			{
				reader.close();
			}
		}
		
	}
	
	
	
	public static String httpPost(String url,String para,int timeout)
		throws SocketTimeoutException,Exception
	{
		DefaultHttpClient httpclient = null;
		BufferedReader reader = null;
		HttpEntity entity = null;
		
		try
		{
			httpclient = new DefaultHttpClient();  
		    // 代理的设置  
		    //HttpHost proxy = new HttpHost("10.60.8.20", 8080);  
		    //httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);  
		     
		    StringEntity reqEntity = new StringEntity(para);  // 构造最简单的字符串数据 
		    reqEntity.setContentType("application/x-www-form-urlencoded;charset=utf-8");   // 设置类型  
		    
		    // 目标地址  
		    HttpPost httppost = new HttpPost(url); 
		    httppost.setEntity(reqEntity);  // 设置请求的数据  
		    // 执行  
		    httpclient.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");  
		    //httpclient.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, timeout);
		    HttpResponse response = httpclient.execute(httppost);  
		    entity = response.getEntity();  
		    
//			org.apache.http.Header[] headers = response.getAllHeaders();
//			for (int i=0; i<headers.length; i++)
//			{
//				//system.out.println(headers[i].getName()+"="+headers[i].getValue());
//			}
//			
//            //system.out.println("Response content length: " + entity.getContentLength());  
//	        //system.out.println("Response getContentEncoding: " + entity.getContentEncoding());  
//	        //system.out.println("Response getContentType: " + entity.getContentType());  

		    // 显示结果  
		    reader = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));  
		    String line = null; 
		    StringBuffer resultSB = new StringBuffer();
		    while ((line = reader.readLine()) != null) {  
		      resultSB.append(line);
		    }  
		    
		    return(resultSB.toString());
		}
		catch (ClientProtocolException e)
		{
			throw new SystemException(e,"ClientProtocolException",log);
		}
		catch (SocketTimeoutException e)
		{
			throw e;
		}
		catch (IOException e)
		{
			throw new SystemException(e,"IOException",log);
		}
		catch (Exception e)
		{
			throw new SystemException(e,"Exception",log);
		}
		finally
		{
			if (httpclient!=null)
			{
				httpclient.getConnectionManager().shutdown();
			}
			
			if (entity != null)    
			{
//				entity.consumeContent();
				EntityUtils.consume(entity);
			}
			
			if (reader!=null)
			{
				reader.close();
			}
		}
		
	}
	
	public static String HttpSoap(String url,String soapData,int timeout)
		throws Exception
	{
		 	PostMethod postMethod = new PostMethod(url);    
	  
	        byte[] bytes = soapData.getBytes("utf-8");   
	        InputStream inputStream = new ByteArrayInputStream(bytes, 0,bytes.length);   
	        RequestEntity requestEntity = new InputStreamRequestEntity(inputStream,bytes.length, "application/soap+xml; charset=utf-8");  
	        postMethod.setRequestEntity(requestEntity);   
	  
	        HttpClient httpClient = new HttpClient();   
	        int statusCode = httpClient.executeMethod(postMethod);   
	        String soapResponseData = postMethod.getResponseBodyAsString();    
	        
	        //system.out.println("statusCode:"+statusCode);
	        
	        //system.out.println("soapResponseData:"+soapResponseData);
	        return soapResponseData;
	}
	
	public static void main(String args[]) throws Exception
	{
		
	}
}



