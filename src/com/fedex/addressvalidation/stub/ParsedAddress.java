/**
 * ParsedAddress.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.fedex.addressvalidation.stub;

public class ParsedAddress  implements java.io.Serializable {
    private com.fedex.addressvalidation.stub.ParsedAddressPart parsedUrbanizationCode;

    private com.fedex.addressvalidation.stub.ParsedAddressPart[] parsedStreetLine;

    private com.fedex.addressvalidation.stub.ParsedAddressPart parsedCity;

    private com.fedex.addressvalidation.stub.ParsedAddressPart parsedStateOrProvinceCode;

    private com.fedex.addressvalidation.stub.ParsedAddressPart parsedPostalCode;

    private com.fedex.addressvalidation.stub.ParsedAddressPart parsedCountryCode;

    public ParsedAddress() {
    }

    public ParsedAddress(
           com.fedex.addressvalidation.stub.ParsedAddressPart parsedUrbanizationCode,
           com.fedex.addressvalidation.stub.ParsedAddressPart[] parsedStreetLine,
           com.fedex.addressvalidation.stub.ParsedAddressPart parsedCity,
           com.fedex.addressvalidation.stub.ParsedAddressPart parsedStateOrProvinceCode,
           com.fedex.addressvalidation.stub.ParsedAddressPart parsedPostalCode,
           com.fedex.addressvalidation.stub.ParsedAddressPart parsedCountryCode) {
           this.parsedUrbanizationCode = parsedUrbanizationCode;
           this.parsedStreetLine = parsedStreetLine;
           this.parsedCity = parsedCity;
           this.parsedStateOrProvinceCode = parsedStateOrProvinceCode;
           this.parsedPostalCode = parsedPostalCode;
           this.parsedCountryCode = parsedCountryCode;
    }


    /**
     * Gets the parsedUrbanizationCode value for this ParsedAddress.
     * 
     * @return parsedUrbanizationCode
     */
    public com.fedex.addressvalidation.stub.ParsedAddressPart getParsedUrbanizationCode() {
        return parsedUrbanizationCode;
    }


    /**
     * Sets the parsedUrbanizationCode value for this ParsedAddress.
     * 
     * @param parsedUrbanizationCode
     */
    public void setParsedUrbanizationCode(com.fedex.addressvalidation.stub.ParsedAddressPart parsedUrbanizationCode) {
        this.parsedUrbanizationCode = parsedUrbanizationCode;
    }


    /**
     * Gets the parsedStreetLine value for this ParsedAddress.
     * 
     * @return parsedStreetLine
     */
    public com.fedex.addressvalidation.stub.ParsedAddressPart[] getParsedStreetLine() {
        return parsedStreetLine;
    }


    /**
     * Sets the parsedStreetLine value for this ParsedAddress.
     * 
     * @param parsedStreetLine
     */
    public void setParsedStreetLine(com.fedex.addressvalidation.stub.ParsedAddressPart[] parsedStreetLine) {
        this.parsedStreetLine = parsedStreetLine;
    }

    public com.fedex.addressvalidation.stub.ParsedAddressPart getParsedStreetLine(int i) {
        return this.parsedStreetLine[i];
    }

    public void setParsedStreetLine(int i, com.fedex.addressvalidation.stub.ParsedAddressPart _value) {
        this.parsedStreetLine[i] = _value;
    }


    /**
     * Gets the parsedCity value for this ParsedAddress.
     * 
     * @return parsedCity
     */
    public com.fedex.addressvalidation.stub.ParsedAddressPart getParsedCity() {
        return parsedCity;
    }


    /**
     * Sets the parsedCity value for this ParsedAddress.
     * 
     * @param parsedCity
     */
    public void setParsedCity(com.fedex.addressvalidation.stub.ParsedAddressPart parsedCity) {
        this.parsedCity = parsedCity;
    }


    /**
     * Gets the parsedStateOrProvinceCode value for this ParsedAddress.
     * 
     * @return parsedStateOrProvinceCode
     */
    public com.fedex.addressvalidation.stub.ParsedAddressPart getParsedStateOrProvinceCode() {
        return parsedStateOrProvinceCode;
    }


    /**
     * Sets the parsedStateOrProvinceCode value for this ParsedAddress.
     * 
     * @param parsedStateOrProvinceCode
     */
    public void setParsedStateOrProvinceCode(com.fedex.addressvalidation.stub.ParsedAddressPart parsedStateOrProvinceCode) {
        this.parsedStateOrProvinceCode = parsedStateOrProvinceCode;
    }


    /**
     * Gets the parsedPostalCode value for this ParsedAddress.
     * 
     * @return parsedPostalCode
     */
    public com.fedex.addressvalidation.stub.ParsedAddressPart getParsedPostalCode() {
        return parsedPostalCode;
    }


    /**
     * Sets the parsedPostalCode value for this ParsedAddress.
     * 
     * @param parsedPostalCode
     */
    public void setParsedPostalCode(com.fedex.addressvalidation.stub.ParsedAddressPart parsedPostalCode) {
        this.parsedPostalCode = parsedPostalCode;
    }


    /**
     * Gets the parsedCountryCode value for this ParsedAddress.
     * 
     * @return parsedCountryCode
     */
    public com.fedex.addressvalidation.stub.ParsedAddressPart getParsedCountryCode() {
        return parsedCountryCode;
    }


    /**
     * Sets the parsedCountryCode value for this ParsedAddress.
     * 
     * @param parsedCountryCode
     */
    public void setParsedCountryCode(com.fedex.addressvalidation.stub.ParsedAddressPart parsedCountryCode) {
        this.parsedCountryCode = parsedCountryCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ParsedAddress)) return false;
        ParsedAddress other = (ParsedAddress) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.parsedUrbanizationCode==null && other.getParsedUrbanizationCode()==null) || 
             (this.parsedUrbanizationCode!=null &&
              this.parsedUrbanizationCode.equals(other.getParsedUrbanizationCode()))) &&
            ((this.parsedStreetLine==null && other.getParsedStreetLine()==null) || 
             (this.parsedStreetLine!=null &&
              java.util.Arrays.equals(this.parsedStreetLine, other.getParsedStreetLine()))) &&
            ((this.parsedCity==null && other.getParsedCity()==null) || 
             (this.parsedCity!=null &&
              this.parsedCity.equals(other.getParsedCity()))) &&
            ((this.parsedStateOrProvinceCode==null && other.getParsedStateOrProvinceCode()==null) || 
             (this.parsedStateOrProvinceCode!=null &&
              this.parsedStateOrProvinceCode.equals(other.getParsedStateOrProvinceCode()))) &&
            ((this.parsedPostalCode==null && other.getParsedPostalCode()==null) || 
             (this.parsedPostalCode!=null &&
              this.parsedPostalCode.equals(other.getParsedPostalCode()))) &&
            ((this.parsedCountryCode==null && other.getParsedCountryCode()==null) || 
             (this.parsedCountryCode!=null &&
              this.parsedCountryCode.equals(other.getParsedCountryCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getParsedUrbanizationCode() != null) {
            _hashCode += getParsedUrbanizationCode().hashCode();
        }
        if (getParsedStreetLine() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getParsedStreetLine());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getParsedStreetLine(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getParsedCity() != null) {
            _hashCode += getParsedCity().hashCode();
        }
        if (getParsedStateOrProvinceCode() != null) {
            _hashCode += getParsedStateOrProvinceCode().hashCode();
        }
        if (getParsedPostalCode() != null) {
            _hashCode += getParsedPostalCode().hashCode();
        }
        if (getParsedCountryCode() != null) {
            _hashCode += getParsedCountryCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ParsedAddress.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddress"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parsedUrbanizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedUrbanizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddressPart"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parsedStreetLine");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedStreetLine"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddressPart"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parsedCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddressPart"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parsedStateOrProvinceCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedStateOrProvinceCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddressPart"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parsedPostalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedPostalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddressPart"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parsedCountryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedCountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddressPart"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
