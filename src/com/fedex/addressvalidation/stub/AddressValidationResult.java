/**
 * AddressValidationResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.fedex.addressvalidation.stub;

public class AddressValidationResult  implements java.io.Serializable {
    private java.lang.String addressId;

    private com.fedex.addressvalidation.stub.ProposedAddressDetail[] proposedAddressDetails;

    public AddressValidationResult() {
    }

    public AddressValidationResult(
           java.lang.String addressId,
           com.fedex.addressvalidation.stub.ProposedAddressDetail[] proposedAddressDetails) {
           this.addressId = addressId;
           this.proposedAddressDetails = proposedAddressDetails;
    }


    /**
     * Gets the addressId value for this AddressValidationResult.
     * 
     * @return addressId
     */
    public java.lang.String getAddressId() {
        return addressId;
    }


    /**
     * Sets the addressId value for this AddressValidationResult.
     * 
     * @param addressId
     */
    public void setAddressId(java.lang.String addressId) {
        this.addressId = addressId;
    }


    /**
     * Gets the proposedAddressDetails value for this AddressValidationResult.
     * 
     * @return proposedAddressDetails
     */
    public com.fedex.addressvalidation.stub.ProposedAddressDetail[] getProposedAddressDetails() {
        return proposedAddressDetails;
    }


    /**
     * Sets the proposedAddressDetails value for this AddressValidationResult.
     * 
     * @param proposedAddressDetails
     */
    public void setProposedAddressDetails(com.fedex.addressvalidation.stub.ProposedAddressDetail[] proposedAddressDetails) {
        this.proposedAddressDetails = proposedAddressDetails;
    }

    public com.fedex.addressvalidation.stub.ProposedAddressDetail getProposedAddressDetails(int i) {
        return this.proposedAddressDetails[i];
    }

    public void setProposedAddressDetails(int i, com.fedex.addressvalidation.stub.ProposedAddressDetail _value) {
        this.proposedAddressDetails[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddressValidationResult)) return false;
        AddressValidationResult other = (AddressValidationResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addressId==null && other.getAddressId()==null) || 
             (this.addressId!=null &&
              this.addressId.equals(other.getAddressId()))) &&
            ((this.proposedAddressDetails==null && other.getProposedAddressDetails()==null) || 
             (this.proposedAddressDetails!=null &&
              java.util.Arrays.equals(this.proposedAddressDetails, other.getProposedAddressDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddressId() != null) {
            _hashCode += getAddressId().hashCode();
        }
        if (getProposedAddressDetails() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProposedAddressDetails());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProposedAddressDetails(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddressValidationResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "AddressValidationResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "AddressId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proposedAddressDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ProposedAddressDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ProposedAddressDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
