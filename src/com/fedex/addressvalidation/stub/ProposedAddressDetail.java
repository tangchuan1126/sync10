/**
 * ProposedAddressDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.fedex.addressvalidation.stub;

public class ProposedAddressDetail  implements java.io.Serializable {
    private java.math.BigInteger score;

    private com.fedex.addressvalidation.stub.AddressValidationChangeType[] changes;

    private com.fedex.addressvalidation.stub.ResidentialStatusType residentialStatus;

    private com.fedex.addressvalidation.stub.DeliveryPointValidationType deliveryPointValidation;

    private java.lang.String companyName;

    private com.fedex.addressvalidation.stub.Address address;

    private com.fedex.addressvalidation.stub.ParsedAddressPart parsedCompanyName;

    private com.fedex.addressvalidation.stub.ParsedAddress parsedAddress;

    private java.lang.String removedNonAddressData;

    public ProposedAddressDetail() {
    }

    public ProposedAddressDetail(
           java.math.BigInteger score,
           com.fedex.addressvalidation.stub.AddressValidationChangeType[] changes,
           com.fedex.addressvalidation.stub.ResidentialStatusType residentialStatus,
           com.fedex.addressvalidation.stub.DeliveryPointValidationType deliveryPointValidation,
           java.lang.String companyName,
           com.fedex.addressvalidation.stub.Address address,
           com.fedex.addressvalidation.stub.ParsedAddressPart parsedCompanyName,
           com.fedex.addressvalidation.stub.ParsedAddress parsedAddress,
           java.lang.String removedNonAddressData) {
           this.score = score;
           this.changes = changes;
           this.residentialStatus = residentialStatus;
           this.deliveryPointValidation = deliveryPointValidation;
           this.companyName = companyName;
           this.address = address;
           this.parsedCompanyName = parsedCompanyName;
           this.parsedAddress = parsedAddress;
           this.removedNonAddressData = removedNonAddressData;
    }


    /**
     * Gets the score value for this ProposedAddressDetail.
     * 
     * @return score
     */
    public java.math.BigInteger getScore() {
        return score;
    }


    /**
     * Sets the score value for this ProposedAddressDetail.
     * 
     * @param score
     */
    public void setScore(java.math.BigInteger score) {
        this.score = score;
    }


    /**
     * Gets the changes value for this ProposedAddressDetail.
     * 
     * @return changes
     */
    public com.fedex.addressvalidation.stub.AddressValidationChangeType[] getChanges() {
        return changes;
    }


    /**
     * Sets the changes value for this ProposedAddressDetail.
     * 
     * @param changes
     */
    public void setChanges(com.fedex.addressvalidation.stub.AddressValidationChangeType[] changes) {
        this.changes = changes;
    }

    public com.fedex.addressvalidation.stub.AddressValidationChangeType getChanges(int i) {
        return this.changes[i];
    }

    public void setChanges(int i, com.fedex.addressvalidation.stub.AddressValidationChangeType _value) {
        this.changes[i] = _value;
    }


    /**
     * Gets the residentialStatus value for this ProposedAddressDetail.
     * 
     * @return residentialStatus
     */
    public com.fedex.addressvalidation.stub.ResidentialStatusType getResidentialStatus() {
        return residentialStatus;
    }


    /**
     * Sets the residentialStatus value for this ProposedAddressDetail.
     * 
     * @param residentialStatus
     */
    public void setResidentialStatus(com.fedex.addressvalidation.stub.ResidentialStatusType residentialStatus) {
        this.residentialStatus = residentialStatus;
    }


    /**
     * Gets the deliveryPointValidation value for this ProposedAddressDetail.
     * 
     * @return deliveryPointValidation
     */
    public com.fedex.addressvalidation.stub.DeliveryPointValidationType getDeliveryPointValidation() {
        return deliveryPointValidation;
    }


    /**
     * Sets the deliveryPointValidation value for this ProposedAddressDetail.
     * 
     * @param deliveryPointValidation
     */
    public void setDeliveryPointValidation(com.fedex.addressvalidation.stub.DeliveryPointValidationType deliveryPointValidation) {
        this.deliveryPointValidation = deliveryPointValidation;
    }


    /**
     * Gets the companyName value for this ProposedAddressDetail.
     * 
     * @return companyName
     */
    public java.lang.String getCompanyName() {
        return companyName;
    }


    /**
     * Sets the companyName value for this ProposedAddressDetail.
     * 
     * @param companyName
     */
    public void setCompanyName(java.lang.String companyName) {
        this.companyName = companyName;
    }


    /**
     * Gets the address value for this ProposedAddressDetail.
     * 
     * @return address
     */
    public com.fedex.addressvalidation.stub.Address getAddress() {
        return address;
    }


    /**
     * Sets the address value for this ProposedAddressDetail.
     * 
     * @param address
     */
    public void setAddress(com.fedex.addressvalidation.stub.Address address) {
        this.address = address;
    }


    /**
     * Gets the parsedCompanyName value for this ProposedAddressDetail.
     * 
     * @return parsedCompanyName
     */
    public com.fedex.addressvalidation.stub.ParsedAddressPart getParsedCompanyName() {
        return parsedCompanyName;
    }


    /**
     * Sets the parsedCompanyName value for this ProposedAddressDetail.
     * 
     * @param parsedCompanyName
     */
    public void setParsedCompanyName(com.fedex.addressvalidation.stub.ParsedAddressPart parsedCompanyName) {
        this.parsedCompanyName = parsedCompanyName;
    }


    /**
     * Gets the parsedAddress value for this ProposedAddressDetail.
     * 
     * @return parsedAddress
     */
    public com.fedex.addressvalidation.stub.ParsedAddress getParsedAddress() {
        return parsedAddress;
    }


    /**
     * Sets the parsedAddress value for this ProposedAddressDetail.
     * 
     * @param parsedAddress
     */
    public void setParsedAddress(com.fedex.addressvalidation.stub.ParsedAddress parsedAddress) {
        this.parsedAddress = parsedAddress;
    }


    /**
     * Gets the removedNonAddressData value for this ProposedAddressDetail.
     * 
     * @return removedNonAddressData
     */
    public java.lang.String getRemovedNonAddressData() {
        return removedNonAddressData;
    }


    /**
     * Sets the removedNonAddressData value for this ProposedAddressDetail.
     * 
     * @param removedNonAddressData
     */
    public void setRemovedNonAddressData(java.lang.String removedNonAddressData) {
        this.removedNonAddressData = removedNonAddressData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProposedAddressDetail)) return false;
        ProposedAddressDetail other = (ProposedAddressDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.score==null && other.getScore()==null) || 
             (this.score!=null &&
              this.score.equals(other.getScore()))) &&
            ((this.changes==null && other.getChanges()==null) || 
             (this.changes!=null &&
              java.util.Arrays.equals(this.changes, other.getChanges()))) &&
            ((this.residentialStatus==null && other.getResidentialStatus()==null) || 
             (this.residentialStatus!=null &&
              this.residentialStatus.equals(other.getResidentialStatus()))) &&
            ((this.deliveryPointValidation==null && other.getDeliveryPointValidation()==null) || 
             (this.deliveryPointValidation!=null &&
              this.deliveryPointValidation.equals(other.getDeliveryPointValidation()))) &&
            ((this.companyName==null && other.getCompanyName()==null) || 
             (this.companyName!=null &&
              this.companyName.equals(other.getCompanyName()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.parsedCompanyName==null && other.getParsedCompanyName()==null) || 
             (this.parsedCompanyName!=null &&
              this.parsedCompanyName.equals(other.getParsedCompanyName()))) &&
            ((this.parsedAddress==null && other.getParsedAddress()==null) || 
             (this.parsedAddress!=null &&
              this.parsedAddress.equals(other.getParsedAddress()))) &&
            ((this.removedNonAddressData==null && other.getRemovedNonAddressData()==null) || 
             (this.removedNonAddressData!=null &&
              this.removedNonAddressData.equals(other.getRemovedNonAddressData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getScore() != null) {
            _hashCode += getScore().hashCode();
        }
        if (getChanges() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getChanges());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getChanges(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResidentialStatus() != null) {
            _hashCode += getResidentialStatus().hashCode();
        }
        if (getDeliveryPointValidation() != null) {
            _hashCode += getDeliveryPointValidation().hashCode();
        }
        if (getCompanyName() != null) {
            _hashCode += getCompanyName().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getParsedCompanyName() != null) {
            _hashCode += getParsedCompanyName().hashCode();
        }
        if (getParsedAddress() != null) {
            _hashCode += getParsedAddress().hashCode();
        }
        if (getRemovedNonAddressData() != null) {
            _hashCode += getRemovedNonAddressData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProposedAddressDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ProposedAddressDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("score");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "Score"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "Changes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "AddressValidationChangeType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("residentialStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ResidentialStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ResidentialStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deliveryPointValidation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "DeliveryPointValidation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "DeliveryPointValidationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "CompanyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "Address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "Address"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parsedCompanyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedCompanyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddressPart"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parsedAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ParsedAddress"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("removedNonAddressData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "RemovedNonAddressData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
