/**
 * AddressValidationChangeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.fedex.addressvalidation.stub;

public class AddressValidationChangeType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected AddressValidationChangeType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _APARTMENT_NUMBER_NOT_FOUND = "APARTMENT_NUMBER_NOT_FOUND";
    public static final java.lang.String _APARTMENT_NUMBER_REQUIRED = "APARTMENT_NUMBER_REQUIRED";
    public static final java.lang.String _NORMALIZED = "NORMALIZED";
    public static final java.lang.String _REMOVED_DATA = "REMOVED_DATA";
    public static final java.lang.String _BOX_NUMBER_REQUIRED = "BOX_NUMBER_REQUIRED";
    public static final java.lang.String _NO_CHANGES = "NO_CHANGES";
    public static final java.lang.String _MODIFIED_TO_ACHIEVE_MATCH = "MODIFIED_TO_ACHIEVE_MATCH";
    public static final java.lang.String _STREET_RANGE_MATCH = "STREET_RANGE_MATCH";
    public static final java.lang.String _BOX_NUMBER_MATCH = "BOX_NUMBER_MATCH";
    public static final java.lang.String _RR_OR_HC_MATCH = "RR_OR_HC_MATCH";
    public static final java.lang.String _CITY_MATCH = "CITY_MATCH";
    public static final java.lang.String _POSTAL_CODE_MATCH = "POSTAL_CODE_MATCH";
    public static final java.lang.String _RR_OR_HC_BOX_NUMBER_NEEDED = "RR_OR_HC_BOX_NUMBER_NEEDED";
    public static final java.lang.String _FORMATTED_FOR_COUNTRY = "FORMATTED_FOR_COUNTRY";
    public static final java.lang.String _APO_OR_FPO_MATCH = "APO_OR_FPO_MATCH";
    public static final java.lang.String _GENERAL_DELIVERY_MATCH = "GENERAL_DELIVERY_MATCH";
    public static final java.lang.String _FIELD_TRUNCATED = "FIELD_TRUNCATED";
    public static final java.lang.String _UNABLE_TO_APPEND_NON_ADDRESS_DATA = "UNABLE_TO_APPEND_NON_ADDRESS_DATA";
    public static final java.lang.String _INSUFFICIENT_DATA = "INSUFFICIENT_DATA";
    public static final java.lang.String _HOUSE_OR_BOX_NUMBER_NOT_FOUND = "HOUSE_OR_BOX_NUMBER_NOT_FOUND";
    public static final java.lang.String _POSTAL_CODE_NOT_FOUND = "POSTAL_CODE_NOT_FOUND";
    public static final java.lang.String _INVALID_COUNTRY = "INVALID_COUNTRY";
    public static final java.lang.String _SERVICE_UNAVAILABLE_FOR_ADDRESS = "SERVICE_UNAVAILABLE_FOR_ADDRESS";
    public static final AddressValidationChangeType APARTMENT_NUMBER_NOT_FOUND = new AddressValidationChangeType(_APARTMENT_NUMBER_NOT_FOUND);
    public static final AddressValidationChangeType APARTMENT_NUMBER_REQUIRED = new AddressValidationChangeType(_APARTMENT_NUMBER_REQUIRED);
    public static final AddressValidationChangeType NORMALIZED = new AddressValidationChangeType(_NORMALIZED);
    public static final AddressValidationChangeType REMOVED_DATA = new AddressValidationChangeType(_REMOVED_DATA);
    public static final AddressValidationChangeType BOX_NUMBER_REQUIRED = new AddressValidationChangeType(_BOX_NUMBER_REQUIRED);
    public static final AddressValidationChangeType NO_CHANGES = new AddressValidationChangeType(_NO_CHANGES);
    public static final AddressValidationChangeType MODIFIED_TO_ACHIEVE_MATCH = new AddressValidationChangeType(_MODIFIED_TO_ACHIEVE_MATCH);
    public static final AddressValidationChangeType STREET_RANGE_MATCH = new AddressValidationChangeType(_STREET_RANGE_MATCH);
    public static final AddressValidationChangeType BOX_NUMBER_MATCH = new AddressValidationChangeType(_BOX_NUMBER_MATCH);
    public static final AddressValidationChangeType RR_OR_HC_MATCH = new AddressValidationChangeType(_RR_OR_HC_MATCH);
    public static final AddressValidationChangeType CITY_MATCH = new AddressValidationChangeType(_CITY_MATCH);
    public static final AddressValidationChangeType POSTAL_CODE_MATCH = new AddressValidationChangeType(_POSTAL_CODE_MATCH);
    public static final AddressValidationChangeType RR_OR_HC_BOX_NUMBER_NEEDED = new AddressValidationChangeType(_RR_OR_HC_BOX_NUMBER_NEEDED);
    public static final AddressValidationChangeType FORMATTED_FOR_COUNTRY = new AddressValidationChangeType(_FORMATTED_FOR_COUNTRY);
    public static final AddressValidationChangeType APO_OR_FPO_MATCH = new AddressValidationChangeType(_APO_OR_FPO_MATCH);
    public static final AddressValidationChangeType GENERAL_DELIVERY_MATCH = new AddressValidationChangeType(_GENERAL_DELIVERY_MATCH);
    public static final AddressValidationChangeType FIELD_TRUNCATED = new AddressValidationChangeType(_FIELD_TRUNCATED);
    public static final AddressValidationChangeType UNABLE_TO_APPEND_NON_ADDRESS_DATA = new AddressValidationChangeType(_UNABLE_TO_APPEND_NON_ADDRESS_DATA);
    public static final AddressValidationChangeType INSUFFICIENT_DATA = new AddressValidationChangeType(_INSUFFICIENT_DATA);
    public static final AddressValidationChangeType HOUSE_OR_BOX_NUMBER_NOT_FOUND = new AddressValidationChangeType(_HOUSE_OR_BOX_NUMBER_NOT_FOUND);
    public static final AddressValidationChangeType POSTAL_CODE_NOT_FOUND = new AddressValidationChangeType(_POSTAL_CODE_NOT_FOUND);
    public static final AddressValidationChangeType INVALID_COUNTRY = new AddressValidationChangeType(_INVALID_COUNTRY);
    public static final AddressValidationChangeType SERVICE_UNAVAILABLE_FOR_ADDRESS = new AddressValidationChangeType(_SERVICE_UNAVAILABLE_FOR_ADDRESS);
    public java.lang.String getValue() { return _value_;}
    public static AddressValidationChangeType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        AddressValidationChangeType enumeration = (AddressValidationChangeType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static AddressValidationChangeType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddressValidationChangeType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "AddressValidationChangeType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
