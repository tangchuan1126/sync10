/**
 * AddressValidationAccuracyType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.fedex.addressvalidation.stub;

public class AddressValidationAccuracyType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected AddressValidationAccuracyType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _EXACT = "EXACT";
    public static final java.lang.String _TIGHT = "TIGHT";
    public static final java.lang.String _MEDIUM = "MEDIUM";
    public static final java.lang.String _LOOSE = "LOOSE";
    public static final AddressValidationAccuracyType EXACT = new AddressValidationAccuracyType(_EXACT);
    public static final AddressValidationAccuracyType TIGHT = new AddressValidationAccuracyType(_TIGHT);
    public static final AddressValidationAccuracyType MEDIUM = new AddressValidationAccuracyType(_MEDIUM);
    public static final AddressValidationAccuracyType LOOSE = new AddressValidationAccuracyType(_LOOSE);
    public java.lang.String getValue() { return _value_;}
    public static AddressValidationAccuracyType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        AddressValidationAccuracyType enumeration = (AddressValidationAccuracyType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static AddressValidationAccuracyType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddressValidationAccuracyType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "AddressValidationAccuracyType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
