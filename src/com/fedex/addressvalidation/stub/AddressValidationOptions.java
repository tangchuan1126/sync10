/**
 * AddressValidationOptions.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.fedex.addressvalidation.stub;

public class AddressValidationOptions  implements java.io.Serializable {
    private java.lang.Boolean verifyAddresses;

    private java.lang.Boolean checkResidentialStatus;

    private org.apache.axis.types.PositiveInteger maximumNumberOfMatches;

    private com.fedex.addressvalidation.stub.AddressValidationAccuracyType streetAccuracy;

    private com.fedex.addressvalidation.stub.AddressValidationAccuracyType directionalAccuracy;

    private com.fedex.addressvalidation.stub.AddressValidationAccuracyType companyNameAccuracy;

    private java.lang.Boolean convertToUpperCase;

    private java.lang.Boolean recognizeAlternateCityNames;

    private java.lang.Boolean returnParsedElements;

    public AddressValidationOptions() {
    }

    public AddressValidationOptions(
           java.lang.Boolean verifyAddresses,
           java.lang.Boolean checkResidentialStatus,
           org.apache.axis.types.PositiveInteger maximumNumberOfMatches,
           com.fedex.addressvalidation.stub.AddressValidationAccuracyType streetAccuracy,
           com.fedex.addressvalidation.stub.AddressValidationAccuracyType directionalAccuracy,
           com.fedex.addressvalidation.stub.AddressValidationAccuracyType companyNameAccuracy,
           java.lang.Boolean convertToUpperCase,
           java.lang.Boolean recognizeAlternateCityNames,
           java.lang.Boolean returnParsedElements) {
           this.verifyAddresses = verifyAddresses;
           this.checkResidentialStatus = checkResidentialStatus;
           this.maximumNumberOfMatches = maximumNumberOfMatches;
           this.streetAccuracy = streetAccuracy;
           this.directionalAccuracy = directionalAccuracy;
           this.companyNameAccuracy = companyNameAccuracy;
           this.convertToUpperCase = convertToUpperCase;
           this.recognizeAlternateCityNames = recognizeAlternateCityNames;
           this.returnParsedElements = returnParsedElements;
    }


    /**
     * Gets the verifyAddresses value for this AddressValidationOptions.
     * 
     * @return verifyAddresses
     */
    public java.lang.Boolean getVerifyAddresses() {
        return verifyAddresses;
    }


    /**
     * Sets the verifyAddresses value for this AddressValidationOptions.
     * 
     * @param verifyAddresses
     */
    public void setVerifyAddresses(java.lang.Boolean verifyAddresses) {
        this.verifyAddresses = verifyAddresses;
    }


    /**
     * Gets the checkResidentialStatus value for this AddressValidationOptions.
     * 
     * @return checkResidentialStatus
     */
    public java.lang.Boolean getCheckResidentialStatus() {
        return checkResidentialStatus;
    }


    /**
     * Sets the checkResidentialStatus value for this AddressValidationOptions.
     * 
     * @param checkResidentialStatus
     */
    public void setCheckResidentialStatus(java.lang.Boolean checkResidentialStatus) {
        this.checkResidentialStatus = checkResidentialStatus;
    }


    /**
     * Gets the maximumNumberOfMatches value for this AddressValidationOptions.
     * 
     * @return maximumNumberOfMatches
     */
    public org.apache.axis.types.PositiveInteger getMaximumNumberOfMatches() {
        return maximumNumberOfMatches;
    }


    /**
     * Sets the maximumNumberOfMatches value for this AddressValidationOptions.
     * 
     * @param maximumNumberOfMatches
     */
    public void setMaximumNumberOfMatches(org.apache.axis.types.PositiveInteger maximumNumberOfMatches) {
        this.maximumNumberOfMatches = maximumNumberOfMatches;
    }


    /**
     * Gets the streetAccuracy value for this AddressValidationOptions.
     * 
     * @return streetAccuracy
     */
    public com.fedex.addressvalidation.stub.AddressValidationAccuracyType getStreetAccuracy() {
        return streetAccuracy;
    }


    /**
     * Sets the streetAccuracy value for this AddressValidationOptions.
     * 
     * @param streetAccuracy
     */
    public void setStreetAccuracy(com.fedex.addressvalidation.stub.AddressValidationAccuracyType streetAccuracy) {
        this.streetAccuracy = streetAccuracy;
    }


    /**
     * Gets the directionalAccuracy value for this AddressValidationOptions.
     * 
     * @return directionalAccuracy
     */
    public com.fedex.addressvalidation.stub.AddressValidationAccuracyType getDirectionalAccuracy() {
        return directionalAccuracy;
    }


    /**
     * Sets the directionalAccuracy value for this AddressValidationOptions.
     * 
     * @param directionalAccuracy
     */
    public void setDirectionalAccuracy(com.fedex.addressvalidation.stub.AddressValidationAccuracyType directionalAccuracy) {
        this.directionalAccuracy = directionalAccuracy;
    }


    /**
     * Gets the companyNameAccuracy value for this AddressValidationOptions.
     * 
     * @return companyNameAccuracy
     */
    public com.fedex.addressvalidation.stub.AddressValidationAccuracyType getCompanyNameAccuracy() {
        return companyNameAccuracy;
    }


    /**
     * Sets the companyNameAccuracy value for this AddressValidationOptions.
     * 
     * @param companyNameAccuracy
     */
    public void setCompanyNameAccuracy(com.fedex.addressvalidation.stub.AddressValidationAccuracyType companyNameAccuracy) {
        this.companyNameAccuracy = companyNameAccuracy;
    }


    /**
     * Gets the convertToUpperCase value for this AddressValidationOptions.
     * 
     * @return convertToUpperCase
     */
    public java.lang.Boolean getConvertToUpperCase() {
        return convertToUpperCase;
    }


    /**
     * Sets the convertToUpperCase value for this AddressValidationOptions.
     * 
     * @param convertToUpperCase
     */
    public void setConvertToUpperCase(java.lang.Boolean convertToUpperCase) {
        this.convertToUpperCase = convertToUpperCase;
    }


    /**
     * Gets the recognizeAlternateCityNames value for this AddressValidationOptions.
     * 
     * @return recognizeAlternateCityNames
     */
    public java.lang.Boolean getRecognizeAlternateCityNames() {
        return recognizeAlternateCityNames;
    }


    /**
     * Sets the recognizeAlternateCityNames value for this AddressValidationOptions.
     * 
     * @param recognizeAlternateCityNames
     */
    public void setRecognizeAlternateCityNames(java.lang.Boolean recognizeAlternateCityNames) {
        this.recognizeAlternateCityNames = recognizeAlternateCityNames;
    }


    /**
     * Gets the returnParsedElements value for this AddressValidationOptions.
     * 
     * @return returnParsedElements
     */
    public java.lang.Boolean getReturnParsedElements() {
        return returnParsedElements;
    }


    /**
     * Sets the returnParsedElements value for this AddressValidationOptions.
     * 
     * @param returnParsedElements
     */
    public void setReturnParsedElements(java.lang.Boolean returnParsedElements) {
        this.returnParsedElements = returnParsedElements;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddressValidationOptions)) return false;
        AddressValidationOptions other = (AddressValidationOptions) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.verifyAddresses==null && other.getVerifyAddresses()==null) || 
             (this.verifyAddresses!=null &&
              this.verifyAddresses.equals(other.getVerifyAddresses()))) &&
            ((this.checkResidentialStatus==null && other.getCheckResidentialStatus()==null) || 
             (this.checkResidentialStatus!=null &&
              this.checkResidentialStatus.equals(other.getCheckResidentialStatus()))) &&
            ((this.maximumNumberOfMatches==null && other.getMaximumNumberOfMatches()==null) || 
             (this.maximumNumberOfMatches!=null &&
              this.maximumNumberOfMatches.equals(other.getMaximumNumberOfMatches()))) &&
            ((this.streetAccuracy==null && other.getStreetAccuracy()==null) || 
             (this.streetAccuracy!=null &&
              this.streetAccuracy.equals(other.getStreetAccuracy()))) &&
            ((this.directionalAccuracy==null && other.getDirectionalAccuracy()==null) || 
             (this.directionalAccuracy!=null &&
              this.directionalAccuracy.equals(other.getDirectionalAccuracy()))) &&
            ((this.companyNameAccuracy==null && other.getCompanyNameAccuracy()==null) || 
             (this.companyNameAccuracy!=null &&
              this.companyNameAccuracy.equals(other.getCompanyNameAccuracy()))) &&
            ((this.convertToUpperCase==null && other.getConvertToUpperCase()==null) || 
             (this.convertToUpperCase!=null &&
              this.convertToUpperCase.equals(other.getConvertToUpperCase()))) &&
            ((this.recognizeAlternateCityNames==null && other.getRecognizeAlternateCityNames()==null) || 
             (this.recognizeAlternateCityNames!=null &&
              this.recognizeAlternateCityNames.equals(other.getRecognizeAlternateCityNames()))) &&
            ((this.returnParsedElements==null && other.getReturnParsedElements()==null) || 
             (this.returnParsedElements!=null &&
              this.returnParsedElements.equals(other.getReturnParsedElements())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVerifyAddresses() != null) {
            _hashCode += getVerifyAddresses().hashCode();
        }
        if (getCheckResidentialStatus() != null) {
            _hashCode += getCheckResidentialStatus().hashCode();
        }
        if (getMaximumNumberOfMatches() != null) {
            _hashCode += getMaximumNumberOfMatches().hashCode();
        }
        if (getStreetAccuracy() != null) {
            _hashCode += getStreetAccuracy().hashCode();
        }
        if (getDirectionalAccuracy() != null) {
            _hashCode += getDirectionalAccuracy().hashCode();
        }
        if (getCompanyNameAccuracy() != null) {
            _hashCode += getCompanyNameAccuracy().hashCode();
        }
        if (getConvertToUpperCase() != null) {
            _hashCode += getConvertToUpperCase().hashCode();
        }
        if (getRecognizeAlternateCityNames() != null) {
            _hashCode += getRecognizeAlternateCityNames().hashCode();
        }
        if (getReturnParsedElements() != null) {
            _hashCode += getReturnParsedElements().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddressValidationOptions.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "AddressValidationOptions"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verifyAddresses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "VerifyAddresses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkResidentialStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "CheckResidentialStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maximumNumberOfMatches");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "MaximumNumberOfMatches"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "positiveInteger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("streetAccuracy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "StreetAccuracy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "AddressValidationAccuracyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directionalAccuracy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "DirectionalAccuracy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "AddressValidationAccuracyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyNameAccuracy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "CompanyNameAccuracy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "AddressValidationAccuracyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("convertToUpperCase");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ConvertToUpperCase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recognizeAlternateCityNames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "RecognizeAlternateCityNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnParsedElements");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/addressvalidation/v2", "ReturnParsedElements"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
