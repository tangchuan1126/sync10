
package com.santai;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HeaderRequest" type="{http://www.example.org/ShipRate/}HeaderRequest"/>
 *         &lt;element name="addWarehouseOrderRequestInfo" type="{http://www.example.org/ShipRate/}addWarehouseOrderRequestInfoArray"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerRequest",
    "addWarehouseOrderRequestInfo"
})
@XmlRootElement(name = "addWarehouseOrderRequest")
public class AddWarehouseOrderRequest {

    @XmlElement(name = "HeaderRequest", required = true)
    protected HeaderRequest headerRequest;
    @XmlElement(required = true)
    protected AddWarehouseOrderRequestInfoArray addWarehouseOrderRequestInfo;

    /**
     * Gets the value of the headerRequest property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderRequest }
     *     
     */
    public HeaderRequest getHeaderRequest() {
        return headerRequest;
    }

    /**
     * Sets the value of the headerRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderRequest }
     *     
     */
    public void setHeaderRequest(HeaderRequest value) {
        this.headerRequest = value;
    }

    /**
     * Gets the value of the addWarehouseOrderRequestInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AddWarehouseOrderRequestInfoArray }
     *     
     */
    public AddWarehouseOrderRequestInfoArray getAddWarehouseOrderRequestInfo() {
        return addWarehouseOrderRequestInfo;
    }

    /**
     * Sets the value of the addWarehouseOrderRequestInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddWarehouseOrderRequestInfoArray }
     *     
     */
    public void setAddWarehouseOrderRequestInfo(AddWarehouseOrderRequestInfoArray value) {
        this.addWarehouseOrderRequestInfo = value;
    }

}
