
package com.santai;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for goodsDetailsArray complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="goodsDetailsArray">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="detailDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="detailQuantity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="detailCustomLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="detailWorth" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="detailWeight" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="detailEbayTxnId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="detailEbayItemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="detailEbayUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="detailEbayPayDate" type="{http://www.w3.org/2001/XMLSchema}time" minOccurs="0"/>
 *         &lt;element name="detailEbaySoldDate" type="{http://www.w3.org/2001/XMLSchema}time" minOccurs="0"/>
 *         &lt;element name="detailDescriptionCN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="detailName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="origin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enMaterial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cnMaterial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "goodsDetailsArray", propOrder = {
    "detailDescription",
    "detailQuantity",
    "detailCustomLabel",
    "detailWorth",
    "detailWeight",
    "detailEbayTxnId",
    "detailEbayItemId",
    "detailEbayUserId",
    "detailEbayPayDate",
    "detailEbaySoldDate",
    "detailDescriptionCN",
    "detailName",
    "hsCode",
    "origin",
    "enMaterial",
    "cnMaterial"
})
public class GoodsDetailsArray {

    protected String detailDescription;
    protected Integer detailQuantity;
    protected String detailCustomLabel;
    protected Float detailWorth;
    protected Float detailWeight;
    protected String detailEbayTxnId;
    protected String detailEbayItemId;
    protected String detailEbayUserId;
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar detailEbayPayDate;
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar detailEbaySoldDate;
    protected String detailDescriptionCN;
    protected String detailName;
    protected String hsCode;
    protected String origin;
    protected String enMaterial;
    protected String cnMaterial;

    /**
     * Gets the value of the detailDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailDescription() {
        return detailDescription;
    }

    /**
     * Sets the value of the detailDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailDescription(String value) {
        this.detailDescription = value;
    }

    /**
     * Gets the value of the detailQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDetailQuantity() {
        return detailQuantity;
    }

    /**
     * Sets the value of the detailQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDetailQuantity(Integer value) {
        this.detailQuantity = value;
    }

    /**
     * Gets the value of the detailCustomLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailCustomLabel() {
        return detailCustomLabel;
    }

    /**
     * Sets the value of the detailCustomLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailCustomLabel(String value) {
        this.detailCustomLabel = value;
    }

    /**
     * Gets the value of the detailWorth property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getDetailWorth() {
        return detailWorth;
    }

    /**
     * Sets the value of the detailWorth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setDetailWorth(Float value) {
        this.detailWorth = value;
    }

    /**
     * Gets the value of the detailWeight property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getDetailWeight() {
        return detailWeight;
    }

    /**
     * Sets the value of the detailWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setDetailWeight(Float value) {
        this.detailWeight = value;
    }

    /**
     * Gets the value of the detailEbayTxnId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailEbayTxnId() {
        return detailEbayTxnId;
    }

    /**
     * Sets the value of the detailEbayTxnId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailEbayTxnId(String value) {
        this.detailEbayTxnId = value;
    }

    /**
     * Gets the value of the detailEbayItemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailEbayItemId() {
        return detailEbayItemId;
    }

    /**
     * Sets the value of the detailEbayItemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailEbayItemId(String value) {
        this.detailEbayItemId = value;
    }

    /**
     * Gets the value of the detailEbayUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailEbayUserId() {
        return detailEbayUserId;
    }

    /**
     * Sets the value of the detailEbayUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailEbayUserId(String value) {
        this.detailEbayUserId = value;
    }

    /**
     * Gets the value of the detailEbayPayDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDetailEbayPayDate() {
        return detailEbayPayDate;
    }

    /**
     * Sets the value of the detailEbayPayDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDetailEbayPayDate(XMLGregorianCalendar value) {
        this.detailEbayPayDate = value;
    }

    /**
     * Gets the value of the detailEbaySoldDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDetailEbaySoldDate() {
        return detailEbaySoldDate;
    }

    /**
     * Sets the value of the detailEbaySoldDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDetailEbaySoldDate(XMLGregorianCalendar value) {
        this.detailEbaySoldDate = value;
    }

    /**
     * Gets the value of the detailDescriptionCN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailDescriptionCN() {
        return detailDescriptionCN;
    }

    /**
     * Sets the value of the detailDescriptionCN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailDescriptionCN(String value) {
        this.detailDescriptionCN = value;
    }

    /**
     * Gets the value of the detailName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailName() {
        return detailName;
    }

    /**
     * Sets the value of the detailName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailName(String value) {
        this.detailName = value;
    }

    /**
     * Gets the value of the hsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHsCode() {
        return hsCode;
    }

    /**
     * Sets the value of the hsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHsCode(String value) {
        this.hsCode = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Gets the value of the enMaterial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnMaterial() {
        return enMaterial;
    }

    /**
     * Sets the value of the enMaterial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnMaterial(String value) {
        this.enMaterial = value;
    }

    /**
     * Gets the value of the cnMaterial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnMaterial() {
        return cnMaterial;
    }

    /**
     * Sets the value of the cnMaterial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnMaterial(String value) {
        this.cnMaterial = value;
    }

}
