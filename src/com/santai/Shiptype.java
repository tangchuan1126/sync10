
package com.santai;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for shiptype complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="shiptype">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="method_code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="en_name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="cn_name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="deliverytime" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="iftracking" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="is_weight" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "shiptype")
public class Shiptype {

    @XmlAttribute(name = "method_code")
    protected String methodCode;
    @XmlAttribute(name = "en_name")
    protected String enName;
    @XmlAttribute(name = "cn_name")
    protected String cnName;
    @XmlAttribute(name = "deliverytime")
    protected String deliverytime;
    @XmlAttribute(name = "iftracking")
    protected String iftracking;
    @XmlAttribute(name = "is_weight")
    protected String isWeight;

    /**
     * Gets the value of the methodCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodCode() {
        return methodCode;
    }

    /**
     * Sets the value of the methodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodCode(String value) {
        this.methodCode = value;
    }

    /**
     * Gets the value of the enName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnName() {
        return enName;
    }

    /**
     * Sets the value of the enName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnName(String value) {
        this.enName = value;
    }

    /**
     * Gets the value of the cnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnName() {
        return cnName;
    }

    /**
     * Sets the value of the cnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnName(String value) {
        this.cnName = value;
    }

    /**
     * Gets the value of the deliverytime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliverytime() {
        return deliverytime;
    }

    /**
     * Sets the value of the deliverytime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliverytime(String value) {
        this.deliverytime = value;
    }

    /**
     * Gets the value of the iftracking property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIftracking() {
        return iftracking;
    }

    /**
     * Sets the value of the iftracking property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIftracking(String value) {
        this.iftracking = value;
    }

    /**
     * Gets the value of the isWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsWeight() {
        return isWeight;
    }

    /**
     * Sets the value of the isWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsWeight(String value) {
        this.isWeight = value;
    }

}
