
package com.santai;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addWarehouseOrderRequestInfoArray complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addWarehouseOrderRequestInfoArray">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="recipientName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="recipientCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="recipientZipCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="recipientState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="recipientPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerOrderNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="warehouseEnName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="shippingMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="goodsDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="goodsQuantity" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="goodsDeclareWorth" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="goodsWeight" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="productDetails" type="{http://www.example.org/ShipRate/}productDetailsArray" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addWarehouseOrderRequestInfoArray", propOrder = {
    "recipientName",
    "recipientCountry",
    "recipientZipCode",
    "recipientState",
    "recipientCity",
    "recipientAddress",
    "recipientPhone",
    "customerOrderNo",
    "warehouseEnName",
    "shippingMethod",
    "goodsDescription",
    "goodsQuantity",
    "goodsDeclareWorth",
    "goodsWeight",
    "productDetails"
})
public class AddWarehouseOrderRequestInfoArray {

    @XmlElement(required = true)
    protected String recipientName;
    @XmlElement(required = true)
    protected String recipientCountry;
    @XmlElement(required = true)
    protected String recipientZipCode;
    protected String recipientState;
    protected String recipientCity;
    @XmlElement(required = true)
    protected String recipientAddress;
    protected String recipientPhone;
    protected String customerOrderNo;
    @XmlElement(required = true)
    protected String warehouseEnName;
    @XmlElement(required = true)
    protected String shippingMethod;
    @XmlElement(required = true)
    protected String goodsDescription;
    protected int goodsQuantity;
    protected float goodsDeclareWorth;
    protected Float goodsWeight;
    @XmlElement(required = true)
    protected List<ProductDetailsArray> productDetails;

    /**
     * Gets the value of the recipientName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientName() {
        return recipientName;
    }

    /**
     * Sets the value of the recipientName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientName(String value) {
        this.recipientName = value;
    }

    /**
     * Gets the value of the recipientCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientCountry() {
        return recipientCountry;
    }

    /**
     * Sets the value of the recipientCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientCountry(String value) {
        this.recipientCountry = value;
    }

    /**
     * Gets the value of the recipientZipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientZipCode() {
        return recipientZipCode;
    }

    /**
     * Sets the value of the recipientZipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientZipCode(String value) {
        this.recipientZipCode = value;
    }

    /**
     * Gets the value of the recipientState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientState() {
        return recipientState;
    }

    /**
     * Sets the value of the recipientState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientState(String value) {
        this.recipientState = value;
    }

    /**
     * Gets the value of the recipientCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientCity() {
        return recipientCity;
    }

    /**
     * Sets the value of the recipientCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientCity(String value) {
        this.recipientCity = value;
    }

    /**
     * Gets the value of the recipientAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientAddress() {
        return recipientAddress;
    }

    /**
     * Sets the value of the recipientAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientAddress(String value) {
        this.recipientAddress = value;
    }

    /**
     * Gets the value of the recipientPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientPhone() {
        return recipientPhone;
    }

    /**
     * Sets the value of the recipientPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientPhone(String value) {
        this.recipientPhone = value;
    }

    /**
     * Gets the value of the customerOrderNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerOrderNo() {
        return customerOrderNo;
    }

    /**
     * Sets the value of the customerOrderNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerOrderNo(String value) {
        this.customerOrderNo = value;
    }

    /**
     * Gets the value of the warehouseEnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarehouseEnName() {
        return warehouseEnName;
    }

    /**
     * Sets the value of the warehouseEnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarehouseEnName(String value) {
        this.warehouseEnName = value;
    }

    /**
     * Gets the value of the shippingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * Sets the value of the shippingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingMethod(String value) {
        this.shippingMethod = value;
    }

    /**
     * Gets the value of the goodsDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodsDescription() {
        return goodsDescription;
    }

    /**
     * Sets the value of the goodsDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodsDescription(String value) {
        this.goodsDescription = value;
    }

    /**
     * Gets the value of the goodsQuantity property.
     * 
     */
    public int getGoodsQuantity() {
        return goodsQuantity;
    }

    /**
     * Sets the value of the goodsQuantity property.
     * 
     */
    public void setGoodsQuantity(int value) {
        this.goodsQuantity = value;
    }

    /**
     * Gets the value of the goodsDeclareWorth property.
     * 
     */
    public float getGoodsDeclareWorth() {
        return goodsDeclareWorth;
    }

    /**
     * Sets the value of the goodsDeclareWorth property.
     * 
     */
    public void setGoodsDeclareWorth(float value) {
        this.goodsDeclareWorth = value;
    }

    /**
     * Gets the value of the goodsWeight property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getGoodsWeight() {
        return goodsWeight;
    }

    /**
     * Sets the value of the goodsWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setGoodsWeight(Float value) {
        this.goodsWeight = value;
    }

    /**
     * Gets the value of the productDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductDetailsArray }
     * 
     * 
     */
    public List<ProductDetailsArray> getProductDetails() {
        if (productDetails == null) {
            productDetails = new ArrayList<ProductDetailsArray>();
        }
        return this.productDetails;
    }

}
