
package com.santai;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HeaderRequest" type="{http://www.example.org/ShipRate/}HeaderRequest" minOccurs="0"/>
 *         &lt;element name="searchOrderRequestInfo" type="{http://www.example.org/ShipRate/}searchOrderRequestInfoArray"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerRequest",
    "searchOrderRequestInfo"
})
@XmlRootElement(name = "searchOrderRequest")
public class SearchOrderRequest {

    @XmlElement(name = "HeaderRequest")
    protected HeaderRequest headerRequest;
    @XmlElement(required = true)
    protected SearchOrderRequestInfoArray searchOrderRequestInfo;

    /**
     * Gets the value of the headerRequest property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderRequest }
     *     
     */
    public HeaderRequest getHeaderRequest() {
        return headerRequest;
    }

    /**
     * Sets the value of the headerRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderRequest }
     *     
     */
    public void setHeaderRequest(HeaderRequest value) {
        this.headerRequest = value;
    }

    /**
     * Gets the value of the searchOrderRequestInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SearchOrderRequestInfoArray }
     *     
     */
    public SearchOrderRequestInfoArray getSearchOrderRequestInfo() {
        return searchOrderRequestInfo;
    }

    /**
     * Sets the value of the searchOrderRequestInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchOrderRequestInfoArray }
     *     
     */
    public void setSearchOrderRequestInfo(SearchOrderRequestInfoArray value) {
        this.searchOrderRequestInfo = value;
    }

}
