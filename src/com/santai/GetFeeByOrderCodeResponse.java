
package com.santai;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ask" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sysTime" type="{http://www.w3.org/2001/XMLSchema}time"/>
 *         &lt;element name="orderCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="baseFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="regFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dealFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insurance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chargebackTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chargebackWorkDay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subShipType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="waybillCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="discount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otherFee" type="{http://www.example.org/ShipRate/}otherFee" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ask",
    "sysTime",
    "orderCode",
    "msg",
    "baseFee",
    "regFee",
    "dealFee",
    "insurance",
    "totalFee",
    "currencyCode",
    "chargebackTime",
    "chargebackWorkDay",
    "shipTypeCode",
    "subShipType",
    "waybillCode",
    "discount",
    "otherFee"
})
@XmlRootElement(name = "getFeeByOrderCodeResponse")
public class GetFeeByOrderCodeResponse {

    @XmlElement(required = true)
    protected String ask;
    @XmlElement(required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar sysTime;
    @XmlElement(required = true)
    protected String orderCode;
    @XmlElement(required = true)
    protected String msg;
    protected String baseFee;
    protected String regFee;
    protected String dealFee;
    protected String insurance;
    protected String totalFee;
    protected String currencyCode;
    protected String chargebackTime;
    protected String chargebackWorkDay;
    protected String shipTypeCode;
    protected String subShipType;
    protected String waybillCode;
    protected String discount;
    protected List<OtherFee> otherFee;

    /**
     * Gets the value of the ask property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAsk() {
        return ask;
    }

    /**
     * Sets the value of the ask property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAsk(String value) {
        this.ask = value;
    }

    /**
     * Gets the value of the sysTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSysTime() {
        return sysTime;
    }

    /**
     * Sets the value of the sysTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSysTime(XMLGregorianCalendar value) {
        this.sysTime = value;
    }

    /**
     * Gets the value of the orderCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderCode() {
        return orderCode;
    }

    /**
     * Sets the value of the orderCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderCode(String value) {
        this.orderCode = value;
    }

    /**
     * Gets the value of the msg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Sets the value of the msg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsg(String value) {
        this.msg = value;
    }

    /**
     * Gets the value of the baseFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseFee() {
        return baseFee;
    }

    /**
     * Sets the value of the baseFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseFee(String value) {
        this.baseFee = value;
    }

    /**
     * Gets the value of the regFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegFee() {
        return regFee;
    }

    /**
     * Sets the value of the regFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegFee(String value) {
        this.regFee = value;
    }

    /**
     * Gets the value of the dealFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealFee() {
        return dealFee;
    }

    /**
     * Sets the value of the dealFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealFee(String value) {
        this.dealFee = value;
    }

    /**
     * Gets the value of the insurance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurance() {
        return insurance;
    }

    /**
     * Sets the value of the insurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurance(String value) {
        this.insurance = value;
    }

    /**
     * Gets the value of the totalFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalFee() {
        return totalFee;
    }

    /**
     * Sets the value of the totalFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalFee(String value) {
        this.totalFee = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the chargebackTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargebackTime() {
        return chargebackTime;
    }

    /**
     * Sets the value of the chargebackTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargebackTime(String value) {
        this.chargebackTime = value;
    }

    /**
     * Gets the value of the chargebackWorkDay property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargebackWorkDay() {
        return chargebackWorkDay;
    }

    /**
     * Sets the value of the chargebackWorkDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargebackWorkDay(String value) {
        this.chargebackWorkDay = value;
    }

    /**
     * Gets the value of the shipTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipTypeCode() {
        return shipTypeCode;
    }

    /**
     * Sets the value of the shipTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipTypeCode(String value) {
        this.shipTypeCode = value;
    }

    /**
     * Gets the value of the subShipType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubShipType() {
        return subShipType;
    }

    /**
     * Sets the value of the subShipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubShipType(String value) {
        this.subShipType = value;
    }

    /**
     * Gets the value of the waybillCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaybillCode() {
        return waybillCode;
    }

    /**
     * Sets the value of the waybillCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaybillCode(String value) {
        this.waybillCode = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscount(String value) {
        this.discount = value;
    }

    /**
     * Gets the value of the otherFee property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherFee property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherFee().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherFee }
     * 
     * 
     */
    public List<OtherFee> getOtherFee() {
        if (otherFee == null) {
            otherFee = new ArrayList<OtherFee>();
        }
        return this.otherFee;
    }

}
