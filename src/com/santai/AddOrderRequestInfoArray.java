package com.santai;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for addOrderRequestInfoArray complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addOrderRequestInfoArray">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customerOrderNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipperAddressType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="shipperName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipperEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipperAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipperPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipperZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipperCompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shippingMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientOrganization" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="goodsDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="goodsQuantity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="goodsDeclareWorth" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="goodsWeight" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="goodsLength" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="goodsWidth" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="goodsHeight" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="goodsDetails" type="{http://www.example.org/ShipRate/}goodsDetailsArray" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="orderStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ebayIdentify" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="printSortId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="areaCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notifyUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="evaluate" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="invNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxesNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isRemoteConfirm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isReturn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shippingWorth" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="smallLangAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addOrderRequestInfoArray", propOrder = { "customerOrderNo", "shipperAddressType", "shipperName",
		"shipperEmail", "shipperAddress", "shipperPhone", "shipperZipCode", "shipperCompanyName", "shippingMethod",
		"recipientCountry", "recipientName", "recipientState", "recipientCity", "recipientAddress", "recipientZipCode",
		"recipientOrganization", "recipientPhone", "recipientEmail", "goodsDescription", "goodsQuantity",
		"goodsDeclareWorth", "goodsWeight", "goodsLength", "goodsWidth", "goodsHeight", "goodsDetails", "orderStatus",
		"ebayIdentify", "printSortId", "areaCode", "notifyUrl", "evaluate", "invNo", "taxesNumber", "isRemoteConfirm",
		"isReturn", "shippingWorth", "smallLangAddress" })
public class AddOrderRequestInfoArray {
	
	protected String customerOrderNo;
	protected Integer shipperAddressType;
	protected String shipperName;
	protected String shipperEmail;
	protected String shipperAddress;
	protected String shipperPhone;
	protected String shipperZipCode;
	protected String shipperCompanyName;
	protected String shippingMethod;
	protected String recipientCountry;
	protected String recipientName;
	protected String recipientState;
	protected String recipientCity;
	protected String recipientAddress;
	protected String recipientZipCode;
	protected String recipientOrganization;
	protected String recipientPhone;
	protected String recipientEmail;
	protected String goodsDescription;
	protected Integer goodsQuantity;
	protected Float goodsDeclareWorth;
	protected Float goodsWeight;
	protected Float goodsLength;
	protected Float goodsWidth;
	protected Float goodsHeight;
	protected List<GoodsDetailsArray> goodsDetails;
	protected String orderStatus;
	protected String ebayIdentify;
	protected String printSortId;
	protected String areaCode;
	protected String notifyUrl;
	protected Float evaluate;
	protected String invNo;
	protected String taxesNumber;
	protected String isRemoteConfirm;
	protected String isReturn;
	protected Float shippingWorth;
	protected String smallLangAddress;
	
	/**
	 * Gets the value of the customerOrderNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerOrderNo() {
		return customerOrderNo;
	}
	
	/**
	 * Sets the value of the customerOrderNo property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setCustomerOrderNo(String value) {
		this.customerOrderNo = value;
	}
	
	/**
	 * Gets the value of the shipperAddressType property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getShipperAddressType() {
		return shipperAddressType;
	}
	
	/**
	 * Sets the value of the shipperAddressType property.
	 * 
	 * @param value allowed object is {@link Integer }
	 * 
	 */
	public void setShipperAddressType(Integer value) {
		this.shipperAddressType = value;
	}
	
	/**
	 * Gets the value of the shipperName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShipperName() {
		return shipperName;
	}
	
	/**
	 * Sets the value of the shipperName property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setShipperName(String value) {
		this.shipperName = value;
	}
	
	/**
	 * Gets the value of the shipperEmail property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShipperEmail() {
		return shipperEmail;
	}
	
	/**
	 * Sets the value of the shipperEmail property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setShipperEmail(String value) {
		this.shipperEmail = value;
	}
	
	/**
	 * Gets the value of the shipperAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShipperAddress() {
		return shipperAddress;
	}
	
	/**
	 * Sets the value of the shipperAddress property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setShipperAddress(String value) {
		this.shipperAddress = value;
	}
	
	/**
	 * Gets the value of the shipperPhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShipperPhone() {
		return shipperPhone;
	}
	
	/**
	 * Sets the value of the shipperPhone property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setShipperPhone(String value) {
		this.shipperPhone = value;
	}
	
	/**
	 * Gets the value of the shipperZipCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShipperZipCode() {
		return shipperZipCode;
	}
	
	/**
	 * Sets the value of the shipperZipCode property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setShipperZipCode(String value) {
		this.shipperZipCode = value;
	}
	
	/**
	 * Gets the value of the shipperCompanyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShipperCompanyName() {
		return shipperCompanyName;
	}
	
	/**
	 * Sets the value of the shipperCompanyName property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setShipperCompanyName(String value) {
		this.shipperCompanyName = value;
	}
	
	/**
	 * Gets the value of the shippingMethod property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getShippingMethod() {
		return shippingMethod;
	}
	
	/**
	 * Sets the value of the shippingMethod property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setShippingMethod(String value) {
		this.shippingMethod = value;
	}
	
	/**
	 * Gets the value of the recipientCountry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientCountry() {
		return recipientCountry;
	}
	
	/**
	 * Sets the value of the recipientCountry property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setRecipientCountry(String value) {
		this.recipientCountry = value;
	}
	
	/**
	 * Gets the value of the recipientName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientName() {
		return recipientName;
	}
	
	/**
	 * Sets the value of the recipientName property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setRecipientName(String value) {
		this.recipientName = value;
	}
	
	/**
	 * Gets the value of the recipientState property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientState() {
		return recipientState;
	}
	
	/**
	 * Sets the value of the recipientState property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setRecipientState(String value) {
		this.recipientState = value;
	}
	
	/**
	 * Gets the value of the recipientCity property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientCity() {
		return recipientCity;
	}
	
	/**
	 * Sets the value of the recipientCity property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setRecipientCity(String value) {
		this.recipientCity = value;
	}
	
	/**
	 * Gets the value of the recipientAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientAddress() {
		return recipientAddress;
	}
	
	/**
	 * Sets the value of the recipientAddress property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setRecipientAddress(String value) {
		this.recipientAddress = value;
	}
	
	/**
	 * Gets the value of the recipientZipCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientZipCode() {
		return recipientZipCode;
	}
	
	/**
	 * Sets the value of the recipientZipCode property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setRecipientZipCode(String value) {
		this.recipientZipCode = value;
	}
	
	/**
	 * Gets the value of the recipientOrganization property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientOrganization() {
		return recipientOrganization;
	}
	
	/**
	 * Sets the value of the recipientOrganization property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setRecipientOrganization(String value) {
		this.recipientOrganization = value;
	}
	
	/**
	 * Gets the value of the recipientPhone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientPhone() {
		return recipientPhone;
	}
	
	/**
	 * Sets the value of the recipientPhone property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setRecipientPhone(String value) {
		this.recipientPhone = value;
	}
	
	/**
	 * Gets the value of the recipientEmail property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRecipientEmail() {
		return recipientEmail;
	}
	
	/**
	 * Sets the value of the recipientEmail property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setRecipientEmail(String value) {
		this.recipientEmail = value;
	}
	
	/**
	 * Gets the value of the goodsDescription property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGoodsDescription() {
		return goodsDescription;
	}
	
	/**
	 * Sets the value of the goodsDescription property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setGoodsDescription(String value) {
		this.goodsDescription = value;
	}
	
	/**
	 * Gets the value of the goodsQuantity property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getGoodsQuantity() {
		return goodsQuantity;
	}
	
	/**
	 * Sets the value of the goodsQuantity property.
	 * 
	 * @param value allowed object is {@link Integer }
	 * 
	 */
	public void setGoodsQuantity(Integer value) {
		this.goodsQuantity = value;
	}
	
	/**
	 * Gets the value of the goodsDeclareWorth property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getGoodsDeclareWorth() {
		return goodsDeclareWorth;
	}
	
	/**
	 * Sets the value of the goodsDeclareWorth property.
	 * 
	 * @param value allowed object is {@link Float }
	 * 
	 */
	public void setGoodsDeclareWorth(Float value) {
		this.goodsDeclareWorth = value;
	}
	
	/**
	 * Gets the value of the goodsWeight property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getGoodsWeight() {
		return goodsWeight;
	}
	
	/**
	 * Sets the value of the goodsWeight property.
	 * 
	 * @param value allowed object is {@link Float }
	 * 
	 */
	public void setGoodsWeight(Float value) {
		this.goodsWeight = value;
	}
	
	/**
	 * Gets the value of the goodsLength property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getGoodsLength() {
		return goodsLength;
	}
	
	/**
	 * Sets the value of the goodsLength property.
	 * 
	 * @param value allowed object is {@link Float }
	 * 
	 */
	public void setGoodsLength(Float value) {
		this.goodsLength = value;
	}
	
	/**
	 * Gets the value of the goodsWidth property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getGoodsWidth() {
		return goodsWidth;
	}
	
	/**
	 * Sets the value of the goodsWidth property.
	 * 
	 * @param value allowed object is {@link Float }
	 * 
	 */
	public void setGoodsWidth(Float value) {
		this.goodsWidth = value;
	}
	
	/**
	 * Gets the value of the goodsHeight property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getGoodsHeight() {
		return goodsHeight;
	}
	
	/**
	 * Sets the value of the goodsHeight property.
	 * 
	 * @param value allowed object is {@link Float }
	 * 
	 */
	public void setGoodsHeight(Float value) {
		this.goodsHeight = value;
	}
	
	/**
	 * Gets the value of the goodsDetails property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
	 * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
	 * the goodsDetails property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getGoodsDetails().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link GoodsDetailsArray }
	 * 
	 * 
	 */
	public List<GoodsDetailsArray> getGoodsDetails() {
		if (goodsDetails == null) {
			goodsDetails = new ArrayList<GoodsDetailsArray>();
		}
		return this.goodsDetails;
	}
	
	/**
	 * Gets the value of the orderStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOrderStatus() {
		return orderStatus;
	}
	
	/**
	 * Sets the value of the orderStatus property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setOrderStatus(String value) {
		this.orderStatus = value;
	}
	
	/**
	 * Gets the value of the ebayIdentify property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEbayIdentify() {
		return ebayIdentify;
	}
	
	/**
	 * Sets the value of the ebayIdentify property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setEbayIdentify(String value) {
		this.ebayIdentify = value;
	}
	
	/**
	 * Gets the value of the printSortId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	
	public String getPrintSortId() {
		return printSortId;
	}
	
	public void setGoodsDetails(List<GoodsDetailsArray> goodsDetails) {
		this.goodsDetails = goodsDetails;
	}
	
	/**
	 * Sets the value of the printSortId property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setPrintSortId(String value) {
		this.printSortId = value;
	}
	
	/**
	 * Gets the value of the areaCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAreaCode() {
		return areaCode;
	}
	
	/**
	 * Sets the value of the areaCode property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setAreaCode(String value) {
		this.areaCode = value;
	}
	
	/**
	 * Gets the value of the notifyUrl property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNotifyUrl() {
		return notifyUrl;
	}
	
	/**
	 * Sets the value of the notifyUrl property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setNotifyUrl(String value) {
		this.notifyUrl = value;
	}
	
	/**
	 * Gets the value of the evaluate property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getEvaluate() {
		return evaluate;
	}
	
	/**
	 * Sets the value of the evaluate property.
	 * 
	 * @param value allowed object is {@link Float }
	 * 
	 */
	public void setEvaluate(Float value) {
		this.evaluate = value;
	}
	
	/**
	 * Gets the value of the invNo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInvNo() {
		return invNo;
	}
	
	/**
	 * Sets the value of the invNo property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setInvNo(String value) {
		this.invNo = value;
	}
	
	/**
	 * Gets the value of the taxesNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTaxesNumber() {
		return taxesNumber;
	}
	
	/**
	 * Sets the value of the taxesNumber property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setTaxesNumber(String value) {
		this.taxesNumber = value;
	}
	
	/**
	 * Gets the value of the isRemoteConfirm property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIsRemoteConfirm() {
		return isRemoteConfirm;
	}
	
	/**
	 * Sets the value of the isRemoteConfirm property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setIsRemoteConfirm(String value) {
		this.isRemoteConfirm = value;
	}
	
	/**
	 * Gets the value of the isReturn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIsReturn() {
		return isReturn;
	}
	
	/**
	 * Sets the value of the isReturn property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setIsReturn(String value) {
		this.isReturn = value;
	}
	
	/**
	 * Gets the value of the shippingWorth property.
	 * 
	 * @return possible object is {@link Float }
	 * 
	 */
	public Float getShippingWorth() {
		return shippingWorth;
	}
	
	/**
	 * Sets the value of the shippingWorth property.
	 * 
	 * @param value allowed object is {@link Float }
	 * 
	 */
	public void setShippingWorth(Float value) {
		this.shippingWorth = value;
	}
	
	/**
	 * Gets the value of the smallLangAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSmallLangAddress() {
		return smallLangAddress;
	}
	
	/**
	 * Sets the value of the smallLangAddress property.
	 * 
	 * @param value allowed object is {@link String }
	 * 
	 */
	public void setSmallLangAddress(String value) {
		this.smallLangAddress = value;
	}
	
}
