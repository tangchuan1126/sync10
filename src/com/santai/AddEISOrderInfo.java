
package com.santai;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addEISOrderInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addEISOrderInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ebayAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buyerFullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buyerPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buyerAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buyerAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buyerCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buyerState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buyerZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="labelSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EISOrderProducts" type="{http://www.example.org/ShipRate/}EISOrderProducts" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addEISOrderInfo", propOrder = {
    "ebayAccount",
    "refCode",
    "buyerFullName",
    "buyerPhoneNumber",
    "buyerAddress1",
    "buyerAddress2",
    "buyerCity",
    "buyerState",
    "buyerZipCode",
    "labelSize",
    "eisOrderProducts"
})
public class AddEISOrderInfo {

    @XmlElement(required = true)
    protected String ebayAccount;
    protected String refCode;
    protected String buyerFullName;
    protected String buyerPhoneNumber;
    protected String buyerAddress1;
    protected String buyerAddress2;
    protected String buyerCity;
    protected String buyerState;
    protected String buyerZipCode;
    protected Integer labelSize;
    @XmlElement(name = "EISOrderProducts")
    protected List<EISOrderProducts> eisOrderProducts;

    /**
     * Gets the value of the ebayAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEbayAccount() {
        return ebayAccount;
    }

    /**
     * Sets the value of the ebayAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEbayAccount(String value) {
        this.ebayAccount = value;
    }

    /**
     * Gets the value of the refCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefCode() {
        return refCode;
    }

    /**
     * Sets the value of the refCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefCode(String value) {
        this.refCode = value;
    }

    /**
     * Gets the value of the buyerFullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerFullName() {
        return buyerFullName;
    }

    /**
     * Sets the value of the buyerFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerFullName(String value) {
        this.buyerFullName = value;
    }

    /**
     * Gets the value of the buyerPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerPhoneNumber() {
        return buyerPhoneNumber;
    }

    /**
     * Sets the value of the buyerPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerPhoneNumber(String value) {
        this.buyerPhoneNumber = value;
    }

    /**
     * Gets the value of the buyerAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerAddress1() {
        return buyerAddress1;
    }

    /**
     * Sets the value of the buyerAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerAddress1(String value) {
        this.buyerAddress1 = value;
    }

    /**
     * Gets the value of the buyerAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerAddress2() {
        return buyerAddress2;
    }

    /**
     * Sets the value of the buyerAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerAddress2(String value) {
        this.buyerAddress2 = value;
    }

    /**
     * Gets the value of the buyerCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerCity() {
        return buyerCity;
    }

    /**
     * Sets the value of the buyerCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerCity(String value) {
        this.buyerCity = value;
    }

    /**
     * Gets the value of the buyerState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerState() {
        return buyerState;
    }

    /**
     * Sets the value of the buyerState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerState(String value) {
        this.buyerState = value;
    }

    /**
     * Gets the value of the buyerZipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerZipCode() {
        return buyerZipCode;
    }

    /**
     * Sets the value of the buyerZipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerZipCode(String value) {
        this.buyerZipCode = value;
    }

    /**
     * Gets the value of the labelSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLabelSize() {
        return labelSize;
    }

    /**
     * Sets the value of the labelSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLabelSize(Integer value) {
        this.labelSize = value;
    }

    /**
     * Gets the value of the eisOrderProducts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eisOrderProducts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEISOrderProducts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EISOrderProducts }
     * 
     * 
     */
    public List<EISOrderProducts> getEISOrderProducts() {
        if (eisOrderProducts == null) {
            eisOrderProducts = new ArrayList<EISOrderProducts>();
        }
        return this.eisOrderProducts;
    }

}
