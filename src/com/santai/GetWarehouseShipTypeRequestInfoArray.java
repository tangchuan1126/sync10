
package com.santai;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getWarehouseShipTypeRequestInfoArray complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getWarehouseShipTypeRequestInfoArray">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="countryEnName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="warehouseEnName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getWarehouseShipTypeRequestInfoArray", propOrder = {
    "countryEnName",
    "warehouseEnName"
})
public class GetWarehouseShipTypeRequestInfoArray {

    protected String countryEnName;
    protected String warehouseEnName;

    /**
     * Gets the value of the countryEnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryEnName() {
        return countryEnName;
    }

    /**
     * Sets the value of the countryEnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryEnName(String value) {
        this.countryEnName = value;
    }

    /**
     * Gets the value of the warehouseEnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarehouseEnName() {
        return warehouseEnName;
    }

    /**
     * Sets the value of the warehouseEnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarehouseEnName(String value) {
        this.warehouseEnName = value;
    }

}
