
package com.santai;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HeaderRequest" type="{http://www.example.org/ShipRate/}HeaderRequest"/>
 *         &lt;element name="updateOrderVolumeWeight" type="{http://www.example.org/ShipRate/}updateOrderVolumeWeight"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerRequest",
    "updateOrderVolumeWeight"
})
@XmlRootElement(name = "updateOrderVolumeWeightRequest")
public class UpdateOrderVolumeWeightRequest {

    @XmlElement(name = "HeaderRequest", required = true)
    protected HeaderRequest headerRequest;
    @XmlElement(required = true)
    protected UpdateOrderVolumeWeight updateOrderVolumeWeight;

    /**
     * Gets the value of the headerRequest property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderRequest }
     *     
     */
    public HeaderRequest getHeaderRequest() {
        return headerRequest;
    }

    /**
     * Sets the value of the headerRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderRequest }
     *     
     */
    public void setHeaderRequest(HeaderRequest value) {
        this.headerRequest = value;
    }

    /**
     * Gets the value of the updateOrderVolumeWeight property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateOrderVolumeWeight }
     *     
     */
    public UpdateOrderVolumeWeight getUpdateOrderVolumeWeight() {
        return updateOrderVolumeWeight;
    }

    /**
     * Sets the value of the updateOrderVolumeWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateOrderVolumeWeight }
     *     
     */
    public void setUpdateOrderVolumeWeight(UpdateOrderVolumeWeight value) {
        this.updateOrderVolumeWeight = value;
    }

}
