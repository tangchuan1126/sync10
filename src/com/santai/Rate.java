
package com.santai;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="totalfee" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="costfee" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="base_fee" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="dealfee" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="regfee" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="addons" type="{http://www.w3.org/2001/XMLSchema}float" />
 *       &lt;attribute name="deliverytime" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="isweight" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="iftracking" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="classtype" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="classtypecode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="shiptypecode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="shiptypename" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="shiptypecnname" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rate")
public class Rate {

    @XmlAttribute(name = "totalfee")
    protected Float totalfee;
    @XmlAttribute(name = "costfee")
    protected Float costfee;
    @XmlAttribute(name = "base_fee")
    protected Float baseFee;
    @XmlAttribute(name = "dealfee")
    protected Float dealfee;
    @XmlAttribute(name = "regfee")
    protected Float regfee;
    @XmlAttribute(name = "addons")
    protected Float addons;
    @XmlAttribute(name = "deliverytime")
    protected String deliverytime;
    @XmlAttribute(name = "isweight")
    protected String isweight;
    @XmlAttribute(name = "iftracking")
    protected String iftracking;
    @XmlAttribute(name = "classtype")
    protected String classtype;
    @XmlAttribute(name = "classtypecode")
    protected String classtypecode;
    @XmlAttribute(name = "shiptypecode")
    protected String shiptypecode;
    @XmlAttribute(name = "shiptypename")
    protected String shiptypename;
    @XmlAttribute(name = "shiptypecnname")
    protected String shiptypecnname;

    /**
     * Gets the value of the totalfee property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getTotalfee() {
        return totalfee;
    }

    /**
     * Sets the value of the totalfee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setTotalfee(Float value) {
        this.totalfee = value;
    }

    /**
     * Gets the value of the costfee property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getCostfee() {
        return costfee;
    }

    /**
     * Sets the value of the costfee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setCostfee(Float value) {
        this.costfee = value;
    }

    /**
     * Gets the value of the baseFee property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getBaseFee() {
        return baseFee;
    }

    /**
     * Sets the value of the baseFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setBaseFee(Float value) {
        this.baseFee = value;
    }

    /**
     * Gets the value of the dealfee property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getDealfee() {
        return dealfee;
    }

    /**
     * Sets the value of the dealfee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setDealfee(Float value) {
        this.dealfee = value;
    }

    /**
     * Gets the value of the regfee property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getRegfee() {
        return regfee;
    }

    /**
     * Sets the value of the regfee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setRegfee(Float value) {
        this.regfee = value;
    }

    /**
     * Gets the value of the addons property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getAddons() {
        return addons;
    }

    /**
     * Sets the value of the addons property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setAddons(Float value) {
        this.addons = value;
    }

    /**
     * Gets the value of the deliverytime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliverytime() {
        return deliverytime;
    }

    /**
     * Sets the value of the deliverytime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliverytime(String value) {
        this.deliverytime = value;
    }

    /**
     * Gets the value of the isweight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsweight() {
        return isweight;
    }

    /**
     * Sets the value of the isweight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsweight(String value) {
        this.isweight = value;
    }

    /**
     * Gets the value of the iftracking property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIftracking() {
        return iftracking;
    }

    /**
     * Sets the value of the iftracking property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIftracking(String value) {
        this.iftracking = value;
    }

    /**
     * Gets the value of the classtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasstype() {
        return classtype;
    }

    /**
     * Sets the value of the classtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasstype(String value) {
        this.classtype = value;
    }

    /**
     * Gets the value of the classtypecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasstypecode() {
        return classtypecode;
    }

    /**
     * Sets the value of the classtypecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasstypecode(String value) {
        this.classtypecode = value;
    }

    /**
     * Gets the value of the shiptypecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShiptypecode() {
        return shiptypecode;
    }

    /**
     * Sets the value of the shiptypecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShiptypecode(String value) {
        this.shiptypecode = value;
    }

    /**
     * Gets the value of the shiptypename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShiptypename() {
        return shiptypename;
    }

    /**
     * Sets the value of the shiptypename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShiptypename(String value) {
        this.shiptypename = value;
    }

    /**
     * Gets the value of the shiptypecnname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShiptypecnname() {
        return shiptypecnname;
    }

    /**
     * Sets the value of the shiptypecnname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShiptypecnname(String value) {
        this.shiptypecnname = value;
    }

}
