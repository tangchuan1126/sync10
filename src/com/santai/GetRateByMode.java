
package com.santai;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HeaderRequest" type="{http://www.example.org/ShipRate/}HeaderRequest" minOccurs="0"/>
 *         &lt;element name="ratesRequestInfo" type="{http://www.example.org/ShipRate/}getRateByModeRequestInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerRequest",
    "ratesRequestInfo"
})
@XmlRootElement(name = "getRateByMode")
public class GetRateByMode {

    @XmlElement(name = "HeaderRequest")
    protected HeaderRequest headerRequest;
    @XmlElement(required = true)
    protected GetRateByModeRequestInfo ratesRequestInfo;

    /**
     * Gets the value of the headerRequest property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderRequest }
     *     
     */
    public HeaderRequest getHeaderRequest() {
        return headerRequest;
    }

    /**
     * Sets the value of the headerRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderRequest }
     *     
     */
    public void setHeaderRequest(HeaderRequest value) {
        this.headerRequest = value;
    }

    /**
     * Gets the value of the ratesRequestInfo property.
     * 
     * @return
     *     possible object is
     *     {@link GetRateByModeRequestInfo }
     *     
     */
    public GetRateByModeRequestInfo getRatesRequestInfo() {
        return ratesRequestInfo;
    }

    /**
     * Sets the value of the ratesRequestInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetRateByModeRequestInfo }
     *     
     */
    public void setRatesRequestInfo(GetRateByModeRequestInfo value) {
        this.ratesRequestInfo = value;
    }

}
