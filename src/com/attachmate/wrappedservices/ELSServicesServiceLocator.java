/**
 * ELSServicesServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.attachmate.wrappedservices;

public class ELSServicesServiceLocator extends org.apache.axis.client.Service implements com.attachmate.wrappedservices.ELSServicesService {

    public ELSServicesServiceLocator() {
    }


    public ELSServicesServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ELSServicesServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ELSServicesCfc
//    private java.lang.String ELSServicesCfc_address = "http://www.endicia.com/ELS/ELSServices.cfc";
    private java.lang.String ELSServicesCfc_address = "https://www.endicia.com/ELS/ELSServices.cfc?wsdl";
    
    public java.lang.String getELSServicesCfcAddress() {
        return ELSServicesCfc_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ELSServicesCfcWSDDServiceName = "ELSServices.cfc";

    public java.lang.String getELSServicesCfcWSDDServiceName() {
        return ELSServicesCfcWSDDServiceName;
    }

    public void setELSServicesCfcWSDDServiceName(java.lang.String name) {
        ELSServicesCfcWSDDServiceName = name;
    }

    public com.attachmate.wrappedservices.ELSServices getELSServicesCfc() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ELSServicesCfc_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getELSServicesCfc(endpoint);
    }

    public com.attachmate.wrappedservices.ELSServices getELSServicesCfc(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.attachmate.wrappedservices.ELSServicesCfcSoapBindingStub _stub = new com.attachmate.wrappedservices.ELSServicesCfcSoapBindingStub(portAddress, this);
            _stub.setPortName(getELSServicesCfcWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setELSServicesCfcEndpointAddress(java.lang.String address) {
        ELSServicesCfc_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.attachmate.wrappedservices.ELSServices.class.isAssignableFrom(serviceEndpointInterface)) {
                com.attachmate.wrappedservices.ELSServicesCfcSoapBindingStub _stub = new com.attachmate.wrappedservices.ELSServicesCfcSoapBindingStub(new java.net.URL(ELSServicesCfc_address), this);
                _stub.setPortName(getELSServicesCfcWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ELSServices.cfc".equals(inputPortName)) {
            return getELSServicesCfc();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://els", "ELSServicesService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://els", "ELSServices.cfc"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ELSServicesCfc".equals(portName)) {
            setELSServicesCfcEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
