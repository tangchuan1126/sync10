package com.attachmate.wrappedservices;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

public class A {
	
	private ELSServicesCfcSoapBindingStub stub;

	public A() 
	{
		try 
		{
			ELSServicesServiceLocator locator = new ELSServicesServiceLocator();
			
			stub = (ELSServicesCfcSoapBindingStub) locator.getELSServicesCfc();
		}
		catch (ServiceException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Object getCopyMethod(String xml)
	{
		Object returnObject = null;
		try 
		{
			returnObject = stub.statusRequest(xml);
			
		}
		catch (CFCInvocationException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (RemoteException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnObject;
	}
	
	
	public static void main(String[] args) 
	{
		String XMLInput = "<AccountID>770373</AccountID>"
			 +"<Test>Y</Test>" 
			 +"<PassPhrase>Vvme91754</PassPhrase>" 
			 +"<FullStatus>Y</FullStatus>" 
			 +"<StatusList>"
			 	+"<PICNumber>9400115901018469167530</PICNumber>"
			 +"</StatusList>";
		
		A a = new A();
		
		Object cc = a.getCopyMethod(XMLInput);
		
		////system.out.println(cc.toString());
		
	}
}
