/**
 * ELSServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.attachmate.wrappedservices;

public interface ELSServices extends java.rmi.Remote {
    public java.lang.Object multiLocationCarrierPickup(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object carrierPickupChange(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object carrierPickupAvailability(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object refundRequest(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object carrierPickupCancel(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object pitneySignup(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object statusRequest(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object userSignup(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object SCANRequest(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object carrierPickupRequest(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
    public java.lang.Object getTransactionsListing(java.lang.Object XMLInput) throws java.rmi.RemoteException, com.attachmate.wrappedservices.CFCInvocationException;
}
