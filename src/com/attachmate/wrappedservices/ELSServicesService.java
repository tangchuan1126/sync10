/**
 * ELSServicesService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.attachmate.wrappedservices;

public interface ELSServicesService extends javax.xml.rpc.Service {
    public java.lang.String getELSServicesCfcAddress();

    public com.attachmate.wrappedservices.ELSServices getELSServicesCfc() throws javax.xml.rpc.ServiceException;

    public com.attachmate.wrappedservices.ELSServices getELSServicesCfc(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
