package com.vvme.order;

/**
 * 
 * @ClassName: FieldInfo
 * @Description: 
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class FieldInfo implements java.io.Serializable{
	private static final long serialVersionUID = -8294599035902532743L;
	private String name; //字段名称
	private String type; //字段类型
	private int maxLen; //字段最大长度
	private int numPs; //decimal的长度
	private int numSc; //精确长度
	private String columnType; //字段类型
	private String isNil;//是否允许为空 YES可以
	private String format; //格式
	private String orderColumnName; //对应订单表字段
	
	private int excelSn;
	private String xlsTitle;
	
	public FieldInfo(){
		
	}
	
	public int getMaxLen() {
		return maxLen;
	}
	public void setMaxLen(int maxLen) {
		this.maxLen = maxLen;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getNumPs() {
		return numPs;
	}
	public void setNumPs(int numPs) {
		this.numPs = numPs;
	}
	public int getNumSc() {
		return numSc;
	}
	public void setNumSc(int numSc) {
		this.numSc = numSc;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public int getExcelSn() {
		return excelSn;
	}

	public void setExcelSn(int excelSn) {
		this.excelSn = excelSn;
	}

	public String getXlsTitle() {
		return xlsTitle;
	}

	public void setXlsTitle(String xlsTitle) {
		this.xlsTitle = xlsTitle;
	}

	public String getIsNil() {
		return isNil;
	}

	public void setIsNil(String isNil) {
		this.isNil = isNil;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getOrderColumnName() {
		return orderColumnName;
	}

	public void setOrderColumnName(String orderColumnName) {
		this.orderColumnName = orderColumnName;
	}
	
}
