package com.vvme.order.service;


/**
 * 
 * @ClassName: IFileService
 * @Description: 文件服务
 * @author yetl
 * @date 2015年4月29日
 *
 */
public interface IFileService {
	
	/**
	 * 
	 * @param id
	 * @param fName
	 * @param expireSecs
	 * @throws Exception
	 */
	public void update(long id,String fName,String expireSecs) throws Exception;
}
