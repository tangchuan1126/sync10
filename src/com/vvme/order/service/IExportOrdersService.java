package com.vvme.order.service;

import com.cwc.db.DBRow;

public interface IExportOrdersService {
	/**
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public DBRow [] expOrder(DBRow param) throws Exception;
}
