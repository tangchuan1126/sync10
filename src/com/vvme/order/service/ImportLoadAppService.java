package com.vvme.order.service;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.axis.utils.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.cwc.app.floor.api.FloorAppointmentMgr;
import com.cwc.app.floor.api.FloorSyncLoadRelationMgr;
import com.cwc.app.iface.WMSOrderMgrIFace;
import com.cwc.app.key.WmsOrderUpdateTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.vvme.order.AppointmentObject;
import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelDataArray;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.SignManager;
import com.vvme.order.WmsOrderLinesTmp;
import com.vvme.order.dao.B2bOrderDao;
import com.vvme.order.dao.CarrierScacMcdotDao;
import com.vvme.order.dao.CountryCodeDao;
import com.vvme.order.dao.CountryProvinceDao;
import com.vvme.order.dao.CustomerBrandDao;
import com.vvme.order.dao.CustomerIdDao;
import com.vvme.order.dao.ProductStorageCatalogDao;
import com.vvme.order.dao.ShipToDao;
import com.vvme.order.dao.TmpOrderLinesDao;
import com.vvme.order.dic.EnumUtils;
import com.vvme.order.excel.IExcelProcess;
import com.vvme.order.exception.ExcelDataBaseException;
import com.vvme.order.exception.ImportTemplateInvalidateException;
import com.vvme.order.util.BeanToMapUtil;
import com.vvme.order.util.CompareUtils;
import com.vvme.order.util.ExcelUtils;
import com.vvme.order.validate.CarrierValidate;
import com.vvme.order.validate.ExcelDataBaseValidate;
import com.vvme.order.validate.ExcelSingleCustomerValidate;
import com.vvme.order.validate.ExcelTemplateValidate;
import com.vvme.order.validate.FreightTermsValidate;
import com.vvme.order.validate.FreightTypeValidate;
import com.vvme.order.validate.ValidateContext;

/**
 * 
 * @ClassName: ImportLoadAppService
 * @Description: 
 * @author yetl
 * @date 2015年4月29日
 *
 */
public class ImportLoadAppService implements IImportLoadAppService{
	private IExcelProcess excelProcess;
	private IImportOrdersService importOrdersService;
	private IExcelTableService excelTableService;
	
	private TmpOrderLinesDao tmpOrderLinesDao;
	private B2bOrderDao b2bOrderDao;
	private ProductStorageCatalogDao productStorageCatalogDao;
	private CountryCodeDao countryCodeDao;
	private CountryProvinceDao countryProvinceDao;
	private FloorAppointmentMgr floorAppointmentMgr;
	private WMSOrderMgrIFace proxyWMSOrderMgr;
	private FloorSyncLoadRelationMgr floorLoadMgr;
	private CarrierScacMcdotDao carrierScacMcdotDao;
	private ShipToDao shipToDao;
	private CustomerBrandDao customerBrandDao;
	private CustomerIdDao customerIdDao;
	
	public void setCustomerIdDao(CustomerIdDao customerIdDao) {
		this.customerIdDao = customerIdDao;
	}
	
	public void setCustomerBrandDao(CustomerBrandDao customerBrandDao) {
		this.customerBrandDao = customerBrandDao;
	}

	public void setShipToDao(ShipToDao shipToDao) {
		this.shipToDao = shipToDao;
	}

	public void setB2bOrderDao(B2bOrderDao b2bOrderDao) {
		this.b2bOrderDao = b2bOrderDao;
	}

	public void setProductStorageCatalogDao(
			ProductStorageCatalogDao productStorageCatalogDao) {
		this.productStorageCatalogDao = productStorageCatalogDao;
	}

	public void setCountryCodeDao(CountryCodeDao countryCodeDao) {
		this.countryCodeDao = countryCodeDao;
	}

	public void setCountryProvinceDao(CountryProvinceDao countryProvinceDao) {
		this.countryProvinceDao = countryProvinceDao;
	}

	public void setFloorAppointmentMgr(FloorAppointmentMgr floorAppointmentMgr) {
		this.floorAppointmentMgr = floorAppointmentMgr;
	}

	public void setProxyWMSOrderMgr(WMSOrderMgrIFace proxyWMSOrderMgr) {
		this.proxyWMSOrderMgr = proxyWMSOrderMgr;
	}

	public void setFloorLoadMgr(FloorSyncLoadRelationMgr floorLoadMgr) {
		this.floorLoadMgr = floorLoadMgr;
	}

	public void setCarrierScacMcdotDao(CarrierScacMcdotDao carrierScacMcdotDao) {
		this.carrierScacMcdotDao = carrierScacMcdotDao;
	}

	public void setExcelTableService(IExcelTableService excelTableService) {
		this.excelTableService = excelTableService;
	}

	public void setTmpOrderLinesDao(TmpOrderLinesDao tmpOrderLinesDao) {
		this.tmpOrderLinesDao = tmpOrderLinesDao;
	}

	public void setExcelProcess(IExcelProcess excelProcess) {
		this.excelProcess = excelProcess;
	}

	public void setImportOrdersService(IImportOrdersService importOrdersService) {
		this.importOrdersService = importOrdersService;
	}

	public void importOrdersToTemp(ProcessContext ctx) throws Exception {
		ValidateContext validateContext=new ValidateContext(new ExcelTemplateValidate());
		
		//导入前需要校验模板的都得校验
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ImportTemplateInvalidateException("The file format is not consistent with the template format");
			}
		}
		
		if(ctx.getExcelData()==null){
			ExcelDataArray excelData=this.excelProcess.importExcelFileToDataArray(ctx.getPath());
			ctx.setExcelData(excelData);
		}
		
		ExcelUtils.removeEmptyLine(ctx.getExcelData());
		if(ctx.getExcelData().getDataArray().length==0){
			throw new Exception("No import data file!");
		}
		ctx.getResp().setTotal(ctx.getExcelData().getDataArray().length);
		
		//基本类型校验
		validateContext=new ValidateContext(new ExcelDataBaseValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ExcelDataBaseException("Invalid information found, please download the report to review the errors.  <br/>  "
						+ "Cells with incorrect information will be highlighted in yellow, please move your cursor over for more");
			}
		}
		
		validateContext=new ValidateContext(new FreightTermsValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ExcelDataBaseException("Invalid information found, please download the report to review the errors.  <br/>  "
						+ "Cells with incorrect information will be highlighted in yellow, please move your cursor over for more");
			}
		}
		
		validateContext=new ValidateContext(new FreightTypeValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ExcelDataBaseException("Invalid information found, please download the report to review the errors.  <br/>  "
						+ "Cells with incorrect information will be highlighted in yellow, please move your cursor over for more");
			}
		}
		
		validateContext=new ValidateContext(new CarrierValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ExcelDataBaseException("Invalid information found, please download the report to review the errors.  <br/>  "
						+ "Cells with incorrect information will be highlighted in yellow, please move your cursor over for more");
			}
		}
		
		//是否导入的是同一个客户的单子
		validateContext=new ValidateContext(new ExcelSingleCustomerValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ExcelDataBaseException("Invalid information found, please download the report to review the errors.  <br/>  "
						+ "Cells with incorrect information will be highlighted in yellow, please move your cursor over for more");
			}
		}
		
		//导入临时表
		importOrdersService.insertExcelDataToTemp(ctx);
	}

	public void findSign4(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign4(cid, importer);
	}
	
	public void findSign5(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign5(cid, importer);
	}

	public void findSign9(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign9(cid, importer);
	}
	
	/**
	 * 提取标记11的数据处理
	 */
	public void findSign19(ProcessContext ctx) throws Exception {
		String cid=ctx.getCid();
		long importer=ctx.getUser().getAdid();
		
		DBRow [] datas=tmpOrderLinesDao.findCommonDn(cid, importer);
		int [] dataRows=ctx.getExcelData().getDataRows();
		String [][] data=ctx.getExcelData().getDataArray();
		DBRow para=new DBRow();
		
		for(DBRow row:datas){
			String dn=row.getString("dn");
			
			para.put("customer_id", cid);
			para.put("dn", dn);
			para.put("importer", importer);
			
			//校验每条记录
			DBRow[] lines=tmpOrderLinesDao.find(para);

			for(DBRow line:lines){
				DBRow[] dB2b=b2bOrderDao.findB2bLines(cid,dn,line.getString("model_number"),line.getString("material_number"));
				
				if(dB2b.length>2)
					throw new Exception("Exception:cid:"+cid+" dn:"+dn+"查出多条记录或者相同SKU存储多次!");
				
				if(dB2b.length<=0)
					throw new Exception("Exception:"+line.getString("model_number")+" "+line.getString("material_number"));
				
				Map<String, Object> beanMap=DBRowUtils.dbRowAsMap(line);
				WmsOrderLinesTmp source=(WmsOrderLinesTmp)BeanToMapUtil.convertMap(WmsOrderLinesTmp.class, beanMap);
				DBRow dTmp=dB2b[0];
				WmsOrderLinesTmp target=new WmsOrderLinesTmp();
				
				int ps_id=0;
				String companyId=dTmp.getString("company_id");
				target.setDelivery_plant(companyId);
				ps_id=Integer.parseInt(dTmp.getString("send_psid"));
				target.setDn(dTmp.getString("customer_dn"));
				target.setRetail_po(dTmp.getString("retail_po"));
				target.setOrder_number(dTmp.getString("order_number"));
				
				Date order_date=(Date)dTmp.getValue("customer_order_date");
				if(order_date!=null){
					target.setOrder_date(new Timestamp(DateUtils.parseDate(DateUtil.showLocationTime(order_date, ps_id).substring(0,10),new String[]{"yyyy-MM-dd"}).getTime()));
				}
				
				target.setShip_to_party_name(dTmp.getString("ship_to_party_name"));
				target.setShip_to_address(dTmp.getString("deliver_house_number"));
				target.setShip_to_city(dTmp.getString("deliver_city"));
				target.setShip_to_state(dTmp.getString("address_state_deliver"));
				target.setShip_to_zip_code(dTmp.getString("deliver_zip_code"));
				
				String ft=dTmp.getString("freight_term");
//				String billId=dTmp.getString("bill_to_id");
				
				String sft=source.getFt();
				source.setFt(EnumUtils.transFreightType(sft).toUpperCase());
				//Prepaid打算调整到3rd party
				if(ft.equalsIgnoreCase("Prepaid") && EnumUtils.isThirdParty(source.getFt())){
					//设置相同值比较直接通过
					target.setFt(source.getFt());
					
					String l_bill=source.getBill_to();
					if(!StringUtils.isEmpty(l_bill)){//为空默认账单地址寄给Customer
						para.clear();
						para.put("customer_id", l_bill);
						DBRow[] cbRow=customerIdDao.find(para);
						
						if(cbRow!=null && cbRow.length>0){
							String ckey=cbRow[0].getString("customer_key");
							
							para=new DBRow();
							para.put("storage_type", 7);
							para.put("storage_type_id", ckey);
							
							DBRow [] rows=productStorageCatalogDao.find(para);
							if(rows!=null && rows.length>0){
							}else if(rows!=null && rows.length>1){
								tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_60);
								continue;
							}else{
								tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_27);
								continue;
							}
						}else{
							para.clear();
							para.put("ship_to_name", l_bill);
							
							DBRow [] $1=shipToDao.find(para,new String[]{"ship_to_id"});
								
							if($1!=null && $1.length>0){
								para.clear();
								para.put("storage_type_id", $1[0].getString("ship_to_id"));
								para.put("storage_type", 5);
								para.put("is_bill_dc", 1);
								
								$1=productStorageCatalogDao.find(para,new String[]{"id"});
								if($1==null || $1.length==0){
									tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_27);
									continue;
								}else if($1!=null && $1.length>1){
									tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_60);
									continue;
								}else{ //可能是carrier
									para.clear();
									para.put("carrier", l_bill);
									$1=carrierScacMcdotDao.find(para,new String[]{"id"});
									
									if($1!=null && $1.length>0){
										para.clear();
										para.put("storage_type", 8);
										para.put("storage_type_id", $1[0].getString("id"));
										
										$1=productStorageCatalogDao.find(para,new String[]{"id"});
										if($1==null || $1.length==0){
											tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_27);
											continue;
										}else if($1!=null && $1.length>1){
											tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_60);
											continue;
										}
									}else{
										target.setBill_to(source.getBill_to());
									}
								}
							}
						}
					}
				}else{ //其他情况正常比较
					target.setFt(ft);
					target.setBill_to(dTmp.getString("bill_to_name"));
				}
				
				String freightType=dTmp.getString("freight_carrier");
				if(!StringUtils.isEmpty(freightType)){
					target.setFreight_type(freightType);
				}else{
					target.setFreight_type(source.getFreight_type());
				}
				
				String ccid=dTmp.getString("deliver_ccid");
				DBRow countryRow=this.countryCodeDao.findCountry(Integer.parseInt(ccid));
				
				target.setCountry(countryRow.getString("c_country"));
				target.setMabd(DateUtils.parseDate(DateUtil.showLocationTime((Date)dTmp.getValue("mabd"), ps_id).substring(0,10),new String[]{"yyyy-MM-dd"}));
				Date request_date=(Date)dTmp.getValue("requested_date");
				if(request_date!=null)
					target.setReq_ship_date(new Timestamp(DateUtils.parseDate(DateUtil.showLocationTime(request_date, ps_id).substring(0,10),new String[]{"yyyy-MM-dd"}).getTime()));
				Date ship_not_before=(Date)dTmp.getValue("ship_not_before");
				if(ship_not_before!=null)
					target.setShip_not_before(new Timestamp(DateUtils.parseDate(DateUtil.showLocationTime(ship_not_before, ps_id).substring(0,10),new String[]{"yyyy-MM-dd"}).getTime()));
				Date ship_not_later=(Date)dTmp.getValue("ship_not_later");
				if(ship_not_later!=null)
					target.setShip_not_later(new Timestamp(DateUtils.parseDate(DateUtil.showLocationTime(ship_not_later, ps_id).substring(0,10),new String[]{"yyyy-MM-dd"}).getTime()));
				
				Date deliveryed_date=(Date)dTmp.getValue("deliveryed_date");
				if(deliveryed_date!=null)
					target.setShip_date(DateUtils.parseDate(DateUtil.showLocationTime((Date)dTmp.getValue("deliveryed_date"), ps_id).substring(0,10),new String[]{"yyyy-MM-dd"}));
				target.setBol_tracking(dTmp.getString("bol_tracking"));
				
				//明细部分
				target.setMaterial_number(dTmp.getString("lot_number"));
				target.setModel_number(dTmp.getString("item_id"));
				target.setOrder_qty((int)Float.parseFloat(dTmp.getString("b2b_count")));
				target.setWeight(new BigDecimal(dTmp.getString("total_weight")));
				target.setBoxes(Integer.parseInt(dTmp.getString("boxes")));
				target.setPallet_spaces(new BigDecimal(dTmp.getString("pallet_spaces")));
				
				Set<String> fs=CompareUtils.compare(source, target);
				
				if(null!=fs && !fs.isEmpty()){ //有差异的line
					Map<?, ?> targetMap=BeanToMapUtil.convertBean(target);
					ExcelRowResult rst=new ExcelRowResult();
					rst.setRow(Integer.parseInt(line.getString("row")));
					rst.setSign(ExcelConstants.ERROR_TYPE_21);
					rst.setMsg(ExcelConstants.ERROR_MSG_21);
					rst.setDn(dn);
					int index=ExcelUtils.getIndex(dataRows, rst.getRow());
					rst.setData(data[index]);
					for(String key:fs){
						FieldInfo info=excelTableService.getLoadFieldInfoByFieldName(key, ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
						if(StringUtils.isEmpty(String.valueOf(targetMap.get(key))))
							rst.addCellResult(rst.createCellResult(info.getExcelSn(), "System value is blank!"));
						else{
							Object value=targetMap.get(key);
							
							if(value instanceof Date || value instanceof Timestamp){
								String format=info.getFormat();
								SimpleDateFormat sdf=new SimpleDateFormat(format);
								rst.addCellResult(rst.createCellResult(info.getExcelSn(), "System value:"+sdf.format(targetMap.get(key))));
							}else{
								rst.addCellResult(rst.createCellResult(info.getExcelSn(), "System value:"+targetMap.get(key)));
							}
						}
					}
					
					ctx.addExcelRowResult(rst);
					tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_21);
				}else{ //完全一样的line
					tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_19);
				}
			}
		}

	}
	
	public void findSign26(String cid, long importer) throws Exception {
		DBRow [] datas=tmpOrderLinesDao.findSign26(cid, importer);
		List<ExcelRowResult> list=new ArrayList<ExcelRowResult>();
		FieldInfo info=excelTableService.getLoadFieldInfoByFieldName("pickup_appointment_date", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		FieldInfo apptimeInfo=excelTableService.getLoadFieldInfoByFieldName("pickup_appointment_time", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		FieldInfo infoDP=excelTableService.getLoadFieldInfoByFieldName("delivery_plant", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		FieldInfo infoRSD=excelTableService.getLoadFieldInfoByFieldName("req_ship_date", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		FieldInfo infoCarr=excelTableService.getLoadFieldInfoByFieldName("carrier", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		
		Map<String,Set<String>> hubs=new HashMap<String,Set<String>>();
		Map<String,Set<String>> carriers=new HashMap<String,Set<String>>();
		Map<String,Set<String>> reqShipDates=new HashMap<String,Set<String>>();
		Map<String,Set<String>> pickupApp=new HashMap<String,Set<String>>();
		Map<String,Set<String>> pickupApptime=new HashMap<String,Set<String>>();
		
		for(DBRow data:datas){
			String dn=data.getString("dn");
//			int row=(Integer)data.getValue("row");
			String hub=data.getString("delivery_plant");
			String carrier=data.getString("carrier");
			String req_ship_date=data.getString("req_ship_date");
			String loadno=data.getString("loadno");
			String app=data.getString("pickup_appointment_date");
			String apptime=data.getString("pickup_appointment_time");
			
			if(!hubs.containsKey(loadno)){
				Set<String> set=new HashSet<String>();
				set.add(hub);
				hubs.put(loadno, set);
			}else
				hubs.get(loadno).add(hub);
			
			if(!carriers.containsKey(loadno)){
				Set<String> set=new HashSet<String>();
				set.add(carrier);
				carriers.put(loadno, set);
			}else
				carriers.get(loadno).add(carrier);
			
			if(!reqShipDates.containsKey(loadno)){
				Set<String> set=new HashSet<String>();
				set.add(req_ship_date);
				reqShipDates.put(loadno, set);
			}else
				reqShipDates.get(loadno).add(req_ship_date);
			
			if(!pickupApp.containsKey(loadno)){
				Set<String> set=new HashSet<String>();
				set.add(app);
				pickupApp.put(loadno, set);
			}else
				pickupApp.get(loadno).add(app);
			
			if(!pickupApptime.containsKey(loadno)){
				Set<String> set=new HashSet<String>();
				set.add(apptime);
				pickupApptime.put(loadno, set);
			}else
				pickupApptime.get(loadno).add(apptime);
			
			tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_26);
		}
		
		for(DBRow data:datas){
			String dn=data.getString("dn");
			int row=(Integer)data.getValue("row");
			String loadno=data.getString("loadno");
			
			ExcelRowResult bean=new ExcelRowResult();
			bean.setSign(ExcelConstants.ERROR_TYPE_26);
			bean.setRow(row);
			bean.setMsg(ExcelConstants.ERROR_MAP.get(ExcelConstants.ERROR_TYPE_26));
			bean.setDn(dn);
			
			if(hubs.get(loadno).size()>1){
				bean.addCellResult(bean.createCellResult(infoDP.getExcelSn(), ExcelConstants.formatMessage(ExcelConstants.TMP_ERR_TYPE_26, infoDP.getXlsTitle())));
			}
			
			if(carriers.get(loadno).size()>1){
				bean.addCellResult(bean.createCellResult(infoCarr.getExcelSn(), ExcelConstants.formatMessage(ExcelConstants.TMP_ERR_TYPE_26, infoCarr.getXlsTitle())));
			}
			
			if(reqShipDates.get(loadno).size()>1){
				bean.addCellResult(bean.createCellResult(infoRSD.getExcelSn(), ExcelConstants.formatMessage(ExcelConstants.TMP_ERR_TYPE_26, infoRSD.getXlsTitle())));
			}
			
			if(pickupApp.get(loadno).size()>1){
				bean.addCellResult(bean.createCellResult(info.getExcelSn(), ExcelConstants.formatMessage(ExcelConstants.TMP_ERR_TYPE_26, info.getXlsTitle())));
			}
			
			if(pickupApptime.get(loadno).size()>1){
				bean.addCellResult(bean.createCellResult(apptimeInfo.getExcelSn(), ExcelConstants.formatMessage(ExcelConstants.TMP_ERR_TYPE_26, apptimeInfo.getXlsTitle())));
			}
			
			list.add(bean);
		}
	}
	
	public void findSign31(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign31(cid, importer);
	}
	
	public void findSign32(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign32(cid, importer);
	}

	public void findSign37(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign37(cid, importer);
	}
	
	public void findSign38(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign38(cid, importer);
	}
	
	public void findSign40(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign40(cid, importer);
	}
	
	public void findSign45(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign45(cid, importer);
	}
	
	public void findSign47(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign47(cid, importer);
	}
	
	public void findSign50(ProcessContext ctx) throws Exception {
		String cid=ctx.getCid();
		long importer=ctx.getUser().getAdid();
		
		DBRow [] datas=tmpOrderLinesDao.findSign2(cid,importer);
		Collection<FieldInfo> flist=(Collection<FieldInfo>) excelTableService.getImportTemplateInfoByMid(ctx.getImpId()).values();
		
		for(DBRow row:datas){
			String dn=row.getString("dn");
			
			ArrayList<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
			DBRow [] rows=tmpOrderLinesDao.findLoadAppByDn(dn,cid,importer);
			
			for(DBRow r:rows){
				list.add(DBRowUtils.dbRowAsMap(r));
			}
			
			//得到比较结果
			Set<String> set=CompareUtils.compareByMap(list);
			if(set.isEmpty()){
			}else{
				tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_50);
				for(int i=0;i<rows.length;i++){
					ExcelRowResult excelRst=new ExcelRowResult();
					excelRst.setDn(dn);
					excelRst.setSign(ExcelConstants.ERROR_TYPE_50);
					excelRst.setRow(Integer.parseInt(rows[i].getString("row")));
					excelRst.setMsg(ExcelConstants.ERROR_MAP.get(ExcelConstants.ERROR_TYPE_50));
					
					for(String key:set){
						for(FieldInfo info:flist){
							if(info.getName().equals(key)){
								excelRst.addCellResult(excelRst.createCellResult(info.getExcelSn(),ExcelConstants.formatMessage(ExcelConstants.TMP_ERR_TYPE_DN,info.getXlsTitle())));
							}
						}
					}
					
					ctx.addExcelRowResult(excelRst);
				}
			}
		}
		
	}
	
	public void findSign51(ProcessContext ctx) throws Exception {
		String cid=ctx.getCid();
		long importer=ctx.getUser().getAdid();
		DBRow [] datas=tmpOrderLinesDao.findSign2(cid,importer);
		Collection<FieldInfo> flist=(Collection<FieldInfo>) excelTableService.getImportTemplateInfoByMid(ctx.getImpId()).values();
		
		List<ExcelRowResult> excelResults=new ArrayList<ExcelRowResult>();
		
		for(DBRow row:datas){
			String dn=row.getString("dn");
			
			ArrayList<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
			DBRow [] rows=tmpOrderLinesDao.findFreightTermsBillByDn(dn,cid,importer);
			
			for(DBRow r:rows){
				list.add(DBRowUtils.dbRowAsMap(r));
			}
			
			//得到比较结果
			Set<String> set=CompareUtils.compareByMap(list);
			if(set.isEmpty()){
			}else{
				tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_51);
				for(int i=0;i<rows.length;i++){
					ExcelRowResult excelRst=new ExcelRowResult();
					excelRst.setDn(dn);
					excelRst.setSign(ExcelConstants.ERROR_TYPE_51);
					excelRst.setRow(Integer.parseInt(rows[i].getString("row")));
					excelRst.setMsg(ExcelConstants.ERROR_MAP.get(ExcelConstants.ERROR_TYPE_51));
					
					for(String key:set){
						for(FieldInfo info:flist){
							if(info.getName().equals(key)){
								excelRst.addCellResult(excelRst.createCellResult(info.getExcelSn(),ExcelConstants.formatMessage(ExcelConstants.TMP_ERR_TYPE_DN,info.getXlsTitle())));
							}
						}
					}
					
					excelResults.add(excelRst);
				}
			}
		}
	}

	public void findSign53(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign53(cid, importer);
	}
	
	@SuppressWarnings("rawtypes")
	public int getApptNum( Map map,String k){
		int i=0;
		Iterator it=map.keySet().iterator();
		while(it.hasNext()){
			String key=(String)it.next();
			if(key.indexOf(k)>=0)
				i++;
		}
		return i;
	}
	
	/**
	 * 标记可以更新load的记录
	 * @param ctx
	 * @throws Exception
	 */
	public synchronized Object[] findLoad(ProcessContext ctx) throws Exception{
		//查询其它字段校验都没问题的数据进行loadno appointment比较
		String cid=ctx.getCid();
		long importer=ctx.getUser().getAdid();
		Set<Integer> errSet=new HashSet<Integer>();
		errSet.add(ExcelConstants.ERROR_TYPE_19); //查找出没有被改动的dn 对没有改动的dn做loadno appt的创建
		DBRow [] rows=tmpOrderLinesDao.findLoadApp(cid, importer,errSet);
		
		FieldInfo infoLoadno=excelTableService.getLoadFieldInfoByFieldName("loadno", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		FieldInfo infoApp=excelTableService.getLoadFieldInfoByFieldName("pickup_appointment_date", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		FieldInfo infoApptime=excelTableService.getLoadFieldInfoByFieldName("pickup_appointment_time", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		
		//==============对应导入excel里的所有记录做数量统计======================================
		//key:仓库ID+约车时间   value: load 此集合按value的数量来统计约车数量
		Map<String,Set<String>> appMap=new HashMap<String,Set<String>>();
		//key:仓库ID+约车时间   value: order 此集合按value的数量来统计约车数量
		Map<String,Set<String>> oappMap=new HashMap<String,Set<String>>();
		
		//LTL consol的同一时间的各种单子都上 一个车 此集合按key来统计约车数量
		//key:仓库ID+约车时间+LTL/CONSOL   value:load 或者 order 的对象 
		Map<String,Set<AppointmentObject>> canAppMap=new LinkedHashMap<String,Set<AppointmentObject>>();
		
		//LTL consol加入已经存在的appt里
		Map<String,Set<AppointmentObject>> exsitAppMap=new LinkedHashMap<String,Set<AppointmentObject>>();
		
		//需要删除order和app的关系的订单号
		Set<String> delOrderAppSet=new HashSet<String>();
		
		Object [] objs=new Object[]{canAppMap,exsitAppMap,delOrderAppSet};
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		for(DBRow row:rows){
			String dn=row.getString("dn");
			String loadno=row.getString("loadno");
			String freightType=row.getString("freight_type");
			String carrier=row.getString("carrier");
			Date appDate=(Date)row.getValue("pickup_appointment_date");
			Time apptimeDate=(Time)row.getValue("pickup_appointment_time");
			
			String appointment="";
			
			if(appDate!=null && apptimeDate!=null){
				//当pickup_appointment_date pickup_appointment_time都有值的时候才表示客户想要创建约车信息
				String date=DateUtil.FormatDatetime("yyyy-MM-dd", appDate);
				String time=DateUtil.FormatDatetime("HH:mm", apptimeDate);
				appointment=date+" "+time;
			}
			
			DBRow para=new DBRow();
			para.put("customer_dn", dn);
			para.put("customer_id", cid);
			
			DBRow [] datas=this.b2bOrderDao.find(para, new String[]{"b2b_oid","send_psid","carriers","delivery_appointment"});
			
			if(datas.length>1)
				throw new Exception("Exception:customer_id:"+cid+",customer_dn:"+dn+"查出多个order");
			
			if(datas.length<=0)
				throw new Exception("Exception:customer_id:"+cid+",customer_dn:"+dn+"查不到对应的order");
			
			String b2b_oid=datas[0].getString("b2b_oid");
			String sendPsid=datas[0].getString("send_psid");
			String carriers=datas[0].getString("carriers");
			String delivery_appointment=datas[0].getString("delivery_appointment");
			
			if(!StringUtils.isEmpty(appointment)){
				//时区转换
				appointment=DateUtil.showUTCTime(format.parse(appointment), Long.parseLong(sendPsid));
			}
			
			//比较loadno关系

			@SuppressWarnings("rawtypes")

			List rst=this.floorLoadMgr.diffLoad(cid, loadno, b2b_oid,carriers);
			
			int type=(int)rst.get(0);
			String old_loadno=(String)rst.get(1);
			
			para=new DBRow();
			para.put("customer_id", cid);
			para.put("importer", importer);
			para.put("dn", dn);
				
			if(type==1){ //老关系是空，需要添加新关系
				//给了约车时间说明要按load约车 校验 load下的order是否已经约过车 如果约过要去掉order的约车信息改为按Load约车
				if(!StringUtils.isEmpty(appointment)){
					//有约车信息
					//判断order是否已经约过车

					@SuppressWarnings("rawtypes")

					List appList=floorAppointmentMgr.diffAppointment(b2b_oid, appointment,"order");
					int appType=(int)appList.get(0);
					String oldapp=(String)appList.get(1);
					
					//此load下的order已经约过车了，并且时间发生变化
					if(appType==2 || appType==3){ //约车时间有变化
						datas=tmpOrderLinesDao.find(para,new String[]{"row"});
						
						for(DBRow r:datas){
							ExcelRowResult err=new ExcelRowResult();
							err.setDn(dn);
							err.setSign(ExcelConstants.ERROR_TYPE_23);
							err.setMsg(ExcelConstants.ERROR_MSG_23);
							String oldappLocal=DateUtil.showLocalTime(oldapp, Long.parseLong(sendPsid));
							err.addCellResult(err.createCellResult(infoApp.getExcelSn(), "pickup appointment:"+oldappLocal));
							err.addCellResult(err.createCellResult(infoApptime.getExcelSn(), "pickup appointment:"+oldappLocal));
							err.setRow(Integer.parseInt(r.getString("row")));
							ctx.addExcelRowResult(err);
						}
						
						//load下的order已经约过车,改load约车但是时间已经变了
						tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_23);
						continue;
					}
					
					//此load对应的order没有约车或者约车时间没有发生变化 允许按变更load约车
					if(appType==1 || appType==4){
						//计算约车上限
						DBRow appRow=this.floorAppointmentMgr.getAppointWork(appointment,sendPsid);
						if(appRow==null || (Integer)appRow.getValue("limit_")==0){
							tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_29);
							continue;
						}
						
						int limit=Integer.parseInt(appRow.getString("limit_")); //outbound的约车上限
						//已经outbound的数量    每次查询appt时间 尽量做到后去即时的数量
						int cnt=Integer.parseInt(appRow.getString("work")); 
						
						//如果是LTL consol 按仓库 约车时间 运输方式 合并到一辆车
						if(EnumUtils.isLTLOrConsol(freightType)){
							//此key用来做LTL consol合并一辆车 
							String key=sendPsid+"|"+appointment+"|"+freightType;
							//统计数量加上这个appt点的其他数量
							String key_1=sendPsid+"|"+appointment+"|";
							
							AppointmentObject obj=new AppointmentObject();
							obj.setId(loadno);
							obj.setType(1); //按load约车
							obj.setCarrier(carrier);
							obj.setNeedCreate(1); //load appoint都得创建
							obj.getOrderList().add(b2b_oid);
							
							//查询此appt点下的仓库对应的LTL 或者 Consol是否存在的约车
							DBRow dbrow=floorAppointmentMgr.findWorkByAppt(appointment, Long.parseLong(sendPsid), carrier,freightType);
							
							//如果已经已经有了可以共用一个约车信息 不用统计约车数量 排除本身是此订单
							if(dbrow!=null){
								DBRow $2=floorAppointmentMgr.findWorkByApptOid(appointment, Long.parseLong(sendPsid), carrier,freightType,b2b_oid);
								
								if($2!=null)
									delOrderAppSet.add(b2b_oid);
								
								if(exsitAppMap.containsKey(key)){ //已经存在这个仓库这个点的约车,直接合并进去
									exsitAppMap.get(key).add(obj);
								}else{ //
									Set<AppointmentObject> tmpSet=new HashSet<AppointmentObject>();
									tmpSet.add(obj);
									exsitAppMap.put(key,tmpSet);
								}
								continue;
							}else{ //统计数量 实质是+1
								if(canAppMap.containsKey(key)){ //已经存在这个仓库这个点的约车,直接合并进去
									canAppMap.get(key).add(obj);
								}else{
									if(appType==1){ //没约过的新的约车单子统计数量 appt不会为空 
										Set<AppointmentObject> tmpSet=new HashSet<AppointmentObject>();
										tmpSet.add(obj);
										canAppMap.put(key,tmpSet);
										//记数 LTL consol的按key来统计约车数量 + 其他类型的看时间统计数量
										Set<String> $1=appMap.get(key_1);
										if($1!=null)
											cnt+=canAppMap.keySet().size()+$1.size();
										else
											cnt+=canAppMap.keySet().size();
									}else{ //由于appt不会空说明appt已经存在 为4的没有变化的不需要统计 应为work里里面有了它的记数
										delOrderAppSet.add(b2b_oid);
									}
								}
								if(cnt>limit){
									tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_28);
								}
								continue;
							}
						}else{ //其他模式都是一个load 或者 order 一个appointment
							String key=sendPsid+"|"+appointment+"|";//+Laden;
						
							//统计上限时要把order约车的减去在统计 order换load约车
							if(appType==4){ //已经按order约车
								if(!oappMap.containsKey(key)){
									Set<String> set=new HashSet<String>();
									set.add(b2b_oid);
									oappMap.put(key, set);
								}else{
									Set<String> set=oappMap.get(key);
									set.add(b2b_oid);
								}
								cnt-=oappMap.get(key).size();
							}
						
							if(!appMap.containsKey(key)){
								Set<String> set=new HashSet<String>();
								set.add(loadno);
								appMap.put(key, set);
								cnt+=appMap.get(key).size()+getApptNum(canAppMap,key);
							}else{
								Set<String> set=appMap.get(key);
								if(!set.contains(loadno)){ //相同的app下的新loadno
									set.add(loadno);
									cnt+=appMap.get(key).size()+getApptNum(canAppMap,key);
								}
							}
						}
						
						//小于限制数量
						if(cnt<=limit){
							if(appType==4){ //order已经约车要删除order约车信息和关联信息,在改load约车
								tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_42);
							}else{
								tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_15);
							}
						}else{
							tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_28);
						}
						
					}else{
						//更新load
						if(appType==4){ //order已经约车要删除order约车信息和关联信息,在改load约车
							tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_42);
						}else{
							tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_15);
						}
					}
					
				}else{
					//直接更新loadno
					tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_15);
				}
				
			}else if(type==2 || type==3){ //新关系空，存在老关系  || 新-老关系不同
				datas=tmpOrderLinesDao.find(para,new String[]{"row"});
				
				for(DBRow r:datas){
					ExcelRowResult err=new ExcelRowResult();
					err.setDn(dn);
					err.setSign(ExcelConstants.ERROR_TYPE_22);
					err.setMsg(ExcelConstants.ERROR_MSG_22);
					err.addCellResult(err.createCellResult(infoLoadno.getExcelSn(), "loadNo:"+old_loadno));
					err.setRow(Integer.parseInt(r.getString("row")));
					ctx.addExcelRowResult(err);
				}

				tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_22);
			}else if(type==4){ //loadno没有变化
				String load_id=floorLoadMgr.getLoadIdByLoadNo(loadno).getString("load_id");
				
				//校验appointment

				@SuppressWarnings("rawtypes")

				List appList=floorAppointmentMgr.diffAppointment(load_id, appointment,"load");
				int appType=(int)appList.get(0);
				String oldapp=(String)appList.get(1);
				
				if(appType==1){ //约车时间为空 可以更新约车时间
					if(!StringUtils.isEmpty(delivery_appointment)){
						if((appDate==null) || !delivery_appointment.equals(DateUtil.FormatDatetime("yyyy/MM/dd", appDate))){
							tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_39);
							continue;
						}
					}
					
					//判断约车限制
					DBRow appRow=this.floorAppointmentMgr.getAppointWork(appointment,sendPsid);
					if(appRow==null || (Integer)appRow.getValue("limit_")==0){
						tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_29);
						continue;
					}
					int limit=Integer.parseInt(appRow.getString("limit_"));
					int cnt=Integer.parseInt(appRow.getString("work"));
					
					
					//如果是FTL console
					if(EnumUtils.isLTLOrConsol(freightType)){
						
						String key=sendPsid+"|"+appointment+"|"+freightType;
						String key_1=sendPsid+"|"+appointment+"|";
						
						AppointmentObject obj=new AppointmentObject();
						obj.setId(loadno);
						obj.setType(1); //按load约车
						obj.setCarrier(carrier);
						obj.setNeedCreate(2); //appoint创建
						obj.getOrderList().add(b2b_oid);
						
						DBRow dbrow=floorAppointmentMgr.findWorkByAppt(appointment, Long.parseLong(sendPsid), carrier,freightType);
						
						//如果已经已经有了可以共用一个约车
						if(dbrow!=null){
							DBRow $2=floorAppointmentMgr.findWorkByApptOid(appointment, Long.parseLong(sendPsid), carrier,freightType,b2b_oid);
							
							if($2!=null)
								delOrderAppSet.add(b2b_oid);
							
							if(exsitAppMap.containsKey(key)){ //已经存在这个仓库这个点的约车,直接合并进去
								exsitAppMap.get(key).add(obj);
							}else{
								Set<AppointmentObject> tmpSet=new HashSet<AppointmentObject>();
								tmpSet.add(obj);
								exsitAppMap.put(key,tmpSet);
							}
							continue;
						}else{
							if(canAppMap.containsKey(key)){ //已经存在这个仓库这个点的约车,直接合并进去
								canAppMap.get(key).add(obj);
							}else{
								Set<AppointmentObject> tmpSet=new HashSet<AppointmentObject>();
								tmpSet.add(obj);
								canAppMap.put(key,tmpSet);
							}
							Set<String> $1=appMap.get(key_1);
							if($1!=null)
								cnt+=canAppMap.keySet().size()+$1.size();
							else
								cnt+=canAppMap.keySet().size();
							if(cnt>limit){
								canAppMap.remove(key);
								tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_28);
							}
							continue;
						}
					}else{
						String key=sendPsid+"|"+appointment+"|";//+loadno;
						
						if(!appMap.containsKey(key)){
							Set<String> set=new HashSet<String>();
							set.add(loadno);
							appMap.put(key, set);
							cnt+=appMap.get(key).size()+getApptNum(canAppMap,key);
						}else{
							Set<String> set=appMap.get(key);
							if(!set.contains(loadno)){ //相同的app下的新loadno
								set.add(loadno);
								cnt+=appMap.get(key).size()+getApptNum(canAppMap,key);
							}
						}
						
					}
					
					//小于限制数量
					if(cnt<=limit){
						tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_24);
					}else{
						tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_28);
					}
				}else if(appType==2 || appType==3){
					
					datas=tmpOrderLinesDao.find(para,new String[]{"row"});
					
					for(DBRow r:datas){
						ExcelRowResult err=new ExcelRowResult();
						err.setDn(dn);
						err.setSign(ExcelConstants.ERROR_TYPE_23);
						err.setMsg(ExcelConstants.ERROR_MSG_23);
						String oldappLocal=DateUtil.showLocalTime(oldapp, Long.parseLong(sendPsid));
						err.addCellResult(err.createCellResult(infoApp.getExcelSn(), "pickup appointment:"+oldappLocal));
						err.addCellResult(err.createCellResult(infoApptime.getExcelSn(), "pickup appointment:"+oldappLocal));
						err.setRow(Integer.parseInt(r.getString("row")));
						ctx.addExcelRowResult(err);
					}
					
					tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_23);
				}else if(appType==4){
					//load appt 都没变化
				}
			}else if(type==5){
				//都是空
				//有appt就按照order约车
				if(!StringUtils.isEmpty(appointment)){
					//按order约车 计算约车上限

					@SuppressWarnings("rawtypes")

					List appList=floorAppointmentMgr.diffAppointment(b2b_oid, appointment,"order");
					int appType=(int)appList.get(0);
					String oldapp=(String)appList.get(1);
					
					if(appType==1){ //约车时间为空 可以更新约车时间
						if(!StringUtils.isEmpty(delivery_appointment)){
							if(appDate==null || !delivery_appointment.equals(DateUtil.FormatDatetime("yyyy/MM/dd", appDate))){
								tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_39);
								continue;
							}
						}
						
						//判断约车限制
						DBRow appRow=this.floorAppointmentMgr.getAppointWork(appointment,sendPsid);
						if(appRow==null || (Integer)appRow.getValue("limit_")==0){
							tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_29);
							continue;
						}
						int limit=Integer.parseInt(appRow.getString("limit_"));
						int cnt=Integer.parseInt(appRow.getString("work"));
						
						//如果是FTL console
						if(EnumUtils.isLTLOrConsol(freightType)){
							String key=sendPsid+"|"+appointment+"|"+freightType;
							String key_1=sendPsid+"|"+appointment+"|";
									
							AppointmentObject obj=new AppointmentObject();
							obj.setId(b2b_oid);
							obj.setType(2); //按order约车
							obj.setCarrier(carrier);
							obj.setNeedCreate(3); //使用现有appointment
							
							DBRow dbrow=floorAppointmentMgr.findWorkByAppt(appointment, Long.parseLong(sendPsid), carrier,freightType);
							
							//如果已经已经有了可以共用一个约车
							if(dbrow!=null){
								DBRow $2=floorAppointmentMgr.findWorkByApptOid(appointment, Long.parseLong(sendPsid), carrier,freightType,b2b_oid);
								
								if($2!=null)
									delOrderAppSet.add(b2b_oid);
								
								if(exsitAppMap.containsKey(key)){ //已经存在这个仓库这个点的约车,直接合并进去
									exsitAppMap.get(key).add(obj);
								}else{
									Set<AppointmentObject> tmpSet=new HashSet<AppointmentObject>();
									tmpSet.add(obj);
									exsitAppMap.put(key,tmpSet);
								}
								continue;
							}else{
								if(canAppMap.containsKey(key)){ //已经存在这个仓库这个点的约车,直接合并进去
									canAppMap.get(key).add(obj);
								}else{
									Set<AppointmentObject> tmpSet=new HashSet<AppointmentObject>();
									tmpSet.add(obj);
									canAppMap.put(key,tmpSet);
								}
								Set<String> $1=appMap.get(key_1);
								if($1!=null)
									cnt+=canAppMap.keySet().size()+$1.size();
								else
									cnt+=canAppMap.keySet().size();
								
								if(cnt>limit){
									canAppMap.remove(key);
									tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_28);
								}
								continue;
							}
						}else{
							String key=sendPsid+"|"+appointment+"|";//+loadno;
							
							if(!appMap.containsKey(key)){
								Set<String> set=new HashSet<String>();
								set.add(b2b_oid);
								appMap.put(key, set);
							}else{
								Set<String> set=appMap.get(key);
								set.add(b2b_oid);
							}
							//有问题 
							cnt+=appMap.get(key).size()+getApptNum(canAppMap,key);
						}
						//小于限制数量
						if(cnt<=limit){
							tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_41);
						}else{
							tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_28);
						}
					}else if(appType==2 || appType==3){
						datas=tmpOrderLinesDao.find(para,new String[]{"row"});
						
						for(DBRow r:datas){
							ExcelRowResult err=new ExcelRowResult();
							err.setDn(dn);
							err.setSign(ExcelConstants.ERROR_TYPE_23);
							err.setMsg(ExcelConstants.ERROR_MSG_23);
							String oldappLocal=DateUtil.showLocalTime(oldapp, Long.parseLong(sendPsid));
							err.addCellResult(err.createCellResult(infoApp.getExcelSn(), "pickup appointment:"+oldappLocal));
							err.addCellResult(err.createCellResult(infoApptime.getExcelSn(), "pickup appointment:"+oldappLocal));
							err.setRow(Integer.parseInt(r.getString("row")));
							ctx.addExcelRowResult(err);
						}
						
						tmpOrderLinesDao.updateSign(dn, cid, importer, ExcelConstants.ERROR_TYPE_23);
					}
				}
				
			}
		}
		
		return objs;
	}
	
	/**
	 * 标记可以更新appoint的记录
	 * @param ctx
	 * @throws Exception
	 */
	public synchronized void findAppointment(ProcessContext ctx) throws Exception{
		
	}

	public Set<String> findSysMoreDn(String cid, long importer)
			throws Exception {
		DBRow [] rowsOrder=b2bOrderDao.findLeftOrder(cid,importer);
		Set<String> vorder=new HashSet<String>();
		for(DBRow row:rowsOrder){
			vorder.add(row.getString("customer_dn"));
		}
		return vorder;
	}

	public DBRow[] findOrderAndDetail(String[] dns, String cid)
			throws Exception {
		if(dns.length<=0)
			return null;
		
		String dnStr="";
		
		if(null!=dns && dns.length>0){
			for(String dn:dns){
				dnStr+="'"+dn+"',";
			}
		
			dnStr=dnStr.substring(0,dnStr.length()-1);
		}
		return b2bOrderDao.findOrderAndDetail(dnStr,cid);
	}
	
	/**
	 * 导入账单信息
	 * @throws Exception 
	 */
	private void importBillInfo(String billTo,String defaultId,DBRow target) throws Exception{
		if(!StringUtils.isEmpty(billTo)){//为空默认账单地址寄给Customer
			DBRow para=new DBRow();
			para.put("customer_id", billTo);
			DBRow[] cbRow=customerIdDao.find(para);
			if(cbRow!=null && cbRow.length>0){
				String ckey=cbRow[0].getString("customer_key");
				
				para=new DBRow();
				para.put("storage_type", 7);
				para.put("storage_type_id", ckey);
				
				DBRow [] rows=productStorageCatalogDao.find(para);
				if(rows!=null && rows.length>0){
					if(rows.length>1)
						throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
					
					setBillInfo(target,rows[0],billTo);
				}else{
					throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
				}
			}else{
				para.clear();
				
				para.add("ship_to_name", billTo);
				DBRow [] rows=shipToDao.find(para,new String[]{"ship_to_id"});
				
				if(rows!=null && rows.length>0){
					if(rows.length>1)
						throw new RuntimeException("ship_to find multi data by "+para.toString());
					
					para.clear();
					para.put("storage_type_id", rows[0].getString("ship_to_id"));
					para.put("is_bill_dc", 1);
					para.put("storage_type", 5);
					
					rows=productStorageCatalogDao.find(para);
					
					if(rows.length>1)
						throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
					
					setBillInfo(target,rows[0],billTo);
				}else{ //可能是carrier
					para.clear();
					para.put("carrier", billTo);
					rows=carrierScacMcdotDao.find(para,new String[]{"id"});
					
					if(rows!=null && rows.length>0){
						para.clear();
						para.put("storage_type", 8);
						para.put("storage_type_id", rows[0].getString("id"));
						
						rows=productStorageCatalogDao.find(para);
						if(rows.length>1)
							throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
						
						setBillInfo(target,rows[0],billTo);
					}else{
//						throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
					}
				}
			}
		}else{
			DBRow para=new DBRow();
			para.put("storage_type", 7);
			para.put("storage_type_id", defaultId);
			
			DBRow [] rows=productStorageCatalogDao.find(para);
			if(rows!=null && rows.length>0){
				if(rows.length>1)
					throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
				
				setBillInfo(target,rows[0],billTo);
			}
		}
	}
	
	private void setBillInfo(DBRow target,DBRow product,String billTo) throws NumberFormatException, Exception{
		target.put("bill_to_id",product.getString("id"));
		target.put("bill_to_name",billTo);
		target.put("bill_to_address1",product.getString("deliver_house_number"));
		target.put("bill_to_address2",product.getString("deliver_street"));
		target.put("bill_to_city",product.getString("deliver_city"));
		
		String ccid=product.getString("deliver_nation");
		String proid=product.getString("deliver_pro_id");
		if(!StringUtils.isEmpty(ccid)){
			DBRow ccRow=countryCodeDao.getByCcid(Integer.parseInt(ccid));
			if(ccRow!=null){
				target.put("bill_to_country", ccRow.getString("c_country"));
			}
		}
		
		if(!StringUtils.isEmpty(proid)){
			DBRow proRow=countryProvinceDao.getDetailProByProId(Integer.parseInt(proid));
			if(proRow!=null)
				target.put("bill_to_state", proRow.getString("p_code"));
		}
		
		target.put("bill_to_zip_code",product.getString("deliver_zip_code"));
		target.put("bill_to_contact",product.getString("deliver_contact"));
		target.put("bill_to_phone",product.getString("deliver_phone"));
		target.put("bill_to_home",(byte)0);
	}
	
	public synchronized Object[] addLoad(ProcessContext ctx,Map<String,Set<AppointmentObject>> canApp,Map<String,Set<AppointmentObject>> exsitApp,Set<String> delOrders) throws Exception {
		String cid=ctx.getCid();
		DBRow $1=new DBRow();
		$1.put("cb_id", cid);
		DBRow[] cbRow=customerBrandDao.find($1);
		String ckey=cbRow[0].getString("customer_key");
		
		long importer=ctx.getUser().getAdid();
		String userName=ctx.getUser().getEmploye_name();
		
		Set<Integer> errSet=new HashSet<Integer>();
		errSet.add(ExcelConstants.ERROR_TYPE_15); //更新load关系
		errSet.add(ExcelConstants.ERROR_TYPE_24); //更新appointment
		errSet.add(ExcelConstants.ERROR_TYPE_41); //按order来约车
		errSet.add(ExcelConstants.ERROR_TYPE_42); //根据loadno约车,但是loadNo下的order已经约过,so 要删除order在约车模板的所有关系数据
		errSet.add(ExcelConstants.ERROR_TYPE_19);
		
		b2bOrderDao.updateCarrierAndFreightType(cid, importer);
		DBRow [] rows=tmpOrderLinesDao.findLoadApp(cid, importer,errSet);
		Map<String,Set<String>> loadMap=new LinkedHashMap<String,Set<String>>();
		Map<String,String> carrIdMap=new LinkedHashMap<String,String>();
		Map<String,Map<String,Map<String,Object>>> load_App=new LinkedHashMap<String,Map<String,Map<String,Object>>>();
		Map<String,Map<String,Object>> order_App=new LinkedHashMap<String,Map<String,Object>>();
		Set<String> b2bSet=new HashSet<String>();
		Object [] objs=new Object[]{b2bSet};
		
		for(DBRow row:rows){
			String dn=row.getString("dn");
			String loadno=row.getString("loadno");
			String ft=row.getString("ft");
			String billTo=row.getString("bill_to");
			String freightType=row.getString("freight_type").toUpperCase();

			Date appointment=(Date)row.getValue("pickup_appointment_date");
			Date apptime=(Date)row.getValue("pickup_appointment_time");
			
			int correct=(int)row.getValue("correct");
			String carrierId=row.getString("carrier");
			
			DBRow para=new DBRow();
			para.put("customer_dn", dn);
			para.put("customer_id", cid);
			
			DBRow [] datas=b2bOrderDao.find(para, new String[]{"b2b_oid","send_psid","receive_psid","carriers","etd","eta","deliver_street","freight_term"});
			
			if(datas.length>1)
				throw new Exception("customer_id:"+cid+",customer_dn:"+dn+"查出多个order");
			
			if(datas.length<=0)
				throw new Exception("customer_id:"+cid+",customer_dn:"+dn+"查不到对应的order");
			
			String b2b_oid=datas[0].getString("b2b_oid");
			
			DBRow fields=new DBRow();
			if(appointment!=null){
				fields.put("delivery_appointment", DateUtil.FormatDatetime("yyyy/MM/dd",appointment));
			}
			b2bOrderDao.updateById(Long.parseLong(b2b_oid),fields);
			
			String freight_term=datas[0].getString("freight_term");
			
			//prepaid变3rd party
			if(freight_term.equalsIgnoreCase("Prepaid") && EnumUtils.isThirdParty(ft)){
				DBRow p=new DBRow();
				importBillInfo(billTo,ckey,p);
				b2bOrderDao.updateById(Long.parseLong(b2b_oid), p);
			}
			
			if(correct==ExcelConstants.ERROR_TYPE_19){
				continue;
			}
			
			//可以更新loadno
			if(correct==ExcelConstants.ERROR_TYPE_15 || correct==ExcelConstants.ERROR_TYPE_42){
				if(null==loadno)
					loadno="";
				
				if(!loadMap.containsKey(loadno)){
					Set<String> s=new HashSet<String>();
					s.add(b2b_oid);
					loadMap.put(loadno,s);
				}else
					loadMap.get(loadno).add(b2b_oid);
				
				if(!carrIdMap.containsKey(loadno)){
					carrIdMap.put(loadno, carrierId);
				}
			}
			
			if(correct==ExcelConstants.ERROR_TYPE_24 || correct==ExcelConstants.ERROR_TYPE_42 || correct==ExcelConstants.ERROR_TYPE_15){
				//appointment的关系更新是根据Loadno excel模板提供的数据是按loadno约车 并且一个loadno约一次车
				if(appointment!=null && apptime!=null){
					String appStr=DateUtil.FormatDatetime("yyyy-MM-dd", appointment)+" "+DateUtil.FormatDatetime("HH:mm", apptime);
					if(!load_App.containsKey(loadno)){
						Map<String,Object> b2bData=new HashMap<String,Object>();
						b2bData.put("loadno", loadno);
						b2bData.put("send_psid", datas[0].getString("send_psid"));
						b2bData.put("receive_psid", datas[0].getString("receive_psid"));
						b2bData.put("carriers", datas[0].getString("carriers"));
						b2bData.put("etd", datas[0].getString("etd"));
						b2bData.put("eta", datas[0].getString("eta"));
						b2bData.put("deliver_street", datas[0].getString("deliver_street"));
						b2bData.put("b2b_oid", b2b_oid);
						b2bData.put("freight_type",freightType);
						if(correct==ExcelConstants.ERROR_TYPE_42)
							b2bData.put("#", 1); //标记要把order的约车信息删掉
						
						Map<String,Map<String,Object>> abm=new LinkedHashMap<String,Map<String,Object>>();
						abm.put(appStr,b2bData);
						load_App.put(loadno, abm);
					}
				}
			}
			
			//按order约车 一个order一个appT
			if(correct==ExcelConstants.ERROR_TYPE_41){
				if(appointment!=null && apptime!=null){
					String appStr=DateUtil.FormatDatetime("yyyy-MM-dd", appointment)+" "+DateUtil.FormatDatetime("HH:mm", apptime);
					
					Map<String,Object> b2bData=new HashMap<String,Object>();
					b2bData.put("b2boid", b2b_oid);
					b2bData.put("send_psid", datas[0].getString("send_psid"));
					b2bData.put("receive_psid", datas[0].getString("receive_psid"));
					b2bData.put("carriers", datas[0].getString("carriers"));
					b2bData.put("etd", datas[0].getString("etd"));
					b2bData.put("eta", datas[0].getString("eta"));
					b2bData.put("deliver_street", datas[0].getString("deliver_street"));
					b2bData.put("appt", appStr);
					b2bData.put("freight_type",freightType);
					
					order_App.put(b2b_oid,b2bData);
				}
			}
			
		}
		
		//创建load   load和order的关系   创建master order
		Iterator<Entry<String, Set<String>>> it=loadMap.entrySet().iterator();
		while(it.hasNext()){
			Entry<String, Set<String>> entry=it.next();
			String loadno=entry.getKey();
			if(StringUtils.isEmpty(loadno))
				continue;
			Set<String> orderIdList=(Set<String>)entry.getValue();
			b2bSet.addAll(orderIdList);
			floorLoadMgr.updLoad("xls",loadno,orderIdList,userName,carrIdMap.get(loadno)); //load的处理方式
			DBRow para=new DBRow();
			para.add("load_no", loadno);
			
			for(String oid:orderIdList){
				b2bOrderDao.updateById(Long.parseLong(oid),para);
			}
		}
		
		//创建appointment appointment 和order的关系 
		//1个loadno约一个车
		Iterator<Entry<String,Map<String,Map<String,Object>>>> load_app_it=load_App.entrySet().iterator();
		while(load_app_it.hasNext()){
			Entry<String,Map<String,Map<String,Object>>> entry=load_app_it.next();
			String loadno=entry.getKey();
			Map<String,Map<String,Object>> imap=entry.getValue();
			
			Iterator<Entry<String,Map<String,Object>>> appLoadIt=imap.entrySet().iterator();
			
			while(appLoadIt.hasNext()){
				Entry<String,Map<String,Object>> ientry=appLoadIt.next();
				
				String app=ientry.getKey();
				Map<String,Object> map=ientry.getValue();
				
				b2bSet.add((String)map.get("b2b_oid"));
				String send_psid=(String)map.get("send_psid");
				String etd=(String)map.get("etd");
				String eta=(String)map.get("eta");
				String fr_id=(String)map.get("carriers");
				String ftype=(String)map.get("freight_type");
				
				DBRow storage=productStorageCatalogDao.getById(Long.parseLong(send_psid));
				DBRow carrierInfo=carrierScacMcdotDao.getByScac(fr_id);
				
				DBRow row=new DBRow();
				row.add("storage_id",send_psid);
				row.add("carrier_id",fr_id);
				
				app=DateUtil.showUTCTime(DateUtils.parseDate(app, new String[]{"yyyy-MM-dd HH:mm"}), Long.parseLong(send_psid));
				row.add("appointment_time",app);
				
				if(map.get("#")!=null){
					int isdel=(Integer)map.get("#");
					
					//删除order的约车信息
					if(isdel==1){
						Set<String> oids=loadMap.get(loadno);
						for(String oid:oids){
							//删除关系
							floorAppointmentMgr.delAppInvoiceAndApp(oid,"order",userName);
						}
					}
				}
				
				if(!StringUtils.isEmpty(etd))
					row.add("etd",etd);
				
				if(!StringUtils.isEmpty(eta))
					row.add("eta",map.get("eta"));
				
				row.add("user_created",importer);
				row.add("date_created",new Date());
				row.add("date_updated",new Date());
				row.add("status","open");
				row.add("storage_name", storage.getString("title"));
				row.add("storage_linkman", storage.getString("send_contact"));
				row.add("storage_linkman_tel", storage.getString("send_phone"));
				row.add("appointment_type", "outbound");
				row.add("feight_type", ftype);
				if(carrierInfo!=null)
					row.add("carrier_name", carrierInfo.getString("carrier"));
				else
					row.add("carrier_name", "");
				
				String load_id=floorLoadMgr.getLoadIdByLoadNo(loadno).getString("load_id");
				//创建约车
				long aid=floorAppointmentMgr.addApp(row);
				
				floorAppointmentMgr.updWork(send_psid,app,cid);
				//约车跟loadno的关系
				floorAppointmentMgr.addAppInvoice(String.valueOf(aid),"load",load_id,String.valueOf(importer));
				//记录约车日志
				floorAppointmentMgr.addAppLog("load",load_id,"",String.valueOf(aid),userName,"Modified Appointment");
				
				b2bOrderDao.updateAppointment(loadno,cid,app);
			}
		}
		
		//按order约车
		Iterator<Entry<String,Map<String,Object>>> order_app_it=order_App.entrySet().iterator();
		while(order_app_it.hasNext()){
			Entry<String,Map<String,Object>> entry=order_app_it.next();
			String b2boid=entry.getKey();
			b2bSet.add(b2boid);
			
			Map<String,Object> imap=entry.getValue();
				
			String send_psid=(String)imap.get("send_psid");
			String etd=(String)imap.get("etd");
			String eta=(String)imap.get("eta");
			String fr_id=(String)imap.get("carriers");
			String app=(String)imap.get("appt");
			String ftype=(String)imap.get("freight_type");
			
			DBRow storage=productStorageCatalogDao.getById(Long.parseLong(send_psid));
			DBRow carrierInfo=carrierScacMcdotDao.getByScac(fr_id);
				
			DBRow row=new DBRow();
			row.add("storage_id",send_psid);
			row.add("carrier_id",fr_id);
			row.add("feight_type", ftype);
				
			app=DateUtil.showUTCTime(DateUtils.parseDate(app, new String[]{"yyyy-MM-dd HH:mm"}), Long.parseLong(send_psid));
			row.add("appointment_time",app);
				
			if(!StringUtils.isEmpty(etd))
				row.add("etd",etd);
			
			if(!StringUtils.isEmpty(eta))
				row.add("eta",imap.get("eta"));
			
			row.add("user_created",importer);
			row.add("date_created",new Date());
			row.add("date_updated",new Date());
			row.add("status","open");
			row.add("storage_name", storage.getString("title"));
			row.add("storage_linkman", storage.getString("send_contact"));
			row.add("storage_linkman_tel", storage.getString("send_phone"));
			row.add("appointment_type", "outbound");
			if(carrierInfo!=null)
				row.add("carrier_name", carrierInfo.getString("carrier"));
			else
				row.add("carrier_name", "");
			
			//创建约车
			long aid=floorAppointmentMgr.addApp(row);
			floorAppointmentMgr.addAppLog("order",b2boid,"",String.valueOf(aid),userName,"Create Appointment");
			
			floorAppointmentMgr.updWork(send_psid,app,cid);
			//约车跟loadno的关系
			floorAppointmentMgr.addAppInvoice(String.valueOf(aid),"order",b2boid,String.valueOf(importer));
			//记录约车日志
			floorAppointmentMgr.addAppLog("order",b2boid,"",String.valueOf(aid),userName,"Modified Appointment");
			
			DBRow para=new DBRow();
			para.put("pickup_appointment",app);
			
			b2bOrderDao.updateById(Long.parseLong(b2boid),para);
		}
		
		//ltl console的处理
		if(canApp!=null){
			Iterator<String> iit=canApp.keySet().iterator();
			while(iit.hasNext()){
				boolean canCreate=true;
				String key=(String)iit.next();
				Set<AppointmentObject> set=canApp.get(key);
				
				String [] strs=key.split("\\|");
				
				String send_psid=strs[0];
				String app=strs[1];
				String feight_type=strs[2];
				
				DBRow row=new DBRow();
				row.add("appointment_time",app);
				
				DBRow storage=productStorageCatalogDao.getById(Long.parseLong(send_psid));
				
				row.add("storage_id",send_psid);
				row.add("user_created",importer);
				row.add("date_created",new Date());
				row.add("date_updated",new Date());
				row.add("status","open");
				row.add("storage_name", storage.getString("title"));
				row.add("storage_linkman", storage.getString("send_contact"));
				row.add("storage_linkman_tel", storage.getString("send_phone"));
				row.add("appointment_type", "outbound");
				String cname=((AppointmentObject)set.toArray()[0]).getCarrier();
				DBRow carrierInfo=carrierScacMcdotDao.getByScac(cname);
				row.add("carrier_name", carrierInfo.getString("carrier"));
				row.add("carrier_id",cname);
				row.add("feight_type", feight_type);
				
				//创建约车
				long aid=floorAppointmentMgr.addApp(row);

				for(AppointmentObject obj:set){
					if(obj.getType()==1){ //load
						if(obj.getNeedCreate()==1){
							String loadno=obj.getId();
							Set<String> orderIdList=obj.getOrderList();
							long load_id=floorLoadMgr.updLoad("xls",loadno,orderIdList,userName,cname); //load的处理方式
							DBRow para=new DBRow();
							para.add("load_no", loadno);
							
							for(String oid:orderIdList){
								b2bOrderDao.updateById(Long.parseLong(oid),para);
							}
							
							if(floorAppointmentMgr.getAppInvoice(String.valueOf(load_id), "load")==null){
								//约车跟loadno的关系
								floorAppointmentMgr.addAppInvoice(String.valueOf(aid),"load",String.valueOf(load_id),String.valueOf(importer));
								//记录约车日志
								floorAppointmentMgr.addAppLog("load",String.valueOf(load_id),"",String.valueOf(aid),userName,"Modified Appointment");
								
								b2bOrderDao.updateAppointment(loadno,cid,app);
							}
							
							b2bSet.addAll(orderIdList);
							if(canCreate){
								floorAppointmentMgr.updWork(send_psid,app,cid);
								canCreate=false;
							}
						}else if(obj.getNeedCreate()==2){
							String loadno=obj.getId();
							Set<String> orderIdList=obj.getOrderList();
							b2bSet.addAll(orderIdList);
							long load_id=floorLoadMgr.getOrInsterLoadNo(userName, loadno, userName , cname);
							
							if(floorAppointmentMgr.getAppInvoice(String.valueOf(load_id), "load")==null){
								//约车跟loadno的关系
								floorAppointmentMgr.addAppInvoice(String.valueOf(aid),"load",String.valueOf(load_id),String.valueOf(importer));
								//记录约车日志
								floorAppointmentMgr.addAppLog("load",String.valueOf(load_id),"",String.valueOf(aid),userName,"Modified Appointment");
								
								b2bOrderDao.updateAppointment(loadno,cid,app);
								if(canCreate){
									floorAppointmentMgr.updWork(send_psid,app,cid);
									canCreate=false;
								}
							}
						}
					}else if(obj.getType()==2){ //order
						String b2boid=obj.getId();
						b2bSet.add(b2boid);
						
						if(floorAppointmentMgr.getAppInvoice(b2boid, "order")==null){
							//约车跟loadno的关系
							floorAppointmentMgr.addAppInvoice(String.valueOf(aid),"order",b2boid,String.valueOf(importer));
							
							//记录约车日志
							floorAppointmentMgr.addAppLog("order",b2boid,"",String.valueOf(aid),userName,"Modified Appointment");
							
							DBRow para=new DBRow();
							para.put("pickup_appointment",app);
							
							b2bOrderDao.updateById(Long.parseLong(b2boid),para);
							if(canCreate){
								floorAppointmentMgr.updWork(send_psid,app,cid);
								canCreate=false;
							}
						}
					}
					
				}
			}
		}
		
		//合并到现有的appt上
		if(exsitApp!=null){
			Iterator<String> iit=exsitApp.keySet().iterator();
			while(iit.hasNext()){
				String key=(String)iit.next();
				Set<AppointmentObject> set=exsitApp.get(key);
				
				String [] strs=key.split("\\|");
				
				String sendPsid=strs[0];
				String app=strs[1];
				String freightType=strs[2];
				
				DBRow row=new DBRow();
				row.add("appointment_time",app);
				
				String cname=((AppointmentObject)set.toArray()[0]).getCarrier();
				
				DBRow dbrow=floorAppointmentMgr.findWorkByAppt(app, Long.parseLong(sendPsid), cname,freightType);
				//创建约车
				long aid=Long.parseLong(dbrow.getString("id"));
				
				for(AppointmentObject obj:set){
					if(obj.getType()==1){
						if(obj.getNeedCreate()==1){
							String loadno=obj.getId();
							Set<String> orderIdList=obj.getOrderList();
							long load_id=floorLoadMgr.updLoad("xls",loadno,orderIdList,userName,cname); //load的处理方式
							DBRow para=new DBRow();
							para.add("load_no", loadno);
							
							for(String oid:orderIdList){
								b2bOrderDao.updateById(Long.parseLong(oid),para);
							}
							
							//约车跟loadno的关系
							floorAppointmentMgr.addAppInvoice(String.valueOf(aid),"load",String.valueOf(load_id),String.valueOf(importer));
							//记录约车日志
							floorAppointmentMgr.addAppLog("load",String.valueOf(load_id),"",String.valueOf(aid),userName,"Modified Appointment");
							
							b2bOrderDao.updateAppointment(loadno,cid,app);
							
							b2bSet.addAll(orderIdList);
						}else if(obj.getNeedCreate()==2){
							String loadno=obj.getId();
							long load_id=floorLoadMgr.getOrInsterLoadNo(userName, loadno, userName , cname);
							
							//约车跟loadno的关系
							floorAppointmentMgr.addAppInvoice(String.valueOf(aid),"load",String.valueOf(load_id),String.valueOf(importer));
							//记录约车日志
							floorAppointmentMgr.addAppLog("load",String.valueOf(load_id),"",String.valueOf(aid),userName,"Modified Appointment");
							
							b2bOrderDao.updateAppointment(loadno,cid,app);
						}
					}else if(obj.getType()==2){
						String b2boid=obj.getId();
						b2bSet.add(b2boid);
						//约车跟loadno的关系
						floorAppointmentMgr.addAppInvoice(String.valueOf(aid),"order",b2boid,String.valueOf(importer));
						
						//记录约车日志
						floorAppointmentMgr.addAppLog("order",b2boid,"",String.valueOf(aid),userName,"Modified Appointment");
						
						DBRow para=new DBRow();
						para.put("pickup_appointment",app);
						
						b2bOrderDao.updateById(Long.parseLong(b2boid),para);
					}
					
				}
				
			}
		}
		
		for(String oid:delOrders){
			floorAppointmentMgr.delAppInvoiceAndApp(oid,"order",userName);
		}
		
		return objs;
	}
	
	public void synWmsLoadApp(Set<String> set) throws Exception {
		if(set.isEmpty())
			return;
		
		for(String oid:set){
			try{
				proxyWMSOrderMgr.update(Long.parseLong(oid), WmsOrderUpdateTypeKey.LOADAPPT);
			}catch(RuntimeException e){
				e.printStackTrace();
			}
		}
	}

	public void exportImportLoadAppResult(ProcessContext ctx) throws Exception {
		DBRow para=new DBRow();
		para.put("customer_id", ctx.getCid());
		para.put("importer", ctx.getUser().getAdid());
		int [] dataRows=ctx.getExcelData().getDataRows();
		String [][]data=ctx.getExcelData().getDataArray();
		DBRow [] rows=tmpOrderLinesDao.find(para,new String[]{"dn","correct","row"});

		for(DBRow m:rows){
			int row=(Integer)m.getValue("row");
			int errType=(Integer)m.getValue("correct");
			
			String dn=m.getString("dn");
			
			ExcelRowResult bean=new ExcelRowResult();
			
			if(SignManager.canLoadSign(errType)){
				bean.setRight(1);
			}
			
			int index=ExcelUtils.getIndex(dataRows, row);
			bean.setData(data[index]);
			
			if(errType==4 || errType==13){
				errType=25;
			}
			
			bean.setSign(errType);
			bean.setRow(row);
			bean.setMsg(ExcelConstants.ERROR_MAP.get(errType));
			bean.setDn(dn);
			
			ctx.addExcelRowResult(bean);
		}
	}

}
