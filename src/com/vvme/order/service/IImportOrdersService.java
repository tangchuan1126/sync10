package com.vvme.order.service;

import java.util.Set;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.vvme.order.ProcessContext;

/**
 * 
 * @ClassName: IImportOrdersService
 * @Description: 订单导入服务层
 * @author yetl
 * @date 2015年4月24日
 *
 */
public interface IImportOrdersService {
	
	/**
	 * excel数据导入临时表
	 * @param excelData
	 * @return
	 * @throws Exception 
	 */
	public boolean insertExcelDataToTemp(ProcessContext ctx) throws Exception;
	
	/**
	 * 导入excel
	 * @param path 订单excel文件路径
	 * @throws Exception
	 * @return true 导入临时表成功
	 */
	public void importOrdersToTemp(ProcessContext ctx) throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow [] find(String dn,String cid,long importer) throws Exception;
	
	/**
	 * 
	 * @param dn
	 * @param cid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findByDn(String dn,String cid,long importer) throws Exception;
	
	/**
	 * 
	 * @param cid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findImportOrders(String cid,long importer) throws Exception;
	
	/**
	 * 
	 * @param dn
	 * @param cid
	 * @param importer
	 * @param sign
	 * @throws Exception
	 */
	public void update(String dn,String cid,long importer,int sign) throws Exception;
	
	/**
	 * 
	 * @param cid
	 * @param importer
	 * @throws Exception
	 */
	public void exportImportOrdersResult(ProcessContext ctx) throws Exception;
	
	/**
	 * 
	 * @param cid
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Set<Long> ImportOrders(ProcessContext ctx) throws Exception;
	
	/**
	 * 
	 * @param dn
	 * @param cid
	 * @param correct
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public long importOrder(String dn,String cid,int correct,AdminLoginBean user) throws Exception;
	
	/**
	 * 
	 * @param set
	 * @param user
	 * @throws Exception
	 */
	public void synWmsOrders(Set<Long> set,AdminLoginBean user) throws Exception;
	
	/**
	 * 
	 * @param cid
	 * @param importer
	 * @return 
	 * @throws Exception
	 */
	public Set<Integer> findSign17Row(String cid,long importer) throws Exception;
	
	/**
	 * 
	 * @param cname
	 * @return
	 * @throws Exception
	 */
	public String getCidByName(String cname) throws Exception;
	
	//获取对应记号的数据
	public DBRow [] findSign2(String cid,long importer) throws Exception;
	public void findSign8(String cid,long importer) throws Exception;
	public void findSign11(String cid,long importer) throws Exception;
	public void findSign17(String cid,long importer) throws Exception;
	public void findSign20(String cid,long importer) throws Exception;
	public void findSign35(String cid,long importer) throws Exception;
	public void findSign36(String cid,long importer) throws Exception;
	public void findSign43(String cid,long importer) throws Exception;
	public void findSign44(String cid,long importer) throws Exception;
	public void findSign45(String cid,long importer) throws Exception;
	public void findSign46(String cid,long importer) throws Exception;
	public void findSign54(String cid,long importer) throws Exception;
	public void findSign55(String cid,long importer) throws Exception;
	public void findSign56(String cid,long importer) throws Exception;
	public void findSign57(String cid,long importer) throws Exception;
	public void findSignOther(String cid,long importer) throws Exception;
}
