package com.vvme.order.service;

import org.apache.axis.utils.StringUtils;

import com.cwc.db.DBRow;
import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.SignManager;
import com.vvme.order.dao.B2bOrderDao;
import com.vvme.order.dao.B2bOrderItemDao;
import com.vvme.order.dao.TmpOrderLinesDao;
import com.vvme.order.util.ExcelUtils;

public class ImportSOService implements IImportSOService{
	private TmpOrderLinesDao tmpOrderLinesDao;
	private IExcelTableService excelTableService;
	private IImportOrdersService importOrdersService;
	private B2bOrderItemDao b2bOrderItemDao;
	
	public void setB2bOrderItemDao(B2bOrderItemDao b2bOrderItemDao) {
		this.b2bOrderItemDao = b2bOrderItemDao;
	}

	public void setImportOrdersService(IImportOrdersService importOrdersService) {
		this.importOrdersService = importOrdersService;
	}

	public void setTmpOrderLinesDao(TmpOrderLinesDao tmpOrderLinesDao) {
		this.tmpOrderLinesDao = tmpOrderLinesDao;
	}

	public void setExcelTableService(IExcelTableService excelTableService) {
		this.excelTableService = excelTableService;
	}
	
	public void importOrdersToTemp(ProcessContext ctx) throws Exception {
		importOrdersService.importOrdersToTemp(ctx);
	}
	
	public void updateSO(ProcessContext ctx) throws Exception {
		String cid=ctx.getCid();
		long importer=ctx.getUser().getAdid();
		
		DBRow [] rows=tmpOrderLinesDao.findSO(cid,importer,ExcelConstants.ERROR_TYPE_19);
		
		for(DBRow row:rows){
			String order_number=row.getString("order_number");
			long b2b_detail_id=Long.parseLong(row.getString("b2b_detail_id"));
			
			DBRow para=new DBRow();
			para.put("so", order_number);
			b2bOrderItemDao.updateById(b2b_detail_id, para);
		}
	}

	public void findSign52(ProcessContext ctx) throws Exception {
		String cid=ctx.getCid();
		long importer=ctx.getUser().getAdid();
		
		DBRow [] rows=tmpOrderLinesDao.findSO(cid,importer,0);
		
		FieldInfo orderNumInfo=excelTableService.getOrderFieldInfoByFieldName("order_number", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		
		for(DBRow row:rows){
			String dn=row.getString("dn");
			String so=row.getString("so");
			String order_number=row.getString("order_number");
			String r=row.getString("row");
			
			if(order_number.trim().equalsIgnoreCase(so.trim())){
			}else{
				//so不能为空
				if(!StringUtils.isEmpty(so.trim())){
					ExcelRowResult rst=new ExcelRowResult();
					rst.setDn(dn);
					rst.setRow(Integer.parseInt(r));
					rst.setSign(ExcelConstants.ERROR_TYPE_52);
					rst.setMsg(ExcelConstants.ERROR_MSG_52);
					rst.addCellResult(rst.createCellResult(orderNumInfo.getExcelSn(), "System value:"+so));
					ctx.addExcelRowResult(rst);
					tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_52);
					continue;
				}
			}
			
			tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_19);
		}
	}

	public void exportImportSOResult(ProcessContext ctx) throws Exception {
		String cid=ctx.getCid();
		long uid=ctx.getUser().getAdid();
		int [] dataRows=ctx.getExcelData().getDataRows();
		String [][]data=ctx.getExcelData().getDataArray();
		
		DBRow para=new DBRow();
		para.put("customer_id", cid);
		para.put("importer", uid);
		DBRow [] rows=tmpOrderLinesDao.find(para,new String[]{"dn","correct","row"});
		
		for(DBRow m:rows){
			int row=(Integer)m.getValue("row");
			int errType=(Integer)m.getValue("correct");
			String dn=m.getString("dn");
			
			ExcelRowResult bean=new ExcelRowResult();
			
			if(SignManager.canSOSign(errType)){
				bean.setRight(1);
			}
			
			if(errType==4){
				errType=25;
			}			
			
			int index=ExcelUtils.getIndex(dataRows, row);
			bean.setData(data[index]);
			
			bean.setSign(errType);
			bean.setRow(row);
			bean.setMsg(ExcelConstants.ERROR_MAP.get(errType));
			bean.setDn(dn);
			ctx.addExcelRowResult(bean);
		}
	}

}
