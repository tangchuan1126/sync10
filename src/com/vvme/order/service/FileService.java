package com.vvme.order.service;

import com.cwc.db.DBRow;
import com.vvme.order.dao.FileDao;

/**
 * 
 * @ClassName: FileService
 * @Description: 
 * @author yetl
 * @date 2015年4月29日
 *
 */
public class FileService implements IFileService {
	private FileDao fileDao;

	public void setFileDao(FileDao fileDao) {
		this.fileDao = fileDao;
	}

	public void update(long id, String fName, String expireSecs)
			throws Exception {
		DBRow row = new DBRow();
		row.add("file_name", fName);
		row.add("original_file_name", fName);
		row.add("expire_in_secs", expireSecs);
		fileDao.updateById(id, row);
	}

}
