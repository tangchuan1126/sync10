package com.vvme.order.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.cwc.db.DBRow;
import com.vvme.order.ExcelDataArray;
import com.vvme.order.FieldInfo;
import com.vvme.order.dao.ExcelTableDao;
import com.vvme.order.dao.XlsExportDao;

/**
 * 
 * @ClassName: ExcelTableMapper
 * @Description: 获取对应客户的所有模板和模板的映射关系
 * @author yetl
 * @date 2015年4月23日
 *
 */
public class ExcelTableService implements IExcelTableService{
	private static Logger log = Logger.getLogger("platform");
	
	private ExcelTableDao excelTableDao;
	private XlsExportDao xlsExportDao;
	private ICacheService cacheService;
	
	public void setCacheService(ICacheService cacheService) {
		this.cacheService = cacheService;
	}

	public void setExcelTableDao(ExcelTableDao excelTableDao) {
		this.excelTableDao = excelTableDao;
	}
	
	public void setXlsExportDao(XlsExportDao xlsExportDao) {
		this.xlsExportDao = xlsExportDao;
	}

	public String getCid(ExcelDataArray excelData) {
		return null;
	}

	public int getExportOrdersTemplateId(String cid) throws Exception {
		DBRow[] rows;
		try {
			rows = xlsExportDao.getExcelExport();
			return Integer.parseInt(rows[0].getString("file_id"));
		} catch (Exception e) {
			e.printStackTrace();
			log.info("获取导出模板失败:"+e.getMessage());
			throw e;
		}
	}

	public int getImportOrdersTemplateId(String cid) throws Exception {
		DBRow[] rows;
		try {
			rows = xlsExportDao.getExcelExport();
			return Integer.parseInt(rows[0].getString("order_import_file_id"));
		} catch (Exception e) {
			e.printStackTrace();
			log.info("获取导出模板失败:"+e.getMessage());
			throw e;
		}
	}

	public int getExportLoadTemplateId(String cid) {
		return 0;
	}

	public int getImportLoadTemplateId(String cid) throws Exception {
		DBRow[] rows;
		try {
			rows = xlsExportDao.getExcelExport();
			return Integer.parseInt(rows[0].getString("load_import_file_id"));
		} catch (Exception e) {
			e.printStackTrace();
			log.info("获取导出模板失败:"+e.getMessage());
			throw e;
		}
	}

	public Map<Integer, FieldInfo> getImportTemplateInfoByMid(int mid) throws Exception {
		Map<Integer,FieldInfo> map=new HashMap<Integer,FieldInfo>();
		DBRow [] rows=excelTableDao.getFieldInfo(mid);
		
		for(DBRow row:rows){
			FieldInfo fi=new FieldInfo();
			
			String cl=row.getString("cl"); //字段名字
			fi.setName(cl);
			fi.setType(row.getString("dt")); //字段类型
			fi.setIsNil(row.getString("isnil")); //是否可以为空
			
			Object cml=row.getValue("cml"); //字符类型的最大长度
			if(null!=cml){
				fi.setMaxLen(Integer.valueOf(cml.toString()));
			}
			
			Object np=row.getValue("np"); //数值最大位数
			if(null!=np){
				fi.setNumPs(Integer.valueOf(np.toString()));
			}
			
			Object ns=row.getValue("ns"); //数值小数点最大位数
			if(null!=ns){
				fi.setNumSc(Integer.valueOf(ns.toString()));
			}
			
			Object sn=row.getValue("sn"); //临时表字段对应数据库字段哪列
			int snInt=Integer.valueOf(sn.toString());
			fi.setExcelSn(snInt);
			
			fi.setXlsTitle(row.getString("title"));
			fi.setFormat(row.getString("format"));
			fi.setOrderColumnName(row.getString("ocn"));
			map.put(snInt, fi);
			
		}
		return map;
	}

	public DBRow getExportOrdersTemplate(String cid) throws Exception {
		DBRow [] rows=xlsExportDao.getExcelExport();
		return rows[0];
	}

	public FieldInfo getFieldInfoByFieldName(String fieldName,String tableName,int mid) throws Exception {
		Map<Integer, FieldInfo> cache_field=getImportTemplateInfoByMid(mid);
		Iterator<Entry<Integer, FieldInfo>> it=cache_field.entrySet().iterator();
		
		while(it.hasNext()){
			Map.Entry<Integer, FieldInfo> entry=(Map.Entry<Integer, FieldInfo>)it.next();
			FieldInfo value=entry.getValue();
			
			if(value.getName().equalsIgnoreCase(fieldName))
				return value;
			
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public FieldInfo getOrderFieldInfoByFieldName(String fieldName,
			String tableName) throws Exception {
		int mid=getImportOrdersTemplateId("");
		
		Map<Integer, FieldInfo> cache_field=(Map<Integer, FieldInfo>) cacheService.getObject(CACHE_ORDER_KEY+mid);
		if(cache_field==null){
			cache_field=getImportTemplateInfoByMid(mid);
			cacheService.cacheObject(CACHE_ORDER_KEY+mid, cache_field);
			log.info("cache order模板-表字段 信息成功");
		}else{
			log.debug("从redis里获取order模板对应表字段映射信息"+cache_field);
		}
		
		Iterator<Entry<Integer, FieldInfo>> it=cache_field.entrySet().iterator();
		
		while(it.hasNext()){
			Map.Entry<Integer, FieldInfo> entry=(Map.Entry<Integer, FieldInfo>)it.next();
			FieldInfo value=entry.getValue();
			
			if(value.getName().equalsIgnoreCase(fieldName))
				return value;
			
		}
		
		return getFieldInfoByFieldName(fieldName,tableName,mid);
	}

	@SuppressWarnings("unchecked")
	public FieldInfo getLoadFieldInfoByFieldName(String fieldName,
			String tableName) throws Exception {
		int mid=getImportLoadTemplateId("");
		
		Map<Integer, FieldInfo> cache_field=(Map<Integer, FieldInfo>) cacheService.getObject(CACHE_LOAD_KEY+mid);
		if(cache_field==null){
			cache_field=getImportTemplateInfoByMid(mid);
			cacheService.cacheObject(CACHE_LOAD_KEY+mid, cache_field);
			log.info("cache load模板-表字段 信息成功");
		}else{
			log.debug("从redis里获取load模板对应表字段映射信息"+cache_field);
		}
		
		Iterator<Entry<Integer, FieldInfo>> it=cache_field.entrySet().iterator();
		
		while(it.hasNext()){
			Map.Entry<Integer, FieldInfo> entry=(Map.Entry<Integer, FieldInfo>)it.next();
			FieldInfo value=entry.getValue();
			
			if(value.getName().equalsIgnoreCase(fieldName))
				return value;
			
		}
		
		return getFieldInfoByFieldName(fieldName,tableName,mid);
	}

}
