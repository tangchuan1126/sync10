package com.vvme.order.service;

import com.vvme.order.ProcessContext;

/**
 * 
 * @ClassName: IImportSOService
 * @Description: 更新SO
 * @author yetl
 * @date 2015年4月30日
 *
 */
public interface IImportSOService {
	
	/**
	 * 
	 * @param ctx
	 * @throws Exception
	 */
	public void importOrdersToTemp(ProcessContext ctx) throws Exception;
	
	/**
	 * 
	 * @param ctx
	 * @throws Exception
	 */
	public void updateSO(ProcessContext ctx) throws Exception;
	
	/**
	 * 
	 * @param ctx
	 * @throws Exception
	 */
	public void exportImportSOResult(ProcessContext ctx) throws Exception;
	
	
	//
	public void findSign52(ProcessContext ctx) throws Exception;
}
