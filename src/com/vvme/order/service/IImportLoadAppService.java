package com.vvme.order.service;

import java.util.Map;
import java.util.Set;

import com.cwc.db.DBRow;
import com.vvme.order.AppointmentObject;
import com.vvme.order.ProcessContext;

/**
 * 
 * @ClassName: IImportLoadAppService
 * @Description: 导入load app
 * @author yetl
 * @date 2015年4月29日
 *
 */
public interface IImportLoadAppService {
	
	/**
	 * excel数据导入临时表
	 * @param excelData
	 * @return
	 * @throws Exception 
	 */
	public void importOrdersToTemp(ProcessContext ctx) throws Exception;
	
	/**
	 * 
	 * @param ctx
	 * @return
	 * @throws Exception
	 */
	public Object [] findLoad(ProcessContext ctx) throws Exception;
	
	/**
	 * 
	 * @param cid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public Set<String> findSysMoreDn(String cid,long importer) throws Exception;
	
	/**
	 * 
	 * @param dns
	 * @param cid
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findOrderAndDetail(String [] dns,String cid) throws Exception;
	
	/**
	 * 
	 * @param ctx
	 * @param canApp
	 * @param exsitApp
	 * @param delOrders
	 * @return
	 * @throws Exception
	 */
	public Object[] addLoad(ProcessContext ctx,Map<String,Set<AppointmentObject>> canApp,Map<String,Set<AppointmentObject>> exsitApp,Set<String> delOrders) throws Exception;
	
	/**
	 * 
	 * @param set
	 * @throws Exception
	 */
	public void synWmsLoadApp(Set<String> set) throws Exception;
	
	/**
	 * 
	 * @param ctx
	 * @throws Exception
	 */
	public void exportImportLoadAppResult(ProcessContext ctx) throws Exception;
	
	//
	public void findSign4(String cid,long importer) throws Exception;
	public void findSign5(String cid,long importer) throws Exception;
	public void findSign9(String cid,long importer) throws Exception;
	public void findSign19(ProcessContext ctx) throws Exception;
	public void findSign26(String cid,long importer) throws Exception;
	public void findSign31(String cid,long importer) throws Exception;
	public void findSign32(String cid,long importer) throws Exception;
	public void findSign37(String cid,long importer) throws Exception;
	public void findSign38(String cid,long importer) throws Exception;
	public void findSign40(String cid,long importer) throws Exception;
	public void findSign45(String cid,long importer) throws Exception;
	public void findSign47(String cid,long importer) throws Exception;
	public void findSign50(ProcessContext ctx) throws Exception;
	public void findSign51(ProcessContext ctx) throws Exception;
	public void findSign53(String cid,long importer) throws Exception;
}
