package com.vvme.order.service;

import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import com.vvme.order.util.SerializeUtil;

/**
 * 
 * @ClassName: CacheService
 * @Description: 缓存redis的服务
 * @author yetl
 * @date 2015年4月27日
 *
 */
public class CacheService implements ICacheService {
	private StringRedisTemplate redisTemplate;

	public void setRedisTemplate(StringRedisTemplate redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public boolean cacheObject(String key, Object obj) {
		return cacheObject(key, obj, 60 * 5);
	}

	public boolean cacheObject(final String key, final Object obj,
			final long mills) {
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection)
					throws DataAccessException {
				RedisSerializer<String> serializer = redisTemplate
						.getStringSerializer();
				byte[] k = serializer.serialize(key);
				byte[] v = SerializeUtil.serialize(obj);
				connection.set(k, v);
				connection.expire(k, mills); // 5m 失效
				return true;
			}
		});

		return result;
	}

	public Object getObject(final String key) {
		return redisTemplate.execute(new RedisCallback<Object>() {
			public Object doInRedis(RedisConnection connection)
					throws DataAccessException {
				RedisSerializer<String> serializer = redisTemplate
						.getStringSerializer();
				byte[] k = serializer.serialize(key);
				byte[] bytes = connection.get(k);
				return SerializeUtil.unserialize(bytes);
			}
		});
	}

	public byte[] getByte(final String key) {
		return redisTemplate.execute(new RedisCallback<byte[]>() {
			public byte[] doInRedis(RedisConnection connection)
					throws DataAccessException {
				RedisSerializer<String> serializer = redisTemplate
						.getStringSerializer();
				byte[] k = serializer.serialize(key);
				return connection.get(k);
			}
		});
	}

	public long del(final String... keys) {
		return redisTemplate.execute(new RedisCallback<Long>() {
			public Long doInRedis(RedisConnection connection)
					throws DataAccessException {
				long result = 0;
				for (int i = 0; i < keys.length; i++) {
					result = connection.del(keys[i].getBytes());
				}
				return result;
			}
		});
	}

}
