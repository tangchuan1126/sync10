package com.vvme.order.service;

import com.cwc.db.DBRow;

/**
 * 
 * @ClassName: ICommonService
 * @Description: 
 * @author yetl
 * @date 2015年6月4日
 *
 */
public interface ICommonService {
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findAllTitle() throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findAllCompany() throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findAllScac(String scac) throws Exception;
}
