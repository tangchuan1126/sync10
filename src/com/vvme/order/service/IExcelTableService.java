package com.vvme.order.service;

import java.util.Map;

import com.cwc.db.DBRow;
import com.vvme.order.ExcelDataArray;
import com.vvme.order.FieldInfo;



/**
 * 
 * @ClassName: IExcelTableService
 * @Description: 模板映射
 * @author yetl
 * @date 2015年4月23日
 *
 */
public interface IExcelTableService {
	
	/**
	 * 缓存导入订单模板的key
	 */
	static String CACHE_ORDER_KEY="ORDER_ET:";
	
	/**
	 * 缓存导入load的key
	 */
	static String CACHE_LOAD_KEY="LOAD_ET:";
	
	/**
	 * 从数据中获取客户ID
	 * @param excelData
	 * @return
	 */
	public String getCid(ExcelDataArray excelData);
	
	/**
	 * 根据客户ID获取订单导出模板
	 * @param cid
	 * @return
	 * @throws Exception 
	 */
	public int getExportOrdersTemplateId(String cid) throws Exception;
	
	/**
	 * 根据客户ID获取订单导出模板
	 * @param cid
	 * @return
	 * @throws Exception 
	 */
	public DBRow getExportOrdersTemplate(String cid) throws Exception;
	
	/**
	 * 根据客户ID获取订单导入模板
	 * @param cid
	 * @return
	 * @throws Exception 
	 */
	public int getImportOrdersTemplateId(String cid) throws Exception;
	
	/**
	 * 根据客户ID获取更新load时导出的模板
	 * @param cid
	 * @return
	 */
	public int getExportLoadTemplateId(String cid);
	
	/**
	 * 根据客户ID获取更新load时导入的模板
	 * @param cid
	 * @return
	 * @throws Exception 
	 */
	public int getImportLoadTemplateId(String cid) throws Exception;
	
	/**
	 * 拿到当前客户的模板信息
	 * @param cid
	 * @return key:对应excel上列的序号 value:映射的具体信息
	 * @throws Exception 
	 */
	public Map<Integer,FieldInfo> getImportTemplateInfoByMid(int mid) throws Exception;
	
	/**
	 * 根据字段名字获取字段相关的信息
	 * @param fieldName
	 * @param tableName
	 * @return
	 * @throws Exception 
	 */
	public FieldInfo getFieldInfoByFieldName(String fieldName,String tableName,int mid) throws Exception;
	
	/**
	 * 
	 * @param fieldName
	 * @param tableName
	 * @return
	 * @throws Exception
	 */
	public FieldInfo getOrderFieldInfoByFieldName(String fieldName,String tableName) throws Exception;
	
	/**
	 * 
	 * @param fieldName
	 * @param tableName
	 * @return
	 * @throws Exception
	 */
	public FieldInfo getLoadFieldInfoByFieldName(String fieldName,String tableName) throws Exception;
}
