package com.vvme.order.service;

import com.cwc.app.floor.api.zj.FloorTitleMgrZJ;
import com.cwc.db.DBRow;
import com.vvme.order.dao.CarrierScacMcdotDao;
import com.vvme.order.dao.CompanyDicDao;

/**
 * 
 * @ClassName: CommonService
 * @Description: 
 * @author yetl
 * @date 2015年6月4日
 *
 */
public class CommonService implements ICommonService{
	private ICacheService cacheService;
	private FloorTitleMgrZJ floorTitleMgrZJ;
	private CompanyDicDao companyDicDao;
	private CarrierScacMcdotDao carrierScacMcdotDao;
	
	public void setCarrierScacMcdotDao(CarrierScacMcdotDao carrierScacMcdotDao) {
		this.carrierScacMcdotDao = carrierScacMcdotDao;
	}

	public void setCompanyDicDao(CompanyDicDao companyDicDao) {
		this.companyDicDao = companyDicDao;
	}

	public void setFloorTitleMgrZJ(FloorTitleMgrZJ floorTitleMgrZJ) {
		this.floorTitleMgrZJ = floorTitleMgrZJ;
	}
	
	public void setCacheService(ICacheService cacheService) {
		this.cacheService = cacheService;
	}

	public DBRow[] findAllTitle() throws Exception {
		DBRow []rows=floorTitleMgrZJ.findAllTitle();
		return rows;
	}

	public DBRow[] findAllCompany() throws Exception {
		return companyDicDao.find(null);
	}

	public DBRow[] findAllScac(String scac) throws Exception {
		if(scac!=null){
			DBRow para=new DBRow();
			para.put("scac", scac);
			return carrierScacMcdotDao.find(para);
		}else{
			return carrierScacMcdotDao.find(null);
		}
	}

}
