package com.vvme.order.service;

/**
 * 
 * @ClassName: ICacheService
 * @Description: 缓存服务
 * @author yetl
 * @date 2015年4月27日
 *
 */
public interface ICacheService {
	
	public boolean cacheObject(String key, Object obj);
	
	public boolean cacheObject(String key, Object obj,long mills);

	public Object getObject(String key);

	public byte[] getByte(String key);

	public long del(final String... keys);
}
