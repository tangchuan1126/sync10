package com.vvme.order.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.vvme.order.dao.B2bOrderDao;

/**
 * 
 * @ClassName: ExportOrdersService
 * @Description: 
 * @author yetl
 * @date 2015年4月30日
 *
 */
public class ExportOrdersService implements IExportOrdersService{
	private B2bOrderDao b2bOrderDao;
	
	public void setB2bOrderDao(B2bOrderDao b2bOrderDao) {
		this.b2bOrderDao = b2bOrderDao;
	}

	public DBRow[] expOrder(DBRow param) throws Exception {
		DBRow [] rows=b2bOrderDao.findExpOrder(param);
		
		final Map<String,String> ORDER_STATUS=new HashMap<String,String>();
		
		ORDER_STATUS.put("1", "Imported");
		ORDER_STATUS.put("2", "Committed");
		ORDER_STATUS.put("3", "Picking");
		ORDER_STATUS.put("4", "Packing");
		ORDER_STATUS.put("5", "Staging");
		ORDER_STATUS.put("6", "Staged");
		ORDER_STATUS.put("7", "Loading");
		ORDER_STATUS.put("8", "Closed");
		ORDER_STATUS.put("9", "Cancel");
		
		for (DBRow row : rows) {
			int ps_id = (Integer) row.getValue("send_psid");
			
			Date a0 = (Date) row.getValue("a0");
			if (a0 != null)
				row.put("a0", DateUtil.showLocationTimeFormat(a0, ps_id,"MM/dd/yyyy").substring(0, 10));
			
			Date a20 = (Date) row.getValue("a20");
			if (a20 != null)
				row.put("a20", DateUtil.showLocationTimeFormat(a20, ps_id,"MM/dd/yyyy").substring(0, 10));
			
			Date a21 = (Date) row.getValue("a21");
			if (a21 != null)
				row.put("a21", DateUtil.showLocationTimeFormat(a21, ps_id,"MM/dd/yyyy").substring(0, 10));
			
			Date a22 = (Date) row.getValue("a22");
			if (a22 != null)
				row.put("a22", DateUtil.showLocationTimeFormat(a22, ps_id,"MM/dd/yyyy").substring(0, 10));
			
			Date a23 = (Date) row.getValue("a23");
			if (a23 != null)
				row.put("a23", DateUtil.showLocationTimeFormat(a23, ps_id,"MM/dd/yyyy").substring(0, 10));
			
			Date a31 = (Date) row.getValue("a31");
			if (a31 != null){
				row.put("a30", DateUtil.showLocationTimeFormat(a31, ps_id,"MM/dd/yyyy").substring(0, 10));
				row.put("a31", DateUtil.showLocationTimeFormat(a31, ps_id,"MM/dd/yyyy HH:mm").substring(11, 16));
			}else{
				String a30 = row.getString("a30");
				row.put("a30", a30);
			}
			
			Date a32 = (Date) row.getValue("a32");
			if (a32 != null)
				row.put("a32", DateUtil.showLocationTimeFormat((Date) a32, ps_id,"MM/dd/yyyy").substring(0, 10));
			
			String order_status=row.getString("a33");
			row.put("a33", ORDER_STATUS.get(order_status));
		}
		
		return rows;
	}

}
