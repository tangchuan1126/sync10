package com.vvme.order.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.axis.utils.StringUtils;
import org.apache.log4j.Logger;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.iface.WMSOrderMgrIFace;
import com.cwc.app.key.B2BOrderKey;
import com.cwc.app.key.B2BOrderLogTypeKey;
import com.cwc.app.lucene.zyj.B2BOrderIndexMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.exception.SystemException;
import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelDataArray;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.ExcelRowResult.CellResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.SignManager;
import com.vvme.order.dao.B2bOrderDao;
import com.vvme.order.dao.B2bOrderItemDao;
import com.vvme.order.dao.B2bOrderLogDao;
import com.vvme.order.dao.CarrierScacMcdotDao;
import com.vvme.order.dao.CompanyDicDao;
import com.vvme.order.dao.CountryCodeDao;
import com.vvme.order.dao.CountryProvinceDao;
import com.vvme.order.dao.CustomerBrandDao;
import com.vvme.order.dao.CustomerIdDao;
import com.vvme.order.dao.ProductStorageCatalogDao;
import com.vvme.order.dao.ShipToDao;
import com.vvme.order.dao.TmpOrderLinesDao;
import com.vvme.order.dic.EnumUtils;
import com.vvme.order.excel.IExcelProcess;
import com.vvme.order.exception.ExcelDataBaseException;
import com.vvme.order.exception.ImportFileInvalidateException;
import com.vvme.order.exception.ImportTemplateInvalidateException;
import com.vvme.order.util.CommonUtils;
import com.vvme.order.util.ExcelUtils;
import com.vvme.order.validate.ExcelDataBaseValidate;
import com.vvme.order.validate.ExcelImportOrdersValidate;
import com.vvme.order.validate.ExcelSingleCustomerValidate;
import com.vvme.order.validate.ExcelTemplateValidate;
import com.vvme.order.validate.FreightTermsValidate;
import com.vvme.order.validate.FreightTypeValidate;
import com.vvme.order.validate.ValidateContext;

/**
 * 
 * @ClassName: ImportOrdersService
 * @Description: 导入订单服务
 * @author yetl
 * @date 2015年4月27日
 *
 */
public class ImportOrdersService implements IImportOrdersService{
	private static Logger log = Logger.getLogger("platform");
	
	private IExcelTableService excelTableService;
	private IExcelProcess excelProcess;

	private TmpOrderLinesDao tmpOrderLinesDao;
	private CompanyDicDao companyDicDao;
	private ProductStorageCatalogDao productStorageCatalogDao;
	private CountryCodeDao countryCodeDao;
	private CountryProvinceDao countryProvinceDao;
	private ShipToDao shipToDao;
	private B2bOrderDao b2bOrderDao;
	private B2bOrderLogDao b2bOrderLogDao;
	private B2bOrderItemDao b2bOrderItemDao;
	private WMSOrderMgrIFace proxyWMSOrderMgr;
	private CustomerBrandDao customerBrandDao;
	private CarrierScacMcdotDao carrierScacMcdotDao;
	private CustomerIdDao customerIdDao;
	
	public void setCustomerIdDao(CustomerIdDao customerIdDao) {
		this.customerIdDao = customerIdDao;
	}

	public void setCarrierScacMcdotDao(CarrierScacMcdotDao carrierScacMcdotDao) {
		this.carrierScacMcdotDao = carrierScacMcdotDao;
	}

	public void setCustomerBrandDao(CustomerBrandDao customerBrandDao) {
		this.customerBrandDao = customerBrandDao;
	}

	public void setCompanyDicDao(CompanyDicDao companyDicDao) {
		this.companyDicDao = companyDicDao;
	}

	public void setProductStorageCatalogDao(
			ProductStorageCatalogDao productStorageCatalogDao) {
		this.productStorageCatalogDao = productStorageCatalogDao;
	}

	public void setCountryCodeDao(CountryCodeDao countryCodeDao) {
		this.countryCodeDao = countryCodeDao;
	}

	public void setCountryProvinceDao(CountryProvinceDao countryProvinceDao) {
		this.countryProvinceDao = countryProvinceDao;
	}

	public void setShipToDao(ShipToDao shipToDao) {
		this.shipToDao = shipToDao;
	}

	public void setB2bOrderDao(B2bOrderDao b2bOrderDao) {
		this.b2bOrderDao = b2bOrderDao;
	}

	public void setB2bOrderLogDao(B2bOrderLogDao b2bOrderLogDao) {
		this.b2bOrderLogDao = b2bOrderLogDao;
	}

	public void setB2bOrderItemDao(B2bOrderItemDao b2bOrderItemDao) {
		this.b2bOrderItemDao = b2bOrderItemDao;
	}

	public void setProxyWMSOrderMgr(WMSOrderMgrIFace proxyWMSOrderMgr) {
		this.proxyWMSOrderMgr = proxyWMSOrderMgr;
	}

	public void setExcelProcess(IExcelProcess excelProcess) {
		this.excelProcess = excelProcess;
	}
	
	public void setExcelTableService(IExcelTableService excelTableService) {
		this.excelTableService = excelTableService;
	}

	public void setTmpOrderLinesDao(TmpOrderLinesDao tmpOrderLinesDao) {
		this.tmpOrderLinesDao = tmpOrderLinesDao;
	}
	
	public String getCidByName(String cname) throws Exception {
		DBRow row=customerBrandDao.getByCustomer(cname);
		if(row==null)
			return null;
		return row.getString("cb_id");
	}
	
	public boolean insertExcelDataToTemp(ProcessContext ctx) throws Exception {
		String cid=ctx.getCid();
		long importer=ctx.getUser().getAdid();
		
		tmpOrderLinesDao.delLinesTmp(cid, importer);
		String [][] datas=ctx.getExcelData().getDataArray();
		int [] rows=ctx.getExcelData().getDataRows();
		Map<Integer,FieldInfo> fields=excelTableService.getImportTemplateInfoByMid(ctx.getImpId());
		
		for(int i=0;i<datas.length;i++){
			String [] hData=datas[i];
			
			DBRow row=new DBRow();
			row.add("customer_id", cid);
			row.add("importer", importer);
			row.add("import_date", new Date());
			row.add("row", rows[i]);
			
			for(int j=0;j<hData.length;j++){
				FieldInfo fInfo=fields.get(j);
				
				if(null==fInfo)
					continue;
				
				ExcelUtils.addRowData(row,fInfo,hData[j]);
			}
			
			tmpOrderLinesDao.save(row);
		}
		
		return true;
	}
	
	public void importOrdersToTemp(ProcessContext ctx) throws Exception {
		ValidateContext validateContext=new ValidateContext(new ExcelTemplateValidate());
		
		//导入前需要校验模板的都得校验
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ImportTemplateInvalidateException("The file you imported doesn't comply the format of template");
			}
		}
		
		if(ctx.getExcelData()==null){
			ExcelDataArray excelData=this.excelProcess.importExcelFileToDataArray(ctx.getPath());
			ctx.setExcelData(excelData);
		}
		
		ExcelUtils.removeEmptyLine(ctx.getExcelData());
		if(ctx.getExcelData().getDataArray().length==0){
			throw new Exception("You are importing empty file!");
		}
		
		ctx.getResp().setTotal(ctx.getExcelData().getDataArray().length);
		
		//导入文件校验
		validateContext=new ValidateContext(new ExcelImportOrdersValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ImportFileInvalidateException(ExcelConstants.TIP_IMPORT_FILE_ERR_MSG);
			}
		}
		
		//基本类型校验
		validateContext=new ValidateContext(new ExcelDataBaseValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ExcelDataBaseException("Invalid information found, please download the report to review the errors.  <br/>  "
						+ "Cells with incorrect information will be highlighted in yellow, please move your cursor over for more");
			}
		}
		
		validateContext=new ValidateContext(new FreightTermsValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ExcelDataBaseException("Invalid information found, please download the report to review the errors.  <br/>  "
						+ "Cells with incorrect information will be highlighted in yellow, please move your cursor over for more");
			}
		}
		
		validateContext=new ValidateContext(new FreightTypeValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ExcelDataBaseException("Invalid information found, please download the report to review the errors.  <br/>  "
						+ "Cells with incorrect information will be highlighted in yellow, please move your cursor over for more");
			}
		}
		
		//是否导入的是同一个客户的单子
		validateContext=new ValidateContext(new ExcelSingleCustomerValidate());
		if(validateContext.openCheck()){
			if(!validateContext.validate(ctx)){
				throw new ExcelDataBaseException("Invalid information found, please download the report to review the errors.  <br/>  "
						+ "Cells with incorrect information will be highlighted in yellow, please move your cursor over for more");
			}
		}
		
		//导入临时表
		insertExcelDataToTemp(ctx);
	}

	public DBRow[] findSign2(String cid, long importer) throws Exception {
		return tmpOrderLinesDao.findSign2(cid, importer);
	}

	public DBRow[] find(String dn, String cid, long importer) throws Exception {
		DBRow para=new DBRow();
		para.put("dn", dn);
		para.put("customer_id", cid);
		para.put("importer", importer);
		return tmpOrderLinesDao.find(para);
	}

	public void update(String dn, String cid, long importer, int sign)
			throws Exception {
		DBRow para=new DBRow();
		para.put("correct", sign);
		tmpOrderLinesDao.updateByDn(para, dn,cid,importer);
	}

	public DBRow[] findByDn(String dn, String cid, long importer)
			throws Exception {
		if(CommonUtils.isElementCustomer(cid))
			return tmpOrderLinesDao.findByDn0(dn,cid,importer);
		else
			return tmpOrderLinesDao.findByDn(dn,cid,importer);
	}

	public void findSign8(String cid, long importer) throws Exception {
		DBRow [] rows=tmpOrderLinesDao.findSign8(cid,importer);
		for(DBRow row:rows){
			String dn=row.getString("dn");
			String delivery_plant=row.getString("delivery_plant");
			
			DBRow r=companyDicDao.getCompanyId(delivery_plant,cid);
			if(r==null){
				DBRow para=new DBRow();
				para.put("correct", ExcelConstants.ERROR_TYPE_8);
				tmpOrderLinesDao.updateByDn(para,dn,cid,importer);
				continue;
			}
		}
	}
	
	public void findSign11(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign11(cid,importer);
	}

	public void findSign17(String cid, long importer) throws Exception {
	}
	
	public void findSign20(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign20(cid, importer);
	}
	
	public void findSign35(String cid, long importer) throws Exception {
		DBRow [] rows=tmpOrderLinesDao.findSign35(cid, importer);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		for(DBRow row:rows){
			String shipDateStr=row.getString("req_ship_date");
			String ps_id=row.getString("ps_id");
			String dn=row.getString("dn");
			
			String nowStr=DateUtil.showLocationTime(new Date(), Long.parseLong(ps_id));
			Date shipDate=sdf.parse(shipDateStr);
			Date now=sdf.parse(nowStr);
			
			if(shipDate.getTime()<now.getTime()){
				tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_35);
			}
		}
	}

	public void findSign36(String cid, long importer) throws Exception {
		DBRow [] rows=tmpOrderLinesDao.findSign36(cid, importer);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		for(DBRow row:rows){
			String mabdStr=row.getString("mabd");
			String ps_id=row.getString("ps_id");
			String dn=row.getString("dn");
			
			String nowStr=DateUtil.showLocationTime(new Date(), Long.parseLong(ps_id));
			Date mabd=sdf.parse(mabdStr);
			Date now=sdf.parse(nowStr);
			
			if(mabd.getTime()<now.getTime()){
				tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_36);
			}
		}
	}

	public void findSign43(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign43(cid, importer);
	}
	
	public void findSign44(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign44(cid, importer);
	}

	public void findSign45(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign45(cid, importer);
	}

	public void findSign46(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign46(cid, importer);
	}
	
	public void findSign54(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign54(cid, importer);
	}

	public void findSign55(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign55(cid, importer);
	}

	public void findSign56(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign56(cid, importer);
	}

	public void findSign57(String cid, long importer) throws Exception {
		tmpOrderLinesDao.findSign57(cid, importer);
	}

	public void findSignOther(String cid, long importer) throws Exception {
		DBRow [] rows=tmpOrderLinesDao.findSignOther(cid,importer);
		
		for(DBRow r:rows){
			String l_pro_id=r.getString("pro_id_cc");
			String l_customer_key=r.getString("customer_key");
			String l_product_id=r.getString("product_id");
			String l_isright=r.getString("isright");
			String l_b2b_oid=r.getString("b2b_oid");
			String l_ship_to_party_name=r.getString("ship_to_party_name");
			String l_ship_to_city=r.getString("ship_to_city");
			String l_country=r.getString("country");
			if(!StrUtil.isBlank(l_ship_to_city)){
				l_ship_to_city=l_ship_to_city.replaceAll("\\s*", "");
			}
			String l_ship_to_zip_code=r.getString("ship_to_zip_code");
			String l_order_type=r.getString("order_type");
			String l_bill=r.getString("bill_to");
			String l_ft=r.getString("ft");
			String l_retailerid=r.getString("retailerid");
			String dn=r.getString("dn");

			DBRow db=tmpOrderLinesDao.findDNOtherErr(dn,cid,importer);
			//去掉 一个dn对应多条明细部分sku能对应上,部分sku对应不上的问题 这种明细不处理只标记出来
			if(Integer.parseInt(db.getString("cnt"))>0){
				continue;
			}
			
			if(StringUtils.isEmpty(l_b2b_oid) ){ //新订单
				
				if(CommonUtils.isUnitedStates(l_country)){
					if(!CommonUtils.isUnitedStatesZipCode(l_ship_to_zip_code)){
						tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_58);
						continue;
					}
				}
				
				if(StringUtils.isEmpty(l_pro_id)){
					tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_6);
					continue;
				}
				
				if(EnumUtils.isThirdParty(l_ft.trim()) && StringUtils.isEmpty(l_bill)){
					tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_27);
					continue;
				}
				
				if(!StringUtils.isEmpty(l_bill)){//为空默认账单地址寄给Customer
					DBRow para=new DBRow();
					para.put("customer_id", l_bill);
					DBRow[] cbRow=customerIdDao.find(para);
					if(cbRow!=null && cbRow.length>0){
						String ckey=cbRow[0].getString("customer_key");
						
						para=new DBRow();
						para.put("storage_type", 7);
						para.put("storage_type_id", ckey);
						
						DBRow [] $1=productStorageCatalogDao.find(para);
						if($1!=null && $1.length==1){
						}else if($1!=null && $1.length>1){
							tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_60);
							continue;
						}else{
							tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_27);
							continue;
						}
					}else{
						para.clear();
						para.put("ship_to_name", l_bill);
						DBRow [] $1=shipToDao.find(para,new String[]{"ship_to_id"});
						
						if($1!=null && $1.length>0){
							para.clear();
							para.put("storage_type", 5);
							para.put("storage_type_id", $1[0].getString("ship_to_id"));
							para.put("is_bill_dc", 1);
							
							$1=productStorageCatalogDao.find(para,new String[]{"id"});
							if($1==null || $1.length==0){
								tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_27);
								continue;
							}else if($1!=null && $1.length>1){
								tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_60);
								continue;
							}
						}else{ //可能是carrier
							para.clear();
							para.put("carrier", l_bill);
							$1=carrierScacMcdotDao.find(para,new String[]{"id"});
							
							if($1!=null && $1.length>0){
								para.clear();
								para.put("storage_type", 8);
								para.put("storage_type_id", $1[0].getString("id"));
								
								$1=productStorageCatalogDao.find(para,new String[]{"id"});
								if($1==null || rows.length==0){
									tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_27);
									continue;
								}else if($1!=null && $1.length>1){
									tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_60);
									continue;
								}
							}else{
								tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_27);
								continue;
							}
						}
					}
				}else{
					DBRow para=new DBRow();
					para.put("storage_type", 7);
					para.put("storage_type_id", l_customer_key);
					
					DBRow [] $2=productStorageCatalogDao.find(para);
					if($2!=null && $2.length>0){
						if($2.length>1){
							tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_60);
							continue;
						}
					}else{
						tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_27);
						continue;
					}
				}
				
				if(!EnumUtils.isEndUser(l_order_type)){
					if((!StringUtils.isEmpty(l_order_type) && StringUtils.isEmpty(l_retailerid) && StringUtils.isEmpty(l_product_id))
							|| (StringUtils.isEmpty(l_order_type) && StringUtils.isEmpty(l_product_id))){
						tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_48);
						continue;
					}
					
					//根据shiptopartyname没有完全匹配到
					if(StringUtils.isEmpty(l_product_id)){
						tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_13);
						continue;
					}
					if(!StringUtils.isEmpty(l_product_id) && !StringUtils.isEmpty(l_isright)){
						tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_4);
						continue;
					}else if(!StringUtils.isEmpty(l_product_id) && StringUtils.isEmpty(l_isright)){
//							DBRow [] datas=productStorageCatalogDao.find(l_ship_to_party_name,l_ship_to_city);
//							
//							if(datas.length==1){
//							}else{
//								tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_7);
//								continue;
//							}
							
							DBRow [] datas=productStorageCatalogDao.find(l_ship_to_party_name,l_ship_to_city,l_ship_to_zip_code);
							
							if(datas.length==1){
							}else{
								tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_33);
								continue;
							}
							
							if(!StringUtils.isEmpty(l_retailerid)){
								datas=productStorageCatalogDao.find(l_ship_to_party_name,l_ship_to_city
										,l_ship_to_zip_code,l_retailerid);
								
								if(datas.length==1){
								}else{
									tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_49);
									continue;
								}
							}else{
								tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_48);
							}
							
//							datas=productStorageCatalogDao.find(l_ship_to_party_name,l_ship_to_city
//									,l_ship_to_zip_code,l_retailerid,l_ship_to_address);
//							
//							if(datas.length==0){
//								tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_34);
//								continue;
//							}
					}
				}else{
					tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_4);
					continue;
				}
			}else{
				tmpOrderLinesDao.updateSign(dn,cid,importer,ExcelConstants.ERROR_TYPE_11);
			}
		}
	}

	public void exportImportOrdersResult(ProcessContext ctx) throws Exception {
		String cid=ctx.getCid();
		long importer=ctx.getUser().getAdid();
		
		DBRow para=new DBRow();
		para.put("customer_id", cid);
		para.put("importer", importer);
		DBRow [] rows=tmpOrderLinesDao.find(para,new String[]{"dn","correct","row"});
		int [] dataRows=ctx.getExcelData().getDataRows();
		String [][]data=ctx.getExcelData().getDataArray();
		FieldInfo shipAddr=excelTableService.getOrderFieldInfoByFieldName("ship_to_address", ExcelConstants.EXCEL_MODEL_TABLE_IMPORT);
		
		for(DBRow m:rows){
			int row=(Integer)m.getValue("row");
			int errType=(Integer)m.getValue("correct");
			String dn=m.getString("dn");
			
			ExcelRowResult bean=new ExcelRowResult();
			bean.setDn(dn);
			if(SignManager.canImportSign(errType)){
				bean.setRight(1);
			}
			
			int index=ExcelUtils.getIndex(dataRows, row);
			bean.setData(data[index]);
			
			if(errType==ExcelConstants.ERROR_TYPE_34){
				para.clear();
				
				para.put("dn", dn);
				para.put("customer_id", cid);
				para.put("importer", importer);
				
				DBRow [] _row=tmpOrderLinesDao.find(para, new String[]{"ship_to_party_name","order_type"});
				DBRow shipToRow=getShipToId(_row[0].getString("order_type"));
				DBRow db=productStorageCatalogDao.getByTitle(_row[0].getString("ship_to_party_name"),Integer.parseInt(shipToRow.getString("ship_to_id")));
				
				CellResult cellRst=bean.createCellResult();
				cellRst.setLine(shipAddr.getExcelSn());
				cellRst.setMsg("System value:"+db.getString("deliver_street"));
				bean.addCellResult(cellRst);
			}
			
			bean.setRow(row);
			bean.setSign(errType);
			bean.setMsg(ExcelConstants.ERROR_MAP.get(errType));
			
			ctx.addExcelRowResult(bean);
		}
		
	}
	
	public DBRow[] findImportOrders(String cid, long importer) throws Exception {
		return tmpOrderLinesDao.getCanImportOrders(importer,cid);
	}

	public synchronized Set<Long> ImportOrders(ProcessContext ctx) throws Exception {
		Set<Long> b2bOids=new HashSet<Long>();
		String cid=ctx.getCid();
		AdminLoginBean user=ctx.getUser();
		long importer=ctx.getUser().getAdid();
		
		//查出可以处理的正确的数据
		DBRow [] rows=tmpOrderLinesDao.getCanImportOrders(importer,cid);
		for(DBRow r:rows){
			String dn=r.getString("dn");
			int correct=(Integer)r.getValue("correct");
			
			if(correct==4 || correct==13 || correct==34){
				long b2b_oid=importOrder(dn,cid,correct,user);
				b2bOids.add(b2b_oid);
			}else{
				log.info("导入订单时获取订单的转态出现非4 13 34转态码");
			}
		}
		
		return b2bOids;
	}
	
	/**
	 * 
	 */
	public synchronized long importOrder(String dn,String cid,int sign,AdminLoginBean user) throws Exception{
			long adid=user.getAdid();
			DBRow para=new DBRow();
			para.put("cb_id", cid);
			DBRow[] cbRow=customerBrandDao.find(para);
			String ckey=cbRow[0].getString("customer_key");
			//以下所有操作都不用校验直接查询ID,如有问题修改校验部分
			DBRow [] rows=tmpOrderLinesDao.getOrderTmpByDn(dn,cid,adid);
			DBRow data=rows[0];
			
			String shipToState=data.getString("ship_to_state");
			String country=data.getString("country");
			String billTo=data.getString("bill_to");
			String shipToAddress=data.getString("ship_to_address");
			String shipToCity=data.getString("ship_to_city");
			String shipTo=data.getString("ship_to_party_name").trim();
			String deliver_zip_code=data.getString("ship_to_zip_code");
			String order_type=data.getString("order_type");
			shipToAddress=CommonUtils.formatShipToAddress(shipToAddress);//地址去掉空格
			
			long receive_psid=0;
			String contact="";
			String telphone="";
			int storage_type=0;
			int shipToId=0;
			String sendStorage="";
			int ps_id=0;
			
			int ccid=getCcid(country);
			long pro_id=getDeliverProId(shipToState,ccid); //通过州p_code和国家id获取州id

			String  delivery_plant=data.getString("delivery_plant");
			DBRow hubRow=companyDicDao.getCompanyId(delivery_plant,cid);
			ps_id=Integer.parseInt(hubRow.getString("ps_id"));
			
			if(!EnumUtils.isEndUser(order_type)){
				DBRow shipToRow=getShipToId(order_type);
				if(shipToRow==null){
					throw new Exception("Check the parts into the module problems, problems with the data to be imported, please contact the administrator!");
				}
				String ship_to_id=shipToRow.getString("ship_to_id");
				if(!StringUtils.isEmpty(ship_to_id) && !"null".equals(ship_to_id))
					shipToId=Integer.parseInt(ship_to_id);
				
				if(sign==4){
					DBRow row=productStorageCatalogDao.find(shipTo,shipToAddress,shipToCity,pro_id,ship_to_id); //通过shipTo校验地址是否有改变
					receive_psid=Long.parseLong(row.getString("id"));
					contact=row.getString("contact");
					telphone=row.getString("phone");
					storage_type=5;
				}else{ //模糊匹配查询出来
					try{
						receive_psid=insertReceivePsInfo(shipTo,shipToAddress,shipToCity,pro_id,ccid,deliver_zip_code,shipToId);
					}catch(Exception e){
						e.printStackTrace();
						log.error("create product error:"+e.getMessage());
						throw new Exception("The failure to create product storage catalog.");
					}
				}
			}else{
				shipToId=-1;
				receive_psid=ps_id;
			}
			
			DBRow row=new DBRow();

			DBRow r=productStorageCatalogDao.getById(ps_id);
			row.put("from_ps_type", r.getString("storage_type"));
			sendStorage=r.getString("title");
			
			String ship_not_before=data.getString("ship_not_before");
			String ship_not_later=data.getString("ship_not_later");
			String ship_to_date=data.getString("ship_date");
			String mabd=data.getString("mabd");
			String req_ship_date=data.getString("req_ship_date");
			String order_date=data.getString("order_date");
			
			if(!StringUtils.isEmpty(order_date)){
				CommonUtils.dateToUTC(row,ps_id,order_date,"customer_order_date");
			}
			
			CommonUtils.dateToUTC(row,ps_id,ship_to_date,"deliveryed_date");
			CommonUtils.dateToUTC(row,ps_id,mabd,"mabd");
			CommonUtils.dateToUTC(row,ps_id,req_ship_date,"requested_date");
			CommonUtils.dateToUTC(row,ps_id,ship_not_before,"ship_not_before");
			CommonUtils.dateToUTC(row,ps_id,ship_not_later,"ship_not_later");
			
			row.put("customer_dn", data.getString("dn"));
			row.put("deliver_pro_id", pro_id);
			row.put("deliver_ccid", ccid);
			row.put("account_id", shipToId);
			row.put("ship_to_party_name", shipTo);
			row.put("deliver_house_number", shipToAddress);
			row.put("b2b_order_address", shipToAddress);
			row.put("deliver_city", shipToCity);
			row.put("address_state_deliver", shipToState);
			row.put("deliver_zip_code", deliver_zip_code);
			row.put("send_psid", ps_id);
			row.put("company_id",data.getString("delivery_plant"));
			row.put("order_type", order_type);
			row.put("customer_id", data.getString("customer_id"));
			row.put("freight_carrier", data.getString("freight_type").toUpperCase());
			row.put("bol_tracking", data.getString("bol_tracking"));
			row.put("receive_psid", receive_psid);
			row.put("b2b_order_linkman", contact);
			row.put("b2b_order_linkman_phone", telphone);
			row.put("b2b_order_status", 1);
			row.put("b2b_order_date", new Date());
			row.put("create_account", user.getEmploye_name());
			row.put("create_account_id", user.getAdid());
			row.put("purchase_id", 0);
			if(CommonUtils.isElementCustomer(cid)){
				row.put("so", data.getString("order_number"));
			}
			row.put("target_ps_type", storage_type);
			row.put("put_status", 1);
			row.put("freight_term", EnumUtils.transFreightType(data.getString("ft")).toUpperCase());
			row.put("source_type", 1);
			
			importBillInfo(billTo,ckey,row);
			
			//插入前检查是否有其他人员同时导入
			boolean pass=checkOrder(dn,cid,adid);
			
			if(!pass){
				throw new Exception("与其他用户导入操作有冲突");
			}
			
			long b2b_oid=b2bOrderDao.add(row);
			if((Date)row.getValue("eta")==null){
				log(b2b_oid,"create order",null,B2BOrderLogTypeKey.Create,B2BOrderKey.READY,user);
			}else{
				log(b2b_oid,"create order",DateUtil.DatetimetoStr((Date)row.getValue("eta")),B2BOrderLogTypeKey.Create,B2BOrderKey.READY,user);
			}
			String po=importOrderLines(b2b_oid,dn,cid,user.getAdid());
			tmpOrderLinesDao.updateSign(dn, cid, user.getAdid(), ExcelConstants.ERROR_TYPE_17);

			//添加索引
			B2BOrderIndexMgr.getInstance().addIndex(b2b_oid, sendStorage, shipTo, shipToAddress, "", "", data.getString("dn"),po);
			return b2b_oid;
	}
	
	/**
	 * 导入账单信息
	 * @throws Exception 
	 */
	private void importBillInfo(String billTo,String defaultId,DBRow target) throws Exception{
		if(!StringUtils.isEmpty(billTo)){//为空默认账单地址寄给Customer
			DBRow para=new DBRow();
			para.put("customer_id", billTo);
			DBRow[] cbRow=customerIdDao.find(para);
			if(cbRow!=null && cbRow.length>0){
				String ckey=cbRow[0].getString("customer_key");
				
				para=new DBRow();
				para.put("storage_type", 7);
				para.put("storage_type_id", ckey);
				
				DBRow [] rows=productStorageCatalogDao.find(para);
				if(rows!=null && rows.length>0){
					if(rows.length>1)
						throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
					
					setBillInfo(target,rows[0],billTo);
				}else{
					throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
				}
			}else{
				para.clear();
				
				para.add("ship_to_name", billTo);
				DBRow [] rows=shipToDao.find(para,new String[]{"ship_to_id"});
				
				if(rows!=null && rows.length>0){
					if(rows.length>1)
						throw new RuntimeException("ship_to find multi data by "+para.toString());
					
					para.clear();
					para.put("storage_type_id", rows[0].getString("ship_to_id"));
					para.put("storage_type", 5);
					para.put("is_bill_dc", 1);
					
					rows=productStorageCatalogDao.find(para);
					
					if(rows.length>1)
						throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
					
					setBillInfo(target,rows[0],billTo);
				}else{ //可能是carrier
					para.clear();
					para.put("carrier", billTo);
					rows=carrierScacMcdotDao.find(para,new String[]{"id"});
					
					if(rows!=null && rows.length>0){
						para.clear();
						para.put("storage_type", 8);
						para.put("storage_type_id", rows[0].getString("id"));
						
						rows=productStorageCatalogDao.find(para);
						if(rows.length>1)
							throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
						
						setBillInfo(target,rows[0],billTo);
					}else{
						throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
					}
				}
			}
		}else{
			DBRow para=new DBRow();
			para.put("storage_type", 7);
			para.put("storage_type_id", defaultId);
			
			DBRow [] rows=productStorageCatalogDao.find(para);
			if(rows!=null && rows.length>0){
				if(rows.length>1)
					throw new RuntimeException("product_storageCata_log find multi data by "+para.toString());
				
				setBillInfo(target,rows[0],billTo);
			}
		}
	}
	
	private void setBillInfo(DBRow target,DBRow product,String billTo) throws NumberFormatException, Exception{
		target.put("bill_to_id",product.getString("id"));
		target.put("bill_to_name",billTo);
		target.put("bill_to_address1",product.getString("deliver_house_number"));
		target.put("bill_to_address2",product.getString("deliver_street"));
		target.put("bill_to_city",product.getString("deliver_city"));
		
		String ccid=product.getString("deliver_nation");
		String proid=product.getString("deliver_pro_id");
		if(!StringUtils.isEmpty(ccid)){
			DBRow ccRow=countryCodeDao.getByCcid(Integer.parseInt(ccid));
			if(ccRow!=null){
				target.put("bill_to_country", ccRow.getString("c_country"));
			}
		}
		
		if(!StringUtils.isEmpty(proid)){
			DBRow proRow=countryProvinceDao.getDetailProByProId(Integer.parseInt(proid));
			if(proRow!=null)
				target.put("bill_to_state", proRow.getString("p_code"));
		}
		
		target.put("bill_to_zip_code",product.getString("deliver_zip_code"));
		target.put("bill_to_contact",product.getString("deliver_contact"));
		target.put("bill_to_phone",product.getString("deliver_phone"));
		target.put("bill_to_home",(byte)0);
	}
	
	/**
	 * 导入主单据明细 
	 */
	private String importOrderLines(long b2b_oid,String dn, String cid, long importer) throws Exception{
		DBRow [] lines=tmpOrderLinesDao.findOrderLines(dn,cid,importer);
		StringBuilder sb=new StringBuilder();
		int i=1;
		for(DBRow line:lines){
			DBRow item=new DBRow();
	
			item.add("line_no", i);
			item.add("title", line.getString("title"));
			item.add("b2b_pc_id", line.getString("pc_id"));
			item.add("material_number", line.getString("material_number"));
			item.add("lot_number", line.getString("material_number"));
			item.add("blp_type_id", 0);
			item.add("clp_type_id", 0);
			item.add("b2b_volume", 0);
			item.add("b2b_normal_count", 0);
			item.add("b2b_union_count", 0);
			item.add("b2b_p_code", " ");
			item.add("b2b_delivery_count", line.getString("delivery_count"));
			item.add("b2b_send_count", 0);
			item.add("b2b_reap_count", 0);
			item.add("b2b_p_name", line.getString("b2b_p_name"));
			item.add("item_id", line.getString("model_number"));
			item.add("b2b_count", line.getString("b2b_count","0"));
			item.add("b2b_wait_count", line.getString("b2b_wait_count","0"));
			item.add("b2b_weight", line.getString("b2b_weight","0"));
			item.add("total_weight", line.getString("total_weight","0"));
			item.add("pallet_spaces", line.getString("pallet_spaces","0"));
			item.add("boxes", line.getString("boxes","0"));
			item.add("note", line.getString("note"));
			item.add("b2b_oid", b2b_oid);
			item.put("so", line.getString("order_number"));
			item.put("retail_po", line.getString("retail_po"));
			
			b2bOrderItemDao.add(item);
			i++;
			sb.append(line.getString("retail_po")).append(" ");
		}
		
		return sb.toString();
	}
	
	/**
	 * 查找对应国家的ID
	 * @param country
	 * @return
	 * @throws Exception
	 */
	private int getCcid(String country) throws Exception{
		DBRow row=countryCodeDao.findCcid(country);
		int ccid=0;
		
		try{
			ccid=Integer.parseInt(row.getString("ccid"));
		}catch(Exception e){
			log.debug("getDeliverProId-->country:["+country+"]国家录入不正确");
			throw new Exception(country+"没有找到国家ID");
		}
		
		return ccid;
	}
	
	/**
	 * 根据国家州获取 州id
	 * @param shipToState
	 * @param nation_id
	 * @return 州的id
	 * @throws Exception
	 */
	private int getDeliverProId(String shipToState,long ccid) throws Exception{
		DBRow row=countryProvinceDao.getDetailCountryProvinceByProvinceCode(shipToState, ccid);
		if(null==row){
			log.debug("getDeliverProId-->p_code:["+shipToState+"],country:["+ccid+"]没有找到对应的州ID");
			throw new Exception("没有找到对应的州");
		}
		return Integer.parseInt(row.getString("pro_id"));
	}
	
	private DBRow getShipToId(String shipName) throws Exception{
		DBRow [] rows=shipToDao.getShipToId(shipName);
		if(rows==null || rows.length==0)
			return null;
		return rows[0];
	}
	
	private boolean checkOrder(String dn,String cid,long importer) throws Exception{
		int cnt=b2bOrderDao.checkOrder(dn,cid,importer);
		if (cnt > 0) {
			tmpOrderLinesDao.updateSign12(dn, cid, importer);
			return false;
		}
		
		return true;
	}
	
	/**
	 * 插入新的仓库信息并返回ID
	 * @param shipToAddress
	 * @param shipToCity
	 * @param pro_id
	 * @param nation_id
	 * @return
	 * @throws Exception
	 */
	private synchronized long insertReceivePsInfo(String shipTo,String shipToAddress,String shipToCity,long pro_id,
			long nation_id,String deliver_zip_code,int shipToId) throws Exception{
		//先校验是否在插入前本次有其他lines已经导入过此仓库信息
		DBRow dbrow=productStorageCatalogDao.getByTitle(shipTo,shipToId);
		
		if(dbrow!=null){
			return Long.parseLong(dbrow.getString("id"));
		}
		
		DBRow row=new DBRow();
		
		row.add("deliver_address", shipToAddress);
		row.add("deliver_house_number", shipToAddress);
		row.add("send_house_number", shipToAddress);
		row.add("deliver_city", shipToCity.trim());
		row.add("city", shipToCity.trim());
		row.add("deliver_pro_id", pro_id);
		row.add("deliver_nation", nation_id);
		row.add("native", nation_id);
		row.add("title", shipTo.trim());
		row.add("deliver_zip_code", deliver_zip_code);
		row.add("storage_type_id", shipToId);
		row.add("storage_type", 5);
		
		return productStorageCatalogDao.add(row);
	}
	
	//记录日志
	private void log(long b2b_oid,String b2b_content,String eta,int b2b_type,int stage,AdminLoginBean adminLoginBean) throws Exception{
		try {
			long adid=adminLoginBean.getAdid();
			String employe_name=adminLoginBean.getEmploye_name();
			
			TDate tDate = new TDate();
			String currentTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
			DBRow row = new DBRow();
			row.add("b2b_oid", b2b_oid);
			row.add("b2b_content", b2b_content);
			row.add("b2ber_id", adid);
			row.add("b2ber", employe_name);
			row.add("b2b_type", b2b_type);
			row.add("activity_id", stage);
			if (eta != null && eta.trim().length() > 0) {
				row.add("time_complete", eta);
			}
			row.add("b2b_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
			b2bOrderLogDao.add(row);
			
			// 同时更新 主信息上的 updatedate,updateby(id),updatename。
			DBRow b2BOrderRow = new DBRow();
			b2BOrderRow.add("updatedate", currentTime);
			b2BOrderRow.add("updateby", adid);
			b2BOrderRow.add("updatename", employe_name);
			b2bOrderDao.updateById(b2b_oid, b2BOrderRow);
			
		} catch (Exception e) {
			throw new SystemException(e, "insertLogs", log);
		}
	}
	
	public void synWmsOrders(Set<Long> set,AdminLoginBean user) throws Exception {
		if(set.isEmpty())
			return;
	
		for(Long b2boid:set){
			DBRow row=b2bOrderDao.get(b2boid);
			DBRow[] orderlines = b2bOrderItemDao.getByOid(b2boid);
			try{
				proxyWMSOrderMgr.save(row, orderlines);
			}catch(RuntimeException e){
				log(b2boid,"import WMS fail",null,B2BOrderLogTypeKey.Create,B2BOrderKey.READY,user);
			}
		}
	}

	public Set<Integer> findSign17Row(String cid, long importer) throws Exception {
		DBRow [] rows=tmpOrderLinesDao.getCorrectDataRow(importer,cid,new Integer[]{ExcelConstants.ERROR_TYPE_17});
		Set<Integer> set=new TreeSet<Integer>();
		for(DBRow row:rows){
			String lineNo=row.getString("row");
			if(!StringUtils.isEmpty(lineNo))
				set.add(Integer.parseInt(lineNo));
		}
		return set;
	}

}
