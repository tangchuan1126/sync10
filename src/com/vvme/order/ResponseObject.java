package com.vvme.order;


import java.io.Serializable;
import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: ResponseObject
 * @Description: 响应对象
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ResponseObject implements Serializable{
	private static final long serialVersionUID = 6587674283237435938L;
	private int code;
	private String msg;
	private String fileId;
	private String filename;
	private int successCount; //导入成功
	private int failCount; //导入失败
	private int total; //excel所有数据
	private int warning; //0:没警告 1:有警告;
	
	public ResponseObject(){
		super();
		this.code=ExcelConstants.HTTP_CODE_OK;
	}
	
	public ResponseObject(int code){
		super();
		this.code=code;
	}
	
	public ResponseObject(int code,String msg){
		this(code);
		this.msg=msg;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public int getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}
	public int getFailCount() {
		return failCount;
	}
	public void setFailCount(int failCount) {
		this.failCount = failCount;
		if(this.failCount>0)
			this.warning=1;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getWarning() {
		return warning;
	}
	public void setWarning(int warning) {
		this.warning = warning;
	}

	public String toString(){
		return JSONObject.fromObject(this).toString();
	}
}
