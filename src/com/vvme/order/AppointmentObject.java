package com.vvme.order;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @ClassName: AppointmentObject
 * @Description: 
 * @author yetl
 * @date 2015年5月4日
 *
 */
public class AppointmentObject {
	private int needCreate;
	private String id;
	private int type; //类型:1:load 2:order
	private String carrier;
	private Set<String> orderList=new HashSet<String>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Set<String> getOrderList() {
		return orderList;
	}
	public void setOrderList(Set<String> orderList) {
		this.orderList = orderList;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public int getNeedCreate() {
		return needCreate;
	}
	public void setNeedCreate(int needCreate) {
		this.needCreate = needCreate;
	}
}
