package com.vvme.order.exception;

public class ImportFileInvalidateException extends Exception{
	private static final long serialVersionUID = -1411480973093565660L;

	public ImportFileInvalidateException(){
		super();
	}
	
	public ImportFileInvalidateException(String msg){
		super(msg);
	}
}
