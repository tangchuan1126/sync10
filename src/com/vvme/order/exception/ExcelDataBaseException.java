package com.vvme.order.exception;

/**
 * 
 * @ClassName: ExcelDataBaseException
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ExcelDataBaseException extends Exception{
	private static final long serialVersionUID = -4904167673485965324L;

	public ExcelDataBaseException(){
		super();
	}
	
	public ExcelDataBaseException(String msg){
		super(msg);
	}
}
