package com.vvme.order.exception;

/**
 * 
 * @ClassName: ImportTemplateInvalidateException
 * @Description: 导入文件模板不对
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ImportTemplateInvalidateException extends Exception{
	private static final long serialVersionUID = 7364325440430815430L;

	public ImportTemplateInvalidateException(){
		super();
	}
	
	public ImportTemplateInvalidateException(String msg){
		super(msg);
	}
}
