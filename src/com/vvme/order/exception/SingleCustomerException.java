package com.vvme.order.exception;

/**
 * 
 * @ClassName: SingleCustomerException
 * @Description: 单一客户异常
 * @author yetl
 * @date 2015年4月25日
 *
 */
public class SingleCustomerException extends Exception{
	private static final long serialVersionUID = -92618212329270208L;

	public SingleCustomerException(){
		super();
	}
	
	public SingleCustomerException(String msg){
		super(msg);
	}
}
