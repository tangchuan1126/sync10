package com.vvme.order.dic;

/**
 * 
 * @ClassName: FreightTermsEnum
 * @Description: 支付方式
 * @author yetl
 * @date 2015年4月25日
 *
 */
public enum FreightTermsEnum {
	PREPAID("prepaid"),THIRDPARTY("thirdparty"),COLLECT("collect"),_3RDPARTY("3rdparty");
	
	private String value;
	
	private FreightTermsEnum(String value){
		this.value=value;
	}
	
	public String getValue(){
		return this.value.toUpperCase();
	}

}
