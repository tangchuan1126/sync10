package com.vvme.order.dic;

/**
 * 
 * @ClassName: FreightTypeEnum
 * @Description: 运输方式
 * @author yetl
 * @date 2015年4月25日
 *
 */
public enum FreightTypeEnum {
	IMDL("IMDL"),CONSOL("CONSOL"),LTL("LTL"),FTL("FTL"),SBF("SBF"),RAIL("RAIL"),FEDEX("FEDEX"),UPS("UPS");
	private String value;
	
	private FreightTypeEnum(String value){
		this.value=value;
	}
	
	public String getValue(){
		return value;
	}
}
