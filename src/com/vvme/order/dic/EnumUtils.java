package com.vvme.order.dic;

import org.apache.commons.lang.StringUtils;



/**
 * 
 * @ClassName: EnumUtils
 * @Description: 
 * @author yetl
 * @date 2015年4月30日
 *
 */
public class EnumUtils {
	
	/**
	 * 校验是否属于合法值
	 * @param value
	 * @return
	 */
	public static boolean isFreightTerms(String value){
		FreightTermsEnum [] enums=FreightTermsEnum.values();
		for(FreightTermsEnum e:enums){
			if(e.getValue().equalsIgnoreCase(value.replaceAll("\\s*", ""))){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 是否为第三方支付
	 * @return
	 */
	public static boolean isThirdParty(String value){
		if(StringUtils.isEmpty(value) || !isFreightTerms(value))
			return false;
		
		value=format3rdParty(value);
		FreightTermsEnum e=FreightTermsEnum.valueOf(value.replaceAll("\\s*", "").toUpperCase());
		if(FreightTermsEnum.THIRDPARTY==e || FreightTermsEnum._3RDPARTY==e){
			return true;
		}
		return false;
	}
	
	/**
	 * 是否为第三方支付
	 * @return
	 */
	public static boolean isEndUser(String value){
		if(StringUtils.isEmpty(value))
			return false;
		
		if(BillToEnum.ENDUSER.getValue().equalsIgnoreCase(value.replaceAll("\\s*", "").toLowerCase())){
			return true;
		}
		return false;
	}
	
	/**
	 * 校验是否属于合法值
	 * @param value
	 * @return
	 */
	public static boolean isFreightType(String value){
		FreightTypeEnum [] enums=FreightTypeEnum.values();
		for(FreightTypeEnum e:enums){
			if(e.getValue().equalsIgnoreCase(value.replaceAll("\\s*", ""))){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 规范third party写法
	 * @param ft
	 * @return
	 */
	public static String transFreightType(String ft){
		if(isThirdParty(ft))
			return "third party";
		
		return ft==null?"":ft;
	}
	
	/**
	 * 规范third party写法
	 * @param ft
	 * @return
	 */
	public static String format3rdParty(String ft){
		if(ft.replaceAll("\\s*", "").toLowerCase().equals("3rdparty"))
			return "third party";
		
		return ft;
	}
	
	/**
	 * 校验是否属于LTL or Consol
	 * @param value
	 * @return
	 */
	public static boolean isLTLOrConsol(String value){
		if(StringUtils.isEmpty(value)){
			return false;
		}
		
		FreightTypeEnum e=FreightTypeEnum.valueOf(value.replaceAll("\\s*", "").toUpperCase());
		if(e==FreightTypeEnum.CONSOL || e==FreightTypeEnum.LTL)
			return true;
		return false;
	}
	
	
}
