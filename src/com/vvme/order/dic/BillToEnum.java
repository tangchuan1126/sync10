package com.vvme.order.dic;

/**
 * 
 * @ClassName: BillToEnum
 * @Description: 账单
 * @author yetl
 * @date 2015年4月25日
 *
 */
public enum BillToEnum {
	ENDUSER("enduser");
	
	private String value;
	
	private BillToEnum(String value){
		this.value=value;
	}
	
	public String getValue(){
		return this.value;
	}
}
