package com.vvme.order;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import com.cwc.app.beans.AdminLoginBean;

/**
 * 
 * @ClassName: ProcessContext
 * @Description: 处理上下文
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ProcessContext {
	private String path; //excel文件路径
	private ExcelDataArray excelData; //excel内存数据
	private Set<ExcelRowResult> results=new HashSet<ExcelRowResult>(); //对应excel的结果
	private Set<ExcelRowResult> succs=new TreeSet<ExcelRowResult>();
	private Set<ExcelRowResult> errs=new TreeSet<ExcelRowResult>();
	private String cid; //客户ID
	private int expId; //导出的模板
	private int impId; //导入的模板
	private String fid; //下载文件ID
	private AdminLoginBean user;
	
	private ResponseObject resp;
	
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public ExcelDataArray getExcelData() {
		return excelData;
	}
	public void setExcelData(ExcelDataArray excelData) {
		this.excelData = excelData;
	}
	public Set<ExcelRowResult> getResults() {
		return results;
	}
	public ResponseObject getResp() {
		return resp;
	}
	public void setResp(ResponseObject resp) {
		this.resp = resp;
	}
	public void addExcelRowResult(ExcelRowResult rst) throws Exception{
		if(rst==null)
			return;
		
		if(rst.getRow()==0)
			throw new Exception("添加的行对象的row不能为空");
		
		if(results.contains(rst)){
			ExcelRowResult excelResult=getExcelRowResult(rst.getRow());
			if(excelResult==null)
				return;
			excelResult.setMsg(rst.getMsg());
			excelResult.setDn(rst.getDn());
			excelResult.setData(rst.getData());
			excelResult.setRight(rst.getRight());
			excelResult.setSign(rst.getSign());
			excelResult.setIndex(rst.getIndex());
			excelResult.getCells().addAll(rst.getCells());
		}else{
			results.add(rst);
		}
		
		if(rst.getRight()==1){
			succs.add(rst);
			if(errs.contains(rst))
				errs.remove(rst);
		}else{
			errs.add(rst);
			if(succs.contains(rst))
				succs.remove(rst);
		}
	}
	
	public ExcelRowResult getExcelRowResult(int row){
		for(ExcelRowResult rst:results){
			if(rst.getRow()==row)
				return rst;
		}
		return null;
	}
	public int getExpId() {
		return expId;
	}
	public void setExpId(int expId) {
		this.expId = expId;
	}
	public String getFid() {
		return fid;
	}
	public void setFid(String fid) {
		this.fid = fid;
	}
	public AdminLoginBean getUser() {
		return user;
	}
	public void setUser(AdminLoginBean user) {
		this.user = user;
	}
	public Set<ExcelRowResult> getSuccs() {
		return succs;
	}
	public void setSuccs(Set<ExcelRowResult> succs) {
		this.succs = succs;
	}
	public Set<ExcelRowResult> getErrs() {
		return errs;
	}
	public void setErrs(Set<ExcelRowResult> errs) {
		this.errs = errs;
	}
	public void stateResult(){
		Set<String> succsDn=new HashSet<String>();
		Set<String> errsDn=new HashSet<String>();
		
		for(ExcelRowResult rst:results){
			if(rst.getRight()==1){
				succsDn.add(rst.getDn());
			}else{
				errsDn.add(rst.getDn());
			}
		}
		
		if(resp!=null){
			resp.setSuccessCount(succsDn.size());
			resp.setFailCount(errsDn.size());
			resp.setTotal(succsDn.size()+errsDn.size());
			
			if(resp.getFailCount()>0){
				resp.setMsg(ExcelConstants.TIP_IMPORT_MSG);
			}else{
				resp.setMsg("Import Success!");
			}
		}
	}
	
	public void print(){
		for(ExcelRowResult rst:results){
			System.out.println(rst);
		}
	}
	
	public int getImpId() {
		return impId;
	}
	
	public void setImpId(int impId) {
		this.impId = impId;
	}
	
	public void destroy(){
		results.clear();
		succs.clear();
		errs.clear();
		
		excelData=null;
		results=null;
		succs=null;
		errs=null;
	}
	
}
