package com.vvme.order;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.vvme.order.ExcelRowResult.CellResult;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.util.ContextUtils;
import com.vvme.order.util.ExcelUtils;

/**
 * 
 * @ClassName: SignManager
 * @Description: 标记管理器
 * @author yetl
 * @date 2015年4月28日
 *
 */
public class SignManager {
	static Set<Integer> orderSigns=new HashSet<Integer>();
	static Set<Integer> loadSigns=new HashSet<Integer>();
	static Set<Integer> soSigns=new HashSet<Integer>();
	
	static{
		orderSigns.add(ExcelConstants.ERROR_TYPE_17);
	}
	
	static{
		loadSigns.add(ExcelConstants.ERROR_TYPE_15);
		loadSigns.add(ExcelConstants.ERROR_TYPE_24);
		loadSigns.add(ExcelConstants.ERROR_TYPE_41);
		loadSigns.add(ExcelConstants.ERROR_TYPE_42);
		loadSigns.add(ExcelConstants.ERROR_TYPE_34);
		loadSigns.add(ExcelConstants.ERROR_TYPE_19);
	}
	
	static{
		orderSigns.add(ExcelConstants.ERROR_TYPE_19);
	}
	
	/**
	 * 
	 * @param sign
	 * @return
	 */
	public static boolean canImportSign(int sign){
		if(orderSigns.contains(sign))
			return true;
		
		return false;
	}
	
	/**
	 * 
	 * @param sign
	 * @return
	 */
	public static boolean canLoadSign(int sign){
		if(loadSigns.contains(sign))
			return true;
		
		return false;
	}
	
	/**
	 * 
	 * @param sign
	 * @return
	 */
	public static boolean canSOSign(int sign){
		if(orderSigns.contains(sign))
			return true;
		
		return false;
	}
	
	/**
	 * 
	 * @param sign
	 * @return
	 */
	public static boolean canImportOrders(int sign){
		if(sign==ExcelConstants.ERROR_TYPE_4 || sign==ExcelConstants.ERROR_TYPE_13 || sign==ExcelConstants.ERROR_TYPE_34){
			return true;
		}
		return false;
	}
	
	/**
	 * 把错误标记到对应的列上,设置comment
	 * @param path 
	 * @param startRow
	 * @param list
	 * @throws Exception 
	 */
	public static void markExcelError(ProcessContext ctx) throws Exception {
		IExcelTableService excelTableMapper=(IExcelTableService) ContextUtils.getBean("excelTableServiceProxy");
		Map<Integer,FieldInfo> fields=excelTableMapper.getImportTemplateInfoByMid(ctx.getImpId());
		Set<ExcelRowResult> set=ctx.getResults();
		
		for (ExcelRowResult bean : set) {
			int errType=bean.getSign();
			
			if(errType==ExcelConstants.ERROR_TYPE_3){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"dn");
				CellResult cellResult=bean.createCellResult();
				cellResult.setLine(item.getExcelSn());
				cellResult.setMsg(ExcelConstants.ERROR_MSG_3);
				bean.addCellResult(cellResult);
			}else if(errType==ExcelConstants.ERROR_TYPE_5){
				FieldInfo lot=ExcelUtils.getExcelSn(fields,"material_number");
				bean.addCellResult(bean.createCellResult(lot.getExcelSn(), ExcelConstants.ERROR_MSG_5));
				FieldInfo item=ExcelUtils.getExcelSn(fields,"model_number");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_5));
			}else if(errType==ExcelConstants.ERROR_TYPE_6){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"country");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_6));
				FieldInfo state=ExcelUtils.getExcelSn(fields,"ship_to_state");
				bean.addCellResult(bean.createCellResult(state.getExcelSn(), ExcelConstants.ERROR_MSG_6));
			}else if(errType==ExcelConstants.ERROR_TYPE_7){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"ship_to_city");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_7));
			}else if(errType==ExcelConstants.ERROR_TYPE_8){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"delivery_plant");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_8));
			}else if(errType==ExcelConstants.ERROR_TYPE_9){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"status");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_9));
			}else if(errType==ExcelConstants.ERROR_TYPE_10){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"bol_tracking");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_10));
			}else if(errType==ExcelConstants.ERROR_TYPE_11){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"dn");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_11));
			}else if(errType==ExcelConstants.ERROR_TYPE_14){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"dn");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_14));
			}else if(errType==ExcelConstants.ERROR_TYPE_20){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"model_number");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_20));
				FieldInfo lot=ExcelUtils.getExcelSn(fields,"material_number");
				bean.addCellResult(bean.createCellResult(lot.getExcelSn(), ExcelConstants.ERROR_MSG_20));
			}else if(errType==ExcelConstants.ERROR_TYPE_25){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"dn");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_25));
			}else if(errType==ExcelConstants.ERROR_TYPE_27){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"bill_to");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_27));
			}else if(errType==ExcelConstants.ERROR_TYPE_28){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"pickup_appointment_date");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_28));
				FieldInfo apptime=ExcelUtils.getExcelSn(fields,"pickup_appointment_time");
				bean.addCellResult(bean.createCellResult(apptime.getExcelSn(), ExcelConstants.ERROR_MSG_28));
			}else if(errType==ExcelConstants.ERROR_TYPE_29){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"dn");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_29));
			}else if(errType==ExcelConstants.ERROR_TYPE_30){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"ship_to_party_name");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_30));
			}else if(errType==ExcelConstants.ERROR_TYPE_31){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"dn");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_31));
			}else if(errType==ExcelConstants.ERROR_TYPE_33){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"ship_to_zip_code");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_33));
			}else if(errType==ExcelConstants.ERROR_TYPE_34){
				bean.setMsg(ExcelConstants.ERROR_MSG_13+"(shipto地址请核对)");
			}else if(errType==ExcelConstants.ERROR_TYPE_35){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"req_ship_date");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_35));
			}else if(errType==ExcelConstants.ERROR_TYPE_36){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"mabd");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_36));
			}else if(errType==ExcelConstants.ERROR_TYPE_37){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"pickup_appointment_date");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_37));
				FieldInfo item1=ExcelUtils.getExcelSn(fields,"pickup_appointment_time");
				bean.addCellResult(bean.createCellResult(item1.getExcelSn(), ExcelConstants.ERROR_MSG_37));
			}else if(errType==ExcelConstants.ERROR_TYPE_38){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"dn");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_38));
			}else if(errType==ExcelConstants.ERROR_TYPE_39){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"pickup_appointment_date");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_39));
			}else if(errType==ExcelConstants.ERROR_TYPE_40){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"carrier");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_40));
			}else if(errType==ExcelConstants.ERROR_TYPE_43){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"model_number");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_43));
			}else if(errType==ExcelConstants.ERROR_TYPE_44){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"mabd");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_44));
			}else if(errType==ExcelConstants.ERROR_TYPE_45){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"carrier");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_45));
			}else if(errType==ExcelConstants.ERROR_TYPE_46){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"title");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_46));
			}else if(errType==ExcelConstants.ERROR_TYPE_47){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"pickup_appointment_date");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_47));
			}else if(errType==ExcelConstants.ERROR_TYPE_48){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"order_type");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_48));
			}else if(errType==ExcelConstants.ERROR_TYPE_49){
				FieldInfo fieldInfo=ExcelUtils.getExcelSn(fields,"order_type");
				bean.addCellResult(bean.createCellResult(fieldInfo.getExcelSn(), ExcelConstants.ERROR_MSG_49));
				FieldInfo item=ExcelUtils.getExcelSn(fields,"ship_to_party_name");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_49));
			}else if(errType==ExcelConstants.ERROR_TYPE_53){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"loadno");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_53));
			}else if(errType==ExcelConstants.ERROR_TYPE_54){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"ship_not_before");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_54));
			}else if(errType==ExcelConstants.ERROR_TYPE_55){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"ship_not_later");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_55));
			}else if(errType==ExcelConstants.ERROR_TYPE_58){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"ship_to_zip_code");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_58));
				FieldInfo country=ExcelUtils.getExcelSn(fields,"country");
				bean.addCellResult(bean.createCellResult(country.getExcelSn(), ExcelConstants.ERROR_MSG_58));
			}else if(errType==ExcelConstants.ERROR_TYPE_60){
				FieldInfo item=ExcelUtils.getExcelSn(fields,"bill_to");
				bean.addCellResult(bean.createCellResult(item.getExcelSn(), ExcelConstants.ERROR_MSG_60));
			}
		}
	}
	
}
