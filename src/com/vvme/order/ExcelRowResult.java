package com.vvme.order;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @ClassName: ExcelRowResult
 * @Description: 校验结果
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ExcelRowResult implements java.io.Serializable,java.lang.Comparable<ExcelRowResult>{
	private static final long serialVersionUID = -8767822926724357680L;
	private int row; //行 更excel对应
	private int index; // 在ExcelDataArray中data的下标
	private String msg; //处理结果
	private int sign=0; //状态码
	private int right=0; //是否正确的数据
	private String [] data; //行的数据
	private String dn="";
	private Set<CellResult> cells=new HashSet<CellResult>();
	
//	private static String [] arr=new String[]{"A","B","C","D","E","F","G","H","I","J","K"
//		,"L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK"
//		,"AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ"};
	
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getSign() {
		return sign;
	}
	public void setSign(int sign) {
		this.sign = sign;
	}
	public int getRight() {
		return right;
	}
	public void setRight(int right) {
		this.right = right;
	}
	public String[] getData() {
		return data;
	}
	public void setData(String[] data) {
		this.data = data;
	}
	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}
	/**
	 * 创建新的cell
	 * @return
	 */
	public CellResult createCellResult(){
		return new CellResult();
	}
	
	public CellResult createCellResult(int sn,String msg){
		CellResult cell=new CellResult();
		cell.setLine(sn);
		cell.setMsg(msg);
		return cell;
	}
	
	public void addCellResult(CellResult cellResult){
		if(cells.contains(cellResult)){
			CellResult cell=getCellResult(cellResult.getLine());
			cell.setMsg(cell.getMsg()+" & "+cellResult.getMsg());
		}else
			cells.add(cellResult);
	}
	
	public CellResult getCellResult(int line){
		for(CellResult cell:cells){
			if(cell.getLine()==line)
				return cell;
		}
		return createCellResult();
	}
	
	public Set<CellResult> getCells() {
		return cells;
	}
	
	public boolean containCellLine(int line){
		for(CellResult cell:cells){
			if(cell.getLine()==line)
				return true;
		}
		return false;
	}
	
	public int hashCode(){
		int result=31*getRow();
		return result;
	}
	
	public boolean equals(Object t){
		if(t==null)
			return false;
		
		if(this==t)
			return true;
		
		if(!(t instanceof ExcelRowResult)){
			return false;
		}else{
			ExcelRowResult o=(ExcelRowResult)t;
			if(o.getRow()==this.getRow())
				return true;
			else
				return false;
		}
	}
	
	public int compareTo(ExcelRowResult obj) {
		int num=new Integer(this.getRow()).compareTo(new Integer(obj.getRow()));
		return num;
	}
	
	public String toString(){
		return "第"+row+"行,msg:"+msg+" \r\n right:"+right+" sign"+sign;
	}
	
	/**
	 * 
	 * @ClassName: CellResult
	 * @Description: 对应一行里的单元格的信息
	 * @author yetl
	 * @date 2015年4月24日
	 *
	 */
	public class CellResult implements java.io.Serializable{
		private static final long serialVersionUID = -8216037215275613901L;
		private int line; //第几列
		private int code; //单元格状态
		private String msg; //单元格结果
		
		public int getLine() {
			return line;
		}

		public void setLine(int line) {
			this.line = line;
		}

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}
		
		public int hashCode(){
			int result=getLine();
			result=31*result;
			return result;
		}
		
		public boolean equals(Object t){
			if(this==t)
				return true;
			
			if(t==null)
				return false;
			
			if(!(t instanceof CellResult)){
				return false;
			}else{
				CellResult o=(CellResult)t;
				if(o.getLine()==this.getLine())
					return true;
				else
					return false;
			}
		}
		
		public String toString(){
			return "第"+line+"列"+",描述信息:"+msg;
		}
		
	}

}
