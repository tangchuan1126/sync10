package com.vvme.order.util;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

/**
 * 
 * @ClassName: ContextUtils
 * @Description: 获取bean
 * @author yetl
 * @date 2015年4月25日
 *
 */
public class ContextUtils {
	private static Logger log = Logger.getLogger("platform");
	private static ApplicationContext applicationContext;

	public static void setApplicationContext(
			ApplicationContext applicationContext) {
		synchronized (ContextUtils.class) {
			log.debug("setApplicationContext, notifyAll");
			ContextUtils.applicationContext = applicationContext;
			ContextUtils.class.notifyAll();
		}
	}

	public static ApplicationContext getApplicationContext() {
		synchronized (ContextUtils.class) {
			while (applicationContext == null) {
				try {
					log.debug("getApplicationContext, wait...");
					ContextUtils.class.wait(60000);
					if (applicationContext == null) {
						log.warn(
								"Have been waiting for ApplicationContext to be set for 1 minute",
								new Exception());
					}
				} catch (InterruptedException ex) {
					log.debug("getApplicationContext, wait interrupted");
				}
			}
			return applicationContext;
		}
	}

	public static Object getBean(String name) {
		return getApplicationContext().getBean(name);
	}
	
	public static <T> T getBean(Class<T> cls){
		return getApplicationContext().getBean(cls);
	}
	
}
