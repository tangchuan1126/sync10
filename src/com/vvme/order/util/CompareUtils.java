package com.vvme.order.util;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vvme.order.WmsOrderLinesTmp;

/**
 * 
 * @ClassName: CompareUtils
 * @Description: 
 * @author yetl
 * @date 2015年4月30日
 *
 */
public class CompareUtils {
	
	/**
	 * 返回不同的字段
	 * @param list
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws IntrospectionException
	 */
	public static Set<String> compareByMap(List<Map<String,Object>> list) throws IllegalAccessException,
	InvocationTargetException, IntrospectionException{
		if(null==list || list.size()==1)
			return null;
		
		Set<String> set=new HashSet<String>();
		Map<String,Object> source=list.get(0);
			
		for(int j=1;j<list.size();j++){
			Map<String,Object> target=list.get(j);
			
			Set<String> rst=compare(source,target);
			set.addAll(rst);
		}
		
		return set;
	}
	
	/**
	 * 返回不同的字段
	 * @param list
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws IntrospectionException
	 */
	public static Set<String> compare(List<WmsOrderLinesTmp> list) throws IllegalAccessException,
	InvocationTargetException, IntrospectionException{
		if(null==list || list.size()==1)
			return null;
		
		Set<String> set=new HashSet<String>();
		
		for(int i=0;i<list.size()-1;i++){
			
			for(int j=i+1;j<list.size();j++){
				WmsOrderLinesTmp source=list.get(i);
				WmsOrderLinesTmp target=list.get(j);
				
				Set<String> rst=compare(source,target);
				set.addAll(rst);
			}
		}
		
		return set;
	}
	
	/**
	 * 比较2个bean的差异
	 * @param obj1
	 * @param obj2
	 * @throws IntrospectionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	@SuppressWarnings("unchecked")
	public static Set<String> compare(WmsOrderLinesTmp source,WmsOrderLinesTmp target) throws IllegalAccessException,
	InvocationTargetException, IntrospectionException{
		Map<String,Object> sMap=(Map<String, Object>) BeanToMapUtil.convertBean(source);
		Map<String,Object> tMap=(Map<String,Object>)BeanToMapUtil.convertBean(target);
		
		Set<String> fields=new HashSet<String>();
		for(String key:sMap.keySet()){
			Object vs=sMap.get(key);
			Object vt=tMap.get(key);
			if(key.equalsIgnoreCase("ship_to_party_name")){
				if(!((String)vs).replaceAll(" ", "_").equalsIgnoreCase(((String)vt).replaceAll(" ", "_"))){
					fields.add(key);
					continue;
				}
			}else if(key.equalsIgnoreCase("ship_to_address")){
				 if(!((String)vs).replaceAll(" ", "").equalsIgnoreCase(((String)vt).replaceAll(" ", ""))){
					fields.add(key);
					continue;
				 }
			}else if(key.equalsIgnoreCase("total_weight")){
				 if(((BigDecimal)vs).floatValue()!=((BigDecimal)vt).floatValue()){
					fields.add(key);
					continue;
				 }
			}else if(key.equalsIgnoreCase("")){
				
			}else{
				 if(sMap.get(key) instanceof String){
		             if (!((String)sMap.get(key)).equalsIgnoreCase((String)tMap.get(key))) {
		            	 fields.add(key);
		             }
				 }else{
					 if(null!=sMap.get(key)){
			             if (!sMap.get(key).equals(tMap.get(key))) {
			            	 fields.add(key);
			             }
					 }else{
						 if(null!=tMap.get(key))
							 fields.add(key);
					 }
				 }
			}
		}
		
		return fields;
	}
	
	/**
	 * 比较2个bean的差异
	 * @param obj1
	 * @param obj2
	 * @throws IntrospectionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static Set<String> compare(Map<String,Object> sMap,Map<String,Object> tMap){
		Set<String> fields=new HashSet<String>();
		for(String key:sMap.keySet()){
			if(key.equalsIgnoreCase("row"))
				continue;
			
			if(sMap.get(key)==null){
				 if (!String.valueOf(sMap.get(key)).equals(String.valueOf(tMap.get(key)))) {
	            	 fields.add(key);
	             }
			}else{
	             if (!sMap.get(key).equals(tMap.get(key))) {
	            	 fields.add(key);
	             }
            }
		}
		
		return fields;
	}
}
