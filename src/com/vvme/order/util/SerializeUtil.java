package com.vvme.order.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 
 * @ClassName: SerializeUtil
 * @Description: 
 * @author yetl
 * @date 2015年4月30日
 *
 */
public class SerializeUtil {
	 /**
	  * 对象到字节
	  * @param object
	  * @return
	  */
	 public static byte[] serialize(Object object) {
		 ObjectOutputStream oos = null;
		 ByteArrayOutputStream baos = null;
		 try {
			 //序列化
			 baos = new ByteArrayOutputStream();
			 oos = new ObjectOutputStream(baos);
			 oos.writeObject(object);
			 byte[] bytes = baos.toByteArray();
			 return bytes;
		 } catch (Exception e) {
			 e.printStackTrace();
		 }finally{
			 try {
				baos.close();
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }
		 return null;
	}
	 
	/**
	 * 反序列化
	 * @param bytes
	 * @return
	 */
	public static Object unserialize(byte[] bytes) {
		if(null==bytes)
			return null;
		
		 ByteArrayInputStream bais = null;
		 try {
			 //反序列化
			 bais = new ByteArrayInputStream(bytes);
			 ObjectInputStream ois = new ObjectInputStream(bais);
			 return ois.readObject();
		 } catch (Exception e) {
			 e.printStackTrace();
		 }finally{
			 try {
				bais.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }
		 return null;
	 }
}
