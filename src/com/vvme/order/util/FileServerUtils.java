package com.vvme.order.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

/**
 * 
 * @ClassName: FileServerUtils
 * @Description: 文件上传下载工具类
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class FileServerUtils {
	private static Logger log = Logger.getLogger("action");
	
	/**
	 * 在文件服务上获取文件
	 * @param url
	 * @param destFileName
	 * @param sessionId
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static int getFile(String url, String destFileName, String sessionId)
			throws ClientProtocolException, IOException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(url);

		httpget.setHeader("Cookie", "JSESSIONID=" + sessionId);
		HttpResponse response = httpClient.execute(httpget);

		StatusLine statusLine = response.getStatusLine();
		int httpStatus=statusLine.getStatusCode();
		log.info("文件下载服务器返回Code:" + httpStatus);
		if (httpStatus == 200) {
			HttpEntity entity = response.getEntity();
//			String resp = EntityUtils.toString(entity);
//			log.info("文件下载服务器返回数据:" + resp);
			InputStream in = entity.getContent();
			File file = new File(destFileName);

			try {
				FileOutputStream fout = new FileOutputStream(file);
				int l = -1;
				byte[] tmp = new byte[1024];
				while ((l = in.read(tmp)) != -1) {
					fout.write(tmp, 0, l);
				}
				fout.flush();
				fout.close();
			} finally {
				// 关闭低层流。
				in.close();
			}
			//把底层的流给关闭
			EntityUtils.consume(entity);
		}
		httpClient.getConnectionManager().shutdown();
		return httpStatus;
	}
	
	/**
	 * 文件上传到文件服务器
	 * 
	 * @param url
	 * @param destFileName
	 * @param sessionId
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static String uploadFile(String url, String fullPath, String sessionId)
			throws ClientProtocolException, IOException {
		String fileId = "";
		HttpClient httpClient = new DefaultHttpClient();
		try {
			HttpPost httppost = new HttpPost(url);

			httppost.setHeader("Cookie", "JSESSIONID=" + sessionId);
			FileBody bin = new FileBody(new File(fullPath));

			MultipartEntity reqEntity = new MultipartEntity();

			reqEntity.addPart("file", bin);// file1为请求后台的File upload;属性
			httppost.setEntity(reqEntity);

			HttpResponse response = httpClient.execute(httppost);

			int statusCode = response.getStatusLine().getStatusCode();

			if (statusCode == HttpStatus.SC_OK) {
				HttpEntity resEntity = response.getEntity();
				String resp = EntityUtils.toString(resEntity);
				log.info("文件上传服务器返回数据:" + resp);
				JSONArray json = JSONArray.fromObject(resp);
				JSONObject jo = json.getJSONObject(0);
				fileId = jo.getString("file_id");
				EntityUtils.consume(resEntity);
			}
			return fileId;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				httpClient.getConnectionManager().shutdown();
			} catch (Exception ignore) {
				ignore.printStackTrace();
			}
		}
		return fileId;
	}
	
	/**
	 * 删除临时文件
	 * @param fullpath
	 */
	public static void delTmpFile(String fullpath){
		File file=new File(fullpath);
		if(file.exists()){
			log.info(fullpath+"删除状态"+file.delete());
		}
	}
	
}
