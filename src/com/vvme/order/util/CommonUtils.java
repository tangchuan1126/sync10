package com.vvme.order.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.axis.utils.StringUtils;

import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;

/**
 * 
 * @ClassName: CommonUtils
 * @Description: 
 * @author yetl
 * @date 2015年4月25日
 *
 */
public class CommonUtils {
	private static final String CNUMBER_PATTERN = "^[0-9]*(.[0]?)$";// 判断数字的正则表达式
	public static SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd");// 格式化日期字符串
	public static SimpleDateFormat ymdhm = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm");// 格式化日期字符串
	
	/**
	 * 校验是否为整数
	 * @param number
	 * @return
	 */
	public static boolean parseInt(String number){
		try{
			Integer.parseInt(number);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * 校验是否为integer
	 * @param number
	 * @return
	 */
	public static boolean isInteger(String number) {
		  return match(CNUMBER_PATTERN, number);
	}
	
	/**
	 * 正则匹配
	 * @param pattern
	 * @param str
	 * @return
	 */
	private static boolean match(String pattern, String str) {
		  Pattern p = Pattern.compile(pattern);
		  Matcher m = p.matcher(str);
		  return m.find();
	}
	
	/**
	 * 校验是否为decimal类型
	 * @param number
	 * @return
	 */
	public static boolean isDecimalNumber(String number) {
		try{
			Float.parseFloat(number);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * 校验字符串是否为日期
	 * @param str
	 * @return
	 */
	public static boolean isValidDate(String str,String format) {
		 if(StringUtils.isEmpty(format)){
			return isValidDate(str);
		 }
	
		 SimpleDateFormat def=sdf;
	     try {
	    	 // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
	    	 def.setLenient(false);
	    	 def.parse(str);
	    	 return true;
	     } catch (ParseException e) {
	    	 if(format.equals("MM/dd/yyyy")){
	    		 if(!isValidDateMDY(str))
	    			 return false;
	    	 }
	    	 SimpleDateFormat sdf=new SimpleDateFormat(format);
		     try {
		    	 // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
		    	 sdf.setLenient(false);
		    	 sdf.parse(str);
		    	 return true;
		     } catch (ParseException ee) {
		    	 ee.printStackTrace();
		    	 return false;
		     } 
	     } 
	}
	
	public static boolean isValidDateMDY(String date){
		String re="^\\d{1,2}\\/\\d{1,2}\\/(\\d{4}|\\d{2})$";
		Pattern part=Pattern.compile(re);
		Matcher m = part.matcher(date);
		return m.find();
	}
	
	public static boolean isValidDate(String str) {
		 boolean convertSuccess=true;
		 //yyyy-MM-dd HH:mm:ss 
	     try {
	    	 // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
	    	 sdf.setLenient(false);
	    	 sdf.parse(str);
	     } catch (ParseException e) {
	         convertSuccess=false;
	     } 
		 return convertSuccess;
	}
	
    /** 
     * 校验时间格式HH:MM（精确） 
     */  
    public static boolean checkTime(String time) {  
        if (checkHHMM(time)) {  
            String[] temp = time.split(":"); 
            if ((temp[0].length() == 2 || temp[0].length() == 1) && temp[1].length() == 2) {  
                int h,m;  
                try {  
                    h = Integer.parseInt(temp[0]);  
                    m = Integer.parseInt(temp[1]);  
                } catch (NumberFormatException e) {  
                    return false;  
                }     
                if (h >= 0 && h <= 24 && m == 0) {  
                    return true;  
                }  
            }  
        }  
        return false;  
    } 
    
    /** 
     * 校验时间格式（仅格式） 
     */  
    public static boolean checkHHMM(String time) {  
    	if(time.trim().length()>5)
    		return false;
    	
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");  
         try {  
           dateFormat.parse(time);  
         }catch (Exception ex) {  
             return false;  
         }  
        return true;  
    }
    
	/**
	 * 校验字符串是否为日期
	 * @param str
	 * @return
	 */
	public static boolean isValidDateTime(String str) {
		 boolean convertSuccess=true;
		 //yyyy-MM-dd HH:mm:ss 
		 SimpleDateFormat sdf =ymdhm;
	     try {
	    	 // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
	         sdf.setLenient(false);
	         sdf.parse(str);
	     } catch (ParseException e) {
	         convertSuccess=false;
	     } 
		 return convertSuccess;
	 }
	
	 /**
	  * 空格装换'_'
	  * @param shipToPartyName
	  * @return
	  */
	 public static String formatShipToPartyName(String shipToPartyName){
	 	return shipToPartyName.replaceAll(" ", "_");
	 }
	 
	 /**
	  * 去空格
	  * @param shipToAddress
	  * @return
	  */
	 public static String formatShipToAddress(String shipToAddress){
//		return shipToAddress.replaceAll("\\s*", "");
		 return shipToAddress.trim();
	 }
	 
	/**
	 * 存储UTC时间
	 * @param row
	 * @param psId
	 * @param v
	 * @param field
	 * @throws Exception
	 */
	public static void dateToUTC(DBRow row,int psId,String v,String field) throws Exception{
		if(!StringUtils.isEmpty(v)){
			Date date=DateUtil.utcTime(v, psId);
			row.put(field, date); 
		}
	}
	
	/**
	 * 国家是否是美国
	 * @param us
	 * @return
	 */
	public static boolean isUnitedStates(String us){
		if(StringUtils.isEmpty(us))
			return false;
		
		if(us.replaceAll("\\s*", "").equalsIgnoreCase("UnitedStates")){
			return true;
		}
		return false;
	}
	
	/**
	 * 是否为美国邮编
	 * @param zipCode
	 * @return
	 */
	public static boolean isUnitedStatesZipCode(String zipCode){
		String code=zipCode.replaceAll("-", "");
		Pattern p = Pattern.compile("^[0-9]*$");
		Matcher m = p.matcher(code);
		return m.find();
	}
	
	/**
	 * 是否是同方客户
	 * @param cid
	 * @return
	 */
	public static boolean isElementCustomer(String cid){
		if("1".equals(cid) || "2".equals(cid) || "3".equals(cid)){
			return true;
		}
		return false;
	}
}
