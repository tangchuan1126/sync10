package com.vvme.order.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.axis.utils.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.cwc.db.DBRow;
import com.vvme.order.ExcelDataArray;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;

/**
 * 
 * @ClassName: ExcelUtils
 * @Description: 只适用于导入订单模块
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ExcelUtils {
	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");// 格式化日期字符串
	public static SimpleDateFormat ymdhm = new SimpleDateFormat("yyyy-MM-dd HH:mm");// 格式化日期字符串
	
	/**
	 * 去掉空行
	 * @param excelData 
	 */
	public static void removeEmptyLine(ExcelDataArray excelDataArray){
		String[][] data = excelDataArray.getDataArray();
		int[] rows=excelDataArray.getDataRows();
		int row = 0;
		int line = 0;

		if (data.length <= 0)
			return;

		if (data.length > 0) {
			line = data[0].length;
		}

		for (int i = 0; i < data.length; i++) {
			String[] hData = data[i];
			boolean nullLine=isNullLine(hData);
			if(!nullLine)
				row++;
		}

		String[][] newData = new String[row][line];
		int[] newRow = new int[row];
		
		int r = 0;
		for (int i = 0; i < data.length; i++) {
			String[] hData = data[i];
			boolean nullLine=isNullLine(hData);
			// 非空行
			if (!nullLine) {
				for (int j = 0; j < hData.length; j++) {
					newData[r][j] = hData[j];
				}
				newRow[r]=rows[i];
				r++;
			}
		}

		excelDataArray.setDataArray(newData);
		excelDataArray.setDataRows(newRow);
	}
	
	/**
	 * 判断数据是否为空
	 * @param datas
	 * @return
	 */
	public static boolean isNullLine(String [] datas){
		if(null==datas || datas.length<=0)
			return true;
		
		boolean flag=true;
		for(int i=0;i<datas.length;i++){
			if(datas[i]!=null && datas[i].trim().length() > 0){
				flag=false;
				return flag;
			}
		}
		return flag;
	}
	
	/**
	 * 校验结果写回导出模板
	 */
	public static void writeValidateResult(ProcessContext ctx){
		
	}
	
	/**
	 * 根据字段名字查找到具体映射信息
	 * @param fields
	 * @param fieldName
	 * @return
	 */
	public static FieldInfo getExcelSn(Map<Integer,FieldInfo> fields,String fieldName){
		Iterator<Entry<Integer, FieldInfo>> it=fields.entrySet().iterator();
		
		while(it.hasNext()){
			Map.Entry<Integer, FieldInfo> entry=(Map.Entry<Integer, FieldInfo>)it.next();
			FieldInfo value=entry.getValue();
			
			if(value.getName().equalsIgnoreCase(fieldName))
				return value;
		}
		
		return null;
	}
	
	/**
	 * 值格式化为显示值
	 * @param value
	 * @param format
	 * @param sn
	 */
	public static String parseVallue(String value,int sn,Map<Integer,FieldInfo> fields){
		FieldInfo fInfo=fields.get(sn);
		String type=fInfo.getType();
		
		try{
			switch(type){
				case "date":
					if(StringUtils.isEmpty(value))
						return value;
					
					String format=fInfo.getFormat();
					try{
						Date date=sdf.parse(value);
						SimpleDateFormat newsdf=new SimpleDateFormat(format);
						return newsdf.format(date);
					}catch(ParseException e){
						return value;
					}
				case "datetime":
					if(StringUtils.isEmpty(value))
						return value;
					try{
						Date date=ymdhm.parse(value);
						SimpleDateFormat newsdf=new SimpleDateFormat(fInfo.getFormat());
						return newsdf.format(date);
					}catch(ParseException e){
						return value;
					}
				default:
					return value;
			}
		}catch(Exception e){
			e.printStackTrace();
			return value;
		}
	}
	
	/**
	 * 根据数据库字段将数据转成dbrow
	 * @param row
	 * @param fInfo excel对应数据库临时表字段信息
	 * @param value excel 字符串数据
	 */
	public static void addRowData(DBRow row,FieldInfo fInfo,String value){
		String type=fInfo.getType();
		String name=fInfo.getName();
		String format=fInfo.getFormat();
		
		switch(type){
			case "varchar":
				processDate(row,name,value);
				break;
			case "int":
				row.add(name, strToLong(value));
				break;
			case "tinyint":
				row.add(name, strToInt(value));
				break;
			case "decimal":
				row.add(name, strToDb(value));
				break;
			case "date":
				processDate(row,name,value,format);
				break;
			case "time":
				processDate(row,name,value);
				break;
			case "datetime":
				processDate(row,name,value);
				break;
			default:
				row.add(name, value);
		}
	}
	
	/**
	 * 数据添加到dbrow
	 * @param row
	 * @param name
	 * @param v
	 */
	public static void processDate(DBRow row,String name,String v){
    	if(!StringUtils.isEmpty(v)){
    		row.add(name, v);
    	}
	}
	
	/**
	 * 字符转long
	 * @param str
	 * @return
	 */
	public static long strToLong(String str){
		if(StringUtils.isEmpty(str))
			return 0;
		
		return Long.parseLong(str);
	}
	
	/**
	 * 字符串转整数
	 * @param str
	 * @return
	 */
	public static int strToInt(String str){
		if(StringUtils.isEmpty(str))
			return 0;
		
		return Integer.parseInt(str);
	}
	
	/**
	 * 字符转double
	 * @param str
	 * @return
	 */
	public static double strToDb(String str){
		if(StringUtils.isEmpty(str))
			return 0;
		
		return Double.parseDouble(str);
	}
	
	/**
	 * 数据添加到dbrow
	 * @param row
	 * @param name
	 * @param v
	 */
	public static void processDate(DBRow row,String name,String v,String format){
		if(StringUtils.isEmpty(format)){
			processDate(row,name,v);
			return;
		}
		
		String [] formats=format.split("\\|");
		//日志格式转成yyyy-MM-dd 入库
		String pattern="\\d{4}-\\d{1,2}-\\d{2}";
		Pattern p = Pattern.compile(pattern);
		
		if(!StringUtils.isEmpty(v)){
			if(p.matcher(v).matches()){
				row.add(name, v);
				return;
			}
			
			for(int i=0;i<formats.length;i++){
				//转化成sdf格式在入库
				try {
					Date date=DateUtils.parseDate(v, new String[]{formats[i]});
					row.add(name, sdf.format(date));
				} catch (ParseException e) {
					continue;
				}
			}
    	}
	}
	
	/**
	 * 获取对应行号在data[][]里的下标
	 * @param rows
	 * @param value
	 * @return
	 */
	public static int getIndex(int [] rows,int value){
		for(int i=0;i<rows.length;i++){
			if(rows[i]==value)
				return i;
		}
		return -1;
	}
}
