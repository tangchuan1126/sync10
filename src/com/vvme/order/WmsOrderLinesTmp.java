package com.vvme.order;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @ClassName: WmsOrderLinesTmp
 * @Description: 属性命名跟wms_order_lines_tmp字段名一样
 * @author yetl
 * @date 2015年5月13日
 *
 */
public class WmsOrderLinesTmp implements java.io.Serializable{
	
	private static final long serialVersionUID = -6844969037116672238L;
	
	private String delivery_plant;
	private String dn;
	private String retail_po;
	private String order_number;
	private Date order_date;
	private String ship_to_party_name;
	private String ship_to_address;
	private String ship_to_city;
	private String ship_to_state;
	private String ship_to_zip_code;
	private String country;
	private Date mabd;
	private Date req_ship_date;
	private Date ship_not_before;
	private Date ship_not_later;
//	private String loadno;
//	private Date pickup_appointment_date;
	private Date ship_date;
	private String bill_to;
	private String bol_tracking;
	private String ft;
	private String freight_type;
//	private String carrier;
	
	//明细
	private String material_number;
	private String model_number;
	private int order_qty;
	private BigDecimal weight;
	private int boxes;
	private BigDecimal pallet_spaces;
	
	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Date getMabd() {
		return mabd;
	}
	public void setMabd(Date mabd) {
		this.mabd = mabd;
	}
//	public String getLoadno() {
//		return loadno;
//	}
//	public void setLoadno(String loadno) {
//		this.loadno = loadno;
//	}
	public BigDecimal getWeight() {
		return weight;
	}
//	public String getCarrier() {
//		return carrier;
//	}
//	public void setCarrier(String carrier) {
//		this.carrier = carrier;
//	}
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}
	public int getBoxes() {
		return boxes;
	}
	public void setBoxes(int boxes) {
		this.boxes = boxes;
	}
	public String getDelivery_plant() {
		return delivery_plant;
	}
	public void setDelivery_plant(String delivery_plant) {
		this.delivery_plant = delivery_plant;
	}
	public String getRetail_po() {
		return retail_po;
	}
	public void setRetail_po(String retail_po) {
		this.retail_po = retail_po;
	}
	public String getOrder_number() {
		return order_number;
	}
	public void setOrder_number(String order_number) {
		this.order_number = order_number;
	}
	public Date getOrder_date() {
		return order_date;
	}
	public void setOrder_date(Date order_date) {
		this.order_date = order_date;
	}
	public String getShip_to_party_name() {
		return ship_to_party_name;
	}
	public void setShip_to_party_name(String ship_to_party_name) {
		this.ship_to_party_name = ship_to_party_name;
	}
	public String getShip_to_address() {
		return ship_to_address;
	}
	public void setShip_to_address(String ship_to_address) {
		this.ship_to_address = ship_to_address;
	}
	public String getShip_to_city() {
		return ship_to_city;
	}
	public void setShip_to_city(String ship_to_city) {
		this.ship_to_city = ship_to_city;
	}
	public String getShip_to_state() {
		return ship_to_state;
	}
	public void setShip_to_state(String ship_to_state) {
		this.ship_to_state = ship_to_state;
	}
	public String getShip_to_zip_code() {
		return ship_to_zip_code;
	}
	public void setShip_to_zip_code(String ship_to_zip_code) {
		this.ship_to_zip_code = ship_to_zip_code;
	}
	public Date getReq_ship_date() {
		return req_ship_date;
	}
	public void setReq_ship_date(Date req_ship_date) {
		this.req_ship_date = req_ship_date;
	}
	//	public Time getPickup_appointment() {
//		return pickup_appointment;
//	}
//	public void setPickup_appointment(Time pickup_appointment) {
//		this.pickup_appointment = pickup_appointment;
//	}
	public Date getShip_date() {
		return ship_date;
	}
	public void setShip_date(Date ship_date) {
		this.ship_date = ship_date;
	}
	public String getBol_tracking() {
		return bol_tracking;
	}
	public void setBol_tracking(String bol_tracking) {
		this.bol_tracking = bol_tracking;
	}
	public String getMaterial_number() {
		return material_number;
	}
	public void setMaterial_number(String material_number) {
		this.material_number = material_number;
	}
	public String getModel_number() {
		return model_number;
	}
	public void setModel_number(String model_number) {
		this.model_number = model_number;
	}
	public int getOrder_qty() {
		return order_qty;
	}
	public void setOrder_qty(int order_qty) {
		this.order_qty = order_qty;
	}
	public BigDecimal getPallet_spaces() {
		return pallet_spaces;
	}
	public void setPallet_spaces(BigDecimal pallet_spaces) {
		this.pallet_spaces = pallet_spaces;
	}
	public String getBill_to() {
		return bill_to;
	}
	public void setBill_to(String bill_to) {
		this.bill_to = bill_to;
	}
	public String getFt() {
		return ft;
	}
	public void setFt(String ft) {
		this.ft = ft;
	}
	public Date getShip_not_before() {
		return ship_not_before;
	}
	public void setShip_not_before(Date ship_not_before) {
		this.ship_not_before = ship_not_before;
	}
	public Date getShip_not_later() {
		return ship_not_later;
	}
	public void setShip_not_later(Date ship_not_later) {
		this.ship_not_later = ship_not_later;
	}
//	public Date getPickup_appointment_date() {
//		return pickup_appointment_date;
//	}
//	public void setPickup_appointment_date(Date pickup_appointment_date) {
//		this.pickup_appointment_date = pickup_appointment_date;
//	}
	public String getFreight_type() {
		return freight_type;
	}
	public void setFreight_type(String freight_type) {
		this.freight_type = freight_type;
	}
}
