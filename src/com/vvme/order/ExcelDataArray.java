package com.vvme.order;

/**
 * @ClassName: ExcelDataArray
 * @Description: 模板数据
 * @author yetl
 * @date 2015年4月23日
 *
 */
public class ExcelDataArray {
	private String [] titles; //标题
	private long [] widths; //每列的宽度
	  
	private String[][] dataArray; //excel对应内存数据
	private int [] dataRows; //dataArray的数据对应excel多少行
  
    private int rowNum; //行数  
    private int lineNum; //列数  
    private String caption; //excel 名称
    
    public ExcelDataArray(){
    }
    
    public void init(int rowNum, int lineNum){
        this.rowNum = rowNum;  
        this.lineNum = lineNum;  
        dataArray = new String[rowNum][lineNum];  
        titles=new String[lineNum];
        widths=new long[lineNum];
        dataRows=new int[rowNum];
    }
    
    public String[][] getDataArray() {  
        return dataArray;  
    }  
    public void setDataArray(String[][] dataArray) {  
        this.dataArray = dataArray;  
    }  
    public int getRowNum() {  
        return rowNum;  
    }  
    public void setRowNum(int rowNum) {  
        this.rowNum = rowNum;  
    }  
    public int getLineNum() {  
        return lineNum;  
    }  
    public void setLineNum(int lineNum) {  
        this.lineNum = lineNum;  
    }  
    public String[] getTitles() {
		return titles;
	}
	public void setTitles(String[] titles) {
		this.titles = titles;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public long[] getWidths() {
		return widths;
	}
	public void setWidths(long[] widths) {
		this.widths = widths;
	}
	/** 
     * 设置rowIndex行，lineIndex列的数据 
     * @param rowIndex 
     * @param lineIndex 
     * @param value 
     * @return 
     */  
    public String setColumn(int rowIndex, int lineIndex, String value){  
        dataArray[rowIndex][lineIndex] = value;  
        return dataArray[rowIndex][lineIndex];  
    }  
      
      
    /** 
     * 获取rowIndex行，lineIndex列的数据 
     * @param rowIndex 
     * @param lineIndex 
     * @return 
     */  
    public String getColumnColumn(int rowIndex, int lineIndex){  
        return dataArray[rowIndex][lineIndex];  
    }  
    
    /**
     * 设置title头
     * @param lineIndex
     * @param value
     */
    public void setTitle(int lineIndex,String value){
    	titles[lineIndex]=value;
    }
    
    /**
     * 获取lineIndex列的数据
     * @param lineIndex
     * @return
     */
    public String getTitle(int lineIndex){
    	return titles[lineIndex];
    }
    
    /**
     * 设置title头
     * @param lineIndex
     * @param value
     */
    public void setWidth(int lineIndex,long value){
    	widths[lineIndex]=value;
    }
    
    /**
     * 获取lineIndex列的数据
     * @param lineIndex
     * @return
     */
    public long getWidth(int lineIndex){
    	return widths[lineIndex];
    }

	public int[] getDataRows() {
		return dataRows;
	}
	
	public void setDataRows(int[] dataRows) {
		this.dataRows = dataRows;
	}

	public void setDataRow(int index,int row){
		this.dataRows[index]=row;
	}
}
