package com.vvme.order.validate;

import java.util.Map;

import org.apache.axis.utils.StringUtils;

import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.ExcelRowResult.CellResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.util.ContextUtils;
import com.vvme.order.util.ExcelUtils;

/**
 * 
 * @ClassName: CarrierValidate
 * @Description: 校验carrier列
 * @author yetl
 * @date 2015年4月25日
 *
 */
public class CarrierValidate implements IExcelTemplateValidate{

	public boolean validate(ProcessContext ctx) throws Exception {
		String [][] data=ctx.getExcelData().getDataArray();
		int [] rows=ctx.getExcelData().getDataRows();
		IExcelTableService excelTableMapper=(IExcelTableService) ContextUtils.getBean("excelTableServiceProxy");
		Map<Integer,FieldInfo> fields=excelTableMapper.getImportTemplateInfoByMid(ctx.getImpId());
		FieldInfo field=ExcelUtils.getExcelSn(fields,"carrier");
		
		int sn=field.getExcelSn();
		boolean flag=true;
		
		for(int i=0;i<data.length;i++){
			String [] hData=data[i];
			int row=rows[i];
			
			if(StringUtils.isEmpty(hData[sn])){
				ExcelRowResult rst=new ExcelRowResult();
				rst.setRow(row);
				rst.setMsg(ExcelConstants.ERROR_MSG_DATABASE);
				CellResult cellResult = rst.createCellResult();
				cellResult.setLine(sn);
				cellResult.setMsg( field.getXlsTitle()+" Cannot leave blank.  Please enter a valid value。");
				rst.addCellResult(cellResult);

				ctx.addExcelRowResult(rst);
				flag=false;
			}
		}
		return flag;
	}

	public boolean openCheck() {
		return true;
	}

}
