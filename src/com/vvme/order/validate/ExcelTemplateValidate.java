package com.vvme.order.validate;

import java.util.Map;

import org.apache.log4j.Logger;

import com.vvme.order.ExcelDataArray;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.excel.IExcelProcess;
import com.vvme.order.exception.ImportTemplateInvalidateException;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: ExcelTemplateValidate
 * @Description: 校验模板 目前导入模板就一套
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ExcelTemplateValidate implements IExcelTemplateValidate{
	protected static Logger log = Logger.getLogger("logfile");

	public boolean validate(ProcessContext ctx) throws Exception {
		IExcelProcess excelProcess=ContextUtils.getBean(IExcelProcess.class);
		if (null == excelProcess){
			log.error("IExcelProcess Without inject spring");
			throw new Exception("IExcelProcess Without inject spring");
		}
		IExcelTableService excelTableMapper=(IExcelTableService) ContextUtils.getBean("excelTableServiceProxy");
		Map<Integer,FieldInfo> fields=excelTableMapper.getImportTemplateInfoByMid(ctx.getImpId());
		
		ExcelDataArray excelData=excelProcess.importExcelFileToDataArray(ctx.getPath());
		ctx.setExcelData(excelData);
		
		int temp=fields.size();
		String [] titles=excelData.getTitles();
		int fact=titles.length;
		
		if (fact != temp ) {
			log.info("导入excel文件的列数量跟模板不一样-->"+fact+":"+temp);
			throw new ImportTemplateInvalidateException("Import the excel file with the number of columns is not the same as the template-->"+fact+":"+temp);
		}
		
		for (int lineIndex = 0; lineIndex < fact; lineIndex++) {
			String mfn=fields.get(lineIndex).getXlsTitle().trim();
			String efn=titles[lineIndex].trim();
			
			if(!mfn.equalsIgnoreCase(efn)){
				log.info("导入excel文件的列名字跟模板对应的不一样-->"+mfn+":"+efn);
				throw new ImportTemplateInvalidateException("Column name with the corresponding template excel file import is not the same-->"+mfn+":"+efn);
			}
		}
		return true;
	}

	public boolean openCheck() {
		return true;
	}

}
