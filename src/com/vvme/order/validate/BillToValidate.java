package com.vvme.order.validate;

import java.util.Map;

import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.util.ContextUtils;
import com.vvme.order.util.ExcelUtils;

/**
 * 
 * @ClassName: BillToValidate
 * @Description: 校验账单
 * @author yetl
 * @date 2015年4月25日
 *
 */
public class BillToValidate implements IExcelTemplateValidate{

	public boolean validate(ProcessContext ctx) throws Exception {
		String [][] data=ctx.getExcelData().getDataArray();
		IExcelTableService excelTableMapper=ContextUtils.getBean(IExcelTableService.class);
		Map<Integer,FieldInfo> fields=excelTableMapper.getImportTemplateInfoByMid(ctx.getImpId());
		FieldInfo billInfo=ExcelUtils.getExcelSn(fields,"bill_to");
		
		return false;
	}

	public boolean openCheck() {
		return true;
	}

}
