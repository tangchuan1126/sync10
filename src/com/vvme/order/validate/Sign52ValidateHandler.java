package com.vvme.order.validate;

import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportSOService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign52ValidateHandler
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author yetl
 * @date 2015年4月30日
 *
 */
public class Sign52ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportSOService importSOService=(IImportSOService) ContextUtils.getBean("importSOServiceProxy");
		
		importSOService.findSign52(ctx);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;	
	}

}
