package com.vvme.order.validate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.ExcelRowResult.CellResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.service.IImportOrdersService;
import com.vvme.order.util.CompareUtils;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign2ValidateHandler
 * @Description: 
 * @author yetl
 * @date 2015年4月27日
 *
 */
public class Sign2ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportOrdersService importOrdersService=(IImportOrdersService) ContextUtils.getBean("importOrderServiceProxy");
		IExcelTableService excelTableMapper=(IExcelTableService) ContextUtils.getBean("excelTableServiceProxy");
		
		String cid=ctx.getCid();
		AdminLoginBean user=ctx.getUser();
		long importer=user.getAdid();
		
		DBRow [] datas=importOrdersService.findSign2(cid,importer);
		Collection<FieldInfo> flist=(Collection<FieldInfo>) excelTableMapper.getImportTemplateInfoByMid(ctx.getImpId()).values();
		
		for(DBRow row:datas){
			String dn=row.getString("dn");
			
			ArrayList<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
			DBRow [] rows=importOrdersService.findByDn(dn,cid,importer);
			
			for(DBRow r:rows){
				list.add(DBRowUtils.dbRowAsMap(r));
			}
			
			//得到比较结果
			Set<String> set=CompareUtils.compareByMap(list);
			if(set.isEmpty()){
			}else{
				importOrdersService.update(dn, cid, importer, ExcelConstants.ERROR_TYPE_2);
				for(int i=0;i<rows.length;i++){
					ExcelRowResult excelRst=new ExcelRowResult();
					excelRst.setRow(Integer.parseInt(rows[i].getString("row")));
					excelRst.setMsg(ExcelConstants.ERROR_MAP.get(ExcelConstants.ERROR_TYPE_2));
					excelRst.setDn(dn);
					
					for(String key:set){
						for(FieldInfo info:flist){
							if(info.getName().equals(key)){
								CellResult cellResult=excelRst.createCellResult();
								cellResult.setLine(info.getExcelSn());
								cellResult.setMsg(ExcelConstants.formatMessage(ExcelConstants.TMP_ERR_TYPE_DN,info.getXlsTitle()));
								excelRst.addCellResult(cellResult);
								
							}
						}
					}
					ctx.addExcelRowResult(excelRst);
				}
			}
		}
		
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
