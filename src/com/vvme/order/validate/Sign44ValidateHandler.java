package com.vvme.order.validate;

import com.cwc.app.beans.AdminLoginBean;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportOrdersService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign44ValidateHandler
 * @Description: "MABD must be after the Req.ShipDate";
 * 				"Mabd时间必须大于Req Ship Date";
 * @author yetl
 * @date 2015年5月6日
 *
 */
public class Sign44ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportOrdersService importOrdersService=(IImportOrdersService) ContextUtils.getBean("importOrderServiceProxy");

		String cid=ctx.getCid();
		AdminLoginBean user=ctx.getUser();
		long importer=user.getAdid();
		
		importOrdersService.findSign44(cid, importer);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
