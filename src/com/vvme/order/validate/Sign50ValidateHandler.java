package com.vvme.order.validate;

import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportLoadAppService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign50ValidateHandler
 * @Description: 相同DN出现不唯一loadno,pickup_appointment_date,pickup_appointment_time,freight_type.的对应关系，具体请看相对应单元格 优先级3
 * @author yetl
 * @date 2015年4月29日
 *
 */
public class Sign50ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportLoadAppService importLoadAppService=(IImportLoadAppService) ContextUtils.getBean("importLoadAppServiceProxy");
		
		importLoadAppService.findSign50(ctx);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
