package com.vvme.order.validate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.axis.utils.StringUtils;

import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.ExcelRowResult.CellResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.service.IImportOrdersService;
import com.vvme.order.util.ContextUtils;
import com.vvme.order.util.ExcelUtils;

/**
 * 
 * @ClassName: ExcelSingleCustomerValidate
 * @Description: 单一客户校验
 * @author yetl
 * @date 2015年4月25日
 *
 */
public class ExcelSingleCustomerValidate implements IExcelTemplateValidate {

	public boolean validate(ProcessContext ctx) throws Exception {
		String[][] data = ctx.getExcelData().getDataArray();
		int[] rows = ctx.getExcelData().getDataRows();

		IExcelTableService excelTableMapper=(IExcelTableService) ContextUtils.getBean("excelTableServiceProxy");
		Map<Integer, FieldInfo> fields = excelTableMapper
				.getImportTemplateInfoByMid(ctx.getImpId());
		FieldInfo field = ExcelUtils.getExcelSn(fields, "customer");
		int sn = field.getExcelSn();
		TreeMap<String, Set<Integer>> map = new TreeMap<String, Set<Integer>>();
		boolean flag=true;
		
		for (int i = 0; i < data.length; i++) {
			String[] hData = data[i];
			String customer = hData[sn];

			if (!StringUtils.isEmpty(customer)) {
				customer = customer.trim().toUpperCase();
				if (map.containsKey(customer)) {
					Set<Integer> set = map.get(customer);
					set.add(rows[i]);
					map.put(customer, set);
				} else {
					Set<Integer> set = new HashSet<Integer>();
					set.add(rows[i]);
					map.put(customer, set);
				}
			}
		}

		if (map.keySet().size() > 1) { // 超过1个customer
			flag=false;
			List<Map.Entry<String, Set<Integer>>> l = new ArrayList<Map.Entry<String, Set<Integer>>>(
					map.entrySet());
			Collections.sort(l,
					new Comparator<Map.Entry<String, Set<Integer>>>() {
						public int compare(Entry<String, Set<Integer>> o1,
								Entry<String, Set<Integer>> o2) {
							return o2.getValue().size() - o1.getValue().size();
						}
					});

			int i = 0;
			for (Map.Entry<String, Set<Integer>> mapping : l) {
				if (i == 0) {
					i++;
					continue;
				}
				Set<Integer> s = mapping.getValue();
				for (int row : s) {
					ExcelRowResult rst = new ExcelRowResult();
					rst.setRow(row);
					rst.setMsg(ExcelConstants.ERROR_MSG_DATABASE);
					CellResult cellResult = rst.createCellResult();
					cellResult.setLine(sn);
					cellResult.setMsg("Only one Customer ID will be accepted");
					rst.addCellResult(cellResult);

					ctx.addExcelRowResult(rst);
				}
			}

		}else{
			if(data.length>0){
				String customer = data[0][sn];
				IImportOrdersService importOrdersService=(IImportOrdersService) ContextUtils.getBean("importOrderServiceProxy");
				String cid=importOrdersService.getCidByName(customer);
				if(null==cid){
					flag=false;
					for(int i = 0; i < data.length; i++) {
						ExcelRowResult rst = new ExcelRowResult();
						rst.setRow(rows[i]);
						rst.setMsg(ExcelConstants.ERROR_MSG_DATABASE);
						CellResult cellResult = rst.createCellResult();
						cellResult.setLine(sn);
						cellResult.setMsg("Invalid Customer");
						rst.addCellResult(cellResult);

						ctx.addExcelRowResult(rst);
					}
				}
				ctx.setCid(cid);
			}
		}
		
		return flag;
	}

	public boolean openCheck() {
		return true;
	}

}
