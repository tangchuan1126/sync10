package com.vvme.order.validate;

import com.cwc.app.beans.AdminLoginBean;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportLoadAppService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign4ValidateHandler
 * @Description: 更新load appt时标记出不存在的DN 优先级1
 * @author yetl
 * @date 2015年4月29日
 *
 */
public class Sign4ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportLoadAppService importLoadAppService=(IImportLoadAppService) ContextUtils.getBean("importLoadAppServiceProxy");
		String cid=ctx.getCid();
		AdminLoginBean user=ctx.getUser();
		long importer=user.getAdid();
		
		importLoadAppService.findSign4(cid, importer);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
