package com.vvme.order.validate;

import com.vvme.order.ProcessContext;

/**
 * 
 * @ClassName: ValidateHandler
 * @Description: 滤出各种错误
 * @author yetl
 * @date 2015年4月24日
 *
 */
public abstract class ValidateHandler {
	private ValidateHandler nextHandler;
	
	public abstract void handleRequest(ProcessContext ctx) throws Exception;

	public ValidateHandler getNextHandler() {
		return nextHandler;
	}

	public void setNextHandler(ValidateHandler nextHandler) {
		this.nextHandler = nextHandler;
	}
	
}
