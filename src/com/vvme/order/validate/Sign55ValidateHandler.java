package com.vvme.order.validate;

import com.cwc.app.beans.AdminLoginBean;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportOrdersService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign55ValidateHandler
 * @Description: ShipNoLater cannot be in the past
 * @author yetl
 * @date 2015年5月14日
 *
 */
public class Sign55ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportOrdersService importOrdersService=(IImportOrdersService) ContextUtils.getBean("importOrderServiceProxy");

		String cid=ctx.getCid();
		AdminLoginBean user=ctx.getUser();
		long importer=user.getAdid();
		
		importOrdersService.findSign55(cid, importer);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
