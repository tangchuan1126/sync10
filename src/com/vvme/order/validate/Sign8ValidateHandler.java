package com.vvme.order.validate;

import com.cwc.app.beans.AdminLoginBean;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportOrdersService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign8ValidateHandler
 * @Description: 未在系统中找到相应的库房信息
 * @author yetl
 * @date 2015年5月13日
 *
 */
public class Sign8ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportOrdersService importOrdersService=(IImportOrdersService) ContextUtils.getBean("importOrderServiceProxy");

		String cid=ctx.getCid();
		AdminLoginBean user=ctx.getUser();
		long importer=user.getAdid();
		
		importOrdersService.findSign8(cid, importer);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
