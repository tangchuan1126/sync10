package com.vvme.order.validate;

import java.util.Map;

import org.apache.axis.utils.StringUtils;

import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.util.ContextUtils;
import com.vvme.order.util.ExcelUtils;

/**
 * 
 * @ClassName: ExcelImportOrdersValidate
 * @Description: 导入订单的文件防止导入成load
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ExcelImportOrdersValidate implements IExcelTemplateValidate{

	public boolean validate(ProcessContext ctx) throws Exception {
		String [][] data=ctx.getExcelData().getDataArray();
		IExcelTableService excelTableMapper=(IExcelTableService) ContextUtils.getBean("excelTableServiceProxy");
		Map<Integer,FieldInfo> fields=excelTableMapper.getImportTemplateInfoByMid(ctx.getImpId());
		
		FieldInfo apptInfo=ExcelUtils.getExcelSn(fields,"pickup_appointment_date");
		FieldInfo loadInfo=ExcelUtils.getExcelSn(fields,"loadno");
		FieldInfo apptimeInfo=ExcelUtils.getExcelSn(fields,"pickup_appointment_time");
		
		for(int i=0;i<data.length;i++){
			String [] hData=data[i];
			
			int appt=apptInfo.getExcelSn();
			int load=loadInfo.getExcelSn();
			int apptime=apptimeInfo.getExcelSn();
			
			if(!StringUtils.isEmpty(hData[appt]) || !StringUtils.isEmpty(hData[load]) 
					|| !StringUtils.isEmpty(hData[apptime])){
				return false;
			}
		}
		
		return true;
	}

	public boolean openCheck() {
		return true;
	}

}
