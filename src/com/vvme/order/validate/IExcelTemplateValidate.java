package com.vvme.order.validate;

import com.vvme.order.ProcessContext;

/**
 * 
 * @ClassName: IExcelTemplateValidate
 * @Description: 模板校验
 * @author yetl
 * @date 2015年4月24日
 *
 */
public interface IExcelTemplateValidate {
	/**
	 * excel 模板校验
	 * @param path 需要校验格式的路径
	 * @return
	 */
	 boolean validate(ProcessContext ctx)  throws Exception;
	 
	 /**
	  * 校验开关
	  * @return true:校验 false:不校验
	  */
	 boolean openCheck();
}
