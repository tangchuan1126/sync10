package com.vvme.order.validate;

import com.cwc.app.beans.AdminLoginBean;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportOrdersService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign11ValidateHandler
 * @Description: 
 * @author yetl
 * @date 2015年4月27日
 *
 */
public class Sign11ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
//		IImportOrdersService importOrdersService=ContextUtils.getBean(IImportOrdersService.class);
		IImportOrdersService importOrdersService=(IImportOrdersService) ContextUtils.getBean("importOrderServiceProxy");
		String cid=ctx.getCid();
		AdminLoginBean user=ctx.getUser();
		long importer=user.getAdid();
		
		importOrdersService.findSign11(cid,importer);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
