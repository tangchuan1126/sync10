package com.vvme.order.validate;

import java.util.Map;

import org.apache.axis.utils.StringUtils;

import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.ExcelRowResult.CellResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.util.CommonUtils;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: ExcelDataBaseValidate
 * @Description: 数据类型校验
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ExcelDataBaseValidate implements IExcelTemplateValidate{

	public boolean validate(ProcessContext ctx) throws Exception {
		String [][] data=ctx.getExcelData().getDataArray();
		int [] rows=ctx.getExcelData().getDataRows();
		IExcelTableService excelTableMapper=(IExcelTableService) ContextUtils.getBean("excelTableServiceProxy");
		Map<Integer,FieldInfo> fields=excelTableMapper.getImportTemplateInfoByMid(ctx.getImpId());
		
		boolean pass=true;
		for(int i=0;i<data.length;i++){
			String [] hData=data[i];
			int row=rows[i];
			
			ExcelRowResult rst=new ExcelRowResult();
			rst.setRow(row);
			rst.setData(hData);
			
			boolean exists=validate(fields,hData,row,rst);
			if(exists){
				pass=false;
			}
			
			rst.setIndex(i); //设置下标
			ctx.addExcelRowResult(rst);
		}
		return pass;
	}

	public boolean openCheck() {
		return true;
	}
	
	private boolean validate(Map<Integer,FieldInfo> fields,String [] hData,int i,ExcelRowResult rst){
		boolean existsError=false;
		
		for(int j=0;j<hData.length;j++){
			String data=hData[j];
			FieldInfo fInfo=fields.get(j);
			
			if(null==fInfo)
				continue;
			
			if(fInfo.getIsNil().equalsIgnoreCase("yes") && StringUtils.isEmpty(data))
				continue;
			
			String type=fInfo.getType();
			
			if(fInfo.getIsNil().equalsIgnoreCase("no") && StringUtils.isEmpty(data)){
				existsError=true;
				CellResult cellResult=rst.createCellResult();
				cellResult.setLine(j);
				cellResult.setMsg("Data cannot be empty");
				rst.addCellResult(cellResult);
				continue;
			}
			
			switch(type){
				case "varchar": //字符
					int mlen=fInfo.getMaxLen();
					
					if(data.trim().length()>mlen){
						existsError=true;
						CellResult cellResult=rst.createCellResult();
						cellResult.setLine(j);
						cellResult.setMsg("["+data+"]Exceeded allowed character."+mlen);
						rst.addCellResult(cellResult);
					}
					break;
				case "int":
					if(!CommonUtils.isInteger(data)){
						existsError=true;
						CellResult cellResult=rst.createCellResult();
						cellResult.setLine(j);
						cellResult.setMsg("["+data+"]Please enter a whole number.");
						rst.addCellResult(cellResult);
					}else{
							if(data.indexOf(".")>0){
								data=data.substring(0,data.indexOf("."));
								hData[j]=data;
							}
							if(!CommonUtils.parseInt(data)){
								existsError=true;
								CellResult cellResult=rst.createCellResult();
								cellResult.setLine(j);
								cellResult.setMsg("["+data+"]Please enter a whole number.");
								rst.addCellResult(cellResult);
							}else{
								if(Integer.parseInt(data)<0){
									existsError=true;
									CellResult cellResult=rst.createCellResult();
									cellResult.setLine(j);
									cellResult.setMsg("["+data+"]Please enter a numeric value.");
									rst.addCellResult(cellResult);
								}
							}
					}
					break;
				case "tinyint":
					if(!CommonUtils.isInteger(data)){
						existsError=true;
						CellResult cellResult=rst.createCellResult();
						cellResult.setLine(j);
						cellResult.setMsg("["+data+"]Please enter a numeric value.");
						rst.addCellResult(cellResult);
					}else{
						if(Integer.parseInt(data)<0){
							existsError=true;
							CellResult cellResult=rst.createCellResult();
							cellResult.setLine(j);
							cellResult.setMsg("["+data+"]Please enter a numeric value.");
							rst.addCellResult(cellResult);
						}
					}
					break;
				case  "decimal":
					if(!CommonUtils.isDecimalNumber(data)){
						existsError=true;
						CellResult cellResult=rst.createCellResult();
						cellResult.setLine(j);
						cellResult.setMsg("["+data+"]Please enter a numeric value.");
						rst.addCellResult(cellResult);
					}else{
						if(Double.parseDouble(data)<0){
							existsError=true;
							CellResult cellResult=rst.createCellResult();
							cellResult.setLine(j);
							cellResult.setMsg( "["+data+"]Please enter a value greater than zero.");
							rst.addCellResult(cellResult);
						}
					}
					break;
				case "date":
					String format=fInfo.getFormat();
					if(!CommonUtils.isValidDate(data,format)){
						existsError=true;
						CellResult cellResult=rst.createCellResult();
						cellResult.setLine(j);
						cellResult.setMsg(  "["+data+"]Please enter a valid date format ("+format+").");
						rst.addCellResult(cellResult);
					}
					break;
				case "time":
					if(!CommonUtils.checkTime(data))
					{
						existsError=true;
						CellResult cellResult=rst.createCellResult();
						cellResult.setLine(j);
						cellResult.setMsg("["+data+"]Please enter a valid time format (HH:MM).");
						rst.addCellResult(cellResult);
					}
					break;
				case "datetime":
					format=fInfo.getFormat();
					if(!CommonUtils.isValidDateTime(data))
					{
						existsError=true;
						CellResult cellResult=rst.createCellResult();
						cellResult.setLine(j);
						cellResult.setMsg("["+data+"] is not a date and time format, enter the date and time format ("+format+").");
						rst.addCellResult(cellResult);
					}
					break;
				default:
					break;
					
			}
		}
		
		if(existsError){
			rst.setMsg(ExcelConstants.ERROR_MSG_DATABASE);
		}
		return existsError;
	}
}
