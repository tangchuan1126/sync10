package com.vvme.order.validate;

import com.cwc.app.beans.AdminLoginBean;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportLoadAppService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign9ValidateHandler
 * @Description: 系统中状态为Close
 * @author yetl
 * @date 2015年5月13日
 *
 */
public class Sign9ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportLoadAppService importLoadAppService=(IImportLoadAppService) ContextUtils.getBean("importLoadAppServiceProxy");
		String cid=ctx.getCid();
		AdminLoginBean user=ctx.getUser();
		long importer=user.getAdid();
		
		importLoadAppService.findSign9(cid, importer);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
