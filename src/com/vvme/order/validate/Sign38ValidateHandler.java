package com.vvme.order.validate;

import com.cwc.app.beans.AdminLoginBean;
import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportLoadAppService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign38ValidateHandler
 * @Description: 找出未确认库存的订单 优先级2
 * @author yetl
 * @date 2015年4月29日
 *
 */
public class Sign38ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportLoadAppService importLoadAppService=(IImportLoadAppService) ContextUtils.getBean("importLoadAppServiceProxy");
		String cid=ctx.getCid();
		AdminLoginBean user=ctx.getUser();
		long importer=user.getAdid();
		
		importLoadAppService.findSign38(cid, importer);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
