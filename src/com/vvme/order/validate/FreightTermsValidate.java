package com.vvme.order.validate;

import java.util.Map;

import org.apache.axis.utils.StringUtils;

import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.ExcelRowResult.CellResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.dic.EnumUtils;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.util.ContextUtils;
import com.vvme.order.util.ExcelUtils;

/**
 * 
 * @ClassName: FreightTermsValidate
 * @Description: 校验支付方式
 * @author yetl
 * @date 2015年4月25日
 *
 */
public class FreightTermsValidate implements IExcelTemplateValidate{

	public boolean validate(ProcessContext ctx) throws Exception {
		String [][] data=ctx.getExcelData().getDataArray();
		int [] rows=ctx.getExcelData().getDataRows();
		IExcelTableService excelTableMapper=(IExcelTableService) ContextUtils.getBean("excelTableServiceProxy");
		Map<Integer,FieldInfo> fields=excelTableMapper.getImportTemplateInfoByMid(ctx.getImpId());
		FieldInfo field=ExcelUtils.getExcelSn(fields,"ft");
		FieldInfo billField=ExcelUtils.getExcelSn(fields,"bill_to");
		FieldInfo shipTypeField=ExcelUtils.getExcelSn(fields,"order_type");
		
		int sn=field.getExcelSn();
		int billSn=billField.getExcelSn();
		int shipSn=shipTypeField.getExcelSn();
		
		boolean flag=true;
		
		for(int i=0;i<data.length;i++){
			String [] hData=data[i];
			int row=rows[i];
			String ft=hData[sn];
			String shipType=hData[shipSn];
			
			if(!StringUtils.isEmpty(ft)){
				if(!EnumUtils.isFreightTerms(ft)){
					ExcelRowResult rst=new ExcelRowResult();
					rst.setRow(row);
					rst.setMsg(ExcelConstants.ERROR_MSG_DATABASE);
					CellResult cellResult = rst.createCellResult();
					cellResult.setLine(sn);
					cellResult.setMsg( "Incorrect "+field.getXlsTitle()+".  Please enter either Prepaid or Collect or Third Party/3rd Party.");
					rst.addCellResult(cellResult);

					ctx.addExcelRowResult(rst);
					flag=false;
				}else{
					if(EnumUtils.isThirdParty(ft)){
						String bill=hData[billSn];
						if(StringUtils.isEmpty(bill)){
							ExcelRowResult rst=new ExcelRowResult();
							rst.setRow(row);
							rst.setMsg(ExcelConstants.ERROR_MSG_DATABASE);
							CellResult cellResult = rst.createCellResult();
							cellResult.setLine(billSn);
							cellResult.setMsg( "Please enter the Bill To name and address");
							rst.addCellResult(cellResult);

							ctx.addExcelRowResult(rst);
							flag=false;
						}
					}
				}
			}else{
				if(!EnumUtils.isEndUser(shipType)){
					ExcelRowResult rst=new ExcelRowResult();
					rst.setRow(row);
					rst.setMsg(ExcelConstants.ERROR_MSG_DATABASE);
					CellResult cellResult = rst.createCellResult();
					cellResult.setLine(sn);
					cellResult.setMsg( "Incorrect "+field.getXlsTitle()+" can not empty.");
					rst.addCellResult(cellResult);

					ctx.addExcelRowResult(rst);
					flag=false;
				}
			}
		}
		
		return flag;
	}

	public boolean openCheck() {
		return true;
	}

}
