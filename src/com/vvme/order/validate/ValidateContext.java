package com.vvme.order.validate;

import com.vvme.order.ProcessContext;

/**
 * 
 * @ClassName: ValidateContext
 * @Description: 
 * @author yetl
 * @date 2015年4月24日
 *
 */
public class ValidateContext {
	private IExcelTemplateValidate excelValidate;
	
	public ValidateContext(IExcelTemplateValidate excelValidate){
		this.excelValidate=excelValidate;
	}
	
	public boolean validate(ProcessContext ctx) throws Exception{
		return this.excelValidate.validate(ctx);
	}
	
	public boolean openCheck(){
		return this.excelValidate.openCheck();
	}
}
