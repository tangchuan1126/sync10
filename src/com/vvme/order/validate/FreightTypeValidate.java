package com.vvme.order.validate;

import java.util.Map;

import org.apache.axis.utils.StringUtils;

import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.ExcelRowResult.CellResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.dic.EnumUtils;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.util.ContextUtils;
import com.vvme.order.util.ExcelUtils;

/**
 * 
 * @ClassName: FreightTypeValidate
 * @Description: 校验运输方式
 * @author yetl
 * @date 2015年4月25日
 *
 */
public class FreightTypeValidate implements IExcelTemplateValidate{

	public boolean validate(ProcessContext ctx) throws Exception {
		String [][] data=ctx.getExcelData().getDataArray();
		int [] rows=ctx.getExcelData().getDataRows();
		IExcelTableService excelTableMapper=(IExcelTableService) ContextUtils.getBean("excelTableServiceProxy");
		Map<Integer,FieldInfo> fields=excelTableMapper.getImportTemplateInfoByMid(ctx.getImpId());
		FieldInfo field=ExcelUtils.getExcelSn(fields,"freight_type");
		boolean flag=true;
		int sn=field.getExcelSn();
		
		for(int i=0;i<data.length;i++){
			String [] hData=data[i];
			int row=rows[i];
			
			//Freight Type校验
			String ftype=hData[sn];
			
			if(!StringUtils.isEmpty(ftype)){
				if(!EnumUtils.isFreightType(ftype)){
					ExcelRowResult rst=new ExcelRowResult();
					rst.setRow(row);
					rst.setMsg(ExcelConstants.ERROR_MSG_DATABASE);
					CellResult cellResult = rst.createCellResult();
					cellResult.setLine(sn);
					cellResult.setMsg( "Incorrect "+field.getXlsTitle()+".  Please enter either IMDL or CONSOL or LTL or FTL or SBF or RAIL.");
					rst.addCellResult(cellResult);

					ctx.addExcelRowResult(rst);
					flag=false;
				}else{
					if(StringUtils.isEmpty(ftype)){
						ExcelRowResult rst=new ExcelRowResult();
						rst.setRow(row);
						rst.setMsg(ExcelConstants.ERROR_MSG_DATABASE);
						CellResult cellResult = rst.createCellResult();
						cellResult.setLine(sn);
						cellResult.setMsg( field.getXlsTitle()+" Cannot leave blank.  Please enter a valid value。");
						rst.addCellResult(cellResult);

						ctx.addExcelRowResult(rst);
						flag=false;
					}
				}
			}
			
		}
		
		return flag;
	}

	public boolean openCheck() {
		return true;
	}

}
