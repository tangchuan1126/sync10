package com.vvme.order.validate;

import com.vvme.order.ProcessContext;
import com.vvme.order.service.IImportLoadAppService;
import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: Sign51ValidateHandler
 * @Description: 相同DN出现不唯一FreightTerms,BillTo的对应关系，具体请看相对应单元格
 * @author yetl
 * @date 2015年5月13日
 *
 */
public class Sign51ValidateHandler extends ValidateHandler{

	public void handleRequest(ProcessContext ctx) throws Exception {
		IImportLoadAppService importLoadAppService=(IImportLoadAppService) ContextUtils.getBean("importLoadAppServiceProxy");
		
		importLoadAppService.findSign51(ctx);
		ValidateHandler handler=this.getNextHandler();
		if(handler!=null){
			handler.handleRequest(ctx);
		}
		return;
	}

}
