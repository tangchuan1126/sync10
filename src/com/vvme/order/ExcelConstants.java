package com.vvme.order;


import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 用于excel dn导入的所有常量
 * @author ye
 *
 */
public class ExcelConstants {
	public final static int maxSize = 10 * 1024 * 1024; // 50M
	
	public final static String ERROR_MSG_DATABASE="There is a problem with the data";
	
	
//	public final static int ERROR_TYPE_0 = 0; 
	public final static int ERROR_TYPE_1 = 1; // excel 原始数据错误,excel列对数据库字段类型校验
	public final static int ERROR_TYPE_2 = 2; // lines_tmp临时表里数据有不合理数据
	public final static int ERROR_TYPE_3 = 3; // order表里有的dn在excel里没有
	public final static int ERROR_TYPE_4 = 4; // 导入的数据,不用添加shipTo字典。
	public final static int ERROR_TYPE_5 = 5; // 相同的dn号但是lines不同
	public final static int ERROR_TYPE_6 = 6; // 国籍和州没找到的
	public final static int ERROR_TYPE_7 = 7; // shipTo 地址有变化的
	public final static int ERROR_TYPE_8 = 8; // 发货仓库映射没有找到
	public final static int ERROR_TYPE_9 = 9; // 相同的dn在数据库里的order已经关闭不能操作
	public final static int ERROR_TYPE_10 = 10; // 相同的dn在item相同但是生成运单不能操作
	public final static int ERROR_TYPE_11 = 11; //更新的数据,不用添加shipTo字典。
	public final static int ERROR_TYPE_12 = 12; //有其他操作员也导入了此dn号。
	public final static int ERROR_TYPE_13 = 13; // 导入的数据,需要添加shipTo字典。
	public final static int ERROR_TYPE_14 = 14; // 更新的数据,需要添加shipTo字典。
	public final static int ERROR_TYPE_15 = 15; // loadno关系被更新
	public final static int ERROR_TYPE_16 = 16; // 导入数据失败,可能被其他用户导入
	public final static int ERROR_TYPE_17 = 17; // 导入数据成功
	public final static int ERROR_TYPE_18 = 18; // 更新数据成功
	public final static int ERROR_TYPE_19 = 19; // 完全一样的dn
	public final static int ERROR_TYPE_20 = 20; // sku产品库中不存在。
	public final static int ERROR_TYPE_21 = 21; // 订单有变化
	public final static int ERROR_TYPE_22 = 22; // loadno关系有变化
	public final static int ERROR_TYPE_23 = 23; // appoint关系有变化
	public final static int ERROR_TYPE_24 = 24; // appoint关系可以更新
	public final static int ERROR_TYPE_25 = 25; //在比较地方显示
	public final static int ERROR_TYPE_26 = 26; //相同的loadno下pick up appointment不同。
	public final static int ERROR_TYPE_27 = 27; //账单地址不存在
	public final static int ERROR_TYPE_28 = 28; //约车数量达到上限,不让约车
	public final static int ERROR_TYPE_29 = 29; //仓库约车限制没有维护
	public final static int ERROR_TYPE_30 = 30; //shipTo不存在,请核实
	public final static int ERROR_TYPE_31 = 31; //load的关系发生变化
	public final static int ERROR_TYPE_32 = 32; //?
	public final static int ERROR_TYPE_33 = 33; //shipto zip code有问题
	public final static int ERROR_TYPE_34 = 34; //shipto address有问题
	public final static int ERROR_TYPE_35 = 35; //REQ.SHIP DATE日期已过
	public final static int ERROR_TYPE_36 = 36; //mabd日期已过
	public final static int ERROR_TYPE_37 = 37; //appointment日期已过
	public final static int ERROR_TYPE_38 = 38; //订单状态为importer
	public final static int ERROR_TYPE_39 = 39; //pickupappointdate有变化
	public final static int ERROR_TYPE_40 = 40; //pickupappointtime有变化
	public final static int ERROR_TYPE_41 = 41; //按order约车
	public final static int ERROR_TYPE_42 = 42; //删除order和appt的关系
	public final static int ERROR_TYPE_43 = 43; //删除order和appt的关系
	public final static int ERROR_TYPE_44 = 44; //mabd时间小于req ship date
	public final static int ERROR_TYPE_45 = 45; //carrier是否正确
	public final static int ERROR_TYPE_46 = 46; //title不合法
	public final static int ERROR_TYPE_47 = 47; //
	public final static int ERROR_TYPE_48 = 48;
	public final static int ERROR_TYPE_49 = 49;
	public final static int ERROR_TYPE_50 = 50;
	public final static int ERROR_TYPE_51 = 51;
	public final static int ERROR_TYPE_52 = 52;
	public final static int ERROR_TYPE_53 = 53;
	public final static int ERROR_TYPE_54 = 54;
	public final static int ERROR_TYPE_55 = 55;
	public final static int ERROR_TYPE_56 = 56;
	public final static int ERROR_TYPE_57 = 57;
	public final static int ERROR_TYPE_58 = 58;
	public final static int ERROR_TYPE_59 = 59;
	public final static int ERROR_TYPE_60 = 60;
	
	public final static String ERROR_MSG_1="Incorrect information found";//"发现数据异常，请核实。";
	public final static String ERROR_MSG_2="Inconsistent information found under the same DN No. ";//"相同DN出现不唯一SO PO SHIPTO TITLE.的对应关系，具体请看相对应单元格。";
	public final static String ERROR_MSG_3="Total order count did not match";//"系统中的DN，在导入数据中不存在，请核实。";
//	public final static String ERROR_MSG_4="New DN found";//"系统中不存在的DN"; //可以导入的数据。
	public final static String ERROR_MSG_4="The import failed";//"√"
	public final static String ERROR_MSG_5="Total SKU count did not match";//"系统中相同DN，但发现SKU的种类不同。";
	public  final static String ERROR_MSG_6="Incorrect Country or State found";//"Country和State在地址表不存在，请核实并在系统中中添加。";
	public final static String ERROR_MSG_7="Incorrect ShipToCity found";//"SHIPTOCITY可能有问题，请核实。";
	public final static String ERROR_MSG_8="Incorrect HUB name";//"未在系统中找到相应的库房信息。请核实。";
	public final static String ERROR_MSG_9="Uploaded DN was closed";//"系统中状态为Close。";
	public final static String ERROR_MSG_10="This DN No. had BOL";//"在系统中已经生成BOL。";
//	public final static String ERROR_MSG_11="可以更新的数据。";
	public final static String ERROR_MSG_11="DN No. already existed";//"系统中已经存在此dn。";
	public final static String ERROR_MSG_12="The other operator is imported into the DN.";//"有其他操作员也导入了此dn号。";
//	public final static String ERROR_MSG_13="shipTo不存在,请核实.";
	public final static String ERROR_MSG_13="The import failed";//"√";
//	public final static String ERROR_MSG_14="可以更新的数据,会自动添加shipTo信息。";
	public final static String ERROR_MSG_14="DN No. already existed";//"系统中已经存在此dn。";
//	public final static String ERROR_MSG_15="loadno关系将被更新。";
	public final static String ERROR_MSG_15="√";
	public final static String ERROR_MSG_16="Import data failure, may be imported into other users.";//"导入数据失败,可能被其他用户导入。";
	public final static String ERROR_MSG_17="Upload sucessfully";//"数据导入成功。";
	public final static String ERROR_MSG_18="Updated sucessfully";//"数据更新成功。";
	public final static String ERROR_MSG_19="√";
//	public final static String ERROR_MSG_20="Imported Lot No. or Model No. does not match the system";//"Lot和Model在系统中不存在。";
	public final static String ERROR_MSG_20="Imported Model No. does not match the record";//"Model在系统中不存在。";
	public final static String ERROR_MSG_21="Imported DN No. does not match the record";//"订单有变化。";
	public final static String ERROR_MSG_22="DN already contains LoadNo.";//"此DN在系统中的LoadNO已存在，无法更新，请确认。";
	public final static String ERROR_MSG_23="DN already contains appointment time";//"此DN在系统中的APPT已存在，无法更新，请确认。";
//	public final static String ERROR_MSG_24="Pick up appointment will be updated";//"pick up appointment关系将被更新。";
	public final static String ERROR_MSG_24="√"; //按load约
	public final static String ERROR_MSG_25="DN No. does not exist";//"此DN在系统中不存在。";
	public final static String ERROR_MSG_26="This LoadNo. contains different PickUpAppointmentDate or Time or Hub or Carrier Or RequiredShipDate.  Please check again";//"相同的LoadNO下PickUpAppointmentDate/Time/HUB/CARRIER/REQ.SHIP DATE不同,请检查。";
	public final static String ERROR_MSG_27="BillToAddress does not match the system";//"系统中根据号码找不到账单地址。";
	public final static String ERROR_MSG_28="No more appointment can be accepted at this hour";//"约车数量达到上限,不让约车。";
	public final static String ERROR_MSG_29="There is no limit about car maintenance warehouse";//"仓库约车限制没有维护。";
	public final static String ERROR_MSG_30="Ship TO does not exist";//"shipTo不存在,请核实";
	public final static String ERROR_MSG_31="LoadNo or DN No. does not match";//"此LoadNO下的订单关系有变化,请检查";
//	public final static String ERROR_MSG_32="Under the same Load other DN problems";//"同LoadNO下其他DN有问题";
	public final static String ERROR_MSG_32="";
	public final static String ERROR_MSG_33="ZipCode does not match the system";//"SHIPTO ZIP CODE可能是错误的,请检查";
	public final static String ERROR_MSG_34="Ship To Address does not exist";//"SHIPTO ADDRESS可能是错误的,请检查";   //允许导入
	public final static String ERROR_MSG_35="Req.ShipDate cannot be in the past";//"REQ.SHIP DATE日期已过,请检查";
	public final static String ERROR_MSG_36="MABD cannot be in the past";//"MABD日期已过,请检查";
	public final static String ERROR_MSG_37="PickUpAppointmentDate cannot be in the past";//"PickUpAppointment时间小于当前时间,请检查";
	public final static String ERROR_MSG_38="Insufficient inventory, upload failed";//"订单状态为Imported,请检查";
	public final static String ERROR_MSG_39="PickupAppointmentDate does not match";//"PickUpAppointmentDate被修改,请检查";
	public final static String ERROR_MSG_40="Missing Carrier information";//"更新LoadNo/app时carrier不能空,请检查";
	public final static String ERROR_MSG_41="√"; //按order约 
	public final static String ERROR_MSG_42="√"; // 
	public final static String ERROR_MSG_43="This DN contains duplicate Model No.";//"相同DN下的lines不能出现重复的SKU,请检查"; //
	public final static String ERROR_MSG_44="MABD must be after the Req.ShipDate";//"Mabd时间必须大于Req Ship Date";
	public final static String ERROR_MSG_45="Imported SCAC does not match the system";//"Carrier不存在,请检查";
	public final static String ERROR_MSG_46="Imported Title does not match the system";//"title不存在,请检查";
	public final static String ERROR_MSG_47="PickUpAppointment must be before the MABD";//"PickUpAppointment时间大于mabd时间,请检查";
	public final static String ERROR_MSG_48="Imported Retailer information does not match the system";//"Ship to type不存在,请检查";
	public final static String ERROR_MSG_49="Imported Retailer does not match Ship to Party Name";//"Ship to type和Ship to party name不一致,请检查";
	public final static String ERROR_MSG_50="Inconsistent LoadNo./APPT found under the same DN No.";//"相同DN出现不唯一LOADNO APPT.的对应关系，具体请看相对应单元格。";
	public final static String ERROR_MSG_51="The same DN not only FreightTerms, corresponding to BillTo, please see the corresponding cell";//"相同DN出现不唯一FreightTerms,BillTo的对应关系，具体请看相对应单元格。";
	public final static String ERROR_MSG_52="The value of SO to change, please check.";//"SO的值发生变化,请检查。";
	public final static String ERROR_MSG_53="FTL A truck orders only.";//"一个order只能产生一个FTL load。";
	public final static String ERROR_MSG_54="ShipNotBefore cannot be in the past";//"";
	public final static String ERROR_MSG_55="ShipNoLater cannot be in the past";//"";
	public final static String ERROR_MSG_56="Req.ShipDate must be after the Ship Not Before";//"";
	public final static String ERROR_MSG_57="Ship No Later must be after the Req.ShipDate";//"";
	public final static String ERROR_MSG_58="ZipCode and Country does not match the system";//"";
	public final static String ERROR_MSG_59="The same PickUpAppointment with LTL/Consol contains different SCAC."; //同一约车点对于LTL CONSOL出现不同的carr
	public final static String ERROR_MSG_60="Multiple bill addresses found."; //出现多个bill地址
	
	public final static Map<Integer,String> ERROR_MAP=new HashMap<Integer,String>();
	
	static {
		ERROR_MAP.put(ERROR_TYPE_1, ERROR_MSG_1);
		ERROR_MAP.put(ERROR_TYPE_2, ERROR_MSG_2);
		ERROR_MAP.put(ERROR_TYPE_3, ERROR_MSG_3);
		ERROR_MAP.put(ERROR_TYPE_4, ERROR_MSG_4);
		ERROR_MAP.put(ERROR_TYPE_5, ERROR_MSG_5);
		ERROR_MAP.put(ERROR_TYPE_6, ERROR_MSG_6);
		ERROR_MAP.put(ERROR_TYPE_7, ERROR_MSG_7);
		ERROR_MAP.put(ERROR_TYPE_8, ERROR_MSG_8);
		ERROR_MAP.put(ERROR_TYPE_9, ERROR_MSG_9);
		ERROR_MAP.put(ERROR_TYPE_10, ERROR_MSG_10);
		ERROR_MAP.put(ERROR_TYPE_11, ERROR_MSG_11);
		ERROR_MAP.put(ERROR_TYPE_12, ERROR_MSG_12);
		ERROR_MAP.put(ERROR_TYPE_13, ERROR_MSG_13);
		ERROR_MAP.put(ERROR_TYPE_14, ERROR_MSG_14);
		ERROR_MAP.put(ERROR_TYPE_15, ERROR_MSG_15);
		ERROR_MAP.put(ERROR_TYPE_16, ERROR_MSG_16);
		ERROR_MAP.put(ERROR_TYPE_17, ERROR_MSG_17);
		ERROR_MAP.put(ERROR_TYPE_18, ERROR_MSG_18);
		ERROR_MAP.put(ERROR_TYPE_19, ERROR_MSG_19);
		ERROR_MAP.put(ERROR_TYPE_20, ERROR_MSG_20);
		ERROR_MAP.put(ERROR_TYPE_21, ERROR_MSG_21);
		ERROR_MAP.put(ERROR_TYPE_22, ERROR_MSG_22);
		ERROR_MAP.put(ERROR_TYPE_23, ERROR_MSG_23);
		ERROR_MAP.put(ERROR_TYPE_24, ERROR_MSG_24);
		ERROR_MAP.put(ERROR_TYPE_25, ERROR_MSG_25);
		ERROR_MAP.put(ERROR_TYPE_26, ERROR_MSG_26);
		ERROR_MAP.put(ERROR_TYPE_27, ERROR_MSG_27);
		ERROR_MAP.put(ERROR_TYPE_28, ERROR_MSG_28);
		ERROR_MAP.put(ERROR_TYPE_29, ERROR_MSG_29);
		ERROR_MAP.put(ERROR_TYPE_30, ERROR_MSG_30);
		ERROR_MAP.put(ERROR_TYPE_31, ERROR_MSG_31);
		ERROR_MAP.put(ERROR_TYPE_32, ERROR_MSG_32);
		ERROR_MAP.put(ERROR_TYPE_33, ERROR_MSG_33);
		ERROR_MAP.put(ERROR_TYPE_34, ERROR_MSG_34);
		ERROR_MAP.put(ERROR_TYPE_35, ERROR_MSG_35);
		ERROR_MAP.put(ERROR_TYPE_36, ERROR_MSG_36);
		ERROR_MAP.put(ERROR_TYPE_37, ERROR_MSG_37);
		ERROR_MAP.put(ERROR_TYPE_38, ERROR_MSG_38);
		ERROR_MAP.put(ERROR_TYPE_39, ERROR_MSG_39);
		ERROR_MAP.put(ERROR_TYPE_40, ERROR_MSG_40);
		ERROR_MAP.put(ERROR_TYPE_41, ERROR_MSG_41);
		ERROR_MAP.put(ERROR_TYPE_42, ERROR_MSG_42);
		ERROR_MAP.put(ERROR_TYPE_43, ERROR_MSG_43);
		ERROR_MAP.put(ERROR_TYPE_44, ERROR_MSG_44);
		ERROR_MAP.put(ERROR_TYPE_45, ERROR_MSG_45);
		ERROR_MAP.put(ERROR_TYPE_46, ERROR_MSG_46);
		ERROR_MAP.put(ERROR_TYPE_47, ERROR_MSG_47);
		ERROR_MAP.put(ERROR_TYPE_48, ERROR_MSG_48);
		ERROR_MAP.put(ERROR_TYPE_49, ERROR_MSG_49);
		ERROR_MAP.put(ERROR_TYPE_50, ERROR_MSG_50);
		ERROR_MAP.put(ERROR_TYPE_51, ERROR_MSG_51);
		ERROR_MAP.put(ERROR_TYPE_52, ERROR_MSG_52);
		ERROR_MAP.put(ERROR_TYPE_53, ERROR_MSG_53);
		ERROR_MAP.put(ERROR_TYPE_54, ERROR_MSG_54);
		ERROR_MAP.put(ERROR_TYPE_55, ERROR_MSG_55);
		ERROR_MAP.put(ERROR_TYPE_56, ERROR_MSG_56);
		ERROR_MAP.put(ERROR_TYPE_57, ERROR_MSG_57);
		ERROR_MAP.put(ERROR_TYPE_58, ERROR_MSG_58);
		ERROR_MAP.put(ERROR_TYPE_59, ERROR_MSG_59);
		ERROR_MAP.put(ERROR_TYPE_60, ERROR_MSG_60);
	}
	
	public final static int HTTP_CODE_ERROR=500; //服务器错误  参数错误 excel原始数据错误
	public final static int HTTP_CODE_OK=200; //正常导入
	
	public final static String SESSION_PIX_IMPORT="EXCEL_IMPORT:";
	public final static String SESSION_PIX_DIFF="EXCEL_DIFF:";
	public final static String SESSION_PIX_UPD_SO="EXCEL_UPD_SO:";
	
	public final static String COOKIE_PIX_IMPORT="PAGE_IMPORT:";
	public final static String COOKIE_PIX_DIFF="PAGE_DIFF:";
	public final static String COOKIE_PIX_UPD_SO="PAGE_UPD_SO:";
	
	public final static String FILE_PIX="file:";
	
	public final static String EXCEL_MODEL_TABLE_IMPORT="wms_order_lines_tmp";
	
	public final static String TIP_IMPORT_MSG="Invalid information found, please download the report to review the errors.  <br/>  Cells with incorrect information will be highlighted in yellow, please move your cursor over for more ";//"导入文件中存在异常数据，请下载如下Excel文件查看。<br/> 异常数据所在行标记红色，具体异常信息请看对应单元格上批注。";
	public final static String TIP_DIFF_MSG="Invalid information found, please download the report to review the errors.  <br/>  Cells with incorrect information will be highlighted in yellow, please move your cursor over for more ";//"导入文件中存在异常数据，请下载如下Excel文件查看。<br/> 异常数据所在行标记红色，具体异常信息请看对应单元格上批注。";
	public final static String TIP_ALLERROR_MSG="Invalid information found, please download the report to review the errors.  <br/>  Cells with incorrect information will be highlighted in yellow, please move your cursor over for more ";//"导入文件的所有数据都有问题,请下载excel文件修改数据！";
	public final static String TIP_EXCEL_VALIDATE_MSG="Invalid information found, please download the report to review the errors.  <br/>  Cells with incorrect information will be highlighted in yellow, please move your cursor over for more ";//"导入的excel存在数据错误,请下载修改后重新上传";
	public final static String TIP_IMPORT_CUSTOMER_ERR_MSG="Customer name does not match the system";//"导入的excel文件Customer不存在,请检查";
	public final static String TIP_IMPORT_FILE_ERR_MSG="The XLSX file to import DN has data of load/appointment, please check it";//"导入DN的xlsx文件中存在load/appointment数据,请检查导入文件是否正确";
	public final static String TIP_DATA_EMPTY="Data is empty";//"数据为空";
	
	/**
	 * 模板输出
	 */
	public final static String TMP_ERR_TYPE_DN="More than one {0} found.";
	public final static String TMP_ERR_TYPE_26="More than one {0} found.";
	
	public static String formatMessage(String message,Object ... objs){
		return MessageFormat.format(message, objs);
	}
}
