package com.vvme.order.action;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.vvme.order.ExcelConstants;
import com.vvme.order.ProcessContext;
import com.vvme.order.ResponseObject;
import com.vvme.order.SignManager;
import com.vvme.order.excel.IExcelProcess;
import com.vvme.order.exception.ExcelDataBaseException;
import com.vvme.order.exception.ImportTemplateInvalidateException;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.service.IFileService;
import com.vvme.order.service.IImportLoadAppService;
import com.vvme.order.util.FileServerUtils;
import com.vvme.order.validate.ValidateHandler;

/**
 * 
 * @ClassName: ExcelModLoadAppAction
 * @Description: 
 * @author yetl
 * @date 2015年4月29日
 *
 */
public class ExcelImportLoadAppAction extends AbstractExcelImportAction{
	private IImportLoadAppService importLoadAppService;
	private IExcelTableService excelTableService;
	private ValidateHandler validateHandler;
	private IExcelProcess excelProcess;
	private IFileService fileService;
	
	public void setExcelProcess(IExcelProcess excelProcess) {
		this.excelProcess = excelProcess;
	}

	public void setFileService(IFileService fileService) {
		this.fileService = fileService;
	}

	public void setValidateHandler(ValidateHandler validateHandler) {
		this.validateHandler = validateHandler;
	}

	public void setExcelTableService(IExcelTableService excelTableService) {
		this.excelTableService = excelTableService;
	}

	public void setImportLoadAppService(IImportLoadAppService importLoadAppService) {
		this.importLoadAppService = importLoadAppService;
	}

	@SuppressWarnings("unchecked")
	public void perform(HttpServletRequest request, HttpServletResponse response) throws Exception{
		preExecute(request,response);
		final String jsid=request.getSession().getId();
		ProcessContext ctx=getThreadVariable();
		ResponseObject resp=ctx.getResp();
		String fullPath=ctx.getPath();
		String fileName = request.getParameter("fileName");
		long importer=ctx.getUser().getAdid();
		
		Map<?,?> expMap=excelTableService.getExportOrdersTemplate(ctx.getCid());
		int desc=(Integer)expMap.get("error_column");
		int startRow=(Integer)expMap.get("start_row");
		int eptFileId=(Integer)expMap.get("load_export_file_id");
		int impFid=(Integer)expMap.get("load_import_file_id");
		ctx.setImpId(impFid);
		
		//导入临时表
		try{
			importLoadAppService.importOrdersToTemp(ctx);
		}catch(ExcelDataBaseException e){ //存在基础数据错误
			e.printStackTrace();
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			String downPath=downLoadUrl + "/"+eptFileId;
			FileServerUtils.getFile(downPath,fullPath,jsid);
			excelProcess.writeDataArrayToExcelFile(fullPath,ctx, desc, startRow,ctx.getCid());
			String fileId = FileServerUtils.uploadFile(uploadUrl, fullPath, jsid);
			fileService.update(Long.parseLong(fileId), fileName,"600");
			resp.setFileId(fileId);
			resp.setSuccessCount(0);
			resp.setFailCount(resp.getTotal());
			resp.setMsg(e.getMessage());
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}catch(ImportTemplateInvalidateException e){
			e.printStackTrace();
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg(e.getMessage());
			resp.setFileId(excelTableService.getImportLoadTemplateId("")+""); 
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}catch(Exception e){
			e.printStackTrace();
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg("Into the temporary table failure");
			resp.setFileId(""); //不让下载
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}finally{
			FileServerUtils.delTmpFile(fullPath);
		}

		String downPath=downLoadUrl + "/"+eptFileId;
		if(fullPath.endsWith(".xls"))
			fullPath+='x';
		FileServerUtils.getFile(downPath,fullPath,jsid);
		
		//开始基于业务逻辑的数据校验
		validateHandler.handleRequest(ctx);
		
		Object [] obj=importLoadAppService.findLoad(ctx);
		
		//查找出系统多出的dn
		Set<String> orders = importLoadAppService.findSysMoreDn(ctx.getCid(),importer);

		if (!orders.isEmpty()) {
			// 获取到明细
			DBRow[] datas = importLoadAppService.findOrderAndDetail(orders.toArray(new String[orders.size()]),ctx.getCid());
			excelProcess.addNewSheet(fullPath, datas,ctx.getImpId()); // 添加sheet表现需要添加的数据
		}
		
		try{
			Object[] objs=importLoadAppService.addLoad(ctx,(Map)obj[0],(Map)obj[1],(Set)obj[2]);
			importLoadAppService.synWmsLoadApp((Set<String>)objs[0]);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg(e.getMessage());
		}
		//===================
		
		importLoadAppService.exportImportLoadAppResult(ctx);
		SignManager.markExcelError(ctx);
		ctx.stateResult();
		excelProcess.writeDataArrayToExcelFile(fullPath,ctx, desc, startRow,ctx.getCid());
		ctx.destroy();
		//上传写入文件
		String fileId = FileServerUtils.uploadFile(uploadUrl, fullPath, jsid);
		if(fileName.endsWith(".xls"))
			fileName+='x';
		fileService.update(Long.parseLong(fileId), fileName,"600");
		resp.setFileId(fileId);
		FileServerUtils.delTmpFile(fullPath);
		String body=resp.toString();
		log.info("响应:"+body);
		throw new JsonException(body);
	}

}
