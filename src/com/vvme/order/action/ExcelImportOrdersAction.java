package com.vvme.order.action;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.vvme.order.ExcelConstants;
import com.vvme.order.ProcessContext;
import com.vvme.order.ResponseObject;
import com.vvme.order.SignManager;
import com.vvme.order.excel.IExcelProcess;
import com.vvme.order.exception.ExcelDataBaseException;
import com.vvme.order.exception.ImportFileInvalidateException;
import com.vvme.order.exception.ImportTemplateInvalidateException;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.service.IFileService;
import com.vvme.order.service.IImportOrdersService;
import com.vvme.order.util.CommonUtils;
import com.vvme.order.util.FileServerUtils;
import com.vvme.order.validate.ValidateHandler;

/**
 * 
 * @ClassName: ExcelImportOrdersAction
 * @Description: 订单导入
 * @author yetl
 * @date 2015年4月29日
 *
 */
public class ExcelImportOrdersAction extends AbstractExcelImportAction{
	private IImportOrdersService importOrdersService;
	private IExcelTableService excelTableService;
	private IFileService fileService;
	private ValidateHandler validateHandler;
	private IExcelProcess excelProcess;

	public void setFileService(IFileService fileService) {
		this.fileService = fileService;
	}

	public void setImportOrdersService(IImportOrdersService importOrdersService) {
		this.importOrdersService = importOrdersService;
	}

	public void setExcelProcess(IExcelProcess excelProcess) {
		this.excelProcess = excelProcess;
	}

	public void setExcelTableService(IExcelTableService excelTableService) {
		this.excelTableService = excelTableService;
	}

	public void setValidateHandler(ValidateHandler validateHandler) {
		this.validateHandler = validateHandler;
	}

	public void perform(HttpServletRequest request, HttpServletResponse response) throws Exception{
		preExecute(request,response);
		final String jsid=request.getSession().getId();
		ProcessContext ctx=getThreadVariable();
		ResponseObject resp=ctx.getResp();
		String fullPath=ctx.getPath();
		String fileName = request.getParameter("fileName");
		
		Map<?,?> expMap=excelTableService.getExportOrdersTemplate(ctx.getCid());
		int desc=(Integer)expMap.get("error_column");
		int startRow=(Integer)expMap.get("start_row");
		int eptFileId=(Integer)expMap.get("order_export_file_id");
		int impFid=(Integer)expMap.get("order_import_file_id");
		ctx.setImpId(impFid);
		ctx.setExpId(eptFileId);
		
		//导入临时表
		try{
			importOrdersService.importOrdersToTemp(ctx);
		}catch(ExcelDataBaseException e){ //存在基础数据错误
			e.printStackTrace();
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			String downPath=downLoadUrl + "/"+eptFileId;
			FileServerUtils.getFile(downPath,fullPath,jsid);
			excelProcess.writeDataArrayToExcelFile(fullPath,ctx, desc, startRow,ctx.getCid());
			String fileId = FileServerUtils.uploadFile(uploadUrl, fullPath, jsid);
			fileService.update(Long.parseLong(fileId), fileName,"600");
			resp.setFileId(fileId);
			resp.setSuccessCount(0);
			resp.setFailCount(resp.getTotal());
			resp.setMsg(e.getMessage());
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}catch(ImportTemplateInvalidateException e){
			e.printStackTrace();
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg("The file you imported doesn't comply the format of template!");
			resp.setFileId(impFid+""); 
			resp.setSuccessCount(0);
			resp.setFailCount(resp.getTotal());
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}catch(ImportFileInvalidateException e){
			e.printStackTrace();
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg(e.getMessage());
			resp.setFileId(impFid+""); 
			resp.setSuccessCount(0);
			resp.setFailCount(resp.getTotal());
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}catch(Exception e){
			e.printStackTrace();
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg(e.getMessage());
			resp.setFailCount(resp.getTotal());
			resp.setFileId(impFid+"");  //不让下载
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}finally{
			FileServerUtils.delTmpFile(fullPath);
		}
		
		String downPath=downLoadUrl + "/"+eptFileId;
		if(fullPath.endsWith(".xls"))
			fullPath+='x';
		FileServerUtils.getFile(downPath,fullPath,jsid);
		
		try{
			//开始基于业务逻辑的数据校验
			validateHandler.handleRequest(ctx);
			
			//导入订单
			Set<Long> b2bOids=new HashSet<Long>();
			String cid=ctx.getCid();
			AdminLoginBean user=ctx.getUser();
			long importer=ctx.getUser().getAdid();
			
			//查出可以处理的正确的数据
			DBRow [] rows=importOrdersService.findImportOrders(cid,importer);
			for(DBRow r:rows){
				String dn=r.getString("dn");
				int sign=(Integer)r.getValue("correct");
				
				if(SignManager.canImportOrders(sign)){
					try{
						long b2b_oid=importOrdersService.importOrder(dn,cid,sign,user);
						b2bOids.add(b2b_oid);
					}catch(Exception e){
						log.error("订单DN:"+dn+"导入失败,"+e.getMessage());
					}
				}else{
					log.info("导入订单时获取订单的转态出现非4 13 34转态码");
				}
			}
			
			if(CommonUtils.isElementCustomer(ctx.getCid())){
				try{
					importOrdersService.synWmsOrders(b2bOids,ctx.getUser());
				}catch(Exception e){
					e.printStackTrace();
					log.error("同步WMS库异常,"+e.getMessage());
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg(e.getMessage());
			resp.setFileId(impFid+"");  //不让下载
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}
		
		importOrdersService.exportImportOrdersResult(ctx);
		SignManager.markExcelError(ctx);
		ctx.stateResult();
		excelProcess.writeDataArrayToExcelFile(fullPath,ctx, desc, startRow,ctx.getCid());
		ctx.destroy();
		//上传写入文件
		String fileId = FileServerUtils.uploadFile(uploadUrl, fullPath, jsid);
		if(fileName.endsWith(".xls"))
			fileName+='x';
		fileService.update(Long.parseLong(fileId), fileName,"600");
		resp.setFileId(fileId);
		FileServerUtils.delTmpFile(fullPath);
		String body=resp.toString();
		log.info("响应:"+body);
		throw new JsonException(body);
	}

}
