package com.vvme.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.action.core.ActionFatherController;
import com.cwc.db.DBRow;
import com.cwc.exception.JsonException;
import com.vvme.order.service.ICommonService;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: GetAllTitleAction
 * @Description: 获取所有title
 * @author yetl
 * @date 2015年4月7日
 *
 */
public class GetAllTitleAction extends ActionFatherController{
	private ICommonService commonService;
	
	public void setCommonService(ICommonService commonService) {
		this.commonService = commonService;
	}

	public void perform(HttpServletRequest request, HttpServletResponse response) throws JsonException{
		JSONObject json=new JSONObject();
		json.put("code", 200);
		try{
			DBRow [] rows=commonService.findAllTitle();
			json.put("msg", "success");
			json.put("data", rows);
		}catch(Exception e){
			json.put("code", 500);
			json.put("msg", e.getMessage());
		}
		throw new JsonException(json.toString());
	}

}