package com.vvme.order.action;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.api.AdminMgr;
import com.cwc.app.beans.AdminLoginBean;
import com.cwc.exception.JsonException;
import com.cwc.util.StringUtil;
import com.vvme.order.ExcelConstants;
import com.vvme.order.ProcessContext;
import com.vvme.order.ResponseObject;
import com.vvme.order.util.Assert;
import com.vvme.order.util.FileServerUtils;

/**
 * 
 * @ClassName: AbstractExcelImportAction
 * @Description: 
 * @author yetl
 * @date 2015年4月29日
 *
 */
public abstract class AbstractExcelImportAction extends ActionFatherController {
	protected static Logger log = Logger.getLogger("action");

	protected String downLoadUrl = "";
	protected String uploadUrl = "";
	private ThreadLocal<ProcessContext> threadLocal=new ThreadLocal<ProcessContext>();
	
	public void setDownLoadUrl(String downLoadUrl) {
		this.downLoadUrl = downLoadUrl;
	}

	public void setUploadUrl(String uploadUrl) {
		this.uploadUrl = uploadUrl;
	}
	
	/**
	 * 本地变量赋值
	 * @param map
	 */
	private void setThreadVariable(ProcessContext ctx){
		threadLocal.set(ctx);
	}
	
	/**
	 * 获取本地线程变量
	 * @return
	 */
	protected ProcessContext getThreadVariable(){
		return threadLocal.get();
	}
	
	/**
	 * 记录输入参数
	 * @param request
	 */
	protected void logRequest(HttpServletRequest request){
		Map<String, String[]> parms=request.getParameterMap();
		Iterator<Entry<String, String[]>> it=parms.entrySet().iterator();
		StringBuilder sb=new StringBuilder("{");
		while(it.hasNext()){
			Entry<String, String[]> entry=it.next();
			String[] tmp=(String[])entry.getValue();
			sb.append(entry.getKey()).append("=");
			
			for(String i:tmp){
				sb.append(i).append(" | ");
			}
			
			sb.append(",");
		}
		sb.append("}");
		
		log.info(this.getClass().getName()+"--"+" 请求参数-->"+sb.toString());
	}
	
	protected void preExecute(HttpServletRequest request, HttpServletResponse response) throws ClientProtocolException, IOException, JsonException{
		logRequest(request);
		
		final String jsid=request.getSession().getId();
		ResponseObject resp=new ResponseObject();
		String fileId = request.getParameter("fileId");
		String fileName = request.getParameter("fileName");
		
		if(Assert.isEmpty(fileName)){
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg("上传文件不能为空!");
			log.info("响应:"+resp.toString());
			return;
		}
		
		resp.setFileId(fileId);
		resp.setFilename(fileName);

		String fullDownLoadFile = downLoadUrl + "/" + fileId;
		int ind = fileName.indexOf(".");
		String fix = fileName.substring(ind);
		String filePath=request.getServletContext().getRealPath("/upl_excel_tmp");
		File file=new File(filePath);
		
		if(!file.exists())
			file.mkdir();
		
		String fullPath = filePath+ File.separator + fileId + fix;
		// 下载文件
		int httpStatus=FileServerUtils.getFile(fullDownLoadFile, fullPath, jsid);
		
		if(httpStatus!=200){
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			FileServerUtils.delTmpFile(fullPath);
			resp.setMsg("From the file server to download a file, please check that the file is uploaded to the server,fileId:"+fileId);
			resp.setFileId(""); //不让下载
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}
		
		AdminLoginBean user = (new AdminMgr()).getAdminLoginBean(StringUtil.getSession(request));
		ProcessContext ctx=new ProcessContext();
		ctx.setPath(fullPath);
		ctx.setUser(user);
		ctx.setResp(resp);
		ctx.setFid(fileId);
		setThreadVariable(ctx);
	}

}
