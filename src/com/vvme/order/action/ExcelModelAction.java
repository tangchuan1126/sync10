package com.vvme.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.exception.JsonException;
import com.vvme.order.service.IExcelTableService;

/**
 * 
 * @ClassName: ExcelModelAction
 * @Description: 
 * @author yetl
 * @date 2015年5月5日
 *
 */
public class ExcelModelAction extends AbstractExcelImportAction{
	private IExcelTableService excelTableService;
	
	public void setExcelTableService(IExcelTableService excelTableService) {
		this.excelTableService = excelTableService;
	}

	public void perform(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logRequest(request);
		String method=request.getParameter("method").toLowerCase();
		int fileId=0;
		switch(method){
			case "importorder":
				fileId=excelTableService.getImportOrdersTemplateId("");
				break;
			case "importload":
				fileId=excelTableService.getImportLoadTemplateId("");
				break;
		}
		
		throw new JsonException("{\"mid\":"+fileId+"}");
	}

}
