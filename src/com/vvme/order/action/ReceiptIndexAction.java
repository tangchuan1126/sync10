package com.vvme.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.app.lucene.ytl.ReceiptIndexMgr;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;

/**
 * 
 * @ClassName: ReceiptIndexAction
 * @Description: RN lucene索引建立
 * @author yetl
 * @date 2015年7月7日
 *
 */
public class ReceiptIndexAction  extends ActionFatherController{

	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String action=request.getParameter("action");
		String data=request.getParameter("data");
		JSONObject resp=new JSONObject();
		resp.put("code", 200);
		
		if("add".equals(action)){
			JSONObject json=JSONObject.fromObject(data);
			String receiptID=json.getString("receipt_id");
			String referenceNo=json.getString("reference_no");
			String poNo=json.getString("po_no");
			String containerNo=json.getString("container_no");
			String scac=json.getString("scac");
			
			try{
				ReceiptIndexMgr.getInstance().addIndex(Long.parseLong(receiptID), referenceNo, poNo, containerNo, scac);
			}catch(Exception e){
				resp.put("code", 500);
			}
			throw new JsonException(resp.toString());
		}else if("update".equals(action)){
			JSONObject json=JSONObject.fromObject(data);
			String receiptID=json.getString("receipt_id");
			String referenceNo=json.getString("reference_no");
			String poNo=json.getString("po_no");
			String containerNo=json.getString("container_no");
			String scac=json.getString("scac");
			
			try{
				ReceiptIndexMgr.getInstance().updateIndex(Long.parseLong(receiptID), referenceNo, poNo, containerNo, scac);
			}catch(Exception e){
				resp.put("code", 500);
			}
			throw new JsonException(resp.toString());
		}else if("delete".equals(action)){
			JSONObject json=JSONObject.fromObject(data);
			String receiptID=json.getString("receipt_id");
			
			try{
				ReceiptIndexMgr.getInstance().deleteIndex(Long.parseLong(receiptID));
			}catch(Exception e){
				resp.put("code", 500);
			}
			throw new JsonException(resp.toString());
		}else if("find".equals(action)){
			JSONObject json=JSONObject.fromObject(data);
			String q=json.getString("q")!=null?json.getString("q"):json.getString("term");
			int suggestNum = Integer.parseInt(json.getString("size"));
			int page=Integer.parseInt(json.getString("page"));
			PageCtrl pc = new PageCtrl();
			pc.setPageSize(suggestNum);
			pc.setPageNo(page);
			if(!q.equals("")&&!q.equals("\""))
			{
				q = q.replaceAll("\"",""); 
				q = q.replaceAll("\\*","");
				q +="*";
			}
			DBRow bills[] =  ReceiptIndexMgr.getInstance().mergeSearch("merge_field", q, pc);
			json=JSONObject.fromObject(pc);
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("data", bills);
			jsonObject.put("page", json.toString());
			
			throw new JsonException(jsonObject);
		}else if("batch".equalsIgnoreCase(action)){
			JSONArray ja=JSONArray.fromObject(data);
			String [] ids=new String[ja.size()];
			String [] fields=new String[ja.size()];
			for(int i=0;i<ja.size();i++){
				JSONObject json=ja.optJSONObject(i);
				
				String receiptID=json.getString("receipt_id");
				String referenceNo=json.getString("reference_no");
				String poNo=json.getString("po_no");
				String containerNo=json.getString("container_no");
				String scac=json.getString("scac");
				
				ids[i]=receiptID;
				fields[i]="R"+receiptID+" "+referenceNo+" "+poNo+" "+containerNo+" "+scac;
			}
			ReceiptIndexMgr.getInstance().updateIndex(ids, fields);
		}
	}
}
