package com.vvme.order.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import com.cwc.action.core.ActionFatherController;
import com.cwc.db.DBRow;
import com.cwc.exception.DoNothingException;
import com.cwc.exception.Forward2JspException;
import com.cwc.exception.ForwardException;
import com.cwc.exception.JsonException;
import com.cwc.exception.OperationNotPermitException;
import com.cwc.exception.PageNotFoundException;
import com.cwc.exception.RedirectBackUrlException;
import com.cwc.exception.RedirectException;
import com.cwc.exception.RedirectRefException;
import com.cwc.exception.WriteOutResponseException;
import com.vvme.order.service.ICommonService;

/**
 * 
 * @ClassName: GetSCACAction
 * @Description: 
 * @author yetl
 * @date 2015年7月23日
 *
 */
public class GetSCACAction extends ActionFatherController{
	
	private ICommonService commonService;
	
	public void setCommonService(ICommonService commonService) {
		this.commonService = commonService;
	}
	
	@Override
	public void perform(HttpServletRequest request, HttpServletResponse response)
			throws WriteOutResponseException, RedirectRefException,
			ForwardException, Forward2JspException, RedirectBackUrlException,
			RedirectException, OperationNotPermitException,
			PageNotFoundException, DoNothingException, Exception {
		String scac=request.getParameter("scac");
		JSONObject json=new JSONObject();
		json.put("code", 200);
		try{
			DBRow [] rows=commonService.findAllScac(scac);
			json.put("msg", "success");
			json.put("data", rows);
		}catch(Exception e){
			json.put("code", 500);
			json.put("msg", e.getMessage());
		}
		throw new JsonException(json.toString());
	}

}
