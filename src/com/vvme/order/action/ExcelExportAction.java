package com.vvme.order.action;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.service.order.action.B2BOrderJsonActionUtil;
import com.vvme.order.excel.IExcelProcess;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.service.IExportOrdersService;
import com.vvme.order.util.FileServerUtils;

/**
 * 
 * @ClassName: ExcelExportAction
 * @Description: 
 * @author yetl
 * @date 2015年4月30日
 *
 */
public class ExcelExportAction extends AbstractExcelImportAction{
	private IExcelTableService excelTableService;
	private IExcelProcess excelProcess;
	private IExportOrdersService exportOrderService;
	
	public void setExportOrderService(IExportOrdersService exportOrderService) {
		this.exportOrderService = exportOrderService;
	}

	public void setExcelProcess(IExcelProcess excelProcess) {
		this.excelProcess = excelProcess;
	}

	public void setExcelTableService(IExcelTableService excelTableService) {
		this.excelTableService = excelTableService;
	}

	@SuppressWarnings("unchecked")
	public void perform(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logRequest(request);
		String jsid=request.getSession().getId();
		long time=System.currentTimeMillis();
		String dir=request.getServletContext().getRealPath("/upl_excel_tmp");
		File file=new File(dir);
		if(!file.exists())
			file.mkdir();
		String path=dir+File.separator+time+".xlsx";
		Map<?,?> expMap=excelTableService.getExportOrdersTemplate("");
		int eptFid=(Integer)expMap.get("export_file_id");
		String downPath=downLoadUrl + "/"+eptFid;
		FileServerUtils.getFile(downPath,path,jsid);
		DBRow param=new DBRow();
		param = DBRowUtils.mapConvertToDBRow(B2BOrderJsonActionUtil.getParameterMap(request));// request
		DBRow[] datas = exportOrderService.expOrder(param);
		excelProcess.appendNewSheet(path, datas,eptFid); // 添加sheet表现需要添加的数据
		downLoad(path,response,true);
	}
	
	public void downLoad(String filePath, HttpServletResponse response, boolean isOnLine) throws Exception {
        File f = new File(filePath);
        if (!f.exists()) {
            response.sendError(404, "File not found!");
            return;
        }
        BufferedInputStream br = new BufferedInputStream(new FileInputStream(f));
        byte[] buf = new byte[1024];
        int len = 0;

        response.reset(); // 非常重要
        if (isOnLine) { // 在线打开方式
            URL u = new URL("file:///" + filePath);
            response.setContentType(u.openConnection().getContentType());
            response.setHeader("Content-Disposition", "inline; filename=" + f.getName());
            // 文件名应该编码成UTF-8
        } else { // 纯下载方式
            response.setContentType("application/x-msdownload");
            response.setHeader("Content-Disposition", "attachment; filename=" + f.getName());
        }
        OutputStream out = response.getOutputStream();
        while ((len = br.read(buf)) > 0)
            out.write(buf, 0, len);
        br.close();
        out.close();
    }

}
