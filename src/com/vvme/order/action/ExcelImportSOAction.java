package com.vvme.order.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.exception.JsonException;
import com.vvme.order.ExcelConstants;
import com.vvme.order.ProcessContext;
import com.vvme.order.ResponseObject;
import com.vvme.order.SignManager;
import com.vvme.order.excel.IExcelProcess;
import com.vvme.order.exception.ExcelDataBaseException;
import com.vvme.order.exception.ImportFileInvalidateException;
import com.vvme.order.exception.ImportTemplateInvalidateException;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.service.IFileService;
import com.vvme.order.service.IImportSOService;
import com.vvme.order.util.FileServerUtils;
import com.vvme.order.validate.ValidateHandler;

/**
 * 
 * @ClassName: ExcelImportSOAction
 * @Description: 
 * @author yetl
 * @date 2015年4月30日
 *
 */
public class ExcelImportSOAction extends AbstractExcelImportAction{
	private IExcelTableService excelTableService;
	private IFileService fileService;
	private ValidateHandler validateHandler;
	private IExcelProcess excelProcess;
	private IImportSOService importSOService;
	
	public void setExcelTableService(IExcelTableService excelTableService) {
		this.excelTableService = excelTableService;
	}

	public void setFileService(IFileService fileService) {
		this.fileService = fileService;
	}

	public void setValidateHandler(ValidateHandler validateHandler) {
		this.validateHandler = validateHandler;
	}

	public void setExcelProcess(IExcelProcess excelProcess) {
		this.excelProcess = excelProcess;
	}

	public void setImportSOService(IImportSOService importSOService) {
		this.importSOService = importSOService;
	}

	public void perform(HttpServletRequest request, HttpServletResponse response) throws Exception{
		preExecute(request,response);
		final String jsid=request.getSession().getId();
		ProcessContext ctx=getThreadVariable();
		ResponseObject resp=ctx.getResp();
		String fullPath=ctx.getPath();
		String fileName = request.getParameter("fileName");
		
		Map<?,?> expMap=excelTableService.getExportOrdersTemplate(ctx.getCid());
		int desc=(Integer)expMap.get("error_column");
		int startRow=(Integer)expMap.get("start_row");
		int eptFileId=(Integer)expMap.get("order_export_file_id");
		int impFid=(Integer)expMap.get("order_import_file_id");
		ctx.setImpId(impFid);
		ctx.setExpId(eptFileId);
		
		//导入临时表
		try{
			importSOService.importOrdersToTemp(ctx);
		}catch(ExcelDataBaseException e){ //存在基础数据错误
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			String downPath=downLoadUrl + "/"+eptFileId;
			FileServerUtils.getFile(downPath,fullPath,jsid);
			excelProcess.writeDataArrayToExcelFile(fullPath,ctx, desc, startRow,ctx.getCid());
			String fileId = FileServerUtils.uploadFile(uploadUrl, fullPath, jsid);
			fileService.update(Long.parseLong(fileId), fileName,"600");
			resp.setFileId(fileId);
			resp.setMsg(e.getMessage());
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}catch(ImportTemplateInvalidateException e){
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg("Import file format is wrong,Please download the template");
			resp.setFileId(excelTableService.getImportOrdersTemplateId("")+""); 
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}catch(ImportFileInvalidateException e){
			e.printStackTrace();
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg(e.getMessage());
			resp.setFileId(excelTableService.getImportOrdersTemplateId("")+""); 
			resp.setSuccessCount(0);
			resp.setFailCount(resp.getTotal());
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}catch(Exception e){
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg("Into the temporary table failure");
			resp.setFileId(""); //不让下载
			resp.setFailCount(resp.getTotal());
			String body=resp.toString();
			log.info("响应:"+body);
			throw new JsonException(body);
		}finally{
			FileServerUtils.delTmpFile(fullPath);
		}
		
		String downPath=downLoadUrl + "/"+eptFileId;
		if(fullPath.endsWith(".xls"))
			fullPath+='x';
		FileServerUtils.getFile(downPath,fullPath,jsid);
		
		//开始基于业务逻辑的数据校验
		validateHandler.handleRequest(ctx);
		//导入订单
		
		//导入订单
		try{
			importSOService.updateSO(ctx);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
			resp.setCode(ExcelConstants.HTTP_CODE_ERROR);
			resp.setMsg(e.getMessage());
		}
		
		importSOService.exportImportSOResult(ctx);
		SignManager.markExcelError(ctx);
		ctx.stateResult();
		excelProcess.writeDataArrayToExcelFile(fullPath,ctx, desc, startRow,ctx.getCid());
		ctx.destroy();
		//上传写入文件
		String fileId = FileServerUtils.uploadFile(uploadUrl, fullPath, jsid);
		if(fileName.endsWith(".xls"))
			fileName+='x';
		fileService.update(Long.parseLong(fileId), fileName,"600");
		resp.setFileId(fileId);
		FileServerUtils.delTmpFile(fullPath);
		String body=resp.toString();
		log.info("响应:"+body);
		throw new JsonException(body);
	}

}
