package com.vvme.order.excel;

import java.awt.geom.IllegalPathStateException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cwc.db.DBRow;
import com.fr.third.org.apache.poi.hssf.usermodel.HSSFDateUtil;
import com.vvme.order.ExcelConstants;
import com.vvme.order.ExcelDataArray;
import com.vvme.order.ExcelRowResult;
import com.vvme.order.FieldInfo;
import com.vvme.order.ProcessContext;
import com.vvme.order.SignManager;
import com.vvme.order.service.IExcelTableService;
import com.vvme.order.util.ExcelUtils;

/**
 * 
 * @ClassName: POIExcelProcess
 * @Description: 此类采用POI3.9方式解析excel
 * 解析的模板格式为普通格式 第0行为标题,下面为数据
 * 此类跟业务没有关系,不要把业务代码加入此类
 * @author yetl
 * @date 2015年4月23日
 *
 */
public class POIExcelProcess implements IExcelProcess{
	private static Logger logger = Logger.getLogger("platform");
	public static SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd");// 格式化日期字符串
	public static SimpleDateFormat mdy = new SimpleDateFormat(
			"MM/dd/yyyy");// 格式化日期字符串
	public static SimpleDateFormat ymdhm = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm");// 格式化日期字符串
	public static SimpleDateFormat hm = new SimpleDateFormat(
			"HH:mm");// 格式化日期字符串
	public static SimpleDateFormat hmA = new SimpleDateFormat(
			"h:mm a");
	public static DecimalFormat nf = new DecimalFormat("0.00");// 格式化数字
	private static final short TITLE_FONT_SIZE=15; 
	private static final short CONTENT_FONT_SIZE=10;
	
	public static final Set<Short> TIME_VALUE = new HashSet<Short>();

	static{
		TIME_VALUE.add(Short.valueOf("186"));
		TIME_VALUE.add(Short.valueOf("187"));
		TIME_VALUE.add(Short.valueOf("188"));
		TIME_VALUE.add(Short.valueOf("189"));
		TIME_VALUE.add(Short.valueOf("190"));
	}
	
	//对应excel的下标 从0开始
	private int startRow=1;
	private static final int version2003=2003;
	private static final int version2007=2007;
	private static int version=version2007;
	
	private IExcelTableService excelTableService;

	public void setExcelTableService(IExcelTableService excelTableService) {
		this.excelTableService = excelTableService;
	}

	@Override
	public ExcelDataArray importExcelFileToDataArray(String path) throws Exception {
		InputStream in=null;
		try{
			if(path.endsWith(".xls")){
				version=version2003;
			}else if(path.endsWith(".xlsx")){
				version=version2007;
			}
			in = new FileInputStream(path);
			return importExcelFileToDataArray(in);
		}catch(Exception e){
			throw e;
		}finally{
			if(null!=in)
				in.close();
		}
	}
	
	@Override
	public ExcelDataArray importExcelFileToDataArray(InputStream is) throws Exception {
		return importExcelFileToDataArray(is,0);
	}
	
	@Override
	public ExcelDataArray importExcelFileToDataArray(InputStream is,int sheetInd) throws Exception {
		Workbook workBook=null;
		Sheet sheet=null;
		if(version==version2007){
			workBook = (Workbook)new XSSFWorkbook(is);
		}else if(version==version2003){
			workBook=(Workbook)new HSSFWorkbook(is);
		}else{
			throw new IllegalPathStateException("unknown excel file");
		}
		sheet=workBook.getSheetAt(sheetInd);
		return importExcelFileToDataArray(sheet);
	}
	
	/**
	 * 解析指定sheet数据到ExcelDataArray
	 * 多加1列做记录行 1列记录对应excel里的行号
	 * @param sheet
	 * @return ExcelDataArray
	 * @throws Exception
	 */
	public ExcelDataArray importExcelFileToDataArray(Sheet sheet) throws Exception{
		// 获取Sheet表中所包含的最后一行行号
		int sheetRows = sheet.getLastRowNum();
		int rowCnt = sheetRows - startRow + 1;
		
		// 获取Sheet表中所包含的最后一列的列号
		int sheetLines = sheet.getRow(0).getLastCellNum();

		logger.debug("读取Excel表格" + "总行数： " + sheetRows + "\n" + "总列数： "
				+ sheetLines + "\n" + "开始行号： " + startRow + "\n");
		
		// 初始化结果数据数组
		ExcelDataArray dataArray=new ExcelDataArray();
		dataArray.init(rowCnt,sheetLines);
		
		// 读取第一行标题
		Row titleRow = sheet.getRow(0);
		
		for (int lineIndex = 0; lineIndex < sheetLines; lineIndex++) {
			Cell cell = titleRow.getCell(lineIndex);

			if (null == cell) {
				continue;
			}

			cell.setCellType(Cell.CELL_TYPE_STRING);// 默认都先设置为String
			// 设置值
			dataArray.setTitle(lineIndex, cell.getStringCellValue());
			dataArray.setWidth(lineIndex, sheet.getColumnWidth(lineIndex));
		}

		String value = null;
		// 得到第一行标题
		for (int rowIndex = 0; rowIndex < rowCnt; rowIndex++) {
			Row row = sheet.getRow(rowIndex + startRow);

			if (null == row) {
				continue;
			}
			
			for (int lineIndex = 0; lineIndex < sheetLines; lineIndex++) {
				Cell cell = row.getCell(lineIndex);
				value=getStringCellValue(cell);
				dataArray.setColumn(rowIndex, lineIndex, value);
			}

			dataArray.setDataRow(rowIndex,rowIndex + startRow+1); //最后1列记录实际行号
		}
		return dataArray;
	}
	
	/**
	 * 取出cell数据
	 * @param cell
	 * @return
	 */
	protected String getStringCellValue(Cell cell){
		String value="";
		if (null == cell) {
			value = "";
		} else {
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				value = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					if(cell.getCellStyle().getDataFormat()==HSSFDataFormat  
	                        .getBuiltinFormat("h:mm") || cell.getCellStyle().getDataFormat()==
	                        HSSFDataFormat.getBuiltinFormat("h:mm AM/PM")
	                        || TIME_VALUE.contains(cell.getCellStyle().getDataFormat())){
						value = hm.format(HSSFDateUtil.getJavaDate(cell
										.getNumericCellValue()));
					}else if(cell.getCellStyle().getDataFormat()==HSSFDataFormat  
	                        .getBuiltinFormat("m/d/yy h:mm") 
	                        ){
						value = ymdhm.format(HSSFDateUtil.getJavaDate(cell
								.getNumericCellValue()));
					}else if(cell.getCellStyle().getDataFormat()==14){
						value = sdf.format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue()));
					}else{
						value = sdf.format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue()));
					}
				} else {
					cell.setCellType(Cell.CELL_TYPE_STRING);
					value = cell.getStringCellValue();

					if (value.indexOf(".") > -1) {
						value = nf.format(new Double(value)).trim();
					} else {
						value = value.trim();
					}
				}

				break;
			case Cell.CELL_TYPE_BOOLEAN:
				value = String.valueOf(cell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_BLANK:
				value = "";
				break;
			case Cell.CELL_TYPE_ERROR:
				value = "";
				break;
			case Cell.CELL_TYPE_FORMULA:
				cell.setCellType(Cell.CELL_TYPE_STRING);
				value = cell.getStringCellValue();
				if (null != value) {
					value = value.replaceAll("#N/A", "");
				}
				break;
			default: // XSSFCell.CELL_TYPE_FORMULA 公式
				cell.setCellType(Cell.CELL_TYPE_STRING);
				value = cell.getStringCellValue();
				if ("#N/A".equals(value)) {
					value = "";
				}
			}
		}
		
		return value;
	}

	@Override
	public void writeDataArrayToExcelFile(String path,ProcessContext ctx) throws Exception {
		FileInputStream in = new FileInputStream(new File(path));
		XSSFWorkbook wb = new XSSFWorkbook(in); 
		Map<?,?> expMap=excelTableService.getExportOrdersTemplate(ctx.getCid());
		Map<Integer,FieldInfo> fields=excelTableService.getImportTemplateInfoByMid(ctx.getImpId());
		int desc=(Integer)expMap.get("error_column");
		int start=(Integer)expMap.get("start_row");
		ExcelDataArray dataArray=ctx.getExcelData();
		
		XSSFSheet sheet = wb.getSheetAt(0);  
		String [][] data=dataArray.getDataArray();
		
		XSSFFont rowFont = getFont( wb,(short)CONTENT_FONT_SIZE, XSSFFont.COLOR_NORMAL,
				"Arial Unicode MS");
        
		for(int i=0;i<data.length;i++){
			XSSFRow row = sheet.createRow(i+start);
			if(!ExcelUtils.isNullLine(data[i])){
				for(int j=0;j<data[i].length;j++){
					if(j==desc){ //描述文字
						createCell(wb, row, j, "", rowFont);
						createCell(wb, row, j+1, ExcelUtils.parseVallue(data[i][j],j,fields), rowFont);
						continue;
					}else if(j<desc){
						createCell(wb, row, j, ExcelUtils.parseVallue(data[i][j],j,fields), rowFont);
					}else{
						createCell(wb, row, j+1, ExcelUtils.parseVallue(data[i][j],j,fields), rowFont);
					}
				}
			}else{ //建空行为了自动筛选
				for(int j=0;j<data[i].length;j++){
					createCell(wb, row, j+1, ExcelUtils.parseVallue(" ",j,fields), rowFont);
				}
			}
		}
		
		in.close();
		FileOutputStream out = null;

		try {
			out = new FileOutputStream(path);
			wb.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}  
	}
	
	
	/**
	 * 构造font
	 * @param workBook
	 * @param fontSize 字体大小
	 * @param color 颜色
	 * @param fontName 字体名字
	 * @return XSSFFont
	 */
	private XSSFFont getFont(XSSFWorkbook workBook, short fontSize,
			short color, String fontName) {
		XSSFFont font = workBook.createFont();
		
		font.setFontHeightInPoints(fontSize); // 字体高度
		font.setColor(color); // 字体颜色
		font.setFontName(fontName); // 字体

		return font;
	}
	
	/**
	 * 创建cell
	 * @param wb
	 * @param row
	 * @param column
	 * @param value
	 * @param font
	 */
    private Cell createCell(Workbook wb, Row row, int column,  
            String value, Font font) {  
        Cell cell = row.createCell(column);  
        cell.setCellValue(value);  
        CellStyle cellStyle = wb.createCellStyle();  
        cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);  
        cellStyle.setVerticalAlignment(XSSFCellStyle.ALIGN_CENTER);
        cellStyle.setFont(font);  
        cell.setCellStyle(cellStyle);  
        return cell;
    }
    
    //待修改
	public void copyRowsToSheet(String fullPath, Set<Integer> lines,ProcessContext ctx,int desc,int startRow) throws Exception {
		if(fullPath==null || fullPath.trim().equals("")){
			throw new IOException("File path is blank");
		}
		
		Set<ExcelRowResult> errs=ctx.getResults();
		FileInputStream is=null;
		try {
			is=new FileInputStream(fullPath);
			XSSFWorkbook workBook = new XSSFWorkbook(is);
			XSSFSheet sheet = workBook.getSheetAt(0);
			XSSFSheet sheet1=workBook.getSheetAt(1);
			
			int cellNum=sheet1.getRow(0).getLastCellNum();
			
			XSSFFont rowFont = getFont( workBook,(short) 10, XSSFFont.COLOR_NORMAL,
					"Arial Unicode MS");
			CellStyle style = workBook.createCellStyle();
			style.setFont(rowFont); 
			style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			
			Map<Integer,ExcelRowResult> errMap=new HashMap<Integer,ExcelRowResult>();
			//===
			for(ExcelRowResult rst:errs){
				if(rst.getSign()==ExcelConstants.ERROR_TYPE_34){
					errMap.put(rst.getRow(), rst);
				}
			}
			
			//导入成功数据移走
		
			int i=1;
			if(lines!=null){
				for(int lineNo:lines){
					XSSFRow sRow=sheet.getRow(lineNo-startRow+2);
					XSSFRow destRow=sheet1.createRow(i++);
					
					for(int j=0;j<cellNum;j++){
						XSSFCell cell=destRow.createCell(j);
						cell.setCellStyle(style);
						
						if(j>=desc){
							Cell scell=sRow.getCell(j+1);
							if(scell!=null)
								cell.setCellValue(scell.getStringCellValue());
							else
								cell.setCellValue("");
								
							if(errMap.containsKey(lineNo)){
								if(errMap.get(lineNo).containCellLine(j)){
									XSSFDrawing patr = sheet1.createDrawingPatriarch();
									XSSFComment comment = patr.createCellComment(new XSSFClientAnchor(0, 0, 0, 0, (short)3, 3, (short) 5, 6));
									comment.setString(new XSSFRichTextString(errMap.get(lineNo).getCellResult(j).getMsg()));
									cell.setCellComment(comment);
								}
							}
						}else{
							Cell scell=sRow.getCell(j);
							if(scell!=null)
								cell.setCellValue(scell.getStringCellValue());
							else
								cell.setCellValue("");
						}
						
					}
					
					sheet.removeRow(sRow); //删除一行
	 			}
			}
			
			for (int k = 0; k <= sheet.getLastRowNum(); k++) {
				XSSFRow hRow = sheet.getRow(k);
				if (hRow==null) // 找到空行索引
				{
					int m = 0;
					for (m = k + 1; m <= sheet.getLastRowNum(); m++) {
						XSSFRow nhRow = sheet.getRow(m);
						if (nhRow!=null) {
							sheet.shiftRows(m, sheet.getLastRowNum(), k - m);
							reCalRow(errs,k - startRow + 1,k - m);
							break;
						}
					}
					if (m > sheet.getLastRowNum())
						break; // 此工作簿完成
				}
			}
			
//			markExcelError(fullPath,errs,0,desc);
			
			int sheetLines=sheet1.getRow(0).getLastCellNum();
			for (ExcelRowResult rst : errs) {
				if(rst==null)
					continue;
				
				if(rst.getSign()==ExcelConstants.ERROR_TYPE_34)
					continue;
				
				XSSFRow row = sheet.getRow(rst.getRow()); //行是实际行，但是下标是0开始的 
				
				int l=0;
				for(i=0;i<sheetLines;i++){
					l=i;
					Cell cell = row.getCell(i);
					if(cell==null){
						cell = row.createCell((short) i);
						continue;
					}
					
					if(i==desc){
						continue;
					}
						
					if(i>=desc)
						l=i-1;
					
					if(rst.containCellLine(l)){
						XSSFDrawing patr = sheet.createDrawingPatriarch();
						XSSFComment comment = patr.createCellComment(new XSSFClientAnchor(0, 0, 0, 0, (short)3, 3, (short) 5, 6));
						comment.setString(new XSSFRichTextString(rst.getCellResult(l).getMsg()));
						
						cell.setCellStyle(createCellStyle(workBook,HSSFColor.YELLOW.index));
						cell.setCellComment(comment);
					}
				}
			}
			
			FileOutputStream out = null;

			try {
				out = new FileOutputStream(fullPath);
				workBook.write(out);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(null!=is){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
    
	@SuppressWarnings("unused")
	private boolean isBlankRow(XSSFRow row) {
		if (row == null)
			return true;
		for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
			XSSFCell hcell = row.getCell(i);
			if (!isBlankCell(hcell))
				return false;
		}
		return true;
	}
	
	private boolean isBlankCell(XSSFCell hcell) {
		if (hcell == null)
			return true;
		String content = getStringCellValue(hcell);
		if (content == null || "".equals(content)) // 找到非空行
		{
			return true;
		}
		return false;
	}
	
	private CellStyle createCellStyle(XSSFWorkbook workBook,short color){
		XSSFFont rowFont = getFont( workBook,(short) 10, XSSFFont.COLOR_NORMAL,
				"Arial Unicode MS");
		
		CellStyle style = workBook.createCellStyle();
		style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
//		style.setFillPattern(XSSFCellStyle.FINE_DOTS );
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
		style.setFillForegroundColor(color); //设置颜色
		style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style.setFont(rowFont);  
        
		return style;
	}
	
	private void reCalRow(Set<ExcelRowResult> errs,int line,int offset){
		for(ExcelRowResult err:errs){
			int row=err.getRow();
			if(SignManager.canImportSign(err.getSign()))
				continue;
			if(row>line){
				err.setRow(row+offset);
			}
		}
	}
	
	@Override
	public void writeDataArrayToExcelFile(String path, ProcessContext ctx,
			int line, int startRow,String cid) throws Exception {
		FileInputStream in = new FileInputStream(new File(path));
		XSSFWorkbook wb = new XSSFWorkbook(in); 
		Map<Integer,FieldInfo> fields=excelTableService.getImportTemplateInfoByMid(ctx.getImpId());
		XSSFSheet sheet = wb.getSheetAt(0);  
		
		XSSFFont rowFont = getFont( wb,(short)CONTENT_FONT_SIZE, XSSFFont.COLOR_NORMAL,
				"Arial Unicode MS");
		
		//错误文件的描述
		Set<ExcelRowResult> datas=ctx.getErrs();
		
		int i=0;
		for(ExcelRowResult rst:datas){
			XSSFRow row = sheet.createRow(i+startRow);
			String [] data=rst.getData();
			if(data==null){
				int [] dataRows=ctx.getExcelData().getDataRows();
				int index=ExcelUtils.getIndex(dataRows, rst.getRow());
				data=ctx.getExcelData().getDataArray()[index];
			}
			if(!ExcelUtils.isNullLine(data)){
				for(int j=0;j<data.length;j++){
					Cell cell=null;
					if(j==line){ //描述文字
						createCell(wb, row, j, rst.getMsg(), rowFont);
						cell=createCell(wb, row, j+1, ExcelUtils.parseVallue(data[j],j,fields), rowFont);
					}else if(j<line){
						cell=createCell(wb, row, j, ExcelUtils.parseVallue(data[j],j,fields), rowFont);
					}else{
						cell=createCell(wb, row, j+1, ExcelUtils.parseVallue(data[j],j,fields), rowFont);
					}
					if(rst.containCellLine(j)){
						XSSFDrawing patr = sheet.createDrawingPatriarch();
						XSSFComment comment = patr.createCellComment(new XSSFClientAnchor(0, 0, 0, 0, (short)3, 3, (short) 5, 6));
						comment.setString(new XSSFRichTextString(rst.getCellResult(j).getMsg()));
						
						cell.setCellStyle(createCellStyle(wb,HSSFColor.YELLOW.index));
						cell.setCellComment(comment);
					}
				}
			}else{ //建空行为了自动筛选
				if(data[i]!=null){
					for(int j=0;j<data.length;j++){
						createCell(wb, row, j+1, ExcelUtils.parseVallue(" ",j,fields), rowFont);
					}
				}
			}
			i++;
		}
		
		//正确数据写入
		XSSFSheet sheet1 = wb.getSheetAt(1);
		datas=ctx.getSuccs();
		
		i=1;
		for(ExcelRowResult rst:datas){
			XSSFRow row = sheet1.createRow(i);
			String [] data=rst.getData();
			if(!ExcelUtils.isNullLine(data)){
				for(int j=0;j<data.length;j++){
					Cell cell=createCell(wb, row, j, ExcelUtils.parseVallue(data[j],j,fields), rowFont);
					if(rst.containCellLine(j)){
						XSSFDrawing patr = sheet1.createDrawingPatriarch();
						XSSFComment comment = patr.createCellComment(new XSSFClientAnchor(0, 0, 0, 0, (short)3, 3, (short) 5, 6));
						comment.setString(new XSSFRichTextString(rst.getCellResult(j).getMsg()));
						
						cell.setCellStyle(createCellStyle(wb,HSSFColor.YELLOW.index));
						cell.setCellComment(comment);
					}
				}
			}else{ //建空行为了自动筛选
				if(data[i]!=null){
					for(int j=0;j<data.length;j++){
						createCell(wb, row, j+1, ExcelUtils.parseVallue(" ",j,fields), rowFont);
					}
				}
			}
			i++;
		}
		
		in.close();
		FileOutputStream out = null;

		try {
			out = new FileOutputStream(path);
			wb.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}  
	}
	
	
	/**
	 * 将记录用excel形式表现出来
	 * @param path excel文件路径
	 * @param rows 数据集
	 * @param map excel字段信息
	 * @throws Exception 
	 */
	public void addNewSheet(String path,DBRow [] rows,int mid) throws Exception{
		Map<Integer,FieldInfo> map=excelTableService.getImportTemplateInfoByMid(mid);
		FileInputStream is=null;
	 	try {
	 		is=new FileInputStream(path);
	   		XSSFWorkbook workBook = new XSSFWorkbook(is);
	   		XSSFSheet sheet=workBook.createSheet("More"); //创建新sheet
	   		
   			// 设置字体  
	        XSSFFont font = getFont(workBook,(short)TITLE_FONT_SIZE,XSSFFont.COLOR_NORMAL,"Arial Unicode MS"); 
	        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD); //宽度  
	        font.setItalic(false); //是否使用斜体 
//		    font.setStrikeout(true); //是否使用划线
		           
		    XSSFCellStyle cellStyle = workBook.createCellStyle();  
		    cellStyle.setFont(font);
		    cellStyle.setLeftBorderColor(HSSFColor.BLACK.index);
		    cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		    cellStyle.setRightBorderColor(HSSFColor.BLACK.index);
		    cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		    cellStyle.setBottomBorderColor(HSSFColor.BLACK.index);
		    cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
	        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		    cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER); //水平布局：居左
		    cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM); //垂直方向
		    cellStyle.setFillForegroundColor(HSSFColor.WHITE.index); //设置颜色
		    cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
//		    cellStyle.setWrapText(true);  
		           
	   		int sheetLines=map.size();
	   		
	   		XSSFRow titleRow=sheet.createRow((short)0);
	   		titleRow.setHeight((short)1500); 
	   		//创建标题
	   		for(int i=0;i<sheetLines;i++){
	   			sheet.setColumnWidth((short)i,(short)7000);
	   			Cell cell=titleRow.createCell(i);
	   			cellStyle.setWrapText(true);
	   			cell.setCellStyle(cellStyle);//设置单元格样式  
	   			cell.setCellValue(map.get(i).getXlsTitle());
		        cell.setCellType(XSSFCell.CELL_TYPE_STRING);//指定单元格格式：数值、公式或字符串  
	   		}
	   		
	   		XSSFFont fontContent=getFont(workBook,(short)CONTENT_FONT_SIZE,XSSFFont.COLOR_NORMAL,"Arial Unicode MS");
	   		//添加数据
	   		int i=1;
	   		String loadNo="";
	   		String loadNoTmp="";
	   		FieldInfo info=ExcelUtils.getExcelSn(map,"loadno");
	   		int sn=info.getExcelSn();
	   		for(DBRow data:rows){
	   			loadNoTmp=data.getString("a"+sn);
	   			
	   			if(loadNo.equals(loadNoTmp) || i==1){
		   			XSSFRow row=sheet.createRow((short)i);
		   			
			        for(int j=0;j<sheetLines;j++){
				           // 创建单元格  
				           XSSFCell cell = row.createCell((short)j);  
				           cell.setCellValue(data.getString("a"+j));
				           cell.setCellStyle(createCellStyle(workBook,fontContent,map.get(j).getType()));
			         }
			        
			        if(i!=1)
			        	i++;
	   			}
	   			
	   			if(!loadNo.equals(loadNoTmp) && i!=1){
	   				 loadNo=loadNoTmp;
	   				 XSSFRow row=sheet.createRow((short)i);
	   			
		   			 for(int j=0;j<sheetLines;j++){
				           // 创建单元格  
				           XSSFCell cell = row.createCell((short)j);  
				           cell.setCellType(XSSFCell.CELL_TYPE_STRING);//指定单元格格式：数值、公式或字符串  
				           cell.setCellValue("");
			         }
		   			 i++;
		   			 
			   		 row=sheet.createRow((short)i);
			   			
			         for(int j=0;j<sheetLines;j++){
				           // 创建单元格  
				           XSSFCell cell = row.createCell((short)j);  
				           
				           cell.setCellValue(data.getString("a"+j));
				           cell.setCellStyle(createCellStyle(workBook,fontContent,map.get(j).getType()));
			         }
				           
			   		i++;
	   			}
	   			
	   			if(i==1){
	   				i++;
	   				loadNo=loadNoTmp;
	   			}
	   		}
	           
           try {  
               FileOutputStream fileOut = new FileOutputStream(path);  
               workBook.write(fileOut);  
               fileOut.close();  
           } catch (Exception e) {  
              e.printStackTrace();
           }
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(null!=is){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	   	
   }
	
	/**
	 * 将记录用excel形式表现出来
	 * @param path excel文件路径
	 * @param rows 数据集
	 * @param map excel字段信息
	 * @throws Exception 
	 */
	public void appendNewSheet(String path,DBRow [] rows,int mid) throws Exception{
		Map<Integer,FieldInfo> map=excelTableService.getImportTemplateInfoByMid(mid);
		FileInputStream is=null;
	 	try {
	 		is=new FileInputStream(path);
	   		XSSFWorkbook workBook = new XSSFWorkbook(is);
	   		XSSFSheet sheet=null;
	   		
	   		sheet=workBook.getSheetAt(0);
	   		
   			// 设置字体  
	        XSSFFont font =getFont(workBook,(short)TITLE_FONT_SIZE,XSSFFont.COLOR_NORMAL,"Arial Unicode MS"); 
	        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD); //宽度  
	        font.setItalic(false); //是否使用斜体 
	   		int sheetLines=sheet.getRow(0).getLastCellNum();
	   		
	   		XSSFFont fontContent=getFont(workBook,(short)CONTENT_FONT_SIZE,XSSFFont.COLOR_NORMAL,"Arial Unicode MS");
	   		//添加数据
	   		int i=1;
	   		String loadNo="";
	   		String loadNoTmp="";
	   		FieldInfo info=ExcelUtils.getExcelSn(map,"loadno");
	   		int sn=info.getExcelSn();
	   		for(DBRow data:rows){
	   			loadNoTmp=data.getString("a"+sn);
	   			
	   			if(loadNo.equals(loadNoTmp) || i==1){
		   			XSSFRow row=sheet.createRow((short)i);
		   			
			        for(int j=0;j<sheetLines;j++){
				           // 创建单元格  
				           XSSFCell cell = row.createCell((short)j);  
				           cell.setCellValue(data.getString("a"+j));
				           cell.setCellStyle(createCellStyle(workBook,fontContent,map.get(j).getType()));
			        }
			        
			        if(i!=1)
			        	i++;
	   			}
	   			
	   			if(!loadNo.equals(loadNoTmp) && i!=1){
	   				 loadNo=loadNoTmp;
	   				 XSSFRow row=sheet.createRow((short)i);
	   			
		   			 for(int j=0;j<sheetLines;j++){
				           // 创建单元格  
				           XSSFCell cell = row.createCell((short)j);  
				           cell.setCellType(XSSFCell.CELL_TYPE_STRING);//指定单元格格式：数值、公式或字符串  
				           cell.setCellValue("");
			         }
		   			 i++;
		   			 
			   		 row=sheet.createRow((short)i);
			   			
			         for(int j=0;j<sheetLines;j++){
				           // 创建单元格  
				           XSSFCell cell = row.createCell((short)j);  
				           
				           cell.setCellValue(data.getString("a"+j));
				           cell.setCellStyle(createCellStyle(workBook,fontContent,map.get(j).getType()));
			         }
				           
			   		i++;
	   			}
	   			
	   			if(i==1){
	   				i++;
	   				loadNo=loadNoTmp;
	   			}
	   		}
	           
           try {  
               FileOutputStream fileOut = new FileOutputStream(path);  
               workBook.write(fileOut);  
               fileOut.close();  
           } catch (Exception e) {  
              e.printStackTrace();
           }  
	           
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}finally{
			if(null!=is){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	   	
   }
	
	/**
	 * 创建cell
	 * @param workBook
	 * @param fontContent
	 * @param type
	 * @return
	 */
	protected XSSFCellStyle createCellStyle(XSSFWorkbook workBook,XSSFFont fontContent,String type){
   		XSSFCellStyle contentStyle = workBook.createCellStyle();
   		contentStyle.setFont(fontContent);
   		contentStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
   		contentStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
   		
   		XSSFDataFormat format=workBook.createDataFormat();
		switch(type){
			case "date":
				contentStyle.setDataFormat(format.getFormat("yyyy-MM-dd"));
				break;
			case "int":
				contentStyle.setDataFormat(format.getFormat("#,##0"));
				break;
			case "time":
				contentStyle.setDataFormat(format.getFormat("h:mm"));
				break;
			case "decimal":
				contentStyle.setDataFormat(format.getFormat("#,##0.00"));
				break;
			case "varchar":
				contentStyle.setDataFormat(format.getFormat("@"));
				break;
			default:
				contentStyle.setDataFormat(format.getFormat("@"));
				break;
		}
		
		return contentStyle;
	}
	
}
