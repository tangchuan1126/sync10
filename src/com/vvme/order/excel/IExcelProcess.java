package com.vvme.order.excel;

import java.io.InputStream;
import java.util.Set;

import com.cwc.db.DBRow;
import com.vvme.order.ExcelDataArray;
import com.vvme.order.ProcessContext;

/**
 * 
 * @ClassName: IExcelProcess
 * @Description: 处理excel的方式 POI NPOI XLS等
 * 跟业务无关的的
 * @author yetl
 * @date 2015年4月23日
 *
 */
public interface IExcelProcess {
	/**
	 * 解析excel数据到内存 默认解析第一个sheet
	 * @param path excel文件路径
	 * @return excel数据体
	 * @throws Exception
	 */
	public ExcelDataArray importExcelFileToDataArray(String path) throws Exception;
	
	/**
	 * 解析excel数据到内存  默认解析第一个sheet
	 * @param is excel文件流
	 * @return excel数据体
	 * @throws Exception
	 */
	public ExcelDataArray importExcelFileToDataArray(InputStream is) throws Exception;
	
	/**
	 * 解析excel文件对应的sheetInd
	 * @param is excel文件流
	 * @param sheetInd 解析第几个sheet 从0开始
	 * @return excel数据体
	 * @throws Exception
	 */
	public ExcelDataArray importExcelFileToDataArray(InputStream is,int sheetInd) throws Exception;
	
	/**
	 * 内存数据写到excel模板文件
	 * @param path
	 * @param excelData
	 * @throws Exception
	 */
	public void writeDataArrayToExcelFile(String path,ProcessContext ctx) throws Exception;
	
	/**
	 * 
	 * @param path
	 * @param datas
	 * @param line
	 * @param startRow
	 * @param cid
	 * @throws Exception
	 */
	public void writeDataArrayToExcelFile(String path,ProcessContext ctx,int line,int startRow,String cid) throws Exception;
	
	/**
	 * 将指定行从第一个sheet移动到第二个sheet
	 * @param path
	 * @param lines
	 * @param desc
	 * @param startRow
	 * @throws Exception
	 */
	public void copyRowsToSheet(String path,Set<Integer> lines,ProcessContext ctx,int desc,int startRow) throws Exception;
	
	/**
	 * 
	 * @param path
	 * @param rows
	 * @throws Exception
	 */
	public void addNewSheet(String path,DBRow [] rows,int mid) throws Exception;
	
	/**
	 * 
	 * @param path
	 * @param rows
	 * @throws Exception
	 */
	public void appendNewSheet(String path,DBRow [] rows,int mid) throws Exception;
}
