package com.vvme.order;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContextAware;

import com.vvme.order.util.ContextUtils;

/**
 * 
 * @ClassName: ApplicationContextRegister
 * @Description: 协助注入spring context
 * @author yetl
 * @date 2015年4月25日
 *
 */
public class ApplicationContextRegister implements ApplicationContextAware{
	protected static Logger log = Logger.getLogger("logfile");
	
	public void setApplicationContext(
			org.springframework.context.ApplicationContext appContext)
			throws BeansException {
		ContextUtils.setApplicationContext(appContext);
		log.debug("ApplicationContext register");
	}

}
