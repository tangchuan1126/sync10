package org.tuckey.web.filters.urlrewrite;

import org.tuckey.web.filters.urlrewrite.utils.Log;
import org.tuckey.web.filters.urlrewrite.utils.NumberUtils;
import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

import com.cwc.app.key.LocalKey;
import com.cwc.app.util.Environment;
import com.cwc.cache._l1l1l1l1l1l1l1l1l;
import com.cwc.db.DBObjectType;
import com.cwc.util.StringUtil;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

public final class UrlRewriteFilter implements Filter 
{

    private static Log log = Log.getLog(UrlRewriteFilter.class);


    public static final String VERSION = "2.6.0 build 5728";

    public static final String DEFAULT_WEB_CONF_PATH = "/WEB-INF/conf/urlrewrite.xml";
    private UrlRewriter urlRewriter = null;

    private boolean confReloadCheckEnabled = true;
    private int confReloadCheckInterval = 5;
    private long confLastLoad = 0;
    private Conf confLastLoaded = null;
    private long confReloadLastCheck = 30;

    private boolean confReloadInProgress = false;

    private boolean statusEnabled = true;
    private String statusPath = "/rewrite-status";

    private ServerNameMatcher statusServerNameMatcher;
    private static final String DEFAULT_STATUS_ENABLED_ON_HOSTS = "localhost, local, 127.0.0.1";

    private ServletContext context = null;
    private FilterConfig filterConfigInternal;

    public void init(final FilterConfig filterConfig) {
    	//数据库标志
    	DBObjectType.ll1l11ll1l1l1ll11ll1111ll11l1l11l1l11111l1l111l1l1l1 = 1;

    	this.filterConfigInternal = filterConfig;
    	
        log.debug("filter init called");
        if (filterConfig == null) {
            log.error("unable to init filter as filter config is null");
            return;
        }

        log.debug("init: calling destroy just in case we are being re-inited uncleanly");
        destroyActual();

        context = filterConfig.getServletContext();
        if (context == null) {
            log.error("unable to init as servlet context is null");
            return;
        }

        // set the conf of the logger to make sure we get the messages in context log
        Log.setConfiguration(filterConfig);

        log.debug("loaded into servlet context: " + context.getServletContextName());

        // get init paramerers from context web.xml file
        String confReloadCheckIntervalStr = filterConfig.getInitParameter("confReloadCheckInterval");
        String statusPathConf = filterConfig.getInitParameter("statusPath");
        String statusEnabledConf = filterConfig.getInitParameter("statusEnabled");
        String statusEnabledOnHosts = filterConfig.getInitParameter("statusEnabledOnHosts");

        // confReloadCheckInterval (default to null)
        if (confReloadCheckIntervalStr != null && !"".equals(confReloadCheckIntervalStr)) {
            // convert to millis
            confReloadCheckInterval = 1000 * NumberUtils.stringToInt(confReloadCheckIntervalStr);
            confReloadCheckEnabled = true;
            if (confReloadCheckInterval == 0) {
                log.info("reload check performed each request");
            } else {
                log.info("reload check set to " + confReloadCheckInterval / 1000 + "s");
            }
        } else {
            confReloadCheckEnabled = false;
        }

        // status enabled (default true)
        if (statusEnabledConf != null && !"".equals(statusEnabledConf)) {
            log.debug("statusEnabledConf set to " + statusEnabledConf);
            statusEnabled = "true".equals(statusEnabledConf.toLowerCase());
        }
        if (statusEnabled) {
            // status path (default /rewrite-status)
            if (statusPathConf != null && !"".equals(statusPathConf)) {
                statusPath = statusPathConf.trim();
                log.info("status display enabled, path set to " + statusPath);
            }
        } else {
            log.info("status display disabled");
        }

        if (StringUtils.isBlank(statusEnabledOnHosts)) statusEnabledOnHosts = DEFAULT_STATUS_ENABLED_ON_HOSTS;
        else
            log.debug("statusEnabledOnHosts set to " + statusEnabledOnHosts);
        statusServerNameMatcher = new ServerNameMatcher(statusEnabledOnHosts);

        loadConf();
    }

    private void loadConf()
    {
    	//InputStream inputStream = context.getResourceAsStream(DEFAULT_WEB_CONF_PATH);
    	String confA[] = this.filterConfigInternal.getInitParameter("conf").split(",");
    	ArrayList<InputStream> inputStreamList = new ArrayList<InputStream>();
    	//加载多个XML配置
    	for (int i=0; i<confA.length; i++)
    	{
    		InputStream inputStream = context.getResourceAsStream(confA[i]);	
    		inputStreamList.add(inputStream);
    	}
    	
    	InputStream inputStreamA[] = (InputStream[])inputStreamList.toArray(new InputStream[0]);
    	if (inputStreamA == null)
        {
            log.error("unable to find urlrewrite conf file at " + DEFAULT_WEB_CONF_PATH);
            // set the writer back to null
            if (urlRewriter != null) {
                log.error("unloading existing conf");
                urlRewriter = null;
            }

        }
        else
        {
            Conf conf = new Conf(context, inputStreamA, "urlrewrite.xml");
            if (log.isDebugEnabled()) {
                if (conf.getRules() != null) {
                    log.debug("inited with " + conf.getRules().size() + " rules");
                }
                log.debug("conf is " + (conf.isOk() ? "ok" : "NOT ok"));
            }
            confLastLoaded = conf;
            if (conf.isOk()) {
                urlRewriter = new UrlRewriter(conf);
                log.info("loaded (conf ok)");

            } else {
                log.error("Conf failed to load");
                log.error("unloading existing conf");
                urlRewriter = null;
            }
        }
    }

    /**
     * Destroy is called by the application server when it unloads this filter.
     */
    public void destroy() {
        log.info("destroy called");
        destroyActual();
        this.filterConfigInternal = null;
    }

    public void destroyActual() {
        if (urlRewriter != null) {
            urlRewriter.destroy();
            urlRewriter = null;
        }
        context = null;
        confLastLoad = 0;
        confReloadCheckEnabled = false;
        confReloadCheckInterval = 0;
        confReloadInProgress = false;
    }

    /**
     * The main method called for each request that this filter is mapped for.
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException 
    {
    	HttpServletRequest httpServletRequest = (HttpServletRequest)request;
    	
	    String currentURL = httpServletRequest.getRequestURI();
	    String htmlUriKey = LocalKey.getLocalKey(currentURL);

	    _l1l1l1l1l1l1l1l1l l1l11l1ll1ll1l1l1l1l111 = new _l1l1l1l1l1l1l1l1l();
	    ////system.out.println("UrlRewriteFilter:"+htmlUriKey);
	    if ( l1l11l1ll1ll1l1l1l1l111.isContainKey(htmlUriKey) )
	    {
	    	////system.out.println("===> "+currentURL);
	    	//如果HTML已经生成，则直接跳转到静态页面
		    try
			{
		    	if (!response.isCommitted())
		    	{
			    	context.getRequestDispatcher(l1l11l1ll1ll1l1l1l1l111.getObject(htmlUriKey).toString()).forward(request,response);
				    //final RequestDispatcher rq = request.getRequestDispatcher(l1l11l1lll1l1l1l1l111);
				   // rq.forward(request,response);
		    	}		    	

		    	//return;
			}
		    catch (Exception e) 
			{
		    	//Thread.currentThread()
		    	log.error("HtmlCache Before Filter error ["+Thread.currentThread()+"] ["+response+"] Current URL:"+StringUtil.getCurrentURL((HttpServletRequest)request) +e);
			}
	    }
	    else		//没有生成HTML，流程往下个FILTER走
	    {
	        // check to see if the conf needs reloading
	        long now = System.currentTimeMillis();
	        if (confReloadCheckEnabled && !confReloadInProgress &&
	                (now - confReloadCheckInterval) > confReloadLastCheck) {
	            confReloadInProgress = true;
	            confReloadLastCheck = now;

	            log.debug("starting conf reload check");
	            long confFileCurrentTime = getConfFileLastModified();
	            if (confLastLoad < confFileCurrentTime) {
	                // reload conf
	                confLastLoad = System.currentTimeMillis();
	                log.info("conf file modified since last load, reloading");
	                loadConf();
	            } else {
	                log.debug("conf is not modified");
	            }

	            confReloadInProgress = false;
	        }

	        final HttpServletRequest hsRequest = (HttpServletRequest) request;
	        final HttpServletResponse hsResponse = (HttpServletResponse) response;
	        UrlRewriteWrappedResponse urlRewriteWrappedResponse = new UrlRewriteWrappedResponse(hsResponse, hsRequest,
	                urlRewriter);

	        // check for status request
	        if (statusEnabled && statusServerNameMatcher.isMatch(request.getServerName())) {
	            String uri = hsRequest.getRequestURI();
	            if (log.isDebugEnabled()) {
	                log.debug("checking for status path on " + uri);
	            }
	            String contextPath = hsRequest.getContextPath();
	            if (uri != null && uri.startsWith(contextPath + statusPath)) {
	                showStatus(hsRequest, urlRewriteWrappedResponse);
	                return;
	            }
	        }

	        boolean requestRewritten = false;
	        if (urlRewriter != null) {

	            // process the request
	            RewrittenUrl rewrittenUrl = urlRewriter.processRequest(hsRequest, urlRewriteWrappedResponse);
	            if (rewrittenUrl != null) {
	                requestRewritten = rewrittenUrl.doRewrite(hsRequest, urlRewriteWrappedResponse, chain);
	            }
	        } else {
	            if (log.isDebugEnabled()) {
	                log.debug("urlRewriter engine not loaded ignoring request (could be a conf file problem)");
	            }
	        }

	        // if no rewrite has taken place continue as normal
	        if (!requestRewritten) {
	            chain.doFilter(hsRequest, urlRewriteWrappedResponse);
	        }
	    	
	    }

    }


    /**
     * Gets the last modified date of the conf file.
     *
     * @return time as a long
     */
    private long getConfFileLastModified() {
        File confFile = new File(context.getRealPath(DEFAULT_WEB_CONF_PATH));
        return confFile.lastModified();
    }


    /**
     * Show the status of the conf and the filter to the user.
     *
     * @param request
     * @param response
     * @throws java.io.IOException
     */
    private void showStatus(final HttpServletRequest request, final ServletResponse response)
            throws IOException {

        log.debug("showing status");

        Status status = new Status(confLastLoaded, this);
        status.displayStatusInContainer(request);

        response.setContentLength(status.getBuffer().length());

        final PrintWriter out = response.getWriter();
        out.write(status.getBuffer().toString());
        out.close();

    }

    public boolean isConfReloadCheckEnabled() {
        return confReloadCheckEnabled;
    }

    /**
     * The amount of seconds between reload checks.
     */
    public int getConfReloadCheckInterval() {
        return confReloadCheckInterval / 1000;
    }

    public Date getConfReloadLastCheck() {
        return new Date(confReloadLastCheck);
    }

    public boolean isStatusEnabled() {
        return statusEnabled;
    }

    public String getStatusPath() {
        return statusPath;
    }

    public boolean isLoaded() {
        return urlRewriter != null;
    }
}
