package org.tuckey.web.filters.urlrewrite;

import java.io.IOException;




import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.log4j.Logger;

import com.cwc.app.util.Environment;
import com.cwc.util.LCB;


public class UrlRewriteWrappedResponse extends HttpServletResponseWrapper {

	static Logger log = Logger.getLogger("PLATFORM");
    private UrlRewriter urlRerwiter;
    private HttpServletResponse httpServletResponse;
    private HttpServletRequest httpServletRequest;

    public UrlRewriteWrappedResponse(HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest,
                                     UrlRewriter urlRerwiter) {
        super(httpServletResponse);
        this.httpServletResponse = httpServletResponse;
        this.httpServletRequest = httpServletRequest;
        this.urlRerwiter = urlRerwiter;
    }

    public String encodeURL(String s) {
        RewrittenOutboundUrl rou = processPreEncodeURL(s);
        if (rou == null) {
            return super.encodeURL(s);
        }
        if (rou.isEncode()) {
            rou.setTarget(super.encodeURL(rou.getTarget()));
        }
        return processPostEncodeURL(rou.getTarget()).getTarget();
    }

    public String encodeRedirectURL(String s) {
        RewrittenOutboundUrl rou = processPreEncodeURL(s);
        if (rou == null) {
            return super.encodeURL(s);
        }
        if (rou.isEncode()) {
            rou.setTarget(super.encodeRedirectURL(rou.getTarget()));
        }
        return processPostEncodeURL(rou.getTarget()).getTarget();
    }

    public String encodeUrl(String s) {
        RewrittenOutboundUrl rou = processPreEncodeURL(s);
        if (rou == null) {
            return super.encodeURL(s);
        }
        if (rou.isEncode()) {
//          rou.setTarget(super.encodeUrl(rou.getTarget()));
        	rou.setTarget(super.encodeURL(rou.getTarget()));
        }
        return processPostEncodeURL(rou.getTarget()).getTarget();
    }

    public String encodeRedirectUrl(String s) {
        RewrittenOutboundUrl rou = processPreEncodeURL(s);
        if (rou == null) {
            return super.encodeURL(s);
        }
        if (rou.isEncode()) {
//          rou.setTarget(super.encodeRedirectUrl(rou.getTarget()));
        	rou.setTarget(super.encodeRedirectURL(rou.getTarget()));
            
        }
        return processPostEncodeURL(rou.getTarget()).getTarget();
    }

    /**
     * Handle rewriting.
     *
     * @param s
     */
    private RewrittenOutboundUrl processPreEncodeURL(String s) {
        if (urlRerwiter == null) {
            return null;
        }
        return urlRerwiter.processEncodeURL(httpServletResponse, httpServletRequest, false, s);
    }

    /**
     * Handle rewriting after the containers encodeUrl has been called.
     *
     * @param s
     */
    private RewrittenOutboundUrl processPostEncodeURL(String s) {
        if (urlRerwiter == null) {
            return null;
        }
        return urlRerwiter.processEncodeURL(httpServletResponse, httpServletRequest, true, s);
    }
    
    public static void main(String args[]) throws IOException
    {

    }

}
