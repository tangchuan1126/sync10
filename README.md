VVME ERP开发工程Spring+JUnit 4单元测试规范
=====================================

## 开发人员构建自己的单元测试代码

### 1. 创建自己的单元测试源码包路径

每个人按自己的姓名拼音缩写在com.cwc.test下创建子包，例如`com.cwc.test.zj`

### 2. 拷贝单元测试骨架代码

每个人从`com.cwc.test.demo`下拷贝全部文件到上述自己的子包，目前这些文件是：

- BaseTestCase.java

- TestSuite.java
 
- applicationContext.xml

- DemoTestCase1.java

- DemoTestCase2.java

**下文所做的修改全部是基于拷贝到自己子包中的那些文件，而不要修改demo子包下的文件！！！**

### 3. 简捷配置自己需要的测试

最简单的配置方法是：

1. 修改`BaseTestCase.java`，找到以下行：

		@ContextConfiguration("classpath:/com/cwc/test/demo/applicationContext.xml")
		
	将其中的`demo`改为自己的子包名，例如：
	
		@ContextConfiguration("classpath:/com/cwc/test/zj/applicationContext.xml")
		

2. 修改`applicationContext.xml`，在其中加入自己需要测试的业务Bean声明。例如，如需测试`com.cwc.app.floor.api.zj.FloorLocationMgrZJ`，则在其中加入以下行：

		<bean id="floorLocationMgrZJ" class="com.cwc.app.floor.api.zj.FloorLocationMgrZJ" depends-on="dbUtilAutoTran">
  			<property name="dbUtilAutoTran" ref="dbUtilAutoTran"/>
  		</bean>
  		
  	具体声明的依赖，可以参考`WebRoot/WEB-INF/conf`下的配置文件。
  
  
3. 修改DemoTestCase1.java或DemoTestCase2.java任意，在声明部分加入自己需要测的业务Bean的注入字段及标注，例如：

		@Resource
		private FloorLocationMgrZJ floorLocationMgrZJ;
		
4. 修改上述DemoTestCase1.java或DemoTestCase2.java，加入需要的测试方法。例如，如需测试`FloorLocationMgrZJ.getLocationAreaByPsid`方法，则可加入类似下面的测试方法：

		@Test
		public void test_getLocationAreaByPsid() throws Exception{
			DBRow[] rows = floorLocationMgrZJ.getLocationAreaByPsid(100000);
			assertTrue(rows != null && rows.length>0);
			//... 其他校验结果是否正确的逻辑
		}
		
5. 如有需要，可以加入更多的测试用例类，完全仿照`DemoTestCase1`、`DemoTestCase2`，必须是继承本子包下的`BaseTestCase`，例如自己新增测试用例类`MyTestCase`：

		public class MyTestCase extends BaseTestCase {
			@Resource
			private FloorLocationMgrZJ floorLocationMgrZJ;
			
			@Test
			public void test_getLocationAreaByPsid() throws Exception{
				DBRow[] rows = floorLocationMgrZJ.getLocationAreaByPsid(100000);
				assertTrue(rows != null && rows.length>0);
				//... 其他校验结果是否正确的逻辑
			}
		}

	同时，必须在本子包下的`TestSuite`类（用于定义一组系列测试）中加入其名称，找到如下行：

		@SuiteClasses({ DemoTestCase1.class,DemoTestCase2.class })
		
	增加自己的测试用例类：

		@SuiteClasses({ DemoTestCase1.class,DemoTestCase2.class,MyTestCase.class })

	**（其他依次类推）**

6. 定制环境：默认情况下，本测试框架使用com.cwc.test包下的`env.properties`文件来设置测试环境参数，如下：
	
		db.host=localhost     		#业务数据库主机地址
		db.name=1you_zhanjie  		#业务数据库名
		db.username=root      		#业务数据库访问用户
		db.password=root	  		#业务数据库访问密码
		logserv.host=192.168.1.12	#外部日志服务器地址
		neo4j.router=192.168.1.53	#外部图数据库集群路由地址
		
如果开发人员需要改变上述测试环境参数，**请不要直接修改这个文件**！！！而要按以下步骤进行定制：
	
- 在自己的测试包下，以`com.cwc.test.zj`为例，创建一个名为`env.properties`的文件
	
- 在此文件中仅写入需要覆盖默认设置的变量，例如，如果只修改`db.name`这一项为`1you`，则仅写入这一行：`db.name=1you`
	
- 修改自己测试包下的`applicationContext.xml`文件，修改`propertyConfigurer`的定义部分，加入自己的配置属性文件即可
	
			<bean id="propertyConfigurer"
	   			class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
	   		<property name="systemPropertiesModeName" value="SYSTEM_PROPERTIES_MODE_OVERRIDE" />
	    	<property name="locations">
		        <list>
		            <value>classpath:/com/cwc/test/env.properties</value>
		            <value>classpath:/com/cwc/test/zj/env.properties</value>
		        </list>
	    	</property>
			</bean>
		
这样定制后，在自己测试包下`env.properties`中指定的环境变量将会覆盖默认的环境变量，而自己未指定的环境变量仍然使用默认值。

例如，他需测试图数据库相关的业务对象，在zj测试包下`applicationContext.xml`增加了有关Bean的定义：

		……
		<bean id="productStoreMgr" class="com.cwc.app.floor.api.FloorProductStoreMgr">
			<property name="dbUtilAutoTran" ref="dbUtilAutoTran" />
			<property name="restBaseUrl" value="http://${neo4j.router}" />
		</bean>
		……

而上述配置中使用了环境变量`neo4j.router`，则他可在zj测试包下的`env.properties`指定此环境变量的值：

		neo4j.router=192.168.1.53
 


### 4. 执行测试


#### 单独执行一个测试用例（TestCase）

在Eclipse开发环境下，右键点击Package Explorer窗口中自己子包下的测试用例类（例如，DemoTestCase1或DemoTestCase2），选择`Run As -> JUnit Test`

#### 连续执行一系列测试用例

在Eclipse开发环境下，右键点击Package Explorer窗口中自己子包下的TestSuite，选择`Run As -> JUnit Test`，将会执行一系列测试（原始配置是DemoTestCase1，DemoTestCase2）


### 5. Spring+JUnit 4单元测试基本知识


#### 5.1 测试用例类基本结构

JUnit 4与之前的版本不同，测试用例类无须继承任何类。它主要是依靠标注（Annotation）来识别测试方法的。测试方法可以是任意名称，只要添加`@Test`标注即可。例如：

	import org.junit.Test;
	......
	@Test public void myTestCase() { ... }
	

在测试用例类的每个测试方法，可以对测试目标（例如，某个业务对象POJO）的方法进行调用，然后验证逻辑（验证返回值等）。为了便于验证逻辑， JUnit提供了大量的断言函数，用于触发测试失败的异常，例如：

	@Test public void myTestCase(){
		ProductStoreLogBean psLog = new ProductStoreLogBean();
		psLog.setOid(random(1000000, 100000));
		...
		String returnId = logMgr.availableProductStoreLog(psLog, false);
		assertTrue(returnId != null && returnId.length() > 0);
	}
	

#### 5.2 测试方法标注

通常，使用这些断言函数足以根据自己的需要判断测试结果（并在测试未通过时引发断言失败异常）。但还有一种情况，就是唯有当测试目标方法抛出异常时，才认为测试通过，这种情况下，可以使用`@Test`标注类的`expected`参数，例如：

	@Test(expected = SQLException.class)
	public void myDatabaseErrorTestCase(){ ... }
	
上述例子中的测试方法仅在此方法抛出了`SQLException`类型的异常时才会认为测试通过。

除了`expected`, 注解`org.junit.Test`的另一个参数`timeout`，指定被测试方法被允许运行的最长时间应该是多少，如果测试方法运行时间超过了指定的毫秒数，则JUnit认为测试失败。这个参数对于性能测试有一定的帮助。例如，如果解析一份自定义的 XML 文档花费了多于 1 秒的时间，就需要重新考虑 XML 结构的设计，那单元测试方法可以这样来写：

 	@Test(timeout=1000) 
	 public void selfXMLReader(){ 
			……
	 }

有时，某个测试方法虽然存在，但暂时不希望执行（也不想去掉），而是希望先忽略，此时可以在前面加上`@Ignore`标注。加上`@Ignore`和将`@Test`注释掉是不同的效果，因为在开发工具的JUnit测试结果的显示界面上，会列出忽略的测试方法的名称（只是没有执行而已）；注释掉某方法前的`@Test`以后，虽然那个测试方法也不会执行，但JUnit环境也不知道存在那个测试方法，这通常不是我们想要的效果，因为我们需要清楚地知道哪些测试被暂时忽略了。

	@Ignore 
	@Test 
	public void myTestCase() { ... }
	

#### 5.3 测试用例类的Fixture

何谓 Fixture ？它是指在执行一个或者多个测试方法时需要的一系列公共资源或者数据，例如测试环境，测试数据等等。在编写单元测试的过程中，您会发现在大部分的测试方法在进行真正的测试之前都需要做大量的铺垫——为设计准备 Fixture 而忙碌。这些铺垫过程占据的代码往往比真正测试的代码多得多，而且这个比率随着测试的复杂程度的增加而递增。当多个测试方法都需要做同样的铺垫时，重复代码的“坏味道”便在测试代码中弥漫开来。这股“坏味道”会弄脏您的代码，还会因为疏忽造成错误，应该使用一些手段来根除它。
JUnit 专门提供了设置公共 Fixture 的方法，同一测试类中的所有测试方法都可以共用它来初始化 Fixture 和注销 Fixture。和编写 JUnit 测试方法一样，公共 Fixture 的设置也很简单，您只需要：

1. 使用注解 org,junit.Before 修饰用于初始化 Fixture 的方法。
2. 使用注解 org.junit.After 修饰用于注销 Fixture 的方法。
3. 保证这两种方法都使用 public void 修饰，而且不能带有任何参数。

例如：

	// 初始化 Fixture 方法
 	@Before public void init(){ …… } 

 	// 注销 Fixture 方法
 	@After public void destroy(){ …… }

 
`@Before`或`@After`方法一旦存在，他们是在该测试类的每个测试方法（每个标注了`@Test`的方法）执行之前或之后来执行的。所以，这在有些情况下可能不是你想要的效果，比如如果想让所有测试方法共享同一次数据初始化的情况。

在 JUnit 4 中引入了类级别的 Fixture 设置方法，编写规范如下：  

1. 使用注解 org,junit.BeforeClass 修饰用于初始化 Fixture 的方法。 
2. 使用注解 org.junit.AfterClass 修饰用于注销 Fixture 的方法。 
3. 保证这两种方法都使用 public static void 修饰，而且不能带有任何参数。 

类级别的 Fixture 仅会在测试类中所有测试方法执行之前执行初始化，并在全部测试方法测试完毕之后执行注销方法，例如：

	// 类级别 Fixture 初始化方法
 	@BeforeClass public static void dbInit(){ …… } 
	
 	// 类级别 Fixture 注销方法
	@AfterClass public static void dbClose(){ …… }


#### 5.4 测试套件（Test Suite）

在实际项目中，随着项目进度的开展，单元测试类会越来越多，可是直到现在我们还只会一个一个的单独运行测试类，这在实际项目实践中肯定是不可行的。为了解决这个问题，JUnit提供了一种批量运行测试类的方法，叫做测试套件。这样，每次需要验证系统功能正确性时，只执行一个或几个测试套件便可以了。测试套件的写法非常简单，您只需要遵循以下规则：

1. 创建一个空类作为测试套件的入口。
2. 使用注解 org.junit.runner.RunWith 和 org.junit.runners.Suite.SuiteClasses 修饰这个空类。
3. 将 org.junit.runners.Suite 作为参数传入注解 RunWith，以提示 JUnit 为此类使用套件运行器执行。
4. 将需要放入此测试套件的测试类组成数组作为注解 SuiteClasses 的参数。
5. 保证这个空类使用 public 修饰，而且存在公开的不带有任何参数的构造函数。
 
例如：

 	import org.junit.runner.RunWith; 
 	import org.junit.runners.Suite; 
	……

 
 	@RunWith(Suite.class) 
 	@Suite.SuiteClasses({TestCase1.class,TestCase2.class,TestCase3.class}) 
 	public class RunAllUtilTestsSuite { 
 	}
 	

#### 5.5 JUnit与Spring的集成

JUnit的测试方法是由测试运行器来执行的，它为单元测试提供了默认的测试运行器，但JUnit并没有限制必须使用默认的运行器，Spring框架正式利用这个机制，提供自己定制的测试运行器，以达到让JUnit能测试Spring IOC容器管理的业务对象。

	import org.junit.runner.RunWith;
	import org.springframework.test.context.ContextConfiguration;
	import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

	@RunWith(SpringJUnit4ClassRunner.class)
	@ContextConfiguration("classpath:/com/cwc/test/demo/applicationContext.xml")
	public  class MyTestCase {
	 	……	
	}

上述`@RunWith`标注指定本测试类中的测试方法由SpringJUnit4ClassRunner这个运行器来执行，而不是默认，同时使用Spring框架的`@ContextConfiguration`标注指定了Spring的配置文件。
这个Spring的测试运行类会根据配置文件初始化业务Bean建立IOC容器，并且它还能识别在此类中以`javax.annotation.Resource`标注的实例变量，并自动进行注入，例如：

	@RunWith(SpringJUnit4ClassRunner.class)
	@ContextConfiguration("classpath:/com/cwc/test/demo/applicationContext.xml")
	public class DemoTestCase1 extends BaseTestCase {
	
		@Resource	//自动注入标记
		private FloorAdminMgr floorAdminMgr;  //Spring测试运行类查找IOC容器找到此类型Bean并注入

		@Test
		public void test_1() throws Exception{
			long adid = 100025;
			String account = "hanlong";
			DBRow row = floorAdminMgr.getDetailAdmin(adid);
			assertEquals(row.getString("account"),account);
		}
	}
	
但是，如果每个测试用例类都用`@RunWith`和`@ContextConfiguration`的话，当执行一组测试用例类（Test Suite）的时候，会出现这种情况：

1. 每个测试用例类都会起一个独立的Spring IOC环境，互不相关
2. 在每个测试用例类上面都写上`@RunWith`和`@ContextConfiguration`，显得比较繁琐

那么，针对这个问题的解决方案就是：

1. 定义一个这一组相关的测试用例类的公共基类，在这个基类上加标注`@RunWith`和`@ContextConfiguration`标注
2. 其他测试用例类均继承这一基类，但不加`@RunWith`和`@ContextConfiguration`标注

这样，这一组测试用例类在执行时，仅仅会创建一个公共的Spring IOC环境，这通常正是所需要的效果。这也是我们这套测试框架的实现方法。
